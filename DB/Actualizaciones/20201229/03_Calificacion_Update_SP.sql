DROP procedure IF EXISTS `Calificacion_Update_SP`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Calificacion_Update_SP`(
IN _Id INT,
IN _SituacionId INT,
IN _InstitucionId INT,
IN _Fundamento VARCHAR(6800),
IN _TrabajoSocial TINYINT(1),
IN _TotalHoras INT,
IN _SoloArresto TINYINT(1),
IN _TotalDeMultas DECIMAL(10,2),
IN _Agravante DECIMAL(10,2),
IN _Ajuste DECIMAL(10,2),
IN _TotalAPagar DECIMAL(10,2),
IN _Razon VARCHAR(6800),
IN _Activo TINYINT(1),
IN _InternoId INT
)
BEGIN
		
    UPDATE `calificacion`
    SET SituacionId = _SituacionId,
        InstitucionId = _InstitucionId,
        Fundamento = _Fundamento,
        TrabajoSocial = _TrabajoSocial,
        TotalHoras = _TotalHoras,
		SoloArresto = _SoloArresto,
        TotalDeMultas = _TotalDeMultas,
        Agravante = _Agravante,
        Ajuste = _Ajuste,
        TotalAPagar = _TotalAPagar,
        Razon = _Razon,
        Activo = _Activo
	WHERE Id = _Id;
    
END$$

DELIMITER ;

