drop procedure if exists HistorialCalificacion_update_SP;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `HistorialCalificacion_update_SP`(
IN _Id INT,
IN _TrackingId VARCHAR(36),
IN _InternoId INT,
IN _Movimiento VARCHAR(512),
IN _Fecha DATETIME,
IN _CreadoPor INT,
IN _Descripcion VARCHAR(6800)
)
BEGIN

UPDATE `historialcalificacion`
SET

`TrackingId` = _TrackingId,
`InternoId` =_InternoId,
`Movimiento` =_Movimiento,
`Fecha` = _Fecha,
`CreadoPor` = _CreadoPor,
`Descripcion` = _Descripcion
WHERE `Id` = _Id;



END$$
DELIMITER ;
drop procedure if exists HistorialCalificacion_update_SP;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `HistorialCalificacion_update_SP`(
IN _Id INT,
IN _TrackingId VARCHAR(36),
IN _InternoId INT,
IN _Movimiento VARCHAR(512),
IN _Fecha DATETIME,
IN _CreadoPor INT,
IN _Descripcion VARCHAR(6800)
)
BEGIN

UPDATE `historialcalificacion`
SET

`TrackingId` = _TrackingId,
`InternoId` =_InternoId,
`Movimiento` =_Movimiento,
`Fecha` = _Fecha,
`CreadoPor` = _CreadoPor,
`Descripcion` = _Descripcion
WHERE `Id` = _Id;



END$$
DELIMITER ;

drop procedure if exists HistorialCalificacion_Insert_SP;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `HistorialCalificacion_Insert_SP`(
OUT _NewIdentity INT,
IN _TrackingId VARCHAR(36),
IN _InternoId INT,
IN _Movimiento VARCHAR(512),
IN _Fecha DATETIME,
IN _CreadoPor INT,
IN _Descripcion VARCHAR(6800)
)
BEGIN
INSERT INTO `historialcalificacion`
(`TrackingId`,
	`InternoId`,
	`Movimiento`,
    `Fecha`,
    `Activo`,
    `Habilitado`,
    `CreadoPor`,
	`Descripcion`)
	VALUES
	(_TrackingId,
     _InternoId,
	_Movimiento,
    _Fecha,
    1,
    1,
    _CreadoPor,
	_Descripcion);

    SET _NewIdentity = LAST_INSERT_ID(); 

END