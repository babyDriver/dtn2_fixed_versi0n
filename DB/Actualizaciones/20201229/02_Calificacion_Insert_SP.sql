DROP procedure IF EXISTS `Calificacion_Insert_SP`;

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Calificacion_Insert_SP`(
OUT _NewIdentity INT,
IN _TrackingId VARCHAR(36),
IN _InternoId INT,
IN _SituacionId INT,
IN _InstitucionId INT,
IN _Fundamento VARCHAR(6800),
IN _TrabajoSocial TINYINT(1),
IN _TotalHoras INT,
IN _SoloArresto TINYINT(1),
IN _TotalDeMultas DECIMAL(10,2),
IN _Agravante DECIMAL(10,2),
IN _Ajuste DECIMAL(10,2),
IN _TotalAPagar DECIMAL(10,2),
IN _Razon VARCHAR(6800),
IN _CreadoPor  INT
)
BEGIN
	INSERT INTO `calificacion`
	(`TrackingId`,
	`InternoId`,
	`SituacionId`,
	`InstitucionId`,
    `Fundamento`,
    `TrabajoSocial`,
    `TotalHoras`,
    `SoloArresto`,
    `TotalDeMultas`,
    `Agravante`,
    `Ajuste`,
    `TotalAPagar`,
    `Razon`,
    `Activo`,
    `Habilitado`,
    `CreadoPor`,
    `Calificacionanioregistro`,
    `Calificacionmesregistro`
    )
	VALUES
	(_TrackingId,
    _InternoId,
	_SituacionId,
	_InstitucionId,
    _Fundamento,
    _TrabajoSocial,
    _TotalHoras,
    _SoloArresto,
    _TotalDeMultas,
    _Agravante,
    _Ajuste,
    _TotalAPagar,
    _Razon,
    1,
    1,
    _CreadoPor,
    year(CURDATE()),
    month(curdate()));
    
    SET _NewIdentity = last_insert_id();
    
END$$

DELIMITER ;

