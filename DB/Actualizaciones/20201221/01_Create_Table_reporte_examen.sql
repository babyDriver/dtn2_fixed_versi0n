DROP TABLE IF EXISTS `reporte_examen`;

CREATE TABLE `reporte_examen` (
	`IdDetenido` int(11) NULL,
    `IdDetalle` int(11) NULL,
    `TrackingId` varchar(36) NULL,
	`TrackingIdDetalle` varchar(36) NULL,
	`Nombre` varchar(256) NULL,
    `Paterno` varchar(256) NULL,
    `Materno` varchar(256) NULL,
	`Activo` tinyint(1) NULL,
    `Expediente` varchar(256) NULL,
    `ActivoExamen` tinyint(1) NULL,
    `IdExamen` int NULL,
    `FechaExamen` datetime NULL,
    `TrackingIdExamen` varchar(36) NULL,
	`Tipo` varchar(45) NULL,
	`ContratoId` int(11) NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


