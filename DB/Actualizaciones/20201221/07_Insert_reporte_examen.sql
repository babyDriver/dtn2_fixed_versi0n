INSERT INTO reporte_examen 
SELECT  
		D.DetenidoId , 
		D.Id     , 
		DD.TrackingId     , 
		D.TrackingId     , 
		DD.Nombre     , 
		DD.Paterno     , 
		DD.Materno     , 
		D.Activo     , 
		D.Expediente , 
		E.Activo     , 
		E.Id     , 
		E.HoraYFecha     , 
		E.TrackingId,
        D.Tipo ,
	    D.ContratoId
	
FROM examen_medico E 
INNER JOIN detalle_detencion D ON E.DetalleDetencionId = D.Id 
INNER JOIN detenido DD ON DD.Id = D.DetenidoId 
LEFT JOIN general G ON G.DetenidoId = DD.Id;