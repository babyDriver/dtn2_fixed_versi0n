DROP TRIGGER IF EXISTS nuevoExamen;

DELIMITER $$

CREATE TRIGGER nuevoExamen
    AFTER INSERT
    ON examen_medico FOR EACH ROW
BEGIN
	
	DECLARE _fechas VARCHAR(4000);
    DECLARE _indicaciones VARCHAR(4000);
    
    /*Variables del detenido*/
    DECLARE _idDetenido INT;
    DECLARE _idDetalle INT;
    DECLARE _tkgDetenido VARCHAR(36);
    DECLARE _tkgDetalle VARCHAR(36);
    DECLARE _nombre VARCHAR(256);
    DECLARE _paterno VARCHAR(256);
    DECLARE _materno VARCHAR(256);
    DECLARE _activoDetalle TINYINT(1);
    DECLARE _expediente VARCHAR(256);
    DECLARE _contratoId INT;
    DECLARE _tipo VARCHAR(256);
    
    SELECT Id, TrackingId, Expediente, Activo, DetenidoId, ContratoId, Tipo INTO _idDetalle, _tkgDetalle, _expediente, _activoDetalle, _idDetenido, _contratoId, _tipo
    FROM detalle_detencion
    WHERE Id = NEW.DetalleDetencionId;
    
    SELECT Nombre, Paterno, Materno, TrackingId INTO _nombre, _paterno, _materno, _tkgDetenido
    FROM detenido
    WHERE Id = _idDetenido;
    
    SELECT group_concat(HoraYFecha) fechas, group_concat(IndicacionesMedicas) indicaciones INTO _fechas, _indicaciones
	FROM examen_medico
    WHERE DetalleDetencionId = NEW.DetalleDetencionId
	GROUP BY DetalleDetencionId;
    
    UPDATE reporte_remision
	SET FechasExamen = _fechas
	  , IndicacionesExamen = _indicaciones
	WHERE IdDetalle = NEW.DetalleDetencionId;
    
    INSERT INTO reporte_examen (
    `IdDetenido`,
	`IdDetalle`,
	`TrackingId`,
	`TrackingIdDetalle`,
	`Nombre`,
	`Paterno`,
	`Materno`,
	`Activo`,
	`Expediente`,
	`ActivoExamen`,
	`IdExamen`,
	`FechaExamen`,
	`TrackingIdExamen`,
    `ContratoId`,
    `Tipo`) VALUES
	(_idDetenido,
	_idDetalle,
	_tkgDetenido,
	_tkgDetalle,
	_nombre,
	_paterno,
	_materno,
	_activoDetalle,
	_expediente,
	NEW.Activo,
	NEW.Id,
	NEW.HoraYFecha,
	NEW.TrackingId,
    _contratoId,
    _tipo);

    
END$$    

DELIMITER ;