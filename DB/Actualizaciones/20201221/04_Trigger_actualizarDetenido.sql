DROP TRIGGER IF EXISTS actualizarDetenido;

DELIMITER $$

CREATE TRIGGER actualizarDetenido
    AFTER UPDATE
    ON detenido FOR EACH ROW
BEGIN

	DECLARE _domicilioId INT;
    DECLARE _paisId INT;
    DECLARE _estadoId INT;
    DECLARE _municipioId INT;
    DECLARE _coloniaId INT;
    DECLARE _calle VARCHAR(256);
    DECLARE _telefono VARCHAR(45);

	SELECT DomicilioId INTO _domicilioId FROM Detenido WHERE Id = NEW.Id;
    
    IF _domicilioId is not null and _domicilioId > 0 THEN
    
		IF OLD.Id = NEW.Id THEN
    
			SELECT PaisId, EstadoId, MunicipioId, ColoniaId, Calle, Telefono INTO _paisId, _estadoId, _municipioId, _coloniaId, _calle, _telefono
			FROM domicilio
			WHERE Id = _domicilioId;
    
			UPDATE informacion_detenidos_busqueda
			SET Nombre = NEW.Nombre
			  , Apellido_Paterno = NEW.Paterno
			  , Apellido_Materno = NEW.Materno
			  , RutaImagen = NEW.RutaImagen
			  , Ultima_detencion_id = NEW.ultima_detencion_id
              , PaisId = _paisId
              , EstadoId = _estadoId
              , MunicipioId = _municipioId
              , ColoniaId = _coloniaId
              , Calle = _calle
              , Telefono = _telefono
			WHERE DetenidoId = NEW.Id;
            
            UPDATE reporte_remision
			SET Nombre = NEW.Nombre
			  , Apellido_Paterno = NEW.Paterno
			  , Apellido_Materno = NEW.Materno
			  , RutaImagen = NEW.RutaImagen			  
			WHERE DetenidoId = NEW.Id;
		
			UPDATE reporte_examen
            SET Nombre = NEW.Nombre
			  , Paterno = NEW.Paterno
              , Materno = NEW.Materno
			WHERE IdDetenido = NEW.Id;
        
		END IF;
    
    ELSE
    
		IF OLD.Id = NEW.Id THEN
    
			UPDATE informacion_detenidos_busqueda
			SET Nombre = NEW.Nombre
			  , Apellido_Paterno = NEW.Paterno
			  , Apellido_Materno = NEW.Materno
			  , RutaImagen = NEW.RutaImagen
			  , Ultima_detencion_id = NEW.ultima_detencion_id
			WHERE DetenidoId = NEW.Id;    
		
			UPDATE reporte_remision
			SET Nombre = NEW.Nombre
			  , Apellido_Paterno = NEW.Paterno
			  , Apellido_Materno = NEW.Materno
			  , RutaImagen = NEW.RutaImagen			  
			WHERE DetenidoId = NEW.Id;
        
			UPDATE reporte_examen
            SET Nombre = NEW.Nombre
			  , Paterno = NEW.Paterno
              , Materno = NEW.Materno
			WHERE IdDetenido = NEW.Id;
        
		END IF;
    
    END IF;	
    
END$$    

DELIMITER ;