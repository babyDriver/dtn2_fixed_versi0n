DROP PROCEDURE IF EXISTS ListadoReporteExamenMedico_GetAll_SP;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListadoReporteExamenMedico_GetAll_SP`(
IN _contratoId INT,
IN _tipo VARCHAR(256),
IN _activo TINYINT(1)
)
BEGIN

	SELECT R.IdDetenido
		 , R.IdDetalle
		 , R.TrackingId
		 , R.TrackingIdDetalle
		 , R.Nombre
		 , R.Paterno
		 , R.Materno
		 , R.Activo
		 , R.Expediente     
		 , R.ActivoExamen
		 , R.IdExamen
		 , R.FechaExamen
		 , R.TrackingIdExamen
		 , S.Nombre Sexo  
	FROM reporte_examen R
	LEFT JOIN general G
	ON R.IdDetenido = G.DetenidoId
	LEFT JOIN sexo S
	ON S.Id = G.SexoId
    WHERE R.ContratoId = _contratoId AND R.Tipo = _tipo AND R.ActivoExamen = _activo;

END$$
DELIMITER ;
