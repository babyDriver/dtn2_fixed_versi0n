DROP TRIGGER IF EXISTS actualizarExamen;

DELIMITER $$

CREATE TRIGGER actualizarExamen
    AFTER UPDATE
    ON examen_medico FOR EACH ROW
BEGIN
	
	DECLARE _fechas VARCHAR(4000);
    DECLARE _indicaciones VARCHAR(4000);
    
    /*Variables del detenido*/
    DECLARE _idDetenido INT;
    DECLARE _idDetalle INT;
    DECLARE _tkgDetenido VARCHAR(36);
    DECLARE _tkgDetalle VARCHAR(36);
    DECLARE _nombre VARCHAR(256);
    DECLARE _paterno VARCHAR(256);
    DECLARE _materno VARCHAR(256);
    DECLARE _activoDetalle TINYINT(1);
    DECLARE _expediente VARCHAR(256);
	DECLARE _contratoId INT;
    DECLARE _tipo VARCHAR(256);
    
    SELECT Id, TrackingId, Expediente, Activo, DetenidoId, ContratoId, Tipo INTO _idDetalle, _tkgDetalle, _expediente, _activoDetalle, _idDetenido, _contratoId, _tipo
    FROM detalle_detencion
    WHERE Id = NEW.DetalleDetencionId;
    
    SELECT Nombre, Paterno, Materno, TrackingId INTO _nombre, _paterno, _materno, _tkgDetenido
    FROM detenido
    WHERE Id = _idDetenido;
    
    SELECT group_concat(HoraYFecha) fechas, group_concat(IndicacionesMedicas) indicaciones INTO _fechas, _indicaciones
	FROM examen_medico
    WHERE DetalleDetencionId = NEW.DetalleDetencionId
	GROUP BY DetalleDetencionId;
    
    UPDATE reporte_remision
	SET FechasExamen = _fechas
	  , IndicacionesExamen = _indicaciones
	WHERE IdDetalle = NEW.DetalleDetencionId;
    
    UPDATE reporte_examen
    SET Nombre = _nombre
	  , Paterno = _paterno
      , Materno = _materno
      , Activo = _activoDetalle
      , ActivoExamen = NEW.Activo
      , FechaExamen = NEW.HoraYFecha
      , ContratoId = _contratoId
      , Tipo = _tipo
	WHERE IdExamen = NEW.Id;      
    
END$$    

DELIMITER ;