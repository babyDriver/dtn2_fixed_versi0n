DROP TRIGGER IF EXISTS nuevoDetenido;

DELIMITER $$

CREATE TRIGGER nuevoDetenido
    AFTER INSERT
    ON detenido FOR EACH ROW
BEGIN
	
    DECLARE _domicilioId INT;
    DECLARE _paisId INT;
    DECLARE _estadoId INT;
    DECLARE _municipioId INT;
    DECLARE _coloniaId INT;
    DECLARE _calle VARCHAR(256);
    DECLARE _telefono VARCHAR(45);
    
    SELECT DomicilioId INTO _domicilioId FROM Detenido WHERE Id = NEW.Id;
    
    IF _domicilioId is not null and _domicilioId > 0 THEN
		        
		SELECT PaisId, EstadoId, MunicipioId, ColoniaId, Calle, Telefono INTO _paisId, _estadoId, _municipioId, _coloniaId, _calle, _telefono
		FROM domicilio
		WHERE Id = _domicilioId;
	
		INSERT INTO informacion_detenidos_busqueda (DetenidoId
										  , TrackingId
										  , Nombre
										  , Apellido_Paterno
										  , Apellido_Materno
										  , RutaImagen
										  , Ultima_detencion_id
										  , PaisId
										  , EstadoId
										  , MunicipioId
										  , ColoniaId
                                          , Calle
                                          , Telefono)
		VALUES (NEW.Id, NEW.TrackingId, NEW.Nombre, NEW.Paterno, NEW.Materno, NEW.RutaImagen, NEW.ultima_detencion_id, _paisId, _estadoId, _municipioId, _coloniaId, _calle, _telefono);
        
       
	ELSE		
        
		INSERT INTO informacion_detenidos_busqueda (DetenidoId
										  , TrackingId
										  , Nombre
										  , Apellido_Paterno
										  , Apellido_Materno
										  , RutaImagen
										  , Ultima_detencion_id)
		VALUES (NEW.Id, NEW.TrackingId, NEW.Nombre, NEW.Paterno, NEW.Materno, NEW.RutaImagen, NEW.ultima_detencion_id);        
        
       
	END IF;    

    INSERT INTO reporte_remision (DetenidoId, Nombre, Apellido_Paterno, Apellido_Materno, RutaImagen) VALUES(NEW.Id, NEW.Nombre, NEW.Paterno, NEW.Materno, NEW.RutaImagen);
 
END$$    

DELIMITER ;