

UPDATE reporte_remision
inner join calificacion on calificacion.InternoId = reporte_remision.IdDetalle
inner join situacion_detenido on  calificacion.SituacionId = situacion_detenido.Id
	SET reporte_remision.CalificacionId = calificacion.Id
	  , reporte_remision.Situacion = situacion_detenido.Nombre
	  , reporte_remision.TotalHoras = calificacion.TotalHoras;
	
    
	
UPDATE informacion_detenidos_busqueda
inner join calificacion on calificacion.InternoId = informacion_detenidos_busqueda.Ultima_detencion_id
inner join situacion_detenido on  calificacion.SituacionId = situacion_detenido.Id

	SET  informacion_detenidos_busqueda.situacion_ultima_detencion = situacion_detenido.Nombre
		,informacion_detenidos_busqueda.multa_ultima_detencion = calificacion.TotalAPagar
		,informacion_detenidos_busqueda.horas_arresto_ultima_detencion = calificacion.TotalHoras;
