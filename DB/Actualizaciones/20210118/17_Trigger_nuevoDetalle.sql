DROP TRIGGER IF EXISTS nuevoDetalle;

DELIMITER $$

CREATE TRIGGER nuevoDetalle
    AFTER INSERT
    ON detalle_detencion FOR EACH ROW
BEGIN

	DECLARE _estatusNombre VARCHAR(100);
	DECLARE _estatusTracking VARCHAR(36);
    
    SELECT Nombre INTO _estatusNombre FROM estatus WHERE Id = NEW.Estatus;
	SELECT Nombre INTO _estatusTracking FROM estatus WHERE Id = NEW.Estatus;

	UPDATE informacion_detenidos_busqueda
    SET Expediente = NEW.Expediente
	  , NCP = NEW.NCP
      , Estatus = NEW.Estatus
      , Activo = NEW.Activo
      , TrackingIdEstatus = _estatusTracking
      , ContratoId = NEW.ContratoId
      , Tipo = NEW.Tipo
      , EstatusNombre = _estatusNombre
	  , fecha_registro_ultima_detencion=NEW.Fecha
	WHERE DetenidoId = NEW.DetenidoId;
    
    UPDATE reporte_remision
	SET TrackingIdDetalle = NEW.TrackingId
	  , Expediente = NEW.Expediente
      , Fecha = NEW.Fecha
      , DetalledetencionEdad = NEW.DetalledetencionEdad
      , ContratoId = NEW.ContratoId
      , Tipo = NEW.Tipo
      , IdDetalle = NEW.Id
	WHERE DetenidoId = NEW.DetenidoId;
    
END$$    

DELIMITER ;