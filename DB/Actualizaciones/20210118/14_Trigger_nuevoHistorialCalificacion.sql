DROP TRIGGER IF EXISTS nuevoHistorialCalificacion;

DELIMITER $$

CREATE TRIGGER nuevoHistorialCalificacion
    AFTER INSERT
    ON historialcalificacion FOR EACH ROW
BEGIN	
	

	UPDATE reporte_sanciones_juez
	SET Fecha = NEW.Fecha	  
	WHERE IdDetalle = NEW.InternoId;
	
END$$    

DELIMITER ;