DROP TABLE IF EXISTS `reporte_sanciones_juez`;

CREATE TABLE `reporte_sanciones_juez` (
	`Id` int(11)   NOT NULL AUTO_INCREMENT,
    `IdDetalle` int(11) NULL,
    `CalificacionId` int(11) NULL,
	`NombreJuez` varchar(36) NULL,
	`NombreDetenido` varchar(256) NULL,
    `Multa`  DECIMAL(10,2),
    `HorasArresto` int(11) NULL,
	`Activo` tinyint(1) NULL,
    `NoRemision` varchar(256) NULL,    
    `Fecha` datetime NULL,
	`ContratoId` int(11) NULL,
	 PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE reporte_sanciones_juez ADD INDEX i_rsj_contratoId (ContratoId);
ALTER TABLE reporte_sanciones_juez ADD INDEX i_rsj_fecha (Fecha);

