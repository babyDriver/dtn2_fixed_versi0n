DROP TRIGGER IF EXISTS pertenencia_AFTER_INSERT;

DELIMITER $$

CREATE TRIGGER pertenencia_AFTER_INSERT
    AFTER INSERT
    ON pertenencia FOR EACH ROW
BEGIN	
    DECLARE _FechaRegistro Datetime; 
    DECLARE _NombreDetenido VARCHAR(256); 
	DECLARE _NoRemision VARCHAR(256); 
	DECLARE _NombreRecibe VARCHAR(256); 
    DECLARE _ContratoId INT (11); 
    
    
	SELECT CURRENT_TIMESTAMP INTO _FechaRegistro ; 
	INSERT INTO `pertenencialog`
			(`Id`,
			`TrackingId`,
			`Nombre`,
			`Descripcion`,
			`Activo`,
			`Habilitado`,
			`Creadopor`,
			`Accion`,
			`AccionId`,
			`Fec_Movto`)
	VALUES
			(new.Id,
			 new.TrackingId,
			 new.Pertenencia,
			 new.Observacion,
			 new.Activo,
			 new.Habilitado,
		     new.Creadopor,
			 'Registrado',
			 1,
			_FechaRegistro);
    
	IF NEW.Clasificacion = 1 THEN
    
    SELECT CONCAT(Nombre, " ", ApellidoPaterno," ", ApellidoMaterno) INTO _NombreRecibe FROM usuario WHERE Id = NEW.Creadopor;
	SELECT CONCAT(NombreDetenido, " ", APaternoDetenido," ", AMaternoDetenido) INTO _NombreDetenido FROM detalle_detencion WHERE Id = NEW.InternoId;
	SELECT Expediente INTO _NoRemision FROM detalle_detencion WHERE Id = NEW.InternoId;
	SELECT ContratoId INTO _ContratoId FROM detalle_detencion WHERE Id = NEW.InternoId;
	
	
	insert into  reporte_pertenencias (
		PertenenciaId,
		Cantidad,
		Clasificacion,
		Pertenencia,
		Observacion,
		NoRemision,
		Detenido,
		Recibe,
		FechaRegistro,
		ContratoId,
		Activo)

	values ( 
		 NEW.Id,
		 NEW.Cantidad,
		 'Evidencia' ,
			 NEW.Pertenencia,
		 NEW.Observacion,
		 _NoRemision,
		 _NombreDetenido,
		 _NombreRecibe,
		 _FechaRegistro,
		 _ContratoId,
		 1);
	END IF;
	
END$$    

DELIMITER ;