drop procedure if exists Pertenencias_GetResumenEvidencia_SP;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Pertenencias_GetResumenEvidencia_SP`(
IN  _ContratoId int,
IN  _Fechainicio Date,
IN  _Fechafin date)
begin
Select
   NoRemision,
                                Cantidad,
                                Detenido,
                                Observacion,
                                Pertenencia,                             
                                Recibe ,
               Clasificacion,
                                 date_format(FechaRegistro , '%d-%m-%Y %H:%i') Fecha
  FROM reporte_pertenencias	
 
    WHERE  ContratoId=_ContratoId 
  and  cast(FechaRegistro as date) between _Fechainicio and _Fechafin;
end$$
DELIMITER ;
