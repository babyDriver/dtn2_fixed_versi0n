DROP TRIGGER IF EXISTS nuevoCalificacion;

DELIMITER $$

CREATE TRIGGER nuevoCalificacion
    AFTER INSERT
    ON calificacion FOR EACH ROW
BEGIN	
	
	DECLARE _Situacion VARCHAR(500);    
	DECLARE _NombreJuez VARCHAR(500);    
	DECLARE _NombreDetenido VARCHAR(150); 
	DECLARE _NoRemision VARCHAR(256); 
	DECLARE _ContratoId INT(11); 

    SELECT Nombre INTO _Situacion FROM situacion_detenido WHERE Id = NEW.SituacionId;
    SELECT CONCAT(Nombre, " ", ApellidoPaterno," ", ApellidoMaterno) INTO _NombreJuez FROM usuario WHERE Id = NEW.Creadopor;
	SELECT CONCAT(NombreDetenido, " ", APaternoDetenido," ", AMaternoDetenido) INTO _NombreDetenido FROM detalle_detencion WHERE Id = NEW.InternoId;
	SELECT Expediente INTO _NoRemision FROM detalle_detencion WHERE Id = NEW.InternoId;
	SELECT ContratoId INTO _ContratoId FROM detalle_detencion WHERE Id = NEW.InternoId;
	
	UPDATE reporte_remision
	SET CalificacionId = NEW.Id
	  , Situacion = _Situacion
	  , TotalHoras = NEW.TotalHoras
	WHERE IdDetalle = NEW.InternoId;
    
	
	UPDATE informacion_detenidos_busqueda
	SET  situacion_ultima_detencion = _Situacion
		,multa_ultima_detencion = NEW.TotalAPagar
		,horas_arresto_ultima_detencion = NEW.TotalHoras
	WHERE Ultima_detencion_id = NEW.InternoId;
	
	
	insert into  reporte_sanciones_juez (
		IdDetalle
		,calificacionId,
		NombreJuez,
		NombreDetenido,
		Multa,
		HorasArresto,
		Activo,
		NoRemision,		
		ContratoId)

values ( NEW.InternoId,
		 NEW.Id,
		 _NombreJuez ,
		 _NombreDetenido,
		 NEW.TotalAPagar ,
		 NEW.TotalHoras ,
		 1,
		 _NoRemision,
		 _ContratoId);
		
	
END$$    

DELIMITER ;