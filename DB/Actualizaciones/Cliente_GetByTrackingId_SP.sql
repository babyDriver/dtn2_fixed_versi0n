DROP PROCEDURE IF EXISTS Cliente_GetByTrackingId_SP;

DELIMITER $$
CREATE PROCEDURE Cliente_GetByTrackingId_SP(IN _TrackingId  VARCHAR(36))
BEGIN
	
		SELECT Id, TenantId, Cliente, Rfc, Activo, Habilitado, CreadoPor, AlertaWeb
        FROM cliente
        WHERE TenantId = _TrackingId;

END $$
DELIMITER ;