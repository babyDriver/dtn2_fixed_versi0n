DROP PROCEDURE IF EXISTS ListadoReporteCorteDetenidos_GetAll_SP;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListadoReporteCorteDetenidos_GetAll_SP`(
IN _tipo VARCHAR(256),
IN _contratoId INT
)
BEGIN

	SELECT CAST( R.Expediente AS unsigned) Remision
		 , CAST(R.Folio AS unsigned) Evento
		 , concat(upper(R.Nombre),' ',upper(R.Apellido_Paterno),' ',upper(R.Apellido_Materno)) Nombre
		 , R.DetalledetencionEdad Edad
		 , date_format(R.Fecha, '%Y-%m-%d %H:%i:%S') Ingreso
		 , R.Situacion Calificacion
		 , C.Nombre Celda
		 , date_format(date_add(R.HoraYFechaEvento, INTERVAL ifnull(R.TotalHoras, 36) HOUR), '%Y-%m-%d %H:%i:%S') Cumple
		 , R.Motivo
		 , ifnull(date_format(WE.FechaCapturaDetenidos,'%Y-%m-%d %H:%i:%S' ),'') Fecha
		 , ifnull(WE.Folio,'') Folio
		 , ifnull(Mo.Motivo,'') Motivos
		 , date_format(R.HoraYFecha,'%Y-%m-%d %H:%i:%S') HoraDetencion
		 , R.Unidad
		 , R.Responsable
		 , R.LugarDetencion Lugar
		 , IdDetalle 
	FROM reporte_remision R
	LEFT JOIN movimiento M
	ON M.DetalledetencionId = R.IdDetalle
	LEFT JOIN celda C
	ON M.CeldaId = C.Id
	LEFT JOIN wsaevento WE
	ON WE.Id = R.IdEventoWS
	LEFT JOIN motivodetencion MO
	ON MO.Id = WE.IdMotivo
    WHERE R.ContratoId = _contratoId AND R.Tipo = _tipo;

END$$
DELIMITER ;
