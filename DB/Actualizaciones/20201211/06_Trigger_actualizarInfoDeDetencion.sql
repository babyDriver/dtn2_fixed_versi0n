DROP TRIGGER IF EXISTS actualizarInfoDeDetencion;

DELIMITER $$

CREATE TRIGGER actualizarInfoDeDetencion
    AFTER UPDATE
    ON informaciondedetencion FOR EACH ROW
BEGIN	

	DECLARE _Id INT;
    DECLARE _IdInterno INT;
	DECLARE _motivos VARCHAR(500);
    DECLARE _HoraYFechaEvento DATETIME;
    DECLARE _folio VARCHAR(50);
    DECLARE _IdEventoWS INT;
	DECLARE _descEvento VARCHAR(4000);

    SELECT Id, IdInterno INTO _Id, _IdInterno
    FROM informaciondedetencion
    WHERE IdInterno = NEW.IdInterno
    ORDER BY HoraYFecha DESC LIMIT 1;
    
    SELECT GROUP_CONCAT(distinct w.nombreMotivoLlamada SEPARATOR ',') INTO _motivos
	FROM detenido_evento e
	INNER JOIN wsamotivo w
	ON e.MotivoId = w.idMotivo
	WHERE EventoId = NEW.IdEvento;
    
    SELECT Folio, HoraYFecha, IdEventoWS, Descripcion INTO _folio, _HoraYFechaEvento, _IdEventoWS, _descEvento
    FROM eventos
    WHERE Id = NEW.IdEvento;
    
    IF _Id = NEW.Id THEN
    
		UPDATE reporte_remision
		SET LugarDetencion = NEW.LugarDetencion
		  , IdEvento = NEW.IdEvento
          , IdDetenido_Evento = NEW.IdDetenido_Evento
          , Motivo = _motivos
	      , HoraYFecha = NEW.HoraYFecha
          , Folio = _folio
		  , HoraYFechaEvento = _HoraYFechaEvento
          , IdEventoWS = _IdEventoWS
		  , DescripcionEvento = _descEvento
		WHERE DetenidoId = NEW.IdInterno;
    
    END IF;
    
    
END$$    

DELIMITER ;