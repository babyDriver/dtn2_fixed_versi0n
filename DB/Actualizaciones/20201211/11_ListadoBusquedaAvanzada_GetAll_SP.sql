DROP PROCEDURE IF EXISTS ListadoBusquedaAvanzada_GetAll_SP;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListadoBusquedaAvanzada_GetAll_SP`(
IN _tipo VARCHAR(256),
IN _contratoId INT,
IN _inicio DATETIME,
IN _fin DATETIME,
IN _wheres VARCHAR(5000)
)
BEGIN

SET SQL_SAFE_UPDATES = 0;

DROP TEMPORARY TABLE IF EXISTS detalleDetencionTmp;

SET @query = CONCAT('CREATE TEMPORARY TABLE detalleDetencionTmp SELECT D.Id, D.Expediente, D.DetenidoId, D.NCP, D.Activo, D.Estatus, D.TrackingId, D.Fecha, D.DetalledetencionEdad
					  FROM detalle_detencion D
					  WHERE D.ContratoId = ', _contratoId, ' AND 
					  D.Tipo = ', '"', _tipo, '"', ' AND D.Fecha BETWEEN ', '"', _inicio, '" AND "', _fin, '"');

PREPARE stmt FROM @query;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

CREATE INDEX `indice_detalleDetencionTmp` ON `detalleDetencionTmp` (DetenidoId) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;

SET @queryPrincipal = CONCAT('
SELECT TT.TrackingId
	 , TT.Nombre
	 , TT.Paterno
	 , TT.Materno     
	 , TT.Expediente
	 , TT.Alias
	 , TT.Evento
	 , TT.DescripcionHechos
	 , TT.Folio
	 , GROUP_CONCAT(TT.Unidad) Unidad
	 , GROUP_CONCAT(TT.Corporacion) Corporacion
	 , TT.Horas
	 , TT.Motivo
	 , TT.Edad
     , TT.TkgDet
FROM
(
	SELECT D.TrackingId
		 , DD.TrackingId TkgDet
		 , D.Nombre
		 , D.Paterno
		 , D.Materno     
		 , CAST(DD.Expediente AS unsigned) Expediente
		 , IFNULL((SELECT GROUP_CONCAT(`xd`.`Alias` SEPARATOR ",")
				   FROM `alias` `xd`
				   WHERE ((`xd`.`DetenidoId` = `D`.`Id`) AND
						 (`xd`.`Activo` = 1) AND 
						 (`xd`.`Habilitado` = 1))), "") AS Alias
		 , E.Descripcion Evento
		 , E.Descripcion DescripcionHechos
		 , ifnull(WE.Folio, "") Folio
		 , ifnull(U.Nombre, "") Unidad
		 , ifnull(C.Nombre, "") Corporacion
		 , ifnull(CA.TotalHoras, 0) Horas
		 , IFNULL((SELECT GROUP_CONCAT(`mo`.`Motivo` SEPARATOR ",") AS `Motivo`
				   FROM `motivo_detencion_interno` `mdi`
				   INNER JOIN `motivodetencion` `mo` 
				   ON `mdi`.`MotivoDetencionId` = `mo`.`Id`
				   WHERE `mdi`.`InternoId` = `DD`.`Id`), "") AS `Motivo`
		 , DD.DetalledetencionEdad Edad
	FROM detalleDetencionTmp DD
	INNER JOIN detenido D
	ON DD.DetenidoId = D.Id
	LEFT JOIN informaciondedetencion INF
	ON INF.IdInterno = D.Id
	LEFT JOIN eventos E
	ON INF.IdEvento = E.Id
	LEFT JOIN wsaevento WE
	ON WE.Id = E.IdEventoWS
	LEFT JOIN evento_unidad_responsable EUR
	ON EUR.EventoId =  E.Id
	LEFT JOIN unidad U
	ON EUR.UnidadId = U.Id
	LEFT JOIN corporacion C
	ON U.CorporacionId = C.Id
	LEFT JOIN calificacion CA
	ON CA.InternoId = DD.Id
	INNER JOIN general G
	ON G.DetenidoId = D.Id
) TT',' ', _wheres, ' ', 'GROUP BY TT.TrackingId
	 , TT.Nombre
	 , TT.Paterno
	 , TT.Materno     
	 , TT.Expediente
	 , TT.Alias
	 , TT.Evento
	 , TT.DescripcionHechos
	 , TT.Folio	 
	 , TT.Horas
	 , TT.Motivo
	 , TT.Edad
     , TT.TkgDet');

PREPARE stmt2 FROM @queryPrincipal;
EXECUTE stmt2;
DEALLOCATE PREPARE stmt2;

DROP TEMPORARY TABLE IF EXISTS detalleDetencionTmp;

END$$
DELIMITER ;
