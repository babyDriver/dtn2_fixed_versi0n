DROP PROCEDURE IF EXISTS ListadoReporteIngresos_GetAll_SP;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListadoReporteIngresos_GetAll_SP`(
IN _tipo VARCHAR(256),
IN _contratoId INT
)
BEGIN

	SELECT R.TrackingIdDetalle TrackingId
		 , concat(R.Nombre, ' ', R.Apellido_Paterno,' ', R.Apellido_Materno) Detenido
         , cast(R.Expediente as unsigned) Expediente
         , R.Fecha
         , ifnull(D.Folio, '') Folio
         , ifnull(MO.Motivo, '') Motivo
         , R.TotalHoras
         , ifnull(R.Unidad, '') Unidad
         , ifnull(R.Responsable, '') Responsable
         , R.LugarDetencion         
    FROM reporte_remision R
    LEFT JOIN wsaevento D
    ON R.IdEventoWS = D.Id
    LEFT JOIN motivodetencion MO
	ON MO.Id = D.IdMotivo
    WHERE R.ContratoId = _contratoId AND R.Tipo = _tipo;

END$$
DELIMITER ;
