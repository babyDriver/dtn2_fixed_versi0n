DROP PROCEDURE IF EXISTS ListadoReporteFichaTecnica_GetAll_SP;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ListadoReporteFichaTecnica_GetAll_SP`(
IN _contratoId INT,
IN _subcontratoId INT,
IN _clienteId INT
)
BEGIN

	IF _subcontratoId <> 0 THEN
    
		SELECT A.IdDetalle DetalleDetencionId
			 , A.TrackingIdDetalle
			 , cast(A.Expediente as unsigned) Expediente
			 , concat(A.Nombre, ' ', A.Apellido_Paterno, ' ', A.Apellido_Materno) Nombre
			 , A.DetalledetencionEdad Edad
			 , A.DescripcionEvento
			 , A.Motivo
			 , A.Unidad
			 , A.Responsable
			 , A.HoraYFecha
			 , A.Situacion
			 , A.TotalHoras
			 , A.FechasExamen
			 , A.IndicacionesExamen
			 , S.Subcontrato
		FROM reporte_remision A
		INNER JOIN subcontrato S
		ON S.Id = A.ContratoId
		INNER JOIN contrato C
		ON C.Id = S.ContratoId
        WHERE S.Id = _subcontratoId;
    
    ELSEIF _contratoId <> 0 THEN
    
		SELECT A.IdDetalle DetalleDetencionId
			 , A.TrackingIdDetalle
			 , cast(A.Expediente as unsigned) Expediente
			 , concat(A.Nombre, ' ', A.Apellido_Paterno, ' ', A.Apellido_Materno) Nombre
			 , A.DetalledetencionEdad Edad
			 , A.DescripcionEvento
			 , A.Motivo
			 , A.Unidad
			 , A.Responsable
			 , A.HoraYFecha
			 , A.Situacion
			 , A.TotalHoras
			 , A.FechasExamen
			 , A.IndicacionesExamen
			 , S.Subcontrato
		FROM reporte_remision A
		INNER JOIN subcontrato S
		ON S.Id = A.ContratoId
		INNER JOIN contrato C
		ON C.Id = S.ContratoId
        WHERE C.Id = _contratoId;
    
    ELSEIF _clienteId <> 0 THEN
    
		SELECT A.IdDetalle DetalleDetencionId
			 , A.TrackingIdDetalle
			 , cast(A.Expediente as unsigned) Expediente
			 , concat(A.Nombre, ' ', A.Apellido_Paterno, ' ', A.Apellido_Materno) Nombre
			 , A.DetalledetencionEdad Edad
			 , A.DescripcionEvento
			 , A.Motivo
			 , A.Unidad
			 , A.Responsable
			 , A.HoraYFecha
			 , A.Situacion
			 , A.TotalHoras
			 , A.FechasExamen
			 , A.IndicacionesExamen
			 , S.Subcontrato
		FROM reporte_remision A
		INNER JOIN subcontrato S
		ON S.Id = A.ContratoId
		INNER JOIN contrato C
		ON C.Id = S.ContratoId
        WHERE C.ClienteId = _clienteId;
    
    ELSE
    
		SELECT A.IdDetalle DetalleDetencionId
			 , A.TrackingIdDetalle
			 , cast(A.Expediente as unsigned) Expediente
			 , concat(A.Nombre, ' ', A.Apellido_Paterno, ' ', A.Apellido_Materno) Nombre
			 , A.DetalledetencionEdad Edad
			 , A.DescripcionEvento
			 , A.Motivo
			 , A.Unidad
			 , A.Responsable
			 , A.HoraYFecha
			 , A.Situacion
			 , A.TotalHoras
			 , A.FechasExamen
			 , A.IndicacionesExamen
			 , S.Subcontrato
		FROM reporte_remision A
		INNER JOIN subcontrato S
		ON S.Id = A.ContratoId
		INNER JOIN contrato C
		ON C.Id = S.ContratoId;
    
    END IF;

END$$
DELIMITER ;
