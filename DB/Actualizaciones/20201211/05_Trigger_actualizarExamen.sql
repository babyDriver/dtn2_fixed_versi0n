DROP TRIGGER IF EXISTS actualizarExamen;

DELIMITER $$

CREATE TRIGGER actualizarExamen
    AFTER UPDATE
    ON examen_medico FOR EACH ROW
BEGIN
	
	DECLARE _fechas VARCHAR(4000);
    DECLARE _indicaciones VARCHAR(4000);
    
    SELECT group_concat(HoraYFecha) fechas, group_concat(IndicacionesMedicas) indicaciones INTO _fechas, _indicaciones
	FROM examen_medico
    WHERE DetalleDetencionId = NEW.DetalleDetencionId
	GROUP BY DetalleDetencionId;
    
    UPDATE reporte_remision
	SET FechasExamen = _fechas
	  , IndicacionesExamen = _indicaciones
	WHERE IdDetalle = NEW.DetalleDetencionId;
    
END$$    

DELIMITER ;