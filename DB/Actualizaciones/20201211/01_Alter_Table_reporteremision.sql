ALTER TABLE reporte_remision ADD IdDetalle INT NULL;
ALTER TABLE reporte_remision ADD HoraYFecha DATETIME NULL;
ALTER TABLE reporte_remision ADD HoraYFechaEvento DATETIME NULL;
ALTER TABLE reporte_remision ADD Folio VARCHAR(50) NULL;
ALTER TABLE reporte_remision ADD CalificacionId INT NULL;
ALTER TABLE reporte_remision ADD Situacion VARCHAR(200) NULL;
ALTER TABLE reporte_remision ADD TotalHoras INT NULL;
ALTER TABLE reporte_remision ADD IdEventoWS INT NULL;

alter table reporte_remision add DescripcionEvento varchar(4000) null;
alter table reporte_remision add FechasExamen varchar(4000) null;
alter table reporte_remision add IndicacionesExamen varchar(4000) null;

update reporte_remision R inner join(SELECT IDD.Id, IDD.IdInterno, IDD.HoraYFecha    FROM informaciondedetencion IDD    WHERE IDD.Id = (SELECT Id   FROM informaciondedetencion    WHERE IdInterno = IDD.IdInterno    ORDER BY HoraYFecha DESC LIMIT 1)) I ON R.DetenidoId = I.IdInterno set R.HoraYFecha = I.HoraYFecha;

update reporte_remision R inner join(SELECT IDD.Id, IDD.IdInterno, IDD.HoraYFecha, IDD.IdEvento    FROM informaciondedetencion IDD        WHERE IDD.Id = (SELECT Id   FROM informaciondedetencion                        WHERE IdInterno = IDD.IdInterno                        ORDER BY HoraYFecha DESC LIMIT 1)) I ON R.DetenidoId = I.IdInterno inner join eventos E on R.IdEvento = E.Id set R.HoraYFechaEvento = E.HoraYFecha  , R.Folio = E.Folio;

 update reporte_remision R left join (SELECT C.Id, S.Nombre, C.InternoId, C.TotalHoras    FROM calificacion C    LEFT JOIN situacion_detenido S    ON C.SituacionId = S.Id    WHERE C.Activo = 1) CC ON R.IdDetalle = CC.InternoId SET R.CalificacionId = CC.Id  , R.Situacion = CC.Nombre  , R.TotalHoras = CC.TotalHoras;

update reporte_remision R inner join eventos E ON R.IdEvento = E.Id set R.IdEventoWS = E.IdEventoWS;

update reporte_remision R inner join eventos E on R.IdEvento = E.Id set R.DescripcionEvento = E.Descripcion;

SET group_concat_max_len=15000;

update reporte_remision R inner join(select DetalleDetencionId, group_concat(HoraYFecha) fechas, group_concat(IndicacionesMedicas) indicaciones from examen_medico group by DetalleDetencionId) EE on R.IdDetalle = EE.DetalleDetencionId set R.FechasExamen = EE.fechas  , R.IndicacionesExamen = EE.indicaciones;