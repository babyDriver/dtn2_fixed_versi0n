DROP TRIGGER IF EXISTS actualizarDetalle;

DELIMITER $$

CREATE TRIGGER actualizarDetalle
    AFTER UPDATE
    ON detalle_detencion FOR EACH ROW
BEGIN

	DECLARE _estatusNombre VARCHAR(100);
    
    SELECT Nombre INTO _estatusNombre FROM estatus WHERE Id = NEW.Estatus;

	UPDATE informacion_detenidos_busqueda
    SET Expediente = NEW.Expediente
	  , NCP = NEW.NCP
      , Estatus = NEW.Estatus
      , Activo = NEW.Activo
      , TrackingIdEstatus = NEW.TrackingId
      , ContratoId = NEW.ContratoId
      , Tipo = NEW.Tipo
      , EstatusNombre = _estatusNombre
	WHERE DetenidoId = NEW.DetenidoId;
    
    UPDATE reporte_remision
	SET TrackingIdDetalle = NEW.TrackingId
	  , Expediente = NEW.Expediente
      , Fecha = NEW.Fecha
      , DetalledetencionEdad = NEW.DetalledetencionEdad
      , ContratoId = NEW.ContratoId
      , Tipo = NEW.Tipo
      , IdDetalle = NEW.Id
	WHERE DetenidoId = NEW.DetenidoId;
    
END$$    

DELIMITER ;