DROP TRIGGER IF EXISTS nuevoCalificacion;

DELIMITER $$

CREATE TRIGGER nuevoCalificacion
    AFTER INSERT
    ON calificacion FOR EACH ROW
BEGIN	
	
	DECLARE _Situacion VARCHAR(500);    

    SELECT Nombre INTO _Situacion
    FROM situacion_detenido
    WHERE Id = NEW.SituacionId;
    
	UPDATE reporte_remision
	SET CalificacionId = NEW.Id
	  , Situacion = _Situacion
	  , TotalHoras = NEW.TotalHoras
	WHERE IdDetalle = NEW.InternoId;
    
END$$    

DELIMITER ;