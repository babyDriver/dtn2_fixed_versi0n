CREATE INDEX `ind_detdet_tkg` ON `detalle_detencion` (TrackingId) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;
CREATE INDEX `ind_reprem_tkg` ON `reporte_remision` (TrackingIdDetalle) COMMENT '' ALGORITHM DEFAULT LOCK DEFAULT;

update reporte_remision R inner join detalle_detencion D on R.TrackingIdDetalle = D.TrackingId set R.IdDetalle = D.Id;