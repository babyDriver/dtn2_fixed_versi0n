/*
    Reemplazar hermosillo_210125 por el nombre de tu base de datos
*/
USE hermosillo_210125;
CREATE TABLE IF NOT EXISTS evento_alias (
  Id int(11)   NOT NULL AUTO_INCREMENT,
  nameAlias VARCHAR(35) NOT NULL,
  eventoId INT,
  PRIMARY KEY(Id),
  CONSTRAINT FOREIGN KEY fk_alias_evento (eventoId) REFERENCES detenido_evento(Id)

)ENGINE=InnoDB AUTO_INCREMENT=1;

ALTER TABLE hermosillo_210125.evento_alias
ADD COLUMN TrackingId VARCHAR(36) AFTER eventoId;

/*--------------------------------------------------------------------------------*/

USE hermosillo_210125;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `EventoAlias_GetByAlias_SP`(
IN _Alias  varchar(256),
IN _EventoAliasId int
)
BEGIN
SELECT `evento_alias`.`Id`,
    `evento_alias`.`nameAlias`,
    `evento_alias`.`eventoId` ,
    `evento_alias`.`TrackingId`
FROM evento_alias 
WHERE  evento_alias.nameAlias = _Alias  and 
 evento_alias.eventoId= _EventoAliasId;
END$$
DELIMITER ;

/*--------------------------------------------------------------------------------*/
/*USE hermosillo_210125;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `EventoAlias_GetById_SP`(
IN _Id  INT(11)
)
BEGIN
SELECT `evento_alias`.`Id`,
    `evento_alias`.`nameAlias`,
    `evento_alias`.`eventoId` 
FROM evento_alias
 WHERE  `evento_alias.Id` = _Id;
END
DELIMITER ;*/

USE hermosillo_210125;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `EventoAlias_Update_SP`(
IN _Id  INT(11),
IN _nameAlias VARCHAR(36),
IN _EventoAliasId  INT(11)
)
BEGIN
UPDATE `evento_alias`
SET
`nameAlias` = _nameAlias,
`_EventoAliasId` = _EventoAliasId 
WHERE `Id` = _Id;
END$$
DELIMITER ;
/*--------------------------------------------------------------------------------*/

/*drop procedure hermosillo_210125.EventoAlias_GetByEventoDetenidoId_SP;*/
USE hermosillo_210125;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `EventoAlias_GetByEventoDetenidoId_SP`(
IN _EventoAliasId  INT(11)
)
BEGIN
SELECT `evento_alias`.`Id`,
    `evento_alias`.`nameAlias`,
    `evento_alias`.`eventoId` ,
	`evento_alias`.`TrackingId`
FROM evento_alias 
WHERE  evento_alias.eventoId= _EventoAliasId;
END$$
DELIMITER ;

/*--------------------------------------------------------------------------------*/
USE hermosillo_210125;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `EventoAlias_GetByEventoDetenidoTrackId_SP`(
IN _TrackingId  varchar(36)
)
BEGIN
SELECT `evento_alias`.`Id`,
    `evento_alias`.`nameAlias`,
    `evento_alias`.`eventoId`,
    `evento_alias`.`TrackingId`
FROM evento_alias 
WHERE  evento_alias.TrackingId = _TrackingId ;
END$$
DELIMITER ;

/*--------------------------------------------------------------------------------*/
/*DROP PROCEDURE hermosillo_210125.EventoAlias_Insert_SP;*/
USE hermosillo_210125;
DELIMITER $$
CREATE PROCEDURE hermosillo_210125.EventoAlias_Insert_SP
(
	OUT _NewIdentity INT,
	IN _nameAlias VARCHAR(36),
	IN _EventoAliasId INT(11),
	IN _TrackingId VARCHAR(36)
)
BEGIN
INSERT INTO `evento_alias`
(`nameAlias`,
	`eventoId`,
  `TrackingId`)
VALUES
(_nameAlias,
_EventoAliasId,
_TrackingId);
    SET _NewIdentity = LAST_INSERT_ID(); 
END$$
DELIMITER ;

/*--------------------------------------------------------------------------------*/
DROP PROCEDURE hermosillo_210125.Detenido_evento_Insert_SP;
use hermosillo_210125;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Detenido_evento_Insert_SP`(
	OUT _NewIdentity INT,
	IN _TrackingId varchar(36),
    IN _Nombre  varchar(250),
    IN _Paterno  varchar(250),
    IN _Materno varchar(250),
	IN _SexoId int,
	IN _Activo boolean,
	IN _Habilitado boolean,
	IN _CreadoPor int,
	IN _EventoId int,
    IN _Edad int,
    IN _MotivoId int
  
)
BEGIN
INSERT INTO `detenido_evento`
	(`TrackingId`,`Nombre`,`Paterno`,`Materno`,
		`SexoId`,`Activo`,`Habilitado`,`CreadoPor`,
		`EventoId`,`Edad`,`MotivoId`
	)
	VALUES
	(_TrackingId, _Nombre,
    _Paterno,
	_Materno,
	_SexoId,
	_Activo,
	_Habilitado, 
	_CreadoPor,
	_EventoId,
    _Edad,
    _MotivoId
	);
    
     SET _NewIdentity = LAST_INSERT_ID(); 
END$$
DELIMITER ;