DROP TRIGGER IF EXISTS actualizarCalificacion;

DELIMITER $$

CREATE TRIGGER actualizarCalificacion
    AFTER UPDATE
    ON calificacion FOR EACH ROW
BEGIN	
	
	DECLARE _Situacion VARCHAR(500);    

    SELECT Nombre INTO _Situacion
    FROM situacion_detenido
    WHERE Id = NEW.SituacionId;	        
    
	UPDATE reporte_remision
	SET CalificacionId = NEW.Id
	  , Situacion = _Situacion
	  , TotalHoras = NEW.TotalHoras	  
	WHERE IdDetalle = NEW.InternoId;
	
	UPDATE informacion_detenidos_busqueda
	SET  situacion_ultima_detencion = _Situacion
		,multa_ultima_detencion = NEW.TotalAPagar
		,horas_arresto_ultima_detencion = NEW.TotalHoras
	WHERE Ultima_detencion_id = NEW.InternoId;
    
	
	UPDATE reporte_sanciones_juez
	SET 
		Multa = NEW.TotalAPagar
		,HorasArresto = NEW.TotalHoras
	WHERE IdDetalle = NEW.InternoId;
END$$    

DELIMITER ;