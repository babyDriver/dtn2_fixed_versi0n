
drop procedure if exists EgresoIngreso_GetAllByDate_SP;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `EgresoIngreso_GetAllByDate_SP`(
IN _FechaInicio datetime,
IN _FechaFin datetime,
IN _Egreso TINYINT(1)
)
BEGIN

	SELECT Id, TrackingId, Fecha, Folio, Concepto, Total, PersonaQuePaga, Observacion, 	Activo,Habilitado, CreadoPor, Egreso, ContratoId, Tipo
	FROM egresoingreso
	WHERE Egreso = _Egreso AND Habilitado = 1 AND Fecha BETWEEN _FechaInicio AND _FechaFin; 

END