	insert into  reporte_pertenencias (
		PertenenciaId,
		Cantidad,
		Clasificacion,
		Pertenencia,
		Observacion,
		NoRemision,
		Detenido,
		Recibe,
		FechaRegistro,
		ContratoId,
        DetalleDetencionId,
		Activo
        )
       ( 
			Select 
            P.Id,
			P.Cantidad,
			'Evidencia',
			P.pertenencia,
								P.Observacion,
								E.Expediente,
								CONCAT(E.Nombre, " ", E.Apellido_Paterno," ", E.Apellido_Materno),
								CONCAT(U.Nombre, " ", U.ApellidoPaterno," ", U.ApellidoMaterno),
								PL.Fec_Movto,
                                E.ContratoId,
								E.Ultima_detencion_id,
                                1
  FROM pertenencia P	
  INNER JOIN informacion_detenidos_busqueda E ON E.ultima_detencion_id  = P.InternoId
  INNER JOIN vusuarios V ON P.CreadoPor = V.Id
  INNER JOIN	usuario U ON V.UsuarioId = U.Id
  INNER JOIN pertenencialog PL	ON  P.Id = PL.Id
    WHERE  P.Activo=1
	AND P.Estatus=4
	AND P.Clasificacion = 1
	AND E.Estatus <> 2
	AND  E.Activo=1 
	AND PL.Accion ='Registrado')  ;	 