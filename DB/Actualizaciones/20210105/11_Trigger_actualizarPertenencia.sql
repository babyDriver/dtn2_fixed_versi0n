DROP TRIGGER IF EXISTS pertenencia_AFTER_UPDATE;

DELIMITER $$

CREATE TRIGGER pertenencia_AFTER_UPDATE
    AFTER UPDATE
    ON pertenencia FOR EACH ROW
BEGIN	
    
    DECLARE _Clasificacion VARCHAR(256); 
    
	INSERT INTO `pertenencialog`
			(`Id`,
			 `TrackingId`,
		     `Nombre`,
			 `Descripcion`,
			 `Activo`,
			 `Habilitado`,
			 `Creadopor`,
	         `Accion`,
	         `AccionId`,
			 `Fec_Movto`)
			VALUES
			(new.Id,
			new.TrackingId,
			new.Pertenencia,
			new.Observacion,
			new.Activo,
			new.Habilitado,
			new.Creadopor,
			case	when  new.Activo = 0 AND (OLD.Activo IS NULL) THEN 'Eliminado'
					when  new.Activo = 0 AND OLD.Activo = 1	THEN 'Eliminado'
					else 'Modificado'
			  end,
			 case	when  new.Activo = 0 AND (OLD.Activo  IS NULL) THEN 3
					when  new.Activo = 0 AND OLD.Activo  = 1	THEN 3
					else 2
			  end,
			CURRENT_TIMESTAMP);
    
	
	SELECT _Clasificacion INTO _Clasificacion FROM reporte_pertenencias WHERE PertenenciaId = NEW.Id;
	
	IF NEW.Clasificacion = 1 THEN
    	
	UPDATE reporte_pertenecias
	SET  Cantidad = NEW.Cantidad, Pertenencia = NEW.Pertenencia, Observacion=NEW.Observacion
	WHERE PertenenciaId= NEW.Id;
	
    ELSE   
			UPDATE reporte_pertenecias
			SET  Activo = 0, Clasificacion = ''
			WHERE PertenenciaId= NEW.Id;
	
	END IF;
	
END$$    

DELIMITER ;