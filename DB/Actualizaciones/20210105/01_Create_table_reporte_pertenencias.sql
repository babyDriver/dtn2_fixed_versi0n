DROP TABLE IF EXISTS `reporte_pertenencias`;

CREATE TABLE `reporte_pertenencias` (
	`PertenenciaId` int(11)   NULL,
    `Cantidad` int(11) NULL,
    `Clasificacion` varchar(256) NULL,
	`Pertenencia` varchar(512) NULL,
	`Observacion` varchar(512) NULL,
	`Detenido` varchar(256) NULL,
    `Recibe`   varchar(512) NULL,    
	`Activo` tinyint(1) NULL,
    `NoRemision` varchar(256) NULL,    
    `FechaRegistro` datetime NULL,
	`DetalleDetencionId` int(11) NULL
	`ContratoId` int(11) NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE reporte_pertenencias ADD INDEX i_rp_contratoId (ContratoId);
ALTER TABLE reporte_pertenencias ADD INDEX i_rp_fecha (FechaRegistro);