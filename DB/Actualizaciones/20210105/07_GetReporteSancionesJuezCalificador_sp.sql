drop procedure if exists GetReporteSancionesJuezCalificador_sp;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetReporteSancionesJuezCalificador_sp`(
IN  _ContratoId int,
IN  _Fechainicio Date,
IN  _Fechafin date)
begin
Select
  Id,      
  uuid() TrackingId,
  NombreJuez Juez,
  NombreDetenido Detenido,
   date_format(Fecha, '%d-%m-%Y %H:%i') Fecha,
  NoRemision Expediente,
  Multa Totaldemultas,
  HorasArresto Totalhoras
  FROM reporte_sanciones_juez	
    WHERE  ContratoId=_ContratoId 
  and  cast(Fecha as date) between _Fechainicio and _Fechafin
end$$
DELIMITER ;
