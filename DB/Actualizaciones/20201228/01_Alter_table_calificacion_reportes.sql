
ALTER TABLE calificacion ADD INDEX i_internoid (internoId);
alter table  informacion_detenidos_busqueda add  fecha_registro_ultima_detencion datetime null;
alter table  informacion_detenidos_busqueda add  fecha_detencion_ultima_detencion datetime null;
alter table  informacion_detenidos_busqueda add  situacion_ultima_detencion varchar(256) null;
alter table  informacion_detenidos_busqueda add  multa_ultima_detencion decimal(10,2) null;
alter table  informacion_detenidos_busqueda add  horas_arresto_ultima_detencion int null;

ALTER TABLE informacion_detenidos_busqueda ADD INDEX i_situacion_ultima_detencion (situacion_ultima_detencion);
ALTER TABLE informacion_detenidos_busqueda ADD INDEX i_EstatusNombre (EstatusNombre);

update informacion_detenidos_busqueda inner join informaciondedetencion on informacion_detenidos_busqueda.ultima_detencion_id = informaciondedetencion.IdInterno set informacion_detenidos_busqueda.fecha_detencion_ultima_detencion = informaciondedetencion.HoraYFecha;

update informacion_detenidos_busqueda inner join detalle_detencion on informacion_detenidos_busqueda.ultima_detencion_id = detalle_detencion.Id set informacion_detenidos_busqueda.fecha_registro_ultima_detencion = detalle_detencion.Fecha;

update informacion_detenidos_busqueda inner join calificacion on informacion_detenidos_busqueda.ultima_detencion_id = calificacion.InternoId set informacion_detenidos_busqueda.multa_ultima_detencion = calificacion.TotalAPagar, informacion_detenidos_busqueda.horas_arresto_ultima_detencion = calificacion.TotalHoras;

update informacion_detenidos_busqueda inner join calificacion on informacion_detenidos_busqueda.ultima_detencion_id = calificacion.InternoId set informacion_detenidos_busqueda.situacion_ultima_detencion = (Select Nombre from situacion_detenido where Id= calificacion.SituacionId);


UPDATE reporte_remision
inner join calificacion on calificacion.InternoId = reporte_remision.IdDetalle
inner join situacion_detenido on  calificacion.SituacionId = situacion_detenido.Id
	SET reporte_remision.CalificacionId = calificacion.Id
	  , reporte_remision.Situacion = situacion_detenido.Nombre
	  , reporte_remision.TotalHoras = calificacion.TotalHoras;
	
    
	
UPDATE informacion_detenidos_busqueda
inner join calificacion on calificacion.InternoId = informacion_detenidos_busqueda.Ultima_detencion_id
inner join situacion_detenido on  calificacion.SituacionId = situacion_detenido.Id

	SET  informacion_detenidos_busqueda.situacion_ultima_detencion = situacion_detenido.Nombre
		,informacion_detenidos_busqueda.multa_ultima_detencion = calificacion.TotalAPagar
		,informacion_detenidos_busqueda.horas_arresto_ultima_detencion = calificacion.TotalHoras;
