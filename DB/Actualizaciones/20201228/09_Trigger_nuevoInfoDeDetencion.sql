DROP TRIGGER IF EXISTS nuevoInfoDeDetencion;

DELIMITER $$

CREATE TRIGGER nuevoInfoDeDetencion
    AFTER INSERT
    ON informaciondedetencion FOR EACH ROW
BEGIN	
	
    DECLARE _motivos VARCHAR(500);
	DECLARE _HoraYFechaEvento DATETIME;
    DECLARE _folio VARCHAR(50);
    DECLARE _IdEventoWS INT;
    DECLARE _descEvento VARCHAR(4000);

	SELECT GROUP_CONCAT(distinct w.nombreMotivoLlamada SEPARATOR ',') INTO _motivos
	FROM detenido_evento e
	INNER JOIN wsamotivo w
	ON e.MotivoId = w.idMotivo
	WHERE EventoId = NEW.IdEvento;
    
    SELECT Folio, HoraYFecha, IdEventoWS, Descripcion INTO _folio, _HoraYFechaEvento, _IdEventoWS, _descEvento
    FROM eventos
    WHERE Id = NEW.IdEvento;
    
	UPDATE reporte_remision
	SET LugarDetencion = NEW.LugarDetencion
	  , HoraYFecha = NEW.HoraYFecha
	  , IdEvento = NEW.IdEvento
	  , IdDetenido_Evento = NEW.IdDetenido_Evento
      , Motivo = _motivos
      , Folio = _folio
      , HoraYFechaEvento = _HoraYFechaEvento
      , IdEventoWS = _IdEventoWS
      , DescripcionEvento = _descEvento
	
	WHERE IdDetalle = NEW.IdInterno;
	
	UPDATE informacion_detenidos_busqueda
		SET  fecha_detencion_ultima_detencion = NEW.HoraYFecha
		WHERE IdDetalle = NEW.IdInterno;
    
END$$    

DELIMITER ;