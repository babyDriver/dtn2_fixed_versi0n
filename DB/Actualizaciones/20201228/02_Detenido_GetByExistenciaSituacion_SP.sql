DROP PROCEDURE IF EXISTS Detenido_GetByExistenciaSituacion_SP;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Detenido_GetByExistenciaSituacion_SP`(
IN _contratoId INT,
IN _situacion varchar(500)
)
BEGIN
	SELECT
	
	
	I.TrackingId EstatusTracking,
	  I.DetenidoId,
    I.RutaImagen,
    I.Nombre,
    I.apellido_paterno Paterno,
    I.apellido_materno Materno,
    CAST( I.Expediente AS unsigned) Expediente,                           
    date_format(I.fecha_registro_ultima_detencion, '%d-%m-%Y %H:%i') Fecha,
    RR.Motivo,
    date_format(I.fecha_detencion_ultima_detencion, '%d-%m-%Y %H:%i') DateDetencion,                              
    I.Edad Edad,                              
    I.horas_arresto_ultima_detencion TotalHoras
							   
	FROM informacion_detenidos_busqueda	I
	LEFT JOIN reporte_remision RR ON RR.DetenidoId = I.DetenidoId
    WHERE I.EstatusNombre = 'Ingreso' and I.ContratoId=_contratoId and I.situacion_ultima_detencion =_situacion;

END$$
DELIMITER ;