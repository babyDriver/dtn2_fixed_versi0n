DROP PROCEDURE IF EXISTS Detenido_GetByProximosCumplir_SP;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Detenido_GetByProximosCumplir_SP`(
IN _contratoId INT
)
BEGIN
	SELECT
	Nombre,
	Apellido_Paterno,
	Apellido_Materno,
	Ultima_detencion_id,
	DetenidoId,
	fecha_registro_ultima_detencion,
	horas_arresto_ultima_detencion,
	situacion_ultima_detencion,
	multa_ultima_detencion
	FROM informacion_detenidos_busqueda	
    WHERE EstatusNombre = 'Ingreso' and ContratoId=_contratoId and situacion_ultima_detencion is not null;

END$$
DELIMITER ;