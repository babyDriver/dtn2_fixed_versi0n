﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="listaceldas.aspx.cs" Inherits="Web.Application.Control_Celdas.listaceldas" %>



<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
  
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Admin/catalogo.aspx?name=escolaridad">Administración</a></li>
        <li>Celdas</li> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
            <style type="text/css">
         td.strikeout {
            text-decoration: line-through;
        }
     
    </style>
    <div class="scroll">
    <div class="row" id="addentry" >
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark" id="titlehead">
                <i class="fa fa-bomb"></i>
                Catálogo de celda
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
       </div>
    </div>
    <p></p>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-arma-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar catálogo </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-certificate"></i>
                                                <input type="text" name="nombre" runat="server" id="nombre" placeholder="Celda" maxlength="256" />
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-arma-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-bomb"></i></span>
                        <h2 id="titlegrid">Celda</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th data-hide="phone,tablet">Capacidad</th>
                                        <th data-hide="phone,tablet">Nivel de peligrosidad</th>
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="input">Celda <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="nombre" runat="server" id="itemnombre" class="alphanumeric" placeholder="Nombre" maxlength="256" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el nombre o número de celda</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input" >Capacidad <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="capacidad" runat="server" id="itemcapacidad" class="number" placeholder="Capacidad" maxlength="256" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese la capacidad</b>
                                    </label>
                                </section>
                                <section>
                                    <label>Nivel de peligrosidad <a style="color: red">*</a></label>
                                    <label class="select">
                                        <select name="rol" id="dropdown" runat="server">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                             
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id="delete-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar 
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro <strong><span id="itemeliminar"></span></strong>&nbsp;será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="history-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="capacidadhistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="history" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistory" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th>Movimiento</th>
                                        <th data-hide="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

       <div id="historycanceled-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="catalogohistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div2" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistorydeleted" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th>Movimiento</th>
                                        <th data-hide="phone,tablet">Realizado por</th>
                                        <th data-hide="phone,tablet">Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="idhistory" runat="server" />
     <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        

                    }
                },
                ajax: {
                    type: "POST",
                    url: "listaceldas.aspx/getcelda",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });


                        var celda = $("#ctl00_contenido_nombre").val();
                        parametrosServerSide.celda = celda;
                        parametrosServerSide.emptytable = false;        //window.emptytable;

                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                     {
                         name: "Capacidad",
                         data: "Capacidad"
                     },
                     {
                         name: "M.Nombre",
                         data: "Peligrosidad"
                     },
                     null,
                    null

                ],
                columnDefs: [
                    {
                        targets: 3,
                        name: "Habilitado",
                        render: function (data, type, row, meta) {
                            return row.Habilitado ? "Habilitado" : "Deshabilitado"
                        }
                    },
                    {
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "";
                            var editar = "";
                            var habilitar = "";
                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                edit = "edit";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                edit= "disabled";
                            }

                            // deshabilitar estas dos columnas para activar permisos y desactivar las 2 que siguen
                            //if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' "href="javascript:void(0);" data-value = "' + row.Nombre + '" data-capacidad="' + row.Capacidad + '" data-tracking="' + row.TrackingId + '" data-Nivel_Peligrosidad="' + row.Nivel_Peligrosidad + '" data-habilitado="' + row.Habilitado + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            //if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Nombre + '" title="' + txtestatus + '" data-tracking="' + row.TrackingId + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';

                            editar = '<a class="btn btn-primary btn-circle ' + edit + ' "href="javascript:void(0);" data-value = "' + row.Nombre + '" data-capacidad="' + row.Capacidad + '" data-tracking="' + row.TrackingId + '" data-Nivel_Peligrosidad="' + row.Nivel_Peligrosidad + '" data-habilitado="' + row.Habilitado + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Nombre + '" title="' + txtestatus + '" data-tracking="' + row.TrackingId + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';


                            return editar +
                              habilitar +
                                '<a class="btn btn-default btn-circle historial" href="javascript:void(0);" data-value="' + row.Nombre + '" data-tracking="' + row.TrackingId + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>'
                            ;
                        }
                    }
                ]
            });
           

            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-tracking"));
                $("#blockitem-modal").modal("show");
            });
                //Cambio id por tracking. Revisar funcionalidad.
            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "celda.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho!</strong>" +
                                " El registro  se actualizó correctamente.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", " El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });


            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_itemnombre").val("");
                $("#ctl00_contenido_itemcapacidad").val("");
                $("#ctl00_contenido_dropdown").val("");              
            });
           

            $("body").on("click", ".add", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_itemnombre").val("");
                $("#ctl00_contenido_itemcapacidad").val("");
                $("#ctl00_contenido_peligrosidad").val("");
                CargarListado(0);
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#form-modal-title").empty();
                $('#habilitado').prop('checked', false);
                $("#form-modal-title").html("Agregar registro");
                $("#form-modal").modal("show");
            });

            $("body").on("click", ".edit", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                var capacidad = $(this).attr("data-capacidad");
                var Nivel_Peligrosidad = $(this).attr("data-Nivel_Peligrosidad");
                var habilitado = $(this).attr("data-habilitado");

                $("#btnsave").attr("data-id", id);
                $("#btnsave").attr("data-tracking", tracking);
                $("#btnsave").attr("data-habilitado", habilitado);
                $("#ctl00_contenido_itemnombre").val($(this).attr("data-value"));
                $("#ctl00_contenido_itemcapacidad").val($(this).attr("data-capacidad"));
                CargarListado(Nivel_Peligrosidad);
                $("#itemhabilitado").val($(this).attr("data-habilitado"));
                $("#form-modal-title").empty();
                $("#form-modal-title").html("Editar registro");
                $("#form-modal").modal("show");
            });

            function CargarListado(id) {
                $('#ctl00_contenido_peligrosidad').empty();
                $.ajax({

                    type: "POST",
                    url: "celda.aspx/getListadoCombo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_dropdown');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Peligrosidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("body").on("click", ".delete", function () {
                var id = $(this).attr("data-id");
                var nombre = $(this).attr("data-value");
                $("#itemeliminar").text(nombre);
                $("#btndelete").attr("data-id", id);
                $("#delete-modal").modal("show");
            });

            $("body").on("click", ".save", function () {
                var id = $("#btnsave").attr("data-id");
                var tracking = $("#btnsave").attr("data-tracking");
                var habilitado = $("#btnsave").attr("data-habilitado");

                if (id == "") {
                    habilitado = true;
                }
              
                datos = [
                      id = id,
                      tracking = tracking,
                      nombre = $("#ctl00_contenido_itemnombre").val(),
                      capacidad = $("#ctl00_contenido_itemcapacidad").val(),
                      peligrosidad = $("#ctl00_contenido_dropdown").val()

                ];

                if (validar()) {
                    Save(datos);
                }

            });

            function limpiar() {
                $('#ctl00_contenido_itemcapacidad').parent().removeClass('state-success');
                $('#ctl00_contenido_itemcapacidad').parent().removeClass("state-error");
                $('#ctl00_contenido_itemnombre').parent().removeClass('state-success');
                $('#ctl00_contenido_itemnombre').parent().removeClass("state-error");
                $('#ctl00_contenido_dropdown').parent().removeClass('state-success');
                $('#ctl00_contenido_dropdown').parent().removeClass("state-error");
            }

            function validar() {
                var esvalido = true;
                if ($("#ctl00_contenido_itemnombre").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#ctl00_contenido_itemnombre').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemnombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemnombre').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemnombre').addClass('valid');
                }

                if ($("#ctl00_contenido_itemcapacidad").val().split(" ").join("") == "") {

                    ShowError("Descripción", "La capacidad es obligatoria.");
                    $('#ctl00_contenido_itemcapacidad').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemcapacidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemcapacidad').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemcapacidad').addClass('valid');
                }



                if ($("#ctl00_contenido_dropdown").val() == null || $("#ctl00_contenido_dropdown").val() == "0") {
                    ShowError("Nivel de peligrosidad", "El nivel de peligrosidad es obligatorio.");
                    $('#ctl00_contenido_dropdown').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_dropdown').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_dropdown').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_dropdown').addClass('valid');
                }

                return esvalido;
            }

            function Save(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "celda.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modal").modal("hide");
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                        } else {
                            ShowError("Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');


                    },
                    error: function () {
                        ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }



            $("#btndelete").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "celda.aspx/delete",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "El registro fue eliminado del catálogo.", "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El registro fue eliminado del catálogo.");
                            $("#delete-modal").modal("hide");
                            $('#main').waitMe('hide');
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }

                    }
                });
            });


            $("body").on("click", ".historial", function () {
                var id = $(this).attr("data-tracking");
                $('#ctl00_contenido_idhistory').val(id);
                var capacidad = $(this).attr("data-value");
                $("#capacidadhistorial").text(capacidad);
                $("#history-modal").modal("show");

                window.emptytablehistory = false;
                window.tablehistory.api().ajax.reload();

            });

            var responsiveHelper_dt_basichistory = undefined;
            var breakpointHistoryDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistory = true;
            window.tablehistory = $('#dt_basichistory').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[3, 'desc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistory) {
                        responsiveHelper_dt_basichistory = new ResponsiveDatatablesHelper($('#dt_basichistory'), breakpointHistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistory.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistory.respond();
                    $('#dt_basichistory').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "celda.aspx/getceldalog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basichistory').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        var celdaid = $('#ctl00_contenido_idhistory').val();
                        parametrosServerSide.celdaid = celdaid;
                        parametrosServerSide.todoscancelados = false;
                        parametrosServerSide.emptytable = window.emptytablehistory;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        orderable: false
                    },
                    null,
                    null,
                ],
                columnDefs: [

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {


                            return row.CreadoPor != null ? row.CreadoPor : '';


                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            return row.Fec_Motvo != null ? toDateTime(row.Fec_Motvo) : '';

                        }
                    }

                ]
            });

            $("body").on("click", ".historialcancelado", function () {
                $("#historycanceled-modal").modal("show");

                window.emptytablehistorydeleted = false;
                window.tablehistorydeleted.api().ajax.reload();
            });

            var responsiveHelper_dt_basichistorydeleted = undefined;
            window.emptytablehistorydeleted = true;

            window.tablehistorydeleted = $('#dt_basichistorydeleted').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[4, 'asc'], [3, 'desc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistorydeleted) {
                        responsiveHelper_dt_basichistorydeleted = new ResponsiveDatatablesHelper($('#dt_basichistorydeleted'), breakpointHistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistorydeleted.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistorydeleted.respond();
                    $('#dt_basichistorydeleted').waitMe('hide');
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(4, { page: 'current' }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"><i class="fa fa-info-circle"></i>&nbsp;<strong>Eliminados</strong></td></tr>'
                            );

                            last = group;
                        }
                    });
                },

                ajax: {
                    type: "POST",
                    url: "celda.aspx/getceldalog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basichistorydeleted').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });


                        parametrosServerSide.celdaid = 0;
                        parametrosServerSide.todoscancelados = true;
                        parametrosServerSide.emptytable = window.emptytablehistorydeleted;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: false
                    },

                    {
                        name: "Accion",
                        data: "Accion",
                        orderable: false
                    },
                    null,
                    null,

                    {
                        targets: 4,
                        "class": "details-control",
                        orderable: false,
                        data: null,
                        visible: false,
                        name: "C.Id",
                        data: "Id"
                    }
                ],
                columnDefs: [

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {



                            return row.CreadoPor != null ? row.CreadoPor : '';


                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            return row.Fec_Movto != null ? toDateTime(row.Fec_Movto) : '';

                        }
                    }
                ]
            });
            $('#ctl00_contenido_itemcapacidad').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
            });
            init();
            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }

            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
