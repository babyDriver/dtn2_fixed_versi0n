﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="evidencias_report.aspx.cs" Inherits="Web.Application.Report.evidencias_report" %>


<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
    <li>Control de pertenencias</li>
    <li>Resumen de evidencias</li>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contenido" runat="server">
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                              <i class="fa fa-file-text-o fa-fw "></i>

              Resumen de evidencias
            </h1>
        </div>
    </div>
    <asp:Label ID="Label1" runat="server"></asp:Label>
    <section id="widget-grid-proximos" class="">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-evidencias-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                        <%--<span class="widget-icon"></span>--%>               <i style="float:left; margin-top:10px; margin-left:5px;" class="fa fa-file-text-o fa-fw "></i>

                        <h2>Resumen de evidencias</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div id="smart-form-register-entry" class="smart-form">
                            <fieldset>
                               
                                <div class="row">
                                    <section class="col col-3">
                                                    <label class="label">Fecha de inicio <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <label class='input-group date' >
                                                        <input type="text" name="inicio" id="inicio" placeholder="Fecha de inicio" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha de inicio.</b>
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                    </label>
                                                </section>
                                    <section class="col col-3">
                                                    <label class="label">Fecha final <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <label class='input-group date' >
                                                        <input type="text" name="final" id="final" placeholder="Fecha final" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha final.</b>
                                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                    </label>
                                                </section>
                                    <section class="col" id="BotonEvidencia">
                                        <a class="btn btn-md btn-success reportePdf" style="margin-top:22px;padding:5px"><i class="glyphicon glyphicon-file"></i> Generar PDF</a>
                                        <a class="btn btn-md btn-default" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Control_Pertenencias/control_pertenencias.aspx" style="padding:5px;margin-top:22px">
                                                    <i class="fa fa-mail-reply"></i> Regresar a control de pertenencias</a>
                                    </section>
                                    </div>
                                </fieldset>
                            </div>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>No. remisión</th>
                                        <th >Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Cantidad</th>
                                        <th>Evidencia</th>
                                        <th>Observación</th>
                                        <th>Recibe</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        </section>
        </div>
    <input type="hidden" id="Hidden1" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            
            $("body").on("click", ".reportePdf", function () {
                $("#ctl00_contenido_lblMessage").html("");
                var inicio = new Date($("#ctl00_contenido_inicio").val());
                var fin = new Date($("#ctl00_contenido_final").val());

                if ($("#ctl00_contenido_inicio").val() == "" || $("#ctl00_contenido_final").val() == "") {
                    ShowAlert("¡Aviso!", "Seleccione un rango de fechas");
                }
                else {
                    pdf();
                }
            });
            ValidarBoton();

            function ValidarBoton() {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/evidencias_report.aspx/ValidarBoton",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            if (resultado.Boton) {
                                $("#BotonEvidencia").show();
                            }
                            else {
                                $("#BotonEvidencia").hide();
                            }
                        }
                        else {
                            ShowError("¡Error! No fue posible validar si mostrar el boton", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }
            $('#ctl00_contenido_inicio').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });
            $('#ctl00_contenido_final').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });

            function pdf() {
                
                $.ajax({

                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/evidencias_report.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        Inicio: $("#ctl00_contenido_inicio").val(),
                        Fin: $("#ctl00_contenido_final").val()
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/evidencias_report.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
               
                    {
                        name: "Cantidad",
                        data: "Cantidad"
                    },
                    {
                        name: "Pertenencia",
                        data: "Pertenencia"
                    },
                    {
                        name: "Observacion",
                        data: "Observacion"
                    },
                    {
                        name: "NombreUsuario",
                        data: "NombreUsuario"
                    },
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false

                    }
                    //null
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    //{
                    //    data: "Entregado",
                    //    targets: 10,
                    //    orderable: false,
                    //    render: function (data, type, row, meta) {
                    //        if (data != null) {
                    //            return "Si";
                    //        }
                    //        else
                    //            return "No";
                    //    }
                    //} 
                ]

            });
        });
    </script>
</asp:Content>

