﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="existenciaPorSituacion.aspx.cs" Inherits="Web.Application.Report.existenciaPorSituacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
        <li>Juez calificador</li>
    <li>Detenidos en existencia por situación</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">  
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group"></i>&nbsp;Detenidos en existencia por situación
            </h1>
        </div>
    </div>


    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="scroll">
    <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-remisiones-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                <header>
                  <span class="widget-icon"><i class="fa fa-group"></i></span>
       
                    <h2> Detenidos en existencia por situación</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <div id="smart-form-register-entry" class="smart-form">
                        
                            <fieldset>
                               
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Situación</label>
                                        <label class="select">
                                            <select name="situacion" id="situacion">
                                            </select>
                                            
                                        </label>
                                    </section>
                                   <br />
                                    <section class="col col-4" >
                                        <label class="label"> </label>
                                        <a class="btn btn-success reporte" style="height:30px;padding:5px" ><i class="glyphicon glyphicon-file"></i> Generar PDF</a>
                                    </section>
                                    </div>
                                
                                </fieldset>
                            </div>
                        <table id="dt_basic_recibos" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">#</th>
                                        <th>No. remisión</th>
                                        <th>Detenido</th>
                                        <th>Edad</th>
                                        <th>Fecha detención</th>
                                        <th>Fecha registro</th>
                                        <th>Horas</th>
                                        <th>Motivos(s) calificación</th>
                                        <th>Pertenencias</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
                <footer>
                </footer>
                </div>
        </article>
        </div>
        </section>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();

            var responsiveHelper_dt_basic_recibos = undefined;
            var responsiveHelper_datatable_fixed_column_recibos = undefined;
            var responsiveHelper_datatable_col_reorder_recibos = undefined;
            var responsiveHelper_datatable_tabletools_recibos = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            init();
            function init() {
                CargarSituaciones(0);
                cargarTabla();
            }

            $("body").on("click", ".reporte", function () {
                $("#ctl00_contenido_lblMessage").html("");
                pdf();
            });



            function pdf() {

                $.ajax({

                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/existenciaPorSituacion.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        situacionId: $("#situacion option:selected").text(),
                        Id: $("#situacion").val()
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.message + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.message);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function cargarTabla() {
                responsiveHelper_dt_basic_recibos = undefined;
                $('#dt_basic_recibos').dataTable({
                    destroy: true,
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    //"scrollY":        "350px",
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_recibos) {
                            responsiveHelper_dt_basic_recibos = new ResponsiveDatatablesHelper($('#dt_basic_recibos'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_recibos.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_recibos.respond();
                        $('#dt_basic_recibos').waitMe('hide');
                    },
                    ajax: {
                        type: "POST",
                        url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/existenciaPorSituacion.aspx/getDetenidos",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.Situacion = $("#situacion").val();
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            name: "Id",
                            data: "Nombre"
                        },
                        {
                            name: "Remision",
                            data: "Expediente"
                        },
                        {
                            name: "NombreCompleto",
                            data: "NombreCompleto"
                        },
                        {
                            name: "Edad",
                            data: "Edad"
                        },
                        {
                            name: "FechaDetencion",
                            data: "DateDetencion"
                        },
                        {
                            name: "FechaRegistro",
                            data: "Fecha"
                        },
                        {
                            name: "Horas",
                            data: "TotalHoras"
                        },
                        {
                            name: "Motivo",
                            data: "Motivo"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        //{
                        //    targets: 3,
                        //    orderable: false,
                        //    render: function (data, type, row, meta) {
                        //        var fecha = row.Edad != null ? toDate(row.Edad) : '';
                        //        if (fecha != '')
                        //            return moment().diff(moment(fecha, 'DDMMYYYY'), 'years');
                        //        else
                        //            if (row.Edaddetenido != "0")
                        //            {
                        //                return row.Edaddetenido
                        //            }
                        //            return fecha;
                        //    }
                        //},
                        //{
                        //    targets: 2,
                        //    orderable: false,
                        //    render: function (data, type, row, meta) {
                        //        return row.Nombre + " " + row.Paterno + " " + row.Materno;
                        //    }
                        //},
                        {
                            targets: 8,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.Pertenencia != null)
                                    return "Si";
                                else
                                    return "No";
                            }
                        }
                    ]
                });
            };

            function CargarSituaciones(set) {
                $('#situacion').empty();
                $.ajax({

                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/existenciaPorSituacion.aspx/getSituacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#situacion');

                        Dropdown.append(new Option("[Situación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de situaciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("#situacion").change(function () {               
                cargarTabla();
            });

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
            
        });
    </script>
</asp:Content>