﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="oficioTurnacion.aspx.cs" Inherits="Web.Application.Report.oficioTurnacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
        <li>Barandilla</li>
    <li>Oficio de turnación</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="scroll">
        <section id="widget-grid" class="">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-file-text-o"></i>
                        Oficio de turnación
                    </h1>
                </div>
            </div>
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-remisiones-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-file-text-o"></i></span>
                        <h2>Oficio de turnación </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body">
                            <div class="row">
                                <section class="col col-md-12" id="BotonBarandilla">
                                    <a class="btn btn-md btn-default" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Registry/entrylist.aspx" style="padding:5px"><i class="fa fa-mail-reply"></i> Regresar a barandilla</a>
                                </section>
                            </div>
                            <div id="smart-form-register-entry" class="smart-form"></div>
                            <table id="dt_basic_recibos" class="table table-striped table-bordered table-hover" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th data-class="expand">#</th>
                                        <th>No. remisión</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th>Edad</th>
                                        <th>Registro</th>
                                        <th>Lugar de detención</th>
                                        <th>Unidad</th>
                                        <th>Responsable</th>
                                        <th>Motivo de detención</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <footer>
                    </footer>
                </div>
            </article>
        </section>
    </div>
    <div id="autoridad-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class="fa fa-pencil" +=""></i> Autoridad a disposición
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <label style="color: dodgerblue" class="label">Nombre de la autoridad <a style="color: red">*</a></label>
                                <label class="input">
                                    <i class="icon-append fa fa-certificate"></i>
                                    <input type="text" name="autoridad" id="idautoridad" placeholder="Autoridad" class="alphanumeric alptext"/>
                                    <b class="tooltip tooltip-bottom-right">Ingresa el nombre de la autoridad.</b>
                                </label>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();
            ValidarBoton();
            function ValidarBoton() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/autorizacionSalida.aspx/ValidarBoton",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            if (resultado.Boton) {
                                $("#BotonBarandilla").show();
                            }
                            else {
                                $("#BotonBarandilla").hide();
                            }
                        }
                        else {
                            ShowError("¡Error! No fue posible validar si mostrar el boton", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }
            var responsiveHelper_dt_basic_recibos = undefined;
            var responsiveHelper_datatable_fixed_column_recibos = undefined;
            var responsiveHelper_datatable_col_reorder_recibos = undefined;
            var responsiveHelper_datatable_tabletools_recibos = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            
            function generarOficio(datos, autoridad) {

                $.ajax({

                    type: "POST",
                    url: "oficioTurnacion.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: datos,
                        Autoridad: autoridad
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.message + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.message);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $('#dt_basic_recibos').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_recibos) {
                        responsiveHelper_dt_basic_recibos = new ResponsiveDatatablesHelper($('#dt_basic_recibos'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_recibos.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_recibos.respond();
                    $('#dt_basic_recibos').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/oficioTurnacion.aspx/getDetenidos",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Nombre"
                    },
                    {
                        name: "Remision",
                        data: "Expediente"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    null,
                    {
                        name: "Registro",
                        data: "Fecha"
                    },
                    {
                        name: "Lugar",
                        data: "LugarDetencion"
                    },
                    {
                        name: "Unidad",
                        data: "Unidad"
                    },
                    {
                        name: "Responsable",
                        data: "Responsable"
                    },
                    {
                        name: "Motivo",
                        data: "Motivo"
                    },
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false

                    }
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 5,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var fecha = row.Edad != null ? toDate(row.Edad) : '';
                            if (fecha != '')
                                return moment().diff(moment(fecha, 'DDMMYYYY'), 'years');
                            else
                                return row.Edaddetenido;
                        }
                    },
                    {
                        targets: 11,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "imprimir";
                            var registro = '<a class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" data-id="' + row.TrackingId + '" title = "Editar" id="print"><i class="glyphicon glyphicon-file"></i> Generar PDF</a>&nbsp; ';

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                registro = '<a class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" data-reciboid="' + row.ReciboTracking + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;'
                                block = "";
                            }                            

                            return registro;
                        }
                    }
                ]
            });

            $("body").on("click", ".imprimir", function () {
                clean();
                $("#autoridad-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                if (validar()) {
                    var tracking = $("#print").attr("data-id");
                    $("#autoridad-modal").modal("hide");
                    var autoridad = $("#idautoridad").val();
                    generarOficio(tracking, autoridad);
                }
            });
            function clean() {
                $(".input").val("");
                $(".input").removeClass('state-success');
                $(".input").removeClass('state-error');
            }
            function validar() {

                var esvalido = true;

                if ($("#idautoridad").val() == "" || $("#idautoridad").val().split(" ").join("") == "") {
                    ShowError("Autoridad", "El nombre de la autoridad es obligatorio");
                    $('#idautoridad').parent().removeClass('state-success').addClass("state-error");
                    $('#idautoridad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#idautoridad').parent().removeClass("state-error").addClass('state-success');
                    $('#idautoridad').addClass('valid');
                }
                return esvalido;
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>