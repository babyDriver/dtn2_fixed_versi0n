﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="record.aspx.cs" Inherits="Web.Application.Registry.family" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Antecedentes</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }

        .img-thumbnail {
            height: 300px;
        }       
        .scroll2 {
            height: 20vw;
            overflow: auto;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>    
    <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-list-alt"></i>
                Información personal
            </h1>
        </div>
    </div>-->
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="row padding-bottom-10">
            <div class="menu-container" style="display: inline-block; float: right">
                <a style="display: none;font-size:smaller" class="btn btn-default btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
                <a style="display: none;font-size:smaller" class="btn btn-success txt-color-white btn-lg" id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a> 
                <a style="display: none;font-size:smaller" class="btn btn-info btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
                <a style="font-size:smaller" class="btn btn-primary txt-color-white btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
                <a style="display: none;font-size:smaller" class="btn btn-warning btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-purple txt-color-white btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-yellow txt-color-white btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-red txt-color-white btn-lg" id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
                <a style="margin-right:30px; font-size:smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa fa-paw"></i> Biométricos</a>
            </div>
        </div>
        <div class="scroll2" id="ScrollableContent">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-family-2"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-child"></i></span>
                        <h2>Antecedentes </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body">
                            <p></p><!--
                            <div class="row" id="addfamily" style="display: none;">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                    <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                </div>
                            </div>-->
                            <p></p>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover"  width="100%">
                                <thead>
                                        <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                        <th data-hide="phone,tablet">Fecha</th>
                                        <th data-hide="phone,tablet">Centro</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                        <th data-hide="phone,tablet">NCP</th>
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>                                       
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                        <div class="row smart-form">
                            <footer>
                                <div class="row" style="display: inline-block; float: right">
                                    <a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default" id="ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>
                                    <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form ">
                        <div class="row">
                            <fieldset>
                                <section>
                                    <label class="input">Parentesco <a style="color: red">*</a></label>
                                    <select name="_parentesco" id="_parentesco" runat="server" style="width: 100%" class="input-sm">
                                        <option value="0">[Parentesco]</option>
                                    </select>
                                    <i></i>
                                </section>
                                <section>
                                    <label class="input">Nombre(s) <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="nombrefamiliar" id="nombrefamiliar" runat="server" placeholder="Nombre(s)" maxlength="250" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el nombre(s).</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input">Apelido paterno <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="paterno" id="paterno" runat="server" placeholder="Apellido paterno" maxlength="250" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el apellido paterno.</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input">Apelido materno <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="materno" id="materno" runat="server" placeholder="Apellido materno" maxlength="250" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el apellido materno.</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input">Ocupación <a style="color: red">*</a></label>
                                    <select name="ocupacion" id="ocupacion" runat="server" style="width: 100%" class="input-sm">
                                        <option value="0">[Ocupación]</option>
                                    </select>
                                    <i></i>
                                </section>
                                <section class="col col-6">
                                    <label class="input">Sexo <a style="color: red">*</a></label>
                                    <select name="sexo" id="sexo" runat="server" style="width: 100%" class="input-sm">
                                        <option value="0">[Sexo]</option>
                                    </select>
                                    <i></i>
                                </section>
                                <section class="col col-6">
                                    <label class="input">Situación <a style="color: red">*</a></label>
                                    <select name="situacion" id="situacion" runat="server" style="width: 100%" class="input-sm">
                                        <option value="0">[Situación]</option>
                                        <option value="Vivo">Vivo(a)</option>
                                        <option value="Finado">Finado(a)</option>
                                    </select>
                                    <i></i>
                                </section>
                                <header>Domicilio </header>
                                <section class="col col-8">
                                    <label class="input">Calle <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-street-view"></i>
                                        <input type="text" name="calle" id="calle" runat="server" placeholder="Calle" maxlength="250" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la calle.</b>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="input">Número <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-sort-numeric-desc"></i>
                                        <input type="text" name="numero" id="numero" runat="server" placeholder="Número" maxlength="15" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el número.</b>
                                    </label>
                                </section>
                                <section class="col col-8">
                                    <label class="input">Colonia <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-street-view"></i>
                                        <input type="text" name="colonia" id="colonia" runat="server" placeholder="Colonia" maxlength="250" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la colonia.</b>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="input">C.P. <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-sort-numeric-desc"></i>
                                        <input type="text" name="cp" id="cp" runat="server" placeholder="C.P." maxlength="5" class="integer" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el C.P.</b>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input">País <a style="color: red">*</a></label>
                                    <select name="pais" id="pais" runat="server" style="width: 100%" class="input-sm">
                                        <option value="0">[País]</option>
                                    </select>
                                    <i></i>
                                </section>
                                <section class="col col-6" id="sectionestado">
                                    <label class="input">Estado <a style="color: red">*</a></label>
                                    <select name="estado" id="estado" runat="server" style="width: 100%" class="input-sm">
                                        <option value="0">[Estado]</option>
                                    </select>
                                    <i></i>
                                </section>
                                <section class="col col-6" id="sectionmunicipio">
                                    <label class="input">Municipio <a style="color: red">*</a></label>
                                    <select name="municipio" id="municipio" runat="server" style="width: 100%" class="input-sm">
                                        <option value="0">[Municipio]</option>
                                    </select>
                                    <i></i>
                                </section>
                                <section class="col col-6" id="sectionlocalidad">
                                    <label class="input">Localidad <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-street-view"></i>
                                        <input type="text" name="localidad" id="localidad" runat="server" placeholder="Localidad" maxlength="250" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la localidad.</b>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label>Teléfono <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-mobile"></i>
                                        <input type="tel" name="movil" id="telefono" runat="server" placeholder="Teléfono" data-mask="(999) 999-9999" />
                                        <b class="tooltip tooltip-bottom-right">Teléfono</b>
                                    </label>
                                </section>
                            </fieldset>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar familiar
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro <strong><span id="itemeliminar"></span></strong>&nbsp;será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="Ingreso" runat="server" value="" />
    <input type="hidden" id="Informacion_Personal" runat="server" value="" />
    <input type="hidden" id="Antropometria" runat="server" value="" />
    <input type="hidden" id="Señas_Particulares" runat="server" value="" />
    <input type="hidden" id="Adicciones" runat="server" value="" />
    <input type="hidden" id="Estudios_Criminologicos" runat="server" value="" />
    <input type="hidden" id="Expediente_Medico" runat="server" value="" />
    <input type="hidden" id="tracking" runat="server" value="" />
    <input type="hidden" id="editable" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 39) {
                e.preventDefault();
                document.getElementById("linkexpediente").click();
            }
            else if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                document.getElementById("linkseñas").click();
            }
        });

        $(document).ready(function () {
            $("#ScrollableContent").css("height", "calc(100vh - 443px)");

            $("#datospersonales-modal").on("hidden.bs.modal", function () {
                document.getElementsByClassName("detencion")[0].focus();
            });

            pageSetUp();
            init();

            $("#ligarNuevoEvento").hide();
            if ($("#<%= editable.ClientID %>").val() == "0") {
                $("#ligarNuevoEvento").show();
            }

            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },/*
               "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                        $('td', row).eq(7).addClass('strikeout');
                    }
                },*/
                ajax: {
                    type: "POST",
                    url: "record.aspx/getinterno",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = RequestQueryString("tracking");
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    null,
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    null,
                    {
                        name: "Centro",
                        data: "Centro"
                    }
                    ,
                    {
                        name: "Expediente",
                        data: "Expediente",
                        visible: false
                    },
                    {
                        name: "NCP",
                        data: "NCP",
                        visible: false
                    },
                    {
                        name: "EstatusNombre",
                        data: "EstatusNombre"
                    },
                    null
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = ResolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + ResolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" />' +
                                    '</a>' +
                                    '<div>';
                            } else { return ''; }
                        }
                    },
                    {
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            return row.Fecha;
                        }
                    },
                    {
                        targets: 9,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "edit"
                            var transfer = "tranfer";
                            var reentry = "reentry";
                            var registrar = "";
                            var habilitar = "";
                            var egreso = "";
                            var reingreso = "";
                            if (row.Activo) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled"; transfer = "disabled"; reentry = "disabled";

                            }

                            if ($("#ctl00_contenido_KAQWPK").val() == "true")
                                registrar = '<a class="btn btn-primary btn-circle ' + edit + '" href="entry.aspx?tracking=' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            else if ($("#ctl00_contenido_WERQEQ").val() == "true")
                                registrar = '<a class="btn btn-primary btn-circle ' + edit + '" href="entry.aspx?tracking=' + row.TrackingId + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-id="' + row.TrackingIdEstatus + '" data-value = "' + row.Expediente + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                            // if ($("#ctl00_contenido_VYXMBM").val() == "true" & row.Estatus != 2 & $("#ctl00_contenido_KAQWPK").val() == "true") { egreso = '<a class="btn btn-danger btn-circle ' + transfer + '" href="transfer.aspx?tracking=' + row.TrackingId + '" title="Egreso"><i class="glyphicon glyphicon-transfer"></i></a>&nbsp;'; }
                            //  if ($("#ctl00_contenido_RAWMOV").val() == "true" & row.Estatus == 2 & $("#ctl00_contenido_KAQWPK").val() == "true") reingreso = '<a class="btn btn-warning btn-circle ' + reentry + '" href="reentry.aspx?tracking=' + row.TrackingId + '" title="Reingreso"><i class="glyphicon glyphicon-share-alt"></i></a>&nbsp;';

                            return registrar + egreso + reingreso + habilitar;

                        }
                    }
                ]

            });

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);
                $("#photo-arrested").modal("show");
            });



            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    CargarDatos(param);
                }
                if ($("#ctl00_contenido_KAQWPK").val() == "false")
                    $("#btnsave").hide();

                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addfamily").show();
                }
                else {
                    $("textarea,input, select").each(function (index, element) { $(this).attr('disabled', true); });
                }
                if ($("#ctl00_contenido_Ingreso").val() == "true") {
                    $("#linkingreso").show();
                }
                if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                    $("#linkinformacion").show();
                }
                if ($("#ctl00_contenido_Antropometria").val() == "true") {
                    $("#linkantropometria").show();
                }
                if ($("#ctl00_contenido_Señas_Particulares").val() == "true") {
                    $("#linkseñas").show();
                }
                if ($("#ctl00_contenido_Estudios_Criminologicos").val() == "true") {
                    $("#linkestudios").show();
                }
                if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                    $("#linkexpediente").show();
                }
                if ($("#ctl00_contenido_Adicciones").val() == "true") {
                    $("#linkadicciones").show();
                }
                $("#linkfamiliares").show();
                $("#linkInfoDetencion").show();

            }



            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "record.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            $('#edadInterno').text(resultado.obj.Edad);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);
                            $('#sexoInterno').text(resultado.obj.Sexo);
                            $('#municipioInterno').text(resultado.obj.municipioNombre);
                            $('#domicilioInterno').text(resultado.obj.domiclio);
                            $('#coloniaInterno').text(resultado.obj.coloniaNombre);


                            $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkvistas").attr("href", "huella.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkestudios").attr("href", "record.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());

                            $("#linkseñas").attr("href", "signal.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkingreso").attr("href", "entry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }


            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
                var param = RequestQueryString("tracking");
                CargarDatosModal(param);
            });

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 443px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');

                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 216px)");
            });

        });
        </script>
</asp:Content>
