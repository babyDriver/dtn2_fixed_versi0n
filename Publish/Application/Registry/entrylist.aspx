﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="entrylist.aspx.cs" Inherits="Web.Application.Registry.entrylist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
  <li>Registro en barandilla</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' />
    <style type="text/css">
        #mapid {
            height: 30vw;
            width: 100%;
        }

        .coordinates {
            background: rgba(0,0,0,0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding: 5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }

        td.strikeout {
            text-decoration: line-through;
        }

        .wrapping {
            /* These are technically the same, but use both */
            overflow-wrap: break-word;
            word-wrap: break-word;

            -ms-word-break: break-all;
            /* This is the dangerous one in WebKit, as it breaks things wherever */
            word-break: break-all;
            /* Instead use this non-standard one: */
            word-break: break-word;
    
            /* Adds a hyphen where the word breaks, if supported (No Blink) */
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
    </style>
    <div class="scroll">
       
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry" style="display: none;" >
        <div class="col-xs-12 col-sm-12 col-md-12 ">
            <a class="btn btn-md btn-default events" id="events">
                <img style="width:15px; height:15px; filter: brightness(0%); transform: translateY(-3px);" src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/img/EventAlarmWhite.png" />
                &nbsp;Eventos
            </a>
            <a class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
            <a class="btn btn-md btn-default " id="agrupar"><i class="fa fa-object-group"></i>&nbsp;Agrupar </a>
            <div class="btn-group" style="float:right">
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style="width:275px">Reportes barandilla<span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a class="btn-sm " title="Remisiones" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/remisiones_report.aspx">Remisiones</a></li>
                        <li class="divider"></li>
                        <li><a class="btn-sm " title="Informe de detención y boleta de control" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/reportes_detencion_boleta.aspx">Informe de detención y boleta de control</a></li>
                        <li class="divider"></li>
                        <li><a class="btn-sm " title="Autorización de salida por pago de multa" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/autorizacionSalidaB.aspx">Autorización de salida por pago de multa</a></li>
                        <li class="divider"></li>
                        <li><a class="btn-sm " title="Oficio de turnación" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/oficioTurnacion.aspx">Oficio de turnación</a></li>
                        <li class="divider"></li>
                        <li><a class="btn-sm " title="Corte de detenidos" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/corte_detenidos.aspx">Corte de detenidos</a></li>
                        <li class="divider"></li>
                        <li><a class="btn-sm " title="Reporte de ingresos" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/ReporteIngresos.aspx">Reporte de ingresos</a></li>
                    </ul>
                </div>
        </div>
    </div>
    <p></p>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-users-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar interno </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <%--<label class="select">--%>
                                                <select name="peril" id="registro" runat="server" class="select2" style="width:100%;">
                                                    <option value="0">[Perfil]</option>
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                    
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="nombre" id="nombre" runat="server" placeholder="Nombre" maxlength="256"/>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-group"></i></span>
                            <h2>Detenidos </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                        <section>
                                            <label>Año </label>
                                            <select id="idAnioDetenidos" class="select2"></select>
                                        </section>
                                    </div>
                                </div>
                                <table id="dt_basic" class="table table-striped table-bordered table-hover"  width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th data-class="expand">#</th>
                                            <th>Fotografía</th>
                                            <th data-hide="phone,tablet">Nombre</th>
                                            <th data-hide="phone,tablet">Apellido Paterno</th>
                                            <th data-hide="phone,tablet">Apellido Materno</th>
                                            <th data-hide="phone,tablet">No. remisión</th>
                                            <th data-hide="phone,tablet">NCP</th>
                                            <th data-hide="phone,tablet">Estatus</th>
                                            <th data-hide="phone,tablet">Fecha</th>
                                            <th data-hide="phone,tablet">Acciones</th>
                                            <th></th>
                                        </tr>
                                    </thead>                               
                                </table>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section> 
    

    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-keyboard="true" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el No. remisión  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                            
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="Agrupado-modal" class="modal fade" tabindex="-1" data-keyboard="true" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div2" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    Se agruparan los detenidos seleccionados con el registro de 
                                     <strong><span id="verb2"></span></strong><br />
                                    ¿Desea continuar?

                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuaragrupado"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                            
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="evento-reciente-modal"  data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="btncanceleventoX" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        Eventos recientes
                        <i class='fa fa-pencil' style="float:left; margin-top:4px; margin-right:3px;"></i>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label style="color:dodgerblue">Eventos de las últimas <a style="color:red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="eventoReciente" class="select2" style="width:100%;">
                                        </select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section >
                                    <label  style="color:dodgerblue">Evento <a style="color:red">*</a></label>
                                    <label class="select">
                                        <select name="rol" id="evento">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                                <section>
                                    <label  style="color:dodgerblue">Detenido <a style="color:red">*</a></label>
                                    <label class="select">
                                        <select name="rol" id="detenido">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>

                                <section>
                                    <label class="input" id="idsexo"></label>
                                </section>
                                <section>
                                    <label class="input" id="idedad"></label>
                                </section>
                                <%--<section>
                                    <label>Fotografía del detenido </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-file-image-o"></i>
                                        <asp:FileUpload ID="fileUpload" runat="server" />
                                    </label>
                                </section>--%>
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" id="btncancelevento"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default save" id="save"><i class="fa fa-save"></i>&nbsp;Continuar </a>                            
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div><input type="hidden" id="originalid"  value="" />

    <div class="modal fade" id="Agrupar-modal" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        <i class="fa fa-object-group"></i>
                        Agrupar detenidos
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Agrupar-form" class="smart-form">
                        <fieldset>
                          <span><label class="label">Criterios para búsqueda de coincidencias</label>   </span>
                                      
                       
                            <div class="row">
                               <section class="col col-6">
                                    <label class="label"  style="color:dodgerblue">Nombre(s)</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="nombre"  id="idnombre" placeholder="Nombre" maxlength="50" class="alphanumeric"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el nombre.</b>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label"  style="color:dodgerblue">Apellido paterno</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="apellido_paterno"  id="paterno" placeholder="Apellido paterno" maxlength="50" class="alphanumeric"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el apellido paterno.</b>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label"  style="color:dodgerblue">Apellido materno</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="apellido_materno"  id="materno" placeholder="Apellido materno" maxlength="50" class="alphanumeric"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el apellido materno.</b>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label"  style="color:dodgerblue">Alias</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="alias"  id="alias" placeholder="Alias" maxlength="50" class="alphanumeric"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el alias.</b>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-12">
                                    <button  type="button" class="btn btn-primary btn-sm " id="buscar" title="Buscar coincidencias"><i class="fa fa-search"></i>&nbsp;Buscar coincidencias </button>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-12">
                                        <label class="label" id="iddetenido"></label>
                                </section>
                            </div>
                            <div class="row">
                                <table id="dt_basicGroup" class="table table-striped table-bordered table-hover"  width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th data-class="expand">#</th>
                                            <th data-hide="phone,tablet">Apellido Paterno</th>
                                            <th data-hide="phone,tablet">Apellido Materno</th>
                                            <th data-hide="phone,tablet">Nombre</th>
                                            <th data-hide="phone,tablet">Alias</th>
                                            <th data-hide="phone,tablet">No. remisión</th>
                                            <th data-hide="phone,tablet">Acciones</th>
                                            <th></th>
                                        </tr>
                                    </thead>                               
                                </table>
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default " id="btnAgrupar"><i class="fa fa-object-group"></i>&nbsp;Agrupar </a>                            
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="originalid"  value="" />
    <input type="hidden" id="avatarOriginal" runat="server" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="VYXMBM" runat="server" value=""/>
    <input type="hidden" id="RAWMOV" runat="server" value=""/>
    <input type="hidden" id="WERQEQ" runat="server" value=""/>
    <input type="hidden" id="estado" value=""/>
    
    <div id="photo-arrested" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="<%= ConfigurationManager.AppSettings["relativepath"]  %> #" alt="fotografía del detenido" /> 
                                </div> 
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="events-list-modal" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog" style="width: 180vh;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        <img style="width:25px; height:25px; filter: brightness(0%); transform: translateY(-3px);" src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/img/EventAlarmWhite.png" />                        
                        Lista de Eventos
                    </h4>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <div id="events-list-form" class="smart-form">
                        <fieldset>
                            <a class="btn btn-md btn-default event-form" style="padding:6px 12px;"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                            <br />
                            <br />
                            <div class="row" style="margin: 0 0px;">
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                    <section>
                                        <label>Año </label>
                                        <select id="anioSelect" class="select2"></select>
                                    </section>
                                </div>
                            </div>
                            <div class="row" style="margin:0">
                                <table id="dt_basic_events" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                    <thead>
                                        <tr>                                        
                                            <th data-class="expand">#</th> 
                                            <th data-hide="phone,tablet">Llamada</th>
                                            <th data-hide="phone,tablet">Descripción evento</th>
                                            <th data-hide="phone,tablet">Folio</th>
                                            <th data-hide="phone,tablet">Fecha</th>
                                            <th data-hide="phone,tablet">Asentamiento</th>                                      
                                            <th data-hide="phone,tablet">Código postal</th>
                                            <th data-hide="phone,tablet">Municipio</th>                                      
                                            <th data-hide="phone,tablet">Estado</th> 
                                            <th data-hide="phone,tablet">Lugar de detención</th>                                   
                                            <th data-hide="phone,tablet">Acciones</th>                                       
                                        </tr>
                                    </thead>                               
                                </table>
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;Cerrar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-events-list-modal" class="modal fade" tabindex="-1" data-keyboard="true" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div3" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb-events"></span></strong>&nbsp;el registro  <strong>&nbsp<span id="itemnameblock-events-list"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar-events-list"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="events-form-modal"   data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog" style="width: 170vh;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="btn-events-form-modal-close" aria-hidden="true">
                        &times;
                    </button>
                    <span class="widget-icon"><i class="glyphicon  glyphicon-edit"></i></span>
                    <h4 class="modal-title" style="display: inline-block">
                        Evento
                    </h4>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    <div id="events-form-form" class="smart-form">
                        <fieldset>
                            <input type="hidden" id="trackingEvent" />
                            <div class="row">
                                <header class="col col-1" style="border-bottom: none;" id="alerta1">Alerta web</header>
                                <section class="col col-4">
                                    <label class="label">Institución</label>
                                    <select name="Unidad" id="alertaWebInstitucion" class="select2" style="width:100%;"></select>
                                    <i></i>
                                </section>
                                <section class="col col-4">
                                    <label class="label">Unidad</label>
                                    <select name="Unidad" id="alertaWebUnidad" class="select2" style="width:100%;"></select>
                                    <i></i>
                                </section>
                                <a style="float: right; margin-right: 15px; display: none;" id="add-events-form" class="btn btn-primary btn-sm" href="javascript:void(0);"><i class="fa fa-plus"></i>&nbsp;Registro en barandilla</a>
                            </div>
                            <div class="row table-responsive" style="margin:0">
                                <table id="dt_basic_tabla_eventos" class="table table-striped table-bordered table-hover" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th data-class="expand"></th>
                                            <th data-class="phone,tablet,desktop">Folio</th>
                                            <th data-hide="phone,tablet,desktop">Fecha</th>
                                            <th data-hide="phone,tablet,desktop">Motivo</th>
                                            <th data-hide="phone,tablet,desktop">Número detenidos</th>
                                            <th data-hide="phone,tablet,desktop">Detenidos</th>
                                            <th data-hide="phone,tablet,desktop">Estado</th>
                                            <th data-hide="phone,tablet,desktop">Municipio</th>
                                            <th data-hide="phone,tablet,desktop">Colonia</th>
                                            <th data-hide="phone,tablet,desktop">Número</th>
                                            <th data-hide="phone,tablet,desktop">Entre calle y calle</th>
                                            <th data-hide="phone,tablet,desktop">Responsable</th>
                                            <th data-hide="phone,tablet,desktop">Descripción</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="row">
                                <header id="localizacionHeader"></header>
                            </div>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Llamada</label>
                                    <select name="Llamada" id="llamada" class="select2" style="width: 100%;"></select>
                                </section>
                                <section class="col col-3">
                                    <label class="label">Fecha y hora <a style="color: red">*</a></label>
                                    <label class="input">
                                        <div class='input-group date' id='autorizaciondatetimepicker'>
                                            <input type="text" name="Fecha y hora" id="fecha" class='form-control' placeholder="Fecha y hora de ingreso" data-requerido="true" />
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </label>
                                </section>
                                <section class="col col-2">
                                    <label class="label">Folio <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="Folio" id="folio" placeholder="Folio" maxlength="50" class="alphanumeric alptext" data-requerido="true" disabled="disabled" />
                                    </label>
                                </section>
                                <section class="col col-4">
                                                <label class="label">Motivo </label>
                                                <select name="Motivo" id="motivoevento" class="select2" style="width: 100%;" >
                                               
                                                    </select>
                                            </section>
                            </div>
                            <div class="row">
                                <section class="col col-8">
                                    <label class="label">Descripción<a style="color: red">*</a></label>
                                    <label class="input">
                                        <textarea id="descripcion" name="Descripción" placeholder="Descripción" maxlength="1000" class="form-control alptext" rows="6" data-requerido="true"></textarea>
                                        <b class="tooltip tooltip-bottom-right">Ingresa la descripción.</b>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="label">Lugar detención <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="Lugar detención" id="lugar" placeholder="Lugar detención" maxlength="1000" class="alphanumeric alptext" data-requerido="true" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el lugar de detención.</b>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="label">Municipio <a style="color: red">*</a></label>
                                    <select name="Municipio" id="municipio" class="select2" style="width:100%;"></select>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">Colonia <a style="color: red">*</a></label>
                                    <select name="Colonia" id="colonia" data-requerido="true" class="select2" style="width:100%;"></select>
                                </section>
                                <section class="col col-2">
                                    <label class="label">Código postal <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="Código postal" id="codigoPostal" placeholder="Código Postal" maxlength="6" class="numeric" data-requerido="true" disabled="disabled" />
                                    </label>
                                </section>
                                <section class="col col-2">
                                    <label class="label">Número detenidos <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="Número detenidos" id="numeroDetenidos" placeholder="Número detenidos" maxlength="6" class="number" data-requerido="true" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el número de detenidos.</b>
                                    </label>
                                </section>
                                <section class="col col-2">
                                    <label class="label">Latitud</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="Latitud" id="latitud" placeholder="Latitud" maxlength="25" class="number" data-requerido="false" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese la latitud.</b>
                                    </label>
                                </section>
                                <section class="col col-2">
                                    <label class="label">Longitud</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="Longitud" id="longitud" placeholder="Longitud" maxlength="25" class="number" data-requerido="false" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese la longitud.</b>
                                    </label>
                                </section>
                            </div>
                            <footer>
                                <a class="btn btn-default" data-dismiss="modal"  id="cancel_" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                <a class="btn btn-default" id="save_" title="Guardar registro actual"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            </footer>
                            <div class="row">
                                <div class="col col-6">
                                    <div class="row">
                                        <header>Detenidos</header>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col col-12">
                                            <a style="float: left; display: none; font-size: smaller" class="btn btn-default  btn-lg" title="Agregar Detenidos" id="linkDetenidos"><i class="fa fa-plus"></i> Agregar detenido</a>
                                        </div>
                                    </div>
                                    <br />
                                    <table id="dt_basic_tabla_detenidos" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand">Nombre</th>
                                                <th data-hide="phone,tablet">Apellido paterno</th>
                                                <th data-hide="phone,tablet">Apellido materno</th>
                                                <th data-hide="phone,tablet">Sexo</th>
                                                <!--<th data-hide="phone,tablet">Edad</th>-->
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col col-6">
                                    <div class="row">
                                        <header>Unidades involucradas</header>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col col-12">
                                            <a style="float: left; display: none; font-size: smaller" class="btn btn-default  btn-lg" title="Agregar Unidad" id="linkUnidad"><i class="fa fa-plus"></i>Agregar unidad</a>
                                        </div>
                                    </div>
                                    <br />
                                    <table id="dt_basic_tabla_unidades" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand">Unidad</th>
                                                <th data-hide="phone,tablet">Clave-responsable</th>
                                                <th data-hide="phone,tablet">Corporación</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <header>Ubicación del evento en el mapa</header>
                                <br />
                            </div>
                            <div id="mapid"></div>
                    <div id="map"></div>
                            <pre id='coordinates' class='coordinates'></pre>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal" id="events-form-modal-close"><i class="fa fa-close"></i>&nbsp;Cerrar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalUnidad" data-backdrop="static" role="dialog" data-keyboard="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-unidad-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-unidad" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="label" style="color: dodgerblue">Unidad <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>                                                
                                        <select name="unidad" id="unidad" style="width: 100%" class="select2" data-requerido-unidad="true"></select>
                                    </label>
                                </section>
                                <section>
                                    <label class="label" style="color: dodgerblue">Clave-responsable <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>                                                
                                        <select name="responsable" id="responsable" style="width: 100%" class="select2" data-requerido-unidad="true"></select>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="events-form-unidades-modal-close"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default" id="btnGuardarUnidad"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDetenido"  role="dialog"  data-keyboard="true" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-detenido-title"></h4>
                </div>
                <div class="modal-body1">
                    <br />
                    
                    
                    <div id="register-detendio-form" class="smart-form">
                        
                     
                        <fieldset>
                           
                            <div class="row" style="width:100%;margin:0 auto;">
                                    <table id="dt_nuevo_detenido" class="table table-striped table-bordered table-hover" width="100%" >
                                        <thead>
                                            <tr>
                                                <th style="width:8%  !important; "></th>
                                                <th style="text-align:center;" >Nombre</th>                                                
                                                <th style="text-align:center">Apellido paterno</th>                                                
                                                <th style="text-align:center">Apellido materno</th>
                                                
                                                <th style="text-align:center">Sexo</th>
                                               <!-- <th style="text-align:center">Edad</th>-->
                                                <th style="text-align:center">Motivo</th>
                                                <%--<th style="text-align:center">Acciones</th>--%>

                                                
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                           
                        </fieldset>
                        <footer>
                           <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="events-form-detenido-modal-close"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save2" id="btnGuardarDetenido"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            <a href="javascript:void(0);" class="btn btn-md btn-primary " id="addRow"  style="margin-left:10px"><i class="fa fa-plus"></i>&nbsp;Agregar nueva fila </a>
                        </footer>
                    </div>
                      
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetenido2"  role="dialog" data-keyboard="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-detenido-title2"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-detendio-form2" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="label" style="color: dodgerblue">Nombre <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="nombreDetenido" id="nombreDetenido" placeholder="Nombre" maxlength="250" class="alphanumeric alptext" data-requerido-detenido="true" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el nombre.</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="label" style="color: dodgerblue">Apellido paterno <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="paternoDetenido" id="paternoDetenido" placeholder="Paterno" maxlength="250" class="alphanumeric alptext" data-requerido-detenido="true" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el apellido paterno.</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="label" style="color: dodgerblue">Apellido materno <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input type="text" name="maternoDetenido" id="maternoDetenido" placeholder="Materno" maxlength="250" class="alphanumeric alptext" data-requerido-detenido="true" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el apellido materno.</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="label" style="color: dodgerblue">Sexo <a style="color: red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="sexoDetenido" id="sexoDetenido" data-requerido-detenido="true" class="select2" style="width:100%;"></select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section>
                                    <label class="label" style="color: dodgerblue">Edad: <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-sort-numeric-asc"></i>
										<input name="edad"  type="text"  id="edad" title="Edad"  pattern="^[0-9]*$" maxlength="3"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese la edad.</b>
									</label>
                                    <%--<label class="input">
                                        <label class='input-group date' id='FehaNacimientodatetimepicker'>
                                            <input type="text" name="fechanacimiento" id="fechanacimiento" class='input-group date' placeholder="Fecha de nacimiento" data-requerido-detenido="true" />
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </label>
                                    </label>--%>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <%--<a class="btn btn-sm btn-default cancel" id="events-form-detenido-modal-close"><i class="fa fa-close"></i>&nbsp;Cancelar </a>--%>
                            <a class="btn btn-sm btn-default" id="btnGuardarDetenido2"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="events-form-evento-reciente-modal" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="btn-events-form-evento-reciente-modal-close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa  fa-folder"></i> Eventos recientes</h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-detenido" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section >
                                    <label style="color: dodgerblue">Evento<a style="color: red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="evento-events-form-evento-reciente" disabled="disabled"  style="background-color:#eee; width:100%;" class="select2" >
                                        </select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section>
                                    <label style="color: dodgerblue">Detenido <a style="color: red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="detenido-events-form-evento-reciente" class="select2" style="width:100%;"></select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section>
                                    <label class="input" id="idsexo-events-form"></label>
                                </section>
                                <section>
                                    <label class="input" id="idedad-events-form"></label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" id="events-form-evento-reciente-modal-close"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default events-form-saveDetenido" id="saveDetenido-events-form-evento-reciente"><i class="fa fa-save"></i>&nbsp;Continuar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <input type="hidden" id="L1" />
    <input type="hidden" id="L2" />
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.keyCode === 27) {
                if ($("#modalUnidad").is(":visible")) {
                    $("#modalUnidad").modal("hide");
                }
                else if ($("#modalDetenido").is(":visible")) {
                    $("#modalDetenido").modal("hide");
                }
                else if ($("#events-form-evento-reciente-modal").is(":visible")) {
                    $("#events-form-evento-reciente-modal").modal("hide");
                }
                else if ($("#events-form-modal").is(":visible")) {
                    $("#events-form-modal").modal("hide");
                }
                else if ($("#blockitem-events-list-modal").is(":visible")) {
                    $("#blockitem-events-list-modal").modal("hide");
                }
                else if ($("#events-list-modal").is(":visible")) {
                    $("#events-list-modal").modal("hide");
                }
                else if ($("#evento-reciente-modal").is(":visible")) {
                    $("#evento-reciente-modal").modal("hide");
                }
            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#modalUnidad").is(":visible")) {
                    document.getElementById("btnGuardarUnidad").click();
                }
                else if ($("#modalDetenido").is(":visible")) {
                    document.getElementById("btnGuardarDetenido").click();
                }
                else if ($("#events-form-evento-reciente-modal").is(":visible")) {
                    document.getElementById("saveDetenido-events-form-evento-reciente").click();
                }
                else if ($("#events-form-modal").is(":visible")) {
                    document.getElementById("save_").click();
                }
                else if ($("#evento-reciente-modal").is(":visible")) {
                    document.getElementsByClassName("save")[0].click();
                }
                else if ($("#Agrupar-modal").is(":visible")) {
                    document.getElementById("btnAgrupar").click();
                }
            }
        });
        var maps, infowindow;
        function initMap() {
            maps = new google.maps.Map(document.getElementById('map'), {
                center: { lat: 25.4653403, lng: 0 },
                zoom: 15
            });
            infoWindow = new google.maps.InfoWindow;

            var lat = 25.4653403;
            var lon = -101.0320401;
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getpositiobycontract",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                processdata: true,
                traditional: true,
                data: JSON.stringify({ LlamadaId: "" }),
                cache: false,
                success: function (response) {
                    response = JSON.parse(response.d);
                    if (response.exitoso) {
                        $("#L1").val(response.obj.Latitud);
                        $("#L2").val(response.obj.Longitud);
                    } else {

                    }
                },
                error: function () {
                    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                }
            });
        }

        $(document).ready(function () {
            $("#idAnioDetenidos").change(function () {
                window.table.api().ajax.reload();
            });
            $(".motivoeventogrid").select2();
            var fecha = new Date();
            var anio = fecha.getFullYear();
            loadDetenidosPorAnio(anio);
            //$("#idAnioDetenidos").val(2020);

            getalerta();
            function getalerta() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/GetAlertaWeb",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.d != null) {
                            data = data.d;

                            var alerta1 = data.Denominacion;
                            var alerta2 = data.AlertaWerb;
                            
                            //$("#Denominacin").val(data.Denominacion);

                            //$("#linkEvento").html('<i class="glyphicon glyphicon-folder-open"></i>&nbsp; ' + alerta1 + ' / evento');
                            $("#alerta1").html(alerta1);
                            //$("#alerta2").html('<i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>' + alerta1);
                            //$("#alerta3").html(' <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de ' + alerta2);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de alerta web. Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
            }

            function loadDetenidosPorAnio(setvalor) {

                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/LoadDetenidosAniosTrabajo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idAnioDetenidos');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Selecione año]", "0"));

                        if (response.d.length > 0) {
                            setvalue = response.d[0].Id;
                        }

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });
                        //alert(setvalor);
                        if (setvalor != "") {
                            Dropdown.val(setvalor);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de estados. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            var rutaDefaultServer = "";

            getRutaDefaultServer();

            function getRutaDefaultServer() {
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;
                        }
                    }
                });
            }

            pageSetUp();
            var _datos = [];
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_dt_basicGroup = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            //CargarGirdGroup("");
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

             var responsiveHelper_dt_nuevo_detenido = undefined;

      
            var dataMotivo = null;
            var datosMotivo = [];

            var dataSexo = null;
            var datosSexo = [];

             function loadMotivos(set) {
                 $.ajax({                     
                    type: "POST",
                    url: "eventsform.aspx/getMotivogrid",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        
                    }),
                    success: function (response) {
                        var Dropdown = $("#motivoevento");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Motivo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de motivos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
        
            $("#addRow").click(function () {
                $("#dt_nuevo_detenido").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""

                ]).draw(false);
                $(".motivoeventogrid").select2();
            });

            LoadNuevoDetenido();
            function LoadNuevoDetenido() {

                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getDataDetenidoNuevo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        interno: "0"
                    }),
                    cache: false,
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        responsiveHelper_dt_pertenencias_nuevas = undefined;
                        $("#dt_nuevo_detenido").DataTable().destroy();
                        Createtablenuevodetenido(resultado.list);
                        $(".motivoeventogrid").select2();
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la tabla. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });

            }

            function cargaMotivorGrid() {

                if (dataMotivo == null && datosMotivo.length == 0) {

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "entrylist.aspx/getMotivogrid",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            dataMotivo += "<option></option>";

                            $.each(response.d, function (index, item) {
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;

                                datosMotivo.push(tt);
                                dataMotivo += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                            });
                        },
                        error: function () {

                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");

                        }
                    });
                }
                return datosMotivo;
            }

            function cargarSexo() {

                if (dataSexo == null && datosSexo.length == 0) {

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "entrylist.aspx/getSexogrid",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            dataSexo += "<option></option>";

                            $.each(response.d, function (index, item) {
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;

                                datosSexo.push(tt);
                                dataSexo += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                            });
                        },
                        error: function () {

                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");

                        }
                    });
                }
                return datosSexo;
            }

           

            function eliminaFilas() {

                //$("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                //    $(this).addClass('selected');
                //    //$(this).node().addClass('1selected');
                //   $(this).parent().addClass("selectedRow");

                //});
                var table = $('#dt_nuevo_detenido').DataTable();
            $('#dt_nuevo_detenido tbody tr').each(function () {
                     

                    
                        $(this).addClass('selected');
                      
                    table.row($(this)).remove().draw(false);
                });


            }

             $("#modalDetenido").on('shown.bs.modal', function () {
                var table = $('#dt_nuevo_detenido').DataTable();
                 eliminaFilas();
                 table.row('.selected').remove().draw(false);
                $("#dt_nuevo_detenido").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""

                ]).draw(false);
                 $(".motivoeventogrid").select2();
                table.columns.adjust().draw();
            });
      
            function validarCamposEnTabla() {
                var isValid = true;

                $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var nombre = this.node().childNodes[1].childNodes[0].getAttribute("data-required");
                    var Apellidopaterno = this.node().childNodes[2].childNodes[0].getAttribute("data-required");
                    var Apellidomaterno = this.node().childNodes[3].childNodes[0].getAttribute("data-required");
                    //var bolsa = this.node().childNodes[4].childNodes[0].getAttribute("data-required");                        
                    var Sexo = this.node().childNodes[4].childNodes[0].getAttribute("data-required");
                   // var Edad = this.node().childNodes[5].childNodes[0].getAttribute("data-required");
                    var Motivo = this.node().childNodes[5].childNodes[0].getAttribute("data-required");

                    if (nombre === "true") {
                        if (this.node().childNodes[1].childNodes[0].value === "" ||
                            this.node().childNodes[1].childNodes[0].value === undefined ||
                            this.node().childNodes[1].childNodes[0].value === null) {
                            this.node().childNodes[1].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Nombre", "El campo nombre es obligatorio.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[1].childNodes[0].removeAttribute('class');
                        }
                    }

                    if (Apellidopaterno === "true") {
                        if (this.node().childNodes[2].childNodes[0].value === "" ||
                            this.node().childNodes[2].childNodes[0].value === undefined ||
                            this.node().childNodes[2].childNodes[0].value === null) {
                            this.node().childNodes[2].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Apellido paterno", "El campo apellido paterno es obligatorio.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[2].childNodes[0].removeAttribute('class');
                        }
                    }

                    if (Apellidomaterno === "true") {
                        if (this.node().childNodes[3].childNodes[0].value === "" ||
                            this.node().childNodes[3].childNodes[0].value === undefined ||
                            this.node().childNodes[3].childNodes[0].value === null) {
                            this.node().childNodes[3].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Apellido materno", "El campo apellido materno es obligatorio.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[3].childNodes[0].removeAttribute('class');
                        }
                    }

                    if (Sexo === "true") {
                        if (this.node().childNodes[4].childNodes[0].value === "0" ||
                            this.node().childNodes[4].childNodes[0].value === undefined ||
                            this.node().childNodes[4].childNodes[0].value === null) {
                            this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Sexo", "El sexo es obligatorio.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[1].childNodes[0].removeAttribute('class');
                        }
                    }

                    /*
                    if (Edad == "true") {
                        if (this.node().childNodes[5].childNodes[0].value === "") {
                            this.node().childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Edad", "El campo edad es obligatorio.");
                            isValid = false;
                        }
                        else {
                            if (this.node().childNodes[5].childNodes[0].value === "0") {
                                this.node().childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Edad", "El campo edad debe ser mayor a 0.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[5].childNodes[0].removeAttribute('class');
                            }
                        }
                    }*/

                   
                });

                return isValid;
            }
                     
            function Createtablenuevodetenido(data) {
                $('#dt_nuevo_detenido').dataTable({
                    "lengthMenu": [5, 10, 20, 50],
                    iDisplayLength: 7,
                    serverSide: false,
                    paging: true,
                    retrieve: true,
                    //"scrollY": "350px",
                    //"scrollCollapse": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_nuevo_detenido) {
                            responsiveHelper_dt_nuevo_detenido = new ResponsiveDatatablesHelper($('#dt_nuevo_detenido'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_nuevo_detenido.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_nuevo_detenido.respond();
                        $('#dt_nuevo_detenido').waitMe('hide');
                    },
                    data: data,
                    columns: [
                        null,
                        null,
                        null,
                        null,
                        null,
                       // null,
                        null
                        //null
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return '<a class="btn btn-xs btn-danger btn-circle removeRow" title="Eliminar"><i class="glyphicon glyphicon-remove"></i></a>';
                            }
                        },
                        {
                            width: "200px",
                            targets: 1,
                            orderable: true,
                            render: function (data, type, row, meta) {
                                return "<input type='text' value='' style='width:100%' data-required='true' />";
                            }
                        },
                        {
                            width: "200px",
                            targets: 2,
                            orderable: true,
                            render: function (data, type, row, meta) {
                                // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                                return "<input type='text' value='' style='width:100%' data-required='true' />";
                            }
                        },
                        {
                            width: "200px",
                            targets: 3,
                            orderable: true,
                            render: function (data, type, row, meta) {
                                // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                                return "<input type='text' value='' style='width:100%' data-required='true' />";
                            }
                        },
                        {
                            width: "145px",
                            targets: 4,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var datos = cargarSexo();
                                var options = "";
                                options += "<option value='0'>[Sexo]</option>";

                                for (var i = 0; i < datos.length; i++) {
                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                }

                                return "<select style='width:100%' data-required='true'>" + options + "</select>";
                            }
                        },/*
                        {
                            width: "40px",
                            targets: 5,
                            orderable: true,
                            render: function (data, type, row, meta) {
                                // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                                return "<input type='text' maxlength='3' pattern='^[0-9]*$' value='' style=' width:100%' data-required='true'  />";
                            }
                        },*/
                        {
                            width: "440px",
                            targets: 5,
                            orderable: true,
                            render: function (data, type, row, meta) {
                                // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                                var datos = cargaMotivorGrid();
                                var options = "";
                                options += "<option value='0'>[Motivo]</option>";

                                for (var i = 0; i < datos.length; i++) {

                                    if (parseInt(datos[i].Id) == parseInt($("#motivoevento").val())) {
                                        options += "<option value='" + datos[i].Id + "'selected='selected'>" + datos[i].Desc + "</option>";
                                    }
                                    else {
                                        options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                    }
                                }

                                return "<select name='Motivo' id='motivoeventogrid' class='select2 motivoeventogrid'  style='width:100%' data-required='true'>" + options + "</select>";
                            }
                        }
                        //,
                        //{
                        //     targets: 7,
                        //    orderable: false,
                        //    render: function (data, type, row, meta) {
                        //        return '<a class="btn btn-primary btn-circle  deleteitem "  title="Eliminar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                        //    }
                        //}
                    ]
                });
            }

            $("body").on("click", ".removeRow", function () {
                var table = $('#dt_nuevo_detenido').DataTable();
                $(this).parent().parent().addClass("selectedRow");
                table.row('.selectedRow').remove().draw(false);
            });

            function CargarGirdGroup(id) {
                var Filtro = ObtieneValoresFiltro();
                responsiveHelper_dt_basicGroup = undefined;
                window.table = $('#dt_basicGroup').dataTable({
                    destroy: true,

                    "lengthMenu": [10, 20, 50, 100],
                    // iDisplayLength: 5,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,

                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basicGroup) {
                            responsiveHelper_dt_basicGroup = new ResponsiveDatatablesHelper($('#dt_basicGroup'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basicGroup.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basicGroup.respond();
                        $('#dt_basicGroup').waitMe('hide');
                    },

                    ajax: {
                        type: "POST",
                        url: "entrylist.aspx/getinternoFiltro",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basicGroup').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            parametrosServerSide.emptytable = false;
                            Filtro.Id = id;
                            parametrosServerSide.Filtro = Filtro;
                            return JSON.stringify(parametrosServerSide);
                        }

                    },
                    columns: [
                        {
                            data: "TrackingId",
                            targets: 0,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                return "";
                            }
                        },
                        null,
                        null,


                        {
                            name: "Nombre",
                            data: "Paterno"
                        },
                        {
                            name: "Paterno",
                            data: "Materno"
                        },
                        {
                            name: "Materno",
                            data: "Nombre"
                        }
                        ,
                        null,
                        {
                            name: "Expediente",
                            data: "Expediente"
                        },

                        null,
                        {
                            name: "NombreCompleto",
                            data: "NombreCompleto",
                            visible:false
                        }
                    ],
                    columnDefs: [

                        {
                            data: "TrackingId",
                            targets: 0,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                return "";
                            }
                        },
                        {
                            targets: 1,
                            orderable: false,
                            className: 'select-checkbox',
                            render: function (data, type, row, meta) {
                                if (id == "") {
                                    return '<div class="text-center"><input type="checkbox" id="groupcheck" disabled="disabled" data-TrackingId="' + row.TrackingId + '" data-internoId="' + row.Id + '" data-TrabajoSocialId="' + row.TrabajoSocialId + '" name="elemento1" value="1"/></div>';
                                }
                                else {
                                    if (id == row.Id) {
                                        return '<div class="text-center"><input type="checkbox" id="groupcheck" disabled="disabled" data-TrackingId="' + row.TrackingId + '" data-internoId="' + row.Id + '" data-TrabajoSocialId="' + row.TrabajoSocialId + '" name="elemento1" value="1"/></div>';
                                    }
                                    else {
                                        return '<div class="text-center"><input type="checkbox" id="groupcheck"  data-TrackingId="' + row.TrackingId + '" data-internoId="' + row.Id + '" data-TrabajoSocialId="' + row.TrabajoSocialId + '" name="elemento1" value="1"/></div>';
                                    }
                                }
                            }
                        },
                        {
                            targets: 2,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            targets: 6,
                            "width": 80,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='wrapping'>" + row.Alias + "</div>";
                            }
                        },
                        {
                            targets: 8,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "original"
                                var transfer = "tranfer";
                                var reentry = "reentry";
                                var registrar = "";
                                var habilitar = "";
                                var egreso = "";
                                var reingreso = "";

                                var detenido = "";
                                if (row.Alias == "")
                                    detenido = row.Paterno + " " + row.Materno + " " + row.Nombre;
                                else
                                    detenido = row.Paterno + " " + row.Materno + " " + row.Nombre + " alías " + row.Alias;

                                registrar = '<a class="btn btn-primary btn-sm ' + edit + '" data-detenido="' + detenido + '" data-habilitado="' + row.Activo + '" data-id="' + row.Id + '"  title="Original"><i class="fa fa-bookmark">&nbsp;</i>&nbsp;Original</a>&nbsp;';



                                return registrar;

                            }
                        }
                    ]

                });

            }

            window.table = $('#dt_basic').dataTable({
                "order": [[10, "desc"], [6, "desc"]],
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                        //$('td', row).eq(7).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "entrylist.aspx/getinterno",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        parametrosServerSide.anio = $("#idAnioDetenidos").val();
                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    }
                    ,
                    {
                        name: "Expediente",
                        data: "Expediente",

                    },
                    {
                        name: "NCP",
                        data: "NCP",
                        visible: false
                    },
                    {
                        name: "EstatusNombre",
                        data: "EstatusNombre"
                    },
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },
                    null
                    , null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                ],
                columnDefs: [
                    {
                        data: "Activo",
                        targets: 11,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "0";
                        }
                    },

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },

                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != "" && row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 10,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "edit"
                            var transfer = "tranfer";
                            var reentry = "reentry";
                            var registrar = "";
                            var habilitar = "";
                            var egreso = "";
                            var reingreso = "";
                            if (row.Activo) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {

                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled"; transfer = "disabled"; reentry = "disabled";

                            }
                            var agrupado = "";
                            if (row.AgrupadoId != "0") {
                                agrupado = "disabled";
                            }

                            var verInfoEgresado = ""
                            if(row.EstatusNombre == "Egreso" && txtestatus == "Habilitar") verInfoEgresado = '<a class="btn btn-default" href="entry.aspx?tracking=' + row.TrackingId + '&editable=0" title="Ver"><i class="fa fa-eye"></i>&nbsp;Ver</a>&nbsp;';


                            if ($("#ctl00_contenido_KAQWPK").val() == "true")
                                registrar = '<a class="btn btn-primary  ' + edit + '" href="entry.aspx?tracking=' + row.TrackingId + '&editable=1" title="Editar"><i class="glyphicon glyphicon-pencil"></i>&nbsp;Editar</a>&nbsp;';
                            else if ($("#ctl00_contenido_WERQEQ").val() == "true")
                                registrar = '<a class="btn btn-primary  ' + edit + '" href="entry.aspx?tracking=' + row.TrackingId + '&editable=1" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;Ver</a>&nbsp;';

                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + '  blockitem ' + agrupado + '" href="javascript:void(0);" data-id="' + row.TrackingIdEstatus + '" data-value = "' + row.Expediente + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i>&nbsp;' + txtestatus + '</a>&nbsp;';
                            // if ($("#ctl00_contenido_VYXMBM").val() == "true" & row.Estatus != 2 & $("#ctl00_contenido_KAQWPK").val() == "true") { egreso = '<a class="btn btn-danger btn-circle ' + transfer + '" href="transfer.aspx?tracking=' + row.TrackingId + '" title="Egreso"><i class="glyphicon glyphicon-transfer"></i></a>&nbsp;'; }
                            //  if ($("#ctl00_contenido_RAWMOV").val() == "true" & row.Estatus == 2 & $("#ctl00_contenido_KAQWPK").val() == "true") reingreso = '<a class="btn btn-warning btn-circle ' + reentry + '" href="reentry.aspx?tracking=' + row.TrackingId + '" title="Reingreso"><i class="glyphicon glyphicon-share-alt"></i></a>&nbsp;';
                            var imprimirPDFBoleta = '<a class="btn bg-color-green txt-color-white ' + agrupado + '" id="imprimirBoleta" data-id="' + row.Id + '" title="Boleta PDF"><i class="fa fa-file-pdf-o"></i> Boleta PDF</a>&nbsp;';

                            return  registrar + egreso + reingreso + habilitar + verInfoEgresado + imprimirPDFBoleta;
                        }
                    }
                ]

            });

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            $("#detenido").change(function () {
                var id = $("#detenido").val();

                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getbyid",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ _id: id }),
                    cache: false,
                    success: function (data) {
                        data = data.d;
                        if (data.Sexo != undefined & data.Sexo!="") {
                            $("#idsexo").text('Sexo: ' + data.Sexo);
                        }
                        else {
                            $("#idsexo").text('');
                        }
                        if (data.Edad != undefined & data.Edad!="") {
                            $("#idedad").text('Edad: ' + data.Edad);
                        }
                        else {
                            $("#idedad").text('');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de motivos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            });

            
            $("#btncancelevento").click(function () {
                limpiar_evento_reciente_modal();
                $("#evento-reciente-modal").modal("hide");
            });

            $("#btncanceleventoX").click(function () {
                limpiar_evento_reciente_modal();
                $("#evento-reciente-modal").modal("hide");
            });
            function limpiar_evento_reciente_modal(){
                $("#idsexo").text("");
                $("#idedad").text("");

                $("#detenido").val("0");
                $("#detenido").empty();
                $("#evento").val("0");
                $("#evento").empty();
                $("#eventoReciente").val("0");
                $("#eventoReciente").empty();
            }

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";

                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("#eventoReciente").change(function () {
                if ($("#eventoReciente").val() != 0) {
                    cargarEvento($("#eventoReciente").val());
                }
            });

            $("#evento").change(function () {
                cargarDetenido($("#evento").val());
            });

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");

                window.emptytable = true;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "entrylist.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                JSON.parse(data.d).mensaje + " </div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", JSON.parse(data.d).mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });

            init();

            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }
            }

            
            $('#evento-reciente-modal').on('hidden.bs.modal', function () {
                 //location.reload(true);
                window.emptytable = true;
                $('#dt_basic').DataTable().ajax.reload();
            });
            $("body").on("click", ".add", function () {
                limpiar();
                //$("#eventoReciente").val("");
                $("#evento").val("");
                $("#detenido").val("");
                $("#ctl00_contenido_fileUpload").val('').clone(true);
                //cargarEventoReciente(0);
                // $("#eventoReciente").val("");
                //$("#eventoReciente").change();
                // cargarEvento(0);
                $("#evento-reciente-modal").modal("show");
            });

            $("body").on("click", "#buscar", function () {
                if ($("#idnombre").val() == "" && $("#paterno").val() == "" && $("#materno").val() == "" && $("#alias").val() == "") {
                    ShowAlert("¡Aviso!", "Favor de ingresar nombre(s), apellido paterno, apellido materno o alias para buscar coincidencias.");
                    return;
                }
                CargarGirdGroup("");
            });

            $("body").on("click", "#agrupar", function () {
                $("#idnombre").val("");
                $("#paterno").val("");
                $("#materno").val("");
                $("#alias").val("");
                $("#iddetenido").text("");
                CargarGirdGroup("-1");
                responsiveHelper_dt_basicGroup = undefined;

                $("#Agrupar-modal").modal("show");
            });

            $("body").on("click", "#btnAgrupar", function () {
                var valor = "";
                var contador = 0;
                var trackingId = "";
                $("input[type=checkbox]:checked").each(function () {
                    trackingId
                    if ($(this).val() != "on" && $(this).val() != "null") {
                        contador += 1;
                    }
                });

                if (contador < 1) {
                    ShowAlert("¡Alerta!", "Seleccione  un registro");
                    return;
                }

                $("input[type=checkbox]:checked").each(function () {
                    valor = $(this).attr('data-internoid');
                    _datos.push(valor);
                });

                $("#Agrupar-modal").modal("hide");
                $("#verb2").text($("#iddetenido").text().replace("Original:", "").replace("null", ""));
                $("#Agrupado-modal").modal("show");
            });

            $("body").on("click", "#btncontinuaragrupado", function () {
                guardaAgrupado();
                $("#Agrupado-modal").modal("hide");
                 _datos=new Array();
            });

            function guardaAgrupado() {

                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/AgrupaDetenidos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: _datos, idoriginal: $("#originalid").val(),
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso && resultado.alerta == false) {
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Se agruparon los detenidos exitosamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Se agruparon los detenidos exitosamente.");
                            $('#main').waitMe('hide');

                            $("#evento-reciente-modal").modal("hide");

                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }
                        else if (resultado.exitoso && resultado.alerta == true) {
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-times'></i><strong>Atención! </strong>" +
                                resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowAlert("Atención!", resultado.mensaje);
                            $('#main').waitMe('hide');

                            $("#evento-reciente-modal").modal("hide");

                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                        $("#Agrupar-modal").modal("hide");
                    }
                });
            }

            $("body").on("click", ".original", function () {
                var detenido = "";
                var habilitado = habilitado = $(this).attr('data-habilitado');
                
                if (habilitado=="false")
                {
                    ShowAlert("¡Atención!","Seleccione a un detenido habilitado como original")
                    return;
                }

                detenido = $(this).attr('data-detenido');
                $("#iddetenido").text('Original:' + detenido);
                CargarGirdGroup($(this).attr('data-id'));
                $("#originalid").val($(this).attr('data-id'));
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    GuardarDetenido("");
                }
            });

            function validar() {
                var esvalido = true;

                if ($("#eventoReciente").val() == null || $("#eventoReciente").val() == 0) {
                    ShowError("Evento reciente", "El evento reciente es obligatorio.");
                    $('#eventoReciente').parent().removeClass('state-success').addClass("state-error");
                    $('#eventoReciente').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#eventoReciente').parent().removeClass("state-error").addClass('state-success');
                    $('#eventoReciente').addClass('valid');
                }

                if ($("#evento").val() == null || $("#evento").val() == 0) {
                    ShowError("Evento", "El evento es obligatorio.");
                    $('#evento').parent().removeClass('state-success').addClass("state-error");
                    $('#evento').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#evento').parent().removeClass("state-error").addClass('state-success');
                    $('#evento').addClass('valid');
                }

                if ($("#detenido").val() == null || $("#detenido").val() == 0) {
                    ShowError("Detenido", "El campo de detenido es obligatorio.");
                    $('#detenido').parent().removeClass('state-success').addClass("state-error");
                    $('#detenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#detenido').parent().removeClass("state-error").addClass('state-success');
                    $('#detenido').addClass('valid');
                }

              <%--  var file = document.getElementById('<% = fileUpload.ClientID %>').value;
                if (file != null && file != '') {

                    if (!validaImagen(file)) {
                        ShowError("Logo", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        esvalido = false;
                    }
                }--%>
                //else {
                //    ShowError("Fotografía", "La fotografía es obligatoria.");
                //    $("#ctl00_contenido_fileUpload").parent().removeClass('state-success').addClass("state-error");
                //    $("#ctl00_contenido_fileUpload").removeClass('valid');

                //    esvalido = false;
                //}

                return esvalido;
            }

            function guardar() {
                var files = $("#ctl00_contenido_fileUpload").get(0).files;

                var nombreAvatarAnterior = $("#ctl00_contenido_avatarOriginal").val();

                var nombreAvatar = "";

                if (files.length > 0) {

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "../Handlers/FileUploadHandler.ashx?action=2&before=" + nombreAvatarAnterior,
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (Results) {
                            if (Results.exitoso) {
                                nombreAvatar = Results.nombreArchivo;
                                GuardarDetenido(nombreAvatar);
                            }
                            else {
                                ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                            }
                        },
                        error: function (err) {
                            ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else {
                    GuardarDetenido(nombreAvatar);
                }
            }

            function GuardarDetenido(rutaAvatar) {
                startLoading();

                var datos = ObtenerValores("");

                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso && !resultado.alerta ) {
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del detenido se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del detenido se " + resultado.mensaje + " correctamente.");
                            $('#main').waitMe('hide');

                            limpiar_evento_reciente_modal();
                            

                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();

                            //location.href = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Registry/entry.aspx?tracking="+resultado.id;
                            //location.href = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/entrylist.aspx?tracking=" + resultado.id + "&editable=1";
                            location.href = "entry.aspx?tracking=" + resultado.id;
                            $("#evento-reciente-modal").modal("hide");
                        }
                        else if (resultado.exitoso && resultado.alerta) {
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-times'></i><strong>Atención! </strong>" +
                                resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowAlert("Atención!", resultado.mensaje);
                            $('#main').waitMe('hide');

                            limpiar_evento_reciente_modal();
                            $("#evento-reciente-modal").modal("hide");

                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            function ObtieneValoresFiltro() {
                var Filtro = {
                    Nombre: $("#idnombre").val(),
                    Paterno: $("#paterno").val(),
                    Materno: $("#materno").val(),
                    Alias: $("#alias").val()

                }
                return Filtro;
            }
            function ObtenerValores(rutaAvatar) {
                var datos = [
                    eventoId = $('#evento').val(),
                    detenidoId = $('#detenido').val(),
                    Avatar = rutaAvatar
                ];

                return datos;
            }

            function limpiar() {
                cargarEventoReciente(0);
                $('#eventoReciente').parent().removeClass('state-success');
                $('#eventoReciente').parent().removeClass("state-error");
                $('#evento').parent().removeClass("state-success");
                $('#evento').parent().removeClass("state-error");
                $('#detenido').parent().removeClass("state-success");
                $('#detenido').parent().removeClass("state-error");
                $('#ctl00_contenido_fileUpload').parent().removeClass('state-success');
                $('#ctl00_contenido_fileUpload').parent().removeClass("state-error");
            }

            function cargarEventoReciente(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getEventoReciente",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#eventoReciente');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Evento Reciente]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                        Dropdown.trigger("change");
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de eventos recientes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function cargarEvento(id) {
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getEventos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $('#evento');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Evento]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc.substring(0, 85) + "...", item.Id));
                        });

                        Dropdown.val("0");
                        Dropdown.trigger("change");
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de eventos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function cargarDetenido(id) {
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getDetenidos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $('#detenido');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Detenido]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        Dropdown.val("0");
                        Dropdown.trigger("change");
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }
                }
                return false;
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

            $("body").on("click", "#imprimirBoleta", function () {
                var id = $(this).attr("data-id");
                datos = id;

                pdfBoleta(id);
            });

            function pdfBoleta(InternoId) {
                datos = [id = InternoId];
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/pdfBoleta",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            open(resultado.ubicacionarchivo.replace("~", ""));

                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", resultado.mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el informe de detenidos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            //Events
            var responsiveHelper_dt_basic_events = undefined;

            $("body").on("click", ".events", function () {                
                loadYears(new Date().getFullYear().toString());
                startLoading();
                responsiveHelper_dt_basic_events = undefined;
                $("#dt_basic_events").DataTable().destroy();
                var val = $("#anioSelect").val();
                loadTableEventos2(val);
                //loadEventsTable(val);
                $("#events-list-modal").modal("show");
            });            

            $("#anioSelect").change(function () {
                setTimeout(function () {
                    startLoading();
                }, 100);
                var val = $("#anioSelect").val();                
                responsiveHelper_dt_basic_events = undefined;
                $("#dt_basic_events").DataTable().destroy();
                loadTableEventos2(val);
                //loadEventsTable(val);                
            });

            function loadYears(val) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "entrylist.aspx/getYears",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $("#anioSelect");
                        Dropdown.append(new Option("", ""));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (val !== "") {
                            Dropdown.val(val).trigger('change.select2');
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de años. Si el problema persiste, consulta al personal de soporte técnico.");
                    }
                });
            }            
            function loadTableEventos2(val) {
                $('#dt_basic_events').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    destroy: true,
                    autoWidth: true,
                    serverSide: true,
                    //"scrollY":"250px",
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_events) {
                            responsiveHelper_dt_basic_events = new ResponsiveDatatablesHelper($('#dt_basic_events'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_events.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_events.respond();
                        $('#dt_basic_events').waitMe('hide');
                    },
                    initComplete: function (settings, json) {
                        endLoading();
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                            $('td', row).eq(4).addClass('strikeout');
                            $('td', row).eq(5).addClass('strikeout');
                            $('td', row).eq(6).addClass('strikeout');
                            $('td', row).eq(7).addClass('strikeout');
                            $('td', row).eq(8).addClass('strikeout');
                            $('td', row).eq(9).addClass('strikeout');
                        }
                    },
                    ajax: {
                        type: "POST",
                        url: "entrylist.aspx/getLlamadas",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_events').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.year = val || 0;
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            name: "Id",
                            data: "Id",
                        },
                        {
                            name: "DescripcionLlamada",
                            data: "DescripcionLlamada"
                        },
                        {
                            name: "Descripcion",
                            data: "Descripcion"
                        },
                        {
                            name: "Folio",
                            data: "Folio"
                        },
                        {
                            name: "HoraYFecha",
                            data: "HoraYFecha"
                        },
                        {
                            name: "Asentamiento",
                            data: "Asentamiento"
                        },
                        {
                            name: "CodigoPostal",
                            data: "CodigoPostal"
                        },
                        {
                            name: "Municipio",
                            data: "Municipio"
                        },
                        {
                            name: "Estado",
                            data: "Estado"
                        },
                        {
                            name: "LugarDetencion",
                            data: "LugarDetencion"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            //width: 75,
                            targets: 10,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "edit";
                                var editar = '<a class="btn btn-primary btn-circle event-form ' + edit + '" data-id="' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                var habilitar = "";
                                if (row.Habilitado) {
                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                    edit = "edit";
                                }
                                else {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                    edit = "disabled";
                                }

                                if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle event-form ' + edit + '" data-id="' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem-events" href="javascript:void(0);" data-value="' + row.Descripcion + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                                return editar + habilitar;
                            }
                        }
                    ]
                });
            }
            function loadEventsTable(val) {
                $('#dt_basic_events').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    destroy: true,
                    autoWidth: true,                    
                    serverSide: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_events) {
                            responsiveHelper_dt_basic_events = new ResponsiveDatatablesHelper($('#dt_basic_events'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_events.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_events.respond();
                        $('#dt_basic_events').waitMe('hide');
                    },
                    initComplete: function (settings, json) {
                        endLoading();
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                            $('td', row).eq(4).addClass('strikeout');
                            $('td', row).eq(5).addClass('strikeout');
                            $('td', row).eq(6).addClass('strikeout');
                            $('td', row).eq(7).addClass('strikeout');
                            $('td', row).eq(8).addClass('strikeout');
                            $('td', row).eq(9).addClass('strikeout');
                        }
                    },
                    ajax: {
                        url: "entrylist.aspx/getEvents",
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: function (d) {
                            d.pages = $('#dt_basic').DataTable().page.info().page || "";
                            d.year = val || 0;
                            return JSON.stringify(d);
                        },
                        dataSrc: "data",
                        dataFilter: function (data) {
                            var json = jQuery.parseJSON(data);
                            json.recordsTotal = json.d.recordsTotal;
                            json.recordsFiltered = json.d.recordsFiltered;
                            json.data = json.d.data;
                            return JSON.stringify(json);
                        }
                    },
                    columns: [
                        {
                            name: "Id",
                            data: "Id",
                        },
                        {
                            name: "DescripcionLlamada",
                            data: "DescripcionLlamada"
                        },
                        {
                            name: "Descripcion",
                            data: "Descripcion"
                        },
                        {
                            name: "Folio",
                            data: "Folio"
                        },
                        {
                            name: "HoraYFecha",
                            data: "HoraYFecha"
                        },
                        {
                            name: "Asentamiento",
                            data: "Asentamiento"
                        },
                        {
                            name: "CodigoPostal",
                            data: "CodigoPostal"
                        },
                        {
                            name: "Municipio",
                            data: "Municipio"
                        },
                        {
                            name: "Estado",
                            data: "Estado"
                        },
                        {
                            name: "LugarDetencion",
                            data: "LugarDetencion"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            //width: 75,
                            targets: 10,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "edit";
                                var editar = '<a class="btn btn-primary btn-circle event-form ' + edit + '" data-id="' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                var habilitar = "";
                                if (row.Habilitado) {
                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                    edit = "edit";
                                }
                                else {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                    edit = "disabled";
                                }

                                if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle event-form ' + edit + '" data-id="' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem-events" href="javascript:void(0);" data-value="' + row.Descripcion + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                                return editar + habilitar;
                            }
                        }
                    ]
                });
            }

            //var tablaEventos = 

            $("body").on("click", ".blockitem-events", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock-events-list").text(itemnameblock);
                $("#verb-events").text(verb);
                $("#btncontinuar-events-list").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-events-list-modal").modal("show");
            })

            $("#btncontinuar-events-list").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "entrylist.aspx/blockitemEventsList",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            var val = $("#anioSelect").val();
                            responsiveHelper_dt_basic_events = undefined;
                            $("#dt_basic_events").DataTable().destroy();
                            loadEventsTable(val);
                            $("#blockitem-events-list-modal").modal("toggle");
                            ShowSuccess("¡Bien hecho!", data.mensaje);
                        }
                        else {
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }
                });
                tablaEventos.api().ajax.reload();
                $(".modal").css("overflow-y", "auto");
            });

            $("body").on("click", ".event-form", function () {
                var id = $(this).attr("data-id");
                $("#trackingEvent").val(id)

                //initEventForm();

                loadCalls("0");
                loadInstitucionesAW("0");
                loadUnidadesAW("0", "");
                if ($("#trackingEvent").val() !== undefined && $("#trackingEvent").val() !== "") {
                    obtenerEvento($("#trackingEvent").val());
                    $('#add-events-form').show();
                    $('#add-events-form').attr("data-tracking", $("#trackingEvent").val());
                    cargarPaisEstadoMunicipio(false);
                }
                else {
                    loadMotivos("0");
                    cargarPaisEstadoMunicipio(true);
                    loadFolio();
                    CargarMapaAgregar();                    
                }

                if (responsiveHelper_dt_basic_tabla_detenidos !== undefined) {
                    $("#dt_basic_tabla_detenidos").DataTable().ajax.reload();
                }
                else {
                    responsiveHelper_dt_basic_tabla_detenidos = undefined;
                    $("#dt_basic_tabla_detenidos").DataTable().destroy();
                    LoadDetenidos();
                }

                if (responsiveHelper_dt_basic_tabla_unidades !== undefined) {
                    $("#dt_basic_tabla_unidades").DataTable().ajax.reload();
                }
                else {
                    responsiveHelper_dt_basic_tabla_unidades = undefined;
                    $("#dt_basic_tabla_unidades").DataTable().destroy();
                    LoadUnidades();
                }

                $("#autorizaciondatetimepicker").val(new Date());                
                $("#events-form-modal").modal("show");
            });

            $("#events-form-modal-close").click(function () {
                limpiar_events_form();
                $("#events-form-modal").modal("toggle");
                $(".modal").css("overflow-y", "auto");

                $("#dt_basic_events").DataTable().ajax.reload();
            });

            $("#cancel_").click(function () {
                limpiar_events_form();
                $("#events-form-modal").modal("toggle");
                $(".modal").css("overflow-y", "auto");

                $("#dt_basic_events").DataTable().ajax.reload();
            });

            $("#btn-events-form-modal-close").click(function () {
                limpiar_events_form();
                $("#events-form-modal").modal("toggle");
                $(".modal").css("overflow-y", "auto");

                $("#dt_basic_events").DataTable().ajax.reload();
            });

            $("#events-form-detenido-modal-close").click(function () {
                $("#modalDetenido").modal("toggle");
                $(".modal").css("overflow-y", "auto");
                 eliminaFilas();
            });

            $("#events-form-unidades-modal-close").click(function () {
                $("#modalUnidad").modal("toggle");
                $(".modal").css("overflow-y", "auto");
            });

            function limpiar_events_form() {
                $("#codigoPostal").val("");
                $("#descripcion").val("");
                $("#lugar").val("");
                $("#numeroDetenidos").val("");
                $("#municipio").val("0");
                $("#colonia").val("0");

                $('#codigoPostal').parent().removeClass("state-success");
                $('#descripcion').parent().removeClass("state-success");
                $('#lugar').parent().removeClass("state-success");
                $('#numeroDetenidos').parent().removeClass("state-success");
                $('#municipio').parent().removeClass("state-success");
                $('#colonia').parent().removeClass("state-success");
                $('#fecha').parent().removeClass("state-success");
                $('#folio').parent().removeClass("state-success");

                $('#codigoPostal').parent().removeClass("state-error");
                $('#descripcion').parent().removeClass("state-error");
                $('#lugar').parent().removeClass("state-error");
                $('#numeroDetenidos').parent().removeClass("state-error");
                $('#municipio').parent().removeClass("state-error");
                $('#colonia').parent().removeClass("state-error");
                $('#fecha').parent().removeClass("state-error");
                $('#folio').parent().removeClass("state-error");

                responsiveHelper_dt_basic_tabla_eventos = undefined;
                $("#dt_basic_tabla_eventos").DataTable().destroy();
                responsiveHelper_dt_basic_tabla_detenidos = undefined;
                $("#dt_basic_tabla_detenidos").DataTable().destroy();
                responsiveHelper_dt_basic_tabla_unidades = undefined;
                $("#dt_basic_tabla_unidades").DataTable().destroy();

                $("#trackingEvent").val("");

                $('#add-events-form').hide();
                $('#linkUnidad').hide();
                $('#linkDetenidos').hide();

                $("#latitud").val("");
                $("#longitud").val("");
                initMap();
            }

            $(document).on('keydown', 'input[pattern]', function (e) {
                var input = $(this);
                var oldVal = input.val();
                var regex = new RegExp(input.attr('pattern'), 'g');

                setTimeout(function () {
                    var newVal = input.val();
                    if (!regex.test(newVal)) {
                        input.val(oldVal);
                    }
                }, 0);
            });

            //function initEventForm() {
                $("#llamada").select2();
                $("#unidad").select2();
                $("#responsable").select2();

                $('#autorizaciondatetimepicker').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm:ss',
                    defaultDate: new Date(),
                    autoclose: true
                });

                //Se reemplazo por edad
                //$('#FehaNacimientodatetimepicker').datetimepicker({
                //    format: 'DD/MM/YYYY'
                //});

                $('#autorizaciondatetimepicker').data("DateTimePicker").hide();

                $('#autorizaciondatetimepicker').datetimepicker().on('dp.change', function (e) {
                    if (!e.oldDate || !e.date.isSame(e.oldDate, 'day')) {
                        $(this).data('DateTimePicker').hide();
                    }
                });

                $('.datetimepicker').datetimepicker().on('dp.change', function (e) {
                    if (!e.oldDate || !e.date.isSame(e.oldDate, 'day')) {
                        $(this).data('DateTimePicker').hide();
                    }
                });

                var responsiveHelper_dt_basic_tabla_unidades = undefined;
                var responsiveHelper_dt_basic_tabla_detenidos = undefined;
                var responsiveHelper_dt_basic_tabla_eventos = undefined;

                $("#dt_basic_tabla_unidades").DataTable().destroy();
                $("#dt_basic_tabla_detenidos").DataTable().destroy();
                $("#dt_basic_tabla_eventos").DataTable().destroy();


                $("#dt_basic_tabla_detenidos").DataTable().destroy();
                function LoadDetenidos() {
                    $('#dt_basic_tabla_detenidos').dataTable({
                        "lengthMenu": [5, 20, 50, 100],
                        iDisplayLength: 5,
                        serverSide: true,
                        fixedColumns: true,
                        autoWidth: true,
                        destroy: true,
                        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                        "autoWidth": true,
                        "oLanguage": {
                            "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                        },
                        "preDrawCallback": function () {
                            if (!responsiveHelper_dt_basic_tabla_detenidos) {
                                responsiveHelper_dt_basic_tabla_detenidos = new ResponsiveDatatablesHelper($('#dt_basic_tabla_detenidos'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_dt_basic_tabla_detenidos.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_dt_basic_tabla_detenidos.respond();
                            $('#dt_basic_tabla_detenidos').waitMe('hide');
                        },
                        "createdRow": function (row, data, index) {

                        },
                        ajax: {
                            type: "POST",
                            url: "entrylist.aspx/getDetenidosTabla",
                            contentType: "application/json; charset=utf-8",
                            data: function (parametrosServerSide) {
                                $('#dt_basic_tabla_detenidos').waitMe({
                                    effect: 'bounce',
                                    text: 'Cargando...',
                                    bg: 'rgba(255,255,255,0.7)',
                                    color: '#000',
                                    sizeW: '',
                                    sizeH: '',
                                    source: ''
                                });

                                var trackingid;
                                if ($("#trackingEvent").val() !== undefined && $("#trackingEvent").val() !== "") trackingid = $("#trackingEvent").val();
                                else trackingid = "";

                                parametrosServerSide.emptytable = false;
                                parametrosServerSide.tracking = trackingid;
                                return JSON.stringify(parametrosServerSide);
                            }
                        },
                        columns: [
                            {
                                name: "Nombre",
                                data: "Nombre"
                            },
                            {
                                name: "Paterno",
                                data: "Paterno"
                            },
                            {
                                name: "Materno",
                                data: "Materno"
                            },
                            {
                                name: "Sexo",
                                data: "Sexo"
                            },
                            {
                                name: "NombreCompleto",
                                data: "NombreCompleto",
                                visible: false
                            }
                        ],
                        columnDefs: [
                            {
                            }
                        ]
                    });
                }

                $("#dt_basic_tabla_unidades").DataTable().destroy();
                function LoadUnidades() {
                    $('#dt_basic_tabla_unidades').dataTable({
                        "lengthMenu": [5, 20, 50, 100],
                        iDisplayLength: 5,
                        serverSide: true,
                        fixedColumns: true,
                        autoWidth: true,
                        destroy: true,
                        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                        "autoWidth": true,
                        "oLanguage": {
                            "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                        },
                        "preDrawCallback": function () {
                            if (!responsiveHelper_dt_basic_tabla_unidades) {
                                responsiveHelper_dt_basic_tabla_unidades = new ResponsiveDatatablesHelper($('#dt_basic_tabla_unidades'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_dt_basic_tabla_unidades.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_dt_basic_tabla_unidades.respond();
                            $('#dt_basic_tabla_unidades').waitMe('hide');
                        },
                        ajax: {
                            type: "POST",
                            url: "entrylist.aspx/getUnidadadesTabla",
                            contentType: "application/json; charset=utf-8",
                            data: function (parametrosServerSide) {
                                $('#dt_basic_tabla_unidades').waitMe({
                                    effect: 'bounce',
                                    text: 'Cargando...',
                                    bg: 'rgba(255,255,255,0.7)',
                                    color: '#000',
                                    sizeW: '',
                                    sizeH: '',
                                    source: ''
                                });

                                var trackingid;
                                if ($("#trackingEvent").val() !== undefined && $("#trackingEvent").val() !== "") trackingid = $("#trackingEvent").val();
                                else trackingid = "";

                                parametrosServerSide.emptytable = false;
                                parametrosServerSide.tracking = trackingid;
                                return JSON.stringify(parametrosServerSide);
                            }
                        },
                        columns: [
                            {
                                name: "Unidad",
                                data: "Unidad"
                            },
                            {
                                name: "ClaveResponsable",
                                data: "ClaveResponsable"
                            },
                            {
                                name: "Corporacion",
                                data: "Corporacion"
                            },
                        ],
                        columnDefs: [

                        ]
                    });
                }

                $("#municipio").change(function () {
                    loadNeighborhood("0", $("#municipio").val());
                });

                $("#colonia").change(function () {
                    loadZipCode($("#colonia").val());
                });

                $("#detenido-events-form-evento-reciente").change(function () {
                    var id = $("#detenido-events-form-evento-reciente").val();

                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getbyid",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ _id: id }),
                        cache: false,
                        success: function (data) {
                            data = data.d;
                            if (data.Sexo != undefined) {
                                $("#idsexo-events-form").text('Sexo: ' + data.Sexo);
                            }
                            else {
                                $("#idsexo-events-form").text('');
                            }
                            if (data.Edad != undefined) {
                                $("#idedad-events-form").text('Edad: ' + data.Edad);
                            }
                            else {
                                $("#idedad-events-form").text('');
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "No fue posible cargar la lista de detenidos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                });

                $("#llamada").change(function () {
                    if ($(this).val() == "0") {
                        loadCities("0", $("#estado").val());
                        loadNeighborhood("0", "0");
                        $("#codigoPostal").val("");
                        $("#descripcion").val("");
                        $("#lugar").val("");
                    }
                    else {
                        $.ajax({
                            type: "POST",
                            url: "entrylist.aspx/getCall",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            processdata: true,
                            traditional: true,
                            data: JSON.stringify({ LlamadaId: $("#llamada").val() }),
                            cache: false,
                            success: function (response) {
                                response = JSON.parse(response.d);

                                if (response.exitoso) {
                                    console.log("dentro ajax " + response.obj.IdMunicipio);
                                    loadCities(response.obj.IdMunicipio, response.obj.IdEstado);
                                    loadNeighborhood(response.obj.ColoniaId, response.obj.IdMunicipio);
                                    $("#codigoPostal").val(response.obj.CodigoPostal);
                                    $("#descripcion").val(response.obj.Descripcion);
                                    $("#lugar").val(response.obj.Lugar);
                                } else {
                                    ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                                }
                            },
                            error: function () {
                                ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                            }
                        });
                    }

                });

                $("#alertaWebInstitucion").change(function () {
                    loadUnidadesAW("0", $("#alertaWebInstitucion").val());
                    loadEventosAW("0", 0);
                }); 

                $("#alertaWebUnidad").change(function () {
                    loadEventosAW($("#alertaWebUnidad").val(), 0);
                });

                $("body").on("click", "#add-events-form", function () {
                    var tracking = $(this).attr("data-tracking");

                    cargarEvento_events_form(tracking);
                     
                    cargarDetenido_events_form(tracking);

                    $("#events-form-evento-reciente-modal").modal("show");
                });

                function loadFolio() {
                    $.ajax({                        
                        type: "POST",
                        url: "entrylist.aspx/getFolio",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            response = JSON.parse(response.d);

                            if (response.exitoso) {                                
                                $("#folio").val(response.folio);
                            } else {
                                ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                function loadCities(setvalue, idEstado) {

                    $.ajax({                        
                        type: "POST",
                        url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getCities",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({
                            idEstado: idEstado
                        }),
                        cache: false,
                        success: function (response) {
                            var Dropdown = $('#municipio');
                            Dropdown.children().remove();
                            Dropdown.append(new Option("[Municipio]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            if (setvalue != "") {
                                Dropdown.val(setvalue);
                                Dropdown.trigger("change.select2");
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "No fue posible cargar la lista de municipios. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                function loadNeighborhood(set, idMunicipio) {
                    $.ajax({                        
                        type: "POST",
                        url: "entrylist.aspx/getNeighborhoods",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                            idMunicipio: idMunicipio
                        }),
                        success: function (response) {
                            var Dropdown = $("#colonia");
                            Dropdown.empty();
                            Dropdown.append(new Option("[Colonia]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            if (set != "") {
                                Dropdown.val(set);
                                Dropdown.trigger("change.select2");
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }

                function loadUnidad(set) {
                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getUnidades",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                        }),
                        success: function (response) {
                            var Dropdown = $("#unidad");
                            Dropdown.empty();
                            Dropdown.append(new Option("[Unidad]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            if (set != "") {
                                Dropdown.val(set);
                                Dropdown.trigger("change.select2");
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "No fue posible cargar la lista de unidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }

                function loadSexo(set) {
                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getSexo",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                        }),
                        success: function (response) {
                            var Dropdown = $("#sexoDetenido");
                            Dropdown.empty();
                            Dropdown.append(new Option("[Sexo]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            if (set != "") {
                                Dropdown.val(set);
                                Dropdown.trigger("change.select2");
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }

                function loadResponsable(set) {
                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getResponsables",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                        }),
                        success: function (response) {
                            var Dropdown = $("#responsable");
                            Dropdown.empty();
                            Dropdown.append(new Option("[Clave-responsable]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            if (set != "") {
                                Dropdown.val(set);
                                Dropdown.trigger("change.select2");
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }

                function loadZipCode(idColonia) {
                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getZipCode",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                            idColonia: idColonia
                        }),
                        success: function (response) {
                            var resultado = JSON.parse(response.d);
                            $("#codigoPostal").val(resultado.cp);
                        },
                        error: function () {
                            ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }

                function generarObjeto($items) {
                    var obj = {};
                    $items.each(function () {
                        var id = this.id;
                        obj[id] = $(this).val();
                    });
                    return obj;
                }

                function llenarSelect(idSelect, datos) {
                    var select = document.getElementById('' + idSelect);

                    for (var i = 0; i < datos.length; i++) {
                        var opt = document.createElement('option');
                        opt.innerHTML = datos[i].Nombre;
                        opt.value = datos[i].Id;
                        select.appendChild(opt);
                    }
                }

                function camposVacios(_items) {
                    var _tmpItems = [].slice.call(_items);
                    var esValido = true;
                    _tmpItems.map(function (item) {
                        var parent = item.parentNode;
                        var attName = item.getAttribute('name');
                        if (item.nodeName.toLowerCase() === 'select' && item.value === '0') {
                            ShowError('' + attName, 'El campo ' + attName + ' es obligatorio.');
                            parent.classList.remove('state-success');
                            parent.classList.add('state-error');
                            item.classList.remove('valid');
                            esValido = false;
                        } else if (item.value === '' || item.value === undefined || item.value.length === 0) {
                            ShowError('' + attName, 'El campo ' + attName + ' es obligatorio.');
                            parent.classList.remove('state-success');
                            parent.classList.add('state-error');
                            item.classList.remove('valid');
                            esValido = false;
                        } else {
                            parent.classList.add('state-success');
                            parent.classList.remove('state-error');
                            item.classList.add('valid');
                        }
                    });
                    return esValido;
                }

                function validarUnidad() {
                    var esvalido = true;
                    if ($("#unidad").val() == null || $("#unidad").val() == "0") {
                        ShowError("Unidad", "El campo unidad es obligatorio.");
                        $('#unidad').parent().removeClass('state-success').addClass("state-error");
                        $('#unidad').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#unidad').parent().removeClass("state-error").addClass('state-success');
                        $('#unidad').addClass('valid');
                    }

                    if ($("#responsable").val() == null || $("#responsable").val() == "0") {
                        ShowError("Clave-responsable", "El campo clave-responsable es obligatorio.");
                        $('#responsable').parent().removeClass('state-success').addClass("state-error");
                        $('#responsable').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#responsable').parent().removeClass("state-error").addClass('state-success');
                        $('#responsable').addClass('valid');
                    }

                    return esvalido;
            }
            $("#nombreDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#paternoDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#maternoDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

                function validarResponsable() {
                    var esvalido = true;
                    if ($("#nombreDetenido").val().split(" ").join("") == "") {
                        ShowError("Nombre", "El campo nombre es obligatorio.");
                        $('#nombreDetenido').parent().removeClass('state-success').addClass("state-error");
                        $('#nombreDetenido').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#nombreDetenido').parent().removeClass("state-error").addClass('state-success');
                        $('#nombreDetenido').addClass('valid');
                    }

                    if ($("#paternoDetenido").val().split(" ").join("") == "") {
                        ShowError("Apellido paterno", "El campo apellido paterno es obligatorio.");
                        $('#paternoDetenido').parent().removeClass('state-success').addClass("state-error");
                        $('#paternoDetenido').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#paternoDetenido').parent().removeClass("state-error").addClass('state-success');
                        $('#paternoDetenido').addClass('valid');
                    }

                    if ($("#maternoDetenido").val().split(" ").join("") == "") {
                        ShowError("Apellido materno", "El campo apellido materno es obligatorio.");
                        $('#maternoDetenido').parent().removeClass('state-success').addClass("state-error");
                        $('#maternoDetenido').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#maternoDetenido').parent().removeClass("state-error").addClass('state-success');
                        $('#maternoDetenido').addClass('valid');
                    }

                    if ($("#sexoDetenido").val() == null || $("#sexoDetenido").val() == "0") {
                        ShowError("Sexo", "El campo sexo es obligatorio.");
                        $('#sexoDetenido').parent().removeClass('state-success').addClass("state-error");
                        $('#sexoDetenido').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#sexoDetenido').parent().removeClass("state-error").addClass('state-success');
                        $('#sexoDetenido').addClass('valid');
                    }


                    /*
                    if ($("#edad").val() == null || $("#edad").val() == "" || $("#edad").val() <= 0 || $("#edad").val() > 105) {
                        if ($("#edad").val() == null || $("#edad").val() == "") ShowError("Edad", "El campo edad es obligatorio.");
                        else if ($("#edad").val() <= 0) ShowError("Edad", "La edad mínima es de 1 año.");
                        else if ($("#edad").val() > 105) ShowError("Edad", "La edad máxima es de 105 años.");
                        $('#edad').parent().removeClass('state-success').addClass("state-error");
                        $('#edad').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#edad').parent().removeClass("state-error").addClass('state-success');
                        $('#edad').addClass('valid');
                    }*/

                    return esvalido;
                }

                function obtenerEvento(tkg) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processdata: true,
                        traditional: true,
                        url: "entrylist.aspx/getEventoByTrackingId",
                        data: JSON.stringify({ tracking: tkg }),
                        success: function (response) {
                            response = JSON.parse(response.d);
                            endLoading();

                            if (response.exitoso) {
                                loadCalls(response.obj.IdLlamada);
                                loadCities(response.obj.IdMunicipio, response.obj.IdEstado);
                                loadNeighborhood(response.obj.ColoniaId, response.obj.IdMunicipio);
                                loadMotivos(response.obj.MotivoId);
                                $("#codigoPostal").val(response.obj.CodigoPostal);
                                $("#descripcion").val(response.obj.Descripcion);
                                $("#lugar").val(response.obj.Lugar);
                                $("#fecha").val(response.obj.HoraYFecha);
                                $("#folio").val(response.obj.Folio);
                                $("#numeroDetenidos").val(response.obj.NumeroDetenidos);

                                $("#latitud").val(response.obj.Latitud);
                                $("#longitud").val(response.obj.Longitud);
                                if ($('#latitud').val() == "" || $('#longitud').val() == "") {

                                }
                                else {
                                    $("#L1").val(response.obj.Latitud);
                                    $("#L2").val(response.obj.Longitud);
                                }
                                CargarMapa();
                                $('#linkUnidad').show();
                                $('#linkDetenidos').show();
                                responsiveHelper_dt_basic_tabla_eventos = undefined;
                                $("#dt_basic_tabla_eventos").DataTable().destroy();
                                // loadEventosAW(response.obj.UnidadIdAW,response.obj.relacionado);

                                $("#btnGuardarUnidad").attr("data-EventoId", response.obj.Id);
                                $("#btnGuardarDetenido").attr("data-EventoId", response.obj.Id);
                            } else {
                                ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                            }
                        },
                        error: function (error) {
                            endLoading();
                        }
                    });
                }

                function loadCalls(setvalue) {
                    $.ajax({                        
                        type: "POST",
                        url: "entrylist.aspx/getCalls",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            var Dropdown = $('#llamada');
                            Dropdown.children().remove();
                            Dropdown.append(new Option("[Sin llamada]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            if (setvalue != "") {
                                Dropdown.val(setvalue);
                                Dropdown.trigger("change.select2");
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de llamadas. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                function loadUnidadesAW(setvalue, institucionId) {
                    $.ajax({                        
                        type: "POST",
                        url: "eventsform.aspx/getUnidadesAW",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ "institucionId": institucionId }),
                        cache: false,
                        success: function (response) {
                            var Dropdown = $('#alertaWebUnidad');
                            Dropdown.children().remove();
                            Dropdown.append(new Option("[Unidad]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            if (setvalue != "") {
                                Dropdown.val(setvalue);
                                Dropdown.trigger("change.select2");
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                function loadInstitucionesAW(setvalue) {
                    $.ajax({                        
                        type: "POST",
                        url: "entrylist.aspx/getInstitucionesAW",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            var Dropdown = $('#alertaWebInstitucion');
                            Dropdown.children().remove();
                            Dropdown.append(new Option("[Institucion]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            if (setvalue != "") {
                                Dropdown.val(setvalue);
                                Dropdown.trigger("change.select2");
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de instituciones. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                function loadEventosAW(idUnidadInstitucion, relacionado) {
                    responsiveHelper_dt_basic_tabla_eventos = undefined;
                    $("#dt_basic_tabla_eventos").DataTable().destroy();

                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getEventosAW",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({
                            idUnidadInstitucion: idUnidadInstitucion
                        }),
                        cache: false,
                        success: function (response) {
                            var r = JSON.parse(response.d);

                            if (r.latitud != '')
                                $("#latitud").val(r.latitud);

                            if (r.longitud != '')
                                $("#longitud").val(r.longitud);

                            llenarTabla(r.lista, relacionado);
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de eventos. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                function llenarTabla(lista, relacionado) {
                    $('#dt_basic_tabla_eventos').dataTable({
                        "lengthMenu": [10, 20, 50, 100],
                        iDisplayLength: 10,
                        serverSide: false,
                        fixedColumns: true,
                        fixedColumns: true,
                        autoWidth: true,
                        "scrollY": "100%",
                        "scrollX": "0%", // // // // // // // // // // // // // // // //// // // // // // // //// // // // // // // //// // // // // // // //// // // // // // // //
                        "scrollCollapse": true,
                        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                            "t" +
                            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                        "autoWidth": true,
                        "oLanguage": {
                            "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                        },
                        "preDrawCallback": function () {
                            if (!responsiveHelper_dt_basic_tabla_eventos) {
                                responsiveHelper_dt_basic_tabla_eventos = new ResponsiveDatatablesHelper($('#dt_basic_tabla_eventos'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_dt_basic_tabla_eventos.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_dt_basic_tabla_eventos.respond();
                            $('#dt_basic_tabla_eventos').waitMe('hide');
                        },
                        "createdRow": function (row, data, index) {
                        },
                        data: lista,
                        columnDefs: [
                            {
                                targets: 0,
                                name: "UnidadId",
                                orderable: false,
                                render: function (data, type, row, meta) {
                                    if ($("#trackingEvent").val() != undefined && $("#trackingEvent").val() !== "") {
                                        if (relacionado == 0)
                                            return '<a class="btn btn-info btn-xs relacionar" href="#" data-unidadId="' + row.UnidadId + '">Relacionar</a>';
                                        else
                                            return "";
                                    }
                                    else {
                                        return "";
                                    }
                                }
                            },
                            {
                                targets: 1,
                                data: "Folio",
                                render: function (data, type, row, meta) {
                                    return row.Folio;
                                }
                            },
                            {
                                targets: 2,
                                data: "Fecha",
                                render: function (data, type, row, meta) {
                                    return row.Fecha;
                                }
                            },
                            {
                                targets: 3,
                                data: "Motivo",
                                render: function (data, type, row, meta) {
                                    return row.Motivo;
                                }
                            },
                            {
                                targets: 4,
                                data: "NumeroDetenidos",
                                render: function (data, type, row, meta) {
                                    return row.NumeroDetenidos;
                                }
                            },
                            {
                                targets: 5,
                                data: "Detenidos",
                                render: function (data, type, row, meta) {
                                    return row.Detenidos;
                                }
                            },
                            {
                                targets: 6,
                                data: "Estado",
                                render: function (data, type, row, meta) {
                                    return "<div class='wrapping'>" + row.Estado + "</div>";
                                }
                            },
                            {
                                targets: 7,
                                data: "Municipio",
                                render: function (data, type, row, meta) {
                                    return row.Municipio;
                                }
                            },
                            {
                                targets: 8,
                                data: "Colonia",
                                render: function (data, type, row, meta) {
                                    return row.Colonia;
                                }
                            },
                            {
                                targets: 9,
                                data: "Numero",
                                render: function (data, type, row, meta) {
                                    return row.Numero;
                                }
                            },
                            {
                                targets: 10,
                                data: "EntreCalle",
                                render: function (data, type, row, meta) {
                                    return row.EntreCalle;
                                }
                            },
                            {
                                targets: 11,
                                data: "Responsable",
                                render: function (data, type, row, meta) {
                                    return "<div class='wrapping'>" + row.Responsable + "</div>";
                                }
                            },
                            {
                                targets: 12,
                                data: "Descripcion",
                                render: function (data, type, row, meta) {
                                    return "<div class='wrapping'>" + row.Descripcion + "</div>";
                                }
                            }
                        ]
                    });
                }

                function init2() {
                    
                }

                function cargarPaisEstadoMunicipio(cargarCombo) {
                    $.ajax({                        
                        type: "POST",
                        url: "entrylist.aspx/getPaisEstadoMunicipio",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            var r = JSON.parse(response.d);

                            if (r.exitoso) {
                                $("#estado").val(r.Estado);
                                $("#municipio").val(r.Municipio);
                                if (cargarCombo) {
                                    loadCities("1918", r.Estado);
                                    loadNeighborhood("0", "1918");
                                }

                                $("#localizacionHeader").text(r.Localizacion);
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                function cargarDetenido_events_form(id) {
                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getDetenidosEventoReciente",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                            id: id
                        }),
                        success: function (response) {
                            var Dropdown = $('#detenido-events-form-evento-reciente');
                            Dropdown.children().remove();
                            Dropdown.append(new Option("[Detenido]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });


                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                //function cargarEvento(tracking) {
                //    $.ajax({
                //        type: "POST",
                //        url: "entrylist.aspx/getEventoEventoReciente",
                //        contentType: "application/json; charset=utf-8",
                //        dataType: "json",
                //        cache: false,
                //        data: JSON.stringify({
                //            tracking: tracking
                //        }),
                //        success: function (response) {
                //            var Dropdown = $('#evento-events-form-evento-reciente');
                //            $.each(response.d, function (index, item) {
                //                Dropdown.append(new Option(item.Desc, item.Id));
                //            });
                //        },
                //        error: function () {
                //            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar el evento. Si el problema persiste contacte al soporte técnico del sistema.");
                //        }
                //    });
                //}

                function cargarEvento_events_form(tracking) {
                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getEventoEventoReciente",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                            tracking: tracking
                        }),
                        success: function (response) {
                            var Dropdown = $('#evento-events-form-evento-reciente');
                             Dropdown.children().remove();
                           // Dropdown.append(new Option("[Evento]", "0"));
                            var set;
                            $.each(response.d, function (index, item) {
                                set = item.Id;
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });
                             Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar el evento. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                function cargarDetenido_events_form(id) {
                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/getDetenidosEventoReciente",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                            id: id
                        }),
                        success: function (response) {
                            var Dropdown = $('#detenido-events-form-evento-reciente');
                            Dropdown.children().remove();
                            Dropdown.append(new Option("[Detenido]", "0"));
                            $.each(response.d, function (index, item) {
                                Dropdown.append(new Option(item.Desc, item.Id));
                            });

                            Dropdown.val("0");
                            Dropdown.trigger("change");
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                $("#events-form-evento-reciente-modal-close").click(function () {
                    limpiar_events_form_evento_reciente_modal();
                    $("#events-form-evento-reciente-modal").modal("toggle");
                    $(".modal").css("overflow-y", "auto");
                });

                $("#btn-events-form-evento-reciente-modal-close").click(function () {
                    limpiar_events_form_evento_reciente_modal();
                    $("#events-form-evento-reciente-modal").modal("toggle");
                    $(".modal").css("overflow-y", "auto");
                });

                function limpiar_events_form_evento_reciente_modal(){
                    $("#idsexo-events-form").text("");
                    $("#idedad-events-form").text("");

                    $("#detenido-events-form-evento-reciente").val("0");
                    $("#detenido-events-form-evento-reciente").empty();
                    $("#evento-events-form-evento-reciente").val("0");
                    $("#evento-events-form-evento-reciente").empty();

                    $("#detenido-events-form-evento-reciente").parent().removeClass("state-error").removeClass("state-success");
                    $("#evento-events-form-evento-reciente").parent().removeClass("state-error").removeClass("state-success");
                }

                function ObtenerValores_events_form() {
                    var datos = [
                        eventoId = $('#evento-events-form-evento-reciente').val(),
                        detenidoId = $('#detenido-events-form-evento-reciente').val()
                    ];

                    return datos;
                }

                function validar_events_form() {
                    var esvalido = true;

                    if ($("#detenido-events-form-evento-reciente").val() == null || $("#detenido-events-form-evento-reciente").val() == 0) {
                        ShowError("Detenido", "El campo de detenido es obligatorio.");
                        $('#detenido-events-form-evento-reciente').parent().removeClass('state-success').addClass("state-error");
                        $('#detenido-events-form-evento-reciente').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#detenido-events-form-evento-reciente').parent().removeClass("state-error").addClass('state-success');
                        $('#detenido-events-form-evento-reciente').addClass('valid');
                    }

                    return esvalido;
                }

                function GuardarDetenido_events_form_evento_reciente() {
                    startLoading();

                    var datos = ObtenerValores_events_form();

                    $.ajax({
                        type: "POST",
                        url: "entrylist.aspx/saveDetenidoBarandilla",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({
                            datos: datos,
                        }),
                        success: function (data) {
                            var resultado = JSON.parse(data.d);

                            if (resultado.exitoso && resultado.alerta == false) {
                                limpiar_events_form_evento_reciente();
                                ShowSuccess("¡Bien hecho!", "La información del detenido se " + resultado.mensaje + " correctamente.");
                                $('#main').waitMe('hide');

                                $('#dt_basic').DataTable().ajax.reload();
                                $("#events-form-evento-reciente-modal").modal("toggle");

                                location.href = "entry.aspx?tracking=" + resultado.id + "&editable=1";
                            }
                            else if (resultado.exitoso && resultado.alerta == true) {
                                limpiar_events_form_evento_reciente();
                                ShowAlert("Atención!", resultado.mensaje);
                                $('#main').waitMe('hide');

                                $('#dt_basic').DataTable().ajax.reload();

                                limpiar_events_form_evento_reciente_modal();
                                $("#events-form-evento-reciente-modal").modal("toggle");

                                //Modal event form
                                limpiar_events_form();
                                $("#events-form-modal").modal("toggle");
                                $(".modal").css("overflow-y", "auto");
                                $("#dt_basic_events").DataTable().ajax.reload();

                                //Modal event list
                                $("#events-list-modal").modal("toggle");
                            }
                            else {
                                $('#main').waitMe('hide');
                                ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                            }
                            $('#main').waitMe('hide');
                        }
                    });

                    $('#dt_basic').DataTable().ajax.reload();
                    $(".modal").css("overflow-y", "auto");
                }

                $("body").on("click", "#saveDetenido-events-form-evento-reciente", function () {
                    if (validar_events_form()) {
                        GuardarDetenido_events_form_evento_reciente();
                    }
                });

                $("#save_").on("click", function () {
                    startLoading();
                    var $items = $('[data-requerido]');

                    var items_validar = $('[data-requerido="true"]');

                    if (!camposVacios(items_validar)) {
                        endLoading();
                        return false;
                    }

                    var latitud;
                    var longitud;

                    var obj = generarObjeto($items);
                    obj.tracking = $("#trackingEvent").val() !== undefined && $("#trackingEvent").val() != "" ? $("#trackingEvent").val() : "";
                    obj.llamada = $("#llamada").val();
                    obj.unidadAW = $("#alertaWebUnidad").val();
                    obj.motivoevento = $("#motivoevento").val();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processdata: true,
                        traditional: true,
                        url: "entrylist.aspx/saveEvento",
                        data: JSON.stringify({ csObj: obj }),
                        success: function (response) {
                            endLoading();
                            var response = JSON.parse(response.d);
                            if (response.exitoso) {
                                $("#folio").val(response.folio);
                                ShowSuccess("¡Bien hecho!", "La información del evento se " + response.mensaje + " correctamente.");
                                $('#main').waitMe('hide');

                                $('#linkUnidad').show();
                                $('#linkDetenidos').show();
                                $('#add-events-form').show();

                                $("#btnGuardarUnidad").attr("data-EventoId", response.EventoId);
                                $("#btnGuardarUnidad").attr("data-tracking", response.tracking);

                                $("#btnGuardarDetenido").attr("data-EventoId", response.EventoId);
                                $("#btnGuardarDetenido").attr("data-tracking", response.tracking);

                                $('#add-events-form').attr("data-tracking", response.tracking);
                                $("#trackingEvent").val(response.tracking);

                                var val = $("#anioSelect").val();
                                responsiveHelper_dt_basic_events = undefined;
                                $("#dt_basic_events").DataTable().destroy();
                                loadEventsTable(val);

                                latitud = $("#latitud").val();
                                longitud = $("#longitud").val();

                                loadEventosAW($("#alertaWebUnidad").val());

                                $("#latitud").val(latitud);
                                $("#longitud").val(longitud);

                            } else {
                                ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico. " + response.mensaje);

                                if (response.fallo == 'fecha') {
                                    var fecha = document.getElementById("fecha");
                                    fecha.parentNode.classList.remove('state-success');
                                    fecha.parentNode.classList.add('state-error');
                                }
                            }
                        },
                        error: function (error) {
                            endLoading();
                        }
                    });
                });

                $("#btnGuardarUnidad").on("click", function () {
                    startLoading();
                    var $items = $('[data-requerido-unidad]');

                    if (!validarUnidad()) {
                        endLoading();
                        return false;
                    }

                    var obj = generarObjeto($items);
                    obj.EventoId = $(this).attr("data-EventoId");

                    if ($("#trackingEvent").val() == undefined || $("#trackingEvent").val() == "") {
                        $("#trackingEvent").val($(this).attr("data-tracking"));
                    }

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processdata: true,
                        traditional: true,
                        url: "entrylist.aspx/saveUnidad",
                        data: JSON.stringify({ csObj: obj }),
                        success: function (response) {
                            var response = JSON.parse(response.d);
                            if (response.exitoso) {
                                ShowSuccess("¡Bien hecho!", "La información de la unidad se " + response.mensaje + " correctamente.");
                                $('#main').waitMe('hide');

                                $("#modalUnidad").modal("toggle");

                                $("#dt_basic_tabla_unidades").DataTable().ajax.reload();
                            } else {
                                if (response.alerta) {
                                    $('#main').waitMe('hide');
                                    ShowAlert("¡Atención!", response.mensaje);
                                }

                                else {
                                    ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                                    $('#main').waitMe('hide');
                                }
                            }
                        },
                        error: function (error) {
                            endLoading();
                        }
                    });
                    $("#dt_basic_tabla_unidades").DataTable().ajax.reload();
                    $(".modal").css("overflow-y", "auto");
                });

                //$("#btnGuardarDetenido").on("click", function () {
                //    startLoading();
                //    var $items = $('[data-requerido-detenido]');

                //    if (!validarResponsable()) {
                //        endLoading();
                //        return false;
                //    }

                //    var obj = generarObjeto($items);
                //    obj.EventoId = $(this).attr("data-EventoId");
                //    obj.NumeroDetenidos = $("#numeroDetenidos").val();
                //    //obj.FechaNacimiento = $("#fechanacimiento").val();
                //    obj.Edad = $("#edad").val();
                //    if ($("#trackingEvent").val() == undefined || $("#trackingEvent").val() == "") {
                //        $("#trackingEvent").val($(this).attr("data-tracking"));
                //    }

                //    $.ajax({
                //        type: "POST",
                //        contentType: "application/json; charset=utf-8",
                //        dataType: "json",
                //        processdata: true,
                //        traditional: true,
                //        url: "entrylist.aspx/saveDetenido",
                //        data: JSON.stringify({ csObj: obj }),
                //        success: function (response) {
                //            endLoading();
                //            var response = JSON.parse(response.d);
                //            if (response.exitoso) {
                //                ShowSuccess("¡Bien hecho!", "La información del detenido se " + response.mensaje + " correctamente.");
                //                $('#main').waitMe('hide');

                //                $("#modalDetenido").modal("toggle");

                //                $("#dt_basic_tabla_detenidos").DataTable().ajax.reload();
                //            } else {
                //                //if (response.mensaje == " \n La fecha de nacimiento debe de ser mayor a un año") {
                //                //    ShowError("Fecha de nacimiento", response.mensaje);
                //                //}
                //                //else {
                //                    ShowError("¡Error!", response.mensaje + " Si el problema persiste contacte al personal de soporte técnico.");
                //                //}$("#dt_basic_tabla_detenidos").DataTable().ajax.reload();
                //            }
                //        },
                //        error: function (error) {
                //            endLoading();
                //        }
                //    });
                //    $("#dt_basic_tabla_detenidos").DataTable().ajax.reload();
                //    $(".modal").css("overflow-y", "auto");
                //});
            function obtenerDatosTabla(eventoId) {
                var dataArreglo = new Array();
                var DetenidoEvento;

                $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    DetenidoEvento = {};
                    DetenidoEvento.EventoId = eventoId;
                    DetenidoEvento.Nombre = this.node().childNodes[1].childNodes[0].value;
                    DetenidoEvento.Paterno = this.node().childNodes[2].childNodes[0].value;
                    DetenidoEvento.Materno = this.node().childNodes[3].childNodes[0].value;
                    DetenidoEvento.SexoId = this.node().childNodes[4].childNodes[0].value;
                   // DetenidoEvento.Edad = this.node().childNodes[5].childNodes[0].value;
                    DetenidoEvento.Motivo = this.node().childNodes[5].childNodes[0].value;

                    dataArreglo.push(DetenidoEvento);
                });

                return dataArreglo;
            }

            $("#btnGuardarDetenido").on("click", function () {
                startLoading();
                var $items = $('[data-requerido-detenido]');
                var hasSelected = false;

                var filas = 0;
                $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    filas++;
                });

                if (filas > 0) {
                    if (!validarCamposEnTabla()) {
                        endLoading();
                        return;
                    }
                }
                else {
                    endLoading();
                    return;
                }

                var list2 = obtenerDatosTabla($(this).attr("data-EventoId"));
                var obj = generarObjeto($items);
                obj.EventoId = $(this).attr("data-EventoId");
                obj.NumeroDetenidos = $("#numeroDetenidos").val();
                //obj.FechaNacimiento = $("#fechanacimiento").val();
                obj.Edad = $("#edad").val();
                //if (param == undefined) {
                //    param = $(this).attr("data-tracking");
                //}
                if ($("#trackingEvent").val() == undefined || $("#trackingEvent").val() == "") {
                    $("#trackingEvent").val($(this).attr("data-tracking"));
                }
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "entrylist.aspx/saveDetenido",
                    data: JSON.stringify({ csObj: obj, list: list2 }),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso && response.Alertadetenido == false) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se registro correctamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se registró satisfactoriamente.");
                            setTimeout(hideMessage, hideTime);
                            $("#dt_basic_tabla_detenidos").DataTable().ajax.reload();
                            $('#main').waitMe('hide');
                           
                            $("#modalDetenido").modal("toggle");
                            $(".modal").css("overflow-y", "auto");

                        }
                        else if (response.exitoso && response.Alertadetenido == true) {
                            limpiar();
                            ShowAlert("¡Atención!", response.Mensajealerta);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se registró satisfactoriamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se registró satisfactoriamente.");

                            setTimeout(hideMessage, hideTime);

                            $('#main').waitMe('hide');

                           $("#modalDetenido").modal("toggle");
                           $(".modal").css("overflow-y", "auto");

                            window.emptytable = true;
                            $("#dt_basic_tabla_detenidos").DataTable().ajax.reload();
                            $('#dt_basic').DataTable().ajax.reload();
                        }

                        else {
                            if (response.mensaje == " \n La fecha de nacimiento debe de ser mayor a un año") {
                                ShowError("Fecha de nacimiento", response.mensaje);
                            }
                            else {
                                ShowError("¡Error!", response.mensaje + " Si el problema persiste contacte al personal de soporte técnico.");
                            }
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
            });

                $("body").on("click", "#linkUnidad", function () {
                    limpiar();
                    loadUnidad("0");
                    loadResponsable("0");
                    $("#modal-unidad-title").html("<i class='fa fa-pencil'></i>Agregar unidad");
                    $("#modalUnidad").modal("show");
                });

                $("body").on("click", "#linkDetenidos", function () {
                    $("#nombreDetenido").val("");
                    $("#paternoDetenido").val("");
                    $("#maternoDetenido").val("");

                    $('#nombreDetenido').parent().removeClass('state-success');
                    $('#nombreDetenido').parent().removeClass("state-error");
                    $('#paternoDetenido').parent().removeClass('state-success');
                    $('#paternoDetenido').parent().removeClass("state-error");
                    $('#maternoDetenido').parent().removeClass('state-success');
                    $('#maternoDetenido').parent().removeClass("state-error");
                    $('#sexoDetenido').parent().removeClass('state-success');
                    $('#sexoDetenido').parent().removeClass("state-error");
                    //$('#fechanacimiento').parent().removeClass('state-success');
                    //$("#fechanacimiento").val("");
                    $('#edad').parent().removeClass('state-success');
                    $("#edad").val("");

                    loadSexo("0");
                    $("#modal-detenido-title").html("<i class='fa fa-pencil'></i> Agregar detenido");
                    $("#modalDetenido").modal("show");
                });

                $('#numeroDetenidos').on('input', function () {
                    this.value = this.value.replace(/[^0-9]/g, '');
                });

                $("body").on("click", ".relacionar", function () {
                    var obj = {};
                    console.log($("#alertaWebInstitucion").val());
                    obj.unidadId = $(this).attr("data-unidadId");
                    obj.institucionId = $("#alertaWebInstitucion").val();
                    obj.tracking = $("#trackingEvent").val() !== undefined  && $("#trackingEvent").val() !== "" ? $("#trackingEvent").val() : "";

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processdata: true,
                        traditional: true,
                        url: "entrylist.aspx/relacionarLlamadaAW",
                        data: JSON.stringify({ csObj: obj }),
                        success: function (response) {
                            endLoading();
                            var response = JSON.parse(response.d);
                            if (response.exitoso) {
                                ShowSuccess("¡Bien hecho!", "La información de alerta web se relacionó correctamente.");
                                $('#main').waitMe('hide');
                            } else {
                                ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                            }
                        },
                        error: function (error) {
                            endLoading();
                        }
                    });
                    $(".modal").css("overflow-y", "auto");
                });

                init2();

                function limpiar_events_form_evento_reciente() {
                    $('#unidad').parent().removeClass('state-success');
                    $('#unidad').parent().removeClass("state-error");
                    $('#responsable').parent().removeClass('state-success');
                    $('#responsable').parent().removeClass("state-error");
                }
            //}
        });

        function CargarMapa() {

            //if ($('#latitud').val() == "" || $('#longitud').val() == "") {
            mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
            var coordinates = document.getElementById('coordinates');
            function onDragEnd() {
                var lngLat = marker.getLngLat();
                coordinates.style.display = 'block';
                coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                let x = lngLat.lng;
                let y = x;
                x = x.toString();
                x = x.substring(0, x.length - 3);
                $('#longitud').val(x);

                x = lngLat.lat;
                let z = x;
                x = x.toString();
                x = x.substring(0, x.length - 3);
                $('#latitud').val(x);
            }
            var Latitud = -89.61086650942;
            var longitud = 20.97689912377;
            Latitud = $("#L1").val();
            longitud = $("#L2").val();

            var map = new mapboxgl.Map({
                container: 'mapid',
                style: 'mapbox://styles/mapbox/streets-v11',
                center: [longitud, Latitud],
                zoom: 16
            });
            var marker = new mapboxgl.Marker({
                draggable: true
            })
                .setLngLat([longitud, Latitud])
                .addTo(map);

            map.addControl(new mapboxgl.NavigationControl());
            marker.on('dragend', onDragEnd);

            //else {
            //    var mymap = L.map('mapid').setView([$('#latitud').val(), $('#longitud').val()], zoom = 16, 13, 16);

            //    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            //        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            //        maxZoom: 18,
            //        id: 'mapbox.streets',
            //        accessToken: 'pk.eyJ1IjoiYXNlc29ydXNpdGVjaCIsImEiOiJjanhtbzh6aW0wNXIwM2NvNjVweHlnd2JxIn0.YbBuq1IIm9cVDgg64NaxcQ'
            //    }).addTo(mymap);

            //    var marker = L.marker([$('#latitud').val(), $('#longitud').val()]).addTo(mymap);
            //}
        }

        function CargarMapaAgregar() {
            setTimeout(function () {
                if ($('#latitud').val() == "" || $('#longitud').val() == "") {
                    mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
                    var coordinates = document.getElementById('coordinates');
                    function onDragEnd() {
                        var lngLat = marker.getLngLat();
                        coordinates.style.display = 'block';
                        coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                        let x = lngLat.lng;
                        let y = x;
                        x = x.toString();
                        x = x.substring(0, x.length - 3);
                        $('#longitud').val(x);

                        x = lngLat.lat;
                        let z = x;
                        x = x.toString();
                        x = x.substring(0, x.length - 3);
                        $('#latitud').val(x);
                    }
                    var Latitud = -89.61086650942;
                    var longitud = 20.97689912377;
                    Latitud = $("#L1").val();
                    longitud = $("#L2").val();

                    $('#latitud').val(Latitud);
                    $('#longitud').val(longitud);

                    //alert($("#L1").val() + "  " + $("#L2").val());
                    var map = new mapboxgl.Map({
                        container: 'mapid',
                        style: 'mapbox://styles/mapbox/streets-v11',
                        center: [longitud, Latitud],
                        zoom: 16
                    });
                    var marker = new mapboxgl.Marker({
                        draggable: true
                    })

                        .setLngLat([longitud, Latitud])
                        .addTo(map);
                    map.addControl(new mapboxgl.NavigationControl());
                    marker.on('dragend', onDragEnd);
                }
                else {

                    //var mymap = L.map('mapid').setView([$('#latitud').val(), $('#longitud').val(), zoom = 16], 13);

                    //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                    //    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                    //    maxZoom: 18,
                    //    id: 'mapbox.streets',
                    //    accessToken: 'pk.eyJ1IjoiYXNlc29ydXNpdGVjaCIsImEiOiJjanhtbzh6aW0wNXIwM2NvNjVweHlnd2JxIn0.YbBuq1IIm9cVDgg64NaxcQ'
                    //}).addTo(mymap);


                    //var marker = L.marker([$('#latitud').val(), $('#longitud').val()]).addTo(mymap);
                }
            }, 2500);
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<%= ConfigurationManager.AppSettings["ApiKeyGoogle"]  %>&callback=initMap">
    </script>
</asp:Content>