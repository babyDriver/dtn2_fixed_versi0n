﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="anthropometry.aspx.cs" Inherits="Web.Application.Registry.anthropometry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Filiación</li>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">

    <style>
        .bootstrapWizard1 {
            display: block;
            list-style: none;
            padding: 0;
            position: relative;
            width: 100%;
        }

        .bootstrapWizard1 a:hover, .bootstrapWizard a:active, .bootstrapWizard a:focus {
            text-decoration: none;
        }

        .bootstrapWizard1 li {
            display: block;
            float: left;
            width: 9%;
            text-align: center;
            padding-left: 0;
        }

        .bootstrapWizard1 li:before {
            border-top: 3px solid #55606E;
            content: "";
            display: block;
            font-size: 0;
            overflow: hidden;
            position: relative;
            top: 11px;
            right: 1px;
            width: 100%;
            z-index: 1;
        }

        .bootstrapWizard1 li:first-child:before {
            left: 50%;
            max-width: 50%;
        }

        .bootstrapWizard1 li:last-child:before {
            max-width: 50%;
            width: 50%;
        }

        .bootstrapWizard1 li.complete .step {
            background: #0aa66e;
            padding: 1px 6px;
            border: 3px solid #55606E;
        }

        .bootstrapWizard1 li .step i {
            font-size: 10px;
            font-weight: 400;
            position: relative;
            top: -1.5px;
        }

        .bootstrapWizard1 li .step {
            background: #B2B5B9;
            color: #fff;
            display: inline;
            font-size: 15px;
            font-weight: 700;
            line-height: 12px;
            padding: 7px 13px;
            border: 3px solid transparent;
            border-radius: 50%;
            line-height: normal;
            position: relative;
            text-align: center;
            z-index: 2;
            transition: all .1s linear 0s;
        }

        .bootstrapWizard1 li.active .step, .bootstrapWizard li.active.complete .step {
            background: #0091d9;
            color: #fff;
            font-weight: 700;
            padding: 7px 13px;
            font-size: 15px;
            border-radius: 50%;
            border: 3px solid #55606E;
        }

        .bootstrapWizard1 li.complete .title, .bootstrapWizard li.active .title {
            color: #2B3D53;
        }

        .bootstrapWizard1 li .title {
            color: #bfbfbf;
            display: block;
            font-size: 13px;
            line-height: 15px;
            max-width: 100%;
            position: relative;
            table-layout: fixed;
            text-align: center;
            top: 20px;
            word-wrap: break-word;
            z-index: 104;
        }

        .wizard-actions {
            display: block;
            list-style: none;
            padding: 0;
            position: relative;
            width: 100%;
        }

        .wizard-actions li {
            display: inline;
        }

        .tab-content.transparent {
            background-color: transparent;
        }

        .img-thumbnail {
            height: 300px;
        }
        td.strikeout {
            text-decoration: line-through;
        }
        .scroll2 {
            height: 20vw;
            overflow: auto;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-list-alt"></i>
                Información personal
            </h1>
        </div>
    </div>-->
    <asp:Label ID="lblMessage" runat="server"></asp:Label>    

    <section id="widget-grid" class="">
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="row padding-bottom-10">
            <div class="menu-container" style="display: inline-block; float: right">
                <a style="display: none;font-size:smaller" class="btn btn-default btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
                <a style="display: none;font-size:smaller" class="btn btn-success txt-color-white btn-lg" id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a> 
                <a style="display: none;font-size:smaller" class="btn btn-info btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
                <a style="font-size:smaller" class="btn btn-primary txt-color-white btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
                <a style="display: none;font-size:smaller" class="btn btn-warning btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-purple txt-color-white btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-yellow txt-color-white btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-red txt-color-white btn-lg" id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
                <a style="margin-right:30px; font-size:smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa fa-paw"></i> Biométricos</a>
            </div>
        </div>
        <div class="scroll2" id="ScrollableContent">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-anthorpometry-2"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-language"></i></span>
                        <h2>Filiación</h2>
                        <a id="showFiliacion" href="javascript:void(0);" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>
                        <a id="hideFiliacion" href="javascript:void(0);" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Ver"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>
                    </header>
                    <div >
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="Filiacionoculta">
                            <div id="data-1" class="smart-form">
                                <fieldset >
                                    <legend class="hide">&nbsp;</legend>
                                    <div class="row">
                                        <section class="col col-3">
                                            <label>Estatura en metros <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                <input type="text" name="estatura"  id="estaturam" placeholder="Estatura (metros)" maxlength="6" class="decimal alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la estatura.</b>
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label>Peso en kilogramos <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                <input type="text" name="peso" id="peso" placeholder="Peso (kg)" maxlength="9" class="decimal_3" runat="server" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa el peso (kg).</b>
                                            </label>
                                        </section>                   
                                        <section class="col col-3" style="display:none">
                                            <label>Fórmula dactiloscopica <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                <input type="text" name="dactiloscopica" id="dactiloscopica" placeholder="Fórmula dactiloscopica" maxlength="250" class="alphanumeric alptext" runat="server" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la fórmula dactiloscópica </b>
                                            </label>
                                        </section>
                                        <section class="col col-9">
                                            <div class="inline-group">
                                                <section class="col col-12">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="general" id="lunar" />
                                                        <i></i>
                                                        Lunares
                                                    </label>
                                                    <label class="checkbox">
                                                        <input id="tatuaje" type="checkbox" name="general" />
                                                        <i></i>
                                                        Tatuajes
                                                    </label>
                                                    <label class="checkbox">
                                                        <input id="defecto" type="checkbox" name="general" />
                                                        <i></i>
                                                        Defectos
                                                    </label>
                                                    <label class="checkbox">
                                                        <input id="cicatris" type="checkbox" name="general" />
                                                        <i></i>
                                                        Cicatrices
                                                    </label>
                                                    <label class="checkbox">
                                                        <input id="anteojos" type="checkbox" name="general" />
                                                        <i></i>
                                                        Anteojos
                                                    </label>
                                                </section>
                                            </div>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <div class="row" style="display: inline-block; float: right">
                                        <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                        <%--<a id="showFiliacion" class="btn btn-default" title="Ver">&nbsp;Ver </a>--%> 
                                        <a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default" id="ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>
                                        <a style="float: none; display: none;" href="javascript:void(0);" class="btn btn-default saveGeneral" title="Guardar" id="addGeneral"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                        <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                    </div>
                                    <input type="hidden" id="datosgenerales"/>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>        
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" style="margin-bottom: 20px;" id="wid-anthorpometry-3" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-language"></i></span>
                        <h2>Filiación general </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body">
                            <!-- cinta--->
                            <div id="data-2" class="col-sm-12">
                                <div class="form-bootstrapWizard1">
                                    <ul id="lstTabs" class="bootstrapWizard1 form-wizard">
                                        <li class="active" data-target="#step1">
                                            <a href="#tab1" id="linkTab1" data-toggle="tab">
                                                <span class="step">1</span>
                                                <span class="title">General</span>
                                            </a>
                                        </li>
                                        <li data-target="#step2">
                                            <a href="#tab2" id="linkTab2" data-toggle="tab">
                                                <span class="step">2</span>
                                                <span class="title">Cabello </span>
                                            </a>
                                        </li>
                                        <li data-target="#step3">
                                            <a href="#tab3" id="linkTab3" data-toggle="tab">
                                                <span class="step">3</span>
                                                <span class="title">Frente </span>
                                            </a>
                                        </li>
                                        <li data-target="#step4">
                                            <a href="#tab4" id="linkTab4" data-toggle="tab">
                                                <span class="step">4</span>
                                                <span class="title">Cejas </span>
                                            </a>
                                        </li>
                                        <li data-target="#step5">
                                            <a href="#tab5" id="linkTab5" data-toggle="tab">
                                                <span class="step">5</span>
                                                <span class="title">Ojos </span>
                                            </a>
                                        </li>
                                        <li data-target="#step6">
                                            <a href="#tab6" id="linkTab6" data-toggle="tab">
                                                <span class="step">6</span>
                                                <span class="title">Nariz </span>
                                            </a>
                                        </li>
                                        <li data-target="#step7">
                                            <a href="#tab7" id="linkTab7" data-toggle="tab">
                                                <span class="step">7</span>
                                                <span class="title">Boca y labios </span>
                                            </a>
                                        </li>
                                        <li data-target="#step8">
                                            <a href="#tab8" id="linkTab8" data-toggle="tab">
                                                <span class="step">8</span>
                                                <span class="title">Mentón </span>
                                            </a>
                                        </li>
                                        <li data-target="#step9">
                                            <a href="#tab9" id="linkTab9" data-toggle="tab">
                                                <span class="step">9</span>
                                                <span class="title">Oreja derecha y hélix </span>
                                            </a>
                                        </li>
                                        <li data-target="#step10">
                                            <a href="#tab10" id="linkTab10" data-toggle="tab">
                                                <span class="step">10</span>
                                                <span class="title">Lóbulo </span>
                                            </a>
                                        </li>
                                        <li data-target="#step11">
                                            <a href="#tab11" id="linkTab11" data-toggle="tab">
                                                <span class="step">11</span>
                                                <span class="title">Factor RH </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--fin cinta-->
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <!--tab contenido -->
                                <div class="tab-content">
                                    <!--tab numero1 general -->
                                    <div class="tab-pane active" id="tab1">
                                        <div id="smart-form-register" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Complexión</label>
                                                    <section class="col col-4" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="radiocomplexion" id="delgada" value="1" />
                                                            <i></i>
                                                            Delgada
                                                        </label>
                                                        <label class="radio">
                                                            <input id="regular" type="radio" name="radiocomplexion" value="2" />
                                                            <i></i>
                                                            Regular
                                                        </label>
                                                        <label class="radio">
                                                            <input id="robusta" type="radio" name="radiocomplexion" value="3" />
                                                            <i></i>
                                                            Robusta
                                                        </label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="radio">
                                                            <input id="atletico" type="radio" name="radiocomplexion" value="4" />
                                                            <i></i>
                                                            Atlética
                                                        </label>
                                                        <label class="radio">
                                                            <input id="obesa" type="radio" name="radiocomplexion" value="5" />
                                                            <i></i>
                                                            Obesa
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindato" type="radio" name="radiocomplexion" value="6" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-5">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Color de piel</label>
                                                    <section class="col col-4" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="colorpiel" id="albino" value="1" />
                                                            <i></i>
                                                            Albino
                                                        </label>
                                                        <label class="radio">
                                                            <input id="blanco" type="radio" name="colorpiel" value="2" />
                                                            <i></i>
                                                            Blanco
                                                        </label>
                                                        <label class="radio">
                                                            <input id="amarillo" type="radio" name="colorpiel" value="3" />
                                                            <i></i>
                                                            Amarillo
                                                        </label>
                                                    </section>
                                                    <section class="col col-4" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input id="morenoc" type="radio" name="colorpiel" value="4" />
                                                            <i></i>
                                                            Moreno claro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="moreno" type="radio" name="colorpiel" value="5" />
                                                            <i></i>
                                                            Moreno
                                                        </label>
                                                        <label class="radio">
                                                            <input id="morenoo" type="radio" name="colorpiel" value="6" />
                                                            <i></i>
                                                            Moreno oscuro
                                                        </label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="radio">
                                                            <input id="negro" type="radio" name="colorpiel" value="7" />
                                                            <i></i>
                                                            Negro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="otrog" type="radio" name="colorpiel" value="8" />
                                                            <i></i>
                                                            Otro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatog" type="radio" name="colorpiel" value="9" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la segunda seccion -->
                                                <!--tercera seccion -->
                                                <section class="col col-4">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Cara</label>
                                                    <section class="col col-4" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="cara" id="alargada" value="1" />
                                                            <i></i>
                                                            Alargada
                                                        </label>
                                                        <label class="radio">
                                                            <input id="cuadrada" type="radio" name="cara" value="2" />
                                                            <i></i>
                                                            Cuadrada
                                                        </label>
                                                        <label class="radio">
                                                            <input id="ovalada" type="radio" name="cara" value="3" />
                                                            <i></i>
                                                            Ovalada
                                                        </label>
                                                    </section>
                                                    <section class="col col-4">
                                                        <label class="radio">
                                                            <input id="redonda" type="radio" name="cara" value="4" />
                                                            <i></i>
                                                            Redonda
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatogc" type="radio" name="cara" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab1-->
                                    <!--tab nuemro 2 cabello-->
                                    <div class="tab-pane" id="tab2">
                                        <div id="smart-from-register2" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Cantidad</label>
                                                    <section class="col col-4" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="cabellocantidad" id="abundantec" value="1" />
                                                            <i></i>
                                                            Abundante
                                                        </label>
                                                        <label class="radio">
                                                            <input id="escasoc" type="radio" name="cabellocantidad" value="2" />
                                                            <i></i>
                                                            Escaso
                                                        </label>
                                                        <label class="radio">
                                                            <input id="regulsarc" type="radio" name="cabellocantidad" value="3" />
                                                            <i></i>
                                                            Regular
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sincabelloc" type="radio" name="cabellocantidad" value="4" />
                                                            <i></i>
                                                            Sin&nbsp;cabello
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoc" type="radio" name="cabellocantidad" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin&nbsp;dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-4">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Color</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="piel" id="albinopiel" value="1" />
                                                            <i></i>
                                                            Albino
                                                        </label>
                                                        <label class="radio">
                                                            <input id="canopiel" type="radio" name="piel" value="2" />
                                                            <i></i>
                                                            Cano total
                                                        </label>
                                                        <label class="radio">
                                                            <input id="castanocpiel" type="radio" name="piel" value="3" />
                                                            <i></i>
                                                            Castaño claro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="castanoopiel" type="radio" name="piel" value="4" />
                                                            <i></i>
                                                            Castaño oscuro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="entrecanop" type="radio" name="piel" value="5" />
                                                            <i></i>
                                                            Entrecano
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input type="radio" name="piel" id="negropiel" value="6" />
                                                            <i></i>
                                                            Negro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="pelirrojopiel" type="radio" name="piel" value="7" />
                                                            <i></i>
                                                            Pelirrojo
                                                        </label>
                                                        <label class="radio">
                                                            <input id="rubiopiel" type="radio" name="piel" value="8" />
                                                            <i></i>
                                                            Rubio
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatopiel" type="radio" name="piel" value="9" checked="checked" />
                                                            <i></i>
                                                            Sin&nbsp;dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la sgunda seccion -->
                                                <!--tercera seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Forma</label>
                                                    <section class="col col-4" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="formacabello" id="crespoforma" value="1" />
                                                            <i></i>
                                                            Crespo
                                                        </label>
                                                        <label class="radio">
                                                            <input id="lacioforma" type="radio" name="formacabello" value="2" />
                                                            <i></i>
                                                            Lacio
                                                        </label>
                                                        <label class="radio">
                                                            <input id="onduladoforma" type="radio" name="formacabello" value="3" />
                                                            <i></i>
                                                            Ondulado
                                                        </label>
                                                        <label class="radio">
                                                            <input id="riazadoforma" type="radio" name="formacabello" value="4" />
                                                            <i></i>
                                                            Rizado
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoforma" type="radio" name="formacabello" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin&nbsp;dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                                <!--cuarta seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Calvicie</label>
                                                    <section class="col col-4" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="calvicie" id="frontalcalvicie" value="1" />
                                                            <i></i>
                                                            Frontal
                                                        </label>
                                                        <label class="radio">
                                                            <input id="tonsuralcalvicie" type="radio" name="calvicie" value="2" />
                                                            <i></i>
                                                            Tonsural
                                                        </label>
                                                        <label class="radio">
                                                            <input id="frontoparientalcalvicie" type="radio" name="calvicie" value="3" />
                                                            <i></i>
                                                            Frontopariental
                                                        </label>
                                                        <label class="radio">
                                                            <input id="totalcalvicie" type="radio" name="calvicie" value="4" />
                                                            <i></i>
                                                            Total
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatocalvicie" type="radio" name="calvicie" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin&nbsp;dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la cuarta seccion -->
                                                <!--quinta seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Implantación</label>
                                                    <section class="col col-4" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="implementacioncabello" id="circularcabello" value="1" />
                                                            <i></i>
                                                            Circular
                                                        </label>
                                                        <label class="radio">
                                                            <input id="rectangularcabello" type="radio" name="implementacioncabello" value="2" />
                                                            <i></i>
                                                            Rectangular
                                                        </label>
                                                        <label class="radio">
                                                            <input id="puntacabello" type="radio" name="implementacioncabello" value="3" />
                                                            <i></i>
                                                            En&nbsp;punta
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoancho" type="radio" name="implementacioncabello" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin&nbsp;dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la quinta seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab2-->
                                    <!--tab nuemro 3 frente-->
                                    <div class="tab-pane" id="tab3">
                                        <div id="smart-form-register3" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Altura <a style="color: red">*</a></label>
                                                        <select name="motivo" id="frentealtura" runat="server" class="select2" style="width:100%;">
                                                            <option>[Altura]</option>
                                                        </select>
                                                        <i></i>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Ancho <a style="color: red">*</a></label>
                                                        <select name="motivo" id="frenteancho" runat="server" class="select2" style="width:100%;">
                                                            <option>[Ancho]</option>
                                                        </select>
                                                        <i></i>
                                                </section>
                                                <!--fin de la sgunda seccion -->
                                                <!--tercera seccion -->
                                                <section class="col col-4">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Inclinación</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="frente" id="oblicuainclinacion" value="1" />
                                                            <i></i>
                                                            Oblicua
                                                        </label>
                                                        <label class="radio">
                                                            <input id="intermediainclinacion" type="radio" name="frente" value="2" />
                                                            <i></i>
                                                            Intermedia
                                                        </label>
                                                        <label class="radio">
                                                            <input id="verticalinclinacion" type="radio" name="frente" value="3" />
                                                            <i></i>
                                                            Vertical
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="prominenteinclinacion" type="radio" name="frente" value="4" />
                                                            <i></i>
                                                            Prominente
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoinclinacion" type="radio" name="frente" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab3-->
                                    <!--tab numero4 cejas -->
                                    <div class="tab-pane" id="tab4">
                                        <div id="smart-form-register4" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Dirección</label>
                                                    <section class="col col-5" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="direccion" id="internascejas" value="1" />
                                                            <i></i>
                                                            Internas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="horizontalcejas" type="radio" name="direccion" value="2" />
                                                            <i></i>
                                                            Horizontal
                                                        </label>
                                                        <label class="radio">
                                                            <input id="externas" type="radio" name="direccion" value="3" />
                                                            <i></i>
                                                            Externas
                                                        </label>
                                                    </section>
                                                    <section class="col col-5">
                                                        <label class="radio">
                                                            <input id="sindatodireccion" type="radio" name="direccion" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Implantación</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="implementacioncejas" id="altas" checked="checked"value="1" />
                                                            <i></i>
                                                            Altas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="bajas" type="radio" name="implementacioncejas" value="2" />
                                                            <i></i>
                                                            Bajas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="proximas" type="radio" name="implementacioncejas" value="3" />
                                                            <i></i>
                                                            Próximas
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="separadas" type="radio" name="implementacioncejas" value="4" />
                                                            <i></i>
                                                            Separadas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoimplementacion" type="radio" name="implementacioncejas" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la segunda seccion -->
                                                <!--tercera seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Forma</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="formacejas" id="arqueadas" value="1" />
                                                            <i></i>
                                                            Arqueadas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="arqueadasinuosa" type="radio" name="formacejas" value="2" />
                                                            <i></i>
                                                            Arqueadas sinuosas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="retilinea" type="radio" name="formacejas" value="3" />
                                                            <i></i>
                                                            Retilíneas
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="retilineasinuosa" type="radio" name="formacejas" value="4" />
                                                            <i></i>
                                                            Retilíneas sinuosas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoformaimplementacion" type="radio" name="formacejas" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                                <!--cuarta seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Tamaño</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="tamañocejas" id="guresascejas" value="1" />
                                                            <i></i>
                                                            Gruesas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="delgadascejas" type="radio" name="tamañocejas" value="2" />
                                                            <i></i>
                                                            Delgadas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="cortascejas" type="radio" name="tamañocejas" value="3" />
                                                            <i></i>
                                                            Cortas
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="largascejas" type="radio" name="tamañocejas" value="4" />
                                                            <i></i>
                                                            Largas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatotamaño" type="radio" name="tamañocejas" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la cuarta seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab4-->
                                    <!--tab numero5  ojos-->
                                    <div class="tab-pane" id="tab5">
                                        <div id="smart-form-register5" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-4">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Color</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="ojoscalor" id="azulojo" value="1" />
                                                            <i></i>
                                                            Azul
                                                        </label>
                                                        <label class="radio">
                                                            <input id="cafecojo" type="radio" name="ojoscalor" value="2" />
                                                            <i></i>
                                                            Café claro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="cafeoojo" type="radio" name="ojoscalor" value="3" />
                                                            <i></i>
                                                            Café obscuro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="grisojo" type="radio" name="ojoscalor" value="4" />
                                                            <i></i>
                                                            Gris
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input type="radio" name="ojoscalor" id="verdeojo" value="5" />
                                                            <i></i>
                                                            Verde
                                                        </label>
                                                        <label class="radio">
                                                            <input id="otroojo" type="radio" name="ojoscalor" value="6" />
                                                            <i></i>
                                                            Otro
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatojo" type="radio" name="ojoscalor" value="7" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Forma</label>
                                                    <section class="col col-10" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="implementacion" id="alargadoojo" value="1" />
                                                            <i></i>
                                                            Alargados
                                                        </label>
                                                        <label class="radio">
                                                            <input id="redondoojo" type="radio" name="implementacion" value="2" />
                                                            <i></i>
                                                            Redondos
                                                        </label>
                                                        <label class="radio">
                                                            <input id="ovaleojo" type="radio" name="implementacion" value="3" />
                                                            <i></i>
                                                            Ovales
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoojo" type="radio" name="implementacion" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la segunda seccion -->
                                                <!--tercera seccion -->
                                                <section class="col col-3">
                                                    <section class="col col-10">
                                                        <label class="label" style="color: #006ead; font-weight: bold">Tamaño <a style="color: red">*</a></label>
                                                            <select name="motivo" id="ojoastamaño" runat="server" class="select2" style="width:100%;">
                                                                <option>[Tamaño]</option>
                                                            </select>
                                                            <i></i>
                                                    </section>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab5-->
                                    <!--tab nuemro 6 naraiz-->
                                    <div class="tab-pane" id="tab6">
                                        <div id="smart-form-register6" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Raíz (prof) <a style="color: red">*</a></label>
                                                        <select name="motivo" id="narizraiz" runat="server" class="select2" style="width:100%;">
                                                            <option>[Raíz (prof)]</option>
                                                        </select>
                                                        <i></i>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Altura <a style="color: red">*</a></label>
                                                        <select name="motivo" id="narizaltura" runat="server" class="select2" style="width:100%;">
                                                            <option>[Altura]</option>
                                                        </select>
                                                        <i></i>
                                                </section>
                                                <!--fin de la sgunda seccion -->
                                                <!--tercera seccion seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Ancho <a style="color: red">*</a></label>
                                                        <select name="motivo" id="narizancho" runat="server" class="select2" style="width:100%;">
                                                            <option>[Ancho]</option>
                                                        </select>
                                                        <i></i>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                                <!--cuarta seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Dorso</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="narizdorso" id="concovo" value="1" />
                                                            <i></i>
                                                            Cóncavo
                                                        </label>
                                                        <label class="radio">
                                                            <input id="convexo" type="radio" name="narizdorso" value="2" />
                                                            <i></i>
                                                            Convexo
                                                        </label>
                                                        <label class="radio">
                                                            <input id="recto" type="radio" name="narizdorso" value="3" />
                                                            <i></i>
                                                            Recto
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="sinuoso" type="radio" name="narizdorso" value="4" />
                                                            <i></i>
                                                            Sinuoso
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatodorso" type="radio" name="narizdorso" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la cuarta seccion -->
                                                <!--quinta seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Base</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="narizbase" id="abatidabase" value="1" />
                                                            <i></i>
                                                            Abatida
                                                        </label>
                                                        <label class="radio">
                                                            <input id="horizontalbase" type="radio" name="narizbase" value="2" />
                                                            <i></i>
                                                            Horizontal
                                                        </label>
                                                        <label class="radio">
                                                            <input id="levantadabase" type="radio" name="narizbase" value="3" />
                                                            <i></i>
                                                            Levantada
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="sindatobase" type="radio" name="narizbase" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la quinta seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab6-->
                                    <!--tab nuemro 7 bocay labios-->
                                    <div class="tab-pane" id="tab7">
                                        <div id="smart-form-register7" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-5">
                                                    <h2><b>Boca</b></h2>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion  -->
                                                <section class="col col-1">
                                                </section>
                                                <!--fin de la segunda seccion -->
                                                <!--tercera  seccion -->
                                                <section class="col col-5">
                                                    <h2><b>Labios</b></h2>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                            </div>
                                            <div class="row">
                                                <!--cuarta seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Tamaño <a style="color: red">*</a></label>
                                                    <select name="motivo" id="bocatamaño" runat="server" class="select2" style="width:100%;">
                                                        <option>[Tamaño]</option>
                                                    </select>
                                                    <i></i>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Comisuras</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="boca" id="abatidasboca" value="1" />
                                                            <i></i>
                                                            Abatidas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="elevadasboca" type="radio" name="boca" value="2" />
                                                            <i></i>
                                                            Elevadas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="simetricasboca" type="radio" name="boca" value="3" />
                                                            <i></i>
                                                            Simétricas
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="asimetricasboca" type="radio" name="boca" value="4" />
                                                            <i></i>
                                                            Asimétricas
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoboca" type="radio" name="boca" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin&nbsp;dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la cuarta seccion -->
                                                <!--quinta seccion  -->
                                                <!--fin de la quinta seccion -->
                                                <!--sexta  seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Espesor</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="espesor" id="delgadoslabios" value="1" />
                                                            <i></i>
                                                            Delgados
                                                        </label>
                                                        <label class="radio">
                                                            <input id="medianoslabios" type="radio" name="espesor" value="2" />
                                                            <i></i>
                                                            Medianos
                                                        </label>
                                                        <label class="radio">
                                                            <input id="gruesoslabios" type="radio" name="espesor" value="3" />
                                                            <i></i>
                                                            Gruesos
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="morrudoslabios" type="radio" name="espesor" value="4" />
                                                            <i></i>
                                                            Morrudos
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoespesor" type="radio" name="espesor" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la sexta seccion -->
                                                <!--septima  seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Altura naso labial <a style="color: red">*</a></label>
                                                    <select name="motivo" id="labiosaltura" runat="server" class="select2" style="width:100%;">
                                                        <option>[Altura naso labial]</option>
                                                    </select>
                                                    <i></i>
                                                </section>
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Prominencia</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="prominencia" id="labioinferior" value="1" />
                                                            <i></i>
                                                            Labio inferior
                                                        </label>
                                                        <label class="radio">
                                                            <input id="labiosuperior" type="radio" name="prominencia" value="2" />
                                                            <i></i>
                                                            Labio superior
                                                        </label>
                                                        <label class="radio">
                                                            <input id="ninguno" type="radio" name="prominencia" value="3" />
                                                            <i></i>
                                                            Ninguno
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="sindatolabios" type="radio" name="prominencia" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la septima seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab7-->
                                    <!--tab numero8 menton-->
                                    <div class="tab-pane" id="tab8">
                                        <div id="smart-form-rigister8" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Tipo</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="mentontipo" id="bilovado" value="1" />
                                                            <i></i>
                                                            Bilovado
                                                        </label>
                                                        <label class="radio">
                                                            <input id="foseta" type="radio" name="mentontipo" value="2" />
                                                            <i></i>
                                                            Foseta
                                                        </label>
                                                        <label class="radio">
                                                            <input id="borla" type="radio" name="mentontipo" value="3" />
                                                            <i></i>
                                                            Borla
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="ningunomenton" type="radio" name="mentontipo" value="4" />
                                                            <i></i>
                                                            Ninguno
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatomenton" type="radio" name="mentontipo" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Forma</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="mentonforma" id="ovalforma" value="1" />
                                                            <i></i>
                                                            Oval
                                                        </label>
                                                        <label class="radio">
                                                            <input id="cuadradoforma" type="radio" name="mentonforma" value="2" />
                                                            <i></i>
                                                            Cuadrado
                                                        </label>
                                                        <label class="radio">
                                                            <input id="puntaforma" type="radio" name="mentonforma" value="3" />
                                                            <i></i>
                                                            En punto
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="sindatoformam" type="radio" name="mentonforma" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la segunda seccion -->
                                                <!--tercera seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Inclinación</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="inclinacionm" id="huyente" value="1" s />
                                                            <i></i>
                                                            Huyente
                                                        </label>
                                                        <label class="radio">
                                                            <input id="prominente" type="radio" name="inclinacionm" value="2" />
                                                            <i></i>
                                                            Prominente
                                                        </label>
                                                        <label class="radio">
                                                            <input id="vertical" type="radio" name="inclinacionm" value="3" />
                                                            <i></i>
                                                            Vertical
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="sindatoiclinacion" type="radio" name="inclinacionm" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab8-->
                                    <!--tab nuemro 9 orja-->
                                    <div class="tab-pane" id="tab9">
                                        <div id="smart-form-register9" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-3">
                                                    <h2><b>Oreja derecha</b></h2>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion  -->
                                                <section class="col col-1">
                                                </section>
                                                <!--fin de la segunda seccion -->
                                                <!--tercera  seccion -->
                                                <section class="col col-6">
                                                    <h2><b>Hélix</b></h2>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                            </div>
                                            <div class="row">
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Original <a style="color: red">*</a></label>
                                                    <select name="motivo" id="original" runat="server" class="select2" style="width:100%;">
                                                        <option>[Original]</option>
                                                    </select>
                                                    <i></i>
                                                </section>
                                                <!--cuarta seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Forma</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="oreja" id="cuadradaoreja" value="1" />
                                                            <i></i>
                                                            Cuadrada
                                                        </label>
                                                        <label class="radio">
                                                            <input id="ovaladaoreja" type="radio" name="oreja" value="2" />
                                                            <i></i>
                                                            Ovalada
                                                        </label>
                                                        <label class="radio">
                                                            <input id="redondaoreja" type="radio" name="oreja" value="3" />
                                                            <i></i>
                                                            Redonda
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="triangularoreja" type="radio" name="oreja" value="3" />
                                                            <i></i>
                                                            Triangular
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatooreja" type="radio" name="oreja" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la cuarta seccion -->
                                                <!--quinta seccion  -->
                                                <!--fin de la quinta seccion -->
                                                <!--sexta seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Superior <a style="color: red">*</a></label>
                                                    <select name="motivo" id="superior" runat="server" class="select2" style="width:100%;">
                                                        <option>[Superior]</option>
                                                    </select>
                                                    <i></i>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Posterior <a style="color: red">*</a></label>
                                                    <select name="motivo" id="posterior" runat="server" class="select2" style="width:100%;">
                                                        <option>[Posterior]</option>
                                                    </select>
                                                    <i></i>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Adherencia <a style="color: red">*</a></label>
                                                    <select name="motivo" id="helixadherencia" runat="server" class="select2" style="width:100%;">
                                                        <option>[Adherencia]</option>
                                                    </select>
                                                    <i></i>
                                                </section>
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Contorno</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="contorno" id="descendente" value="1" />
                                                            <i></i>
                                                            Descendente
                                                        </label>
                                                        <label class="radio">
                                                            <input id="escuadra" type="radio" name="contorno" value="2" />
                                                            <i></i>
                                                            En escuadra
                                                        </label>
                                                        <label class="radio">
                                                            <input id="golfo" type="radio" name="contorno" value="3" />
                                                            <i></i>
                                                            En golfo
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="intermedio" type="radio" name="contorno" value="4" />
                                                            <i></i>
                                                            Intermedio
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatohelix" type="radio" name="contorno" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la sexta seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab9-->
                                    <!--tab nuemro 10 lobulo-->
                                    <div class="tab-pane" id="tab10">
                                        <div id="smart-form-register10" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Adherencia <a style="color: red">*</a></label>
                                                        <select name="motivo" id="labuloadherencia" runat="server" class="select2" style="width:100%;">
                                                            <option>[Adherencia]</option>
                                                        </select>
                                                        <i></i>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Dimensión <a style="color: red">*</a></label>
                                                        <select name="motivo" id="dimension" runat="server" class="select2" style="width:100%;">
                                                            <option>[Dimensión]</option>
                                                        </select>
                                                        <i></i>
                                                </section>
                                                <!--fin de la sgunda seccion -->
                                                <!--tercera seccion seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Particularidad</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="lobulo" id="perforado" value="1" />
                                                            <i></i>
                                                            Perforado
                                                        </label>
                                                        <label class="radio">
                                                            <input id="fosetalobulo" type="radio" name="lobulo" value="2" />
                                                            <i></i>
                                                            Foseta
                                                        </label>
                                                        <label class="radio">
                                                            <input id="islote" type="radio" name="lobulo" value="3" />
                                                            <i></i>
                                                            Islote
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="sindatolobulo" type="radio" name="lobulo" value="4" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la tercera seccion -->
                                                <!--cuarta seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Trago</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="trago" id="puntatrago" value="1" />
                                                            <i></i>
                                                            En punta
                                                        </label>
                                                        <label class="radio">
                                                            <input id="inexistentetrago" type="radio" name="trago" value="2" />
                                                            <i></i>
                                                            Inexistente
                                                        </label>
                                                        <label class="radio">
                                                            <input id="intermediotrago" type="radio" name="trago" value="3" />
                                                            <i></i>
                                                            Intermedio
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="redondotrago" type="radio" name="trago" value="4" />
                                                            <i></i>
                                                            Redondo
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatotrago" type="radio" name="trago" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la cuarta seccion -->
                                                <!--quinta seccion -->
                                                <section class="col col-2">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Antitrago</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="antitrago" id="puntaanti" value="1" />
                                                            <i></i>
                                                            En punta
                                                        </label>
                                                        <label class="radio">
                                                            <input id="horizontalanti" type="radio" name="antitrago" value="1" />
                                                            <i></i>
                                                            Horizontal
                                                        </label>
                                                        <label class="radio">
                                                            <input id="intermedioanti" type="radio" name="antitrago" value="2" />
                                                            <i></i>
                                                            Intermedio
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="oblicuaanti" type="radio" name="antitrago" value="3" />
                                                            <i></i>
                                                            Oblicua
                                                        </label>
                                                        <label class="radio">
                                                            <input id="redondoanti" type="radio" name="antitrago" value="4" />
                                                            <i></i>
                                                            Redondo
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatoanti" type="radio" name="antitrago" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la quinta seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab10-->
                                    <!--tab numero11 general -->
                                    <div class="tab-pane" id="tab11">
                                        <div id="smart-form-register11" class="smart-form">
                                            <div class="row">
                                                <!--primera seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Sangre</label>
                                                    <section class="col col-3" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="sangre" id="a" value="1" />
                                                            <i></i>
                                                            A
                                                        </label>
                                                        <label class="radio">
                                                            <input id="b" type="radio" name="sangre" value="2" />
                                                            <i></i>
                                                            B
                                                        </label>
                                                        <label class="radio">
                                                            <input id="o" type="radio" name="sangre" value="3" />
                                                            <i></i>
                                                            O
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="radio">
                                                            <input id="ab" type="radio" name="sangre" value="4" />
                                                            <i></i>
                                                            AB
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatosangre" type="radio" name="sangre" value="5" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la primera seccion -->
                                                <!--segunda seccion seccion -->
                                                <section class="col col-3">
                                                    <label class="label" style="color: #006ead; font-weight: bold">Tipo</label>
                                                    <section class="col col-6" style="padding-left: 0px;">
                                                        <label class="radio">
                                                            <input type="radio" name="tipo" id="mas" value="1" />
                                                            <i></i>
                                                            +
                                                        </label>
                                                        <label class="radio">
                                                            <input id="menos" type="radio" name="tipo" value="2" />
                                                            <i></i>
                                                            -
                                                        </label>
                                                        <label class="radio">
                                                            <input id="sindatotipo" type="radio" name="tipo" value="3" checked="checked" />
                                                            <i></i>
                                                            Sin dato
                                                        </label>
                                                    </section>
                                                </section>
                                                <!--fin de la segunda seccion -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--fin tab11-->
                                </div>
                                <!--fin tab contenido -->
                                <div class="row"></div>
                                <div class="row smart-form">
                                    <footer>
                                        <div class="row" style="display: inline-block; float: right">
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-default save" title="Guardar" id="add_"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                            <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                        </div>
                                        <input type="hidden" id="tracking" runat="server" />
                                        <input type="hidden" id="id" runat="server" />
                                        <input type="hidden" id="trackinga" runat="server" />
                                        <input type="hidden" id="ida" runat="server" />
                                        <input type="hidden" id="trackingg" runat="server" />
                                        <input type="hidden" id="idg" runat="server" />
                                    </footer>
                                </div>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo"  class="form-control alptext" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text" maxlength="4"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"  maxlength="10"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="MNBVCX" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="Ingreso" runat="server" value="" />
    <input type="hidden" id="Informacion_Personal" runat="server" value="" />

    <input type="hidden" id="Señas_Particulares" runat="server" value="" />
    <input type="hidden" id="Adicciones" runat="server" value="" />
    <input type="hidden" id="Estudios_Criminologicos" runat="server" value="" />
    <input type="hidden" id="Expediente_Medico" runat="server" value="" />
    <input type="hidden" id="Familiares" runat="server" value="" />

    <input type="hidden" id="editable" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            var sectionNumber = $("#lstTabs .active").children().attr("id").substring(7);

            if (e.ctrlKey && e.keyCode === 39) {
                e.preventDefault();
                document.getElementById("linkseñas").click();
            }
            else if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                document.getElementById("linkinforme").click();
            }
            else if (e.ctrlKey && e.keyCode === 38) {
                e.preventDefault();
                switch (sectionNumber) {
                    case "1": document.getElementById("linkTab11").click(); break;
                    case "2": document.getElementById("linkTab1").click(); break;
                    case "3": document.getElementById("linkTab2").click(); break;
                    case "4": document.getElementById("linkTab3").click(); break;
                    case "5": document.getElementById("linkTab4").click(); break;
                    case "6": document.getElementById("linkTab5").click(); break;
                    case "7": document.getElementById("linkTab6").click(); break;
                    case "8": document.getElementById("linkTab7").click(); break;
                    case "9": document.getElementById("linkTab8").click(); break;
                    case "10": document.getElementById("linkTab9").click(); break;
                    case "11": document.getElementById("linkTab10").click(); break;
                }
            }
            else if (e.ctrlKey && e.keyCode === 40) {
                e.preventDefault();
                switch (sectionNumber) {
                    case "1": document.getElementById("linkTab2").click(); break;
                    case "2": document.getElementById("linkTab3").click(); break;
                    case "3": document.getElementById("linkTab4").click(); break;
                    case "4": document.getElementById("linkTab5").click(); break;
                    case "5": document.getElementById("linkTab6").click(); break;
                    case "6": document.getElementById("linkTab7").click(); break;
                    case "7": document.getElementById("linkTab8").click(); break;
                    case "8": document.getElementById("linkTab9").click(); break;
                    case "9": document.getElementById("linkTab10").click(); break;
                    case "10": document.getElementById("linkTab11").click(); break;
                    case "11": document.getElementById("linkTab1").click(); break;
                }
            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if (ExistenCambiosGeneral()) {
                    document.getElementsByClassName("saveGeneral")[0].click();
                }
                else {
                    document.getElementsByClassName("save")[0].click();
                }
            }
        });

        function ExistenCambiosGeneral() {
            var oldValues = $("#datosgenerales").val().split(",");
            var newValues = ObtenerValoresGeneral();

            var existenCambios = false;
            if (oldValues[2] != newValues[2]) existenCambios = true;
            else if (oldValues[3] != newValues[3]) existenCambios = true;
            else if (!((oldValues[5] == "false" && newValues[5] == false) || (oldValues[5] == "true" && newValues[5] == true))) existenCambios = true;
            else if (!((oldValues[6] == "false" && newValues[6] == false) || (oldValues[6] == "true" && newValues[6] == true))) existenCambios = true;
            else if (!((oldValues[7] == "false" && newValues[7] == false) || (oldValues[7] == "true" && newValues[7] == true))) existenCambios = true;
            else if (!((oldValues[8] == "false" && newValues[8] == false) || (oldValues[8] == "true" && newValues[8] == true))) existenCambios = true;
            else if (!((oldValues[9] == "false" && newValues[9] == false) || (oldValues[9] == "true" && newValues[9] == true))) existenCambios = true;
            console.log(existenCambios);
            return existenCambios
        }

        function ObtenerValoresGeneral() {

            var lunares = false;
            var tatuajes = false;
            var defectos = false;
            var cicatrices = false;
            var anteojos = false;

            if ($('#lunar').is(":checked")) {
                lunares = true;
            }

            if ($('#tatuaje').is(":checked")) {
                tatuajes = true;
            }

            if ($('#defecto').is(":checked")) {
                defectos = true;
            }

            if ($('#cicatris').is(":checked")) {
                cicatrices = true;
            }

            if ($('#anteojos').is(":checked")) {
                anteojos = true;
            }

            var datos = [
                Id = $('#ctl00_contenido_id').val(),
                TrackingId = $('#ctl00_contenido_tracking').val(),
                e = $('#estaturam').val(),
                p = $('#ctl00_contenido_peso').val(),

                dactiloscopica = "sindato",
                lunares = lunares,
                tatuajes = tatuajes,
                defectos = defectos,
                cicatrices = cicatrices,
                anteojos = anteojos,
                Ida = $('#ctl00_contenido_ida').val(),
                TrackingIda = $('#ctl00_contenido_trackinga').val(),
            ];
            return datos;
        }

        $(function () {
            $('.decimal_3').maskMoney({
                thousands: ',',
                decimal: '.',
                allowZero: true,
                suffix: '',
                precision: 3
            });

            $(".integer").keypress(function (event) {
                return /\d/.test(String.fromCharCode(event.keyCode));
            });
        });

        $(document).ready(function () {
            $("#ScrollableContent").css("height", "calc(100vh - 443px)");

            $("#datospersonales-modal").on("hidden.bs.modal", function () {
                document.getElementsByClassName("detencion")[0].focus();
            });

            pageSetUp();
            init();

            $("#ligarNuevoEvento").hide();
            if ($("#<%= editable.ClientID %>").val() == "0") {
                $("#estaturam").attr("disabled", "disabled");
                $("#<%= peso.ClientID %>").attr("disabled", "disabled");
                $("#<%= dactiloscopica.ClientID %>").attr("disabled", "disabled");
                $("input[name=general]").attr("disabled", "disabled");
                $("input[name=radiocomplexion]").attr("disabled", "disabled");
                $("input[name=colorpiel]").attr("disabled", "disabled");
                $("input[name=cara]").attr("disabled", "disabled");
                $("input[name=cabellocantidad]").attr("disabled", "disabled");
                $("input[name=piel]").attr("disabled", "disabled");
                $("input[name=formacabello]").attr("disabled", "disabled");
                $("input[name=calvicie]").attr("disabled", "disabled");
                $("input[name=implementacioncabello]").attr("disabled", "disabled");
                $("#<%= frentealtura.ClientID %>").attr("disabled", "disabled");
                $("#<%= frenteancho.ClientID %>").attr("disabled", "disabled");
                $("input[name=frente]").attr("disabled", "disabled");
                $("input[name=direccion]").attr("disabled", "disabled");
                $("input[name=implementacioncejas]").attr("disabled", "disabled");
                $("input[name=formacejas]").attr("disabled", "disabled");
                $("input[name=implementacioncabello]").attr("disabled", "disabled");
                $("input[name=tamañocejas]").attr("disabled", "disabled");
                $("input[name=ojoscalor]").attr("disabled", "disabled");
                $("input[name=implementacion]").attr("disabled", "disabled");
                $("input[name=implementacion]").attr("disabled", "disabled");
                $("input[name=implementacion]").attr("disabled", "disabled");
                $("input[name=implementacion]").attr("disabled", "disabled");
                $("#<%= ojoastamaño.ClientID %>").attr("disabled", "disabled");
                $("#<%= narizraiz.ClientID %>").attr("disabled", "disabled");
                $("#<%= narizaltura.ClientID %>").attr("disabled", "disabled");
                $("#<%= narizancho.ClientID %>").attr("disabled", "disabled");
                $("input[name=narizdorso]").attr("disabled", "disabled");
                $("input[name=narizbase]").attr("disabled", "disabled");
                $("#<%= bocatamaño.ClientID %>").attr("disabled", "disabled");
                $("#<%= labiosaltura.ClientID %>").attr("disabled", "disabled");
                $("input[name=boca]").attr("disabled", "disabled");
                $("input[name=espesor]").attr("disabled", "disabled");
                $("input[name=prominencia]").attr("disabled", "disabled");
                $("input[name=mentontipo]").attr("disabled", "disabled");
                $("input[name=mentonforma]").attr("disabled", "disabled");
                $("input[name=inclinacionm]").attr("disabled", "disabled");
                $("#<%= original.ClientID %>").attr("disabled", "disabled");
                $("#<%= superior.ClientID %>").attr("disabled", "disabled");
                $("#<%= posterior.ClientID %>").attr("disabled", "disabled");
                $("input[name=oreja]").attr("disabled", "disabled");
                $("#<%= helixadherencia.ClientID %>").attr("disabled", "disabled");
                $("input[name=contorno]").attr("disabled", "disabled");
                $("#<%= labuloadherencia.ClientID %>").attr("disabled", "disabled");
                $("#<%= dimension.ClientID %>").attr("disabled", "disabled");
                $("input[name=lobulo]").attr("disabled", "disabled");
                $("input[name=trago]").attr("disabled", "disabled");
                $("input[name=antitrago]").attr("disabled", "disabled");
                $("input[name=sangre]").attr("disabled", "disabled");
                $("input[name=tipo]").attr("disabled", "disabled");

                $("#estaturam").css("background", "#eee");
                $("#<%= peso.ClientID %>").css("background", "#eee");
                $("#<%= dactiloscopica.ClientID %>").css("background", "#eee");
                $("input[name=general]").next().css("background", "#eee");
                $("input[name=radiocomplexion]").next().css("background", "#eee");
                $("input[name=colorpiel]").next().css("background", "#eee");
                $("input[name=cara]").next().css("background", "#eee");
                $("input[name=cabellocantidad]").next().css("background", "#eee");
                $("input[name=piel]").next().css("background", "#eee");
                $("input[name=formacabello]").next().css("background", "#eee");
                $("input[name=calvicie]").next().css("background", "#eee");
                $("input[name=implementacioncabello]").next().css("background", "#eee");
                $("#<%= frentealtura.ClientID %>").css("background", "#eee");
                $("#<%= frenteancho.ClientID %>").css("background", "#eee");
                $("input[name=frente]").next().css("background", "#eee");
                $("input[name=direccion]").next().css("background", "#eee");
                $("input[name=implementacioncejas]").next().css("background", "#eee");
                $("input[name=formacejas]").next().css("background", "#eee");
                $("input[name=implementacioncabello]").next().css("background", "#eee");
                $("input[name=tamañocejas]").next().css("background", "#eee");
                $("input[name=ojoscalor]").next().css("background", "#eee");
                $("input[name=implementacion]").next().css("background", "#eee");
                $("input[name=implementacion]").next().css("background", "#eee");
                $("input[name=implementacion]").next().css("background", "#eee");
                $("input[name=implementacion]").next().css("background", "#eee");
                $("#<%= ojoastamaño.ClientID %>").css("background", "#eee");
                $("#<%= narizraiz.ClientID %>").css("background", "#eee");
                $("#<%= narizaltura.ClientID %>").css("background", "#eee");
                $("#<%= narizancho.ClientID %>").css("background", "#eee");
                $("input[name=narizdorso]").next().css("background", "#eee");
                $("input[name=narizbase]").next().css("background", "#eee");
                $("#<%= bocatamaño.ClientID %>").css("background", "#eee");
                $("#<%= labiosaltura.ClientID %>").css("background", "#eee");
                $("input[name=boca]").next().css("background", "#eee");
                $("input[name=espesor]").next().css("background", "#eee");
                $("input[name=prominencia]").next().css("background", "#eee");
                $("input[name=mentontipo]").next().css("background", "#eee");
                $("input[name=mentonforma]").next().css("background", "#eee");
                $("input[name=inclinacionm]").next().css("background", "#eee");
                $("#<%= original.ClientID %>").css("background", "#eee");
                $("#<%= superior.ClientID %>").css("background", "#eee");
                $("#<%= posterior.ClientID %>").css("background", "#eee");
                $("input[name=oreja]").next().css("background", "#eee");
                $("#<%= helixadherencia.ClientID %>").css("background", "#eee");
                $("input[name=contorno]").next().css("background", "#eee");
                $("#<%= labuloadherencia.ClientID %>").css("background", "#eee");
                $("#<%= dimension.ClientID %>").css("background", "#eee");
                $("input[name=lobulo]").next().css("background", "#eee");
                $("input[name=trago]").next().css("background", "#eee");
                $("input[name=antitrago]").next().css("background", "#eee");
                $("input[name=sangre]").next().css("background", "#eee");
                $("input[name=tipo]").next().css("background", "#eee");

                $("#ligarNuevoEvento").show();
            }

            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "anthropometry.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var editarA = JSON.parse(data.d).EditarA;
                            var editar = JSON.parse(data.d).Editar;

                            $('#edadInterno').text(JSON.parse(data.d).objA.edad);
                            $('#sexoInterno').text(JSON.parse(data.d).objA.sexo);
                            $('#municipioInterno').text(JSON.parse(data.d).objA.municipioNombre);
                            $('#domicilioInterno').text(JSON.parse(data.d).objA.domiclio);
                            $('#coloniaInterno').text(JSON.parse(data.d).objA.coloniaNombre);

                            if (editarA) {


                                $('#estaturam').val(JSON.parse(data.d).objA.estaturam);
                                $('#<%= peso.ClientID %>').val(JSON.parse(data.d).objA.peso);
                                $('#<%= dactiloscopica.ClientID %>').val(JSON.parse(data.d).objA.dactiloscopica);

                                if (JSON.parse(data.d).objA.lunar)
                                    $('#lunar').prop('checked', true);

                                if (JSON.parse(data.d).objA.tatuaje)
                                    $('#tatuaje').prop('checked', true);

                                if (JSON.parse(data.d).objA.defecto)
                                    $('#defecto').prop('checked', true);

                                if (JSON.parse(data.d).objA.cicatris)
                                    $('#cicatris').prop('checked', true);

                                if (JSON.parse(data.d).objA.anteojos)
                                    $('#anteojos').prop('checked', true);

                                $("#datosgenerales").val(ObtenerValoresGeneral());

                                if (editar) {

                                    $("input[name='radiocomplexion'][value='" + JSON.parse(data.d).obj.radiocomplexion + "']").prop('checked', true);
                                    $("input[name='colorpiel'][value='" + JSON.parse(data.d).obj.colorpiel + "']").prop('checked', true);
                                    $("input[name='cara'][value='" + JSON.parse(data.d).obj.cara + "']").prop('checked', true);
                                    $("input[name='cabellocantidad'][value='" + JSON.parse(data.d).obj.cabellocantidad + "']").prop('checked', true);
                                    $("input[name='piel'][value='" + JSON.parse(data.d).obj.piel + "']").prop('checked', true);
                                    $("input[name='formacabello'][value='" + JSON.parse(data.d).obj.formacabello + "']").prop('checked', true);
                                    $("input[name='calvicie'][value='" + JSON.parse(data.d).obj.calvicie + "']").prop('checked', true);
                                    $("input[name='implementacioncabello'][value='" + JSON.parse(data.d).obj.implementacioncabello + "']").prop('checked', true);

                                    $("input[name='frente'][value='" + JSON.parse(data.d).obj.frente + "']").prop('checked', true);
                                    $("input[name='direccion'][value='" + JSON.parse(data.d).obj.direccion + "']").prop('checked', true);
                                    $("input[name='implementacioncejas'][value='" + JSON.parse(data.d).obj.implementacioncejas + "']").prop('checked', true);
                                    $("input[name='formacejas'][value='" + JSON.parse(data.d).obj.formacejas + "']").prop('checked', true);
                                    $("input[name='tamañocejas'][value='" + JSON.parse(data.d).obj.tamanocejas + "']").prop('checked', true);
                                    $("input[name='ojoscalor'][value='" + JSON.parse(data.d).obj.ojoscalor + "']").prop('checked', true);
                                    $("input[name='implementacion'][value='" + JSON.parse(data.d).obj.implementacion + "']").prop('checked', true);

                                    $("input[name='narizdorso'][value='" + JSON.parse(data.d).obj.narizdorso + "']").prop('checked', true);
                                    $("input[name='narizbase'][value='" + JSON.parse(data.d).obj.narizbase + "']").prop('checked', true);

                                    $("input[name='boca'][value='" + JSON.parse(data.d).obj.boca + "']").prop('checked', true);

                                    $("input[name='espesor'][value='" + JSON.parse(data.d).obj.espesor + "']").prop('checked', true);
                                    $("input[name='prominencia'][value='" + JSON.parse(data.d).obj.prominencia + "']").prop('checked', true);
                                    $("input[name='mentontipo'][value='" + JSON.parse(data.d).obj.mentontipo + "']").prop('checked', true);
                                    $("input[name='mentonforma'][value='" + JSON.parse(data.d).obj.mentonforma + "']").prop('checked', true);
                                    $("input[name='inclinacionm'][value='" + JSON.parse(data.d).obj.inclinacionm + "']").prop('checked', true);

                                    $("input[name='oreja'][value='" + JSON.parse(data.d).obj.oreja + "']").prop('checked', true);
                                    $("input[name='contorno'][value='" + JSON.parse(data.d).obj.contorno + "']").prop('checked', true);
                                    $("input[name='lobulo'][value='" + JSON.parse(data.d).obj.lobulo + "']").prop('checked', true);
                                    $("input[name='trago'][value='" + JSON.parse(data.d).obj.trago + "']").prop('checked', true);
                                    $("input[name='antitrago'][value='" + JSON.parse(data.d).obj.antitrago + "']").prop('checked', true);
                                    $("input[name='sangre'][value='" + JSON.parse(data.d).obj.sangre + "']").prop('checked', true);
                                    $("input[name='tipo'][value='" + JSON.parse(data.d).obj.tipo + "']").prop('checked', true);



                                    $('#<%= trackingg.ClientID %>').val(JSON.parse(data.d).obj.Tracking);
                                    $('#<%= idg.ClientID %>').val(JSON.parse(data.d).obj.Id);
                                    $('#<%= trackinga.ClientID %>').val(JSON.parse(data.d).objA.TrackingA);
                                    $('#<%= ida.ClientID %>').val(JSON.parse(data.d).objA.IdA);

                                    CargarTamaño(JSON.parse(data.d).obj.frentealtura, '#ctl00_contenido_frentealtura', "[Altura]");
                                    CargarTamaño(JSON.parse(data.d).obj.frenteancho, '#ctl00_contenido_frenteancho', "[Ancho]");
                                    CargarTamaño(JSON.parse(data.d).obj.ojoastamano, '#ctl00_contenido_ojoastamaño', "[Tamaño]");
                                    CargarTamaño(JSON.parse(data.d).obj.narizraiz, '#ctl00_contenido_narizraiz', "[Raíz]");
                                    CargarTamaño(JSON.parse(data.d).obj.narizaltura, '#ctl00_contenido_narizaltura', "[Altura]");
                                    CargarTamaño(JSON.parse(data.d).obj.narizancho, '#ctl00_contenido_narizancho', "[Ancho]");
                                    CargarTamaño(JSON.parse(data.d).obj.bocatamano, '#ctl00_contenido_bocatamaño', "[Tamaño]");
                                    CargarTamaño(JSON.parse(data.d).obj.labiosaltura, '#ctl00_contenido_labiosaltura', "[Altura]");
                                    CargarTamaño(JSON.parse(data.d).obj.original, '#ctl00_contenido_original', "[Original]");
                                    CargarTamaño(JSON.parse(data.d).obj.superior, '#ctl00_contenido_superior', "[Superior]");
                                    CargarTamaño(JSON.parse(data.d).obj.posterior, '#ctl00_contenido_posterior', "[Posterior]");
                                    CargarTamaño(JSON.parse(data.d).obj.dimension, '#ctl00_contenido_dimension', "[Dimensión]");

                                    CargarAdherencia(JSON.parse(data.d).obj.helixadherencia, '#ctl00_contenido_helixadherencia');
                                    CargarAdherencia(JSON.parse(data.d).obj.labuloadherencia, '#ctl00_contenido_labuloadherencia');

                                }
                                else {
                                    if ($("#ctl00_contenido_KAQWPK").val() == "true") {
                                        if ($("#<%= editable.ClientID %>").val() == "0") {
                                            $("#add_").hide();
                                        }
                                        else {
                                            $("#add_").show();
                                        }
                                    }

                                    CargarTamaño(0, '#ctl00_contenido_frentealtura', "[Altura]");
                                    CargarTamaño(0, '#ctl00_contenido_frenteancho', "[Ancho]");
                                    CargarTamaño(0, '#ctl00_contenido_ojoastamaño', "[Tamaño]")
                                    CargarTamaño(0, '#ctl00_contenido_narizraiz', "[Raíz]");
                                    CargarTamaño(0, '#ctl00_contenido_narizaltura', "[Altura]");
                                    CargarTamaño(0, '#ctl00_contenido_narizancho', "[Ancho]");
                                    CargarTamaño(0, '#ctl00_contenido_bocatamaño', "[Tamaño]");
                                    CargarTamaño(0, '#ctl00_contenido_labiosaltura', "[Altura]");
                                    CargarTamaño(0, '#ctl00_contenido_original', "[Original]");
                                    CargarTamaño(0, '#ctl00_contenido_superior', "[Superior]");
                                    CargarTamaño(0, '#ctl00_contenido_posterior', "[Posterior]");
                                    CargarTamaño(0, '#ctl00_contenido_dimension', "[Dimensión]");

                                    CargarAdherencia(0, '#ctl00_contenido_helixadherencia');
                                    CargarAdherencia(0, '#ctl00_contenido_labuloadherencia');
                                    $('#<%= trackinga.ClientID %>').val(JSON.parse(data.d).objA.TrackingA);
                                    $('#<%= ida.ClientID %>').val(JSON.parse(data.d).objA.IdA);
                                }
                            }
                            else {
                                CargarTamaño(0, '#ctl00_contenido_frentealtura', "[Altura]");
                                CargarTamaño(0, '#ctl00_contenido_frenteancho', "[Ancho]");
                                CargarTamaño(0, '#ctl00_contenido_ojoastamaño', "[Tamaño]")
                                CargarTamaño(0, '#ctl00_contenido_narizraiz', "[Raíz]");
                                CargarTamaño(0, '#ctl00_contenido_narizaltura', "[Altura]");
                                CargarTamaño(0, '#ctl00_contenido_narizancho', "[Ancho]");
                                CargarTamaño(0, '#ctl00_contenido_bocatamaño', "[Tamaño]");
                                CargarTamaño(0, '#ctl00_contenido_labiosaltura', "[Altura]");
                                CargarTamaño(0, '#ctl00_contenido_original', "[Original]");
                                CargarTamaño(0, '#ctl00_contenido_superior', "[Superior]");
                                CargarTamaño(0, '#ctl00_contenido_posterior', "[Posterior]");
                                CargarTamaño(0, '#ctl00_contenido_dimension', "[Dimensión]");

                                CargarAdherencia(0, '#ctl00_contenido_helixadherencia');
                                CargarAdherencia(0, '#ctl00_contenido_labuloadherencia');
                            }

                            $('#<%= tracking.ClientID %>').val(JSON.parse(data.d).objA.TrackingId);
                            $('#<%= id.ClientID %>').val(JSON.parse(data.d).objA.Id);


                            $('#nombreInterno').text(JSON.parse(data.d).objA.Nombre);
                            $('#expedienteInterno').text(JSON.parse(data.d).objA.Expediente);
                            $('#centroInterno').text(JSON.parse(data.d).objA.Centro);
                            var imagenAvatar = ResolveUrl(JSON.parse(data.d).objA.RutaImagen);
                            if (imagenAvatar != "")
                                $('#avatar').attr("src", imagenAvatar);

                            $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + JSON.parse(data.d).objA.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkvistas").attr("href", "huella.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkestudios").attr("href", "record.aspx?tracking=" + JSON.parse(data.d).objA.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + resultado.objA.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkseñas").attr("href", "signal.aspx?tracking=" + JSON.parse(data.d).objA.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + JSON.parse(data.d).objA.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + JSON.parse(data.d).objA.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkingreso").attr("href", "entry.aspx?tracking=" + JSON.parse(data.d).objA.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + JSON.parse(data.d).objA.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                        } else {
                            ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }


                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            function CargarTamaño(set, combo, opcion) {

                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "anthropometry.aspx/getTamano",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option(opcion, "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de centros. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }


            function CargarAdherencia(set, combo) {
                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "anthropometry.aspx/getAdherencia",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Adherencia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de adherecia. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("#showFiliacion").click(function () {
                $("#hideFiliacion").css('display', 'block');
                $("#showFiliacion").css('display', 'none');

                $("#Filiacionoculta").show();
            })
            $("#hideFiliacion").click(function () {
                $("#hideFiliacion").css('display', 'none');
                $("#showFiliacion").css('display', 'block');

                $("#Filiacionoculta").hide();
            });


            $("body").on("click", ".saveGeneral", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validarGeneral()) {
                    GuardarGeneral();

                }

            });

            $("body").on("click", ".clear", function () {
                $("#ctl00_estaturam").val("");
                $("#ctl00_contenido_peso").val("");
                //$("#ctl00_contenido_formulai").val("");
                //$("#ctl00_contenido_subformulai").val("");
                //$("#ctl00_contenido_formulad").val("");
                //$("#ctl00_contenido_subformulad").val("");
                //$("#ctl00_contenido_vucetich").val("");
                // $("#ctl00_contenido_dactiloscopica").val("");
                $("#lunar:checkbox").attr('checked', false);
                $("#tatuaje:checkbox").attr('checked', false);
                $("#defecto:checkbox").attr('checked', false);
                $("#cicatris:checkbox").attr('checked', false);
                $("#anteojos:checkbox").attr('checked', false);


            });
            function validarGeneral() {
                var esvalido = true;


                if ($("#estaturam").val() == null || $("#estaturam").val().split(" ").join("") == "") {
                    ShowError("Estatura", "La estatura es obligatoria.");
                    $('#estaturam').parent().removeClass('state-success').addClass("state-error");
                    $('#estaturam').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#estaturam').parent().removeClass("state-error").addClass('state-success');
                    $('#estaturam').addClass('valid');
                }

                if ($("#ctl00_contenido_peso").val() == null || $("#ctl00_contenido_peso").val().split(" ").join("") == "") {
                    ShowError("Peso", "El peso es obligatorio.");
                    $('#ctl00_contenido_peso').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_peso').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_peso').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_peso').addClass('valid');
                }


                return esvalido;

            }


            function limpiarGeneral() {
                $('#estaturam').parent().removeClass('state-success');
                $('#estaturam').parent().removeClass("state-error");
                $('#ctl00_contenido_peso').parent().removeClass('state-success');
                $('#ctl00_contenido_peso').parent().removeClass("state-error");

            }



            function GuardarGeneral() {
                startLoading();
                var datos = ObtenerValoresGeneral();
                $.ajax({
                    type: "POST",
                    url: "anthropometry.aspx/saveGeneral",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_id').val(resultado.Id);
                            $('#ctl00_contenido_tracking').val(resultado.TrackingId);
                            $('#<%= trackinga.ClientID %>').val(resultado.TrackingIdA);
                            $('#<%= ida.ClientID %>').val(resultado.IdA);
                            $("#datosgenerales").val(datos);
                            $("#add_").show();
                            limpiarGeneral();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);

                            $('#main').waitMe('hide');
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                            $("#Filiacionoculta").hide();
                            $("#hideFiliacion").css('display', 'none');
                            $("#showFiliacion").css('display', 'block');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);

                            if (resultado.mensaje == "El valor del peso y estatura debe ser mayor a cero.") {
                                $('#estaturam').parent().removeClass('state-success').addClass("state-error");
                                $('#estaturam').removeClass('valid');
                                $('#ctl00_contenido_peso').parent().removeClass('state-success').addClass("state-error");
                                $('#ctl00_contenido_peso').removeClass('valid');
                            }
                            if (resultado.mensaje == "El valor de la estatura debe ser mayor a cero.") {
                                $('#estaturam').parent().removeClass('state-success').addClass("state-error");
                                $('#estaturam').removeClass('valid');
                            }
                            if (resultado.mensaje == "El valor del peso debe ser mayor a cero.") {
                                $('#ctl00_contenido_peso').parent().removeClass('state-success').addClass("state-error");
                                $('#ctl00_contenido_peso').removeClass('valid');
                            }

                            ShowError("¡Error!", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    Guardar();
                }
            });

            function validar() {
                var esvalido = true;

                if ($("#ctl00_contenido_MNBVCX").val() == 2) {
                    if ($("#ctl00_contenido_frentealtura").val() == null || $("#ctl00_contenido_frentealtura").val() == 0) {
                        ShowError("Frente altura", "La altura es obligatoria.");
                        $('#ctl00_contenido_frentealtura').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_frentealtura').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_frentealtura').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_frentealtura').addClass('valid');
                    }

                    if ($("#ctl00_contenido_frenteancho").val() == null || $("#ctl00_contenido_frenteancho").val() == 0) {
                        ShowError("Frente ancho", "El ancho es obligatorio.");
                        $('#ctl00_contenido_frenteancho').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_frenteancho').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_frenteancho').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_frenteancho').addClass('valid');
                    }
                }


                if ($("#ctl00_contenido_MNBVCX").val() == 4) {
                    if ($("#ctl00_contenido_ojoastamaño").val() == null || $("#ctl00_contenido_ojoastamaño").val() == 0) {
                        ShowError("Ojos tamaño", "El tamaño es obligatorio.");
                        $('#ctl00_contenido_ojoastamaño').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_ojoastamaño').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_ojoastamaño').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_ojoastamaño').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_MNBVCX").val() == 5) {
                    if ($("#ctl00_contenido_narizraiz").val() == null || $("#ctl00_contenido_narizraiz").val() == 0) {
                        ShowError("Nariz raíz", "La raíz es obligatoria.");
                        $('#ctl00_contenido_narizraiz').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_narizraiz').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_narizraiz').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_narizraiz').addClass('valid');
                    }


                    if ($("#ctl00_contenido_narizaltura").val() == null || $("#ctl00_contenido_narizaltura").val() == 0) {
                        ShowError("Nariz altura", "La altura es obligatoria.");
                        $('#ctl00_contenido_narizaltura').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_narizaltura').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_narizaltura').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_narizaltura').addClass('valid');
                    }


                    if ($("#ctl00_contenido_narizancho").val() == null || $("#ctl00_contenido_narizancho").val() == 0) {
                        ShowError("Nariz ancho", "El ancho es obligatorio.");
                        $('#ctl00_contenido_narizancho').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_narizancho').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_narizancho').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_narizancho').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_MNBVCX").val() == 6) {
                    if ($("#ctl00_contenido_bocatamaño").val() == null || $("#ctl00_contenido_bocatamaño").val() == 0) {
                        ShowError("Boca tamaño", "El tamaño es obligatorio.");
                        $('#ctl00_contenido_bocatamaño').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_bocatamaño').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_bocatamaño').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_bocatamaño').addClass('valid');
                    }


                    if ($("#ctl00_contenido_labiosaltura").val() == null || $("#ctl00_contenido_labiosaltura").val() == 0) {
                        ShowError("Labio altura", "La altura es obligatoria.");
                        $('#ctl00_contenido_labiosaltura').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_labiosaltura').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_labiosaltura').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_labiosaltura').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_MNBVCX").val() == 8) {
                    if ($("#ctl00_contenido_original").val() == null || $("#ctl00_contenido_original").val() == 0) {
                        ShowError("Oreja derecha original", "El dato original es obligatorio.");
                        $('#ctl00_contenido_original').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_original').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_original').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_original').addClass('valid');
                    }


                    if ($("#ctl00_contenido_superior").val() == null || $("#ctl00_contenido_superior").val() == 0) {
                        ShowError("Hélix superior", "El hélix superior es obligatorio.");
                        $('#ctl00_contenido_superior').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_superior').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_superior').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_superior').addClass('valid');
                    }


                    if ($("#ctl00_contenido_posterior").val() == null || $("#ctl00_contenido_posterior").val() == 0) {
                        ShowError("Hélix posterior", "El hélix superior es obligatorio.");
                        $('#ctl00_contenido_posterior').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_posterior').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_posterior').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_posterior').addClass('valid');
                    }


                    if ($("#ctl00_contenido_helixadherencia").val() == null || $("#ctl00_contenido_helixadherencia").val() == 0) {
                        ShowError("Hélix adherencia", "La adherencia es obligatoria.");
                        $('#ctl00_contenido_helixadherencia').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_helixadherencia').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_helixadherencia').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_helixadherencia').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_MNBVCX").val() == 9) {
                    if ($("#ctl00_contenido_labuloadherencia").val() == null || $("#ctl00_contenido_labuloadherencia").val() == 0) {
                        ShowError("Lóbulo adherencia", "La adherencia es obligatoria.");
                        $('#ctl00_contenido_labuloadherencia').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_labuloadherencia').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_labuloadherencia').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_labuloadherencia').addClass('valid');
                    }


                    if ($("#ctl00_contenido_dimension").val() == null || $("#ctl00_contenido_dimension").val() == 0) {
                        ShowError("Lóbulo dimensión", "La dimensión es obligatoria.");
                        $('#ctl00_contenido_dimension').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_dimension').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_dimension').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_dimension').addClass('valid');
                    }
                }

                return esvalido;

            }


            function limpiar() {

                $('#ctl00_contenido_frentealtura').parent().removeClass('state-success');
                $('#ctl00_contenido_frentealtura').parent().removeClass("state-error");
                $('#ctl00_contenido_frenteancho').parent().removeClass('state-success');
                $('#ctl00_contenido_frenteancho').parent().removeClass("state-error");
                $('#ctl00_contenido_ojoastamaño').parent().removeClass('state-success');
                $('#ctl00_contenido_ojoastamaño').parent().removeClass("state-error");
                $('#ctl00_contenido_narizraiz').parent().removeClass('state-success');
                $('#ctl00_contenido_narizraiz').parent().removeClass("state-error");
                $('#ctl00_contenido_narizaltura').parent().removeClass('state-success');
                $('#ctl00_contenido_narizaltura').parent().removeClass("state-error");
                $('#ctl00_contenido_narizancho').parent().removeClass('state-success');
                $('#ctl00_contenido_narizancho').parent().removeClass("state-error");
                $('#ctl00_contenido_bocatamaño').parent().removeClass('state-success');
                $('#ctl00_contenido_bocatamaño').parent().removeClass("state-error");
                $('#ctl00_contenido_labiosaltura').parent().removeClass('state-success');
                $('#ctl00_contenido_labiosaltura').parent().removeClass("state-error");
                $('#ctl00_contenido_original').parent().removeClass('state-success');
                $('#ctl00_contenido_original').parent().removeClass("state-error");
                $('#ctl00_contenido_superior').parent().removeClass('state-success');
                $('#ctl00_contenido_superior').parent().removeClass("state-error");
                $('#ctl00_contenido_posterior').parent().removeClass('state-success');
                $('#ctl00_contenido_posterior').parent().removeClass("state-error");
                $('#ctl00_contenido_helixadherencia').parent().removeClass('state-success');
                $('#ctl00_contenido_helixadherencia').parent().removeClass("state-error");
                $('#ctl00_contenido_labuloadherencia').parent().removeClass('state-success');
                $('#ctl00_contenido_labuloadherencia').parent().removeClass("state-error");
                $('#ctl00_contenido_dimension').parent().removeClass('state-success');
                $('#ctl00_contenido_dimension').parent().removeClass("state-error");

            }



            function Guardar() {
                startLoading();
                var datos = ObtenerValores();
                $.ajax({
                    type: "POST",
                    url: "anthropometry.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_idg').val(resultado.Id);
                            $('#ctl00_contenido_trackingg').val(resultado.TrackingId);
                            $('#<%= trackinga.ClientID %>').val(resultado.TrackingIdA);
                            $('#<%= ida.ClientID %>').val(resultado.IdA);
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            $('#main').waitMe('hide');

                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Aviso!</strong>" +
                                " " + resultado.mensaje + "</div>");
                            ShowAlert("¡Aviso!", resultado.mensaje);
                            setTimeout(hideMessage, hideTime);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }



            function ObtenerValores() {


                var datos = [
                    Ida = $('#ctl00_contenido_ida').val(),
                    TrackingIda = $('#ctl00_contenido_trackinga').val(),
                    ra = $('input:radio[name=radiocomplexion]:checked').val(),
                    cp = $('input:radio[name=colorpiel]:checked').val(),
                    ca = $('input:radio[name=cara]:checked').val(),
                    cc = $('input:radio[name=cabellocantidad]:checked').val(),
                    p = $('input:radio[name=piel]:checked').val(),
                    fc = $('input:radio[name=formacabello]:checked').val(),
                    calv = $('input:radio[name=calvicie]:checked').val(),
                    imp = $('input:radio[name=implementacioncabello]:checked').val(),
                    cfre = $('#ctl00_contenido_frentealtura').val(),
                    cfrea = $('#ctl00_contenido_frenteancho').val(),
                    fre = $('input:radio[name=frente]:checked').val(),
                    dir = $('input:radio[name=direccion]:checked').val(),
                    imc = $('input:radio[name=implementacioncejas]:checked').val(),
                    fcj = $('input:radio[name=formacejas]:checked').val(),
                    tc = $('input:radio[name=tamañocejas]:checked').val(),
                    ojc = $('input:radio[name=ojoscalor]:checked').val(),
                    impl = $('input:radio[name=implementacion]:checked').val(),
                    comboo = $('#ctl00_contenido_ojoastamaño').val(),
                    comboraiz = $('#ctl00_contenido_narizraiz').val(),
                    d1 = $('#ctl00_contenido_narizaltura').val(),
                    d2 = $('#ctl00_contenido_narizancho').val(),
                    nd = $('input:radio[name=narizdorso]:checked').val(),
                    nb = $('input:radio[name=narizbase]:checked').val(),
                    d3 = $('#ctl00_contenido_bocatamaño').val(),
                    boca = $('input:radio[name=boca]:checked').val(),
                    d4 = $('#ctl00_contenido_labiosaltura').val(),
                    esp = $('input:radio[name=espesor]:checked').val(),
                    prom = $('input:radio[name=prominencia]:checked').val(),
                    ment = $('input:radio[name=mentontipo]:checked').val(),
                    mentf = $('input:radio[name=mentonforma]:checked').val(),
                    incl = $('input:radio[name=inclinacionm]:checked').val(),
                    d5 = $('#ctl00_contenido_original').val(),
                    oreja = $('input:radio[name=oreja]:checked').val(),
                    d6 = $('#ctl00_contenido_superior').val(),
                    d7 = $('#ctl00_contenido_posterior').val(),
                    d8 = $('#ctl00_contenido_helixadherencia').val(),
                    cont = $('input:radio[name=contorno]:checked').val(),
                    d8 = $('#ctl00_contenido_labuloadherencia').val(),
                    d9 = $('#ctl00_contenido_dimension').val(),
                    lobu = $('input:radio[name=lobulo]:checked').val(),
                    tra = $('input:radio[name=trago]:checked').val(),
                    atra = $('input:radio[name=antitrago]:checked').val(),
                    sangre = $('input:radio[name=sangre]:checked').val(),
                    tipo = $('input:radio[name=tipo]:checked').val(),
                    Idg = $('#ctl00_contenido_idg').val(),
                    TrackingIdg = $('#ctl00_contenido_trackingg').val(),
                ];
                return datos;
            }



            function init() {

                var param = RequestQueryString("tracking");
                $("#ctl00_contenido_MNBVCX").val(0);
                $('#<%= tracking.ClientID %>').val(param);
                if ($("#ctl00_contenido_HQLNBB").val() == "true" || $("#ctl00_contenido_KAQWPK").val() == "true") {
                    if ($("#<%= editable.ClientID %>").val() == "0") {
                        $("#add_").hide();
                        $("#addGeneral").hide();
                    }
                    else {
                        $("#add_").show();
                        $("#addGeneral").show();
                    }
                }

                if ($("#ctl00_contenido_KAQWPK").val() == "false")


                    if ($("#ctl00_contenido_HQLNBB").val() != "true") {
                        $("textarea,input, select").each(function (index, element) { $(this).attr('disabled', true); });
                    }


                if ($("#ctl00_contenido_Ingreso").val() == "true") {
                    $("#linkingreso").show();
                }
                if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                    $("#linkinformacion").show();
                }
                if ($("#ctl00_contenido_Señas_Particulares").val() == "true") {
                    $("#linkseñas").show();
                }
                if ($("#ctl00_contenido_Familiares").val() == "true") {
                    $("#linkfamiliares").show();
                }
                if ($("#ctl00_contenido_Estudios_Criminologicos").val() == "true") {
                    $("#linkestudios").show();
                }
                if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                    $("#linkexpediente").show();
                }
                if ($("#ctl00_contenido_Adicciones").val() == "true") {
                    $("#linkadicciones").show();
                }
                $("#linkantropometria").show();
                $("#linkInfoDetencion").show();

                if (param != undefined) {
                    CargarDatos(param);
                }



                tabs();

            }
            function tabs() {
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

                    var $target = $(e.target);

                    $("#ctl00_contenido_MNBVCX").val($target.parent().index());

                    limpiar();
                    CargarDatos($('#<%= tracking.ClientID %>').val());

                });
            }
            $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
                var param = RequestQueryString("tracking");
                CargarDatosModal(param);
            });

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 443px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');

                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 216px)");
            });

        });

    </script>
</asp:Content>
