﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="AdministrarExamenMedico.aspx.cs" Inherits="Web.Application.AdministracionExamenmedico.AdministrarExamenMedico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
<li>Administrar examen médico </li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
   <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #dropdown {
            width: 439px;
        }
        
     
        /*input[type=text]:disabled{
            background-color:lightgrey;
            cursor:not-allowed;
        }*/
        /*input[type=datetime]:disabled{
            background-color:lightgrey;
            cursor:not-allowed;
        }*/
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    
<style>
        table {

          overflow-x:auto;
        }
        table td {
          word-wrap: break-word;
          max-width: 400px;
        }
        #dt_basic_observaciones td {
          white-space:inherit;
        }
</style>

    <div class="scroll">
        <!--
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
                Defensor público
            </h1>
        </div>
    </div>-->


    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a   class="btn btn-md btn-default  idadd"  id="idadd"><i class="fa fa-plus"></i> Agregar </a>&nbsp;
            
        </div>
    </div>
    <p></p>

    <section id="widget-grid" class="">
        <div class="row">            
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-detenidos-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Detenidos en existencia </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                        <%--<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">--%>
                            <table id="dt_basic" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th >Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <%--<th data-hide="phone,tablet">Justificación</th>--%>
                                        <th data-hide="phone,tablet">No. remisión</th>                                     
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Justificación</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
 
        <div id="photo-arrested" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src=" <%= ConfigurationManager.AppSettings["relativepath"]  %> #" alt="fotografía del detenido" />


                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="form-modal-justificacion"  role="dialog" data-backdrop="static" data-keyboard="true" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form ">
                        <div class="row">
                            <asp:Label ID="Label2" runat="server"></asp:Label>
                        </div>
                        <div class="row">
                            <fieldset> 
                                <section >
                                            <label style="color: dodgerblue" class="input">Detenido: <a style="color: red">*</a></label>                                                                                                                                    
                                            <select name="internoSelect" id="internoSelect" style="width: 100%" class="select2"></select>                                            
                                        </section>  
                                <section>
                                    <label style="color: dodgerblue" class="input">Justificación: <a style="color: red">*</a></label>
                                    <label class="input" id="contObs">
                                        <i class="icon-append fa fa-archive"></i>
                                        <input type="text" name="justificacion" id="justificacion" placeholder="Justificación" maxlength="512" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la justificación.</b>
                                    </label>
                                </section>                                                              
                            </fieldset>
                            
                        </div>                        
                       
                        <footer>
                                <a class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelObservacion"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                                <a class="btn btn-sm btn-default" id="btnSaveObservacion"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            </footer>
                    </div>
                </div>
            </div>
        </div>
    </div> 


     <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>  
     </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <input type="hidden" id="IdHistOb" />
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#form-modal-justificacion").is(":visible")) {
                    document.querySelector("#btnSaveObservacion").click();
                }
            }
        });
        $(document).ready(function () {            
            var rutaDefaultServer = "";

            getRutaDefaultServer();

            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "AdministrarExamenMedico.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }                                             
                    }
                });
            }
            
            function loadInternosAgregar(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "AdministrarExamenMedico.aspx/getInternos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#internoSelect');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Detenido]", "0"));
                        $.each(response.d, function (index, item) {
                            var str = item.Desc;
                            var nombreStr = "";
                            var res = str.split(" ");
                            for (var i = 2; i < res.length; i++) {
                                nombreStr = nombreStr + " " + res[i];
                            } 
                            Dropdown.append(new Option(nombreStr, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }




             var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
                 $("#ctl00_contenido_Label2").html("");
            }


      
             $("body").on("click", "#idadd", function () {


               
                 loadInternosAgregar("0");
                 limpiar();
                $("#form-modal-justificacion").modal('show');
                $("#form-modal-title").empty();
                $("#form-modal-title").html('<i class="fa fa-plus"></i> Agregar');
            });
       
            function limpiar()
            {
                 $('#justificacion').parent().removeClass("state-error");
                 $('#justificacion').parent().removeClass('state-success');
                 $('#justificacion').val("");

            }
            $("body").on("click", "#btnSaveObservacion", function () {

                if (validarCeldas())
                {
                    guardar();
                }

            });

            function AdministrarExamen() {                                
                var administrarExamen = {                    
                    DetalledetencionId: $("#internoSelect").val(),
                    Justificacion:$("#justificacion").val(),
                    TrackingId: "",
                    Id:$('#ctl00_contenido_hideid').val()
                    
                };
                return administrarExamen;
            }

             function guardar() {
                startLoading();
                var administrarExamen = AdministrarExamen();

                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    MovimientoCelda.TrackingId = param;
                }
               
                $.ajax({
                    type: "POST",
                    url: "AdministrarExamenMedico.aspx/Save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'administrarExamen': administrarExamen }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {

                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!",   "La información se registro correctamente" );                            
                           window.emptytable = false;
                        window.table.api().ajax.reload();
                           // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                          $('#main').waitMe('hide');
                            $("#form-modal-justificacion").modal("hide");
                            limpiarCampos();
                           
                        }
                        else {
                            $('#main').waitMe('hide');

                            if (resultado.alerta) {

                                ShowAlert("¡Atención!", resultado.mensaje);

                            }
                            else {
                                if (resultado.mensaje == "La celda no tiene lugares disponibles") {
                                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención! </strong>" +
                                        " " + resultado.mensaje + "</div>");
                                    setTimeout(hideMessage, hideTime);
                                    ShowAlert("Atencion!", resultado.mensaje);
                                }
                                else {
                                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                        "Algo salió mal. " + resultado.mensaje + "</div>");
                                    setTimeout(hideMessage, hideTime);
                                    ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                                }
                            }
                        }
                    }
                });
            }



            function validarCeldas() {
                var esvalido = true;

                if ($("#internoSelect").val() == "0") {
                    ShowError("Detenido", "El campo detenido es obligatorio");
                    $('#internoSelect').parent().removeClass('state-success').addClass("state-error");
                    $('#internoSelect').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#internoSelect').parent().removeClass("state-error").addClass('state-success');
                    $('#internoSelect').addClass('valid');
                }


              if ($("#justificacion").val() == ""|| $('#justificacion').val() == null)
                     {
                      ShowError("Justificación", "El campo justificación es obligatorio");
                    $('#justificacion').parent().removeClass('state-success').addClass("state-error");
                    $('#justificacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#justificacion').parent().removeClass("state-error").addClass('state-success');
                    $('#justificacion').addClass('valid');
                     }


              

                return esvalido;
            }

           

            $("#addObservacion").click(function () {
                $("#form-modal-observacion").modal('show');
            });

            pageSetUp();

            //Variables para tabla principal
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

         

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "AdministrarExamenMedico.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
               
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    null,
                    {
                        name: "Justificacion",
                        data: "Justificacion"
                    },
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },       {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("~/Content/img/avatars/male.png");
                                return '<div class="text-center">'+
                                    '<img id="avatar" class="img-thumbnail text-center" alt = "" src = "' + pathfoto + '" height = "10" width = "50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';"/>' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var estatus = "Pendiente";                               

                            return row.EstatusNombre;
                        }
                    },
                    //{
                    //    targets: 8,
                    //    orderable: false,
                    //    width: "400px",
                    //    render: function (data, type, row, meta) {
                    //        var edit = "edit";
                    //        var editar = "";
                    //        var color = "";
                    //        var txtestatus = "";
                    //        var icon = "";
                    //        var habilitar = "";                            

                    //        if (row.Habilitado) {
                    //            txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                    //        }
                    //        else {
                    //            txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                    //        }


                    //     //   if ($("#ctl00_contenido_KAQWPK").val() == "true")
                    //          //  editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                    //       // else if($("#ctl00_contenido_WERQEQ").val() == "true")
                    //           // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                    //        if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Proceso + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                            
                    //        var action5 = '<a class="btn datos btn-primary href="#"  id="datos" data-id="' + row.TrackingId + '" title="Datos personales"><i class="fa fa-bars"></i> Datos personales</a>&nbsp;';
                    //        var action6 = '';
                    //        var action7 = '';
                    //        //if (row.ObservacionId != 0) {
                            
                    //        if ($("#ctl00_contenido_KAQWPK").val() == "true")action6 = '<a class="btn observaciones bg-color-red txt-color-white href="#" data-id="' + row.TrackingId + '" title="Observaciones"><i class="glyphicon glyphicon-eye-open"></i> Notas médicas</a>&nbsp;';                                                       
                    //        action7 = '<a class="btn historial bg-color-yellow txt-color-white href="#" data-id="' + row.TrackingId + '" title="Historial de movimientos"><i class="fa fa-history fa-lg"></i> Historial</a>&nbsp;';
                    //        //}
                    //        //else {
                    //        //    action6 = '<a class="btn observaciones btn-primary disabled href="#" data-id="' + row.TrackingId + '" title="Observaciones">Observaciones</a>&nbsp;';
                    //        //}

                    //        return '';// action5 + action6 + action7;
                    //    }
                    //}
                ]

            });
          
            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");

                window.emptytable = true;
                window.table.api().ajax.reload();
            });                                                                                                                                  

            if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                $("#addentry").show();
            }                                                                                           

             $("body").on("click", ".datos", function () {
                var id = $(this).attr("data-id");
                CargarDatosPersonales(id);
                
            });

        
        });
    </script>
</asp:Content>