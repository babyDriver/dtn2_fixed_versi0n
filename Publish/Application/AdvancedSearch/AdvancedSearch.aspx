﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="AdvancedSearch.aspx.cs" Inherits="Web.Application.AdvancedSearch.AdvancedSearch" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Buscador avanzado</li>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #content {
            height: 600px;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
        div.scroll {
            overflow: auto;
            overflow-x: hidden;
        }
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
              <i class="glyphicon glyphicon-zoom-in fa-fw "></i>
              Buscador avanzado
            </h1>
        </div>
    </div>
    <asp:Label ID="Label1" runat="server"></asp:Label>
    <section id="widget-grid-sanciones" class="">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-sanciones-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                       <%-- <span class="widget-icon"></span> --%>              <i style="float:left; margin-top:10px; margin-left:5px;" class="glyphicon glyphicon-zoom-in fa-fw"></i>

                        <h2>Buscador avanzado</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div id="smart-form-register-entry" class="smart-form">
                            <fieldset>
                               
                                <div class="row">
                                    <section class="col col-3">
                                                    <label class="label">Fecha de inicio</label>
                                                    <label class="input">
                                                        <label class='input-group date' >
                                                        <input type="text" name="inicio" id="inicio" placeholder="Fecha de inicio" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha de inicio.</b>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                    </label>
                                                </section>
                                    <section class="col col-3">
                                                    <label class="label">Fecha final</label>
                                                    <label class="input">
                                                        <label class='input-group date' >
                                                        <input type="text" name="final" id="final" placeholder="Fecha final" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha final.</b>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                    </label>
                                                </section>
                                    <section class="col col-3">
                                        <a class="btn btn-primary Buscar" id="buscar"title="Volver al listado" style="margin-top:22px;padding:5px;height:35px"><i class="fa fa-search"></i>&nbsp;Buscar</a>
                                         
                                    </section>
                                    </div>
                                </fieldset>
                            </div>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th data-class="expand">Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th >Edad</th>
                                        <th>Alias</th>
                                        <th>No. remisión</th>
                                        <th>Folio llamada</th>
                                        <th>Unidad</th>
                                        <th>Corporación</th>
                                        <th>Evento</th>
                                        <th>Descripción hechos</th>
                                        <th>Motivo calificación</th>
                                        <th>Horas</th>
                                        <th>Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        </section>
    </div>
     <div id="informacionrapida-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información rápida </h4>
                </div>
                <div class="modal-body">
                    <%--<div class="smart-form ">
                            --%>
                        <div class="row">
                                <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Nombre: </label>
                            <div class="col-md-10">
                                <input class="form-control" id="Nombre" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">No. remisión: </label>
                            <div class="col-md-4">
                                <input id="Remision" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Registro: </label>
                            <div class="col-md-4">
                                <input id="Registro" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Domicilio: </label>
                            <div class="col-md-10">
                                <input id="Domicilio" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Edad: </label>
                            <div class="col-md-4">
                                <input id="Edad" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Alias: </label>
                            <div class="col-md-4">
                                <input id="Alias" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Estatus: </label>
                            <div class="col-md-10">
                                <input id="Estatus" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Llamada: </label>
                            <div class="col-md-4">
                                <input id="llamada" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Evento: </label>
                            <div class="col-md-4">
                                <input id="Evento" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Motivo del evento: </label>
                            <div class="col-md-10">
                                <input id="MotivoE1" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Descripción de los hechos: </label>
                            <div class="col-md-10">
                                <textarea id="Descripcion" class="form-control" disabled="disabled" rows="3" style="overflow:scroll"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Motivo de detención: </label>
                            <div class="col-md-10">
                                <input id="motivosDetencion" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Tipo de salida: </label>
                            <div class="col-md-4">
                                <input id="tipoSalida" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Fecha y hora de salida: </label>
                            <div class="col-md-4">
                                <input id="fechaSalida" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                        

                    </div>
                            <footer style="text-align:right">
                                <a class="btn  btn-default" " data-dismiss="modal" style="font-size:14px;width:110px" ><i class="fa fa-close"></i>&nbsp;Cerrar </a>                                                            
                            </footer>

                    

                   

                </div>
            </div>
        </div>
    </div>

    <div id="informaciondetallada-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información detallada</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Nombre: </label>
                            <div class="col-md-4">
                                <input class="form-control" id="Nombre2" disabled="disabled" type="text" />
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">No. remisión: </label>
                            <div class="col-md-4">
                                <input id="Remision2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Registro: </label>
                            <div class="col-md-4">
                                <input id="Registro2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Estatus: </label>
                            <div class="col-md-4">
                                <input id="Estatus2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Domicilio: </label>
                            <div class="col-md-10">
                                <input id="Domicilio2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Institución: </label>
                            <div class="col-md-10">
                                <input id="Institucion2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Edad: </label>
                            <div class="col-md-4">
                                <input id="Edad2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Sexo: </label>
                            <div class="col-md-4">
                                <input id="Sexo2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Alias: </label>
                            <div class="col-md-4">
                                <input id="Alias2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Hit biométricos: </label>
                            <div class="col-md-4">
                                <input id="HitBiometricos2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Llamada: </label>
                            <div class="col-md-4">
                                <input id="llamada2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Evento: </label>
                            <div class="col-md-4">
                                <input id="Evento2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                         <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Motivo evento: </label>
                            <div class="col-md-10">
                                <input id="Motivoe2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Descripción de los hechos: </label>
                            <div class="col-md-10">
                                <textarea id="Descripcion2" class="form-control" disabled="disabled" rows="3" style="overflow:initial" ></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Lugar de detención: </label>
                            <div class="col-md-10">
                                <input id="Lugar" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Colonia y C.P.: </label>
                            <div class="col-md-10">
                                <input id="ColoniaNumeroEvento2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Unidad: </label>
                            <div class="col-md-10">
                                <input id="Unidad" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Responsable: </label>
                            <div class="col-md-10">
                                <input id="Responsable" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Corporación: </label>
                            <div class="col-md-10">
                                <input id="Corporacion" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Certificados médicos: </label>
                            <div class="col-md-10">
                                <textarea id="CertificadosMedicos2" class="form-control" disabled="disabled" rows="3" style="overflow:initial" ></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Diagnóstico: </label>
                            <div class="col-md-10">
                                <textarea id="DiagnosticoMedico2" class="form-control" disabled="disabled" rows="6" style="overflow:initial" ></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Situación: </label>
                            <div class="col-md-4">
                                <input id="Situacion2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Calificó: </label>
                            <div class="col-md-4">
                                <input id="Califico2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Motivo de detención: </label>
                            <div class="col-md-10">
                                <input id="motivosDetencion2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Tipo de salida: </label>
                            <div class="col-md-4">
                                <input id="tipoSalida2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Salida: </label>
                            <div class="col-md-4">
                                <input id="fechaSalida2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#3276b1; text-align:right;">Horas: </label>
                            <div class="col-md-4">
                                <input id="HorasSalida2" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">

                        <img id="imgInfo2" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />

                    </div>
                    </div>
                    
                    <footer style="text-align:right">
                                <a class="btn btn-default"  data-dismiss="modal" style="font-size:14px;width:110px"   ><i class="fa fa-close"></i>&nbsp;Cerrar </a>                                                            
                            </footer>
                    
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="Hidden1" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $("body").on("click", "#buscar", function () {
                var valido = validarFechas();
                if (valido) {
                    window.table.api().ajax.reload();
                }
                
                
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }
           

            function limpiar1() {
                $("#Nombre").val("");
                $("#Remision").val("");
                $("#Registro").val("");
                $("#Domicilio").val("");
                $("#Edad").val("");
                $("#Alias").val("");
                $("#Estatus").val("");
                $("#llamada").val("");
                $("#Evento").val("");
                $("#Descripcion").val("");
                $("#Hora").val("");
                $("#fechaSalida").val("");
                $("#tipoSalida").val("");
                $("#motivosDetencion").val("");
            }
            function limpiar2() {
                $("#Nombre2").val("");
                $("#Remision2").val("");
                $("#Registro2").val("");
                $("#Estatus2").val("");
                $("#llamada2").val("");
                $("#Evento2").val("");
                $("#Descripcion2").val("");

                $("#Lugar").val("");
                $("#Unidad").val("");
                $("#Responsable").val("");
                $("#Corporacion").val("");
                $("#Hora2").val("");

                $("#Domicilio2").val("");
                $("#Edad2").val("");
                $("#Sexo2").val("");
                $("#Alias2").val("");
                $("#fechaSalida2").val("");
                $("#tipoSalida2").val("");
                $("#motivosDetencion2").val("");

                $("#ColoniaNumeroEvento2").val("");
                $("#HitBiometricos2").val("");
                $("#HorasSalida2").val("");
                $("#Situacion2").val("");
                $("#Califico2").val("");
                $("#CertificadosMedicos2").val("");
                $("#DiagnosticoMedico2").val("");
                $("#Institucion2").val("");

            }
            fechasiniciales();
            function fechasiniciales() {
                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth() + 1;
                var yyyy = hoy.getFullYear();

                3
                4
                5
                6
                7
                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (mm < 10) {
                    mm = '0' + mm;
                }
                var fecha1 = dd + "/" + mm + "/" + yyyy;
                var fecha2 = "01/01/" + yyyy;

                $("#ctl00_contenido_inicio").val(fecha2);
                $("#ctl00_contenido_final").val(fecha1);
            }


            $('#ctl00_contenido_inicio').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });
            $('#ctl00_contenido_final').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });


            function validarFechas() {
                var esvalido = true;
                if ($("#ctl00_contenido_inicio").val() == "" || $("#ctl00_contenido_inicio").val() == null) {
                    ShowError("Fecha inicial", "Por favor ingrese el valor en la fecha inicial.");
                    $('#ctl00_contenido_inicio').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_inicio').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_inicio').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_inicio').addClass('valid');
                }
                if ($("#ctl00_contenido_final").val() == "" || $("#ctl00_contenido_final").val() == null) {
                    ShowError("Fecha final", "Por favor ingrese el valor en la fecha final.");
                    $('#ctl00_contenido_final').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_final').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_final').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_final').addClass('valid');
                }
                var inicio = $("#ctl00_contenido_inicio").val() == null ? "" : $("#ctl00_contenido_inicio").val();
                var fin = $("#ctl00_contenido_final").val() == null ? "" : $("#ctl00_contenido_final").val();
                if (inicio != "") {
                    inicio = inicio.split("/");
                    inicio = inicio[2] + "/" + inicio[1] + "/" + inicio[0];

                }
                if (fin != "") {
                    fin = fin.split("/");
                    fin = fin[2] + "/" + fin[1] + "/" + fin[0];
                }
                if (inicio != "" & fin != "") {
                    if (fin < inicio) {
                        ShowError("Fecha final", "La fecha final deberá ser mayor que la fecha inicial.");
                        $('#ctl00_contenido_final').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_final').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_final').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_final').addClass('valid');
                    }
                }
                return esvalido;
            }
            function getDetallado(tracking) {
                $.ajax({
                    type: "POST",
                    url: "AdvancedSearch.aspx/getDetallado",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        trackingid: tracking,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var nombre = resultado.obj.Detenido;
                            var Remision = resultado.obj.Numero;
                            var Registro = resultado.obj.Reg;
                            var estatus = resultado.obj.Esta;
                            var llamada = resultado.obj.Call;
                            var evento = resultado.obj.Eve;
                            var Hechos = resultado.obj.Hechos;
                            var lugar = resultado.obj.place;
                            var unidad = resultado.obj.Unit;
                            var responsable = resultado.obj.Responsable;
                            var corporacion = resultado.obj.Corporacion;
                            var horas = resultado.obj.Horas;
                            var foto = resultado.obj.Picture;
                            var motivo = resultado.obj.MotivoEvento;
                            $("#Nombre2").val(nombre);
                            $("#Remision2").val(Remision);
                            $("#Registro2").val(Registro);
                            $("#Estatus2").val(estatus);
                            $("#llamada2").val(llamada);
                            $("#Evento2").val(evento);
                            $("#Descripcion2").val(Hechos);
                            $("#Motivoe2").val(motivo);
                            $("#Lugar").val(lugar);
                            $("#Unidad").val(unidad);
                            $("#Responsable").val(responsable);
                            $("#Corporacion").val(corporacion);
                            $("#Hora2").val(horas);
                            $('#imgInfo2').attr("src", foto);

                            $("#Domicilio2").val(resultado.obj.domicilio);
                            $("#Edad2").val(resultado.obj.edad);
                            $("#Sexo2").val(resultado.obj.sexo);
                            $("#Alias2").val(resultado.obj.alias);
                            $("#fechaSalida2").val(resultado.obj.fechaSalida);
                            $("#tipoSalida2").val(resultado.obj.tipoSalida);
                            $("#motivosDetencion2").val(resultado.obj.motivos);

                            $("#ColoniaNumeroEvento2").val(resultado.obj.coloniaNumeroEvento);
                            $("#HitBiometricos2").val(resultado.obj.tieneHit);
                            $("#HorasSalida2").val(resultado.obj.horasSalida);
                            $("#Situacion2").val(resultado.obj.situacion);
                            $("#Califico2").val(resultado.obj.juezCalifico);
                            $("#CertificadosMedicos2").val(resultado.obj.certificadosMedicos);
                            $("#DiagnosticoMedico2").val(resultado.obj.diagnosticoMedico);
                            $("#Institucion2").val(resultado.obj.institucion);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                    }
                });
            }
            function GetSencilla(tracking) {
                $.ajax({
                    type: "POST",
                    url: "AdvancedSearch.aspx/GetSencilla",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        Tracking: tracking,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var nombre = resultado.obj.Detenido;
                            var Remision = resultado.obj.Numero;
                            var Registro = resultado.obj.REg;
                            var estatus = resultado.obj.Esta;
                            var llamada = resultado.obj.Call;
                            var evento = resultado.obj.Eve;
                            var Hechos = resultado.obj.Hechos;
                            var horas = resultado.obj.Horas;
                            var foto = resultado.obj.Imagen;
                            var motivo = resultado.obj.MotivoEvento;
                            $("#Nombre").val(nombre);
                            $("#Remision").val(Remision);
                            $("#Registro").val(Registro);
                            $("#Estatus").val(estatus);
                            $("#llamada").val(llamada);
                            $("#Evento").val(evento);
                            $("#MotivoE1").val(motivo);
                            $("#Descripcion").val(Hechos);
                            $("#Hora").val(horas);
                            $('#imgInfo').attr("src", foto);
                            $("#Domicilio").val(resultado.obj.domicilio);
                            $("#Edad").val(resultado.obj.edad);
                            $("#Alias").val(resultado.obj.alias);
                            $("#fechaSalida").val(resultado.obj.fechaSalida);
                            $("#tipoSalida").val(resultado.obj.tipoSalida);
                            $("#motivosDetencion").val(resultado.obj.motivos);

                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                    }
                });
            }
            
            $("body").on("click", ".datos", function () {
                limpiar1();
                var tkg = $(this).attr("data-id");
                GetSencilla(tkg);
                $("#informacionrapida-modal").modal("show");
            });

            $("body").on("click", ".editarDetenido", function () {
                limpiar2();
                var tkg = $(this).attr("data-id");
                getDetallado(tkg);
                $("#informaciondetallada-modal").modal("show");
            });

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,

                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                //"order": [[6, "desc"], [7, "desc"]],
                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"] %>Application/AdvancedSearch/AdvancedSearch.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var inicio = $("#ctl00_contenido_inicio").val() == null ? "" : $("#ctl00_contenido_inicio").val();
                        var fin = $("#ctl00_contenido_final").val() == null ? "" : $("#ctl00_contenido_final").val();
                        if (inicio != "") {
                            inicio = inicio.split("/");
                            inicio = inicio[2] + "/" + inicio[1] + "/"+inicio[0];

                        }
                        if (fin != "") {
                            fin = fin.split("/");
                            fin = fin[2] + "/" + fin[1] + "/" + fin[0];
                        }
                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.Nombre = $("#inputNombre").val() == null ? "" : $("#inputNombre").val();
                        parametrosServerSide.Apellido_paterno = $("#inputApellido_paterno").val() == null ? "" : $("#inputApellido_paterno").val();
                        parametrosServerSide.Apellido_materno = $("#inputApellido_materno").val() == null ? "" : $("#inputApellido_materno").val();
                        parametrosServerSide.Edad = $("#inputEdad").val() == null ? "" : $("#inputEdad").val();
                        parametrosServerSide.Alias = $("#inputAlias").val() == null ? "" : $("#inputAlias").val();
                        parametrosServerSide.No_remision = $("#inputNo_remision").val() == null ? "" : $("#inputNo_remision").val();
                        parametrosServerSide.Folio_llamada = $("#inputFolio_llamada").val() == null ? "" : $("#inputFolio_llamada").val();
                        parametrosServerSide.Unidad = $("#inputUnidad").val() == null ? "" : $("#inputUnidad").val();
                        parametrosServerSide.Corporacion = $("#inputCorporacion").val() == null ? "" : $("#inputCorporacion").val();
                        parametrosServerSide.Evento = $("#inputEvento").val() == null ? "" : $("#inputEvento").val();
                        parametrosServerSide.Descripcion_hechos = $("#inputDescripcion_hechos").val() == null ? "" : $("#inputDescripcion_hechos").val();
                        parametrosServerSide.Motivo_calificacion = $("#inputMotivo_calificacion").val() == null ? "" : $("#inputMotivo_calificacion").val();
                        parametrosServerSide.Horas = $("#inputHoras").val() == null ? "" : $("#inputHoras").val();
                        parametrosServerSide.inicial = inicio;
                        parametrosServerSide.final = fin;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Edad",
                        data: "Edad"
                    },
                    {
                        name: "Alias",
                        data: "Alias"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "Folio",
                        data: "Folio"
                    },
                    {
                        name: "Unidad",
                        data: "Unidad"
                    },
                    {
                        name: "Corporacion",
                        data: "Corporacion"
                    },
                    {
                        name: "Evento",
                        data: "Evento"
                    },
                    {
                        name: "DescripcionHechos",
                        data: "DescripcionHechos"
                    },
                    {
                        name: "Motivo",
                        data: "Motivo"
                    },
                    {
                        name: "Horas",
                        data: "Horas"
                    },
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    },
                ],
                columnDefs: [
                    {
                        targets: 13,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var action5 = "";
                            var vacio = "";
                            action5 = '<div class="btn-group" data-id="' + row.trackingId + '"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Ver información <span class="caret"></span></button><ul class="dropdown-menu"><li><a class="btn-sm datos' + vacio + '"  href="#"  id="datos" data-id="' + row.trackingId + '" title="Información rápida"><i class="fa fa-bars"></i> Información rápida</a></li><li class="divider"></li>';



                            var action8 = '<li><a class="btn-sm editarDetenido" href="javascript:void(0);" id="editarDetenido" data-tracking="' + row.TrackingId + '" data-id="' + row.trackingId + '" title = "Editar detenido" > <i class="fa fa-gavel"></i> Información detallada</a></li><li class="divider"></li></ul></div>';

                            //return action2 + action3 + editar + action5 + action8 + action9 + action10 + action7;
                            return action5 + action8;

                            return "";
                        }
                    }
                ]

            });

            function CrearHeaderBusqueda() {
                var header = $("#dt_basic thead");
                var tr = '<tr>';
                $("#dt_basic thead th").each(function () {
                    var title = $(this).text();
                    if (title != "Acciones" && title != "") {
                        var titleNoSpaces = title.replace(" ", "_").replace(".", "").replace("ó", "o");
                        var td = '<th><input id="input' + titleNoSpaces + '" type="text" placeholder="' + title + '" /></th>';
                        tr += td;
                    }
                    else {
                        tr += '<th></th>';
                    }

                });
                tr += '</tr>';

                $(header).children('tr:first').after(tr);

                $("#dt_basic thead tr:last th").css("padding", "9px");
            }

            CrearHeaderBusqueda();
            $('#dt_basic thead tr:eq(1) th').each(function (i) {
                $('input', this).on('keyup change', function () {
                    console.log("datatable: " + $("#dt_basic").DataTable().column(i).search() + ", this.value: " + this.value);
                    $("#dt_basic").DataTable().ajax.reload();
                });
            });

            

        });
    </script>
</asp:Content>
