﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="casillero.aspx.cs" Inherits="Web.Application.Admin.casillero" %>


<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
  
    <li>Administración</li>
    <li>Pertenencia</li>
    <li>Casillero</li> 
</asp:Content> 

<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
            <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #content {
            height: 600px;
        }
        div.scroll {
            height: 590px;
            overflow: auto;
            overflow-x: hidden;
        }
    </style>
    <div class="scroll">
    <div class="row" id="addentry" >
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark" id="titlehead">
<%--                <i class="fa fa-th"></i>--%>
                  <img style="width:40px; height:40px; margin-bottom:5px" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/Locker.png" />
                Catálogo de casillero
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
       </div>
    </div>
    <p></p>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-casillero-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-casillero-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>

<%--                       <span class="widget-icon"><i class="fa fa-th"></i></span>--%>
<%--                        <span class="widget-icon"><i class="fa fa-lock"></i></span>--%>
                <img style="width:25px; height:25px; margin-bottom:5px; float:left; margin:5px 0px 0px 5px;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/LockerWhite.png" />

                        <h2 style="margin-left:5px" id="titlegrid">Casilleros</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th data-class="expand">Nombre</th>
                                        <th data-hide="phone,tablet">Capacidad</th>
                                        <th data-hide="phone,tablet">Descripción</th>
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
    <div class="modal fade" id="form-modal" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label style="color:dodgerblue" class="input">Casillero <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="nombre" runat="server" id="itemnombre" class="alphanumeric alptext" placeholder="Casillero" maxlength="255" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el casillero</b>
                                    </label>
                                </section>
                                <section>
                                    <label style="color:dodgerblue" class="input" >Capacidad <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="capacidad" runat="server" id="itemcapacidad" class="number alptext" placeholder="Capacidad" maxlength="9" />
                                        <b  class="tooltip tooltip-bottom-right">Ingrese la capacidad</b>
                                    </label>
                                </section>
                                <section>
                                    <label style="color:dodgerblue">Descripción <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="descripcion" runat="server" id="descripcion" class="alphanumeric alptext" placeholder="Descripción" maxlength="200" />
                                        <b   class="tooltip tooltip-bottom-right">Ingrese la descripción</b>
                                    </label>
                                </section>
                             
                            </div>
                        </fieldset>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete-modal" class="modal fade" tabindex="-1" data-keyboard="true" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar 
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro <strong><span id="itemeliminar"></span></strong>&nbsp;será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-keyboard="true" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="historycasillero-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcioncasillero"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-sm-18">
                        <table id="dt_basichistorycas" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
									<th data-hiden="tablet,fablet,phone">Nombre</th>
									<th>Disponible</th>
									<th>Movimiento</th>
                                    <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                    <th>Fecha movimiento</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                   </div>
                </div>
            </div>
    </div>
 </div>
    <input type="hidden" id="IdCasillero" />
     <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#form-modal").is(":visible")) {
                    document.getElementsByClassName("save")[0].click();
                }
            }
        });
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic_history = undefined;
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            var breakpointHistoryDefinition = {
                tablet: 1024,
                phone: 480
            };
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

           

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "casillero.aspx/getcasillero",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;        //window.emptytable;

                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Capacidad",
                        data: "Capacidad"
                    },
                    {
                        name: "Descripcion",
                        data: "Descripcion"
                    },
                    null,
                    null
                ],
                columnDefs: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 5,
                        name: "Activo",
                        render: function (data, type, row, meta) {
                            return row.Activo ? "Habilitado" : "Deshabilitado"
                        }
                    },
                    {
                        targets: 6,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "";
                            var editar = "";
                            var habilitar = "";
                            if (row.Activo) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                edit = "edit";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                edit= "disabled";
                            }

                            editar = '<a class="btn btn-primary btn-circle ' + edit + ' "href="javascript:void(0);" data-value = "' + row.Nombre + '" data-id="' + row.Id + '" data-capacidad="' + row.Capacidad + '" data-tracking="' + row.TrackingId + '" data-descripcion="' + row.Descripcion + '" data-activo="' + row.Activo + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Nombre + '" title="' + txtestatus + '" data-id="' + row.Id + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';


                            return editar +
                                habilitar + '<a name="history" class="btn btn-default btn-circle historialcas" href="javascript:void(0);" data-value="' + row.Nombre + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                        }
                    }
                ]
            });
            window.emptytableHistory = true;
            window.tableHistory = $("#dt_basichistorycas").dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[4, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_history) {
                        responsiveHelper_dt_basic_history = new ResponsiveDatatablesHelper($("#dt_basichistorycas"), breakpointHistoryDefinition);
                    }
                    
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_history.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_history.respond();
                    $("#dt_basichistorycas").waitMe("hide");
                },
                ajax: {
                    type: "POST",
                    url: "casillero.aspx/GetCasilleroLog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosserverside) {
                        $("#dt_basichistorycas").waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var centroid = $('#IdCasillero').val();
                        parametrosserverside.centroid = centroid;
                        parametrosserverside.todoscancelados = false;
                        parametrosserverside.emptytable = emptytableHistory;
                        return JSON.stringify(parametrosserverside);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        ordertable: false

                    },
                    
                    {
                        name: "Disponible",
                        data: "Disponible",
                        ordertable: false

                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        ordertable: false
                    },

                    {
                        name: "Creadopor",
                        data: "Creadopor",
                        ordertable: false
                    }
                    ,
                    {
                        name: "Fec_Movto",
                        data: "Fec_Movto",
                        ordertable: false
                    }
                ],
            });
            $("body").on("click", ".historialcas", function () {
                var id = $(this).attr("data-id");
                $("#IdCasillero").val(id);
                var descr = $(this).attr("data-value");
                $("#descripcioncasillero").text(" del casillero " + descr);
                $("#historycasillero-modal").modal("show");
                window.emptytableHistory = false;
                window.tableHistory.api().ajax.reload();

            })
            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "casillero.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                data.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", data.mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });


            $("body").on("click", ".add", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_itemnombre").val("");
                $("#ctl00_contenido_itemcapacidad").val("");
                $("#ctl00_contenido_itemcapacidad").attr('disabled',false);
                $("#ctl00_contenido_descripcion").val("");
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#form-modal-title").empty();
                $('#habilitado').prop('checked', false);
                $("#form-modal-title").html("<i class='fa fa-pencil'+></i> Agregar Registro"); 
                $("#form-modal").modal("show");
            });

            $("body").on("click", ".edit", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                var capacidad = $(this).attr("data-capacidad");
                var descripcion = $(this).attr("data-descripcion");
                var habilitado = $(this).attr("data-habilitado");

                $("#btnsave").attr("data-id", id);
                $("#btnsave").attr("data-tracking", tracking);
                $("#btnsave").attr("data-habilitado", habilitado);
                $("#ctl00_contenido_itemnombre").val($(this).attr("data-value"));
                $("#ctl00_contenido_itemcapacidad").val($(this).attr("data-capacidad"));
                $("#ctl00_contenido_descripcion").val($(this).attr("data-descripcion"));
                $("#itemhabilitado").val($(this).attr("data-habilitado"));
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'+></i> Editar Registro"); 
                $("#form-modal").modal("show");
            });

            function limpiar() {
                $('#ctl00_contenido_itemcapacidad').parent().removeClass('state-success');
                $('#ctl00_contenido_itemcapacidad').parent().removeClass("state-error");
                $('#ctl00_contenido_itemnombre').parent().removeClass('state-success');
                $('#ctl00_contenido_itemnombre').parent().removeClass("state-error");
                $('#ctl00_contenido_descripcion').parent().removeClass('state-success');
                $('#ctl00_contenido_descripcion').parent().removeClass("state-error");
            }
            
            $("body").on("click", ".save", function () {
                var id = $("#btnsave").attr("data-id");
                var tracking = $("#btnsave").attr("data-tracking");
                var habilitado = $("#btnsave").attr("data-habilitado");

                if (id == "") {
                    habilitado = true;
                }
              
                datos = [
                      id = id,
                      tracking = tracking,
                      nombre = $("#ctl00_contenido_itemnombre").val(),
                      capacidad = $("#ctl00_contenido_itemcapacidad").val(),
                      peligrosidad = $("#ctl00_contenido_descripcion").val()

                ];

                if (validar()) {
                    Save(datos);
                }

            });

            function validar() {
                var esvalido = true;
                if ($("#ctl00_contenido_itemnombre").val().split(" ").join("") == "") {
                    ShowError("Casillero", "El casillero es obligatorio.");
                    $('#ctl00_contenido_itemnombre').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemnombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemnombre').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemnombre').addClass('valid');
                }

                if ($("#ctl00_contenido_itemcapacidad").val().split(" ").join("") == "") {
                    ShowError("Capacidad", "La capacidad es obligatoria.");
                    $('#ctl00_contenido_itemcapacidad').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemcapacidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemcapacidad').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemcapacidad').addClass('valid');
                }

                if ($("#ctl00_contenido_itemcapacidad").val() == "0") {
                    ShowError("Capacidad", "La capacidad debe ser mayor a 0.");
                    $('#ctl00_contenido_itemcapacidad').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemcapacidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemcapacidad').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemcapacidad').addClass('valid');
                }

                if ($("#ctl00_contenido_descripcion").val().split(" ").join("") == "") {
                    ShowError("Descripción", "La descripción es obligatoria.");
                    $('#ctl00_contenido_descripcion').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_descripcion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_descripcion').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_descripcion').addClass('valid');
                }

                return esvalido;
            }

            function Save(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Admin/casillero.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modal").modal("hide");
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');


                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            
            $('#ctl00_contenido_itemcapacidad').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
            });

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>