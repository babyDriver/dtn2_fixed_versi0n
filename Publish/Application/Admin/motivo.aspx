﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="motivo.aspx.cs" Inherits="Web.Application.Admin.motivo" %>



<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li >Administración</li>
    <li>Motivo de detención</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        .wrapping {
            /* These are technically the same, but use both */
            overflow-wrap: break-word;
            word-wrap: break-word;

            -ms-word-break: break-all;
            /* This is the dangerous one in WebKit, as it breaks things wherever */
            word-break: break-all;
            /* Instead use this non-standard one: */
            word-break: break-word;
    
            /* Adds a hyphen where the word breaks, if supported (No Blink) */
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
        #dt_basic tbody tr td:nth-child(3), #dt_basic tbody tr td:nth-child(5), #dt_basic tbody tr td:nth-child(6), #dt_basic tbody tr td:nth-child(7) {
            text-align: right;
        }
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
<%--                <i class="fa fa-book fa-fw "></i>--%>

        <img style="width:40px; height:40px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/HandCuffs.png" />

                Catálogo de motivo detención
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
        </div>
    </div>
    
    <p></p>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-motivo-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" ">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar clientes </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="select">
                                                <select name="peril" id="perfil" runat="server">
                                                    <option value="0">[Perfil]</option>
                                                </select>
                                                <i></i>
                                            </label>
                                        </section>
                                    
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="nombre" id="nombre" runat="server" placeholder="Nombre" maxlength="256"/>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-motivo-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
<%--                        <span class="widget-icon"><i class="fa fa-group"></i></span>--%>
                 <img style="width:30px; height:30px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/HandCuffsWhite.png" />
                        <h2>Motivos de detención </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%" >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="text-align:center" data-class="expand">Motivo </th>
                                        <th style="text-align:center" data-hide="phone,tablet">Artículo    </th>  
                                        <th style="text-align:center" data-hide="phone,tablet">Tipo</th>  
                                        <th data-hide="phone,tablet">Multa mínima</th> 
                                        <th data-hide="phone,tablet">Multa máxima</th> 
                                        <th data-hide="phone,tablet">Horas de arresto</th> 
                                        <th data-hide="phone,tablet">Acciones</th>                                       
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
    <div id="blockuser-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"  >Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de <strong>&nbsp<span id="usernameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="historymotivo-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorialmotivo"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-sm-12">
                        <table id="dt_basichistorymov" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th data-hiden="tablet,fablet,phone">Motivo</th>
									<th>Movimiento</th>
                                    <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                    <th>Fecha movimiento</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                        </div>
                </div>
            </div>
    </div>
 </div>
    <div class="modal fade" id="form-modal" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title">  <%--///////////////////////////////////////////////////////////--%>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="input" style="color:dodgerblue">Motivo detención <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <textarea id="motivo" name="motivo" placeholder="Motivo detención" maxlength="1000" class="form-control alptext" rows="5"></textarea>
                                    </label>
                                </section>
                                <section>
                                    <label class="input" style="color:dodgerblue">Descripción <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="textarea" name="descripcion" id="descripcion" placeholder="Descripción" maxlength="256" class="alphanumeric"/>
                                        <b class="tooltip tooltip-bottom-right" >Ingrese la descripción.</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input" style="color:dodgerblue">Artículo </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="articulo" id="articulo" placeholder="Artículo" maxlength="256" class="alphanumeric"/>
                                        <b class="tooltip tooltip-bottom-right" >Ingrese el artículo.</b>
                                    </label>
                                </section> 
                               <section >
                                            <label class="label" style="color:dodgerblue">Tipo<a style="color:red">*</a></label>
                                            <label class="select">
                                                <select name="Tipo_Motivo" id="Tipo_Motivo">                                                
                                                </select>
                                                <i></i>
                                            </label>
                                        </section>
                                <section>
                                    <label id="divisacontrato"></label>
                                </section>
                                <section id="multamin">
                                    <label  class="input" style="color:dodgerblue">Multa mínima  <a  style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-hashtag"></i>
                                        <input  title="" type ="text" name="multaMinimos" pattern="^[0-9]*$" maxlength="9" id="multaMinimos"/>
                                        <b  class="tooltip tooltip-bottom-right" >Ingrese la multa mínima.</b>
                                    </label>
                                </section>
                                <section id="multamax">
                                    <label class="input" style="color:dodgerblue">Multa máxima <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-hashtag"></i>
                                        <input  title="" type ="text" name="multaMaximos" pattern="^[0-9]*$" maxlength="9" id="multaMaximos"/>
                                        <b   class="tooltip tooltip-bottom-right" >Ingrese la multa máxima.</b>
                                    </label>
                                </section>
                                <section id="hora">
                                    <label  class="input" style="color:dodgerblue">Horas de arresto <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-hashtag"></i>
                                        <input title="" type="text" name="horaArresto" id="horaArresto" placeholder="Horas de arresto"  pattern="^[0-9]*$" maxlength="9" class="alphanumeric"/>
                                        <b  class="tooltip tooltip-bottom-right" >Ingrese la(s) hora(s) de arresto.</b>
                                    </label>
                                </section>
                                </div>
                        </fieldset>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hideid" runat="server" />
    <input type="hidden" id="hide"   runat="server" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="abc" runat="server" value="" />
    <input type="hidden" id="tipodivisacontrato" runat="server" value="" /> 
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="IdHistoryMov" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">

        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#form-modal").is(":visible")) {
                    document.getElementsByClassName("save")[0].click();
                }
            }
        });
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $('.input-number').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            });

            //$("#multaMaximos").on({
            //    "focus": function (event) {
            //        $(event.target).select();
            //    },
            //    "keyup": function (event) {
            //        $(event.target).val(function (index, value) {
            //            return value.replace(/\D/g, "")
            //            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            //                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            //        });
            //    }
            //});

            //$("#multaMinimos").on({
            //    "focus": function (event) {
            //        $(event.target).select();
            //    },
            //    "keyup": function (event) {
            //        $(event.target).val(function (index, value) {
            //            return value.replace(/\D/g, "")
            //            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            //                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            //        });
            //    }
            //});


            $(document).on('keydown', 'input[pattern]', function(e){
              var input = $(this);
              var oldVal = input.val();
              var regex = new RegExp(input.attr('pattern'), 'g');

              setTimeout(function(){
                var newVal = input.val();
                if(!regex.test(newVal)){
                  input.val(oldVal); 
                }
              }, 0);
            });
            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(0).addClass('strikeout');
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "motivo.aspx/getMotivos",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });                       
                        parametrosServerSide.emptytable = window.emptytable;
                        return JSON.stringify(parametrosServerSide);
                    }
                   
                },
                columns: [
                    null,
                    null,
                    {
                        name: "Articulo",
                        data: "Articulo"
                    }, 
                    {
                        name: "Tipo_Motivo",
                        data: "Tipo_Motivo"
                    },
                    {
                        name: "MultaMinimo",
                        data: "MultaMinimo"
                    }, 
                    {
                        name: "MultaMaximo",
                        data: "MultaMaximo"
                    }, 
                    {
                        name: "HorasdeArresto",
                        data: "HorasdeArresto"
                    }, 
                    null
                ],
                 
              
                columnDefs: [
                {
                        targets: 0,
                        data:"Id",
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "Motivo",
                        targets: 1,
                        width: 400,
                        render: function (data, type, row, meta) {
                            return "<div class='wrapping'>" + row.Motivo + "</div>";
                        }
                    },
                    {
                        targets: 4,
                            orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.MultaMinimo > 0) return "<div class='text-right'>" + (row.MultaMinimo) + "</div>";
                            else return "0";
                                    
                        }
                    },
                    {
                        targets: 5,
                            orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.MultaMaximo > 0) return "<div class='text-right'>" + (row.MultaMaximo) + "</div>";
                            else return "<div class='text-right'>0</div>";
                                    
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "edit";
                            var editar = '<a class="btn btn-primary btn-circle ' + edit + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            var habilitar = "";
                            if (!row.Habilitado) {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                                
                            }
                            else {
                                
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-tracking="' + row.TrackingId + '" data-motivo="' + row.Motivo + '" data-descripcion="' + row.Descripcion + '" data-articulo="' + row.Articulo + '" data-multaMinimo="' + row.MultaMinimo + '" data-multaMaximo="' + row.MultaMaximo + '" data-horas="' + row.HorasdeArresto + '"data-Tipo_MotivoId="'+row.Tipo_MotivoId +'" title = "Editar" > <i class="glyphicon glyphicon-pencil"></i></a >&nbsp; ';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-value="' + row.Motivo + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                           
                            return editar + habilitar + '<a name="history" class="btn btn-default btn-circle historialmov" href="javascript:void(0);" data-value="' + row.Motivo + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>'
                               ;
                        }
                    }
                ]
              
            });
            var responsiveHelper_dt_basichistorymov = undefined;
            var breakpointhisyorymovDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistorymov = true;
            window.tablehisyorymov = $("#dt_basichistorymov").dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[3, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistorymov) {
                        responsiveHelper_dt_basichistorymov = new ResponsiveDatatablesHelper($("#dt_basichistorymov"), breakpointhisyorymovDefinition);
                    }

                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistorymov.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistorymov.respond();
                    $("#dt_basichistorymov").waitMe("hide");
                },
                ajax: {
                    type: "POST",
                    url: "motivo.aspx/Getmotivoslog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosserverside) {
                        $("#dt_basichistorymov").waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var centroid = $('#IdHistoryMov').val() || "0";
                        console.log(centroid);
                        parametrosserverside.centroid = centroid;
                        parametrosserverside.todoscancelados = false;
                        parametrosserverside.emptytable = emptytablehistorymov;
                        return JSON.stringify(parametrosserverside);
                    }
                },
                columns: [
                    {
                        name: "Motivo",
                        data: "Motivo",
                        ordertable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        ordertable: false
                    },

                    {
                        name: "Creadopor",
                        data: "Creadopor",
                        ordertable: false
                    }
                    ,
                    {
                        name: "Fec_Movto",
                        data: "Fec_Movto",
                        ordertable: false
                    }
                ],
            });
            $ ("body").on("click", ".historialmov", function () {
                var id = $(this).attr("data-id");
                $("#IdHistoryMov").val(id);
                var descripcion = $(this).attr("data-value");
                $("#descripcionhistorialmotivo").text(descripcion);

                $("#historymotivo-modal").modal("show");
                window.emptytablehistorymov = false;
                window.tablehisyorymov.api().ajax.reload(); 
            });
            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");
            });

            $("body").on("click", ".blockuser", function () {
                var usernameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockuser-modal").modal("show");
            });   

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingid = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "motivo.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: trackingid,
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockuser-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                data.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", data.mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del usuario. . Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });
            init();
            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                    $("#divisacontrato").text($("#ctl00_contenido_tipodivisacontrato").val());
                }

            }

            $("body").on("click", ".edit", function () {
                limpiar();   
                
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");            
           
                $("#btnsave").attr("data-id", id);                 
                
                $("#btnsave").attr("data-tracking", tracking);
                $("#motivo").val($(this).attr("data-motivo"));
                $("#descripcion").val($(this).attr("data-descripcion"));
                $("#articulo").val($(this).attr("data-articulo"));
                $("#multaMinimos").val($(this).attr("data-multaMinimo"));
               $("#Tipo_Motivo").val($(this).attr("data-Tipo_MotivoId"));
                $("#Tipo_Motivo").change();
                    $("#multaMaximos").val($(this).attr("data-multaMaximo"));
                
                $("#horaArresto").val($(this).attr("data-horas"));
                $('#ctl00_contenido_hideid').val(id);
                $('#ctl00_contenido_hide').val(tracking);
                $("#form-modal").modal("show");
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar registro");
                //CargarContrato(tracking);  
            });

            $("body").on("click", ".add", function () {
                limpiar();
                $("#motivo").val("");
                $("#descripcion").val("");
                $("#articulo").val("");
                $("#multa").val("");
                $("#horaArresto").val("");
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                $("#form-modal").modal("show");
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar registro");
                $("#multaMinimos").val("");
                $("#multaMaximos").val("");
                $("#Tipo_Motivo").val("0");
            });

            function limpiar() {

                $('#motivo').parent().removeClass('state-success');
                $('#motivo').parent().removeClass("state-error");
                $('#articulo').parent().removeClass('state-success');
                $('#articulo').parent().removeClass("state-error");
                $('#multa').parent().removeClass('state-success');
                $('#multa').parent().removeClass("state-error");
                $('#horaArresto').parent().removeClass('state-success');
                $('#horaArresto').parent().removeClass("state-error");
                $('#descripcion').parent().removeClass('state-success');
                $('#descripcion').parent().removeClass("state-error");
                $("#multaMinimos").parent().removeClass('state-error');
                $("#multaMinimos").parent().removeClass('state-success');
                $('#multaMaximos').parent().removeClass('state-success');
                $('#multaMaximos').parent().removeClass("state-error");
                $("#Tipo_Motivo").parent().removeClass('state-success');
                $("#Tipo_Motivo").parent().removeClass('state-error');
            }

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    guardar();
                }
            });

            function validar() {
                var esvalido = true;
                
               
               
                
                if ($("#motivo").val() == "") {
                    ShowError("Motivo", "El motivo de detención es obligatorio.");
                    $('#motivo').parent().removeClass('state-success').addClass("state-error");
                    $('#motivo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#motivo').parent().removeClass("state-error").addClass('state-success');
                    $('#motivo').addClass('valid');
                }

                if ($("#descripcion").val() == "") {
                    ShowError("Descripción", "El campo descripción es obligatorio.");
                    $('#descripcion').parent().removeClass('state-success').addClass("state-error");
                    $('#descripcion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#descripcion').parent().removeClass("state-error").addClass('state-success');
                    $('#descripcion').addClass('valid');
                }
              
                if ( $("#Tipo_Motivo").val() =="0") 
                {
                ShowError("Tipo", "El tipo de motivo es obligatorio.")
                    $("#Tipo_Motivo").parent().removeClass("state-success").addClass('state-error');;
                    $("#Tipo_Motivo").removeClass('valid');
                    esvalido = false;
                }
                else {
                    $("#Tipo_Motivo").parent().removeClass('state-error').addClass("state-success");
                    $("#Tipo_Motivo").addClass('valid');
                }

                if ($("#Tipo_Motivo").val() ==  "1") {

                    var horas = 72;


                     if ( $('#horaArresto').val() == "0") {
                        ShowError("Hora de arresto", "Las horas de arresto deben ser mayor a las 0 horas.")
                        $('#horaArresto').parent().removeClass('state-success').addClass("state-error");
                        $('#horaArresto').removeClass('valid');
                        esvalido = false;
                    }
                    
                   
                    if (isNaN($('#horaArresto').val()) == true || $('#horaArresto').val() == "" ) {
                        ShowError("Hora de arresto", "Horas de arresto es obligatorio para este tipo de motivo.")
                        $('#horaArresto').parent().removeClass('state-success').addClass("state-error");
                        $('#horaArresto').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        if ($('#horaArresto').val() > horas) {
                            ShowError("Hora de arresto", "No se pudo guardar el Motivo de detención, El máximo de horas que puede tener un delito es 72 horas.")
                            $('#horaArresto').parent().removeClass('state-success').addClass("state-error");
                            $('#horaArresto').removeClass('valid');
                            esvalido = false;

                        }
                        else {
                            $('#horaArresto').parent().removeClass("state-error").addClass('state-success');
                            $('#horaArresto').addClass('valid');
                        }

                        
                    }
                    
                }
                
                if ($("#Tipo_Motivo").val()== "2") {
                    horas = 36;
          
                    if ($("#multaMinimos").val() == "") {
                        ShowError("Multa mínima", "La multa mínima es obligatoria para este tipo de motivo.");
                        $('#multaMinimos').parent().removeClass('state-success').addClass("state-error");
                        $('#multaMinimos').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#multaMinimos').parent().removeClass("state-error").addClass('state-success');
                        $('#multaMinimos').addClass('valid');
                    }

                    if ($("#multaMaximos").val() == "") {
                        ShowError("Multa máxima", "La multa máxima es obligatoria para este tipo de motivo.");
                        $('#multaMaximos').parent().removeClass('state-success').addClass("state-error");
                        $('#multaMaximos').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#multaMaximos').parent().removeClass("state-error").addClass('state-success');
                        $('#multaMaximos').addClass('valid');
                    }
                     if ( $('#horaArresto').val() == "0") {
                        ShowError("Hora de arresto", "Las horas de arresto deben ser mayor a las 0 horas.")
                        $('#horaArresto').parent().removeClass('state-success').addClass("state-error");
                        $('#horaArresto').removeClass('valid');
                        esvalido = false;
                    }
                    if (isNaN($('#horaArresto').val()) == true || $('#horaArresto').val() == "" ) {
                        ShowError("Hora de arresto", "Horas de arresto es obligatorio para este tipo de motivo.")
                        $('#horaArresto').parent().removeClass('state-success').addClass("state-error");
                        $('#horaArresto').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        if ($('#horaArresto').val() > horas) {
                            ShowError("Hora de arresto", "No se pudo guardar el Motivo de detención, El máximo de horas que puede tener una falta es 36 horas.")
                            $('#horaArresto').parent().removeClass('state-success').addClass("state-error");
                            $('#horaArresto').removeClass('valid');
                            esvalido = false;
                        }
                        else {
                            $('#horaArresto').parent().removeClass("state-error").addClass('state-success');
                            $('#horaArresto').addClass('valid');
                        }
                        
                    }
                    
                }
               
               

                //else {
                //    $("#motivo").select2({ containerCss: { "border-color": "#bdbdbd" } });
                //}

                //if ($("#articulo").val() == "") {
                //    ShowError("Artículo", "El artículo es obligatorio.");
                //    $('#articulo').parent().removeClass('state-success').addClass("state-error");
                //    $('#articulo').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#articulo').parent().removeClass("state-error").addClass('state-success');
                //    $('#articulo').addClass('valid');
                //}                

              
                

               
                
                return esvalido;
            }
            function Desabilidarbotones() {
                if ($("#Tipo_Motivo").val() == "0") {
                    //$('#multaMinimos').prop('disabled', true);
                    //$('#multaMaximos').prop('disabled', true);
                    //$('#horaArresto').prop('disabled', true);
                    $('#multamin').show();
                    $('#multamax').show();
                    $('#hora').show();
                }
                $("#Tipo_Motivo").change(function () {
                    if ($("#Tipo_Motivo").val() == "0") {
                        //$('#multaMinimos').prop('disabled', true);
                        //$('#multaMaximos').prop('disabled', true);
                        //$('#horaArresto').prop('disabled', true);
                        $("#divisacontrato").show();
                        $('#multamin').show();
                        $('#multamax').show();
                        $('#hora').show();
                    }
                    if ($("#Tipo_Motivo").val() == "1") {
                        
                        //$('#multaMinimos').prop('disabled', true);
                        //$('#multaMaximos').prop('disabled', true);
                        //$('#horaArresto').prop('disabled', false);
                        $("#divisacontrato").hide();
                        $('#multamin').hide();
                        $('#multamax').hide();
                        $('#hora').show();
                    }
                    if ($("#Tipo_Motivo").val() == "3") {
                        
                        //$('#multaMinimos').prop('disabled', true);
                        //$('#multaMaximos').prop('disabled', true);
                        //$('#horaArresto').prop('disabled', true);
                        $("#divisacontrato").hide();
                        $('#multamin').hide();
                        $('#multamax').hide();
                        $('#hora').hide();

                        
                        

                    }
                    if ($("#Tipo_Motivo").val() == "2") {

                        //$('#multaMinimos').prop('disabled', false);
                        //$('#multaMaximos').prop('disabled', false);
                        //$('#horaArresto').prop('disabled', false);
                        $("#divisacontrato").show();
                        $('#multamin').show();
                        $('#multamax').show();
                        $('#hora').show();
                    }
                });

            }
            Desabilidarbotones();
            function CargarTipos_Motivos(set) {
                $.ajax({

                    type: "POST",
                    url: "motivo.aspx/GetTipoCombo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        item: set
                    }),
                    success: function (response) {
                        var Dropdown = $("#Tipo_Motivo");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Tipo ]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            //function CargarTipos_Motivos(set) {
            //    $.ajax({

            //        type: "POST",
            //        url: "motivo.aspx/GetTipoCombo",
            //contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        cache: false,
            //        data: JSON.stringify({

            //            item: set
            //        }),
            //        success: function (response) {
            //            var DropDown = $("#Tipo_Motivo");
            //            DropDown.empty();
            //            DropDown.append(new Option("[Tipo]","0"));
            //            $.each(response.d, function (index, item) {
            //                DropDown.append(new Option(item.Desc, item.Id));
            //            });
            //            if (set != "") {
            //                DropDown.val(set);
            //                DropDown.trigger("change.select2");
            //            }
            //        },
            //        error: function () {
            //            ShowError("Error!", "No fue posible cargar la lista de tipos. si el problema persiste, por favor, consulte al personal de soporte tecnico.");
            //        }
            //    });

            //}
            CargarTipos_Motivos("0");
            function guardar() {
                startLoading();
                
                var contrato = ObtenerValores();
                $.ajax({
                    type: "POST",
                    url: "motivo.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'contrato': contrato }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modal").modal("hide");
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del motivo de detención se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del  motivo de detención se  " + resultado.mensaje + " correctamente.");                            
                            
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal",  resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            function CargarContrato(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "motivo.aspx/getContract",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;                        
                         $("#motivo").val(data.Motivo);
                         $("#descripcion").val(data.Descripcion);                         
                         $("#articulo").val(data.Articulo); 
                         $("#multa").val(data.Multa); 
                         $("#horaArresto").val(data.horaArresto); 
                         $('#ctl00_contenido_hideid').val(data.Id);
                        
                         $('#ctl00_contenido_hide').val(data.TrackingId);
                        
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function ObtenerValores() { 
                if ($('#Tipo_Motivo').val()=="1") {
                    var multamin = 0;
                    var multamax = 0;
                    var horas = $('#horaArresto').val();
                }
                 if ($('#Tipo_Motivo').val() == "2") {
                     var multamin = $('#multaMinimos').val();
                     var multamax = $('#multaMaximos').val();
                     var horas = $('#horaArresto').val();
                }
                if ($('#Tipo_Motivo').val() == "3") {
                    var multamin = 0;
                    var multamax = 0;
                    var horas = 0;
                }
                var contrato = {                    
                    Motivo: $('#motivo').val(),
                    Descripcion: $('#descripcion').val(),
                    Articulo: $('#articulo').val(),       
                    
                    MultaMinimo: multamin,
                    MultaMaximo: multamax,
                    horaArresto: horas,
                    Id: $('#ctl00_contenido_hideid').val(),
                    TrackingId: $('#ctl00_contenido_hide').val(),
                    Tipo_Motivo: $('#Tipo_Motivo').val()
                };
                return contrato;
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
        



    </script>
</asp:Content>

