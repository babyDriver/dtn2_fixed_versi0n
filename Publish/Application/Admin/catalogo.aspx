﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="catalogo.aspx.cs" Inherits="Web.Application.Admin.catalogo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
        <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
 
    </style>
    <style>
        table {

          overflow-x:auto;
        }
        table td {
          word-wrap: break-word;
          max-width: 400px;
        }
        #dt_basic td {
          white-space:inherit;
        }
</style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark" id="titlehead">

                <i class="fa fa-book"></i>
                
            </h1>
        </div>
    </div>
    <div class="row" id="addentry" style="display: none;">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
       </div>
    </div>
     <p></p>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-catalogo-0" >
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar catalogo </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-certificate"></i>
                                                <input type="text" name="nombre"  runat="server" id="nombre" placeholder="Fondo" maxlength="256" class="alptext"/>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-catalogo-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon" id="icongrid"></span>
                        <h2 id="titlegrid"> </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                                <thead>
                                    <tr>
                                        <th ></th>
                                        <th data-class="expand">#</th>
                                        <th >Nombre</th>    
                                        <th data-hide="phone,tablet">Descripción</th> 
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
    <div class="modal fade" id="form-modal" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>


                    <h4 class="modal-title" id="form-modal-title">


                    </h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="input" style="color:dodgerblue;">Nombre <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="nombre" runat="server" id="itemnombre" class="alphanumeric alptext" placeholder="Nombre" maxlength="256" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el nombre </b>
                                    </label>
                                </section>
                                <section id="modalDescripcionSection">
                                    <label class="input" style="color:dodgerblue;" >Descripción <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="descripcion" runat="server" id="itemdescripcion" class="alphanumeric alptext" placeholder="Descripción" maxlength="256" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese la descripción</b>
                                    </label>
                                </section>  
                                <input type="hidden" name="itemhabilitado" id="itemhabilitado" value="0" />                              
                            </div>
                        </fieldset>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar 
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro <strong><span id="itemeliminar"></span></strong>&nbsp;será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <div id="blockitem-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="tipocatalogo" runat="server" />
   
    <div id="history-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="history" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistory" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th>Movimiento</th>
                                        <th data-hide="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="historycanceled-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="catalogohistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div2" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistorydeleted" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Movimiento</th>
                                        <th>Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="idhistory" runat="server" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>

  




    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#form-modal").is(":visible")) {
                    document.getElementsByClassName("save")[0].click();
                }
            }
        });
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = false;
            
            setTitles();
            
            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                       
                    }
                },
                ajax: {
                    type: "POST",
                    url: "catalogo.aspx/getcatalogo",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                       
                        var catalogo = $("#ctl00_contenido_nombre").val();
                        parametrosServerSide.catalogo = catalogo;
                        parametrosServerSide.table = $("#ctl00_contenido_tipocatalogo").val();
                        parametrosServerSide.emptytable = false;        //window.emptytable;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                      null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                     {
                         name: "Descripcion",
                         data: "Descripcion"
                     },
                     null
                   

                ],
                columnDefs: [
                    {
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "";
                            var editar = "";
                            var habilitar = "";
                          
                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                edit = "edit";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                edit = "disabled";
                            }

                            if($("#ctl00_contenido_KAQWPK").val() == "true")editar='<a class="btn btn-primary btn-circle '+edit+'" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Nombre + '" data-descripcion="' + row.Descripcion + '" data-tracking="' + row.TrackingId + '" data-habilitado="' + row.Habilitado + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-id="' + row.Nombre + '" data-tracking="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                            return editar +
                                   habilitar +
                                   '<a name="history" class="btn btn-default btn-circle historial" href="javascript:void(0);" data-value="' + row.Nombre + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>'
                            ;
                        }
                    },
                     {
                         data: "TrackingId",
                         targets: 0,
                         orderable: false,
                         visible: false,
                         render: function (data, type, row, meta) {
                             return "";
                         }
                     },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    }

                ]
            });

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                $('#dt_basic').DataTable().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_itemnombre").val("");
                $("#ctl00_contenido_itemdescripcion").val("");
            });

            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-id");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-tracking", $(this).attr("data-tracking"));
                $("#blockitem-modal").modal("show");
            });

            $("body").on("click", ".add", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_itemnombre").val("");               
                $("#ctl00_contenido_itemdescripcion").val("");               
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i>Agregar registro");
                $("#form-modal").modal("show");
            });

            $("body").on("click", ".edit", function () {
                limpiar();                
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                var codigo = $(this).attr("data-codigo");               
           
                $("#btnsave").attr("data-id", id);                 
                
                $("#btnsave").attr("data-tracking", tracking);
                $("#ctl00_contenido_itemnombre").val($(this).attr("data-value"));
                $("#ctl00_contenido_itemdescripcion").val($(this).attr("data-descripcion"));
                $("#itemhabilitado").val($(this).attr("data-habilitado"));
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i>Editar registro");
                $("#form-modal").modal("show");
            });
 
            $("body").on("click", ".delete", function () {
                var id = $(this).attr("data-id");
                var nombre = $(this).attr("data-value");
                $("#itemeliminar").text(nombre);
                $("#btndelete").attr("data-id", id);
                $("#delete-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingId = $(this).attr("data-tracking");
                startLoading();
                $.ajax({
                    url: "catalogo.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingId: trackingId,
                        tipocatalogo: $('#ctl00_contenido_tipocatalogo').val()
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            $('#dt_basic').DataTable().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            ShowSuccess("¡Bien hecho!", data.msg);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }
                     
                });
                window.emptytable = true;
                $('#dt_basic').DataTable().ajax.reload();
            });

            $("body").on("click", ".save", function () {
                var id = $("#btnsave").attr("data-id");
                var tracking = $("#btnsave").attr("data-tracking");
                datos = [
                      id = id,
                      tracking = tracking,
                      nombre = $("#ctl00_contenido_itemnombre").val(),
                      descripcion = $("#ctl00_contenido_itemdescripcion").val(),
                      tipocatalogo = $('#ctl00_contenido_tipocatalogo').val(),
                      habilitado =$("#itemhabilitado").val()
                      
                ];

                if (validar()) {
                    Save(datos);
                }
            
            });

            var cambiarDescripcionPorClave = false;

            function setTitles()
            {
                $('#titlegrid').empty();
                $('#titlehead').empty();
                
                switch ($('#ctl00_contenido_tipocatalogo').val()) {
                    case 'sexo': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Sexo</li>');
                        $('#titlegrid').html('Sexos');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-venus-mars"></i> ');
                        $('#titlehead').html('<i class="fa fa-venus-mars"></i>  Catálogo de sexo');
                        break;
                    case 'motivo_rehabilitacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Motivo de rehabilitación</li>');
                        $('#titlegrid').html('Motivos de rehabilitación');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-plus"></i>');
                        $('#titlehead').html(' <i class="fa fa-plus "></i> Catálogo de motivo de rehabilitación');
                        break;
                    case 'lenguanativa': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Lengua nativa</li>');
                        $('#titlegrid').html('Lenguas nativas');
                        $('#icongrid').empty();
                        $('#icongrid').html('<img style="width:25px; height:25px;margin-top:2px; margin-left:2px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/LanguageWhite.png" />');/*<i class="fa fa-language"></i>*/
                        $('#titlehead').html('<img style="width:40px; height:40px;margin-top:1px; margin-right:2px;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/Language.png" /> Catálogo de lengua nativa'); /*<i class="fa fa-language"></i>*/



                        break;

                    case 'tipodediagnostico': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Tipo de diagnóstico</li>');
                        $('#titlegrid').html('Tipos de diagnóstico');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-book"></i>');
                        $('#titlehead').html(' <i class="fa fa-book "></i> Catálogo de tipo de diagnóstico');
                        break;
                    case 'Catalgo_Adiccion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Adicción</li>');
                        $('#titlegrid').html('Adicciones');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-male"></i>');
                        $('#titlehead').html(' <i class="fa fa-male "></i> Catálogo de adicción');
                        break;
                    case 'divisas': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Divisa</li>');
                        $('#titlegrid').html('Divisas');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-money"></i>');
                        $('#titlehead').html(' <i class="fa fa-money "></i> Catálogo de divisa');
                        break;
                    case 'pantalla': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Pantalla</li>');
                        $('#titlegrid').html('Pantallas');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-laptop"></i>');
                        $('#titlehead').html(' <i class="fa fa-laptop "></i> Catálogo de pantalla');
                        break;
                    case 'permiso': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Permiso</li>');
                        $('#titlegrid').html('Permisos');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-shield"></i>');
                        $('#titlehead').html(' <i class="fa fa-shield"></i> Catálogo de permiso');
                        break;
                    case 'tipo_media_filiacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Media filiación</li><li>Tipos de media filiación</li>');
                        $('#titlegrid').html('Tipos de media filiación');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-male"></i>');
                        $('#titlehead').html(' <i class="fa fa-male"></i> Catálogo de tipos de media filiación');
                        break;
                    case 'etnia': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Etnia</li>');
                        $('#titlegrid').html('Etnias');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="glyphicon glyphicon-asterisk"></i>');
                        $('#titlehead').html(' <i class="glyphicon glyphicon-asterisk"></i> Catálogo de etnia');
                        break;
                    case 'escolaridad': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Escolaridad</li>');
                        $('#titlegrid').html('Escolaridades');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-graduation-cap"></i>');
                        $('#titlehead').html(' <i class="fa fa-graduation-cap"></i> Catálogo de escolaridad');
                        break;
                    case 'religion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Religión</li>');
                        $('#titlegrid').html('Religiones');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-list-ul"></i>');
                        $('#titlehead').html(' <i class="fa fa-list-ul"></i> Catálogo de religión');
                        break;
                    case 'estado_civil': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Estado civil</li>');
                        $('#titlegrid').html('Estados civiles');
                        $('#icongrid').empty();
                        $('#icongrid').html('  <i class="fa fa-book"></i> ');
                        $('#titlehead').html('<i class="fa fa-book"></i>  Catálogo de estado civil');
                        break;
                    case 'nacionalidad': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Nacionalidad</li>');
                        $('#titlegrid').html('Nacionalidadades');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-globe"></i>');
                        $('#titlehead').html(' <i class="fa fa-globe"></i> Catálogo de nacionalidad');
                        break;
                    case 'parentesco': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Parentesco</li>');
                        $('#titlegrid').html('Parentescos');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-group"></i>');
                        $('#titlehead').html(' <i class="fa fa-group"></i> Catálogo de parentesco');
                        break;
                    case 'tipo_estudio': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tipo de estudio</li>');
                        $('#titlegrid').html('Tipos de estudio');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube"></i> Catálogo de tipo de estudio');
                        break;
                    case 'tatuaje': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tatuaje</li>');
                        $('#titlegrid').html('Tatuajes');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-ge"></i>');
                        $('#titlehead').html(' <i class="fa fa-ge"></i> Catálogo de tatuaje');
                        break;
                    case 'lunar': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Lunar</li>');
                        $('#titlegrid').html('Lunares');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-circle-o-notch"></i>');
                        $('#titlehead').html(' <i class="fa fa-circle-o-notch"></i> Catálogo de lunar');
                        break;
                    case 'media_filiacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Media filiación</li>');
                        $('#titlegrid').html('Media filiación');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cubes"></i>');
                        $('#titlehead').html(' <i class="fa fa-cubes"></i> Catálogo de media filiación');
                        break;
                    case 'mes': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Mes</li>');
                        $('#titlegrid').html('Meses');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube"></i> Catálogo de mes');
                        break;


                    case 'tamaño': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tamaño</li>');
                        $('#titlegrid').html('Tamaños');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube"></i> Catálogo de tamaño');
                        break;

                    case 'adherencia': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Adherencia</li>');
                        $('#titlegrid').html('Adherencias');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube"></i> Catálogo de adherencia');
                        break;

                    case 'tipo_senal': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tipo señal</li>');
                        $('#titlegrid').html('Tipos de señal');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube"></i> Catálogo de tipo señal');
                        break;

                    case 'lado_senal': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Lado señal</li>');
                        $('#titlegrid').html('Lados de señal');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube"></i> Catálogo de lado señal');
                        break;

                    case 'vista_senal': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Vista señal</li>');
                        $('#titlegrid').html('Vistas de señal');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de vista de señal');
                        break;

                    case 'calificacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Calificación</li>');
                        $('#titlegrid').html('Calificaciones');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de calificación');
                        break;
                    case 'corporacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Unidad</li><li>Corporación</li>');
                        $('#titlegrid').html('Corporaciones');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-institution"></i>');
                        $('#titlehead').html(' <i class="fa fa-institution "></i> Catálogo de corporación');
                        break;
                    case 'delegacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Institución a disposición</li>');
                        $('#titlegrid').html('Instituciones a disposición');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-sitemap"></i>');
                        $('#titlehead').html(' <i class="fa fa-sitemap "></i> Catálogo de institución a disposición');
                        break;
                    case 'entrega_pertenencia': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Pertenencia</li><li>Entrega de pertenencia</li>');
                        $('#titlegrid').html('Entrega de pertenencias');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-repeat"></i>');
                        $('#titlehead').html(' <i class="fa fa-repeat "></i> Catálogo de entrega de pertenencia');
                        break;
                    case 'estatus_pertenencia': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Pertenencia</li><li>Estatus de pertenencia</li>');
                        $('#titlegrid').html('Estatus de pertenencias');
                        $('#icongrid').empty();
                        $('#icongrid').html(' <i class="fa fa-book"></i>');
                        $('#titlehead').html(' <i class="fa fa-book "></i> Catálogo de estatus de pertenencia');
                        break;
                    case 'evento_reciente': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Evento reciente</li>');
                        $('#titlegrid').html('Eventos recientes');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de evento reciente');
                        break;

                    case 'institucion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Institución</li>');
                        $('#titlegrid').html('Instituciones');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de institución');
                        break;

                    case 'institucion_disposicion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Institución </li>');
                        $('#titlegrid').html('Institución a disposición');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Institución   a disposición');
                        break;

                    case 'pertenencia': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Pertenencia</li>');
                        $('#titlegrid').html('Pertenencias');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de pertenencia');
                        break;
                    case 'prueba': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Prueba</li>');
                        $('#titlegrid').html('Pruebas');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de prueba');
                        break;
                    case 'responsable_unidad': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Unidad</li><li>Reponsable de unidad</li>');
                        $('#titlegrid').html('Responsables de unidad');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-male"></i>');
                        $('#titlehead').html(' <i class="fa fa-male "></i> Catálogo de responsable de unidad');
                        cambiarDescripcionPorClave = true;
                        break;

                    case 'unidad': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Unidad</li><li>Unidad</li>');
                        $('#titlegrid').html('Unidades');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Unidad');
                        break;
                    case 'salida_autorizada': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Salida autorizada</li>');
                        $('#titlegrid').html('Salidas autorizadas');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-check"></i>');
                        $('#titlehead').html(' <i class="fa fa-check-circle "></i> Catálogo de salida autorizada');
                        break;
                    case 'salida_tipo': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Salida</li><li>Tipo de salida</li>');
                        $('#titlegrid').html('Tipo de salida');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-check-circle"></i>');
                        $('#titlehead').html(' <i class="fa fa-check-circle"></i> Tipo de salida');
                        break;
                    case 'situacion_detenido': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Situación del detenido</li>');
                        $('#titlegrid').html('Situación del detenido');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-warning"></i>');
                        $('#titlehead').html(' <i class="fa fa-warning "></i> Catálogo de situación del detenido');
                        break;
                    case 'tipo_examen': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tipo de examen</li>');
                        $('#titlegrid').html('Tipos de examen');
                        $('#icongrid').empty();
                        $('#icongrid').html('<img style="width:30px; height:30px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/MedicalTestLineWhite.png" />'); /*<i class="fa fa-eyedropper"></i>*/
                        $('#titlehead').html('<img style="width:40px; height:40px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/MedicalTestLine.png" /> Catálogo de tipo de examen'); /*<i class="fa fa-eyedropper"></i>*/
                        break;
                    case 'tipo_intoxicacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tipo de intoxicación</li>');
                        $('#titlegrid').html('Tipos de  intoxicación');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-glass"></i>');
                        $('#titlehead').html(' <i class="fa fa-glass "></i> Catálogo de tipo intoxicación');
                      
                        break;
                    case 'tipo_tatuaje': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tipo de tatuaje</li>');
                        $('#titlegrid').html('Tipos de tatuaje');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-asterisk"></i>');
                        $('#titlehead').html(' <i class="fa fa-asterisk"></i> Catálogo de tipo de tatuaje');
                        break;
                    case 'tipo_lesion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tipo de lesión</li>');
                        $('#titlegrid').html('Tipos de lesión');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-flash"></i>');
                        $('#titlehead').html(' <i class="fa fa-flash"></i> Catálogo de tipo de lesión');
                        break;
                    case 'turno': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Turno</li>');
                        $('#titlegrid').html('Turnos');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de tTurno');
                        break;
                    case 'sector': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Sector</li>');
                        $('#titlegrid').html('Sectores');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de sector');
                        break;
                    case 'peligrosidad_criminologica': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Peligrosidad criminológica en celda </li>');
                        $('#titlegrid').html('Peligrosidad criminológica en celda');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-warning"></i>');
                        $('#titlehead').html(' <i class="fa fa-warning"></i> Catálogo de peligrosidad criminológica en celda');
                        break;

                    case 'mucosas': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Mucosa</li>');
                        $('#titlegrid').html('Mucosas');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de mucosas');
                        break;
                    case 'aliento': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Aliento</li>');
                        $('#titlegrid').html('Aliento');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de aliento');
                        break;
                    case 'examen_neurologico': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Examen neurológico</li>');
                        $('#titlegrid').html('Examenes neurológicos');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de examen neurológico');
                        break;
                    case 'conjuntivas': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Conjuntivas</li>');
                        $('#titlegrid').html('Conjuntivas');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de conjuntivas');
                        break;
                    case 'marcha': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Marcha</li>');
                        $('#titlegrid').html('Marcha');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de marcha');
                        break;
                    case 'discurso': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Discurso</li>');
                        $('#titlegrid').html('Discurso');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de discurso');
                        break;
                    case 'reflejos_pupilares': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Reflejos pupilares</li>');
                        $('#titlegrid').html('Reflejos pupilares');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de reflejos pupilares');
                        break;
                    case 'reflejos_osteo_tendinosos': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Reflejo osteo tendinoso</li>');
                        $('#titlegrid').html('Reflejos osteo tendinosos');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de reflejos osteo tendinosos');
                        break;
                    case 'conducta': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Conducta</li>');
                        $('#titlegrid').html('Conducta');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de conducta');
                        break;
                    case 'romberq': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Romberg</li>');
                        $('#titlegrid').html('Romberg');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de romberg');
                        break;
                    case 'lenguaje': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Lenguaje</li>');
                        $('#titlegrid').html('Lenguaje');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de lenguaje');
                        break;
                    case 'atencion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Atención</li>');
                        $('#titlegrid').html('Atención');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de atención');
                        break;
                    case 'orientacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li> Orientación</li>');
                        $('#titlegrid').html('Orientación');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de orientación');
                        break;
                    case 'diadococinencia': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Diadococinencia</li>');
                        $('#titlegrid').html('Diadococinencia');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de diadococinencia');
                        break;
                    case 'dedo_nariz': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Dedo-nariz</li>');
                        $('#titlegrid').html('Dedo-nariz');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de dedo-nariz');
                        break;
                    case 'talon_rodilla': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Talón-rodilla</li>');
                        $('#titlegrid').html('Talón-rodilla');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de talón-rodilla');
                        break;
                    case 'pupilas': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Pupila</li>');
                        $('#titlegrid').html('Pupilas');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de pupila');
                        break;
                    case 'coordinacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Coordinación</li>');
                        $('#titlegrid').html('Coordinación');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de coordinación');
                        break;

                    case 'evidencia_clasificacion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Clasificación de evidencia</li>');
                        $('#titlegrid').html('Clasificación de evidencia');
                        $('#icongrid').empty();
                        $('#icongrid').html('<img style="width:30px; height:30px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/EvidenceWhite.png" />'); /*<i class="fa fa-globe"></i>*/

                        $('#titlehead').html(' <img style="width:40px; height:40px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/Evidence1.png" /> Catálogo de clasificación de evidencia');/* <i class="fa fa-globe "></i>*/
                        //$("#form-modal-title").html("<i class='fa fa-pencil'+></i>Agregar Registro"); 
                        break;

                    case 'tipomovimiento_celda': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Tipo de movimiento en celda</li>');
                        $('#titlegrid').html('Tipo de movimiento en celda');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-cube"></i>');
                        $('#titlehead').html(' <i class="fa fa-cube "></i> Catálogo de tipo de movimiento en celda');
                        break;

                    case 'pais': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li >Localización</li><li>País</li>');
                        $('#titlegrid').html('Países');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-globe"></i>');
                        $('#titlehead').html(' <i class="fa fa-globe"></i> Catálogo de países');
                        break;

                    case 'cicatriz': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li >Media filiación</li><li>Cicatriz</li>');
                        $('#titlegrid').html('Cicatrices');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="fa fa-flash"></i>');
                        $('#titlehead').html(' <i class="fa fa-flash"></i> Catálogo de cicatriz');
                        break;

                    case 'pertenenciacomun': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Pertenencia</li><li>Pertenencia común</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;">Pertenencia común</p>');
                        $('#icongrid').empty();
                        $('#icongrid').html('<img style="width:30px; height:30px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/ItemsWhite.png" />'); /*<i class="fa fa-book"></i>*/
                        $('#titlehead').html('<img style="width:40px; height:40px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/Items.png" />   Catálogo de pertenencia común'); /*<i class="fa fa-book"></i>*/
                        break;

                    

                    case 'actitud': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Actitud</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;">Actitud</p>');
                        $('#icongrid').empty();
                         $('#icongrid').html('<i class="glyphicon glyphicon-hand-right"></i>');
                        $('#titlehead').html('<i class="glyphicon glyphicon-hand-right"></i>  Catálogo actitud'); 
                        break;
                    case 'cavidad_oral': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li> Cavidad oral</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Cavidad oral</p>');
                        $('#icongrid').empty();
                          $('#icongrid').html('<i class="fa fa-comments-o"></i>');
                        $('#titlehead').html(' <i class="fa fa-comments-o"></i> Catálogo de cavidad oral'); 
                        break;
                    case 'equipo_a_utilizar': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li> Equipo a utilizar</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Equipo a utilizar</p>');
                        $('#icongrid').empty();
                          $('#icongrid').html('<i class="glyphicon glyphicon-th-list"></i>');
                        $('#titlehead').html(' <i class="glyphicon glyphicon-th-list"></i> Catálogo de equipo a utilizar'); 
                        break;

                    case 'ojos_abiertos_dedo_dedo': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li> Ojos abiertos dedo-dedo</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Ojos abiertos dedo-dedo</p>');
                        $('#icongrid').empty();
                        $('#icongrid').html('<i class="glyphicon glyphicon-eye-open"></i>');
                        $('#titlehead').html('<i class="glyphicon glyphicon-eye-open"></i> Catálogo ojos abiertos dedo-dedo'); 
                        break;
                    case 'ojos_cerrados_dedo_dedo': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Ojos cerrados dedo-dedo</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Ojos cerrados dedo-dedo</p>');
                        $('#icongrid').empty();
                       $('#icongrid').html('<i class="glyphicon glyphicon-eye-close"></i>');
                         $('#titlehead').html('<i class="glyphicon glyphicon-eye-close"></i> Catálogo ojos cerrados dedo-dedo'); 
                        break;
                    case 'ojos_abiertos_dedo_nariz': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li>Ojos abiertos dedo-nariz</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Ojos abiertos dedo-nariz</p>');
                        $('#icongrid').empty();
                       $('#icongrid').html('<i class="glyphicon glyphicon-eye-open"></i>');
                         $('#titlehead').html('<i class="glyphicon glyphicon-eye-open"></i> Catálogo ojos abiertos dedo-nariz');
                        break;
                    case 'ojos_cerrados_dedo_nariz': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Ojos cerrados dedo-nariz</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Ojos cerrados dedo-nariz</p>');
                        $('#icongrid').empty();
                       $('#icongrid').html('<i class="glyphicon glyphicon-eye-close"></i>');
                         $('#titlehead').html('<i class="glyphicon glyphicon-eye-close"></i> Catálogo ojos cerrados dedo-nariz'); 
                        break;
                    
                    case 'tipo_alergia': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li >Administración</li><li> Tipo de alergia</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Tipos de alergia</p>');
                        $('#icongrid').empty();
                       $('#icongrid').html('<i class="glyphicon glyphicon-certificate"></i>');
                         $('#titlehead').html('<i class="glyphicon glyphicon-certificate"></i> Catálogo tipo de alergia'); 
                        break;
                    
                    case 'opcion_comun': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li> Opción común</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Opciones comunes</p>');
                        $('#icongrid').empty();
                       $('#icongrid').html('<i class="fa fa-codepen"></i>');
                         $('#titlehead').html('<i class="fa fa-codepen"></i> Catálogo de opción común'); 
                        break;
                    case 'Antecedentes': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Antecedente</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Antecedentes</p>');
                        $('#icongrid').empty();
                       $('#icongrid').html('<i class="fa fa-codepen"></i>');
                         $('#titlehead').html('<i class="fa fa-codepen"></i> Catálogo de antecedente'); 
                        break;
                    case 'Conclusion': $('#ol_breadcrumb').html('<li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx">Inicio</a></li><li>Administración</li><li>Conclusión</li>');
                        $('#titlegrid').html('<p style="margin-left:5px;"> Conclusión</p>');
                        $('#icongrid').empty();
                       $('#icongrid').html('<i class="fa fa-codepen"></i>');
                         $('#titlehead').html('<i class="fa fa-codepen"></i> Catálogo de conclusión'); 
                        break;
               
                }

                if (cambiarDescripcionPorClave) CambiarDescripcionPorClave();
            }


            function CambiarDescripcionPorClave() {
                $("#modalDescripcionSection label:first-child").html("Clave <a style='color:red'>*</a>");
                $("#modalDescripcionSection input").attr("placeholder", "Clave");
                $("#dt_basic th:nth-child(4)").text("Clave");
            }


            function limpiar() {

                $('#ctl00_contenido_itemdescripcion').parent().removeClass('state-success');
                $('#ctl00_contenido_itemdescripcion').parent().removeClass("state-error");
                $('#ctl00_contenido_itemnombre').parent().removeClass('state-success');
                $('#ctl00_contenido_itemnombre').parent().removeClass("state-error");

            }

            function validar() {
                var esvalido = true;

                if ($("#ctl00_contenido_itemnombre").val().split(" ").join("") == "")
                {

                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#ctl00_contenido_itemnombre').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemnombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemnombre').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemnombre').addClass('valid');
                }

               

                if ($("#ctl00_contenido_itemdescripcion").val().split(" ").join("") == "") {
                    
                    
                     ShowError($("#modalDescripcionSection label:first-child").text().replace('*',''),"La "+ $("#modalDescripcionSection label:first-child").text().replace('*','').toLowerCase()+" es obligatoria.");
                    $('#ctl00_contenido_itemdescripcion').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemdescripcion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemdescripcion').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemdescripcion').addClass('valid');
                }
                
                return esvalido;
            }

            function Save(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "catalogo.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");

                            $("#form-modal").modal("hide");
                            
                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }
                        else {
                            ShowError("Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

           

            $("#btndelete").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "catalogo.aspx/delete",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id,
                        tipocatalogo: $('#ctl00_contenido_tipocatalogo').val()
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            $('#dt_basic').DataTable().ajax.reload();
                            ShowSuccess("¡Bien hecho!", "El registro fue eliminado del catálogo.");
                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);

                        }
                        $("#delete-modal").modal("hide");
                        $('#main').waitMe('hide');
                    }
                });
            });




            $("body").on("click", ".historial", function () {
                var id = $(this).attr("data-id");
                $('#ctl00_contenido_idhistory').val(id);
                var descripcion = $(this).attr("data-value");
                $("#descripcionhistorial").text(descripcion);
                $("#history-modal").modal("show");

                window.emptytablehistory = false;
                window.tablehistory.api().ajax.reload();

            });

            var responsiveHelper_dt_basichistory = undefined;
            var breakpointHistoryDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistory = true;
            window.tablehistory = $('#dt_basichistory').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                "order": [[3, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistory) {
                        responsiveHelper_dt_basichistory = new ResponsiveDatatablesHelper($('#dt_basichistory'), breakpointHistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistory.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistory.respond();
                    $('#dt_basichistory').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "catalogo.aspx/getcatalogolog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basichistory').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        table = $('#ctl00_contenido_tipocatalogo').val();
                        var catalogoid = $('#ctl00_contenido_idhistory').val();
                        parametrosServerSide.catalogoid = catalogoid;
                        parametrosServerSide.table = table;
                        parametrosServerSide.todoscancelados = false;
                        parametrosServerSide.emptytable = window.emptytablehistory;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        orderable: false
                    },
                    null,
                    {
                        name: "fecha",
                        data: "Fec_Movto",
                        orderable: true
                    }
                ],
                columnDefs: [

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {


                            return row.CreadoPor != null ? row.CreadoPor : '';


                        }
                    }

                ]
            });

            $("body").on("click", ".historialcancelado", function () {
                $("#historycanceled-modal").modal("show");

                window.emptytablehistorydeleted = false;
                window.tablehistorydeleted.api().ajax.reload();
            });

            var responsiveHelper_dt_basichistorydeleted = undefined;
            window.emptytablehistorydeleted = true;

            window.tablehistorydeleted = $('#dt_basichistorydeleted').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[4, 'asc'], [3, 'desc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistorydeleted) {
                        responsiveHelper_dt_basichistorydeleted = new ResponsiveDatatablesHelper($('#dt_basichistorydeleted'), breakpointHistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistorydeleted.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistorydeleted.respond();
                    $('#dt_basichistorydeleted').waitMe('hide');
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(4, { page: 'current' }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"><i class="fa fa-info-circle"></i>&nbsp;<strong>Eliminados</strong></td></tr>'
                            );

                            last = group;
                        }
                    });
                },

                ajax: {
                    type: "POST",
                    url: "catalogo.aspx/getcatalogolog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basichistorydeleted').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        table = $('#ctl00_contenido_tipocatalogo').val();

                        parametrosServerSide.catalogoid = 0;
                        parametrosServerSide.table = table;
                        parametrosServerSide.todoscancelados = true;
                        parametrosServerSide.emptytable = window.emptytablehistorydeleted;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: false
                    },

                    {
                        name: "Accion",
                        data: "Accion",
                        orderable: false
                    },
                    null,
                    null,

                    {
                        targets: 4,
                        "class": "details-control",
                        orderable: false,
                        data: null,
                        visible: false,
                        name: "C.Id",
                        data: "Id"
                    }
                ],
                columnDefs: [

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {



                            return row.CreadoPor != null ? row.CreadoPor : '';


                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            return row.Fec_Movto != null ? toDateTime(row.Fec_Movto) : '';

                        }
                    }
                ]
            });
            init();
            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }

            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
           
        });
    </script>
</asp:Content>
