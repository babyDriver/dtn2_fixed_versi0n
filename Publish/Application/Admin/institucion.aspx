﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="institucion.aspx.cs" Inherits="Web.Application.Admin.institucion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li >Administración</li>
    <li>Institución</li> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
         <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
    
    </style>
    <div class="scroll">
    <div class="row"  >
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark" id="titlehead">
                 <i class="fa fa-institution"></i>
                Catálogo de institución
            </h1>
        </div>
    </div>
    <div class="row" id="addentry" style="display: none;">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
        </div>
    </div>
    <p></p>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-centroreclusion-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar catálogo </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-certificate"></i>
                                                <input type="text" name="nombre" runat="server" id="nombre" placeholder="Institución a disposición" maxlength="256" class="alptext"/>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-centroreclusion-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                          <span class="widget-icon"><i class="fa fa-institution"></i></span>
                        <h2 id="titlegrid">Institución</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th data-class="expand">Estado</th>
                                        <th data-class="expand">Nombre</th>
                                         <th data-hide="phone,tablet">Domicilio</th>
                                         <th data-hide="phone,tablet">Teléfono</th>
                                         <th data-hide="phone,tablet">Encargado</th>
                                         <th data-hide="phone,tablet">Cupo Hombres</th>
                                         <th data-hide="phone,tablet">Cupo Mujeres</th>
                                         <th data-hide="phone,tablet">Clave / nombre corto </th>                                       
                                        <th data-hide="phone,tablet" style="width: 10%">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
    <div class="modal fade" id="form-modal"    data-keyboard="true" role="dialog" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title">Agregar registro</h4> <%--//////////////////////--%>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                                <legend class="hide">&nbsp;</legend>
                                <section>
                                    <label style="color:dodgerblue">País <a style="color:red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="pais" runat="server" class="select2" style="width:100%;">
                                        </select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section id="sectionestado">
                                    <label style="color:dodgerblue">Estado <a style="color:red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="estado" runat="server" class="select2" style="width:100%;">
                                        </select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section id="sectionmunicipio">
                                    <label style="color:dodgerblue">Municipio <a style="color:red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="municipio" runat="server" class="select2" style="width:100%;">
                                        </select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section id="sectioncolonia">
                                    <label style="color:dodgerblue">Colonia <a style="color:red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="colonia" runat="server" class="select2" style="width:100%;">
                                        </select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>

                                 <section id="sectionlocalidadn">
                                    <label class="label" style="color:dodgerblue">Localidad </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-street-view"></i>
                                        <input type="text" name="colonialugar" id="localidadn" hidden="hidden" runat="server" placeholder="Localidad" maxlength="250" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa localidad.</b>
                                    </label>
                                </section>

                                <section>
                                    <label style="color:dodgerblue">Institución <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input name="nombre" id="centro" runat="server" placeholder="Institución" maxlength="256" class="alphanumeric alptext"  />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la institución</b>
                                        </label>
                                 </section>
                                <section>
                                    <label style="color:dodgerblue">Domicilio <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input name="domicilio" id="domicilio" runat="server" placeholder="Domicilio" maxlength="256" class="alphanumeric alptext"  />
                                            <b class="tooltip tooltip-bottom-right">Domicilio</b>
                                        </label>
                                </section>
                                <section>
                                    <label style="color:dodgerblue">Teléfono <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-mobile"></i>
                                            <input type="tel" name="movil" id="telefono" runat="server" placeholder="Teléfono" data-mask="(999) 999-9999" />
                                            <b class="tooltip tooltip-bottom-right">Teléfono</b>
                                        </label>
                                </section>
                                <section>
                                    <label style="color:dodgerblue">Encargado <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="tel" name="director" id="director" runat="server" placeholder="Encargado" maxlength="256" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el encargado</b>
                                        </label>
                                </section>
                                <section>

                                    <label style="color:dodgerblue">Cupo hombres <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-male"></i>
                                            <input  name="cupohombre" id="cupohombre" runat="server" placeholder="Cupo hombres" maxlength="6" />
                                            <b class="tooltip tooltip-bottom-right">Cupo hombres</b>
                                        </label>
                                   
                                </section>                            
                                <section>
                                    <label style="color:dodgerblue">Cupo mujeres <a style="color:red">*</a></label>
                                    <label class="input">
                                            <i class="icon-append fa fa-female"></i>
                                            <input  name="cupomujere" id="cupomujere" runat="server" placeholder="Cupo mujeres" maxlength="6" />
                                            <b class="tooltip tooltip-bottom-right">Cupo mujeres</b>
                                        </label>
                   <%--                 <label style="color:dodgerblue">Cupo mujeres <a style="color:red">*</a></label>
                                        <label class="text">
                                            <i class="icon-append fa fa-female"></i>
                                            <input type="number" name="cupomujere" id="cupomujeres" runat="server" placeholder="Cupo mujeres" maxlength="6"   />
                                            <b class="tooltip tooltip-bottom-right">Cupo mujeres</b>
                                        </label>--%>
                                </section>                           
                                <section>
                                    <label style="color:dodgerblue">Clave / nombre corto <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-hashtag"></i>
                                            <input name="claverni" id="claverni" runat="server" placeholder="Clave / nombre corto " maxlength="50" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Clave ó nombre corto de la institución </b>
                                        </label>
                                </section>
                        </fieldset>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                 <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>  
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                                <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                                         
                            </div>
                                </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar 
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro <strong><span id="itemeliminar"></span></strong>&nbsp;será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a href="javascript:void(0);" class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                            <a href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                       <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                         
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>                       
                       </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="history-modal" class="modal fade"  data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="history" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistory" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-hide="tablet,fablet,phone">Nombre</th>
                                        <th>Movimiento</th>
                                        <th data-hide="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       <div id="historycanceled-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="catalogohistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div2" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistorydeleted" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Movimiento</th>
                                        <th>Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="idhistory" runat="server" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
        <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
   
    <script type="text/javascript">

        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                console.log("ctl+g");
                e.preventDefault();

                document.getElementsByClassName("save")[0].click();
            }
        });

        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = true;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');

                }, "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                        $('td', row).eq(7).addClass('strikeout');
                        $('td', row).eq(8).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "institucion.aspx/getlist",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                   
                        parametrosServerSide.centro = $("#ctl00_contenido_nombre").val();
                        parametrosServerSide.emptytable = false;        //window.emptytable;

                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    null,
                    null,
                    null,
                     {
                         name: "Nombre",
                         data: "Nombre"
                     },
                     //{
                     //    name: "Domicilio",
                     //    data: "Domicilio"
                     //},
                     null,
                     {
                         name: "Telefono",
                         data: "Telefono"
                     },
                     {
                         name: "Encargado",
                         data: "Encargado"
                     },
                     {
                         name: "CupoHombres",
                         data: "CupoHombres"
                     },
                    {
                        name: "CupoMujeres",
                        data: "CupoMujeres"
                    },
                      {
                          name: "Clave",
                          data: "Clave"
                      },
                    null
                ],
                columnDefs: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        name: "Estado",
                        data: "Entidad",
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.Entidad == null || row.Entidad == "") return "Sin dato";
                            else return row.Entidad;
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
               
                    {
                        targets: 10,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "";
                            var editar = "";
                            var habilitar = "";

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger"; edit = "edit";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }


                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' " href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Nombre + '" data-domicilio="' + row.Domicilio + '" data-tracking="' + row.TrackingId + '" data-tipoid="' + row.EstadoId + '" data-encargado="' + row.Encargado + '" data-telefono="' + row.Telefono + '" data-rni="' + row.Clave + '" data-mujeres="' + row.CupoMujeres + '" data-hombres="' + row.CupoHombres + '" data-habilitado="' + row.Habilitado + '" data-PaisId="' + row.PaisId + '" data-EstadoId="' + row.EstadoId + '" data-MunicipioId="' + row.MunicipioId + '" data-coloniaId="' + row.ColoniaId + '" data-localidad="' + row.Localidad + '" data-trackingIdDomicilio="' + row.TrackingIdDomicilio + '" data-domicilioId="' + row.DomicilioId + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;' ;
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Nombre + '" data-tracking="' + row.TrackingId + '" data-id="' + row.Id + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';

                            return editar+
                                habilitar+
                                '<a name="history" class="btn btn-default btn-circle historial" href="javascript:void(0);" data-value="' + row.Nombre + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>'
                            ;
                        }
                    },
                    {
                        data: "Pais",
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.PaisId == 73) {
                                return data + ((row.Entidad == null || row.Entidad == "") ? "" : ", " + row.Entidad) +
                                    ((row.Municipio == null || row.Municipio == "") ? "" : ", " + row.Municipio) +
                                    ((row.Colonia == null || row.Colonia == "") ? "" : ", " + row.Colonia) +
                                    ((row.Domicilio == null || row.Domicilio == "") ? "" : ", " + row.Domicilio);
                            }
                            else
                            {
                                var cadena = row.Pais + ((row.Colonia == null || row.Colonia == "") ? "" : ", " + row.Colonia) +
                                ((row.Domicilio == null || row.Domicilio == "") ? "" : ", " + row.Domicilio);
                                if (cadena == "") return "Sin dato";
                                else return cadena;
                            }
                        }
                    },
                ]
            });

            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-tracking", $(this).attr("data-tracking"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-tracking");
                startLoading();
                $.ajax({
                    url: "institucion.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: id
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            ShowSuccess("¡Bien hecho!", data.mensaje);
                        }
                        else {
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico. " + data.mensaje);
                        }
                        $("#blockitem-modal").modal("hide");
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });


            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_centro").val("");
                $("#ctl00_contenido_domicilio").val("");
                $("#ctl00_contenido_telefono").val("");
                $("#ctl00_contenido_claverni").val("");
                $("#ctl00_contenido_cupohombre").val("");
                $("#ctl00_contenido_cupomujere").val("");
                $("#ctl00_contenido_pais").val("");
                $("#ctl00_contenido_estado").val("");
                $("#ctl00_contenido_municipio").val("");
                $("#ctl00_contenido_colonia").val("");
            });

            $("body").on("click", ".add", function () {
                limpiar();               
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_centro").val("");
                $("#ctl00_contenido_domicilio").val("");
                $("#ctl00_contenido_telefono").val("");
                $("#ctl00_contenido_claverni").val("");
                $("#ctl00_contenido_cupohombre").val("");
                $("#ctl00_contenido_cupomujere").val("");
                $("#ctl00_contenido_tipo").val("");
                $("#ctl00_contenido_director").val("");
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar registro");
                $('#habilitado').prop('checked', false);               
                $("#form-modal").modal("show");
                $('#sectionestado').show();
                $('#sectionmunicipio').show();
                $('#sectioncolonia').show();
                CargarPais(0, '#ctl00_contenido_pais');
                CargarEstado(0, '#ctl00_contenido_estado', $("#ctl00_contenido_pais").val());
            });

            $("body").on("click", ".edit", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                var trackingIdDomicilio = $(this).attr("data-trackingIdDomicilio");
                var nombre = $(this).attr("data-nombre");
                var domicilio = $(this).attr("data-domicilio");
                var telefono = $(this).attr("data-telefono");
                var encargado = $(this).attr("data-encargado");
                var cupohombres = $(this).attr("data-hombres");
                var cupomujeres = $(this).attr("data-mujeres");
                var claverni = $(this).attr("data-rni");
                var tipoId = $(this).attr("data-tipoid");
                var habilitado = $(this).attr("data-habilitado");
                var paisId = $(this).attr("data-paisId");
                
                var estadoId = $(this).attr("data-estadoId");
                var municipioId = $(this).attr("data-MunicipioId");
                var coloniaId = $(this).attr("data-coloniaId");
                var domicilioId = $(this).attr("data-domicilioId");

                if (paisId == "null") {
                    paisId = 0;
                }

                if (estadoId == "null") {
                    estadoId = 0;
                }

                if (municipioId == "null") {
                    municipioId = 0;
                }

                if (coloniaId == "null") {
                    coloniaId = 0;
                }

                if (domicilio == "null") {
                    domicilio = "";
                }

                if (trackingIdDomicilio == "null") {
                    trackingIdDomicilio = "";
                }

                if (domicilioId == "null") {
                    domicilioId = "";
                }
                
                

                CargarPais(paisId, '#ctl00_contenido_pais');
                if (paisId != 73)
                {
                    
                    $('#sectionmunicipio').hide();
                    $('#sectionestado').hide();
                    $('#sectioncoloniaDomicilio').hide();
                    $('#sectionlocalidadn').show();
                    
                    $('#sectioncolonia').hide();
                }
                else {
                    //alert(paisId);
                    $('#sectionestado').show();
                    $('#sectionmunicipio').show();
                    $('#sectioncolonia').show();
                    $('#sectionlocalidadn').hide();

                    
                }

                CargarEstado(estadoId, '#ctl00_contenido_estado', paisId);
                CargarMunicipio(municipioId, '#ctl00_contenido_municipio', estadoId);
                CargarColonia(coloniaId, municipioId);

                $("#btnsave").attr("data-id", id);
                $("#btnsave").attr("data-tracking", tracking);
                $("#btnsave").attr("data-trackingIdDomicilio", trackingIdDomicilio);
                $("#btnsave").attr("data-nombre", nombre);
                $("#btnsave").attr("data-domicilio", domicilio);
                $("#btnsave").attr("data-director", encargado);
                $("#btnsave").attr("data-telefono", telefono);
                $("#btnsave").attr("data-cupohombres", cupohombres);
                $("#btnsave").attr("data-cupomujeres", cupomujeres);
                $("#btnsave").attr("data-claverni", claverni);
                $("#btnsave").attr("data-tipoId", tipoId);
                $("#btnsave").attr("data-domicilioId", domicilioId);
                $("#btnsave").attr("data-habilitado", habilitado);
                $("#ctl00_contenido_centro").val($(this).attr("data-value"));
                $("#ctl00_contenido_domicilio").val(domicilio);
                $("#ctl00_contenido_director").val($(this).attr("data-encargado"));
                $("#ctl00_contenido_telefono").val($(this).attr("data-telefono"));
                $("#ctl00_contenido_claverni").val($(this).attr("data-rni"));
                $("#ctl00_contenido_cupohombre").val($(this).attr("data-hombres"));
                $("#ctl00_contenido_cupomujere").val($(this).attr("data-mujeres"));
                $("#ctl00_contenido_pais").val($(this).attr("data-paisId"));
                $("#ctl00_contenido_estado").val($(this).attr("data-estadoId"));
                $("#ctl00_contenido_municipio").val($(this).attr("data-municipioId"));
                $("#ctl00_contenido_colonia").val($(this).attr("data-coloniaId"));
                $("#itemhabilitado").val($(this).attr("data-habilitado"));
                $("#ctl00_contenido_localidadn").val($(this).attr("data-localidad"));
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar registro");

                if ($("#ctl00_contenido_pais option:selected").val() != 73) {
                    $('#sectionmunicipio').hide();
                    $('#sectionestado').hide();
                    $('#sectioncoloniaDomicilio').hide();
                    $('#sectionlocalidadn').show();
                    
                    $('#sectioncolonia').hide();
                }
                else {
                    
                    $('#sectionestado').show();
                    $('#sectionmunicipio').show();
                    $('#sectioncolonia').show();
                    $('#sectionlocalidadn').hide();
                    CargarEstado(0, '#ctl00_contenido_estado', $("#ctl00_contenido_pais").val());
                }

                if (paisId != 73)
                {
                    
                    $('#sectionmunicipio').hide();
                    $('#sectionestado').hide();
                    $('#sectioncoloniaDomicilio').hide();
                    $('#sectionlocalidadn').show();
                    
                    $('#sectioncolonia').hide();
                }
                else {
                    //alert(paisId);
                    $('#sectionestado').show();
                    $('#sectionmunicipio').show();
                    $('#sectioncolonia').show();
                    $('#sectionlocalidadn').hide();

                    
                }



                $("#form-modal").modal("show");
            });

            function CargarPais(set, combo) {
                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "institucion.aspx/getPais",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[País]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                            if (set == 0 && item.Desc == "MEXICO") {
                                $("#ctl00_contenido_pais option[value= '" + item.Id + "']").attr("selected", "selected");
                                Dropdown.trigger("change.select2");
                                $('#sectionlocalidadn').hide();
                            }
                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEstado(setestado, combo, paisid) {

                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "institucion.aspx/getEstado",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ paisId: 73 }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Estado]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (setestado != "") {

                            Dropdown.val(setestado);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de estados. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarMunicipio(set, combo, estadoid) {
                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "institucion.aspx/getMunicipio",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ estadoId: estadoid }),
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Municipio]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de municipio. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarColonia(set, idMunicipio) {                
                $.ajax({

                    type: "POST",
                    url: "institucion.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#ctl00_contenido_colonia");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("body").on("click", ".delete", function () {
                var id = $(this).attr("data-id");
                var nombre = $(this).attr("data-value");
                $("#itemeliminar").text(nombre);
                $("#btndelete").attr("data-id", id);
                $("#delete-modal").modal("show");
            });

            $("body").on("click", ".save", function () {
                var id = $("#btnsave").attr("data-id");
                var tracking = $("#btnsave").attr("data-tracking");
                var trackingIdDomicilio = $("#btnsave").attr("data-trackingIdDomicilio");
                var domicilioId = $("#btnsave").attr("data-domicilioId");
                var habilitado = $("#btnsave").attr("data-habilitado");

                if (id == "") {
                    habilitado = true;
                }
               
                datos = [
                    id = id,
                    tracking = tracking,
                    nombre = $("#ctl00_contenido_centro").val(),
                    telefono = $("#ctl00_contenido_telefono").val(),
                    director = $("#ctl00_contenido_director").val(),
                    claverni = $("#ctl00_contenido_claverni").val(),
                    mujeres = $("#ctl00_contenido_cupomujere").val(),
                    hombres = $("#ctl00_contenido_cupohombre").val(),
                    pais = $("#ctl00_contenido_pais").val(),
                    estado = $("#ctl00_contenido_estado").val(),
                    municipio = $("#ctl00_contenido_municipio").val(),
                    colonia = $("#ctl00_contenido_colonia").val(),
                    domicilio = $("#ctl00_contenido_domicilio").val(),
                    trackingIdDomicilio = trackingIdDomicilio,
                    domicilioId = domicilioId,
                    localidad=$("#ctl00_contenido_localidadn").val()
                ];

                if (validar()) {
                    Save(datos);
                }

            });

            function limpiar() {
            
                $('#ctl00_contenido_colonia').parent().removeClass('state-success');
                $('#ctl00_contenido_colonia').parent().removeClass('state-error');
                $('#ctl00_contenido_municipio').parent().removeClass('state-success');
                $('#ctl00_contenido_municipio').parent().removeClass('state-error');
                $('#ctl00_contenido_domicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_domicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_telefono').parent().removeClass('state-success');
                $('#ctl00_contenido_telefono').parent().removeClass("state-error");
                $('#ctl00_contenido_claverni').parent().removeClass('state-success');
                $('#ctl00_contenido_claverni').parent().removeClass("state-error");
                $('#ctl00_contenido_cupohombre').parent().removeClass('state-success');
                $('#ctl00_contenido_cupohombre').parent().removeClass("state-error");
                $('#ctl00_contenido_cupomujere').parent().removeClass('state-success');
                $('#ctl00_contenido_cupomujere').parent().removeClass("state-error");
                $('#ctl00_contenido_director').parent().removeClass('state-success');
                $('#ctl00_contenido_director').parent().removeClass("state-error");
                $('#ctl00_contenido_centro').parent().removeClass('state-success');
                $('#ctl00_contenido_centro').parent().removeClass("state-error");
                $('#ctl00_contenido_estado').parent().removeClass('state-success');
                $('#ctl00_contenido_estado').parent().removeClass("state-error");

                $("#ctl00_contenido_estado").empty();
                $("#ctl00_contenido_municipo").empty();
                $("#ctl00_contenido_colonia").empty();
                $("#ctl00_contenido_estado").val("");
                $("#ctl00_contenido_municipio").val(0);
                $("#ctl00_contenido_colonia").val("");
            }

            function validar() {
                var esvalido = true;

                if ($("#ctl00_contenido_centro").val().split(" ").join("") == "") {

                    if ($("#ctl00_contenido_centro").val().split(" ").join("") == "") {

                        ShowError("Institución", "La institución es obligatoria.");
                        $('#ctl00_contenido_centro').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_centro').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_centro').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_centro').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_domicilio").val().split(" ").join("") == "") {

                      if ($("#ctl00_contenido_domicilio").val().split(" ").join("") == "") {

                            ShowError("Domicilio", "El domicilio es obligatorio.");
                            $('#ctl00_contenido_domicilio').parent().removeClass('state-success').addClass("state-error");
                            $('#ctl00_contenido_domicilio').removeClass('valid');
                            esvalido = false;
                        }
                        else {
                            $('#ctl00_contenido_domicilio').parent().removeClass("state-error").addClass('state-success');
                            $('#ctl00_contenido_domicilio').addClass('valid');
                        }
                    }
                
                if ($("#ctl00_contenido_director").val().split(" ").join("") == "") {

                    if ($("#ctl00_contenido_director").val().split(" ").join("") == "") {

                        ShowError("Encargado", "El encargado es obligatorio.");
                        $('#ctl00_contenido_director').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_director').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_director').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_director').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_telefono").val().split(" ").join("") == "") {

                    if ($("#ctl00_contenido_telefono").val().split(" ").join("") == "") {

                        ShowError("Teléfono", "El teléfono es obligatorio.");
                        $('#ctl00_contenido_telefono').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_telefono').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_telefono').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_telefono').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_claverni").val().split(" ").join("") == "") {

                    if ($("#ctl00_contenido_claverni").val().split(" ").join("") == "") {

                        ShowError("Clave / nombre corto", "La clave ó nombre corto es obligatorio(a).");
                        $('#ctl00_contenido_claverni').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_claverni').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_claverni').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_claverni').addClass('valid');
                    }
                }
                if ($("#ctl00_contenido_cupohombre").val().split(" ").join("") == "") {

                    if ($("#ctl00_contenido_cupohombre").val().split(" ").join("") == "") {

                        ShowError("Cupo hombres", "El cupo hombres es obligatorio.");
                        $('#ctl00_contenido_cupohombre').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_cupohombre').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_cupohombre').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_cupohombre').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_cupomujere").val().split(" ").join("") == "") {

                    if ($("#ctl00_contenido_cupomujere").val().split(" ").join("") == "") {

                        ShowError("Cupo mujeres", "El cupo mujeres es obligatorio.");
                        $('#ctl00_contenido_cupomujere').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_cupomujere').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_cupomujere').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_cupomujere').addClass('valid');
                    }
                }
                if ($("#ctl00_contenido_pais").val() == "73") {
                    if ($("#ctl00_contenido_estado").val() == null || $("#ctl00_contenido_estado").val() == "0") {
                        ShowError("Estado", "El estado es obligatorio.");
                        $('#ctl00_contenido_estado').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_estado').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_estado').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_estado').addClass('valid');
                    }
                    if ($("#ctl00_contenido_municipio").val() == null || $("#ctl00_contenido_municipio").val() == "0") {
                        ShowError("Municipio", "El municipio es obligatorio.");
                        $('#ctl00_contenido_municipio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_municipio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_municipio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_municipio').addClass('valid');
                    }
                }
                if ($("#ctl00_contenido_pais").val() == "73") {
                   
                    if ($("#ctl00_contenido_colonia").val() == null || $("#ctl00_contenido_colonia").val() == "0") {
                        ShowError("Colonia", "La colonia es obligatoria.");
                        $('#ctl00_contenido_colonia').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_colonia').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_colonia').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_colonia').addClass('valid');
                    }
                }

                return esvalido;
            }

            function Save(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "institucion.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modal").modal("hide");
                            $('#main').waitMe('hide');
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');


                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("#ctl00_contenido_pais").change(function () {
                if ($("#ctl00_contenido_pais option:selected").val() != 73) {
                 
                    $('#sectionmunicipio').hide();
                    $('#sectionestado').hide();
                    $('#sectioncoloniaDomicilio').hide();
                    $('#sectionlocalidadn').show();
                    
                    $('#sectioncolonia').hide();
                }
                else {
                    
                    $('#sectionestado').show();
                    $('#sectionmunicipio').show();
                    $('#sectioncolonia').show();
                    $('#sectionlocalidadn').hide();
                    CargarEstado(0, '#ctl00_contenido_estado', $("#ctl00_contenido_pais").val());
                }


            });

            $("#ctl00_contenido_estado").change(function () {
                CargarMunicipio(0, '#ctl00_contenido_municipio', $("#ctl00_contenido_estado").val());
            });

            $("#ctl00_contenido_municipio").change(function () {               
                CargarColonia("0", $("#ctl00_contenido_municipio").val());
            });

            $("#btndelete").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "institucion.aspx/delete",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            ShowSuccess("¡Bien hecho!", "El registro fue eliminado del catálogo.");
                            $("#delete-modal").modal("hide");
                            $('#main').waitMe('hide');
                        }
                        else {
                            ShowError("Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");

                        }

                    }
                });
            });


            $("body").on("click", ".historial", function () {
                var id = $(this).attr("data-id");
                $('#ctl00_contenido_idhistory').val(id);
                var descripcion = $(this).attr("data-value");
                $("#descripcionhistorial").text(descripcion);
                $("#history-modal").modal("show");

                window.emptytablehistory = false;
                window.tablehistory.api().ajax.reload();

            });

            var responsiveHelper_dt_basichistory = undefined;
            var breakpointHistoryDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistory = true;
            window.tablehistory = $('#dt_basichistory').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[3, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistory) {
                        responsiveHelper_dt_basichistory = new ResponsiveDatatablesHelper($('#dt_basichistory'), breakpointHistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistory.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistory.respond();
                    $('#dt_basichistory').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "institucion.aspx/getlistlog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basichistory').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                      
                        var centroid = $('#ctl00_contenido_idhistory').val();
                        parametrosServerSide.centroid = centroid;
                        parametrosServerSide.todoscancelados = false;
                        parametrosServerSide.emptytable = window.emptytablehistory;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        orderable: false
                    },
                    null,
                    {
                        name: "fecha",
                        data: "Fec_Movto",
                        orderable: false
                    }
                ],
                columnDefs: [

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {


                            return row.CreadoPor != null ? row.CreadoPor : '';


                        }
                    }
                ]
            });

            $("body").on("click", ".historialcancelado", function () {
                $("#historycanceled-modal").modal("show");

                window.emptytablehistorydeleted = false;
                window.tablehistorydeleted.api().ajax.reload();
            });

            var responsiveHelper_dt_basichistorydeleted = undefined;
            window.emptytablehistorydeleted = true;

            window.tablehistorydeleted = $('#dt_basichistorydeleted').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[4, 'asc'], [3, 'desc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistorydeleted) {
                        responsiveHelper_dt_basichistorydeleted = new ResponsiveDatatablesHelper($('#dt_basichistorydeleted'), breakpointHistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistorydeleted.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistorydeleted.respond();
                    $('#dt_basichistorydeleted').waitMe('hide');
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(4, { page: 'current' }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"><i class="fa fa-info-circle"></i>&nbsp;<strong>Eliminados</strong></td></tr>'
                            );

                            last = group;
                        }
                    });
                },

                ajax: {
                    type: "POST",
                    url: "institucion.aspx/getlistlog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basichistorydeleted').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                    

                        parametrosServerSide.centroid = 0;
                        parametrosServerSide.todoscancelados = true;
                        parametrosServerSide.emptytable = window.emptytablehistorydeleted;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: false
                    },

                    {
                        name: "Accion",
                        data: "Accion",
                        orderable: false
                    },
                    null,
                    null,

                    {
                        targets: 4,
                        "class": "details-control",
                        orderable: false,
                        data: null,
                        visible: false,
                        name: "C.Id",
                        data: "Id"
                    }
                ],
                columnDefs: [

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {



                            return row.CreadoPor != null ? row.CreadoPor : '';


                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            return row.Fec_Movto != null ? toDateTime(row.Fec_Movto) : '';

                        }
                    }
                ]
            });

            init();
            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }

            }

        });


        
         $('#ctl00_contenido_cupohombre').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
        });
          $('#ctl00_contenido_cupomujere').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
        });
    </script>
</asp:Content>