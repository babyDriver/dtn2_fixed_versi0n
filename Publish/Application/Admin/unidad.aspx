﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="unidad.aspx.cs" Inherits="Web.Application.Admin.unidad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li >Administración</li>
     <li>Unidad</li>
    <li>Unidad</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
         }
      
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                 <i class="fa fa-group fa-fw "></i>
                Catálogo de unidad
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
        </div>
    </div>
    
    <p></p>
    <section id="widget-grid" class="">
        <div class="row">
           
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-motivo-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Unidades </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%" >
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th data-class="expand">Nombre</th>
                                        <th data-hide="phone,tablet">Descripción</th>                                        
                                        <th data-hide="phone,tablet">Corporación</th>
                                        <th data-hide="phone,tablet">Placa</th> 
                                        <th data-hide="phone,tablet">Acciones</th>                                       
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
   
    <div class="modal fade" id="form-modal"  data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"> <%--///////////////////////////////////////////////////////////--%>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="input" style="color:dodgerblue">Nombre <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="nombre" runat="server" id="itemnombre" class="alphanumeric alptext" placeholder="Nombre" maxlength="250" />
                                        <b class="tooltip tooltip-bottom-right" >Ingrese el nombre </b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input" style="color:dodgerblue">Descripción <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="descripcion" runat="server" id="itemdescripcion" class="alphanumeric alptext" placeholder="Descripción" maxlength="250" />
                                        <b class="tooltip tooltip-bottom-right" >Ingrese la descripción</b>
                                    </label>
                                </section>  
                                <section>
                                    <label class="input" style="color:dodgerblue">Corporación <a style="color:red">*</a></label>
                                    <select name="corporacion" id="corporacion"  class="select2"> 
                                    </select>
                                </section>
                                <section>
                                    <label class="input" style="color:dodgerblue">Placa</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="placa" runat="server" id="placa" class="alphanumeric alptext" placeholder="Placa" maxlength="14" />
                                        <b class="tooltip tooltip-bottom-right" >Ingrese la placa</b>
                                    </label>
                                    
                                </section>
                                <input type="hidden" name="itemhabilitado" id="itemhabilitado" value="0" />                              
                            </div>
                        </fieldset>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="history-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="history" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistory" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th>Movimiento</th>
                                        <th data-hide="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="blockitem-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="idhistory" runat="server" />
    <input type="hidden" id="hideid" runat="server" />
    <input type="hidden" id="hide"   runat="server" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">

        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#form-modal").is(":visible")) {
                    document.getElementsByClassName("save")[0].click();
                }
            }
        });
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $('.input-number').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            });

            //$("#multaMaximos").on({
            //    "focus": function (event) {
            //        $(event.target).select();
            //    },
            //    "keyup": function (event) {
            //        $(event.target).val(function (index, value) {
            //            return value.replace(/\D/g, "")
            //            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            //                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            //        });
            //    }
            //});

            //$("#multaMinimos").on({
            //    "focus": function (event) {
            //        $(event.target).select();
            //    },
            //    "keyup": function (event) {
            //        $(event.target).val(function (index, value) {
            //            return value.replace(/\D/g, "")
            //            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            //                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            //        });
            //    }
            //});
            CargarCorporacion("0");
            function CargarCorporacion(set) {
                $('#corporacion').empty();
                $.ajax({

                    type: "POST",
                    url: "unidad.aspx/getcorporacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#corporacion');

                        Dropdown.append(new Option("[Corporación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de corporaciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }


             var responsiveHelper_dt_basichistory = undefined;
            var breakpointHistoryDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistory = true;
            window.tablehistory = $('#dt_basichistory').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                "order": [[3, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistory) {
                        responsiveHelper_dt_basichistory = new ResponsiveDatatablesHelper($('#dt_basichistory'), breakpointHistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistory.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistory.respond();
                    $('#dt_basichistory').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "catalogo.aspx/getcatalogolog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basichistory').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        table = $('#ctl00_contenido_tipocatalogo').val();
                        var catalogoid = $('#ctl00_contenido_idhistory').val();
                        parametrosServerSide.catalogoid = catalogoid;
                        parametrosServerSide.table = "unidad";
                        parametrosServerSide.todoscancelados = false;
                        parametrosServerSide.emptytable = window.emptytablehistory;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        orderable: false
                    },
                    null,
                    {
                        name: "fecha",
                        data: "Fec_Movto",
                        orderable: false
                    }
                ],
                columnDefs: [

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {


                            return row.CreadoPor != null ? row.CreadoPor : '';


                        }
                    }

                ]
            });

            $("body").on("click", ".historialcancelado", function () {
                $("#historycanceled-modal").modal("show");

                window.emptytablehistorydeleted = false;
                window.tablehistorydeleted.api().ajax.reload();
            });

            var responsiveHelper_dt_basichistorydeleted = undefined;
            window.emptytablehistorydeleted = true;

            window.tablehistorydeleted = $('#dt_basichistorydeleted').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[4, 'asc'], [3, 'desc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistorydeleted) {
                        responsiveHelper_dt_basichistorydeleted = new ResponsiveDatatablesHelper($('#dt_basichistorydeleted'), breakpointHistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistorydeleted.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistorydeleted.respond();
                    $('#dt_basichistorydeleted').waitMe('hide');
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(4, { page: 'current' }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"><i class="fa fa-info-circle"></i>&nbsp;<strong>Eliminados</strong></td></tr>'
                            );

                            last = group;
                        }
                    });
                },

                ajax: {
                    type: "POST",
                    url: "catalogo.aspx/getcatalogolog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basichistorydeleted').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        table = $('#ctl00_contenido_tipocatalogo').val();

                        parametrosServerSide.catalogoid = 0;
                        parametrosServerSide.table = table;
                        parametrosServerSide.todoscancelados = true;
                        parametrosServerSide.emptytable = window.emptytablehistorydeleted;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: false
                    },

                    {
                        name: "Accion",
                        data: "Accion",
                        orderable: false
                    },
                    null,
                    null,

                    {
                        targets: 4,
                        "class": "details-control",
                        orderable: false,
                        data: null,
                        visible: false,
                        name: "C.Id",
                        data: "Id"
                    }
                ],
                columnDefs: [

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {



                            return row.CreadoPor != null ? row.CreadoPor : '';


                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            return row.Fec_Movto != null ? toDateTime(row.Fec_Movto) : '';

                        }
                    }
                ]
            });

            $(document).on('keydown', 'input[pattern]', function(e){
              var input = $(this);
              var oldVal = input.val();
              var regex = new RegExp(input.attr('pattern'), 'g');

              setTimeout(function(){
                var newVal = input.val();
                if(!regex.test(newVal)){
                  input.val(oldVal); 
                }
              }, 0);
            });
            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(0).addClass('strikeout');
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');                                                
                        $('td', row).eq(3).addClass('strikeout');                                                
                        $('td', row).eq(4).addClass('strikeout');
                        //$('td', row).eq(5).addClass('strikeout');                                                
                    }
                },
                ajax: {
                    type: "POST",
                    url: "unidad.aspx/getcatalogo",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });                       
                        parametrosServerSide.emptytable = window.emptytable;
                        return JSON.stringify(parametrosServerSide);
                    }
                   
                },
                columns: [
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Descripcion",
                        data: "Descripcion"
                    }, 
                     {
                        name: "Corporacion",
                        data: "Corporacion"
                    },
                    {
                        name: "Placa",
                        data: "Placa"
                    }, 
                    
                    null
                ],
                 
              
                columnDefs: [
                 {
                         data: "TrackingId",
                         targets: 0,
                         orderable: false,
                         visible: false,
                         render: function (data, type, row, meta) {
                             return "";
                         }
                     },

                 {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 6,
                        orderable: false,
                         render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "";
                            var editar = "";
                            var habilitar = "";
                          
                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                edit = "edit";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                edit = "disabled";
                            }

                            if($("#ctl00_contenido_KAQWPK").val() == "true")editar='<a class="btn btn-primary btn-circle '+edit+'" href="javascript:void(0);" data-id="' + row.Id    + '" data-value = "' + row.Nombre + '" data-descripcion="' + row.Descripcion + '" data-CorporacionId="' + row.CorporacionId    +     '" data-tracking="' + row.TrackingId+ '" data-placa="' + row.Placa + '" data-habilitado="' + row.Habilitado + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-id="' + row.Nombre + '" data-tracking="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                            return editar +
                                   habilitar +
                                   '<a name="history" class="btn btn-default btn-circle historial" href="javascript:void(0);" data-value="' + row.Nombre + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                        }
                    }
                ]
              
            });

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");
            });

            $("body").on("click", ".blockuser", function () {
                var usernameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockuser-modal").modal("show");
            });   

            init();
            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }

            }

               $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-id");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-tracking", $(this).attr("data-tracking"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingId = $(this).attr("data-tracking");
                startLoading();
                $.ajax({
                    url: "unidad.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingId: trackingId
                        
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            $('#dt_basic').DataTable().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            ShowSuccess("¡Bien hecho!", data.msg);
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }
                     
                });
                
            });

                $("body").on("click", ".historial", function () {
                var id = $(this).attr("data-id");
                $('#ctl00_contenido_idhistory').val(id);
                var descripcion = $(this).attr("data-value");
                $("#descripcionhistorial").text(descripcion);
                $("#history-modal").modal("show");

                window.emptytablehistory = false;
                window.tablehistory.api().ajax.reload();

            });

            $("body").on("click", ".edit", function () {
                limpiar();                
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");            
           
                $("#btnsave").attr("data-id", id);                 
                
                $("#btnsave").attr("data-tracking", tracking);
                $("#ctl00_contenido_itemnombre").val($(this).attr("data-value"));
                $("#ctl00_contenido_itemdescripcion").val($(this).attr("data-descripcion"));

                $("#corporacion").val($(this).attr("data-CorporacionId"));
                $("#corporacion").change();
                $("#ctl00_contenido_placa").val($(this).attr("data-placa"));


                $('#ctl00_contenido_hideid').val(id);
                $('#ctl00_contenido_hide').val(tracking);
                $("#form-modal").modal("show");
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar registro");
                //CargarContrato(tracking);  
            });

            $("body").on("click", ".add", function () {
                limpiar();
              $("#form-modal").modal("show");
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar registro");
            });

           

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    guardar();
                }
            });

            function validar() {
                var esvalido = true;

                if ($("#ctl00_contenido_itemnombre").val() == "") {
                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#motivo').parent().removeClass('state-success').addClass("state-error");
                    $('#motivo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemnombre').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemnombre').addClass('valid');
                }

                if ($("#ctl00_contenido_itemdescripcion").val() == "") {
                    ShowError("Descripción", "La descripción es obligatoria.");
                    $('#ctl00_contenido_itemdescripcion').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemdescripcion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemdescripcion').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemdescripcion').addClass('valid');
                }

                if ($("#corporacion").val() == "0") {
                    ShowError("Corporación", "La corporación es obligatoria.");
                    $('#corporacion').parent().removeClass('state-success').addClass("state-error");
                    $('#corporacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#corporacion').parent().removeClass("state-error").addClass('state-success');
                    $('#corporacion').addClass('valid');
                }
             
               
                
                return esvalido;
            }

            function limpiar()
            {
                $('#ctl00_contenido_itemnombre').val("");
                $('#ctl00_contenido_itemnombre').parent().removeClass('state-success')
                $('#ctl00_contenido_itemdescripcion').val("");
                $('#ctl00_contenido_itemdescripcion').parent().removeClass('state-success')
                $('#ctl00_contenido_placa').val("");
                $('#ctl00_contenido_placa').parent().removeClass('state-success')
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                $('#corporacion').parent().removeClass('state-success')
                $('#corporacion').parent().removeClass('state-error')
                $('#corporacion').val("0");
                $("#select2-corporacion-container").text("[Corporación]");
            }
            
            function guardar() {
                startLoading();
                
                var contrato = ObtenerValores();
                $.ajax({
                    type: "POST",
                    url: "unidad.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ '_unidad': contrato }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modal").modal("hide");
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La unidad se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La unidad se  " + resultado.mensaje + " correctamente.");                            
                            
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });

                limpiar();
            }

            function CargarContrato(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "motivo.aspx/getContract",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;                        
                         $("#motivo").val(data.Motivo);
                         $("#descripcion").val(data.Descripcion);                         
                         $("#articulo").val(data.Articulo); 
                         $("#multa").val(data.Multa); 
                         $("#horaArresto").val(data.horaArresto); 
                         $('#ctl00_contenido_hideid').val(data.Id);
                        
                         $('#ctl00_contenido_hide').val(data.TrackingId);
                        
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function ObtenerValores() {                                
                var contrato = {                    
                    Nombre: $('#ctl00_contenido_itemnombre').val(),
                    Descripcion: $('#ctl00_contenido_itemdescripcion').val(),
                    Placa: $('#ctl00_contenido_placa').val(),
                    Id: $('#ctl00_contenido_hideid').val(),
                    TrackingId: $('#ctl00_contenido_hide').val(),
                    CorporacionId:$('#corporacion').val()
                };
                return contrato;
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>

