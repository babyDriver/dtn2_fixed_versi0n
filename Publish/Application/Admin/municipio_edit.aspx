﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="municipio_edit.aspx.cs" Inherits="Web.Application.Admin.municipio_edit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li style="color:silver">Administración</li>
    <li style="color:silver">Localización</li>
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Admin/municipio.aspx">Municipio</a></li>
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Admin/municipio_edit.aspx">Registro</a></li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>
        .input-group-addon
        {
             background-color:#FFF;
        }

        .input-group .input-group-addon + .form-control
        {
            border-left:none;
            height:32px;
        }
    </style>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-municipio-edit-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-edit"></i></span>
                    <h2>Municipio </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">                        
                        <div id="smart-form-register" class="smart-form">
                            <header id="header_form">
                                Formulario de registro
                            </header>
                           <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="input">País <a style="color:red">*</a></label>                                        
                                        <select name="pais" id="pais" style="width: 100%" class="select2">
                                        </select>                                                                                    
                                    </section>
                                    <section class="col col-4">
                                        <label class="input">Estado <a style="color:red">*</a></label>                                        
                                        <select name="estado" id="estado" style="width: 100%" class="select2">
                                        </select>                                                                                    
                                    </section>
                                    <section class="col col-4">
                                        <label class="input">Municipio <a style="color:red">*</a></label>                                        
                                        <label class="input">
                                            <i class="icon-append fa fa-book"></i>
                                            <input type="text" style="height:34px;"  name="municipio" class="alptext" id="municipio" placeholder="Municipio" maxlength="256" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el municipio.</b>
                                        </label>                                                                             
                                    </section>
                                   </div>
                               <div class="row">
                                     <section class="col col-4">
                                        <label>Descripción</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-building"></i>
                                            <input type="text" name="descripcion" id="descripcion" placeholder="Descripción" class="alptext" maxlength="255" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la descripción.</b>
                                        </label>
                                      
                                    </section>
                                    
                                    <section class="col col-4">
                                        <label>Clave <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-hashtag"></i>
                                            <input type="text" name="clave" id="clave" placeholder="Clave del municipio" class="alptext" maxlength="5" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la clave.</b>
                                        </label>
                                    </section>
                                    
                                    <%--<section class="col col-4">
                                    <label>Salario mínimo <a style="color:red">*</a></label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input  title="" style="height:30px;" type ="text" name="salario" id="salario" placeholder="Salario mínimo" pattern="^\d*(\.\d{0,2})?$" maxlength="256" class="form-control alphanumeric"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el salario mínimo.</b>
                                    </div>
                                </section>--%>
                                        
                                </div>
                                </fieldset>
                                <footer>
                                    <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                        <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                        <a style="float: none;" href="municipio.aspx" class="btn btn-sm btn-default"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                                
                                    </div>
                                </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
        </section>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="hideid" runat="server" value="" />
    <input type="hidden" id="hide" runat="server" value="" />
 </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                document.getElementsByClassName("save")[0].click();
            }
        });
        $(document).ready(function () {

            pageSetUp();              

            $('.input-number').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            });

            $("#pais").change(function () {
                loadStates("0", $("#pais").val());
            });

          $("#estado").change(function () {
                //loadCities("0", $("#estado").val());
            });

            $("#pais").select2();
            $("#estado").select2();       

            $("#salario").on({
                "focus": function (event) {
                    $(event.target).select();
                },
                "keyup": function (event) {
                    $(event.target).val(function (index, value) {
                        return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }
            });
          
            //loadCountries("0");                        

            function loadCountries(setvalue) {

                $.ajax({                    
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Admin/colonia_edit.aspx/getCountries",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false
                    //success: function (response) {
                    //    var Dropdown = $('#pais');
                    //    Dropdown.children().remove();
                    //    Dropdown.append(new Option("[País]", "0"));
                    //    $.each(response.d, function (index, item) {
                    //        Dropdown.append(new Option(item.Desc, item.Id));
                    //    });

                    //    if (setvalue != "") {
                    //        Dropdown.val(setvalue);
                    //        Dropdown.trigger("change.select2");
                    //    }
                    //},
                    //error: function () {
                    //    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    //}
                }).done(function (response) {
                    var Dropdown = $('#pais');
                    Dropdown.children().remove();
                    Dropdown.append(new Option("[País]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setvalue != "") {
                        Dropdown.val(setvalue);
                       
                        Dropdown.trigger("change.select2");
                    }                    
                });
            }

           function loadStates(setvalue, idPais) {

                $.ajax({                    
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Admin/colonia_edit.aspx/getStates",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idPais: idPais
                    }),
                    cache: false
                }).done(function (response) {
                    var Dropdown = $('#estado');
                    Dropdown.children().remove();
                    Dropdown.append(new Option("[Estado]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setvalue != "") {
                        Dropdown.val(setvalue);
                        Dropdown.trigger("change.select2");
                    }
                });
            }

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                $("#ctl00_contenido_usuario").val("");
                $("#ctl00_contenido_nombre").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#ctl00_contenido_email").val("");                                                                
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    guardar();
                }
            });            

             function Cargarmunicipio(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Admin/municipio_edit.aspx/GetMunicipio",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;                         
                         loadCountries(data.PaisId);
                         
                         data.IdPais=data.PaisId
                         loadStates(data.EstadoId, data.IdPais);
                         //loadCities(data.IdMunicipio, data.IdEstado);
                         $("#municipio").val(data.Municipio);
                         $("#descripcion").val(data.Descripcion);
                         //$("#salario").val(data.Salario);
                         
                         $("#clave").val(data.Clave);                                                  
                         $('#ctl00_contenido_hideid').val(data.Id);
                         $('#ctl00_contenido_hide').val(data.TrackingId);                         
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function validar() {                
                var esvalido = true;                

                if ($('#pais').val() == "0" || $('#pais').val() == null) {
                    ShowError("País", "El país es obligatorio.");
                    $("#pais").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#pais").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }
                   if ($('#estado').val() == "0" || $('#estado').val() == null) {
                    ShowError("Estado", "El estado es obligatorio.");
                    $("#estado").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#estado").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#municipio').val() == "" || $('#municipio').val() == null) {
                    ShowError("Municipio", "El municipio es obligatorio.");
                    $('#municipio').parent().removeClass('state-success').addClass("state-error");
                    $('#municipio').removeClass('valid');
                    esvalido = false;
                }
                else {
                     $('#municipio').addClass('valid');
                }

               

                      

                if ($("#clave").val().split(" ").join("") == "") {
                    ShowError("Clave", "La clave es obligatoria.");
                    $('#clave').parent().removeClass('state-success').addClass("state-error");
                    $('#clave').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#clave').parent().removeClass("state-error").addClass('state-success');
                    $('#clave').addClass('valid');
                } 

                //if ($("#salario").val().split(" ").join("") == "") {
                //    ShowError("salario", "El salario mínimo es obligatorio");
                //    $('#salario').parent().removeClass('state-success').addClass("state-error");
                //    $('#salario').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#salario').parent().removeClass("state-error").addClass('state-success');
                //    $('#salario').addClass('valid');
                //} 

                return esvalido;
            }

            function guardar() {
                startLoading();
                var municipio = ObtenerValores();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Admin/municipio_edit.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'municipio': municipio }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            ShowSuccess("¡Bien hecho!", "La información del municipio se  " + resultado.mensaje + " correctamente.");                            
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ObtenerValores() {                                
                var municipio = {
                    PaisId:$('#pais').val(),
                    EstadoId: $("#estado").val(),
                    Municipio:$("#municipio").val(),
                    Descripcion: $("#descripcion").val(),
                    Clave: $("#clave").val(),
                    //Salario: $("#salario").val(),
                    Salario: 0,
                    Id: $('#ctl00_contenido_hideid').val(),
                    TrackingId: $('#ctl00_contenido_hide').val()
                };
                return municipio;
            }

            function init() {
                var param = RequestQueryString("tracking");                
                if (param != undefined) {                    
                    Cargarmunicipio(param);
                }
                else {
                    loadCountries("0");                    
                }                                
            }

            init();
        });
    </script>
</asp:Content>
