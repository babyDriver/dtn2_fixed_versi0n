﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="Trabajosocial.aspx.cs" Inherits="Web.Application.Trabajo_social.Trabajosocial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
  <li>Trabajo social</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        .dropdown-menu {
            position: relative;
        }
        td.strikeout {
            text-decoration: line-through;
        }
        #dropdown {
            width: 439px;
        }
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }
        #mapid {
            height: 20vw;
            width: 100%;
        }
        .coordinates {
            background: rgba(0,0,0,0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding: 5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }
        .modal {
            overflow-y: auto;
        }
        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 13px;
            line-height: 1.42857143;
        }
        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .same-height-thumbnail {
            height: 150px;
            margin-top: 0;
        }
        #ScrollableContent {
            height: calc(100vh - 140px);
        }
        #registroExpedienteModalTamanio {
            width: 75vw;
        }
    </style>
    <!--<div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
                Trabajo social
            </h1>
        </div>
    </div>-->
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry" style="display: none;">
        <section class="col col-4">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
              <!--  <a href="javascript:void(0);" class="btn btn-md btn-default calculate" id="calculate"><i class="fa fa-calculator"></i>&nbsp;Cálculo de porcentaje compurgado </a>-->
            </div>
        </section>
        <section class="col col-6">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <%--<a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Sentence/celdas.aspx"" class="btn btn-md btn-default" id="celdas"><i class="fa"></i>Celdas </a>--%>
            </div>
        </section>
    </div>
    <div class ="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a class="btn btn-md btn-default" href="javascript:void(0);" id="imprimir"><i class="fa fa-print"></i>&nbsp; Imprimir</a>
            <a class="btn btn-primary dropdown-toggle" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/ReporteTrabajoSocial.aspx" id="rpt">Reporte trabajo social </a>
           <%-- <div class="btn-group">
            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"  >Reportes trabajo social
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li >
                    <a class="btn-sm " title="Examen médico" href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/ReporteTrabajoSocial.aspx">
                        Reporte de trabajo social
                    </a>
                </li>
            </ul>
        </div>--%>
               <!-- <a class="btn btn-md btn-default" id="salidaefectuada"><i class="fa fa-sign-out"> </i>&nbsp;Salida efecutada</a>
                <a class="btn btn-md btn-default" id="salidaefectuadajuez"><i class="fa fa-sign-out"> </i>&nbsp;Salida por juez</a>-->
        </article>
    </div>
    <section id="widget-grid" class="">
        <div class="scroll" id="ScrollableContent">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px;">
                <br />
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Listado de personas en trabajo social</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" >
                            <div class="row">
                                <table id="dt_basic" style="width: 100%;" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th data-class="expand">#</th>
                                            <th>Fotografía</th>
                                            <th >Nombre</th>
                                            <th>Apellido paterno</th>
                                            <th data-hide="phone,tablet">Apellido materno</th>
                                            <th data-hide="phone,tablet">No. remisión</th>
                                            <th data-hide="phone,tablet">Situación de la persona</th>
                                            <th data-hide="phone,tablet">Edad</th>
                                            <th data-hide="phone,tablet">Acciones</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
 
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="WERQEQ" runat="server" value=""/>

    <div id="printpdf-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Reporte de extravío</h4>
                </div>
                <div class="modal-body">
                    <div id="printpdf-modal-body" class="smart-form">
                        <div class="modal-body">
                            <div id="printpdf-modal-body-form" class="smart-form">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="row">
                                                <section class="col col-10">
                                                     <img id="avatar" class="img-thumbnail" alt=""  src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" width="120" height="120" />
                                                </section>
                                            </div>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Nombre <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" readonly="true" name="nombrereportepdf" id="nombrereportepdf" placeholder="Nombre" />
                                                        <b class="tooltip tooltip-bottom-right">Nombre</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Edad <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="edadreportepdf" id="edadreportepdf" placeholder="Edad" class="integer" maxlength="2" />
                                                        <b class="tooltip tooltip-bottom-right">Edad</b>
                                                    </label>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioBuscandopdf" value="BUSCADO(A)" /><i></i>Buscado
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioExtraviadopdf" value ="EXTRAVIADO(A)" /><i></i>Extraviado
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioInformacionpdf" value="INFORMACION" /><i></i>Información
                                            </label>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Alias <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="aliasreportepdf" id="aliasreportepdf" class="alptext" placeholder="Alias" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Alias</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Violento</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="violentopdf" id="violentopdf" class="alptext" placeholder="Violento" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Violento</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Enfermo mental</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="enfermopdf" id="enfermopdf" class="alptext" placeholder="Enfermo mental" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Enfermo mental</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Señas particulares</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="textarea">
                                                        <textarea rows="3" name="senaspdf" id="senaspdf" class="alptext" placeholder="Señas particulares" maxlength="500"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Señas particulares</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Comentarios</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="textarea">
                                                        <textarea rows="3" name="comentariospdf" class="alptext" id="comentariospdf" placeholder="Comentarios" maxlength="500"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Comentarios</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-9">
                                                    <label>Información de contacto:</label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Institución</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="institucionpdf" class="alptext" id="institucionpdf" placeholder="Institución" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Institución para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Dirección</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="direccionpdf" id="direccionpdf" placeholder="Dirección" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Dirección para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Teléfono</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="tel" data-mask="(999) 999-9999" name="telefonopdf" id="telefonopdf" placeholder="Telefono" maxlength="50" />
                                                        <b class="tooltip tooltip-bottom-right">Teléfono para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <input type="hidden" id="trackingidpdf"/>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default printpdfbtn" id="printpdfbtn"><i class="fa fa-file-pdf-o"></i>&nbsp;Generar reporte </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="cancelarpdfbtn"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="icon-append fa fa-user"></i>&nbsp;Datos personales</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Nombre completo:</label>
                            <div class="col-md-7">
                                <input type="text" name="nombreInterno" id="nombreInterno" disabled="disabled" class="form-control c" style="margin-bottom:20px;" />                                    
                            </div>                                
                        </div>                                
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Edad:</label>
                            <div class="col-md-7">
                                <input type="text" name="edad" id="edad" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>                                              
                        </div> 
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Alias:</label>
                            <div class="col-md-7">
                                <input type="text" name="alias" id="alias" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>                                              
                        </div> 
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Fecha y hora de registro:</label>
                            <div class="col-md-7">
                                <input type="datetime" id="horaRegistro" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right; padding-right: 1px">Motivo:</label>
                            <div class="col-md-7">
                                <textarea class="form-control" id="motivo" disabled="disabled"></textarea><p></p>
                            </div>
                        </div>
                        <br />
                        <br />            
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Domicilio:</label>
                            <div class="col-md-7">
                                <%--<texttarea type="text" name="domicilio" id="domicilio" cols="30" rows="5"  disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />--%>                                    
                                <label class="input">
                                    <textarea id="domicilio" disabled="disabled" name="form-control domicilio" class="alptext" cols="40" rows="3"></textarea>                                                            
                                </label> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Nombre de la madre:</label>
                            <div class="col-md-7">
                                <input type="text" name="nombredelamadre" id="nombredelamadre" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Teléfono de la madre:</label>
                            <div class="col-md-7">
                                <input type="tel" data-mask="(999) 999-9999" name="teldelamadre" id="teldelamadre" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Nombre del padre:</label>
                            <div class="col-md-7">
                                <input type="text" name="nombredelpadre" id="nombredelpadre" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Teléfono del padre:</label>
                            <div class="col-md-7">
                                <input type="tel" data-mask="(999) 999-9999" name="teldelpadre" id="teldelpadre" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Nombre del tutor:</label>
                            <div class="col-md-7">
                                <input type="text" name="nombredeltutor" id="nombredeltutor" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Teléfono del tutor:</label>
                            <div class="col-md-7">
                                <input type="tel" data-mask="(999) 999-9999" name="teldeltutor" id="teldeltutor" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Escolaridad:</label>
                            <div class="col-md-7">
                                <input type="text" name="idescolaridad" id="idescolaridad" disabled="disabled" class="form-control alptext" style="margin-bottom:20px;" />                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img id="Img1" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" width="240" height="240"/>
                    </div>                                                                                                                                                    
                </div>
            </div>
        </div>
    </div>

    <div id="RegistroExpediente-modal" class="modal fade" data-backdrop="static">
        <div class="modal-dialog" id="registroExpedienteModalTamanio">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class="fa fa-pencil" +=""></i> Expediente</h4>
                </div>
                <div class="modal-body">
                    <div id="registromovimiento-form" class="smart-form">
                        <div class="modal-body">
                            <div class="row">
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Motivo <a style="color: red">*</a></label>
                                    <select name="motivorehabilitacion" id="motivorehabilitacion" class="select2"></select>
                                    <i></i>
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Adicción <a style="color: red">*</a></label>
                                    <select name="adiccion" id="adiccion" class="select2"></select>
                                    <i></i>
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Pandilla <a style="color: red">*</a></label>
                                    <label class="input">
                                        <label class='input' id='celda'>
                                            <input type="text" name="txtpandilla" id="txtpandilla" class='input' maxlength="50"placeholder="Pandilla"  data-requerido="true"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese la pandilla</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Religión <a style="color: red">*</a></label>
                                    <select name="religion" id="religion" class="select2"></select>
                                    <i></i>
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Escolaridad</label>
                                    <select name="adiccion" id="escolaridad" class="select2"></select>
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Nombre madre </label>
                                    <label class="input">
                                        <label class='input' id='madre'>
                                            <input type="text" name="txtnadre" id="txtmadre" maxlength="50" class='input' placeholder="Nombre madre" data-requerido="true"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el nombre de la madre</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Celular </label>
                                    <label class="input">
                                        <label class='input' id='celmadre'>
                                            <input type="tel" name="txtcelmadre" id="txtcelmadre" runat="server" placeholder="Celular" data-requerido="true" data-mask="(999) 999-9999"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el celular</b>
                                        </label>
                                    </label>
                                    &nbsp;
                                    <i></i>
                                </section>
                                <section class="col" style="width: 60%;">
                                    <label style="color: dodgerblue" class="input">Domicilio de la madre</label>
                                    <label class="textarea">
                                        <textarea id="domiciliomadre" name="domiciliomadre" class="form-control alptext" rows="3" maxlength="250" placeholder="Domiclio de la madre"></textarea>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el domicilio de la madre</b>
                                    </label>                                                        
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Nombre Padre </label>
                                    <label class="input">
                                        <label class='input' id='padre'>
                                            <input type="text" name="txtpadre" id="txtpadre" maxlength="50" class='input alptext' placeholder="Nombre padre" data-requerido="true"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el nombre del padre</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section >
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Celular </label>
                                    <label class="input">
                                        <label class="input" id="celpadre">
                                            <input type="tel" name="txtcelpadre" id="txtcelpadre" class="alptext" runat="server" placeholder="Celular del padre" data-requerido="true" data-mask="(999) 999-9999"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el celular</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <section class="col" style="width: 60%;">
                                    <label style="color: dodgerblue" class="input">Domicilio del padre</label>
                                    <label class="textarea">
                                        <textarea id="domiciliopadre" name="domiciliopadre" maxlength="250" class="form-control alptext" rows="3" placeholder="Domicilio del padre"></textarea>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el domicilio del padre</b>
                                    </label>                                               
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Tutor </label>
                                    <label class="input">
                                        <label class='input' id='tutor'>
                                            <input type="text" name="txttutor" id="txttutor" maxlength="50" class='input alptext' placeholder="Tutor" data-requerido="true"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el tutor</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section >
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Celular </label>
                                    <label class="input">
                                        <label class='input' id='celtutor'>
                                            <input type="tel" name="txtceltutor" id="txtceltutor" runat="server" placeholder="Celular del tutor" data-requerido="true" data-mask="(999) 999-9999"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el celular</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Persona a quien se notifica</label>
                                    <label class="input">
                                        <label class='input' id='nitificacion'>
                                            <input type="text" name="txt" id="txtnotificacion" maxlength="50" class='input alptext' placeholder="Persona a quien se notifica" data-requerido="true"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese la persona a quien se notifica</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <section class="col" style="width: 20%;">
                                    <label style="color: dodgerblue" class="input">Celular </label>
                                    <label class="input">
                                        <label class="input" id="notificadoceltutor">
                                            <input type="tel" name="txtcelnotificado" id="txtcelnotificado" runat="server" placeholder="Celular de quien se notifica" data-requerido="true" data-mask="(999) 999-9999"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el celular</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <div class="col" style="width: 20%;"></div>
                                <section class="col" style="width: 40%;">
                                    <label style="color: dodgerblue" class="input">Cuadro patológico <a style="color: red">*</a></label>
                                    <label class="textarea">
                                        <textarea id="cuadropatologico" name="cuadropatologico" class="form-control alptext" rows="3" maxlength="255" placeholder="Cuadro patológico"></textarea>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el cuadro patológico</b>
                                    </label>                                                        
                                </section>
                                <section class="col" style="width: 60%;">
                                    <label style="color: dodgerblue" class="input">Observación</label>
                                    <label class="textarea">
                                        <textarea id="observaciones" name="observaciones" class="form-control alptext" rows="3" maxlength="255"></textarea>
                                        <b class="tooltip tooltip-bottom-right">Ingrese la observación</b>
                                    </label>                                                        
                                </section>
                            </div>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveregistro" id="guardarexpediente"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="RegistroSalidaEfectuada-modal" class="modal fade" tabindex="-1" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Salida efectuada</h4>
                </div>
                <div class="modal-body">
                    <div id="registrosalidaefectuada-form" class="smart-form">
                        <div class="modal-body">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-8">
                                        <label class="label">Observación <a style="color: red">*</a></label>
                                        <label class="input">
                                            <textarea id="observacion" name="observacion" class="alptext" cols="60" rows="5"></textarea>                                                            
                                        </label>                                                        
                                        <i></i>
                                    </section>
                                    <section class="col col-8">
                                        <label class="label">Responsable <a style="color: red">*</a></label>
                                        <label class="input">
                                            <label class='input' id='responsableid'>
                                                <input type="text" name="responsable" id="responsable" class='input alptext' placeholder="Responsable" data-requerido="true" />
                                            </label>
                                        </label>
                                        <i></i>
                                    </section >
                                </div>
                            </fieldset>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default GuardaSalidaEfectuada" id="GuardaSalidaEfectuada"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelaSalidaEfecutada"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="RegistroSalidaEfectuadaJuez-modal" class="modal fade" tabindex="-1" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Salida efectuada por juez</h4>
                </div>
                <div class="modal-body">
                    <div id="registrosalidaefectuadaJuez-form" class="smart-form">
                        <div class="modal-body">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-8">
                                        <label class="label">Fundamento <a style="color: red">*</a></label>
                                        <label class="input">
                                            <textarea id="fundamento" name="fundamento" cols="60" class="alptext" rows="5"></textarea>                                                            
                                        </label>                                                        
                                        <i></i>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default GuardaSalidaEfectuada" id="GuardaSalidaEfectuadaJuez"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelaSalidaEfecutadajuez"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="photo-arrested" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src=" <%= ConfigurationManager.AppSettings["relativepath"]  %> #" alt="fotografía del detenido" />
                                </div>
                            </section>
                        </fieldset>
                        <footer style="padding: 0px;">
                            <a href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="Antecedentes-modal" class="modal fade" tabindex="-1" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class=" fa fa-inbox"></i> Antecedentes</h4>
                </div>
                <div class="modal-body">
                    <div id="antecedentes-form" class="smart-form">
                        <div class="modal-body">
                            <div>
                                <div class="jarviswidget-editbox"></div>
                                <div class="widget-body">
                                    <p></p>
                                    <!--<div class="row" id="addfamily" style="display: none;">
                                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                        </div>
                                    </div>-->
                                    <p></p>
                                    <table id="dt_basicAntecedente" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                        <thead>
                                             <tr>
                                                <th></th>
                                                <th data-class="expand">#</th>
                                                <th>Fotografía</th>
                                                <th data-hide="phone,tablet">No. remisión</th>
                                                <th data-hide="phone,tablet">Fecha</th>
                                                <th data-hide="phone,tablet">Centro</th>
                                                <th data-hide="phone,tablet">No. de remisión</th>
                                                <th data-hide="phone,tablet">NCP</th>
                                                <th data-hide="phone,tablet">Estatus</th>
                                                <th data-hide="phone,tablet">Acciones</th>                                       
                                            </tr>
                                        </thead>                               
                                    </table>
                                </div>
                                <br />
                            </div>
                        </div>
                        <footer style="padding: 0px;">
                            <a href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" ><i class="fa fa-close"></i>&nbsp;Cerrar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="Editapesona-modal" class="modal fade"  tabindex="-1" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class="fa fa-pencil" +=""></i> Editar persona</h4>
                </div>
                <div class="modal-body">
                    <div id="editapersona-form" class="smart-form">
                        <div class="modal-body">
                            <fieldset>
                                <div  class="Row">
                                    <section class="col col-8">
										<label style="color: dodgerblue" class="label" >Apellido paterno: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="name" type="text" class="alptext" id="primerapellido"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el apellido paterno.</b>
										</label>
									</section>
									<section class="col col-8">
										<label style="color: dodgerblue" class="label" >Apellido materno: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="segundoapellido" class="alptext" type="text" id="segundoapellido"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el apellido materno.</b>
										</label>
									</section>
                                    <section class="col col-8">
										<label style="color: dodgerblue" class="label" >Nombre: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="nombre" type="text" class="alptext" id="nombre"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el nombre.</b>
										</label>
									</section>   
                                    <section class="col col-8">
										<label style="color: dodgerblue" class="label" >Fecha nacimiento: <a style="color:red">*</a></label>
										<label class="input">
                                            <label class='input-group date' id='FehaHoradatetimepicker'>
                                                <input type="text" name="fechahora" id="fechahora" class='input-group date' placeholder="Fecha nacimiento" data-requerido="true"/>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la fecha de nacimiento.</b>
                                                </span>
                                            </label>
                                        </label>
									</section>
                                </div>
                            </fieldset>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default GuardaSalidaEfectuada" id="guardaeditpersona"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="CeldaTrackingId"  value="" />
    <input type="hidden" id="hide" />
    <input type="hidden" id="hideid"  />
    <input type="hidden" id="TrabajoSocialId"  />
    <input type="hidden" id="expedienteId"  />
    <input type="hidden" id="expedientetrackingId"  />
    <input type="hidden" id="trackingId" runat="server" value="" />
    <input type="hidden" id="Hidden2" runat="server" value="" />
    <input type="hidden" id="Hidden3" runat="server" value=""/>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                if ($("#RegistroExpediente-modal").is(":visible")) {
                    document.getElementById("guardarexpediente").click();
                }
                else if ($("#Editapesona-modal").is(":visible")) {
                    document.getElementById("guardaeditpersona").click();
                }
            }
        });

        $(document).ready(function () {
            $("#Editapesona-modal").on("shown.bs.modal", function () {
                $("#primerapellido").focus();
            });

            $("#RegistroExpediente-modal").on("shown.bs.modal", function () {
                $("#motivorehabilitacion").next().children().children().focus();
            });

            pageSetUp();
            CargarListadoMovimientos(120);
            CargarListadoReligion(23);
            CargarComboAddiccion(122);
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_dt_basicAntecedente = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            var rutaDefaultServer = "";

            getRutaDefaultServer();

            function getRutaDefaultServer() {
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;
                        }
                    }
                });
            }

            function CargaAntecedentes(trackingid) {
                responsiveHelper_dt_basicAntecedente = undefined;
                window.table = $('#dt_basicAntecedente').dataTable({
                    destroy: true,
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basicAntecedente) {
                            responsiveHelper_dt_basicAntecedente = new ResponsiveDatatablesHelper($('#dt_basicAntecedente'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basicAntecedente.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basicAntecedente.respond();
                        $('#dt_basicAntecedente').waitMe('hide');
                    },/*
               "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                        $('td', row).eq(7).addClass('strikeout');
                    }
                },*/
                    ajax: {
                        type: "POST",
                        url: "Trabajosocial.aspx/getantecedente",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basicAntecedente').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.tracking = trackingid;
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            data: "TrackingId",
                            targets: 0,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                return "";
                            }
                        },
                        null,
                        null,
                        {
                            name: "Expediente",
                            data: "Expediente"
                        },
                        null,
                        {
                            name: "Centro",
                            data: "Centro"
                        },
                        {
                            name: "Expediente",
                            data: "Expediente",
                            visible: false
                        },
                        {
                            name: "NCP",
                            data: "NCP",
                            visible: false
                        },
                        {
                            name: "EstatusNombre",
                            data: "EstatusNombre"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            data: "TrackingId",
                            targets: 0,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                return "";
                            }
                        },
                        {
                            targets: 1,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            targets: 2,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                if (row.RutaImagen != null) {
                                    var ext = "." + row.RutaImagen.split('.').pop();
                                    var photo = row.RutaImagen.replace(ext, ".thumb");
                                    var imgAvatar = resolveUrl(photo);
                                    return '<div class="text-center">' +
                                        '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                        '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" />' +
                                        '</a>' +
                                        '<div>';
                                } else { return ''; }
                            }
                        },
                        {
                            targets: 4,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return row.Fecha;
                            }
                        },
                        {
                            targets: 9,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "edit"
                                var transfer = "tranfer";
                                var reentry = "reentry";
                                var registrar = "";
                                var habilitar = "";
                                var egreso = "";
                                var reingreso = "";
                                if (row.Activo) {
                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                }
                                else {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled"; transfer = "disabled"; reentry = "disabled";
                                }

                                if ($("#ctl00_contenido_KAQWPK").val() == "true")
                                    registrar = '<a class="btn btn-primary btn-circle ' + edit + '" href="entry.aspx?tracking=' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                else if ($("#ctl00_contenido_WERQEQ").val() == "true")
                                    registrar = '<a class="btn btn-primary btn-circle ' + edit + '" href="entry.aspx?tracking=' + row.TrackingId + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                                if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-id="' + row.TrackingIdEstatus + '" data-value = "' + row.Expediente + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                                // if ($("#ctl00_contenido_VYXMBM").val() == "true" & row.Estatus != 2 & $("#ctl00_contenido_KAQWPK").val() == "true") { egreso = '<a class="btn btn-danger btn-circle ' + transfer + '" href="transfer.aspx?tracking=' + row.TrackingId + '" title="Egreso"><i class="glyphicon glyphicon-transfer"></i></a>&nbsp;'; }
                                //  if ($("#ctl00_contenido_RAWMOV").val() == "true" & row.Estatus == 2 & $("#ctl00_contenido_KAQWPK").val() == "true") reingreso = '<a class="btn btn-warning btn-circle ' + reentry + '" href="reentry.aspx?tracking=' + row.TrackingId + '" title="Reingreso"><i class="glyphicon glyphicon-share-alt"></i></a>&nbsp;';

                                return registrar + egreso + reingreso + habilitar;
                            }
                        }
                    ]
                });
            }

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY": "350px",
                "scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "Trabajosocial.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },

                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    null,
                    {
                        name: "Edad",
                        data: "Edad"

                    },
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false

                    }
                ],
                columnDefs: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        className: 'select-checkbox',
                        render: function (data, type, row, meta) {
                            return '<div clas="text-center"><input type="checkbox" data-TrackingId="' + row.TrackingId + '" data-internoId="' + row.Id + '" data-TrabajoSocialId="' + row.TrabajoSocialId + '" data-expedienteId="' + row.ExpedienteId + '" data-Nombrecompleto="' + row.Nombre + ' ' + row.Paterno + " " + row.Materno + '" name="elemento1" value="1"/></div>';
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("~/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar" class="img-thumbnail text-center" alt = "" src = "' + pathfoto + '" height = "10" width = "50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';"/>' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var estatus = row.situacion;

                            return estatus;
                        }
                    },
                    //{
                    //    targets: 9,
                    //    orderable: false,
                    //    render: function (data, type, row, meta) {
                    //        if (row.Edad == 0) {
                    //            if (row.Edad2 == 0) return "Sin dato";
                    //            else return row.Edad2;
                    //        }
                    //        else return row.Edad;
                    //    }
                    //},
                    {
                        targets: 10,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var edit = "edit";
                            var editar = "";
                            var color = "";
                            var txtestatus = "";
                            var icon = "";
                            var habilitar = "";

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                            }

                            //   if ($("#ctl00_contenido_KAQWPK").val() == "true")
                            //  editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            // else if($("#ctl00_contenido_WERQEQ").val() == "true")
                            // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Proceso + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';

                            var action2 = "";
                            var motivo = "Motivodetencion";
                            if (row.ExpedienteId != "0") {
                                action2 = '<div class="btn-group"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Ver acciones <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="javascript:void(0);" class="  disabled ' + motivo + '"data-ExpedienteId="' + row.ExpedienteId + '"data-personanotifica="' + row.PersonaNotifica + '"data-celularnotifica="' + row.CelularNotifica + '" data-tracking="' + row.TrackingId + '"  id="idexpediente" title="Expediente"><i class="fa fa-folder-o"></i>&nbsp;<font zize="1">Expediente</font></a></li><li class="divider"></li>';
                            }
                            else {
                                action2 = '<div class="btn-group"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Ver acciones <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="javascript:void(0);" class="  ' + motivo + '"data-personanotifica="' + row.PersonaNotifica + '"data-celularnotifica="' + row.CelularNotifica + '"data-tracking="' + row.TrackingId + '"  id="idexpediente" title="Expediente"><i class="fa fa-folder-o"></i>&nbsp;<font zize="1">Expediente</font></a></li><li class="divider"></li>';
                            }

                            var action3 = "";

                            action3 = '<li><a   ' + motivo + '" href="EstudioDiagnostico.aspx?tracking=' + row.TrackingId + '" title="Diagnóstico"><i class="fa fa-file-o"></i>&nbsp;<font zize="1">Diagnóstico</font></a></li><li class="divider"></li>';

                            var action4 = "";
                            if (row.ExpedienteId == "0") {
                                // action4 = '<li><a class="  disabled ' + motivo + '"data-ExpedienteId="' + row.ExpedienteId + '" data-tracking="' + row.TrackingId + '"  id="IdVer" title="Ver"><i class="fa fa-eye"></i>&nbsp;<font zize="1">Ver</font></a></li><li class="divider"></li>';
                                action4 = '';
                            }
                            else {
                                action4 = '<li><a href="javascript:void(0);" class="' + motivo + '"data-ExpedienteId="' + row.ExpedienteId + '"data-ExpedientetrackingId="' + row.ExpedientetrackingId + '" data-tracking="' + row.TrackingId + '" data-Motivo="' + row.Motivoreahabilitacion + '"data-Adiccion="' + row.AdiccionId + '"data-Pandilla="' + row.Pandilla + '"data-religion="' + row.ReligionId + '"data-Cuadropatalogico="' + row.Cuadropatalogico + '"data-Observacion="' + row.Observacion + '"data-madre="' + row.Nombremadre + '"data-celmadre="' + row.Celularmadre + '"data-padre="' + row.Nombrepadre + '"data-celpadre="' + row.Celularpadre + '"data-tutor="' + row.Tutor + '"data-celtutor="' + row.celulartutor + '"data-escolaridad="' + row.EscolaridadId + '"data-notificado="' + row.Notificado + '"data-celnotificado="' + row.Celularnotificado + '"data-domiciliomadre="' + row.Domiciliomadre + '"data-domiciliopadre="' + row.Domiciliopadre + '" id="IdVer" title="Ver"><i class="fa fa-eye"></i>&nbsp;<font zize="1">Ver</font></a></li><li class="divider"></li>';
                            }
                            //var action5 = "";
                            //action5 = '&nbsp;<a class="btn datos btn-primary '+vacio+  '"  href="#"  id="datos" data-id="' + row.TrackingId + '" title="Datos personales">Datos personales</a>&nbsp;';
                            var action5 = "";
                            var vacio = "";
                            action5 = '<li><a class=" datos ' + vacio + '" href="javascript:void(0);" id="datos" data-id="' + row.TrackingId + '"data-madre="' + row.Nombremadre + '"data-celmadre="' + row.Celularmadre + '"data-padre="' + row.Nombrepadre + '"data-celpadre="' + row.Celularpadre + '"data-tutor="' + row.Tutor + '"data-celtutor="' + row.celulartutor + '"data-alias="' + row.Alias + '"data-escolaridad="' + row.Escolaridad + '" title="Datos personales"><i class="fa fa-bars"></i>&nbsp;<font zize="1">Datos Personales</font></a></li><li class="divider"></li>';

                            var action6 = "";
                            action6 = '<li><a class="  ' + vacio + '" href="javascript:void(0);" id="editardetenido" data-id="' + row.TrackingId + '"data-paterno="' + row.Paterno + '"data-materno="' + row.Materno + '"data-nombre="' + row.Nombre + '"data-fechanacimiento="' + row.FechaNacimineto + '"data-tutor="' + row.Tutor + '"data-celtutor="' + row.celulartutor + '" title="Editar persona"><i class="fa fa-gavel"></i>&nbsp;<font zize="1">Editar persona</font></a></li><li class="divider"></li>';

                            var action7 = "";
                            action7 = '<li><a class="  ' + vacio + '" href="javascript:void(0);" id="antecedentes" data-id="' + row.TrackingId + '"data-madre="' + row.Nombremadre + '"data-celmadre="' + row.Celularmadre + '"data-padre="' + row.Nombrepadre + '"data-celpadre="' + row.Celularpadre + '"data-tutor="' + row.Tutor + '"data-celtutor="' + row.celulartutor + '" title="Antecedentes"><i class=" fa fa-inbox"></i>&nbsp;<font zize="1">Antecedentes</font></a></li><li class="divider"></li></ul></div>';

                            return action2 + action3 + action4 + action5 + action6 + action7;
                        }
                    }
                ]
            });

            var table = $('#dt_basic').DataTable();

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            $("body").on("click", "#idexpediente", function () {
                LimpiarExpediente();
                var CeldaTrackingId = $(this).attr("data-tracking");
                var personanotifica = $(this).attr("data-personanotifica");
                var celularnotifica = $(this).attr("data-celularnotifica");
                $("#txtnotificacion").val(personanotifica);
                $("#ctl00_contenido_txtcelnotificado").val(celularnotifica);
                $("#hide").val(CeldaTrackingId);

                $("#guardarexpediente").show();
                $("#expedienteId").val("0");

                $("#RegistroExpediente-modal").modal("show");
            });

            function LimpiarExpediente() {
                $("#motivorehabilitacion").val("");
                $('#motivorehabilitacion').parent().removeClass('state-success')
                $("#adiccion").val("");
                $('#adiccion').parent().removeClass('state-success');
                $('#txtpandilla').val("");
                $('#txtpandilla').parent().removeClass('state-success');
                $('#religion').val("");
                $('#religion').parent().removeClass('state-success');
                $("#cuadropatologico").val("");
                $('#cuadropatologico').parent().removeClass('state-success').removeClass("state-error");
                $("#observaciones").val("");
                $('#observaciones').parent().removeClass('state-success');
                $("#motivorehabilitacion").prop('disabled', false);
                $("#adiccion").prop('disabled', false);
                $('#txtpandilla').prop('disabled', false);
                $('#txtpandilla').parent().removeClass("state-error");
                $('#religion').prop('disabled', false);
                $("#cuadropatologico").prop('disabled', false);
                $("#observaciones").prop('disabled', false);
                $("#guardarexpediente").prop('disabled', false);
                $("#txtmadre").val("");
                $("#ctl00_contenido_txtcelmadre").val("");
                $("#txtpadre").val("");
                $("#ctl00_contenido_txtcelpadre").val("");
                $("#txttutor").val("");
                $("#ctl00_contenido_txtceltutor").val("");
                $("#escolaridad").val("");
                $("#txtnotificacion").val("");
                $("#ctl00_contenido_txtcelnotificacion").val("");
                $("#domiciliomadre").val("");
                $("#domiciliopadre").val("");

                $("#select2-motivorehabilitacion-container").text("[Seleccione motivo]");
                $("#select2-adiccion-container").text("[Seleccione adicción]");
                $("#select2-religion-container").text("[Seleccione religión]");
                $("#select2-escolaridad-container").text("[Seleccione escolaridad]");
            }

            $('#motivorehabilitacion').on('select2:opening', function (evt) {
                if (this.disabled) {
                    return false;
                }
            });

            $('#adiccion').on('select2:opening', function (evt) {
                if (this.disabled) {
                    return false;
                }
            });

            $('#religion').on('select2:opening', function (evt) {
                if (this.disabled) {
                    return false;
                }
            });

            $('#escolaridad').on('select2:opening', function (evt) {
                if (this.disabled) {
                    return false;
                }
            });

            $("body").on("click", "#IdVer", function () {
                var CeldaTrackingId = $(this).attr("data-tracking");

                LimpiarExpediente();

                $("#hide").val(CeldaTrackingId);
                
                $("#motivorehabilitacion").val($(this).attr("data-motivo"));
                $("#select2-motivorehabilitacion-container").text($("#motivorehabilitacion option:selected").text());
                $("#motivorehabilitacion").prop('disabled', true);
                $("#adiccion").val($(this).attr("data-Adiccion"));
                $("#select2-adiccion-container").text($("#adiccion option:selected").text());
                $("#adiccion").prop('disabled', true);
                $('#txtpandilla').val($(this).attr("data-pandilla"));
                $('#txtpandilla').prop('disabled', true);
                $('#religion').val($(this).attr("data-religion"));
                $("#select2-religion-container").text($("#religion option:selected").text());
                $('#religion').prop('disabled', true);
                $("#cuadropatologico").val($(this).attr("data-cuadropatalogico"));
                $("#cuadropatologico").prop('disabled', true);
                $("#observaciones").val($(this).attr("data-Observacion"));
                $("#observaciones").prop('disabled', true);
                $("#txtmadre").val($(this).attr("data-madre"));
                $("#ctl00_contenido_txtcelmadre").val($(this).attr("data-celmadre"));
                $("#txtpadre").val($(this).attr("data-padre"));
                $("#ctl00_contenido_txtcelpadre").val($(this).attr("data-celpadre"));
                $("#txttutor").val($(this).attr("data-tutor"));
                $("#ctl00_contenido_txtceltutor").val($(this).attr("data-celtutor"));
                $("#escolaridad").val($(this).attr("data-escolaridad"));
                $("#select2-escolaridad-container").text($("#escolaridad option:selected").text());
                $("#escolaridad").prop('disabled', true);
                $("#txtnotificacion").val($(this).attr("data-notificado"));
                $("#ctl00_contenido_txtcelnotificacion").val($(this).attr("data-celnotificado"));
                $("#domiciliomadre").val($(this).attr("data-domiciliomadre"));
                $("#domiciliopadre").val($(this).attr("data-domiciliopadre"));

                $//("#guardarexpediente").prop('disabled', true);
                $("#expedienteId").val($(this).attr("data-expedienteid"));

                $("#expedientetrackingId").val($(this).attr("data-expedientetrackingid"))
                //$("#guardarexpediente").hide();
                $("#RegistroExpediente-modal").modal("show");
            });

            $("body").on("click", "#GuardaSalidaEfectuada", function () {
                if (ValidarSalidaEfectuada()) {
                    guardarsalidaefectuada();
                }
            });

            $("body").on("click", "#GuardaSalidaEfectuadaJuez", function () {

                if (ValidarSalidaEfectuadajuez()) {
                    guardarsalidaefectuadajuez();
                }
            });

            $("body").on("click", "#guardarexpediente", function () {
                if (validarregistroExpediente()) {
                    guardarExpedienteTrabajoSocial();
                }
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");

                window.emptytable = true;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "sentence_entrylist.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Bien hecho!</strong>" +
                                "El registro  se actualizó correctamente.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }
                        $('#main').waitMe('hide');
                    }
                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });

            //JGB 22052019
            function CargarListadoMovimientos(id) {
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/llena_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#motivorehabilitacion');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Seleccione motivo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(0);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de motivos de rehabilitación. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarListadoReligion(id) {
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/llena_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#religion');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Seleccione religión]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (id != "") {
                            Dropdown.val(0);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de religiones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarComboAddiccion(id) {
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/llena_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#adiccion');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Seleccione adicción]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(0);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de adicciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarListado(id) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getListadoCombo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_dropdown');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Delegación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("body").on("click", ".edit", function () {
                var nombre = $(this).attr("data-value");
                $("#IdNombreInterno").text(nombre);

                $("#trasladoInterno-modal").modal("show")
                $("#guardatraslado").attr("TrackingId", $(this).attr("data-tracking"));

                CargarListado(0);
                window.emptytableadd = false;
                window.tableadd.api().ajax.reload();
            });

            $("body").on("click", ".save", function () {
                var tracking = $(this).attr("TrackingId")
                if (validarTraslado()) {
                    guardar(tracking);
                    $("#trasladoInterno-modal").modal("hide");
                }
            });

            //$("body").on("click", ".guardatraslado", function () {
            //  $("#ctl00_contenido_lblMessage").html("");


            //      guardar();

            function ValidarSalidaEfectuadajuez() {
                var esvalido = true;

                if ($("#fundamento").val() == "" || $('#fundamento').val() == null) {
                    ShowError("Fundamento", "Capture un valor para el campo fundamento para poder continuar");
                    $('#fundamento').parent().removeClass('state-success').addClass("state-error");
                    $('#fundamento').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fundamento').parent().removeClass("state-error").addClass('state-success');
                    $('#fundamento').addClass('valid');
                }

                return esvalido;
            }

            function ValidarSalidaEfectuada() {
                var esvalido = true;

                if ($("#observacion").val() == "" || $('#observacion').val() == null) {
                    ShowError("Observación", "Capture un valor para el campo observación para poder continuar");
                    $('#observacion').parent().removeClass('state-success').addClass("state-error");
                    $('#observacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observacion').parent().removeClass("state-error").addClass('state-success');
                    $('#observacion').addClass('valid');
                }

                if ($("#responsable").val() == "" || $('#responsable').val() == null) {
                    ShowError("Responsable", "Capture un valor para el campo responsable para poder continuar");
                    $('#responsable').parent().removeClass('state-success').addClass("state-error");
                    $('#responsable').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#responsable').parent().removeClass("state-error").addClass('state-success');
                    $('#responsable').addClass('valid');
                }

                return esvalido;
            }

            function validarregistroExpediente() {
                var esvalido = true;

                if ($("#motivorehabilitacion").val() == "0" || $("#motivorehabilitacion").val() == null) {
                    ShowError("Motivo", "El motivo es obligatorio");
                    $('#motivorehabilitacion').parent().removeClass('state-success').addClass("state-error");
                    $('#motivorehabilitacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#motivorehabilitacion').parent().removeClass("state-error").addClass('state-success');
                    $('#motivorehabilitacion').addClass('valid');
                }

                if ($("#adiccion").val() == "0" || $("#adiccion").val() == null) {
                    ShowError("Adicción", "La adicción es obligatoria");
                    $('#adiccion').parent().removeClass('state-success').addClass("state-error");
                    $('#adiccion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#adiccion').parent().removeClass("state-error").addClass('state-success');
                    $('#adiccion').addClass('valid');
                }

                if ($("#txtpandilla").val() == "" || $('#txtpandilla').val() == null) {
                    ShowError("Pandilla", "La pandilla es obligatoria");
                    $('#txtpandilla').parent().removeClass('state-success').addClass("state-error");
                    $('#txtpandilla').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#txtpandilla').parent().removeClass("state-error").addClass('state-success');
                    $('#txtpandilla').addClass('valid');
                }

                if ($("#religion").val() == "0" || $("#religion").val() == null) {
                    ShowError("Religión", "La religion es obligatoria");
                    $('#religion').parent().removeClass('state-success').addClass("state-error");
                    $('#religion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#religion').parent().removeClass("state-error").addClass('state-success');
                    $('#religion').addClass('valid');
                }

                if ($("#cuadropatologico").val() == "" || $('#cuadropatologico').val() == null) {
                    ShowError("Cuadro patológico", "El cuadro patológico es obligatorio");
                    $('#cuadropatologico').parent().removeClass('state-success').addClass("state-error");
                    $('#cuadropatologico').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#cuadropatologico').parent().removeClass("state-error").addClass('state-success');
                    $('#cuadropatologico').addClass('valid');
                }

                return esvalido;
            }

            $("body").on("click", ".add", function () {
                $("#ingresocelda-modal").modal("show");

                window.emptytableadd = false;
                window.tableadd.api().ajax.reload();
            });

            $("body").on("click", ".calculate", function () {
                $("#ctl00_contenido_fecha").val("");
                $("#ctl00_contenido_fechac").val("");
                $("#ctl00_contenido_anoss").val("0");
                $("#ctl00_contenido_mesess").val("0");
                $("#ctl00_contenido_diass").val("0");
                $("#ctl00_contenido_anosa").val("0");
                $("#ctl00_contenido_mesesa").val("0");
                $("#ctl00_contenido_diasa").val("0");
                $("#ctl00_contenido_diasc").val("");
                $("#ctl00_contenido_fechac").val("");
                $("#ctl00_contenido_porcentaje").val("");
                limpiar();

                $("#calculate-modal").modal("show");
            });

            $("body").on("click", ".calculatebtn", function () {
                if (validar()) {
                    limpiar();
                    $("#ctl00_contenido_fechac").val("");
                    $("#ctl00_contenido_diasc").val("");
                    $("#ctl00_contenido_porcentaje").val("");
                    var str = $("#ctl00_contenido_fecha").val();

                    if (/^\d{2}\/\d{2}\/\d{4}$/i.test(str)) {

                        var parts = str.split("/");

                        var day = parts[0] && parseInt(parts[0], 10);
                        var month = parts[1] && parseInt(parts[1], 10);
                        var year = parts[2] && parseInt(parts[2], 10);

                        if (day < 10) {
                            day = '0' + day
                        }
                        if (month < 10) {
                            month = '0' + month
                        }
                        var startdate = year + "/" + month + "/" + day;
                        var durationyear = parseInt($("#ctl00_contenido_anoss").val(), 10) - parseInt($("#ctl00_contenido_anosa").val(), 10);
                        var durationmonth = parseInt($("#ctl00_contenido_mesess").val(), 10) - parseInt($("#ctl00_contenido_mesesa").val(), 10);
                        var durationday = parseInt($("#ctl00_contenido_diass").val(), 10) - parseInt($("#ctl00_contenido_diasa").val(), 10);

                        if (day <= 31 && day >= 1 && month <= 12 && month >= 1) {
                            var expiryDate = new Date(year, month - 1, day);

                            expiryDate.setDate(expiryDate.getDate() + durationday);
                            expiryDate.setMonth(expiryDate.getMonth() + durationmonth);
                            expiryDate.setFullYear(expiryDate.getFullYear() + durationyear);
                            //expiryDate.setMonth(expiryDate.getMonth(), durationmonth);

                            var day = ('0' + expiryDate.getDate()).slice(-2);
                            var month = ('0' + (expiryDate.getMonth() + 1)).slice(-2);

                            var year = expiryDate.getFullYear();
                            var finaldate = year + "/" + month + "/" + day;

                            // $('#ctl00_contenido_diasc').val(expiryDate.getDay());
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();

                            if (dd < 10) {
                                dd = '0' + dd
                            }

                            if (mm < 10) {
                                mm = '0' + mm
                            }

                            today = yyyy + '/' + mm + '/' + dd;

                            var compurgados = moment(today).diff(moment(startdate), 'days');
                            var total = moment(finaldate).diff(moment(startdate), 'days');

                            $("#ctl00_contenido_fechac").val(day + "/" + month + "/" + year);
                            if (compurgados >= 0) {
                                var porcentaje = parseFloat(Math.round(100 * compurgados) / total).toFixed(2);

                                if (total > 0) {
                                    $('#ctl00_contenido_diasc').val(compurgados);
                                    $('#ctl00_contenido_porcentaje').val(porcentaje);
                                }
                                else {
                                    ShowError("¡Error!", "La fecha a partir debe ser mayor a la fecha cumplimiento de sentencia.");
                                }
                            }
                            else {
                                ShowError("¡Error!", "La fecha a partir debe ser mayor a la fecha cumplimiento de sentencia.");
                            }

                        } else {
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    }
                }
            });

            function validar() {
                var esvalido = true;

                if ($("#ctl00_contenido_fecha").val() == null || $("#ctl00_contenido_fecha").val().split(" ").join("") == "") {
                    ShowError("Fecha a partir", "La fecha a partir es obligatoria.");
                    $('#ctl00_contenido_fecha').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_fecha').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_fecha').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_fecha').addClass('valid');
                }

                if ($("#ctl00_contenido_anoss").val() == null || $("#ctl00_contenido_anoss").val().split(" ").join("") == "") {
                    ShowError("Sentenca años", "Los años son obligatorios.");
                    $('#ctl00_contenido_anoss').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_anoss').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_anoss').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_anoss').addClass('valid');
                }

                if ($("#ctl00_contenido_mesess").val() == null || $("#ctl00_contenido_mesess").val().split(" ").join("") == "") {
                    ShowError("Sentenca meses", "Los meses son obligatorios.");;
                    $('#ctl00_contenido_mesess').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_mesess').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_mesess').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_mesess').addClass('valid');
                }

                if ($("#ctl00_contenido_diass").val() == null || $("#ctl00_contenido_diass").val().split(" ").join("") == "") {
                    ShowError("Sentenca días", "Los días son obligatorios.");
                    $('#ctl00_contenido_diass').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_diass').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_diass').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_diass').addClass('valid');
                }

                if ($("#ctl00_contenido_anosa").val() == null || $("#ctl00_contenido_anosa").val().split(" ").join("") == "") {
                    ShowError("Abono años", "Los años son obligatorios.");
                    $('#ctl00_contenido_anosa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_anosa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_anosa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_anosa').addClass('valid');
                }

                if ($("#ctl00_contenido_mesesa").val() == null || $("#ctl00_contenido_mesesa").val().split(" ").join("") == "") {
                    ShowError("Abono meses", "Los meses son obligatorios.");;
                    $('#ctl00_contenido_mesesa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_mesesa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_mesesa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_mesesa').addClass('valid');
                }

                if ($("#ctl00_contenido_diasa").val() == null || $("#ctl00_contenido_diasa").val().split(" ").join("") == "") {
                    ShowError("Abono días", "Los días son obligatorios.");
                    $('#ctl00_contenido_diasa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_diasa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_diasa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_diasa').addClass('valid');
                }

                return esvalido;
            }

            function limpiar() {
                $('#ctl00_contenido_fecha').parent().removeClass('state-success');
                $('#ctl00_contenido_fecha').parent().removeClass("state-error");
                $('#ctl00_contenido_anoss').parent().removeClass('state-success');
                $('#ctl00_contenido_anoss').parent().removeClass("state-error");
                $('#ctl00_contenido_mesess').parent().removeClass('state-success');
                $('#ctl00_contenido_mesess').parent().removeClass("state-error");
                $('#ctl00_contenido_diass').parent().removeClass('state-success');
                $('#ctl00_contenido_diass').parent().removeClass("state-error");
                $('#ctl00_contenido_anosa').parent().removeClass('state-success');
                $('#ctl00_contenido_anosa').parent().removeClass("state-error");
                $('#ctl00_contenido_mesesa').parent().removeClass('state-success');
                $('#ctl00_contenido_mesesa').parent().removeClass("state-error");
                $('#ctl00_contenido_diasa').parent().removeClass('state-success');
                $('#ctl00_contenido_diasa').parent().removeClass("state-error");
            }

            var breakpointAddDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };

            if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                $("#addentry").show();
            }

            $("body").on("click", ".printpdf", function () {
                LimpiarModalReporte();

                var nombre = $(this).attr("data-nombre");
                $("#nombrereportepdf").val(nombre);
                var rutaavatar = $(this).attr("data-avatar");
                var imagenAvatar = ResolveUrl(rutaavatar.trim());
                $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                var tracking = $(this).attr("data-tracking");
                $("#trackingidpdf").val(tracking);

                var alias = $(this).attr("data-alias");
                $("#aliasreportepdf").val(alias);

                var fechanacimiento = $(this).attr("data-fn");
                var today = new Date();
                var edad = moment(today).diff(moment(fechanacimiento), 'years');
                if (edad > 0) {
                    $("#edadreportepdf").val(edad);
                }

                // console.log(tracking);
                // console.log($("#trackingidpdf").val())

                $("#printpdf-modal").modal("show");
            });

            function LimpiarModalReporte() {
                $('#edadreportepdf').parent().removeClass('state-success');
                $('#edadreportepdf').parent().removeClass("state-error");
                $('#aliasreportepdf').parent().removeClass('state-success');
                $('#aliasreportepdf').parent().removeClass("state-error");
                $('#edadreportepdf').val("");
                $('#aliasreportepdf').val("");
                $('#nombrereportepdf').val("");
                $('#violentopdf').val("");
                $('#enfermopdf').val("");
                $('#comentariospdf').val("");
                $('#trackingidpdf').val("");
                $("#radioBuscandopdf").prop("checked", false);
                $("#radioExtraviadopdf").prop("checked", false);
                $("#radioInformacionpdf").prop("checked", false);
                $('#institucionpdf').val("");
                $('#direccionpdf').val("");
                $('#telefonopdf').val("");
                $('#senaspdf').val("");
            }

            function validarTraslado() {
                var esvalido = true;

                if ($("#ctl00_contenido_dropdown").val() == "0") {
                    ShowError("Delegación", "Capture un valor para el campo delegación para poder continuar");
                    $('#ctl00_contenido_dropdown').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_dropdown').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_dropdown').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_dropdown').addClass('valid');
                }
                return esvalido;
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".printpdfbtn", function () {
                if (validareporteextravio()) {
                    generareporteextravio();
                }
            });

            function obtenerevaloresSalidaefectuadajuez() {
                var salidaefectuadajuez = {
                    fundamento: $("#fundamento").val(),
                    internoId: $("#hideid").val(),
                    trackingId: $("#hide").val(),
                    TrabajoSocialId: $("#TrabajoSocialId").val()
                }

                return salidaefectuadajuez;
            }

            function limpiarDatoSalidaEfectuadajuez() {
                $("#fundamento").val("");
                $("#hideid").val("");
                $("#hide").val("");
                $('#fundamento').parent().removeClass('state-success');
                $('#fundamento').parent().removeClass("state-error");
            }

            function guardarsalidaefectuadajuez() {
                var salidaefectuadajuez = obtenerevaloresSalidaefectuadajuez();
                //alert(salidaefectuadajuez.TrabajoSocialId);
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/guardaSalidaEfectuadaJuez",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'salidaefectuadajuez': salidaefectuadajuez }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se registro correctamente");
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            $('#main').waitMe('hide');
                            limpiarDatoSalidaEfectuadajuez();
                            $("#RegistroSalidaEfectuadaJuez-modal").modal("hide");
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function obtenerevaloresSalidaefectuada() {
                var salidaefectuada = {
                    observacion: $("#observacion").val(),
                    responsable: $("#responsable").val(),
                    internoId: $("#hideid").val(),
                    trackingId: $("#hide").val(),
                    TrabajoSocialId: $("#TrabajoSocialId").val()
                }
                return salidaefectuada;
            }

            function limpiarDatoSalidaEfectuada() {
                $("#observacion").val("");
                $("#responsable").val("");
                $("#hideid").val("");
                $("#hide").val();
                $('#observacion').parent().removeClass('state-success');
                $('#observacion').parent().removeClass("state-error");
                $('#responsable').parent().removeClass('state-success');
                $('#responsable').parent().removeClass("state-error");
            }
            function guardarsalidaefectuada() {
                var salidaefectuada = obtenerevaloresSalidaefectuada();

                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/guardaSalidaEfectuada",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'salidaefectuada': salidaefectuada }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se registro correctamente");
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            $('#main').waitMe('hide');
                            $("#RegistroSalidaEfectuada-modal").modal("hide");
                            limpiarDatoSalidaEfectuada();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ObtenerValoresexpedienteTrabajoSocial() {
                var expedienteTrabajoSocial = {
                    motivorehabilitacionId: $("#motivorehabilitacion").val(),
                    adiccionId: $("#adiccion").val(),
                    pandilla: $('#txtpandilla').val(),
                    religionId: $('#religion').val(),
                    cuadropatologico: $("#cuadropatologico").val(),
                    observaciones: $("#observaciones").val(),
                    nombremadre: $("#txtmadre").val(),
                    celularmadre: $("#ctl00_contenido_txtcelmadre").val(),
                    nombrepadre: $("#txtpadre").val(),
                    celularpadre: $("#ctl00_contenido_txtcelpadre").val(),
                    tutor: $("#txttutor").val(),
                    celulartutor: $("#ctl00_contenido_txtceltutor").val(),
                    escolaridadid: $("#escolaridad").val(),
                    notificado: $("#txtnotificacion").val(),
                    celularnotificado: $("#ctl00_contenido_txtcelnotificado").val(),
                    TrackingId: $("#hide").val(),
                    ExpedienteId: $("#expedienteId").val(),
                    ExpedienteTrackingId: $("#expedientetrackingId").val(),
                    Domiciliomadre: $("#domiciliomadre").val(),
                    Domiciliopadre: $("#domiciliopadre").val()
                };
                return expedienteTrabajoSocial;
            }

            CargarEscolaridad("");
            function CargarEscolaridad(set) {
                $('#escolaridad').empty();
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/getEscolaridad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#escolaridad');

                        Dropdown.append(new Option("[Escolaridad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de escolaridad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function guardarExpedienteTrabajoSocial() {
                startLoading();
                var expedienteTrabajoSocial = ObtenerValoresexpedienteTrabajoSocial();

                //var param = RequestQueryString("tracking");
                //if (param != undefined) {
                //    MovimientoCelda.TrackingId = param;
                //}

                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/ExpedienteTrabajoSocial",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'expedienteTrabajoSocial': expedienteTrabajoSocial }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", resultado.mensaje);
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            LimpiarExpediente();
                            // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            $('#main').waitMe('hide');
                            $("#RegistroExpediente-modal").modal("hide");
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            $("body").on("click", "#guardaeditpersona", function () {
                if (ValidarPersonaEdit()) {
                    guardarpersonaeditada($(this).attr("data-id"));
                }
            });

            function limpiardatospesona() {
                $("#primerapellido").val("");
                $("#segundoapellido").val("");
                $("#nombre").val("");
                $("#fechahora").val("");
                $('#primerapellido').parent().removeClass('state-success');
                $('#primerapellido').parent().removeClass("state-error");
                $('#segundoapellido').parent().removeClass('state-success');
                $('#segundoapellido').parent().removeClass("state-error");
                $('#nombre').parent().removeClass('state-success');
                $('#nombre').parent().removeClass("state-error");
                $('#fechahora').parent().removeClass('state-success');
                $('#fechahora').parent().removeClass("state-error");
            }

            function guardarpersonaeditada(id) {
                var datospersona = {
                    paterno: $("#primerapellido").val(),
                    materno: $("#segundoapellido").val(),
                    nombre: $("#nombre").val(),
                    fechanacimiento: $("#fechahora").val(),
                    trackingid: id
                }

                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/editpersona",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'datospersona': datospersona }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se registro correctamente");
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            $('#main').waitMe('hide');
                            limpiardatospesona();
                            $("#Editapesona-modal").modal("hide");
                            // limpiarDatoSalidaEfectuada();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            $("body").on("click", "#editardetenido", function () {
                limpiardatospesona();
                $('#guardaeditpersona').attr("data-id", $(this).attr("data-id"));
                $("#primerapellido").val($(this).attr("data-paterno"));
                $("#segundoapellido").val($(this).attr("data-materno"));
                $("#nombre").val($(this).attr("data-nombre"));
                var data_fechanacimiento = $(this).attr("data-fechanacimiento");
                if (data_fechanacimiento == "null") {
                    $("#fechahora").val("");
                }
                else {
                    $("#fechahora").val(data_fechanacimiento);
                }
                $("#Editapesona-modal").modal("show");

                $('#FehaHoradatetimepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });

            $("body").on("click", "#antecedentes", function () {
                $("#Antecedentes-modal").modal("show");
                CargaAntecedentes($(this).attr("data-id"));
            });

            function ValidarPersonaEdit() {
                var esvalido = true;

                if ($("#primerapellido").val() == "" || $('#primerapellido').val() == null) {
                    ShowError("Apellido paterno", "El apellido paterno es obligatorio");
                    $('#primerapellido').parent().removeClass('state-success').addClass("state-error");
                    $('#primerapellido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#primerapellido').parent().removeClass("state-error").addClass('state-success');
                    $('#primerapellido').addClass('valid');
                }

                if ($("#segundoapellido").val() == "" || $('#segundoapellido').val() == null) {
                    ShowError("Apellido materno", "El apellido materno es obligatorio");
                    $('#segundoapellido').parent().removeClass('state-success').addClass("state-error");
                    $('#segundoapellido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#segundoapellido').parent().removeClass("state-error").addClass('state-success');
                    $('#segundoapellido').addClass('valid');
                }

                if ($("#nombre").val() == "" || $('#nombre').val() == null) {
                    ShowError("Nombre", "El nombre es obligatorio");
                    $('#nombre').parent().removeClass('state-success').addClass("state-error");
                    $('#nombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#nombre').parent().removeClass("state-error").addClass('state-success');
                    $('#nombre').addClass('valid');
                }
                if ($("#fechahora").val() == null || $("#fechahora").val().split(" ").join("") == "") {
                    ShowError("Fecha de nacimiento", "La fecha de nacimiento  es obligatoria.");
                    $('#fechahora').parent().removeClass('state-success').addClass("state-error");
                    $('#fechahora').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fechahora').parent().removeClass("state-error").addClass('state-success');
                    $('#fechahora').addClass('valid');
                }

                return esvalido;
            }

            $("body").on("click", ".datos", function () {
                var id = $(this).attr("data-id");
                $("#nombredelamadre").val($(this).attr("data-madre"));
                $("#teldelamadre").val($(this).attr("data-celmadre"));
                $("#nombredelpadre").val($(this).attr("data-padre"));
                $("#teldelpadre").val($(this).attr("data-celpadre"));
                $("#nombredeltutor").val($(this).attr("data-tutor"));
                $("#teldeltutor").val($(this).attr("data-celtutor"));
                $("#alias").val($(this).attr("data-alias"));
                $("#idescolaridad").val($(this).attr("data-escolaridad"));
                CargarDatosPersonales(id);
            });

            $('#dt_basic tbody').on('click', 'tr', function () {
                $("input[type=checkbox]:checked").each(function () {
                    $(this).val(null);
                });
            });


            $("body").on("click", "#imprimir", function () {
                var valor = "";
                var contador = 0;
                var trackingId = "";
                var expedienteId = "";
                var persona = "";
                $("input[type=checkbox]:checked").each(function () {
                    trackingId
                    if ($(this).val() != "on" && $(this).val() != "null") {
                        contador += 1;
                        valor = $(this).attr('data-internoid');
                        trackingId = $(this).attr('data-TrackingId');
                        expedienteId = $(this).attr('data-expedienteId');
                        persona = $(this).attr('data-Nombrecompleto');
                    }
                });

                if (valor == "") {
                    ShowAlert("¡Atención!", "Selecione un registro.");
                    return;
                }
                if (expedienteId == "0") {
                    ShowAlert("¡Atención!", "La persona " + persona + " no cuenta con información en el expediente.");
                    return;
                }
                if (contador > 1) {
                    ShowAlert("¡Atención!", "Seleccione solo un registro");
                    return;
                }

                $("#hideid").val(valor);
                $("#hide").val(trackingId);
                $('#expedienteId').val(expedienteId);
                ImprimeTrabajoSocial();
            });

            function ImprimeTrabajoSocial() {
                startLoading();
                var datos = {
                    internoId: $("#hideid").val(),
                    trackingId: $("#hide").val(),
                    expedienteId: $('#expedienteId').val()
                };
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/imprimereporte",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'datos': datos
                    }),
                    cache: false,
                    success: function (data) {
                        if (data.d.exitoso) {
                            $("#ctl00_contenido_idCalificacion").val(data.d.Id);
                            $("#ctl00_contenido_trackingIdCalificacion").val(data.d.TrackingId);

                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Se genero correctamente el reporte.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Se genero correctamente el reporte .");

                            var ruta = ResolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                        }
                        else {
                            ShowError("¡Error! Algo salió mal", data.d.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + data.d.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", "#salidaefectuada", function () {
                var valor = "";
                var contador = 0;
                var trackingId = "";
                var TrabajoSocialId = "";
                $("input[type=checkbox]:checked").each(function () {
                    trackingId
                    if ($(this).val() != "on" && $(this).val() != "null") {
                        contador += 1;
                        valor = $(this).attr('data-internoid');
                        trackingId = $(this).attr('data-TrackingId');
                        TrabajoSocialId = $(this).attr('data-trabajoSocialId');
                    }
                });

                if (valor == "") {
                    ShowAlert("¡Alerta!", "selecione un registro");
                    return;
                }

                if (contador > 1) {
                    ShowAlert("¡Alerta!", "Seleccione solo un registro");
                    return;
                }
                $("#TrabajoSocialId").val(TrabajoSocialId);
                $("#hideid").val(valor);
                $("#hide").val(trackingId);
                $("#RegistroSalidaEfectuada-modal").modal("show");
            });

            $("body").on("click", "#salidaefectuadajuez", function () {
                var valor = "";
                var contador = 0;
                var trackingId = "";
                $("input[type=checkbox]:checked").each(function () {
                    trackingId
                    if ($(this).val() != "on" && $(this).val() != "null") {
                        contador += 1;
                        valor = $(this).attr('data-internoid');
                        trackingId = $(this).attr('data-TrackingId');
                        TrabajoSocialId = $(this).attr('data-trabajoSocialId');
                    }
                });

                if (valor == "") {
                    ShowAlert("¡Alerta!", "selecione un registro");
                    return;
                }

                if (contador > 1) {
                    ShowAlert("¡Alerta!", "Seleccione solo un registro");
                    return;
                }
                $("#TrabajoSocialId").val(TrabajoSocialId);
                $("#hideid").val(valor);
                $("#hide").val(trackingId);
                $("#RegistroSalidaEfectuadaJuez-modal").modal("show");
            });

            function CargarDatosPersonales(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Sentence/sentence_entrylist.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno').val(resultado.obj.Nombre);
                            if (resultado.obj.Edad == 0) {
                                $('#edad').val("Sin dato");
                            }
                            else {
                                $('#edad').val(resultado.obj.Edad);
                            }

                            $('#situacion').val(resultado.obj.Situacion);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (resultado.obj.RutaImagen == "") {
                                imagenAvatar = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png";
                            }
                            $('#Img1').attr("src", imagenAvatar);
                            $('#horaRegistro').val(resultado.obj.Registro);
                            $("#motivo").val(resultado.obj.Motivo);
                            $('#salida').val(resultado.obj.Salida);
                            $('#domicilio').val(resultado.obj.Domicilio);
                            $("#datospersonales-modal").modal("show");
                        } else {
                            ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }

                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>