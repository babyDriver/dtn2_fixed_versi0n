﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="caja.aspx.cs" Inherits="Web.Application.Caja.caja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
 
    <li>Caja</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">   
    <style>
        #form > img {
            border-radius: 50%;
        }
        

        td.strikeout {
            text-decoration: line-through;
        }
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }
        input[type="number"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }        
   
    </style>
    <div class="scroll">
    <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-money"></i>
                Caja
            </h1>
        </div>
    </div>-->
    <div style="display:none;">
        <input type="text" />
        <input type="password" />
    </div>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
     <div class="row" style="margin-left: 0px;">
    <div class="col-lg-6"></div>
            <div class="col-lg-6" align="right" style="padding: 5px;">
                <div class="btn-group">
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Reportes de caja<span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="btn-sm" title="Recibos de caja" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/recibo_caja.aspx">Recibos de caja</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="btn-sm " title="Movimientos de caja" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/movimiento_caja.aspx">Movimientos de caja</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="btn-sm " title="Cortes de caja" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/corte_caja.aspx">Cortes de caja</a>
                        </li>
                    </ul>
                </div>
                <a style="float: none; font-size: smaller;" href="javascript:void(0);" class="btn bg-color-green txt-color-white btn-lg" id="linkCorte" title="Corte de caja"><i class="fa fa-th-list"></i>&nbsp;Corte de caja</a>
                <a style="float: none; font-size: smaller;" href="javascript:void(0);" class="btn bg-color-red txt-color-white  btn-lg " id="linkCancelacion" title="Cancelación"><i class="fa fa-ban"></i>&nbsp;Cancelación</a>        
                <a style="float: none; font-size: smaller;" href="javascript:void(0);" class="btn bg-color-yellow txt-color-white  btn-lg" id="linkCambioTurno" title="Cambio de turno""><i class=" fa fa-reply-all"></i>&nbsp;Cambio de turno</a>        
                <!-- <a style="float: none; font-size: smaller;" href="javascript:void(0);" class="btn btn-success txt-color-white  btn-lg " id="linkConsultarRecibos" title="Consultar recibos"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;Consultar recibos</a> -->
            </div>
            <p></p>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-caja-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Caja </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">                        
                            <div id="data-1" class="col-lg-12" style="width:100%">
                                <ul class="nav nav-tabs" id="myTab">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#tab1" data-toggle="tab" onclick="reload()">Ingresos por cobros de multas</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab2" data-toggle="tab" onclick="reload()" id="receipts">Recibos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab3" data-toggle="tab" onclick="reload()">Cortes</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab4" data-toggle="tab" onclick="reload()">Ingresos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab5" data-toggle="tab" onclick="reload()">Egresos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab6" data-toggle="tab" onclick="reload()">Cancelaciones</a>
                                    </li>
                                </ul>    
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">                                       
                                    <div class="row">                  
                                        <div class="col-lg-12">
                                            <br />
                                            <section class="col col-lg-12">
                                                <table id="dt_basic_multas" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>                                             
                                                            <th data-class="expand">#</th>
                                                            <th data-class="expand">Fotografía</th>
                                                            <th>Nombre</th>
                                                            <th>Apellido paterno</th>
                                                            <th data-hide="phone,tablet">Apellido materno</th>
                                                            <th data-hide="phone,tablet">No. remisión</th>
                                                            <th>Monto </th>
                                                            <th data-hide="phone,tablet">Acciones</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    
                                                </table>
                                            </section>                                        
                                        </div>                                    
                                    </div>
                                </div>                                                    
                                <div class="tab-pane" id="tab2">
                                    <div class="row">                  
                                        <div class="col-lg-12">
                                            <br />
                                            <section class="col col-lg-12">
                                                <table id="dt_basic_recibos" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>                                                        
                                                            <th data-class="expand">#</th>
                                                            <th>Folio</th>
                                                            <th>Fecha</th>
                                                            <th>Nombre</th>
                                                            <th>Apellido Paterno</th>
                                                            <th data-hide="phone,tablet">Apellido Materno</th>
                                                            <th data-hide="phone,tablet">No. remisión</th>
                                                            <th data-hide="phone,tablet">Monto</th>
                                                            <th data-hide="phone,tablet">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                   
                                                </table>
                                            </section>                                        
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab3">
                                    <div class="row">                  
                                        <div class="col-lg-12">
                                            <br />
                                            <section class="col col-lg-12">
                                                <table id="dt_basic_cortes" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>                                                        
                                                            <th data-class="expand">#</th>
                                                            <th>Inicio del corte</th>
                                                            <th>Fin del corte</th>
                                                            <th>Usuario</th>                                                        
                                                            <th data-hide="phone,tablet">Acciones</th>
                                                        </tr>                                                        
                                                    </thead>
                                                                                           
                                                </table>
                                            </section>                                        
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab4">
                                    <div class="row">                  
                                        <div class="col-lg-12">
                                            <br />                                        
                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                                <a class="btn btn-md btn-default add" id="agregarIngreso"><i class="fa fa-plus"></i>&nbsp;Agregar </a>                                            
                                            </div>                                        
                                            <div class="col col-lg-12"><br /></div>
                                            <section class="col col-lg-12">
                                                <table id="dt_basic_ingresos" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>                                                        
                                                            <th data-class="expand">#</th>
                                                            <th>Folio</th>
                                                            <th>Fecha</th>
                                                            <th>Concepto</th>
                                                            <th>Total</th>
                                                            <th>Persona que paga</th>
                                                            <th>Observaciones</th>
                                                            <th data-hide="phone,tablet">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </section>                                        
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab5">
                                    <div class="row">                  
                                        <div class="col-lg-12">
                                            <br />                                        
                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                                <a class="btn btn-md btn-default add" id="agregarEgreso"><i class="fa fa-plus"></i>&nbsp;Agregar </a>                                            
                                            </div>                                        
                                            <div class="col col-lg-12"><br /></div>
                                            <section class="col col-lg-12">
                                                <table id="dt_basic_egresos" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>                                                        
                                                            <th data-class="expand">#</th>
                                                            <th>Folio</th>
                                                            <th>Fecha</th>
                                                            <th>Concepto</th>
                                                            <th>Total</th>
                                                            <th>Persona que paga</th>
                                                            <th>Observaciones</th>
                                                            <th data-hide="phone,tablet">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </section>                                        
                                        </div>                                    
                                    </div>
                                </div>  
                                <div class="tab-pane" id="tab6">
                                    <div class="row">                  
                                        <div class="col-lg-12">
                                            <br />                                        
                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                                <a class="btn btn-md btn-default add" id="agregarCancelacion"><i class="fa fa-plus"></i>&nbsp;Agregar </a>                                            
                                            </div>                                        
                                            <div class="col col-lg-12"><br /></div>
                                            <section class="col col-lg-12">
                                                <table id="dt_basic_cancelaciones" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>                                                        
                                                            <th data-class="expand">#</th>
                                                            <th>Folio</th>
                                                            <th>Fecha</th>
                                                            <th>Tipo</th>
                                                            <th>Total</th>
                                                            <th>Persona que paga</th>
                                                            <th>Observaciones</th>
                                                            <th data-hide="phone,tablet">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </section>                                        
                                        </div>                                    
                                    </div>
                                </div>  
                            </div>                            
                        </div>
                    </div>                
                </div>
            </article>
        </div>
    </section> 
         </div>
    </div>
    <div class="modal fade" id="addEgreso" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal_title_egreso"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <div class="modal-body">
                            <div id="adiction_1" class="smart-form">
                                <fieldset>                                    
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input" style="color:dodgerblue">Concepto <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-file-text-o"></i>
                                                    <input  type="text" name="concepto" id="concepto" class="alphanumeric" placeholder="Concepto" maxlength="164" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el concepto.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                            <label class="input"style="color:dodgerblue">Total <a style="color: red">*</a></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" maxlength="13" class="form-control  alptext" name="total" id="total"  />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el total.</b>
                                                </div>  
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input"style="color:dodgerblue">Persona que paga <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-newspaper-o"></i>
                                                    <input type="text" name="persona" id="persona" placeholder="Persona que paga" maxlength="164" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la persona que paga.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input"style="color:dodgerblue">Observación</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-commenting-o"></i>
                                                    <input type="text" name="observacion" id="observacion" placeholder="Observación" maxlength="164" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la observación</b>
                                                </label>                                                
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="guardarEgreso"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <!--<a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default clear1"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="cancelarEgreso"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="historyingreso-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorialingreso"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dt_basichistorying" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
									    <th>Folio</th>
                                        <th data-hiden="tablet,fablet,phone">Motivo</th>
									    <th>Movimiento</th>
                                        <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="historyegreso-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorialegreso"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dt_basichistoryiegre" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
									    <th>Folio</th>
                                        <th data-hiden="tablet,fablet,phone">Motivo</th>
									    <th>Movimiento</th>
                                        <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addCorteCaja" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal_title_corte"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <div class="modal-body">
                            <div class="smart-form">
                                <fieldset>                                    
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label style="color: dodgerblue" class="input">Saldo inicial <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-file-text-o"></i>
                                                    <input type="text"  pattern="^\d*(\.\d{0,2})?$" maxlength="16" title="" name="saldoInicial" id="saldoInicial"  />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el saldo inicial.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>                                    
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="guardarCorte"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <!--<a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default clear1"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="cancelarCorte"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="modal fade" id="addCambioTurno" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal_title_cambio"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <div class="modal-body">
                            <div class="smart-form">
                                <fieldset>
                                    <div style="display:none;">
                                        <input name="username" type="text" />
                                        <input name="password" type="password" />
                                    </div>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label style="color: dodgerblue" class="input">Usuario <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-file-text-o"></i>
                                                    <input type="text" name="usuario" id="Cajaidusuario" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el usuario.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label style="color: dodgerblue" class="input">Password <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-file-text-o"></i>
                                                    <input type="password" id="password" readonly autocomplete="new-password" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el password.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label style="color: dodgerblue" class="input">Saldo inicial <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-file-text-o"></i>
                                                    <input type="text" name="saldoInicialCambio" id="saldoInicialCambio" maxlength="13" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el saldo inicial.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="cancelarCambioTurno"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear1"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default save" id="guardarCambioTurno"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="modal fade" id="addCancelacion" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal_title_cancelacion"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <div class="modal-body">
                            <div class="smart-form">
                                <fieldset>                                    
                                    <section class="col col-6">                                        
                                        <div>
                                            <label class="input" style="color:dodgerblue">Folio <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-file-text-o"></i>
                                                <input type="text" name="folioCancelacion" id="folioCancelacion" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa el folio.</b>
                                            </label>
                                        </div>                                        
                                    </section>
                                    <label class="input" style="color:dodgerblue">Tipo de movimiento <a style="color: red">*</a></label>
                                    <section class="col col-6" align="center">                                                        
                                        <div align="center">                                             
                                            Egreso <input type="radio" id="egreso" name="busqueda" />
                                            Ingreso <input type="radio" id="ingreso" name="busqueda" />
                                        </div>
                                    </section> 
                                    <section class="col col-6">
                                        <div class="row" align="center">
                                            <div>
                                                <a class="btn btn-sm btn-default save" id="buscarEgresoIngreso"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                            </div>
                                        </div>
                                    </section>                                    
                                    <section class="col col-6">                                        
                                        <div>
                                            <label class="input" style="color:dodgerblue">Total <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-file-text-o"></i>
                                                <input type="number" name="totalCancelacion" id="totalCancelacion" step="0.01" disabled="disabled"/>
                                                <b class="tooltip tooltip-bottom-right">Ingresa el total.</b>
                                            </label>
                                        </div>                                        
                                    </section>
                                    <section class="col col-6">                                        
                                        <div>
                                            <label class="input" style="color:dodgerblue">Persona que paga <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-file-text-o"></i>
                                                <input type="text" name="personaCancelacion" id="personaCancelacion" disabled="disabled" maxlength="100"/>
                                                <b class="tooltip tooltip-bottom-right">Ingresa la persona.</b>
                                            </label>
                                        </div>                                        
                                    </section>
                                    <section class="col col-6">                                        
                                        <div>
                                            <label class="input" style="color:dodgerblue">Observaciones</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-file-text-o"></i>
                                                <input type="text" name="observacionCancelacion" id="observacionCancelacion" disabled="disabled" maxlength="100" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la observación.</b>
                                            </label>                                            
                                        </div>                                        
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                          <button type="button" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="cancelarCancelacion">
                                     <i class="fa fa-close"></i>&nbsp;Cancelar
                            </button>
                             <button type="button" class="btn btn-sm btn-default save"  id="guardarCancelacion">
                                     <i class="fa fa-save"></i>&nbsp;Guardar
                            </button>


                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="blockEgreso-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbEgreso"></span></strong>&nbsp;el egreso  <strong>&nbsp<span id="itemEgresoblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>                            
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="continuarEgreso"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addIngreso" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal_title_ingreso"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <div class="modal-body">
                            <div class="smart-form">
                                <fieldset>                                    
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input" style="color:dodgerblue">Concepto <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-file-text-o"></i>
                                                    <input type="text" name="conceptoIngreso" id="conceptoIngreso" class="alphanumeric" placeholder="Concepto" maxlength="164" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el concepto.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <label class="input"style="color:dodgerblue">Total <a style="color: red">*</a></label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" maxlength="13" class="form-control  alptext" name="totalIngreso" id="totalIngreso"  />
                                                <b class="tooltip tooltip-bottom-right">Ingresa el total.</b>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input"style="color:dodgerblue">Persona que paga <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-newspaper-o"></i>
                                                    <input type="text" name="personaIngreso" id="personaIngreso" placeholder="Persona que paga" maxlength="164" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la persona que paga.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input"style="color:dodgerblue">Observación</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-commenting-o"></i>
                                                    <input type="text" name="observacionIngreso" id="observacionIngreso" placeholder="Observación" maxlength="164" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la observación</b>
                                                </label>                                                
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="guardarIngreso"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <!--<a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default clear1"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="cancelarIngreso"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="blockIngreso-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbIngreso"></span></strong>&nbsp;el ingreso <strong>&nbsp<span id="itemIngresoblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>                            
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="continuarIngreso"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="blockCancelacion-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbCancelacion"></span></strong>&nbsp;la cancelación con el folio <strong>&nbsp<span id="itemCancelacionblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="continuarCancelacion"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="photo-arrested" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src=" <%= ConfigurationManager.AppSettings["relativepath"]  %> #" alt="fotografía de la pertenencia / evidencia" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addPagarMulta" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal_title_pagarMulta"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <div class="modal-body">
                            <div class="smart-form">
                                <fieldset>                                                                        
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label style="color: dodgerblue" class="input">Total <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-money"></i>
                                                    <input type="number" name="totalPagar" id="totalPagar" step="0.01" disabled="disabled" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el total.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label style="color: dodgerblue" class="input">Persona que paga <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-newspaper-o"></i>
                                                    <input type="text" name="personaPagar" id="personaPagar" placeholder="Persona que paga" maxlength="100" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la persona que paga.</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label style="color: dodgerblue" class="input">Observación</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-commenting-o"></i>
                                                    <input type="text" name="observacionPagar" id="observacionPagar" placeholder="Observación" maxlength="164" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la observación</b>
                                                </label>                                                
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="cancelarPagar"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear1"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default save" id="guardarPagar"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" /> 
    <input type="hidden" id="idhisting" />
    <input type="hidden" id="idhistegr" />
    <input type="hidden" id="validaegresoingresoCancelacion" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">         
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>

    <script type="text/javascript">
        var valorPrevCount = -1;

        $(document).ready(function () { 

            $("#password").on('click', function (e) {
                e.preventDefault();
                var valorLength = $("#password").val().length;
                if (valorLength == 0) {
                    $("#password").attr("readonly", false);
                    $("#password").blur();
                    $("#password").focus();
                }
            });

            $("#password").on('keyup', function (e) {
                e.preventDefault();
                var valorLength = $("#password").val().length;
                if (valorLength == 0 && valorPrevCount != 0) {
                    $("#password").attr("readonly", false);
                    $("#password").blur();
                    $("#password").focus();
                }
                valorPrevCount = valor.length;
            });



            window.addEventListener("keydown", function (e) {
                if (e.ctrlKey && e.keyCode === 71) {
                    e.preventDefault();

                    if ($("#addEgreso").is(":visible")) {
                        document.getElementById("guardarEgreso").click();
                    }

                    if ($("#addCorteCaja").is(":visible")) {
                        document.getElementById("guardarCorte").click();
                    }


                    if ($("#addCambioTurno").is(":visible")) {
                        document.getElementById("guardarCambioTurno").click();
                    }


                    if ($("#addCancelacion").is(":visible")) {
                        document.getElementById("guardarCancelacion").click();
                    }


                    if ($("#addIngreso").is(":visible")) {
                        document.getElementById("guardarIngreso").click();
                    }

                    if ($("#addPagarMulta").is(":visible")) {
                        document.getElementById("guardarPagar").click();
                    }
                }


            });




            function validarCambioturno() {
                var esvalido = true;

                if ($("#Cajaidusuario").val() == "") {
                    ShowError("Usuario", "El usuario es obligatorio.");
                 
                    esvalido = false;
                }
                
            
                if ($("#password").val() == "") {
                    ShowError("Password", "El password es obligatorio.");
                   
                    esvalido = false;
                }

               if ($("#saldoInicialCambio").val()== "") {
                    ShowError("Saldo incial", "El saldo incial es obligatorio.");
                   
                    esvalido = false;
                }
              
                return esvalido;
            }

            $("#guardarCambioTurno").click(function () {
                data = [
                    Usuario = $("#Cajaidusuario").val(),
                    Password = $("#password").val(),
                    Saldo = $("#saldoInicialCambio").val()
                ];
                if (!validarCambioturno()) { return;}
                realizarCambioDeTurno(data);
                $('#saldoInicialCambio').parent().removeClass('state-error');
                $('#Cajaidusuario').parent().removeClass('state-error');
                $('#password').parent().removeClass('state-error');
            });
            var rutaDefaultServer = "";

            $("#saldoInicial").on({
                "focus": function (event) {
                    $(event.target).select();
                },
                "keyup": function (event) {
                    $(event.target).val(function (index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }
            });

            
            $("#saldoInicialCambio").on({
                "focus": function (event) {
                    $(event.target).select();
                },
                "keyup": function (event) {
                    $(event.target).val(function (index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }
            });
            getRutaDefaultServer();

            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "caja.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }
                    }
                });
            }

            $("#guardarCorte").click(function () {
                var saldoInicial = $("#saldoInicial").val();
                guardarCorte(saldoInicial);
            });

            $("#guardarPagar").click(function () {
                if (validarPagarMulta()) {
                    data = [
                        TrackingCalificacion = $("#guardarPagar").attr("data-trackingcalificacion"),
                        TrackingEstatus = $("#guardarPagar").attr("data-trackingestatus"),
                        Persona = $("#personaPagar").val(),
                        Observaciones = $("#observacionPagar").val()
                    ];                    

                    if (existeUnCorteActivo()) {
                        pagarMulta(data);
                    }
                    else {
                        $("#addPagarMulta").modal("hide");
                        $("#modal_title_corte").empty();                
                        $("#modal_title_corte").html("Corte / saldo inicial");
                        $("#addCorteCaja").modal('show');
                    }
                }                
            });

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);
                $("#photo-arrested").modal("show");
            });

            $("#guardarCancelacion").click(function () {
               
                if ($("#validaegresoingresoCancelacion").val() == "0")
                {
                    return;
                }

                var id = $("#guardarCancelacion").attr("data-id");
                var tracking = $("#guardarCancelacion").attr("data-trackingId");

                if (id == undefined || id == null) {
                    id = "";
                }

                if (tracking == undefined || tracking == null) {
                    tracking = "";
                }

                data = [
                    Id = id,
                    TrackingId = tracking,
                    TrackingItem = $("#guardarCancelacion").attr("trackingitem")
                ];

                if (existeUnCorteActivo()) {
                    saveCancelacion(data);
                }
                else {
                    $("#addCancelacion").modal('hide');
                    $("#modal_title_corte").empty();                
                    $("#modal_title_corte").html("Corte / saldo inicial");
                    $("#addCorteCaja").modal('show');
                }
                
            });

            function clearCancelacionModal() {
                //Campos
                $("#folioCancelacion").val("");
                $("#totalCancelacion").val("");
                $("#personaCancelacion").val("");
                $("#observacionCancelacion").val("");
                $("#egreso").prop("checked", false);
                $("#ingreso").prop("checked", false);
                //Estilos
                $('.input').removeClass('valid');
                $('.input').removeClass('state-success');
                $('.input').removeClass('state-error');
            }

            $('#addCancelacion').on('hidden.bs.modal', function () {
                clearCancelacionModal();
            });

            function saveCancelacion(data) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/saveCancelacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: data
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            clearCancelacionModal();
                            $("#addCancelacion").modal("hide");
                            $('#main').waitMe('hide');
                            $("#dt_basic_cancelaciones").DataTable().ajax.reload();
                            $("#dt_basic").DataTable().ajax.reload();
                            window.tableingreso.api().ajax.reload();
                            //$("#dt_basic_ingresos").DataTable().ajax.reload();
                            $("#dt_basic_multas").DataTable().ajax.reload();
                            $("#dt_basic_recibos").DataTable().ajax.reload();
                            $("#dt_basic_cortes").DataTable().ajax.reload();
                            window.table.api().ajax.reload();
                            //$("#dt_basic_egresos").DataTable().ajax.reload();

                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.message + " correctamente.");                            
                        } else {
                            
                            if (resultado.message == "Debe buscar un folio para realizar una cancelación" || resultado.message == "No se encontro el folio en el sistema")
                                ShowAlert("¡Alerta! ", resultado.message);
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                    "Algo salió mal, " + resultado.message + ". Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                                setTimeout(hideMessage, hideTime);
                                ShowError("¡Error! Algo salió mal ", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                            }
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar la cancelación", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("#buscarEgresoIngreso").click(function () {
                if (validarBusquedaEgresoIngreso()) {
                    data = [
                        folio = $("#folioCancelacion").val(),
                        egreso = $("#egreso").is(":checked") == true ? true : false
                    ];

         

                    obtenerDatos(data);
                }
            });

            function obtenerDatos(datos) {                
                $.ajax({
                    type: "POST",
                    url: "caja.aspx/obtenerDatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            $("#totalCancelacion").val(resultado.obj.Total);
                            $("#personaCancelacion").val(resultado.obj.PersonaQuePaga);
                            $("#observacionCancelacion").val(resultado.obj.Observacion);
                            $("#guardarCancelacion").attr("trackingitem", resultado.obj.TrackingId);
                           
                            if (resultado.obj.TrackingId == ""|| resultado.obj.TrackingId == "00000000-0000-0000-0000-000000000000")
                            {
                                ShowAlert("¡Atención!", "No se encontro el folio en el sistema.");
                                return;

                            }
                            if (resultado.obj.Habilitado)
                            {
                                $("#validaegresoingresoCancelacion").val("1");

                            }
                            else
                            {
                                ShowAlert("¡Atención!", "No se puede cancelar registros deshabilitados.")
                                $("#validaegresoingresoCancelacion").val("0");
                            }


                        } else {
                            if (resultado.message == "El folio es demasiado grande" || resultado.message == "El folio solo puede contener numeros")
                                ShowAlert("¡Alerta! ", resultado.message);
                            else
                                ShowError("¡Error! Algo salió mal", resultado.message + " Si el problema persiste, contacte al personal de soporte técnico.");
                        }                       
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible obtener el registro buscado", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function validarBusquedaEgresoIngreso() {
                var esvalido = true;                                

                if ($("#folioCancelacion").val().split(" ").join("") == "") {
                    ShowError("Folio", "El folio es obligatorio.");
                    $('#folioCancelacion').parent().removeClass('state-success').addClass("state-error");
                    $('#folioCancelacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $(".input").removeClass("state-success");
                    $(".input").removeClass("state-error");
                    $(".input").removeClass("valid");
                }

                if (!$("#egreso").is(":checked") && !$("#ingreso").is(":checked")) {
                    ShowError("Tipo", "El tipo es obligatorio.");
                    esvalido = false;                    
                }

                return esvalido;
            }

            $("#continuarEgreso").click(function () {
                var tracking = $(this).attr("data-trackingid");
                eliminarEgreso(tracking);
            });

            $("#continuarIngreso").click(function () {
                var tracking = $(this).attr("data-trackingid");
                eliminarIngreso(tracking);
            });

            $("#continuarCancelacion").click(function () {
                var tracking = $(this).attr("data-trackingid");
                eliminarCancelacion(tracking);
            });

            $("#linkCancelacion").click(function () {
                $("#modal_title_cancelacion").empty();                
                $("#modal_title_cancelacion").html("<i class='fa fa-pencil'></i> Cancelación");
                $("#addCancelacion").modal('show');
            });

            $("#linkCambioTurno").click(function () {
                $("#modal_title_cambio").empty();                
                $("#modal_title_cambio").html('<i class="fa fa-pencil" +=""></i> Cambio de turno');
                $("#addCambioTurno").modal('show');
            });

            $("#linkCorte").click(function () {
                $('#saldoInicial').parent().removeClass('state-Error')
                $("#modal_title_corte").empty();                
                $("#modal_title_corte").html("Corte / saldo inicial");
                $("#addCorteCaja").modal('show');
            });

            $("body").on("click", ".blockEgreso", function () {
                var verb = $(this).attr("style");
                $("#verbEgreso").text(verb);
                $("#itemEgresoblock").text($(this).attr('data-value'));
                $("#continuarEgreso").attr("data-trackingid", $(this).attr("data-trackingid"));
                $("#blockEgreso-modal").modal('show');
            });

            $("body").on("click", ".blockIngreso", function () {
                var verb = $(this).attr("style");
                $("#verbIngreso").text(verb);
                $("#itemIngresoblock").text($(this).attr('data-value'));
                $("#continuarIngreso").attr("data-trackingid", $(this).attr("data-trackingid"));
                $("#blockIngreso-modal").modal('show');
            });

            $("body").on("click", ".blockCancelacion", function () {
                var verb = $(this).attr("style");
                $("#verbCancelacion").text(verb);
                $("#itemCancelacionblock").text($(this).attr('data-value'));
                $("#continuarCancelacion").attr("data-trackingid", $(this).attr("data-trackingid"));
                $("#blockCancelacion-modal").modal('show');
            });

            $("body").on("click", ".editEgreso", function () {                
                $("#concepto").val($(this).attr('data-value'));
                $("#total").val($(this).attr('data-total'));
                $("#persona").val($(this).attr('data-persona'));
                $("#observacion").val($(this).attr('data-observacion'));
                $("#guardarEgreso").attr("data-trackingId", $(this).attr("data-tracking"));
                $("#guardarEgreso").attr("data-id", $(this).attr("data-id"));
                $("#modal_title_egreso").empty();                
                $("#modal_title_egreso").html("<i class='fa fa-pencil'></i> Editar registro");
                $("#addEgreso").modal("show");
            });

             $("#totalIngreso").on({
                "focus": function (event) {
                    $(event.target).select();
                },
                "keyup": function (event) {
                    $(event.target).val(function (index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }
            });

              $("#total").on({
                "focus": function (event) {
                    $(event.target).select();
                },
                "keyup": function (event) {
                    $(event.target).val(function (index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }
            });

            $("body").on("click", ".editIngreso", function () {                
                $("#conceptoIngreso").val($(this).attr('data-value'));
                $("#totalIngreso").val($(this).attr('data-total'));
                $("#personaIngreso").val($(this).attr('data-persona'));
                $("#observacionIngreso").val($(this).attr('data-observacion'));
                $("#guardarIngreso").attr("data-trackingId", $(this).attr("data-tracking"));
                $("#guardarIngreso").attr("data-id", $(this).attr("data-id"));
                $("#modal_title_ingreso").empty();                
                $("#modal_title_ingreso").html("<i class='fa fa-pencil'></i> Editar registro");
                $("#addIngreso").modal("show");

            });

            $("#guardarEgreso").click(function () {
                if (validarEgreso()) {

                    var id = $("#guardarEgreso").attr("data-id");
                    var tracking = $("#guardarEgreso").attr("data-trackingId");

                    if (id == undefined || id == null) {
                        id = "";
                    }

                    if (tracking == undefined || tracking == null) {
                        tracking = "";
                    }

                    egreso = [
                        Id = id,
                        TrackingId = tracking,
                        Concepto = $("#concepto").val(),
                        Total = $("#total").val(),
                        Persona = $("#persona").val(),
                        Observacion = $("#observacion").val()
                    ];

                    if (existeUnCorteActivo()) {
                        saveEgreso(egreso);
                        tablaegresos();
                          //window.table.api().ajax.reload();
                          //  $("#dt_basic_egresos").DataTable().ajax.reload();
                    }
                    else {
                        $("#addEgreso").modal("hide");
                        $("#modal_title_corte").empty();                
                        $("#modal_title_corte").html("Corte / saldo inicial");
                        $("#addCorteCaja").modal('show');
                    }                    
                }                
            });

            function existeUnCorteActivo() {
                var isActivo = false;
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "caja.aspx/existeUnCorteActivo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",                    
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            if (resultado.isActive) {
                                isActivo = true;
                            }
                            else {
                                isActivo = false;
                            }
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible eliminar la cancelación", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });

                return isActivo;
            }

            function eliminarCancelacion(tracking) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/eliminarCancelacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: tracking
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {                            
                            $("#blockCancelacion-modal").modal("hide");
                            $('#main').waitMe('hide');
                            $("#dt_basic_cancelaciones").DataTable().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Registro " + resultado.message + " con éxito.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Registro " + resultado.message + " con éxito.");                            
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible eliminar la cancelación", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function eliminarIngreso(tracking) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/eliminarEgreso",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: tracking
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {                            
                            $("#blockIngreso-modal").modal("hide");
                            $('#main').waitMe('hide');
                            //$("#dt_basic_ingresos").DataTable().ajax.reload();
                           // window.tableingreso.api().ajax.reload();
                            cargaringresos();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Registro " + resultado.message + " con éxito.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Registro " + resultado.message + " con éxito.");                            
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible eliminar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function eliminarEgreso(tracking) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/eliminarEgreso",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: tracking
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {                            
                            $("#blockEgreso-modal").modal("hide");
                            $('#main').waitMe('hide');
                            window.table.api().ajax.reload();
                            //$("#dt_basic_egresos").DataTable().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Registro " + resultado.message + " con éxito.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Registro " + resultado.message + " con éxito.");                            
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible eliminar el egreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function realizarCambioDeTurno(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/cambiarTurno",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {                                           
                            $("#addCambioTurno").modal('hide');
                            $('#main').waitMe('hide');                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.message + " correctamente.");
                            window.location.href = "../../Signout.aspx";
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el egreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function saveEgreso(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/saveEgreso",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            clearEgreso();
                            $("#addEgreso").modal("hide");
                            $('#main').waitMe('hide');
                            window.table.api().ajax.reload();
                            //$("#dt_basic_egresos").DataTable().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el egreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function ValidarCorte()
            {
                var isvalid = true;

                if ($("#saldoInicial").val() == "")
                {
                    ShowError("Saldo inicial", "El saldo inicial es obligatorio.");
                      $('#saldoInicial').parent().removeClass('state-success').addClass("state-error");
                    $('#saldoInicial').removeClass('valid');
                    isvalid = false;
                }

                return isvalid;
            }

            function guardarCorte(saldoInicial) {
                if (!ValidarCorte()){ return; }

                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/guardarCorte",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        saldoInicial: saldoInicial
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {                                  
                            $("#addCorteCaja").modal("hide");
                            $("#saldoInicial").val("");
                            $('#main').waitMe('hide');                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.message + " correctamente.");                            
                             $("#dt_basic_cortes").DataTable().ajax.reload();
                        } else {
                            ShowError("¡Error! No fue posible guardar el pago de la multa", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                          $('#saldoInicial').parent().removeClass('state-Error')
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $('#addPagarMulta').on('hidden.bs.modal', function () {
                clearPagarMulta();
            });

            function clearPagarMulta() {
                //Campos
                $("#totalPagar").val("");
                $("#personaPagar").val("");
                $("#observacionPagar").val("");                                
                //Estilos
                $('.input').removeClass('valid');
                $('.input').removeClass('state-success');
                $('.input').removeClass('state-error');
            }

            function pagarMulta(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/pagarMulta",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            clearIngreso();
                            var ruta = ResolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                            clearPagarMulta()
                            $("#addPagarMulta").modal("hide");
                            $('#main').waitMe('hide');                            
                            $("#dt_basic_recibos").DataTable().ajax.reload();
                            window.tableingreso.api().ajax.reload();
                            //$("#dt_basic_ingresos").DataTable().ajax.reload();
                            $("#dt_basic_multas").DataTable().ajax.reload();                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! No fue posible guardar el pago de la multa", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function generarReciboCorte(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/generarReciboCorteDeCaja",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            var ruta = ResolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }                    
                            $('#main').waitMe('hide');
                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El recibo se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "El recibo se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! No fue posible guardar el pago de la multa", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function generarReciboMulta(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/imprimirRecibo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            
                            var ruta = ResolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }                            
                            $('#main').waitMe('hide');
                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El recibo se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "El recibo se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! No fue posible guardar el pago de la multa", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function saveIngreso(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "caja.aspx/saveIngreso",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            clearIngreso();
                            $("#addIngreso").modal("hide");
                            $('#main').waitMe('hide');
                            window.tableingreso.api().ajax.reload();
                            //$("#dt_basic_ingresos").DataTable().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function validarPagarMulta() {
                var esvalido = true;                                                

                if ($("#totalPagar").val().split(" ").join("") == "") {
                    ShowError("Total", "El total es obligatorio.");
                    $('#totalPagar').parent().removeClass('state-success').addClass("state-error");
                    $('#totalPagar').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#totalPagar').parent().removeClass("state-error").addClass('state-success');
                    $('#totalPagar').addClass('valid');
                }

                if ($("#personaPagar").val().split(" ").join("") == "") {
                    ShowError("Persona", "La persona es obligatoria.");
                    $('#personaPagar').parent().removeClass('state-success').addClass("state-error");
                    $('#personaPagar').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#personaPagar').parent().removeClass("state-error").addClass('state-success');
                    $('#personaPagar').addClass('valid');
                }                         

                return esvalido;
            }

            function validarEgreso() {
                var esvalido = true;                                

                if ($("#concepto").val().split(" ").join("") == "") {
                    ShowError("Concepto", "El concepto es obligatorio.");
                    $('#concepto').parent().removeClass('state-success').addClass("state-error");
                    $('#concepto').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#concepto').parent().removeClass("state-error").addClass('state-success');
                    $('#concepto').addClass('valid');
                }

                var total = parseInt($("#total").val());

                if (isNaN(total)) {
                    $('#total').parent().removeClass('state-success').addClass("state-error");
                    $('#total').removeClass('valid');
                    ShowError("Total", "El total es obligatorio.");
                    esvalido = false;
                }

                if (total <= 0) {
                    $('#total').parent().removeClass('state-success').addClass("state-error");
                    $('#total').removeClass('valid');
                    ShowError("Total", "El campo tiene un valor invalido, capture solo números mayores a cero.");
                    esvalido = false;
                }

                if (!isNaN(total) && total > 0) {
                    $('#total').parent().removeClass("state-error").addClass('state-success');
                    $('#total').addClass('valid');
                }                

                if ($("#persona").val().split(" ").join("") == "") {
                    ShowError("Persona", "La persona es obligatoria.");
                    $('#persona').parent().removeClass('state-success').addClass("state-error");
                    $('#persona').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#persona').parent().removeClass("state-error").addClass('state-success');
                    $('#persona').addClass('valid');
                }                         

                return esvalido;
            }


            function validarIngreso() {
                var esvalido = true;                                

                if ($("#conceptoIngreso").val().split(" ").join("") == "") {
                    ShowError("Concepto", "El concepto es obligatorio.");
                    $('#conceptoIngreso').parent().removeClass('state-success').addClass("state-error");
                    $('#conceptoIngreso').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#conceptoIngreso').parent().removeClass("state-error").addClass('state-success');
                    $('#conceptoIngreso').addClass('valid');
                }

                var total = parseInt($("#totalIngreso").val());

                if (isNaN(total)) {
                    $('#totalIngreso').parent().removeClass('state-success').addClass("state-error");
                    $('#totalIngreso').removeClass('valid');
                    ShowError("Total", "El total es obligatorio.");
                    esvalido = false;
                }

                if (total <= 0) {
                    $('#totalIngreso').parent().removeClass('state-success').addClass("state-error");
                    $('#totalIngreso').removeClass('valid');
                    ShowError("Total", "El campo tiene un valor invalido, capture solo números mayores a cero.");
                    esvalido = false;
                }

                if (!isNaN(total) && total > 0) {
                    $('#totalIngreso').parent().removeClass("state-error").addClass('state-success');
                    $('#totalIngreso').addClass('valid');
                }

                if ($("#personaIngreso").val().split(" ").join("") == "") {
                    ShowError("Persona", "La persona es obligatoria.");
                    $('#personaIngreso').parent().removeClass('state-success').addClass("state-error");
                    $('#personaIngreso').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#personaIngreso').parent().removeClass("state-error").addClass('state-success');
                    $('#personaIngreso').addClass('valid');
                }                         

                return esvalido;
            }

            $("#cancelarEgreso").on("click", function () {
                clearEgreso();
            });

            $("#cancelarIngreso").on("click", function () {
                clearIngreso();
            });

            $("#cancelarCorte").on("click", function () {
                $("#saldoInicial").val("");
                //Estilos
                $('.input').removeClass('valid');
                $('.input').removeClass('state-success');
                $('.input').removeClass('state-error');  
            });

            function clearEgreso() {
                //Campos
                $("#concepto").val("");
                $("#total").val("");
                $("#observacion").val("");
                $("#persona").val("");
                //Estilos
                $('.input').removeClass('valid');
                $('.input-group').removeClass('valid');
                $('.input').removeClass('state-success');
                $('.input-group').removeClass('state-success');
                $('.input').removeClass('state-error');                
                $('.input-group').removeClass('state-error');                
            }

            function clearIngreso() {
                //Campos
                $("#conceptoIngreso").val("");
                $("#totalIngreso").val("");
                $("#observacionIngreso").val("");
                $("#personaIngreso").val("");
                //Estilos
                $('.input').removeClass('valid');
                $('.input').removeClass('state-success');
                $('.input').removeClass('state-error');                
            }

            $("body").on("click", ".pagar", function () {
                $("#guardarPagar").attr("data-trackingcalificacion", $(this).attr("data-trackingcalificacion"));
                $("#guardarPagar").attr("data-trackingestatus", $(this).attr("data-trackingestatus"));
                $("#totalPagar").val($(this).attr("data-totalAPagar"));
                $("#modal_title_pagarMulta").empty();                
                $("#modal_title_pagarMulta").html('<i class="fa fa-pencil" +=""></i> Datos de la multa');
                $("#addPagarMulta").modal("show");
            });

            $("body").on("click", ".imprimirCorte", function () {
                var tracking = $(this).attr("data-tracking");
                generarReciboCorte(tracking);
            });

            $("body").on("click", ".imprimir", function () {
                var tracking = $(this).attr("data-reciboid");
                generarReciboMulta(tracking)
            });

            $("#agregarEgreso").click(function () {
                $("#guardarEgreso").attr("data-trackingId", "");
                $("#guardarEgreso").attr("data-id", "");
                $("#modal_title_egreso").empty();                
                $("#modal_title_egreso").html("<i class='fa fa-pencil'></i> Agregar registro");
                $("#addEgreso").modal("show");
            });

            $("#agregarIngreso").click(function () {
                $("#guardarIngreso").attr("data-trackingId", "");
                $("#guardarIngreso").attr("data-id", "");
                $("#modal_title_ingreso").empty();                
                $("#modal_title_ingreso").html("<i class='fa fa-pencil'></i> Agregar registro");
                $("#addIngreso").modal("show");
            });

            $("#agregarCancelacion").click(function () {
                $("#guardarCancelacion").attr("data-id", "");
                $("#guardarCancelacion").attr("data-tracking", "");
                $("#modal_title_cancelacion").empty();
                $("#modal_title_cancelacion").html("<i class='fa fa-pencil'></i> Cancelación");
                $("#addCancelacion").modal('show');
            });

            $("#guardarIngreso").click(function () {
                if (validarIngreso()) {
                    var id = $("#guardarIngreso").attr("data-id");
                    var tracking = $("#guardarIngreso").attr("data-trackingId");                    

                    if (id == undefined || id == null) {
                        id = "";
                    }

                    if (tracking == undefined || tracking == null) {
                        tracking = "";
                    }

                    ingreso = [
                        Id = id,
                        TrackingId = tracking,                        
                        Concepto = $("#conceptoIngreso").val(),
                        Total = $("#totalIngreso").val(),
                        Persona = $("#personaIngreso").val(),
                        Observacion = $("#observacionIngreso").val()
                    ];                    

                    if (existeUnCorteActivo()) {
                        saveIngreso(ingreso);
                        cargaringresos();
                        //window.tableingreso.api().ajax.reload();
                        //$("#dt_basic_ingresos").DataTable().ajax.reload();
                    }
                    else {
                        $("#addIngreso").modal("hide");
                        $("#modal_title_corte").empty();                
                        $("#modal_title_corte").html('<i class="fa fa-pencil" +=""></i> Corte / saldo inicial');
                        $("#addCorteCaja").modal('show');
                    }
                }
            });

            
            //Variables tabla de egresos
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            //Variables tabla de ingresos
            var responsiveHelper_dt_basic_ingresos = undefined;
            var responsiveHelper_datatable_fixed_column_ingresos = undefined;
            var responsiveHelper_datatable_col_reorder_ingresos = undefined;
            var responsiveHelper_datatable_tabletools_ingresos = undefined;

            //Variables tabla de cancelaciones
            var responsiveHelper_dt_basic_cancelacion = undefined;
            var responsiveHelper_datatable_fixed_column_cancelacion = undefined;
            var responsiveHelper_datatable_col_reorder_cancelacion = undefined;
            var responsiveHelper_datatable_tabletools_cancelacion = undefined;

            //Variables tabla de multas
            var responsiveHelper_dt_basic_multas = undefined;
            var responsiveHelper_datatable_fixed_column_multas = undefined;
            var responsiveHelper_datatable_col_reorder_multas = undefined;
            var responsiveHelper_datatable_tabletools_multas = undefined;

            //Variables tabla de recibos
            var responsiveHelper_dt_basic_recibos = undefined;
            var responsiveHelper_datatable_fixed_column_recibos = undefined;
            var responsiveHelper_datatable_col_reorder_recibos = undefined;
            var responsiveHelper_datatable_tabletools_recibos = undefined;

            //Variables tabla de cortes
            var responsiveHelper_dt_basic_cortes = undefined;
            var responsiveHelper_datatable_fixed_column_cortes = undefined;
            var responsiveHelper_datatable_col_reorder_cortes = undefined;
            var responsiveHelper_datatable_tabletools_cortes = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = true;
            tablaegresos();
            function tablaegresos() {
                responsiveHelper_dt_basic = undefined;
                window.table = $('#dt_basic_egresos').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    destroy: true,
                    /* "scrollY":        "350px"*/
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic_egresos'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic_egresos').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            //$('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                            $('td', row).eq(4).addClass('strikeout');
                            $('td', row).eq(5).addClass('strikeout');
                            $('td', row).eq(6).addClass('strikeout');
                        }
                    },
                    ajax: {
                        type: "POST",
                        url: "caja.aspx/getEgresos",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            data: "Id",
                            name: "Id"
                        },
                        {
                            name: "Folio",
                            data: "Folio"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "Concepto",
                            data: "Concepto"
                        },
                        null,
                        {
                            name: "PersonaQuePaga",
                            data: "PersonaQuePaga"
                        },
                        {
                            name: "Observacion",
                            data: "Observacion"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            targets: 4,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.Total > 0) return "<div class='text-right'>" + toCurrency(row.Total) + "</div>";
                                else return "0.00";

                            }
                        },
                        {
                            targets: 7,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "editEgreso";
                                var registro = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Concepto + '" data-tracking="' + row.TrackingId + '" data-observacion="' + row.Observacion + '" data-habilitado="' + row.Habilitado + '" data-total="' + row.Total + '" data-persona="' + row.PersonaQuePaga + '" title = "Editar" > <i class="glyphicon glyphicon-pencil"></i></a>&nbsp; ';

                                if (row.Habilitado) {
                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                }
                                else {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                                }
                                if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                    registro = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Concepto + '" data-tracking="' + row.TrackingId + '" data-observacion="' + row.Observacion + '" data-habilitado="' + row.Habilitado + '"  title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;'
                                    block = "";
                                }
                                var block = ""
                                if ($("#ctl00_contenido_LCADLW").val() == "true") block='<a class="btn btn-' + color + ' btn-circle blockEgreso" href="javascript:void(0);" data-value="' + row.Concepto + '" data-trackingid="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                                //var block = '<a class="btn btn-' + color + ' btn-circle blockEgreso" href="javascript:void(0);" data-value="' + row.Concepto + '" data-trackingid="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                                return registro + block + '<a name="history" class="btn btn-default btn-circle historialeg" href="javascript:void(0);" data-value="' + row.Concepto + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                            }
                        }
                    ]
                });
            }
            cargaringresos();
            function cargaringresos() {
                responsiveHelper_dt_basic_ingresos = undefined;
                window.tableingreso = $('#dt_basic_ingresos').dataTable({

                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    destroy: true,
                    //"scrollY":        "350px",
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_ingresos) {
                            responsiveHelper_dt_basic_ingresos = new ResponsiveDatatablesHelper($('#dt_basic_ingresos'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_ingresos.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_ingresos.respond();
                        $('#dt_basic_ingresos').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            //$('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                            $('td', row).eq(4).addClass('strikeout');
                            $('td', row).eq(5).addClass('strikeout');
                            $('td', row).eq(6).addClass('strikeout');
                        }
                    },
                    ajax: {
                        type: "POST",
                        url: "caja.aspx/getIngresos",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            data: "Id",
                            name: "Id"
                        },
                        {
                            name: "Folio",
                            data: "Folio"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "Concepto",
                            data: "Concepto"
                        },
                        null,
                        {
                            name: "PersonaQuePaga",
                            data: "PersonaQuePaga"
                        },
                        {
                            name: "Observacion",
                            data: "Observacion"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            targets: 4,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.Total > 0) return "<div class='text-right'>" + toCurrency(row.Total) + "</div>";
                                else return "0.00";

                            }
                        },
                        {
                            targets: 7,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "editIngreso";
                                var registro = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Concepto + '" data-tracking="' + row.TrackingId + '" data-observacion="' + row.Observacion + '" data-habilitado="' + row.Habilitado + '" data-total="' + row.Total + '" data-persona="' + row.PersonaQuePaga + '" title = "Editar" > <i class="glyphicon glyphicon-pencil"></i></a>&nbsp; ';

                                if (row.Habilitado) {
                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                }
                                else {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                                }
                                if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                    registro = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Concepto + '" data-tracking="' + row.TrackingId + '" data-observacion="' + row.Observacion + '" data-habilitado="' + row.Habilitado + '"  title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;'
                                    block = "";
                                }

                                 var block = "";
                                if ($("#ctl00_contenido_LCADLW").val() == "true") block='<a class="btn btn-' + color + ' btn-circle blockIngreso" href="javascript:void(0);" data-value="' + row.Concepto + '" data-trackingid="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';


                                return registro + block + '  <a name="history" class="btn btn-default btn-circle historialing" href="javascript:void(0);" data-value="' + row.Concepto + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                            }
                        }
                    ]
                });


            }

            $('#dt_basic_cancelaciones').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_cancelacion) {
                        responsiveHelper_dt_basic_cancelacion = new ResponsiveDatatablesHelper($('#dt_basic_cancelaciones'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_cancelacion.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_cancelacion.respond();
                    $('#dt_basic_cancelaciones').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        //$('td', row).eq(0).addClass('strikeout');
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "caja.aspx/getCancelaciones",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        data: "Id",
                        name: "Id"
                    },                    
                    {
                        name: "Folio",
                        data: "Folio"
                    },
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },
                    {
                        name: "Tipo",
                        data: "Tipo"
                    },
                    null,
                    {
                        name: "PersonaQuePaga",
                        data: "PersonaQuePaga"
                    },
                    {
                        name: "Observacion",
                        data: "Observacion"
                    },
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 4,
                        orderable: false,
                        render:function (data, type, row, meta) {                                
                          if (row.Total > 0) return "<div class='text-right'>" + toCurrency(row.Total) + "</div>";
                                else return "0.00";
                                    
                            }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "editCancelacion";
                            var registro = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Concepto + '" data-tracking="' + row.TrackingId + '" data-observacion="' + row.Observacion + '" data-habilitado="' + row.Habilitado + '" data-total="' + row.Total + '" data-persona="' + row.PersonaQuePaga + '" title = "Editar" > <i class="glyphicon glyphicon-pencil"></i></a>&nbsp; ';

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                registro = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Concepto + '" data-tracking="' + row.TrackingId + '" data-observacion="' + row.Observacion + '" data-habilitado="' + row.Habilitado + '"  title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;'
                                block = "";
                            }
                            var block = "";
                            if ($("#ctl00_contenido_LCADLW").val() == "true")block='<a class="btn btn-' + color + ' btn-circle blockCancelacion" href="javascript:void(0);" data-value="' + row.Folio + '" data-trackingid="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return block;
                        }
                    }
                ]
            });

            $('#dt_basic_multas').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_multas) {
                        responsiveHelper_dt_basic_multas = new ResponsiveDatatablesHelper($('#dt_basic_multas'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_multas.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_multas.respond();
                    $('#dt_basic_multas').waitMe('hide');
                },

                //"createdRow": function (row, data, index) {
                //    if (!data["Habilitado"]) {
                //        $('td', row).eq(1).addClass('strikeout');
                //        $('td', row).eq(2).addClass('strikeout');
                //        $('td', row).eq(3).addClass('strikeout');

                //    }
                //},
                ajax: {
                    type: "POST",
                    url: "caja.aspx/getMultas",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id"
                    },                  
                    {
                        name: "RutaImagen",
                        data: "RutaImagen"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    null,
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false

                    }
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 6,
                        orderable: false,
                        render:function (data, type, row, meta) {                                
                          if (row.TotalAPagar > 0) return "<div class='text-right'>" + toCurrency(row.TotalAPagar) + "</div>";
                                else return "0.00";
                                    
                            }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                           if (row.RutaImagen != "" && row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");                                
                                var imgAvatar = resolveUrl(photo);                                
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "pagar";
                            var registro = '<a style="width: 10vw" class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" title = "Pagar" ><i class="glyphicon glyphicon-pencil"></i> Pagar</a>&nbsp; ';

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                registro = '<a class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;'
                                block = "";
                            }                            

                            return registro;
                        }
                    }
                ]
            });
            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }
            $('#dt_basic_recibos').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY": "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_recibos) {
                        responsiveHelper_dt_basic_recibos = new ResponsiveDatatablesHelper($('#dt_basic_recibos'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_recibos.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_recibos.respond();
                    $('#dt_basic_recibos').waitMe('hide');
                },

                //"createdRow": function (row, data, index) {
                //    if (!data["Habilitado"]) {
                //        $('td', row).eq(1).addClass('strikeout');
                //        $('td', row).eq(2).addClass('strikeout');
                //        $('td', row).eq(3).addClass('strikeout');

                //    }
                //},
                ajax: {
                    type: "POST",
                    url: "caja.aspx/getRecibos",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id"
                    },
                    {
                        name: "ReciboId",
                        data: "ReciboId"
                    },
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },                    
                    //{
                    //    name: "TotalAPagar",
                    //    data: "TotalAPagar"
                    //},                    
                    null,
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    }, 
                     {
                        targets: 1,
                        orderable: false,
                         render: function (data, type, row, meta) {   
                            
                             if (row.ReciboId != 0) {

                                 return row.ReciboId;
                             }
                             else { return row.IdRecibo;}
                        }
                    },
                     {
                        targets: 7,
                        orderable: false,
                        render:function (data, type, row, meta) {                                
                          if (row.TotalAPagar > 0) return "<div class='text-right'>" + toCurrency(row.TotalAPagar) + "</div>";
                                else return "0.00";
                                    
                            }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "imprimir";
                            var registro = '<a style="width: 20vw" class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" data-reciboid="' + row.ReciboTracking + '" title = "Imprimir" ><i class="fa fa-file-pdf-o"></i> Imprimir recibo</a>';

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                registro = '<a class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" data-reciboid="' + row.ReciboTracking + '" title="Imprimir"><i class="fa fa-file-pdf-o"></i></a>&nbsp;'
                                block = "";
                            }                            

                            return registro;
                        }
                    }
                ]
            });
            var responsiveHelper_dt_basichistorying = undefined;
            var breakpointhistoryDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistorying = true;
            window.tablehistorying = $("#dt_basichistorying").dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[4, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistorying) {
                        responsiveHelper_dt_basichistorying = new ResponsiveDatatablesHelper($("#dt_basichistorying"), breakpointhistoryDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistorying.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistorying.respond();
                    $("#dt_basichistorying").waitMe("hide");
                },
                ajax: {
                    type: "POST",
                    url: "caja.aspx/GetIngresoslog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosserverside) {
                        $("#dt_basichistorying").waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var centroid = $('#idhisting').val() ;
                        parametrosserverside.centroid = centroid;
                        parametrosserverside.todoscancelados = false;
                        parametrosserverside.emptytable = emptytablehistorying;
                        return JSON.stringify(parametrosserverside);
                    }
                },
                columns: [
                    {
                        name: "Folio",
                        data: "Folio",
                        odertable: false
                    },
                    {
                        name: "Concepto",
                        data: "Concepto",
                        ordertable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        ordertable: false
                    },

                    {
                        name: "Creadopor",
                        data: "Creadopor",
                        ordertable: false
                    }
                    ,
                    {
                        name: "Fec_Movto",
                        data: "Fec_Movto",
                        ordertable: false
                    }
                ],

            });
            var responsiveHelper_dt_basichistoryegr = undefined;
            var breakpointhistoryegrDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistoryegr = true;
            window.tablehistoryegr = $("#dt_basichistoryiegre").dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[4, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistoryegr) {
                        responsiveHelper_dt_basichistoryegr = new ResponsiveDatatablesHelper($("#dt_basichistoryiegre"), breakpointhistoryegrDefinition);

                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistoryegr.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistoryegr.respond();
                    $("#dt_basichistoryiegre").waitMe("hide");
                },
                ajax: {
                    type: "POST",
                    url: "caja.aspx/GetEgresolog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosserverside) {
                        $("#dt_basichistoryiegre").waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var centroid = $('#idhistegr').val();
                        parametrosserverside.centroid = centroid;
                        parametrosserverside.todoscancelados = false;
                        parametrosserverside.emptytable = emptytablehistoryegr;
                        return JSON.stringify(parametrosserverside);
                    }
                },
                columns: [
                    {
                        name: "Folio",
                        data: "Folio",
                        odertable: false
                    },
                    {
                        name: "Concepto",
                        data: "Concepto",
                        ordertable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        ordertable: false
                    },

                    {
                        name: "Creadopor",
                        data: "Creadopor",
                        ordertable: false
                    }
                    ,
                    {
                        name: "Fec_Movto",
                        data: "Fec_Movto",
                        ordertable: false
                    }
                ],
            });
            $("body").on("click", ".historialeg", function () {
                var id = $(this).attr("data-id");
                $("#idhistegr").val(id);
                var descripcion = $(this).attr("data-value");
                $("#descripcionhistorialegreso").text(descripcion);
                $("#historyegreso-modal").modal("show");
                window.emptytablehistoryegr = false;
                window.tablehistoryegr.api().ajax.reload();
            });
            $("body").on("click", ".historialing", function () {
                var id = $(this).attr("data-id");
                $("#idhisting").val(id);
                var descripcion = $(this).attr("data-value");
                $("#descripcionhistorialingreso").text(descripcion);

                $("#historyingreso-modal").modal("show");
                window.emptytablehistorying = false;
                window.tablehistorying.api().ajax.reload();
            });
            $('#dt_basic_cortes').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_cortes) {
                        responsiveHelper_dt_basic_cortes = new ResponsiveDatatablesHelper($('#dt_basic_cortes'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_cortes.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_cortes.respond();
                    $('#dt_basic_cortes').waitMe('hide');
                },

                //"createdRow": function (row, data, index) {
                //    if (!data["Habilitado"]) {
                //        $('td', row).eq(1).addClass('strikeout');
                //        $('td', row).eq(2).addClass('strikeout');
                //        $('td', row).eq(3).addClass('strikeout');

                //    }
                //},
                ajax: {
                    type: "POST",
                    url: "caja.aspx/getCortes",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id"
                    },                  
                    {
                        name: "FechaInicio",
                        data: "FechaInicio"
                    },
                    {
                        name: "FechaFin",
                        data: "FechaFin"
                    },
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto"
                    },                    
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },                    
                    {
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "imprimirCorte";
                            var registro = '<a style="width: 20vw" class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-tracking="' + row.TrackingId + '" title = "Imprimir" ><i class="fa fa-file-pdf-o"></i> Imprimir corte</a>';

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                registro = '<a class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-tracking="' + row.TrackingId + '" title="Imprimir"><i class="fa fa-file-pdf-o"></i></a>&nbsp;'
                                block = "";
                            }                            

                            return registro;
                        }
                    }
                ]
            });

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
    <script>
        function reload() {
            $("#dt_basic_cancelaciones").DataTable().ajax.reload();
            $("#dt_basic").DataTable().ajax.reload();
            $("#dt_basic_ingresos").DataTable().ajax.reload();
            $("#dt_basic_multas").DataTable().ajax.reload();
            $("#dt_basic_recibos").DataTable().ajax.reload();
            $("#dt_basic_cortes").DataTable().ajax.reload();
            $("#dt_basic_egresos").DataTable().ajax.reload();
        }
    </script>
</asp:Content>

