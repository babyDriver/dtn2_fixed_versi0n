﻿$(function () {

    $("body").on("click", ".refreshnotify", function () {
        refreshNotifyInit();
    });

    $('#btndisplaynotify').click(function () {
        refreshNotifyInit();
    });

    
    function getNotify() {

        var ruta = $(location).attr('href');

        var url = "Handlers/notifyHandler.ashx";

        if (ruta.includes("Application/Admin")) {
            url = "../Handlers/notifyHandler.ashx";
        }

        var data = new FormData();

        var notificacionli = '';

        var notificacion = document.getElementById("notifybyuser");

        if (notificacion != null) {
            document.getElementById("notifybyuser").innerHTML = "";
        }

        $('#notifybyuser').innerHTML="";
       

        $.ajax({
            url: url,
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (Results) {
                if (Results.exitoso) {

                    $.each(Results.notificacion, function (index, item) {
                                              
                        notificacionli = '<li>' +
                           '<span>' +
                           '<a data-notifytrackingid="' + item.TrackingId + '" data-notifytitle="' + item.Titulo + '" data-notifymsgbody="' + item.Detalle + '" href="javascript:void(0);" class="msg modalNotify">' +
                           '<time>' + item.FechaNotificacion.toLocaleString() + '</time>' +
                           '<span class="subject">' + item.Titulo + '</span>' +
                           '<span class="msg-body">' + item.Detalle + '</span>' +
                           '</a>' +
                           '</span>' +
                           '</li>';
                        
                        $('#notifybyuser').append(notificacionli);
                    });
                    
                }
            },
            error: function (err) {
            }
        });
    }
        
    

    function refreshNotifyInit() {

        getNotify();

        var now = new Date().toLocaleString();

        var span = document.getElementById("refreshNotifySpan");
        var txt = document.createTextNode("Ultima actualización " + now);
        span.innerText = txt.textContent;

    }

    //refreshNotifyInit();

    $("body").on("click", ".modalNotify", function () {
        var id = $(this).attr("data-notifytrackingid");
        var titulo = $(this).attr("data-notifytitle");
        var notificacion = $(this).attr("data-notifymsgbody");
        $("#notifyTitle").text(titulo);
        $("#notifyMessage").text(notificacion);
        $("#checkoff").attr("data-id", id);
        $("#notify-modal").modal("show");
    });

    $("#checkoff").unbind("click").on("click", function () {
        var id = $(this).attr("data-id");
       
        var ruta = $(location).attr('href');

        var url = "Handlers/notifyHandler.ashx?action=2&Tracking=" + id;

        if (ruta.includes("Application/Admin")) {
            url = "../Handlers/notifyHandler.ashx?action=2&Tracking="+id;
        }

        var data = new FormData();

        $.ajax({
            url: url,
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (Results) {
                if (Results.exitoso) {
                    refreshNotifyInit();
                }
                $("#notify-modal").modal("hide");
            },
            error: function (err) {
                $("#notify-modal").modal("hide");
            }
        });

        

    });
    
});

