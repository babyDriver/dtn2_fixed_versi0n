﻿$(function () {
    $('.decimal').maskMoney({
        thousands: ',',
        decimal: '.',
        allowZero: true,
        suffix: ''
    });

    $(".integer").keypress(function (event) {
        return /\d/.test(String.fromCharCode(event.keyCode));
    });
});

function ShowError(field, errormessage) {
    $.smallBox({
        title: field,
        content: errormessage,
        color: "#C46A69",
        iconSmall: "fa fa-warning bounce animated",
        timeout: 4000
    });
}

function ShowSuccess(title, message) {
    $.smallBox({
        title: title,
        content: message,
        color: "#739E73",
        iconSmall : "fa fa-thumbs-up bounce animated",
        timeout : 4000
    });
}
function ShowAlert(field, errormessage) {
    $.smallBox({
        title: field,
        content: errormessage,
        color: "#c79121",
        iconSmall: "fa fa-warning bounce animated",
        timeout: 4000
    });
}
//Validar una URL
function validateUrl(url) {
    return (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(url));
}

//Validar URL compranet
function validateUrlCompranet(url) {
    return /compranet.funcionpublica.gob.mx/i.test(url);
}

function ValidateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function toDateTime(string) {
    var fecha = new Date(parseFloat(string.substr(6)));
    var dia = "0" + fecha.getDate();
    var mes = "0" + (fecha.getMonth() + 1);
    var anio = fecha.getFullYear();
    var minutos = "0" + fecha.getMinutes();
    var segundos = "0" + fecha.getSeconds();
    var time = fecha.getHours() + ":" + minutos.slice(-2) + ":" + segundos.slice(-2);

    return dia.slice(-2) + "/" + mes.slice(-2) + "/" + anio + " " + time;
}

function toDate(string) {
    var today = new Date(parseFloat(string.substr(6)));
    var dd = today.getUTCDate();
    var mm = today.getUTCMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    return today = dd + '/' + mm + '/' + yyyy;
}

//stringToDate("17/9/2014", "dd/MM/yyyy", "/");
//stringToDate("9/17/2014", "mm/dd/yyyy", "/")
//stringToDate("9-17-2014", "mm-dd-yyyy", "-")
//Convierte una fecha en string, se indica el formato y el delimitador
function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]);
    month -= 1;
    var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    return formatedDate;
}

function toCurrency(number) {
    var number = number.toString(),
    dollars = number.split('.')[0],
    cents = (number.split('.')[1] || '') + '00';
    dollars = dollars.split('').reverse().join('')
        .replace(/(\d{3}(?!$))/g, '$1,')
        .split('').reverse().join('');
    return '$' + dollars + '.' + cents.slice(0, 2);
}

$.formatMoney = function (n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function RequestQueryString(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function startLoading(text) {
    text == "" ? "Cargando..." : text;
    $('#main').waitMe({
        effect: 'bounce',
        text: text,
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: '',
        source: ''
    });
}

function startLoading() {
    $('#main').waitMe({
        effect: 'bounce',
        text: 'Cargando...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: '',
        source: ''
    });
}

function endLoading() {
    $('#main').waitMe('hide');
}

function FileValidation(validFileExtensions, controlui) {
    var esvalido = true;

    var files = $(controlui).get(0).files;
    if (files.length > 0) {
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var valid = false;

            for (var j = 0; j < validFileExtensions.length; j++) {
                var extension = validFileExtensions[j];
                //TODO: Fixbug when compares pdf vs .jpeg
                if (file.name.substr(file.name.length - extension.length, extension.length).toLowerCase() == extension.toLowerCase()) {
                    valid = true;
                    break;
                }
            }

            if (valid) {
                $(controlui).parent().removeClass("state-error").addClass('state-success');
                $(controlui).addClass('valid');
            }
            else {
                ShowError("Archivo", "Solo se permiten archivos con extensión: " + validFileExtensions.join(", "));
                $(controlui).parent().removeClass('state-success').addClass("state-error");
                $(controlui).removeClass('valid');
                esvalido = false;
            }
        }
    }
    else {
        ShowError("Archivo", "Elige un archivo para cargar");
        $(controlui).parent().removeClass('state-success').addClass("state-error");
        $(controlui).removeClass('valid');
        esvalido = false;
    }
    return esvalido;
}

function obtenerString(val) {
    if (arguments.length === 0) {
        return "";
    } else if (typeof val === "undefined") {
        return "undefined";
    } else if (val === null) {
        return "null";
    } else if (typeof val === "boolean") {
        return val ? "true" : "false";
    } else if (typeof val === "number") {
        val.toString();
    } else if (typeof val === "string") {
        return val;
    } else {
        JSON.stringify(val);
    }
};

function limitLines(obj, e) {
    var keynum, lines = 1;
    if (window.event) {
        keynum = e.keyCode;
    } else if (e.which) {
        keynum = e.which;
    }

    if (keynum == 13) {
        if (lines == obj.rows) {
            return false;
        } else {
            lines++;
        }
    }
}

$.urlGen = function (anio) {
    var match,
        search = /([^&=]+)=?([^&]*)/ig,
        decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); },
        query = window.location.search.substring(1);
    var url = location.pathname + "?anio=" + anio;
    while (match = search.exec(query))
        if (decode(match[1]) != "anio")
            url += "&" + decode(match[1]) + "=" + decode(match[2]);

    return url;
}

function CargarListaAnios(setanio, inicio, fin) {
    var Dropdown = $("#anio");
    Dropdown.append(new Option("[TODOS]", "0"));

    for (var anio = inicio; anio <= fin; anio++) {
        Dropdown.append("<option value='" + anio + "'>" + anio + "</option>");
    }

    if (setanio > 0) {
        Dropdown.val(setanio);
    }
}


function disabledselect(controlname) {
    $(controlname).select2({
        containerCss: {
            "background": "#eee",
            "cursor": "not-allowed",
        }
    });
}

function enabledselect(controlname) {
    $(controlname).select2({
        containerCss: {
            "background": "#FFFFFF",
            "cursor": "default",
        }
    });
}


alphanumeric(".alphanumeric");
function alphanumeric(textbox) {

    var mask = new RegExp('^[a-zA-Z0-9!\r\n\u00f1\u00d1À-ÿ @#,:;-|\¡?¿/\$%\^\&*\)}\(+=._-]*$');
    $(textbox).keypress(function (event) {
        if (!event.charCode) return true;
        var part1 = this.value.substring(0, this.selectionStart);
        var part2 = this.value.substring(this.selectionEnd, this.value.length);
        if (!mask.test(part1 + String.fromCharCode(event.charCode) + part2))
            return false;
    });



}