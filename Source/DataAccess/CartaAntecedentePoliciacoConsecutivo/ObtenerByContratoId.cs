﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.CartaAntecedentePoliciacoConsecutivo
{
    public class ObtenerByContratoId : ISelectFactory<int>
    {
    
        public DbCommand ConstructSelectCommand(Database db, int Contratoid)
        {
            DbCommand cmd = db.GetStoredProcCommand("GetCartaAntecedentesConsecutivosByContratoId_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Contratoid);
            return cmd;
        }
    }
}
