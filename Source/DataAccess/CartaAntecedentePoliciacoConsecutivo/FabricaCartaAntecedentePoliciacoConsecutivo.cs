﻿using System.Data;
using DataAccess.Interfaces;


namespace DataAccess.CartaAntecedentePoliciacoConsecutivo
{
    public class FabricaCartaAntecedentePoliciacoConsecutivo : IEntityFactory<Entity.CartaAntecedentePoliciacoConsecutivo>
    {
        public Entity.CartaAntecedentePoliciacoConsecutivo ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CartaAntecedentePoliciacoConsecutivo();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrakingId = dr.GetString(index);
            }
            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Consecutivo");
            if (!dr.IsDBNull(index))
            {
                item.Consecutivo = dr.GetInt32(index);
            }
           
            index = dr.GetOrdinal("Ciudadano");
            if (!dr.IsDBNull(index))
            {
                item.Ciudadano = dr.GetString(index);
            }



            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Rutafoto");
            if (!dr.IsDBNull(index))
            {
                item.Rutafoto = dr.GetString(index);
            }
            index = dr.GetOrdinal("Domicilio");
            if (!dr.IsDBNull(index))
            {
                item.Domicilio = dr.GetString(index);
            }
            index = dr.GetOrdinal("Ciudad");
            if (!dr.IsDBNull(index))
            {
                item.Ciudad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Antecedentes");
            if (!dr.IsDBNull(index))
            {
                item.Antecedentes = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Anio");
            if (!dr.IsDBNull(index))
            {
                item.Anio = dr.GetInt32(index);
            }


        
            index = dr.GetOrdinal("FechaAntecedente");
            if (!dr.IsDBNull(index))
            {
                item.FechaAntecedente = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Elaboro");
            if (!dr.IsDBNull(index))
            {
                item.Elaboro = dr.GetString(index);
            }

            index = dr.GetOrdinal("Reviso");
            if (!dr.IsDBNull(index))
            {
                item.Reviso = dr.GetString(index);
            }
            index = dr.GetOrdinal("ReciboCaja");
            if (!dr.IsDBNull(index))
            {
                item.ReciboCaja = dr.GetString(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }


            return item;

        }
    }
}
