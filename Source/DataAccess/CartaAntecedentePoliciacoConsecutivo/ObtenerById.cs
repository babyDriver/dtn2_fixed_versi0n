﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.CartaAntecedentePoliciacoConsecutivo
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("GetCartaAntecedentesConsecutivosById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, Id);
            return cmd;
        }
    }
}
