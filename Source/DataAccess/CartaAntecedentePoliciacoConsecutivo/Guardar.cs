﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CartaAntecedentePoliciacoConsecutivo
{
    public class Guardar : IInsertFactory<Entity.CartaAntecedentePoliciacoConsecutivo>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CartaAntecedentePoliciacoConsecutivo carta)
        {
            DbCommand cmd = db.GetStoredProcCommand("CartaAntecedentesConsecutivos_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, carta.TrakingId.ToString());
            db.AddInParameter(cmd, "_Ciudadano", DbType.String, carta.Ciudadano.ToString());
            db.AddInParameter(cmd, "_Consecutivo", DbType.Int32, carta.Consecutivo.ToString());
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, carta.ContratoId.ToString());
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, carta.Fecha);
            db.AddInParameter(cmd, "_Rutafoto", DbType.String, carta.Rutafoto.ToString());
            db.AddInParameter(cmd, "_Domicilio", DbType.String, carta.Domicilio.ToString());
            db.AddInParameter(cmd, "_Ciudad", DbType.String, carta.Ciudad.ToString());
            db.AddInParameter(cmd, "_Antecedentes", DbType.Int32, carta.Antecedentes);
            db.AddInParameter(cmd, "_Anio", DbType.Int32, carta.Anio);
            db.AddInParameter(cmd, "_FechaAntecedente", DbType.DateTime, carta.FechaAntecedente);
            db.AddInParameter(cmd, "_Elaboro", DbType.String, carta.Elaboro.ToString());
            db.AddInParameter(cmd, "_Reviso", DbType.String, carta.Reviso.ToString());
            db.AddInParameter(cmd, "_ReciboCaja", DbType.String, carta.ReciboCaja.ToString());

            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, carta.CreadoPor);

            return cmd;
        }
    }
}
