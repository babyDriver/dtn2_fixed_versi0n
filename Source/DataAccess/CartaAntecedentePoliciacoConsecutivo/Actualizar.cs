﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CartaAntecedentePoliciacoConsecutivo
{
    public class Actualizar : IUpdateFactory<Entity.CartaAntecedentePoliciacoConsecutivo>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.CartaAntecedentePoliciacoConsecutivo carta)
        {
            DbCommand cmd = db.GetStoredProcCommand("CartaAntecedentesConsecutivos_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, carta.Id);
            return cmd;
        }
    }
}
