﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CartaAntecedentePoliciacoConsecutivo
{
    public interface ICartaAntecendentePoliciacoConsecutivo
    {
        int Guardar(Entity.CartaAntecedentePoliciacoConsecutivo carta);
        void Actualizar(Entity.CartaAntecedentePoliciacoConsecutivo carta);
        Entity.CartaAntecedentePoliciacoConsecutivo GetByContratoId(int ContratoId);
        Entity.CartaAntecedentePoliciacoConsecutivo GetById(int Id);
    }
}
