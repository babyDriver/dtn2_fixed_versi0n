﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;
namespace DataAccess.CartaAntecedentePoliciacoConsecutivo
{
   public class ServicioCartaAntecedentePoliciacoConsecutivo : ClsAbstractService<Entity.CartaAntecedentePoliciacoConsecutivo>, ICartaAntecendentePoliciacoConsecutivo
    {

        public ServicioCartaAntecedentePoliciacoConsecutivo(string dataBase)
          : base(dataBase)
        {
        }
        public void Actualizar(Entity.CartaAntecedentePoliciacoConsecutivo carta)
        {
            IUpdateFactory<Entity.CartaAntecedentePoliciacoConsecutivo> update = new Actualizar();
            Update(update, carta);
        }

        public Entity.CartaAntecedentePoliciacoConsecutivo GetByContratoId(int ContratoId)
        {
            ISelectFactory<int> select = new ObtenerByContratoId();
            return GetByKey(select, new FabricaCartaAntecedentePoliciacoConsecutivo(), ContratoId);
        }

        public Entity.CartaAntecedentePoliciacoConsecutivo GetById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaCartaAntecedentePoliciacoConsecutivo(), Id);
        }

        public int Guardar(Entity.CartaAntecedentePoliciacoConsecutivo carta)
        {
            IInsertFactory<Entity.CartaAntecedentePoliciacoConsecutivo> insert = new Guardar();
            return Insert(insert, carta);
        }
    }
}
