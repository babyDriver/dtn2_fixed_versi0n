﻿using System;
using System.Collections.Generic;

namespace DataAccess.Antecedente
{
    public interface IAntecedente
    {
        List<Entity.Antecedente> ObtenerTodos();
        Entity.Antecedente ObtenerById(int Id);
        Entity.Antecedente ObtenerByDetenidoId(int Id);
        Entity.Antecedente ObtenerByProcesoId(int Id);
        Entity.Antecedente ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Antecedente antecedente);
        void Actualizar(Entity.Antecedente antecedente);
    }
}