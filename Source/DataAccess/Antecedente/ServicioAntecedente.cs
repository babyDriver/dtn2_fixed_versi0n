﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Antecedente
{
    public class ServicioAntecedente : ClsAbstractService<Entity.Antecedente>, IAntecedente
    {
        public ServicioAntecedente(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Antecedente> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaAntecedente(), new Entity.NullClass());
        }

        public Entity.Antecedente ObtenerByTrackingId(Guid trackinid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaAntecedente(), trackinid);
        }

        public Entity.Antecedente ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaAntecedente(), Id);
        }

        public Entity.Antecedente ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaAntecedente(), Id);
        }

        public Entity.Antecedente ObtenerByProcesoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByProcesoId();
            return GetByKey(select, new FabricaAntecedente(), Id);
        }

        public int Guardar(Entity.Antecedente antecedente)
        {
            IInsertFactory<Entity.Antecedente> insert = new Guardar();
            return Insert(insert, antecedente);
        }

        public void Actualizar(Entity.Antecedente antecedente)
        {
            IUpdateFactory<Entity.Antecedente> update = new Actualizar();
            Update(update, antecedente);
        }


    }
}