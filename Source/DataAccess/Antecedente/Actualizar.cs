﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Antecedente
{
    public class Actualizar : IUpdateFactory<Entity.Antecedente>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Antecedente antecedente)
        {
            DbCommand cmd = db.GetStoredProcCommand("Antecedente_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, antecedente.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, antecedente.TrackingId);
            db.AddInParameter(cmd, "_ProcesoId", DbType.String, antecedente.ProcesoId);
            db.AddInParameter(cmd, "_Averiguacion", DbType.String, antecedente.Averiguacion);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, antecedente.Fecha);
            db.AddInParameter(cmd, "_Consignado", DbType.String, antecedente.Consignado);
            db.AddInParameter(cmd, "_FechaRadicacion", DbType.DateTime, antecedente.FechaRadicacion);
            db.AddInParameter(cmd, "_Numero", DbType.Int32, antecedente.Numero);
            db.AddInParameter(cmd, "_FechaEjecucion", DbType.DateTime, antecedente.FechaEjecucion);
            db.AddInParameter(cmd, "_Autoridad", DbType.String, antecedente.Autoridad);
            db.AddInParameter(cmd, "_FechaEmpezo", DbType.DateTime, antecedente.FechaEmpezo);
            db.AddInParameter(cmd, "_Anios", DbType.Int32, antecedente.Anios);
            db.AddInParameter(cmd, "_Meses", DbType.Int32, antecedente.Meses);
            db.AddInParameter(cmd, "_Dias", DbType.Int32, antecedente.Dias);
            db.AddInParameter(cmd, "_TipoId", DbType.Int32, antecedente.TipoId);
            db.AddInParameter(cmd, "_FueroId", DbType.Int32, antecedente.FueroId);
            db.AddInParameter(cmd, "_JuzgadoId", DbType.Int32, antecedente.JuzgadoId);
            db.AddInParameter(cmd, "_CentroId", DbType.Int32, antecedente.CentroId);
            db.AddInParameter(cmd, "_FechaPrision", DbType.DateTime, antecedente.FechaPrision);
            db.AddInParameter(cmd, "_FechaSentencia", DbType.DateTime, antecedente.FechaSentencia);
            db.AddInParameter(cmd, "_FechaExternacion", DbType.DateTime, antecedente.FechaExternacion);
            db.AddInParameter(cmd, "_Motivo", DbType.String, antecedente.Motivo);
            db.AddInParameter(cmd, "_Terminos", DbType.String, antecedente.Terminos);
            db.AddInParameter(cmd, "_Observacion", DbType.String, antecedente.Observacion);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, antecedente.DetenidoId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, antecedente.Habilitado);


            return cmd;
        }
    }
}
