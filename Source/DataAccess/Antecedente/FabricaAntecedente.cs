﻿using DataAccess.Interfaces;


namespace DataAccess.Antecedente
{
    public class FabricaAntecedente : IEntityFactory<Entity.Antecedente>
    {
       

        public Entity.Antecedente ConstructEntity(System.Data.IDataReader dr)
        {
            var antecedente = new Entity.Antecedente();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                antecedente.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                antecedente.TrackingId = dr.GetGuid(index);
            }

       

            index = dr.GetOrdinal("ProcesoId");
            if (!dr.IsDBNull(index))
            {
                antecedente.ProcesoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Averiguacion");
            if (!dr.IsDBNull(index))
            {
                antecedente.Averiguacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                antecedente.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Consignado");
            if (!dr.IsDBNull(index))
            {
                antecedente.Consignado = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaRadicacion");
            if (!dr.IsDBNull(index))
            {
                antecedente.FechaRadicacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Numero");
            if (!dr.IsDBNull(index))
            {
                antecedente.Numero = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaEjecucion");
            if (!dr.IsDBNull(index))
            {
                antecedente.FechaEjecucion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Autoridad");
            if (!dr.IsDBNull(index))
            {
                antecedente.Autoridad = dr.GetString(index);
            }


            index = dr.GetOrdinal("FechaEmpezo");
            if (!dr.IsDBNull(index))
            {
                antecedente.FechaEmpezo = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Anios");
            if (!dr.IsDBNull(index))
            {
                antecedente.Anios = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Meses");
            if (!dr.IsDBNull(index))
            {
                antecedente.Meses = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Dias");
            if (!dr.IsDBNull(index))
            {
                antecedente.Dias = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TipoId");
            if (!dr.IsDBNull(index))
            {
                antecedente.TipoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FueroId");
            if (!dr.IsDBNull(index))
            {
                antecedente.FueroId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("JuzgadoId");
            if (!dr.IsDBNull(index))
            {
                antecedente.JuzgadoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CentroId");
            if (!dr.IsDBNull(index))
            {
                antecedente.CentroId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaPrision");
            if (!dr.IsDBNull(index))
            {
                antecedente.FechaPrision = dr.GetDateTime(index);
            }


            index = dr.GetOrdinal("FechaSentencia");
            if (!dr.IsDBNull(index))
            {
                antecedente.FechaSentencia = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("FechaExternacion");
            if (!dr.IsDBNull(index))
            {
                antecedente.FechaExternacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                antecedente.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Terminos");
            if (!dr.IsDBNull(index))
            {
                antecedente.Terminos = dr.GetString(index);
            }

            index = dr.GetOrdinal("Observacion");
            if (!dr.IsDBNull(index))
            {
                antecedente.Observacion = dr.GetString(index);
            }

          
            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                antecedente.DetenidoId = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                antecedente.Habilitado = dr.GetBoolean(index);
            }


            return antecedente;
        }
    }
}