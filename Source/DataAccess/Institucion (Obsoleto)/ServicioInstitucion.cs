﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Institucion
{
    public class ServicioInstitucion : ClsAbstractService<Entity.Institucion>, IInstitucion
    {
        public ServicioInstitucion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Institucion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaInstitucion(), new Entity.NullClass());
        }
    }
}
