﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Institucion
{
    public interface IInstitucion
    {
        List<Entity.Institucion> ObtenerTodos();
    }
}
