﻿using DataAccess.Interfaces;

namespace DataAccess.Institucion
{
    public class FabricaInstitucion : IEntityFactory<Entity.Institucion>
    {
        public Entity.Institucion ConstructEntity(System.Data.IDataReader dr)
        {
            var institucion = new Entity.Institucion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                institucion.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                institucion.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                institucion.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                institucion.Descripcion = dr.GetString(index);
            }            

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                institucion.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                institucion.CreadoPor = dr.GetInt32(index);
            }

            return institucion;
        }
    }
}
