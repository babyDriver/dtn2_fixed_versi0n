﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ListadoInformacionDetencionModal
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ListadoInformacionDetencionModal_GetAll_SP");
            db.AddInParameter(cmd, "_tipo", DbType.String, parametros[0]);
            db.AddInParameter(cmd, "_contrato", DbType.Int32, parametros[1]);
            db.AddInParameter(cmd, "_detenidoId", DbType.Int32, parametros[2]);
            
            return cmd;
        }
    }
}
