﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoInformacionDetencionModal
{
    public class ServicioListadoInformacionDetencionModal : ClsAbstractService<Entity.ListadoInformacionDetencionModal>, IListadoInformacionDetencionModal
    {
        public ServicioListadoInformacionDetencionModal(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoInformacionDetencionModal> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoInformacionDetencionModal(), parametros);
        }
    }
}
