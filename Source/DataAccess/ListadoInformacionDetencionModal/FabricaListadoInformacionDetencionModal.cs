﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoInformacionDetencionModal
{
    public class FabricaListadoInformacionDetencionModal : IEntityFactory<Entity.ListadoInformacionDetencionModal>
    {
        public Entity.ListadoInformacionDetencionModal ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoInformacionDetencionModal();

            var index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoId = dr.GetInt32(index);
            }            

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaIngreso");
            if (!dr.IsDBNull(index))
            {
                item.FechaIngreso = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                item.LugarDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Institucion");
            if (!dr.IsDBNull(index))
            {
                item.Institucion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Situacion");
            if (!dr.IsDBNull(index))
            {
                item.Situacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaSalida");
            if (!dr.IsDBNull(index))
            {
                item.FechaSalida = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Fundamento");
            if (!dr.IsDBNull(index))
            {
                item.Fundamento = dr.GetString(index);
            }

            index = dr.GetOrdinal("TipoSalida");
            if (!dr.IsDBNull(index))
            {
                item.TipoSalida = dr.GetString(index);
            }

            return item;
        }
    }
}
