﻿using System.Collections.Generic;

namespace DataAccess.ListadoInformacionDetencionModal
{
    public interface IListadoInformacionDetencionModal
    {
        List<Entity.ListadoInformacionDetencionModal> ObtenerTodos(object[] data);
    }
}
