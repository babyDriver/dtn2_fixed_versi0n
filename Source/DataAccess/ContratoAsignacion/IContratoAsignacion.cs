﻿using System;
using System.Collections.Generic;

namespace DataAccess.ContratoAsignacion
{
    public interface IContratoAsignacion
    {
        List<Entity.ContratoAsignacion> ObtenerByIdUsuario(int id);
    }
}
