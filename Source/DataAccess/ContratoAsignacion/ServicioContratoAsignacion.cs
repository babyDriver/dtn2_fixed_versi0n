﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.ContratoAsignacion
{
    public class ServicioContratoAsignacion : ClsAbstractService<Entity.ContratoAsignacion>, IContratoAsignacion
    {
        public ServicioContratoAsignacion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ContratoAsignacion> ObtenerByIdUsuario(int id)
        {
            ISelectFactory<int> select = new ObtenerByIdUsuario();
            return GetAll(select, new FabricaContratoAsignacion(), id);
        }
    }
}
