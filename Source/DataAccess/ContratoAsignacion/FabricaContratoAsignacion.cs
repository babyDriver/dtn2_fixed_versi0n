﻿using DataAccess.Interfaces;

namespace DataAccess.ContratoAsignacion
{
    public class FabricaContratoAsignacion : IEntityFactory<Entity.ContratoAsignacion>
    {
        public Entity.ContratoAsignacion ConstructEntity(System.Data.IDataReader dr)
        {
            var contrato = new Entity.ContratoAsignacion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                contrato.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Contrato");
            if (!dr.IsDBNull(index))
            {
                contrato.Contrato = dr.GetString(index);
            }


            index = dr.GetOrdinal("VigenciaInicial");
            if (!dr.IsDBNull(index))
            {
                contrato.VigenciaInicial = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("VigenciaFinal");
            if (!dr.IsDBNull(index))
            {
                contrato.VigenciaFinal = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("IdUsuario");
            if (!dr.IsDBNull(index))
            {
                contrato.IdUsuario = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                contrato.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                contrato.Tipo = dr.GetString(index);
            }

            return contrato;
        }
    }
}
