﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.ExamenMedico
{
    public class ServicioExamenMedico : ClsAbstractService<Entity.ExamenMedico>, IExamenMedico
    { 
        public ServicioExamenMedico(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.ExamenMedico ObtenerByTrackingId(Guid trackingid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaExamenMedico(), trackingid);
        }

        public Entity.ExamenMedico ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaExamenMedico(), Id);
        }

        public Entity.ExamenMedico ObtenerByDetalleDetencionId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetalleDetencionId();
            return GetByKey(select, new FabricaExamenMedico(), Id);
        }

        public int Guardar(Entity.ExamenMedico item)
        {
            IInsertFactory<Entity.ExamenMedico> insert = new Guardar();
            return Insert(insert, item);
        }

        public void Actualizar(Entity.ExamenMedico item)
        {
            IUpdateFactory<Entity.ExamenMedico> update = new Actualizar();
            Update(update, item);
        }

        public List<Entity.ExamenMedico> ObtenerTodosByDetalleDetencionId(int id)
        {
            ISelectFactory<int> select = new ObtenerTodosByDetalleDetencionId();
            return GetAll(select, new FabricaExamenMedico(), id);
        }
    }
}