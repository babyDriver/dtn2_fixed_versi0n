﻿
using DataAccess.Interfaces;
using DataAccess.Catalogo;


namespace DataAccess.ExamenMedico
{
    public class FabricaExamenMedico : IEntityFactory<Entity.ExamenMedico>
    {

        private static ServicioCatalogo servicioCatalogo = new ServicioCatalogo("SQLConnectionString");
        public Entity.ExamenMedico ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.ExamenMedico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);

            }

            index = dr.GetOrdinal("TipoExamenId");
            if (!dr.IsDBNull(index))
            {
                var parametros = new object[2];
                parametros[0] = dr.GetInt32(index);
                parametros[1] = Entity.TipoDeCatalogo.tipo_examen;
                item.TipoExamen = servicioCatalogo.ObtenerPorId(parametros);

            }

            index = dr.GetOrdinal("SexoId");
            if (!dr.IsDBNull(index))
            {
                item.SexoId = dr.GetInt32(index);

            }

            index = dr.GetOrdinal("InspeccionOcular");
            if (!dr.IsDBNull(index))
            {
                item.InspeccionOcular = dr.GetString(index);
            }


            index = dr.GetOrdinal("IndicacionesMedicas");
            if (!dr.IsDBNull(index))
            {
                item.IndicacionesMedicas = dr.GetString(index);
            }



            index = dr.GetOrdinal("Observacion");
            if (!dr.IsDBNull(index))
            {
                item.Observacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }


            index = dr.GetOrdinal("RiesgoVida");
            if (!dr.IsDBNull(index))
            {
                item.RiesgoVida = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("ConsecuenciasLesiones");
            if (!dr.IsDBNull(index))
            {
                item.ConsecuenciasLesiones = dr.GetBoolean(index);
            }


            index = dr.GetOrdinal("DiasSanarLesiones");
            if (!dr.IsDBNull(index))
            {
                item.DiasSanarLesiones = dr.GetInt32(index);

            }


            index = dr.GetOrdinal("DetalleDetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalleDetencionId = dr.GetInt32(index);

            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.RegistradoPor =dr.GetInt32(index);
              
            }


            index = dr.GetOrdinal("HoraYFecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);

            }
           
            index = dr.GetOrdinal("Lesion_visible");
            if (!dr.IsDBNull(index))
            {
                item.Lesion_visible = dr.GetBoolean(index);
            }
            index = dr.GetOrdinal("FechaRegistro");
            if (!dr.IsDBNull(index))
            {
                item.FechaRegistro = dr.GetDateTime(index);

            }

            index = dr.GetOrdinal("Fallecimiento");
            if (!dr.IsDBNull(index))
            {
                item.Fallecimiento = dr.GetBoolean(index);
            }
            return item;
            

        }
    }
}