﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.ExamenMedico
{
    public class Guardar : IInsertFactory<Entity.ExamenMedico>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.ExamenMedico item)
        {
            DbCommand cmd = db.GetStoredProcCommand("ExamenMedico_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_Edad", DbType.Int32, item.Edad);
            db.AddInParameter(cmd, "_SexoId", DbType.Int32, item.SexoId);
            db.AddInParameter(cmd, "_TipoExamenId", DbType.Int32, item.TipoExamen.Id);
            db.AddInParameter(cmd, "_InspeccionOcular", DbType.String, item.InspeccionOcular);
            db.AddInParameter(cmd, "_IndicacionesMedicas", DbType.String, item.IndicacionesMedicas);
            db.AddInParameter(cmd, "_Observacion", DbType.String, item.Observacion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_RiesgoVida", DbType.Boolean, item.RiesgoVida);
            db.AddInParameter(cmd, "_ConsecuenciasLesiones", DbType.Boolean, item.ConsecuenciasLesiones);
            db.AddInParameter(cmd, "_DiasSanarLesiones", DbType.Int32, item.DiasSanarLesiones);
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, item.DetalleDetencionId);
            db.AddInParameter(cmd, "_HoraYFecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.RegistradoPor);
            db.AddInParameter(cmd, "_Lesion_visible", DbType.Boolean, item.Lesion_visible);
            db.AddInParameter(cmd, "_Fallecimiento", DbType.Boolean, item.Fallecimiento);
            return cmd;
           
        }
    }
}
