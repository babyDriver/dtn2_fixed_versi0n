﻿using System;
using System.Collections.Generic;

namespace DataAccess.ExamenMedico
{
    public interface IExamenMedico
    {

        Entity.ExamenMedico ObtenerById(int Id);
        //Entity.ExamenMedico ObtenerByDetalleDetencionId(int Id);
        Entity.ExamenMedico ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.ExamenMedico item);
        void Actualizar(Entity.ExamenMedico item);
        List<Entity.ExamenMedico> ObtenerTodosByDetalleDetencionId(int id);
    }
}