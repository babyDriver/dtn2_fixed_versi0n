﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ExamenMedico
{
    public class ObtenerTodosByDetalleDetencionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("ExamenMedico_GetByDetalleDetencionId_SP");
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, id);
            return cmd;
        }
    }
}
