﻿using System;
using System.Collections.Generic;

namespace DataAccess.Usuario
{
    public interface IUsuario
    {
        Entity.Usuario ObtenerUsuario(Guid UserId);
        Entity.Usuario ObtenerUsuario(int Id);
        List<Entity.Usuario> ObtenerTodos();
        int Guardar(Entity.Usuario usuario);
        void Actualizar(Entity.Usuario usuario);
    }
}