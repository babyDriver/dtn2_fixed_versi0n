﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Usuario
{
    public class ObtenerByEmail : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string email)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_GetByEmail_SP");
            db.AddInParameter(cmd, "Email", DbType.String, email);
            return cmd;
        }
    }
}
