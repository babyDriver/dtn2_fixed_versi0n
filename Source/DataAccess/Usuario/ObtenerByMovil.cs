﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Usuario
{
    public class ObtenerByMovil : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string movil)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_GetByMovil_SP");
            db.AddInParameter(cmd, "Movil", DbType.String, movil);
            return cmd;
        }
    }
}
