﻿using DataAccess.Interfaces;


namespace DataAccess.Usuario
{
    public class FabricaUsuario : IEntityFactory<Entity.Usuario>
    {
       

        public Entity.Usuario ConstructEntity(System.Data.IDataReader dr)
        {
            var usuario = new Entity.Usuario();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                usuario.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("UserId");
            if (!dr.IsDBNull(index))
            {
                usuario.UserId = dr.GetGuid(index);
            }

       
            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                usuario.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("ApellidoPaterno");
            if (!dr.IsDBNull(index))
            {
                usuario.ApellidoPaterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("ApellidoMaterno");
            if (!dr.IsDBNull(index))
            {
                usuario.ApellidoMaterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Usuario");
            if (!dr.IsDBNull(index))
            {
                usuario.User = dr.GetString(index);
            }

            index = dr.GetOrdinal("UltimaActualizacion");
            if (!dr.IsDBNull(index))
            {
                usuario.UltimaActualizacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Email");
            if (!dr.IsDBNull(index))
            {
                usuario.Email = dr.GetString(index);
            }

            index = dr.GetOrdinal("RolId");
            if (!dr.IsDBNull(index))
            {
                usuario.RolId = dr.GetInt32(index);
            }

            //index = dr.GetOrdinal("CentroId");
            //if (!dr.IsDBNull(index))
            //{
            //    usuario.CentroId = dr.GetInt32(index);
            //}

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                usuario.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                usuario.Habilitado = dr.GetBoolean(index);
            }


            index = dr.GetOrdinal("Movil");
            if (!dr.IsDBNull(index))
            {
                usuario.Movil = dr.GetString(index);
            }

            index = dr.GetOrdinal("Avatar");
            if (!dr.IsDBNull(index))
            {
                usuario.Avatar = dr.GetString(index);
            }

            index = dr.GetOrdinal("CUIP");
            if (!dr.IsDBNull(index))
            {
                usuario.CUIP = dr.GetString(index);
            }
            index = dr.GetOrdinal("BanderaPassword");
            if (!dr.IsDBNull(index))
            {
                usuario.BanderaPassword = dr.GetBoolean(index);
            }
            /*
            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                usuario.CreadoPor = dr.GetInt32(index);
            }*/

            return usuario;
        }
    }
}