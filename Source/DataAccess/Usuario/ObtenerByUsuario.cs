﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Usuario
{
    public class ObtenerByUsuario : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string usuario)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_GetByUsuario_SP");
            db.AddInParameter(cmd, "Usuario", DbType.String, usuario);
            return cmd;
        }
    }
}
