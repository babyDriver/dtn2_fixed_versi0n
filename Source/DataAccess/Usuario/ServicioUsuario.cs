﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Usuario
{
    public class ServicioUsuario : ClsAbstractService<Entity.Usuario>, IUsuario
    {
        public ServicioUsuario(string dataBase)
            : base(dataBase)
        {
        }
        
        public Entity.Usuario ObtenerUsuario(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaUsuario(), userId);
        }

        public Entity.Usuario ObtenerByUsuario(string usuario)
        {
            ISelectFactory<string> select = new ObtenerByUsuario();
            return GetByKey(select, new FabricaUsuario(), usuario);
        }

        public Entity.Usuario ObtenerByEmail(string email)
        {
            ISelectFactory<string> select = new ObtenerByEmail();
            return GetByKey(select, new FabricaUsuario(), email);
        }

        public Entity.Usuario ObtenerByMovil(string movil)
        {
            ISelectFactory<string> select = new ObtenerByMovil();
            return GetByKey(select, new FabricaUsuario(), movil);
        }

        public List<Entity.Usuario> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaUsuario(), new Entity.NullClass());
        }

        public Entity.Usuario ObtenerUsuario(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaUsuario(), Id);
        }

        public int Guardar(Entity.Usuario usuario)
        {
            IInsertFactory<Entity.Usuario> insert = new Guardar();
            return Insert(insert, usuario);
        }

        public void Actualizar(Entity.Usuario usuario)
        {
            IUpdateFactory<Entity.Usuario> update = new Actualizar();
            Update(update, usuario);
        }


    }
}