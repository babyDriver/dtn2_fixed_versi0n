using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Usuario
{
    public class Guardar : IInsertFactory<Entity.Usuario>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Usuario usuario)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_Id", DbType.Int32, usuario.Id);
            db.AddInParameter(cmd, "_UserId", DbType.String, usuario.UserId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, usuario.Nombre);
            db.AddInParameter(cmd, "_UltimaActualizacion", DbType.DateTime, usuario.UltimaActualizacion);
            db.AddInParameter(cmd, "_Usuario", DbType.String, usuario.User);
            db.AddInParameter(cmd, "_ApellidoPaterno", DbType.String, usuario.ApellidoPaterno);
            db.AddInParameter(cmd, "_ApellidoMaterno", DbType.String, usuario.ApellidoMaterno);
            db.AddInParameter(cmd, "_Email", DbType.String, usuario.Email);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, usuario.Activo);
            db.AddInParameter(cmd, "_RolId", DbType.Int32, usuario.RolId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, usuario.Habilitado);
            db.AddInParameter(cmd, "_Avatar", DbType.String, usuario.Avatar);
            db.AddInParameter(cmd, "_Movil", DbType.String, usuario.Movil);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, usuario.CreadoPor);
            db.AddInParameter(cmd, "_CUIP", DbType.String, usuario.CUIP);
            //db.AddInParameter(cmd, "_CentroId", DbType.String, usuario.CentroId);

            return cmd;
        }
    }
}
