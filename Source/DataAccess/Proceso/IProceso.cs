﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Proceso
{
    public interface IProceso
    {
        List<Entity.Proceso> ObtenerTodo();
    }
}
