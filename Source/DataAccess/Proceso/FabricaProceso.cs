﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.Proceso
{
    public class FabricaProceso : IEntityFactory<Entity.Proceso>
    {
        public Entity.Proceso ConstructEntity(IDataReader dr)
        {
            var item = new Entity.Proceso();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Proceso");
            if (!dr.IsDBNull(index))
            {
                item.proceso = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt16(index);
            }



            return item;
        }
    }
}
