﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;


namespace DataAccess.Proceso
{
    public class ServicioProceso : ClsAbstractService<Entity.Proceso>, IProceso
    {
        public ServicioProceso(string dataBase)
            : base(dataBase)
        {
        }
        public List<Entity.Proceso> ObtenerTodo()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodo();
            return GetAll(select, new FabricaProceso(), new Entity.NullClass());
        }
    }
}
