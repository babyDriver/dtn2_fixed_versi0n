﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Entity;

namespace DataAccess.Proceso
{
    public class ObtenerTodo : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Proceso_GetAll_SP");
            return cmd;

        }
    }
}
