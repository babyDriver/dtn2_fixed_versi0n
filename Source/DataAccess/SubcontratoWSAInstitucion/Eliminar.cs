﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public class Eliminar : IDeleteFactory<int>
    {
        public DbCommand ConstructDeleteCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_wsainstitucion_Delete_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, identity);

            return cmd;
        }
    }
}
