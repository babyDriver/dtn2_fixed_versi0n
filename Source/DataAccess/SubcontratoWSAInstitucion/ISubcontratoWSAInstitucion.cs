﻿using System;
using System.Collections.Generic;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public interface ISubcontratoWSAInstitucion
    {
        List<Entity.SubcontratoWSAInstitucion> ObtenerTodos();
        List<Entity.SubcontratoWSAInstitucion> ObtenerPorWSAInstitucionId(int wsainstitucionId);
        List<Entity.SubcontratoWSAInstitucion> ObtenerPorSubcontratoId(int subcontratoId);
        Entity.SubcontratoWSAInstitucion ObtenerPorTrackingId(Guid trackingId);
        Entity.SubcontratoWSAInstitucion ObtenerPorId(int id);
        int Guardar(Entity.SubcontratoWSAInstitucion subcontratoWSAInstitucion);
        void Actualizar(Entity.SubcontratoWSAInstitucion subcontratoWSAInstitucion);
        void Eliminar(int id);
    }
}
