﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public class ObtenerByWSAInstitucionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_wsainstitucion_GetByWSAInstitucionId_SP");
            db.AddInParameter(cmd, "_WSAInstitucionId", DbType.Int32, identity);

            return cmd;
        }
    }
}
