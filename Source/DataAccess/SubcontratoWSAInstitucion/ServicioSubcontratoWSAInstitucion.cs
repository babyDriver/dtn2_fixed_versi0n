﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public class ServicioSubcontratoWSAInstitucion : ClsAbstractService<Entity.SubcontratoWSAInstitucion>, ISubcontratoWSAInstitucion
    {
        public ServicioSubcontratoWSAInstitucion(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.SubcontratoWSAInstitucion subcontratoWSAInstitucion)
        {
            IUpdateFactory<Entity.SubcontratoWSAInstitucion> update = new Actualizar();
            Update(update, subcontratoWSAInstitucion);
        }

        public void Eliminar(int id)
        {
            IDeleteFactory<int> delete = new Eliminar();
            Delete(delete, id);
        }

        public int Guardar(Entity.SubcontratoWSAInstitucion subcontratoWSAInstitucion)
        {
            IInsertFactory<Entity.SubcontratoWSAInstitucion> insert = new Guardar();
            return Insert(insert, subcontratoWSAInstitucion);
        }

        public Entity.SubcontratoWSAInstitucion ObtenerPorId(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaSubcontratoWSAInstitucion(), id);
        }

        public List<Entity.SubcontratoWSAInstitucion> ObtenerPorSubcontratoId(int subcontratoId)
        {
            ISelectFactory<int> select = new ObtenerBySubcontratoId();
            return GetAll(select, new FabricaSubcontratoWSAInstitucion(), subcontratoId);
        }

        public Entity.SubcontratoWSAInstitucion ObtenerPorTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaSubcontratoWSAInstitucion(), trackingId);
        }

        public List<Entity.SubcontratoWSAInstitucion> ObtenerPorWSAInstitucionId(int wsainstitucionId)
        {
            ISelectFactory<int> select = new ObtenerByWSAInstitucionId();
            return GetAll(select, new FabricaSubcontratoWSAInstitucion(), wsainstitucionId);
        }

        public List<Entity.SubcontratoWSAInstitucion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaSubcontratoWSAInstitucion(), new Entity.NullClass());
        }
    }
}
