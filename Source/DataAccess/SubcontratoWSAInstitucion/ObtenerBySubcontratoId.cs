﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public class ObtenerBySubcontratoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_wsainstitucion_GetBySubcontratoId_SP");
            db.AddInParameter(cmd, "_SubcontratoId", DbType.Int32, identity);

            return cmd;
        }
    }
}
