﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public class Guardar : IInsertFactory<Entity.SubcontratoWSAInstitucion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.SubcontratoWSAInstitucion entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_wsainstitucion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, entity.TrackingId);
            db.AddInParameter(cmd, "_IdSubcontrato", DbType.Int32, entity.IdSubcontrato);
            db.AddInParameter(cmd, "_IdWSAInstitucion", DbType.Int32, entity.IdWSAInstitucion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, entity.Habilitado);

            return cmd;
        }
    }
}
