﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_wsainstitucion_GetAll_SP");
            return cmd;
        }
    }
}
