﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public class Actualizar : IUpdateFactory<Entity.SubcontratoWSAInstitucion>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.SubcontratoWSAInstitucion entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_wsainstitucion_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, entity.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, entity.TrackingId);
            db.AddInParameter(cmd, "_IdSubcontrato", DbType.Int32, entity.IdSubcontrato);
            db.AddInParameter(cmd, "_IdWSAInstitucion", DbType.Int32, entity.IdWSAInstitucion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, entity.Habilitado);

            return cmd;
        }
    }
}
