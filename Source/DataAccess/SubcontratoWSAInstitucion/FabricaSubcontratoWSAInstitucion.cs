﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.SubcontratoWSAInstitucion
{
    public class FabricaSubcontratoWSAInstitucion : IEntityFactory<Entity.SubcontratoWSAInstitucion>
    {
        public Entity.SubcontratoWSAInstitucion ConstructEntity(IDataReader dr)
        {
            var obj = new Entity.SubcontratoWSAInstitucion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                obj.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                obj.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("IdSubcontrato");
            if (!dr.IsDBNull(index))
            {
                obj.IdSubcontrato = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IdWSAInstitucion");
            if (!dr.IsDBNull(index))
            {
                obj.IdWSAInstitucion = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                obj.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                obj.Habilitado = dr.GetBoolean(index);
            }
            
            return obj;
        }
    }
}
