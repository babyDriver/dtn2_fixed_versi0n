﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoBusquedaAvanzada
{
    public class ServicioListadoBusquedaAvanzada : ClsAbstractService<Entity.ListadoBusquedaAvanzada>, IListadoBusquedaAvanzada
    {
        public ServicioListadoBusquedaAvanzada(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoBusquedaAvanzada> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoBusquedaAvanzada(), parametros);
        }
    }
}
