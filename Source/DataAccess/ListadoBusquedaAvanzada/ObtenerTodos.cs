﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ListadoBusquedaAvanzada
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ListadoBusquedaAvanzada_GetAll_SP");
            db.AddInParameter(cmd, "_tipo", DbType.String, parametros[0]);
            db.AddInParameter(cmd, "_contratoId", DbType.Int32, parametros[1]);
            db.AddInParameter(cmd, "_inicio", DbType.DateTime, parametros[2]);
            db.AddInParameter(cmd, "_fin", DbType.DateTime, parametros[3]);
            db.AddInParameter(cmd, "_wheres", DbType.String, parametros[4]);
            return cmd;
        }
    }
}
