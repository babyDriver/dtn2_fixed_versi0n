﻿using System.Collections.Generic;

namespace DataAccess.ListadoBusquedaAvanzada
{
    public interface IListadoBusquedaAvanzada
    {
        List<Entity.ListadoBusquedaAvanzada> ObtenerTodos(object[] data);
    }
}
