﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoBusquedaAvanzada
{
    public class FabricaListadoBusquedaAvanzada : IEntityFactory<Entity.ListadoBusquedaAvanzada>
    {
        public Entity.ListadoBusquedaAvanzada ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoBusquedaAvanzada();

            var index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("TkgDet");
            if (!dr.IsDBNull(index))
            {
                item.TkgDet = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Alias");
            if (!dr.IsDBNull(index))
            {
                item.Alias = dr.GetString(index);
            }

            index = dr.GetOrdinal("Evento");
            if (!dr.IsDBNull(index))
            {
                item.Evento = dr.GetString(index);
            }

            index = dr.GetOrdinal("DescripcionHechos");
            if (!dr.IsDBNull(index))
            {
                item.DescripcionHechos = dr.GetString(index);
            }

            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                if (string.IsNullOrEmpty(dr.GetString(index)))
                {
                    item.Folio = "0";
                }
                else
                {
                    item.Folio = dr.GetString(index);
                }
            }
            else
            {
                item.Folio = "0";
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                item.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Corporacion");
            if (!dr.IsDBNull(index))
            {
                item.Corporacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Horas");
            if (!dr.IsDBNull(index))
            {
                item.Horas = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            item.NombreCompleto = item.Nombre + " " + item.Paterno + " " + item.Materno;

            return item;
        }
    }
}
