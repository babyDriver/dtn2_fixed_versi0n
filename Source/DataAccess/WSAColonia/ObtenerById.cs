﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.WSAColonia
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int estadoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAColonia_GetById_SP");
            db.AddInParameter(cmd, "Id", DbType.Int32, estadoId);
            return cmd;
        }
    }
}
