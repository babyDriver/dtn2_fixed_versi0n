﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.WSAColonia
{
    public class FabricaWSAColonia : IEntityFactory<Entity.WSAColonia>
    {
        public Entity.WSAColonia ConstructEntity(IDataReader dr)
        {
            var colonia = new Entity.WSAColonia();

            var index = dr.GetOrdinal("idColonia");
            if (!dr.IsDBNull(index))
            {
                colonia.IdColonia = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("nombreColonia");
            if (!dr.IsDBNull(index))
            {
                colonia.NombreColonia = dr.GetString(index);
            }

            index = dr.GetOrdinal("idMunicipio");
            if (!dr.IsDBNull(index))
            {
                colonia.IdMunicipio = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaModificacion");
            if (!dr.IsDBNull(index))
            {
                colonia.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                colonia.Activo = dr.GetBoolean(index);
            }

            return colonia;
        }
    }
}
