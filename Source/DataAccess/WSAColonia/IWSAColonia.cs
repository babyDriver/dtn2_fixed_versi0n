﻿using System.Collections.Generic;

namespace DataAccess.WSAColonia
{
    public interface IWSAColonia
    {
        List<Entity.WSAColonia> ObtenerTodos();
        Entity.WSAColonia ObtenerPorId(int estadoId);
        int Guardar(Entity.WSAColonia colonia);
        void Actualizar(Entity.WSAColonia colonia);
    }
}
