﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.WSAColonia
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAColonia_GetAll_SP");
            return cmd;
        }
    }
}
