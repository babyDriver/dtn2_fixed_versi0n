﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAColonia
{
    public class Guardar : IInsertFactory<Entity.WSAColonia>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSAColonia entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAColonia_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_idColonia", DbType.Int32, entity.IdColonia);
            db.AddInParameter(cmd, "_nombreColonia", DbType.String, entity.NombreColonia);
            db.AddInParameter(cmd, "_idMunicipio", DbType.Int32, entity.IdMunicipio);

            return cmd;
        }
    }
}
