﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAColonia
{
    public class Actualizar : IUpdateFactory<Entity.WSAColonia>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.WSAColonia entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAColonia_Update_SP");
            db.AddInParameter(cmd, "_idColonia", DbType.Int32, entity.IdColonia);
            db.AddInParameter(cmd, "_nombreColonia", DbType.String, entity.NombreColonia);
            db.AddInParameter(cmd, "_idMunicipio", DbType.Int32, entity.IdMunicipio);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);

            return cmd;
        }
    }
}
