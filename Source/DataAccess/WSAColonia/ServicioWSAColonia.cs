﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.WSAColonia
{
    public class ServicioWSAColonia : ClsAbstractService<Entity.WSAColonia>, IWSAColonia
    {
        public ServicioWSAColonia(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.WSAColonia> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaWSAColonia(), new Entity.NullClass());
        }

        public Entity.WSAColonia ObtenerPorId(int estadoId)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSAColonia(), estadoId);
        }

        public int Guardar(Entity.WSAColonia colonia)
        {
            IInsertFactory<Entity.WSAColonia> insert = new Guardar();
            return Insert(insert, colonia);
        }

        public void Actualizar(Entity.WSAColonia colonia)
        {
            IUpdateFactory<Entity.WSAColonia> update = new Actualizar();
            Update(update, colonia);
        }
    }
}
