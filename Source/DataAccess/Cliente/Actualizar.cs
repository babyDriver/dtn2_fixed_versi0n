﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Cliente
{
    public class Actualizar : IUpdateFactory<Entity.Cliente>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Cliente cliente)
        {
            DbCommand cmd = db.GetStoredProcCommand("Cliente_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, cliente.Id);
            db.AddInParameter(cmd, "_Nombre", DbType.String, cliente.Nombre);
            db.AddInParameter(cmd, "_Rfc", DbType.String, cliente.Rfc);            
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, cliente.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, cliente.Habilitado);

            return cmd;
        }
    }
}
