﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Cliente
{
    public class Guardar : IInsertFactory<Entity.Cliente>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Cliente cliente)
        {
            DbCommand cmd = db.GetStoredProcCommand("Cliente_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_Cliente", DbType.String, cliente.Nombre);
            db.AddInParameter(cmd, "_RFC", DbType.String, cliente.Rfc);
            db.AddInParameter(cmd, "_TenantId", DbType.String, cliente.TenantId.ToString());            
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, cliente.CreadoPor);

            return cmd;
        }
    }
}
