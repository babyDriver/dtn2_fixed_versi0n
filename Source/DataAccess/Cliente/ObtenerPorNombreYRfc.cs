﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Cliente
{
    public class ObtenerPorNombreYRfc : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Cliente_GetByNombreAndRFC_SP");
            db.AddInParameter(cmd, "Nombre", DbType.String, parametros[0].ToString());
            db.AddInParameter(cmd, "RFC", DbType.String, parametros[1].ToString());
            return cmd;
        }
    }
}
