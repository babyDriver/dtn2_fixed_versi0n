﻿using System;
using System.Collections.Generic;

namespace DataAccess.Cliente
{
    public interface ICliente
    {
        List<Entity.Cliente> ObtenerTodos();
        Entity.Cliente ObtenerPorNombreYRfc(object[] datos);
        int Guardar(Entity.Cliente cliente);
        Entity.Cliente ObtenerByTrackingId(Guid tracking);
        void Actualizar(Entity.Cliente cliente);
        Entity.Cliente ObtenerById(int id);
    }
}
