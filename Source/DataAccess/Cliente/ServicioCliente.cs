﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Cliente
{
    public class ServicioCliente : ClsAbstractService<Entity.Cliente>, ICliente
    {
        public ServicioCliente(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Cliente> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaCliente(), new Entity.NullClass());
        }

        public Entity.Cliente ObtenerPorNombreYRfc(object[] datos)
        {
            ISelectFactory<object[]> select = new ObtenerPorNombreYRfc();
            return GetByKey(select, new FabricaCliente(), datos);
        }

        public int Guardar(Entity.Cliente cliente)
        {
            IInsertFactory<Entity.Cliente> insert = new Guardar();
            return Insert(insert, cliente);
        }

        public Entity.Cliente ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaCliente(), trackingId);
        }

        public Entity.Cliente ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaCliente(), id);
        }

        public void Actualizar(Entity.Cliente cliente)
        {
            IUpdateFactory<Entity.Cliente> update = new Actualizar();
            Update(update, cliente);
        }
    }
}
