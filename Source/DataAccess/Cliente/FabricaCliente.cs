﻿using DataAccess.Interfaces;

namespace DataAccess.Cliente
{
    public class FabricaCliente : IEntityFactory<Entity.Cliente>
    {
        public Entity.Cliente ConstructEntity(System.Data.IDataReader dr)
        {
            var cliente = new Entity.Cliente();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                cliente.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TenantId");
            if (!dr.IsDBNull(index))
            {
                cliente.TenantId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Cliente");
            if (!dr.IsDBNull(index))
            {
                cliente.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Rfc");
            if (!dr.IsDBNull(index))
            {
                cliente.Rfc = dr.GetString(index);
            }            

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                cliente.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                cliente.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                cliente.CreadoPor = dr.GetInt32(index);
            }

            return cliente;
        }
    }
}
