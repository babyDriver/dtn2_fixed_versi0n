﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Informacion
{
    public class ServicioInformacion : ClsAbstractService<Entity.Informacion>, IInformacion
    {
        public ServicioInformacion(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Informacion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaInformacion(), new Entity.NullClass());
        }

        public Entity.Informacion ObtenerByTrackingId(Guid trackinid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaInformacion(), trackinid);
        }

        public Entity.Informacion ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaInformacion(), Id);
        }

        public Entity.Informacion ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaInformacion(), Id);
        }


        public Entity.Informacion ObtenerByProcesoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByProcesoId();
            return GetByKey(select, new FabricaInformacion(), Id);
        }

        public int Guardar(Entity.Informacion informacion)
        {
            IInsertFactory<Entity.Informacion> insert = new Guardar();
            return Insert(insert, informacion);
        }

        public void Actualizar(Entity.Informacion informacion)
        {
            IUpdateFactory<Entity.Informacion> update = new Actualizar();
            Update(update, informacion);
        }


    }
}