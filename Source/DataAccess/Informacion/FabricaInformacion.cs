﻿using DataAccess.Interfaces;


namespace DataAccess.Informacion
{
    public class FabricaInformacion : IEntityFactory<Entity.Informacion>
    {
       

        public Entity.Informacion ConstructEntity(System.Data.IDataReader dr)
        {
            var informacion = new Entity.Informacion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                informacion.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                informacion.TrackingId = dr.GetGuid(index);
            }
            
            index = dr.GetOrdinal("Jornadas");
            if (!dr.IsDBNull(index))
            {
                informacion.Jornadas = dr.GetString(index);
            }

            index = dr.GetOrdinal("Multa");
            if (!dr.IsDBNull(index))
            {
                informacion.Multa = dr.GetString(index);
            }

            index = dr.GetOrdinal("Reparacion");
            if (!dr.IsDBNull(index))
            {
                informacion.Reparacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                informacion.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Anios");
            if (!dr.IsDBNull(index))
            {
                informacion.Anios = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Meses");
            if (!dr.IsDBNull(index))
            {
                informacion.Meses = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Dias");
            if (!dr.IsDBNull(index))
            {
                informacion.Dias = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Amparo");
            if (!dr.IsDBNull(index))
            {
                informacion.Amparo = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaAmparo");
            if (!dr.IsDBNull(index))
            {
                informacion.FechaAmparo = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("TribunalId");
            if (!dr.IsDBNull(index))
            {
                informacion.TribunalId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Terminos");
            if (!dr.IsDBNull(index))
            {
                informacion.Terminos = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaInstancia");
            if (!dr.IsDBNull(index))
            {
                informacion.FechaInstancia = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("JuzgadoId");
            if (!dr.IsDBNull(index))
            {
                informacion.JuzgadoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Resolucion");
            if (!dr.IsDBNull(index))
            {
                informacion.Resolucion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Toca");
            if (!dr.IsDBNull(index))
            {
                informacion.Toca = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaApelacion");
            if (!dr.IsDBNull(index))
            {
                informacion.FechaApelacion = dr.GetDateTime(index);
            }


            index = dr.GetOrdinal("FechaResolucion");
            if (!dr.IsDBNull(index))
            {
                informacion.FechaResolucion = dr.GetDateTime(index);
            }


            index = dr.GetOrdinal("TribunalApelacionId");
            if (!dr.IsDBNull(index))
            {
                informacion.TribunalApelacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TerminoApelacion");
            if (!dr.IsDBNull(index))
            {
                informacion.TerminoApelacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("AmparoDirecto");
            if (!dr.IsDBNull(index))
            {
                informacion.AmparoDirecto = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaAmparoD");
            if (!dr.IsDBNull(index))
            {
                informacion.FechaAmparoD = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("FechaResolucionD");
            if (!dr.IsDBNull(index))
            {
                informacion.FechaResolucionD = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("TribunalDirectoId");
            if (!dr.IsDBNull(index))
            {
                informacion.TribunalDirectoId = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("TerminoDirecto");
            if (!dr.IsDBNull(index))
            {
                informacion.TerminoDirecto = dr.GetString(index);
            }

            index = dr.GetOrdinal("AutorId");
            if (!dr.IsDBNull(index))
            {
                informacion.AutorId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AutoriaId");
            if (!dr.IsDBNull(index))
            {
                informacion.AutoriaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ClasificacionId");
            if (!dr.IsDBNull(index))
            {
                informacion.ClasificacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("PeligrosidadId");
            if (!dr.IsDBNull(index))
            {
                informacion.PeligrosidadId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DeterminadaId");
            if (!dr.IsDBNull(index))
            {
                informacion.DeterminadaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                informacion.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ProcesoId");
            if (!dr.IsDBNull(index))
            {
                informacion.ProcesoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                informacion.Habilitado = dr.GetBoolean(index);
            }


            return informacion;
        }
    }
}