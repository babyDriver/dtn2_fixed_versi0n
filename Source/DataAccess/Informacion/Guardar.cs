﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Informacion
{
    public class Guardar : IInsertFactory<Entity.Informacion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Informacion informacion)
        {
            DbCommand cmd = db.GetStoredProcCommand("Informacion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, informacion.TrackingId);
            db.AddInParameter(cmd, "_Jornadas", DbType.String, informacion.Jornadas);
            db.AddInParameter(cmd, "_Multa", DbType.String, informacion.Multa);
            db.AddInParameter(cmd, "_Reparacion", DbType.String, informacion.Reparacion);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, informacion.Fecha);
            db.AddInParameter(cmd, "_Anios", DbType.Int32, informacion.Anios);
            db.AddInParameter(cmd, "_Meses", DbType.Int32, informacion.Meses);
            db.AddInParameter(cmd, "_Dias", DbType.Int32, informacion.Dias);
            db.AddInParameter(cmd, "_Amparo", DbType.String, informacion.Amparo);
            db.AddInParameter(cmd, "_FechaAmparo", DbType.DateTime, informacion.FechaAmparo);
            db.AddInParameter(cmd, "_TribunalId", DbType.Int32, informacion.TribunalId);
            db.AddInParameter(cmd, "_Terminos", DbType.String, informacion.Terminos);
            db.AddInParameter(cmd, "_FechaInstancia", DbType.DateTime, informacion.FechaInstancia);
            db.AddInParameter(cmd, "_JuzgadoId", DbType.Int32, informacion.JuzgadoId);
            db.AddInParameter(cmd, "_Resolucion", DbType.String, informacion.Resolucion);
            db.AddInParameter(cmd, "_Toca", DbType.String, informacion.Toca);
            db.AddInParameter(cmd, "_FechaApelacion", DbType.DateTime, informacion.FechaApelacion);
            db.AddInParameter(cmd, "_FechaResolucion", DbType.DateTime, informacion.FechaResolucion);
            db.AddInParameter(cmd, "_TribunalApelacionId", DbType.Int32, informacion.TribunalApelacionId);
            db.AddInParameter(cmd, "_TerminoApelacion", DbType.String, informacion.TerminoApelacion);
            db.AddInParameter(cmd, "_AmparoDirecto", DbType.String, informacion.AmparoDirecto);
            db.AddInParameter(cmd, "_FechaAmparoD", DbType.DateTime, informacion.FechaAmparoD);
            db.AddInParameter(cmd, "_FechaResolucionD", DbType.DateTime, informacion.FechaResolucionD);
            db.AddInParameter(cmd, "_TribunalDirectoId", DbType.Int32, informacion.TribunalDirectoId);
            db.AddInParameter(cmd, "_TerminoDirecto", DbType.String, informacion.TerminoDirecto);
            db.AddInParameter(cmd, "_AutorId", DbType.Int32, informacion.AutorId);
            db.AddInParameter(cmd, "_AutoriaId", DbType.Int32, informacion.AutoriaId);
            db.AddInParameter(cmd, "_ClasificacionId", DbType.Int32, informacion.ClasificacionId);
            db.AddInParameter(cmd, "_PeligrosidadId", DbType.Int32, informacion.PeligrosidadId);
            db.AddInParameter(cmd, "_DeterminadaId", DbType.Int32, informacion.DeterminadaId);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, informacion.DetenidoId);
            db.AddInParameter(cmd, "_ProcesoId", DbType.Int32, informacion.ProcesoId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, informacion.Habilitado);

            return cmd;
        }
    }
}
