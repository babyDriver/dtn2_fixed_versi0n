﻿using System;
using System.Collections.Generic;

namespace DataAccess.Informacion
{
    public interface IInformacion
    {
        List<Entity.Informacion> ObtenerTodos();
        Entity.Informacion ObtenerById(int Id);
        Entity.Informacion ObtenerByDetenidoId(int Id);
        Entity.Informacion ObtenerByProcesoId(int Id);
        Entity.Informacion ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Informacion informacion);
        void Actualizar(Entity.Informacion informacion);
    }
}