﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;


namespace DataAccess.CertificadoLesionAlergias
{
   public class ServicioCertificadoLesionAlergias:ClsAbstractService<Entity.CertificadoLesionAlergia>,ICertificadoLesionAlergias
    {
        public ServicioCertificadoLesionAlergias(string dataBase)
    : base(dataBase)
        {
        }

        public int Guardar(CertificadoLesionAlergia certificadoLesionAlergia)
        {
            IInsertFactory<Entity.CertificadoLesionAlergia> insert = new Guardar();
            return Insert(insert, certificadoLesionAlergia);
        }

        public CertificadoLesionAlergia ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoLesionAlergias(), Id);
        }
    }
}
