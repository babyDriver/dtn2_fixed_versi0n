﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionAlergias
{
    public interface ICertificadoLesionAlergias
    {
        int Guardar(Entity.CertificadoLesionAlergia certificadoLesionAlergia);
        Entity.CertificadoLesionAlergia ObtenerPorId(int Id);
    }
}
