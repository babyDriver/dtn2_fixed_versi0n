﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.FichaTecnica
{
    public class ServicioFichaTecnica : ClsAbstractService<Entity.FichaTecnica>, IFichaTecnica
    {
        public ServicioFichaTecnica(string dataBase)
            : base(dataBase) { }
        public int Guardar(Entity.FichaTecnica Ficha)
        {
            IInsertFactory<Entity.FichaTecnica> insert = new Guardar();
            return Insert(insert, Ficha);
        }
    }
}
