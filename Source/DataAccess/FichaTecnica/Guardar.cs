﻿using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;
using DataAccess.Interfaces;

namespace DataAccess.FichaTecnica
{
    public class Guardar : IInsertFactory<Entity.FichaTecnica>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.FichaTecnica Ficha)
        {
            DbCommand cmd = db.GetStoredProcCommand("Ficha_Tecnica_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, Ficha.TrackingId);

            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, Ficha.DetenidoId);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, Ficha.Fecha);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, Ficha.Creadopor);

            return cmd;
        }
    }
}
