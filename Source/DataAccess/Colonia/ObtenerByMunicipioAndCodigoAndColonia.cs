﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Colonia
{
    public class ObtenerByMunicipioAndCodigoAndColonia : ISelectFactory<string> //object[]
    {
        public DbCommand ConstructSelectCommand(Database db, string parametro)
        {
            DbCommand cmd = db.GetStoredProcCommand("Colonia_GetByMunicipioAndCodigoAndColonia_SP");
            db.AddInParameter(cmd, "_value", DbType.String, parametro);
            //db.AddInParameter(cmd, "_CodigoPostal", DbType.String, parametros[1].ToString());
            //db.AddInParameter(cmd, "_Asentamiento", DbType.String, parametros[2].ToString());

            return cmd;
        }
    }
}
