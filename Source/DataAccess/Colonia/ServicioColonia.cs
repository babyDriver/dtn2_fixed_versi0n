﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Colonia
{
    public class ServicioColonia : ClsAbstractService<Entity.Colonia>, IColonia
    {
        public ServicioColonia(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.Colonia colonia)
        {
            IInsertFactory<Entity.Colonia> insert = new Guardar();
            return Insert(insert, colonia);
        }

        public Entity.Colonia ObtenerByMunicipioAndCodigoAndColonia(string parametro)
        {
            //object[]
            ISelectFactory<string> select = new ObtenerByMunicipioAndCodigoAndColonia(); 
            return GetByKey(select, new FabricaColoniaAux(), parametro);
        }

        public Entity.Colonia ObtenerByTrackingId(Guid guid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaColonia(), guid);
        }

        public Entity.Colonia ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaColonia(), id);
        }

        public void Actualizar(Entity.Colonia colonia)
        {
            IUpdateFactory<Entity.Colonia> update = new Actualizar();
            Update(update, colonia);
        }

        public List<Entity.Colonia> ObtenerByMunicipioId(int id)
        {
            ISelectFactory<int> select = new ObtenerByMunicipioId();
            return GetAll(select, new FabricaColonia(), id);
        }
        
        public List<Entity.Colonia> ObtenerTodas()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodas();
            return GetAll(select, new FabricaColonia(), new Entity.NullClass());
        }
    }
}
