﻿using System;
using System.Collections.Generic;

namespace DataAccess.Colonia
{
    public interface IColonia
    {
        int Guardar(Entity.Colonia colonia);
        Entity.Colonia ObtenerByMunicipioAndCodigoAndColonia(string parametro); //object[] parametros
        void Actualizar(Entity.Colonia colonia);
        Entity.Colonia ObtenerByTrackingId(Guid guid);
        List<Entity.Colonia> ObtenerByMunicipioId(int id);
        Entity.Colonia ObtenerById(int id);
        List<Entity.Colonia> ObtenerTodas();
    }
}
