﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Colonia
{
    public class Guardar : IInsertFactory<Entity.Colonia>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Colonia colonia)
        {
            DbCommand cmd = db.GetStoredProcCommand("Colonia_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, colonia.TrackingId);
            db.AddInParameter(cmd, "_IdMunicipio", DbType.Int32, colonia.IdMunicipio);
            db.AddInParameter(cmd, "_CodigoPostal", DbType.String, colonia.CodigoPostal);
            db.AddInParameter(cmd, "_Asentamiento", DbType.String, colonia.Asentamiento);
            db.AddInParameter(cmd, "_TipoDeAsentamiento", DbType.String, colonia.TipoDeAsentamiento);
            db.AddInParameter(cmd, "_Municipio", DbType.String, colonia.Municipio);
            db.AddInParameter(cmd, "_Estado", DbType.String, colonia.Estado);
            db.AddInParameter(cmd, "_ClaveDeOficina", DbType.String, colonia.ClaveDeOficina);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, colonia.CreadoPor);

            return cmd;
        }
    }
}
