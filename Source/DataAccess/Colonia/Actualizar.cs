﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Colonia
{
    public class Actualizar : IUpdateFactory<Entity.Colonia>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Colonia colonia)
        {
            DbCommand cmd = db.GetStoredProcCommand("Colonia_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, colonia.Id);
            db.AddInParameter(cmd, "_IdMunicipio", DbType.Int32, colonia.IdMunicipio);
            db.AddInParameter(cmd, "_CodigoPostal", DbType.String, colonia.CodigoPostal);
            db.AddInParameter(cmd, "_Asentamiento", DbType.String, colonia.Asentamiento);
            db.AddInParameter(cmd, "_TipoDeAsentamiento", DbType.String, colonia.TipoDeAsentamiento);
            db.AddInParameter(cmd, "_Municipio", DbType.String, colonia.Municipio);
            db.AddInParameter(cmd, "_Estado", DbType.String, colonia.Estado);
            db.AddInParameter(cmd, "_ClaveDeOficina", DbType.String, colonia.ClaveDeOficina);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, colonia.Habilitado);

            return cmd;
        }
    }
}
