﻿using DataAccess.Interfaces;

namespace DataAccess.Colonia
{
    public class FabricaColonia : IEntityFactory<Entity.Colonia>
    {
        public Entity.Colonia ConstructEntity(System.Data.IDataReader dr)
        {
            var colonia = new Entity.Colonia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                colonia.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                colonia.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("IdMunicipio");
            if (!dr.IsDBNull(index))
            {
                colonia.IdMunicipio = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CodigoPostal");
            if (!dr.IsDBNull(index))
            {
                colonia.CodigoPostal = dr.GetString(index);
            }

            index = dr.GetOrdinal("Asentamiento");
            if (!dr.IsDBNull(index))
            {
                colonia.Asentamiento = dr.GetString(index);
            }

            index = dr.GetOrdinal("TipoDeAsentamiento");
            if (!dr.IsDBNull(index))
            {
                colonia.TipoDeAsentamiento = dr.GetString(index);
            }

            index = dr.GetOrdinal("Municipio");
            if (!dr.IsDBNull(index))
            {
                colonia.Municipio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                colonia.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("ClaveDeOficina");
            if (!dr.IsDBNull(index))
            {
                colonia.ClaveDeOficina = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                colonia.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                colonia.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                colonia.CreadoPor = dr.GetInt32(index);
            }

            return colonia;
        }
    }
}
