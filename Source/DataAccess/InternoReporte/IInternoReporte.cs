﻿using System;
using System.Collections.Generic;

namespace DataAccess.InternoReporte
{
    public interface IInternoReporte
    {
        List<Entity.Interno_Reporte> ObtenerTodosPorParametros(object[] parametros);
        Entity.Interno_Reporte ObtenerPorParametros(object[] parametros);
    }
}
