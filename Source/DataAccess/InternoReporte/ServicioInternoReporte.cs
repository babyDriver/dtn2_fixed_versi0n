﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.InternoReporte
{
    public class ServicioInternoReporte : ClsAbstractService<Entity.Interno_Reporte>, IInternoReporte
    {
        public ServicioInternoReporte(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Interno_Reporte> ObtenerTodosPorParametros(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByParametros();
            return GetAll(select, new FabricaInternoReporte(), parametros);
        }

        public Entity.Interno_Reporte ObtenerPorParametros(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByParametros();
            return GetByKey(select, new FabricaInternoReporte(), parametros);
        }


    }
}
