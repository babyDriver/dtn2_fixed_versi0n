﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.InternoReporte
{
    public class ObtenerByParametros: ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Interno_reporte_GetByParmaetros");
            db.AddInParameter(cmd, "_Etnia", DbType.Int32, Convert.ToInt32(parametros[0]));
            db.AddInParameter(cmd, "_Religion", DbType.Int32, Convert.ToInt32(parametros[1]));
            db.AddInParameter(cmd, "_Sexo", DbType.Int32, Convert.ToInt32(parametros[2]));
            db.AddInParameter(cmd, "_Estado", DbType.Int32, Convert.ToInt32(parametros[3]));
            db.AddInParameter(cmd, "_Ocupacion", DbType.Int32, Convert.ToInt32(parametros[4]));
            db.AddInParameter(cmd, "_Estatus", DbType.Int32, Convert.ToInt32(parametros[5]));
            db.AddInParameter(cmd, "_Peligrosidad", DbType.Int32, Convert.ToInt32(parametros[6]));
            db.AddInParameter(cmd, "_Calidad", DbType.Int32, Convert.ToInt32(parametros[7]));
            db.AddInParameter(cmd, "_Fuero", DbType.Int32, Convert.ToInt32(parametros[8]));
            db.AddInParameter(cmd, "_Clasificacion", DbType.Int32, Convert.ToInt32(parametros[9]));
            db.AddInParameter(cmd, "_Escolaridad", DbType.Int32, Convert.ToInt32(parametros[10]));
            db.AddInParameter(cmd, "_Nacionalidad", DbType.Int32, Convert.ToInt32(parametros[11]));
            db.AddInParameter(cmd, "_Centro", DbType.Int32, Convert.ToInt32(parametros[12]));
            //db.AddInParameter(cmd, "_Fecha1", DbType.DateTime, Convert.ToDateTime(parametros[13]));
            //db.AddInParameter(cmd, "_Fecha2", DbType.DateTime, Convert.ToDateTime(parametros[14]));
            return cmd;
        }



    
    }
}
