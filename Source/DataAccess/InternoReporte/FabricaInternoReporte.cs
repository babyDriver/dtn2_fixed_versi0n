﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.InternoReporte
{
    public class FabricaInternoReporte : IEntityFactory<Entity.Interno_Reporte>
    {
        public Entity.Interno_Reporte ConstructEntity(IDataReader dr)
        {
            var item = new Entity.Interno_Reporte();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

 

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("NCP");
            if (!dr.IsDBNull(index))
            {
                item.NCP = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                item.Estatus = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingIdEstatus");
            if (!dr.IsDBNull(index))
            {
                item.TrackingIdEstatus = dr.GetGuid(index);
            }

            return item;
        }
    }
}
