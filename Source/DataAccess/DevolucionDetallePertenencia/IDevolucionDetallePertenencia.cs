﻿using System;
using System.Collections.Generic;

namespace DataAccess.DevolucionDetallePertenencia
{
    public interface IDevolucionDetallePertenencia
    {
        int Guardar(Entity.DevolucionDetallePertenencia devolucion);
        List<Entity.DevolucionDetallePertenencia> ObtenerByDevolucionId(int id);
    }
}
