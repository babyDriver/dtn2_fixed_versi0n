﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.DevolucionDetallePertenencia
{
    public class ServicioDevolucionDetallePertenencia : ClsAbstractService<Entity.DevolucionDetallePertenencia>, IDevolucionDetallePertenencia
    {
        public ServicioDevolucionDetallePertenencia(string dataBase)
           : base(dataBase)
        {
        }

        public int Guardar(Entity.DevolucionDetallePertenencia devolucion)
        {
            IInsertFactory<Entity.DevolucionDetallePertenencia> insert = new Guardar();
            return Insert(insert, devolucion);
        }

        public List<Entity.DevolucionDetallePertenencia> ObtenerByDevolucionId(int id)
        {
            ISelectFactory<int> select = new ObtenerByDevolucionId();
            return GetAll(select, new FabricaDevolucionDetallePertenencia(), id);
        }
    }
}
