﻿using DataAccess.Interfaces;

namespace DataAccess.DevolucionDetallePertenencia
{
    public class FabricaDevolucionDetallePertenencia : IEntityFactory<Entity.DevolucionDetallePertenencia>
    {
        public Entity.DevolucionDetallePertenencia ConstructEntity(System.Data.IDataReader dr)
        {
            var devolucion = new Entity.DevolucionDetallePertenencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                devolucion.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                devolucion.TrackingId = dr.GetGuid(index);
            }            

            index = dr.GetOrdinal("PertenenciaId");
            if (!dr.IsDBNull(index))
            {
                devolucion.PertenenciaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DevolucionId");
            if (!dr.IsDBNull(index))
            {
                devolucion.DevolucionId = dr.GetInt32(index);
            }            

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                devolucion.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                devolucion.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                devolucion.CreadoPor = dr.GetInt32(index);
            }

            return devolucion;
        }
    }
}
