﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.DevolucionDetallePertenencia
{
    public class Guardar : IInsertFactory<Entity.DevolucionDetallePertenencia>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.DevolucionDetallePertenencia devolucion)
        {
            DbCommand cmd = db.GetStoredProcCommand("DevolucionDetallePertenencia_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, devolucion.TrackingId);            
            db.AddInParameter(cmd, "_PertenenciaId", DbType.Int32, devolucion.PertenenciaId);
            db.AddInParameter(cmd, "_DevolucionId", DbType.Int32, devolucion.DevolucionId);            
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, devolucion.CreadoPor);

            return cmd;
        }
    }
}
