﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.EgresoIngreso
{
    public class ObtenerByFolioAndTipo : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("EgresoIngreso_GetByFolioAndTipo_SP");
            db.AddInParameter(cmd, "_Folio", DbType.Int32, Convert.ToInt32(parametros[0]));
            db.AddInParameter(cmd, "_Egreso", DbType.Boolean, Convert.ToBoolean(parametros[1]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(parametros[2]));
            db.AddInParameter(cmd, "_Tipo", DbType.String, parametros[3].ToString());

            return cmd;
        }
    }
}
