﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.EgresoIngreso
{
    public class ServicioEgresoIngreso : ClsAbstractService<Entity.EgresoIngreso>, IEgresoIngreso
    {
        public ServicioEgresoIngreso(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.EgresoIngreso> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaEgresoIngreso(), new Entity.NullClass());
        }

        public int Guardar(Entity.EgresoIngreso item)
        {
            IInsertFactory<Entity.EgresoIngreso> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.EgresoIngreso ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaEgresoIngreso(), trackingId);
        }

        public Entity.EgresoIngreso ObtenerByFolioAndTipo(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByFolioAndTipo();
            return GetByKey(select, new FabricaEgresoIngreso(), parametros);
        }

        public Entity.EgresoIngreso ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaEgresoIngreso(), id);
        }

        public Entity.EgresoIngreso ObtenerFolio(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerFolio();
            return GetByKey(select, new FabricaEgresoIngreso(), parametros);
        }

        public void Actualizar(Entity.EgresoIngreso item)
        {
            IUpdateFactory<Entity.EgresoIngreso> update = new Actualizar();
            Update(update, item);
        }

        public List<Entity.EgresoIngreso> ObtenerAllByDate(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodosByDate();
            return GetAll(select, new FabricaEgresoIngreso(), parametros);
        }
    }
}
