﻿using System;
using System.Collections.Generic;

namespace DataAccess.EgresoIngreso
{
    public interface IEgresoIngreso
    {
        int Guardar(Entity.EgresoIngreso item);
        void Actualizar(Entity.EgresoIngreso item);
        Entity.EgresoIngreso ObtenerById(int id);
        Entity.EgresoIngreso ObtenerByTrackingId(Guid guid);
        List<Entity.EgresoIngreso> ObtenerTodos();
        Entity.EgresoIngreso ObtenerFolio(object[] parametros);
        Entity.EgresoIngreso ObtenerByFolioAndTipo(object[] parametros);
        List<Entity.EgresoIngreso> ObtenerAllByDate(object[] parametros);
    }
}
