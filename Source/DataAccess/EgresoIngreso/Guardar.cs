﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.EgresoIngreso
{
    public class Guardar : IInsertFactory<Entity.EgresoIngreso>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.EgresoIngreso item)
        {
            DbCommand cmd = db.GetStoredProcCommand("EgresoIngreso_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId.ToString());
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_Folio", DbType.Int32, item.Folio);
            db.AddInParameter(cmd, "_Concepto", DbType.String, item.Concepto);
            db.AddInParameter(cmd, "_Total", DbType.Decimal, item.Total);
            db.AddInParameter(cmd, "_PersonaQuePaga", DbType.String, item.PersonaQuePaga);
            db.AddInParameter(cmd, "_Observacion", DbType.String, item.Observacion);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_Egreso", DbType.Boolean, item.Egreso);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);

            return cmd;
        }
    }
}
