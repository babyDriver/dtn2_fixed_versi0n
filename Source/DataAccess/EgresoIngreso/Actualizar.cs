﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.EgresoIngreso
{
    public class Actualizar : IUpdateFactory<Entity.EgresoIngreso>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.EgresoIngreso item)
        {
            DbCommand cmd = db.GetStoredProcCommand("EgresoIngreso_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_Folio", DbType.Int32, item.Folio);
            db.AddInParameter(cmd, "_Concepto", DbType.String, item.Concepto);
            db.AddInParameter(cmd, "_Total", DbType.Decimal, item.Total);
            db.AddInParameter(cmd, "_PersonaQuePaga", DbType.String, item.PersonaQuePaga);
            db.AddInParameter(cmd, "_Observacion", DbType.String, item.Observacion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);

            return cmd;
        }
    }
}
