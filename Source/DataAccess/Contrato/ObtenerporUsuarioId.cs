﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.Contrato
{
    public class ObtenerporUsuarioId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int _UsuarioId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Contrato_GetByusuarioId_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, _UsuarioId);
            return cmd;
        }
    }
}
