﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Contrato
{
    public class ServicioContrato : ClsAbstractService<Entity.Contrato>, IContrato
    {
        public ServicioContrato(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Contrato> ObtenerByCliente(int id)
        {
            ISelectFactory<int> select = new ObtenerByCliente();
            return GetAll(select, new FabricaContrato(), id);
        }

        public List<Entity.Contrato> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaContrato(), new Entity.NullClass());
        }

        public Entity.Contrato ObtenerByClienteAndContrato(object[] datos)
        {
            ISelectFactory<object[]> select = new ObtenerByClienteAndContrato();
            return GetByKey(select, new FabricaContrato(), datos);
        }

        public Entity.Contrato ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaContrato(), trackingId);
        }

        public Entity.Contrato ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaContrato(), id);
        }

        public int Guardar(Entity.Contrato contrato)
        {
            IInsertFactory<Entity.Contrato> insert = new Guardar();
            return Insert(insert, contrato);
        }

        public void Actualizar(Entity.Contrato contrato)
        {
            IUpdateFactory<Entity.Contrato> update = new Actualizar();
            Update(update, contrato);
        }

        public Entity.Contrato ObtenerbySubcontrato(int subcontratoid)
        {
            ISelectFactory<int> select = new ObtenerBySubcontratoId();
            return GetByKey(select, new FabricaContrato(), subcontratoid);
        }

        public List<Entity.Contrato> ObtenerporUsuarioId(int UsuarioId)
        {
            ISelectFactory<int> select = new ObtenerporUsuarioId();
            return GetAll(select, new FabricaContrato(), UsuarioId);
        }
    }
}
