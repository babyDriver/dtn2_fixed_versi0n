﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Contrato
{
    public class Actualizar : IUpdateFactory<Entity.Contrato>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Contrato contrato)
        {
            DbCommand cmd = db.GetStoredProcCommand("Contrato_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, contrato.Id);
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, contrato.ClienteId);            
            db.AddInParameter(cmd, "_Contrato", DbType.String, contrato.Nombre);
            db.AddInParameter(cmd, "_VigenciaInicial", DbType.Date, contrato.VigenciaInicial);
            db.AddInParameter(cmd, "_VigenciaFinal", DbType.Date, contrato.VigenciaFinal);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, contrato.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, contrato.Habilitado);
            db.AddInParameter(cmd, "_InstitucionId", DbType.Int32, contrato.InstitucionId);

            return cmd;
        }
    }
}
