﻿using System;
using System.Collections.Generic;

namespace DataAccess.Contrato
{
    public interface IContrato
    {
        List<Entity.Contrato> ObtenerByCliente(int id);
        List<Entity.Contrato> ObtenerTodos();
        Entity.Contrato ObtenerByTrackingId(Guid tracking);
        Entity.Contrato ObtenerByClienteAndContrato(object[] parametros);
        int Guardar(Entity.Contrato contrato);
        void Actualizar(Entity.Contrato contrato);
        Entity.Contrato ObtenerById(int id);
        Entity.Contrato ObtenerbySubcontrato(int subcontratoid);
        List<Entity.Contrato> ObtenerporUsuarioId(int UsuarioId);
    }
}
