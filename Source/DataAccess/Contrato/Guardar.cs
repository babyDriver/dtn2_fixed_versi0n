﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Contrato
{
    public class Guardar : IInsertFactory<Entity.Contrato>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Contrato contrato)
        {
            DbCommand cmd = db.GetStoredProcCommand("Contrato_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, contrato.ClienteId);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, contrato.TrackingId.ToString());
            db.AddInParameter(cmd, "_Contrato", DbType.String, contrato.Nombre);
            db.AddInParameter(cmd, "_VigenciaInicial", DbType.Date, contrato.VigenciaInicial);
            db.AddInParameter(cmd, "_VigenciaFinal", DbType.Date, contrato.VigenciaFinal);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, contrato.CreadoPor);
            db.AddInParameter(cmd, "_InstitucionId", DbType.Int32, contrato.InstitucionId);

            return cmd;
        }
    }
}
