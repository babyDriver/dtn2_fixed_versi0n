﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;
namespace DataAccess.Contrato
{
    public class ObtenerBySubcontratoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int subcontratoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Contrato_GetBySubcontratoId_SP");
            db.AddInParameter(cmd, "_SubcontratoId", DbType.Int32, subcontratoId);
            return cmd;
        }
    }
}
