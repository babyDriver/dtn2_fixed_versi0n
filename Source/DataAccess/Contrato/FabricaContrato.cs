﻿using DataAccess.Interfaces;

namespace DataAccess.Contrato
{
    public class FabricaContrato : IEntityFactory<Entity.Contrato>
    {
        public Entity.Contrato ConstructEntity(System.Data.IDataReader dr)
        {
            var contrato = new Entity.Contrato();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                contrato.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                contrato.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("ClienteId");
            if (!dr.IsDBNull(index))
            {
                contrato.ClienteId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Contrato");
            if (!dr.IsDBNull(index))
            {
                contrato.Nombre = dr.GetString(index);
            }                        

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                contrato.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                contrato.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("VigenciaInicial");
            if (!dr.IsDBNull(index))
            {
                contrato.VigenciaInicial = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("VigenciaFinal");
            if (!dr.IsDBNull(index))
            {
                contrato.VigenciaFinal = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                contrato.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("InstitucionId");
            if (!dr.IsDBNull(index))
            {
                contrato.InstitucionId = dr.GetInt32(index);
            }

            return contrato;
        }
    }
}
