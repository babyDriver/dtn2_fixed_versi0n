﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Contrato
{
    public class ObtenerByClienteAndContrato : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Contrato_GetByClienteAndContrato_SP");
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, parametros[0]);
            db.AddInParameter(cmd, "_Contrato", DbType.String, parametros[1].ToString());
            return cmd;
        }
    }
}
