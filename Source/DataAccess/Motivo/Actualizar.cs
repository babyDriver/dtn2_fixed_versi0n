﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Motivo
{
    public class Actualizar : IUpdateFactory<Entity.Motivo>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Motivo motivo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Celda_Update_SP");
            db.AddOutParameter(cmd, "_Id", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, motivo.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, motivo.Nombre);
            db.AddInParameter(cmd, "_Capacidad", DbType.String, motivo.Descripcion);
            db.AddInParameter(cmd, "_CreadoPor", DbType.String, motivo.CreadoPor);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, motivo.Habilitado);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, motivo.Activo);

            return cmd;
        }
    }
}
