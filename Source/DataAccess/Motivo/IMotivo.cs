﻿using System;
using System.Collections.Generic;

namespace DataAccess.Motivo
{
    public interface IMotivo
    {
        List<Entity.Motivo> ObtenerTodos();
        Entity.Motivo ObtenerById(int Id);
        Entity.Motivo ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Motivo Motivo);
        void Actualizar(Entity.Motivo Motivo);
    }
}