﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Motivo
{
    public class ServicioMotivo : ClsAbstractService<Entity.Motivo>, IMotivo
    {
        public ServicioMotivo(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Motivo> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaMotivo(), new Entity.NullClass());
        }

        public Entity.Motivo ObtenerByTrackingId(Guid trackinid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaMotivo(), trackinid);
        }

        public Entity.Motivo ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaMotivo(), Id);
        }
       
        public int Guardar(Entity.Motivo Motivo)
        {
            IInsertFactory<Entity.Motivo> insert = new Guardar();
            return Insert(insert, Motivo);
        }

        public void Actualizar(Entity.Motivo Motivo)
        {
            IUpdateFactory<Entity.Motivo> update = new Actualizar();
            Update(update, Motivo);
        }

        public Entity.Motivo ObtenerByNombre(string nombre)
        {
            ISelectFactory<string> select = new ObtenerByNombre();
            return GetByKey(select, new FabricaMotivo(), nombre);
        }
    }
}