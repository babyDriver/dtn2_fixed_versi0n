﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Motivo
{
    public class Guardar : IInsertFactory<Entity.Motivo>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Motivo motivo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Motivo_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddOutParameter(cmd, "_Id", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, motivo.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, motivo.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, motivo.Descripcion);
            db.AddInParameter(cmd, "_CreadoPor", DbType.String, motivo.CreadoPor);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, motivo.Habilitado);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, motivo.Activo);

            return cmd;
        }
    }
}
