﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Motivo
{
    public class FabricaMotivo : IEntityFactory<Entity.Motivo>
    {
        public Entity.Motivo ConstructEntity(IDataReader dr)
        {
            var motivo = new Entity.Motivo();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                motivo.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                motivo.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                motivo.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                motivo.CreadoPor = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                motivo.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                motivo.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                motivo.Habilitado = dr.GetBoolean(index);
            }

            return motivo;
        }

    }
}