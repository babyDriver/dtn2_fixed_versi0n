﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Motivo
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int motivoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Motivo_GetById_SP");
            db.AddInParameter(cmd, "Id", DbType.Int32, motivoId);
            return cmd;
        }
    }
}
