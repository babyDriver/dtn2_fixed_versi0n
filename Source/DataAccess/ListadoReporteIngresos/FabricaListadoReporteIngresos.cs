﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoReporteIngresos
{
    public class FabricaListadoReporteIngresos : IEntityFactory<Entity.ListadoReporteIngresos>
    {
        public Entity.ListadoReporteIngresos ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoReporteIngresos();

            var index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Detenido");
            if (!dr.IsDBNull(index))
            {
                item.Detenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);                
            }            

            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("TotalHoras");
            if (!dr.IsDBNull(index))
            {
                item.TotalHoras = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                item.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Responsable");
            if (!dr.IsDBNull(index))
            {
                item.Responsable = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                item.LugarDetencion = dr.GetString(index);
            }

            if(item.Fecha == DateTime.MinValue)
            {
                item.FechaAux = string.Empty;
            }
            else
            {
                item.FechaAux = item.Fecha.ToString("yyyy-MM-dd HH:mm:ss");
            }

            return item;
        }
    }
}
