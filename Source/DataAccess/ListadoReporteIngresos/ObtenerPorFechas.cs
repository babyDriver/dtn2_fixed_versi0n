﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ListadoReporteIngresos
{
    public class ObtenerPorFechas : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ListadoReporteIngresos_GetByDates_SP");
            db.AddInParameter(cmd, "_tipo", DbType.String, parametros[0]);
            db.AddInParameter(cmd, "_contratoId", DbType.Int32, parametros[1]);
            db.AddInParameter(cmd, "_inicio", DbType.DateTime, parametros[2]);
            db.AddInParameter(cmd, "_fin", DbType.DateTime, parametros[3]);

            return cmd;
        }
    }
}
