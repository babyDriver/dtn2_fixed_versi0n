﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoReporteIngresos
{
    public class ServicioListadoReporteIngresos : ClsAbstractService<Entity.ListadoReporteIngresos>, IListadoReporteIngresos
    {
        public ServicioListadoReporteIngresos(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoReporteIngresos> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoReporteIngresos(), parametros);
        }

        public List<Entity.ListadoReporteIngresos> ObtenerPorFechas(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerPorFechas();
            return GetAll(select, new FabricaListadoReporteIngresos(), parametros);
        }
    }
}
