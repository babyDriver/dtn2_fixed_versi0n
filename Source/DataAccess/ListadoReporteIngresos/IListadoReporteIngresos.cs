﻿using System.Collections.Generic;

namespace DataAccess.ListadoReporteIngresos
{
    public interface IListadoReporteIngresos
    {
        List<Entity.ListadoReporteIngresos> ObtenerTodos(object[] data);
        List<Entity.ListadoReporteIngresos> ObtenerPorFechas(object[] data);
    }
}
