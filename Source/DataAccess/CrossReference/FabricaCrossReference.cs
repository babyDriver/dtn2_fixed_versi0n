﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CrossReference
{
    public class FabricaCrossReference : IEntityFactory<Entity.CrossReference>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
    {
        public Entity.CrossReference ConstructEntity(IDataReader dr)
        {
            var detalle = new Entity.CrossReference();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                detalle.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("tcn");
            if (!dr.IsDBNull(index))
            {
                detalle.tcn = dr.GetString(index);
            }

            index = dr.GetOrdinal("tcn_candidate");
            if (!dr.IsDBNull(index))
            {
                detalle.tcn_candidate = dr.GetString(index);
            }
            index = dr.GetOrdinal("scores");
            if (!dr.IsDBNull(index))
            {
                detalle.scores = dr.GetString(index);
            }

            index = dr.GetOrdinal("identification");
            if (!dr.IsDBNull(index))
            {
                detalle.identification = dr.GetString(index);
            }

            index = dr.GetOrdinal("product");
            if (!dr.IsDBNull(index))
            {
                detalle.product = dr.GetString(index);
            }
            index = dr.GetOrdinal("transaction_type");
            if (!dr.IsDBNull(index))
            {
                detalle.transaction_type = dr.GetString(index);
            }

            index = dr.GetOrdinal("biometric_type");
            if (!dr.IsDBNull(index))
            {
                detalle.biometric_type = dr.GetString(index);
            }

            index = dr.GetOrdinal("received_datetime");
            if (!dr.IsDBNull(index))
            {
                detalle.received_datetime = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("ip");
            if (!dr.IsDBNull(index))
            {
                detalle.ip = dr.GetString(index);
            }


            index = dr.GetOrdinal("result");
            if (!dr.IsDBNull(index))
            {
                detalle.result = dr.GetString(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                detalle.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                detalle.DetenidoId = dr.GetInt32(index);
            }

            return detalle;
        }
    }
}
