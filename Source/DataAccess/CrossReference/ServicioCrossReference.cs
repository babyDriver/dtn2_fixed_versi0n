﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;
namespace DataAccess.CrossReference
{
    public class ServicioCrossReference:ClsAbstractService<Entity.CrossReference>,ICrossRefence
    {
        public ServicioCrossReference(string dataBase)
        : base(dataBase)
        {
        }

        public List<Entity.CrossReference> GetHit(int[] filtro)
        {
            ISelectFactory<int[]> select = new ObtenerCrossReference();
            return GetAll(select, new FabricaCrossReference(), filtro);
        }
    }
}
