﻿using System;
using System.Data;
using System.Data.Common;
using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.CrossReference
{
    public class ObtenerCrossReference : ISelectFactory<int[]>
    {
        public DbCommand ConstructSelectCommand(Database db, int[] filtros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Cross_reference_GetbyContratoidandDetenidoId");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32,filtros[0]);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, filtros[1]);

            return cmd;
        }
    }
}
