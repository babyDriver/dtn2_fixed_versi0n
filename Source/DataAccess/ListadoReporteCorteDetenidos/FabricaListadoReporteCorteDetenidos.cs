﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoReporteCorteDetenidos
{
    public class FabricaListadoReporteCorteDetenidos : IEntityFactory<Entity.ListadoReporteCorteDetenidos>
    {
        public Entity.ListadoReporteCorteDetenidos ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoReporteCorteDetenidos();

            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                item.Remision = dr.GetString(index);
            }

            index = dr.GetOrdinal("Evento");
            if (!dr.IsDBNull(index))
            {
                item.Evento = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Ingreso");
            if (!dr.IsDBNull(index))
            {
                item.Ingreso = dr.GetString(index);
            }

            index = dr.GetOrdinal("Calificacion");
            if (!dr.IsDBNull(index))
            {
                item.Calificacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Celda");
            if (!dr.IsDBNull(index))
            {
                item.Celda = dr.GetString(index);
            }

            index = dr.GetOrdinal("Cumple");
            if (!dr.IsDBNull(index))
            {
                item.Cumple = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            //Aqui setear val por default
            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {                
                item.Fecha = dr.GetString(index);
                if(item.Fecha == string.Empty)
                {
                    item.Fecha = DateTime.MinValue.ToString();
                }
            }
            else
            {
                item.Fecha = DateTime.MinValue.ToString();
            }

            //Aqui setear val por default
            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetString(index);
                if(item.Folio == string.Empty)
                {
                    item.Folio = "0";
                }
            }
            else
            {
                item.Folio = "0";
            }

            index = dr.GetOrdinal("Motivos");
            if (!dr.IsDBNull(index))
            {
                item.Motivos = dr.GetString(index);
            }

            index = dr.GetOrdinal("HoraDetencion");
            if (!dr.IsDBNull(index))
            {
                item.HoraDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                item.Unidad = dr.GetString(index);
            }            

            index = dr.GetOrdinal("Responsable");
            if (!dr.IsDBNull(index))
            {
                item.Responsable = dr.GetString(index);
            }

            index = dr.GetOrdinal("Lugar");
            if (!dr.IsDBNull(index))
            {
                item.Lugar = dr.GetString(index);
            }

            index = dr.GetOrdinal("IdDetalle");
            if (!dr.IsDBNull(index))
            {
                item.IdDetalle = dr.GetInt32(index);
            }

            return item;
        }
    }
}
