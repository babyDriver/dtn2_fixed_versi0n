﻿using System.Collections.Generic;

namespace DataAccess.ListadoReporteCorteDetenidos
{
    public interface IListadoReporteCorteDetenidos
    {
        List<Entity.ListadoReporteCorteDetenidos> ObtenerTodos(object[] data);
    }
}
