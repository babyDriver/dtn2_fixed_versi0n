﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoReporteCorteDetenidos
{
    public class ServicioListadoReporteCorteDetenidos : ClsAbstractService<Entity.ListadoReporteCorteDetenidos>, IListadoReporteCorteDetenidos
    {
        public ServicioListadoReporteCorteDetenidos(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoReporteCorteDetenidos> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoReporteCorteDetenidos(), parametros);
        }
    }
}
