﻿using DataAccess.Interfaces;

namespace DataAccess.ClasificacionEvidencia
{
    public class FabricaClasificacionEvidencia : IEntityFactory<Entity.ClasificacionEvidencia>
    {
        public Entity.ClasificacionEvidencia ConstructEntity(System.Data.IDataReader dr)
        {
            var clasificacion = new Entity.ClasificacionEvidencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                clasificacion.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                clasificacion.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                clasificacion.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                clasificacion.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                clasificacion.Activo = dr.GetBoolean(index);
            }            

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                clasificacion.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                clasificacion.Habilitado = dr.GetBoolean(index);
            }

            return clasificacion;
        }
    }
}
