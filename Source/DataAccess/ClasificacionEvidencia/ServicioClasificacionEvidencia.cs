﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;


namespace DataAccess.ClasificacionEvidencia
{
    public class ServicioClasificacionEvidencia : ClsAbstractService<Entity.ClasificacionEvidencia>, IClasificacionEvidencia
    {
        public ServicioClasificacionEvidencia(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ClasificacionEvidencia> ObtenerTodo()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodo();
            return GetAll(select, new FabricaClasificacionEvidencia(), new Entity.NullClass());
        }

        public Entity.ClasificacionEvidencia ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaClasificacionEvidencia(), id);
        }
    }
}
