﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ClasificacionEvidencia
{
    public interface IClasificacionEvidencia
    {
        List<Entity.ClasificacionEvidencia> ObtenerTodo();
        Entity.ClasificacionEvidencia ObtenerById(int id);
    }
}
