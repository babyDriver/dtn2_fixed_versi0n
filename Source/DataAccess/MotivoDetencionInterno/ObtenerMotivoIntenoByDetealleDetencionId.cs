﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.MotivoDetencionInterno
{
    public class ObtenerMotivoIntenoByDetealleDetencionId : ISelectFactory<Entity.MotivoDetencionInterno>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.MotivoDetencionInterno motivo)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencionInternoByDetalleDetencionId_SP");

            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, motivo.InternoId);

            return cmd;
        }
    }
}
