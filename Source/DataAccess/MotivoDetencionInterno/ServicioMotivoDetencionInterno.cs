﻿using DataAccess.Catalogo;
using DataAccess.Interfaces;
using DataAccess.MotivoDetencionInterno;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.MotivoDetencionInterno
{
    public class ServicioMotivoDetencionInterno : ClsAbstractService<Entity.MotivoDetencionInterno>, IMotivoDetencionInterno
    {
        public ServicioMotivoDetencionInterno(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.MotivoDetencionInterno item)
        {
            IUpdateFactory<Entity.MotivoDetencionInterno> update = new Actualizar();
            Update(update, item);
        }

        public Entity.MotivoDetencionInterno GetMotivoDentencionInterno(Entity.MotivoDetencionInterno motivoDetencion)
        {
            ISelectFactory<Entity.MotivoDetencionInterno> select = new ObtenerMotivoDetencionInterno();
            return GetByKey(select, new FabricaMotivoDetencionInterno(), motivoDetencion);
        }

        public List<Entity.MotivoDetencionInterno> GetMotivoDetencionInternosByDetalleDetencionId(Entity.MotivoDetencionInterno motivoDetencion)
        {

            ISelectFactory<Entity.MotivoDetencionInterno> select = new ObtenerMotivoIntenoByDetealleDetencionId();
            return GetAll(select, new FabricaMotivoDetencionInterno(), motivoDetencion);
        }

        public int Guardar(Entity.MotivoDetencionInterno motivoDetencion)
        {
            IInsertFactory<Entity.MotivoDetencionInterno> insert = new Guardar();
            return Insert(insert, motivoDetencion);
        }

        public Entity.MotivoDetencionInterno ObtenerByTrackingId(Guid id)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaMotivoDetencionInterno(), id);
        }
    }
}

