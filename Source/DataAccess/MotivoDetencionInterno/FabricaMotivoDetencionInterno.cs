﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.MotivoDetencionInterno
{
    class FabricaMotivoDetencionInterno : IEntityFactory<Entity.MotivoDetencionInterno>
    {
        public Entity.MotivoDetencionInterno ConstructEntity(IDataReader dr)
        {          
            var contrato = new Entity.MotivoDetencionInterno();

            var index = dr.GetOrdinal("InternoId");
            if (!dr.IsDBNull(index))
            {
                contrato.InternoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("MotivoDetencionId");
            if (!dr.IsDBNull(index))
            {
                contrato.MotivoDetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("MultaSugerida");
            if (!dr.IsDBNull(index))
            {
                contrato.MultaSugerida = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                contrato.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                contrato.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                contrato.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                contrato.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                contrato.TrackingId = dr.GetGuid(index);
            }

            return contrato;            
        }
    }
}
