﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.MotivoDetencionInterno
{
    class ObtenerMotivoDetencionInterno : ISelectFactory<Entity.MotivoDetencionInterno>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.MotivoDetencionInterno identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencion_Interno_GET_SP");
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, identity.InternoId);
            db.AddInParameter(cmd, "_MotivoDetencionId", DbType.Int32, identity.MotivoDetencionId);
            return cmd;
        }
    }
}
