﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.MotivoDetencionInterno
{
    public interface IMotivoDetencionInterno
    {
        int Guardar(Entity.MotivoDetencionInterno motivoDetencion);

        void Actualizar(Entity.MotivoDetencionInterno motivoDetencion);
        Entity.MotivoDetencionInterno GetMotivoDentencionInterno(Entity.MotivoDetencionInterno motivoDetencion);
        List<Entity.MotivoDetencionInterno> GetMotivoDetencionInternosByDetalleDetencionId(Entity.MotivoDetencionInterno motivoDetencion);
        Entity.MotivoDetencionInterno ObtenerByTrackingId(Guid id);
    }
}
