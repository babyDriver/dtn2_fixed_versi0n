﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.MotivoDetencionInterno
{
    public class Actualizar : IUpdateFactory<Entity.MotivoDetencionInterno>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.MotivoDetencionInterno item)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencionInterno_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_MotivoDetencionId", DbType.Int32, item.MotivoDetencionId);
            db.AddInParameter(cmd, "_MultaSugerida", DbType.Decimal, item.MultaSugerida);            
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);

            return cmd;
        }
    }
}
