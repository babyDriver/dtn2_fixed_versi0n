﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.MotivoDetencionInterno
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid id)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencionInterno_GetById_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, id);
            return cmd;
        }
    }
}
