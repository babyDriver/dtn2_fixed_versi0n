﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;


namespace DataAccess.MotivoDetencionInterno
{
    public class Guardar : IInsertFactory<Entity.MotivoDetencionInterno>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.MotivoDetencionInterno motivoDetencionInterno)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencion_interno_Insert_SP");
         
            db.AddInParameter(cmd, "_MotivoDetencionId", DbType.Int32, motivoDetencionInterno.MotivoDetencionId);

            db.AddInParameter(cmd, "_Internoid", DbType.Int32, motivoDetencionInterno.InternoId);
            db.AddInParameter(cmd, "_MultaSugerida", DbType.Decimal, motivoDetencionInterno.MultaSugerida);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, motivoDetencionInterno.TrackingId.ToString());
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, motivoDetencionInterno.CreadoPor);
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            return cmd;
        }

    }
}
