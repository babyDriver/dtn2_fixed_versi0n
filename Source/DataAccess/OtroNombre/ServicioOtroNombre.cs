﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.OtroNombre
{
    public class ServicioOtroNombre : ClsAbstractService<Entity.Otro_Nombre>, IOtroNombre
    {
        public ServicioOtroNombre (string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Otro_Nombre> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaOtroNombre(), new Entity.NullClass());
        }

   

        public Entity.Otro_Nombre ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaOtroNombre(), userId);
        }

        public Entity.Otro_Nombre ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaOtroNombre(), Id);
        }

        public Entity.Otro_Nombre ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaOtroNombre(), Id);
        }

        public Entity.Otro_Nombre ObtenerByParametros(string[] otro)
        {
            ISelectFactory<string[]> select = new ObtenerByParametros();
            return GetByKey(select, new FabricaOtroNombre(), otro);
        }

        public int Guardar(Entity.Otro_Nombre otro)
        {
            IInsertFactory<Entity.Otro_Nombre> insert = new Guardar();
            return Insert(insert, otro);
        }

        public void Actualizar(Entity.Otro_Nombre otro)
        {
            IUpdateFactory<Entity.Otro_Nombre> update = new Actualizar();
            Update(update, otro);
        }


    }
}