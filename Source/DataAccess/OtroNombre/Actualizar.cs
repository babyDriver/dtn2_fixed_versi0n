﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.OtroNombre
{
    public class Actualizar : IUpdateFactory<Entity.Otro_Nombre>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Otro_Nombre otro)
        {
            DbCommand cmd = db.GetStoredProcCommand("Otro_Nombre_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, otro.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, otro.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, otro.Nombre);
            db.AddInParameter(cmd, "_Paterno", DbType.String, otro.Paterno);
            db.AddInParameter(cmd, "_Materno", DbType.String, otro.Materno);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, otro.Activo);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, otro.DetenidoId);

           

            return cmd;
        }
    }
}
