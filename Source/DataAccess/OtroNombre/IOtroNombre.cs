﻿using System;
using System.Collections.Generic;

namespace DataAccess.OtroNombre
{
    public interface IOtroNombre
    {
        List<Entity.Otro_Nombre> ObtenerTodos();
        Entity.Otro_Nombre ObtenerById(int Id);
        Entity.Otro_Nombre ObtenerByDetenidoId(int Id);
        Entity.Otro_Nombre ObtenerByTrackingId(Guid trackingid);
        Entity.Otro_Nombre ObtenerByParametros(string[] otro);
        int Guardar(Entity.Otro_Nombre otro);
        void Actualizar(Entity.Otro_Nombre otro);
    }
}