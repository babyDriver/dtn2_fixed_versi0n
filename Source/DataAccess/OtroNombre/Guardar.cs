﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.OtroNombre
{
    public class Guardar : IInsertFactory<Entity.Otro_Nombre>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Otro_Nombre otro)
        {
            DbCommand cmd = db.GetStoredProcCommand("Otro_Nombre_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, otro.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, otro.Nombre);
            db.AddInParameter(cmd, "_Paterno", DbType.String, otro.Paterno);
            db.AddInParameter(cmd, "_Materno", DbType.String, otro.Materno);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, otro.Activo);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, otro.DetenidoId);

            return cmd;
        }
    }
}
