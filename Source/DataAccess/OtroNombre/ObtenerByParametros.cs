﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.OtroNombre
{
    public class ObtenerByParametros : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] otro)
        {
            DbCommand cmd = db.GetStoredProcCommand("Otro_Nombre_GetByParametros_SP");
            db.AddInParameter(cmd, "_Nombre", DbType.String, otro[0]);
            db.AddInParameter(cmd, "_Paterno", DbType.String, otro[1]);
            db.AddInParameter(cmd, "_Materno", DbType.String, otro[2]);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, otro[3]);
            return cmd;
        }
    }
}
