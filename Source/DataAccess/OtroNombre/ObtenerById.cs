﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.OtroNombre
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Otro_Nombre_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, id);
            return cmd;
        }
    }
}
