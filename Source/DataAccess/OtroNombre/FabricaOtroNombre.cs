﻿using DataAccess.Interfaces;


namespace DataAccess.OtroNombre
{
    public class FabricaOtroNombre : IEntityFactory<Entity.Otro_Nombre>
    {
       

        public Entity.Otro_Nombre ConstructEntity(System.Data.IDataReader dr)
        {
            var otro = new Entity.Otro_Nombre();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                otro.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                otro.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                otro.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                otro.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                otro.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                otro.Activo = dr.GetBoolean(index);
            }



            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                otro.DetenidoId = dr.GetInt32(index);
            }


            return otro;
        }
    }
}