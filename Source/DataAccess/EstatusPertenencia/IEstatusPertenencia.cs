﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.EstatusPertenencia
{
    public interface IEstatusPertenencia
    {
        Entity.EstatusPertenencia ObtenerById(int id);
        List<Entity.EstatusPertenencia> ObtenerTodo();
    }
}
