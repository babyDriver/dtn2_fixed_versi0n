﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.EstatusPertenencia
{
    public class ServicioEstatusPertenencia : ClsAbstractService<Entity.EstatusPertenencia>, IEstatusPertenencia
    {
        public ServicioEstatusPertenencia(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.EstatusPertenencia ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaEstatusPertenencia(), id);
        }

        public List<Entity.EstatusPertenencia> ObtenerTodo()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodo();
            return GetAll(select, new FabricaEstatusPertenencia(), new Entity.NullClass());
        }
    }
}
