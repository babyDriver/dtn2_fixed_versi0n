﻿using DataAccess.Interfaces;

namespace DataAccess.EstatusPertenencia
{
    public class FabricaEstatusPertenencia : IEntityFactory<Entity.EstatusPertenencia>
    {
        public Entity.EstatusPertenencia ConstructEntity(System.Data.IDataReader dr)
        {
            var estatusPertenencia = new Entity.EstatusPertenencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                estatusPertenencia.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                estatusPertenencia.Nombre = dr.GetString(index);
            }


            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                estatusPertenencia.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                estatusPertenencia.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                estatusPertenencia.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                estatusPertenencia.CreadoPor = dr.GetInt32(index);
            }            

            return estatusPertenencia;
        }
    }
}
