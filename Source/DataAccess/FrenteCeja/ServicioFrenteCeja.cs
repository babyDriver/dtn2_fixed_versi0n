﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.FrenteCeja
{
    public class ServicioFrenteCeja : ClsAbstractService<Entity.FrenteCeja>, IFrenteCeja
    {
        public ServicioFrenteCeja(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.FrenteCeja> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaFrenteCeja(), new Entity.NullClass());
        }

   

        public Entity.FrenteCeja ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaFrenteCeja(), userId);
        }

        public Entity.FrenteCeja ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaFrenteCeja(), Id);
        }

        public Entity.FrenteCeja ObtenerByAntropometriaId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByAntropometriaId();
            return GetByKey(select, new FabricaFrenteCeja(), Id);
        }

        public int Guardar(Entity.FrenteCeja frente)
        {
            IInsertFactory<Entity.FrenteCeja> insert = new Guardar();
            return Insert(insert, frente);
        }

        public void Actualizar(Entity.FrenteCeja frente)
        {
            IUpdateFactory<Entity.FrenteCeja> update = new Actualizar();
            Update(update, frente);
        }


    }
}