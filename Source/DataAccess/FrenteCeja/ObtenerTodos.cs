﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.FrenteCeja
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.NullClass id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Frente_Cejas_GetAll_SP");
            return cmd;
        }
    }
}
