﻿using DataAccess.Interfaces;


namespace DataAccess.FrenteCeja
{
    public class FabricaFrenteCeja : IEntityFactory<Entity.FrenteCeja>
    {
       

        public Entity.FrenteCeja ConstructEntity(System.Data.IDataReader dr)
        {
            var frente = new Entity.FrenteCeja();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                frente.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                frente.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("AlturaF");
            if (!dr.IsDBNull(index))
            {
                frente.AlturaF = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AnchoF");
            if (!dr.IsDBNull(index))
            {
                frente.AnchoF = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("InclinacionF");
            if (!dr.IsDBNull(index))
            {
                frente.InclinacionF = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DireccionC");
            if (!dr.IsDBNull(index))
            {
                frente.DireccionC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ImplantacionC");
            if (!dr.IsDBNull(index))
            {
                frente.ImplantacionC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FormaC");
            if (!dr.IsDBNull(index))
            {
                frente.FormaC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TamanoC");
            if (!dr.IsDBNull(index))
            {
                frente.TamanoC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AntropometriaId");
            if (!dr.IsDBNull(index))
            {
                frente.AntropometriaId = dr.GetInt32(index);
            }
            
            return frente;
        }
    }
}