﻿using System;
using System.Collections.Generic;

namespace DataAccess.FrenteCeja
{
    public interface IFrenteCeja
    {
        List<Entity.FrenteCeja> ObtenerTodos();
        Entity.FrenteCeja ObtenerById(int Id);
        Entity.FrenteCeja ObtenerByAntropometriaId(int Id);
        Entity.FrenteCeja ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.FrenteCeja frente);
        void Actualizar(Entity.FrenteCeja frente);
    }
}