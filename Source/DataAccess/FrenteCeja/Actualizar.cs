﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.FrenteCeja
{
    public class Actualizar : IUpdateFactory<Entity.FrenteCeja>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.FrenteCeja frente)
        {
            DbCommand cmd = db.GetStoredProcCommand("Frente_Cejas_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, frente.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, frente.TrackingId);
            if (frente.AlturaF != null)
                db.AddInParameter(cmd, "_AlturaF", DbType.Int32, frente.AlturaF);
            else
                db.AddInParameter(cmd, "_AlturaF", DbType.Int32, null);

            if (frente.AnchoF != null)
                db.AddInParameter(cmd, "_AnchoF", DbType.Int32, frente.AnchoF);
            else
                db.AddInParameter(cmd, "_AnchoF", DbType.Int32, null);

            db.AddInParameter(cmd, "_InclinacionF", DbType.Int32, frente.InclinacionF);
            db.AddInParameter(cmd, "_DireccionC", DbType.Int32, frente.DireccionC);
            db.AddInParameter(cmd, "_ImplantacionC", DbType.Int32, frente.ImplantacionC);
            db.AddInParameter(cmd, "_FormaC", DbType.Int32, frente.FormaC);
            db.AddInParameter(cmd, "_TamanoC", DbType.Int32, frente.TamanoC);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, frente.AntropometriaId);

           

            return cmd;
        }
    }
}
