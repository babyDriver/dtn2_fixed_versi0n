﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.FrenteCeja
{
    public class Guardar : IInsertFactory<Entity.FrenteCeja>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.FrenteCeja frente)
        {
            DbCommand cmd = db.GetStoredProcCommand("Frente_Cejas_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, frente.TrackingId);
            if (frente.AlturaF != null)
                db.AddInParameter(cmd, "_AlturaF", DbType.Int32, frente.AlturaF);
            else
                db.AddInParameter(cmd, "_AlturaF", DbType.Int32, null);

            if (frente.AnchoF != null)
                db.AddInParameter(cmd, "_AnchoF", DbType.Int32, frente.AnchoF);
            else
                db.AddInParameter(cmd, "_AnchoF", DbType.Int32, null);

            db.AddInParameter(cmd, "_InclinacionF", DbType.Int32, frente.InclinacionF);
            db.AddInParameter(cmd, "_DireccionC", DbType.Int32, frente.DireccionC);
            db.AddInParameter(cmd, "_ImplantacionC", DbType.Int32, frente.ImplantacionC);
            db.AddInParameter(cmd, "_FormaC", DbType.Int32, frente.FormaC);
            db.AddInParameter(cmd, "_TamanoC", DbType.Int32, frente.TamanoC);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, frente.AntropometriaId);


            return cmd;
        }
    }
}
