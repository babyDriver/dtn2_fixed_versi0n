﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.EstudioDiganostico
{
    public class Guardar : IInsertFactory<Entity.EstudioDiagnostico>
    {
        public DbCommand ConstructInsertCommand(Database db, EstudioDiagnostico estudioDiagnostico)
        {
            DbCommand cmd = db.GetStoredProcCommand("DiagnosticoTrabajoSocial_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, estudioDiagnostico.TrackingId);
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, estudioDiagnostico.DetalledetencionId);
            db.AddInParameter(cmd, "_TipodediagnosticoId", DbType.Int32, estudioDiagnostico.TipodediagnosticoId);
            db.AddInParameter(cmd, "_Fechayhora", DbType.DateTime, estudioDiagnostico.Fechayhora);
            db.AddInParameter(cmd, "_Observaciones", DbType.String, estudioDiagnostico.Observaciones);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, estudioDiagnostico.Creadopor);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, estudioDiagnostico.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, estudioDiagnostico.Habilitado);
            db.AddInParameter(cmd, "_Detallediagnostico", DbType.String, estudioDiagnostico.Detallediagnostico);

            return cmd;
        }
    }
}
