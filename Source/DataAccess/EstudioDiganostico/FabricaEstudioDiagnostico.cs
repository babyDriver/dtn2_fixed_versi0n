﻿
using DataAccess.Interfaces;
using DataAccess.Catalogo;
using Entity;
using System.Data;

namespace DataAccess.EstudioDiganostico
{
    public class FabricaEstudioDiagnostico : IEntityFactory<Entity.EstudioDiagnostico>
    {
        public EstudioDiagnostico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.EstudioDiagnostico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalledetencionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TipodediagnosticoId");
            if (!dr.IsDBNull(index))
            {
                item.TipodediagnosticoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Fechayhora");
            if (!dr.IsDBNull(index))
            {
                item.Fechayhora = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Observaciones");
            if (!dr.IsDBNull(index))
            {
                item.Observaciones = dr.GetString(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Detallediagnostico");
            if (!dr.IsDBNull(index))
            {
                item.Detallediagnostico = dr.GetString(index);
            }

            return item;
        }
    }
}
