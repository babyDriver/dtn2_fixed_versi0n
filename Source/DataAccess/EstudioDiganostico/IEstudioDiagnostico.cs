﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.EstudioDiganostico
{
     public interface IEstudioDiagnostico
    {
        int Guardar(Entity.EstudioDiagnostico estudioDiagnostico);
        int ObtenerByTrackingId(string TrackingId);
    }
}
