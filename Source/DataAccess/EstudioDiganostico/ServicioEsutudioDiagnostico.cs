﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;


namespace DataAccess.EstudioDiganostico
{
    public class ServicioEsutudioDiagnostico : ClsAbstractService<Entity.EstudioDiagnostico>, IEstudioDiagnostico
    {
        public int Guardar(Entity.EstudioDiagnostico estudioDiagnostico)
        {
            IInsertFactory<Entity.EstudioDiagnostico> insert = new Guardar();
            return Insert(insert, estudioDiagnostico);
        }

        public int ObtenerByTrackingId(string TrackingId)
        {
            throw new NotImplementedException();
        }
        public Entity.EstudioDiagnostico ObtenerByTrackingId(Guid trackingid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaEstudioDiagnostico(), trackingid);
        }
    }
}
