﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.RelacionDetenidoCalificacion
{
    class FabricaRelacionDetenidoCalificacion : IEntityFactory<Entity.RelacionDetenidoCalificacion>
    {
        public Entity.RelacionDetenidoCalificacion ConstructEntity(IDataReader dr)
        {
            var item = new Entity.RelacionDetenidoCalificacion();
            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                item.Remision = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fechadetencion");
            if (!dr.IsDBNull(index))
            {
                item.Fechadetencion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                item.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecharegistro");
            if (!dr.IsDBNull(index))
            {
                item.Fecharegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Totalhoras");
            if (!dr.IsDBNull(index))
            {
                item.Totalhoras = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Motivodetencion");
            if (!dr.IsDBNull(index))
            {
                item.Motivodetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fechasalida");
            if (!dr.IsDBNull(index))
            {
                item.Fechasalida = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("SituacionId");
            if (!dr.IsDBNull(index))
            {
                item.SituacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Situacion");
            if (!dr.IsDBNull(index))
            {
                item.Situacion = dr.GetString(index);
            }

            return item;
        }
    }
}