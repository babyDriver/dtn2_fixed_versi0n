﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.RelacionDetenidoCalificacion
{
    public class ServicioRelacionDetenidoCalificacion : ClsAbstractService<Entity.RelacionDetenidoCalificacion>, IRelacionDetenidoCalificacion
    {
        public ServicioRelacionDetenidoCalificacion(string dataBase)
       : base(dataBase)
        {

        }

        public List<Entity.RelacionDetenidoCalificacion> GetList(string[] filtros)
        {
            ISelectFactory<string[]> select = new ObtenerRelacionDetenidosCalificacion();
            return GetAll(select, new FabricaRelacionDetenidoCalificacion(), filtros);
        }
    }
}