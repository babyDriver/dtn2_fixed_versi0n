﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.RelacionDetenidoCalificacion
{
    public interface IRelacionDetenidoCalificacion
    {
        List<Entity.RelacionDetenidoCalificacion> GetList(string[] filtros);
    }
}