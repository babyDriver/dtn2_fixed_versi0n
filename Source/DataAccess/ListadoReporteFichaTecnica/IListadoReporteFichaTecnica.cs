﻿using System.Collections.Generic;

namespace DataAccess.ListadoReporteFichaTecnica
{
    public interface IListadoReporteFichaTecnica
    {
        List<Entity.ListadoReporteFichaTecnica> ObtenerTodos(object[] data);
    }
}
