﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoReporteFichaTecnica
{
    public class ServicioListadoReporteFichaTecnica : ClsAbstractService<Entity.ListadoReporteFichaTecnica>, IListadoReporteFichaTecnica
    {
        public ServicioListadoReporteFichaTecnica(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoReporteFichaTecnica> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoReporteFichaTecnica(), parametros);
        }
    }
}
