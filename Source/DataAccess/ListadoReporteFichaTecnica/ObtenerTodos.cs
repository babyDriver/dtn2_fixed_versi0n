﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ListadoReporteFichaTecnica
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ListadoReporteFichaTecnica_GetAll_SP");            
            db.AddInParameter(cmd, "_contratoId", DbType.Int32, parametros[0]);
            db.AddInParameter(cmd, "_subcontratoId", DbType.Int32, parametros[1]);
            db.AddInParameter(cmd, "_clienteId", DbType.Int32, parametros[2]);

            return cmd;
        }
    }
}
