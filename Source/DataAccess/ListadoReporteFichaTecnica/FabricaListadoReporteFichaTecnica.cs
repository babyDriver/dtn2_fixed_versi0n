﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoReporteFichaTecnica
{
    public class FabricaListadoReporteFichaTecnica : IEntityFactory<Entity.ListadoReporteFichaTecnica>
    {
        public Entity.ListadoReporteFichaTecnica ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoReporteFichaTecnica();

            var index = dr.GetOrdinal("DetalleDetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalleDetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingIdDetalle");
            if (!dr.IsDBNull(index))
            {
                item.TrackingIdDetalle = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DescripcionEvento");
            if (!dr.IsDBNull(index))
            {
                item.DescripcionEvento = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                item.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Responsable");
            if (!dr.IsDBNull(index))
            {
                item.Responsable = dr.GetString(index);
            }
            
            index = dr.GetOrdinal("HoraYFecha");
            if (!dr.IsDBNull(index))
            {
                item.HoraYFecha = dr.GetDateTime(index);                
            }                        

            index = dr.GetOrdinal("Situacion");
            if (!dr.IsDBNull(index))
            {
                item.Situacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("TotalHoras");
            if (!dr.IsDBNull(index))
            {
                item.TotalHoras = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechasExamen");
            if (!dr.IsDBNull(index))
            {
                item.FechasExamen = dr.GetString(index);
            }

            index = dr.GetOrdinal("IndicacionesExamen");
            if (!dr.IsDBNull(index))
            {
                item.IndicacionesExamen = dr.GetString(index);
            }

            index = dr.GetOrdinal("Subcontrato");
            if (!dr.IsDBNull(index))
            {
                item.Subcontrato = dr.GetString(index);
            }
            
            item.FechaAux = item.HoraYFecha.ToString("yyyy-MM-dd HH:mm:ss");

            return item;
        }
    }
}
