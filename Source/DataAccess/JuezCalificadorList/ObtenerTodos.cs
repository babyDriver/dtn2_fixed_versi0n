﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.JuezCalificadorList
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("JuezCalificadorList_GetByData_SP");
            db.AddInParameter(cmd, "_anio", DbType.Int32, parametros[0]);
            db.AddInParameter(cmd, "_opcion", DbType.String, parametros[1]);
            db.AddInParameter(cmd, "_contratoId", DbType.Int32, parametros[2]);
            db.AddInParameter(cmd, "_tipo", DbType.String, parametros[3]);
            db.AddInParameter(cmd, "_tipoSalida", DbType.Int32, parametros[4]);
            return cmd;
        }
    }
}
