﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.JuezCalificadorList
{
    public class ServicioJuezCalificadorList : ClsAbstractService<Entity.JuezCalificadorList>, IJuezCalificadorList
    {
        public ServicioJuezCalificadorList(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.JuezCalificadorList> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaJuezCalificadorList(), parametros);
        }
    }
}
