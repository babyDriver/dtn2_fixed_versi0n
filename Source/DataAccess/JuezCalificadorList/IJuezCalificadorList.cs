﻿using System.Collections.Generic;

namespace DataAccess.JuezCalificadorList
{
    public interface IJuezCalificadorList
    {
        List<Entity.JuezCalificadorList> ObtenerTodos(object[] data);
    }
}
