﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.JuezCalificadorList
{
    public class FabricaJuezCalificadorList : IEntityFactory<Entity.JuezCalificadorList>
    {
        public Entity.JuezCalificadorList ConstructEntity(IDataReader dr)
        {
            var item = new Entity.JuezCalificadorList();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("RutaImagen");
            if (!dr.IsDBNull(index))
            {
                item.RutaImagen = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("NCP");
            if (!dr.IsDBNull(index))
            {
                item.NCP = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                item.Estatus = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingIdEstatus");
            if (!dr.IsDBNull(index))
            {
                item.TrackingIdEstatus = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("EstatusNombre");
            if (!dr.IsDBNull(index))
            {
                item.EstatusNombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalleDetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("FechaNacimiento");
            if (!dr.IsDBNull(index))
            {
                item.FechaNacimiento = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("NombreCompleto");
            if (!dr.IsDBNull(index))
            {
                item.NombreCompleto = dr.GetString(index);
            }

            index = dr.GetOrdinal("CalificacionId");
            if (!dr.IsDBNull(index))
            {
                item.CalificacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TotalAPagar");
            if (!dr.IsDBNull(index))
            {
                item.TotalAPagar = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Situacion");
            if (!dr.IsDBNull(index))
            {
                item.Situacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("IngresoId");
            if (!dr.IsDBNull(index))
            {
                item.IngresoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("horas");
            if (!dr.IsDBNull(index))
            {
                item.horas = dr.GetString(index);
            }

            return item;
        }
    }
}
