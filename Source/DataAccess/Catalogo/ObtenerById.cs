﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Catalogo
{
    public class ObtenerById : ISelectFactory<object[]>
    {

        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            string table = getTable(System.Convert.ToInt32(parametros[1]));
            DbCommand cmd = db.GetStoredProcCommand("Catalogo_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, parametros[0]);
            db.AddInParameter(cmd, "_table", DbType.String, table);
            return cmd;
        }
        public string getTable(int tipocatalogo)
        {
            string _table = "";
            switch (tipocatalogo)
            {

                case 1: _table = "tipo_lesion"; break;
                case 4: _table = "sexo"; break;
                case 5:
                    _table = "pantalla"; break;
                case 6:
                    _table = "permiso"; break;
                case 13: _table = "tipo_media_filiacion"; break;
                case 14: _table = "etnia"; break;
                case 15: _table = "escolaridad"; break;
                case 21: _table = "peligrosidad_criminologica"; break;
                case 23: _table = "religion"; break;
                case 26: _table = "estado_civil"; break;
                case 28: _table = "nacionalidad"; break;
                case 34: _table = "parentesco"; break;
                case 37: _table = "tipo_estudio"; break;
                case 38: _table = "tipo_tatuaje"; break;
                case 39: _table = "lunar"; break;
                case 41: _table = "media_filiacion"; break;
                case 43: _table = "mes"; break;
                case 46: _table = "tamaño"; break;
                case 47: _table = "adherencia"; break;
                case 48: _table = "tipo_senal"; break;
                case 49: _table = "lado_senal"; break;
                case 50: _table = "vista_senal"; break;
                case 80: _table = "evento_reciente"; break;
                case 81:
                    _table = "turno"; break;
                case 82:
                    _table = "unidad"; break;
                case 83:
                    _table = "responsable_unidad"; break;
                case 84:
                    _table = "tipo_intoxicacion"; break;
                case 85:
                    _table = "clasificacion_evidencias"; break;
                case 86:
                    _table = "delegacion"; break;
                case 87:
                    _table = "corporacion"; break;
                case 89:
                    _table = "situacion_detenido"; break;
                case 90:
                    _table = "institucion"; break;
                case 91:
                    _table = "pertenencia"; break;
                case 92:
                    _table = "estatus_pertenencia"; break;
                case 93:
                    _table = "tipo_salida"; break;
                case 94:
                    _table = "prueba"; break;
                case 95:
                    _table = "calificacion"; break;
                case 96:
                    _table = "tipo_examen"; break;
                case 97:
                    _table = "salida_autorizada"; break;
                case 98:
                    _table = "institucion_disposicion"; break;

                case 100:
                    _table = "entrega_pertenencia"; break;
                case 101:
                    _table = "sector"; break;


                case 102: _table = "mucosas"; break;
                case 103:
                    _table = "aliento"; break;
                case 104:
                    _table = "examen_neurologico"; break;
                case 105:
                    _table = "conjuntivas"; break;
                case 106:
                    _table = "marcha"; break;
                case 107:
                    _table = "reflejos_pupilares"; break;
                case 108:
                    _table = "reflejos_osteo_tendinosos"; break;
                case 109:
                    _table = "conducta"; break;
                case 110:
                    _table = "romberq"; break;
                case 111:
                    _table = "lenguaje"; break;
                case 112:
                    _table = "atencion"; break;
                case 113:
                    _table = "orientacion"; break;
                case 114:
                    _table = "diadococinencia"; break;
                case 115:
                    _table = "dedo_nariz"; break;
                case 116:
                    _table = "talon_rodilla"; break;
                case 117:
                    _table = "pupilas"; break;
                case 118:
                    _table = "coordinacion"; break;
                case 119:
                    _table = "tipomovimiento_celda"; break;
                case 120:
                    _table = "motivo_rehabilitacion"; break;
                case 121:
                    _table = "tipodediagnostico"; break;
                case 122:
                    _table = "Catalgo_Adiccion"; break;
                case 123:
                    _table = "pertenenciacomun"; break;
                case 124:
                    _table = "lenguanativa"; break;
                case 125:
                    _table = "divisas"; break;
                case 2: _table = "delito_modalidad"; break;
                case 3: _table = "delito_tipo"; break;
                // case 4: _table = "sexo"; break;
                // case 5: _table = "pantalla"; break;
                // case 6: _table = "permiso"; break;
                case 7: _table = "arma"; break;
                case 8: _table = "delito"; break;
                case 9: _table = "emisor"; break;
                case 10: _table = "cicatriz"; break;
                case 11: _table = "vehiculo"; break;
                case 12: _table = "marca_vehiculo"; break;
                //  case 13: _table = "tipo_media_filiacion"; break;
                // case 14: _table = "etnia"; break;
                // case 15: _table = "escolaridad"; break;
                case 16: _table = "motivo_externacion"; break;
                case 17: _table = "autor"; break;
                case 18: _table = "autoria"; break;
                case 19: _table = "ubicacion_interno"; break;
                case 20: _table = "estado_interno"; break;
                // case 21: _table = "peligrosidad_criminologica"; break;
                case 22: _table = "peligrosidad_dictada_juez"; break;
                // case 23: _table = "religion"; break;
                case 24: _table = "quien_consigna"; break;
                case 25: _table = "clasificacion_juridica"; break;
                // case 26: _table = "estado_civil"; break;
                case 27: _table = "juzgado"; break;
                //  case 28: _table = "nacionalidad"; break;
                case 29: _table = "ocupacion"; break;
                case 30: _table = "ocupacion_centro"; break;
                case 31: _table = "profesion_centro"; break;
                case 32: _table = "tribunal_amparo"; break;
                case 33: _table = "tribunal_apelacion"; break;
                //case 34: _table = "parentesco"; break;
                case 35: _table = "termino_constitucional"; break;
                case 36: _table = "beneficio"; break;
                // case 37: _table = "tipo_estudio"; break;
                // case 38: _table = "tatuaje"; break;
                // case 39: _table = "lunar"; break;
                case 40: _table = "subtipo_arma"; break;
                //case 41: _table = "media_filiacion"; break;
                case 42: _table = "estudio  "; break;
                // case 43: _table = "mes"; break;
                case 44: _table = "tipo_discapacidad"; break;
                case 45: _table = "pais"; break;
                // case 46: _table = "tamaño"; break;
                //case 47: _table = "adherencia"; break;
                //case 48: _table = "tipo_senal"; break;
                //case 49: _table = "lado_senal"; break;
                //case 50: _table = "vista_senal"; break;
                case 51: _table = "clasificacion_delincuencial"; break;
                case 52: _table = "clasificacion_criminologica"; break;
                case 53: _table = "fuero"; break;
                case 54: _table = "situacion_juridica"; break;
                case 55: _table = "modelo"; break;
                case 56: _table = "estatus"; break;
                case 126: _table = "actitud"; break;
                case 127: _table = "cavidad_oral"; break;
                case 128: _table = "ojos_abiertos_dedo_dedo"; break;
                case 129: _table = "ojos_abiertos_dedo_nariz"; break;
                case 130: _table = "ojos_cerrados_dedo_dedo"; break;
                case 131: _table = "ojos_cerrados_dedo_nariz"; break;
                case 132: _table = "tipo_alergia"; break;
                case 133: _table = "opcion_comun"; break;
                case 134: _table = "Antecedentes"; break;
                case 135: _table = "Conclusion"; break;
                case 136: _table = "equipo_a_utilizar"; break;
                case 137: _table = "lugar"; break;

            }

            return _table;
        }

    }
}
