﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.Catalogo
{
    public class ServicioCatalogo : ClsAbstractService<Entity.Catalogo>, ICatalogo
    {
        public ServicioCatalogo(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Catalogo> ObtenerTodos(int tipoCatalogo)
        {
            ISelectFactory<int> select = new ObtenerTodos();
            return GetAll(select, new FabricaCatalogo(), tipoCatalogo);
        }

        public List<Entity.Catalogo> ObtenerHabilitados(int tipoCatalogo)
        {
            ISelectFactory<int> select = new ObtenerHabilitados();
            return GetAll(select, new FabricaCatalogo(), tipoCatalogo);
        }
       
        /*
         * parametros: object[] (id, idtipocatalogo)
         */
        public Entity.Catalogo ObtenerPorId(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerById();
            return GetByKey(select, new FabricaCatalogo(), parametros);
        }

        /*
        * parametros: object[] (nombre, idtipocatalogo)
        */
        public Entity.Catalogo ObtenerPorNombre(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByNombre();
            return GetByKey(select, new FabricaCatalogo(), parametros);
        }

        public Entity.Catalogo ObtenerPorTrackingId(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaCatalogo(), parametros);
        }

        public int Guardar(Entity.Catalogo catalogo)
        {
            IInsertFactory<Entity.Catalogo> insert = new Guardar();
            return Insert(insert, catalogo);
        }

        public void Actualizar(Entity.Catalogo catalogo)
        {
            IUpdateFactory<Entity.Catalogo> update = new Actualizar();
            Update(update, catalogo);
        }
    }
}
