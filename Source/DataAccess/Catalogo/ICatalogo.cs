﻿using System;
using System.Collections.Generic;

namespace DataAccess.Catalogo
{
    public interface ICatalogo
    {
        List<Entity.Catalogo> ObtenerTodos(int tipoCatalogo);
        List<Entity.Catalogo> ObtenerHabilitados(int tipoCatalogo);
        Entity.Catalogo ObtenerPorId(object[] parametros);
        Entity.Catalogo ObtenerPorNombre(object[] parametros);
        Entity.Catalogo ObtenerPorTrackingId(object[] parametros);
        int Guardar(Entity.Catalogo catalogo);
        void Actualizar(Entity.Catalogo catalogo);
    }
}
