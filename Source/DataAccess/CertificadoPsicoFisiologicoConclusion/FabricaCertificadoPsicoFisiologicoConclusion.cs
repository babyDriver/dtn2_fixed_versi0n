﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CertificadoPsicoFisiologicoConclusion
{
    class FabricaCertificadoPsicoFisiologicoConclusion : IEntityFactory<Entity.CertificadoPsicoFisilogicoConclusion>
    {
        public CertificadoPsicoFisilogicoConclusion ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoPsicoFisilogicoConclusion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }
            return item;
        }
    }
}
