﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoPsicoFisiologicoConclusion
{
    interface ICertificadoPsicoFisiologicoConclusion
    {
        int Guardar(Entity.CertificadoPsicoFisilogicoConclusion certificadoPsicoFisilogicoOrientacion);
        Entity.CertificadoPsicoFisilogicoConclusion ObtenerPorId(int Id);
    }
}
