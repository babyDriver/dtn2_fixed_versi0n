﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.CertificadoPsicoFisiologicoConclusion
{
    public class ServicioCertificadoPsicoFisiologicoConclusion : ClsAbstractService<Entity.CertificadoPsicoFisilogicoConclusion>, ICertificadoPsicoFisiologicoConclusion
    {
        public ServicioCertificadoPsicoFisiologicoConclusion(string dataBase)
       : base(dataBase)
        {
        }

        public int Guardar(CertificadoPsicoFisilogicoConclusion certificadoPsicoFisilogicoOrientacion)
        {
            IInsertFactory<Entity.CertificadoPsicoFisilogicoConclusion> insert = new Guardar();
            return Insert(insert, certificadoPsicoFisilogicoOrientacion);
        }

        public CertificadoPsicoFisilogicoConclusion ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoPsicoFisiologicoConclusion(), Id);
        }
    }
}
