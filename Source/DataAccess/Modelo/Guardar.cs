﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Modelo
{
    public class Guardar : IInsertFactory<Entity.Modelo>
    {

        public DbCommand ConstructInsertCommand(Database db, Entity.Modelo modelo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Modelo_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_Nombre", DbType.String, modelo.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, modelo.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, modelo.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, modelo.Habilitado);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, modelo.TrackingId);
            db.AddInParameter(cmd, "_MarcaId", DbType.Int32, modelo.marca_vehiculo.Id);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, modelo.Creadopor);

            return cmd;
        }
    }
}
