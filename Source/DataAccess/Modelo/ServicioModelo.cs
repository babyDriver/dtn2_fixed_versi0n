﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;

namespace DataAccess.Modelo
{
    public class ServicioModelo : ClsAbstractService<Entity.Modelo>, IModelo
    {
        public ServicioModelo(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.Modelo ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaModelo(), trackingId);
        }

        public List<Entity.Modelo> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaModelo(), new Entity.NullClass());
        }


        public Entity.Modelo ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaModelo(), Id);
        }

        public List<Entity.Modelo> ObtenerByMarcaId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByMarcaId();
            return GetAll(select, new FabricaModelo(), Id);
        }

        public Entity.Modelo ObtenerByNombreMarca(Entity.Modelo modelo)
        {
           
            ISelectFactory<Entity.Modelo> select = new ObtenerByNombreMarca();
            return GetByKey(select, new FabricaModelo(), modelo);
        }

            public int Guardar(Entity.Modelo modelo)
        {
            IInsertFactory<Entity.Modelo> insert = new Guardar();
            return Insert(insert, modelo);
        }

        public void Actualizar(Entity.Modelo modelo)
        {
            IUpdateFactory<Entity.Modelo> update = new Actualizar();
            Update(update, modelo);
        }
    }
}
