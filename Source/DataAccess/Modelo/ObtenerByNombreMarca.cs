﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Modelo
{
    public class ObtenerByNombreMarca : ISelectFactory<Entity.Modelo>
    {

        public DbCommand ConstructSelectCommand(Database db, Entity.Modelo modelo)
        {

            DbCommand cmd = db.GetStoredProcCommand("Modelo_GetByNombreMarca_SP");
            db.AddInParameter(cmd, "_Nombre", DbType.String, modelo.Nombre);
            db.AddInParameter(cmd, "_MarcaId", DbType.Int32, modelo.marca_vehiculo.Id);
            return cmd;
        }

    
    }
}
