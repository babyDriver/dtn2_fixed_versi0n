﻿using System.Collections.Generic;
using System;

namespace DataAccess.Modelo
{
    public interface IModelo
    {
        Entity.Modelo ObtenerById(int Id);
        List<Entity.Modelo> ObtenerByMarcaId(int Id);
        Entity.Modelo ObtenerByNombreMarca(Entity.Modelo Modelo);
        List<Entity.Modelo> ObtenerTodos();     
        int Guardar(Entity.Modelo modelo);
        void Actualizar(Entity.Modelo modelo);
        Entity.Modelo ObtenerByTrackingId(Guid trackingId);
    }
}
