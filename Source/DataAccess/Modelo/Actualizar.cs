﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Modelo
{
    public class Actualizar : IUpdateFactory<Entity.Modelo>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Modelo modelo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Modelo_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, modelo.Id);
            db.AddInParameter(cmd, "_Nombre", DbType.String, modelo.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, modelo.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, modelo.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, modelo.Habilitado);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, modelo.TrackingId);
            db.AddInParameter(cmd, "_MarcaId", DbType.Int32, modelo.marca_vehiculo.Id);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, modelo.Creadopor);


            return cmd;
        }
    }
}
