﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Modelo
{
    public class ObtenerByMarcaId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Modelo_GetByMarcaId_SP");
            db.AddInParameter(cmd, "_MarcaId", DbType.Int32, id);
            return cmd;
        }

    }
}
