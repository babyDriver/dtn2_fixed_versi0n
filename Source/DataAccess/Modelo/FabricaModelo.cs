﻿
using System;
using System.Data;
using DataAccess.Interfaces;
using DataAccess.Catalogo;


namespace DataAccess.Modelo
{
    public class FabricaModelo : IEntityFactory<Entity.Modelo>
    {
        private static ServicioCatalogo servicioCatalogo = new ServicioCatalogo("SQLConnectionString");

        public Entity.Modelo ConstructEntity(System.Data.IDataReader dr)
        {
            var modelo = new Entity.Modelo();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                modelo.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                modelo.Nombre = dr.GetString(index);
            }


            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                modelo.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                modelo.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                modelo.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                modelo.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("MarcaId");
            if (!dr.IsDBNull(index))
            {
                var parametros = new object[2];
                parametros[0] = dr.GetInt32(index);
                parametros[1] = Entity.TipoDeCatalogo.marca_vehiculo;
                 var tipo= servicioCatalogo.ObtenerPorId(parametros);
                 if (tipo != null)
                    modelo.marca_vehiculo = tipo;
            }


            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                modelo.Creadopor = dr.GetInt32(index);
            }


            return modelo;
        }
    }
}
