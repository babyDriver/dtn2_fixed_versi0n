﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Detenido
{
    public class ServicioDetenido : ClsAbstractService<Entity.Detenido>, IDetenido
    {
        public ServicioDetenido(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Detenido> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaInterno(), new Entity.NullClass());
        }

   

        public Entity.Detenido ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaInterno(), userId);
        }

        public Entity.Detenido ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaInterno(), Id);
        }

        public int Guardar(Entity.Detenido interno)
        {
            IInsertFactory<Entity.Detenido> insert = new Guardar();
            return Insert(insert, interno);
        }

        public void Actualizar(Entity.Detenido interno)
        {
            IUpdateFactory<Entity.Detenido> update = new Actualizar();
            Update(update, interno);
        }

        public List<Entity.Detenido> ObtenerByNombre(string nombre)
        {
            ISelectFactory<string> select = new ObtenerByNombre();
            return GetAll(select, new FabricaInterno(), nombre);
        }

    }
}