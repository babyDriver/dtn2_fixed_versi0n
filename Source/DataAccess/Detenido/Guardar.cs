﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Detenido
{
    public class Guardar : IInsertFactory<Entity.Detenido>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Detenido interno)
        {
            //chequetere
            DbCommand cmd = db.GetStoredProcCommand("Detenido_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, interno.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, interno.Nombre);
            db.AddInParameter(cmd, "_Paterno", DbType.String, interno.Paterno);
            db.AddInParameter(cmd, "_Materno", DbType.String, interno.Materno);
            db.AddInParameter(cmd, "_RutaImagen", DbType.String, interno.RutaImagen);
            db.AddInParameter(cmd, "_DomicilioId", DbType.String, interno.DomicilioId);
            db.AddInParameter(cmd, "_DomicilioNId", DbType.Int32, interno.DomicilioNId);
            db.AddInParameter(cmd, "_Detenidoanioregistro", DbType.Int32, interno.Detenidoanioregistro);
            db.AddInParameter(cmd, "_Detenidomesregistro", DbType.Int32, interno.Detenidomesregistro);



            return cmd;
        }
    }
}
