﻿using DataAccess.Interfaces;


namespace DataAccess.Detenido
{
    public class FabricaInterno : IEntityFactory<Entity.Detenido>
    {
       

        public Entity.Detenido ConstructEntity(System.Data.IDataReader dr)
        {
            var interno = new Entity.Detenido();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                interno.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                interno.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("DomicilioId");
            if (!dr.IsDBNull(index))
            {
                interno.DomicilioId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DomicilioNId");
            if (!dr.IsDBNull(index))
            {
                interno.DomicilioNId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                interno.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                interno.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                interno.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("RutaImagen");
            if (!dr.IsDBNull(index))
            {
                interno.RutaImagen = dr.GetString(index);
            }
            index = dr.GetOrdinal("Detenidoanioregistro");
            if (!dr.IsDBNull(index))
            {
                interno.Detenidoanioregistro = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Detenidomesregistro");
            if (!dr.IsDBNull(index))
            {
                interno.Detenidomesregistro = dr.GetInt32(index);
            }






            return interno;
        }
    }
}