﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Detenido
{
    public class Actualizar : IUpdateFactory<Entity.Detenido>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Detenido interno)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, interno.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, interno.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, interno.Nombre);
            db.AddInParameter(cmd, "_Paterno", DbType.String, interno.Paterno);
            db.AddInParameter(cmd, "_Materno", DbType.String, interno.Materno);
            db.AddInParameter(cmd, "_DomicilioId", DbType.Int32, interno.DomicilioId);
            db.AddInParameter(cmd, "_DomicilioNId", DbType.Int32, interno.DomicilioNId);
            db.AddInParameter(cmd, "_RutaImagen", DbType.String, interno.RutaImagen);
            db.AddInParameter(cmd, "_Detenidoanioregistro", DbType.Int32, interno.Detenidoanioregistro);
            db.AddInParameter(cmd, "_Detenidomesregistro", DbType.Int32, interno.Detenidomesregistro);

            return cmd;
        }
    }
}
