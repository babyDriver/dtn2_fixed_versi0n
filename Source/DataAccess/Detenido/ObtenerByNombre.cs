﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Detenido
{
    public class ObtenerByNombre : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_GetByNombre_SP");
            db.AddInParameter(cmd, "_Nombre", DbType.String, nombre);
            return cmd;
        }
    }
}
