﻿using System;
using System.Collections.Generic;

namespace DataAccess.Detenido
{
    public interface IDetenido
    {
        List<Entity.Detenido> ObtenerTodos();
        Entity.Detenido ObtenerById(int Id);
        Entity.Detenido ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Detenido interno);
        void Actualizar(Entity.Detenido interno);
        List<Entity.Detenido> ObtenerByNombre(string nombre);
    }
}