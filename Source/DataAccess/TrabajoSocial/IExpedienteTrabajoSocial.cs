﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.TrabajoSocial
{
    public interface IExpedienteTrabajoSocial
    {
        int Guardar(Entity.ExpedienteTrabajoSocial expedienteTrabajoSocial);
        void Actualizar(Entity.ExpedienteTrabajoSocial expedienteTrabajoSocial);


    }
}
