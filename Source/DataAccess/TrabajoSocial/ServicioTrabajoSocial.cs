﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.TrabajoSocial
{
    public class ServicioTrabajoSocial : ClsAbstractService<Entity.TrabajoSocial>, ITrabajoSocial
    {
        public ServicioTrabajoSocial(string dataBase)
          : base(dataBase)
        {
        }
        public int guardarTs(Entity.TrabajoSocial trabajoSocial)
        {
            IInsertFactory<Entity.TrabajoSocial> insert = new GuardarTS();
            return Insert(insert, trabajoSocial);
        
        }

        public void SalidaTs(Entity.TrabajoSocial trabajosocial)
        {
            IUpdateFactory<Entity.TrabajoSocial> update = new SalidaTrabajoSocial();
             Update(update, trabajosocial);
        }

        public Entity.TrabajoSocial ObtenerByDetenidoId(int id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaTrabajoSocial(), id);
        }
    }
}
