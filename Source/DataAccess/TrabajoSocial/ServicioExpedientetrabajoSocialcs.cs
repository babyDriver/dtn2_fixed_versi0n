﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.TrabajoSocial
{
    public class ServicioExpedientetrabajoSocialcs : ClsAbstractService<Entity.ExpedienteTrabajoSocial>, IExpedienteTrabajoSocial
    {
        public void Actualizar(ExpedienteTrabajoSocial expedienteTrabajoSocial)
        {
            IUpdateFactory<Entity.ExpedienteTrabajoSocial> update = new Actualizar();
            Update(update, expedienteTrabajoSocial);
        }

        public int Guardar(Entity.ExpedienteTrabajoSocial expedienteTrabajoSocial)
        {
            IInsertFactory<Entity.ExpedienteTrabajoSocial> insert = new Guardar();
            return Insert(insert, expedienteTrabajoSocial);
        }
       
    }
}
