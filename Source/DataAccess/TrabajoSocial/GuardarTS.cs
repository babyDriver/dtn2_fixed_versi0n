﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.TrabajoSocial
{
    public class GuardarTS : IInsertFactory<Entity.TrabajoSocial>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.TrabajoSocial trabajoSocial)
        {
            DbCommand cmd = db.GetStoredProcCommand("trabajosocial_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, trabajoSocial.TrackingId);
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, trabajoSocial.DetalledetencionId);
            db.AddInParameter(cmd, "_Programa", DbType.String, trabajoSocial.Programa);
            db.AddInParameter(cmd, "_PorcentajeHoras", DbType.Decimal, trabajoSocial.PorcentajeHoras);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, trabajoSocial.FechaHora);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, trabajoSocial.Creadopor);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, trabajoSocial.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, trabajoSocial.Habilitado);

            return cmd;


        }
    }
}
