﻿using DataAccess.Interfaces;

namespace DataAccess.TrabajoSocial
{
    public class FabricaTrabajoSocial : IEntityFactory<Entity.TrabajoSocial>
    {
        public Entity.TrabajoSocial ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.TrabajoSocial();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetString(index);
            }


            index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalledetencionId = dr.GetInt32(index);
            }

            //index = dr.GetOrdinal("Programa");
            //if (!dr.IsDBNull(index))
            //{
            //    item.Programa = dr.GetString(index);
            //}

            //index = dr.GetOrdinal("PorcetajeHoras");
            //if (!dr.IsDBNull(index))
            //{
            //    item.PorcentajeHoras = dr.GetDecimal(index);
            //}

            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }

            return item;
        }
    }
}
