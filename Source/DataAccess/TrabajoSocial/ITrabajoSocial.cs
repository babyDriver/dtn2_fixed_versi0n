﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.TrabajoSocial
{
    public interface ITrabajoSocial
    {
        int guardarTs(Entity.TrabajoSocial trabajoSocial);
        void SalidaTs(Entity.TrabajoSocial trabajosocial);
        Entity.TrabajoSocial ObtenerByDetenidoId(int id);
    }
}
