﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.TrabajoSocial
{
    public class Actualizar : IUpdateFactory<Entity.ExpedienteTrabajoSocial>
    {
        public DbCommand ConstructUpdateCommand(Database db, ExpedienteTrabajoSocial expediente)
        {
            DbCommand cmd = db.GetStoredProcCommand("expediente_trabajo_social_update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, expediente.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, expediente.TrackingId);
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, expediente.DetalledetencionId);
            db.AddInParameter(cmd, "_MotivorehabilitacionId", DbType.Int32, expediente.MotivorehabilitacionId);
            db.AddInParameter(cmd, "_AdiccionId", DbType.Int32, expediente.AdiccionId);
            db.AddInParameter(cmd, "_Pandilla", DbType.String, expediente.Pandilla);
            db.AddInParameter(cmd, "_ReligionId", DbType.Int32, expediente.ReligionId);
            db.AddInParameter(cmd, "_Cuadropatologico", DbType.String, expediente.Cuadropatologico);
            db.AddInParameter(cmd, "_Observacion", DbType.String, expediente.Observacion);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, expediente.Creadopor);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, expediente.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, expediente.Habilitado);
            db.AddInParameter(cmd, "_Nombremadre", DbType.String, expediente.Nombremadre);
            db.AddInParameter(cmd, "_Celularmadre", DbType.String, expediente.Celularmadre);
            db.AddInParameter(cmd, "_Nombrepadre", DbType.String, expediente.Nombrepadre);
            db.AddInParameter(cmd, "_Celularpadre", DbType.String, expediente.Celularpadre);
            db.AddInParameter(cmd, "_Tutor", DbType.String, expediente.Tutor);
            db.AddInParameter(cmd, "_Celulartutor", DbType.String, expediente.Celulartutor);
            db.AddInParameter(cmd, "_EscolaridadId", DbType.Int32, expediente.EscolaridadId);
            db.AddInParameter(cmd, "_Notificado", DbType.String, expediente.Notificado);
            db.AddInParameter(cmd, "_Celularnotificado", DbType.String, expediente.Celularnotificado);
            db.AddInParameter(cmd, "_Domiciliomadre", DbType.String, expediente.Domiciliomadre);
            db.AddInParameter(cmd, "_Domiciliopadre", DbType.String, expediente.Domiciliopadre);

            return cmd;
        }
    }
}
