﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.TrabajoSocial
{
    public class SalidaTrabajoSocial : IUpdateFactory<Entity.TrabajoSocial>

    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.TrabajoSocial trabajoSocial)
        {
            DbCommand cmd = db.GetStoredProcCommand("trabajosocial_UpdateSalida_SP");
        
            db.AddInParameter(cmd, "_Id", DbType.Int32, trabajoSocial.Id);
            db.AddInParameter(cmd, "_Activo", DbType.Int32,  0);
            return cmd;
        }
    }
}
