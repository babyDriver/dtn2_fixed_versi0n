﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.TipoReporte
{
    public class FabricaTipoReporte : IEntityFactory<Entity.Tiporeporte>
    {
        public Tiporeporte ConstructEntity(IDataReader dr)
        {
            var item = new Entity.Tiporeporte();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Reporte");
            if (!dr.IsDBNull(index))
            {
                item.Reporte = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("PantallaId");
            if (!dr.IsDBNull(index))
            {
                item.PantallaId = dr.GetInt32(index);
            }

            return item;

        }
    }
}
