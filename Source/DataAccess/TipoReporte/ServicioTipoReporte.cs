﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;
namespace DataAccess.TipoReporte
{
    public class ServicioTipoReporte : ClsAbstractService<Entity.Tiporeporte>, ITipoReporte
    {
        public ServicioTipoReporte(string dataBase)
        : base(dataBase)
        {
        }
        public List<Tiporeporte> GetTiporeportes()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaTipoReporte(), new Entity.NullClass());
        }
    }
}
