﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.CalificacionIngreso
{
    public class ObtenerFolio : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("CalificacionIngreso_GetFolio_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(parametros[0]));
            db.AddInParameter(cmd, "_Tipo", DbType.String, parametros[1].ToString());

            return cmd;
        }
    }
}
