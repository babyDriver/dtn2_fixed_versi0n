﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.CalificacionIngreso
{
    public class ServicioCalificacionIngreso : ClsAbstractService<Entity.CalificacionIngreso>, ICalificacionIngreso
    {
        public ServicioCalificacionIngreso(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.CalificacionIngreso item)
        {
            IInsertFactory<Entity.CalificacionIngreso> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.CalificacionIngreso ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaCalificacionIngreso(), trackingId);
        }

        public Entity.CalificacionIngreso ObtenerByCalificacionId(int id)
        {
            ISelectFactory<int> select = new ObtenerByCalificacionId();
            return GetByKey(select, new FabricaCalificacionIngreso(), id);
        }

        public Entity.CalificacionIngreso ObtenerFolio(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerFolio();
            return GetByKey(select, new FabricaCalificacionIngreso(), parametros);
        }

        public void Actualizar(Entity.CalificacionIngreso item)
        {
            IUpdateFactory<Entity.CalificacionIngreso> update = new Actualizar();
             Update(update, item);
        }
    }
}
