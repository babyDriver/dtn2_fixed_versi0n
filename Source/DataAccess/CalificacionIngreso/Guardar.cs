﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.CalificacionIngreso
{
    public class Guardar : IInsertFactory<Entity.CalificacionIngreso>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CalificacionIngreso item)
        {
            DbCommand cmd = db.GetStoredProcCommand("CalificacionIngreso_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId.ToString());
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_CalificacionId", DbType.Int32, item.CalificacionId);
            db.AddInParameter(cmd, "_IngresoId", DbType.Int32, item.IngresoId);            
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_Folio", DbType.Int32, item.Folio);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);

            return cmd;
        }
    }
}
