﻿using DataAccess.Interfaces;

namespace DataAccess.CalificacionIngreso
{
    public class FabricaCalificacionIngreso : IEntityFactory<Entity.CalificacionIngreso>
    {
        public Entity.CalificacionIngreso ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.CalificacionIngreso();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("CalificacionId");
            if (!dr.IsDBNull(index))
            {
                item.CalificacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IngresoId");
            if (!dr.IsDBNull(index))
            {
                item.IngresoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                item.Tipo = dr.GetString(index);
            }

            return item;
        }
    }
}
