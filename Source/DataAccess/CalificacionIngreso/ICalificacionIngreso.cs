﻿using System;
using System.Collections.Generic;

namespace DataAccess.CalificacionIngreso
{
    public interface ICalificacionIngreso
    {
        int Guardar(Entity.CalificacionIngreso item);
        Entity.CalificacionIngreso ObtenerByTrackingId(Guid guid);
        Entity.CalificacionIngreso ObtenerByCalificacionId(int id);
        Entity.CalificacionIngreso ObtenerFolio(object[] parametros);
        void Actualizar(Entity.CalificacionIngreso item);
    }
}
