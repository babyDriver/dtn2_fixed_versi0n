﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Estado
{
    public class FabricaEstado : IEntityFactory<Entity.Estado>
    {
        public Entity.Estado ConstructEntity(IDataReader dr)
        {
            var estado = new Entity.Estado();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                estado.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                estado.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                estado.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                estado.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Clave");
            if (!dr.IsDBNull(index))
            {
                estado.Clave = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                estado.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                estado.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("PaisId");
            if (!dr.IsDBNull(index))
            {
                estado.IdPais = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                estado.CreadoPor = dr.GetInt32(index);
            }

            return estado;
        }
    }
}
