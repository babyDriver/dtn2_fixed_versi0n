﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.Estado
{
    class Actualizar : IUpdateFactory<Entity.Estado>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Estado estado)
        {
            DbCommand cmd = db.GetStoredProcCommand("Estado_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, estado.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, estado.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, estado.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, estado.Descripcion);
            db.AddInParameter(cmd, "_Clave", DbType.String, estado.Clave);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, estado.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, estado.Habilitado);
            db.AddInParameter(cmd, "_PaisId", DbType.Int32, estado.IdPais);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, estado.CreadoPor);


            return cmd;
        }
    }
}
