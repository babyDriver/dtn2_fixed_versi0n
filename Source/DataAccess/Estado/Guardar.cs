﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;


namespace DataAccess.Estado
{
    public class Guardar : IInsertFactory<Entity.Estado>

    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Estado estado)
        {
            DbCommand cmd = db.GetStoredProcCommand("Estado_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, estado.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, estado.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, estado.Descripcion);
            db.AddInParameter(cmd, "_Clave", DbType.String, estado.Clave);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, estado.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, estado.Habilitado);
            db.AddInParameter(cmd, "_PaisId", DbType.Int32, estado.IdPais);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, estado.CreadoPor);


            return cmd;
        }
    }
}
