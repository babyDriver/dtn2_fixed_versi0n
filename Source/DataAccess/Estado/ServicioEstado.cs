﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entity;

namespace DataAccess.Estado
{
    public class ServicioEstado : ClsAbstractService<Entity.Estado>, IEstado
    {
        public ServicioEstado(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Estado> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaEstado(), new Entity.NullClass());
        }

        public Entity.Estado ObtenerPorId(int estadoId)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaEstado(), estadoId);
        }

        public List<Entity.Estado> ObtenerPorPaisId(int paisId)
        {
            ISelectFactory<int> select = new ObtenerByPais();
            return GetAll(select, new FabricaEstado(), paisId);
        }

        public Entity.Estado ObtenerPorTrackingId(System.Guid trackingId)
        {
            ISelectFactory<System.Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaEstado(), trackingId);
        }

        public int Guardar(Entity.Estado estado)
        {
            IInsertFactory<Entity.Estado> insert = new Guardar();
            return Insert(insert, estado);
        }

        public void Actualizar(Entity.Estado estado)
        {
            IUpdateFactory<Entity.Estado> update = new Actualizar();
            Update(update, estado);
        }
    }
}
