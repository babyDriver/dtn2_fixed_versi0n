﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Estado
{
    public class ObtenerByTrackingId : ISelectFactory<System.Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, System.Guid trackingId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Estado_GetByTrackingId_SP");
            db.AddInParameter(cmd, "TrackingId", DbType.Guid, trackingId);
            return cmd;
        }
    }
}