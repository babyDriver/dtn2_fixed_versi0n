﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Estado
{
    public class ObtenerByPais : ISelectFactory<int>
    {

        public DbCommand ConstructSelectCommand(Database db, int paisId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Estado_GetByPais_SP");
            db.AddInParameter(cmd, "_PaisId", DbType.Int32, paisId);
            return cmd;
        }

    }
}
