﻿using System;
using System.Collections.Generic;

namespace DataAccess.Estado
{
    public interface IEstado
    {
        List<Entity.Estado> ObtenerTodos();
        Entity.Estado ObtenerPorId(int estadoId);
        List<Entity.Estado> ObtenerPorPaisId(int paisId);
        Entity.Estado ObtenerPorTrackingId(System.Guid trackingId);
        int Guardar(Entity.Estado estado);
        void Actualizar(Entity.Estado estado);

    }
}
