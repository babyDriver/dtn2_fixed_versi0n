﻿using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.Interfaces
{
    public interface ISelectFactory<TIdentity>
    {
        DbCommand ConstructSelectCommand(Database db, TIdentity identity);
    }
}
