﻿using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.Interfaces
{
    public interface IUpdateFactory<TEntity>
    {
        DbCommand ConstructUpdateCommand(Database db, TEntity entity);  
    }
}
