﻿using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.Interfaces
{
    public interface IDeleteFactory<TIdentity>
    {
        DbCommand ConstructDeleteCommand(Database db, TIdentity identity);
    }
}