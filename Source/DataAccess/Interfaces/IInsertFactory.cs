﻿using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.Interfaces
{
    public interface IInsertFactory<TEntity>
    {
        DbCommand ConstructInsertCommand(Database db, TEntity entity);   
    }
}
