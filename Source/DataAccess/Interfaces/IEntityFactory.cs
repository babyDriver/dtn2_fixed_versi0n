﻿using System.Data;

namespace DataAccess.Interfaces
{
    public interface IEntityFactory<TEntity>
    {
        TEntity ConstructEntity(IDataReader dr);
    }
}