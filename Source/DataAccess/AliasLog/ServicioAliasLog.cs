﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.AliasLog
{
    public class ServicioAliasLog : ClsAbstractService<Entity.AliasLog>, IAliasLog
    {
        public ServicioAliasLog(string dataBase)
               : base(dataBase)
        {
        }
        public void Actualizar(Entity.AliasLog item)
        {
            IUpdateFactory<Entity.AliasLog> update = new Actualizar();
            Update(update, item);
        }

        public Entity.AliasLog Obtener(Entity.AliasLog filtro)
        {
            ISelectFactory<Entity.AliasLog> select = new ObtenerAliasLog();
            return GetByKey(select, new FabricaAliasLog(), filtro);
        }
    }
}
