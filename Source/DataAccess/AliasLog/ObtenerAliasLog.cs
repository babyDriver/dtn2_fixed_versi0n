﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.AliasLog
{
    public class ObtenerAliasLog : ISelectFactory<Entity.AliasLog>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.AliasLog filtro)
        {
            DbCommand cmd = db.GetStoredProcCommand("AliasLog_Get_SP");

            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, filtro.DetenidoId);

            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, filtro.Fecha);

            return cmd;
        }
    }
}
