﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.AliasLog
{
    public class FabricaAliasLog : IEntityFactory<Entity.AliasLog>
    {
        public Entity.AliasLog ConstructEntity(IDataReader dr)
        {
            var movimiento = new Entity.AliasLog();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                movimiento.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                movimiento.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                movimiento.Fecha = dr.GetDateTime(index);
            }

            return movimiento;
            ;
        }
    }
}
