﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.AliasLog
{
    public interface IAliasLog
    {
        void Actualizar(Entity.AliasLog item);
        Entity.AliasLog Obtener(Entity.AliasLog filtro);
    }
}
