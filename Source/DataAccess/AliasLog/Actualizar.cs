﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;


namespace DataAccess.AliasLog
{
    public class Actualizar : IUpdateFactory<Entity.AliasLog>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.AliasLog filtro)
        {
            DbCommand cmd = db.GetStoredProcCommand("AliasLog_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, filtro.Id);

            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, filtro.DetenidoId);

            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, filtro.Fecha);
            db.AddInParameter(cmd, "_FechaAnt", DbType.DateTime, filtro.FechaAnt);

            return cmd;
        }
    }
}
