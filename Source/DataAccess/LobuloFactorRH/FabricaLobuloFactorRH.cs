﻿using DataAccess.Interfaces;


namespace DataAccess.LobuloFactorRH
{
    public class FabricaLobuloFactorRH : IEntityFactory<Entity.LobuloFactorRH>
    {
       

        public Entity.LobuloFactorRH ConstructEntity(System.Data.IDataReader dr)
        {
            var lobulo = new Entity.LobuloFactorRH();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                lobulo.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                lobulo.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("AdherenciaL");
            if (!dr.IsDBNull(index))
            {
                lobulo.AdherenciaL = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DimensionL");
            if (!dr.IsDBNull(index))
            {
                lobulo.DimensionL = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ParticularidadL");
            if (!dr.IsDBNull(index))
            {
                lobulo.ParticularidadL = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TragoL");
            if (!dr.IsDBNull(index))
            {
                lobulo.TragoL = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AntitragoL");
            if (!dr.IsDBNull(index))
            {
                lobulo.AntitragoL = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("SangreF");
            if (!dr.IsDBNull(index))
            {
                lobulo.SangreF = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TipoF");
            if (!dr.IsDBNull(index))
            {
                lobulo.TipoF = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AntropometriaId");
            if (!dr.IsDBNull(index))
            {
                lobulo.AntropometriaId = dr.GetInt32(index);
            }

            return lobulo;
        }
    }
}