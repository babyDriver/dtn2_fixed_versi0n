﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.LobuloFactorRH
{
    public class Actualizar : IUpdateFactory<Entity.LobuloFactorRH>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.LobuloFactorRH lobulo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Lobulo_FactorRH_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, lobulo.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, lobulo.TrackingId);
            db.AddInParameter(cmd, "_ParticularidadL", DbType.Int32, lobulo.ParticularidadL);
            db.AddInParameter(cmd, "_TragoL", DbType.Int32, lobulo.TragoL);
            db.AddInParameter(cmd, "_AntitragoL", DbType.Int32, lobulo.AntitragoL);
            db.AddInParameter(cmd, "_SangreF", DbType.Int32, lobulo.SangreF);
            db.AddInParameter(cmd, "_TipoF", DbType.Int32, lobulo.TipoF);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, lobulo.AntropometriaId);

            if (lobulo.AdherenciaL != null)
                db.AddInParameter(cmd, "_AdherenciaL", DbType.Int32, lobulo.AdherenciaL);
            else
                db.AddInParameter(cmd, "_AdherenciaL", DbType.Int32, null);

            if (lobulo.DimensionL != null)
                db.AddInParameter(cmd, "_DimensionL", DbType.Int32, lobulo.DimensionL);
            else
                db.AddInParameter(cmd, "_DimensionL", DbType.Int32, null);


            return cmd;
        }
    }
}
