﻿using System;
using System.Collections.Generic;

namespace DataAccess.LobuloFactorRH
{
    public interface ILobuloFactorRH
    {
        List<Entity.LobuloFactorRH> ObtenerTodos();
        Entity.LobuloFactorRH ObtenerById(int Id);
        Entity.LobuloFactorRH ObtenerByAntropometriaId(int Id);
        Entity.LobuloFactorRH ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.LobuloFactorRH lobulo);
        void Actualizar(Entity.LobuloFactorRH lobulo);
    }
}