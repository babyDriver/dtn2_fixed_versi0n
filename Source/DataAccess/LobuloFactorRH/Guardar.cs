﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.LobuloFactorRH
{
    public class Guardar : IInsertFactory<Entity.LobuloFactorRH>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.LobuloFactorRH lobulo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Lobulo_FactorRH_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, lobulo.TrackingId);
            db.AddInParameter(cmd, "_AdherenciaL", DbType.Int32, lobulo.AdherenciaL);
            db.AddInParameter(cmd, "_DimensionL", DbType.Int32, lobulo.DimensionL);
            db.AddInParameter(cmd, "_ParticularidadL", DbType.Int32, lobulo.ParticularidadL);
            db.AddInParameter(cmd, "_TragoL", DbType.Int32, lobulo.TragoL);
            db.AddInParameter(cmd, "_AntitragoL", DbType.Int32, lobulo.AntitragoL);
            db.AddInParameter(cmd, "_SangreF", DbType.Int32, lobulo.SangreF);
            db.AddInParameter(cmd, "_TipoF", DbType.Int32, lobulo.TipoF);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, lobulo.AntropometriaId);


            return cmd;
        }
    }
}
