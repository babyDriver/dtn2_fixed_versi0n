﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.LobuloFactorRH
{
    public class ObtenerByAntropometriaId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Lobulo_FactorRH_GetByAntropometriaId_SP");
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, id);
            return cmd;
        }
    }
}
