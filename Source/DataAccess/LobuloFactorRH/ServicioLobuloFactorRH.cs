﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.LobuloFactorRH
{
    public class ServicioLobuloFactorRH : ClsAbstractService<Entity.LobuloFactorRH>, ILobuloFactorRH
    {
        public ServicioLobuloFactorRH(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.LobuloFactorRH> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaLobuloFactorRH(), new Entity.NullClass());
        }

   

        public Entity.LobuloFactorRH ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaLobuloFactorRH(), userId);
        }

        public Entity.LobuloFactorRH ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaLobuloFactorRH(), Id);
        }

        public Entity.LobuloFactorRH ObtenerByAntropometriaId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByAntropometriaId();
            return GetByKey(select, new FabricaLobuloFactorRH(), Id);
        }

        public int Guardar(Entity.LobuloFactorRH general)
        {
            IInsertFactory<Entity.LobuloFactorRH> insert = new Guardar();
            return Insert(insert, general);
        }

        public void Actualizar(Entity.LobuloFactorRH boca)
        {
            IUpdateFactory<Entity.LobuloFactorRH> update = new Actualizar();
            Update(update, boca);
        }


    }
}