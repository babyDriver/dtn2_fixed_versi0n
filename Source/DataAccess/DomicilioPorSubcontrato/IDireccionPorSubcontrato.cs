﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DomicilioPorSubcontrato
{
    public interface IDireccionPorSubcontrato
    {
        Entity.DomicilioPorSubcontrato GetById(int Id);
    }
}
