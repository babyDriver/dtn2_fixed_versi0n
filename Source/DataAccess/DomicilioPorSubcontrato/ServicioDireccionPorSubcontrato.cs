﻿using DataAccess.Interfaces;
using DataAccess.DomicilioPorSubcontrato;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DomicilioPorSubcontrato
{
    public class ServicioDireccionPorSubcontrato : ClsAbstractService<Entity.DomicilioPorSubcontrato>, IDireccionPorSubcontrato
    {
        public ServicioDireccionPorSubcontrato(string dataBase)
            :base(dataBase)
        {

        }
        public Entity.DomicilioPorSubcontrato GetById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select,new FabricaDireccionPorSubcontrato(), Id);
        }
    }
}
