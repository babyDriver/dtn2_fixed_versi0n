﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DomicilioPorSubcontrato
{
    class FabricaDireccionPorSubcontrato : IEntityFactory<Entity.DomicilioPorSubcontrato>
    {
        public Entity.DomicilioPorSubcontrato ConstructEntity(IDataReader dr)
        {
            var item = new Entity.DomicilioPorSubcontrato();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("MunicipioId");
            if (!dr.IsDBNull(index))
            {
                item.MunicipioId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("EstadoId");
            if (!dr.IsDBNull(index))
            {
                item.EstadoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("PaisId");
            if (!dr.IsDBNull(index))
            {
                item.PaisId = dr.GetInt32(index);
            }
            return item;
        }
    }
}
