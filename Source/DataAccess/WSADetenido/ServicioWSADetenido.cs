﻿using DataAccess.Interfaces;

namespace DataAccess.WSADetenido
{
    public class ServicioWSADetenido : ClsAbstractService<Entity.WSADetenido>, IWSADetenido
    {
        public ServicioWSADetenido(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.WSADetenido item)
         {
            IInsertFactory<Entity.WSADetenido> insert = new Guardar();
            return Insert(insert, item);
        }
    }
}
