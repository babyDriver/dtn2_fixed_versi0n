﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.WSADetenido
{
    public class Guardar : IInsertFactory<Entity.WSADetenido>
    {
        // ataj0
        public DbCommand ConstructInsertCommand(Database db, Entity.WSADetenido item)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSADetenido_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.IdentCAD);
            //db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, item.EventoId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, item.Nombre);
            db.AddInParameter(cmd, "_Paterno", DbType.String, item.Paterno);
            db.AddInParameter(cmd, "_Materno", DbType.String, item.Materno);
            db.AddInParameter(cmd, "_Alias", DbType.String, item.Alias);
            //db.AddInParameter(cmd, "_FechaCaptura", DbType.DateTime, item.FechaCaptura);
            //db.AddInParameter(cmd, "_FechaNac", DbType.DateTime, item.FechaNac);
            db.AddInParameter(cmd, "_Edad", DbType.Int32, item.Edad);
            db.AddInParameter(cmd, "_Sexo", DbType.Int32, item.Sexo);
            db.AddInParameter(cmd, "_Telefono", DbType.Int32, 0);
            db.AddInParameter(cmd, "_Nacionalidad", DbType.Int32, 38);
            db.AddInParameter(cmd, "_Papeles", DbType.Int32, item.IdentCAD);
            db.AddInParameter(cmd, "_FechaCreacion", DbType.DateTime, DateTime.Now);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, 1);

            return cmd;
        }
    }
}
