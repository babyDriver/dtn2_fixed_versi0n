﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.CertificadoLesionTipoLesion
{
    public class ObtenerPorId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionAntecedentesDetalle_ById_SP");
            db.AddInParameter(cmd, "_certificado_LesionAlergiaId", DbType.Int32, Id);
            
            return cmd;
        }
    }
}
