﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoLesionTipoLesion
{
    public class Guardar : IInsertFactory<Entity.CertificadoLesionTipoLesion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CertificadoLesionTipoLesion entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_LesionTatuajes_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, entity.FechaHora);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, entity.Creadopor);
            return cmd;
        }
    }
}
