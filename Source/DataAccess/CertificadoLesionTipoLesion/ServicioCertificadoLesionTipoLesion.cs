﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.CertificadoLesionTipoLesion
{
    public class ServicioCertificadoLesionTipoLesion : ClsAbstractService<Entity.CertificadoLesionTipoLesion>, ICertificadoLesionTipoLesion
    {
        public ServicioCertificadoLesionTipoLesion(string dataBase)
      : base(dataBase)
        {
        }
        public int Guardar(Entity.CertificadoLesionTipoLesion certificadoLesionTipoLesion)
        {
            IInsertFactory<Entity.CertificadoLesionTipoLesion> insert = new Guardar();
            return Insert(insert, certificadoLesionTipoLesion);
        }

        public Entity.CertificadoLesionTipoLesion ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoLesionTipoLesion(), Id);
        }
    }
}
