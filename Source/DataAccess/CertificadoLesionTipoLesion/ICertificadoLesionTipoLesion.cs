﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionTipoLesion
{
    public interface ICertificadoLesionTipoLesion
    {
        int Guardar(Entity.CertificadoLesionTipoLesion certificadoLesionTipoLesion);
        Entity.CertificadoLesionTipoLesion ObtenerPorId(int Id);
    }
}
