﻿using System;
using System.Collections.Generic;

namespace DataAccess.EventoUnidadResponsable
{
    public interface IEventoUnidadResponsable
    {
        void Guardar(Entity.EventoUnidadResponsable item);
        Entity.EventoUnidadResponsable ObtenerByEvendoId(int EventoId);
        List<Entity.EventoUnidadResponsable> ObtenerTodosByEvendoId(int EventoId);
    }
}
