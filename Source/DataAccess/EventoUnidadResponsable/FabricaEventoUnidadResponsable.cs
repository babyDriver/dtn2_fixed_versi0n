﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.EventoUnidadResponsable
{
    public class FabricaEventoUnidadResponsable : IEntityFactory<Entity.EventoUnidadResponsable>
    {
        public Entity.EventoUnidadResponsable ConstructEntity(IDataReader dr)
        {
            var item = new Entity.EventoUnidadResponsable();

            var index = dr.GetOrdinal("EventoId");
            if (!dr.IsDBNull(index))
            {
                item.EventoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("UnidadId");
            if (!dr.IsDBNull(index))
            {
                item.UnidadId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ResponsableId");
            if (!dr.IsDBNull(index))
            {
                item.ResponsableId = dr.GetInt32(index);
            }

            return item;
        }
    }
}
