﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.EventoUnidadResponsable
{
    public class Guardar : IInsertFactory<Entity.EventoUnidadResponsable>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.EventoUnidadResponsable item)
        {
            DbCommand cmd = db.GetStoredProcCommand("EventoUnidadResponsable_Insert_SP");
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, item.EventoId);
            db.AddInParameter(cmd, "_UnidadId", DbType.Int32, item.UnidadId);
            db.AddInParameter(cmd, "_ResponsableId", DbType.Int32, item.ResponsableId);
            
            return cmd;
        }
    }
}
