﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.EventoUnidadResponsable
{
    public class ObtenerTodosPorEventoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("EventoUnidadResponsable_GetByEventoId_SP");
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, identity);

            return cmd;
        }
    }
}
