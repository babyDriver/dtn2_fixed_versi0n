﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.EventoUnidadResponsable
{
    public class ServicioEventoUnidadResponsable : ClsAbstractService<Entity.EventoUnidadResponsable>, IEventoUnidadResponsable
    {
        public ServicioEventoUnidadResponsable(string dataBase)
            : base(dataBase)
        {
        }

        public void Guardar(Entity.EventoUnidadResponsable item)
        {
            IInsertFactory<Entity.EventoUnidadResponsable> insert = new Guardar();
            Insert(insert, item);
        }

        public Entity.EventoUnidadResponsable ObtenerByEvendoId(int EventoId)
        {
            ISelectFactory<int> select = new ObtenerPorEventoId();
            return GetByKey(select, new FabricaEventoUnidadResponsable(), EventoId);
        }

        public List<Entity.EventoUnidadResponsable> ObtenerTodosByEvendoId(int EventoId)
        {
            ISelectFactory<int> select = new ObtenerTodosPorEventoId();
            return GetAll(select, new FabricaEventoUnidadResponsable(), EventoId);
        }
    }
}
