﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ParametroContrato
{
    class FabricaParametroContrato : IEntityFactory<Entity.ParametroContrato>
    {
        public Entity.ParametroContrato ConstructEntity(IDataReader dr)
        {
            var parametro = new Entity.ParametroContrato();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                parametro.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                parametro.TrackingId = dr.GetString(index);
            }
            index = dr.GetOrdinal("Parametroid");
            if (!dr.IsDBNull(index))
            {
                parametro.Parametroid = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                parametro.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                parametro.FechaHora = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Valor");
            if (!dr.IsDBNull(index))
            {
                parametro.Valor = dr.GetString(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                parametro.CreadoPor = dr.GetInt32(index);
            }

            return parametro;
        }
    }
}
