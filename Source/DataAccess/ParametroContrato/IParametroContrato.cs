﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ParametroContrato
{
    public interface IParametroContrato
    {
        int Guardar(Entity.ParametroContrato parametroContrato);
        void Actualizar(Entity.ParametroContrato parametroContrato);
        Entity.ParametroContrato GetById(int Id);
        Entity.ParametroContrato GetByTrackingId(string TrackingId);
        List<Entity.ParametroContrato> GetAll(); 

    }
}
