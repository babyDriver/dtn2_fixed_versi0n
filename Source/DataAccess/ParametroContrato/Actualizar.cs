﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ParametroContrato
{
    public class Actualizar : IUpdateFactory<Entity.ParametroContrato>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.ParametroContrato parametroContrato)
        {
            DbCommand cmd = db.GetStoredProcCommand("ParametroContrato_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, parametroContrato.Id);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, parametroContrato.ContratoId);
            db.AddInParameter(cmd, "_Valor", DbType.String, parametroContrato.Valor);
           

            return cmd;
        }
    }
}
