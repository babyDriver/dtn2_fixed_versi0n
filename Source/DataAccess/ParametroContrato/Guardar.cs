﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.ParametroContrato
{
    public class Guardar : IInsertFactory<Entity.ParametroContrato>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.ParametroContrato parametroContrato)
        {
            DbCommand cmd = db.GetStoredProcCommand("ParametroContrato_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, parametroContrato.TrackingId);
            db.AddInParameter(cmd, "_Parametroid", DbType.Int32, parametroContrato.Parametroid);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, parametroContrato.ContratoId);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, parametroContrato.FechaHora);
            db.AddInParameter(cmd, "_Valor", DbType.String, parametroContrato.Valor);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, parametroContrato.CreadoPor);

            return cmd;
        }
    }
}
