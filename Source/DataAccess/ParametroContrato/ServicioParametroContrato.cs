﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;


namespace DataAccess.ParametroContrato
{
 public   class ServicioParametroContrato : ClsAbstractService<Entity.ParametroContrato>, IParametroContrato
    {
        public ServicioParametroContrato(string dataBase)
           : base(dataBase)
        {
        }

        public void Actualizar(Entity.ParametroContrato parametroContrato)
        {
            IUpdateFactory<Entity.ParametroContrato> update = new Actualizar();
            Update(update, parametroContrato);
        }

        public new List<Entity.ParametroContrato> GetAll()
        {
            ISelectFactory<Entity.NullClass> select =  new ObtenerTodos();
            return GetAll(select, new FabricaParametroContrato(), new Entity.NullClass());
        }




        public Entity.ParametroContrato GetById(int Id)
        {
            throw new NotImplementedException();
        }

        public Entity.ParametroContrato GetByTrackingId(string TrackingId)
        {
            throw new NotImplementedException();
        }

        public int Guardar(Entity.ParametroContrato parametroContrato)
        {
            IInsertFactory<Entity.ParametroContrato> insert = new Guardar();
            return Insert(insert, parametroContrato);
        }
    }
}
