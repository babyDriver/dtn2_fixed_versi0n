﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.AdministracionExamenMedico
{
   public class ServicioAdministracionExamenMedico:ClsAbstractService<Entity.AdministracionExamenMedico>,IAdministracionExamenMedico
    {
        public ServicioAdministracionExamenMedico(string dataBase)
           : base(dataBase)
        {
        }

        public int Guardar(Entity.AdministracionExamenMedico administracionexamenmedico)
        {
            IInsertFactory<Entity.AdministracionExamenMedico> insert = new Guardar();
            return Insert(insert, administracionexamenmedico);
        }

        public Entity.AdministracionExamenMedico ObtenerPorDetalledetencionID(int DetalledetencionId)
        {
            ISelectFactory<int> select = new ObtenerPorDetalledetencionid();
            return GetByKey(select, new FabricaAdministracionExamenMedico(), DetalledetencionId);
        }
    }
}
