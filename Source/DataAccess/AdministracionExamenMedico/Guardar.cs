﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.AdministracionExamenMedico
{
    public class Guardar : IInsertFactory<Entity.AdministracionExamenMedico>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.AdministracionExamenMedico administracionExamen)
        {
            DbCommand cmd = db.GetStoredProcCommand("administracionexamenmedico_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, administracionExamen.TrackingId.ToString());
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, administracionExamen.DetalledetencionId);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, administracionExamen.Fecha);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, administracionExamen.CreadoPor);
            db.AddInParameter(cmd, "_Justificacion", DbType.String, administracionExamen.Justificacion);

            return cmd;
        }
    }
}
