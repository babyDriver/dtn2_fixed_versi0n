﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.AdministracionExamenMedico
{
   public interface IAdministracionExamenMedico
    {
        int Guardar(Entity.AdministracionExamenMedico administracionexamenmedico);
        Entity.AdministracionExamenMedico ObtenerPorDetalledetencionID(int DetalledetencionId);
    }
}
