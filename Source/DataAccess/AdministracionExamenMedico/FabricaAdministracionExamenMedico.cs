﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.AdministracionExamenMedico
{
    class FabricaAdministracionExamenMedico : IEntityFactory<Entity.AdministracionExamenMedico>
    {
        public Entity.AdministracionExamenMedico ConstructEntity(IDataReader dr)
        {
            var administracionExamenMedico = new Entity.AdministracionExamenMedico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                administracionExamenMedico.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                administracionExamenMedico.TrackingId = dr.GetString(index);
            }
            index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                administracionExamenMedico.DetalledetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                administracionExamenMedico.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                administracionExamenMedico.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Justificacion");
            if (!dr.IsDBNull(index))
            {
                administracionExamenMedico.Justificacion = dr.GetString(index);
            }

            return administracionExamenMedico;




        }
    }
}
