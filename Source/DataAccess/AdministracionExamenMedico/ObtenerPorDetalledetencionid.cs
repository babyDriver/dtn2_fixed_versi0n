﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.AdministracionExamenMedico
{
    public class ObtenerPorDetalledetencionid : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Detalledetencionid)
        {
            DbCommand cmd = db.GetStoredProcCommand("administracionexamenmedico_GetByDetalleDetencionId_SP");
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, Detalledetencionid);
            return cmd;
        }
    }
}
