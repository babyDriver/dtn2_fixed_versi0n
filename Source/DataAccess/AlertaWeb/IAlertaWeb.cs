﻿using System;
using System.Collections.Generic;

namespace DataAccess.AlertaWeb
{
    public interface IAlertaWeb
    {
        List<Entity.AlertaWeb> ObtenerTodos();
        Entity.AlertaWeb ObtenerPorId(int id);
        Entity.AlertaWeb ObtenerPorTrackingId(Guid trackingId);
        int Guardar(Entity.AlertaWeb alertaWeb);
        void Actualizar(Entity.AlertaWeb alertaWeb);
    }
}
