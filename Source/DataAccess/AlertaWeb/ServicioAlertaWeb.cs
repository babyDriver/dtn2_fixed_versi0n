﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.AlertaWeb
{
    public class ServicioAlertaWeb : ClsAbstractService<Entity.AlertaWeb>, IAlertaWeb
    {
        public ServicioAlertaWeb(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.AlertaWeb alertaWeb)
        {
            IUpdateFactory<Entity.AlertaWeb> update = new Actualizar();
            Update(update, alertaWeb);
        }

        public int Guardar(Entity.AlertaWeb alertaWeb)
        {
            IInsertFactory<Entity.AlertaWeb> insert = new Guardar();
            return Insert(insert, alertaWeb);
        }

        public Entity.AlertaWeb ObtenerPorId(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaAlertaWeb(), id);
        }

        public Entity.AlertaWeb ObtenerPorTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaAlertaWeb(), trackingId);
        }

        public List<Entity.AlertaWeb> ObtenerTodos()
        {
            ISelectFactory<NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaAlertaWeb(), new NullClass());
        }
    }
}
