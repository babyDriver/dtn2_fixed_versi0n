﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.AlertaWeb
{
    public class FabricaAlertaWeb : IEntityFactory<Entity.AlertaWeb>
    {
        public Entity.AlertaWeb ConstructEntity(IDataReader dr)
        {
            var alertaWeb = new Entity.AlertaWeb();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("UrlWebService");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.UrlWebService = dr.GetString(index);
            }

            index = dr.GetOrdinal("Username");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.Username = dr.GetString(index);
            }

            index = dr.GetOrdinal("Password");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.Password = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaRegistro");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.FechaRegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaModificacion");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("ModificadoPor");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.ModificadoPor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Denominacion");
            if (!dr.IsDBNull(index))
            {
                alertaWeb.Denominacion = dr.GetString(index);
            }
            return alertaWeb;
        }
    }
}
