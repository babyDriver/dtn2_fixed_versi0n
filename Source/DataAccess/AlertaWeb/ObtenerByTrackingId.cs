﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.AlertaWeb
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("ConfiguracionAlertaWeb_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, identity);
            return cmd;
        }
    }
}
