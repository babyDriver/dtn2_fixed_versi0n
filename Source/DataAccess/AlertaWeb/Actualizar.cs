﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.AlertaWeb
{
    public class Actualizar : IUpdateFactory<Entity.AlertaWeb>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.AlertaWeb entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("ConfiguracionAlertaWeb_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, entity.Id);
            db.AddInParameter(cmd, "_UrlWebService", DbType.String, entity.UrlWebService);
            db.AddInParameter(cmd, "_Username", DbType.String, entity.Username);
            db.AddInParameter(cmd, "_Password", DbType.String, entity.Password);
            db.AddInParameter(cmd, "_ModificadoPor", DbType.Int32, entity.ModificadoPor);
            db.AddInParameter(cmd, "_Denominacion", DbType.String, entity.Denominacion);
            return cmd;
        }
    }
}
