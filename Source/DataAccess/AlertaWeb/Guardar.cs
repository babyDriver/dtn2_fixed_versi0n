﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.AlertaWeb
{
    public class Guardar : IInsertFactory<Entity.AlertaWeb>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.AlertaWeb entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("ConfiguracionAlertaWeb_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, entity.TrackingId);
            db.AddInParameter(cmd, "_UrlWebService", DbType.String, entity.UrlWebService);
            db.AddInParameter(cmd, "_Username", DbType.String, entity.Username);
            db.AddInParameter(cmd, "_Password", DbType.String, entity.Password);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, entity.CreadoPor);
            db.AddInParameter(cmd, "_Denominacion", DbType.String, entity.Denominacion);
            return cmd;
        }
    }
}
