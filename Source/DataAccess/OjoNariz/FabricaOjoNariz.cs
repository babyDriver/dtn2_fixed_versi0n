﻿using DataAccess.Interfaces;


namespace DataAccess.OjoNariz
{
    public class FabricaOjoNariz : IEntityFactory<Entity.OjoNariz>
    {
       

        public Entity.OjoNariz ConstructEntity(System.Data.IDataReader dr)
        {
            var ojo = new Entity.OjoNariz();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                ojo.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                ojo.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("ColorO");
            if (!dr.IsDBNull(index))
            {
                ojo.ColorO = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FormaO");
            if (!dr.IsDBNull(index))
            {
                ojo.FormaO = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TamanoO");
            if (!dr.IsDBNull(index))
            {
                ojo.TamanoO = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("RaizN");
            if (!dr.IsDBNull(index))
            {
                ojo.RaizN = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AlturaN");
            if (!dr.IsDBNull(index))
            {
                ojo.AlturaN = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AnchoN");
            if (!dr.IsDBNull(index))
            {
                ojo.AnchoN = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DorsoN");
            if (!dr.IsDBNull(index))
            {
                ojo.DorsoN = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("BaseN");
            if (!dr.IsDBNull(index))
            {
                ojo.BaseN = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AntropometriaId");
            if (!dr.IsDBNull(index))
            {
                ojo.AntropometriaId = dr.GetInt32(index);
            }
            
            return ojo;
        }
    }
}