﻿using System;
using System.Collections.Generic;

namespace DataAccess.OjoNariz
{
    public interface IOjoNariz
    {
        List<Entity.OjoNariz> ObtenerTodos();
        Entity.OjoNariz ObtenerById(int Id);
        Entity.OjoNariz ObtenerByAntropometriaId(int Id);
        Entity.OjoNariz ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.OjoNariz ojo);
        void Actualizar(Entity.OjoNariz ojo);
    }
}