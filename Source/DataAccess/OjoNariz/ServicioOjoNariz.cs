﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.OjoNariz
{
    public class ServicioOjoNariz : ClsAbstractService<Entity.OjoNariz>, IOjoNariz
    {
        public ServicioOjoNariz(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.OjoNariz> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaOjoNariz(), new Entity.NullClass());
        }

   

        public Entity.OjoNariz ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaOjoNariz(), userId);
        }

        public Entity.OjoNariz ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaOjoNariz(), Id);
        }

        public Entity.OjoNariz ObtenerByAntropometriaId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByAntropometriaId();
            return GetByKey(select, new FabricaOjoNariz(), Id);
        }

        public int Guardar(Entity.OjoNariz ojo)
        {
            IInsertFactory<Entity.OjoNariz> insert = new Guardar();
            return Insert(insert, ojo);
        }

        public void Actualizar(Entity.OjoNariz ojo)
        {
            IUpdateFactory<Entity.OjoNariz> update = new Actualizar();
            Update(update, ojo);
        }


    }
}