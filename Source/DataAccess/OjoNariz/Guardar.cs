﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.OjoNariz
{
    public class Guardar : IInsertFactory<Entity.OjoNariz>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.OjoNariz ojo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Ojos_Nariz_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, ojo.TrackingId);
            db.AddInParameter(cmd, "_ColorO", DbType.Int32, ojo.ColorO);
            db.AddInParameter(cmd, "_FormaO", DbType.Int32, ojo.FormaO);

            if (ojo.TamanoO != null)
                db.AddInParameter(cmd, "_TamanoO", DbType.Int32, ojo.TamanoO);
            else
                db.AddInParameter(cmd, "_TamanoO", DbType.Int32, null);

            if (ojo.RaizN != null)
                db.AddInParameter(cmd, "_RaizN", DbType.Int32, ojo.RaizN);
            else
                db.AddInParameter(cmd, "_RaizN", DbType.Int32, null);

            if (ojo.AlturaN != null)
                db.AddInParameter(cmd, "_AlturaN", DbType.Int32, ojo.AlturaN);
            else
                db.AddInParameter(cmd, "_AlturaN", DbType.Int32, null);

            if (ojo.AnchoN != null)
                db.AddInParameter(cmd, "_AnchoN", DbType.Int32, ojo.AnchoN);
            else
                db.AddInParameter(cmd, "_AnchoN", DbType.Int32, null);

            db.AddInParameter(cmd, "_DorsoN", DbType.Int32, ojo.DorsoN);
            db.AddInParameter(cmd, "_BaseN", DbType.Int32, ojo.BaseN);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, ojo.AntropometriaId);


            return cmd;
        }
    }
}
