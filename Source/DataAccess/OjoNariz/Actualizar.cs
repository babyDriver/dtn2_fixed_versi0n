﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.OjoNariz
{
    public class Actualizar : IUpdateFactory<Entity.OjoNariz>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.OjoNariz ojo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Ojos_Nariz_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, ojo.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, ojo.TrackingId);
            db.AddInParameter(cmd, "_ColorO", DbType.Int32, ojo.ColorO);
            db.AddInParameter(cmd, "_FormaO", DbType.Int32, ojo.FormaO);

            if (ojo.TamanoO != null)
                db.AddInParameter(cmd, "_TamanoO", DbType.Int32, ojo.TamanoO);
            else
                db.AddInParameter(cmd, "_TamanoO", DbType.Int32, null);

            if (ojo.RaizN != null)
                db.AddInParameter(cmd, "_RaizN", DbType.Int32, ojo.RaizN);
            else
                db.AddInParameter(cmd, "_RaizN", DbType.Int32, null);

            if (ojo.AlturaN != null)
                db.AddInParameter(cmd, "_AlturaN", DbType.Int32, ojo.AlturaN);
            else
                db.AddInParameter(cmd, "_AlturaN", DbType.Int32, null);

            if (ojo.AnchoN != null)
                db.AddInParameter(cmd, "_AnchoN", DbType.Int32, ojo.AnchoN);
            else
                db.AddInParameter(cmd, "_AnchoN", DbType.Int32, null);

            db.AddInParameter(cmd, "_DorsoN", DbType.Int32, ojo.DorsoN);
            db.AddInParameter(cmd, "_BaseN", DbType.Int32, ojo.BaseN);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, ojo.AntropometriaId);

           

            return cmd;
        }
    }
}
