﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.HistoricoAgrupadoDetenido
{
    public class ObtenerPorDetenido : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("historico_agrupado_detenido_ObtenerPorDetenido_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, id);
            return cmd;
        }
    }
}
