﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.HistoricoAgrupadoDetenido
{
    public class FabricaHistoricoAgrupadoDetenido : IEntityFactory<Entity.HistoricoAgrupadoDetenido>
    {
        public Entity.HistoricoAgrupadoDetenido ConstructEntity(IDataReader dr)
        {
            var item = new Entity.HistoricoAgrupadoDetenido();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }

           
            index = dr.GetOrdinal("DetenidoOrignalId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoOriginalId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetalleDetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalleDetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expedienteoriginal");
            if (!dr.IsDBNull(index))
            {
                item.Expedienteoriginal = dr.GetString(index);
            }

            return item;
        }
    }
}
