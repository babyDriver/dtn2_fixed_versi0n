﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.HistoricoAgrupadoDetenido
{
    public class ServicioHistoricoAgrupadoDetenido : ClsAbstractService<Entity.HistoricoAgrupadoDetenido>, IHistoricoAgrupadoDetenido
    {
        public ServicioHistoricoAgrupadoDetenido(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.HistoricoAgrupadoDetenido> ObtenerPorDetenidoOriginal(int DetenidoOriginalId)
        {
            ISelectFactory<int> select = new ObtenerPorDetenidoOriginal();
            return GetAll(select, new FabricaHistoricoAgrupadoDetenido(), DetenidoOriginalId);
        }

        public List<Entity.HistoricoAgrupadoDetenido> ObtenerPorDetenidol(int DetenidolId)
        {
            ISelectFactory<int> select = new ObtenerPorDetenido();
            return GetAll(select, new FabricaHistoricoAgrupadoDetenido(), DetenidolId);
        }
        public int save(Entity.HistoricoAgrupadoDetenido historicoAgrupadoDetenido)
        {
            IInsertFactory<Entity.HistoricoAgrupadoDetenido> insert = new Guardar();
            return Insert(insert, historicoAgrupadoDetenido);
        }
    }
}
