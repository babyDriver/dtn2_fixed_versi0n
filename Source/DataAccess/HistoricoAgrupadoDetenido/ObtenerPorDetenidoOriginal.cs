﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.HistoricoAgrupadoDetenido
{
    public class ObtenerPorDetenidoOriginal : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetenidoOriginalID)
        {
            DbCommand cmd = db.GetStoredProcCommand("historico_agrupado_detenido_ObtenerPorDetenidoOriginal_SP");
            db.AddInParameter(cmd, "_DetenidoOrignalId", DbType.Int32, DetenidoOriginalID);
            return cmd;
        }
    }
}
