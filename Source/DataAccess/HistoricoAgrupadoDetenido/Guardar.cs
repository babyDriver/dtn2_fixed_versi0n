﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.HistoricoAgrupadoDetenido
{
    public class Guardar : IInsertFactory<Entity.HistoricoAgrupadoDetenido>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.HistoricoAgrupadoDetenido item)
        {
            DbCommand cmd = db.GetStoredProcCommand("HistoricoAgrupadoDetenido_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, item.FechaHora);
            db.AddInParameter(cmd, "_DetenidoOrignalId", DbType.Int32, item.DetenidoOriginalId);
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, item.DetalleDetencionId);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, item.DetenidoId);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_Expediente", DbType.String, item.Expediente);
            db.AddInParameter(cmd, "_Expedienteoriginal", DbType.String, item.Expedienteoriginal);

            return cmd;
        }
    }
}
