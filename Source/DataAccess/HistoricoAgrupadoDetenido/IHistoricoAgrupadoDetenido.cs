﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace DataAccess.HistoricoAgrupadoDetenido
{
    public interface IHistoricoAgrupadoDetenido
    {
        int save(Entity.HistoricoAgrupadoDetenido historicoAgrupadoDetenido);
    }
}
