﻿using System;
using System.Collections.Generic;

namespace DataAccess.Matrizpermiso
{
    interface IMatrizpermiso
    {
        Entity.Matrizpermiso ObtenerByUsuarioId(int usuarioId);
        List<Entity.Matrizpermiso> ObtenerTodos();
        int Guardar(Entity.Matrizpermiso matriz);
        void Actualizar(Entity.Matrizpermiso matriz);
    }
}
