﻿using DataAccess.Interfaces;


namespace DataAccess.Matrizpermiso
{
    public class FabricaMatrizpermiso : IEntityFactory<Entity.Matrizpermiso>
    {
        public Entity.Matrizpermiso ConstructEntity(System.Data.IDataReader dr)
        {
            var matriz = new Entity.Matrizpermiso();

            var index = dr.GetOrdinal("UsuarioId");
            if (!dr.IsDBNull(index))
            {
                matriz.UsuarioId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("PermisoId");
            if (!dr.IsDBNull(index))
            {
                matriz.PermisoId = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                matriz.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("PantallaId");
            if (!dr.IsDBNull(index))
            {
                matriz.PantallaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("RolId");
            if (!dr.IsDBNull(index))
            {
                matriz.RolId = dr.GetInt32(index);
            }



            return matriz;
        }
    }
}
