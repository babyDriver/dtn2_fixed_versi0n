﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Matrizpermiso
{
    public class ServicioMatrizpermiso : ClsAbstractService<Entity.Matrizpermiso>, IMatrizpermiso
    {
        public ServicioMatrizpermiso(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Matrizpermiso> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaMatrizpermiso(), new Entity.NullClass());
        }

        public Entity.Matrizpermiso ObtenerByUsuarioId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByUsuarioId();
            return GetByKey(select, new FabricaMatrizpermiso(), Id);
        }

        public int Guardar(Entity.Matrizpermiso usuario)
        {
            IInsertFactory<Entity.Matrizpermiso> insert = new Guardar();
            return Insert(insert, usuario);
        }

        public void Actualizar(Entity.Matrizpermiso usuario)
        {
            IUpdateFactory<Entity.Matrizpermiso> update = new Actualizar();
            Update(update, usuario);
        }
    }
}
