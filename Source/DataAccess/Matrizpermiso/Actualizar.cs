﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Matrizpermiso
{
    public class Actualizar : IUpdateFactory<Entity.Matrizpermiso>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Matrizpermiso matriz)
        {
            DbCommand cmd = db.GetStoredProcCommand("Matrizpermisos_Insert_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, matriz.UsuarioId);
            db.AddInParameter(cmd, "_PermisoId", DbType.Int32, matriz.PermisoId);
            db.AddInParameter(cmd, "_PantallaId", DbType.String, matriz.PantallaId);
            db.AddInParameter(cmd, "_RolId", DbType.String, matriz.RolId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, matriz.Habilitado);


            return cmd;
        }
    }
}
