﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.Matrizpermiso
{
    public class ObtenerByUsuarioId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int usuarioid)
        {
            DbCommand cmd = db.GetStoredProcCommand("Matrizpermisos_GetByUsuarioId_SP");
            db.AddInParameter(cmd, "UsuarioId", DbType.Int32, usuarioid);
            return cmd;
        }
    }
}
