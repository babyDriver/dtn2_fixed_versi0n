﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.FichaInvestigacion
{
    public interface IFichaInvestigacion
    {
        int Guardar(Entity.FichaInvestigacion Ficha);
    }
}
