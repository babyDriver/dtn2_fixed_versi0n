﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.FichaInvestigacion
{
    public class ServicioFichaInvestigacion : ClsAbstractService<Entity.FichaInvestigacion>, IFichaInvestigacion
    {
        public ServicioFichaInvestigacion(string dataBase)
            : base(dataBase) { }
        public int Guardar(Entity.FichaInvestigacion Ficha)
        {
            IInsertFactory<Entity.FichaInvestigacion> insert = new Guardar();
            return Insert(insert, Ficha);
        }
    }
}
