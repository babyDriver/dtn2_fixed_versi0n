﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteEstadisticoDetenidoPorTipoLesionCL
{
    public interface IReporteEstadisticoDetenidoPorTipoLesionCL
    {
        List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL> GetByFilter(string[] filter);
    }
}
