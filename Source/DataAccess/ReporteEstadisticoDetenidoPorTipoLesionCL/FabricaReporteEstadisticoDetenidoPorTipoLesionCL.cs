﻿using DataAccess.Interfaces;
using Entity;
using System.Data;

namespace DataAccess.ReporteEstadisticoDetenidoPorTipoLesionCL
{
    public class FabricaReporteEstadisticoDetenidoPorTipoLesionCL : IEntityFactory<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL>
    {
        public Entity.ReporteEstadisticoDetenidoPorTipoLesionCL ConstructEntity(IDataReader dr)
        {
            var detenidos = new Entity.ReporteEstadisticoDetenidoPorTipoLesionCL();

            var index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                detenidos.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                detenidos.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Lesion");
            if (!dr.IsDBNull(index))
            {
                detenidos.Lesion = dr.GetString(index);
            }

        

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                detenidos.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                detenidos.Edad = dr.GetInt32(index);
            }

            return detenidos;
        }
    }
}
