﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteEstadisticoDetenidoPorTipoLesionCL
{
    public class ServicioReporteEstadisticoDetenidoPorTipoLesionCL : ClsAbstractService<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL>, IReporteEstadisticoDetenidoPorTipoLesionCL
    {
        public ServicioReporteEstadisticoDetenidoPorTipoLesionCL(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ReporteEstadisticoDetenidoPorTipoLesionCL> GetByFilter(string[] filter)
        {
            ISelectFactory<string[]> select = new ObtenerPorFiltros();
            return GetAll(select, new FabricaReporteEstadisticoDetenidoPorTipoLesionCL(), filter);
        }
    }
}
