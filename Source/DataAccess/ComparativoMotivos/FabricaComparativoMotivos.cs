﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ComparativoMotivos
{
    public class FabricaComparativoMotivos : IEntityFactory<Entity.Comparativomotivos>
    {
        public Comparativomotivos ConstructEntity(IDataReader dr)
        {
            var comparativo = new Entity.Comparativomotivos();

            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                comparativo.Remision = dr.GetString(index);
            }


            index = dr.GetOrdinal("Detenido");
            if (!dr.IsDBNull(index))
            {
                comparativo.Detenido = dr.GetString(index);
            }


            index = dr.GetOrdinal("Registro");
            if (!dr.IsDBNull(index))
            {
                comparativo.Registro = dr.GetString(index);
            }


            index = dr.GetOrdinal("Motivos");
            if (!dr.IsDBNull(index))
            {
                comparativo.Motivos = dr.GetString(index);
            }

            index = dr.GetOrdinal("Califico");
            if (!dr.IsDBNull(index))
            {
                comparativo.Califico = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivocalificacion");
            if (!dr.IsDBNull(index))
            {
                comparativo.Motivocalificacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Foliollamada");
            if (!dr.IsDBNull(index))
            {
                comparativo.Foliollamada = dr.GetString(index);
            }
            index = dr.GetOrdinal("Registrollamada");
            if (!dr.IsDBNull(index))
            {
                comparativo.Registrollamada = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivollamada");
            if (!dr.IsDBNull(index))
            {
                comparativo.Motivollamada = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                comparativo.Fecha = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                comparativo.Edad = dr.GetInt32(index);
            }

            return comparativo;
        }
    }
}