﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.ComparativoMotivos
{
    public class GetComparativoMotivos : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] filtros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ReporteEstadistico_Comparativodemotivos_SP");
            db.AddInParameter(cmd, "_Fechainicio", DbType.Date, Convert.ToDateTime(filtros[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.Date, Convert.ToDateTime(filtros[1]));
            db.AddInParameter(cmd, "_Motivodetencionid", DbType.Int32, Convert.ToInt32(filtros[2]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(filtros[3]));
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, Convert.ToInt32(filtros[5]));
            return cmd;
        }
    }
}