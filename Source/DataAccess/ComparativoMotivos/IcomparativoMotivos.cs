﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ComparativoMotivos
{
    public interface IcomparativoMotivos
    {
        List<Entity.Comparativomotivos> GetComparativomotivos(string[] filtromotivos);
    }
}