﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.ComparativoMotivos
{
    public class ServicioComparativoMotivos : ClsAbstractService<Entity.Comparativomotivos>, IcomparativoMotivos
    {
        public ServicioComparativoMotivos(string dataBase)
            : base(dataBase)
        {
        }
        public List<Comparativomotivos> GetComparativomotivos(string[] filtromotivos)
        {
            ISelectFactory<string[]> select = new GetComparativoMotivos();
            return GetAll(select, new FabricaComparativoMotivos(), filtromotivos);
        }
    }
}