﻿using System;
using System.Collections.Generic;

namespace DataAccess.Reportes
{
    public interface IReportes
    {
        List<Entity.Reportes> ObtenerTodos();
        List<Entity.Reportes> ObtenerPorPantallaId(int pantallaId);
        Entity.Reportes ObtenerPorId(int id);
        Entity.Reportes ObtenerPorTrackingId(Guid trackingId);
    }
}
