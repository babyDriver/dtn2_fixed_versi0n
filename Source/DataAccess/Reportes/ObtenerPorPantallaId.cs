﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Reportes
{
    public class ObtenerPorPantallaId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Reportes_GetByPantallaId_SP");
            db.AddInParameter(cmd, "_PantallaId", DbType.Int32, identity);

            return cmd;
        }
    }
}
