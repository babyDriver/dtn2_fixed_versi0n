﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;

namespace DataAccess.Reportes
{
    public class ServicioReportes : ClsAbstractService<Entity.Reportes>, IReportes
    {
        public ServicioReportes(string dataBase) : base(dataBase) { }

        public Entity.Reportes ObtenerPorId(int id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaReportes(), id);
        }

        public List<Entity.Reportes> ObtenerPorPantallaId(int pantallaId)
        {
            ISelectFactory<int> select = new ObtenerPorPantallaId();
            return GetAll(select, new FabricaReportes(), pantallaId);
        }

        public Entity.Reportes ObtenerPorTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerPorTrackingId();
            return GetByKey(select, new FabricaReportes(), trackingId);
        }

        public List<Entity.Reportes> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaReportes(), new Entity.NullClass());
        }
    }
}
