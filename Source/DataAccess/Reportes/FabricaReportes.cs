﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Reportes
{
    public class FabricaReportes : IEntityFactory<Entity.Reportes>
    {
        public Entity.Reportes ConstructEntity(IDataReader dr)
        {
            var reporte = new Entity.Reportes();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                reporte.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                reporte.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                reporte.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                reporte.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                reporte.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                reporte.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                reporte.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                reporte.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                reporte.Tipo = dr.GetString(index);
            }

            index = dr.GetOrdinal("PantallaId");
            if (!dr.IsDBNull(index))
            {
                reporte.PantallaId = dr.GetInt32(index);
            }

            return reporte;
        }
    }
}
