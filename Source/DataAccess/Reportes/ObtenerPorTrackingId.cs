﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Reportes
{
    public class ObtenerPorTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Reportes_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, identity);

            return cmd;
        }
    }
}
