﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using DataAccess.Interfaces;

namespace DataAccess
{
    public abstract class ClsAbstractService<TEntity>
    {
        private Database attDataBase;
        private string attDataBaseName = "SQLConnectionString";

        #region Constructs

        public ClsAbstractService()
        {
            if (attDataBase == null)
            {
                attDataBase = DatabaseFactory.CreateDatabase(attDataBaseName);
            }
        }

        public ClsAbstractService(string dataBase)
        {
            if (attDataBase == null)
            {
                attDataBase = DatabaseFactory.CreateDatabase(dataBase);
            }
        }

        #endregion

        #region Methods Common

        public List<TEntity> GetAll<TIdentity>
                             (ISelectFactory<TIdentity> selectFactory,
                              IEntityFactory<TEntity> entityFactory,
                              TIdentity identity)
        {

            List<TEntity> listEntity = new List<TEntity>();
            using (DbCommand cmd = selectFactory.ConstructSelectCommand(attDataBase, identity))
            {
                cmd.CommandTimeout = 60;
                using (IDataReader dr = attDataBase.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        listEntity.Add(entityFactory.ConstructEntity(dr));
                    }
                }
            }
            return listEntity;
        }

        public List<TEntity> GetAll<TIdentity>
                             (ISelectFactory<TIdentity> selectFactory,
                              IEntityFactory<TEntity> entityFactory,
                              TIdentity identity, DateTime fechaInicial, DateTime fechaFinal)
        {

            List<TEntity> listEntity = new List<TEntity>();
            using (DbCommand cmd = selectFactory.ConstructSelectCommand(attDataBase, identity))
            {
                DbParameter parameterFechaInicial = cmd.CreateParameter();
                parameterFechaInicial.ParameterName = "@fechaInicial";
                parameterFechaInicial.DbType = DbType.Date;
                parameterFechaInicial.Value = fechaInicial;
                parameterFechaInicial.SourceVersion = DataRowVersion.Current;

                cmd.Parameters.Add(parameterFechaInicial);

                DbParameter parameterFechaFinal = cmd.CreateParameter();
                parameterFechaFinal.ParameterName = "@fechaFinal";
                parameterFechaFinal.Value = fechaFinal;
                parameterFechaFinal.DbType = DbType.Date;
                parameterFechaFinal.SourceVersion = DataRowVersion.Current;

                cmd.Parameters.Add(parameterFechaFinal);

                using (IDataReader dr = attDataBase.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                        listEntity.Add(entityFactory.ConstructEntity(dr));
                    }
                }
            }
            return listEntity;
        }

        public TEntity GetByKey<TIdentity>
                       (ISelectFactory<TIdentity> selectFactory,
                        IEntityFactory<TEntity> entityFactory,
                        TIdentity identity)
        {

            TEntity entity = default(TEntity);
            using (DbCommand cmd = selectFactory.ConstructSelectCommand(attDataBase, identity))
            {
                using (IDataReader dr = attDataBase.ExecuteReader(cmd))
                {
                    while (dr.Read())
                    {
                         
                        entity = entityFactory.ConstructEntity(dr);
                    }
                }
            }
            return entity;
        }

        public Guid Insert(IInsertFactory<TEntity> insertFactory, TEntity entity, Guid newIdentity)
        {
            try
            {
                using (DbCommand cmd = insertFactory.ConstructInsertCommand(attDataBase, entity))
                {
                    attDataBase.ExecuteNonQuery(cmd);

                    if (cmd.Parameters.Contains("_NewIdentity"))
                    {
                        newIdentity = new Guid((attDataBase.GetParameterValue(cmd, "_NewIdentity")).ToString());
                        return newIdentity;
                    }
                    return new Guid();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Insert(IInsertFactory<TEntity> insertFactory, TEntity entity)
        {
            var identity = -1;
            try
            {
                using (DbCommand cmd = insertFactory.ConstructInsertCommand(attDataBase, entity))
                {
                    attDataBase.ExecuteNonQuery(cmd);

                    if (cmd.Parameters.Contains("_NewIdentity"))
                    {
                        var newId = Convert.ToInt32(attDataBase.GetParameterValue(cmd, "_NewIdentity"));
                        return newId;
                    }
                    else
                    {
                        return identity;
                    }
                }
            }
            catch (Exception ex)
            {
                var eatError = ex.Message;
                return identity;
            }
        }

        public long Insert(IInsertFactory<TEntity> insertFactory, TEntity entity, ref long newIdentity)
        {
            newIdentity = -1;
            try
            {
                using (DbCommand cmd = insertFactory.ConstructInsertCommand(attDataBase, entity))
                {
                    attDataBase.ExecuteNonQuery(cmd);

                    if (cmd.Parameters.Contains("_NewIdentity"))
                    {
                        newIdentity = Convert.ToInt32(attDataBase.GetParameterValue(cmd, "_NewIdentity"));
                        return newIdentity;
                    }
                    else
                    {
                        return newIdentity;
                    }
                }
            }
            catch (Exception ex)
            {
                var eatError = ex.Message;
                return newIdentity;
            }
        }

        public void Update(IUpdateFactory<TEntity> updateFactory, TEntity entity)
        {
            using (DbCommand cmd = updateFactory.ConstructUpdateCommand(attDataBase, entity))
            {
                attDataBase.ExecuteNonQuery(cmd);
            }
        }


        public void Delete<TIdentity>(IDeleteFactory<TIdentity> deleteFactory, TIdentity identity)
        {

            using (DbCommand cmd = deleteFactory.ConstructDeleteCommand(attDataBase, identity))
            {
                attDataBase.ExecuteNonQuery(cmd);
            }
        }

        //public DataTable GetDataTable<TIdentity>
        //                     (ISelectFactory<TIdentity> selectFactory,
        //                      IEntityFactory<TEntity> entityFactory,
        //                      TIdentity identity)
        //{

        //    DataTable dt = new DataTable();

        //    using (DbCommand cmd = selectFactory.ConstructSelectCommand(attDataBase, identity))
        //    {
        //        IDataReader dr = attDataBase.ExecuteReader(cmd);
        //        dt.Load(dr);
        //    }
        //    return dt;
        //}

        #endregion
    }
}
