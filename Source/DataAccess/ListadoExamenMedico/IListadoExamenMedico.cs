﻿using System.Collections.Generic;

namespace DataAccess.ListadoExamenMedico
{
    public interface IListadoExamenMedico
    {
        List<Entity.ListadoExamenMedico> ObtenerTodos(object[] data);
    }
}
