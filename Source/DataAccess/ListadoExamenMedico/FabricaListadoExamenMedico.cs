﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoExamenMedico
{
    public class FabricaListadoExamenMedico : IEntityFactory<Entity.ListadoExamenMedico>
    {
        public Entity.ListadoExamenMedico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoExamenMedico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("NCP");
            if (!dr.IsDBNull(index))
            {
                item.NCP = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                item.Estatus = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingIdEstatus");
            if (!dr.IsDBNull(index))
            {
                item.TrackingIdEstatus = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("RutaImagen");
            if (!dr.IsDBNull(index))
            {
                item.RutaImagen = dr.GetString(index);
            }

            index = dr.GetOrdinal("ExamenId");
            if (!dr.IsDBNull(index))
            {
                item.ExamenId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ExamenTrackingId");
            if (!dr.IsDBNull(index))
            {
                item.ExamenTrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Registro");
            if (!dr.IsDBNull(index))
            {
                item.Registro = dr.GetString(index);
            }

            index = dr.GetOrdinal("Situacion");
            if (!dr.IsDBNull(index))
            {
                item.Situacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("NombreCompleto");
            if (!dr.IsDBNull(index))
            {
                item.NombreCompleto = dr.GetString(index);
            }            

            return item;
        }
    }
}
