﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoExamenMedico
{
    public class ServicioListadoExamenMedico : ClsAbstractService<Entity.ListadoExamenMedico>, IListadoExamenMedico
    {
        public ServicioListadoExamenMedico(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoExamenMedico> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoExamenMedico(), parametros);
        }
    }
}
