﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.DetenidoEvento
{
    public class Guardar : IInsertFactory<Entity.DetenidoEvento>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.DetenidoEvento item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_evento_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, item.Nombre);
            db.AddInParameter(cmd, "_Paterno", DbType.String, item.Paterno);
            db.AddInParameter(cmd, "_Materno", DbType.String, item.Materno);
            db.AddInParameter(cmd, "_SexoId", DbType.Int32, item.SexoId);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, item.EventoId);
            db.AddInParameter(cmd, "_Edad", DbType.Int32, item.Edad);
            db.AddInParameter(cmd, "_MotivoId", DbType.Int32, item.MotivoId);

            return cmd;
        }
    }
}
