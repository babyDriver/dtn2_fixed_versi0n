﻿using System;
using System.Collections.Generic;

namespace DataAccess.DetenidoEvento
{
    public interface IDetenidoEvento
    {
        void Actualizar(Entity.DetenidoEvento item);
        int Guardar(Entity.DetenidoEvento item);
        List<Entity.DetenidoEvento> ObtenerByEventoId(int id);
        Entity.DetenidoEvento ObtenerById(int Id);
        List<Entity.DetenidoEvento> ObtenersinregistroBByEventoId(int id);
    }
}
