﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.DetenidoEvento
{
    class Actualizar : IUpdateFactory<Entity.DetenidoEvento>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.DetenidoEvento entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_evento_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, entity.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, entity.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, entity.Nombre);
            db.AddInParameter(cmd, "_Paterno", DbType.String, entity.Paterno);
            db.AddInParameter(cmd, "_Materno", DbType.String, entity.Materno);
            db.AddInParameter(cmd, "_SexoId", DbType.Int32, entity.SexoId);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, entity.Habilitado);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, entity.CreadoPor);
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, entity.EventoId);
            db.AddInParameter(cmd, "_Edad", DbType.Int32, entity.Edad);
            db.AddInParameter(cmd, "_MotivoId", DbType.Int32, entity.MotivoId);

            return cmd;
        }
    }
}
