﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.DetenidoEvento
{
    public class ObtenerByEventoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_evento_GetByEventoId_SP");
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, id);
            return cmd;
        }
    }
}
