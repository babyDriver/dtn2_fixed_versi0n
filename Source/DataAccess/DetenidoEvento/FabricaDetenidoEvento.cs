﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.DetenidoEvento
{
    public class FabricaDetenidoEvento : IEntityFactory<Entity.DetenidoEvento>
    {
        public Entity.DetenidoEvento ConstructEntity(IDataReader dr)
        {
            var item = new Entity.DetenidoEvento();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("SexoId");
            if (!dr.IsDBNull(index))
            {
                item.SexoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("EventoId");
            if (!dr.IsDBNull(index))
            {
                item.EventoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fechanacimiento");
            if (!dr.IsDBNull(index))
            {
                item.Fechanacimiento = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("MotivoId");
            if (!dr.IsDBNull(index))
            {
                item.MotivoId = dr.GetInt32(index);
            }

            return item;
        }
    }
}
