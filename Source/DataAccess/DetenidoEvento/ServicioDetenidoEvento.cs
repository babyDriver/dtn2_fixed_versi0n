﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DetenidoEvento
{
    public class ServicioDetenidoEvento : ClsAbstractService<Entity.DetenidoEvento>, IDetenidoEvento
    {
        public ServicioDetenidoEvento(String dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.DetenidoEvento item)
        {
            IUpdateFactory<Entity.DetenidoEvento> update = new Actualizar();
            Update(update, item);
        }

        public int Guardar(Entity.DetenidoEvento item)
        {
            IInsertFactory<Entity.DetenidoEvento> insert = new Guardar();
            return Insert(insert, item);
        }

        public List<Entity.DetenidoEvento> ObtenerByEventoId(int id)
        {
            ISelectFactory<int> select = new ObtenerByEventoId();
            return GetAll(select, new FabricaDetenidoEvento(), id);
        }

        public Entity.DetenidoEvento ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaDetenidoEvento(), Id);
        }

        public List<Entity.DetenidoEvento> ObtenersinregistroBByEventoId(int id)
        {
            ISelectFactory<int> select = new ObtenersinregistroiBarandeillaPorEventoId();
            return GetAll(select, new FabricaDetenidoEvento(), id);
        }
    }
}
