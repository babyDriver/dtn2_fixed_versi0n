﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.DashboardDetenidos
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass id)
        {
            DbCommand cmd = db.GetStoredProcCommand("DashboardDetenidosG_GetAll");
            return cmd;

        }
    }
}
