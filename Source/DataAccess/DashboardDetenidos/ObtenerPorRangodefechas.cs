﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;


namespace DataAccess.DashboardDetenidos
{
    class ObtenerPorRangodefechas : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] fechas)
        {
            DbCommand cmd = db.GetStoredProcCommand("DashboardDetenidosG_Getbydates");
            db.AddInParameter(cmd, "_Fechainicial", DbType.Date,Convert.ToDateTime( fechas[0]));
            db.AddInParameter(cmd, "_Fechafinal", DbType.Date,Convert.ToDateTime( fechas[1]));
            return cmd;
        }
    }
}
