﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DashboardDetenidos
{
    public interface IDashboardDetenidos
    {
        List<Entity.DashboarDetenidos> GetAll();
        List<Entity.DashboarDetenidos> GetbyDates(string[]fechas);
        List<Entity.DashboarDetenidos> ObtenerPorAnio(int Anio);
        List<Entity.DashboarDetenidos> ObtenerEnTrabajoSocail(int Anio);

    }
}
