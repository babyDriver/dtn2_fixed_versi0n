﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DashboardDetenidos
{
    public class ServicioDashboardDetenidos : ClsAbstractService<Entity.DashboarDetenidos>, IDashboardDetenidos
    {
        public ServicioDashboardDetenidos(string dataBase)
            : base(dataBase)
        {
        }
        public new List<DashboarDetenidos> GetAll()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaDashboardDetenidos(), new Entity.NullClass());
        }

        public List<DashboarDetenidos> GetbyDates(string[] fechas)
        {
            ISelectFactory<string []> select = new ObtenerPorRangodefechas();
            return GetAll(select, new FabricaDashboardDetenidos(), fechas);
        }

        public List<DashboarDetenidos> ObtenerEnTrabajoSocail(int Anio)
        {
            ISelectFactory<int> select = new ObtenerEnTrabajoSocial();
            return GetAll(select, new FabricaDashboardDetenidos(), Anio);
        }

        public List<DashboarDetenidos> ObtenerPorAnio(int Anio)
        {
            ISelectFactory<int> select = new ObtenerPorAnio();
            return GetAll(select, new FabricaDashboardDetenidos(), Anio);
        }
    }
}
