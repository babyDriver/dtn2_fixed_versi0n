﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.DashboardDetenidos
{
    public class ObtenerPorAnio : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Anio)
        {
            DbCommand cmd = db.GetStoredProcCommand("DashboardDetenidosG_GetByAnio");
            db.AddInParameter(cmd, "_Anio", DbType.Int32,Anio);
            return cmd;
        }
    }
}
