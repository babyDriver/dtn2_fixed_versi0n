﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DataAccess.DashboardDetenidos
{
    public class ObtenerEnTrabajoSocial : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("DashboardDetenidosG_GetByTrabajosocial_SP");
            db.AddInParameter(cmd, "_Anio", DbType.Int32, id);
            return cmd;

        }
    }
}