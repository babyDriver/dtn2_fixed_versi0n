﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DashboardDetenidos
{
    class FabricaDashboardDetenidos : IEntityFactory<Entity.DashboarDetenidos>
    {
        public DashboarDetenidos ConstructEntity(IDataReader dr)
        {
            var item = new Entity.DashboarDetenidos();

            var index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalledetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("SexoId");
            if (!dr.IsDBNull(index))
            {
                item.SexoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                item.Estatus = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TrabajosocialId");
            if (!dr.IsDBNull(index))
            {
                item.TrabajosocialId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("MovimentoceldaId");
            if (!dr.IsDBNull(index))
            {
                item.MovimentoceldaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TipoMovimientoCelda");
            if (!dr.IsDBNull(index))
            {
                item.TipoMovimientoCelda = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("EstatusMovimiento");
            if (!dr.IsDBNull(index))
            {
                item.EstatusMovimiento = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }

            return item;
        }
    }
}
