﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.Huella
{
    public class ServicioHuella : ClsAbstractService<Entity.Huella>, IHuella
    {
        public ServicioHuella(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.Huella huella)
        {
            IUpdateFactory<Entity.Huella> update = new Actualizar();
            Update(update, huella);
        }

        public int Guardar(Entity.Huella huella)
        {
            IInsertFactory<Entity.Huella> insert = new Guardar();
            return Insert(insert, huella);
        }

        public List<Entity.Huella> ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetAll(select, new FabricaHuella(), Id);
        }

        public Entity.Huella ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaHuella(), Id);
        }
    }
}
