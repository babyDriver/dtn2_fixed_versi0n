﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.Huella
{
    public class Actualizar : IUpdateFactory<Entity.Huella>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Huella huella)
        {
            DbCommand cmd = db.GetStoredProcCommand("Huella_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, huella.Id);
            db.AddInParameter(cmd, "_Mano", DbType.String, huella.Mano);
            db.AddInParameter(cmd, "_Dedo", DbType.String, huella.Dedo);
            db.AddInParameter(cmd, "_Fotografia", DbType.String, huella.Fotografia);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, huella.DetenidoId);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, huella.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int32, huella.Habilitado);

            return cmd;
        }
    }
}
