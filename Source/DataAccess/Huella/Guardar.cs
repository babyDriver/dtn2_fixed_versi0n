﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.Huella
{
    public class Guardar : IInsertFactory<Entity.Huella>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Huella huella)
        {
            DbCommand cmd = db.GetStoredProcCommand("Huella_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_Id", DbType.Int32, huella.Id);
            db.AddInParameter(cmd, "_Mano", DbType.String, huella.Mano);
            db.AddInParameter(cmd, "_Dedo", DbType.String, huella.Dedo);
            db.AddInParameter(cmd, "_Fotografia", DbType.String, huella.Fotografia);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, huella.DetenidoId);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, huella.Activo);

            return cmd;
        }
    }
}
