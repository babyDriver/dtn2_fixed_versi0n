﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.Huella
{
    public class FabricaHuella : IEntityFactory<Entity.Huella>
    {


        public Entity.Huella ConstructEntity(System.Data.IDataReader dr)
        {
            var huella = new Entity.Huella();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                huella.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                huella.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Mano");
            if (!dr.IsDBNull(index))
            {
                huella.Mano = dr.GetString(index);
            }

            index = dr.GetOrdinal("Dedo");
            if (!dr.IsDBNull(index))
            {
                huella.Dedo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fotografia");
            if (!dr.IsDBNull(index))
            {
                huella.Fotografia = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                huella.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                huella.Habilitado = dr.GetInt32(index);
            }


            return huella;
        }
    }
}