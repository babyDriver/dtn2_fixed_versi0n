﻿using System;
using System.Collections.Generic;

namespace DataAccess.Huella
{
    public interface IHuella
    {
        List<Entity.Huella> ObtenerByDetenidoId(int Id);
        int Guardar(Entity.Huella huella);
        void Actualizar(Entity.Huella huella);
        Entity.Huella ObtenerById(int Id);
    }
}