﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.ContratoCliente
{
    public class ServicioContratoCliente : ClsAbstractService<Entity.ContratoCliente>, IContratoCliente
    {
        public ServicioContratoCliente(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.ContratoCliente ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerByUsuarioId();
            return GetByKey(select, new FabricaContratoCliente(), Id);
        }
    }
}
