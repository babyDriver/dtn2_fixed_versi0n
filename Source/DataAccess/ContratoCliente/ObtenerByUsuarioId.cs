﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ContratoCliente
{
    public class ObtenerByUsuarioId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("ContratoCliente_GetByUsuarioId_SP");
            db.AddInParameter(cmd, "Id", DbType.Int32, id);
            return cmd;
        }
    }
}
