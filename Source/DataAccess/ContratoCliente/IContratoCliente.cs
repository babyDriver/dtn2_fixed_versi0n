﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ContratoCliente
{
    public interface IContratoCliente
    {
        Entity.ContratoCliente ObtenerById(int Id);
    }
}
