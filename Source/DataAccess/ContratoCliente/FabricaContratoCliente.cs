﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ContratoCliente
{
    public class FabricaContratoCliente : IEntityFactory<Entity.ContratoCliente>
    {
        public Entity.ContratoCliente ConstructEntity(System.Data.IDataReader dr)
        {
            var contratoCliente = new Entity.ContratoCliente();

            var index = dr.GetOrdinal("contratoId");
            if (!dr.IsDBNull(index))
            {
                contratoCliente.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("clienteId");
            if (!dr.IsDBNull(index))
            {
                contratoCliente.ClienteId = dr.GetInt32(index);
            }            

            return contratoCliente;
        }
    }
}
