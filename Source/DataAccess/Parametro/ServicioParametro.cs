﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.Parametro
{
  public  class ServicioParametro : ClsAbstractService<Entity.Parametro>, IParametro
    {
        public ServicioParametro(string dataBase)
           : base(dataBase)
        {
        }

        public List<Entity.Parametro> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaParametro(), new Entity.NullClass());
        }
    }
}
