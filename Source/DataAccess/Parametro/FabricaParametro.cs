﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.Parametro
{
    public class FabricaParametro : IEntityFactory<Entity.Parametro>
    {
        public Entity.Parametro ConstructEntity(IDataReader dr)
        {
            var parametro = new Entity.Parametro();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                parametro.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("NombreParametro");
            if (!dr.IsDBNull(index))
            {
                parametro.NombreParametro = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                parametro.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                parametro.FechaHora = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("TipoParametroId");
            if (!dr.IsDBNull(index))
            {
                parametro.TipoParametroId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("NombreParametro");
            if (!dr.IsDBNull(index))
            {
                parametro.NombreParametro = dr.GetString(index);
            }
            return parametro;

        }
    }
}
