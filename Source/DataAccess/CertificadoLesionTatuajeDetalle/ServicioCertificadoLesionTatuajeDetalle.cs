﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.CertificadoLesionTatuajeDetalle
{
    public class ServicioCertificadoLesionTatuajeDetalle : ClsAbstractService<Entity.CertificadoLesionTatuajesDetalle>, ICertificadoLesionTatuajeDetalle
    {
        public ServicioCertificadoLesionTatuajeDetalle(string dataBase)
  : base(dataBase)
        {
        }
        public int Guardar(CertificadoLesionTatuajesDetalle certificadoLesionTatuajesDetalle)
        {
            IInsertFactory<Entity.CertificadoLesionTatuajesDetalle> insert = new Guardar();
            return Insert(insert, certificadoLesionTatuajesDetalle);
        
        }

        public List<CertificadoLesionTatuajesDetalle> ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetAll(select, new FabricaCertificadoLesionTatuajeDetalle(), Id);
        }
    }
}
