﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.CertificadoLesionTatuajeDetalle
{
    public class ObtenerPorId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionTatuajesDetalle_ById_SP");
            db.AddInParameter(cmd, "_certificado_LesionTatuajesId", DbType.Int32, Id);
            return cmd;
        }

    }
}
