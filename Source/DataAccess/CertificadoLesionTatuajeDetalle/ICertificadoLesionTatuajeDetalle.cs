﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionTatuajeDetalle
{
   public interface ICertificadoLesionTatuajeDetalle
    {
        int Guardar(Entity.CertificadoLesionTatuajesDetalle certificadoLesionTatuajesDetalle);
        List<Entity.CertificadoLesionTatuajesDetalle> ObtenerPorId(int Id);
    }
}
