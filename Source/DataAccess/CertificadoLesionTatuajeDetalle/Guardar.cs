﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoLesionTatuajeDetalle
{
    public class Guardar : IInsertFactory<Entity.CertificadoLesionTatuajesDetalle>
    {
        public DbCommand ConstructInsertCommand(Database db, CertificadoLesionTatuajesDetalle entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionTatuajesdetalle_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_certificado_LesionTatuajesId", DbType.Int32, entity.certificado_LesionTatuajesId);
            db.AddInParameter(cmd, "_TatuajeId", DbType.Int32, entity.TatuajeId);
            return cmd;
        }
    }
}
