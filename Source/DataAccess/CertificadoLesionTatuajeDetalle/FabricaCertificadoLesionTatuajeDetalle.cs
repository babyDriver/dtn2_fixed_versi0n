﻿using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.CertificadoLesionTatuajeDetalle
{
    public class FabricaCertificadoLesionTatuajeDetalle : IEntityFactory<Entity.CertificadoLesionTatuajesDetalle>
    {
        public CertificadoLesionTatuajesDetalle ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoLesionTatuajesDetalle();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("certificado_LesionTatuajesId");
            if (!dr.IsDBNull(index))
            {
                item.certificado_LesionTatuajesId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TatuajeId");
            if (!dr.IsDBNull(index))
            {
                item.TatuajeId = dr.GetInt32(index);
            }
            return item;
        }
    }
}
