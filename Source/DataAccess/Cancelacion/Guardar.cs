﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Cancelacion
{
    public class Guardar : IInsertFactory<Entity.Cancelacion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Cancelacion item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Cancelacion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId.ToString());
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_Folio", DbType.Int32, item.Folio);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);
            db.AddInParameter(cmd, "_Total", DbType.Decimal, item.Total);
            db.AddInParameter(cmd, "_PersonaQuePaga", DbType.String, item.PersonaQuePaga);
            db.AddInParameter(cmd, "_Observacion", DbType.String, item.Observacion);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_FolioMovimiento", DbType.Int32, item.FolioMovimiento);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_TipoContrato", DbType.String, item.TipoContrato);

            return cmd;
        }
    }
}
