﻿using DataAccess.Interfaces;

namespace DataAccess.Cancelacion
{
    public class FabricaCancelacion : IEntityFactory<Entity.Cancelacion>
    {
        public Entity.Cancelacion ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.Cancelacion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                item.Tipo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Total");
            if (!dr.IsDBNull(index))
            {
                item.Total = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("PersonaQuePaga");
            if (!dr.IsDBNull(index))
            {
                item.PersonaQuePaga = dr.GetString(index);
            }

            index = dr.GetOrdinal("Observacion");
            if (!dr.IsDBNull(index))
            {
                item.Observacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FolioMovimiento");
            if (!dr.IsDBNull(index))
            {
                item.FolioMovimiento = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TipoContrato");
            if (!dr.IsDBNull(index))
            {
                item.TipoContrato = dr.GetString(index);
            }

            return item;
        }
    }
}
