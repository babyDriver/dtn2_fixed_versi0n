﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Cancelacion
{
    public class ObtenerByTipoAndFolio : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Cancelacion_GetByTipoAndFolio_SP");
            db.AddInParameter(cmd, "_Tipo", DbType.String, parametros[0].ToString());
            db.AddInParameter(cmd, "_Id", DbType.Int32, Convert.ToInt32(parametros[1]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(parametros[2]));
            db.AddInParameter(cmd, "_TipoContrato", DbType.String, parametros[3].ToString());

            return cmd;
        }
    }
}
