﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Cancelacion
{
    public class ObtenerTodosByDate : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Cancelacion_GetAllByDate_SP");
            db.AddInParameter(cmd, "_FechaInicio", DbType.DateTime, Convert.ToDateTime(parametros[0]));
            db.AddInParameter(cmd, "_FechaFin", DbType.DateTime, Convert.ToDateTime(parametros[1]));            

            return cmd;
        }
    }
}
