﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Cancelacion
{
    public class ServicioCancelacion : ClsAbstractService<Entity.Cancelacion>, ICancelacion
    {
        public ServicioCancelacion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Cancelacion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaCancelacion(), new Entity.NullClass());
        }

        public int Guardar(Entity.Cancelacion item)
        {
            IInsertFactory<Entity.Cancelacion> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.Cancelacion ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaCancelacion(), trackingId);
        }

        public Entity.Cancelacion ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaCancelacion(), id);
        }

        public Entity.Cancelacion ObtenerFolio(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerFolio();
            return GetByKey(select, new FabricaCancelacion(), parametros);
        }

        public void Actualizar(Entity.Cancelacion item)
        {
            IUpdateFactory<Entity.Cancelacion> update = new Actualizar();
            Update(update, item);
        }

        public List<Entity.Cancelacion> ObtenerAllByDate(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodosByDate();
            return GetAll(select, new FabricaCancelacion(), parametros);
        }

        public Entity.Cancelacion ObtenerByTipoAndFolio(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByTipoAndFolio();
            return GetByKey(select, new FabricaCancelacion(), parametros);
        }
    }
}
