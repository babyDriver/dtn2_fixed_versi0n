﻿using System;
using System.Collections.Generic;

namespace DataAccess.Cancelacion
{
    public interface ICancelacion
    {
        int Guardar(Entity.Cancelacion item);
        void Actualizar(Entity.Cancelacion item);
        Entity.Cancelacion ObtenerById(int id);
        Entity.Cancelacion ObtenerByTrackingId(Guid guid);
        List<Entity.Cancelacion> ObtenerTodos();
        Entity.Cancelacion ObtenerFolio(object[] parametros);
        List<Entity.Cancelacion> ObtenerAllByDate(object[] parametros);
        Entity.Cancelacion ObtenerByTipoAndFolio(object[] parametros);
    }
}
