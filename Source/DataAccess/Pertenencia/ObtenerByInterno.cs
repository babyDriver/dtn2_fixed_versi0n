﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Pertenencia
{
    public class ObtenerByInterno : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Pertenencia_GetByInterno_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, id);
            return cmd;
        }
    }
}
