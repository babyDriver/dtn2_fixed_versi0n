﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Pertenencia
{
    public class Guardar : IInsertFactory<Entity.Pertenencia>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Pertenencia pertenencia)
        {
            DbCommand cmd = db.GetStoredProcCommand("Pertenencia_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, pertenencia.TrackingId);            
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, pertenencia.InternoId);
            db.AddInParameter(cmd, "_Pertenencia", DbType.String, pertenencia.PertenenciaNombre);
            db.AddInParameter(cmd, "_Observacion", DbType.String, pertenencia.Observacion);
            db.AddInParameter(cmd, "_Clasificacion", DbType.Int32, pertenencia.Clasificacion);
            db.AddInParameter(cmd, "_Bolsa", DbType.Int32, pertenencia.Bolsa);
            db.AddInParameter(cmd, "_Cantidad", DbType.Int32, pertenencia.Cantidad);
            db.AddInParameter(cmd, "_Fotografia", DbType.String, pertenencia.Fotografia);            
            db.AddInParameter(cmd, "_Estatus", DbType.Int32, pertenencia.Estatus);            
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, pertenencia.CreadoPor);
            db.AddInParameter(cmd, "_CasilleroId", DbType.Int32, pertenencia.CasilleroId);

            return cmd;
        }
    }
}
