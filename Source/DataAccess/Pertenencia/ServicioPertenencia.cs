﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Pertenencia
{
    public class ServicioPertenencia : ClsAbstractService<Entity.Pertenencia>, IPertenencia
    {
        public ServicioPertenencia(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Pertenencia> ObtenerByMultipleIds(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByMultipleIds();
            return GetAll(select, new FabricaPertenencia(), parametros);
        }

        public List<Entity.Pertenencia> ObtenerByInterno(int id)
        {
            ISelectFactory<int> select = new ObtenerByInterno();
            return GetAll(select, new FabricaPertenencia(), id);
        }

        public Entity.Pertenencia ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaPertenencia(), id);
        }

        public Entity.Pertenencia ObtenerById(Guid tracking)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaPertenencia(), tracking);
        }

        public void Actualizar(Entity.Pertenencia pertenencia)
        {
            IUpdateFactory<Entity.Pertenencia> update = new Actualizar();
            Update(update, pertenencia);
        }

        public int Guardar(Entity.Pertenencia pertenencia)
        {
            IInsertFactory<Entity.Pertenencia> insert = new Guardar();
            return Insert(insert, pertenencia);
        }
    }
}
