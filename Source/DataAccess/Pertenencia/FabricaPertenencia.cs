﻿using DataAccess.Interfaces;

namespace DataAccess.Pertenencia
{
    public class FabricaPertenencia : IEntityFactory<Entity.Pertenencia>
    {
        public Entity.Pertenencia ConstructEntity(System.Data.IDataReader dr)
        {
            var pertenencia = new Entity.Pertenencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                pertenencia.TrackingId = dr.GetGuid(index);
            }            

            index = dr.GetOrdinal("InternoId");
            if (!dr.IsDBNull(index))
            {
                pertenencia.InternoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Pertenencia");
            if (!dr.IsDBNull(index))
            {
                pertenencia.PertenenciaNombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Observacion");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Observacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Clasificacion");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Clasificacion = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Bolsa");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Bolsa = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Cantidad");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Cantidad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fotografia");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Fotografia = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                pertenencia.Estatus = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                pertenencia.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CasilleroId");
            if (!dr.IsDBNull(index))
            {
                pertenencia.CasilleroId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaEntrada");
            if (!dr.IsDBNull(index))
            {
                pertenencia.FechaEntrada = dr.GetDateTime(index);
            }

            return pertenencia;
        }
    }
}
