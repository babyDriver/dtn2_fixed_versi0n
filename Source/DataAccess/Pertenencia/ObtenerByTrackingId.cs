﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Pertenencia
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("Pertenencia_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.String, tracking.ToString());
            return cmd;
        }
    }
}
