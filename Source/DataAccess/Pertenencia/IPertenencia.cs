﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Pertenencia
{
    public interface IPertenencia
    {
        List<Entity.Pertenencia> ObtenerByInterno(int id);
        Entity.Pertenencia ObtenerById(int id);
        void Actualizar(Entity.Pertenencia pertenencia);
        int Guardar(Entity.Pertenencia pertenencia);
        List<Entity.Pertenencia> ObtenerByMultipleIds(object[] parametros);
    }
}
