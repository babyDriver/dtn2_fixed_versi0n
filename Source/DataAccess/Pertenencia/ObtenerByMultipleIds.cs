﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.Pertenencia
{
    public class ObtenerByMultipleIds : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Pertenencia_GetByMultipleIds_SP");
            db.AddInParameter(cmd, "_Ids", DbType.String, parametros[0].ToString());
            db.AddInParameter(cmd, "_IdInterno", DbType.Int32, Convert.ToInt32(parametros[1]));
            return cmd;
        }
    }
}
