﻿using System.Collections.Generic;

namespace DataAccess.WSAMunicipio
{
    public interface IWSAMunicipio
    {
        List<Entity.WSAMunicipio> ObtenerTodos();
        Entity.WSAMunicipio ObtenerPorId(int estadoId);
        int Guardar(Entity.WSAMunicipio municipio);
        void Actualizar(Entity.WSAMunicipio municipio);
    }
}
