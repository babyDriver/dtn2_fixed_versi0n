﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAMunicipio
{
    public class Guardar : IInsertFactory<Entity.WSAMunicipio>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSAMunicipio entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAMunicipio_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_idMunicipio", DbType.Int32, entity.IdMunicipio);
            db.AddInParameter(cmd, "_nombreMunicipio", DbType.String, entity.NombreMunicipio);
            db.AddInParameter(cmd, "_idEstado", DbType.Int32, entity.IdEstado);

            return cmd;
        }
    }
}
