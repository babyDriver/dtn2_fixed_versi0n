﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.WSAMunicipio
{
    public class ServicioWSAMunicipio : ClsAbstractService<Entity.WSAMunicipio>, IWSAMunicipio
    {
        public ServicioWSAMunicipio(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.WSAMunicipio> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaWSAMunicipio(), new Entity.NullClass());
        }

        public Entity.WSAMunicipio ObtenerPorId(int estadoId)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSAMunicipio(), estadoId);
        }

        public int Guardar(Entity.WSAMunicipio municipio)
        {
            IInsertFactory<Entity.WSAMunicipio> insert = new Guardar();
            return Insert(insert, municipio);
        }

        public void Actualizar(Entity.WSAMunicipio municipio)
        {
            IUpdateFactory<Entity.WSAMunicipio> update = new Actualizar();
            Update(update, municipio);
        }
    }
}
