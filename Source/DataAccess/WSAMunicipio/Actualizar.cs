﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAMunicipio
{
    public class Actualizar : IUpdateFactory<Entity.WSAMunicipio>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.WSAMunicipio entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAMunicipio_Update_SP");
            db.AddInParameter(cmd, "_idMunicipio", DbType.Int32, entity.IdMunicipio);
            db.AddInParameter(cmd, "_nombreMunicipio", DbType.String, entity.NombreMunicipio);
            db.AddInParameter(cmd, "_idEstado", DbType.Int32, entity.IdEstado);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);

            return cmd;
        }
    }
}
