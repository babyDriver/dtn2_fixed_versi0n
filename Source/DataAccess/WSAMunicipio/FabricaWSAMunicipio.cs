﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.WSAMunicipio
{
    public class FabricaWSAMunicipio : IEntityFactory<Entity.WSAMunicipio>
    {
        public Entity.WSAMunicipio ConstructEntity(IDataReader dr)
        {
            var municipio = new Entity.WSAMunicipio();

            var index = dr.GetOrdinal("idMunicipio");
            if (!dr.IsDBNull(index))
            {
                municipio.IdMunicipio = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("nombreMunicipio");
            if (!dr.IsDBNull(index))
            {
                municipio.NombreMunicipio = dr.GetString(index);
            }

            index = dr.GetOrdinal("idEstado");
            if (!dr.IsDBNull(index))
            {
                municipio.IdEstado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaModificacion");
            if (!dr.IsDBNull(index))
            {
                municipio.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                municipio.Activo = dr.GetBoolean(index);
            }

            return municipio;
        }
    }
}
