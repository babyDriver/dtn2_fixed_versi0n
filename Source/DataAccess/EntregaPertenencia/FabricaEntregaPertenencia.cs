﻿using DataAccess.Interfaces;

namespace DataAccess.EntregaPertenencia
{
    public class FabricaEntregaPertenencia : IEntityFactory<Entity.EntregaPertenencia>
    {
        public Entity.EntregaPertenencia ConstructEntity(System.Data.IDataReader dr)
        {
            var entrega = new Entity.EntregaPertenencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                entrega.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                entrega.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                entrega.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                entrega.Descripcion = dr.GetString(index);
            }            

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                entrega.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                entrega.CreadoPor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                entrega.Habilitado = dr.GetBoolean(index);
            }

            return entrega;
        }
    }
}
