﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.EntregaPertenencia
{
    public class ServicioEntregaPertenencia : ClsAbstractService<Entity.EntregaPertenencia>, IEntregaPertenencia
    {
        public ServicioEntregaPertenencia(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.EntregaPertenencia> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaEntregaPertenencia(), new Entity.NullClass());
        }
    }
}
