﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.EntregaPertenencia
{
    public interface IEntregaPertenencia
    {
        List<Entity.EntregaPertenencia> ObtenerTodos();
    }
}
