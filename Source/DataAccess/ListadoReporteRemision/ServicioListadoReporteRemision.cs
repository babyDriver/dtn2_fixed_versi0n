﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoReporteRemision
{
    public class ServicioListadoReporteRemision : ClsAbstractService<Entity.ListadoReporteRemision>, IListadoReporteRemision
    {
        public ServicioListadoReporteRemision(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoReporteRemision> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoReporteRemision(), parametros);
        }
    }
}
