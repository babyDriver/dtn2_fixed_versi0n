﻿using System.Collections.Generic;

namespace DataAccess.ListadoReporteRemision
{
    public interface IListadoReporteRemision
    {
        List<Entity.ListadoReporteRemision> ObtenerTodos(object[] data);
    }
}
