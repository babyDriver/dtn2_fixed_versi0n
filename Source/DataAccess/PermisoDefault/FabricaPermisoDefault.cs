﻿using DataAccess.Interfaces;

namespace DataAccess.PermisoDefault
{
    public class FabricaPermisoDefault : IEntityFactory<Entity.PermisoDefault>
    {
        public Entity.PermisoDefault ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.PermisoDefault();

            var index = dr.GetOrdinal("RolId");
            if (!dr.IsDBNull(index))
            {
                item.RolId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("PantallaId");
            if (!dr.IsDBNull(index))
            {
                item.PantallaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("PermisoId");
            if (!dr.IsDBNull(index))
            {
                item.PermisoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            item.Habilitado = true;

            return item;
        }
    }
}
