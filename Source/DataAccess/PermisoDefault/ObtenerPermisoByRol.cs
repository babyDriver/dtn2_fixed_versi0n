﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.PermisoDefault
{
    public class ObtenerPermisoByRol : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("rol_permisodefault_GetByRol_SP");
            db.AddInParameter(cmd, "_rolId", DbType.Int32, id);
            return cmd;
        }
    }
}
