﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.PermisoDefault
{
    public class ServicioPermisoDefault : ClsAbstractService<Entity.PermisoDefault>, IPermisoDefault
    {
        public ServicioPermisoDefault(string dataBase)
               : base(dataBase)
        {
        }

        public List<Entity.PermisoDefault> ObtenerPermisoByRol(int id)
        {
            ISelectFactory<int> select = new ObtenerPermisoByRol();
            return GetAll(select, new FabricaPermisoDefault(), id);
        }
    }
}
