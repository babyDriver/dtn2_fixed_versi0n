﻿using System.Collections.Generic;

namespace DataAccess.PermisoDefault
{
    public interface IPermisoDefault
    {
        List<Entity.PermisoDefault> ObtenerPermisoByRol(int id);
    }
}
