﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.WSAEstado
{
    public class FabricaWSAEstado : IEntityFactory<Entity.WSAEstado>
    {
        public Entity.WSAEstado ConstructEntity(IDataReader dr)
        {
            var estado = new Entity.WSAEstado();

            var index = dr.GetOrdinal("IdEstado");
            if (!dr.IsDBNull(index))
            {
                estado.IdEstado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("nombreEstado");
            if (!dr.IsDBNull(index))
            {
                estado.NombreEstado = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaModificacion");
            if (!dr.IsDBNull(index))
            {
                estado.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                estado.Activo = dr.GetBoolean(index);
            }

            return estado;
        }
    }
}
