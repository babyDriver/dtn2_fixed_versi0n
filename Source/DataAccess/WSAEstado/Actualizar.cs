﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAEstado
{
    public class Actualizar : IUpdateFactory<Entity.WSAEstado>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.WSAEstado entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAEstado_Update_SP");
            db.AddInParameter(cmd, "_idEstado", DbType.Int32, entity.IdEstado);
            db.AddInParameter(cmd, "_nombreEstado", DbType.String, entity.NombreEstado);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);

            return cmd;
        }
    }
}
