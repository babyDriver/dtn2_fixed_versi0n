﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.WSAEstado
{
    public class ServicioWSAEstado : ClsAbstractService<Entity.WSAEstado>, IWSAEstado
    {
        public ServicioWSAEstado(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.WSAEstado> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaWSAEstado(), new Entity.NullClass());
        }

        public Entity.WSAEstado ObtenerPorId(int estadoId)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSAEstado(), estadoId);
        }

        public int Guardar(Entity.WSAEstado estado)
        {
            IInsertFactory<Entity.WSAEstado> insert = new Guardar();
            return Insert(insert, estado);
        }

        public void Actualizar(Entity.WSAEstado estado)
        {
            IUpdateFactory<Entity.WSAEstado> update = new Actualizar();
            Update(update, estado);
        }
    }
}
