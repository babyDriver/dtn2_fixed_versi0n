﻿using System.Collections.Generic;

namespace DataAccess.WSAEstado
{
    public interface IWSAEstado
    {
        List<Entity.WSAEstado> ObtenerTodos();
        Entity.WSAEstado ObtenerPorId(int estadoId);
        int Guardar(Entity.WSAEstado estado);
        void Actualizar(Entity.WSAEstado estado);
    }
}
