﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAEstado
{
    public class Guardar : IInsertFactory<Entity.WSAEstado>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSAEstado entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAEstado_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_idEstado", DbType.Int32, entity.IdEstado);
            db.AddInParameter(cmd, "_nombreEstado", DbType.String, entity.NombreEstado);

            return cmd;
        }
    }
}
