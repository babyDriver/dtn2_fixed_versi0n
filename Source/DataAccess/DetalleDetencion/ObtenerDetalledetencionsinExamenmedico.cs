﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;


namespace DataAccess.DetalleDetencion
{
    public class ObtenerDetalledetencionsinExamenmedico : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("DetalleDetencion_GetAllsinexamenmedico_SP");
            return cmd;
        }
    }
}
