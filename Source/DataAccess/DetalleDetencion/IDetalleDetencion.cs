﻿using System;
using System.Collections.Generic;

namespace DataAccess.DetalleDetencion
{
    public interface IDetalleDetencion
    {
        List<Entity.DetalleDetencion> ObtenerTodos();
        Entity.DetalleDetencion ObtenerById(int Id);
        List<Entity.DetalleDetencion> ObtenerByDetenidoId(int Id);
        Entity.DetalleDetencion ObtenerByDetenidoIdActivo(string[] Id);
        Entity.DetalleDetencion ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.DetalleDetencion estatus);
        void Actualizar(Entity.DetalleDetencion estatus);
        void ActualizarActivos(Entity.DetalleDetencion internoid);
        Entity.DetalleDetencion ObtenerByExpediente(string expediente);
        List<Entity.DetalleDetencion> ObtenerTodossinExamenMedico();
    }
}