﻿using DataAccess.Interfaces;


namespace DataAccess.DetalleDetencion
{
    public class FabricaEstatusInterno : IEntityFactory<Entity.DetalleDetencion>
    {


        public Entity.DetalleDetencion ConstructEntity(System.Data.IDataReader dr)
        {
            var estatus = new Entity.DetalleDetencion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                estatus.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                estatus.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                estatus.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                estatus.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("ExpedienteAdm");
            if (!dr.IsDBNull(index))
            {
                estatus.ExpedienteAdm = dr.GetString(index);
            }

            index = dr.GetOrdinal("NCP");
            if (!dr.IsDBNull(index))
            {
                estatus.NCP = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                estatus.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CentroId");
            if (!dr.IsDBNull(index))
            {
                estatus.CentroId = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                estatus.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                estatus.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                estatus.Estatus = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                estatus.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                estatus.Tipo = dr.GetString(index);
            }
            index = dr.GetOrdinal("Lesion_visible");
            if (!dr.IsDBNull(index))
            {
                estatus.Lesion_visible = dr.GetBoolean(index);
            }


            index = dr.GetOrdinal("Detalledetencionanioregistro");
            if (!dr.IsDBNull(index))
            {
                estatus.Detalledetencionanioregistro = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Detalldetencionmesregistro");
            if (!dr.IsDBNull(index))
            {
                estatus.Detalldetencionmesregistro = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("DetalledetencionSexoId");
            if (!dr.IsDBNull(index))
            {
                estatus.DetalledetencionSexoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("NombreDetenido");
            if (!dr.IsDBNull(index))
            {
                estatus.NombreDetenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("APaternoDetenido");
            if (!dr.IsDBNull(index))
            {
                estatus.APaternoDetenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("AMaternoDetenido");
            if (!dr.IsDBNull(index))
            {
                estatus.AMaternoDetenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetalledetencionSexoId");
            if (!dr.IsDBNull(index))
            {
                estatus.DetalledetencionSexoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetalledetencionEdad");
            if (!dr.IsDBNull(index))
            {
                estatus.DetalledetencionEdad = dr.GetInt32(index);
            }

            return estatus;
        }
    }
}