﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.DetalleDetencion
{
    public class ObtenerByTrackignId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("DetalleDetencion_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, tracking);
            return cmd;
        }
    }
}
