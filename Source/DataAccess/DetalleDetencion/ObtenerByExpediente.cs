﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.DetalleDetencion
{
    public class ObtenerByExpediente : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string expediente)
        {
            DbCommand cmd = db.GetStoredProcCommand("DetalleDetencion_GetByExpediente_SP");
            db.AddInParameter(cmd, "_Expediente", DbType.String, expediente);
            return cmd;
        }
    }
}
