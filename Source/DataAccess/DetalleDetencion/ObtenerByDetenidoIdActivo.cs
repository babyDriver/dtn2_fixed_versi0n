﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.DetalleDetencion
{
    public class ObtenerByDetenidoIdActivo : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] id)
        {
            DbCommand cmd = db.GetStoredProcCommand("DetalleDetencion_GetByDetenidoIdActivo_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, Convert.ToInt32(id[0]));
            db.AddInParameter(cmd, "_Activo", DbType.Int32, Convert.ToBoolean(id[1]));
            return cmd;
        }
    }
}
