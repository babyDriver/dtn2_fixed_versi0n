﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.DetalleDetencion
{
    public class ObtenerPorparametros : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] id)
        {
            DbCommand cmd = db.GetStoredProcCommand("DetalleDetencion_GetByFILTER_SP");
            db.AddInParameter(cmd, "_Nombre", DbType.String, Convert.ToString(id[0]));
            db.AddInParameter(cmd, "_Paterno", DbType.String, Convert.ToString(id[1]));
            db.AddInParameter(cmd, "_Materno", DbType.String, Convert.ToString(id[2]));
            db.AddInParameter(cmd, "_Alias", DbType.String, Convert.ToString(id[3]));
            db.AddInParameter(cmd, "_Criterio", DbType.Int32, Convert.ToInt32(id[4]));
            return cmd;
        }
    }
}
