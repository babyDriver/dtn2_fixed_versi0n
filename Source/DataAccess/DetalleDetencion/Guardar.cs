﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.DetalleDetencion
{
    public class Guardar : IInsertFactory<Entity.DetalleDetencion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.DetalleDetencion estatus)
        {
            DbCommand cmd = db.GetStoredProcCommand("DetalleDetencion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, estatus.TrackingId);
            db.AddInParameter(cmd, "_Expediente", DbType.String, estatus.Expediente);
            if (estatus.Fecha != null)
                db.AddInParameter(cmd, "_Fecha", DbType.DateTime, estatus.Fecha);
            else
                db.AddInParameter(cmd, "_Fecha", DbType.DateTime, null);
            db.AddInParameter(cmd, "_ExpedienteAdm", DbType.String, estatus.ExpedienteAdm);
            db.AddInParameter(cmd, "_NCP", DbType.String, estatus.NCP);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, estatus.Activo);

            if(estatus.CentroId != null)
                db.AddInParameter(cmd, "_CentroId", DbType.Int32, estatus.CentroId);
            else
                db.AddInParameter(cmd, "_CentroId", DbType.Int32, null);

            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, estatus.DetenidoId);
            db.AddInParameter(cmd, "_Estatus", DbType.Int32, estatus.Estatus);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, estatus.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, estatus.Tipo);
            db.AddInParameter(cmd, "_Lesion_visible", DbType.Boolean, estatus.Lesion_visible);
            db.AddInParameter(cmd, "_Detalledetencionanioregistro", DbType.Int32, estatus.Detalledetencionanioregistro);
            db.AddInParameter(cmd, "_Detalldetencionmesregistro", DbType.Int32, estatus.Detalldetencionmesregistro);
            db.AddInParameter(cmd, "_DetalledetencionSexoId", DbType.Int32, estatus.DetalledetencionSexoId);
            db.AddInParameter(cmd, "_NombreDetenido", DbType.String, estatus.NombreDetenido);
            db.AddInParameter(cmd, "_APaternoDetenido", DbType.String, estatus.APaternoDetenido);
            db.AddInParameter(cmd, "_AMaternoDetenido", DbType.String, estatus.AMaternoDetenido);
            db.AddInParameter(cmd, "_DetalledetencionEdad", DbType.Int32, estatus.DetalledetencionEdad);

            return cmd;
        }
    }
}
