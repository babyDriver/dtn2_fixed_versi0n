﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DetalleDetencion
{
    public class ServicioDetalleDetencion : ClsAbstractService<Entity.DetalleDetencion>, IDetalleDetencion
    {
        public ServicioDetalleDetencion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.DetalleDetencion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaEstatusInterno(), new Entity.NullClass());
        }


        public List<Entity.DetalleDetencion> ObtenerporParametros(string[] filtro)
        {
            ISelectFactory<string[]> select = new ObtenerPorparametros();
            return GetAll(select, new FabricaEstatusInterno(), filtro);
        }

        public Entity.DetalleDetencion ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaEstatusInterno(), userId);
        }

        public Entity.DetalleDetencion ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaEstatusInterno(), Id);
        }

        public List<Entity.DetalleDetencion> ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetAll(select, new FabricaEstatusInterno(), Id);
        }

        public Entity.DetalleDetencion ObtenerByDetenidoIdActivo(string[] Id)
        {
            ISelectFactory<string[]> select = new ObtenerByDetenidoIdActivo();
            return GetByKey(select, new FabricaEstatusInterno(), Id);
        }

        public int Guardar(Entity.DetalleDetencion estatus)
        {
            IInsertFactory<Entity.DetalleDetencion> insert = new Guardar();
            return Insert(insert, estatus);
        }

        public void Actualizar(Entity.DetalleDetencion estatus)
        {
            IUpdateFactory<Entity.DetalleDetencion> update = new Actualizar();
            Update(update, estatus);
        }

        public void ActualizarActivos(Entity.DetalleDetencion estatus)
        {
            IUpdateFactory<Entity.DetalleDetencion> update = new ActualizarActivos();
            Update(update, estatus);
        }

        public Entity.DetalleDetencion ObtenerByExpediente(string expediente)
        {
            ISelectFactory<string> select = new ObtenerByExpediente();
            return GetByKey(select, new FabricaEstatusInterno(), expediente);
        }
        public List<Entity.DetalleDetencion> ObtenerTodossinExamenMedico()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerDetalledetencionsinExamenmedico();
            return GetAll(select, new FabricaEstatusInterno(), new Entity.NullClass());
        }
    }
}