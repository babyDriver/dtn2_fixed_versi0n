﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.DetalleDetencion
{
    public class ActualizarActivos : IUpdateFactory<Entity.DetalleDetencion>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.DetalleDetencion internoid)
        {
            DbCommand cmd = db.GetStoredProcCommand("DetalleDetencion_UpdateActivos_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, internoid.DetenidoId);

           

            return cmd;
        }
    }
}
