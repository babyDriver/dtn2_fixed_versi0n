﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DatosDetenido
{
    public interface IDatosDetenido
    {
         Entity.DatosDetenido GetDatosDetenido(int DetalledetencionId);
    }
}
