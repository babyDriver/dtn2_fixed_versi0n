﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.DatosDetenido
{
    public class ObtenerDatosdetenidos : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetalledetebcionId)
        {
            DbCommand cmd = db.GetStoredProcCommand("GetDatosDetenido_SP");
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, DetalledetebcionId);
            return cmd;
        }
    }
}
