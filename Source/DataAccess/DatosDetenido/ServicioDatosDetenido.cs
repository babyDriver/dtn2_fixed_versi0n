﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;


namespace DataAccess.DatosDetenido
{
    public class ServicioDatosDetenido : ClsAbstractService<Entity.DatosDetenido>, IDatosDetenido
    {
        public ServicioDatosDetenido(string dataBase)
           : base(dataBase)
        {
        }
        public Entity.DatosDetenido GetDatosDetenido(int DetalledetencionId)
        {
            ISelectFactory<int> select = new ObtenerDatosdetenidos();
            return GetByKey(select, new FabricaDatosDetenido(), DetalledetencionId);
        }
    }
}
