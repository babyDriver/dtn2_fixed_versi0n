﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DatosDetenido
{
    public class FabricaDatosDetenido : IEntityFactory<Entity.DatosDetenido>
    {
        public Entity.DatosDetenido ConstructEntity(IDataReader dr)
        {
            var datosDetenido = new Entity.DatosDetenido();

            var index = dr.GetOrdinal("CentroReclusion");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.CentroReclusion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Nombredetenido");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Nombredetenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Sexo = dr.GetString(index);
            }

            index = dr.GetOrdinal("EstadoCivil");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.EstadoCivil = dr.GetString(index);
            }
            index = dr.GetOrdinal("Escolaridad");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Escolaridad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nacionalidad");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Nacionalidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Domicilio");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Domicilio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Colonia");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Colonia = dr.GetString(index);
            }

            index = dr.GetOrdinal("Municipio");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Municipio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Telefono");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Telefono = dr.GetString(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Responsableunidad");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Responsableunidad = dr.GetString(index);
            }
            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("InformacionDetencionId");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.InformacionDetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Lugardetencion");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Lugardetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Coloniadetencion");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Coloniadetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("CalificacionId");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.CalificacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Soloarresto");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Soloarresto = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Totalhoras");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Totalhoras = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Totalapagar");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Totalapagar = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Fundamento");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Fundamento = dr.GetString(index);
            }

            index = dr.GetOrdinal("Razon");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Razon = dr.GetString(index);
            }

            index = dr.GetOrdinal("UnidadId");
            
                if (!dr.IsDBNull(index))
            {
                datosDetenido.UnidadId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Situacion");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Situacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Usuariocalifico");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.Usuariocalifico = dr.GetString(index);
            }

            index = dr.GetOrdinal("FolioEvento");
            if (!dr.IsDBNull(index))
            {
                datosDetenido.FolioEvento = dr.GetString(index);
            }

            return datosDetenido;
        }
    }
}
