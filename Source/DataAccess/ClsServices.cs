﻿#region Declare services

#endregion

namespace DataAccess
{
    public class ClsServices
    {
        #region Singleton

        static ClsServices attInstance = null;
        static readonly object attSynlock = new object();

        private ClsServices()
        {
        }

        public static ClsServices Instance
        {
            get
            {
                lock (attSynlock)
                {
                    if (attInstance != null)
                        return attInstance;

                    attInstance = new ClsServices();
                    return attInstance;
                }
            }
        }

        #endregion

        #region Access repositories



        #endregion
    }
}

