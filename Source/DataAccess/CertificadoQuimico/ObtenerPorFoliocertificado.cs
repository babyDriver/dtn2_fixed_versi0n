﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DataAccess.CertificadoQuimico
{
    public class ObtenerPorFoliocertificado : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] FolioCertificado)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_Quimico_ObtenerPorTFolioCertificado_SP");
            db.AddInParameter(cmd, "_Foliocertificado", DbType.String, FolioCertificado[0]);
            db.AddInParameter(cmd, "_ContratoId", DbType.String, FolioCertificado[1]);
            return cmd;
        }
    }
}
