﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoQuimico
{
    public interface ICertificadoQuimico
    {
        List<Entity.CertificadoQuimico> ObtenerTodos();
        Entity.CertificadoQuimico ObtenerById(int Id);

        Entity.CertificadoQuimico ObtenerByTrackingId(string trackingid);
        int Guardar(Entity.CertificadoQuimico certificadoQuimico);
        void Actualizar(Entity.CertificadoQuimico certificadoQuimico);
        Entity.CertificadoQuimico ObtenerByFolioCertificadoId(string[] Id);
    }
}
