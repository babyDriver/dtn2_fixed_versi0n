﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CertificadoQuimico
{
    public class FabricaCertificadoQuimico : IEntityFactory<Entity.CertificadoQuimico>
    {
        public Entity.CertificadoQuimico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoQuimico();

            var index = dr.GetOrdinal("Id");

            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetString(index);
            }
            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Fecha_toma");
            if (!dr.IsDBNull(index))
            {
                item.Fecha_toma = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Fecha_proceso");
            if (!dr.IsDBNull(index))
            {
                item.Fecha_proceso = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("EtanolId");
            if (!dr.IsDBNull(index))
            {
                item.EtanolId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Grado");
            if (!dr.IsDBNull(index))
            {
                item.Grado = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("BenzodiapinaId");
            if (!dr.IsDBNull(index))
            {
                item.BenzodiapinaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("AnfetaminaId");
            if (!dr.IsDBNull(index))
            {
                item.AnfetaminaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("CannabisId");
            if (!dr.IsDBNull(index))
            {
                item.CannabisId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("CocainaId");
            if (!dr.IsDBNull(index))
            {
                item.CocainaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("ExtasisId");
            if (!dr.IsDBNull(index))
            {
                item.ExtasisId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Dimension");
            if (!dr.IsDBNull(index))
            {
                item.Dimension = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Axysm");
            if (!dr.IsDBNull(index))
            {
                item.Axysm = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Architect");
            if (!dr.IsDBNull(index))
            {
                item.Architect = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("VivaE");
            if (!dr.IsDBNull(index))
            {
                item.VivaE = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("EquipoautilizarId");
            if (!dr.IsDBNull(index))
            {
                item.EquipoautilizarId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaRegistro");
            if (!dr.IsDBNull(index))
            {
                item.FechaRegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Registradopor");
            if (!dr.IsDBNull(index))
            {
                item.Registradopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Modificadopor");
            if (!dr.IsDBNull(index))
            {
                item.Modificadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fechaultimamodificacion");
            if (!dr.IsDBNull(index))
            {
                item.Fechaultimamodificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Foliocertificado");
            if (!dr.IsDBNull(index))
            {
                item.Foliocertificado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoId = dr.GetInt32(index);
            }
            return item;
        }
    }
}
