﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.CertificadoQuimico
{
    public class ServicioCertificadoQuimico : ClsAbstractService<Entity.CertificadoQuimico>, ICertificadoQuimico
    {
        public ServicioCertificadoQuimico(string dataBase)
         : base(dataBase)
        {
        }

        public void Actualizar(Entity.CertificadoQuimico certificadoQuimico)
        {
            IUpdateFactory<Entity.CertificadoQuimico> update = new Actualizar();
            Update(update, certificadoQuimico);
        }

        public int Guardar(Entity.CertificadoQuimico certificadoQuimico)
        {
            IInsertFactory<Entity.CertificadoQuimico> insert = new Guardar();
            return Insert(insert, certificadoQuimico);
        }

        public Entity.CertificadoQuimico ObtenerByFolioCertificadoId(string[] Id)
        {
            ISelectFactory<string[]> select = new ObtenerPorFoliocertificado();
            return GetByKey(select, new FabricaCertificadoQuimico(), Id);
        }

        public Entity.CertificadoQuimico ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoQuimico(), Id); 
        }

        public Entity.CertificadoQuimico ObtenerByTrackingId(string trackingid)
        {
            ISelectFactory<string> select = new ObtenerPorTrackingId();
            return GetByKey(select, new FabricaCertificadoQuimico(), trackingid);
        }

        public List<Entity.CertificadoQuimico> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodo();
            return GetAll(select, new FabricaCertificadoQuimico(), new Entity.NullClass());
        }
    }
}
