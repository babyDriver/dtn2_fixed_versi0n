﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoQuimico
{
    public class Actualizar : IUpdateFactory<Entity.CertificadoQuimico>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.CertificadoQuimico certificadoQuimico)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_quimico_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, certificadoQuimico.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, certificadoQuimico.TrackingId);
            db.AddInParameter(cmd, "_Folio", DbType.Int32, certificadoQuimico.Folio);
            db.AddInParameter(cmd, "_Fecha_toma", DbType.DateTime, certificadoQuimico.Fecha_toma);
            db.AddInParameter(cmd, "_Fecha_proceso", DbType.DateTime, certificadoQuimico.Fecha_proceso);
            db.AddInParameter(cmd, "_EtanolId", DbType.Int32, certificadoQuimico.EtanolId);
            db.AddInParameter(cmd, "_Grado", DbType.Int32, certificadoQuimico.Grado);
            db.AddInParameter(cmd, "_BenzodiapinaId", DbType.Int32, certificadoQuimico.BenzodiapinaId);
            db.AddInParameter(cmd, "_AnfetaminaId", DbType.Int32, certificadoQuimico.AnfetaminaId);
            db.AddInParameter(cmd, "_CannabisId", DbType.Int32, certificadoQuimico.CannabisId);
            db.AddInParameter(cmd, "_CocainaId", DbType.Int32, certificadoQuimico.CocainaId);
            db.AddInParameter(cmd, "_ExtasisId", DbType.Int32, certificadoQuimico.ExtasisId);
            db.AddInParameter(cmd, "_Dimension", DbType.Int16, certificadoQuimico.Dimension);
            db.AddInParameter(cmd, "_Axysm", DbType.Int16, certificadoQuimico.Axysm);
            db.AddInParameter(cmd, "_Architect", DbType.Int16, certificadoQuimico.Architect);
            db.AddInParameter(cmd, "_VivaE", DbType.Int16, certificadoQuimico.VivaE);
            db.AddInParameter(cmd, "_EquipoautilizarId", DbType.Int32, certificadoQuimico.EquipoautilizarId);
            db.AddInParameter(cmd, "_FechaRegistro", DbType.DateTime, certificadoQuimico.FechaRegistro);
            db.AddInParameter(cmd, "_Registradopor", DbType.Int32, certificadoQuimico.Registradopor);
            db.AddInParameter(cmd, "_Modificadopor", DbType.Int32, certificadoQuimico.Modificadopor);
            db.AddInParameter(cmd, "_Fechaultimamodificacion", DbType.DateTime, certificadoQuimico.Fechaultimamodificacion);
            db.AddInParameter(cmd, "_Foliocertificado", DbType.String, certificadoQuimico.Foliocertificado);

            return cmd;

        }
    }
}
