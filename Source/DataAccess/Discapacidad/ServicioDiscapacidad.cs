﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;

namespace DataAccess.Discapacidad
{
    public class ServicioDiscapacidad : ClsAbstractService<Entity.Discapacidad>, IDiscapacidad
    {
        public ServicioDiscapacidad(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.Discapacidad ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaDiscapacidad(), trackingId);
        }

        public List<Entity.Discapacidad> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaDiscapacidad(), new Entity.NullClass());
        }


        public Entity.Discapacidad ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaDiscapacidad(), Id);
        }


        public Entity.Discapacidad ObtenerByNombreTipo(object[] parametros)
        {

            ISelectFactory<object[]> select = new ObtenerByNombreTipo();
            return GetByKey(select, new FabricaDiscapacidad(), parametros);
        }

        public int Guardar(Entity.Discapacidad item)
        {
            IInsertFactory<Entity.Discapacidad> insert = new Guardar();
            return Insert(insert, item);
        }
         
        public void Actualizar(Entity.Discapacidad item)
        {
            IUpdateFactory<Entity.Discapacidad> update = new Actualizar();
            Update(update, item);
        }
    }
}
