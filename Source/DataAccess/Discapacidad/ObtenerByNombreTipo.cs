﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Discapacidad
{
    public class ObtenerByNombreTipo : ISelectFactory<object[]>
    {

        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {

            DbCommand cmd = db.GetStoredProcCommand("Discapacidad_GetByDescripcionTipo_SP");
            db.AddInParameter(cmd, "_Descripcion", DbType.String, parametros[0].ToString());
            db.AddInParameter(cmd, "_TipoDiscapacidadId", DbType.String, System.Convert.ToInt32(parametros[1]));
            return cmd;
        }

    
    }
}
