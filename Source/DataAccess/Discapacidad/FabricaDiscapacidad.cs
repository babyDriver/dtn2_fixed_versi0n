﻿
using System;
using System.Data;
using DataAccess.Interfaces;
using DataAccess.Catalogo;


namespace DataAccess.Discapacidad
{
    public class FabricaDiscapacidad : IEntityFactory<Entity.Discapacidad>
    {
        private static ServicioCatalogo servicioCatalogo = new ServicioCatalogo("SQLConnectionString");

        public Entity.Discapacidad ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.Discapacidad();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }


        

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("TipoDiscapacidadId");
            if (!dr.IsDBNull(index))
            {
                var parametros = new object[2];
                parametros[0] = dr.GetInt32(index);
                parametros[1] = Entity.TipoDeCatalogo.tipo_discapacidad;
                 var _tipo= servicioCatalogo.ObtenerPorId(parametros);
                 if (_tipo != null)
                     item.TipoDiscapacidad = _tipo;
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }


            return item;
        }
    }
}
