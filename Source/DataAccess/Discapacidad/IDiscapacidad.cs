﻿using System.Collections.Generic;
using System;

namespace DataAccess.Discapacidad
{
    public interface IDiscapacidad
    {
        Entity.Discapacidad ObtenerById(int Id);
        Entity.Discapacidad ObtenerByNombreTipo(object[] parametros);
        List<Entity.Discapacidad> ObtenerTodos();
        int Guardar(Entity.Discapacidad item);
        void Actualizar(Entity.Discapacidad item);
        Entity.Discapacidad ObtenerByTrackingId(Guid trackingId);
    }
}
