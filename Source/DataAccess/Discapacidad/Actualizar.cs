﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Discapacidad
{
    public class Actualizar : IUpdateFactory<Entity.Discapacidad>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Discapacidad item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Discapacidad_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, item.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_TipoDiscapacidadId", DbType.Int32, item.TipoDiscapacidad.Id);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, item.Creadopor);


            return cmd;
        }
    }
}
