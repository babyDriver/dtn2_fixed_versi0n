﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Discapacidad
{
    public class Guardar : IInsertFactory<Entity.Discapacidad>
    {

        public DbCommand ConstructInsertCommand(Database db, Entity.Discapacidad item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Discapacidad_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, item.Descripcion);        
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_TipoDiscapacidadId", DbType.Int32, item.TipoDiscapacidad.Id);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, item.Creadopor);

            return cmd;
        }
    }
}
