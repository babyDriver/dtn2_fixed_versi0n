﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.WSALugar
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int LugarId)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSALugar_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, LugarId);
            return cmd;
        }
    }
}
