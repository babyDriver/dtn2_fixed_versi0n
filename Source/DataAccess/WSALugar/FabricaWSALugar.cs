﻿using DataAccess.Interfaces;
using System.Data;


namespace DataAccess.WSALugar
{
    public class FabricaWSALugar : IEntityFactory<Entity.WSALugar>
    {
        public Entity.WSALugar ConstructEntity(IDataReader dr)
        {
             var lugar = new Entity.WSALugar();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                lugar.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                lugar.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("IdEstado");
            if (!dr.IsDBNull(index))
            {
                lugar.IdEstado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IdMunicipio");
            if (!dr.IsDBNull(index))
            {
                lugar.IdMunicipio = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IdColonia");
            if (!dr.IsDBNull(index))
            {
                lugar.IdColonia = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Numero");
            if (!dr.IsDBNull(index))
            {
                lugar.Numero = dr.GetString(index);
            }

            index = dr.GetOrdinal("EntreCalle");
            if (!dr.IsDBNull(index))
            {
                lugar.EntreCalle = dr.GetString(index);
            }

            index = dr.GetOrdinal("Sector");
            if (!dr.IsDBNull(index))
            {
                lugar.Sector = dr.GetString(index);
            }

            index = dr.GetOrdinal("Latitud");
            if (!dr.IsDBNull(index))
            {
                lugar.Latitud = dr.GetString(index);
            }

            index = dr.GetOrdinal("Longitud");
            if (!dr.IsDBNull(index))
            {
                lugar.Longitud = dr.GetString(index);
            }

            index = dr.GetOrdinal("Ycalle");
            if (!dr.IsDBNull(index))
            {
                lugar.Ycalle = dr.GetString(index);
            }

            return lugar;
        }
    }
}
