﻿using DataAccess.Interfaces;

namespace DataAccess.WSALugar
{
    public class ServicioWSALugar : ClsAbstractService<Entity.WSALugar>, IWSALugar
    {
        public ServicioWSALugar(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.WSALugar item)
        {
            IInsertFactory<Entity.WSALugar> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.WSALugar ObtenerPorId(int EventoIdWS)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSALugar(), EventoIdWS);
        }
    }
}
