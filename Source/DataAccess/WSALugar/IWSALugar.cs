﻿namespace DataAccess.WSALugar
{
    public interface IWSALugar
    {
        int Guardar(Entity.WSALugar item);
        Entity.WSALugar ObtenerPorId(int LugarId);
    }
}
