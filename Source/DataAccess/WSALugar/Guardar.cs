﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.WSALugar
{
    public class Guardar : IInsertFactory<Entity.WSALugar>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSALugar item)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSALugar_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId.ToString());
            db.AddInParameter(cmd, "_IdEstado", DbType.Int32, item.IdEstado);
            db.AddInParameter(cmd, "_IdMunicipio", DbType.Int32, item.IdMunicipio);
            db.AddInParameter(cmd, "_IdColonia", DbType.Int32, item.IdColonia);
            db.AddInParameter(cmd, "_Numero", DbType.String, item.Numero);
            db.AddInParameter(cmd, "_EntreCalle", DbType.String, item.EntreCalle);
            db.AddInParameter(cmd, "_Sector", DbType.String, item.Sector);
            db.AddInParameter(cmd, "_Latitud", DbType.String, item.Latitud);
            db.AddInParameter(cmd, "_Longitud", DbType.String, item.Longitud);
            db.AddInParameter(cmd, "_Ycalle", DbType.String, item.Ycalle);            

            return cmd;
        }
    }
}
