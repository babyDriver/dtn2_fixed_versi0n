﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Evento
{
    public class ServicioFolio : ClsAbstractService<Entity.Contador>, IFolio
    {
        public ServicioFolio(string dataBase)
           : base(dataBase)
        {
        }

        public Entity.Contador ObtenerFolio(int contract)
        {
            ISelectFactory<int> select = new ObtenerFolio();
            return GetByKey(select, new FabricaFolio(), contract);
        }
        
    }
}
