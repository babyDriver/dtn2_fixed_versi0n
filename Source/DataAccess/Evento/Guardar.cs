﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Evento
{
    public class Guardar : IInsertFactory<Entity.Evento>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Evento item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Evento_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_LlamadaId", DbType.Int32, item.LlamadaId);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, item.Descripcion);
            db.AddInParameter(cmd, "_Lugar", DbType.String, item.Lugar);
            //db.AddInParameter(cmd, "_Municipio", DbType.Int32, item.Municipio);
            //db.AddInParameter(cmd, "_Sector", DbType.Int32, item.Sector);
            //db.AddInParameter(cmd, "_Colonia", DbType.Int32, item.Colonia);
            db.AddInParameter(cmd, "_Folio", DbType.String, item.Folio);
            db.AddInParameter(cmd, "_HoraYFecha", DbType.DateTime, item.HoraYFecha);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_ColoniaId", DbType.Int32, item.ColoniaId);
            db.AddInParameter(cmd, "_NumeroDetenidos", DbType.Int32, item.NumeroDetenidos);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);
            db.AddInParameter(cmd, "_Latitud", DbType.String, item.Latitud);
            db.AddInParameter(cmd, "_Longitud", DbType.String, item.Longitud);
            db.AddInParameter(cmd, "_IdEventoWS", DbType.Int32, item.IdEventoWS);
            db.AddInParameter(cmd, "_Eventoanioregistro", DbType.Int32, item.Eventoanioregistro);
            db.AddInParameter(cmd, "_Eventomesregistro", DbType.Int32, item.Eventomesregistro);
            db.AddInParameter(cmd, "_MotivoId", DbType.Int32, item.MotivoId);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            return cmd;
        }
    }
}
