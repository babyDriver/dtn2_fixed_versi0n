﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Evento
{
    public class ServicioEvento : ClsAbstractService<Entity.Evento>, IEvento
    {
        public ServicioEvento(string dataBase)
           : base(dataBase)
        {
        }

        public void Actualizar(Entity.Evento item)
        {
            IUpdateFactory<Entity.Evento> update = new Actualizar();
            Update(update, item);
        }

        public int Guardar(Entity.Evento item)
        {
            IInsertFactory<Entity.Evento> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.Evento ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaEvento(), Id);
        }

        public Entity.Evento ObtenerByTrackingId(Guid Id)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaEvento(), Id);
        }

        public List<Entity.Evento> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaEvento(), new Entity.NullClass());
        }
        
        public List<Entity.Evento> ObtenerByLlamadaId(int id)
        {
            ISelectFactory<int> select = new ObtenerByLlamadaId();
            return GetAll(select, new FabricaEvento(), id);
        }

        public List<Entity.Evento> ObtenerByFechas(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByFechas();
            return GetAll(select, new FabricaEvento(),parametros);
        }
    }
}
