﻿using DataAccess.Interfaces;
using Entity;
using System.Data;

namespace DataAccess.Evento
{
    public class FabricaFolio : IEntityFactory<Contador>
    {
        public Contador ConstructEntity(IDataReader dr)
        {
            var item = new Contador();

            var index = dr.GetOrdinal("total");
            if (!dr.IsDBNull(index))
            {
                item.total = dr.GetInt32(index);
            }
            return item;
        }
    }
}
