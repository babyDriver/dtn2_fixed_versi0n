﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.Evento
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Evento_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, id);
            return cmd;
        }
    }
}
