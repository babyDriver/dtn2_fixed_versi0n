﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Evento
{
    public class FabricaEvento : IEntityFactory<Entity.Evento>
    {
        public Entity.Evento ConstructEntity(IDataReader dr)
        {
            var item = new Entity.Evento();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("LlamadaId");
            if (!dr.IsDBNull(index))
            {
                item.LlamadaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                item.Lugar = dr.GetString(index);
            }            

            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetString(index);
            }

            index = dr.GetOrdinal("HoraYFecha");
            if (!dr.IsDBNull(index))
            {
                item.HoraYFecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("ColoniaId");
            if (!dr.IsDBNull(index))
            {
                item.ColoniaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("NumeroDetenidos");
            if (!dr.IsDBNull(index))
            {
                item.NumeroDetenidos = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                item.Tipo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Latitud");
            if (!dr.IsDBNull(index))
            {
                item.Latitud = dr.GetString(index);
            }

            index = dr.GetOrdinal("Longitud");
            if (!dr.IsDBNull(index))
            {
                item.Longitud = dr.GetString(index);
            }

            index = dr.GetOrdinal("IdEventoWS");
            if (!dr.IsDBNull(index))
            {
                item.IdEventoWS = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Eventoanioregistro");
            if (!dr.IsDBNull(index))
            {
                item.Eventoanioregistro = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Eventomesregistro");
            if (!dr.IsDBNull(index))
            {
                item.Eventomesregistro = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("MotivoId");
            if (!dr.IsDBNull(index))
            {
                item.MotivoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            return item;
        }
    }
}
