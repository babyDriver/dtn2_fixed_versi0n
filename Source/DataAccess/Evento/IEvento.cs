﻿using System;
using System.Collections.Generic;

namespace DataAccess.Evento
{
    public interface IEvento
    {
        List<Entity.Evento> ObtenerTodos();
        Entity.Evento ObtenerById(int Id);
        Entity.Evento ObtenerByTrackingId(Guid Id);
        int Guardar(Entity.Evento item);
        void Actualizar(Entity.Evento item);
        List<Entity.Evento> ObtenerByLlamadaId(int id);
        List<Entity.Evento> ObtenerByFechas(object[] parametros);
    }
}
