﻿using System;
using System.Collections.Generic;

namespace DataAccess.Evento
{
    public interface IFolio
    {
        Entity.Contador ObtenerFolio(int contract);
       
    }
}
