﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ObtenerFolio : ISelectFactory<int>
{
    public DbCommand ConstructSelectCommand(Database db, int contract)
    {
        DbCommand cmd = db.GetStoredProcCommand("Evento_GetFolio_SP");
        db.AddInParameter(cmd, "_IdContrato", DbType.Int32, contract);
        return cmd;
    }
}