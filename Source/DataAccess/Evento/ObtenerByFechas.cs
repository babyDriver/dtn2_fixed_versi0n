﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Evento
{
    public class ObtenerByFechas : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Evento_GetByDate_SP");
            db.AddInParameter(cmd, "_FechaInicio", DbType.DateTime, Convert.ToDateTime(parametros[0]));
            db.AddInParameter(cmd, "_FechaFin", DbType.DateTime, Convert.ToDateTime(parametros[1]));
            return cmd;
        }
    }
}
