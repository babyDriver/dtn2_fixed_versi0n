﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.ListadoPertenencia
{
    public class ServicioListadoPertenencia : ClsAbstractService<Entity.ListadoPertenencia>, IListadoPertenencia
    {
        public ServicioListadoPertenencia(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoPertenencia> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoPertenencia(), parametros);
        }

        public List<Entity.ListadoPertenencia> ObtenerTodosConPertenencias(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodosConPertenencias();
            return GetAll(select, new FabricaListadoPertenencia(), parametros);
        }

        public List<Entity.ListadoPertenencia> ObtenerDetenidosSinPertenencias(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerDetenidosSinPertenencias();
            return GetAll(select, new FabricaListadoPertenencia(), parametros);
        }

        public List<Entity.ListadoPertenencia> ObtenerDetenidosConPertenencias(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerDetenidosConPertenencias();
            return GetAll(select, new FabricaListadoPertenencia(), parametros);
        }

        public List<Entity.ListadoPertenencia> ObtenerLibresConPertenencias(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerLibresConPertenencias();
            return GetAll(select, new FabricaListadoPertenencia(), parametros);
        }
    }
}
