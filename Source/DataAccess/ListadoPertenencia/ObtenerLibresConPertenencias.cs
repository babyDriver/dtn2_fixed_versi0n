﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.ListadoPertenencia
{
    public class ObtenerLibresConPertenencias : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ListadoPertenencias_GetLibresConPertenencias_SP");
            db.AddInParameter(cmd, "_tipo", DbType.String, parametros[0].ToString());
            db.AddInParameter(cmd, "_contratoId", DbType.Int32, Convert.ToInt32(parametros[1].ToString()));
            db.AddInParameter(cmd, "_id", DbType.Int32, Convert.ToInt32(parametros[2].ToString()));
            db.AddInParameter(cmd, "_anio", DbType.Int32, Convert.ToInt32(parametros[3].ToString()));

            return cmd;
        }
    }
}
