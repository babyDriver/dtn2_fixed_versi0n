﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ListadoPertenencia
{
    public interface IListadoPertenencia
    {
        List<Entity.ListadoPertenencia> ObtenerTodos(object[] parametros);
        List<Entity.ListadoPertenencia> ObtenerTodosConPertenencias(object[] parametros);
        List<Entity.ListadoPertenencia> ObtenerDetenidosSinPertenencias(object[] parametros);
        List<Entity.ListadoPertenencia> ObtenerDetenidosConPertenencias(object[] parametros);
        List<Entity.ListadoPertenencia> ObtenerLibresConPertenencias(object[] parametros);
    }
}
