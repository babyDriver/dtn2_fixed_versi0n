﻿using DataAccess.Interfaces;

namespace DataAccess.ListadoPertenencia
{
    public class FabricaListadoPertenencia : IEntityFactory<Entity.ListadoPertenencia>
    {
        public Entity.ListadoPertenencia ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.ListadoPertenencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("IdEstatusInterno");
            if (!dr.IsDBNull(index))
            {
                item.IdEstatusInterno = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("PertenenciaId");
            if (!dr.IsDBNull(index))
            {
                item.PertenenciaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Clasificacion");
            if (!dr.IsDBNull(index))
            {
                item.Clasificacion = dr.GetString(index);
            }
            else
            {
                item.Clasificacion = "Sin pertenencia";
            }

            index = dr.GetOrdinal("Pertenencia");
            if (!dr.IsDBNull(index))
            {
                item.Pertenencia = dr.GetString(index);
            }
            else
            {
                item.Pertenencia = "Sin pertenencia";
            }

            index = dr.GetOrdinal("Cantidad");
            if (!dr.IsDBNull(index))
            {
                item.Cantidad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Bolsa");
            if (!dr.IsDBNull(index))
            {
                item.Bolsa = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                item.Estatus = dr.GetString(index);
            }
            else
            {
                item.Estatus = "Sin pertenencia";
            }

            index = dr.GetOrdinal("ClasificacionId");
            if (!dr.IsDBNull(index))
            {
                item.ClasificacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("NombreUsuario");
            if (!dr.IsDBNull(index))
            {
                item.NombreUsuario = dr.GetString(index);
            }

            index = dr.GetOrdinal("ApellidoPaterno");
            if (!dr.IsDBNull(index))
            {
                item.ApellidoPaterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("ApellidoMaterno");
            if (!dr.IsDBNull(index))
            {
                item.ApellidoMaterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Observacion");
            if (!dr.IsDBNull(index))
            {
                item.Observacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fotografia");
            if (!dr.IsDBNull(index))
            {
                item.Fotografia = dr.GetString(index);
            }

            index = dr.GetOrdinal("UsusarioUsuario");
            if (!dr.IsDBNull(index))
            {
                item.UsuarioUsuario = dr.GetString(index);
            }
                return item;
        }
    }
}
