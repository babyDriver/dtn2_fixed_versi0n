﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.EventoReciente
{
    public class ObtenerByHora : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Evento_reciente_GetByHora_SP");
            db.AddInParameter(cmd, "_Hora", DbType.Int32, id);

            return cmd;
        }
    }
}
