﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entity;

namespace DataAccess.EventoReciente
{
    public class ServicioEventoReciente : ClsAbstractService<Entity.EventoReciente>, IEventoReciente
    {
        public ServicioEventoReciente(string dataBase)
            : base(dataBase)
        {
        }
        public void Actualizar(Entity.EventoReciente eventoReciente)
        {
            IUpdateFactory<Entity.EventoReciente> update = new Actualizar();
            Update(update, eventoReciente);
        }

        public int Guardar(Entity.EventoReciente eventoReciente)
        {
            IInsertFactory<Entity.EventoReciente> insert = new Guardar();
            return Insert(insert, eventoReciente);
        }

        public Entity.EventoReciente ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaEventoReciente(), id);
        }

        public Entity.EventoReciente ObtenerByHora(int hora)
        {
            ISelectFactory<int> select = new ObtenerByHora();
            return GetByKey(select, new FabricaEventoReciente(), hora);
        }

        public List<Entity.EventoReciente> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaEventoReciente(), new Entity.NullClass());
        }
    }
}
