﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.EventoReciente
{
    public class FabricaEventoReciente : IEntityFactory<Entity.EventoReciente>
    {
        public Entity.EventoReciente ConstructEntity(IDataReader dr)
        {
            var eventoReciente = new Entity.EventoReciente();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Hora");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.Hora = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.Creadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                eventoReciente.Tipo = dr.GetString(index);
            }

            return eventoReciente;
        }
    }
}
