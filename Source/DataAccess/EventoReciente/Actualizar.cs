﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.EventoReciente
{
    public class Actualizar : IUpdateFactory<Entity.EventoReciente>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.EventoReciente item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Evento_reciente_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_Hora", DbType.Int32, item.Hora);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, item.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.Creadopor);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);

            return cmd;
        }
    }
}
