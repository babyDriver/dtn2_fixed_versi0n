﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.EventoReciente
{
    public interface IEventoReciente
    {
        List<Entity.EventoReciente> ObtenerTodos();
        Entity.EventoReciente ObtenerById(int id);        
        int Guardar(Entity.EventoReciente eventoReciente);
        void Actualizar(Entity.EventoReciente eventoReciente);
    }
}
