﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Smtp
{
    public class FabricaSmtp : IEntityFactory<Entity.Smtp>
    {
        public Entity.Smtp ConstructEntity(IDataReader dr)
        {
            var smtp = new Entity.Smtp();

            var index = dr.GetOrdinal("id");
            if (!dr.IsDBNull(index))
            {
                smtp.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("trackingId");
            if (!dr.IsDBNull(index))
            {
                smtp.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("host");
            if (!dr.IsDBNull(index))
            {
                smtp.Host = dr.GetString(index);
            }

            index = dr.GetOrdinal("password");
            if (!dr.IsDBNull(index))
            {
                smtp.Password = dr.GetString(index);
            }

            index = dr.GetOrdinal("from");
            if (!dr.IsDBNull(index))
            {
                smtp.From = dr.GetString(index);
            }

            index = dr.GetOrdinal("port");
            if (!dr.IsDBNull(index))
            {
                smtp.Port = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("username");
            if (!dr.IsDBNull(index))
            {
                smtp.Username = dr.GetString(index);
            }

            index = dr.GetOrdinal("cc");
            if (!dr.IsDBNull(index))
            {
                smtp.CC = dr.GetString(index);
            }

            index = dr.GetOrdinal("enviarcorreo");
            if (!dr.IsDBNull(index))
            {
                smtp.SendMail = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("fechaRegistro");
            if (!dr.IsDBNull(index))
            {
                smtp.FechaRegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("fechaModificacion");
            if (!dr.IsDBNull(index))
            {
                smtp.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("creadoPor");
            if (!dr.IsDBNull(index))
            {
                smtp.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("modificadoPor");
            if (!dr.IsDBNull(index))
            {
                smtp.ModificadoPor = dr.GetInt32(index);
            }

            return smtp;
        }
    }
}