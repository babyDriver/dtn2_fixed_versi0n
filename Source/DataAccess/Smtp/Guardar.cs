﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Smtp
{
    public class Guardar : IInsertFactory<Entity.Smtp>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Smtp smtp)
        {
            DbCommand cmd = db.GetStoredProcCommand("Configuracionsmtp_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_host", DbType.String, smtp.Host)
                ;
            if (smtp.Port > 0)
                db.AddInParameter(cmd, "_port", DbType.Int32, smtp.Port);
            else
                db.AddInParameter(cmd, "_port", DbType.Int32, null);

            db.AddInParameter(cmd, "_username", DbType.String, smtp.Username);
            db.AddInParameter(cmd, "_password", DbType.String, smtp.Password);
            db.AddInParameter(cmd, "_from", DbType.String, smtp.From);
            db.AddInParameter(cmd, "_enviarcorreo", DbType.Boolean, smtp.SendMail);

            if (string.IsNullOrEmpty(smtp.CC))
                db.AddInParameter(cmd, "_cc", DbType.Int32, null);
            else
                db.AddInParameter(cmd, "_cc", DbType.Int32, smtp.CC);

            if (string.IsNullOrEmpty(smtp.CCO))
                db.AddInParameter(cmd, "_cco", DbType.String, null);
            else
                db.AddInParameter(cmd, "_cco", DbType.String, smtp.CCO);

            if (smtp.CreadoPor != null)
                db.AddInParameter(cmd, "_creadoPor", DbType.Int32, smtp.CreadoPor);
            else
                db.AddInParameter(cmd, "_creadoPor", DbType.Int32, null);

            return cmd;
        }
    }
}
