﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Smtp
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("Configuracionsmtp_ByTrackingId_SP");
            db.AddInParameter(cmd, "_trackingId", DbType.Guid, tracking);
            return cmd;
        }
    }
}
