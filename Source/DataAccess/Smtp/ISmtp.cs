﻿using System;

namespace DataAccess.Smtp
{
    public interface ISmtp
    {
        int Guardar(Entity.Smtp sector);
        void Actualizar(Entity.Smtp sector);
        Entity.Smtp ObtenerConfiguracion();
        Entity.Smtp ObtenerByTrackingId(Guid trackingId);
    }
}