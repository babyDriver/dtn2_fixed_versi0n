﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;

namespace DataAccess.Smtp
{
    public class ServicioSmtp : ClsAbstractService<Entity.Smtp>, ISmtp
    {
        public ServicioSmtp(string dataBase)
            : base(dataBase) 
        {
        }

        public Entity.Smtp ObtenerConfiguracion()
        {
            ISelectFactory <Entity.NullClass> selectFactory = new ObtenerSmtp();
            return GetByKey(selectFactory, new FabricaSmtp(), new Entity.NullClass());
        }

        public int Guardar(Entity.Smtp smtp)
        {
            IInsertFactory<Entity.Smtp> insert = new Guardar();
            return Insert(insert, smtp);
        }

        public void Actualizar(Entity.Smtp smtp)
        {
            IUpdateFactory<Entity.Smtp> update = new Actualizar();
            Update(update, smtp);
        }

        public Entity.Smtp ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaSmtp(), trackingId);
        }
    }
}