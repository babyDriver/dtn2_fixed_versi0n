﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Alias
{
    public class Guardar : IInsertFactory<Entity.Alias>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Alias alias)
        {
            DbCommand cmd = db.GetStoredProcCommand("Alias_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, alias.TrackingId);
            db.AddInParameter(cmd, "_Alias", DbType.String, alias.Nombre);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, alias.Activo);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, alias.DetenidoId);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, alias.Creadopor);

            return cmd;
        }
    }
}
