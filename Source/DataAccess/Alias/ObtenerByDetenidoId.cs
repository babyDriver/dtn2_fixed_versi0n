﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Alias
{
    public class ObtenerByDetenidoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Alias_GetByDetenidoId_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, id);
            return cmd;
        }
    }
}
