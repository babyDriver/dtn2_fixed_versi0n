﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Alias
{
    public class ServicioAlias : ClsAbstractService<Entity.Alias>, IAlias
    {
        public ServicioAlias(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Alias> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaAlias(), new Entity.NullClass());
        }

        public Entity.Alias ObtenerByAlias(Entity.Alias userId)
        {
            ISelectFactory<Entity.Alias> select = new ObtenerByAlias();
            return GetByKey(select, new FabricaAlias(), userId);
        }

        public Entity.Alias ObtenerByTrackingId(Guid trackinid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaAlias(), trackinid);
        }

        public Entity.Alias ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaAlias(), Id);
        }

        public Entity.Alias ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaAlias(), Id);
        }

        public int Guardar(Entity.Alias alias)
        {
            IInsertFactory<Entity.Alias> insert = new Guardar();
            return Insert(insert, alias);
        }

        public void Actualizar(Entity.Alias alias)
        {
            IUpdateFactory<Entity.Alias> update = new Actualizar();
            Update(update, alias);
        }


    }
}