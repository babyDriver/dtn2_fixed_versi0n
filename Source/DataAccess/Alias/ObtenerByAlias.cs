﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Alias
{
    public class ObtenerByAlias : ISelectFactory<Entity.Alias>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.Alias alias)
        {
            DbCommand cmd = db.GetStoredProcCommand("Alias_GetByAlias_SP");
            db.AddInParameter(cmd, "_Alias", DbType.String, alias.Nombre);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, alias.DetenidoId);
            return cmd;
        }
    }
}
