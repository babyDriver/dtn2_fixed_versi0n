﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Alias
{
    public class Actualizar : IUpdateFactory<Entity.Alias>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Alias alias)
        {
            DbCommand cmd = db.GetStoredProcCommand("Alias_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, alias.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, alias.TrackingId);
            db.AddInParameter(cmd, "_Alias", DbType.String, alias.Nombre);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, alias.Activo);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, alias.DetenidoId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int32, alias.Habilitado);

           

            return cmd;
        }
    }
}
