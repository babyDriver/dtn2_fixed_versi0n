﻿using System;
using System.Collections.Generic;

namespace DataAccess.Alias
{
    public interface IAlias
    {
        List<Entity.Alias> ObtenerTodos();
        Entity.Alias ObtenerById(int Id);
        Entity.Alias ObtenerByDetenidoId(int Id);
        Entity.Alias ObtenerByTrackingId(Guid trackingid);
        Entity.Alias ObtenerByAlias(Entity.Alias alias);
        int Guardar(Entity.Alias alias);
        void Actualizar(Entity.Alias alias);
    }
}