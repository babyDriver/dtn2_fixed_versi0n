﻿using DataAccess.Interfaces;


namespace DataAccess.Alias
{
    public class FabricaAlias : IEntityFactory<Entity.Alias>
    {
       

        public Entity.Alias ConstructEntity(System.Data.IDataReader dr)
        {
            var alias = new Entity.Alias();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                alias.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                alias.TrackingId = dr.GetGuid(index);
            }

       
            index = dr.GetOrdinal("Alias");
            if (!dr.IsDBNull(index))
            {
                alias.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                alias.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                alias.DetenidoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if(!dr.IsDBNull(index))
            {
                alias.Habilitado = dr.GetInt32(index);
            }


            return alias;
        }
    }
}