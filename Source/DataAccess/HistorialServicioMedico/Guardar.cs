﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.HistorialServicioMedico
{
    public class Guardar : IInsertFactory<Entity.HistorialServicioMedico>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.HistorialServicioMedico entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("HistorialServicioMedico_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, entity.TrackingId);
            db.AddInParameter(cmd, "_IdHistorial", DbType.Int32, entity.IdHistorial);
            db.AddInParameter(cmd, "_IdServicioMedico", DbType.Int32, entity.IdServicioMedico);

            return cmd;
        }
    }
}
