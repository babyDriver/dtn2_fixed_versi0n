﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.HistorialServicioMedico
{
    public class ServicioHistorialServicioMedico : ClsAbstractService<Entity.HistorialServicioMedico>, IHistorialServicioMedico
    {
        public ServicioHistorialServicioMedico(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.HistorialServicioMedico historialServicioMedico)
        {
            IInsertFactory<Entity.HistorialServicioMedico> insert = new Guardar();
            return Insert(insert, historialServicioMedico);
        }

        public Entity.HistorialServicioMedico ObtenerPorId(int id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaHistorialServicioMedico(), id);
        }

        public Entity.HistorialServicioMedico ObtenerPorIdHistorial(int id)
        {
            ISelectFactory<int> select = new ObtenerPorIdHistorial();
            return GetByKey(select, new FabricaHistorialServicioMedico(), id);
        }

        public List<Entity.HistorialServicioMedico> ObtenerPorIdServicioMedico(int id)
        {
            ISelectFactory<int> select = new ObtenerPorIdServicioMedico();
            return GetAll(select, new FabricaHistorialServicioMedico(), id);
        }
    }
}
