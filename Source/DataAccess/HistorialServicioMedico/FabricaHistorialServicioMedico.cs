﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.HistorialServicioMedico
{
    public class FabricaHistorialServicioMedico : IEntityFactory<Entity.HistorialServicioMedico>
    {
        public Entity.HistorialServicioMedico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.HistorialServicioMedico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("IdHistorial");
            if (!dr.IsDBNull(index))
            {
                item.IdHistorial = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IdServicioMedico");
            if (!dr.IsDBNull(index))
            {
                item.IdServicioMedico = dr.GetInt32(index);
            }

            return item;
        }
    }
}
