﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.HistorialServicioMedico
{
    public class ObtenerPorIdServicioMedico : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("HistorialServicioMedico_GetByIdServicioMedico_SP");
            db.AddInParameter(cmd, "_IdServicioMedico", DbType.Int32, identity);

            return cmd;
        }
    }
}
