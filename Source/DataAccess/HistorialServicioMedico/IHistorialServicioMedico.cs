﻿using System.Collections.Generic;

namespace DataAccess.HistorialServicioMedico
{
    public interface IHistorialServicioMedico
    {
        Entity.HistorialServicioMedico ObtenerPorId(int id);
        Entity.HistorialServicioMedico ObtenerPorIdHistorial(int id);
        List<Entity.HistorialServicioMedico> ObtenerPorIdServicioMedico(int id);
        int Guardar(Entity.HistorialServicioMedico historialServicioMedico);
    }
}
