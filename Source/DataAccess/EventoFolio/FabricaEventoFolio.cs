﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.EventoFolio
{
    public class FabricaEventoFolio : IEntityFactory<Entity.EventoFolio>
    {
        public Entity.EventoFolio ConstructEntity(IDataReader dr)
        {
            var item = new Entity.EventoFolio();

            var index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetInt32(index);
            }

            return item;
        }
    }
}
