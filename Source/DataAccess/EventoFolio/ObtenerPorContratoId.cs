﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.EventoFolio
{
    public class ObtenerPorContratoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("EventoFolio_GetLastByContratoId_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, id);
            return cmd;
        }
    }
}
