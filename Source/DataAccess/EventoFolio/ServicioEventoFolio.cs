﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.EventoFolio
{
    public class ServicioEventoFolio : ClsAbstractService<Entity.EventoFolio>, IEventoFolio
    {
        public ServicioEventoFolio(string dataBase)
           : base(dataBase)
        {
        }

        public Entity.EventoFolio ObtenerPorContratoId(int id)
        {
            ISelectFactory<int> select = new ObtenerPorContratoId();
            return GetByKey(select, new FabricaEventoFolio(), id);
        }
    }
}
