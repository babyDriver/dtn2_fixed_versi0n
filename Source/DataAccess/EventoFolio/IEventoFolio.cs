﻿namespace DataAccess.EventoFolio
{
    public interface IEventoFolio
    {
        Entity.EventoFolio ObtenerPorContratoId(int id);
    }
}
