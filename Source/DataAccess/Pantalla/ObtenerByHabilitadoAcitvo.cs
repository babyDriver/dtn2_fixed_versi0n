﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Pantalla
{
    public class ObtenerByHabilitadoAcitvo : ISelectFactory<Entity.Pantalla>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.Pantalla pantalla)
        {
            DbCommand cmd = db.GetStoredProcCommand("Pantalla_GetByHabilitadoAcitvo_SP");
            db.AddInParameter(cmd, "Activo", DbType.Boolean, pantalla.Activo);
            db.AddInParameter(cmd, "Habilitado", DbType.Boolean, pantalla.Habilitado);
            return cmd;
        }

    }
}
