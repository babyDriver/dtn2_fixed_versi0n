﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Pantalla
{
    public class Guardar : IInsertFactory<Entity.Pantalla>
    {

        public DbCommand ConstructInsertCommand(Database db, Entity.Pantalla pantalla)
        {
            DbCommand cmd = db.GetStoredProcCommand("Pantalla_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_Nombre", DbType.String, pantalla.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, pantalla.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, pantalla.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, pantalla.Habilitado);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, pantalla.TrackingId);

            return cmd;
        }
    }
}
