﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;

namespace DataAccess.Pantalla
{
    public class ServicioPantalla : ClsAbstractService<Entity.Pantalla>, IPantalla
    {
        public ServicioPantalla(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.Pantalla ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaPantalla(), trackingId);
        }

        public List<Entity.Pantalla> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaPantalla(), new Entity.NullClass());
        }

        public List<Entity.Pantalla> ObtenerByHabilitadoAcitvo(Entity.Pantalla pantalla)
        {
            ISelectFactory<Entity.Pantalla> select = new ObtenerByHabilitadoAcitvo();
            return GetAll(select, new FabricaPantalla(), pantalla);
        }

        public Entity.Pantalla ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaPantalla(), Id);
        }

        public int Guardar(Entity.Pantalla pantalla)
        {
            IInsertFactory<Entity.Pantalla> insert = new Guardar();
            return Insert(insert, pantalla);
        }

        public void Actualizar(Entity.Pantalla pantalla)
        {
            IUpdateFactory<Entity.Pantalla> update = new Actualizar();
            Update(update, pantalla);
        }
    }
}
