﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Pantalla
{
    public class Actualizar : IUpdateFactory<Entity.Pantalla>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Pantalla pantalla)
        {
            DbCommand cmd = db.GetStoredProcCommand("Pantalla_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, pantalla.Id);
            db.AddInParameter(cmd, "_Nombre", DbType.Int32, pantalla.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, pantalla.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, pantalla.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, pantalla.Habilitado);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, pantalla.TrackingId);


            return cmd;
        }
    }
}
