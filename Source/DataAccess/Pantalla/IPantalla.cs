﻿using System.Collections.Generic;
using System;

namespace DataAccess.Pantalla
{
    public interface IPantalla
    {
        Entity.Pantalla ObtenerById(int Id);
        List<Entity.Pantalla> ObtenerTodos();
        List<Entity.Pantalla> ObtenerByHabilitadoAcitvo(Entity.Pantalla pantalla);
        int Guardar(Entity.Pantalla pantalla);
        void Actualizar(Entity.Pantalla pantalla);
        Entity.Pantalla ObtenerByTrackingId(Guid trackingId);
    }
}
