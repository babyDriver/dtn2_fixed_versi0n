﻿using DataAccess.Interfaces;

namespace DataAccess.Pantalla
{
    public class FabricaPantalla : IEntityFactory<Entity.Pantalla>
    {
        public Entity.Pantalla ConstructEntity(System.Data.IDataReader dr)
        {
            var pantalla = new Entity.Pantalla();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                pantalla.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                pantalla.Nombre = dr.GetString(index);
            }


            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                pantalla.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                pantalla.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                pantalla.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                pantalla.TrackingId = dr.GetGuid(index);
            }
            return pantalla;
        }
    }
}
