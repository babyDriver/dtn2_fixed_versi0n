﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.ReporteIngresos
{
    public class FabricaReporteIngresos : IEntityFactory<Entity.ReporteIngresos>
    {
        public Entity.ReporteIngresos ConstructEntity(IDataReader dr)
        {

            var reporte = new Entity.ReporteIngresos();

            var index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                reporte.DetalledetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                reporte.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Detenido");
            if (!dr.IsDBNull(index))
            {
                reporte.Detenido = dr.GetString(index);
            }
            index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                reporte.Remision = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecharegistro");
            if (!dr.IsDBNull(index))
            {
                reporte.Fecharegistro = dr.GetDateTime(index);
            }


            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                reporte.Folio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                reporte.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Horas");
            if (!dr.IsDBNull(index))
            {
                reporte.Horas = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                reporte.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Responsable");
            if (!dr.IsDBNull(index))
            {
                reporte.Responsable = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                reporte.LugarDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("ResultadoQuimico");
            if (!dr.IsDBNull(index))
            {
                reporte.ResultadoQuimico = dr.GetString(index);
            }
            return reporte;

        }
    }
}
