﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;


namespace DataAccess.ReporteIngresos
{
    public class ObtenerListadoporfechas : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] fechas)
        {
            DbCommand cmd = db.GetStoredProcCommand("Reporte_Ingreso_byDates_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(fechas[0]));
            db.AddInParameter(cmd, "_FechaInicio", DbType.DateTime, Convert.ToDateTime(fechas[1]));
            db.AddInParameter(cmd, "_Fechafin", DbType.DateTime, Convert.ToDateTime(fechas[2]));
            return cmd;
        }
    }
}
