﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;
namespace DataAccess.ReporteIngresos
{
    public class ServicioReporteIngresos : ClsAbstractService<Entity.ReporteIngresos>, IReporteIngresos
    {
        public ServicioReporteIngresos(string dataBase)
     : base(dataBase)
        {

        }
        public List<Entity.ReporteIngresos> GetListByDates(string[] fechas)
        {
            ISelectFactory<string[]> select = new ObtenerListadoporfechas();
            return GetAll(select, new FabricaReporteIngresos(), fechas);
        }
    }
}
