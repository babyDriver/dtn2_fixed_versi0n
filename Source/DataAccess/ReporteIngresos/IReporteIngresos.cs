﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteIngresos
{
    public interface IReporteIngresos
    {
        List<Entity.ReporteIngresos> GetListByDates(string[] fechas);
    }
}
