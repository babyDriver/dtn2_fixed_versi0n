﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Epi
{
   public interface IEpi
    {
        List<Entity.Epi> ObtenerListadoPorFechas(string[] fechas);
    }
}
