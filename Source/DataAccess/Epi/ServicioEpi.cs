﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.Epi
{
    public class ServicioEpi : ClsAbstractService<Entity.Epi>, IEpi
    {
        public ServicioEpi(string dataBase)
            : base(dataBase)
        {

        }
        public List<Entity.Epi> ObtenerListadoPorFechas(string[] fechas)
        {
            ISelectFactory<string[]> select = new ObtenerListadoPorFechas();
            return GetAll(select, new FabricaEpi(), fechas);
            
        }
    }
}
