﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.Epi
{
    public class FabricaEpi : IEntityFactory<Entity.Epi>
    {
        public Entity.Epi ConstructEntity(IDataReader dr)
        {

            var item = new Entity.Epi();
            var index = dr.GetOrdinal("Expediente");

            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);   
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                item.Sexo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Conclucion");
            if (!dr.IsDBNull(index))
            {
                item.Conclucion = dr.GetString(index);
            }
            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            return item;
            
        }
    }
}
