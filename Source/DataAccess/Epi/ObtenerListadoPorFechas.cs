﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Epi
{
    public class ObtenerListadoPorFechas : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] fechas)
        {
            DbCommand cmd = db.GetStoredProcCommand("Reporte_Epi_ByDates_Data_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(fechas[0]));
            db.AddInParameter(cmd, "_FechaInicio", DbType.DateTime, Convert.ToDateTime(fechas[1]));
            db.AddInParameter(cmd, "_FechaFin", DbType.DateTime, Convert.ToDateTime(fechas[2]));
            return cmd;
        }
    }
}
