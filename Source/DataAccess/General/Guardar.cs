﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;

namespace DataAccess.General
{
    public class Guardar : IInsertFactory<Entity.General>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.General general)
        {
            DbCommand cmd = db.GetStoredProcCommand("General_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, general.TrackingId);
            db.AddInParameter(cmd, "_FechaNacimineto", DbType.DateTime, general.FechaNacimineto != DateTime.MinValue ? general.FechaNacimineto : (object)DBNull.Value);
            db.AddInParameter(cmd, "_Nacionalidad", DbType.Int32, general.NacionalidadId);
            db.AddInParameter(cmd, "_RFC", DbType.String, general.RFC);
            db.AddInParameter(cmd, "_EscolaridadId", DbType.Int32, general.EscolaridadId);
            db.AddInParameter(cmd, "_ReligionId", DbType.Int32, general.ReligionId);
            db.AddInParameter(cmd, "_OcupacionId", DbType.Int32, general.OcupacionId);
            db.AddInParameter(cmd, "_EstadoCivilId", DbType.Int32, general.EstadoCivilId);
            db.AddInParameter(cmd, "_EtniaId", DbType.Int32, general.EtniaId);
            db.AddInParameter(cmd, "_SexoId", DbType.Int32, general.SexoId);
            db.AddInParameter(cmd, "_EstadoMental", DbType.Boolean, general.EstadoMental);
            db.AddInParameter(cmd, "_Inimputable", DbType.Boolean, general.Inimputable);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, general.DetenidoId);
            db.AddInParameter(cmd, "_LenguanativaId", DbType.Int32, general.LenguanativaId);
            db.AddInParameter(cmd, "_CURP", DbType.String, general.CURP);
            db.AddInParameter(cmd, "_Edaddetenido", DbType.Int32, general.Edaddetenido);
            db.AddInParameter(cmd, "_SalarioSemanal", DbType.Decimal, general.SalarioSemanal);
            db.AddInParameter(cmd, "_Generalanioregistro", DbType.Int32, general.Generalanioregistro);
            db.AddInParameter(cmd, "_Generalmesregistro", DbType.Int32, general.Generalmesregistro);
            return cmd;
        }
    }
}
