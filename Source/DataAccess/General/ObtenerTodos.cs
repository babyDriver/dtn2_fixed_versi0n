﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.General
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.NullClass id)
        {
            DbCommand cmd = db.GetStoredProcCommand("General_GetAll_SP");
            return cmd;
        }
    }
}
