﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.General
{
    public class ServicioGeneral: ClsAbstractService<Entity.General>, IGeneral
    {
        public ServicioGeneral(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.General> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaGeneral(), new Entity.NullClass());
        }

   

        public Entity.General ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaGeneral(), userId);
        }

        public Entity.General ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaGeneral(), Id);
        }

        public Entity.General ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaGeneral(), Id);
        }

        public int Guardar(Entity.General general)
        {
            IInsertFactory<Entity.General> insert = new Guardar();
            return Insert(insert, general);
        }

        public void Actualizar(Entity.General general)
        {
            IUpdateFactory<Entity.General> update = new Actualizar();
            Update(update, general);
        }


    }
}