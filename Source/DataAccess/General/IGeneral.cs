﻿using System;
using System.Collections.Generic;

namespace DataAccess.General
{
    public interface IGeneral
    {
        List<Entity.General> ObtenerTodos();
        Entity.General ObtenerById(int Id);
        Entity.General ObtenerByDetenidoId(int Id);
        Entity.General ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.General general);
        void Actualizar(Entity.General general);
    }
}