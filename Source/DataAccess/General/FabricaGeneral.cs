﻿using DataAccess.Interfaces;


namespace DataAccess.General
{
    public class FabricaGeneral : IEntityFactory<Entity.General>
    {
       

        public Entity.General ConstructEntity(System.Data.IDataReader dr)
        {
            var general = new Entity.General();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                general.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                general.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("FechaNacimineto");
            if (!dr.IsDBNull(index))
            {
                general.FechaNacimineto = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("NacionalidadId");
            if (!dr.IsDBNull(index))
            {
                general.NacionalidadId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("RFC");
            if (!dr.IsDBNull(index))
            {
                general.RFC = dr.GetString(index);
            }

            index = dr.GetOrdinal("EscolaridadId");
            if (!dr.IsDBNull(index))
            {
                general.EscolaridadId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ReligionId");
            if (!dr.IsDBNull(index))
            {
                general.ReligionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("OcupacionId");
            if (!dr.IsDBNull(index))
            {
                general.OcupacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("EstadoCivilId");
            if (!dr.IsDBNull(index))
            {
                general.EstadoCivilId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("EtniaId");
            if (!dr.IsDBNull(index))
            {
                general.EtniaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("SexoId");
            if (!dr.IsDBNull(index))
            {
                general.SexoId = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("EstadoMental");
            if (!dr.IsDBNull(index))
            {
                general.EstadoMental = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Inimputable");
            if (!dr.IsDBNull(index))
            {
                general.Inimputable = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                general.DetenidoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("LenguanativaId");
            if (!dr.IsDBNull(index))
            {
                general.LenguanativaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CURP");
            if (!dr.IsDBNull(index))
            {
                general.CURP = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edaddetenido");
            if (!dr.IsDBNull(index))
            {
                general.Edaddetenido = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("SalarioSemanal");
            if (!dr.IsDBNull(index))
            {
                general.SalarioSemanal = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Generalanioregistro");
            if (!dr.IsDBNull(index))
            {
                general.Generalanioregistro = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Generalmesregistro");
            if (!dr.IsDBNull(index))
            {
                general.Generalmesregistro = dr.GetInt32(index);
            }

            return general;
        }
    }
}