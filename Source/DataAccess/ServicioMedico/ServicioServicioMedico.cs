﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.ServicioMedico
{
    public class ServicioServicioMedico : ClsAbstractService<Entity.ServicioMedico>, IServicioMedico
    {
        public ServicioServicioMedico(string dataBase)
          : base(dataBase)
        {
        }

        public void Actualizar(Entity.ServicioMedico servicioMedico)
        {

            IUpdateFactory<Entity.ServicioMedico> update = new Actualizar();
            Update(update, servicioMedico);
        }

        public int Guardar(Entity.ServicioMedico servicioMedico)
        {
            IInsertFactory<Entity.ServicioMedico> insert = new Guardar();
            return Insert(insert, servicioMedico);
        }

        public Entity.ServicioMedico ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaServicioMedico(), Id);
        }

        public Entity.ServicioMedico ObtenerByTrackingId(string trackingid)
        {
            ISelectFactory<string> select = new ObtenerPorTrackingId();
            return GetByKey(select, new FabricaServicioMedico(), trackingid);
        }

        public List<Entity.ServicioMedico> ObtenerPorDetalleDetencionId(int DetalledetencionId)
        {
            ISelectFactory<int> select = new ObtenerPorDetalleDetencion();
            return GetAll(select, new FabricaServicioMedico(), DetalledetencionId);
        }

        public List<Entity.ServicioMedico> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodo();
            return GetAll(select, new FabricaServicioMedico(), new Entity.NullClass());
        }
    }
}
