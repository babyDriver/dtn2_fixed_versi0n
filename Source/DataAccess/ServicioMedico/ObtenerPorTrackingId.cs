﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ServicioMedico
{
    public class ObtenerPorTrackingId : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("servicio_medico_ObtenerPorTrackinngId_SP");
            db.AddInParameter(cmd, "_TrackinngId", DbType.String, tracking);
            return cmd;
        }
    }
}
