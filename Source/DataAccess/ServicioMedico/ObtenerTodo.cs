﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.ServicioMedico
{
    public class ObtenerTodo : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {

            DbCommand cmd = db.GetStoredProcCommand("servicio_medico_ObtenerTodo_SP");

            return cmd;
        }
    }
}
