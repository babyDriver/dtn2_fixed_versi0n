﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ServicioMedico
{
    public class Actualizar : IUpdateFactory<Entity.ServicioMedico>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.ServicioMedico servicioMedico)
        {
            DbCommand cmd = db.GetStoredProcCommand("servicio_medico_Update_SP");
            
            db.AddInParameter(cmd, "_Id", DbType.Int32, servicioMedico.Id);
            db.AddInParameter(cmd, "_TrackinngId", DbType.String, servicioMedico.TrackinngId);
            db.AddInParameter(cmd, "_FechaRegistro", DbType.DateTime, servicioMedico.FechaRegistro);
            db.AddInParameter(cmd, "_RegistradoPor", DbType.Int32, servicioMedico.RegistradoPor);
            db.AddInParameter(cmd, "_ModificadoPor", DbType.Int32, servicioMedico.ModificadoPor);
            db.AddInParameter(cmd, "_FechaUltimaModificacion", DbType.DateTime, servicioMedico.FechaUltimaModificacion);
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, servicioMedico.DetalledetencionId);
            db.AddInParameter(cmd, "_CertificadoQuimicoId", DbType.Int32, servicioMedico.CertificadoQuimicoId);
            db.AddInParameter(cmd, "_CertificadoLesionId", DbType.Int32, servicioMedico.CertificadoLesionId);
            db.AddInParameter(cmd, "_CertificadoMedicoPsicofisiologicoId", DbType.Int32, servicioMedico.CertificadoMedicoPsicofisiologicoId);
            db.AddInParameter(cmd, "_Activo", DbType.Int16, servicioMedico.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int16, servicioMedico.Habilitado);

            return cmd;
        }
    }
}
