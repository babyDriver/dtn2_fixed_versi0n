﻿using System.Collections.Generic;

namespace DataAccess.ServicioMedico
{
    interface IServicioMedico
    {
        List<Entity.ServicioMedico> ObtenerTodos();
        Entity.ServicioMedico ObtenerById(int Id);

        Entity.ServicioMedico ObtenerByTrackingId(string trackingid);
        int Guardar(Entity.ServicioMedico servicioMedico);
        void Actualizar(Entity.ServicioMedico servicioMedico);
        List<Entity.ServicioMedico> ObtenerPorDetalleDetencionId(int DetalledetencionId);
    }
}
