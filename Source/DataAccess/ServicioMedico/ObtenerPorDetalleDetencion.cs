﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ServicioMedico
{
    public class ObtenerPorDetalleDetencion : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetalleDetencionId)
        {
            DbCommand cmd = db.GetStoredProcCommand("servicio_medico_ObtenerPorDetalledetencionId_SP");
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, DetalleDetencionId);
            return cmd;
        }
    }
}
