﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ServicioMedico
{
    public class FabricaServicioMedico : IEntityFactory<Entity.ServicioMedico>
    {
        public Entity.ServicioMedico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ServicioMedico();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TrackinngId");
            if (!dr.IsDBNull(index))
            {
                item.TrackinngId = dr.GetString(index);
            }
            index = dr.GetOrdinal("FechaRegistro");
            if (!dr.IsDBNull(index))
            {
                item.FechaRegistro = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("RegistradoPor");
            if (!dr.IsDBNull(index))
            {
                item.RegistradoPor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("ModificadoPor");
            if (!dr.IsDBNull(index))
            {
                item.ModificadoPor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaUltimaModificacion");
            if (!dr.IsDBNull(index))
            {
                item.FechaUltimaModificacion = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalledetencionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("CertificadoQuimicoId");
            if (!dr.IsDBNull(index))
            {
                item.CertificadoQuimicoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("CertificadoLesionId");
            if (!dr.IsDBNull(index))
            {
                item.CertificadoLesionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("CertificadoMedicoPsicofisiologicoId");
            if (!dr.IsDBNull(index))
            {
                item.CertificadoMedicoPsicofisiologicoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetInt16(index);
            }

            return item;
        }
    }
}
