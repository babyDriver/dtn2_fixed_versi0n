﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.DashboardMotivoDetencion
{
    public class ObtenerPorContratoid : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int ContratoId)
        {
            
            DbCommand cmd = db.GetStoredProcCommand("DashboardMotivos_Get_By_ContratoId");
            db.AddInParameter(cmd, "_Contratoid", DbType.Int32, ContratoId);
            return cmd;
        }
    }
}
