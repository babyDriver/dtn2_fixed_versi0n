﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DashboardMotivoDetencion
{
    public class ServicioDashboardMotivoDetencion:ClsAbstractService<Entity.DashboardMotivoDetencion>,IDashboardMotivoDetencion
    {
        public ServicioDashboardMotivoDetencion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.DashboardMotivoDetencion> Get_List_By_ContratoId(int ContratoId)
        {
            ISelectFactory<int> select = new ObtenerPorContratoid();
            return GetAll(select, new FabricadashboardMotivoDetencion(), ContratoId);
        }
    }
}
