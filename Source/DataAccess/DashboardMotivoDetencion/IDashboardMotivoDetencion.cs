﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DashboardMotivoDetencion
{
   public interface IDashboardMotivoDetencion
    {
        List<Entity.DashboardMotivoDetencion> Get_List_By_ContratoId(int ContratoId);
    }
}
