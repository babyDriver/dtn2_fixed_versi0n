﻿using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.DashboardMotivoDetencion
{
    public class FabricadashboardMotivoDetencion : IEntityFactory<Entity.DashboardMotivoDetencion>
    {
        public Entity.DashboardMotivoDetencion ConstructEntity(IDataReader dr)
        {
            var item = new Entity.DashboardMotivoDetencion();

            var index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Cantidad");
            if (!dr.IsDBNull(index))
            {
                item.Cantidad = dr.GetInt32(index);
            }
            return item;
        }
    }
}
