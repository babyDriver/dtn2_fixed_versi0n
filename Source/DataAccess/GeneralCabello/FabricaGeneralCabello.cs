﻿using DataAccess.Interfaces;


namespace DataAccess.GeneralCabello
{
    public class FabricaGeneralCabello : IEntityFactory<Entity.GeneralCabello>
    {
       

        public Entity.GeneralCabello ConstructEntity(System.Data.IDataReader dr)
        {
            var general = new Entity.GeneralCabello();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                general.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                general.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("ComplexionG");
            if (!dr.IsDBNull(index))
            {
                general.ComplexionG = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ColorPielG");
            if (!dr.IsDBNull(index))
            {
                general.ColorPielG = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CaraG");
            if (!dr.IsDBNull(index))
            {
                general.CaraG = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CantidadC");
            if (!dr.IsDBNull(index))
            {
                general.CantidadC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ColorC");
            if (!dr.IsDBNull(index))
            {
                general.ColorC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FormaC");
            if (!dr.IsDBNull(index))
            {
                general.FormaC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CalvicieC");
            if (!dr.IsDBNull(index))
            {
                general.CalvicieC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ImplementacionC");
            if (!dr.IsDBNull(index))
            {
                general.ImplementacionC = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AntropometriaId");
            if (!dr.IsDBNull(index))
            {
                general.AntropometriaId = dr.GetInt32(index);
            }
            
            return general;
        }
    }
}