﻿using System;
using System.Collections.Generic;

namespace DataAccess.GeneralCabello
{
    public interface IGeneralCabello
    {
        List<Entity.GeneralCabello> ObtenerTodos();
        Entity.GeneralCabello ObtenerById(int Id);
        Entity.GeneralCabello ObtenerByAntropometriaId(int Id);
        Entity.GeneralCabello ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.GeneralCabello general);
        void Actualizar(Entity.GeneralCabello general);
    }
}