﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.GeneralCabello
{
    public class Guardar : IInsertFactory<Entity.GeneralCabello>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.GeneralCabello general)
        {
            DbCommand cmd = db.GetStoredProcCommand("General_Cabello_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, general.TrackingId);
            db.AddInParameter(cmd, "_ComplexionG", DbType.Int32, general.ComplexionG);
            db.AddInParameter(cmd, "_ColorPielG", DbType.Int32, general.ColorPielG);
            db.AddInParameter(cmd, "_CaraG", DbType.Int32, general.CaraG);
            db.AddInParameter(cmd, "_CantidadC", DbType.Int32, general.CantidadC);
            db.AddInParameter(cmd, "_ColorC", DbType.Int32, general.ColorC);
            db.AddInParameter(cmd, "_FormaC", DbType.Int32, general.FormaC);
            db.AddInParameter(cmd, "_CalvicieC", DbType.Int32, general.CalvicieC);
            db.AddInParameter(cmd, "_ImplementacionC", DbType.Int32, general.ImplementacionC);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, general.AntropometriaId);


            return cmd;
        }
    }
}
