﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.GeneralCabello
{
    public class ServicioGeneralCabello : ClsAbstractService<Entity.GeneralCabello>, IGeneralCabello
    {
        public ServicioGeneralCabello(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.GeneralCabello> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaGeneralCabello(), new Entity.NullClass());
        }

   

        public Entity.GeneralCabello ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaGeneralCabello(), userId);
        }

        public Entity.GeneralCabello ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaGeneralCabello(), Id);
        }

        public Entity.GeneralCabello ObtenerByAntropometriaId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByAntropometriaId();
            return GetByKey(select, new FabricaGeneralCabello(), Id);
        }

        public int Guardar(Entity.GeneralCabello general)
        {
            IInsertFactory<Entity.GeneralCabello> insert = new Guardar();
            return Insert(insert, general);
        }

        public void Actualizar(Entity.GeneralCabello general)
        {
            IUpdateFactory<Entity.GeneralCabello> update = new Actualizar();
            Update(update, general);
        }


    }
}