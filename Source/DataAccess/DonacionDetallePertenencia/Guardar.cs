﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.DonacionDetallePertenencia
{
    public class Guardar : IInsertFactory<Entity.DonacionDetallePertenencia>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.DonacionDetallePertenencia donacion)
        {
            DbCommand cmd = db.GetStoredProcCommand("DonacionDetallePertenencia_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, donacion.TrackingId);
            db.AddInParameter(cmd, "_PertenenciaId", DbType.Int32, donacion.PertenenciaId);
            db.AddInParameter(cmd, "_DonacionId", DbType.Int32, donacion.DonacionId);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, donacion.CreadoPor);

            return cmd;
        }
    }
}
