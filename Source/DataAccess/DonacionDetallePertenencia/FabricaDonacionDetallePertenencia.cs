﻿using DataAccess.Interfaces;

namespace DataAccess.DonacionDetallePertenencia
{
    public class FabricaDonacionDetallePertenencia : IEntityFactory<Entity.DonacionDetallePertenencia>
    {
        public Entity.DonacionDetallePertenencia ConstructEntity(System.Data.IDataReader dr)
        {
            var donacion = new Entity.DonacionDetallePertenencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                donacion.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                donacion.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("PertenenciaId");
            if (!dr.IsDBNull(index))
            {
                donacion.PertenenciaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DonacionId");
            if (!dr.IsDBNull(index))
            {
                donacion.DonacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                donacion.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                donacion.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                donacion.CreadoPor = dr.GetInt32(index);
            }

            return donacion;
        }
    }
}
