﻿using System;
using System.Collections.Generic;

namespace DataAccess.DonacionDetallePertenencia
{
    public interface IDonacionDetallePertenencia
    {
        int Guardar(Entity.DonacionDetallePertenencia donacion);
        List<Entity.DonacionDetallePertenencia> ObtenerByDonacionId(int id);
    }
}
