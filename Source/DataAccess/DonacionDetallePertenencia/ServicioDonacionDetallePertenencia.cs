﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.DonacionDetallePertenencia
{
    public class ServicioDonacionDetallePertenencia : ClsAbstractService<Entity.DonacionDetallePertenencia>, IDonacionDetallePertenencia
    {
        public ServicioDonacionDetallePertenencia(string dataBase)
           : base(dataBase)
        {
        }

        public int Guardar(Entity.DonacionDetallePertenencia donacion)
        {
            IInsertFactory<Entity.DonacionDetallePertenencia> insert = new Guardar();
            return Insert(insert, donacion);
        }

        public List<Entity.DonacionDetallePertenencia> ObtenerByDonacionId(int id)
        {
            ISelectFactory<int> select = new ObtenerByDonacionId();
            return GetAll(select, new FabricaDonacionDetallePertenencia(), id);
        }
    }
}
