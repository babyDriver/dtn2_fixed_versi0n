﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.TipoMotivoDetencion 
{
  public  class ObtenerTpoMotivoDetencion :ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db,Entity.NullClass id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Tipos_Motivo_Detencion_GetAll_SP");
            return cmd;
        }
    }
}
