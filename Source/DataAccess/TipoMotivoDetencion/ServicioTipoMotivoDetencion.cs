﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
using DataAccess.Interfaces;

namespace DataAccess.TipoMotivoDetencion
{
   public class ServicioTipoMotivoDetencion : ClsAbstractService<Entity.TipoMotivoDetencion> , ITipoMotivoDetencion
    {
        public ServicioTipoMotivoDetencion(string database)
            : base(database)
        {
        }

        public List<Entity.TipoMotivoDetencion> ObtenerTpoMotivoDetencion()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTpoMotivoDetencion();
            return GetAll(select, new FabricaTipoMotivoDetencion(), new Entity.NullClass());
        }
    }
}
