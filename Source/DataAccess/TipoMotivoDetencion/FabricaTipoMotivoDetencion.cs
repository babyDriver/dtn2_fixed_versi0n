﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.TipoMotivoDetencion
{
    class FabricaTipoMotivoDetencion : IEntityFactory<Entity.TipoMotivoDetencion>
    {
        public Entity.TipoMotivoDetencion ConstructEntity(IDataReader dr)
        {

            var item = new Entity.TipoMotivoDetencion();
            var index = dr.GetOrdinal("TipoId");
            if (!dr.IsDBNull(index))
            {
                item.TipoId= dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }
            return item;
        }
    }
}
