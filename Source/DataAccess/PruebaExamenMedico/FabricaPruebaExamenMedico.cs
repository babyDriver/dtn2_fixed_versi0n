﻿
using DataAccess.Interfaces;



namespace DataAccess.PruebaExamenMedico
{
    public class FabricaPruebaExamenMedico : IEntityFactory<Entity.PruebaExamenMedico>
    {

       
        public Entity.PruebaExamenMedico ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.PruebaExamenMedico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("ExamenMedicoId");
            if (!dr.IsDBNull(index))
            {
                item.ExamenMedicoId = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("MucosasId");
            if (!dr.IsDBNull(index))
            {
                item.MucosasId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("AlientoId");
            if (!dr.IsDBNull(index))
            {
                item.AlientoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Examen_neurologicoId");
            if (!dr.IsDBNull(index))
            {
                item.Examen_neurologicoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Disartia");
            if (!dr.IsDBNull(index))
            {
                item.Disartia = dr.GetBoolean(index);
            }
            index = dr.GetOrdinal("ConjuntivasId");
            if (!dr.IsDBNull(index))
            {
                item.ConjuntivasId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("MarchaId");
            if (!dr.IsDBNull(index))
            {
                item.MarchaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("PupilasId");
            if (!dr.IsDBNull(index))
            {
                item.PupilasId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("CoordinacionId");
            if (!dr.IsDBNull(index))
            {
                item.CoordinacionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Reflejos_pupilaresId");
            if (!dr.IsDBNull(index))
            {
                item.Reflejos_pupilaresId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TendinososId");
            if (!dr.IsDBNull(index))
            {
                item.TendinososId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("RomberqId");
            if (!dr.IsDBNull(index))
            {
                item.RomberqId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("ConductaId");
            if (!dr.IsDBNull(index))
            {
                item.ConductaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("LenguajeId");
            if (!dr.IsDBNull(index))
            {
                item.LenguajeId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("AtencionId");
            if (!dr.IsDBNull(index))
            {
                item.AtencionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("OrientacionId");
            if (!dr.IsDBNull(index))
            {
                item.OrientacionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("DiadococinenciaId");
            if (!dr.IsDBNull(index))
            {
                item.DiadococinenciaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("DedoId");
            if (!dr.IsDBNull(index))
            {
                item.DedoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TalonId");
            if (!dr.IsDBNull(index))
            {
                item.TalonId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Alcoholimetro");
            if (!dr.IsDBNull(index))
            {
                item.Alcoholimetro = dr.GetString(index);
            }
            index = dr.GetOrdinal("TA");
            if (!dr.IsDBNull(index))
            {
                item.TA = dr.GetString(index);
            }
            index = dr.GetOrdinal("FC");
            if (!dr.IsDBNull(index))
            {
                item.FC = dr.GetString(index);
            }
            index = dr.GetOrdinal("FR");
            if (!dr.IsDBNull(index))
            {
                item.FR = dr.GetString(index);
            }
            index = dr.GetOrdinal("Pulso");
            if (!dr.IsDBNull(index))
            {
                item.Pulso = dr.GetString(index);
            }



            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.RegistradoPor = dr.GetInt32(index);
              
            }


            return item;

        }
    }
}