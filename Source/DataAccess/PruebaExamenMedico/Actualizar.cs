﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.PruebaExamenMedico

{
    public class Actualizar : IUpdateFactory<Entity.PruebaExamenMedico>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.PruebaExamenMedico item)
        {
            DbCommand cmd = db.GetStoredProcCommand("PruebaExamenMedico_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_ExamenMedicoId", DbType.Int32, item.ExamenMedicoId);
            db.AddInParameter(cmd, "_MucosasId", DbType.Int32, item.MucosasId);
            db.AddInParameter(cmd, "_AlientoId", DbType.Int32, item.AlientoId);
            db.AddInParameter(cmd, "_Examen_neurologicoId", DbType.Int32, item.Examen_neurologicoId);
            db.AddInParameter(cmd, "_Disartia", DbType.Boolean, item.Disartia);
            db.AddInParameter(cmd, "_ConjuntivasId", DbType.Int32, item.ConjuntivasId);
            db.AddInParameter(cmd, "_MarchaId", DbType.Int32, item.MarchaId);
            db.AddInParameter(cmd, "_PupilasId", DbType.Int32, item.PupilasId);
            db.AddInParameter(cmd, "_CoordinacionId", DbType.Int32, item.CoordinacionId);
            db.AddInParameter(cmd, "_Reflejos_pupilaresId", DbType.Int32, item.Reflejos_pupilaresId);
            db.AddInParameter(cmd, "_TendinososId", DbType.Int32, item.TendinososId);
            db.AddInParameter(cmd, "_RomberqId", DbType.Int32, item.RomberqId);
            db.AddInParameter(cmd, "_ConductaId", DbType.Int32, item.ConductaId);
            db.AddInParameter(cmd, "_LenguajeId", DbType.Int32, item.LenguajeId);
            db.AddInParameter(cmd, "_AtencionId", DbType.Int32, item.AtencionId);
            db.AddInParameter(cmd, "_OrientacionId", DbType.Int32, item.OrientacionId);
            db.AddInParameter(cmd, "_DiadococinenciaId", DbType.Int32, item.DiadococinenciaId);
            db.AddInParameter(cmd, "_DedoId", DbType.Int32, item.DedoId);
            db.AddInParameter(cmd, "_TalonId", DbType.Int32, item.TalonId);
            db.AddInParameter(cmd, "_Alcoholimetro", DbType.String, item.Alcoholimetro);
            db.AddInParameter(cmd, "_TA", DbType.String, item.TA);
            db.AddInParameter(cmd, "_FC", DbType.String, item.FC);
            db.AddInParameter(cmd, "_FR", DbType.String, item.FR);
            db.AddInParameter(cmd, "_Pulso", DbType.String, item.Pulso);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.RegistradoPor);

            return cmd;
        }
    }
}
