﻿using System;
using System.Collections.Generic;

namespace DataAccess.PruebaExamenMedico
{
    public interface IPruebaExamenMedico
    {

        Entity.PruebaExamenMedico ObtenerById(int Id);
        Entity.PruebaExamenMedico ObtenerByExamenMedicoId(int Id);
        Entity.PruebaExamenMedico ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.PruebaExamenMedico item);
        void Actualizar(Entity.PruebaExamenMedico item);
    }
}