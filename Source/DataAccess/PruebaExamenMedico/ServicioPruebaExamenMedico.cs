﻿using DataAccess.Interfaces;
using System;


namespace DataAccess.PruebaExamenMedico
{
    public class ServicioPruebaExamenMedico : ClsAbstractService<Entity.PruebaExamenMedico>, IPruebaExamenMedico
    { 
        public ServicioPruebaExamenMedico(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.PruebaExamenMedico ObtenerByTrackingId(Guid trackingid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaPruebaExamenMedico(), trackingid);
        }

        public Entity.PruebaExamenMedico ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaPruebaExamenMedico(), Id);
        }

        public Entity.PruebaExamenMedico ObtenerByExamenMedicoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByExamenMedicoId();
            return GetByKey(select, new FabricaPruebaExamenMedico(), Id);
        }

        public int Guardar(Entity.PruebaExamenMedico item)
        {
            IInsertFactory<Entity.PruebaExamenMedico> insert = new Guardar();
            return Insert(insert, item);
        }

        public void Actualizar(Entity.PruebaExamenMedico item)
        {
            IUpdateFactory<Entity.PruebaExamenMedico> update = new Actualizar();
            Update(update, item);
        }


    }
}