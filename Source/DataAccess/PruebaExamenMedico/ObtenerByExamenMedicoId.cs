﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.PruebaExamenMedico
{
    public class ObtenerByExamenMedicoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("PruebaExamenMedico_GetByExamenMedicoId_SP");
            db.AddInParameter(cmd, "_ExamenMedicoId", DbType.Int32, id);
            return cmd;
        }
    }
}
