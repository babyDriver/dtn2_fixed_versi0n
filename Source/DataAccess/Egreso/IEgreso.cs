﻿using System;
using System.Collections.Generic;

namespace DataAccess.Egreso
{
    public interface IEgreso
    {
        List<Entity.Egreso> ObtenerTodos();
        Entity.Egreso ObtenerById(int Id);
        Entity.Egreso ObtenerByEstatusInternoId(int Id);
        Entity.Egreso ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Egreso egreso);
        void Actualizar(Entity.Egreso egreso);
    }
}