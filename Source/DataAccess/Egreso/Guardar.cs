﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Egreso
{
    public class Guardar : IInsertFactory<Entity.Egreso>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Egreso egreso)
        {
            DbCommand cmd = db.GetStoredProcCommand("Egreso_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, egreso.TrackingId);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, egreso.Fecha);
            if (egreso.Traslado != null)
                db.AddInParameter(cmd, "_Traslado", DbType.Int32, egreso.Traslado);
            else
                db.AddInParameter(cmd, "_Traslado", DbType.Int32, null);

            if (egreso.MotivoId != null)
                db.AddInParameter(cmd, "_MotivoId", DbType.Int32, egreso.MotivoId);
            else
                db.AddInParameter(cmd, "_MotivoId", DbType.Int32, null);
            db.AddInParameter(cmd, "_Nombre", DbType.String, egreso.Nombre);
            db.AddInParameter(cmd, "_Puesto", DbType.String, egreso.Puesto);
            db.AddInParameter(cmd, "_EstatusInternoId", DbType.Int32, egreso.EstatusInternoId);

            return cmd;
        }
    }
}
