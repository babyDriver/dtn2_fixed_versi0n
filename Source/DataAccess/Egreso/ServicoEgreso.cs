﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Egreso
{
    public class ServicioEgreso : ClsAbstractService<Entity.Egreso>, IEgreso
    {
        public ServicioEgreso(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Egreso> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaEgreso(), new Entity.NullClass());
        }



        public Entity.Egreso ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaEgreso(), userId);
        }

        public Entity.Egreso ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaEgreso(), Id);
        }

        public Entity.Egreso ObtenerByEstatusInternoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByEstatusInternoId();
            return GetByKey(select, new FabricaEgreso(), Id);
        }

        public int Guardar(Entity.Egreso egreso)
        {
            IInsertFactory<Entity.Egreso> insert = new Guardar();
            return Insert(insert, egreso);
        }

        public void Actualizar(Entity.Egreso egreso)
        {
            IUpdateFactory<Entity.Egreso> update = new Actualizar();
            Update(update, egreso);
        }


    }
}