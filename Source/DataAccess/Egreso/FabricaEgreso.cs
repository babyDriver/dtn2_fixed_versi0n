﻿using DataAccess.Interfaces;


namespace DataAccess.Egreso
{
    public class FabricaEgreso : IEntityFactory<Entity.Egreso>
    {


        public Entity.Egreso ConstructEntity(System.Data.IDataReader dr)
        {
            var egreso = new Entity.Egreso();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                egreso.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                egreso.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                egreso.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Traslado");
            if (!dr.IsDBNull(index))
            {
                egreso.Traslado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("MotivoId");
            if (!dr.IsDBNull(index))
            {
                egreso.MotivoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                egreso.Nombre = dr.GetString(index);
            }


            index = dr.GetOrdinal("Puesto");
            if (!dr.IsDBNull(index))
            {
                egreso.Puesto = dr.GetString(index);
            }
          
            index = dr.GetOrdinal("EstatusInternoId");
            if (!dr.IsDBNull(index))
            {
                egreso.EstatusInternoId = dr.GetInt32(index);
            }


            return egreso;
        }
    }
}