﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Egreso
{
    public class Actualizar : IUpdateFactory<Entity.Egreso>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Egreso egreso)
        {
            DbCommand cmd = db.GetStoredProcCommand("Egreso_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, egreso.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, egreso.TrackingId);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, egreso.Fecha);

            if(egreso.Traslado !=null)
                db.AddInParameter(cmd, "_Traslado", DbType.Int32, egreso.Traslado);
            else
                db.AddInParameter(cmd, "_Traslado", DbType.Int32,null);

            if(egreso.MotivoId != null)
                db.AddInParameter(cmd, "_MotivoId", DbType.Int32, egreso.MotivoId);
            else
                db.AddInParameter(cmd, "_MotivoId", DbType.Int32, null);
            
            db.AddInParameter(cmd, "_Nombre", DbType.String, egreso.Nombre);
            db.AddInParameter(cmd, "_Puesto", DbType.String, egreso.Puesto);
            db.AddInParameter(cmd, "_EstatusInternoId", DbType.String, egreso.EstatusInternoId);
            return cmd;
        }
    }
}
