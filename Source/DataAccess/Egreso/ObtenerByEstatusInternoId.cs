﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Egreso
{
    public class ObtenerByEstatusInternoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Egreso_GetByEstatusInternoId_SP");
            db.AddInParameter(cmd, "_EstatusInternoId", DbType.Int32, id);
            return cmd;
        }
    }
}
