﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.PagoMulta
{
    public class ObtenerByInternoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("PagoMulta_GetByInternoId_SP");
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, id);
            return cmd;
        }
    }
}
