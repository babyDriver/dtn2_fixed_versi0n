﻿using DataAccess.Interfaces;

namespace DataAccess.PagoMulta
{
    public class FabricaPagoMulta : IEntityFactory<Entity.PagoMulta>
    {
        public Entity.PagoMulta ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.PagoMulta();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Articulo");
            if (!dr.IsDBNull(index))
            {
                item.Articulo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("MultaSugerida");
            if (!dr.IsDBNull(index))
            {
                item.MultaSugerida = dr.GetDecimal(index);
            }            

            return item;
        }
    }
}
