﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.PagoMulta
{
    public class ServicioPagoMulta : ClsAbstractService<Entity.PagoMulta>, IPagoMulta
    {
        public ServicioPagoMulta(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.PagoMulta> ObtenerByInternoId(int id)
        {
            ISelectFactory<int> select = new ObtenerByInternoId();
            return GetAll(select, new FabricaPagoMulta(), id);
        }
    }
}
