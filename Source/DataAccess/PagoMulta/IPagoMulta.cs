﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.PagoMulta
{
    public interface IPagoMulta
    {
        List<Entity.PagoMulta> ObtenerByInternoId(int id);
    }
}
