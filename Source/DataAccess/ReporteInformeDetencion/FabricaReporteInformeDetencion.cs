﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteInformeDetencion
{
    public class FabricaReporteInformeDetencion : IEntityFactory<Entity.ReporteInformeDetencion>
    {
        public Entity.ReporteInformeDetencion ConstructEntity(IDataReader dr)
        {
            var reporte = new Entity.ReporteInformeDetencion();

            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                reporte.Remision = dr.GetString(index);
            }

            index = dr.GetOrdinal("Detenido");
            if (!dr.IsDBNull(index))
            {
                reporte.Detenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("Domicilio");
            if (!dr.IsDBNull(index))
            {
                reporte.Domicilio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                reporte.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Municipio");
            if (!dr.IsDBNull(index))
            {
                reporte.Municipio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Colonia");
            if (!dr.IsDBNull(index))
            {
                reporte.Colonia = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaNacimiento");
            if (!dr.IsDBNull(index))
            {
                reporte.FechaNacimineto = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                reporte.Sexo = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaRegistro");
            if (!dr.IsDBNull(index))
            {
                reporte.FechaRegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                reporte.LugarDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                reporte.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaDetencion");
            if (!dr.IsDBNull(index))
            {
                reporte.FechaDetencion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                reporte.CreadoPor = dr.GetString(index);
            }

            return reporte;
        }
    }
}
