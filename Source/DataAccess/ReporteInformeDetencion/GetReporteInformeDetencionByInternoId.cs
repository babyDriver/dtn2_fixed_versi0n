﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ReporteInformeDetencion
{
    public class GetReporteInformeDetencionByInternoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int InternoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("GetReporteInformeDetencionByInternoId_SP");

            db.AddInParameter(cmd, "_InternoId", DbType.Int32, InternoId);

            return cmd;
        }
    }
}
