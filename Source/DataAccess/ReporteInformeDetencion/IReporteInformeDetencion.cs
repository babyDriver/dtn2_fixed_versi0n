﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteInformeDetencion
{
    public interface IReporteInformeDetencion
    {
        Entity.ReporteInformeDetencion GetReporteInformeDetencion(int InternoId);
    }
}
