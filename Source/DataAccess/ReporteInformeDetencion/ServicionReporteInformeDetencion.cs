﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.ReporteInformeDetencion
{
    public class ServicionReporteInformeDetencion : ClsAbstractService<Entity.ReporteInformeDetencion>, IReporteInformeDetencion
    {
        public Entity.ReporteInformeDetencion GetReporteInformeDetencion(int InternoId)
        {
            ISelectFactory<int> select = new GetReporteInformeDetencionByInternoId();
            return GetByKey(select, new FabricaReporteInformeDetencion(), InternoId);
        }
    }
}
