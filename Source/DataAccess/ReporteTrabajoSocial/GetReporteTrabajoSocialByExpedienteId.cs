﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ReporteTrabajoSocial
{
    class GetReporteTrabajoSocialByExpedienteId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int ExpedienteId)
        {
            DbCommand cmd = db.GetStoredProcCommand("GetRepoteTrabajoSociaByExpedienteId_SP");

            db.AddInParameter(cmd, "_ExpedienteId", DbType.Int32, ExpedienteId);

            return cmd;
        }
    }
}
