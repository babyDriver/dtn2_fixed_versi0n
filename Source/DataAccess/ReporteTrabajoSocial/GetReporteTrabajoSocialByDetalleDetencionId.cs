﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ReporteTrabajoSocial
{
    public class GetReporteTrabajoSocialByDetalleDetencionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetalleDetencionId)
        {
            DbCommand cmd = db.GetStoredProcCommand("GetRepoteTrabajoSociaByDetalleDetencionId_SP");

            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, DetalleDetencionId);

            return cmd;
        }
    }
}