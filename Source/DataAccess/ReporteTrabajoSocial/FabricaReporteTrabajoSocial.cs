﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteTrabajoSocial
{
    public class FabricaReporteTrabajoSocial : IEntityFactory<Entity.ReporteTrabajoSocial>
    {
        public Entity.ReporteTrabajoSocial ConstructEntity(IDataReader dr)
        {
            var reporte = new Entity.ReporteTrabajoSocial();

            var index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                reporte.Nombre = dr.GetString(index);
            }
            index = dr.GetOrdinal("Alias");
            if (!dr.IsDBNull(index))
            {
                reporte.Alias = dr.GetString(index);
            }
            index = dr.GetOrdinal("FechaNacimineto");
            if (!dr.IsDBNull(index))
            {
                reporte.FechaNacimiento = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                reporte.Edad = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("EstadoCivil");
            if (!dr.IsDBNull(index))
            {
                reporte.EstadoCivil = dr.GetString(index);
            }
            index = dr.GetOrdinal("Domicilio");
            if (!dr.IsDBNull(index))
            {
                reporte.Domicilio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Telefono");
            if (!dr.IsDBNull(index))
            {
                reporte.Telefono = dr.GetString(index);
            }
            index = dr.GetOrdinal("Colonia");
            if (!dr.IsDBNull(index))
            {
                reporte.Colonia = dr.GetString(index);
            }

            index = dr.GetOrdinal("Municipio");
            if (!dr.IsDBNull(index))
            {
                reporte.Municipio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                reporte.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nacionalidad");
            if (!dr.IsDBNull(index))
            {
                reporte.Nacionalidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                reporte.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                reporte.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                reporte.LugarDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                reporte.Folio = dr.GetString(index);
            }

            index = dr.GetOrdinal("MotivoRehabilitacion");
            if (!dr.IsDBNull(index))
            {
                reporte.MotivoRehabilitacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Adiccion");
            if (!dr.IsDBNull(index))
            {
                reporte.Adiccion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Religion");
            if (!dr.IsDBNull(index))
            {
                reporte.Religion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Pandilla");
            if (!dr.IsDBNull(index))
            {
                reporte.Pandilla = dr.GetString(index);
            }

            index = dr.GetOrdinal("Cuadropatalogico");
            if (!dr.IsDBNull(index))
            {
                reporte.CuadroPatologico = dr.GetString(index);
            }

            index = dr.GetOrdinal("Observacion");
            if (!dr.IsDBNull(index))
            {
                reporte.Observacion = dr.GetString(index);
            }
            index = dr.GetOrdinal("Delegacion");
            if (!dr.IsDBNull(index))
            {
                reporte.Delegacion = dr.GetString(index);
            }
            index = dr.GetOrdinal("Escolaridad");
            if (!dr.IsDBNull(index))
            {
                reporte.Escolaridad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                reporte.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("NombreMadre");
            if (!dr.IsDBNull(index))
            {
                reporte.NombreMadre = dr.GetString(index);
            }
            index = dr.GetOrdinal("CelularMadre");
            if (!dr.IsDBNull(index))
            {
                reporte.CelularMadre = dr.GetString(index);
            }
            index = dr.GetOrdinal("NombrePadre");
            if (!dr.IsDBNull(index))
            {
                reporte.NombrePadre = dr.GetString(index);
            }
            index = dr.GetOrdinal("CelularPadre");
            if (!dr.IsDBNull(index))
            {
                reporte.CelularPadre = dr.GetString(index);
            }
            index = dr.GetOrdinal("Tutor");
            if (!dr.IsDBNull(index))
            {
                reporte.Tutor = dr.GetString(index);
            }
            index = dr.GetOrdinal("Celulartutor");
            if (!dr.IsDBNull(index))
            {
                reporte.Celulartutor = dr.GetString(index);
            }
            index = dr.GetOrdinal("DomicilioMadre");
            if (!dr.IsDBNull(index))
            {
                reporte.DomicilioMadre = dr.GetString(index);
            }
            index = dr.GetOrdinal("DomicilioPadre");
            if (!dr.IsDBNull(index))
            {
                reporte.DomicilioPadre = dr.GetString(index);
            }
            index = dr.GetOrdinal("EscolaridadTS");
            if (!dr.IsDBNull(index))
            {
                reporte.EscolaridadTS = dr.GetString(index);
            }
            index = dr.GetOrdinal("Notificado");
            if (!dr.IsDBNull(index))
            {
                reporte.Notificado = dr.GetString(index);
            }
            index = dr.GetOrdinal("CelularNotificado");
            if (!dr.IsDBNull(index))
            {
                reporte.CelularNotificado = dr.GetString(index);
            }

            return reporte;
        }
    }
}

