﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.ReporteTrabajoSocial
{
    public class ServicioReporteTrabajoSocial : ClsAbstractService<Entity.ReporteTrabajoSocial>, IReporteTrabajoSocial
    {
        public Entity.ReporteTrabajoSocial GetReporteTrabajoSocial(int DetalleDetencionId)
        {
            ISelectFactory<int> select = new GetReporteTrabajoSocialByDetalleDetencionId();
            return GetByKey(select, new FabricaReporteTrabajoSocial(), DetalleDetencionId);
        }

        public Entity.ReporteTrabajoSocial GetReporteTrabajoSocialExpedienteId(int ExpedienteId)
        {
            ISelectFactory<int> select = new GetReporteTrabajoSocialByExpedienteId();
            return GetByKey(select, new FabricaReporteTrabajoSocial(), ExpedienteId);

        }
    }
}
