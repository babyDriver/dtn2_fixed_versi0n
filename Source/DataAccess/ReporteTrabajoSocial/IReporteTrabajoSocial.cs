﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteTrabajoSocial
{
   public interface IReporteTrabajoSocial
    {
        Entity.ReporteTrabajoSocial GetReporteTrabajoSocial(int DetalleDetencionId);
        Entity.ReporteTrabajoSocial GetReporteTrabajoSocialExpedienteId(int ExpedienteId);
    }
}
