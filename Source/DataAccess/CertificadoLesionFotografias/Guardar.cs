﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionFotografias
{
    public class Guardar : IInsertFactory<Entity.CertificadoLesionFotografias>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CertificadoLesionFotografias entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionFotografias_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, entity.FechaHora);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, entity.Creadopor);
            return cmd;
        }
    }
}
