﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoLesionFotografias
{
    public class ObtenerPorId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionFotografias_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, identity);
            return cmd;
        }
    }
}
