﻿using DataAccess.Interfaces;

namespace DataAccess.CertificadoLesionFotografias
{
    public class ServicioCertificadoLesionFotografias : ClsAbstractService<Entity.CertificadoLesionFotografias>, ICertificadoLesionFotografias
    {
        public ServicioCertificadoLesionFotografias(string dataBase)
        : base(dataBase)
        {
        }
        
        public int Guardar(Entity.CertificadoLesionFotografias certificadoLesionFotografias)
        {
            IInsertFactory<Entity.CertificadoLesionFotografias> insert = new Guardar();
            return Insert(insert, certificadoLesionFotografias);
        }

        public Entity.CertificadoLesionFotografias ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoLesionFotografias(), Id);
        }
    }
}
