﻿namespace DataAccess.CertificadoLesionFotografias
{
    public interface ICertificadoLesionFotografias
    {
        int Guardar(Entity.CertificadoLesionFotografias certificadoLesionFotografias);
        Entity.CertificadoLesionFotografias ObtenerPorId(int Id);
    }
}
