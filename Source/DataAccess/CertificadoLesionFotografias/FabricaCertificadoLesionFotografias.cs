﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.CertificadoLesionFotografias
{
    public class FabricaCertificadoLesionFotografias : IEntityFactory<Entity.CertificadoLesionFotografias>
    {
        public Entity.CertificadoLesionFotografias ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoLesionFotografias();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }
            return item;
        }
    }
}
