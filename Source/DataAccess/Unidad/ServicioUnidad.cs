﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.Unidad
{
    public class ServicioUnidad : ClsAbstractService<Entity.Unidad>, IUnidad
    {
        public ServicioUnidad(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.Unidad unidad)
        {
            IUpdateFactory<Entity.Unidad> update = new Actualizar();
            Update(update, unidad);
        }

        public int Guardar(Entity.Unidad unidad)
        {
            IInsertFactory<Entity.Unidad> insert = new Guardar();
            return Insert(insert, unidad);
        }

        public Entity.Unidad ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaUnidad(), id);
        }
         
        public List<Entity.Unidad> ObtenerPorEventoId(int EventoId)
        {
            ISelectFactory<int> select = new ObtenerPorEventoId();
            return GetAll(select, new FabricaUnidad(), EventoId);
        }
    }
}
