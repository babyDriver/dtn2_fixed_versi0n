﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Unidad
{
    public class FabricaUnidad : IEntityFactory<Entity.Unidad>
    {
        public Entity.Unidad ConstructEntity(IDataReader dr)
        {
            var item = new Entity.Unidad();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index).ToString();
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                item.Tipo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Placa");
            if (!dr.IsDBNull(index))
            {
                item.Placa = dr.GetString(index);
            }

            index = dr.GetOrdinal("CorporacionId");
            if (!dr.IsDBNull(index))
            {
                item.CorporacionId = dr.GetInt32(index);
            }

            return item;
        }
    }
}
