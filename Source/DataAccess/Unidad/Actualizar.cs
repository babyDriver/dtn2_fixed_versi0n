﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;
namespace DataAccess.Unidad
{
    class Actualizar : IUpdateFactory<Entity.Unidad>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Unidad unidad)
        {

            DbCommand cmd = db.GetStoredProcCommand("Unidad_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, unidad.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, unidad.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, unidad.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, unidad.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, unidad.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, unidad.Habilitado);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, unidad.Creadopor);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, unidad.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, unidad.Tipo);
            db.AddInParameter(cmd, "_Placa", DbType.String, unidad.Placa);
            db.AddInParameter(cmd, "_CorporacionId", DbType.Int32, unidad.CorporacionId);


            return cmd;
        }
    }
}
