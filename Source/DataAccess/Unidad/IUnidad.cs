﻿using System;
using System.Collections.Generic;

namespace DataAccess.Unidad
{
    public interface IUnidad
    {
        List<Entity.Unidad> ObtenerPorEventoId(int EventoId);
        int Guardar(Entity.Unidad unidad);
        void Actualizar(Entity.Unidad unidad);
        Entity.Unidad ObtenerById(int id);
    }
}
