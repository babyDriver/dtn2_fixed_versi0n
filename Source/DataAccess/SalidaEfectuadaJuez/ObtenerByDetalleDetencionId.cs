﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.SalidaEfectuadaJuez
{
    public class ObtenerByDetalleDetencionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetalleDetencionId)
        {
            DbCommand cmd = db.GetStoredProcCommand("SalidaEfectuadaJuez_GetByDetalleDetencionId_SP");
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, DetalleDetencionId);
            return cmd;
        }
    }
}
