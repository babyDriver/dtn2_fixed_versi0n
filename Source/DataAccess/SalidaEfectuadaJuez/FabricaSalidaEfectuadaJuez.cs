﻿using System.Data;
using DataAccess.Interfaces;

namespace DataAccess.SalidaEfectuadaJuez
{
    public class FabricaSalidaEfectuadaJuez : IEntityFactory<Entity.SalidaEfectuadaJuez>
    {
        public Entity.SalidaEfectuadaJuez ConstructEntity(IDataReader dr)
        {
            var salida = new Entity.SalidaEfectuadaJuez();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                salida.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                salida.TrackingId = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetalleDetencionId");
            if (!dr.IsDBNull(index))
            {
                salida.DetalledetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fundamento");
            if (!dr.IsDBNull(index))
            {
                salida.Fundamento = dr.GetString(index);
            }
            
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                salida.Habilitado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                salida.Activo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                salida.Creadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TiposalidaId");
            if (!dr.IsDBNull(index))
            {
                salida.TiposalidaId = dr.GetInt32(index);
            }

            return salida;
        }
    }
}
