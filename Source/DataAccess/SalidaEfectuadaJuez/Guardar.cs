﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;
namespace DataAccess.SalidaEfectuadaJuez
{
    public class Guardar : IInsertFactory<Entity.SalidaEfectuadaJuez>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.SalidaEfectuadaJuez salidaEfectuadajuez)
        {
            DbCommand cmd = db.GetStoredProcCommand("SalidaEfecuadajuez_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, salidaEfectuadajuez.TrackingId);
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, salidaEfectuadajuez.DetalledetencionId);
            db.AddInParameter(cmd, "_Fundamento", DbType.String, salidaEfectuadajuez.Fundamento);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, salidaEfectuadajuez.Creadopor);
            db.AddInParameter(cmd, "_Activo", DbType.Int32, salidaEfectuadajuez.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int32, salidaEfectuadajuez.Habilitado);
            db.AddInParameter(cmd, "_TiposalidaId", DbType.Int32, salidaEfectuadajuez.TiposalidaId);


            return cmd;
        }
    }
}