﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.SalidaEfectuadaJuez
{
    public class ServicioSalidaEfectuadaJuez : ClsAbstractService<Entity.SalidaEfectuadaJuez>, ISalidaEfectuadaJuez
    {
        public ServicioSalidaEfectuadaJuez(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.SalidaEfectuadaJuez salidaEfectuadaJuez)
        {
            IUpdateFactory<Entity.SalidaEfectuadaJuez> update = new Actualizar();
            Update(update, salidaEfectuadaJuez);

        }

        public int Guardar(Entity.SalidaEfectuadaJuez salidaEfectuadajuez)
        {
            IInsertFactory<Entity.SalidaEfectuadaJuez> insert = new Guardar();
            return Insert(insert, salidaEfectuadajuez);
        }

        public Entity.SalidaEfectuadaJuez ObtenerByDetalleDetencionId(int detalledetencionId)
        {
            ISelectFactory<int> select = new ObtenerByDetalleDetencionId();
            return GetByKey(select, new FabricaSalidaEfectuadaJuez(), detalledetencionId);
        }
    }
}
