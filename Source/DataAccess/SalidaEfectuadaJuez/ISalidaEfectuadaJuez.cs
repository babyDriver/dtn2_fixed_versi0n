﻿namespace DataAccess.SalidaEfectuadaJuez
{
    public interface ISalidaEfectuadaJuez
    {
        int Guardar(Entity.SalidaEfectuadaJuez salidaEfectuadajuez);
        Entity.SalidaEfectuadaJuez ObtenerByDetalleDetencionId(int id);
        void Actualizar(Entity.SalidaEfectuadaJuez salidaEfectuadaJuez);
    }
}
