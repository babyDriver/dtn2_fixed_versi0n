﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoReporteOficio
{
    public class ServicioListadoReporteOficio : ClsAbstractService<Entity.ListadoReporteOficio>, IListadoReporteOficio
    {
        public ServicioListadoReporteOficio(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoReporteOficio> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoReporteOficio(), parametros);
        }
    }
}
