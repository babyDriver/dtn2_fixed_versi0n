﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoReporteOficio
{
    public class FabricaListadoReporteOficio : IEntityFactory<Entity.ListadoReporteOficio>
    {
        public Entity.ListadoReporteOficio ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoReporteOficio();

            var index = dr.GetOrdinal("EstatusTracking");
            if (!dr.IsDBNull(index))
            {
                item.EstatusTracking = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("RutaImagen");
            if (!dr.IsDBNull(index))
            {
                item.RutaImagen = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetString(index);
            }

            index = dr.GetOrdinal("NombreCompleto");
            if (!dr.IsDBNull(index))
            {
                item.NombreCompleto = dr.GetString(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                item.LugarDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                item.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Responsable");
            if (!dr.IsDBNull(index))
            {
                item.Responsable = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Edaddetenido");
            if (!dr.IsDBNull(index))
            {
                item.EdadDetenido = dr.GetInt32(index);
            }

            return item;
        }
    }
}
