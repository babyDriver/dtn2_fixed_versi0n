﻿using System.Collections.Generic;

namespace DataAccess.ListadoReporteOficio
{
    public interface IListadoReporteOficio
    {
        List<Entity.ListadoReporteOficio> ObtenerTodos(object[] data);
    }
}
