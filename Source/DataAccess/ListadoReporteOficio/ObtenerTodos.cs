﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ListadoReporteOficio
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ListadoReporteOficio_GetAll_SP");
            db.AddInParameter(cmd, "_tipo", DbType.String, parametros[0]);
            db.AddInParameter(cmd, "_contratoId", DbType.Int32, parametros[1]);

            return cmd;
        }
    }
}
