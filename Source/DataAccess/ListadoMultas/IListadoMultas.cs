﻿using System.Collections.Generic;

namespace DataAccess.ListadoMultas
{
    public interface IListadoMultas
    {
        List<Entity.ListadoMultas> ObtenerTodos(object[] data);
    }
}
