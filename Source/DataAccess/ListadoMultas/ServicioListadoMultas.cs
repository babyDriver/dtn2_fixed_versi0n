﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoMultas
{
    public class ServicioListadoMultas : ClsAbstractService<Entity.ListadoMultas>, IListadoMultas
    {
        public ServicioListadoMultas(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoMultas> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoMultas(), parametros);
        }
    }
}
