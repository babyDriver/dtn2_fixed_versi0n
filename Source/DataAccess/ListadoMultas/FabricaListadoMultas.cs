﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoMultas
{
    public class FabricaListadoMultas : IEntityFactory<Entity.ListadoMultas>
    {
        public Entity.ListadoMultas ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoMultas();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CalificacionTracking");
            if (!dr.IsDBNull(index))
            {
                item.CalificacionTracking = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("EstatusTracking");
            if (!dr.IsDBNull(index))
            {
                item.EstatusTracking = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("RutaImagen");
            if (!dr.IsDBNull(index))
            {
                item.RutaImagen = dr.GetString(index);
            }

            index = dr.GetOrdinal("NombreDetenido");
            if (!dr.IsDBNull(index))
            {
                item.NombreDetenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("APaternoDetenido");
            if (!dr.IsDBNull(index))
            {
                item.APaternoDetenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("AMaternoDetenido");
            if (!dr.IsDBNull(index))
            {
                item.AMaternoDetenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("TotalAPagar");
            if (!dr.IsDBNull(index))
            {
                item.TotalAPagar = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("NombreCompleto");
            if (!dr.IsDBNull(index))
            {
                item.NombreCompleto = dr.GetString(index);
            }

            return item;
        }
    }
}
