﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Senal
{
    public class ObtenerByDetenidoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Senales_GetByDetenido_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, id);
            return cmd;
        }
    }
}
