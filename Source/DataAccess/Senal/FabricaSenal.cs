﻿using DataAccess.Interfaces;


namespace DataAccess.Senal
{
    public class FabricaSenal : IEntityFactory<Entity.Senal>
    {
       

        public Entity.Senal ConstructEntity(System.Data.IDataReader dr)
        {
            var senal = new Entity.Senal();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                senal.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                senal.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                senal.Tipo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Lado");
            if (!dr.IsDBNull(index))
            {
                senal.Lado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Region");
            if (!dr.IsDBNull(index))
            {
                senal.Region = dr.GetString(index);
            }

            index = dr.GetOrdinal("Vista");
            if (!dr.IsDBNull(index))
            {
                senal.Vista = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Cantidad");
            if (!dr.IsDBNull(index))
            {
                senal.Cantidad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                senal.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                senal.Activo = dr.GetBoolean(index);
            }


            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                senal.DetenidoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                senal.Habilitado = dr.GetInt32(index);

            }
            index = dr.GetOrdinal("LugarId");
            if (!dr.IsDBNull(index))
            {
                senal.LugarId = dr.GetInt32(index);

            }

            return senal;
        }
    }
}