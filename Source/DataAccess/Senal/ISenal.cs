﻿using System;
using System.Collections.Generic;

namespace DataAccess.Senal
{
    public interface ISenal
    {
        List<Entity.Senal> ObtenerTodos();
        Entity.Senal ObtenerById(int Id);
        Entity.Senal ObtenerByDetenidoId(int Id);
        Entity.Senal ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Senal senal);
        void Actualizar(Entity.Senal senal);
    }
}