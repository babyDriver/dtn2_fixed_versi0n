﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Senal
{
    public class Guardar : IInsertFactory<Entity.Senal>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Senal senal)
        {
            DbCommand cmd = db.GetStoredProcCommand("Senales_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, senal.TrackingId);
            db.AddInParameter(cmd, "_Tipo", DbType.Int32, senal.Tipo);
            db.AddInParameter(cmd, "_Lado", DbType.Int32, senal.Lado);
            db.AddInParameter(cmd, "_Region", DbType.String, senal.Region);
            db.AddInParameter(cmd, "_Vista", DbType.Int32, senal.Vista);
            db.AddInParameter(cmd, "_Cantidad", DbType.Int32, senal.Cantidad);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, senal.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Int32, senal.Activo);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, senal.DetenidoId);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, senal.Creadopor);
            
            if(senal.LugarId !=null)
            {
                db.AddInParameter(cmd, "_LugarId", DbType.Int32, senal.LugarId);
            }
            else
            {
                db.AddInParameter(cmd, "_LugarId", DbType.Int32, null);
            }

            return cmd;
        }
    }
}
