﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Senal
{
    public class ServicioSenal : ClsAbstractService<Entity.Senal>, ISenal
    {
        public ServicioSenal(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Senal> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaSenal(), new Entity.NullClass());
        }

   

        public Entity.Senal ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaSenal(), userId);
        }

        public Entity.Senal ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaSenal(), Id);
        }

        public Entity.Senal ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaSenal(), Id);
        }

        public int Guardar(Entity.Senal senal)
        {
            IInsertFactory<Entity.Senal> insert = new Guardar();
            return Insert(insert, senal);
        }

        public void Actualizar(Entity.Senal senal)
        {
            IUpdateFactory<Entity.Senal> update = new Actualizar();
            Update(update, senal);
        }


    }
}