﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Senal
{
    public class Actualizar : IUpdateFactory<Entity.Senal>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Senal senal)
        {
            DbCommand cmd = db.GetStoredProcCommand("Senales_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, senal.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, senal.TrackingId);
            db.AddInParameter(cmd, "_Tipo", DbType.Int32, senal.Tipo);
            db.AddInParameter(cmd, "_Lado", DbType.Int32, senal.Lado);
            db.AddInParameter(cmd, "_Region", DbType.String, senal.Region);
            db.AddInParameter(cmd, "_Vista", DbType.Int32, senal.Vista);
            db.AddInParameter(cmd, "_Cantidad", DbType.Int32, senal.Cantidad);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, senal.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Int32, senal.Activo);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, senal.DetenidoId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int32, senal.Habilitado);

            if (senal.LugarId != null)
            {
                db.AddInParameter(cmd, "_LugarId", DbType.Int32, senal.LugarId);
            }
            else
            {
                db.AddInParameter(cmd, "_LugarId", DbType.Int32, null);
            }

            return cmd;
        }
    }
}
