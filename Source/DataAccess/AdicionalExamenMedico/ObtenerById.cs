﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.AdicionalExamenMedico
{
    public class ObtenerById : ISelectFactory<Entity.AdicionalExamenMedico>
    {

        public DbCommand ConstructSelectCommand(Database db, Entity.AdicionalExamenMedico item)
        {


            string sp = "";
            if (item.Clasificacion == "tatuaje")
                sp = "Tatuaje_GetById_SP";
            if (item.Clasificacion == "lesion")
                sp = "Lesion_GetById_SP";
            if (item.Clasificacion == "intoxicacion")
                sp = "Intoxicacion_GetById_SP";

            DbCommand cmd = db.GetStoredProcCommand(sp);
            db.AddInParameter(cmd, "_Id", DbType.Int32,item.Id);
            
            return cmd;
        }

    }
}
