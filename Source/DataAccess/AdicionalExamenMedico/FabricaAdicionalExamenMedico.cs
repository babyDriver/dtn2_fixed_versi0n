﻿using DataAccess.Interfaces;
using System.Data;
using DataAccess.Catalogo;

namespace DataAccess.AdicionalExamenMedico
{
    public class FabricaAdicionalExamenMedico : IEntityFactory<Entity.AdicionalExamenMedico>
    {
        private static ServicioCatalogo servicioCatalogo = new ServicioCatalogo("SQLConnectionString");
        public Entity.AdicionalExamenMedico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.AdicionalExamenMedico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("ExamenMedicoId");
            if (!dr.IsDBNull(index))
            {
                item.ExamenMedicoId = dr.GetInt32(index);
            }

          

            index = dr.GetOrdinal("Observacion");
            if (!dr.IsDBNull(index))
            {
                item.Observacion = dr.GetString(index);
            }
             index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Clasificacion");
            if (!dr.IsDBNull(index))
            {
                item.Clasificacion = dr.GetString(index);
            }

            var parametros = new object[2];
            if (item.Clasificacion.ToUpper() == "Tatuaje".ToUpper())
            {
                index = dr.GetOrdinal("TipoTatuajeId");
                if (!dr.IsDBNull(index))
                {
                    parametros[0] = dr.GetInt32(index);
                }
                parametros[1] = Entity.TipoDeCatalogo.tipo_tatuaje;
                item.Tipo = servicioCatalogo.ObtenerPorId(parametros);

                index = dr.GetOrdinal("LugarId");
                if (!dr.IsDBNull(index))
                {
                    item.Lugar = dr.GetInt32(index);
                }
            }

            if (item.Clasificacion == "lesion")
            {
                index = dr.GetOrdinal("TipoLesionId");
                if (!dr.IsDBNull(index))
                {
                    parametros[0] = dr.GetInt32(index);
                }
                parametros[1] = Entity.TipoDeCatalogo.tipo_lesion;
                item.Tipo = servicioCatalogo.ObtenerPorId(parametros);

                index = dr.GetOrdinal("LugarId");
                if (!dr.IsDBNull(index))
                {
                    item.Lugar = dr.GetInt32(index);
                }
            }
            if (item.Clasificacion == "intoxicacion")
            {
                index = dr.GetOrdinal("TipoIntoxicacionId");
                if (!dr.IsDBNull(index))
                {
                    parametros[0] = dr.GetInt32(index);
                }
                parametros[1] = Entity.TipoDeCatalogo.tipo_intoxicacion;
                item.Tipo = servicioCatalogo.ObtenerPorId(parametros);

                
            }

            return item;
        }
    }
}
