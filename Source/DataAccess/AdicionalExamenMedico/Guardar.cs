﻿using System.Data;
using System.Data.Common;
using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.AdicionalExamenMedico
{
    public class Guardar : IInsertFactory<Entity.AdicionalExamenMedico>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.AdicionalExamenMedico item) 
        {

            string sp = "";
            string param = "";
            if (item.Clasificacion == "tatuaje")
            {
                sp = "Tatuaje_Insert_SP";
                param = "_TipoTatuajeId";
            }

            if (item.Clasificacion == "lesion")
            {
                sp = "Lesion_Insert_SP";
                param = "_TipoLesionId";
            }
            if (item.Clasificacion == "intoxicacion")
            {
                sp = "Intoxicacion_Insert_SP";
                param = "_TipoIntoxicacionId";
            }

            DbCommand cmd = db.GetStoredProcCommand(sp);
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_ExamenMedicoId", DbType.Int32, item.ExamenMedicoId);
            if (item.Clasificacion != "intoxicacion")
            {
                db.AddInParameter(cmd, "_LugarId", DbType.Int32, System.Convert.ToInt32(item.Lugar));
            }
            db.AddInParameter(cmd, param, DbType.String, item.Tipo.Id);
            db.AddInParameter(cmd, "_Observacion", DbType.String, item.Observacion);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, item.Creadopor);

            return cmd;
        }

    }
}
