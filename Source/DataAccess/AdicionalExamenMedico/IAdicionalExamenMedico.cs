﻿using System;
using System.Collections.Generic;

namespace DataAccess.AdicionalExamenMedico
{
    public interface IAdicionalExamenMedico
    {
        List<Entity.AdicionalExamenMedico> ObtenerTodos();
        Entity.AdicionalExamenMedico ObtenerById(Entity.AdicionalExamenMedico item);
        List<Entity.AdicionalExamenMedico> ObtenerByExamenMedicoId(Entity.AdicionalExamenMedico item);
        Entity.AdicionalExamenMedico ObtenerByTrackingId(Entity.AdicionalExamenMedico item);
        int Guardar(Entity.AdicionalExamenMedico item);
        void Actualizar(Entity.AdicionalExamenMedico item);
    }
}
