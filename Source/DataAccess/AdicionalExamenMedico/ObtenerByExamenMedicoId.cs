﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.AdicionalExamenMedico
{
    public class ObtenerByExamenMedicoId : ISelectFactory<Entity.AdicionalExamenMedico>
    {

        public DbCommand ConstructSelectCommand(Database db, Entity.AdicionalExamenMedico item)
        {

            string sp = "";
            if (item.Clasificacion == "tatuaje")
                sp = "Tatuaje_GetByExamenMedicoId_SP";
            if (item.Clasificacion == "lesion")
                sp = "Lesion_GetByExamenMedicoId_SP";
            if (item.Clasificacion == "intoxicacion")
                sp = "Intoxicacion_GetByExamenMedicoId_SP";

            DbCommand cmd = db.GetStoredProcCommand(sp);
            db.AddInParameter(cmd, "_ExamenMedicoId", DbType.Int32,item.ExamenMedicoId);
            
            return cmd;
        }

    }
}
