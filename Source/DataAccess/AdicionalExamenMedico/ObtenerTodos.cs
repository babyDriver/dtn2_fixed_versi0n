﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.AdicionalExamenMedico
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.NullClass sp)
        {
            DbCommand cmd = db.GetStoredProcCommand("Intoxicacion_GetAll_SP");
            return cmd;
        }
    }
}
