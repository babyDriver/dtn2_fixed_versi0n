﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.AdicionalExamenMedico
{
    public class ObtenerHabilitados : ISelectFactory<Entity.AdicionalExamenMedico>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.AdicionalExamenMedico item)
        {

            string sp = "";
            if (item.Clasificacion == "tatuaje")
                sp = "Tatuaje_GetAll_SP";
            if (item.Clasificacion == "tipo_lesion")
                sp = "Lesion_GetAll_SP";
            if (item.Clasificacion == "tipo_intoxicacion")
                sp = "Intoxicacion_GetAll_SP";

            DbCommand cmd = db.GetStoredProcCommand(sp);

            return cmd;
        }
      
    }
}
