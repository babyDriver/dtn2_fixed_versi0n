﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;


namespace DataAccess.AdicionalExamenMedico
{
    public class ServicioAdicionalExamenMedico : ClsAbstractService<Entity.AdicionalExamenMedico>, IAdicionalExamenMedico
    {
        public ServicioAdicionalExamenMedico(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.AdicionalExamenMedico> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaAdicionalExamenMedico(), new Entity.NullClass());
        }

        public Entity.AdicionalExamenMedico ObtenerByTrackingId(Entity.AdicionalExamenMedico item)
        {
            ISelectFactory<Entity.AdicionalExamenMedico> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaAdicionalExamenMedico(), item);
        }

        public Entity.AdicionalExamenMedico ObtenerById(Entity.AdicionalExamenMedico item)
        {
            ISelectFactory<Entity.AdicionalExamenMedico> select = new ObtenerById();
            return GetByKey(select, new FabricaAdicionalExamenMedico(), item);
        }

        public List<Entity.AdicionalExamenMedico> ObtenerByExamenMedicoId(Entity.AdicionalExamenMedico item)
        {
            ISelectFactory<Entity.AdicionalExamenMedico> select = new ObtenerByExamenMedicoId();
            return GetAll(select, new FabricaAdicionalExamenMedico(), item);
        }

        public int Guardar(Entity.AdicionalExamenMedico item)
        {
            IInsertFactory<Entity.AdicionalExamenMedico> insert = new Guardar();
            return Insert(insert, item);
        }

        public void Actualizar(Entity.AdicionalExamenMedico item)
        {
            IUpdateFactory<Entity.AdicionalExamenMedico> update = new Actualizar();
            Update(update, item);
        }

    }
}
