﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.AdicionalExamenMedico
{
    public class ObtenerByTrackingId : ISelectFactory< Entity.AdicionalExamenMedico>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.AdicionalExamenMedico item)
        {

            string sp = "";
            if (item.Clasificacion == "tatuaje")
                sp = "Tatuaje_GetByTrackingId_SP";
            if (item.Clasificacion == "lesion")
                sp = "Lesion_GetByTrackingId_SP";
            if (item.Clasificacion == "intoxicacion")
                sp = "Intoxicacion_GetByTrackingId_SP";

            DbCommand cmd = db.GetStoredProcCommand(sp);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            return cmd;
        }
       
    }
}