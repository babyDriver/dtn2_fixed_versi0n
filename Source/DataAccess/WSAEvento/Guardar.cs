﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.WSAEvento
{
    public class Guardar : IInsertFactory<Entity.WSAEvento>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSAEvento item)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAEvento_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId.ToString());
            db.AddInParameter(cmd, "_IdUnidad", DbType.Int32, item.IdUnidad);
            db.AddInParameter(cmd, "_IdInstitucion", DbType.Int32, item.IdInstitucion);
            db.AddInParameter(cmd, "_IdLugar", DbType.Int32, item.IdLugar);
            db.AddInParameter(cmd, "_Folio", DbType.String, item.Folio);
            db.AddInParameter(cmd, "_FechaEvento", DbType.DateTime, item.FechaEvento);
            db.AddInParameter(cmd, "_FechaCapturaDetenidos", DbType.DateTime, item.FechaCapturaDetenidos);
            db.AddInParameter(cmd, "_NumeroDetenidos", DbType.Int32, item.NumeroDetenidos);
            db.AddInParameter(cmd, "_NombreResponsable", DbType.String, item.NombreResponsable);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, item.Descripcion);
            db.AddInParameter(cmd, "_IdMotivo", DbType.Int32, item.IdMotivo);

            return cmd;
        }
    }
}
