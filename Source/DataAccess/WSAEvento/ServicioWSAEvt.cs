﻿using DataAccess.Interfaces;
using Entity;

namespace DataAccess.WSAEvt
{
    public class ServicioWSAEvt : ClsAbstractService<Entity.WSAEvt>, IWSAEvt
    {
        public ServicioWSAEvt(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.WSAEvt item)
        {
            IInsertFactory<Entity.WSAEvt> insert = new GuardarEvt();
            return Insert(insert, item);
        }
                
        public Entity.WSAEvt ObtenerPorId(int EvtWS)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSAEvt(), EvtWS);
        }

    }
}
