﻿using DataAccess.Interfaces;
using Entity;

namespace DataAccess.WSAEvento
{
    public class ServicioWSAEvento : ClsAbstractService<Entity.WSAEvento>, IWSAEvento
    {
        public ServicioWSAEvento(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.WSAEvento item)
        {
            IInsertFactory<Entity.WSAEvento> insert = new Guardar();
            return Insert(insert, item);
        }
                
        public Entity.WSAEvento ObtenerPorId(int EventoIdWS)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSAEvento(), EventoIdWS);
        }

        public Entity.WSAEvento ObtenerPorFolio(string folio)
        {
            ISelectFactory<string> select = new ObtenerByFolio();
            return GetByKey(select, new FabricaWSAEvento(), folio);
        }
    }
}
