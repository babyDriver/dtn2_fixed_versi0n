﻿namespace DataAccess.WSAEvento
{
    public interface IWSAEvento
    {
        int Guardar(Entity.WSAEvento item);
        Entity.WSAEvento ObtenerPorId(int EventoIdWS);
        Entity.WSAEvento ObtenerPorFolio(string folio);
    }
}
