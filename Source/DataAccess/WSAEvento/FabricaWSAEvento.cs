﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.WSAEvento
{
    public class FabricaWSAEvento : IEntityFactory<Entity.WSAEvento>
    {
        public Entity.WSAEvento ConstructEntity(IDataReader dr)
        {
            var evento = new Entity.WSAEvento();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                evento.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                evento.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("IdUnidad");
            if (!dr.IsDBNull(index))
            {
                evento.IdUnidad = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("IdInstitucion");
            if (!dr.IsDBNull(index))
            {
                evento.IdInstitucion = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("IdLugar");
            if (!dr.IsDBNull(index))
            {
                evento.IdLugar = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                evento.Folio = dr.GetString(index);
            }
            index = dr.GetOrdinal("FechaEvento");
            if (!dr.IsDBNull(index))
            {
                evento.FechaEvento = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("FechaCapturaDetenidos");
            if (!dr.IsDBNull(index))
            {
                evento.FechaCapturaDetenidos = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("NumeroDetenidos");
            if (!dr.IsDBNull(index))
            {
                evento.NumeroDetenidos = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("NombreResponsable");
            if (!dr.IsDBNull(index))
            {
                evento.NombreResponsable = dr.GetString(index);
            }
            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                evento.Descripcion = dr.GetString(index);
            }
            index = dr.GetOrdinal("IdMotivo");
            if (!dr.IsDBNull(index))
            {
                evento.IdMotivo = dr.GetInt32(index);
            }

            return evento;
        }
    }
}
