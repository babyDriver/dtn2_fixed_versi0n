﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAEvento
{
    public class ObtenerByFolio : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAEvento_GetByFolio_SP");
            db.AddInParameter(cmd, "_Folio", DbType.String, identity);

            return cmd;
        }
    }
}
