﻿namespace DataAccess.WSAEvt { 
    public interface IWSAEvt
    {
        int Guardar(Entity.WSAEvt item);
        Entity.WSAEvt ObtenerPorId(int EventoIdWS);
    }
}
