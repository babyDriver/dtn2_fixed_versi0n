﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.WSAEvt
{
    public class FabricaWSAEvt : IEntityFactory<Entity.WSAEvt>
    {
        public Entity.WSAEvt ConstructEntity(IDataReader dr)
        {
            var evento = new Entity.WSAEvt();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                evento.Id = dr.GetInt32(index);
            }
			index = dr.GetOrdinal("EventoId");
			if (!dr.IsDBNull(index))
			{
				evento.EventoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Referencia");
            if (!dr.IsDBNull(index))
            {
                evento.Referencia = dr.GetString(index);
            }
            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                evento.Folio = dr.GetString(index);
			}
			index = dr.GetOrdinal("Fecha");
			if (!dr.IsDBNull(index))
			{
				evento.Fecha = dr.GetDateTime(index);
			}
			index = dr.GetOrdinal("Origen");
            if (!dr.IsDBNull(index))
            {
                evento.IdOrigen = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                evento.IdUnidad = dr.GetInt32(index);
            }
			index = dr.GetOrdinal("Institucion");
            if (!dr.IsDBNull(index))
            {
                evento.IdInstitucion = dr.GetInt32(index);
			}
			index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                evento.IdMotivo = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Domicilio");
            if (!dr.IsDBNull(index))
            { 
                evento.IdDomicilio = dr.GetInt32(index);
			}
            index = dr.GetOrdinal("Colonia");
            if (!dr.IsDBNull(index))
            {
                evento.IdColonia = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Vehiculo");
            if (!dr.IsDBNull(index))
            {
                evento.IdVehiculo = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Log");
            if (!dr.IsDBNull(index))
            {
                evento.IdVehiculo = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("NoDet");
			if (!dr.IsDBNull(index))
			{
				evento.NoDetenidos = dr.GetInt32(index);
			}
			index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                evento.Descripcion = dr.GetString(index);
            }

            return evento;
        }
    }
}
