﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.WSAEvt
{
    public class GuardarEvt : IInsertFactory<Entity.WSAEvt>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSAEvt item)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAEvt_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, item.EventoId);
            db.AddInParameter(cmd, "_Folio", DbType.String, item.Folio);
            db.AddInParameter(cmd, "_Referencia", DbType.String, item.Referencia);
			db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
			db.AddInParameter(cmd, "_Origen", DbType.Int32, item.IdOrigen);
			db.AddInParameter(cmd, "_Unidad", DbType.Int32, item.IdUnidad);
			db.AddInParameter(cmd, "_Institucion", DbType.Int32, item.IdInstitucion);
			db.AddInParameter(cmd, "_Motivo", DbType.Int32, item.IdMotivo);
            db.AddInParameter(cmd, "_Vehiculo", DbType.Int32, item.IdVehiculo);
            db.AddInParameter(cmd, "_Colonia", DbType.Int32, item.IdColonia);
            db.AddInParameter(cmd, "_Domicilio", DbType.Int32, item.IdDomicilio);
            db.AddInParameter(cmd, "_NoDetenidos", DbType.Int32, item.NoDetenidos);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, item.Descripcion);
			db.AddInParameter(cmd, "_FechaCreacion", DbType.DateTime, DateTime.Now);
			db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, 1);
            db.AddInParameter(cmd, "_Log", DbType.Boolean, item.Log);

            return cmd;
        }
    }
}
