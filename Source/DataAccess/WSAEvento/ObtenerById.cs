﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.WSAEvento
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int EventoIdWS)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAEvento_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, EventoIdWS);
            return cmd;
        }
    }
}
