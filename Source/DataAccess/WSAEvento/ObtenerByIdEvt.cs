﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.WSAEvt
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int EvtWS)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAEvento_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, EvtWS);
            return cmd;
        }
    }
}
