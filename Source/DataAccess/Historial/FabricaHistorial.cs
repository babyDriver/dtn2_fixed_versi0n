﻿using DataAccess.Interfaces;

namespace DataAccess.Historial
{
    public class FabricaHistorial : IEntityFactory<Entity.Historial>
    {
        public Entity.Historial ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.Historial();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("InternoId");
            if (!dr.IsDBNull(index))
            {
                item.InternoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Movimiento");
            if (!dr.IsDBNull(index))
            {
                item.Movimiento = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ModificadoPor");
            if (!dr.IsDBNull(index))
            {
                item.ModificadoPor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }


            return item;
        }
    }
}
