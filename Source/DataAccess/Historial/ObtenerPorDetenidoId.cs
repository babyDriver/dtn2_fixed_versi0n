﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.Historial
{
    public class ObtenerPorDetenidoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Historial_GetByDetenidoId_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, identity);

            return cmd;
        }
    }
}
