﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.Historial
{
    public class Actualizar : IUpdateFactory<Entity.Historial>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Historial item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Historial_Insert_FechaUpdate");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_ModificadoPor", DbType.Int32, item.ModificadoPor);
            

            return cmd;
        }
    }
}
