﻿using System;
using System.Collections.Generic;

namespace DataAccess.Historial
{
    public interface IHistorial
    {
        int Guardar(Entity.Historial item);
        void Actualizar(Entity.Historial item);
        Entity.Historial ObtenerPorId(int id);
        List<Entity.Historial> ObtenerPorDetenidoId(int id);

    }
}
