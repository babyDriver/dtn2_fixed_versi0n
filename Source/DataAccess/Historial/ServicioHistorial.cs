﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Historial
{
    public class ServicioHistorial : ClsAbstractService<Entity.Historial>, IHistorial
    {
        public ServicioHistorial(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.Historial item)
        {
            IUpdateFactory<Entity.Historial> update = new Actualizar();
             Update(update, item);
        }

        public int Guardar(Entity.Historial item)
        {
            IInsertFactory<Entity.Historial> insert = new Guardar();
            return Insert(insert, item);
        }

        public List<Entity.Historial> ObtenerPorDetenidoId(int id)
        {
            ISelectFactory<int> select = new ObtenerPorDetenidoId();
            return GetAll(select, new FabricaHistorial(), id);
        }

        public Entity.Historial ObtenerPorId(int id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaHistorial(), id);
        }
    }
}
