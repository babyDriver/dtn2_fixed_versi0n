﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Historial
{
    public class Guardar : IInsertFactory<Entity.Historial>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Historial item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Historial_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId.ToString());
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, item.InternoId);
            db.AddInParameter(cmd, "_Movimiento", DbType.String, item.Movimiento);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            return cmd;
        }
    }
}
