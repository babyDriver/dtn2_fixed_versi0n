﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DetenidosPorEdad
{
   public interface IDetenidosPorEdad
    {
        List<Entity.Detenidosporedad> GetDetenidosporEdad(string[] filtro);
        List<Entity.Detenidosporedad> GetDetenidosporEdadLesion(string[] filtro);
    }
}
