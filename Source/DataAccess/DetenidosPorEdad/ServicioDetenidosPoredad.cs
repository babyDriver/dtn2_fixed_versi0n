﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DetenidosPorEdad
{
    public class ServicioDetenidosPoredad : ClsAbstractService<Entity.Detenidosporedad>, IDetenidosPorEdad
    {
        public ServicioDetenidosPoredad(String dataBase)
            : base(dataBase)
        {
        }

        public List<Detenidosporedad> GetDetenidosporEdad(string[] filtro)
        {
            ISelectFactory<string[]> select = new ObtenerDetenidosPorEdad();
            return GetAll(select, new FabricaDetenidosPorEdad(), filtro);
        }

        public List<Detenidosporedad> GetDetenidosporEdadLesion(string[] filtro)
        {
            ISelectFactory<string[]> select = new DetenidosporEdadLesion();
            return GetAll(select, new FabricaDetenidosPorEdad(), filtro);
        }
    }
}