﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DetenidosPorEdad
{
    public class FabricaDetenidosPorEdad : IEntityFactory<Entity.Detenidosporedad>
    {
        public Detenidosporedad ConstructEntity(IDataReader dr)
        {
            var detenidosporedad = new Entity.Detenidosporedad();

            var index = dr.GetOrdinal("Sexoid");
            if (!dr.IsDBNull(index))
            {
                detenidosporedad.Sexoid = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                detenidosporedad.Sexo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                detenidosporedad.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("cantidad");
            if (!dr.IsDBNull(index))
            {
                detenidosporedad.cantidad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                detenidosporedad.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Subcontrato");
            if (!dr.IsDBNull(index))
            {
                detenidosporedad.Subcontrato = dr.GetInt32(index);
            }

            return detenidosporedad;
        }
    }
}