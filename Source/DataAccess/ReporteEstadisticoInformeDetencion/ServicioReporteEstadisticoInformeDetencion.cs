﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.ReporteEstadisticoInformeDetencion
{
    public class ServicioReporteEstadisticoInformeDetencion : ClsAbstractService<Entity.ReporteEstadisticoInformeDetencion>, IReporteEstadisticoInformeDetencion
    {
        public ServicioReporteEstadisticoInformeDetencion(string dataBase)
       : base(dataBase)
        {

        }
        public List<Entity.ReporteEstadisticoInformeDetencion> GetDetail(string[] filter)
        {
            ISelectFactory<string[]> select = new ObtenerInformesDetencion();
            return GetAll(select, new FabricaReporteEstadisticoInformeDetencion(), filter);
        }
    }
}