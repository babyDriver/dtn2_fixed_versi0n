﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteEstadisticoInformeDetencion
{
    public class FabricaReporteEstadisticoInformeDetencion : IEntityFactory<Entity.ReporteEstadisticoInformeDetencion>
    {
        public Entity.ReporteEstadisticoInformeDetencion ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ReporteEstadisticoInformeDetencion();

            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                item.Remision = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecharegistro");
            if (!dr.IsDBNull(index))
            {
                item.Fecharegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Fechadetencion");
            if (!dr.IsDBNull(index))
            {
                item.Fechadetencion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fechanacimiento");
            if (!dr.IsDBNull(index))
            {
                item.Fechanacimiento = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                item.Sexo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nacionalidad");
            if (!dr.IsDBNull(index))
            {
                item.Nacionalidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Domicilio");
            if (!dr.IsDBNull(index))
            {
                item.Domicilio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Colonia");
            if (!dr.IsDBNull(index))
            {
                item.Colonia = dr.GetString(index);
            }

            index = dr.GetOrdinal("Ciudad");
            if (!dr.IsDBNull(index))
            {
                item.Ciudad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                item.Estado = dr.GetString(index);
            }


            index = dr.GetOrdinal("Telefono");
            if (!dr.IsDBNull(index))
            {
                item.Telefono = dr.GetString(index);
            }

            //index = dr.GetOrdinal("Seniasparticulares");
            //if (!dr.IsDBNull(index))
            //{
            //    item.Seniasparticulares = dr.GetString(index);
            //}

            index = dr.GetOrdinal("Alias");
            if (!dr.IsDBNull(index))
            {
                item.Alias = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estadocivil");
            if (!dr.IsDBNull(index))
            {
                item.Estadocivil = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estatura");
            if (!dr.IsDBNull(index))
            {
                item.Estatura = dr.GetDouble(index);
            }

            index = dr.GetOrdinal("Peso");
            if (!dr.IsDBNull(index))
            {
                item.peso = dr.GetDouble(index);
            }

            index = dr.GetOrdinal("Ocupacion");
            if (!dr.IsDBNull(index))
            {
                item.Ocupacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Escolaridad");
            if (!dr.IsDBNull(index))
            {
                item.Escolaridad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Sueldo");
            if (!dr.IsDBNull(index))
            {
                item.Sueldo = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Noevento");
            if (!dr.IsDBNull(index))
            {
                item.Noevento = dr.GetString(index);
            }

            index = dr.GetOrdinal("Eventodescripcion");
            if (!dr.IsDBNull(index))
            {
                item.Eventodescripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Lugardetencion");
            if (!dr.IsDBNull(index))
            {
                item.Lugardetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Coloniadetencion");
            if (!dr.IsDBNull(index))
            {
                item.Coloniadetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Claveunidad");
            if (!dr.IsDBNull(index))
            {
                item.Claveunidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Claveagente");
            if (!dr.IsDBNull(index))
            {
                item.Claveagente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombreagente");
            if (!dr.IsDBNull(index))
            {
                item.Nombreagente = dr.GetString(index);
            }
            return item;
        }
    }
}