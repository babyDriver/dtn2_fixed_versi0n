﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteEstadisticoInformeDetencion
{
    public interface IReporteEstadisticoInformeDetencion
    {
        List<Entity.ReporteEstadisticoInformeDetencion> GetDetail(string[] filter);
    }
}