﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.ReporteEstadisticoInformeDetencion
{
    public class ObtenerInformesDetencion : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] filtros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ReporteEstadistico_Informededetencion_SP");
            db.AddInParameter(cmd, "_Fechainicio", DbType.Date, Convert.ToDateTime(filtros[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.Date, Convert.ToDateTime(filtros[1]));
            db.AddInParameter(cmd, "_Remisioninicial", DbType.Int32, Convert.ToInt32(filtros[3]));
            db.AddInParameter(cmd, "_Remisionfinal", DbType.Int32, Convert.ToInt32(filtros[4]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(filtros[2]));
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, Convert.ToInt32(filtros[6]));
            return cmd;
        }
    }
}