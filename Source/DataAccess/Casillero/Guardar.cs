﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.Casillero
{
    public class Guardar : IInsertFactory<Entity.Casillero>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Casillero casillero)
        {
            DbCommand cmd = db.GetStoredProcCommand("Casillero_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, casillero.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, casillero.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, casillero.Descripcion);
            db.AddInParameter(cmd, "_Capacidad", DbType.Int32, casillero.Capacidad);
            db.AddInParameter(cmd, "_Disponible", DbType.Int32, casillero.Capacidad);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, casillero.Activo);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, casillero.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, casillero.Tipo);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, casillero.Creadopor);


            return cmd;
        }
    }
}
