﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Casillero
{
    public class ObtenerPorNombre : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("casillero_GetByNombre_SP");
            db.AddInParameter(cmd, "_Nombre", DbType.String, nombre);
            return cmd;
        }
    }
}
