﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Casillero
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Casillero_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, Id);
            return cmd;
        }
    }
}
