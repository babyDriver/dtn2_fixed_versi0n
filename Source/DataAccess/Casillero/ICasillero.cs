﻿using System;
using System.Collections.Generic;

namespace DataAccess.Casillero
{
    public interface ICasillero
    {
        List<Entity.Casillero> ObtenerTodos();
        Entity.Casillero ObtenerById(int Id);
        Entity.Casillero ObtenerByNombre(string nombre);
        int Guardar(Entity.Casillero celda);
        void Actualizar(Entity.Casillero celda);
    }
}
