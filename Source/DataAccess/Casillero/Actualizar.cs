﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Casillero
{
    public class Actualizar : IUpdateFactory<Entity.Casillero>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Casillero casillero)
        {
            DbCommand cmd = db.GetStoredProcCommand("Casillero_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, casillero.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, casillero.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, casillero.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, casillero.Descripcion);
            db.AddInParameter(cmd, "_Capacidad", DbType.Int32, casillero.Capacidad);
            db.AddInParameter(cmd, "_Disponible", DbType.Int32, casillero.Capacidad);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, casillero.Activo);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, casillero.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, casillero.Tipo);
            db.AddInParameter(cmd,"_Habilitado",DbType.Int32, casillero.Habilitado);


            return cmd;
        }
    }
}
