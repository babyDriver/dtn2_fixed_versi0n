﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Casillero
{
    public class FabricaCasillero : IEntityFactory<Entity.Casillero>
    {
        public Entity.Casillero ConstructEntity(IDataReader dr)
        {
            var casillero = new Entity.Casillero();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                casillero.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                casillero.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                casillero.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                casillero.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                casillero.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Capacidad");
            if (!dr.IsDBNull(index))
            {
                casillero.Capacidad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Disponible");
            if (!dr.IsDBNull(index))
            {
                casillero.Disponible = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                casillero.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                casillero.Tipo = dr.GetString(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                casillero.Habilitado = dr.GetInt32(index);
            }
            return casillero;
        }
    }
}
