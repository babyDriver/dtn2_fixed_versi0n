﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Casillero
{
    public class ServicioCasillero : ClsAbstractService<Entity.Casillero>, ICasillero
    {
        public ServicioCasillero(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.Casillero casillero)
        {
            IUpdateFactory<Entity.Casillero> update = new Actualizar();
            Update(update, casillero);
        }

        public int Guardar(Entity.Casillero casillero)
        {
            IInsertFactory<Entity.Casillero> insert = new Guardar();
            return Insert(insert, casillero);
        }

        public Entity.Casillero ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaCasillero(), Id);
        }

        public List<Entity.Casillero> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaCasillero(), new Entity.NullClass());
        }

        public Entity.Casillero ObtenerByNombre(string nombre)
        {
            ISelectFactory<string> select = new ObtenerPorNombre();
            return GetByKey(select, new FabricaCasillero(), nombre);
        }
    }
}
