﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DetalleDetenidoPorMotivo
{
    public class FabricaDetalleDetenidoPorMotivo : IEntityFactory<Entity.Detalledetenidopormotivo>
    {
        public Detalledetenidopormotivo ConstructEntity(IDataReader dr)
        {
            var detalle = new Entity.Detalledetenidopormotivo();

            var index = dr.GetOrdinal("MotivodetencionId");
            if (!dr.IsDBNull(index))
            {
                detalle.MotivodetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Motivodetencion");
            if (!dr.IsDBNull(index))
            {
                detalle.Motivodetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                detalle.Remision = dr.GetString(index);
            }
            index = dr.GetOrdinal("Detenido");
            if (!dr.IsDBNull(index))
            {
                detalle.Detenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("Alias");
            if (!dr.IsDBNull(index))
            {
                detalle.Alias = dr.GetString(index);
            }

            index = dr.GetOrdinal("Domicilio");
            if (!dr.IsDBNull(index))
            {
                detalle.Domicilio = dr.GetString(index);
            }
            index = dr.GetOrdinal("SituacionId");
            if (!dr.IsDBNull(index))
            {
                detalle.SituacionId = dr.GetInt32(index);
            }
            return detalle;

        }
    }
}
