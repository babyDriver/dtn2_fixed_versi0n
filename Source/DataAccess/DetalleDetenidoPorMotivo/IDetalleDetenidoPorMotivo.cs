﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DetalleDetenidoPorMotivo
{
    public interface IDetalleDetenidoPorMotivo
    {
        List<Entity.Detalledetenidopormotivo> GetIDetalleDetenidoPorMotivo(string[] filtro);
       
    }
}
