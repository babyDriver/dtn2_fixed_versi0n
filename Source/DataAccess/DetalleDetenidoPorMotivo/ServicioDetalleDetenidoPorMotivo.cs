﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;
namespace DataAccess.DetalleDetenidoPorMotivo
{
    public class ServicioDetalleDetenidoPorMotivo : ClsAbstractService<Entity.Detalledetenidopormotivo>, IDetalleDetenidoPorMotivo
    {
        public ServicioDetalleDetenidoPorMotivo(string dataBase)
        : base(dataBase)
        {
        }
        public List<Detalledetenidopormotivo> GetIDetalleDetenidoPorMotivo(string[] filtro)
        {
            ISelectFactory<string[]> select = new GetDetalleDetenidosporMotivo();
            return GetAll(select, new FabricaDetalleDetenidoPorMotivo(), filtro);
        }
    }
}
