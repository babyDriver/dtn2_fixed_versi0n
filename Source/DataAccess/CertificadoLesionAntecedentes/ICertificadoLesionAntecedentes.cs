﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionAntecedentes
{
    public interface ICertificadoLesionAntecedentes
    {
        int Guardar(Entity.CertificadoLesionAntecedentes certificadoLesionTipoLesion);
        Entity.CertificadoLesionAntecedentes ObtenerPorId(int Id);
    }
}
