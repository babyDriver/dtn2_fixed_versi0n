﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoLesionAntecedentes
{
    public class Guardar : IInsertFactory<Entity.CertificadoLesionAntecedentes>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CertificadoLesionAntecedentes entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionantecedentes_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, entity.FechaHora);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, entity.Creadopor);
            return cmd;
        }
    }
}
