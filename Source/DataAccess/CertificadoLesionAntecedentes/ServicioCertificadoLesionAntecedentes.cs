﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;


namespace DataAccess.CertificadoLesionAntecedentes
{
   public class ServicioCertificadoLesionAntecedentes : ClsAbstractService<Entity.CertificadoLesionAntecedentes>, ICertificadoLesionAntecedentes
    {
        public ServicioCertificadoLesionAntecedentes(string dataBase)
    : base(dataBase)
        {
        }
        public int Guardar(Entity.CertificadoLesionAntecedentes certificadoLesionTipoLesion)
        {
            IInsertFactory<Entity.CertificadoLesionAntecedentes> insert = new Guardar();
            return Insert(insert, certificadoLesionTipoLesion);
        }

        public Entity.CertificadoLesionAntecedentes ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoLesionAntecedentes(), Id);
        }
    }
}
