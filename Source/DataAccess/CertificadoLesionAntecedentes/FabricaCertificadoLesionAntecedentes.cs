﻿using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.CertificadoLesionAntecedentes
{
    public class FabricaCertificadoLesionAntecedentes : IEntityFactory<Entity.CertificadoLesionAntecedentes>
    {
        public Entity.CertificadoLesionAntecedentes ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoLesionAntecedentes();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }
            return item;
        }
    }
}
