﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoLesionAntecedentesDetalle
{
    public class Guardar : IInsertFactory<Entity.CertificadoAntecedentesDetalle>
    {
        public DbCommand ConstructInsertCommand(Database db, CertificadoAntecedentesDetalle entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionAntecedentesdetalle_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_certificado_LesionAntecedentesId", DbType.Int32, entity.certificado_LesionAntecedentesId);
            db.AddInParameter(cmd, "_AntecedenteId", DbType.Int32, entity.AntecedenteId);

            return cmd;
        }
    }
}
