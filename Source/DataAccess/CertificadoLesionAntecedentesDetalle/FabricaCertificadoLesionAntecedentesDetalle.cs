﻿using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.CertificadoLesionAntecedentesDetalle
{
    public class FabricaCertificadoLesionAntecedentesDetalle : IEntityFactory<Entity.CertificadoAntecedentesDetalle>
    {
        public CertificadoAntecedentesDetalle ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoAntecedentesDetalle();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("certificado_LesionAntecedentesId");
            if (!dr.IsDBNull(index))
            {
                item.certificado_LesionAntecedentesId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("AntecedenteId");
            if (!dr.IsDBNull(index))
            {
                item.AntecedenteId = dr.GetInt32(index);
            }
            

            return item;
        }
    }
}
