﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionAntecedentesDetalle
{
   public interface ICertificadoLesionAntecedentesDetalle
    {
        int Guardar(Entity.CertificadoAntecedentesDetalle certificadoAntecedentesDetalle);
        List<Entity.CertificadoAntecedentesDetalle> ObtenerPorId(int Id);
    }
}
