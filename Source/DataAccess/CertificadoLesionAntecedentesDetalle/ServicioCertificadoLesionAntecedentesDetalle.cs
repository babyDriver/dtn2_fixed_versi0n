﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.CertificadoLesionAntecedentesDetalle
{
    public class ServicioCertificadoLesionAntecedentesDetalle : ClsAbstractService<Entity.CertificadoAntecedentesDetalle>, ICertificadoLesionAntecedentesDetalle
    {
        public ServicioCertificadoLesionAntecedentesDetalle(string dataBase)
     : base(dataBase)
        {
        }
        public int Guardar(CertificadoAntecedentesDetalle certificadoAntecedentesDetalle)
        {
            IInsertFactory<Entity.CertificadoAntecedentesDetalle> insert = new Guardar();
            return Insert(insert, certificadoAntecedentesDetalle);
        }

        public List<Entity.CertificadoAntecedentesDetalle> ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetAll(select, new FabricaCertificadoLesionAntecedentesDetalle(), Id);
        }
    }
}
