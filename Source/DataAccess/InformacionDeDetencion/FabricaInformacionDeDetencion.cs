﻿using DataAccess.Interfaces;

namespace DataAccess.InformacionDeDetencion
{
    public class FabricaInformacionDeDetencion : IEntityFactory<Entity.InformacionDeDetencion>
    {
        public Entity.InformacionDeDetencion ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.InformacionDeDetencion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("IdUnidad");
            if (!dr.IsDBNull(index))
            {
                item.UnidadId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IdResponsable");
            if (!dr.IsDBNull(index))
            {
                item.ResponsableId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                item.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                item.LugarDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetString(index);
            }

            index = dr.GetOrdinal("HoraYFecha");
            if (!dr.IsDBNull(index))
            {
                item.HoraYFecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("IdColonia");
            if (!dr.IsDBNull(index))
            {
                item.ColoniaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IdEvento");
            if (!dr.IsDBNull(index))
            {
                item.IdEvento = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Personanotifica");
            if (!dr.IsDBNull(index))
            {
                item.Personanotifica = dr.GetString(index);
            }

            index = dr.GetOrdinal("Celular");
            if (!dr.IsDBNull(index))
            {
                item.Celular = dr.GetString(index);
            }

            index = dr.GetOrdinal("IdDetenido_Evento");
            if (!dr.IsDBNull(index))
            {
                item.IdDetenido_Evento = dr.GetInt32(index);
            }

            return item;
        }
    }
}
