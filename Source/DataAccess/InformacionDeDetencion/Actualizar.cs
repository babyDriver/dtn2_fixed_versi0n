﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.InformacionDeDetencion
{
    public class Actualizar : IUpdateFactory<Entity.InformacionDeDetencion>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.InformacionDeDetencion item)
        {
            DbCommand cmd = db.GetStoredProcCommand("InformacionDeDetencion_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);            
            db.AddInParameter(cmd, "_IdUnidad", DbType.Int32, item.UnidadId);
            db.AddInParameter(cmd, "_IdResponsable", DbType.Int32, item.ResponsableId);
            db.AddInParameter(cmd, "_Motivo", DbType.String, item.Motivo);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, item.Descripcion);
            db.AddInParameter(cmd, "_Lugar", DbType.String, item.LugarDetencion);
            db.AddInParameter(cmd, "_Folio", DbType.String, item.Folio);
            db.AddInParameter(cmd, "_HoraYFecha", DbType.DateTime, item.HoraYFecha);
            db.AddInParameter(cmd, "_IdColonia", DbType.Int32, item.ColoniaId);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_IdEvento", DbType.Int32, item.IdEvento);
            db.AddInParameter(cmd, "_Personanotifica", DbType.String, item.Personanotifica);
            db.AddInParameter(cmd, "_Celular", DbType.String, item.Celular);

            return cmd;
        }
    }
}
