﻿using System;
using System.Collections.Generic;

namespace DataAccess.InformacionDeDetencion
{
    public interface IInformacionDeDetencion
    {
        int Guardar(Entity.InformacionDeDetencion item);
        void Actualizar(Entity.InformacionDeDetencion item);
        Entity.InformacionDeDetencion ObtenerById(int id);
        Entity.InformacionDeDetencion ObtenerByTrackingId(Guid guid);
        List<Entity.InformacionDeDetencion> ObtenerTodos();
        Entity.InformacionDeDetencion ObtenerByInternoId(int id);
    }
}
