﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.InformacionDeDetencion
{
    public class Guardar : IInsertFactory<Entity.InformacionDeDetencion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.InformacionDeDetencion item)
        {
            DbCommand cmd = db.GetStoredProcCommand("InformacionDeDetencion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId);
            db.AddInParameter(cmd, "_IdInterno", DbType.Int32, item.IdInterno);
            db.AddInParameter(cmd, "_IdUnidad", DbType.Int32, item.UnidadId);
            db.AddInParameter(cmd, "_IdResponsable", DbType.Int32, item.ResponsableId);
            db.AddInParameter(cmd, "_Motivo", DbType.String, item.Motivo);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, item.Descripcion);
            db.AddInParameter(cmd, "_Lugar", DbType.String, item.LugarDetencion);
            db.AddInParameter(cmd, "_Folio", DbType.String, item.Folio);
            db.AddInParameter(cmd, "_HoraYFecha", DbType.DateTime, item.HoraYFecha);
            db.AddInParameter(cmd, "_IdColonia", DbType.Int32, item.ColoniaId);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, item.IdEvento);
            db.AddInParameter(cmd, "_Personanotifica", DbType.String, item.Personanotifica);
            db.AddInParameter(cmd, "_Celular", DbType.String, item.Celular);
            db.AddInParameter(cmd, "_IdDetenido_Evento", DbType.Int32, item.IdDetenido_Evento);

            return cmd;
        }
    }
}
