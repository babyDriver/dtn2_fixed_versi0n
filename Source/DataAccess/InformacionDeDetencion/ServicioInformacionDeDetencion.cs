﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.InformacionDeDetencion
{
    public class ServicioInformacionDeDetencion : ClsAbstractService<Entity.InformacionDeDetencion>, IInformacionDeDetencion
    {
        public ServicioInformacionDeDetencion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.InformacionDeDetencion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaInformacionDeDetencion(), new Entity.NullClass());
        }

        public int Guardar(Entity.InformacionDeDetencion item)
        {
            IInsertFactory<Entity.InformacionDeDetencion> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.InformacionDeDetencion ObtenerByTrackingId(Guid guid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaInformacionDeDetencion(), guid);
        }

        public Entity.InformacionDeDetencion ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaInformacionDeDetencion(), id);
        }

        public Entity.InformacionDeDetencion ObtenerByInternoId(int id)
        {
            ISelectFactory<int> select = new ObtenerByInternoId();
            return GetByKey(select, new FabricaInformacionDeDetencion(), id);
        }

        public void Actualizar(Entity.InformacionDeDetencion item)
        {
            IUpdateFactory<Entity.InformacionDeDetencion> update = new Actualizar();
            Update(update, item);
        }
    }
}
