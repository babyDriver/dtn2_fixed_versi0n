﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteEstadisticoPorLesion
{
    public class ObtenerPorFiltros : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] filtros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ReporteEstadistico_DetenidosPorTipoLesionCL_SP");
            db.AddInParameter(cmd, "_Fechainicio", DbType.DateTime, DateTime.Parse(filtros[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.DateTime, DateTime.Parse(filtros[1]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(filtros[3]));
            db.AddInParameter(cmd, "_TipoLesionId", DbType.Int32, Convert.ToInt32(filtros[2]));
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, Convert.ToInt32(filtros[7]));
            return cmd;
        }
    }
}
