﻿using DataAccess.Interfaces;
using Entity;
using System.Data;

namespace DataAccess.ReporteEstadisticoPorLesion
{
    public class FabricaReporteEstadisticoPorLesion : IEntityFactory<Entity.ReporteEstadisticoPorLesion>
    {
        public Entity.ReporteEstadisticoPorLesion ConstructEntity(IDataReader dr)
        {
            var detenidos = new Entity.ReporteEstadisticoPorLesion();


            var index = dr.GetOrdinal("Lesion");
            if (!dr.IsDBNull(index))
            {
                detenidos.Lesion = dr.GetString(index);
            }



            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                detenidos.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Cantidad");
            if (!dr.IsDBNull(index))
            {
                detenidos.Cantidad = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                detenidos.Edad = dr.GetInt32(index);
            }

            return detenidos;

        }
    }
}
