﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteEstadisticoPorLesion
{
    public class ServicioReporteEstadisticoPorLesion : ClsAbstractService<Entity.ReporteEstadisticoPorLesion>, IReporteEstadisticoPorLesion
    {
        public ServicioReporteEstadisticoPorLesion(string dataBase)
            : base(dataBase)
        {
        }
        public List<Entity.ReporteEstadisticoPorLesion> GetByFilter(string[] filter)
        {
            ISelectFactory<string[]> select = new ObtenerPorFiltros();
            return GetAll(select, new FabricaReporteEstadisticoPorLesion(), filter);
        }
    }
}
