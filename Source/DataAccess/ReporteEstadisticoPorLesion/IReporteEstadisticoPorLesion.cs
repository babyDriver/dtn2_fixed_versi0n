﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteEstadisticoPorLesion
{
    public interface IReporteEstadisticoPorLesion
    {
        List<Entity.ReporteEstadisticoPorLesion> GetByFilter(string[] filter);
    }
}
