﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ListadoInformacionDetencion
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ListadoInformacionDetencion_GetAll_SP");
            db.AddInParameter(cmd, "_wheres", DbType.String, parametros[0]);
            return cmd;
        }
    }
}
