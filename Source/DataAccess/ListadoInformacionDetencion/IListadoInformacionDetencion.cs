﻿using System.Collections.Generic;

namespace DataAccess.ListadoInformacionDetencion
{
    public interface IListadoInformacionDetencion
    {
        List<Entity.ListadoInformacionDetencion> ObtenerTodos(object[] data);
    }
}
