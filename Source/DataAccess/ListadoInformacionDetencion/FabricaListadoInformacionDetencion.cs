﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoInformacionDetencion
{
    public class FabricaListadoInformacionDetencion : IEntityFactory<Entity.ListadoInformacionDetencion>
    {
        public Entity.ListadoInformacionDetencion ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoInformacionDetencion();

            var index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Apellido_Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Apellido_Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("RutaImagen");
            if (!dr.IsDBNull(index))
            {
                item.RutaImagen = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("NCP");
            if (!dr.IsDBNull(index))
            {
                item.NCP = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                item.Estatus = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingIdEstatus");
            if (!dr.IsDBNull(index))
            {
                item.TrackingIdEstatus = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("EstatusNombre");
            if (!dr.IsDBNull(index))
            {
                item.EstatusNombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            return item;
        }
    }
}
