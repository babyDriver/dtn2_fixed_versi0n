﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoInformacionDetencion
{
    public class ServicioListadoInformacionDetencion : ClsAbstractService<Entity.ListadoInformacionDetencion>, IListadoInformacionDetencion
    {
        public ServicioListadoInformacionDetencion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoInformacionDetencion> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoInformacionDetencion(), parametros);
        }
    }
}
