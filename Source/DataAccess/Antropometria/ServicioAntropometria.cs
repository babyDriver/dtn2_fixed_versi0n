﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Antropometria
{
    public class ServicioAntropometria : ClsAbstractService<Entity.Antropometria>, IAntropometria
    {
        public ServicioAntropometria(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Antropometria> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaAntropometria(), new Entity.NullClass());
        }

   

        public Entity.Antropometria ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaAntropometria(), userId);
        }

        public Entity.Antropometria ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaAntropometria(), Id);
        }

        public Entity.Antropometria ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaAntropometria(), Id);
        }

        public int Guardar(Entity.Antropometria antropometria)
        {
            IInsertFactory<Entity.Antropometria> insert = new Guardar();
            return Insert(insert, antropometria);
        }

        public void Actualizar(Entity.Antropometria antropometria)
        {
            IUpdateFactory<Entity.Antropometria> update = new Actualizar();
            Update(update, antropometria);
        }


    }
}