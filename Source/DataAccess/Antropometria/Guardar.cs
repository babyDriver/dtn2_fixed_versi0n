﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Antropometria
{
    public class Guardar : IInsertFactory<Entity.Antropometria>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Antropometria antropometria)
        {
            DbCommand cmd = db.GetStoredProcCommand("Antropometria_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, antropometria.TrackingId);
            db.AddInParameter(cmd, "_Estatura", DbType.Decimal, antropometria.Estatura);
            db.AddInParameter(cmd, "_Peso", DbType.Decimal, antropometria.Peso);
            db.AddInParameter(cmd, "_FormulaI", DbType.String, antropometria.FormulaI);
            db.AddInParameter(cmd, "_SubformulaI", DbType.String, antropometria.SubformulaI);
            db.AddInParameter(cmd, "_FormulaD", DbType.String, antropometria.FormulaD);
            db.AddInParameter(cmd, "_SubformulaD", DbType.String, antropometria.SubformulaD);
            db.AddInParameter(cmd, "_Vucetich", DbType.String, antropometria.Vucetich);
            db.AddInParameter(cmd, "_Dactiloscopica", DbType.String, antropometria.Dactiloscopica);
            db.AddInParameter(cmd, "_Lunares", DbType.Boolean, antropometria.Lunares);
            db.AddInParameter(cmd, "_Tatuaje", DbType.Boolean, antropometria.Tatuaje);
            db.AddInParameter(cmd, "_Defecto", DbType.Boolean, antropometria.Defecto);
            db.AddInParameter(cmd, "_Cicatris", DbType.Boolean, antropometria.Cicatris);
            db.AddInParameter(cmd, "_Anteojos", DbType.Boolean, antropometria.Anteojos);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, antropometria.DetenidoId);


            return cmd;
        }
    }
}
