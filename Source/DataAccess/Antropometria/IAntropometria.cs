﻿using System;
using System.Collections.Generic;

namespace DataAccess.Antropometria
{
    public interface IAntropometria
    {
        List<Entity.Antropometria> ObtenerTodos();
        Entity.Antropometria ObtenerById(int Id);
        Entity.Antropometria ObtenerByDetenidoId(int Id);
        Entity.Antropometria ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Antropometria antropometria);
        void Actualizar(Entity.Antropometria antropometria);
    }
}