﻿using DataAccess.Interfaces;


namespace DataAccess.Antropometria
{
    public class FabricaAntropometria : IEntityFactory<Entity.Antropometria>
    {
       

        public Entity.Antropometria ConstructEntity(System.Data.IDataReader dr)
        {
            var antropometria = new Entity.Antropometria();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                antropometria.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                antropometria.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Estatura");
            if (!dr.IsDBNull(index))
            {
                antropometria.Estatura = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Peso");
            if (!dr.IsDBNull(index))
            {
                antropometria.Peso = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("FormulaI");
            if (!dr.IsDBNull(index))
            {
                antropometria.FormulaI = dr.GetString(index);
            }

            index = dr.GetOrdinal("SubformulaI");
            if (!dr.IsDBNull(index))
            {
                antropometria.SubformulaI = dr.GetString(index);
            }

            index = dr.GetOrdinal("FormulaD");
            if (!dr.IsDBNull(index))
            {
                antropometria.FormulaD = dr.GetString(index);
            }

            index = dr.GetOrdinal("SubformulaD");
            if (!dr.IsDBNull(index))
            {
                antropometria.SubformulaD = dr.GetString(index);
            }

            index = dr.GetOrdinal("Vucetich");
            if (!dr.IsDBNull(index))
            {
                antropometria.Vucetich = dr.GetString(index);
            }


            index = dr.GetOrdinal("Dactiloscopica");
            if (!dr.IsDBNull(index))
            {
                antropometria.Dactiloscopica = dr.GetString(index);
            }

            index = dr.GetOrdinal("Lunares");
            if (!dr.IsDBNull(index))
            {
                antropometria.Lunares = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Tatuaje");
            if (!dr.IsDBNull(index))
            {
                antropometria.Tatuaje = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Defecto");
            if (!dr.IsDBNull(index))
            {
                antropometria.Defecto = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Cicatris");
            if (!dr.IsDBNull(index))
            {
                antropometria.Cicatris = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Anteojos");
            if (!dr.IsDBNull(index))
            {
                antropometria.Anteojos = dr.GetBoolean(index);
            }


            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                antropometria.DetenidoId = dr.GetInt32(index);
            }



            return antropometria;
        }
    }
}