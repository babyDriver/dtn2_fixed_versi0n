﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.FichaBiometrico
{
    public class ServicioFichaBiometrico : ClsAbstractService<Entity.Fichabiometrico>, IFichaBiometrico
    {
        public ServicioFichaBiometrico(string dataBase)
            : base(dataBase)
        {
        }

        public Fichabiometrico GetFichaBiometrico(int DetenidoId)
        {
            ISelectFactory<int> select = new ObtenerFichaBiometrico();
            return GetByKey(select, new FabricaFichaBiometrico(), DetenidoId);
        }
    }
}
