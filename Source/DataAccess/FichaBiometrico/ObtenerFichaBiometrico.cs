﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.FichaBiometrico
{
    public class ObtenerFichaBiometrico : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int detenidoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Reporte_Fichabiometrico_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, detenidoId);
            return cmd;
        }
    }
}
