﻿using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.FichaBiometrico
{
    public class FabricaFichaBiometrico : IEntityFactory<Entity.Fichabiometrico>
    {
        public Fichabiometrico ConstructEntity(IDataReader dr)
        {
            var fichabiometrico = new Entity.Fichabiometrico();

            var index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.DetenidoId = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Apellidos");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Apellidos = dr.GetString(index);
            }

            index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Remision = dr.GetString(index);
            }

            index = dr.GetOrdinal("Alias");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Alias = dr.GetString(index);
            }
            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Sexo = dr.GetString(index);
            }
            index = dr.GetOrdinal("Estatura");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Estatura = dr.GetDouble(index);
            }

            index = dr.GetOrdinal("Fechanacimiento");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Fechanacimiento = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Nacionalidad");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Nacionalidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Ocupacion");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Ocupacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Escolaridad");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Escolaridad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estadocivil");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Estadocivil = dr.GetString(index);
            }
            index = dr.GetOrdinal("Lenguanativa");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Lenguanativa = dr.GetString(index);
            }

            index = dr.GetOrdinal("Etnia");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Etnia = dr.GetString(index);

            }
            index = dr.GetOrdinal("Municipio");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Municipio = dr.GetString(index);

            }
            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.Estado = dr.GetString(index);

            }
            index = dr.GetOrdinal("RFC");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.RFC = dr.GetString(index);

            }
            index = dr.GetOrdinal("CURP");
            if (!dr.IsDBNull(index))
            {
                fichabiometrico.CURP = dr.GetString(index);

            }


            return fichabiometrico;


        }
    }
}
