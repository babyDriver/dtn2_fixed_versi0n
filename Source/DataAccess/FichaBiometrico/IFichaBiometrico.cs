﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.FichaBiometrico
{
    public interface IFichaBiometrico
    {
        Entity.Fichabiometrico GetFichaBiometrico(int DetenidoId);
    
    }
}
