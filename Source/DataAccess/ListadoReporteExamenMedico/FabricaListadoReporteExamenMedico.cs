﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoReporteExamenMedico
{
    public class FabricaListadoReporteExamenMedico : IEntityFactory<Entity.ListadoReporteExamenMedico>
    {
        public Entity.ListadoReporteExamenMedico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoReporteExamenMedico();

            var index = dr.GetOrdinal("IdDetenido");
            if (!dr.IsDBNull(index))
            {
                item.IdDetenido = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IdDetalle");
            if (!dr.IsDBNull(index))
            {
                item.IdDetalle = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("TrackingIdDetalle");
            if (!dr.IsDBNull(index))
            {
                item.TrackingIdDetalle = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }                       

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("ActivoExamen");
            if (!dr.IsDBNull(index))
            {
                item.ActivoExamen = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("IdExamen");
            if (!dr.IsDBNull(index))
            {
                item.IdExamen = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaExamen");
            if (!dr.IsDBNull(index))
            {
                item.FechaExamen = dr.GetDateTime(index);
            }            

            index = dr.GetOrdinal("TrackingIdExamen");
            if (!dr.IsDBNull(index))
            {
                item.TrackingIdExamen = dr.GetGuid(index);
            }
            
            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                item.Sexo = dr.GetString(index);
            }

            item.NombreCompleto = item.Nombre + " " + item.Paterno + " " + item.Materno;
            item.FechaExamenAux = item.FechaExamen.ToString("dd/MM/yyyy HH:mm:ss");

            return item;
        }
    }
}
