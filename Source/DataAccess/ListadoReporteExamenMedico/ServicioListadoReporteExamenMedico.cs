﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoReporteExamenMedico
{
    public class ServicioListadoReporteExamenMedico : ClsAbstractService<Entity.ListadoReporteExamenMedico>, IListadoReporteExamenMedico
    {
        public ServicioListadoReporteExamenMedico(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoReporteExamenMedico> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoReporteExamenMedico(), parametros);
        }
    }
}
