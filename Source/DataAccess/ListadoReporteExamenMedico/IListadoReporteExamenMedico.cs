﻿using System.Collections.Generic;

namespace DataAccess.ListadoReporteExamenMedico
{
    public interface IListadoReporteExamenMedico
    {
        List<Entity.ListadoReporteExamenMedico> ObtenerTodos(object[] data);
    }
}
