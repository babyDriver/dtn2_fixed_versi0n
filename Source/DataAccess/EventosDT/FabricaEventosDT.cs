﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.EventosDT
{
    public class FabricaEventosDT : IEntityFactory<Entity.EventosDT>
    {
        public Entity.EventosDT ConstructEntity(IDataReader dr)
        {
            var item = new Entity.EventosDT();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("DescripcionLlamada");
            if (!dr.IsDBNull(index))
            {
                item.DescripcionLlamada = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                item.LugarDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Asentamiento");
            if (!dr.IsDBNull(index))
            {
                item.Asentamiento = dr.GetString(index);
            }

            index = dr.GetOrdinal("CodigoPostal");
            if (!dr.IsDBNull(index))
            {
                item.CodigoPostal = dr.GetString(index);
            }

            index = dr.GetOrdinal("Municipio");
            if (!dr.IsDBNull(index))
            {
                item.Municipio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                item.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Pais");
            if (!dr.IsDBNull(index))
            {
                item.Pais = dr.GetString(index);
            }

            index = dr.GetOrdinal("IdColonia");
            if (!dr.IsDBNull(index))
            {
                item.IdColonia = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetString(index);
            }

            index = dr.GetOrdinal("HoraYFecha");
            if (!dr.IsDBNull(index))
            {
                item.HoraYFecha = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            return item;
        }
    }
}
