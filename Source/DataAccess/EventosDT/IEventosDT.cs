﻿using System.Collections.Generic;

namespace DataAccess.EventosDT
{
    public interface IEventosDT
    {
        List<Entity.EventosDT> ObtenerTodos(object[] data);
    }
}
