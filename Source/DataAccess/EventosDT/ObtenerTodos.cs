﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.EventosDT
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] data)
        {
            DbCommand cmd = db.GetStoredProcCommand("Eventos_GetForTable_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, data[0]);
            db.AddInParameter(cmd, "_Tipo", DbType.String, data[1]);
            db.AddInParameter(cmd, "_Anio", DbType.Int32, data[2]);
            return cmd;
        }
    }
}
