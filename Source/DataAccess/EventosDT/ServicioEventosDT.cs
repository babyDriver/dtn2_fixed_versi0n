﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.EventosDT
{
    public class ServicioEventosDT : ClsAbstractService<Entity.EventosDT>, IEventosDT
    {
        public ServicioEventosDT(string dataBase)
           : base(dataBase)
        {
        }

        public List<Entity.EventosDT> ObtenerTodos(object[] data)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaEventosDT(), data);
        }
    }
}
