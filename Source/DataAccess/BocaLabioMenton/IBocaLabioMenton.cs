﻿using System;
using System.Collections.Generic;

namespace DataAccess.BocaLabioMenton
{
    public interface IBocaLabioMenton
    {
        List<Entity.BocaLabioMenton> ObtenerTodos();
        Entity.BocaLabioMenton ObtenerById(int Id);
        Entity.BocaLabioMenton ObtenerByAntropometriaId(int Id);
        Entity.BocaLabioMenton ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.BocaLabioMenton boca);
        void Actualizar(Entity.BocaLabioMenton boca);
    }
}