﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.BocaLabioMenton
{
    public class Guardar : IInsertFactory<Entity.BocaLabioMenton>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.BocaLabioMenton boca)
        {
            DbCommand cmd = db.GetStoredProcCommand("BocaLabios_Menton_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, boca.TrackingId);
            db.AddInParameter(cmd, "_ComisuraB", DbType.Int32, boca.ComisuraB);
            db.AddInParameter(cmd, "_EspesorL", DbType.Int32, boca.EspesorL);
            db.AddInParameter(cmd, "_ProminenciaL", DbType.Int32, boca.ProminenciaL);
            db.AddInParameter(cmd, "_TipoM", DbType.Int32, boca.TipoM);
            db.AddInParameter(cmd, "_FormaM", DbType.Int32, boca.FormaM);
            db.AddInParameter(cmd, "_InclinacionM", DbType.Int32, boca.InclinacionM);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, boca.AntroprometriaId);

            if (boca.TamanoB != null)
                db.AddInParameter(cmd, "_TamanoB", DbType.Int32, boca.TamanoB);
            else
                db.AddInParameter(cmd, "_TamanoB", DbType.Int32, null);

            if (boca.AlturaL != null)
                db.AddInParameter(cmd, "_AlturaL", DbType.Int32, boca.AlturaL);
            else
                db.AddInParameter(cmd, "_AlturaL", DbType.Int32, null);

            return cmd;
        }
    }
}
