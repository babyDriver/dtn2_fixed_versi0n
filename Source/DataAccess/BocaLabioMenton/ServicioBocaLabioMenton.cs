﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.BocaLabioMenton
{
    public class ServicioBocaLabioMenton : ClsAbstractService<Entity.BocaLabioMenton>, IBocaLabioMenton
    {
        public ServicioBocaLabioMenton(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.BocaLabioMenton> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaBocaLabioMenton(), new Entity.NullClass());
        }

   

        public Entity.BocaLabioMenton ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaBocaLabioMenton(), userId);
        }

        public Entity.BocaLabioMenton ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaBocaLabioMenton(), Id);
        }

        public Entity.BocaLabioMenton ObtenerByAntropometriaId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByAntropometriaId();
            return GetByKey(select, new FabricaBocaLabioMenton(), Id);
        }

        public int Guardar(Entity.BocaLabioMenton general)
        {
            IInsertFactory<Entity.BocaLabioMenton> insert = new Guardar();
            return Insert(insert, general);
        }

        public void Actualizar(Entity.BocaLabioMenton boca)
        {
            IUpdateFactory<Entity.BocaLabioMenton> update = new Actualizar();
            Update(update, boca);
        }


    }
}