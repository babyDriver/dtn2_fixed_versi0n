﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.BocaLabioMenton
{
    public class ObtenerByAntropometriaId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("BocaLabios_Menton_GetByAntroprometriaId_SP");
            db.AddInParameter(cmd, "_AntroprometriaId", DbType.Int32, id);
            return cmd;
        }
    }
}
