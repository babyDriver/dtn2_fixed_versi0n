﻿using DataAccess.Interfaces;


namespace DataAccess.BocaLabioMenton
{
    public class FabricaBocaLabioMenton : IEntityFactory<Entity.BocaLabioMenton>
    {
       

        public Entity.BocaLabioMenton ConstructEntity(System.Data.IDataReader dr)
        {
            var boca = new Entity.BocaLabioMenton();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                boca.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                boca.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("TamanoB");
            if (!dr.IsDBNull(index))
            {
                boca.TamanoB = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ComisuraB");
            if (!dr.IsDBNull(index))
            {
                boca.ComisuraB = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AlturaL");
            if (!dr.IsDBNull(index))
            {
                boca.AlturaL = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("EspesorL");
            if (!dr.IsDBNull(index))
            {
                boca.EspesorL = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ProminenciaL");
            if (!dr.IsDBNull(index))
            {
                boca.ProminenciaL = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TipoM");
            if (!dr.IsDBNull(index))
            {
                boca.TipoM = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FormaM");
            if (!dr.IsDBNull(index))
            {
                boca.FormaM = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("InclinacionM");
            if (!dr.IsDBNull(index))
            {
                boca.InclinacionM = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AntroprometriaId");
            if (!dr.IsDBNull(index))
            {
                boca.AntroprometriaId = dr.GetInt32(index);
            }
            
            return boca;
        }
    }
}