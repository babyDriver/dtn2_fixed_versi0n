﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Accion
{
    public class ServicioAccion : ClsAbstractService<Entity.Accion>, IAccion
    {
        public ServicioAccion(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.Accion accion)
        {
            IInsertFactory<Entity.Accion> insert = new Guardar();
            return Insert(insert, accion);
        }
    }
}
