﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Accion
{
    public class FabricaAccion : IEntityFactory<Entity.Accion>
    {
        public Entity.Accion ConstructEntity(System.Data.IDataReader dr)
        {
            var accion = new Entity.Accion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                accion.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                accion.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                accion.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                accion.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                accion.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                accion.CreadoPor = dr.GetInt32(index);
            }            

            return accion;
        }
    }
}
