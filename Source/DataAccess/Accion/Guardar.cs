﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Accion
{
    public class Guardar : IInsertFactory<Entity.Accion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Accion accion)
        {
            DbCommand cmd = db.GetStoredProcCommand("Accion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, accion.TrackingId.ToString());
            db.AddInParameter(cmd, "_Nombre", DbType.String, accion.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, accion.Descripcion);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, accion.Fecha);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, accion.CreadoPor);            

            return cmd;
        }
    }
}
