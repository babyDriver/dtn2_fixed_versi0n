﻿using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.ReporteEstadistico
{
    public class FabricaReporteEstadistico : IEntityFactory<Entity.Reporteestadistico>
    {
        public Reporteestadistico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.Reporteestadistico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo_reporteId");
            if (!dr.IsDBNull(index))
            {
                item.Tipo_reporteId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ReporteId");
            if (!dr.IsDBNull(index))
            {
                item.ReporteId = dr.GetInt32(index);
            }

            return item;
        }
    }
}
