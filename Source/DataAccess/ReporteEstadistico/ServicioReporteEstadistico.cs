﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;
namespace DataAccess.ReporteEstadistico
{
    public class ServicioReporteEstadistico : ClsAbstractService<Entity.Reporteestadistico>, IReporteEstadistico
    {
        public ServicioReporteEstadistico(string dataBase)
        : base(dataBase)
        {

        }
        public new List<Reporteestadistico> GetAll()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select,new FabricaReporteEstadistico(),new Entity.NullClass());
        }
    }
}
