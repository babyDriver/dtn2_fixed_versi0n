﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.DashboardAnioTrabajo
{
    public class ObtenerAniosTrabajo : ISelectFactory<NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("DashboardAnioTrabajo_GetAll");
            return cmd;
        }
    }
}
