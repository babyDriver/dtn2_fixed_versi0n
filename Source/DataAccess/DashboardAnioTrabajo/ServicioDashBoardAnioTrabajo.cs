﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DashboardAnioTrabajo
{
    public class ServicioDashBoardAnioTrabajo : ClsAbstractService<DasboardAnioTrabajo>, IDashboardAnioTrabajo
    {
        public ServicioDashBoardAnioTrabajo(string dataBase)
           : base(dataBase)
        {
        }

        public List<DasboardAnioTrabajo> GetYears()
        {
            ISelectFactory<NullClass> select = new ObtenerAniosTrabajo();
            return GetAll(select, new FabricaDashboardAnioTrabajo(), new NullClass());
        }
    }
}
