﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DashboardAnioTrabajo
{
    public class FabricaDashboardAnioTrabajo : IEntityFactory<Entity.DasboardAnioTrabajo>
    {
        public DasboardAnioTrabajo ConstructEntity(IDataReader dr)
        {
            var item = new Entity.DasboardAnioTrabajo();

            var index = dr.GetOrdinal("Aniotrabajo");
            if (!dr.IsDBNull(index))
            {
                item.Aniotrabajo = dr.GetInt32(index);
            }
            return item;
        }
    }
}
