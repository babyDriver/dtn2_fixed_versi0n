﻿using System.Collections.Generic;

namespace DataAccess.DashboardAnioTrabajo
{
    public interface IDashboardAnioTrabajo
    {
        List<Entity.DasboardAnioTrabajo> GetYears();
    }
}
