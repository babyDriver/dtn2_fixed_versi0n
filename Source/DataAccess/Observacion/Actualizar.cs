﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Observacion
{
    public class Actualizar : IUpdateFactory<Entity.Observacion>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Observacion item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Observacion_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, item.InternoId);
            db.AddInParameter(cmd, "_Observaciones", DbType.String, item.Observaciones);            
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);

            return cmd;
        }
    }
}
