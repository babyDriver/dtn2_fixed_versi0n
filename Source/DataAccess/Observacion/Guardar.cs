﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Observacion
{
    public class Guardar : IInsertFactory<Entity.Observacion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Observacion item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Observacion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId.ToString());
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, item.InternoId);
            db.AddInParameter(cmd, "_Observaciones", DbType.String, item.Observaciones);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);

            return cmd;
        }
    }
}
