﻿using DataAccess.Interfaces;

namespace DataAccess.Observacion
{
    public class FabricaObservacion : IEntityFactory<Entity.Observacion>
    {
        public Entity.Observacion ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.Observacion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("InternoId");
            if (!dr.IsDBNull(index))
            {
                item.InternoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Observaciones");
            if (!dr.IsDBNull(index))
            {
                item.Observaciones = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            return item;
        }
    }
}
