﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Observacion
{
    public class ServicioObservacion : ClsAbstractService<Entity.Observacion>, IObservacion
    {
        public ServicioObservacion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Observacion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaObservacion(), new Entity.NullClass());
        }

        public int Guardar(Entity.Observacion item)
        {
            IInsertFactory<Entity.Observacion> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.Observacion ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaObservacion(), trackingId);
        }

        public Entity.Observacion ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaObservacion(), id);
        }

        public void Actualizar(Entity.Observacion item)
        {
            IUpdateFactory<Entity.Observacion> update = new Actualizar();
            Update(update, item);
        }
    }
}
