﻿using System;
using System.Collections.Generic;

namespace DataAccess.Observacion
{
    public interface IObservacion
    {
        int Guardar(Entity.Observacion item);
        void Actualizar(Entity.Observacion item);
        List<Entity.Observacion> ObtenerTodos();
        Entity.Observacion ObtenerById(int id);
        Entity.Observacion ObtenerByTrackingId(Guid guid);
    }
}
