﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.CertificadoLesionAlergiasDetalle
{
    public class ServicioCertificadoLesionAlergiasDetalle:ClsAbstractService<Entity.CertificadoLesionAlergiaDetalle>,ICertificadoLesionAlergiasDetalle
    {
        public ServicioCertificadoLesionAlergiasDetalle(string dataBase)
        : base(dataBase)
        {
        }

        public int Guardar(CertificadoLesionAlergiaDetalle certificadoLesionAlergiaDetalle)
        {
            IInsertFactory<Entity.CertificadoLesionAlergiaDetalle> insert = new Guardar();
            return Insert(insert, certificadoLesionAlergiaDetalle);
        }

        public List<CertificadoLesionAlergiaDetalle> ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetAll(select, new FabricaCertificadoLesionAlergiasDetalle(), Id);
        }
    }
}
