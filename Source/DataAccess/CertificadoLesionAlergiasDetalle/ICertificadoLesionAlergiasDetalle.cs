﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionAlergiasDetalle
{
   public  interface ICertificadoLesionAlergiasDetalle
    {
        int Guardar(Entity.CertificadoLesionAlergiaDetalle certificadoLesionAlergiaDetalle);
        List<Entity.CertificadoLesionAlergiaDetalle> ObtenerPorId(int Id);
    }
}
