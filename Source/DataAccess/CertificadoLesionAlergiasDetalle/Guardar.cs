﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoLesionAlergiasDetalle
{
    public class Guardar : IInsertFactory<Entity.CertificadoLesionAlergiaDetalle>
    {
        public DbCommand ConstructInsertCommand(Database db, CertificadoLesionAlergiaDetalle entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionalergiadetalle_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_certificado_LesionAlergiaId", DbType.Int32, entity.certificado_LesionAlergiaId);
            db.AddInParameter(cmd, "_AlergiaId", DbType.Int32, entity.AlergiaId);

            return cmd;
        }
    }
}
