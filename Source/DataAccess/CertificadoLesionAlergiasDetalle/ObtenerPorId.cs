﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoLesionAlergiasDetalle
{
    public class ObtenerPorId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionAlergia_ById_SP");
            db.AddInParameter(cmd, "_certificado_LesionAlergiaId", DbType.Int32, Id);
            return cmd;
        }
    }
}
