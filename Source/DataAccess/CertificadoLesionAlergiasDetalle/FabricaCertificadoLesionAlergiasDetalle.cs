﻿using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.CertificadoLesionAlergiasDetalle
{
    public class FabricaCertificadoLesionAlergiasDetalle : IEntityFactory<Entity.CertificadoLesionAlergiaDetalle>
    {
        public CertificadoLesionAlergiaDetalle ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoLesionAlergiaDetalle();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("certificado_LesionAlergiaId");
            if (!dr.IsDBNull(index))
            {
                item.certificado_LesionAlergiaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("AlergiaId");
            if (!dr.IsDBNull(index))
            {
                item.AlergiaId = dr.GetInt32(index);
            }
            return item;
        }
    }
}
