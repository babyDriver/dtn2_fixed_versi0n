﻿using System.Collections.Generic;

namespace DataAccess.ReporteSancionesJuezCalificador
{
    public interface IReporteSancionesJuezCalificador
    {
        List<Entity.ReporteSancionesjuezcalificador> GetList(string[] parametros);
    }
}
