﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.ReporteSancionesJuezCalificador
{
    public class ServicioReporteSancionesJuezcalificador : ClsAbstractService<Entity.ReporteSancionesjuezcalificador>, IReporteSancionesJuezCalificador
    {
        public ServicioReporteSancionesJuezcalificador(string dataBase)
           : base(dataBase)
        {
        }

        public List<ReporteSancionesjuezcalificador> GetList(string[] parametros)
        {
            ISelectFactory<string[]> select = new GetReporteSancionesjuezCalificador();
            return GetAll(select, new FabricaReporteSancionesJuezCalificador(), parametros);
        }
    }
}
