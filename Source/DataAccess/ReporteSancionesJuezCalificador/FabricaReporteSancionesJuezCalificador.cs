﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteSancionesJuezCalificador
{
    public class FabricaReporteSancionesJuezCalificador : IEntityFactory<Entity.ReporteSancionesjuezcalificador>
    {
        public ReporteSancionesjuezcalificador ConstructEntity(IDataReader dr)
        {
            var reporte = new Entity.ReporteSancionesjuezcalificador();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                reporte.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                reporte.TrackingId = dr.GetString(index);
            }

            index = dr.GetOrdinal("Juez");
            if (!dr.IsDBNull(index))
            {
                reporte.Juez = dr.GetString(index);
            }
            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                reporte.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                reporte.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Detenido");
            if (!dr.IsDBNull(index))
            {
                reporte.Detenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("Totaldemultas");
            if (!dr.IsDBNull(index))
            {
                reporte.Totaldemultas = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Totalhoras");
            if (!dr.IsDBNull(index))
            {
                reporte.Totalhoras = dr.GetInt32(index);
            }


            return reporte;

        }
    }
}
