﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ReporteSancionesJuezCalificador
{
    public class GetReporteSancionesjuezCalificador : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("GetReporteSancionesJuezCalificador_sp");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(parametros[0]));
            db.AddInParameter(cmd, "_Fechainicio", DbType.Date, Convert.ToDateTime(parametros[1]));
            db.AddInParameter(cmd, "_Fechafin", DbType.Date, Convert.ToDateTime(parametros[2]));
            return cmd;
        }
    }
}
