﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ResultadosQuimicos
{
    public class FabricaResultadoQuimico : IEntityFactory<Entity.ResultadosQuimicos>
    {
        public Entity.ResultadosQuimicos ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ResultadosQuimicos();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Resultado");
            if (!dr.IsDBNull(index))
            {
                item.Resultado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt32(index);
            }
            return item;
        }
    }
}
