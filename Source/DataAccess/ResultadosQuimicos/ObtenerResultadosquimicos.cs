﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.ResultadosQuimicos
{
    public class ObtenerResultadosquimicos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("resultadosquimicos_GetAll_SP");
            return cmd;
        }
    }
}
