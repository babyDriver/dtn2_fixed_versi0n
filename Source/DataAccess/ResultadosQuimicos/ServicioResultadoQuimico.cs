﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entity;

namespace DataAccess.ResultadosQuimicos
{
    public class ServicioResultadoQuimico : ClsAbstractService<Entity.ResultadosQuimicos>, IResultadoQuimico
    {
        public ServicioResultadoQuimico(string dataBase)
  : base(dataBase)
        {

        }
        
        public List<Entity.ResultadosQuimicos> GetResultados()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerResultadosquimicos();
            return GetAll(select, new FabricaResultadoQuimico(), null);
        }
    }
}
