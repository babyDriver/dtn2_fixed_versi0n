﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ResultadosQuimicos
{
    public interface IResultadoQuimico
    {
        List<Entity.ResultadosQuimicos> GetResultados();
    }
}
