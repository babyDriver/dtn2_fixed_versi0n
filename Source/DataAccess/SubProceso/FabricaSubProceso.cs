﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;


namespace DataAccess.SubProceso
{
    class FabricaSubProceso : IEntityFactory<Entity.SubProceso>
    {
        public Entity.SubProceso ConstructEntity(IDataReader dr)
        {
            var item = new Entity.SubProceso();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ProcesoId");
            if (!dr.IsDBNull(index))
            {
                item.ProcesoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Subproceso");
            if (!dr.IsDBNull(index))
            {
                item.Subproceso = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt16(index);
            }



            return item;
        }
    }
}
