﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.SubProceso
{
    public class ServicioSubProceso : ClsAbstractService<Entity.SubProceso>, ISubProceso
    {
        public ServicioSubProceso(string dataBase)
            : base(dataBase)
        {
        }
        public List<Entity.SubProceso> ObtenerTodo()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodo();
            return GetAll(select, new FabricaSubProceso(), new Entity.NullClass());
        }
    }
}
