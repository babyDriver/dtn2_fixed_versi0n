﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAUnidad
{
    public class Actualizar : IUpdateFactory<Entity.WSAUnidad>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.WSAUnidad entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAUnidad_Update_SP");
            db.AddInParameter(cmd, "_idUnidadInstitucion", DbType.Int32, entity.IdUnidadInstitucion);
            db.AddInParameter(cmd, "_idInstitucion", DbType.Int32, entity.IdInstitucion);
            db.AddInParameter(cmd, "_claveUnidad", DbType.Int32, entity.ClaveUnidad);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);

            return cmd;
        }
    }
}
