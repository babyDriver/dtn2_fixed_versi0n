﻿using System.Collections.Generic;

namespace DataAccess.WSAUnidad
{
    public interface IWSAUnidad
    {
        List<Entity.WSAUnidad> ObtenerTodos();
        Entity.WSAUnidad ObtenerById(int id);
        List<Entity.WSAUnidad> ObtenerByIdInstitucion(int id);
        int Guardar(Entity.WSAUnidad unidad);
        void Actualizar(Entity.WSAUnidad unidad);
    }
}
