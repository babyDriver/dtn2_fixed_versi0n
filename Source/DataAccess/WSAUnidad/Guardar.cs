﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.WSAUnidad
{
    public class Guardar : IInsertFactory<Entity.WSAUnidad>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSAUnidad unidad)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAUnidad_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_idUnidadInstitucion", DbType.Int32, unidad.IdUnidadInstitucion);
            db.AddInParameter(cmd, "_idInstitucion", DbType.Int32, unidad.IdInstitucion);
            db.AddInParameter(cmd, "_claveUnidad", DbType.String, unidad.ClaveUnidad);

            return cmd;
        }
    }
}
