﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.WSAUnidad
{
    public class ServicioWSAUnidad : ClsAbstractService<Entity.WSAUnidad>, IWSAUnidad
    {
        public ServicioWSAUnidad(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.WSAUnidad> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaWSAUnidad(), new Entity.NullClass());
        }

        public Entity.WSAUnidad ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSAUnidad(), id);
        }

        public List<Entity.WSAUnidad> ObtenerByIdInstitucion(int id)
        {
            ISelectFactory<int> select = new ObtenerTodosByIdInstitucion();
            return GetAll(select, new FabricaWSAUnidad(), id);
        }

        public int Guardar(Entity.WSAUnidad unidad)
        {
            IInsertFactory<Entity.WSAUnidad> insert = new Guardar();
            return Insert(insert, unidad);
        }

        public void Actualizar(Entity.WSAUnidad unidad)
        {
            IUpdateFactory<Entity.WSAUnidad> update = new Actualizar();
            Update(update, unidad);
        }
    }
}
