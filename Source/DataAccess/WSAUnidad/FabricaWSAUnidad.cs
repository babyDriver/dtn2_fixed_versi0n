﻿using DataAccess.Interfaces;

namespace DataAccess.WSAUnidad
{
    public class FabricaWSAUnidad : IEntityFactory<Entity.WSAUnidad>
    {
        public Entity.WSAUnidad ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.WSAUnidad();

            var index = dr.GetOrdinal("idUnidadInstitucion");
            if (!dr.IsDBNull(index))
            {
                item.IdUnidadInstitucion = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("idInstitucion");
            if (!dr.IsDBNull(index))
            {
                item.IdInstitucion = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("claveUnidad");
            if (!dr.IsDBNull(index))
            {
                item.ClaveUnidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaModificacion");
            if (!dr.IsDBNull(index))
            {
                item.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            return item;
        }
    }
}
