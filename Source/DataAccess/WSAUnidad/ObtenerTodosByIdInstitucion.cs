﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAUnidad
{
    public class ObtenerTodosByIdInstitucion : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAUnidad_GetByIdInstitucion_SP");
            db.AddInParameter(cmd, "_IdInstitucion", DbType.Int32, id);
            return cmd;
        }
    }
}
