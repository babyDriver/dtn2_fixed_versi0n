﻿using System;
using System.Collections.Generic;

namespace DataAccess.DevolucionPertenencia
{
    public interface IDevolucionPertenencia
    {
        int Guardar(Entity.DevolucionPertenencia devolucion);
        Entity.DevolucionPertenencia ObtenerByTrackingId(Guid guid);
    }
}
