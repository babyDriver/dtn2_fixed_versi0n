﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.DevolucionPertenencia
{
    public class Guardar : IInsertFactory<Entity.DevolucionPertenencia>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.DevolucionPertenencia devolucion)
        {
            DbCommand cmd = db.GetStoredProcCommand("DevolucionPertenencia_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, devolucion.TrackingId);            
            db.AddInParameter(cmd, "_PertenenciasDe", DbType.String, devolucion.PertenenciasDe);
            db.AddInParameter(cmd, "_UsuarioQueRegistro", DbType.String, devolucion.UsuarioQueRegistro);
            db.AddInParameter(cmd, "_PersonaQueRecibe", DbType.String, devolucion.PersonaQueRecibe);
            db.AddInParameter(cmd, "_Condicion", DbType.String, devolucion.Condicion);
            db.AddInParameter(cmd, "_Entrega", DbType.Int32, devolucion.Entrega);
            db.AddInParameter(cmd, "_FechaEntrega", DbType.DateTime, devolucion.FechaEntrega);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, devolucion.CreadoPor);

            return cmd;
        }
    }
}
