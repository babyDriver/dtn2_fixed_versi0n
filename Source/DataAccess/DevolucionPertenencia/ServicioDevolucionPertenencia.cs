﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.DevolucionPertenencia
{
    public class ServicioDevolucionPertenencia : ClsAbstractService<Entity.DevolucionPertenencia>, IDevolucionPertenencia
    {
        public ServicioDevolucionPertenencia(string dataBase)
           : base(dataBase)
        {
        }

        public int Guardar(Entity.DevolucionPertenencia devolucion)
        {
            IInsertFactory<Entity.DevolucionPertenencia> insert = new Guardar();
            return Insert(insert, devolucion);
        }

        public Entity.DevolucionPertenencia ObtenerByTrackingId(Guid tracking)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaDevolucionPertenencia(), tracking);
        }
    }
}
