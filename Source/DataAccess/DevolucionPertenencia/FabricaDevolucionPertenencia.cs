﻿using DataAccess.Interfaces;

namespace DataAccess.DevolucionPertenencia
{
    public class FabricaDevolucionPertenencia : IEntityFactory<Entity.DevolucionPertenencia>
    {
        public Entity.DevolucionPertenencia ConstructEntity(System.Data.IDataReader dr)
        {
            var devolucion = new Entity.DevolucionPertenencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                devolucion.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                devolucion.TrackingId = dr.GetGuid(index);
            }            

            index = dr.GetOrdinal("PertenenciasDe");
            if (!dr.IsDBNull(index))
            {
                devolucion.PertenenciasDe = dr.GetString(index);
            }

            index = dr.GetOrdinal("UsuarioQueRegistro");
            if (!dr.IsDBNull(index))
            {
                devolucion.UsuarioQueRegistro = dr.GetString(index);
            }

            index = dr.GetOrdinal("PersonaQueRecibe");
            if (!dr.IsDBNull(index))
            {
                devolucion.PersonaQueRecibe = dr.GetString(index);
            }

            index = dr.GetOrdinal("Condicion");
            if (!dr.IsDBNull(index))
            {
                devolucion.Condicion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Entrega");
            if (!dr.IsDBNull(index))
            {
                devolucion.Entrega = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaEntrega");
            if (!dr.IsDBNull(index))
            {
                devolucion.FechaEntrega = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                devolucion.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                devolucion.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                devolucion.CreadoPor = dr.GetInt32(index);
            }

            return devolucion;
        }
    }
}
