﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CeldaDetalle
{
    public interface ICeldaDetalle
    {
        List<Entity.DetalleCelda> ObtenerDetallesById(int Id);

    }
}
