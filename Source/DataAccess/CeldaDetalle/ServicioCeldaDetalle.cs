﻿using DataAccess.Catalogo;
using DataAccess.Interfaces;
using DataAccess.Motivo;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.CeldaDetalle
{
    public class ServicioCeldaDetalle : ClsAbstractService<Entity.DetalleCelda>, ICeldaDetalle
    {
        public ServicioCeldaDetalle(string dataBase)
              : base(dataBase)
        {
        }

        public List<DetalleCelda> ObtenerDetallesById(int Id)
        {
            ISelectFactory<int> select = new ObtenerDetalleCelda();
            return GetAll(select, new FeabricaDetalleCelda(), Id);

        }
    }
}
