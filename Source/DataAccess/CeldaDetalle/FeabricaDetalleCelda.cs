﻿using System.Data;
using DataAccess.Catalogo;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CeldaDetalle
{
    public class FeabricaDetalleCelda : IEntityFactory<Entity.DetalleCelda>
    {
        private static ServicioCatalogo servicioCatalogo = new ServicioCatalogo("SQLConnectionString");
        public DetalleCelda ConstructEntity(IDataReader dr)
        {
            var detallecelda = new Entity.DetalleCelda();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                detallecelda.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                detallecelda.Nombre = dr.GetString(index);
            }
            index = dr.GetOrdinal("Capacidad");
            if (!dr.IsDBNull(index))
            {
                detallecelda.Capacidad = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TipoMovimientoId");
            if (!dr.IsDBNull(index))
            {
                detallecelda.TipoMovimientoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                detallecelda.Estatus = dr.GetInt32(index);
            }

            return detallecelda;
        }
    }
}
