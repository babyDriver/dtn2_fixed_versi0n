﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.YearsEvent
{
    public class ObtenerTodos : ISelectFactory<NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("YearsEvent_GetAll");
            return cmd;
        }
    }
}
