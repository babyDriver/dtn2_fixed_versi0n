﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.YearsEvent
{
    public class FabricaYearsEvent : IEntityFactory<Entity.YearsEvent>
    {
        public Entity.YearsEvent ConstructEntity(IDataReader dr)
        {
            var item = new Entity.YearsEvent();

            var index = dr.GetOrdinal("Year");
            if (!dr.IsDBNull(index))
            {
                item.Year = dr.GetInt32(index);
            }
            return item;
        }
    }
}
