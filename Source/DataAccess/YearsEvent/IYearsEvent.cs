﻿using System.Collections.Generic;

namespace DataAccess.YearsEvent
{
    public interface IYearsEvent
    {
        List<Entity.YearsEvent> ObtenerTodos();
    }
}
