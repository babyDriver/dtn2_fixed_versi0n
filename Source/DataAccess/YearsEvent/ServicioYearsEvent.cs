﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.YearsEvent
{
    public class ServicioYearsEvent : ClsAbstractService<Entity.YearsEvent>, IYearsEvent
    {
        public ServicioYearsEvent(string dataBase)
           : base(dataBase)
        {
        }

        public List<Entity.YearsEvent> ObtenerTodos()
        {
            ISelectFactory<NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaYearsEvent(), new NullClass());
        }
    }
}
