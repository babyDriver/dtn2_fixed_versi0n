﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Familiar
{
    public class ObtenerByFamiliar : ISelectFactory<Entity.Familiar>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.Familiar familiar)
        {
            DbCommand cmd = db.GetStoredProcCommand("Familiar_GetByNombre_SP");
            db.AddInParameter(cmd, "_Nombre", DbType.String, familiar.Nombre);
            db.AddInParameter(cmd, "_ApellidoPaterno", DbType.String, familiar.ApellidoPaterno);
            db.AddInParameter(cmd, "_ApellidoMaterno", DbType.String, familiar.ApellidoMaterno);
            db.AddInParameter(cmd, "_ParentescoId", DbType.Int32, familiar.Parentesco.Id);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, familiar.Detenido.Id);
            return cmd;
        }
    }
}
