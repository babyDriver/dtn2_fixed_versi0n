﻿using System;
using System.Collections.Generic;

namespace DataAccess.Familiar
{
    public interface IFamiliar
    {
        List<Entity.Familiar> ObtenerTodos();
        Entity.Familiar ObtenerById(int Id);
        Entity.Familiar ObtenerByDetenidoId(int Id);
        Entity.Familiar ObtenerByTrackingId(Guid trackingid);
        Entity.Familiar ObtenerByFamiliar(Entity.Familiar alias);
        int Guardar(Entity.Familiar alias);
        void Actualizar(Entity.Familiar alias);
    }
}