﻿
using DataAccess.Interfaces;
using DataAccess.Catalogo;
using DataAccess.Domicilio;
using DataAccess.Detenido;


namespace DataAccess.Familiar
{
    public class FabricaFamiliar : IEntityFactory<Entity.Familiar>
    {

        private static ServicioCatalogo servicioCatalogo = new ServicioCatalogo("SQLConnectionString");
        private static ServicioDomicilio servicioDomicilio = new ServicioDomicilio("SQLConnectionString");
        private static ServicioDetenido servicioInterno = new ServicioDetenido("SQLConnectionString");
        public Entity.Familiar ConstructEntity(System.Data.IDataReader dr)
        {
            var familiar = new Entity.Familiar();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                familiar.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                familiar.TrackingId = dr.GetGuid(index);
            }

       
            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                familiar.Nombre = dr.GetString(index);
            }
            
            index = dr.GetOrdinal("ApellidoPaterno");
            if (!dr.IsDBNull(index))
            {
                familiar.ApellidoPaterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("ApellidoMaterno");
            if (!dr.IsDBNull(index))
            {
                familiar.ApellidoMaterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                familiar.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                familiar.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                familiar.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                Entity.Detenido _interno = servicioInterno.ObtenerById(dr.GetInt32(index));
                if (_interno != null)
                    familiar.Detenido = _interno;
            }
            index = dr.GetOrdinal("DomicilioFamiliarId");
            if (!dr.IsDBNull(index))
            {
                Entity.Domicilio _domicilio = servicioDomicilio.ObtenerById(dr.GetInt32(index));
                if (_domicilio != null)
                    familiar.Domicilio = _domicilio;
            }

            index = dr.GetOrdinal("OcupacionId");
            if (!dr.IsDBNull(index))
            {
                var parametros = new object[2];
                parametros[0] = dr.GetInt32(index);
                parametros[1] = Entity.TipoDeCatalogo.ocupacion;
                Entity.Catalogo _ocupacion = servicioCatalogo.ObtenerPorId(parametros);
                if (_ocupacion != null)
                    familiar.Ocupacion = _ocupacion;
            }

            index = dr.GetOrdinal("SexoId");
            if (!dr.IsDBNull(index))
            {
                var parametros = new object[2];
                parametros[0] = dr.GetInt32(index);
                parametros[1] = Entity.TipoDeCatalogo.sexo;
                Entity.Catalogo _sexo = servicioCatalogo.ObtenerPorId(parametros);
                if (_sexo != null)
                    familiar.Sexo = _sexo;
            }

            index = dr.GetOrdinal("ParentescoId");
            if (!dr.IsDBNull(index))
            {
                var parametros = new object[2];
                parametros[0] = dr.GetInt32(index);
                parametros[1] = Entity.TipoDeCatalogo.parentesco;
                Entity.Catalogo _parentesco = servicioCatalogo.ObtenerPorId(parametros);
                if (_parentesco != null)
                    familiar.Parentesco = _parentesco;
            }

            return familiar;
        }
    }
}