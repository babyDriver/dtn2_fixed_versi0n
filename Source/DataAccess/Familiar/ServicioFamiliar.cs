﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Familiar
{
    public class ServicioFamiliar : ClsAbstractService<Entity.Familiar>, IFamiliar
    {
        public ServicioFamiliar(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Familiar> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaFamiliar(), new Entity.NullClass());
        }

        public Entity.Familiar ObtenerByFamiliar(Entity.Familiar userId)
        {
            ISelectFactory<Entity.Familiar> select = new ObtenerByFamiliar();
            return GetByKey(select, new FabricaFamiliar(), userId);
        }

        public Entity.Familiar ObtenerByTrackingId(Guid trackinid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaFamiliar(), trackinid);
        }

        public Entity.Familiar ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaFamiliar(), Id);
        }

        public Entity.Familiar ObtenerByDetenidoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetByKey(select, new FabricaFamiliar(), Id);
        }

        public int Guardar(Entity.Familiar alias)
        {
            IInsertFactory<Entity.Familiar> insert = new Guardar();
            return Insert(insert, alias);
        }

        public void Actualizar(Entity.Familiar familiar)
        {
            IUpdateFactory<Entity.Familiar> update = new Actualizar();
            Update(update, familiar);
        }


    }
}