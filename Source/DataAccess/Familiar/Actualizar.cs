﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Familiar
{
    public class Actualizar : IUpdateFactory<Entity.Familiar>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Familiar familiar)
        {
            DbCommand cmd = db.GetStoredProcCommand("Familiar_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, familiar.Id); 
            db.AddInParameter(cmd, "_Nombre", DbType.String, familiar.Nombre);
            db.AddInParameter(cmd, "_ApellidoPaterno", DbType.String, familiar.ApellidoPaterno);
            db.AddInParameter(cmd, "_ApellidoMaterno", DbType.String, familiar.ApellidoMaterno);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, familiar.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, familiar.Habilitado);
            db.AddInParameter(cmd, "_OcupacionId", DbType.Boolean, familiar.Ocupacion.Id);
            db.AddInParameter(cmd, "_SexoId", DbType.Int32, familiar.Sexo.Id);
            db.AddInParameter(cmd, "_Estado", DbType.String, familiar.Estado);
            db.AddInParameter(cmd, "_DomicilioFamiliarId", DbType.Int32, familiar.Domicilio.Id);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, familiar.Detenido.Id);
            db.AddInParameter(cmd, "_ParentescoId", DbType.Int32, familiar.Parentesco.Id);
            return cmd;
        }
    }
}
