﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Familiar
{
    public class Guardar : IInsertFactory<Entity.Familiar>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Familiar item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Familiar_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, item.Nombre);
            db.AddInParameter(cmd, "_ApellidoPaterno", DbType.String, item.ApellidoPaterno);
            db.AddInParameter(cmd, "_ApellidoMaterno", DbType.String, item.ApellidoMaterno);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_OcupacionId", DbType.Boolean, item.Ocupacion.Id);
            db.AddInParameter(cmd, "_SexoId", DbType.Int32, item.Sexo.Id);
            db.AddInParameter(cmd, "_Estado", DbType.String, item.Estado);
            db.AddInParameter(cmd, "_DomicilioFamiliarId", DbType.Int32, item.Domicilio.Id);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, item.Detenido.Id);
            db.AddInParameter(cmd, "_ParentescoId", DbType.Int32, item.Parentesco.Id);


            return cmd;
        }
    }
}
