﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionTatuaje
{
    public interface ICertificadoLesionTatuaje
    {
        int Guardar(Entity.CertificadoLesionTatuajes certificadoLesionTatuajes);
        Entity.CertificadoLesionTatuajes ObtenerPorId(int Id);
    }
}
