﻿using System.Data;
using DataAccess.Interfaces;
using Entity;
namespace DataAccess.CertificadoLesionTatuaje
{
    public class FabricaCertificadoLesionTatuaje : IEntityFactory<Entity.CertificadoLesionTatuajes>
    {
        public CertificadoLesionTatuajes ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoLesionTatuajes();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }
            return item;
        }
    }
}
