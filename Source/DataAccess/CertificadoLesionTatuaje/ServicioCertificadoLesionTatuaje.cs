﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;


namespace DataAccess.CertificadoLesionTatuaje
{
    public class ServicioCertificadoLesionTatuaje:ClsAbstractService<Entity.CertificadoLesionTatuajes>,ICertificadoLesionTatuaje
    {
        public ServicioCertificadoLesionTatuaje(string dataBase)
    : base(dataBase)
        {
        }

        public int Guardar(CertificadoLesionTatuajes certificadoLesionTatuajes)
        {
            IInsertFactory<Entity.CertificadoLesionTatuajes> insert = new Guardar();
            return Insert(insert, certificadoLesionTatuajes);
        }

        public CertificadoLesionTatuajes ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoLesionTatuaje(), Id);
        }
    }
}
