﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.CertificadoLesionTatuaje
{
    public class ObtenerPorId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionTauajes_ObtenerPorId_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, Id);
            return cmd;
        }
    }
}
