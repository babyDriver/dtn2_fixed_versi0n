﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ReporteExtravioList
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ReporteExtravioList_GetByData_SP");
            db.AddInParameter(cmd, "_anio", DbType.Int32, parametros[0]);
            db.AddInParameter(cmd, "_contratoId", DbType.Int32, parametros[1]);
            db.AddInParameter(cmd, "_tipo", DbType.String, parametros[2]);
            return cmd;
        }
    }
}
