﻿using System.Collections.Generic;

namespace DataAccess.ReporteExtravioList
{
    public interface IReporteExtravioList
    {
        List<Entity.ReporteExtravioList> ObtenerTodos(object[] data);
    }
}
