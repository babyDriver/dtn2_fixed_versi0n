﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ReporteExtravioList
{
    public class ServicioReporteExtravioList : ClsAbstractService<Entity.ReporteExtravioList>, IReporteExtravioList
    {
        public ServicioReporteExtravioList(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ReporteExtravioList> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaReporteExtravioList(), parametros);
        }
    }
}
