﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Traslado
{
    public class ObtenerPorDetalledetencion : ISelectFactory<Entity.Traslado>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.Traslado traslado)
        {
            DbCommand cmd = db.GetStoredProcCommand("Traslado_GET_BY_DetalleDetencionId_SP");
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, traslado.DetalleDetencionId);
          
            return cmd;
        }
    }
}
