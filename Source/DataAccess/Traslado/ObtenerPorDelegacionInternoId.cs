﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccess.Traslado
{
    public class ObtenerPorDelegacionInternoId : ISelectFactory<Entity.Traslado>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.Traslado traslado)
        {
            DbCommand cmd = db.GetStoredProcCommand("Traslado_GET_BY_DelegacionIdDetalleDetencionId__SP");
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, traslado.DetalleDetencionId);
            db.AddInParameter(cmd, "_DelegacionId", DbType.Int32, traslado.DelegacionId);
            return cmd;
        }
    }
}
