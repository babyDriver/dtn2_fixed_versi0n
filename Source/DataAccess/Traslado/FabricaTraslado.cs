﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.Traslado
{
    public class FabricaTraslado : IEntityFactory<Entity.Traslado>
    {
        public Entity.Traslado ConstructEntity(IDataReader dr)
        {
            var traslado = new Entity.Traslado();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                traslado.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                traslado.TrackingId = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetalleDetencionId");
            if (!dr.IsDBNull(index))
            {
                traslado.DetalleDetencionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("DelegacionId");
            if (!dr.IsDBNull(index))
            {
                traslado.DelegacionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Fechahora");
            if (!dr.IsDBNull(index))
            {
                traslado.Fechahora = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                traslado.CreadoPor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                traslado.CreadoPor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                traslado.Activo = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                traslado.Habilitado = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TiposalidaId");
            if (!dr.IsDBNull(index))
            {
                traslado.TiposalidaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Fundamento");
            if (!dr.IsDBNull(index))
            {
                traslado.Fundamento = dr.GetString(index);
            }
            index = dr.GetOrdinal("Horas");
            if (!dr.IsDBNull(index))
            {
                traslado.Horas = dr.GetInt32(index);
            }
            return traslado;
        }   
    }
}
