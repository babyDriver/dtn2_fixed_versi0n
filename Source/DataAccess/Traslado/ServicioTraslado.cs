﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.Traslado
{
    public class ServicioTraslado : ClsAbstractService<Entity.Traslado>, ITraslado

    {
        public ServicioTraslado(string dataBase)
           : base(dataBase)
        {
        }

        public Entity.Traslado GetTrasladoByDetalledetencion(Entity.Traslado traslado)
        {
            ISelectFactory<Entity.Traslado> select = new ObtenerPorDetalledetencion();
            return GetByKey(select, new FabricaTraslado(), traslado);
        }

        public Entity.Traslado GetTrasladoByKeys(Entity.Traslado traslado)
        {
            ISelectFactory<Entity.Traslado> select = new ObtenerPorDelegacionInternoId();
            return GetByKey(select, new FabricaTraslado(), traslado);
        }

        public int Guardar(Entity.Traslado traslado)
        {
            IInsertFactory<Entity.Traslado> insert = new Guardar();
            return Insert(insert, traslado);
        }
    }

}
