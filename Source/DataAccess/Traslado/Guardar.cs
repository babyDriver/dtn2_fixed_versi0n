﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.Traslado
{
    public class Guardar : IInsertFactory<Entity.Traslado>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Traslado traslado)
        {
            DbCommand cmd = db.GetStoredProcCommand("Traslado_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, traslado.TrackingId);
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, traslado.DetalleDetencionId);
            db.AddInParameter(cmd, "_DelegacionId", DbType.Int32, traslado.DelegacionId);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, traslado.Fechahora);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, traslado.CreadoPor);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, traslado.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, traslado.Habilitado);
            db.AddInParameter(cmd, "_TiposalidaId", DbType.Int32, traslado.TiposalidaId);
            db.AddInParameter(cmd, "_Fundamento", DbType.String, traslado.Fundamento);
            db.AddInParameter(cmd, "_Horas", DbType.Int32, traslado.Horas);

            return cmd;
        }
    }
}
