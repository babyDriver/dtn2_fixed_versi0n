﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
namespace DataAccess.Traslado
{
   public interface ITraslado
    {
        int Guardar(Entity.Traslado traslado);
        Entity.Traslado GetTrasladoByKeys(Entity.Traslado traslado);
        Entity.Traslado GetTrasladoByDetalledetencion(Entity.Traslado traslado);
    }
}
