﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.ReportesLog
{
    public class Guardar : IInsertFactory<Entity.ReportesLog>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.ReportesLog reportesLog)
        {

            DbCommand cmd = db.GetStoredProcCommand("Reporteslog_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_ProcesoId", DbType.Int32, reportesLog.ProcesoId);
            db.AddInParameter(cmd, "_ProcesoTrackingId", DbType.String, reportesLog.ProcesoTrackingId);
            db.AddInParameter(cmd, "_Reporte", DbType.String, reportesLog.Reporte);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, reportesLog.CreadoPor);
            
            db.AddInParameter(cmd, "_Fechayhora", DbType.DateTime, reportesLog.Fechayhora);
            db.AddInParameter(cmd, "_Modulo", DbType.String, reportesLog.Modulo);
            db.AddInParameter(cmd, "_EstatusId", DbType.Int32, reportesLog.EstatusId);


            return cmd;
        }
    }
}
