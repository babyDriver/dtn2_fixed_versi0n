﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReportesLog
{
   public interface IReportesLog
    {
        int Guardar(Entity.ReportesLog reportesLog);
    }
}
