﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.ReportesLog
{
    public class ServicioReportesLog : ClsAbstractService<Entity.ReportesLog>, IReportesLog
    {
        public ServicioReportesLog(string dataBase)
         : base(dataBase)
        {
        }
        public int Guardar(Entity.ReportesLog reportesLog)
        {
            IInsertFactory<Entity.ReportesLog> insert = new Guardar();
            return Insert(insert, reportesLog);
        }
    }
}
