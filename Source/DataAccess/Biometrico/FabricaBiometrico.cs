﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.Biometrico
{
    public class FabricaBiometrico : IEntityFactory<Entity.Biometrico>
    {
        public Entity.Biometrico ConstructEntity(IDataReader dr)
        {
            var biometrico = new Entity.Biometrico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                biometrico.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                biometrico.TrackingId = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                biometrico.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                biometrico.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                biometrico.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Rutaimagen");
            if (!dr.IsDBNull(index))
            {
                biometrico.Rutaimagen = dr.GetString(index);
            }

            index = dr.GetOrdinal("TipobiometricoId");
            if (!dr.IsDBNull(index))
            {
                biometrico.TipobiometricoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Clave");
            if (!dr.IsDBNull(index))
            {
                biometrico.Clave = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                biometrico.FechaHora = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                biometrico.Activo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                biometrico.Habilitado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                biometrico.Creadopor = dr.GetInt32(index);
            }

            return biometrico;
        }
    }
}
