﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Biometrico
{
    public class ServicioBiometrico : ClsAbstractService<Entity.Biometrico>, IBiometrico
    {

        public ServicioBiometrico(string dataBase)
          : base(dataBase)
        {
        }

        public void Actualizar(Entity.Biometrico biometrico)
        {
            IUpdateFactory<Entity.Biometrico> update = new Actualizar();
            Update(update, biometrico);

        }

        public new List<Entity.Biometrico> GetAll()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaBiometrico(), new Entity.NullClass());
        }

        public List<Entity.Biometrico> GetByDetenidoId(int DetenidoId)
        {
            ISelectFactory<int> select = new ObtenerByDetenidoId();
            return GetAll(select, new FabricaBiometrico(), DetenidoId);

        }

        public Entity.Biometrico GetById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaBiometrico(), Id);
        }

        public int Guardar(Entity.Biometrico biometrico)
        {
            IInsertFactory<Entity.Biometrico> insert = new Guardar();
            return Insert(insert, biometrico);
        }
    }
}
