﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.Biometrico
{
    public class Guardar : IInsertFactory<Entity.Biometrico>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Biometrico biometrico)
        {
            DbCommand cmd = db.GetStoredProcCommand("Biometrico_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, biometrico.TrackingId);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, biometrico.DetenidoId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, biometrico.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, biometrico.Descripcion);
            db.AddInParameter(cmd, "_Rutaimagen", DbType.String, biometrico.Rutaimagen);
            db.AddInParameter(cmd, "_TipobiometricoId", DbType.Int32, biometrico.TipobiometricoId);
            db.AddInParameter(cmd, "_Clave", DbType.String, biometrico.Clave);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, biometrico.FechaHora);
            db.AddInParameter(cmd, "_Activo", DbType.Int32, biometrico.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int32, biometrico.Habilitado);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, biometrico.Creadopor);
            return cmd;
        }
    }
}
