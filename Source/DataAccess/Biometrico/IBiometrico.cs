﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Biometrico
{
    public interface IBiometrico
    {

        int Guardar(Entity.Biometrico biometrico);

        void Actualizar(Entity.Biometrico biometrico);

        List<Entity.Biometrico> GetByDetenidoId(int DetenidoId);

        List<Entity.Biometrico> GetAll();

        Entity.Biometrico GetById(int Id);
    }
}
