﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Biometrico
{
    public class ObtenerByDetenidoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetenidoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Biometrico_GetByDetenidoId_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, DetenidoId);
            return cmd;
        }
    }
}
