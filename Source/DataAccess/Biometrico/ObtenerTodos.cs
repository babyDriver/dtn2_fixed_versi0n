﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.Biometrico
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Biometrico_GetAll_SP");
            return cmd;
        }
    }
}
