﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAMotivo
{
    public class Actualizar : IUpdateFactory<Entity.WSAMotivo>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.WSAMotivo entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAMotivo_Update_SP");
            db.AddInParameter(cmd, "_idMotivo", DbType.Int32, entity.IdMotivo);
            db.AddInParameter(cmd, "_nombreMotivoLlamada", DbType.String, entity.NombreMotivoLlamada);
            db.AddInParameter(cmd, "_prioridadAtencionEvento", DbType.String, entity.PrioridadAtencionEvento);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);

            return cmd;
        }
    }
}
