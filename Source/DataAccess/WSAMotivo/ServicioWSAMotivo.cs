﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.WSAMotivo
{
    public class ServicioWSAMotivo : ClsAbstractService<Entity.WSAMotivo>, IWSAMotivo
    {
        public ServicioWSAMotivo(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.WSAMotivo> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaWSAMotivo(), new Entity.NullClass());
        }

        public Entity.WSAMotivo ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSAMotivo(), id);
        }

        public int Guardar(Entity.WSAMotivo motivo)
        {
            IInsertFactory<Entity.WSAMotivo> insert = new Guardar();
            return Insert(insert, motivo);
        }

        public void Actualizar(Entity.WSAMotivo motivo)
        {
            IUpdateFactory<Entity.WSAMotivo> update = new Actualizar();
            Update(update, motivo);
        }
    }
}
