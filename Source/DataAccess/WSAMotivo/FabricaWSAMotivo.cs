﻿using DataAccess.Interfaces;

namespace DataAccess.WSAMotivo
{
    public class FabricaWSAMotivo : IEntityFactory<Entity.WSAMotivo>
    {
        public Entity.WSAMotivo ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.WSAMotivo();

            var index = dr.GetOrdinal("idMotivo");
            if (!dr.IsDBNull(index))
            {
                item.IdMotivo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("nombreMotivoLlamada");
            if (!dr.IsDBNull(index))
            {
                item.NombreMotivoLlamada = dr.GetString(index);
            }

            index = dr.GetOrdinal("prioridadAtencionEvento");
            if (!dr.IsDBNull(index))
            {
                item.PrioridadAtencionEvento = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaModificacion");
            if (!dr.IsDBNull(index))
            {
                item.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            return item;
        }
    }
}
