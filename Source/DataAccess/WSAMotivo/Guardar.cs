﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAMotivo
{
    public class Guardar : IInsertFactory<Entity.WSAMotivo>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSAMotivo entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAMotivo_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_idMotivo", DbType.Int32, entity.IdMotivo);
            db.AddInParameter(cmd, "_nombreMotivoLlamada", DbType.String, entity.NombreMotivoLlamada);
            db.AddInParameter(cmd, "_prioridadAtencionEvento", DbType.String, entity.PrioridadAtencionEvento);

            return cmd;
        }
    }
}
