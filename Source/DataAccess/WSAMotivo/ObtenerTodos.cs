﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.WSAMotivo
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.NullClass id)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAMotivo_GetAll_SP");
            return cmd;
        }
    }
}
