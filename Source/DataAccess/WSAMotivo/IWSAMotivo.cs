﻿using System.Collections.Generic;

namespace DataAccess.WSAMotivo
{
    public interface IWSAMotivo
    {
        List<Entity.WSAMotivo> ObtenerTodos();
        Entity.WSAMotivo ObtenerById(int id);
        int Guardar(Entity.WSAMotivo motivo);
        void Actualizar(Entity.WSAMotivo motivo);
    }
}
