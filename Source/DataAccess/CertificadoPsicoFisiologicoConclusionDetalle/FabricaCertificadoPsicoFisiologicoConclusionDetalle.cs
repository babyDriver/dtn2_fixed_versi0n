﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CertificadoPsicoFisiologicoConclusionDetalle
{
    public class FabricaCertificadoPsicoFisiologicoConclusionDetalle : IEntityFactory<Entity.CertificadoPsicoFisiologicoConclusionDetalle>
    {
        public Entity.CertificadoPsicoFisiologicoConclusionDetalle ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoPsicoFisiologicoConclusionDetalle();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("certificado_psicofisiologicoConclusionId");
            if (!dr.IsDBNull(index))
            {
                item.certificado_psicofisiologicoConclusionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("ConclusionId");
            if (!dr.IsDBNull(index))
            {
                item.ConclusionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }

            return item;
        }
    }
}
