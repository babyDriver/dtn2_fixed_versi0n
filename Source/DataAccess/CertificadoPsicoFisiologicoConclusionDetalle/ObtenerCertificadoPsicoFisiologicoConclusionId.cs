﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoPsicoFisiologicoConclusionDetalle
{
    public class ObtenerCertificadoPsicoFisiologicoConclusionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("detallecertificado_psicofisiologicoConclusionDetalle_ById_SP");
            db.AddInParameter(cmd, "_certificado_psicofisiologicoConclusionId", DbType.Int32, Id);
            return cmd;
        }
    }
}
