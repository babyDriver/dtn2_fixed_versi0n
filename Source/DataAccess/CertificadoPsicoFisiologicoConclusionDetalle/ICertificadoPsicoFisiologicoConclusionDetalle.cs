﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoPsicoFisiologicoConclusionDetalle
{
   public interface ICertificadoPsicoFisiologicoConclusionDetalle
    {
        int Guardar(Entity.CertificadoPsicoFisiologicoConclusionDetalle certificadoPsicoFisilogicoConclusion);
        List<Entity.CertificadoPsicoFisiologicoConclusionDetalle> ObtenerPorId(int Id);



    }
}
