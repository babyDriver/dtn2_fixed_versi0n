﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoPsicoFisiologicoConclusionDetalle
{
    public class Guardar : IInsertFactory<Entity.CertificadoPsicoFisiologicoConclusionDetalle>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CertificadoPsicoFisiologicoConclusionDetalle entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("detallecertificado_psicofisiologicoConclusionDetalle_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_certificado_psicofisiologicoConclusionId", DbType.Int32, entity.certificado_psicofisiologicoConclusionId);
            db.AddInParameter(cmd, "_ConclusionId", DbType.Int32, entity.ConclusionId);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, entity.FechaHora);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, entity.Creadopor);
            return cmd;
        }
    }
}
