﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.CertificadoPsicoFisiologicoConclusionDetalle
{
    public class ServicioCertificadoPsicoFisiologicoConclusionDetalle : ClsAbstractService<Entity.CertificadoPsicoFisiologicoConclusionDetalle>, ICertificadoPsicoFisiologicoConclusionDetalle
    {
        public ServicioCertificadoPsicoFisiologicoConclusionDetalle(string dataBase)
        : base(dataBase)
        {
        }
        public int Guardar(Entity.CertificadoPsicoFisiologicoConclusionDetalle certificadoPsicoFisilogicoConclusion)
        {
            IInsertFactory<Entity.CertificadoPsicoFisiologicoConclusionDetalle> insert = new Guardar();
            return Insert(insert, certificadoPsicoFisilogicoConclusion);
        }

        public List<Entity.CertificadoPsicoFisiologicoConclusionDetalle> ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerCertificadoPsicoFisiologicoConclusionId();
            return GetAll(select, new FabricaCertificadoPsicoFisiologicoConclusionDetalle(), Id);
        }
    }
}
