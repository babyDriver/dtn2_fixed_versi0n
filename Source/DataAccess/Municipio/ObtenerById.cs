﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Municipio
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int municipioId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Municipio_GetById_SP");
            db.AddInParameter(cmd, "Id", DbType.Int32, municipioId);
            return cmd;
        }
    }
}
