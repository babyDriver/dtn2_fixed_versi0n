﻿using System;
using System.Collections.Generic;

namespace DataAccess.Municipio
{
    public interface IMunicipio
    {
        List<Entity.Municipio> ObtenerTodos();
        Entity.Municipio ObtenerPorId(int municipioId);
        Entity.Municipio ObtenerPorClave(string clave);
        List<Entity.Municipio> ObtenerPorEstadoId(int estadoId);
        Entity.Municipio ObtenerPorTrackingId(System.Guid trackingId);
    }
}
