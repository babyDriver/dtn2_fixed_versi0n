﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Municipio
{
    public class ObtenerPorCvegeo : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string cvegeo)
        {
            DbCommand cmd = db.GetStoredProcCommand("[Municipio_GetByCvegeo_SP]");
            db.AddInParameter(cmd, "@Cvegeo", DbType.String, cvegeo);
            return cmd;
        }
    }
}
