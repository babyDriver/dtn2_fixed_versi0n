﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.Municipio
{
    public class Actualizar : IUpdateFactory<Entity.Municipio>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Municipio municipio)
        {
            DbCommand cmd = db.GetStoredProcCommand("Municipio_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, municipio.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, municipio.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, municipio.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, municipio.Descripcion);
            db.AddInParameter(cmd, "_Clave", DbType.String, municipio.Clave);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, municipio.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, municipio.Habilitado);
            db.AddInParameter(cmd, "_EstadoId", DbType.Int32, municipio.EstadoId);
            db.AddInParameter(cmd, "_Salario", DbType.Double, municipio.Salario);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, municipio.Creadopor);

            return cmd;
        }
    }
}
