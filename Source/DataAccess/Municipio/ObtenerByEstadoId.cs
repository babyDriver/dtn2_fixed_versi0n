﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Municipio
{
    public class ObtenerByEstadoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int estadoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Municipio_GetByEstadoId_SP");
            db.AddInParameter(cmd, "EstadoId", DbType.Int32, estadoId);
            return cmd;
        }
    }
}
