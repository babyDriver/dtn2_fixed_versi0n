﻿using DataAccess.Interfaces;
using System.Data;
using DataAccess.Estado;

namespace DataAccess.Municipio
{
    public class FabricaMunicipio : IEntityFactory<Entity.Municipio>
    {
        public Entity.Municipio ConstructEntity(IDataReader dr)
        {
            var municipio = new Entity.Municipio();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                municipio.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                municipio.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                municipio.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("EstadoId");
            if (!dr.IsDBNull(index))
            {
                municipio.EstadoId = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                municipio.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Clave");
            if (!dr.IsDBNull(index))
            {
                municipio.Clave = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                municipio.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                municipio.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("SalarioMinimo");
            if (!dr.IsDBNull(index))
            {
                municipio.Salario = dr.GetDouble(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                municipio.Creadopor = dr.GetInt32(index);
            }

            return municipio;
        }

    }
}
