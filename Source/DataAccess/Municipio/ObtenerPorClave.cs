﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Municipio
{
    public class ObtenerPorClave : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string cvegeo)
        {
            DbCommand cmd = db.GetStoredProcCommand("Municipio_GetByClave_SP");
            db.AddInParameter(cmd, "Clave", DbType.String, cvegeo);
            return cmd;
        }
    }
}
