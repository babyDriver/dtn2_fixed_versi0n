﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.Municipio
{
    public class ServicioMunicipio : ClsAbstractService<Entity.Municipio>, IMunicipio
    {
        public ServicioMunicipio(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Municipio> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaMunicipio(), new Entity.NullClass());
        }

        public Entity.Municipio ObtenerPorId(int municipioId)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaMunicipio(), municipioId);
        }

        public List<Entity.Municipio> ObtenerPorEstadoId(int estadoId)
        {
            ISelectFactory<int> select = new ObtenerByEstadoId();
            return GetAll(select, new FabricaMunicipio(), estadoId);
        }

        public Entity.Municipio ObtenerPorTrackingId(System.Guid trackingId)
        {
            ISelectFactory<System.Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaMunicipio(), trackingId);
        }

        public Entity.Municipio ObtenerPorClave(string clave)
        {
            ISelectFactory<string> select = new ObtenerPorClave();
            return GetByKey(select, new FabricaMunicipio(), clave);
        }
        public int Guardar(Entity.Municipio municipio)
        {
            IInsertFactory<Entity.Municipio> insert = new Guardar();
            return Insert(insert, municipio);
        }

        public void Actualizar(Entity.Municipio municipio)
        {
            IUpdateFactory<Entity.Municipio> update = new Actualizar();
            Update(update, municipio);
        }
    }
}
