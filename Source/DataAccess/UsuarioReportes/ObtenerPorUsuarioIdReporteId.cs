﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioReportes
{
    public class ObtenerPorUsuarioIdReporteId : ISelectFactory<int[]>
    {
        public DbCommand ConstructSelectCommand(Database db, int[] identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_Reportes_GetByUsuarioIdReporteId_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, identity[0]);
            db.AddInParameter(cmd, "_ReporteId", DbType.Int32, identity[1]);

            return cmd;
        }
    }
}
