﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioReportes
{
    public class ObtenerPorUsuarioIdPantallaId : ISelectFactory<int[]>
    {
        public DbCommand ConstructSelectCommand(Database db, int[] identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_Reportes_GetByUsuarioIdPantallaId_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, identity[0]);
            db.AddInParameter(cmd, "_PantallaId", DbType.Int32, identity[1]);

            return cmd;
        }
    }
}
