﻿using System;
using System.Collections.Generic;

namespace DataAccess.UsuarioReportes
{
    public interface IUsuarioReportes
    {
        int Guardar(Entity.UsuarioReportes usuarioReportes);
        void Actualizar(Entity.UsuarioReportes usuarioReportes);
        void EliminarPorUsuarioId(int usuarioId);
        List<Entity.UsuarioReportes> ObtenerTodos();
        List<Entity.UsuarioReportes> ObtenerPorUsuarioId(int usuarioId);
        List<Entity.UsuarioReportes> ObtenerPorReporteId(int reporteId);
        Entity.UsuarioReportes ObtenerPorId(int id);
        Entity.UsuarioReportes ObtenerPorTrackingId(Guid trackingId);
        Entity.UsuarioReportes ObtenerPorUsuarioIdReporteId(int[] usuarioId_reporteId);
        Entity.UsuarioReportes ObtenerPorUsuarioIdPantallaId(int[] usuarioId_pantallaId);
    }
}
