﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioReportes
{
    public class Actualizar : IUpdateFactory<Entity.UsuarioReportes>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.UsuarioReportes entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_Reportes_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, entity.Id);
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, entity.UsuarioId);
            db.AddInParameter(cmd, "_ReporteId", DbType.Int32, entity.ReporteId);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, entity.Habilitado);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, entity.CreadoPor);

            return cmd;
        }
    }
}
