﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.UsuarioReportes
{
    public class ServicioUsuarioReportes : ClsAbstractService<Entity.UsuarioReportes>, IUsuarioReportes
    {
        public ServicioUsuarioReportes(string dataBase) : base(dataBase) { }

        public void Actualizar(Entity.UsuarioReportes usuarioReportes)
        {
            IUpdateFactory<Entity.UsuarioReportes> update = new Actualizar();
            Update(update, usuarioReportes);
        }

        public void EliminarPorUsuarioId(int usuarioId)
        {
            IDeleteFactory<int> delete = new EliminarPorUsuarioId();
            Delete(delete, usuarioId);
        }

        public int Guardar(Entity.UsuarioReportes usuarioReportes)
        {
            IInsertFactory<Entity.UsuarioReportes> insert = new Guardar();
            return Insert(insert, usuarioReportes);
        }

        public Entity.UsuarioReportes ObtenerPorId(int id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaUsuarioReportes(), id);
        }

        public List<Entity.UsuarioReportes> ObtenerPorReporteId(int reporteId)
        {
            ISelectFactory<int> select = new ObtenerPorReporteId();
            return GetAll(select, new FabricaUsuarioReportes(), reporteId);
        }

        public Entity.UsuarioReportes ObtenerPorTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerPorTrackingId();
            return GetByKey(select, new FabricaUsuarioReportes(), trackingId);
        }

        public List<Entity.UsuarioReportes> ObtenerPorUsuarioId(int usuarioId)
        {
            ISelectFactory<int> select = new ObtenerPorUsuarioId();
            return GetAll(select, new FabricaUsuarioReportes(), usuarioId);
        }

        public Entity.UsuarioReportes ObtenerPorUsuarioIdPantallaId(int[] usuarioId_pantallaId)
        {
            ISelectFactory<int[]> select = new ObtenerPorUsuarioIdPantallaId();
            return GetByKey(select, new FabricaUsuarioReportes(), usuarioId_pantallaId);
        }

        public Entity.UsuarioReportes ObtenerPorUsuarioIdReporteId(int[] usuarioId_reporteId)
        {
            ISelectFactory<int[]> select = new ObtenerPorUsuarioIdReporteId();
            return GetByKey(select, new FabricaUsuarioReportes(), usuarioId_reporteId);
        }

        public List<Entity.UsuarioReportes> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaUsuarioReportes(), new Entity.NullClass());
        }
    }
}
