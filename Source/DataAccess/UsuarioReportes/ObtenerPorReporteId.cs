﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioReportes
{
    public class ObtenerPorReporteId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_Reportes_GetByReporteId_SP");
            db.AddInParameter(cmd, "_ReporteId", DbType.Int32, identity);

            return cmd;
        }
    }
}
