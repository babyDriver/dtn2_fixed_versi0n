﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioReportes
{
    public class Guardar : IInsertFactory<Entity.UsuarioReportes>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.UsuarioReportes entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_Reportes_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, entity.TrackingId);
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, entity.UsuarioId);
            db.AddInParameter(cmd, "_ReporteId", DbType.Int32, entity.ReporteId);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, entity.Habilitado);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, entity.CreadoPor);

            return cmd;
        }
    }
}
