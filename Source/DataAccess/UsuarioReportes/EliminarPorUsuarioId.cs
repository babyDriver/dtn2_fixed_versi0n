﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioReportes
{
    class EliminarPorUsuarioId : IDeleteFactory<int>
    {
        public DbCommand ConstructDeleteCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_Reportes_DeleteByUsuarioId_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, identity);

            return cmd;
        }
    }
}
