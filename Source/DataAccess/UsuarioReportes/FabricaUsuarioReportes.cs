﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.UsuarioReportes
{
    public class FabricaUsuarioReportes : IEntityFactory<Entity.UsuarioReportes>
    {
        public Entity.UsuarioReportes ConstructEntity(IDataReader dr)
        {
            var usuarioReportes = new Entity.UsuarioReportes();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                usuarioReportes.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                usuarioReportes.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("UsuarioId");
            if (!dr.IsDBNull(index))
            {
                usuarioReportes.UsuarioId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ReporteId");
            if (!dr.IsDBNull(index))
            {
                usuarioReportes.ReporteId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                usuarioReportes.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                usuarioReportes.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                usuarioReportes.CreadoPor = dr.GetInt32(index);
            }

            return usuarioReportes;
        }
    }
}
