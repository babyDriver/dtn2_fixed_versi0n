﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.Rol
{
    public class ServicioRol : ClsAbstractService<Entity.Rol>, IRol
    {
        public ServicioRol(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Rol> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaRol(), new Entity.NullClass());
        }

        public Entity.Rol ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaRol(), Id);
        }

        public Entity.Rol ObtenerByNombre(string nombre)
        {
            ISelectFactory<string> select = new ObtenerByNombre();
            return GetByKey(select, new FabricaRol(), nombre);
        }

        public int Guardar(Entity.Rol rol)
        {
            IInsertFactory<Entity.Rol> insert = new Guardar();
            return Insert(insert, rol);
        }

        public void Actualizar(Entity.Rol rol)
        {
            IUpdateFactory<Entity.Rol> update = new Actualizar();
            Update(update, rol);
        }
    }
}
