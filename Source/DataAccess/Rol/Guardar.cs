﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Rol
{
    public class Guardar : IInsertFactory<Entity.Rol>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Rol rol)
        {
            DbCommand cmd = db.GetStoredProcCommand("my_aspnet_roles_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_applicationId", DbType.Int32, rol.applicationId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, rol.name);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, rol.Descripcion); ;
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, rol.Activo);

            return cmd;
        }
    }
}
