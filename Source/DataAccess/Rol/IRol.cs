﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Rol
{
    public interface IRol
    {
        Entity.Rol ObtenerById(int Id);
        List<Entity.Rol> ObtenerTodos();
        int Guardar(Entity.Rol rol);
        void Actualizar(Entity.Rol rol);
        Entity.Rol ObtenerByNombre(string nombre);
    }
}
