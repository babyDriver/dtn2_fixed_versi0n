﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.Rol
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("my_aspnet_roles_GetAll_SP");
            return cmd;
        }
    }
}
