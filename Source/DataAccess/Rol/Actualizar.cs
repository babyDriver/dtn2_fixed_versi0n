﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Rol
{
    public class Actualizar : IUpdateFactory<Entity.Rol>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Rol rol)
        {
            DbCommand cmd = db.GetStoredProcCommand("my_aspnet_roles_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, rol.id);
            db.AddInParameter(cmd, "_applicationId", DbType.Int32, rol.applicationId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, rol.name);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, rol.Descripcion); ;
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, rol.Activo);


            return cmd;
        }
    }
}
