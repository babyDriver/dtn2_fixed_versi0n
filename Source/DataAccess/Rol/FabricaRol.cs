﻿using DataAccess.Interfaces;


namespace DataAccess.Rol
{
    public class FabricaRol : IEntityFactory<Entity.Rol>
    {
        public Entity.Rol ConstructEntity(System.Data.IDataReader dr)
        {
            var rol = new Entity.Rol();

            var index = dr.GetOrdinal("id");
            if (!dr.IsDBNull(index))
            {
                rol.id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("applicationId");
            if (!dr.IsDBNull(index))
            {
                rol.applicationId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("name");
            if (!dr.IsDBNull(index))
            {
                rol.name = dr.GetString(index);
            }


            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                rol.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                rol.Activo = dr.GetBoolean(index);
            }

            return rol;
        }
    }
}
