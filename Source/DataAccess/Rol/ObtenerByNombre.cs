﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Rol
{
    public class ObtenerByNombre : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string nombre)
        {
            DbCommand cmd = db.GetStoredProcCommand("my_aspnet_roles_GetByNombre_SP");
            db.AddInParameter(cmd, "Nombre", DbType.String, nombre);
            return cmd;
        }

    }
}
