﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;


namespace DataAccess.PermisoUsuario
{
    public class Guardar : IInsertFactory<Entity.PermisoUsuario>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.PermisoUsuario permiso)
        {
            DbCommand cmd = db.GetStoredProcCommand("Permiso_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_Nombre", DbType.Int32, permiso.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, permiso.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, permiso.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, permiso.Habilitado);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, permiso.TrackingId);

            return cmd;
        }
    }
}
