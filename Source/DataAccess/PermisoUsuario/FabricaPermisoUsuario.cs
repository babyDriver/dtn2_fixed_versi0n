﻿using DataAccess.Interfaces;

namespace DataAccess.PermisoUsuario
{
    public class FabricaPermisoUsuario : IEntityFactory<Entity.PermisoUsuario>
    {
        public Entity.PermisoUsuario ConstructEntity(System.Data.IDataReader dr)
        {
            var permiso = new Entity.PermisoUsuario();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                permiso.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                permiso.Nombre = dr.GetString(index);
            }


            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                permiso.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                permiso.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                permiso.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                permiso.TrackingId = dr.GetGuid(index);
            }

            return permiso;
        }
    }
}
