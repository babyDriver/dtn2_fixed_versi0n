﻿using System.Collections.Generic;
using DataAccess.Interfaces;
using System;

namespace DataAccess.PermisoUsuario
{
    public class ServicioPermisoUsuario : ClsAbstractService<Entity.PermisoUsuario>, IPermisoUsuario
    {
        public ServicioPermisoUsuario(string dataBase)
            : base(dataBase)
        {
        }

    

        public List<Entity.PermisoUsuario> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaPermisoUsuario(), new Entity.NullClass());
        }

        public List<Entity.PermisoUsuario> ObtenerByHabilitadoAcitvo(Entity.PermisoUsuario permiso)
        {
            ISelectFactory<Entity.PermisoUsuario> select = new ObtenerByHabilitadoAcitvo();
            return GetAll(select, new FabricaPermisoUsuario(), permiso);
        }

        public Entity.PermisoUsuario ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaPermisoUsuario(), Id);
        }

        public int Guardar(Entity.PermisoUsuario permiso)
        {
            IInsertFactory<Entity.PermisoUsuario> insert = new Guardar();
            return Insert(insert, permiso);
        }

        public void Actualizar(Entity.PermisoUsuario permiso)
        {
            IUpdateFactory<Entity.PermisoUsuario> update = new Actualizar();
            Update(update, permiso);
        }
    }
}
