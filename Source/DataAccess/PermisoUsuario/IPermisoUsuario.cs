﻿using System.Collections.Generic;
using System;

namespace DataAccess.PermisoUsuario
{
    public interface IPermisoUsuario
    {
        Entity.PermisoUsuario ObtenerById(int Id);
        List<Entity.PermisoUsuario> ObtenerTodos();
        List<Entity.PermisoUsuario> ObtenerByHabilitadoAcitvo(Entity.PermisoUsuario pantalla);
        int Guardar(Entity.PermisoUsuario permiso);
        void Actualizar(Entity.PermisoUsuario permiso);
    }
}
