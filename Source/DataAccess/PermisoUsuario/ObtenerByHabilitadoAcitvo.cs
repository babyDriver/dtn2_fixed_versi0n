﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.PermisoUsuario
{
    public class ObtenerByHabilitadoAcitvo : ISelectFactory<Entity.PermisoUsuario>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.PermisoUsuario permiso)
        {
            DbCommand cmd = db.GetStoredProcCommand("Permiso_GetByHabilitadoAcitvo_SP");
            db.AddInParameter(cmd, "Activo", DbType.Boolean, permiso.Activo);
            db.AddInParameter(cmd, "Habilitado", DbType.Boolean, permiso.Habilitado);
            return cmd;
        }

    }
}
