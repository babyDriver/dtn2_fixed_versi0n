﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.PermisoUsuario
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("Permiso_GetByTrackingId_SP");
            db.AddInParameter(cmd, "TrackingId", DbType.Guid, tracking);
            return cmd;
        }
    }
}
