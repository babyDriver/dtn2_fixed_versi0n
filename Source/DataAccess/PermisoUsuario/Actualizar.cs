﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.PermisoUsuario
{
    public class Actualizar : IUpdateFactory<Entity.PermisoUsuario>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.PermisoUsuario permiso)
        {
            DbCommand cmd = db.GetStoredProcCommand("Permiso_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, permiso.Id);
            db.AddInParameter(cmd, "_Nombre", DbType.Int32, permiso.Nombre);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, permiso.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, permiso.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, permiso.Habilitado);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, permiso.TrackingId);


            return cmd;
        }
    }
}
