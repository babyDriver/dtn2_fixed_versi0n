﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;
using Entity;

namespace DataAccess.DetenidosPorColonia
{
    public class ObtenerDetenidosMenoresEdadPorColonia : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] filtros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ReporteEstadistico_GetDetenidosMenorEdadporColonia_SP");
            db.AddInParameter(cmd, "_Fechainicio", DbType.Date, Convert.ToDateTime(filtros[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.Date, Convert.ToDateTime(filtros[1]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(filtros[3]));
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, Convert.ToInt32(filtros[5]));
            return cmd;
        }
    }
}