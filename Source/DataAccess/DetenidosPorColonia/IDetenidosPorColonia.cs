﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DetenidosPorColonia
{
    public interface IDetenidosPorColonia
    {

        List<Entity.Detenidosporcolonia> GetDetenidosPorColonia(string[] Filtro);
        List<Entity.Detenidosporcolonia> GetDetenidosMayorEdadPorColonia(string[] Filtro);
        List<Entity.Detenidosporcolonia> GetDetenidosMenorEdadPorColonia(string[] Filtro);
    }
}