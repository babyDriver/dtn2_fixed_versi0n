﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DetenidosPorColonia
{
    public class ServicioDetenidosPorColonia : ClsAbstractService<Entity.Detenidosporcolonia>, IDetenidosPorColonia
    {
        public ServicioDetenidosPorColonia(String dataBase)
            : base(dataBase)
        {
        }

        public List<Detenidosporcolonia> GetDetenidosMayorEdadPorColonia(string[] Filtro)
        {
            ISelectFactory<string[]> select = new ObtenerDeteniodosMayoresEdadPorColonia();
            return GetAll(select, new FabricaDetenidosPorColonia(), Filtro);
        }

        public List<Detenidosporcolonia> GetDetenidosMenorEdadPorColonia(string[] Filtro)
        {
            ISelectFactory<string[]> select = new ObtenerDetenidosMenoresEdadPorColonia();
            return GetAll(select, new FabricaDetenidosPorColonia(), Filtro);
        }

        public List<Detenidosporcolonia> GetDetenidosPorColonia(string[] Filtro)
        {
            ISelectFactory<string[]> select = new ObtenerDetenidosPorColonia();
            return GetAll(select, new FabricaDetenidosPorColonia(), Filtro);
        }
    }
}