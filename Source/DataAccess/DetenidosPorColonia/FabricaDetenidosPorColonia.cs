﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DetenidosPorColonia
{
    public class FabricaDetenidosPorColonia : IEntityFactory<Entity.Detenidosporcolonia>
    {
        public Detenidosporcolonia ConstructEntity(IDataReader dr)
        {
            var detenidosporcolonia = new Entity.Detenidosporcolonia();

            var index = dr.GetOrdinal("MunicipioId");
            if (!dr.IsDBNull(index))
            {
                detenidosporcolonia.MunicipioId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Municipio");
            if (!dr.IsDBNull(index))
            {
                detenidosporcolonia.Municipio = dr.GetString(index);
            }
            index = dr.GetOrdinal("ColoniaId");
            if (!dr.IsDBNull(index))
            {
                detenidosporcolonia.ColoniaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Colonia");
            if (!dr.IsDBNull(index))
            {
                detenidosporcolonia.Colonia = dr.GetString(index);
            }

            index = dr.GetOrdinal("Cantidad");
            if (!dr.IsDBNull(index))
            {
                detenidosporcolonia.Cantidad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                detenidosporcolonia.Fecha = dr.GetDateTime(index);
            }
            return detenidosporcolonia;
        }
    }
}