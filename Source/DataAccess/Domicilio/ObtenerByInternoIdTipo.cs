﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.Domicilio
{
    public class ObtenerByInternoIdTipo : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] datos)
        {
            DbCommand cmd = db.GetStoredProcCommand("Domicilio_Nacimiento_GetByInternoIdTipo_SP");
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, Convert.ToInt32(datos[0]));
            db.AddInParameter(cmd, "_Tipo", DbType.Boolean, Convert.ToBoolean(datos[1]));
            return cmd;
        }
    }
}
