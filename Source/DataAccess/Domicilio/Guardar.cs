﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Domicilio
{
    public class Guardar : IInsertFactory<Entity.Domicilio>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Domicilio domicilio)
        {
            DbCommand cmd = db.GetStoredProcCommand("Domicilio_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, domicilio.TrackingId);
            db.AddInParameter(cmd, "_Calle", DbType.String, domicilio.Calle);
            db.AddInParameter(cmd, "_Numero", DbType.String, domicilio.Numero);
            //db.AddInParameter(cmd, "_Colonia", DbType.String, domicilio.Colonia);
            //db.AddInParameter(cmd, "_CP", DbType.Int32, domicilio.CP);
            db.AddInParameter(cmd, "_Telefono", DbType.String, domicilio.Telefono);
            db.AddInParameter(cmd, "_ColoniaId", DbType.Int32, domicilio.ColoniaId);
            db.AddInParameter(cmd, "_PaisId", DbType.Int32, domicilio.PaisId);
            db.AddInParameter(cmd, "_Latitud", DbType.String, domicilio.Latitud);
            db.AddInParameter(cmd, "_Longitud", DbType.String, domicilio.Longitud);

            if (domicilio.EstadoId != null)
                db.AddInParameter(cmd, "_EstadoId", DbType.Int32, domicilio.EstadoId);
            else
                db.AddInParameter(cmd, "_EstadoId", DbType.Int32, null);

            if (domicilio.MunicipioId != null)
                db.AddInParameter(cmd, "_MunicipioId", DbType.Int32, domicilio.MunicipioId);
            else
                db.AddInParameter(cmd, "_MunicipioId", DbType.Int32, null);
            db.AddInParameter(cmd, "_Localidad", DbType.String, domicilio.Localidad);

            return cmd;
        }
    }
}
