﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Domicilio
{
    public class ServicioDomicilio : ClsAbstractService<Entity.Domicilio>, IDomicilio
    {
        public ServicioDomicilio(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.Domicilio> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaDomicilio(), new Entity.NullClass());
        }

        public Entity.Domicilio ObtenerByInternoIdTipo(string[] datos)
        {
            ISelectFactory<string[]> select = new ObtenerByInternoIdTipo();
            return GetByKey(select, new FabricaDomicilio(), datos);
        }

        public Entity.Domicilio ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaDomicilio(), trackingId);
        }

        public Entity.Domicilio ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaDomicilio(), Id);
        }

        public Entity.Domicilio ObtenerByInternoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByInternoId();
            return GetByKey(select, new FabricaDomicilio(), Id);
        }

        public int Guardar(Entity.Domicilio domicilio)
        {
            IInsertFactory<Entity.Domicilio> insert = new Guardar();
            return Insert(insert, domicilio);
        }

        public void Actualizar(Entity.Domicilio domicilio)
        {
            IUpdateFactory<Entity.Domicilio> update = new Actualizar();
            Update(update, domicilio);
        }


    }
}