﻿using System;
using System.Collections.Generic;

namespace DataAccess.Domicilio
{
    public interface IDomicilio
    {
        List<Entity.Domicilio> ObtenerTodos();
        Entity.Domicilio ObtenerById(int Id);
        Entity.Domicilio ObtenerByInternoId(int Id);
        Entity.Domicilio ObtenerByInternoIdTipo(string[] datos);
        Entity.Domicilio ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Domicilio domicilio);
        void Actualizar(Entity.Domicilio domicilio);
    }
}