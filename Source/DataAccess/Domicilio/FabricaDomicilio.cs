﻿using DataAccess.Interfaces;


namespace DataAccess.Domicilio
{
    public class FabricaDomicilio : IEntityFactory<Entity.Domicilio>
    {
       

        public Entity.Domicilio ConstructEntity(System.Data.IDataReader dr)
        {
            var domicilio = new Entity.Domicilio();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                domicilio.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                domicilio.TrackingId = dr.GetGuid(index);
            }

       
            index = dr.GetOrdinal("Calle");
            if (!dr.IsDBNull(index))
            {
                domicilio.Calle = dr.GetString(index);
            }

            //index = dr.GetOrdinal("Colonia");
            //if (!dr.IsDBNull(index))
            //{
            //    domicilio.Colonia = dr.GetString(index);
            //}

            index = dr.GetOrdinal("Numero");
            if (!dr.IsDBNull(index))
            {
                domicilio.Numero = dr.GetString(index);
            }

            //index = dr.GetOrdinal("CP");
            //if (!dr.IsDBNull(index))
            //{
            //    domicilio.CP = dr.GetInt32(index);
            //}

            index = dr.GetOrdinal("Telefono");
            if (!dr.IsDBNull(index))
            {
                domicilio.Telefono = dr.GetString(index);
            }

            index = dr.GetOrdinal("PaisId");
            if (!dr.IsDBNull(index))
            {
                domicilio.PaisId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("EstadoId");
            if (!dr.IsDBNull(index))
            {
                domicilio.EstadoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("MunicipioId");
            if (!dr.IsDBNull(index))
            {
                domicilio.MunicipioId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Localidad");
            if (!dr.IsDBNull(index))
            {
                domicilio.Localidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("ColoniaId");
            if (!dr.IsDBNull(index))
            {
                domicilio.ColoniaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Longitud");
            if (!dr.IsDBNull(index))
            {
                domicilio.Longitud = dr.GetString(index);
            }

            index = dr.GetOrdinal("Latitud");
            if (!dr.IsDBNull(index))
            {
                domicilio.Latitud = dr.GetString(index);
            }

            return domicilio;
        }
    }
}