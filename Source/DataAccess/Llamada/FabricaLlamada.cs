﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Llamada
{
    public class FabricaLlamada : IEntityFactory<Entity.Llamada>
    {
        public Entity.Llamada ConstructEntity(IDataReader dr)
        {
            var item = new Entity.Llamada();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Involucrado");
            if (!dr.IsDBNull(index))
            {
                item.Involucrado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                item.Lugar = dr.GetString(index);
            }

            index = dr.GetOrdinal("IdColonia");
            if (!dr.IsDBNull(index))
            {
                item.IdColonia = dr.GetInt32(index);
            }
            
            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetString(index);
            }

            index = dr.GetOrdinal("HoraYFecha");
            if (!dr.IsDBNull(index))
            {
                item.HoraYFecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                item.Tipo = dr.GetString(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }
            return item;
        }
    }
}
