﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Llamada
{
    public class Guardar : IInsertFactory<Entity.Llamada>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Llamada llamada)
        {
            DbCommand cmd = db.GetStoredProcCommand("Llamada_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddOutParameter(cmd, "_Id", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, llamada.TrackingId);
            db.AddInParameter(cmd, "_Involucrado", DbType.String, llamada.Involucrado);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, llamada.Descripcion);
            db.AddInParameter(cmd, "_Lugar", DbType.String, llamada.Lugar);
            db.AddInParameter(cmd, "_IdColonia", DbType.Int32, llamada.IdColonia);            
            db.AddInParameter(cmd, "_Folio", DbType.String, llamada.Folio);
            db.AddInParameter(cmd, "_HoraYFecha", DbType.DateTime, llamada.HoraYFecha);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, llamada.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, llamada.Habilitado);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, llamada.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, llamada.Tipo);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, llamada.CreadoPor);
            return cmd;
        }
    }
}
