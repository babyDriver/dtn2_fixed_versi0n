﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.Llamada
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Llamada_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, id);
            return cmd;
        }
    }
}
