﻿using System;
using System.Collections.Generic;

namespace DataAccess.Llamada
{
    public interface ILlamada
    {
        List<Entity.Llamada> ObtenerTodos();
        Entity.Llamada ObtenerById(int Id);
        Entity.Llamada ObtenerByTrackingId(Guid Id);
        int Guardar(Entity.Llamada item);
        void Actualizar(Entity.Llamada item);
    }
}
