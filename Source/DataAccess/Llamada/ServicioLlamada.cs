﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Llamada
{
    public class ServicioLlamada : ClsAbstractService<Entity.Llamada>, ILlamada
    {
        public ServicioLlamada(string dataBase)
           : base(dataBase)
        {
        }

        public void Actualizar(Entity.Llamada item)
        {
            IUpdateFactory<Entity.Llamada> update = new Actualizar();
            Update(update, item);
        }

        public int Guardar(Entity.Llamada item)
        {
            IInsertFactory<Entity.Llamada> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.Llamada ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaLlamada(), Id);
        }

        public Entity.Llamada ObtenerByTrackingId(Guid Id)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaLlamada(), Id);
        }

        public List<Entity.Llamada> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaLlamada(), new Entity.NullClass());
        }
    }
}
