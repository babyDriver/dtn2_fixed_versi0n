﻿using System;
using System.Collections.Generic;

namespace DataAccess.ResponsableUnidad
{
    public interface IResponsable
    {
        Entity.Catalogo ObtenerPorUnidadIdEventoId(object[] parametros);
    }
}
