﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.ResponsableUnidad
{
    public class ObtenerByUnidadIdEventoId : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            
            DbCommand cmd = db.GetStoredProcCommand("Responsable_GetByParametros");
            db.AddInParameter(cmd, "_UnidadId", DbType.Int32, Convert.ToInt32(parametros[0]));
            db.AddInParameter(cmd, "_EventoId", DbType.Int32, Convert.ToInt32(parametros[1]));

            return cmd;
        }
    }
}
