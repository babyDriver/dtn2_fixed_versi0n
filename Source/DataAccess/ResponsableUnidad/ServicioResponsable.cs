﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.ResponsableUnidad
{
    public class ServicioResponsable : ClsAbstractService<Entity.Catalogo>, IResponsable
    {
        public ServicioResponsable(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.Catalogo ObtenerPorUnidadIdEventoId(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByUnidadIdEventoId();
            return GetByKey(select, new FabricaResponsable(), parametros);
        }
    }
}
