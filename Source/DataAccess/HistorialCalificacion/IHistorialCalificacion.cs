﻿using System.Collections.Generic;

namespace DataAccess.HistorialCalificacion
{
    public interface IHistorialCalificacion
    {
        int Guardar(Entity.HistorialCalificacion item);
        Entity.HistorialCalificacion ObtenerPorId(int id);
        List<Entity.HistorialCalificacion> ObtenerTodosPorInternoId(int id);
        void Actualizar(Entity.HistorialCalificacion item);
    }
}
