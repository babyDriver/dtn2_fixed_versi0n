﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.HistorialCalificacion
{
    public class ObtenerTodosPorInternoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("HistorialCalificacion_GetAllByInternoId_SP");
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, identity);

            return cmd;
        }
    }
}
