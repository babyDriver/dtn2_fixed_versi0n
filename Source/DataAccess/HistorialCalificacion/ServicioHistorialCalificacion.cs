﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.HistorialCalificacion
{
    public class ServicioHistorialCalificacion : ClsAbstractService<Entity.HistorialCalificacion>, IHistorialCalificacion
    {
        public ServicioHistorialCalificacion(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.HistorialCalificacion item)
        {
            IUpdateFactory<Entity.HistorialCalificacion> update = new Actualizar();
            Update(update, item);
        }

        public int Guardar(Entity.HistorialCalificacion item)
        {
            IInsertFactory<Entity.HistorialCalificacion> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.HistorialCalificacion ObtenerPorId(int id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaHistorialCalificacion(), id);
        }

        public List<Entity.HistorialCalificacion> ObtenerTodosPorInternoId(int id)
        {
            ISelectFactory<int> select = new ObtenerTodosPorInternoId();
            return GetAll(select, new FabricaHistorialCalificacion(), id);
        }
    }
}
