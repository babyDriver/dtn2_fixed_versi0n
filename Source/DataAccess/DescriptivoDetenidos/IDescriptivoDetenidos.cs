﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DescriptivoDetenidos
{
    public interface IDescriptivoDetenidos
    {
        List<Entity.DescriptivoDetenidos> GetDetail(string[] filter);
    }
}