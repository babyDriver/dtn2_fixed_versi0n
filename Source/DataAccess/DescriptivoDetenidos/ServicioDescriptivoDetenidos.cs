﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;
namespace DataAccess.DescriptivoDetenidos
{
    public class ServicioDescriptivoDetenidos : ClsAbstractService<Entity.DescriptivoDetenidos>, IDescriptivoDetenidos
    {
        public ServicioDescriptivoDetenidos(string dataBase)
     : base(dataBase)
        {

        }
        public List<Entity.DescriptivoDetenidos> GetDetail(string[] filter)
        {
            ISelectFactory<string[]> select = new ObtenerDescriptivoDetenidos();
            return GetAll(select, new FabricaDescriptivoDetenidos(), filter);
        }
    }
}