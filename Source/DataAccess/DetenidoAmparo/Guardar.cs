﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.DetenidoAmparo
{
    public class Guardar : IInsertFactory<Entity.DetenidoAmparo>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.DetenidoAmparo item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_Amparo_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, item.DetalleDetencionId);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);



            return cmd;
            
        }
    }
}
