﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DetenidoAmparo
{
    public class FabricaDetenidoAmparo : IEntityFactory<Entity.DetenidoAmparo>
    {
        public Entity.DetenidoAmparo ConstructEntity(IDataReader dr)
        {
            var item = new Entity.DetenidoAmparo();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }
            index = dr.GetOrdinal("DetalleDetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalleDetencionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }
            return item;
                
        }
    }
}
