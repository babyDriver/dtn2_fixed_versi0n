﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.DetenidoAmparo
{
    public class Actualizar : IUpdateFactory<Entity.DetenidoAmparo>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.DetenidoAmparo item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_Amparo_Update_SP");
            
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, item.DetalleDetencionId);
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);



            return cmd;
        }
    }
}
