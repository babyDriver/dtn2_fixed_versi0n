﻿using System;
using System.Collections.Generic;

namespace DataAccess.DetenidoAmparo
{
    public interface IDetenidoAmparo
    {
        int Guardar(Entity.DetenidoAmparo item);
        void Actualizar(Entity.DetenidoAmparo item);
        Entity.DetenidoAmparo ObtenerPorId(int Id);
        Entity.DetenidoAmparo ObtenerPorDetalleDetencionId(int DetalleDetencionId);
        Entity.DetenidoAmparo ObtenerPorTrackingId(Guid TrackingId);
    }
}
