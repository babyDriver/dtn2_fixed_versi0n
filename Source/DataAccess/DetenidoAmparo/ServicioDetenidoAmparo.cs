﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.DetenidoAmparo
{
    public class ServicioDetenidoAmparo : ClsAbstractService<Entity.DetenidoAmparo>, IDetenidoAmparo
    {
        public ServicioDetenidoAmparo(string dataBase)
            : base(dataBase)
        {
        }
        public void Actualizar(Entity.DetenidoAmparo item)
        {
            IUpdateFactory<Entity.DetenidoAmparo> update = new Actualizar();
            Update(update, item);
            
        }

        public int Guardar(Entity.DetenidoAmparo item)
        {
            IInsertFactory<Entity.DetenidoAmparo> insert = new Guardar();
            return Insert(insert, item);
            
        }

        public Entity.DetenidoAmparo ObtenerPorDetalleDetencionId(int DetalleDetencionId)
        {
            ISelectFactory<int> select = new ObtenerPorDetalleDetencionId();
            return GetByKey(select, new FabricaDetenidoAmparo(), DetalleDetencionId);
            
        }

        public Entity.DetenidoAmparo ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaDetenidoAmparo(), Id);          
        }

        public Entity.DetenidoAmparo ObtenerPorTrackingId(Guid TrackingId)
        {
            ISelectFactory<Guid> select = new ObtenerPorTrackingId();
            return GetByKey(select, new FabricaDetenidoAmparo(), TrackingId);
            
        }
    }
}
