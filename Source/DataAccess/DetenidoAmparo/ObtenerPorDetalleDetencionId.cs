﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.DetenidoAmparo
{
    public class ObtenerPorDetalleDetencionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetalleDetencionID)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_Amparo_GetByDetalleDetencionId_SP");
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, DetalleDetencionID);
            return cmd;
        }
    }
}
