﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DataAccess.DetenidoAmparo
{
    public class ObtenerPorId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Detenido_Amparo_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, Id);
            return cmd;
        }
    }
}
