﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.SalidaEfectuada
{
    public interface ISalidaEfectuada
    {
        int Guardar(Entity.SalidaEfectuada salidaEfectuada);
        Entity.SalidaEfectuada ObtenerByDetalleDetencionId(int detalledetencionId);

    }
}
