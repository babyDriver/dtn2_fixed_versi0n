﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.SalidaEfectuada
{
    public class SalidaEfectuadaObtenerByDetalleDetencionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetalleDetencionId)
        {
            DbCommand cmd = db.GetStoredProcCommand("SalidaEfectuada_GetByDetalleDetencionId_SP");
            db.AddInParameter(cmd, "_DetalleDetencionId", DbType.Int32, DetalleDetencionId);
            return cmd;
        }
    }
}
