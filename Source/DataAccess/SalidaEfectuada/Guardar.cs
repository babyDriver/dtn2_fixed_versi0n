﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.SalidaEfectuada
{
    public class Guardar : IInsertFactory<Entity.SalidaEfectuada>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.SalidaEfectuada salidaEfectuada)
        {
            DbCommand cmd = db.GetStoredProcCommand("SalidaEfecuada_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, salidaEfectuada.TrackingId);
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, salidaEfectuada.DetalledetencionId);
            db.AddInParameter(cmd, "_Observacion", DbType.String, salidaEfectuada.Observacion);
            db.AddInParameter(cmd, "_Responsabledeldetenido", DbType.String, salidaEfectuada.Responsabledeldetenido);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, salidaEfectuada.Creadopor);
            db.AddInParameter(cmd, "_Activo", DbType.Int32, salidaEfectuada.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int32, salidaEfectuada.Habilitado);


            return cmd;
        }
    }
}
