﻿using System.Data;
using DataAccess.Interfaces;

namespace DataAccess.SalidaEfectuada
{
    class FabricaSalidaEfectuada : IEntityFactory<Entity.SalidaEfectuada>
    {
        public Entity.SalidaEfectuada ConstructEntity(IDataReader dr)
        {
            var salida = new Entity.SalidaEfectuada();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                salida.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                salida.TrackingId = dr.GetString(index);
            }
             index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                salida.DetalledetencionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Responsabledeldetenido");
            if (!dr.IsDBNull(index))
            {
                salida.Responsabledeldetenido = dr.GetString(index);
            }
            index = dr.GetOrdinal("Observacion");
            if (!dr.IsDBNull(index))
            {
                salida.Observacion = dr.GetString(index);
            }
            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                salida.Habilitado = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                salida.Activo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                salida.Creadopor = dr.GetInt32(index);
            }

            return salida;
        }
    }
}
