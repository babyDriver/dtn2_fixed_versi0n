﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.SalidaEfectuada
{
    public class ServicioSalidaEfectuada : ClsAbstractService<Entity.SalidaEfectuada>, ISalidaEfectuada
    {
        public ServicioSalidaEfectuada(string dataBase)
            : base(dataBase)
        {
        }
        public int Guardar(Entity.SalidaEfectuada salidaEfectuada)
        {
            IInsertFactory<Entity.SalidaEfectuada> insert = new Guardar();
            return Insert(insert, salidaEfectuada);

        }
        
        public Entity.SalidaEfectuada ObtenerByDetalleDetencionId(int detalledetencionId)
        {
            ISelectFactory<int> select = new SalidaEfectuadaObtenerByDetalleDetencionId();
              return GetByKey(select, new FabricaSalidaEfectuada(), detalledetencionId);
        }

        //public Entity.ReporteInformeDetencion GetReporteInformeDetencion(int InternoId)
        //{
        //    ISelectFactory<int> select = new GetReporteInformeDetencionByInternoId();
        //    return GetByKey(select, new FabricaReporteInformeDetencion(), InternoId);
        //}

    }
}
