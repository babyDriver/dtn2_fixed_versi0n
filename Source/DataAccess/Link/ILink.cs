﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Link
{
    public interface ILink
    {
        Entity.Link ObtenerPorSubcontrato(int subcontrato);
    }
}
