﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Link
{
    public class ServicioLink : ClsAbstractService<Entity.Link> , ILink
    {
        public ServicioLink(string dataBase)
            : base(dataBase)
        {
        }
        public Entity.Link ObtenerPorSubcontrato(int subcontrato)
        {
            ISelectFactory<int> select = new ObtenerBySubcontrato();
            return GetByKey(select, new FabricaLink(), subcontrato);
        }
    }
}
