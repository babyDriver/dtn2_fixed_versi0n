﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.Link
{
    internal class ObtenerBySubcontrato : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int subcontrato)
        {
            DbCommand cmd = db.GetStoredProcCommand("Link_GetBySubcontrato_SP");
            db.AddInParameter(cmd, "subcontratoId", DbType.Int32, subcontrato);
            return cmd;
        }
    }
}
