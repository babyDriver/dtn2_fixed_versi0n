﻿
using System;
using System.Data;
using DataAccess.Interfaces;

namespace DataAccess.Link
{
    public class FabricaLink : IEntityFactory<Entity.Link>
    {
        public Entity.Link ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.Link();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Url");
            if (!dr.IsDBNull(index))
            {
                item.Url = dr.GetString(index);
            }

            index = dr.GetOrdinal("SubcontratoId");
            if (!dr.IsDBNull(index))
            {
                item.SubcontratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Leyenda");
            if (!dr.IsDBNull(index))
            {
                item.Leyenda = dr.GetString(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }
            return item;
        }
    }
}
