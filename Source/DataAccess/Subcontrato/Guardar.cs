﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Subcontrato
{
    public class Guardar : IInsertFactory<Entity.Subcontrato>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Subcontrato item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackingId.ToString());
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Subcontrato", DbType.String, item.Nombre);
            db.AddInParameter(cmd, "_VigenciaInicial", DbType.Date, item.VigenciaInicial);
            db.AddInParameter(cmd, "_VigenciaFinal", DbType.Date, item.VigenciaFinal);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_InstitucionId", DbType.Int32, item.InstitucionId);
            db.AddInParameter(cmd, "_Logotipo", DbType.String, item.Logotipo);
            db.AddInParameter(cmd, "_SalarioMinimo", DbType.Decimal, item.SalarioMinimo);
            db.AddInParameter(cmd, "_DivisaId", DbType.Int32, item.DivisaId);
            db.AddInParameter(cmd, "_Latitud", DbType.String, item.Latitud);
            db.AddInParameter(cmd, "_Longitud", DbType.String, item.Longitud);
            db.AddInParameter(cmd, "_Banner", DbType.String, item.Banner);
            return cmd;
        }
    }
}
