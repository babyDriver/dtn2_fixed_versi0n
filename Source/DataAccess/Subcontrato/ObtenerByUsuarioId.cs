﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Subcontrato
{
    public class ObtenerByUsuarioId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int usuarioId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_GetByUsuarioId_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, usuarioId);
            return cmd;
        }
    }
}
