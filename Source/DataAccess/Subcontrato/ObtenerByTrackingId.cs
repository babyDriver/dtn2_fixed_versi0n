﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Subcontrato
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.String, tracking.ToString());
            return cmd;
        }
    }
}
