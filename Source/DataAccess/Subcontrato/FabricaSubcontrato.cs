﻿using DataAccess.Interfaces;

namespace DataAccess.Subcontrato
{
    public class FabricaSubcontrato : IEntityFactory<Entity.Subcontrato>
    {
        public Entity.Subcontrato ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.Subcontrato();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Subcontrato");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("VigenciaInicial");
            if (!dr.IsDBNull(index))
            {
                item.VigenciaInicial = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("VigenciaFinal");
            if (!dr.IsDBNull(index))
            {
                item.VigenciaFinal = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("InstitucionId");
            if (!dr.IsDBNull(index))
            {
                item.InstitucionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Logotipo");
            if (!dr.IsDBNull(index))
            {
                item.Logotipo = dr.GetString(index);
            }
            index = dr.GetOrdinal("Banner");
            if (!dr.IsDBNull(index))
            {
                item.Banner = dr.GetString(index);
            }
            try
            {
                index = dr.GetOrdinal("SalarioMinimo");
                if (!dr.IsDBNull(index))
                {
                    item.SalarioMinimo = dr.GetDecimal(index);
                }
            }
            catch (System.Exception)
            {
                
            }

            index = dr.GetOrdinal("DivisaId");
            if (!dr.IsDBNull(index))
            {
                item.DivisaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Latitud");
            if (!dr.IsDBNull(index))
            {
                item.Latitud = dr.GetString(index);
            }

            index = dr.GetOrdinal("Longitud");
            if (!dr.IsDBNull(index))
            {
                item.Longitud = dr.GetString(index);
            }
            return item;
        }
    }
}
