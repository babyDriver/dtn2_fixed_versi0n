﻿using System;
using System.Collections.Generic;

namespace DataAccess.Subcontrato
{
    public interface ISubcontrato
    {
        int Guardar(Entity.Subcontrato item);
        void Actualizar(Entity.Subcontrato item);
        Entity.Subcontrato ObtenerById(int id);
        Entity.Subcontrato ObtenerByTrackingId(Guid guid);
        List<Entity.Subcontrato> ObtenerByContratoId(int id);
        List<Entity.Subcontrato> ObtenerByUsuarioId(int id);
        List<Entity.Subcontrato> ObtenerTodos();
    }
}
