﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Subcontrato
{
    public class ServicioSubcontrato : ClsAbstractService<Entity.Subcontrato>, ISubcontrato
    {
        public ServicioSubcontrato(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.Subcontrato item)
        {
            IInsertFactory<Entity.Subcontrato> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.Subcontrato ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaSubcontrato(), trackingId);
        }

        public Entity.Subcontrato ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaSubcontrato(), id);
        }

        public void Actualizar(Entity.Subcontrato item)
        {
            IUpdateFactory<Entity.Subcontrato> update = new Actualizar();
            Update(update, item);
        }

        public List<Entity.Subcontrato> ObtenerByContratoId(int id)
        {
            ISelectFactory<int> select = new ObtenerByContratoId();
            return GetAll(select, new FabricaSubcontrato(), id);
        }

        public List<Entity.Subcontrato> ObtenerByUsuarioId(int id)
        {
            ISelectFactory<int> select = new ObtenerByUsuarioId();
            return GetAll(select, new FabricaSubcontrato(), id);
        }

        public List<Entity.Subcontrato> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new Obtenertodos();
            return GetAll(select, new FabricaSubcontrato(), new Entity.NullClass()); ;
        }
    }
}
