﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Subcontrato
{
    public class Actualizar : IUpdateFactory<Entity.Subcontrato>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Subcontrato item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Subcontrato", DbType.String, item.Nombre);
            db.AddInParameter(cmd, "_VigenciaInicial", DbType.Date, item.VigenciaInicial);
            db.AddInParameter(cmd, "_VigenciaFinal", DbType.Date, item.VigenciaFinal);
            db.AddInParameter(cmd, "_InstitucionId", DbType.Int32, item.InstitucionId);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_Logotipo", DbType.String, item.Logotipo);
            db.AddInParameter(cmd, "_SalarioMinimo", DbType.Decimal, item.SalarioMinimo);
            db.AddInParameter(cmd, "_DivisaId", DbType.Int32, item.DivisaId);
            db.AddInParameter(cmd, "_Latitud", DbType.String, item.Latitud);
            db.AddInParameter(cmd, "_Longitud", DbType.String, item.Longitud);
            db.AddInParameter(cmd, "_Banner", DbType.String, item.Banner);
            return cmd;
        }
    }
}
