﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Subcontrato
{
    public class ObtenerByContratoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Subcontrato_GetByContratoId_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, id);
            return cmd;
        }
    }
}
