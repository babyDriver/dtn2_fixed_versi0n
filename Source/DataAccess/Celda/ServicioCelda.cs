﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Celda
{
    public class ServicioCelda : ClsAbstractService<Entity.Celda>, ICelda
    {
        public ServicioCelda(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Celda> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaCelda(), new Entity.NullClass());
        }

        public List<Entity.Celda> ObtenerTodos(DateTime fechaInicial, DateTime fechaFinal)
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaCelda(), new Entity.NullClass(),fechaInicial, fechaFinal);
        }

        public Entity.Celda ObtenerByTrackingId(Guid trackinid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaCelda(), trackinid);
        }

        public Entity.Celda ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaCelda(), Id);
        }

        public Entity.Celda ObtenerByPeligrosidad(int Id)
        {
            ISelectFactory<int> select = new ObtenerByPeligrosidad();
            return GetByKey(select, new FabricaCelda(), Id);
        }

        public int Guardar(Entity.Celda Celda)
        {
            IInsertFactory<Entity.Celda> insert = new Guardar();
            return Insert(insert, Celda);
        }

        public void Actualizar(Entity.Celda Celda)
        {
            IUpdateFactory<Entity.Celda> update = new Actualizar();
            Update(update, Celda);
        }

        public Entity.Celda ObtenerByNombre(string nombre)
        {

            ISelectFactory<string> select = new ObtenerByNombre();
            return GetByKey(select, new FabricaCelda(), nombre);
        }


    }
}