﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Celda
{
    public class Actualizar : IUpdateFactory<Entity.Celda>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Celda celda)
        {
            DbCommand cmd = db.GetStoredProcCommand("Celda_Update_SP");
            db.AddOutParameter(cmd, "_Id", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, celda.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, celda.Nombre);
            db.AddInParameter(cmd, "_Capacidad", DbType.Int32, celda.Capacidad);
            db.AddInParameter(cmd, "_CreadoPor", DbType.String, celda.Creadopor);
            db.AddInParameter(cmd, "_Peligrosidad", DbType.Int32, celda.Nivel_Peligrosidad.Id);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, celda.Habilitado);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, celda.Activo);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, celda.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, celda.Tipo);

            return cmd;
        }
    }
}
