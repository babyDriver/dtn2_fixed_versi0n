﻿using System;
using System.Collections.Generic;

namespace DataAccess.Celda
{
    public interface ICelda
    {
        List<Entity.Celda> ObtenerTodos();
        Entity.Celda ObtenerById(int Id);
        Entity.Celda ObtenerByPeligrosidad(int Id);
        Entity.Celda ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.Celda celda);
        void Actualizar(Entity.Celda celda);
    
    }
}