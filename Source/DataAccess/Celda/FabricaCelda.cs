﻿using DataAccess.Catalogo;
using DataAccess.Interfaces;


namespace DataAccess.Celda
{
    public class FabricaCelda : IEntityFactory<Entity.Celda>
    {
        private static ServicioCatalogo servicioCatalogo = new ServicioCatalogo("SQLConnectionString");

        public Entity.Celda ConstructEntity(System.Data.IDataReader dr)
        {
            var celda = new Entity.Celda();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                celda.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                celda.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Capacidad");
            if (!dr.IsDBNull(index))
            {
                celda.Capacidad = dr.GetInt32(index);
            }
            
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                celda.Activo = dr.GetBoolean(index);
            }
            
            index = dr.GetOrdinal("Nivel_Peligrosidad");
            if (!dr.IsDBNull(index))
            {
                var parametros = new object[2];
                parametros[0] = dr.GetInt32(index);
                parametros[1] = Entity.TipoDeCatalogo.nivel_peligrosidad;
                var nivel = servicioCatalogo.ObtenerPorId(parametros);
                if (nivel != null)
                    celda.Nivel_Peligrosidad = nivel;
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                celda.TrackingId = dr.GetGuid(index);
            }            

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                celda.Creadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                celda.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                celda.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                celda.Tipo = dr.GetString(index);
            }

            return celda;
        }
    }
}