﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Celda
{
    public class ObtenerByPeligrosidad : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Celda_GetByPeligrosidad_SP");
            db.AddInParameter(cmd, "_Peligrosidad", DbType.Int32, id);
            return cmd;
        }
    }
}
