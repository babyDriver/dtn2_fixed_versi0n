﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.CertificadoLesion
{
    public class ServicioCertificadolesion : ClsAbstractService<Entity.CertificadoLesion>, ICertificadoLesion
    {
        public ServicioCertificadolesion(string dataBase)
          : base(dataBase)
        {
        }

        public void Actualizar(Entity.CertificadoLesion certificadolesion)
        {
            IUpdateFactory<Entity.CertificadoLesion> update = new Actualizar();
            Update(update, certificadolesion);
        }

        public int Guardar(Entity.CertificadoLesion certificadolesion)
        {
            IInsertFactory<Entity.CertificadoLesion> insert = new Guardar();
            return Insert(insert, certificadolesion);
        }

        public Entity.CertificadoLesion ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoLesion(), Id);
        }

        public Entity.CertificadoLesion ObtenerByTrackingId(string trackingid)
        {
            ISelectFactory<string> select = new ObtenerPorTrackingId();
            return GetByKey(select, new FabricaCertificadoLesion(), trackingid);
        }

        public List<Entity.CertificadoLesion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaCertificadoLesion(), new Entity.NullClass());
        }
    }
}
