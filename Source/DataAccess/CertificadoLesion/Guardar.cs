﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoLesion
{
    public class Guardar : IInsertFactory<Entity.CertificadoLesion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CertificadoLesion certificadoLesion)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_Lesion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, certificadoLesion.TrackingId);
            //db.AddInParameter(cmd, "_Aptitud", DbType.Int16, certificadoLesion.Aptitud);
            db.AddInParameter(cmd, "_Tipo_lesionId", DbType.Int32, certificadoLesion.Tipo_lesionId);
            db.AddInParameter(cmd, "_Observaciones_lesion", DbType.String, certificadoLesion.Observaciones_lesion);
            db.AddInParameter(cmd, "_Lesion_simple_vista", DbType.Int16, certificadoLesion.Lesion_simple_vista);
            db.AddInParameter(cmd, "_Fallecimiento", DbType.Int16, certificadoLesion.Fallecimiento);
            db.AddInParameter(cmd, "_Envio_hospital", DbType.Int16, certificadoLesion.Envio_hospital);
            db.AddInParameter(cmd, "_Peligro_de_vida", DbType.Int16, certificadoLesion.Peligro_de_vida);
            db.AddInParameter(cmd, "_Consecuencias_medico_legales", DbType.Int16, certificadoLesion.Consecuencias_medico_legales);
            db.AddInParameter(cmd, "_Tipo_alergiaId", DbType.Int32, certificadoLesion.Tipo_alergiaId);
            db.AddInParameter(cmd, "_Observacion_alergia", DbType.String, certificadoLesion.Observacion_alergia);
            db.AddInParameter(cmd, "_Tipo_tatuajeId", DbType.Int32, certificadoLesion.Tipo_tatuajeId);
            db.AddInParameter(cmd, "_Observacion_tatuje", DbType.String, certificadoLesion.Observacion_tatuje);
            db.AddInParameter(cmd, "_Observacion_general", DbType.String, certificadoLesion.Observacion_general);
            db.AddInParameter(cmd, "_AntecedentesId", DbType.Int32, certificadoLesion.AntecedentesId);
            db.AddInParameter(cmd, "_Observaciones_antecedentes", DbType.String, certificadoLesion.Observaciones_antecedentes);
            db.AddInParameter(cmd, "_FechaRegistro", DbType.DateTime, certificadoLesion.FechaRegistro);
            db.AddInParameter(cmd, "_Registradopor", DbType.Int32, certificadoLesion.Registradopor);
            db.AddInParameter(cmd, "_Modificadopor", DbType.Int32, certificadoLesion.Modificadopor);
            db.AddInParameter(cmd, "_Fechaultimamodificacion", DbType.DateTime, certificadoLesion.Fechaultimamodificacion);
            db.AddInParameter(cmd, "_Sinlesion", DbType.Int16, certificadoLesion.Sinlesion);
            db.AddInParameter(cmd, "_FotografiasId", DbType.Int32, certificadoLesion.FotografiasId);
            db.AddInParameter(cmd, "_Fechavaloracion", DbType.DateTime, certificadoLesion.Fechavaloracion);
            return cmd;
        }
    }
}
