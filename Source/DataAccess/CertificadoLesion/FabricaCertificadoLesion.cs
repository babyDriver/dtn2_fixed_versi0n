﻿using System.Data;
using DataAccess.Interfaces;
using Entity;
namespace DataAccess.CertificadoLesion
{
    public class FabricaCertificadoLesion : IEntityFactory<Entity.CertificadoLesion>
    {
        public Entity.CertificadoLesion ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoLesion();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetString(index);
            }
            //index = dr.GetOrdinal("Aptitud");
            //if (!dr.IsDBNull(index))
            //{
            //    item.Aptitud = dr.GetInt16(index);
            //}
            index = dr.GetOrdinal("Tipo_lesionId");
            if (!dr.IsDBNull(index))
            {
                item.Tipo_lesionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Observaciones_lesion");
            if (!dr.IsDBNull(index))
            {
                item.Observaciones_lesion = dr.GetString(index);
            }
            index = dr.GetOrdinal("Lesion_simple_vista");
            if (!dr.IsDBNull(index))
            {
                item.Lesion_simple_vista = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Fallecimiento");
            if (!dr.IsDBNull(index))
            {
                item.Fallecimiento = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Envio_hospital");
            if (!dr.IsDBNull(index))
            {
                item.Envio_hospital = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Peligro_de_vida");
            if (!dr.IsDBNull(index))
            {
                item.Peligro_de_vida = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Consecuencias_medico_legales");
            if (!dr.IsDBNull(index))
            {
                item.Consecuencias_medico_legales = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Tipo_alergiaId");
            if (!dr.IsDBNull(index))
            {
                item.Tipo_alergiaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Observacion_alergia");
            if (!dr.IsDBNull(index))
            {
                item.Observacion_alergia = dr.GetString(index);
            }
            index = dr.GetOrdinal("Tipo_tatuajeId");
            if (!dr.IsDBNull(index))
            {
                item.Tipo_tatuajeId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Observacion_tatuje");
            if (!dr.IsDBNull(index))
            {
                item.Observacion_tatuje = dr.GetString(index);
            }
            index = dr.GetOrdinal("Observacion_general");
            if (!dr.IsDBNull(index))
            {
                item.Observacion_general = dr.GetString(index);
            }
            index = dr.GetOrdinal("AntecedentesId");
            if (!dr.IsDBNull(index))
            {
                item.AntecedentesId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Observaciones_antecedentes");
            if (!dr.IsDBNull(index))
            {
                item.Observaciones_antecedentes = dr.GetString(index);
            }
            index = dr.GetOrdinal("FechaRegistro");
            if (!dr.IsDBNull(index))
            {
                item.FechaRegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Registradopor");
            if (!dr.IsDBNull(index))
            {
                item.Registradopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Modificadopor");
            if (!dr.IsDBNull(index))
            {
                item.Modificadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fechaultimamodificacion");
            if (!dr.IsDBNull(index))
            {
                item.Fechaultimamodificacion = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Sinlesion");
            if (!dr.IsDBNull(index))
            {
                item.Sinlesion = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("FotografiasId");
            if (!dr.IsDBNull(index))
            {
                item.FotografiasId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fechavaloracion");
            if (!dr.IsDBNull(index))
            {
                item.Fechavaloracion = dr.GetDateTime(index);
            }

            return item;
        }
    }
}
