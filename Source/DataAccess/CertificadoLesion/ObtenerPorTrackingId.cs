﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoLesion
{
    class ObtenerPorTrackingId : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_Lesion_ObtenerPorTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.String, tracking);
            return cmd;
        }
    }
}
