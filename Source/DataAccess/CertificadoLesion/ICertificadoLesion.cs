﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesion
{
    public interface ICertificadoLesion
    {
        List<Entity.CertificadoLesion> ObtenerTodos();
        Entity.CertificadoLesion ObtenerById(int Id);

        Entity.CertificadoLesion ObtenerByTrackingId(string trackingid);
        int Guardar(Entity.CertificadoLesion certificadolesion);
        void Actualizar(Entity.CertificadoLesion certificadolesion);
    }
}
