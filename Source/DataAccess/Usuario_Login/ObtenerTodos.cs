﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Entity;

namespace DataAccess.Usuario_Login
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("usuarios_login_GetAll_SP");
            return cmd;
        }
    }
}
