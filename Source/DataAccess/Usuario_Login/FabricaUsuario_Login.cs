﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.Usuario_Login
{
    public class FabricaUsuario_Login : IEntityFactory<Entity.Usuario_login>
    {
        public Usuario_login ConstructEntity(IDataReader dr)
        {
            var up = new Entity.Usuario_login();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                up.Id = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("UsuarioId");
            if (!dr.IsDBNull(index))
            {
                up.UsuarioId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IPequipo");
            if (!dr.IsDBNull(index))
            {
                up.IPequipo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fechahora");
            if (!dr.IsDBNull(index))
            {
                up.Fechahora = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                up.Activo = dr.GetBoolean(index);
            }

            return up;
        }
    }
}
