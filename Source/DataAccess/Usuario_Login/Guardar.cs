﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.Usuario_Login
{
    public class Guardar : IInsertFactory<Entity.Usuario_login>
    {
        public DbCommand ConstructInsertCommand(Database db, Usuario_login usuario_Login)
        {
            DbCommand cmd = db.GetStoredProcCommand("usuarios_login_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, usuario_Login.UsuarioId);
            db.AddInParameter(cmd, "_IPequipo", DbType.String, usuario_Login.IPequipo);
            db.AddInParameter(cmd, "_Fechahora", DbType.DateTime, usuario_Login.Fechahora);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, usuario_Login.Activo);
            return cmd;
        }
    }
}
