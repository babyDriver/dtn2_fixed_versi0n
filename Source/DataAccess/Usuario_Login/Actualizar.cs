﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.Usuario_Login
{
    public class Actualizar : IUpdateFactory<Usuario_login>
    {
        public DbCommand ConstructUpdateCommand(Database db, Usuario_login usuario_Login)
        {
            DbCommand cmd = db.GetStoredProcCommand("usuarios_login_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, usuario_Login.Id);
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, usuario_Login.UsuarioId);
            db.AddInParameter(cmd, "_IPequipo", DbType.String, usuario_Login.IPequipo);
            db.AddInParameter(cmd, "_Fechahora", DbType.DateTime, usuario_Login.Fechahora);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, usuario_Login.Activo);
            return cmd;
        }
    }
}
