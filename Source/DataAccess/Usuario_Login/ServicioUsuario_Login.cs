﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Usuario_Login
{
    public class ServicioUsuario_Login : ClsAbstractService<Entity.Usuario_login>, Iusuario_Login
    {
        public ServicioUsuario_Login(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Usuario_login usuario_Login)
        {
            IUpdateFactory<Entity.Usuario_login> update = new Actualizar();
            Update(update, usuario_Login);
        }

        public int Guardar(Usuario_login  userlogim)
        {
            IInsertFactory<Entity.Usuario_login> insert = new Guardar();
            return Insert(insert, userlogim);
        }

        public Usuario_login ObtenerPorUsuarioId(int UsuarioId)
        {
            ISelectFactory<int> select = new ObtenerPorusuarioId();
            return GetByKey(select, new FabricaUsuario_Login(), UsuarioId);
        }

        public List<Usuario_login> ObtenerTodos()
        {
            ISelectFactory<NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaUsuario_Login(), new NullClass());
        }
    }
}
