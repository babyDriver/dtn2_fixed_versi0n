﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Usuario_Login
{
    public interface Iusuario_Login
    {
        int Guardar(Entity.Usuario_login usuario_Login);
        Entity.Usuario_login ObtenerPorUsuarioId(int UsuarioId);
        void Actualizar(Entity.Usuario_login usuario_Login);
        List<Entity.Usuario_login> ObtenerTodos();
    }
}
