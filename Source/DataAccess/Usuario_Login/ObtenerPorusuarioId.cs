﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Usuario_Login
{
    public class ObtenerPorusuarioId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("usuarios_login_GetByUsuarioId_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, id);
            return cmd;
        }
    }
}
