﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.UsuarioPermiso
{
    public class Guardar : IInsertFactory<Entity.UsuarioPermiso>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.UsuarioPermiso permiso)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_permiso_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, permiso.UsuarioId);
            db.AddInParameter(cmd, "_PermisoId", DbType.Int32, permiso.PermisoId);
            db.AddInParameter(cmd, "_PantallaId", DbType.Int32, permiso.PantallaId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, permiso.Habilitado);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, permiso.Activo);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, permiso.TrackingId);
        
            return cmd;
        }
    }
}
