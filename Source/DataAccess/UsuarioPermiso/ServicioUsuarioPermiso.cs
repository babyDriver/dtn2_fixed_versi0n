﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.UsuarioPermiso
{
    public class ServicioUsuarioPermiso : ClsAbstractService<Entity.UsuarioPermiso>, IUsuarioPermiso
    {
        public ServicioUsuarioPermiso(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.UsuarioPermiso> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaUsuarioPermiso(), new Entity.NullClass());
        }

        public List<Entity.UsuarioPermiso> ObtenerByUsuarioId(int usuarioid)
        {
            ISelectFactory<int> select = new ObtenerByUsuarioId();
            return GetAll(select, new FabricaUsuarioPermiso(), usuarioid);
        }

        public Entity.UsuarioPermiso ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaUsuarioPermiso(), trackingId);
        }

        public Entity.UsuarioPermiso ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaUsuarioPermiso(), Id);
        }

        public Entity.UsuarioPermiso ObtenerByParametros(Entity.UsuarioPermiso parametros)
        {
            ISelectFactory<Entity.UsuarioPermiso> select = new ObtenerByParametros();
            return GetByKey(select, new FabricaUsuarioPermiso(), parametros);
        }

        public int Guardar(Entity.UsuarioPermiso usuario)
        {
            IInsertFactory<Entity.UsuarioPermiso> insert = new Guardar();
            return Insert(insert, usuario);
        }

        public void Actualizar(Entity.UsuarioPermiso usuario)
        {
            IUpdateFactory<Entity.UsuarioPermiso> update = new Actualizar();
            Update(update, usuario);
        }

        public void EliminarByUsuarioId(int usuarioid)
        {
            IDeleteFactory<int> delete = new DeleteByUsuarioId();
            Delete(delete, usuarioid);
        }

        public void ActualizarHabilitadoByUserioId(Entity.UsuarioPermiso usuario)
        {
            IUpdateFactory<Entity.UsuarioPermiso> update = new ActualizarHabilitadoByUserioId();
            Update(update, usuario);
        }
    }
}
