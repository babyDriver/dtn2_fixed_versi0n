﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioPermiso
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_permiso_GetByTrackingId_SP");
            db.AddInParameter(cmd, "TrackingId", DbType.Guid, tracking);
            return cmd;
        }
    }
}
