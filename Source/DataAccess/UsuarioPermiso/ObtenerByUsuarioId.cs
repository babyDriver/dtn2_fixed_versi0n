﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.UsuarioPermiso
{
    public class ObtenerByUsuarioId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int usuarioid)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_permiso_GetByUsuarioId_SP");
            db.AddInParameter(cmd, "UsuarioId", DbType.Int32, usuarioid);
            return cmd;
        }
    }
}
