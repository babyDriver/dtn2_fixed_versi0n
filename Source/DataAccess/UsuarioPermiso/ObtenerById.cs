﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioPermiso
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_permiso_GetById_SP");
            db.AddInParameter(cmd, "Id", DbType.Int32, id);
            return cmd;
        }
    }
}
