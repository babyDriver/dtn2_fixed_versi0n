﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioPermiso
{
    public class ActualizarHabilitadoByUserioId : IUpdateFactory<Entity.UsuarioPermiso>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.UsuarioPermiso permiso)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_permiso_UpdateHabilitadoByUsuarioId_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, permiso.UsuarioId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, permiso.Habilitado);
            return cmd;
        }
    }
}
