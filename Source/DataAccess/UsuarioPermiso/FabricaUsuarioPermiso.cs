﻿using DataAccess.Interfaces;


namespace DataAccess.UsuarioPermiso
{
    public class FabricaUsuarioPermiso : IEntityFactory<Entity.UsuarioPermiso>
    {
        public Entity.UsuarioPermiso ConstructEntity(System.Data.IDataReader dr)
        {
            var permiso = new Entity.UsuarioPermiso();

            var index = dr.GetOrdinal("UsuarioId");
            if (!dr.IsDBNull(index))
            {
                permiso.UsuarioId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("PermisoId");
            if (!dr.IsDBNull(index))
            {
                permiso.PermisoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                permiso.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                permiso.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("PantallaId");
            if (!dr.IsDBNull(index))
            {
                permiso.PantallaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                permiso.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                permiso.TrackingId = dr.GetGuid(index);
            }



            return permiso;
        }
    }
}
