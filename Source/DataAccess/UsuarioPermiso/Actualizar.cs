﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioPermiso
{
    public class Actualizar : IUpdateFactory<Entity.UsuarioPermiso>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.UsuarioPermiso permiso)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_permiso_Update_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, permiso.UsuarioId);
            db.AddInParameter(cmd, "_PermisoId", DbType.Int32, permiso.PermisoId);
            db.AddInParameter(cmd, "_PantallaId", DbType.String, permiso.PantallaId);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, permiso.Habilitado);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, permiso.Activo);
            db.AddInParameter(cmd, "_Id", DbType.Int32, permiso.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, permiso.TrackingId);


            return cmd;
        }
    }
}
