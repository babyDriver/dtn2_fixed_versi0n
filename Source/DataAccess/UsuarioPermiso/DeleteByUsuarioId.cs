﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.UsuarioPermiso
{
    public class DeleteByUsuarioId : IDeleteFactory<int>
    {
        public DbCommand ConstructDeleteCommand(Database db, int usuarioid)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_permiso_DeleteByUsuarioId_SP");
            db.AddInParameter(cmd, "UsuarioId", DbType.Int32, usuarioid);
            return cmd;
        }
    }
}
