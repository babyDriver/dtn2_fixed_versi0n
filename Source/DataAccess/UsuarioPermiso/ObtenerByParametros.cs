﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.UsuarioPermiso
{
    public class ObtenerByParametros : ISelectFactory<Entity.UsuarioPermiso>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.UsuarioPermiso parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Usuario_permiso_GetByParametros_SP");
            db.AddInParameter(cmd, "UsuarioId", DbType.Int32, parametros.UsuarioId);
            db.AddInParameter(cmd, "PantallaId", DbType.Int32, parametros.PantallaId);
            db.AddInParameter(cmd, "PermisoId", DbType.Int32, parametros.PermisoId);
            return cmd;
        }
    }
}
