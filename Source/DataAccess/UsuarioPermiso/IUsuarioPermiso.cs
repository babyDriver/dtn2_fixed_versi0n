﻿using System;
using System.Collections.Generic;

namespace DataAccess.UsuarioPermiso
{
    interface IUsuarioPermiso
    {
        List<Entity.UsuarioPermiso> ObtenerByUsuarioId(int usuarioId);
        Entity.UsuarioPermiso ObtenerById(int id);
        Entity.UsuarioPermiso ObtenerByParametros(Entity.UsuarioPermiso parametros);
        Entity.UsuarioPermiso ObtenerByTrackingId(Guid trackingId);
        List<Entity.UsuarioPermiso> ObtenerTodos();
        int Guardar(Entity.UsuarioPermiso permiso);
        void Actualizar(Entity.UsuarioPermiso permiso);
        void EliminarByUsuarioId(int usuarioId);
        void ActualizarHabilitadoByUserioId(Entity.UsuarioPermiso permiso);
    }
}
