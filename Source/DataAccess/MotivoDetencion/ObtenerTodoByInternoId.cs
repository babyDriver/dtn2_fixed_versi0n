﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.MotivoDetencion
{
    public class ObtenerTodoByInternoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencion_GetAllByInternoId_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, id);
            return cmd;
        }
    }
}
