﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.MotivoDetencion
{
   public interface IMotivoDetencion
    {

        int Guardar(Entity.MotivoDetencion motivoDetencion);

        void Actualizar(Entity.MotivoDetencion motivoDetencion);

        List<Entity.MotivoDetencion> ObtenerTodos();

        Entity.MotivoDetencion ObtenerById(int Id);

        Entity.MotivoDetencion ObtenerByTrackingId(Guid trackingid);
        List<Entity.MotivoDetencion> ObtenerTodoByInternoId(int id);
        List<Entity.MotivoDetencion> ObtenerTodosContratoUsuario(int Id);

    }
}
