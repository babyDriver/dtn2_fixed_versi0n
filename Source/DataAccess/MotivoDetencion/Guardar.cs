﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.MotivoDetencion
{
    class Guardar : IInsertFactory<Entity.MotivoDetencion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.MotivoDetencion motivoDetencion)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencion_Insert_SP");

            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddOutParameter(cmd, "_Id", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, motivoDetencion.TrackingId);
            db.AddInParameter(cmd, "_Motivo", DbType.String, motivoDetencion.Motivo);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, motivoDetencion.Descripcion);
            db.AddInParameter(cmd, "_Articulo", DbType.String, motivoDetencion.Articulo);
            db.AddInParameter(cmd, "_MultaMinimo", DbType.Decimal, motivoDetencion.MultaMinimo);
            db.AddInParameter(cmd, "_MultaMaximo", DbType.Decimal, motivoDetencion.MultaMaximo);
            db.AddInParameter(cmd, "_HorasdeArresto", DbType.Int32, motivoDetencion.HorasdeArresto);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, motivoDetencion.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, motivoDetencion.Habilitado);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, motivoDetencion.CreadoPor);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, motivoDetencion.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, motivoDetencion.Tipo);
            db.AddInParameter(cmd, "_Tipo_Motivo", DbType.Int32, motivoDetencion.Tipo_Motivo);

            return cmd;
        }
    }
}
