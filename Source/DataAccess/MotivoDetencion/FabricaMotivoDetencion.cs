﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.MotivoDetencion
{
    class FabricaMotivoDetencion : IEntityFactory<Entity.MotivoDetencion>
    {
        public Entity.MotivoDetencion ConstructEntity(System.Data.IDataReader dr)
        {
            var contrato = new Entity.MotivoDetencion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                contrato.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                contrato.TrackingId = dr.GetGuid(index);
            }

          

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                contrato.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                contrato.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Articulo");
            if (!dr.IsDBNull(index))
            {
                contrato.Articulo = dr.GetString(index);
            }

            index = dr.GetOrdinal("MultaMinimo");
            if (!dr.IsDBNull(index))
            {
                contrato.MultaMinimo = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("MultaMaximo");
            if (!dr.IsDBNull(index))
            {
                contrato.MultaMaximo = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                contrato.Activo = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("HorasdeArresto");
            if (!dr.IsDBNull(index))
            {
                contrato.HorasdeArresto = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                contrato.Habilitado = dr.GetInt16(index);
            }

     

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                contrato.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                contrato.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                contrato.Tipo = dr.GetString(index);
            }
            index = dr.GetOrdinal("Tipo_Motivo");
                if (!dr.IsDBNull(index))
            {

                contrato.Tipo_Motivo = dr.GetInt32(index);
            }


                return contrato;
        }

      
    }
}