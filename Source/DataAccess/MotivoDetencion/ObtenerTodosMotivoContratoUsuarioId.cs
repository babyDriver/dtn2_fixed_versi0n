﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.MotivoDetencion
{
    public class ObtenerTodosMotivoContratoUsuarioId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int UsuarioId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Motivo_GetAllContratoUsuario_SP");
            db.AddInParameter(cmd, "_UsuarioId", DbType.Int32, UsuarioId);
            return cmd;
        }
    }
}
