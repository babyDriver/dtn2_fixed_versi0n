﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.MotivoDetencion
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencion_GetAll_SP");
            return cmd;
        }
    }
}
