﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;


namespace DataAccess.MotivoDetencion
{
    public class obtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int motivoDetencionId)
        {
            DbCommand cmd = db.GetStoredProcCommand("MotivoDetencion_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, motivoDetencionId);
            return cmd;
        }
    }

}

