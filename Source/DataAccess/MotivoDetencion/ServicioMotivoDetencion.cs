﻿using DataAccess.Catalogo;
using DataAccess.Interfaces;
using DataAccess.Motivo;
using Entity;
using System;
using System.Collections.Generic;
namespace DataAccess.MotivoDetencion
{
    public class ServicioMotivoDetencion : ClsAbstractService<Entity.MotivoDetencion>, IMotivoDetencion
    {
        public ServicioMotivoDetencion(string dataBase)
            : base(dataBase)
        {
        }
        public void Actualizar(Entity.MotivoDetencion motivoDetencion)
        {
            IUpdateFactory<Entity.MotivoDetencion> update = new Actualizar();
            Update(update, motivoDetencion);
        }

        public int Guardar(Entity.MotivoDetencion motivoDetencion)
        {
            IInsertFactory<Entity.MotivoDetencion> insert = new Guardar();
            return Insert(insert, motivoDetencion);
        }

        public Entity.MotivoDetencion ObtenerById(int Id)
        {
            ISelectFactory<int> select = new obtenerById();
            return GetByKey(select, new FabricaMotivoDetencion(), Id);
        }

        public Entity.MotivoDetencion ObtenerByTrackingId(Guid trackingid)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaMotivoDetencion(), trackingid);
        }

        public List<Entity.MotivoDetencion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaMotivoDetencion(), new Entity.NullClass());
        }
        public List<Entity.MotivoDetencion> ObtenerHabilitados(int tipoCatalogo)
        {
            ISelectFactory<int> select = new ObtenerHabilitados();
            return GetAll(select, new FabricaMotivoDetencion(), tipoCatalogo);
        }

        public List<Entity.MotivoDetencion> ObtenerTodoByInternoId(int id)
        {
            ISelectFactory<int> select = new ObtenerTodoByInternoId();
            return GetAll(select, new FabricaMotivoDetencion(), id);
        }

        public List<Entity.MotivoDetencion> ObtenerTodosContratoUsuario(int Id)
        {
            ISelectFactory<int> select = new ObtenerTodosMotivoContratoUsuarioId();
            return GetAll(select, new FabricaMotivoDetencion(), Id);
        }
    }
}

