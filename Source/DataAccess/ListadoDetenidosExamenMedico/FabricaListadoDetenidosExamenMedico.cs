﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoDetenidosExamenMedico
{
    public class FabricaListadoDetenidosExamenMedico : IEntityFactory<Entity.ListadoDetenidosExamenMedico>
    {
        public Entity.ListadoDetenidosExamenMedico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoDetenidosExamenMedico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("NCP");
            if (!dr.IsDBNull(index))
            {
                item.NCP = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                item.Estatus = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingIdEstatus");
            if (!dr.IsDBNull(index))
            {
                item.TrackingIdEstatus = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("RutaImagen");
            if (!dr.IsDBNull(index))
            {
                item.RutaImagen = dr.GetString(index);
            }

            index = dr.GetOrdinal("AliasInterno");
            if (!dr.IsDBNull(index))
            {
                item.AliasInterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaNacimiento");
            if (!dr.IsDBNull(index))
            {
                item.FechaNacimiento = dr.GetDateTime(index);
            }

            item.NombreCompleto = item.Nombre + " " + item.Paterno + " " + item.Materno;

            return item;
        }
    }
}
