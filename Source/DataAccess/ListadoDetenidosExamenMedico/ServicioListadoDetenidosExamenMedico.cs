﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoDetenidosExamenMedico
{
    public class ServicioListadoDetenidosExamenMedico : ClsAbstractService<Entity.ListadoDetenidosExamenMedico>, IListadoDetenidosExamenMedico
    {
        public ServicioListadoDetenidosExamenMedico(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoDetenidosExamenMedico> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoDetenidosExamenMedico(), parametros);
        }
    }
}
