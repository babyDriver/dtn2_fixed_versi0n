﻿using System.Collections.Generic;

namespace DataAccess.ListadoDetenidosExamenMedico
{
    public interface IListadoDetenidosExamenMedico
    {
        List<Entity.ListadoDetenidosExamenMedico> ObtenerTodos(object[] data);
    }
}
