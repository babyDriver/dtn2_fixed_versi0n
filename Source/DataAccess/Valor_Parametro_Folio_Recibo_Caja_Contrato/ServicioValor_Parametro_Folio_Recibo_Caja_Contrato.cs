﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.Valor_Parametro_Folio_Recibo_Caja_Contrato
{
    public class ServicioValor_Parametro_Folio_Recibo_Caja_Contrato : ClsAbstractService<Entity.Valor_Parametro_Folio_Recibo_Caja_Contrato>, IValor_Parametro_Folio_Recibo_Caja_Contrato
    {
        public ServicioValor_Parametro_Folio_Recibo_Caja_Contrato(string dataBase)
            : base(dataBase)
        {
        }
        public Entity.Valor_Parametro_Folio_Recibo_Caja_Contrato GetValorparametrofoliocaja(int ContratoId)
        {
            ISelectFactory<int> select = new GetValorparametrofoliocaja ();
            return GetByKey(select, new FabricaValor_Parametro_Folio_Recibo_Caja_Contrato(), ContratoId);
        }

    }
}
