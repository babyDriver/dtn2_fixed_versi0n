﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Valor_Parametro_Folio_Recibo_Caja_Contrato
{
    public class GetValorparametrofoliocaja : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int ContratoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("Valor_Parametro_Folio_recibo_Caja_Contrato_GetByContratoId");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, ContratoId);
            return cmd;
            
        }
    }
}
