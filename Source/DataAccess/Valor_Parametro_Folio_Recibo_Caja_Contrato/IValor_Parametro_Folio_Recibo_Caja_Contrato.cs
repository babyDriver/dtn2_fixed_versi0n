﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Valor_Parametro_Folio_Recibo_Caja_Contrato
{
   public interface IValor_Parametro_Folio_Recibo_Caja_Contrato
    {
        Entity.Valor_Parametro_Folio_Recibo_Caja_Contrato GetValorparametrofoliocaja(int ContratoId);
    }
}
