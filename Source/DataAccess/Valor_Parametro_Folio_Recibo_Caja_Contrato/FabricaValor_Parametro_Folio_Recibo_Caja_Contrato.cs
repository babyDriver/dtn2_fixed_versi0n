﻿using DataAccess.Interfaces;
using Entity;
using System.Data;
namespace DataAccess.Valor_Parametro_Folio_Recibo_Caja_Contrato
{
    public class FabricaValor_Parametro_Folio_Recibo_Caja_Contrato : IEntityFactory<Entity.Valor_Parametro_Folio_Recibo_Caja_Contrato>
    {
        public Entity.Valor_Parametro_Folio_Recibo_Caja_Contrato ConstructEntity(IDataReader dr)
        {
            var val = new Entity.Valor_Parametro_Folio_Recibo_Caja_Contrato();
          var  index = dr.GetOrdinal("Valor");
            if (!dr.IsDBNull(index))
            {
                val.Valor = dr.GetString(index);
            }
            return val;
        }
    }
}
