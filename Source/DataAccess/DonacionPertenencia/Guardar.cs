﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.DonacionPertenencia
{
    public class Guardar : IInsertFactory<Entity.DonacionPertenencia>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.DonacionPertenencia donacion)
        {
            DbCommand cmd = db.GetStoredProcCommand("DonacionPertenencia_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, donacion.TrackingId);
            db.AddInParameter(cmd, "_PertenenciasDe", DbType.String, donacion.PertenenciasDe);
            db.AddInParameter(cmd, "_UsuarioQueRegistro", DbType.String, donacion.UsuarioQueRegistro);
            db.AddInParameter(cmd, "_PersonaQueRecibe", DbType.String, donacion.PersonaQueRecibe);
            db.AddInParameter(cmd, "_Condicion", DbType.String, donacion.Condicion);
            db.AddInParameter(cmd, "_Entrega", DbType.Int32, donacion.Entrega);
            db.AddInParameter(cmd, "_Accion", DbType.String, donacion.Accion);
            db.AddInParameter(cmd, "_FechaEntrega", DbType.DateTime, donacion.FechaEntrega);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, donacion.CreadoPor);
            db.AddInParameter(cmd, "_InstitucionId", DbType.Int32, donacion.InstitucionId);

            return cmd;
        }
    }
}
