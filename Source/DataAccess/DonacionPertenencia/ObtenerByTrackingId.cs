﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.DonacionPertenencia
{
    public class ObtenerByTrackingId : ISelectFactory<Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, Guid tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("DonacionPertenencia_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, tracking);
            return cmd;
        }
    }
}
