﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.DonacionPertenencia
{
    public class ServicioDonacionPertenencia : ClsAbstractService<Entity.DonacionPertenencia>, IDonacionPertenencia
    {
        public ServicioDonacionPertenencia(string dataBase)
           : base(dataBase)
        {
        }

        public int Guardar(Entity.DonacionPertenencia donacion)
        {
            IInsertFactory<Entity.DonacionPertenencia> insert = new Guardar();
            return Insert(insert, donacion);
        }

        public Entity.DonacionPertenencia ObtenerByTrackingId(Guid tracking)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaDonacionPertenencia(), tracking);
        }
    }
}
