﻿using System;
using System.Collections.Generic;

namespace DataAccess.DonacionPertenencia
{
    public interface IDonacionPertenencia
    {
        int Guardar(Entity.DonacionPertenencia donacion);
        Entity.DonacionPertenencia ObtenerByTrackingId(Guid guid);
    }
}
