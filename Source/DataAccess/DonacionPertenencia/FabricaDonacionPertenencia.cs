﻿using DataAccess.Interfaces;

namespace DataAccess.DonacionPertenencia
{
    public class FabricaDonacionPertenencia : IEntityFactory<Entity.DonacionPertenencia>
    {
        public Entity.DonacionPertenencia ConstructEntity(System.Data.IDataReader dr)
        {
            var donacion = new Entity.DonacionPertenencia();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                donacion.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                donacion.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("PertenenciasDe");
            if (!dr.IsDBNull(index))
            {
                donacion.PertenenciasDe = dr.GetString(index);
            }

            index = dr.GetOrdinal("UsuarioQueRegistro");
            if (!dr.IsDBNull(index))
            {
                donacion.UsuarioQueRegistro = dr.GetString(index);
            }

            index = dr.GetOrdinal("PersonaQueRecibe");
            if (!dr.IsDBNull(index))
            {
                donacion.PersonaQueRecibe = dr.GetString(index);
            }

            index = dr.GetOrdinal("Condicion");
            if (!dr.IsDBNull(index))
            {
                donacion.Condicion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Entrega");
            if (!dr.IsDBNull(index))
            {
                donacion.Entrega = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaEntrega");
            if (!dr.IsDBNull(index))
            {
                donacion.FechaEntrega = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                donacion.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                donacion.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                donacion.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Accion");
            if (!dr.IsDBNull(index))
            {
                donacion.Accion = dr.GetString(index);
            }

            return donacion;
        }
    }
}
