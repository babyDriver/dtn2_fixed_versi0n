﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Movimiento
{
    public class Guardar : IInsertFactory<Entity.Movimiento>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Movimiento movimiento)
        {
            DbCommand cmd = db.GetStoredProcCommand("Movimiento_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, movimiento.TrackingId);
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, movimiento.DetalledetencionId);
            db.AddInParameter(cmd, "_TipomovimientoId", DbType.Int32, movimiento.TipomovimientoId);
            db.AddInParameter(cmd, "_Fechahora", DbType.DateTime, movimiento.FechaYHora);
            db.AddInParameter(cmd, "_CeldaId", DbType.Int32, movimiento.CeldaId);
            db.AddInParameter(cmd, "_Observaciones", DbType.String, movimiento.Observacion);
            db.AddInParameter(cmd, "_Responsable", DbType.String, movimiento.Responsable);
            db.AddInParameter(cmd, "_InstitucionId", DbType.String, movimiento.InstitucionId);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, movimiento.Creadopor);
            db.AddInParameter(cmd, "_Activo", DbType.Int32, movimiento.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int32, movimiento.Habilitado);

            return cmd;
        }
    }
}
