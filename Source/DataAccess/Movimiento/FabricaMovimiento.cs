﻿using System.Data;
using DataAccess.Interfaces;

namespace DataAccess.Movimiento
{
    public class FabricaMovimiento : IEntityFactory<Entity.Movimiento>
    {
        public Entity.Movimiento ConstructEntity(IDataReader dr)
        {
            var movimiento = new Entity.Movimiento();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                movimiento.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                movimiento.TrackingId = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                movimiento.DetalledetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TipomovimientoId");
            if (!dr.IsDBNull(index))
            {
                movimiento.TipomovimientoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fechahora");
            if (!dr.IsDBNull(index))
            {
                movimiento.FechaYHora = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("CeldaId");
            if (!dr.IsDBNull(index))
            {
                movimiento.CeldaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Observaciones");
            if (!dr.IsDBNull(index))
            {
                movimiento.Observacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Responsable");
            if (!dr.IsDBNull(index))
            {
                movimiento.Responsable = dr.GetString(index);
            }

            index = dr.GetOrdinal("InstitucionId");
            if (!dr.IsDBNull(index))
            {
                movimiento.InstitucionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                movimiento.Creadopor = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                movimiento.Activo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                movimiento.Habilitado = dr.GetInt32(index);
            }
            return movimiento;
        }
    }
}
