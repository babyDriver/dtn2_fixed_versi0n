﻿using System;
using System.Collections.Generic;

namespace DataAccess.Movimiento
{
    public interface IMovimiento
    {
        List<Entity.Movimiento> ObtenerTodos();
        Entity.Movimiento ObtenerById(int Id);
        Entity.Movimiento ObtenerByInternoId(int Id);
        List<Entity.Movimiento> ObtenerPorDetalleDetencionId(int DetalleDetencionId);
        int Guardar(Entity.Movimiento movimiento);
        void Actualizar(Entity.Movimiento movimiento);
    }
}
