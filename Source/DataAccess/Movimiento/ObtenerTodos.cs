﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DataAccess.Movimiento
{
    public class ObtenerTodos : ISelectFactory<NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("Movimiento_GetAll_SP");
            return cmd;
        }
    }
}
