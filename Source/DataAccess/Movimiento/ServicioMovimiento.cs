﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.Movimiento
{
    public class ServicioMovimiento : ClsAbstractService<Entity.Movimiento>, IMovimiento
    {
        public ServicioMovimiento(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Movimiento> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaMovimiento(), new Entity.NullClass());
        }

        public Entity.Movimiento ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaMovimiento(), Id);
        }

        public void Actualizar(Entity.Movimiento movimiento)
        {
            IUpdateFactory<Entity.Movimiento> update = new Actualizar();
            Update(update, movimiento);
        }

        public int Guardar(Entity.Movimiento movimiento)
        {
            IInsertFactory<Entity.Movimiento> insert = new Guardar();
            return Insert(insert, movimiento);
        }

        public Entity.Movimiento ObtenerByInternoId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByInternoId();
            return GetByKey(select, new FabricaMovimiento(), Id);
        }

        public List<Entity.Movimiento> ObtenerPorDetalleDetencionId(int DetalleDetencionId)
        {
            ISelectFactory<int> select = new ObtenerPorDetalleDetencionId();
            return GetAll(select, new FabricaMovimiento(), DetalleDetencionId);
        }
    }
}
