﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.PertenenciaLog
{
    public class ServicioPertenenciaLog : ClsAbstractService<Entity.PertenenciaLog>, IPertenenciaLog
    {
        public ServicioPertenenciaLog(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.PertenenciaLog ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaPertenenciaLog(), id);
        }
    }
}
