﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.PertenenciaLog
{
    public interface IPertenenciaLog
    {
        Entity.PertenenciaLog ObtenerById(int id);
    }
}