﻿using DataAccess.Interfaces;

namespace DataAccess.PertenenciaLog
{
    public class FabricaPertenenciaLog : IEntityFactory<Entity.PertenenciaLog>
    {
        public Entity.PertenenciaLog ConstructEntity(System.Data.IDataReader dr)
        {
            var pertenenciaLog = new Entity.PertenenciaLog();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Accion");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.Accion = dr.GetString(index);
            }

            index = dr.GetOrdinal("AccionId");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.AccionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fec_Movto");
            if (!dr.IsDBNull(index))
            {
                pertenenciaLog.Fec_Movto = dr.GetDateTime(index);
            }

            
            return pertenenciaLog;
        }
    }
}
