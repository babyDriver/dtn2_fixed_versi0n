﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CertificadoLesionTipoLesionDetalle
{
    public class FabricaCertificadoLesionTipoLesionDetalle : IEntityFactory<Entity.CertificadoLesionTipoLesionDetalle>
    {
        public Entity.CertificadoLesionTipoLesionDetalle ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoLesionTipoLesionDetalle();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Certificado_LesionTipoLesionId");
            if (!dr.IsDBNull(index))
            {
                item.Certificado_LesionTipoLesionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TipoLesionId");
            if (!dr.IsDBNull(index))
            {
                item.TipoLesionId = dr.GetInt32(index);
            }
            return item;
        }
    }
}
