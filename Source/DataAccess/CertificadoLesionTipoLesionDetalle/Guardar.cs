﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoLesionTipoLesionDetalle
{
    public class Guardar : IInsertFactory<Entity.CertificadoLesionTipoLesionDetalle>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CertificadoLesionTipoLesionDetalle entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesiontipolesiondetalle_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_certificado_LesionTipoLesionId", DbType.Int32, entity.Certificado_LesionTipoLesionId);
            db.AddInParameter(cmd, "_TipoLesionId", DbType.Int32, entity.TipoLesionId);
            
            return cmd;
        }
    }
}
