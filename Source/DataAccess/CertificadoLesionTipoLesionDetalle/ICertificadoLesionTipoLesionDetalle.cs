﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionTipoLesionDetalle
{
    public interface ICertificadoLesionTipoLesionDetalle
    {
        int Guardar(Entity.CertificadoLesionTipoLesionDetalle certificadoLesionTipoLesionDetalle);
        List<Entity.CertificadoLesionTipoLesionDetalle> ObtenerPorId(int Id);


    }
}
