﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.CertificadoLesionTipoLesionDetalle
{
    public class ObtenerPorCertificado_LesionTipoLesionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("ccertificado_lesiontipolesiondetalle_ById_SP");
            db.AddInParameter(cmd, "_certificado_LesionTipoLesionId", DbType.Int32, Id);
            return cmd;
        }
    }
}
