﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.CertificadoLesionTipoLesionDetalle
{
    public class ServicioCertificadoLesionTipoLesionDetalle : ClsAbstractService<Entity.CertificadoLesionTipoLesionDetalle>, ICertificadoLesionTipoLesionDetalle
    {
        public ServicioCertificadoLesionTipoLesionDetalle(string dataBase)
       : base(dataBase)
        {
        }
        public int Guardar(Entity.CertificadoLesionTipoLesionDetalle certificadoLesionTipoLesionDetalle)
        {
            IInsertFactory<Entity.CertificadoLesionTipoLesionDetalle> insert = new Guardar();
            return Insert(insert, certificadoLesionTipoLesionDetalle);
        }

        public List<Entity.CertificadoLesionTipoLesionDetalle> ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorCertificado_LesionTipoLesionId();
            return GetAll(select, new FabricaCertificadoLesionTipoLesionDetalle(), Id);
        }
    }
}
