﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteBoletaControl
{
    public interface IReporteBoletaControl
    {
        Entity.ReporteBoletaControl GetReporteBoletaControl(int InternoId);
    }
}
