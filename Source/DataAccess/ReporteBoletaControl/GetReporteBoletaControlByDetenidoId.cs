﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ReporteBoletaControl
{
    public class GetReporteBoletaControlByDetenidoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int InternoId)
        {
            try
            {
                DbCommand cmd = db.GetStoredProcCommand("GetReporteBoletaControlByDetenidoId_SP");

                db.AddInParameter(cmd, "_InternoId", DbType.Int32, InternoId);

                return cmd;
            }

            catch (System.Exception ex)
            {
                throw new System.Exception(string.Concat("Err00000000000r. ", ex.Message));
            }
        }
    }
}
