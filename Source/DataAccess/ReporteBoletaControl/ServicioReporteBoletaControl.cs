﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.ReporteBoletaControl
{
    public class ServicioReporteBoletaControl : ClsAbstractService<Entity.ReporteBoletaControl>, IReporteBoletaControl
    {
        public Entity.ReporteBoletaControl GetReporteBoletaControl(int InternoId)
        {
            ISelectFactory<int> select = new GetReporteBoletaControlByDetenidoId();
            return GetByKey(select, new FabricaReporteBoletaControl(), InternoId);
        }
    }
}
