﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteBoletaControl
{
    public class FabricaReporteBoletaControl : IEntityFactory<Entity.ReporteBoletaControl>
    {
        public Entity.ReporteBoletaControl ConstructEntity(IDataReader dr)
        {
            var reporte = new Entity.ReporteBoletaControl();

            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                reporte.Remision = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                reporte.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaNacimiento");
            if (!dr.IsDBNull(index))
            {
                reporte.FechaNacimiento = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                reporte.Sexo = dr.GetString(index);
            }

            index = dr.GetOrdinal("Domicilio");
            if (!dr.IsDBNull(index))
            {
                reporte.Domicilio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Escolaridad");
            if (!dr.IsDBNull(index))
            {
                reporte.Escolaridad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                reporte.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Localidad");
            if (!dr.IsDBNull(index))
            {
                reporte.Localidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                reporte.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Responsable");
            if (!dr.IsDBNull(index))
            {
                reporte.Responsable = dr.GetString(index);
            }

            index = dr.GetOrdinal("Motivo");
            if (!dr.IsDBNull(index))
            {
                reporte.Motivo = dr.GetString(index);
            }

            index = dr.GetOrdinal("LugarDetencion");
            if (!dr.IsDBNull(index))
            {
                reporte.LugarDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("ColoniaDetencion");
            if (!dr.IsDBNull(index))
            {
                reporte.ColoniaDetencion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Situacion");
            if (!dr.IsDBNull(index))
            {
                reporte.Situacion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Califico");
            if (!dr.IsDBNull(index))
            {
                reporte.Califico = dr.GetString(index);
            }

            index = dr.GetOrdinal("TotalHoras");
            if (!dr.IsDBNull(index))
            {
                reporte.TotalHoras = dr.GetString(index);
            }

            index = dr.GetOrdinal("Multa");
            if (!dr.IsDBNull(index))
            {
                reporte.Multa = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaMedico");
            if (!dr.IsDBNull(index))
            {
                reporte.FechaMedico = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Realizador");
            if (!dr.IsDBNull(index))
            {
                reporte.Realizador = dr.GetString(index);
            }

            index = dr.GetOrdinal("Diagnostico");
            if (!dr.IsDBNull(index))
            {
                reporte.Diagnostico = dr.GetString(index);
            }

            return reporte;
        }
    }
}
