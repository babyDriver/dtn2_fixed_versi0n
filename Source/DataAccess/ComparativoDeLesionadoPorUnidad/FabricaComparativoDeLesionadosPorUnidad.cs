﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ComparativoDeLesionadoPorUnidad
{
    public class FabricaComparativoDeLesionadosPorUnidad : IEntityFactory<Entity.Comparativodelesionadosporunidad>
    {
        public Comparativodelesionadosporunidad ConstructEntity(IDataReader dr)
        {

            var comparativo = new Entity.Comparativodelesionadosporunidad();

            var index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                comparativo.Fecha = dr.GetDateTime(index);
            }


            index = dr.GetOrdinal("TipolesionId");
            if (!dr.IsDBNull(index))
            {
                comparativo.TipolesionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipolesion");
            if (!dr.IsDBNull(index))
            {
                comparativo.Tipolesion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Cantidad");
            if (!dr.IsDBNull(index))
            {
                comparativo.Cantidad = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("UnidadId");
            if (!dr.IsDBNull(index))
            {
                comparativo.UnidadId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                comparativo.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                comparativo.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Subcontrato");
            if (!dr.IsDBNull(index))
            {
                comparativo.Subcontrato = dr.GetInt32(index);
            }

            return comparativo;
        }
    }
}
