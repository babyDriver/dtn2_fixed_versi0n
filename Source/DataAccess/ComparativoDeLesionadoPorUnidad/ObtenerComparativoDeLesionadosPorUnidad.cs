﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.ComparativoDeLesionadoPorUnidad
{
    public class ObtenerComparativoDeLesionadosPorUnidad : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] filtros)
        {
            var f1 = DateTime.Parse(filtros[0]);
            var f2 = DateTime.Parse(filtros[1]);
            DbCommand cmd = db.GetStoredProcCommand("ReporteEstadistico_ComparativodeLesionadoporunidad_SP");
            db.AddInParameter(cmd, "_Fechainicio", DbType.DateTime, f1);
            db.AddInParameter(cmd, "_Fechafin", DbType.DateTime, f2);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(filtros[3]));
            db.AddInParameter(cmd, "_CorporacionId", DbType.Int32, Convert.ToInt32(filtros[4]));
            db.AddInParameter(cmd, "_UnidadId", DbType.Int32, Convert.ToInt32(filtros[5]));
            db.AddInParameter(cmd, "_Cliente", DbType.Int32, Convert.ToInt32(filtros[6]));
            return cmd;
        }
    }
}
