﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ComparativoDeLesionadoPorUnidad
{
    public interface IComparativoDeLesionadosPorUnidad
    {
        List<Entity.Comparativodelesionadosporunidad> ObtenerComparativodelesionados(string[] filtros);

    }
}
