﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.ComparativoDeLesionadoPorUnidad
{
    public class ServicioComparativoDeLesionadosPorUnidad : ClsAbstractService<Entity.Comparativodelesionadosporunidad>, IComparativoDeLesionadosPorUnidad
    {
        public ServicioComparativoDeLesionadosPorUnidad(string dataBase)
            : base(dataBase)
        {
        }
        public List<Comparativodelesionadosporunidad> ObtenerComparativodelesionados(string[] filtros)
        {
            ISelectFactory<string[]> select = new ObtenerComparativoDeLesionadosPorUnidad();
            return GetAll(select, new FabricaComparativoDeLesionadosPorUnidad(), filtros);
        }
    }
}
