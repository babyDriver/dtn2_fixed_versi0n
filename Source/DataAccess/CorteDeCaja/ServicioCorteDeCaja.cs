﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.CorteDeCaja
{
    public class ServicioCorteDeCaja : ClsAbstractService<Entity.CorteDeCaja>, ICorteDeCaja
    {
        public ServicioCorteDeCaja(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.CorteDeCaja item)
        {
            IInsertFactory<Entity.CorteDeCaja> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.CorteDeCaja ObtenerLastCorte(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerLastCorte();
            return GetByKey(select, new FabricaCorteDeCaja(), parametros);
        }

        public void Actualizar(Entity.CorteDeCaja item)
        {
            IUpdateFactory<Entity.CorteDeCaja> update = new Actualizar();
            Update(update, item);
        }

        public Entity.CorteDeCaja ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaCorteDeCaja(), trackingId);
        }
    }
}
