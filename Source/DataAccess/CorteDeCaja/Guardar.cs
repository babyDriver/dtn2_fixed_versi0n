﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.CorteDeCaja
{
    public class Guardar : IInsertFactory<Entity.CorteDeCaja>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CorteDeCaja item)
        {
            DbCommand cmd = db.GetStoredProcCommand("CorteDeCaja_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, item.TrackignId.ToString());
            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_SaldoInicial", DbType.Decimal, item.SaldoInicial);            
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, item.CreadoPor);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);

            return cmd;
        }
    }
}
