﻿using DataAccess.Interfaces;

namespace DataAccess.CorteDeCaja
{
    public class FabricaCorteDeCaja : IEntityFactory<Entity.CorteDeCaja>
    {
        public Entity.CorteDeCaja ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.CorteDeCaja();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackignId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("SaldoInicial");
            if (!dr.IsDBNull(index))
            {
                item.SaldoInicial = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("FechaInicio");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }            

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaFin");
            if (!dr.IsDBNull(index))
            {
                item.FechaFin = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                item.Tipo = dr.GetString(index);
            }

            return item;
        }
    }
}
