﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CorteDeCaja
{
    public class Actualizar : IUpdateFactory<Entity.CorteDeCaja>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.CorteDeCaja item)
        {
            DbCommand cmd = db.GetStoredProcCommand("CorteDeCaja_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_SaldoInicial", DbType.Decimal, item.SaldoInicial);
            db.AddInParameter(cmd, "_FechaInicio", DbType.DateTime, item.Fecha);
            db.AddInParameter(cmd, "_FechaFin", DbType.DateTime, item.FechaFin);            
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);
            return cmd;
        }
    }
}
