﻿using System;
using System.Collections.Generic;

namespace DataAccess.CorteDeCaja
{
    public interface ICorteDeCaja
    {
        int Guardar(Entity.CorteDeCaja item);
        Entity.CorteDeCaja ObtenerLastCorte(object[] parametros);
        Entity.CorteDeCaja ObtenerByTrackingId(Guid guid);
    }
}
