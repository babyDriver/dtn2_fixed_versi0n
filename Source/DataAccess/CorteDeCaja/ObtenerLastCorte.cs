﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CorteDeCaja
{
    public class ObtenerLastCorte : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("CorteDeCaja_GetLastCorte_SP");
            db.AddInParameter(cmd, "_SubcontratoId", DbType.Int32, parametros[0]);
            db.AddInParameter(cmd, "_Tipo", DbType.String, parametros[1].ToString());
            return cmd;
        }
    }
}
