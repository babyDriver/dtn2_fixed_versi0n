﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteEstadisticoTrazabilidad
{
    public class FabricaReporteEstadisticoTrazabilidad : IEntityFactory<Entity.ReporteEstadisticoTrazabilidad>
    {
        public Entity.ReporteEstadisticoTrazabilidad ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ReporteEstadisticoTrazabilidad();

            var index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                item.Remision = dr.GetString(index);
            }
            index = dr.GetOrdinal("Proceso");
            if (!dr.IsDBNull(index))
            {
                item.Proceso = dr.GetString(index);
            }
            index = dr.GetOrdinal("Subproceso");
            if (!dr.IsDBNull(index))
            {
                item.Subproceso = dr.GetString(index);
            }

            index = dr.GetOrdinal("Subproceso");
            if (!dr.IsDBNull(index))
            {
                item.Subproceso = dr.GetString(index);
            }

            index = dr.GetOrdinal("Usuario");
            if (!dr.IsDBNull(index))
            {
                item.Usuario = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estatus");
            if (!dr.IsDBNull(index))
            {
                item.Estatus = dr.GetString(index);
            }
            index = dr.GetOrdinal("Fechayhora");
            if (!dr.IsDBNull(index))
            {
                item.Fechayhora = dr.GetDateTime(index);
            }

            return item;
        }
    }
}
