﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;
namespace DataAccess.ReporteEstadisticoTrazabilidad
{
    public class ServicioReporteEstadisticoTrazabilidad : ClsAbstractService<Entity.ReporteEstadisticoTrazabilidad>, IReporteEstadisticoTrazabilidad
    {
        public ServicioReporteEstadisticoTrazabilidad(string dataBase)
      : base(dataBase)
        {

        }
        public List<Entity.ReporteEstadisticoTrazabilidad> GetList(string[] filtros)
        {
            ISelectFactory<string[]> select = new ObtenerTrazabilidad();
            return GetAll(select, new FabricaReporteEstadisticoTrazabilidad(), filtros);
        }
    }
}
