﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.ReporteEstadisticoTrazabilidad
{
    public class ObtenerTrazabilidad : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] filtros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ReporteEstadistico_Trazabilidad_SP");
            db.AddInParameter(cmd, "_Fechainicio", DbType.Date, Convert.ToDateTime(filtros[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.Date, Convert.ToDateTime(filtros[1]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(filtros[2]));
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, Convert.ToInt32(filtros[4]));
            //db.AddInParameter(cmd, "_ProcesoId", DbType.Int32, Convert.ToInt32(filtros[3]));
            //db.AddInParameter(cmd, "_SubprocesoId", DbType.Int32, Convert.ToInt32(filtros[4]));
            return cmd;
        }
    }
}
