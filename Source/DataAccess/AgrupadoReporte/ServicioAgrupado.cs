﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;


namespace DataAccess.AgrupadoReporte
{
    public class ServicioAgrupado : ClsAbstractService<Entity.AgrupadoReporte>, IAgrupadoReporte
    {
        public ServicioAgrupado(string dataBase)
            : base(dataBase)
        {
        }
        public List<Entity.AgrupadoReporte> GetByrangoFechas(DateTime[] Fecha)
        {
            ISelectFactory<DateTime[]> select = new ObtenerByRangodeFechas();
            return GetAll(select, new FabricaAgrupadoReporte(), Fecha);
        }
    }
}
