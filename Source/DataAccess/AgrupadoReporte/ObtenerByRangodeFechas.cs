﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System;

namespace DataAccess.AgrupadoReporte
{
    public class ObtenerByRangodeFechas : ISelectFactory<DateTime[]>
    {
        public DbCommand ConstructSelectCommand(Database db, DateTime[] fecha)
        {
            DbCommand cmd = db.GetStoredProcCommand("GetReporteAgrupadoByRangoFecha");
            db.AddInParameter(cmd, "_Fechainicio", DbType.Date, Convert.ToDateTime(fecha[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.Date, Convert.ToDateTime(fecha[1]));
            return cmd;
        }
    }
}
