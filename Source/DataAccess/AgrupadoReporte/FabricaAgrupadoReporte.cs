﻿using DataAccess.Interfaces;
using System.Data;
using DataAccess.Catalogo;
using Entity;

namespace DataAccess.AgrupadoReporte
{
    public class FabricaAgrupadoReporte : IEntityFactory<Entity.AgrupadoReporte>
    {
        public Entity.AgrupadoReporte ConstructEntity(IDataReader dr)
        {
            var item = new Entity.AgrupadoReporte();

            var index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                item.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Detenidooriginal");
            if (!dr.IsDBNull(index))
            {
                item.Detenidooriginal = dr.GetString(index);
            }
            index = dr.GetOrdinal("Expedienteoriginal");
            if (!dr.IsDBNull(index))
            {
                item.Expedienteoriginal = dr.GetString(index);
            }
            index = dr.GetOrdinal("DetenidoorignalId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoorignalId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Detenidoagrupado");
            if (!dr.IsDBNull(index))
            {
                item.Detenidoagrupado = dr.GetString(index);
            }
            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            return item;
        }
    }
}
