﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.AgrupadoReporte
{
    public interface IAgrupadoReporte
    {
        List<Entity.AgrupadoReporte> GetByrangoFechas(DateTime []Fecha );
    }
}
