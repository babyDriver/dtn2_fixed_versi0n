﻿using System.Collections.Generic;

namespace DataAccess.CertificadoLesionFotografiasDetalle
{
    public interface ICertificadoLesionFotografiasDetalle
    {
        int Guardar(Entity.CertificadoLesionFotografiasDetalle certificadoLesionFotografiasDetalle);
        void Actualizar(Entity.CertificadoLesionFotografiasDetalle certificadoLesionFotografiasDetalle);
        List<Entity.CertificadoLesionFotografiasDetalle> ObtenerPorCertificadoLesionFotografiaId(int Id);
        Entity.CertificadoLesionFotografiasDetalle ObtenerPorId(int Id);
    }
}