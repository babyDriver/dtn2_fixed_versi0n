﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoLesionFotografiasDetalle
{
    public class Guardar : IInsertFactory<Entity.CertificadoLesionFotografiasDetalle>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CertificadoLesionFotografiasDetalle entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionfotografiasdetalle_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_IdCertificado_lesionfotografias", DbType.Int32, entity.IdCertificado_lesionfotografias);
            db.AddInParameter(cmd, "_Fotografia", DbType.String, entity.Fotografia);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, entity.Descripcion);
            return cmd;
        }
    }
}
