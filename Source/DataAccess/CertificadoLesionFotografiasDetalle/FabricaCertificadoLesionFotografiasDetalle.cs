﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoLesionFotografiasDetalle
{
    public class FabricaCertificadoLesionFotografiasDetalle : IEntityFactory<Entity.CertificadoLesionFotografiasDetalle>
    {
        public Entity.CertificadoLesionFotografiasDetalle ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoLesionFotografiasDetalle();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("IdCertificado_lesionfotografias");
            if (!dr.IsDBNull(index))
            {
                item.IdCertificado_lesionfotografias = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Fotografia");
            if (!dr.IsDBNull(index))
            {
                item.Fotografia = dr.GetString(index);
            }
            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                item.Descripcion = dr.GetString(index);
            }
            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            return item;
        }
    }
}
