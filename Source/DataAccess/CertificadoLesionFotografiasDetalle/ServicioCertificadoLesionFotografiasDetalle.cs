﻿using System.Collections.Generic;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CertificadoLesionFotografiasDetalle
{
    public class ServicioCertificadoLesionFotografiasDetalle : ClsAbstractService<Entity.CertificadoLesionFotografiasDetalle>, ICertificadoLesionFotografiasDetalle
    {
        public ServicioCertificadoLesionFotografiasDetalle(string dataBase)
        : base(dataBase)
        {
        }

        public void Actualizar(Entity.CertificadoLesionFotografiasDetalle certificadoLesionFotografiasDetalle)
        {
            IUpdateFactory<Entity.CertificadoLesionFotografiasDetalle> update = new Actualizar();
            Update(update, certificadoLesionFotografiasDetalle);
        }

        public int Guardar(Entity.CertificadoLesionFotografiasDetalle certificadoLesionFotografiasDetalle)
        {
            IInsertFactory<Entity.CertificadoLesionFotografiasDetalle> insert = new Guardar();
            return Insert(insert, certificadoLesionFotografiasDetalle);
        }

        public List<Entity.CertificadoLesionFotografiasDetalle> ObtenerPorCertificadoLesionFotografiaId(int Id)
        {
            ISelectFactory<int> select = new ObtenerTodosPorCertificadoLesionFotografiaId();
            return GetAll(select, new FabricaCertificadoLesionFotografiasDetalle(), Id);
        }

        public Entity.CertificadoLesionFotografiasDetalle ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoLesionFotografiasDetalle(), Id);
        }
    }
}
