﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoLesionFotografiasDetalle
{
    public class ObtenerTodosPorCertificadoLesionFotografiaId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int identity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionfotografiasdetalle_GetAllById_SP");
            db.AddInParameter(cmd, "_IdCertificado_lesionfotografias", DbType.Int32, identity);
            return cmd;
        }
    }
}
