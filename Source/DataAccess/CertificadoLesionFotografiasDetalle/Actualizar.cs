﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoLesionFotografiasDetalle
{
    public class Actualizar : IUpdateFactory<Entity.CertificadoLesionFotografiasDetalle>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.CertificadoLesionFotografiasDetalle entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_lesionfotografiasdetalle_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, entity.Id);
            db.AddInParameter(cmd, "_IdCertificado_lesionfotografias", DbType.Int32, entity.IdCertificado_lesionfotografias);
            db.AddInParameter(cmd, "_Fotografia", DbType.String, entity.Fotografia);
            db.AddInParameter(cmd, "_Descripcion", DbType.String, entity.Descripcion);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);

            return cmd;
        }
    }
}
