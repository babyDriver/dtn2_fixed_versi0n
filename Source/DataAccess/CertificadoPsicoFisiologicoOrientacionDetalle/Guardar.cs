﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.CertificadoPsicoFisiologicoOrientacionDetalle
{
    public class Guardar : IInsertFactory<Entity.CertificadoPsicoFisiologicoOrientacionDetalle>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.CertificadoPsicoFisiologicoOrientacionDetalle entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("detallecertificado_psicofisiologicoOrientacionDetalle_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_certificado_psicofisiologicoorientacionId", DbType.Int32, entity.certificado_psicofisiologicoorientacionId);
            db.AddInParameter(cmd, "_OrientacionId", DbType.Int32, entity.OrientacionId);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, entity.FechaHora);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, entity.Creadopor);
            return cmd;

        }
    }
}
