﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.CertificadoPsicoFisiologicoOrientacionDetalle
{
    public class ServicioCertificadoPsicoFisiologicoOrientacionDetalle : ClsAbstractService<Entity.CertificadoPsicoFisiologicoOrientacionDetalle>, ICertificadoPsicoFisiologicoOrientacionDetalle
    {
        public ServicioCertificadoPsicoFisiologicoOrientacionDetalle(string dataBase)
        : base(dataBase)
        {
        }
        public int Guardar(Entity.CertificadoPsicoFisiologicoOrientacionDetalle certificadoPsicoFisilogicoOrientacion)
        {
            IInsertFactory<Entity.CertificadoPsicoFisiologicoOrientacionDetalle> insert = new Guardar();
            return Insert(insert, certificadoPsicoFisilogicoOrientacion);
        }

        public List<Entity.CertificadoPsicoFisiologicoOrientacionDetalle> ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerCertificadoPsicoFisiologicoOrientacionId();
            return GetAll(select, new FabricaCertificadoPsicoFisiologicoOrientacionDetalle(), Id);
        }
    }
}
