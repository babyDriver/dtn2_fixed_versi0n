﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CertificadoPsicoFisiologicoOrientacionDetalle
{
    public class FabricaCertificadoPsicoFisiologicoOrientacionDetalle : IEntityFactory<Entity.CertificadoPsicoFisiologicoOrientacionDetalle>
    {
        public Entity.CertificadoPsicoFisiologicoOrientacionDetalle ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoPsicoFisiologicoOrientacionDetalle();
            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("certificado_psicofisiologicoorientacionId");
            if (!dr.IsDBNull(index))
            {
                item.certificado_psicofisiologicoorientacionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("OrientacionId");
            if (!dr.IsDBNull(index))
            {
                item.OrientacionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }

            return item;
        }
    }
}
