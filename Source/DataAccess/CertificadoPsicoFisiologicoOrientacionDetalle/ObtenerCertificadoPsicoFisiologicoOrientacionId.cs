﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoPsicoFisiologicoOrientacionDetalle
{
    public class ObtenerCertificadoPsicoFisiologicoOrientacionId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("detallecertificado_psicofisiologicoOrientacionDetalle_ById_SP");
            db.AddInParameter(cmd, "_certificado_psicofisiologicoorientacionId", DbType.Int32, Id);
            return cmd;
        }
    }
}
