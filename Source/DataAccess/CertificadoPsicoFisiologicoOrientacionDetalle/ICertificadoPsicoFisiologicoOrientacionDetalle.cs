﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoPsicoFisiologicoOrientacionDetalle
{
   public interface ICertificadoPsicoFisiologicoOrientacionDetalle
    {
        int Guardar(Entity.CertificadoPsicoFisiologicoOrientacionDetalle certificadoPsicoFisilogicoOrientacion);
        List<Entity.CertificadoPsicoFisiologicoOrientacionDetalle> ObtenerPorId(int Id);



    }
}
