﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteUsosistema
{
    public interface IReporteUsosistema
    {
        List<Entity.ReporteUsosistema> Obtenerporfechas(string[] fechas);
    }
}
