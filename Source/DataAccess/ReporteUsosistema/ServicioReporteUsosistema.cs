﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;
using Entity;

namespace DataAccess.ReporteUsosistema
{
    public class ServicioReporteUsosistema : ClsAbstractService<Entity.ReporteUsosistema>, IReporteUsosistema
    {
        public ServicioReporteUsosistema(string dataBase)
            : base(dataBase) { }
        public List<Entity.ReporteUsosistema> Obtenerporfechas(string[] fechas)
        {
            ISelectFactory<string[]> select = new Obtenerporfechas();
            return GetAll(select, new FabricaReporteUsosistema(), fechas);
        }
    }
}
