﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteUsosistema
{
    public class FabricaReporteUsosistema : IEntityFactory<Entity.ReporteUsosistema>
    {
        public Entity.ReporteUsosistema ConstructEntity(IDataReader dr)
        {
            var reporte = new Entity.ReporteUsosistema();

            var index = dr.GetOrdinal("IdHistorial");
            if (!dr.IsDBNull(index))
            {
                reporte.IdHistorial = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                reporte.Fecha = dr.GetDateTime(index);
            }
            
            index = dr.GetOrdinal("Usuario");
            if (!dr.IsDBNull(index))
            {
                reporte.Usuario = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                reporte.Nombre = dr.GetString(index);
            }
            
            index = dr.GetOrdinal("ApellidoPaterno");
            if (!dr.IsDBNull(index))
            {
                reporte.ApellidoPaterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("ApellidoMaterno");
            if (!dr.IsDBNull(index))
            {
                reporte.ApellidoMaterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Rol");
            if (!dr.IsDBNull(index))
            {
                reporte.Rol = dr.GetString(index);
            }

            index = dr.GetOrdinal("SubContratos");
            if (!dr.IsDBNull(index))
            {
                reporte.SubContratos = dr.GetString(index);
            }
            index = dr.GetOrdinal("Movimiento");
            if (!dr.IsDBNull(index))
            {
                reporte.Movimiento = dr.GetString(index);
            }
            index = dr.GetOrdinal("NombreCompleto");
            if (!dr.IsDBNull(index))
            {
                reporte.NombreCompleto = dr.GetString(index);
            }
             index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                reporte.ContratoId = dr.GetInt32(index);
            }
            return reporte;
        }
    }
}
