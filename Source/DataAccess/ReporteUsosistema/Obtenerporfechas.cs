﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.ReporteUsosistema
{
    public class Obtenerporfechas : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] fechas)
        {
            DbCommand cmd = db.GetStoredProcCommand("Reporte_Usosistema_byDates_SP");
            
            db.AddInParameter(cmd, "_FechaInicio", DbType.DateTime, Convert.ToDateTime(fechas[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.DateTime, Convert.ToDateTime(fechas[1]));
            return cmd;
        }
    }
}
