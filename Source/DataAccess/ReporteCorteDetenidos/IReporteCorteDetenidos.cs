﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ReporteCorteDetenidos
{
    public interface IReporteCorteDetenidos
    {
        List<Entity.ReporteCorteDetenido> ObtenerReporteByContrato(object[] parametros);
    }
}
