﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ReporteCorteDetenidos
{
    public class FabricaReporteCorteDetenidos : IEntityFactory<Entity.ReporteCorteDetenido>
    {
        public Entity.ReporteCorteDetenido ConstructEntity(IDataReader dr)
        {
            var reporte = new Entity.ReporteCorteDetenido();

            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                reporte.Remision = dr.GetString(index);
            }

            index = dr.GetOrdinal("Evento");
            if (!dr.IsDBNull(index))
            {
                reporte.Evento = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                reporte.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                reporte.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Ingreso");
            if (!dr.IsDBNull(index))
            {
                reporte.Ingreso = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Calificacion");
            if (!dr.IsDBNull(index))
            {
                reporte.Calificación = dr.GetString(index);
            }

            index = dr.GetOrdinal("Celda");
            if (!dr.IsDBNull(index))
            {
                reporte.Celda = dr.GetString(index);
            }

            index = dr.GetOrdinal("Cumple");
            if (!dr.IsDBNull(index))
            {
                reporte.Cumple = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("IdDetenido");
            if (!dr.IsDBNull(index))
            {
                reporte.IdDetenido = dr.GetInt32(index);
            }

                return reporte;
        }
    }
}
