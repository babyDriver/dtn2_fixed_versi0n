﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.ReporteCorteDetenidos
{
    public class ObtenerReporteByContrato : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Reporte_Corte_Detenidos_GetByContrato_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(parametros[0]));
            db.AddInParameter(cmd, "_Tipo", DbType.String, Convert.ToString(parametros[1]));
            cmd.CommandTimeout = 0;
            return cmd;
        }
    }
}
