﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.ReporteCorteDetenidos
{
    public class ServicioReporteCorteDetenidos : ClsAbstractService<Entity.ReporteCorteDetenido>, IReporteCorteDetenidos
    {
        public List<Entity.ReporteCorteDetenido> ObtenerReporteByContrato(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerReporteByContrato();
            return GetAll(select, new FabricaReporteCorteDetenidos(), parametros);
        }
    }
}
