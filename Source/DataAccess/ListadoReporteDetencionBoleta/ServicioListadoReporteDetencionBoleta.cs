﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.ListadoReporteDetencionBoleta
{
    public class ServicioListadoReporteDetencionBoleta : ClsAbstractService<Entity.ListadoReporteDetencionBoleta>, IListadoReporteDetencionBoleta
    {
        public ServicioListadoReporteDetencionBoleta(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.ListadoReporteDetencionBoleta> ObtenerTodos(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaListadoReporteDetencionBoleta(), parametros);
        }
    }
}
