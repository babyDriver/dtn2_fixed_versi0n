﻿using System.Collections.Generic;

namespace DataAccess.ListadoReporteDetencionBoleta
{
    public interface IListadoReporteDetencionBoleta
    {
        List<Entity.ListadoReporteDetencionBoleta> ObtenerTodos(object[] data);
    }
}
