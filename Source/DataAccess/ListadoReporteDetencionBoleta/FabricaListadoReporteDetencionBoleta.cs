﻿using DataAccess.Interfaces;
using System;
using System.Data;

namespace DataAccess.ListadoReporteDetencionBoleta
{
    public class FabricaListadoReporteDetencionBoleta : IEntityFactory<Entity.ListadoReporteDetencionBoleta>
    {
        public Entity.ListadoReporteDetencionBoleta ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ListadoReporteDetencionBoleta();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }            

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                item.Expediente = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Paterno");
            if (!dr.IsDBNull(index))
            {
                item.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Materno");
            if (!dr.IsDBNull(index))
            {
                item.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Sexo");
            if (!dr.IsDBNull(index))
            {
                item.Sexo = dr.GetString(index);
            }            

            index = dr.GetOrdinal("NombreCompleto");
            if (!dr.IsDBNull(index))
            {
                item.NombreCompleto = dr.GetString(index);
            }

            index = dr.GetOrdinal("Estado");
            if (!dr.IsDBNull(index))
            {
                item.Estado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Municipio");
            if (!dr.IsDBNull(index))
            {
                item.Municipio = dr.GetString(index);
            }

            index = dr.GetOrdinal("Colonia");
            if (!dr.IsDBNull(index))
            {
                item.Colonia = dr.GetString(index);
            }

            return item;
        }
    }
}
