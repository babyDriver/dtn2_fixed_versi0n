﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAInstitucion
{
    public class Actualizar : IUpdateFactory<Entity.WSAInstitucion>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.WSAInstitucion entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAInstitucion_Update_SP");
            db.AddInParameter(cmd, "_idInstitucion", DbType.Int32, entity.IdInstitucion);
            db.AddInParameter(cmd, "_nombreInstitucion", DbType.String, entity.NombreInstitucion);
            db.AddInParameter(cmd, "_nombreCorto", DbType.String, entity.NombreCorto);
            db.AddInParameter(cmd, "_idMunicipio", DbType.Int32, entity.IdMunicipio);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, entity.Activo);

            return cmd;
        }
    }
}
