﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.WSAInstitucion
{
    public class Guardar : IInsertFactory<Entity.WSAInstitucion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.WSAInstitucion entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAInstitucion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_idInstitucion", DbType.Int32, entity.IdInstitucion);
            db.AddInParameter(cmd, "_nombreInstitucion", DbType.String, entity.NombreInstitucion);
            db.AddInParameter(cmd, "_nombreCorto", DbType.String, entity.NombreCorto);
            db.AddInParameter(cmd, "_idMunicipio", DbType.Int32, entity.IdMunicipio);

            return cmd;
        }
    }
}
