﻿using System.Collections.Generic;

namespace DataAccess.WSAInstitucion
{
    public interface IWSAInstitucion
    {
        List<Entity.WSAInstitucion> ObtenerTodos();
        Entity.WSAInstitucion ObtenerById(int id);
        int Guardar(Entity.WSAInstitucion institucion);
        void Actualizar(Entity.WSAInstitucion institucion);
    }
}
