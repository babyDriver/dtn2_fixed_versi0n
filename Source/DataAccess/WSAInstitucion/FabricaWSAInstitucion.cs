﻿using DataAccess.Interfaces;

namespace DataAccess.WSAInstitucion
{
    public class FabricaWSAInstitucion : IEntityFactory<Entity.WSAInstitucion>
    {
        public Entity.WSAInstitucion ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.WSAInstitucion();

            var index = dr.GetOrdinal("idInstitucion");
            if (!dr.IsDBNull(index))
            {
                item.IdInstitucion = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("nombreInstitucion");
            if (!dr.IsDBNull(index))
            {
                item.NombreInstitucion = dr.GetString(index);
            }


            index = dr.GetOrdinal("nombreCorto");
            if (!dr.IsDBNull(index))
            {
                item.NombreCorto = dr.GetString(index);
            }

            index = dr.GetOrdinal("idMunicipio");
            if (!dr.IsDBNull(index))
            {
                item.IdMunicipio = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaModificacion");
            if (!dr.IsDBNull(index))
            {
                item.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            return item;
        }
    }
}
