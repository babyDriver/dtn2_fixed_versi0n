﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.WSAInstitucion
{
    public class ServicioWSAInstitucion : ClsAbstractService<Entity.WSAInstitucion>, IWSAInstitucion
    {
        public ServicioWSAInstitucion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.WSAInstitucion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaWSAInstitucion(), new Entity.NullClass());
        }

        public Entity.WSAInstitucion ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaWSAInstitucion(), id);
        }

        public int Guardar(Entity.WSAInstitucion institucion)
        {
            IInsertFactory<Entity.WSAInstitucion> insert = new Guardar();
            return Insert(insert, institucion);
        }

        public void Actualizar(Entity.WSAInstitucion institucion)
        {
            IUpdateFactory<Entity.WSAInstitucion> update = new Actualizar();
            Update(update, institucion);
        }
    }
}
