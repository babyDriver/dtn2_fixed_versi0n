﻿using System;
using System.Data;
using System.Data.Common;
using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace DataAccess.DescuentoAgravanteJuezCalificador
{
    public class ObtenerDescuentoAgravanteJuezCalificador : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] filtros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ReporteEstadistico_DescuentoAgravanteJuezcalificador_SP");
            db.AddInParameter(cmd, "_Fechainicio", DbType.Date, Convert.ToDateTime(filtros[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.Date, Convert.ToDateTime(filtros[1]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(filtros[3]));
            db.AddInParameter(cmd, "_JuezId", DbType.Int32, Convert.ToInt32(filtros[4]));
            db.AddInParameter(cmd, "_MotivoId", DbType.Int32, Convert.ToInt32(filtros[2]));
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, Convert.ToInt32(filtros[6]));
            return cmd;
        }
    }
}
