﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.DescuentoAgravanteJuezCalificador
{
    public class FabricaDescuentoAgravanteJuezCalificador : IEntityFactory<Entity.Descuentosagravantesjuezcalificador>
    {
        public Descuentosagravantesjuezcalificador ConstructEntity(IDataReader dr)
        {
            var descuentosagravantesjuezcalificador = new Entity.Descuentosagravantesjuezcalificador();

            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                descuentosagravantesjuezcalificador.Remision = dr.GetString(index);
            }

            index = dr.GetOrdinal("Detenido");
            if (!dr.IsDBNull(index))
            {
                descuentosagravantesjuezcalificador.Detenido = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fechacalificacion");
            if (!dr.IsDBNull(index))
            {
                descuentosagravantesjuezcalificador.Fechacalificacion = dr.GetDateTime(index);
            }


            index = dr.GetOrdinal("Total");
            if (!dr.IsDBNull(index))
            {
                descuentosagravantesjuezcalificador.Total = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Descuento");
            if (!dr.IsDBNull(index))
            {
                descuentosagravantesjuezcalificador.Descuento = dr.GetDecimal(index);
            }
            index = dr.GetOrdinal("Agravante");
            if (!dr.IsDBNull(index))
            {
                descuentosagravantesjuezcalificador.Agravante = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Totalapagar");
            if (!dr.IsDBNull(index))
            {
                descuentosagravantesjuezcalificador.Totalapagar = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Razon");
            if (!dr.IsDBNull(index))
            {
                descuentosagravantesjuezcalificador.Razon = dr.GetString(index);
            }

            return descuentosagravantesjuezcalificador;
        }
    }
}