﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;


namespace DataAccess.DescuentoAgravanteJuezCalificador
{
    public class ServicioDescuentoAgravanteJuezCalificador : ClsAbstractService<Entity.Descuentosagravantesjuezcalificador>, IDescuentoAgravanteJuezCalificador
    {
        public ServicioDescuentoAgravanteJuezCalificador(String dataBase)
          : base(dataBase)
        {
        }

        public List<Descuentosagravantesjuezcalificador> GetDescuentosagravantesjuezcalificador(string[] filtros)
        {
            ISelectFactory<string[]> select = new ObtenerDescuentoAgravanteJuezCalificador();
            return GetAll(select, new FabricaDescuentoAgravanteJuezCalificador(), filtros);
        }
    }
}