﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DescuentoAgravanteJuezCalificador
{
    public interface IDescuentoAgravanteJuezCalificador
    {
        List<Entity.Descuentosagravantesjuezcalificador> GetDescuentosagravantesjuezcalificador(string[] filtros);
    }
}