﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.ContratoUsuario
{
    public class Guardar : IInsertFactory<Entity.ContratoUsuario>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.ContratoUsuario contratoUsuario)
        {
            DbCommand cmd = db.GetStoredProcCommand("ContratoUsuario_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_IdContrato", DbType.Int32, contratoUsuario.IdContrato);
            db.AddInParameter(cmd, "_IdUsuario", DbType.Int32, contratoUsuario.IdUsuario);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, contratoUsuario.TrackingId);
            db.AddInParameter(cmd, "_CreadoPor", DbType.Int32, contratoUsuario.CreadoPor);
            db.AddInParameter(cmd, "_Tipo", DbType.String, contratoUsuario.Tipo);

            return cmd;
        }
    }
}
