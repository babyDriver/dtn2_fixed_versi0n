﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ContratoUsuario
{
    public class Actualizar : IUpdateFactory<Entity.ContratoUsuario>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.ContratoUsuario contratoUsuario)
        {
            DbCommand cmd = db.GetStoredProcCommand("ContratoUsuario_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, contratoUsuario.Id);
            db.AddInParameter(cmd, "_IdContrato", DbType.Int32, contratoUsuario.IdContrato);
            db.AddInParameter(cmd, "_IdUsuario", DbType.Int32, contratoUsuario.IdUsuario);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, contratoUsuario.TrackingId.ToString());
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, contratoUsuario.Activo);
            db.AddInParameter(cmd, "_Tipo", DbType.String, contratoUsuario.Tipo);

            return cmd;
        }
    }
}
