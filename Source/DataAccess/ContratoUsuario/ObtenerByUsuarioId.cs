﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.ContratoUsuario
{
    public class ObtenerByUsuarioId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("ContratoUsuario_GetByUsuarioId_SP");
            db.AddInParameter(cmd, "_IdUsuario", DbType.Int32, id);
            return cmd;
        }
    }
}
