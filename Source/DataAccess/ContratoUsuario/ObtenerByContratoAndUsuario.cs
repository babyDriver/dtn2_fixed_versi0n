﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ContratoUsuario
{
    public class ObtenerByContratoAndUsuario : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ContratoUsuario_GetByContratoAndUsuario_SP");
            db.AddInParameter(cmd, "_IdUsuario", DbType.Int32, parametros[0]);
            db.AddInParameter(cmd, "_IdContrato", DbType.Int32, parametros[1]);
            db.AddInParameter(cmd, "_Tipo", DbType.String, parametros[2]);
            return cmd;
        }
    }
}
