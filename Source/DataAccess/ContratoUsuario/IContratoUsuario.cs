﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ContratoUsuario
{
    public interface IContratoUsuario
    {
        int Guardar(Entity.ContratoUsuario contratoUsuario);
        List<Entity.ContratoUsuario> ObtenerByUsuarioId(int id);
        Entity.ContratoUsuario ObtenerByContratoAndUsuario(object[] parametros);
        Entity.ContratoUsuario ObtenerById(int id);
        void Actualizar(Entity.ContratoUsuario contratoUsuario);
    }
}
