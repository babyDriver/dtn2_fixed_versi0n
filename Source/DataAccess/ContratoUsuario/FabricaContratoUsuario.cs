﻿using DataAccess.Interfaces;

namespace DataAccess.ContratoUsuario
{
    public class FabricaContratoUsuario : IEntityFactory<Entity.ContratoUsuario>
    {
        public Entity.ContratoUsuario ConstructEntity(System.Data.IDataReader dr)
        {
            var contrato = new Entity.ContratoUsuario();

            var index = dr.GetOrdinal("IdContrato");
            if (!dr.IsDBNull(index))
            {
                contrato.IdContrato = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("IdUsuario");
            if (!dr.IsDBNull(index))
            {
                contrato.IdUsuario = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                contrato.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                contrato.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                contrato.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                contrato.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                contrato.Tipo = dr.GetString(index);
            }

            return contrato;
        }
    }
}
