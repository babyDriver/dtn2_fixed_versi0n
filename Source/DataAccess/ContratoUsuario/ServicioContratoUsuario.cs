﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.ContratoUsuario
{
    public class ServicioContratoUsuario : ClsAbstractService<Entity.ContratoUsuario>, IContratoUsuario
    {
        public ServicioContratoUsuario(string dataBase)
            : base(dataBase)
        {
        }

        public int Guardar(Entity.ContratoUsuario contratoUsuario)
        {
            IInsertFactory<Entity.ContratoUsuario> insert = new Guardar();
            return Insert(insert, contratoUsuario);
        }
        
        public List<Entity.ContratoUsuario> ObtenerByUsuarioId(int id)
        {
            ISelectFactory<int> select = new ObtenerByUsuarioId();
            return GetAll(select, new FabricaContratoUsuario(), id);
        }

        public Entity.ContratoUsuario ObtenerByContratoAndUsuario(object[] datos)
        {
            ISelectFactory<object[]> select = new ObtenerByContratoAndUsuario();
            return GetByKey(select, new FabricaContratoUsuario(), datos);
        }

        public Entity.ContratoUsuario ObtenerById(int id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaContratoUsuario(), id);
        }

        public void Actualizar(Entity.ContratoUsuario contratoUsuario)
        {
            IUpdateFactory<Entity.ContratoUsuario> update = new Actualizar();
            Update(update, contratoUsuario);
        }
    }
}
