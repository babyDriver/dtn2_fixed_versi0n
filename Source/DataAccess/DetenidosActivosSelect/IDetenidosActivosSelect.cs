﻿using System.Collections.Generic;

namespace DataAccess.DetenidosActivosSelect
{
    public interface IDetenidosActivosSelect
    {
        List<Entity.DetenidosActivosSelect> ObtenerTodos(object[] data);
    }
}
