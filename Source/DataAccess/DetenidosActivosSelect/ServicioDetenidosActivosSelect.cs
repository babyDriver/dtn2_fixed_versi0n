﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.DetenidosActivosSelect
{
    public class ServicioDetenidosActivosSelect : ClsAbstractService<Entity.DetenidosActivosSelect>, IDetenidosActivosSelect
    {
        public ServicioDetenidosActivosSelect(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.DetenidosActivosSelect> ObtenerTodos(object[] data)
        {
            ISelectFactory<object[]> select = new ObtenerTodos();
            return GetAll(select, new FabricaDetenidosActivosSelect(), data);
        }
    }
}
