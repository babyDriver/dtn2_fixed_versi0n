﻿using DataAccess.Interfaces;

namespace DataAccess.DetenidosActivosSelect
{
    public class FabricaDetenidosActivosSelect : IEntityFactory<Entity.DetenidosActivosSelect>
    {
        public Entity.DetenidosActivosSelect ConstructEntity(System.Data.IDataReader dr)
        {
            var interno = new Entity.DetenidosActivosSelect();

            var index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                interno.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Expediente");
            if (!dr.IsDBNull(index))
            {
                interno.Expediente = dr.GetString(index);
            }            

            index = dr.GetOrdinal("NombreDetenido");
            if (!dr.IsDBNull(index))
            {
                interno.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("APaternoDetenido");
            if (!dr.IsDBNull(index))
            {
                interno.Paterno = dr.GetString(index);
            }

            index = dr.GetOrdinal("AMaternoDetenido");
            if (!dr.IsDBNull(index))
            {
                interno.Materno = dr.GetString(index);
            }

            index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                interno.IdDetalle = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("NombreCombpleto");
            if (!dr.IsDBNull(index))
            {
                interno.NombreCombpleto = dr.GetString(index);
            }
            return interno;
        }
    }
}
