﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.DetenidosActivosSelect
{
    public class ObtenerTodos : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] data)
        {
            DbCommand cmd = db.GetStoredProcCommand("DetenidosActivosSelect_GetAll_SP");
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, data[0]);
            db.AddInParameter(cmd, "_Tipo", DbType.String, data[1]);
            
            if(data.Length > 2)
            {
                db.AddInParameter(cmd, "_fechaInicial", DbType.DateTime, Convert.ToDateTime(data[2].ToString()));
                db.AddInParameter(cmd, "_fechaFinal", DbType.DateTime, Convert.ToDateTime(data[3].ToString()));
            }

            return cmd;
        }
    }
}
