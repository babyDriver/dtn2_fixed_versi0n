﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.RelacionDetenidosPorSalidaAutorizada
{
    public class ServicioRelacionDetenidosPorSalidaAutorizada : ClsAbstractService<Entity.RelacionDetenidosPorSalidaAutorizada>, IRelacionDetenidosPorSalidaAutorisada
    {
        public ServicioRelacionDetenidosPorSalidaAutorizada(string dataBase) : base(dataBase)
        {

        }
        public List<Entity.RelacionDetenidosPorSalidaAutorizada> GetDetails(string[] filtros)
        {
            ISelectFactory<string[]> select = new ObtenerRelaciondetenidosPorSalidaAutorizada();
            return GetAll(select, new FabricaRelacionDetenidosPorSalidaAutorizada(), filtros);
        }
    }
}