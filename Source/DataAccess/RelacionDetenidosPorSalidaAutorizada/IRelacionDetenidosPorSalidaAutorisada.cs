﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.RelacionDetenidosPorSalidaAutorizada
{
    public interface IRelacionDetenidosPorSalidaAutorisada
    {
        List<Entity.RelacionDetenidosPorSalidaAutorizada> GetDetails(string[] filtros);
    }
}
