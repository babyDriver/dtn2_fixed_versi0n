﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.RelacionDetenidosPorSalidaAutorizada
{
    public class FabricaRelacionDetenidosPorSalidaAutorizada : IEntityFactory<Entity.RelacionDetenidosPorSalidaAutorizada>
    {
        public Entity.RelacionDetenidosPorSalidaAutorizada ConstructEntity(IDataReader dr)
        {
            var item = new Entity.RelacionDetenidosPorSalidaAutorizada();
            var index = dr.GetOrdinal("Remision");
            if (!dr.IsDBNull(index))
            {
                item.Remision = dr.GetString(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }

            index = dr.GetOrdinal("Edad");
            if (!dr.IsDBNull(index))
            {
                item.Edad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fechacalificacion");
            if (!dr.IsDBNull(index))
            {
                item.Fechacalificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Unidad");
            if (!dr.IsDBNull(index))
            {
                item.Unidad = dr.GetString(index);
            }

            index = dr.GetOrdinal("Fecharegistro");
            if (!dr.IsDBNull(index))
            {
                item.Fecharegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Totalhoras");
            if (!dr.IsDBNull(index))
            {
                item.Totalhoras = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Soloarresto");
            if (!dr.IsDBNull(index))
            {
                item.Soloarresto = dr.GetBoolean(index);
            }
            index = dr.GetOrdinal("Totalapagar");
            if (!dr.IsDBNull(index))
            {
                item.Totalapagar = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Motivodetencion");
            if (!dr.IsDBNull(index))
            {
                item.Motivodetencion = dr.GetString(index);
            }



            index = dr.GetOrdinal("Fechasalida");
            if (!dr.IsDBNull(index))
            {
                item.Fechasalida = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("TiposalidaId");
            if (!dr.IsDBNull(index))
            {
                item.TiposalidaId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tiposalida");
            if (!dr.IsDBNull(index))
            {
                item.Tiposalida = dr.GetString(index);
            }
            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoId = dr.GetInt32(index);
            }

                return item;
        }
    }
}
