﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DetenidosPorTipoLesion
{
    public interface IDetenidosPorTipoLesion
    {
        List<Entity.DetenidosPorTipoLesion> ObtenerDetenidosPorTipoLesion(string[] filtros);
    }
}
