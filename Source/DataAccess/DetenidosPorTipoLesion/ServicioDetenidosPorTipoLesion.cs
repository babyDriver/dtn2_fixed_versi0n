﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;

namespace DataAccess.DetenidosPorTipoLesion
{
    public class ServicioDetenidosPorTipoLesion : ClsAbstractService<Entity.DetenidosPorTipoLesion>, IDetenidosPorTipoLesion
    {
        public ServicioDetenidosPorTipoLesion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.DetenidosPorTipoLesion> ObtenerDetenidosPorTipoLesion(string[] filtros)
        {
            ISelectFactory<string[]> select = new ObtenerDetenidosPorTipoLesion();
            return GetAll(select, new FabricaDetenidosPorTipoLesion(), filtros);
        }
    }
}
