﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DetenidosPorTipoLesion
{
    public class ObtenerDetenidosPorTipoLesion : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] filtros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ReporteEstadistico_DetenidosPorTipoLesion_SP");
            db.AddInParameter(cmd, "_Fechainicio", DbType.DateTime, DateTime.Parse(filtros[0]));
            db.AddInParameter(cmd, "_Fechafin", DbType.DateTime, DateTime.Parse(filtros[1]));
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, Convert.ToInt32(filtros[3]));
            db.AddInParameter(cmd, "_TipoLesionId", DbType.Int32, Convert.ToInt32(filtros[4]));
            db.AddInParameter(cmd, "_ClienteId", DbType.Int32, Convert.ToInt32(filtros[5]));
            return cmd;
        }
    }
}
