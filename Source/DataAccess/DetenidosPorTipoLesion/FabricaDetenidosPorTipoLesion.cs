﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.DetenidosPorTipoLesion
{
    public class FabricaDetenidosPorTipoLesion : IEntityFactory<Entity.DetenidosPorTipoLesion>
    {
        public Entity.DetenidosPorTipoLesion ConstructEntity(IDataReader dr)
        {
            var detenidos = new Entity.DetenidosPorTipoLesion();

            var index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                detenidos.Fecha = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("TipolesionId");
            if (!dr.IsDBNull(index))
            {
                detenidos.TipolesionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipolesion");
            if (!dr.IsDBNull(index))
            {
                detenidos.Tipolesion = dr.GetString(index);
            }

            index = dr.GetOrdinal("Cantidad");
            if (!dr.IsDBNull(index))
            {
                detenidos.Cantidad = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                detenidos.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Subcontrato");
            if (!dr.IsDBNull(index))
            {
                detenidos.Subcontrato = dr.GetInt32(index);
            }

            return detenidos;
        }
    }
}
