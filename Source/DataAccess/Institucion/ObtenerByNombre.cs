﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Institucion
{
    public class ObtenerByNombre : ISelectFactory<string>
    {

        public DbCommand ConstructSelectCommand(Database db, string nombre)
        {
           
            DbCommand cmd = db.GetStoredProcCommand("Institucion_GetByNombre_SP");
            db.AddInParameter(cmd, "_Nombre", DbType.String, nombre);
           
            return cmd;
        }

      
    
    }
}
