﻿using DataAccess.Interfaces;
using System.Collections.Generic;
using System;

namespace DataAccess.Institucion
{
    public class ServicioInstitucion : ClsAbstractService<Entity.Institucion>, IInstitucion
    {
        public ServicioInstitucion(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.Institucion ObtenerByTrackingId(Guid trackingId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaInstitucion(), trackingId);
        }

        public List<Entity.Institucion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaInstitucion(), new Entity.NullClass());
        }

        public List<Entity.Institucion> ObtenerHabilitados()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerHabilitados();
            return GetAll(select, new FabricaInstitucion(), new Entity.NullClass());
        }


        public Entity.Institucion ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaInstitucion(), Id);
        }

        public List<Entity.Institucion> ObtenerByEstadoId(int CentroId)
        {
            ISelectFactory<int> select = new ObtenerByEstadoId();
            return GetAll(select, new FabricaInstitucion(), CentroId);
        }


        public Entity.Institucion ObtenerByNombre(string nombre)
        {
           
            ISelectFactory<string> select = new ObtenerByNombre();
            return GetByKey(select, new FabricaInstitucion(), nombre);
        }

        public int Guardar(Entity.Institucion item)
        {
            IInsertFactory<Entity.Institucion> insert = new Guardar();
            return Insert(insert, item);
        }

        public void Actualizar(Entity.Institucion item)
        {
            IUpdateFactory<Entity.Institucion> update = new Actualizar();
            Update(update, item);
        }
    }
}
