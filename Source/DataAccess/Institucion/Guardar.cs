﻿using System.Data;
using System.Data.Common;
using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.Institucion
{
    public class Guardar : IInsertFactory<Entity.Institucion>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.Institucion item) 
        {
           
            DbCommand cmd = db.GetStoredProcCommand("Institucion_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, item.TrackingId);
            db.AddInParameter(cmd, "_Nombre", DbType.String, item.Nombre);
            db.AddInParameter(cmd, "_DomicilioId", DbType.Int32, item.DomicilioId);
            db.AddInParameter(cmd, "_Encargado", DbType.String, item.Encargado);
            db.AddInParameter(cmd, "_Clave", DbType.String, item.Clave);
            db.AddInParameter(cmd, "_Telefono", DbType.String, item.Telefono);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Boolean, item.Habilitado);
            db.AddInParameter(cmd, "_CupoHombres", DbType.Int32, item.CupoHombres);
            db.AddInParameter(cmd, "_CupoMujeres", DbType.Int32, item.CupoMujeres);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, item.Creadopor);
            db.AddInParameter(cmd, "_ContratoId", DbType.Int32, item.ContratoId);
            db.AddInParameter(cmd, "_Tipo", DbType.String, item.Tipo);
            return cmd;
        }

    }
}
