﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Institucion
{
    public class ObtenerHabilitados : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.NullClass identity)
        {

            DbCommand cmd = db.GetStoredProcCommand("Institucion_GetHabilitados_SP");
           
            return cmd;
        }
       
    }
}
