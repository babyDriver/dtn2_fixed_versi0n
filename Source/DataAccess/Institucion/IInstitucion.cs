﻿using System.Collections.Generic;
using System;

namespace DataAccess.Institucion
{
    public interface IInstitucion
    {
        Entity.Institucion ObtenerById(int Id);
        Entity.Institucion ObtenerByNombre(string item);
        List<Entity.Institucion> ObtenerTodos();
        int Guardar(Entity.Institucion item);
        void Actualizar(Entity.Institucion item);
        Entity.Institucion ObtenerByTrackingId(Guid trackingId);
        List<Entity.Institucion> ObtenerByEstadoId(int EstadoId);
    }
}
