﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.Institucion
{
    public class ObtenerByEstadoId : ISelectFactory<int>
    {

        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
          
            DbCommand cmd = db.GetStoredProcCommand("Institucion_GetByEstadoId_SP");
            db.AddInParameter(cmd, "_EstadoId", DbType.Int32, Id);
           
            return cmd;
        }

     
    }
}
