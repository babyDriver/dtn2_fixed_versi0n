﻿
using System;
using System.Data;
using DataAccess.Interfaces;
using DataAccess.Estado;


namespace DataAccess.Institucion
{
    public class FabricaInstitucion : IEntityFactory<Entity.Institucion>
    {
        //private static ServicioEstado servicioEstado = new ServicioEstado("MySQLConnectionString");

        public Entity.Institucion ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.Institucion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }


            index = dr.GetOrdinal("DomicilioId");
            if (!dr.IsDBNull(index))
            {
                item.DomicilioId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Telefono");
            if (!dr.IsDBNull(index))
            {
                item.Telefono = dr.GetString(index);
            }

            index = dr.GetOrdinal("Clave");
            if (!dr.IsDBNull(index))
            {
                item.Clave = dr.GetString(index);
            }
            index = dr.GetOrdinal("Encargado");
            if (!dr.IsDBNull(index))
            {
                item.Encargado = dr.GetString(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("CupoHombres");
            if (!dr.IsDBNull(index))
            {
                item.CupoHombres = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("CupoMujeres");
            if (!dr.IsDBNull(index))
            {
                item.CupoMujeres = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContratoId");
            if (!dr.IsDBNull(index))
            {
                item.ContratoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Tipo");
            if (!dr.IsDBNull(index))
            {
                item.Tipo = dr.GetString(index);
            }

            return item;
        }
    }
}
