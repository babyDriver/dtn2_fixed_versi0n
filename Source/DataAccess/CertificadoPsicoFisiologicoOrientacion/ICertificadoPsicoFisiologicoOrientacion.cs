﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoPsicoFisiologicoOrientacion
{
   public interface ICertificadoPsicoFisiologicoOrientacion
    {
        int Guardar(Entity.CertificadoPsicoFisilogicoOrientacion certificadoPsicoFisilogicoOrientacion);
        Entity.CertificadoPsicoFisilogicoOrientacion ObtenerPorId(int Id);
    }
}
