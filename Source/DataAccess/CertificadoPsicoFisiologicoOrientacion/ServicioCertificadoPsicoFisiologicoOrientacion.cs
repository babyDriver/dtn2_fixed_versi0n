﻿using DataAccess.Interfaces;
using Entity;
using System.Collections.Generic;

namespace DataAccess.CertificadoPsicoFisiologicoOrientacion
{
    public class ServicioCertificadoPsicoFisiologicoOrientacion : ClsAbstractService<Entity.CertificadoPsicoFisilogicoOrientacion>, ICertificadoPsicoFisiologicoOrientacion
    {

        public ServicioCertificadoPsicoFisiologicoOrientacion(string dataBase)
         : base(dataBase)
        {
        }
        public int Guardar(CertificadoPsicoFisilogicoOrientacion certificadoPsicoFisilogicoOrientacion)
        {
            IInsertFactory<Entity.CertificadoPsicoFisilogicoOrientacion> insert = new Guardar();
            return Insert(insert, certificadoPsicoFisilogicoOrientacion);
        }

        public CertificadoPsicoFisilogicoOrientacion ObtenerPorId(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoPsicoFisilogicoOrientacion(), Id);
        }
    }
}
