﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CertificadoPsicoFisiologicoOrientacion
{
    public class FabricaCertificadoPsicoFisilogicoOrientacion : IEntityFactory<Entity.CertificadoPsicoFisilogicoOrientacion>
    {
        public CertificadoPsicoFisilogicoOrientacion ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoPsicoFisilogicoOrientacion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.FechaHora = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }
            return item;
        }
    }
}
