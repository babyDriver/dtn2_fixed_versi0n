﻿using DataAccess.Interfaces;

namespace DataAccess.Calificacion
{
    public class FabricaCalificacion : IEntityFactory<Entity.Calificacion>
    {
        public Entity.Calificacion ConstructEntity(System.Data.IDataReader dr)
        {
            var item = new Entity.Calificacion();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetGuid(index);
            }


            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("CreadoPor");
            if (!dr.IsDBNull(index))
            {
                item.CreadoPor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("InternoId");
            if (!dr.IsDBNull(index))
            {
                item.InternoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("SituacionId");
            if (!dr.IsDBNull(index))
            {
                item.SituacionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("InstitucionId");
            if (!dr.IsDBNull(index))
            {
                item.InstitucionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fundamento");
            if (!dr.IsDBNull(index))
            {
                item.Fundamento = dr.GetString(index);
            }

            index = dr.GetOrdinal("TrabajoSocial");
            if (!dr.IsDBNull(index))
            {
                item.TrabajoSocial = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TotalHoras");
            if (!dr.IsDBNull(index))
            {
                item.TotalHoras = dr.GetInt32(index);
            }

            //index = dr.GetOrdinal("MaxHoras");
            //if (!dr.IsDBNull(index))
            //{
            //    item.MaxHoras = dr.GetString(index);
            //}

            index = dr.GetOrdinal("SoloArresto");
            if (!dr.IsDBNull(index))
            {
                item.SoloArresto = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("TotalDeMultas");
            if (!dr.IsDBNull(index))
            {
                item.TotalDeMultas = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Agravante");
            if (!dr.IsDBNull(index))
            {
                item.Agravante = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Ajuste");
            if (!dr.IsDBNull(index))
            {
                item.Ajuste = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("TotalAPagar");
            if (!dr.IsDBNull(index))
            {
                item.TotalAPagar = dr.GetDecimal(index);
            }

            index = dr.GetOrdinal("Razon");
            if (!dr.IsDBNull(index))
            {
                item.Razon = dr.GetString(index);
            }
            index = dr.GetOrdinal("Calificacionanioregistro");
            if (!dr.IsDBNull(index))
            {
                item.Calificacionanioregistro = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Calificacionmesregistro");
            if (!dr.IsDBNull(index))
            {
                item.Calificacionmesregistro = dr.GetInt32(index);
            }

            return item;
        }
    }
}
