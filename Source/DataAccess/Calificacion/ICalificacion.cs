﻿using System;
using System.Collections.Generic;

namespace DataAccess.Calificacion
{
    public interface ICalificacion
    {
        int Guardar(Entity.Calificacion item);
        void Actualizar(Entity.Calificacion item);
        List<Entity.Calificacion> ObtenerTodos();
        Entity.Calificacion ObtenerByInternoId(int id);
        Entity.Calificacion ObtenerById(object[] parametros);
        Entity.Calificacion ObtenerByTrackingId(object[] parametros);
    }
}
