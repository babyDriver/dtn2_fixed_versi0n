﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Calificacion
{
    public class ObtenerByTrackingId : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Calificacion_GetByTrackingId_SP");
            db.AddInParameter(cmd, "_Id", DbType.String, parametros[0].ToString());            
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, Convert.ToBoolean(parametros[1]));
            return cmd;
        }
    }
}
