﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Calificacion
{
    public class Actualizar : IUpdateFactory<Entity.Calificacion>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.Calificacion item)
        {
            DbCommand cmd = db.GetStoredProcCommand("Calificacion_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);                        
            db.AddInParameter(cmd, "_SituacionId", DbType.Int32, item.SituacionId);
            db.AddInParameter(cmd, "_InstitucionId", DbType.Int32, item.InstitucionId);
            db.AddInParameter(cmd, "_Fundamento", DbType.String, item.Fundamento);
            db.AddInParameter(cmd, "_TrabajoSocial", DbType.Boolean, item.TrabajoSocial);
            db.AddInParameter(cmd, "_TotalHoras", DbType.Int32, item.TotalHoras);
            //db.AddInParameter(cmd, "_MaxHoras", DbType.String, item.MaxHoras);
            db.AddInParameter(cmd, "_SoloArresto", DbType.Boolean, item.SoloArresto);
            db.AddInParameter(cmd, "_TotalDeMultas", DbType.Decimal, item.TotalDeMultas);
            db.AddInParameter(cmd, "_Agravante", DbType.Decimal, item.Agravante);
            db.AddInParameter(cmd, "_Ajuste", DbType.Decimal, item.Ajuste);
            db.AddInParameter(cmd, "_TotalAPagar", DbType.Decimal, item.TotalAPagar);
            db.AddInParameter(cmd, "_Razon", DbType.String, item.Razon);
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, item.Activo);        
            db.AddInParameter(cmd, "_InternoId", DbType.Int32, item.InternoId);

            return cmd;
        }
    }
}
