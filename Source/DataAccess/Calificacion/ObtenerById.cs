﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.Calificacion
{
    public class ObtenerById : ISelectFactory<object[]>
    {
        public DbCommand ConstructSelectCommand(Database db, object[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("Calificacion_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, Convert.ToInt32(parametros[0]));
            db.AddInParameter(cmd, "_Activo", DbType.Boolean, Convert.ToBoolean(parametros[1]));
            return cmd;
        }
    }
}
