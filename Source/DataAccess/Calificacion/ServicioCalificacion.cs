﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.Calificacion
{
    public class ServicioCalificacion : ClsAbstractService<Entity.Calificacion>, ICalificacion
    {
        public ServicioCalificacion(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Calificacion> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaCalificacion(), new Entity.NullClass());
        }

        public int Guardar(Entity.Calificacion item)
        {
            IInsertFactory<Entity.Calificacion> insert = new Guardar();
            return Insert(insert, item);
        }

        public Entity.Calificacion ObtenerByTrackingId(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaCalificacion(), parametros);
        }

        public Entity.Calificacion ObtenerById(object[] parametros)
        {
            ISelectFactory<object[]> select = new ObtenerById();
            return GetByKey(select, new FabricaCalificacion(), parametros);
        }

        public Entity.Calificacion ObtenerByInternoId(int id)
        {
            ISelectFactory<int> select = new ObtenerByInternoId();
            return GetByKey(select, new FabricaCalificacion(), id);
        }

        public void Actualizar(Entity.Calificacion item)
        {
            IUpdateFactory<Entity.Calificacion> update = new Actualizar();
            Update(update, item);
        }
    }
}
