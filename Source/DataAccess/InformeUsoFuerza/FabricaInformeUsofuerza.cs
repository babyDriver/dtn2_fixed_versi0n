﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.InformeUsoFuerza
{
    public class FabricaInformeUsofuerza : IEntityFactory<Entity.InformeUsoFuerza>
    {
        public Entity.InformeUsoFuerza ConstructEntity(IDataReader dr)
        {
            var item = new Entity.InformeUsoFuerza();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetString(index);
            }

            index = dr.GetOrdinal("Primerapellido");
            if (!dr.IsDBNull(index))
            {
                item.Primerapellido = dr.GetString(index);
            }
            index = dr.GetOrdinal("Segundoapellido");
            if (!dr.IsDBNull(index))
            {
                item.Segundoapellido = dr.GetString(index);
            }
            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                item.Nombre = dr.GetString(index);
            }
            index = dr.GetOrdinal("Adscripcion");
            if (!dr.IsDBNull(index))
            {
                item.Adscripcion = dr.GetString(index);
            }
            index = dr.GetOrdinal("Cargo");
            if (!dr.IsDBNull(index))
            {
                item.Cargo = dr.GetString(index);
            }

            index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                item.DetalledetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FechaHora");
            if (!dr.IsDBNull(index))
            {
                item.Fechahora = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                item.Activo = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Habilitado");
            if (!dr.IsDBNull(index))
            {
                item.Habilitado = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Creadopor");
            if (!dr.IsDBNull(index))
            {
                item.Creadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Autoridadeslesionadas");
            if (!dr.IsDBNull(index))
            {
                item.Autoridadeslesionadas = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Autoridadesfallecidas");
            if (!dr.IsDBNull(index))
            {
                item.Autoridadesfallecidas = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Personaslesionadas");
            if (!dr.IsDBNull(index))
            {
                item.Personaslesionadas = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Personasfallecidas");
            if (!dr.IsDBNull(index))
            {
                item.Personasfallecidas = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Reduccionfisicademovimientos");
            if (!dr.IsDBNull(index))
            {
                item.Reduccionfisicademovimientos = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Utilizaciondearmasincapacitantemenosletales");
            if (!dr.IsDBNull(index))
            {
                item.Utilizaciondearmasincapacitantemenosletales = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Utilizaciondearmasdefuego");
            if (!dr.IsDBNull(index))
            {
                item.Utilizaciondearmasdefuego = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("DescripcionConducta");
            if (!dr.IsDBNull(index))
            {
                item.DescripcionConducta = dr.GetString(index);
            }

            index = dr.GetOrdinal("Brindosolicitoasistenciamedica");
            if (!dr.IsDBNull(index))
            {
                item.Brindosolicitoasistenciamedica = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Especifique");
            if (!dr.IsDBNull(index))
            {
                item.Especifique = dr.GetString(index);
            }

            index = dr.GetOrdinal("Firma");
            if (!dr.IsDBNull(index))
            {
                item.Firma = dr.GetString(index);
            }

            index = dr.GetOrdinal("Firma2");
            if (!dr.IsDBNull(index))
            {
                item.Firma2 = dr.GetString(index);
            }

            return item;
        }
    }
}
