﻿using System.Data;
using System.Data.Common;
using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DataAccess.InformeUsoFuerza
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("informeusofuerza_GetById_SP");
            db.AddOutParameter(cmd, "_Id", DbType.Int32, Id);
            return cmd;
        }
    }
}
