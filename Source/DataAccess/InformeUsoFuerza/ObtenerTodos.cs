﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.InformeUsoFuerza
{
    public class ObtenerTodos : ISelectFactory<Entity.NullClass>
    {
        public DbCommand ConstructSelectCommand(Database db, NullClass Id)
        {
            DbCommand cmd = db.GetStoredProcCommand("informeusofuerza_GetAll_SP");
            return cmd;
        }
    }
}
