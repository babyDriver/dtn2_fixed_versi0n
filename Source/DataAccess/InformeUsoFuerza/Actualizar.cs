﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.InformeUsoFuerza
{
    public class Actualizar : IUpdateFactory<Entity.InformeUsoFuerza>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.InformeUsoFuerza informeUsoFuerza)
        {
            DbCommand cmd = db.GetStoredProcCommand("informeusofuerza_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, informeUsoFuerza.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, informeUsoFuerza.TrackingId);
            db.AddInParameter(cmd, "_Primerapellido", DbType.String, informeUsoFuerza.Primerapellido);
            db.AddInParameter(cmd, "_Segundoapellido", DbType.String, informeUsoFuerza.Segundoapellido);
            db.AddInParameter(cmd, "_Nombre", DbType.String, informeUsoFuerza.Nombre);
            db.AddInParameter(cmd, "_Adscripcion", DbType.String, informeUsoFuerza.Adscripcion);
            db.AddInParameter(cmd, "_Cargo", DbType.String, informeUsoFuerza.Cargo);
            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, informeUsoFuerza.DetalledetencionId);
            db.AddInParameter(cmd, "_FechaHora", DbType.DateTime, informeUsoFuerza.Fechahora);
            db.AddInParameter(cmd, "_Activo", DbType.Int32, informeUsoFuerza.Activo);
            db.AddInParameter(cmd, "_Habilitado", DbType.Int32, informeUsoFuerza.Habilitado);
            db.AddInParameter(cmd, "_Creadopor", DbType.Int32, informeUsoFuerza.Creadopor);
            db.AddInParameter(cmd, "_Autoridadeslesionadas", DbType.Int32, informeUsoFuerza.Autoridadeslesionadas);
            db.AddInParameter(cmd, "_Autoridadesfallecidas", DbType.Int32, informeUsoFuerza.Autoridadesfallecidas);
            db.AddInParameter(cmd, "_Personaslesionadas", DbType.Int32, informeUsoFuerza.Personaslesionadas);
            db.AddInParameter(cmd, "_Personasfallecidas", DbType.Int32, informeUsoFuerza.Personasfallecidas);
            db.AddInParameter(cmd, "_Reduccionfisicademovimientos", DbType.Boolean, informeUsoFuerza.Reduccionfisicademovimientos);
            db.AddInParameter(cmd, "_Utilizaciondearmasincapacitantemenosletales", DbType.Boolean, informeUsoFuerza.Utilizaciondearmasincapacitantemenosletales);
            db.AddInParameter(cmd, "_Utilizaciondearmasdefuego", DbType.Boolean, informeUsoFuerza.Utilizaciondearmasdefuego);
            db.AddInParameter(cmd, "_DescripcionConducta", DbType.String, informeUsoFuerza.DescripcionConducta);
            db.AddInParameter(cmd, "_Brindosolicitoasistenciamedica", DbType.Boolean, informeUsoFuerza.Brindosolicitoasistenciamedica);
            db.AddInParameter(cmd, "_Especifique", DbType.String, informeUsoFuerza.Especifique);
            db.AddInParameter(cmd, "_Firma", DbType.String, informeUsoFuerza.Firma);
            db.AddInParameter(cmd, "_Firma2", DbType.String, informeUsoFuerza.Firma2);
            return cmd;
        }
    }
}
