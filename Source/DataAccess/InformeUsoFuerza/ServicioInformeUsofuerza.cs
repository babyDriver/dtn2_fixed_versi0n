﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;

namespace DataAccess.InformeUsoFuerza
{
    public class ServicioInformeUsofuerza : ClsAbstractService<Entity.InformeUsoFuerza>, IInformeUsoFuerza
    {


        public ServicioInformeUsofuerza(string dataBase)
            : base(dataBase)
        {
        }

        public void Actualizar(Entity.InformeUsoFuerza informeUsoFuerza)
        {
            IUpdateFactory<Entity.InformeUsoFuerza> update = new Actualizar();
            Update(update, informeUsoFuerza);
        }

        public new List<Entity.InformeUsoFuerza> GetAll()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaInformeUsofuerza(), new Entity.NullClass());
        }

        public Entity.InformeUsoFuerza GetbyId(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaInformeUsofuerza(), Id);
        }

        public int Guardar(Entity.InformeUsoFuerza informeUsoFuerza)
        {
            IInsertFactory<Entity.InformeUsoFuerza> insert = new Guardar();
            return Insert(insert, informeUsoFuerza);
        }
    }
}
