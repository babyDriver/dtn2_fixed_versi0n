﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.InformeUsoFuerza
{
    public interface IInformeUsoFuerza
    {
        int Guardar(Entity.InformeUsoFuerza informeUsoFuerza);
        void Actualizar(Entity.InformeUsoFuerza informeUsoFuerza);
        Entity.InformeUsoFuerza GetbyId(int Id);
        List<Entity.InformeUsoFuerza> GetAll();

    }

}
