﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.ObservacionesBiometricos
{
    class FabricaObservacionesBiometricos : IEntityFactory<Entity.ObservacionesBiometricos>
    {
        public Entity.ObservacionesBiometricos ConstructEntity(IDataReader dr)
        {
            var item = new Entity.ObservacionesBiometricos();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetenidoId");
            if (!dr.IsDBNull(index))
            {
                item.DetenidoId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Observaciones");
            if (!dr.IsDBNull(index))
            {
                item.Observaciones = dr.GetString(index);
            }

            return item;
        }
    }
}
