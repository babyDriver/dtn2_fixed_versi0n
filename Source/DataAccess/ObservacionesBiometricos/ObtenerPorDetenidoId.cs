﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace DataAccess.ObservacionesBiometricos
{
    public class ObtenerPorDetenidoId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int DetenidoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("observacionesbiometricos_Get_byDetenidoId_SP");
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, DetenidoId);
            return cmd;
        }
    }

}
