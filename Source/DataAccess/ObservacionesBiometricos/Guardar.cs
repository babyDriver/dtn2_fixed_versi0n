﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;

namespace DataAccess.ObservacionesBiometricos
{
    public class Guardar : IInsertFactory<Entity.ObservacionesBiometricos>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.ObservacionesBiometricos entity)
        {
            DbCommand cmd = db.GetStoredProcCommand("observacionesbiometricos_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, entity.DetenidoId);
            db.AddInParameter(cmd, "_Observaciones", DbType.String, entity.Observaciones);
            return cmd;
        }
    }
}
