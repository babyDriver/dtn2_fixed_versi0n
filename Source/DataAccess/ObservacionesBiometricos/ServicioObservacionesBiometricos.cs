﻿using DataAccess.Interfaces;
using Entity;
using System;
using System.Collections.Generic;


namespace DataAccess.ObservacionesBiometricos
{
    public class ServicioObservacionesBiometricos : ClsAbstractService<Entity.ObservacionesBiometricos>, IObservacionesBiometricos
    {
        public ServicioObservacionesBiometricos(string dataBase)
           : base(dataBase)
        {
        }

        public void Actualizar(Entity.ObservacionesBiometricos observacionesBiometricos)
        {
            IUpdateFactory<Entity.ObservacionesBiometricos> update = new Actualizar();
            Update(update, observacionesBiometricos);
        }

        public Entity.ObservacionesBiometricos GetByDetenidoId(int DetenidoId )
        {
            ISelectFactory<int> select = new ObtenerPorDetenidoId();
            return GetByKey(select, new FabricaObservacionesBiometricos(), DetenidoId);
        }

        public int Guardar(Entity.ObservacionesBiometricos observacionesBiometricos)
        {
            IInsertFactory<Entity.ObservacionesBiometricos> insert = new Guardar();
            return Insert(insert, observacionesBiometricos);
        }
    }
}
