﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ObservacionesBiometricos
{
    public interface IObservacionesBiometricos
    {
        int Guardar(Entity.ObservacionesBiometricos observacionesBiometricos);
        void Actualizar(Entity.ObservacionesBiometricos observacionesBiometricos);
        Entity.ObservacionesBiometricos GetByDetenidoId(int DetenidoId);
    }
}
