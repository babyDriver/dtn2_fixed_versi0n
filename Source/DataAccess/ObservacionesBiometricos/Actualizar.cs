﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.ObservacionesBiometricos
{
    public class Actualizar : IUpdateFactory<Entity.ObservacionesBiometricos>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.ObservacionesBiometricos item)
        {
            DbCommand cmd = db.GetStoredProcCommand("observacionesbiometricos_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, item.Id);
            db.AddInParameter(cmd, "_DetenidoId", DbType.Int32, item.DetenidoId);
            db.AddInParameter(cmd, "_Observaciones", DbType.String, item.Observaciones);
            return cmd;
        }
    }
}
