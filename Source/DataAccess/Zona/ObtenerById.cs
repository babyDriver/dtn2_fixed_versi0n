﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Zona
{
    public class ObtenerById : ISelectFactory<int>
    {

        public DbCommand ConstructSelectCommand(Database db, int estadoId)
        {
            DbCommand cmd = db.GetStoredProcCommand("[Zona_GetById_SP]");
            db.AddInParameter(cmd, "@Id", DbType.Int32, estadoId);
            return cmd;
        }

    }
}
