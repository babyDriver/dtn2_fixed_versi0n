﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.Zona
{
    public class ServicioZona : ClsAbstractService<Entity.Zona>, IZona
    {
        public ServicioZona(string dataBase)
            : base(dataBase)
        {
        }

        public List<Entity.Zona> ObtenerActivos(bool activo)
        {
            ISelectFactory<bool> select = new ObtenerActivos();
            return GetAll(select, new FabricaZona(), activo);
        }

        public Entity.Zona ObtenerPorId(int estadoId)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaZona(), estadoId);
        }

        public Entity.Zona ObtenerPorTrackingId(System.Guid trackingId)
        {
            ISelectFactory<System.Guid> select = new ObtenerByTrackingId();
            return GetByKey(select, new FabricaZona(), trackingId);
        }
    }
}