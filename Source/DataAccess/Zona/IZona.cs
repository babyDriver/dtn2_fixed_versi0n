﻿using System;
using System.Collections.Generic;

namespace DataAccess.Zona
{
    public interface IZona
    {
        List<Entity.Zona> ObtenerActivos(bool activo);
        Entity.Zona ObtenerPorId(int estadoId);
        Entity.Zona ObtenerPorTrackingId(System.Guid trackingId);
    }
}