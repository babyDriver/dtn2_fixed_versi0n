﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.Zona
{
    public class FabricaZona : IEntityFactory<Entity.Zona>
    {
        public Entity.Zona ConstructEntity(IDataReader dr)
        {
            var zona = new Entity.Zona();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                zona.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                zona.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Descripcion");
            if (!dr.IsDBNull(index))
            {
                zona.Descripcion = dr.GetString(index);
            }

            index = dr.GetOrdinal("FechaRegistro");
            if (!dr.IsDBNull(index))
            {
                zona.FechaRegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("RegistradoPor");
            if (!dr.IsDBNull(index))
            {
                zona.RegistradoPor = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("FechaModificacion");
            if (!dr.IsDBNull(index))
            {
                zona.FechaModificacion = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("ModificadoPor");
            if (!dr.IsDBNull(index))
            {
                zona.ModificadoPor = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("Activo");
            if (!dr.IsDBNull(index))
            {
                zona.Activo = dr.GetBoolean(index);
            }

            return zona;
        }
    }
}
