﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Zona
{
    public class ObtenerActivos : ISelectFactory<bool>
    {
        public DbCommand ConstructSelectCommand(Database db, bool activo)
        {
            DbCommand cmd = db.GetStoredProcCommand("[Zona_GetActivos_SP]");
            db.AddInParameter(cmd, "@Activo", DbType.Boolean, activo);
            return cmd;
        }
    }
}
