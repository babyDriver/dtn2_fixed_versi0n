﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.Zona
{
    public class ObtenerByTrackingId : ISelectFactory<System.Guid>
    {
        public DbCommand ConstructSelectCommand(Database db, System.Guid trackingId)
        {
            DbCommand cmd = db.GetStoredProcCommand("[Zona_GetByTrackingId_SP]");
            db.AddInParameter(cmd, "@TrackingId", DbType.Guid, trackingId);
            return cmd;
        }
    }
}