﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.CertificadoMedicoPsicoFisiologico
{
    public class Actualizar : IUpdateFactory<Entity.CertificadoMedicoPsicoFisiologico>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsicoFisiologico)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_medico_psicofisiologico_Update");
            db.AddInParameter(cmd, "_Id", DbType.Int32, certificadoMedicoPsicoFisiologico.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.String, certificadoMedicoPsicoFisiologico.TrackingId);
            db.AddInParameter(cmd, "_AlientoId", DbType.Int32, certificadoMedicoPsicoFisiologico.AlientoId);
            db.AddInParameter(cmd, "_MarchaId", DbType.Int32, certificadoMedicoPsicoFisiologico.MarchaId);
            db.AddInParameter(cmd, "_ActitudId", DbType.Int32, certificadoMedicoPsicoFisiologico.ActitudId);
            db.AddInParameter(cmd, "_AtencionId", DbType.Int32, certificadoMedicoPsicoFisiologico.AtencionId);
            db.AddInParameter(cmd, "_Cavidad_oralId", DbType.Int32, certificadoMedicoPsicoFisiologico.Cavidad_oralId);
            db.AddInParameter(cmd, "_PupilaId", DbType.Int32, certificadoMedicoPsicoFisiologico.PupilaId);
            db.AddInParameter(cmd, "_Reflejo_pupilarId", DbType.Int32, certificadoMedicoPsicoFisiologico.Reflejo_pupilarId);
            db.AddInParameter(cmd, "_ConjuntivaId", DbType.Int32, certificadoMedicoPsicoFisiologico.ConjuntivaId);
            db.AddInParameter(cmd, "_LenguajeId", DbType.Int32, certificadoMedicoPsicoFisiologico.LenguajeId);
            db.AddInParameter(cmd, "_RombergId", DbType.Int32, certificadoMedicoPsicoFisiologico.RombergId);
            db.AddInParameter(cmd, "_Ojos_abiertos_dedo_dedoId", DbType.Int32, certificadoMedicoPsicoFisiologico.Ojos_abiertos_dedo_dedoId);
            db.AddInParameter(cmd, "_Ojos_cerrados_dedo_dedoId", DbType.Int32, certificadoMedicoPsicoFisiologico.Ojos_cerrados_dedo_dedoId);
            db.AddInParameter(cmd, "_Ojos_abiertos_dedo_narizId", DbType.Int32, certificadoMedicoPsicoFisiologico.Ojos_abiertos_dedo_narizId);
            db.AddInParameter(cmd, "_Ojos_cerrados_dedo_narizId", DbType.Int32, certificadoMedicoPsicoFisiologico.Ojos_cerrados_dedo_narizId);
            db.AddInParameter(cmd, "_TA", DbType.String, certificadoMedicoPsicoFisiologico.TA);
            db.AddInParameter(cmd, "_FC", DbType.String, certificadoMedicoPsicoFisiologico.FC);
            db.AddInParameter(cmd, "_FR", DbType.String, certificadoMedicoPsicoFisiologico.FR);
            db.AddInParameter(cmd, "_Pulso", DbType.String, certificadoMedicoPsicoFisiologico.Pulso);
            db.AddInParameter(cmd, "_No_valorabe", DbType.Int16, certificadoMedicoPsicoFisiologico.No_valorabe);
            db.AddInParameter(cmd, "_OrientacionId", DbType.Int32, certificadoMedicoPsicoFisiologico.OrientacionId);
            db.AddInParameter(cmd, "_Observacion_orientacion", DbType.String, certificadoMedicoPsicoFisiologico.Observacion_orientacion);
            db.AddInParameter(cmd, "_ConclusionId", DbType.Int32, certificadoMedicoPsicoFisiologico.ConclusionId);
            db.AddInParameter(cmd, "_Sustento_toxicologico", DbType.Int16, certificadoMedicoPsicoFisiologico.Sustento_toxicologico);
            db.AddInParameter(cmd, "_Observacion_extra", DbType.String, certificadoMedicoPsicoFisiologico.Observacion_extra);
            db.AddInParameter(cmd, "_FechaValoracion", DbType.DateTime, certificadoMedicoPsicoFisiologico.FechaValoracion);
            db.AddInParameter(cmd, "_FechaRegistro", DbType.DateTime, certificadoMedicoPsicoFisiologico.FechaRegistro);
            db.AddInParameter(cmd, "_Registradopor", DbType.Int32, certificadoMedicoPsicoFisiologico.Registradopor);
            db.AddInParameter(cmd, "_Modificadopor", DbType.Int32, certificadoMedicoPsicoFisiologico.Modificadopor);
            db.AddInParameter(cmd, "_Fechaultimamodificacion", DbType.DateTime, certificadoMedicoPsicoFisiologico.Fechaultimamodificacion);

            return cmd;
        }
    }
}
