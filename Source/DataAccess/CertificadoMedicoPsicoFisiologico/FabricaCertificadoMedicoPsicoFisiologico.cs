﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.CertificadoMedicoPsicoFisiologico
{
    public class FabricaCertificadoMedicoPsicoFisiologico : IEntityFactory<Entity.CertificadoMedicoPsicoFisiologico>
    {
        public Entity.CertificadoMedicoPsicoFisiologico ConstructEntity(IDataReader dr)
        {
            var item = new Entity.CertificadoMedicoPsicoFisiologico();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                item.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                item.TrackingId = dr.GetString(index);
            }

            index = dr.GetOrdinal("AlientoId");
            if (!dr.IsDBNull(index))
            {
                item.AlientoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("MarchaId");
            if (!dr.IsDBNull(index))
            {
                item.MarchaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("ActitudId");
            if (!dr.IsDBNull(index))
            {
                item.ActitudId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("AtencionId");
            if (!dr.IsDBNull(index))
            {
                item.AtencionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Cavidad_oralId");
            if (!dr.IsDBNull(index))
            {
                item.Cavidad_oralId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("PupilaId");
            if (!dr.IsDBNull(index))
            {
                item.PupilaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Reflejo_pupilarId");
            if (!dr.IsDBNull(index))
            {
                item.Reflejo_pupilarId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("ConjuntivaId");
            if (!dr.IsDBNull(index))
            {
                item.ConjuntivaId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("LenguajeId");
            if (!dr.IsDBNull(index))
            {
                item.LenguajeId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("RombergId");
            if (!dr.IsDBNull(index))
            {
                item.RombergId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Ojos_abiertos_dedo_dedoId");
            if (!dr.IsDBNull(index))
            {
                item.Ojos_abiertos_dedo_dedoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Ojos_cerrados_dedo_dedoId");
            if (!dr.IsDBNull(index))
            {
                item.Ojos_cerrados_dedo_dedoId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Ojos_abiertos_dedo_narizId");
            if (!dr.IsDBNull(index))
            {
                item.Ojos_abiertos_dedo_narizId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Ojos_cerrados_dedo_narizId");
            if (!dr.IsDBNull(index))
            {
                item.Ojos_cerrados_dedo_narizId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("TA");
            if (!dr.IsDBNull(index))
            {
                item.TA = dr.GetString(index);
            }
            index = dr.GetOrdinal("FC");
            if (!dr.IsDBNull(index))
            {
                item.FC = dr.GetString(index);
            }
            index = dr.GetOrdinal("FR");
            if (!dr.IsDBNull(index))
            {
                item.FR = dr.GetString(index);
            }
            index = dr.GetOrdinal("Pulso");
            if (!dr.IsDBNull(index))
            {
                item.Pulso = dr.GetString(index);
            }
            index = dr.GetOrdinal("No_valorabe");
            if (!dr.IsDBNull(index))
            {
                item.No_valorabe = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("OrientacionId");
            if (!dr.IsDBNull(index))
            {
                item.OrientacionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Observacion_orientacion");
            if (!dr.IsDBNull(index))
            {
                item.Observacion_orientacion = dr.GetString(index);
            }
            index = dr.GetOrdinal("ConclusionId");
            if (!dr.IsDBNull(index))
            {
                item.ConclusionId = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("Sustento_toxicologico");
            if (!dr.IsDBNull(index))
            {
                item.Sustento_toxicologico = dr.GetInt16(index);
            }
            index = dr.GetOrdinal("Observacion_extra");
            if (!dr.IsDBNull(index))
            {
                item.Observacion_extra = dr.GetString(index);
            }
            index = dr.GetOrdinal("FechaValoracion");
            if (!dr.IsDBNull(index))
            {
                item.FechaValoracion = dr.GetDateTime(index);
            }
            index = dr.GetOrdinal("Folio");
            if (!dr.IsDBNull(index))
            {
                item.Folio = dr.GetInt32(index);
            }
            index = dr.GetOrdinal("FechaRegistro");
            if (!dr.IsDBNull(index))
            {
                item.FechaRegistro = dr.GetDateTime(index);
            }

            index = dr.GetOrdinal("Registradopor");
            if (!dr.IsDBNull(index))
            {
                item.Registradopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Modificadopor");
            if (!dr.IsDBNull(index))
            {
                item.Modificadopor = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fechaultimamodificacion");
            if (!dr.IsDBNull(index))
            {
                item.Fechaultimamodificacion = dr.GetDateTime(index);
            }
            return item;
        }
    }
}
