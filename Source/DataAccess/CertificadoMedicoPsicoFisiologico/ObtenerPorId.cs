﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DataAccess.CertificadoMedicoPsicoFisiologico
{
    public class ObtenerPorId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_medico_psicofisiologico_ObtenerPorId");
            db.AddInParameter(cmd, "_Id", DbType.Int32, id);
            return cmd;
        }
    }
}
