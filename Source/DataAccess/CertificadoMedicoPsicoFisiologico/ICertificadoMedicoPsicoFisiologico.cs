﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CertificadoMedicoPsicoFisiologico
{
    public interface ICertificadoMedicoPsicoFisiologico
    {
        List<Entity.CertificadoMedicoPsicoFisiologico> ObtenerTodos();
        Entity.CertificadoMedicoPsicoFisiologico ObtenerById(int Id);
      
        Entity.CertificadoMedicoPsicoFisiologico ObtenerByTrackingId(string trackingid);
        int Guardar(Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsico);
        void Actualizar(Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsico);
    }
}
