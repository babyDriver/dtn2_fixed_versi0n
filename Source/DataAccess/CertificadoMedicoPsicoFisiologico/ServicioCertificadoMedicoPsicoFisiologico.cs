﻿using DataAccess.Interfaces;
using System.Collections.Generic;

namespace DataAccess.CertificadoMedicoPsicoFisiologico
{
    public class ServicioCertificadoMedicoPsicoFisiologico : ClsAbstractService<Entity.CertificadoMedicoPsicoFisiologico>, ICertificadoMedicoPsicoFisiologico
    {
        public ServicioCertificadoMedicoPsicoFisiologico(string dataBase)
            : base(dataBase)
        {
        }
        public void Actualizar(Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsico)
        {
            IUpdateFactory<Entity.CertificadoMedicoPsicoFisiologico> update = new Actualizar();
            Update(update, certificadoMedicoPsico);
        }

        public int Guardar(Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsico)
        {
            IInsertFactory<Entity.CertificadoMedicoPsicoFisiologico> insert = new Guardar();
            return Insert(insert, certificadoMedicoPsico);
        }

        public Entity.CertificadoMedicoPsicoFisiologico ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerPorId();
            return GetByKey(select, new FabricaCertificadoMedicoPsicoFisiologico(), Id);
        }

        public Entity.CertificadoMedicoPsicoFisiologico ObtenerByTrackingId(string trackingid)
        {
            ISelectFactory<string> select = new ObtenerPorTrackingId();
            return GetByKey(select, new FabricaCertificadoMedicoPsicoFisiologico(), trackingid);
        }

        public List<Entity.CertificadoMedicoPsicoFisiologico> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaCertificadoMedicoPsicoFisiologico(), new Entity.NullClass());
        }
    }
}
