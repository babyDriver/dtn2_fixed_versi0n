﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;
namespace DataAccess.CertificadoMedicoPsicoFisiologico
{
    public class ObtenerPorTrackingId : ISelectFactory<string>
    {
        public DbCommand ConstructSelectCommand(Database db, string tracking)
        {
            DbCommand cmd = db.GetStoredProcCommand("certificado_medico_psicofisiologico_ObtenerPorTrackingId");
            db.AddInParameter(cmd, "_TrackingId", DbType.String, tracking);
            return cmd;
        }
    }
}
