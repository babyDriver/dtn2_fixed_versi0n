﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.WSAEvtD
{
    public class ObtenerById : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int Id_Dtnd)
        {
            DbCommand cmd = db.GetStoredProcCommand("WSAEvtD_GetById_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, Id_Dtnd);
            return cmd;
        }
    }
}
