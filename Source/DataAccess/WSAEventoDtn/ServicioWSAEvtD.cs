﻿using DataAccess.Interfaces;
using Entity;

namespace DataAccess.WSAEvtD
{
    public class ServicioWSAEvtD : ClsAbstractService<Entity.WSAEvtDtnd>, IWSAEvtD
	{
        public ServicioWSAEvtD(string dataBase)
            : base(dataBase)
        {
        }

		public Entity.WSAEvtDtnd ObtenerPorId(int Id_Dtnd)
		{
			ISelectFactory<int> select = new ObtenerById();
			return GetByKey(select, new FabricaEvtD(), Id_Dtnd);
		}
	}
}
