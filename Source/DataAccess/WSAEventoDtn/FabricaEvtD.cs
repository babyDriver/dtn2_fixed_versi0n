﻿using DataAccess.Interfaces;
using System.Data;

namespace DataAccess.WSAEvtD
{
    public class FabricaEvtD : IEntityFactory<Entity.WSAEvtDtnd>
    {
		public Entity.WSAEvtDtnd ConstructEntity(IDataReader dr)
		{
			var referencia = new Entity.WSAEvtDtnd();

			//var index = dr.GetOrdinal("Id");
			//if (!dr.IsDBNull(index))
			//{
			//	referencia.Id = dr.GetInt32(index);
			//}
			var index = dr.GetOrdinal("Fecha");
			if (!dr.IsDBNull(index))
			{
				referencia.Fecha = dr.GetDateTime(index);
			}
			index = dr.GetOrdinal("Origen");
			if (!dr.IsDBNull(index))
			{
				referencia.Origen = dr.GetString(index);
			}
			//index = dr.GetOrdinal("IdOrigen");
			//if (!dr.IsDBNull(index))
			//{
			//	referencia.IdOrigen = dr.GetInt32(index);
			//}
			  index = dr.GetOrdinal("Institucion");
			if (!dr.IsDBNull(index))
			{
				referencia.IdInstitucion = dr.GetInt32(index);
			}			
			index = dr.GetOrdinal("Calle");
			if (!dr.IsDBNull(index))
			{
				referencia.Calle = dr.GetString(index);
			}
			index = dr.GetOrdinal("Localidad");
			if (!dr.IsDBNull(index))
			{
				referencia.CodigoPostal = dr.GetString(index);
			}
			//index = dr.GetOrdinal("CodigoPostal");
			//if (!dr.IsDBNull(index))
			//{
			//	referencia.CodigoPostal = dr.GetString(index);
			//}
			index = dr.GetOrdinal("Latitud");
			if (!dr.IsDBNull(index))
			{
				referencia.Latitud = dr.GetString(index);
			}
			index = dr.GetOrdinal("Longitud");
			if (!dr.IsDBNull(index))
			{
				referencia.Longitud = dr.GetString(index);
			}
			index = dr.GetOrdinal("EstadoId");
			if (!dr.IsDBNull(index))
			{
				referencia.IdEstado = dr.GetInt32(index);
			}
			index = dr.GetOrdinal("MunicipioId");
			if (!dr.IsDBNull(index))
			{
				referencia.IdMunicipio = dr.GetInt32(index);
			}
			index = dr.GetOrdinal("ColoniaId");
			if (!dr.IsDBNull(index))
			{
				referencia.IdColonia = dr.GetInt32(index);
			}
			index = dr.GetOrdinal("NoDetenidos");
			if (!dr.IsDBNull(index))
			{
				referencia.NoDetenidos = dr.GetInt32(index);
			}
			index = dr.GetOrdinal("Descripcion");
			if (!dr.IsDBNull(index))
			{
				referencia.Descripcion = dr.GetString(index);
			}
			//double or nothing
			index = dr.GetOrdinal("Nombre");
			if (!dr.IsDBNull(index))
			{
				referencia.Nombre = dr.GetString(index);
			}
			index = dr.GetOrdinal("Paterno");
			if (!dr.IsDBNull(index))
			{
				referencia.Paterno = dr.GetString(index);
			}
			index = dr.GetOrdinal("Materno");
			if (!dr.IsDBNull(index))
			{
				referencia.Materno = dr.GetString(index);
			}
			index = dr.GetOrdinal("Alias");
			if (!dr.IsDBNull(index))
			{
				referencia.Alias = dr.GetString(index);
			}
			index = dr.GetOrdinal("Edad");
			if (!dr.IsDBNull(index))
			{
				referencia.Edad = dr.GetInt32(index);
			}
			index = dr.GetOrdinal("Sexo");
			if (!dr.IsDBNull(index))
			{
				referencia.IdSexo = dr.GetInt32(index);
			}
			return referencia;
		}
	}
}
