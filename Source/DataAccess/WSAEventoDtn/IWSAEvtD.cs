﻿namespace DataAccess.WSAEvtD
{
    public interface IWSAEvtD
	{
        Entity.WSAEvtDtnd ObtenerPorId(int ID);
    }
}
