﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using Entity;


namespace DataAccess.MovimientoLog
{
    public class Actualizar : IUpdateFactory<Entity.MovimientoLog>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.MovimientoLog movimiento)
        {
            DbCommand cmd = db.GetStoredProcCommand("MovimientoLog_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, movimiento.Id);

            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, movimiento.DetalledetencionId);

            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, movimiento.Fecha);
            db.AddInParameter(cmd, "_FechaAnt", DbType.DateTime, movimiento.FechaAnt);

            return cmd;

        }
    }
}
