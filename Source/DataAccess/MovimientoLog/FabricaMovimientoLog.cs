﻿using System.Data;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.MovimientoLog
{
    public class FabricaMovimientoLog : IEntityFactory<Entity.MovimientoLog>
    {
        public Entity.MovimientoLog ConstructEntity(IDataReader dr)
        {
            var movimiento = new Entity.MovimientoLog();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                movimiento.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("DetalledetencionId");
            if (!dr.IsDBNull(index))
            {
                movimiento.DetalledetencionId = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("Fecha");
            if (!dr.IsDBNull(index))
            {
                movimiento.Fecha = dr.GetDateTime(index);
            }

            return movimiento;
        }
    }
}
