﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.MovimientoLog
{
    public interface IMovimientoLog
    {
        void Actualizar(Entity.MovimientoLog item);
        Entity.MovimientoLog Obtener(Entity.MovimientoLog filtro);
    }
}
