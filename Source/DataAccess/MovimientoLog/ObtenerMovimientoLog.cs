﻿using DataAccess.Interfaces;
using Entity;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DataAccess.MovimientoLog
{
    public class ObtenerMovimientoLog : ISelectFactory<Entity.MovimientoLog>
    {
        public DbCommand ConstructSelectCommand(Database db, Entity.MovimientoLog filtro)
        {
            DbCommand cmd = db.GetStoredProcCommand("MovimientoLog_Get_SP");

            db.AddInParameter(cmd, "_DetalledetencionId", DbType.Int32, filtro.DetalledetencionId);

            db.AddInParameter(cmd, "_Fecha", DbType.DateTime, filtro.Fecha);

            return cmd;
        }
    }
}
