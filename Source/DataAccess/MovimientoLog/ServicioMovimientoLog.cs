﻿using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using Entity;

namespace DataAccess.MovimientoLog
{
    public class ServicioMovimientoLog : ClsAbstractService<Entity.MovimientoLog>, IMovimientoLog
    {
        public ServicioMovimientoLog(string dataBase)
           : base(dataBase)
        {
        }
        public void Actualizar(Entity.MovimientoLog item)
        {
            IUpdateFactory<Entity.MovimientoLog> update = new Actualizar();
            Update(update, item);
        }

        public Entity.MovimientoLog Obtener(Entity.MovimientoLog filtro)
        {
            ISelectFactory<Entity.MovimientoLog> select = new ObtenerMovimientoLog();
            return GetByKey(select, new FabricaMovimientoLog(), filtro);
        }
    }
}
