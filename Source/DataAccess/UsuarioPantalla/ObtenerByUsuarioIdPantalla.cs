﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System;

namespace DataAccess.UsuarioPantalla
{
    public class ObtenerByUsuarioIdPantalla : ISelectFactory<string[]>
    {
        public DbCommand ConstructSelectCommand(Database db, string[] parametros)
        {
            DbCommand cmd = db.GetStoredProcCommand("ObtenerPermisos_GetByUsuarioIdPantalla_SP");
            db.AddInParameter(cmd, "UsuarioId", DbType.Int32, Convert.ToInt32(parametros[0]));
            db.AddInParameter(cmd, "Pantalla", DbType.String, parametros[1]);
            return cmd;
        }
    }
}
