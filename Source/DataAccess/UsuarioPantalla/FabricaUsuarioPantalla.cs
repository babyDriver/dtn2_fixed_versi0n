﻿using DataAccess.Interfaces;


namespace DataAccess.UsuarioPantalla
{
    public class FabricaUsuarioPantalla : IEntityFactory<Entity.UsuarioPantalla>
    {
        public Entity.UsuarioPantalla ConstructEntity(System.Data.IDataReader dr)
        {
            var up = new Entity.UsuarioPantalla();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                up.Id = dr.GetInt32(index);
            }


            index = dr.GetOrdinal("Registrar");
            if (!dr.IsDBNull(index))
            {
                up.Registrar = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Nombre");
            if (!dr.IsDBNull(index))
            {
                up.Nombre = dr.GetString(index);
            }


            index = dr.GetOrdinal("Consultar");
            if (!dr.IsDBNull(index))
            {
                up.Consultar = dr.GetBoolean(index);
            }

            

            index = dr.GetOrdinal("Modificar");
            if (!dr.IsDBNull(index))
            {
                up.Modificar = dr.GetBoolean(index);
            }

            index = dr.GetOrdinal("Eliminar");
            if (!dr.IsDBNull(index))
            {
                up.Eliminar = dr.GetBoolean(index);
            }




            return up;
        }
    }
}
