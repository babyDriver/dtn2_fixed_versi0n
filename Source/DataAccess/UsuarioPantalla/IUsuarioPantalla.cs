﻿using System;
using System.Collections.Generic;

namespace DataAccess.UsuarioPantalla
{
    interface IUsuarioPantalla
    {
        Entity.UsuarioPantalla ObtenerByUsuarioIdPantalla(string[] id);
    }
}
