﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.UsuarioPantalla
{
    public class ServicioUsuarioPantalla : ClsAbstractService<Entity.UsuarioPantalla>, IUsuarioPantalla
    {
        public ServicioUsuarioPantalla(string dataBase)
            : base(dataBase)
        {
        }

        public Entity.UsuarioPantalla ObtenerByUsuarioIdPantalla(string[] Id)
        {
            ISelectFactory<string[]> select = new ObtenerByUsuarioIdPantalla();
            return GetByKey(select, new FabricaUsuarioPantalla(), Id);
        }

       
    }
}
