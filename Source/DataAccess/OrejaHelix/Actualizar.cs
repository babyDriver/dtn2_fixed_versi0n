﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.OrejaHelix
{
    public class Actualizar : IUpdateFactory<Entity.OrejaHelix>
    {
        public DbCommand ConstructUpdateCommand(Database db, Entity.OrejaHelix oreja)
        {
            DbCommand cmd = db.GetStoredProcCommand("Oreja_Helix_Update_SP");
            db.AddInParameter(cmd, "_Id", DbType.Int32, oreja.Id);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, oreja.TrackingId);
            db.AddInParameter(cmd, "_FormaO", DbType.Int32, oreja.FormaO);
            db.AddInParameter(cmd, "_ContornoH", DbType.Int32, oreja.ContornoH);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, oreja.AntropometriaId);

            if (oreja.OriginalO != null)
                db.AddInParameter(cmd, "_OriginalO", DbType.Int32, oreja.OriginalO);
            else
                db.AddInParameter(cmd, "_OriginalO", DbType.Int32, null);

            if (oreja.SuperiorH != null)
                db.AddInParameter(cmd, "_SuperiorH", DbType.Int32, oreja.SuperiorH);
            else
                db.AddInParameter(cmd, "_SuperiorH", DbType.Int32, null);

            if (oreja.PosteriorH != null)
                db.AddInParameter(cmd, "_PosteriorH", DbType.Int32, oreja.PosteriorH);
            else
                db.AddInParameter(cmd, "_PosteriorH", DbType.Int32, null);

            if (oreja.AdherenciaH != null)
                db.AddInParameter(cmd, "_AdherenciaH", DbType.Int32, oreja.AdherenciaH);
            else
                db.AddInParameter(cmd, "_AdherenciaH", DbType.Int32, null);


            return cmd;
        }
    }
}
