﻿using DataAccess.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DataAccess.OrejaHelix
{
    public class ObtenerByAntropometriaId : ISelectFactory<int>
    {
        public DbCommand ConstructSelectCommand(Database db, int id)
        {
            DbCommand cmd = db.GetStoredProcCommand("Oreja_Helix_GetByAntropometriaId_SP");
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, id);
            return cmd;
        }
    }
}
