﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;

namespace DataAccess.OrejaHelix
{
    public class ServicioOrejaHelix : ClsAbstractService<Entity.OrejaHelix>, IOrejaHelix
    {
        public ServicioOrejaHelix(string dataBase)
            : base(dataBase)
        {
        }
        
        public List<Entity.OrejaHelix> ObtenerTodos()
        {
            ISelectFactory<Entity.NullClass> select = new ObtenerTodos();
            return GetAll(select, new FabricaOrejaHelix(), new Entity.NullClass());
        }

   

        public Entity.OrejaHelix ObtenerByTrackingId(Guid userId)
        {
            ISelectFactory<Guid> select = new ObtenerByTrackignId();
            return GetByKey(select, new FabricaOrejaHelix(), userId);
        }

        public Entity.OrejaHelix ObtenerById(int Id)
        {
            ISelectFactory<int> select = new ObtenerById();
            return GetByKey(select, new FabricaOrejaHelix(), Id);
        }

        public Entity.OrejaHelix ObtenerByAntropometriaId(int Id)
        {
            ISelectFactory<int> select = new ObtenerByAntropometriaId();
            return GetByKey(select, new FabricaOrejaHelix(), Id);
        }

        public int Guardar(Entity.OrejaHelix general)
        {
            IInsertFactory<Entity.OrejaHelix> insert = new Guardar();
            return Insert(insert, general);
        }

        public void Actualizar(Entity.OrejaHelix boca)
        {
            IUpdateFactory<Entity.OrejaHelix> update = new Actualizar();
            Update(update, boca);
        }


    }
}