﻿using DataAccess.Interfaces;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace DataAccess.OrejaHelix
{
    public class Guardar : IInsertFactory<Entity.OrejaHelix>
    {
        public DbCommand ConstructInsertCommand(Database db, Entity.OrejaHelix oreja)
        {
            DbCommand cmd = db.GetStoredProcCommand("Oreja_Helix_Insert_SP");
            db.AddOutParameter(cmd, "_NewIdentity", DbType.Int32, 8);
            db.AddInParameter(cmd, "_TrackingId", DbType.Guid, oreja.TrackingId);
            db.AddInParameter(cmd, "_FormaO", DbType.Int32, oreja.FormaO);
            db.AddInParameter(cmd, "_ContornoH", DbType.Int32, oreja.ContornoH);
            db.AddInParameter(cmd, "_AntropometriaId", DbType.Int32, oreja.AntropometriaId);

            if (oreja.OriginalO != null)
                db.AddInParameter(cmd, "_OriginalO", DbType.Int32, oreja.OriginalO);
            else
                db.AddInParameter(cmd, "_OriginalO", DbType.Int32, null);

            if (oreja.SuperiorH != null)
                db.AddInParameter(cmd, "_SuperiorH", DbType.Int32, oreja.SuperiorH);
            else
                db.AddInParameter(cmd, "_SuperiorH", DbType.Int32, null);

            if (oreja.PosteriorH != null)
                db.AddInParameter(cmd, "_PosteriorH", DbType.Int32, oreja.PosteriorH);
            else
                db.AddInParameter(cmd, "_PosteriorH", DbType.Int32, null);

            if (oreja.AdherenciaH != null)
                db.AddInParameter(cmd, "_AdherenciaH", DbType.Int32, oreja.AdherenciaH);
            else
                db.AddInParameter(cmd, "_AdherenciaH", DbType.Int32, null);

            return cmd;
        }
    }
}
