﻿using System;
using System.Collections.Generic;

namespace DataAccess.OrejaHelix
{
    public interface IOrejaHelix
    {
        List<Entity.OrejaHelix> ObtenerTodos();
        Entity.OrejaHelix ObtenerById(int Id);
        Entity.OrejaHelix ObtenerByAntropometriaId(int Id);
        Entity.OrejaHelix ObtenerByTrackingId(Guid trackingid);
        int Guardar(Entity.OrejaHelix oreja);
        void Actualizar(Entity.OrejaHelix oreja);
    }
}