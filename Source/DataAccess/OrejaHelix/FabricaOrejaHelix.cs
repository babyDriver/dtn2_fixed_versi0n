﻿using DataAccess.Interfaces;


namespace DataAccess.OrejaHelix
{
    public class FabricaOrejaHelix : IEntityFactory<Entity.OrejaHelix>
    {
       

        public Entity.OrejaHelix ConstructEntity(System.Data.IDataReader dr)
        {
            var oreja = new Entity.OrejaHelix();

            var index = dr.GetOrdinal("Id");
            if (!dr.IsDBNull(index))
            {
                oreja.Id = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("TrackingId");
            if (!dr.IsDBNull(index))
            {
                oreja.TrackingId = dr.GetGuid(index);
            }

            index = dr.GetOrdinal("OriginalO");
            if (!dr.IsDBNull(index))
            {
                oreja.OriginalO = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("FormaO");
            if (!dr.IsDBNull(index))
            {
                oreja.FormaO = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("SuperiorH");
            if (!dr.IsDBNull(index))
            {
                oreja.SuperiorH = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("PosteriorH");
            if (!dr.IsDBNull(index))
            {
                oreja.PosteriorH = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AdherenciaH");
            if (!dr.IsDBNull(index))
            {
                oreja.AdherenciaH = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("ContornoH");
            if (!dr.IsDBNull(index))
            {
                oreja.ContornoH = dr.GetInt32(index);
            }

            index = dr.GetOrdinal("AntropometriaId");
            if (!dr.IsDBNull(index))
            {
                oreja.AntropometriaId = dr.GetInt32(index);
            }
            
            return oreja;
        }
    }
}