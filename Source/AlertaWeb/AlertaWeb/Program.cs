﻿using Business;
using Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AlertaWeb
{
    public class Program
    {
        static void Main(string[] args)
        {
            ActualizarWSAColoniaAsync().Wait();
            ActualizarWSAEstadoAsync().Wait();
            ActualizarWSAInstitucionAsync().Wait();
            ActualizarWSAMotivoAsync().Wait();
            ActualizarWSAMunicipioAsync().Wait();
            ActualizarWSAUnidadAsync().Wait();
        }

        public static async Task ActualizarWSAColoniaAsync()
        {
            List<WSAUnidad> lista = new List<WSAUnidad>();
            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Add("ContentType", "application/json");
            var response = await cliente.GetStringAsync(new Uri(@"http://promad.no-ip.biz:7070/api/EventosExternos/ControlDetenidos/catalogo/colonia"));
            var data = JsonConvert.DeserializeObject<List<WSAColoniaAux>>(response);

            var colonias = ControlWSAColonia.ObtenerTodos();
            var coloniasId = new List<int>();

            foreach (var coloniaAux in data)
            {
                var colonia = colonias.FirstOrDefault(x => x.IdColonia == int.Parse(coloniaAux.idColonia));
                if (colonia != null)
                {
                    //Actualizar
                    colonia.NombreColonia = coloniaAux.nombreColonia;
                    colonia.IdMunicipio = int.Parse(coloniaAux.idMunicipio);
                    colonia.Activo = true;
                    ControlWSAColonia.Actualizar(colonia);
                    coloniasId.Add(colonia.IdColonia);
                }
                else
                {
                    //Agregar
                    ControlWSAColonia.Guardar(new WSAColonia()
                    {
                        IdColonia = int.Parse(coloniaAux.idColonia),
                        NombreColonia = coloniaAux.nombreColonia,
                        IdMunicipio = int.Parse(coloniaAux.idMunicipio)
                    });
                    coloniasId.Add(int.Parse(coloniaAux.idColonia));
                }
            }

            //Desactivar los registros no existentes
            var coloniasLeft = colonias.Where(x => !coloniasId.Contains(x.IdColonia));
            foreach (var coloniaLeft in coloniasLeft)
            {
                coloniaLeft.Activo = false;
                ControlWSAColonia.Actualizar(coloniaLeft);
            }
        }

        public static async Task ActualizarWSAEstadoAsync()
        {
            List<WSAEstado> lista = new List<WSAEstado>();
            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Add("ContentType", "application/json");
            var response = await cliente.GetStringAsync(new Uri(@"http://promad.no-ip.biz:7070/api/EventosExternos/ControlDetenidos/catalogo/estado"));
            var data = JsonConvert.DeserializeObject<List<WSAEstadoAux>>(response);

            var estados = ControlWSAEstado.ObtenerTodos();
            var estadosId = new List<int>();

            foreach (var estadoAux in data)
            {
                var estado = estados.FirstOrDefault(x => x.IdEstado == int.Parse(estadoAux.idEstado));
                if (estado != null)
                {
                    //Actualizar
                    estado.NombreEstado = estadoAux.nombreEstado;
                    estado.Activo = true;
                    ControlWSAEstado.Actualizar(estado);
                    estadosId.Add(estado.IdEstado);
                }
                else
                {
                    //Agregar
                    ControlWSAEstado.Guardar(new WSAEstado()
                    {
                        IdEstado = int.Parse(estadoAux.idEstado),
                        NombreEstado = estadoAux.nombreEstado,
                    });
                    estadosId.Add(int.Parse(estadoAux.idEstado));
                }
            }

            //Desactivar los registros no existentes
            var estadosLeft = estados.Where(x => !estadosId.Contains(x.IdEstado));
            foreach (var estadoLeft in estadosLeft)
            {
                estadoLeft.Activo = false;
                ControlWSAEstado.Actualizar(estadoLeft);
            }
        }

        public static async Task ActualizarWSAInstitucionAsync()
        {
            List<WSAInstitucion> lista = new List<WSAInstitucion>();
            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Add("ContentType", "application/json");
            var response = await cliente.GetStringAsync(new Uri(@"http://promad.no-ip.biz:7070/api/EventosExternos/ControlDetenidos/catalogo/institucion"));
            var data = JsonConvert.DeserializeObject<List<WSAInstitucionAux>>(response);

            var instituciones = ControlWSAInstitucion.ObtenerTodos();
            var institucionesId = new List<int>();

            foreach (var institucionAux in data)
            {
                var institucion = instituciones.FirstOrDefault(x => x.IdInstitucion == int.Parse(institucionAux.idInstitucion));
                if (institucion != null)
                {
                    //Actualizar
                    institucion.NombreInstitucion = institucionAux.nombreInstitucion;
                    institucion.NombreCorto = institucionAux.nombreCorto;
                    institucion.IdMunicipio = int.Parse(institucionAux.idMunicipio);
                    institucion.Activo = true;
                    ControlWSAInstitucion.Actualizar(institucion);
                    institucionesId.Add(institucion.IdInstitucion);
                }
                else
                {
                    //Agregar
                    ControlWSAInstitucion.Guardar(new WSAInstitucion()
                    {
                        IdInstitucion = int.Parse(institucionAux.idInstitucion),
                        NombreInstitucion = institucionAux.nombreInstitucion,
                        NombreCorto = institucionAux.nombreCorto,
                        IdMunicipio = int.Parse(institucionAux.idMunicipio)
                    });
                    institucionesId.Add(int.Parse(institucionAux.idInstitucion));
                }
            }

            //Desactivar los registros no existentes
            var institucionesLeft = instituciones.Where(x => !institucionesId.Contains(x.IdInstitucion));
            foreach (var institucionLeft in institucionesLeft)
            {
                institucionLeft.Activo = false;
                ControlWSAInstitucion.Actualizar(institucionLeft);
            }
        }

        public static async Task ActualizarWSAMotivoAsync()
        {
            List<WSAMotivo> lista = new List<WSAMotivo>();
            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Add("ContentType", "application/json");
            var response = await cliente.GetStringAsync(new Uri(@"http://promad.no-ip.biz:7070/api/EventosExternos/ControlDetenidos/catalogo/motivos"));
            var data = JsonConvert.DeserializeObject<List<WSAMotivoAux>>(response);

            var motivos = ControlWSAMotivo.ObtenerTodos();
            var motivosId = new List<int>();

            foreach (var motivoAux in data)
            {
                var motivo = motivos.FirstOrDefault(x => x.IdMotivo == int.Parse(motivoAux.idMotivo));
                if (motivo != null)
                {
                    //Actualizar
                    motivo.NombreMotivoLlamada = motivoAux.nombreMotivoLlamada;
                    motivo.PrioridadAtencionEvento = motivoAux.pioridadAtencionEvento;
                    motivo.Activo = true;
                    ControlWSAMotivo.Actualizar(motivo);
                    motivosId.Add(motivo.IdMotivo);
                }
                else
                {
                    //Agregar
                    ControlWSAMotivo.Guardar(new WSAMotivo()
                    {
                        IdMotivo = int.Parse(motivoAux.idMotivo),
                        NombreMotivoLlamada = motivoAux.nombreMotivoLlamada,
                        PrioridadAtencionEvento = motivoAux.pioridadAtencionEvento
                    });
                    motivosId.Add(int.Parse(motivoAux.idMotivo));
                }
            }

            //Desactivar los registros no existentes
            var motivosLeft = motivos.Where(x => !motivosId.Contains(x.IdMotivo));
            foreach (var motivoLeft in motivosLeft)
            {
                motivoLeft.Activo = false;
                ControlWSAMotivo.Actualizar(motivoLeft);
            }
        }

        public static async Task ActualizarWSAMunicipioAsync()
        {
            List<WSAUnidad> lista = new List<WSAUnidad>();
            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Add("ContentType", "application/json");
            var response = await cliente.GetStringAsync(new Uri(@"http://promad.no-ip.biz:7070/api/EventosExternos/ControlDetenidos/catalogo/municipio"));
            var data = JsonConvert.DeserializeObject<List<WSAMunicipioAux>>(response);

            var municipios = ControlWSAMunicipio.ObtenerTodos();
            var municipiosId = new List<int>();

            foreach (var municipioAux in data)
            {
                var municipio = municipios.FirstOrDefault(x => x.IdMunicipio == int.Parse(municipioAux.idMunicipio));
                if (municipio != null)
                {
                    //Actualizar
                    municipio.NombreMunicipio = municipioAux.nombreMunicipio;
                    municipio.IdEstado = int.Parse(municipioAux.idEstado);
                    municipio.Activo = true;
                    ControlWSAMunicipio.Actualizar(municipio);
                    municipiosId.Add(municipio.IdMunicipio);
                }
                else
                {
                    //Agregar
                    ControlWSAMunicipio.Guardar(new WSAMunicipio()
                    {
                        IdMunicipio = int.Parse(municipioAux.idMunicipio),
                        NombreMunicipio = municipioAux.nombreMunicipio,
                        IdEstado = int.Parse(municipioAux.idEstado)
                    });
                    municipiosId.Add(int.Parse(municipioAux.idMunicipio));
                }
            }

            //Desactivar los registros no existentes
            var municipiosLeft = municipios.Where(x => !municipiosId.Contains(x.IdMunicipio));
            foreach (var municipioLeft in municipiosLeft)
            {
                municipioLeft.Activo = false;
                ControlWSAMunicipio.Actualizar(municipioLeft);
            }
        }

        public static async Task ActualizarWSAUnidadAsync()
        {
            List<WSAUnidad> lista = new List<WSAUnidad>();
            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Add("ContentType", "application/json");
            var response = await cliente.GetStringAsync(new Uri(@"http://promad.no-ip.biz:7070/api/EventosExternos/ControlDetenidos/catalogo/unidad"));
            var data = JsonConvert.DeserializeObject<List<WSAUnidadAux>>(response);

            var unidades = ControlWSAUnidad.ObtenerTodos();
            var unidadesId = new List<int>();

            foreach (var unidadAux in data)
            {
                var unidad = unidades.FirstOrDefault(x => x.IdUnidadInstitucion == int.Parse(unidadAux.idUnidadInstitucion));
                if (unidad != null)
                {
                    //Actualizar
                    unidad.ClaveUnidad = unidadAux.claveUnidad;
                    unidad.IdInstitucion = int.Parse(unidadAux.idInstitucion);
                    unidad.Activo = true;
                    ControlWSAUnidad.Actualizar(unidad);
                    unidadesId.Add(unidad.IdUnidadInstitucion);
                }
                else
                {
                    //Agregar
                    ControlWSAUnidad.Guardar(new WSAUnidad()
                    {
                        IdUnidadInstitucion = int.Parse(unidadAux.idUnidadInstitucion),
                        ClaveUnidad = unidadAux.claveUnidad,
                        IdInstitucion = int.Parse(unidadAux.idInstitucion)
                    });
                    unidadesId.Add(int.Parse(unidadAux.idUnidadInstitucion));
                }
            }

            //Desactivar los registros no existentes
            var unidadesLeft = unidades.Where(x => !unidadesId.Contains(x.IdUnidadInstitucion));
            foreach (var unidadLeft in unidadesLeft)
            {
                unidadLeft.Activo = false;
                ControlWSAUnidad.Actualizar(unidadLeft);
            }
        }
    }
}
