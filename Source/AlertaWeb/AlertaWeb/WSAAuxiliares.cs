﻿namespace AlertaWeb
{
    public class WSAColoniaAux
    {
        public string idColonia { get; set; }
        public string nombreColonia { get; set; }
        public string idMunicipio { get; set; }
    }

    public class WSAEstadoAux
    {
        public string idEstado { get; set; }
        public string nombreEstado { get; set; }
    }

    public class WSAInstitucionAux
    {
        public string idInstitucion { get; set; }
        public string nombreInstitucion { get; set; }
        public string nombreCorto { get; set; }
        public string idMunicipio { get; set; }
    }

    public class WSAMotivoAux
    {
        public string idMotivo { get; set; }
        public string nombreMotivoLlamada { get; set; }
        public string pioridadAtencionEvento { get; set; }
    }

    public class WSAMunicipioAux
    {
        public string idMunicipio { get; set; }
        public string nombreMunicipio { get; set; }
        public string idEstado { get; set; }
    }

    public class WSAUnidadAux
    {
        public string idUnidadInstitucion { get; set; }
        public string idInstitucion { get; set; }
        public string claveUnidad { get; set; }
    }
}
