﻿using System;

namespace Entity
{
    public class ServicioMedico
    {
        public int Id { get; set; }
        public string TrackinngId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int RegistradoPor { get; set; }
        public int ModificadoPor { get; set; }
        public DateTime FechaUltimaModificacion { get; set; }
        public int DetalledetencionId { get; set; }
        public int CertificadoQuimicoId { get; set; }
        public int CertificadoLesionId { get; set; }
        public int CertificadoMedicoPsicofisiologicoId { get; set; }
        public Int16 Activo { get; set; }
        public Int16 Habilitado { get; set; }
    }
}
