﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class InformeUsoFuerza
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public string Primerapellido { get; set; }
        public string Segundoapellido { get; set; }
        public string Nombre { get; set; }
        public string Adscripcion { get; set; }
        public string Cargo { get; set; }
        public int DetalledetencionId { get; set; }
        public DateTime Fechahora { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; }
        public int Creadopor { get; set; }
        public int Autoridadeslesionadas { get; set; }
        public int Autoridadesfallecidas { get; set; }
        public int Personaslesionadas { get; set; }
        public int Personasfallecidas { get; set; }
        public Boolean Reduccionfisicademovimientos { get; set; }
        public Boolean Utilizaciondearmasincapacitantemenosletales { get; set; }
        public Boolean Utilizaciondearmasdefuego { get; set; }
        public string DescripcionConducta { get; set; }
        public Boolean Brindosolicitoasistenciamedica { get; set; }
        public string Especifique { get; set; }
        public string Firma { get; set; }
        public string Firma2 { get; set; }
    }
}
