﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Detalledetenidopormotivo
    {
        public int MotivodetencionId { get; set; }
        public string Motivodetencion { get; set; }
        public string Remision { get; set; }
        public string Detenido { get; set; }
        public string Alias { get; set; }
        public string Domicilio { get; set; }
        public int Edad { get; set; }
        public int SituacionId { get; set; }
    }
}
