﻿using System;
namespace Entity
{
    public class Smtp
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
        public string CC { get; set; }
        public string CCO { get; set; }
        public bool SendMail { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaModificacion { get; set; }
        public int CreadoPor { get; set; }
        public int ModificadoPor { get; set; }
    }
}

