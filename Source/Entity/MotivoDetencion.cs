﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class MotivoDetencion
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Motivo { get; set; } 
        public string Descripcion { get; set; }
        public string Articulo { get; set; }
        public decimal MultaMinimo { get; set; }
        public decimal MultaMaximo { get; set; }
        public int HorasdeArresto { get; set; }
        public int Activo { get; set; }
        public Int16 Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
        public int Tipo_Motivo { get; set; }
    }
}
