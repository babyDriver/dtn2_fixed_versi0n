﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class Parametro
    {
        public int Id { get; set; }
        public string NombreParametro { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaHora { get; set; }
        public int TipoParametroId { get; set; }

    }
}
