﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Cliente
    {
        public int Id { get; set; }
        public Guid TenantId { get; set; }
        public string Nombre { get; set; }
        public string Rfc { get; set; }        
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
    }
}
