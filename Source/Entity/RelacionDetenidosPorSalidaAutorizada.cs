﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class RelacionDetenidosPorSalidaAutorizada
    {
        public string Remision { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public DateTime Fechacalificacion { get; set; }
        public string Unidad { get; set; }
        public DateTime Fecharegistro { get; set; }
        public int Totalhoras { get; set; }
        public Boolean Soloarresto { get; set; }
        public decimal Totalapagar { get; set; }
        public string Motivodetencion { get; set; }
        public DateTime Fechasalida { get; set; }
        public int TiposalidaId { get; set; }
        public string Tiposalida { get; set; }
        public int DetenidoId { get; set; }
    }
}
