﻿using System;

namespace Entity
{
    public class BocaLabioMenton
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public Int32? TamanoB { get; set; }
        public int ComisuraB { get; set; }
        public Int32? AlturaL { get; set; }
        public int EspesorL { get; set; }
        public int ProminenciaL { get; set; }
        public int TipoM { get; set; }
        public int FormaM { get; set; }
        public int InclinacionM { get; set; }
        public int AntroprometriaId { get; set; }
    }
}
