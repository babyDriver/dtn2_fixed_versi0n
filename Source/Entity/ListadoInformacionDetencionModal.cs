﻿using System;

namespace Entity
{
    public class ListadoInformacionDetencionModal
    {
        public int DetenidoId { get; set; }
        public string Motivo { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Expediente { get; set; }
        public string LugarDetencion { get; set; }
        public string Institucion { get; set; }
        public string Situacion { get; set; }
        public DateTime FechaSalida { get; set; }
        public string Fundamento { get; set; }
        public string TipoSalida { get; set; }
    }
}
