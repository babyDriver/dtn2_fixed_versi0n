﻿using System;

namespace Entity
{
    public class HistorialServicioMedico
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int IdHistorial { get; set; }
        public int IdServicioMedico { get; set; }
    }
}
