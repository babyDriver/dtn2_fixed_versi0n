﻿using System;

namespace Entity
{
    public class ListadoReporteIngresos
    {
        public Guid TrackingId { get; set; }
        public string Detenido { get; set; }
        public string Expediente { get; set; }
        public DateTime Fecha { get; set; }
        public string Folio { get; set; }
        public string Motivo { get; set; }
        public int TotalHoras { get; set; }
        public string Unidad { get; set; }
        public string Responsable { get; set; }
        public string LugarDetencion { get; set; }
        public string FechaAux { get; set; }
    }
}
