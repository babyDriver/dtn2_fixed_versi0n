﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class AliasLog
    {
        public int Id { get; set; }
        public int DetenidoId { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime FechaAnt { get; set; }
    }
}
