﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CertificadoPsicoFisiologicoConclusionDetalle
    {
        public int Id { get; set; }
        public int certificado_psicofisiologicoConclusionId { get; set; }
        public int ConclusionId { get; set; }
        public DateTime FechaHora { get; set; }
        public int Creadopor { get; set; }
    }
}
