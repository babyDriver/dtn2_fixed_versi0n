﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Calificacion
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public int InternoId { get; set; }
        public int SituacionId { get; set; }
        public int InstitucionId { get; set; }
        public string Fundamento { get; set; }
        public bool TrabajoSocial { get; set; }
        public int TotalHoras { get; set; }
        //public string MaxHoras { get; set; }
        public bool SoloArresto { get; set; }
        public decimal TotalDeMultas { get; set; }
        public decimal Agravante { get; set; }
        public decimal Ajuste { get; set; }
        public decimal TotalAPagar { get; set; }
        public string Razon { get; set; }
        public int Calificacionanioregistro { get; set; }
        public int Calificacionmesregistro { get; set; }
    }
}
