﻿
namespace Entity
{
    public enum TipoDeMensaje
    {
        ModificacionDeUsuario = 1,
        RecuperacionDeContraseña = 2,
        NuevoUsuario = 3,
        ReestablecerContrasena=4,
        ModificacionDePerfil = 5
    }
}
