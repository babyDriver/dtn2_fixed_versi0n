﻿using System;

namespace Entity
{
    public class Casillero
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public int Capacidad { get; set; }
        public int Disponible { get; set; }
        public bool Activo { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public int Habilitado { get; set; }
        public int Creadopor { get; set; }
    }
}
