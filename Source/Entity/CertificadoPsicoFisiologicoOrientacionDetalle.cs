﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    //CertificadoPsicoFisiologicoOrientacionDetalle
    public class CertificadoPsicoFisiologicoOrientacionDetalle
    {
        public int Id { get; set; }
        public int certificado_psicofisiologicoorientacionId { get; set; }
        public int OrientacionId { get; set; }
        
        public DateTime FechaHora { get; set; }
        public int Creadopor { get; set; }
    }
}
