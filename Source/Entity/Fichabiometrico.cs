﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Fichabiometrico
    {
        public int DetenidoId { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Remision { get; set; }
        public string Alias { get; set; }
        public string Sexo { get; set; }
        public double Estatura { get; set; }
        public DateTime Fechanacimiento { get; set; }
        public string Nacionalidad { get; set; }
        public string Ocupacion { get; set; }
        public string Escolaridad { get; set; }
        public string Estadocivil { get; set; }
        public string Lenguanativa { get; set; }
        public string Etnia { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string RFC { get; set; }
        public string CURP { get; set; }
    }
}
