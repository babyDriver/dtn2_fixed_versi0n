﻿using System;
namespace Entity
{
    public class NucleoPrimario
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string NombrePadre { get; set; }
        public Catalogo OcupacionPadre { get; set; }
        public Int32? EdadPadre { get; set; }
        public string ActitudAntisocialPadre { get; set; }
        public string NombreMadre { get; set; }
        public Catalogo OcupacionMadre { get; set; }
        public Int32? EdadMadre { get; set; }
        public string ActitudAntisocialMadre { get; set; }
        public string UbicacionEnFamilia { get; set; }
        public string NucleoFamiliar { get; set; }
        public string AmbienteFamiliar { get; set; }
        public string TipoReglasFamiliares { get; set; }
        public string TipoRelacionPadres { get; set; }
        public string TipoRelacionPadresConHijos { get; set; }
       

    }
}
