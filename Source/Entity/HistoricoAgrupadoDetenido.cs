﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class HistoricoAgrupadoDetenido
    {
        public int Id { get; set; }

        public DateTime FechaHora { get; set; }

        public int DetenidoOriginalId { get; set; }

        public int DetalleDetencionId { get; set; }

        public int DetenidoId { get; set; }

        public int CreadoPor { get; set; }

        public string Expediente { get; set; }

        public string Expedienteoriginal { get; set; }
    }
}
