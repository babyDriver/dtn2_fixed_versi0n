﻿using System;

namespace Entity
{
    public class Antropometria
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public decimal Estatura { get; set; }
        public decimal Peso { get; set; }
        public string FormulaI { get; set; }
        public string SubformulaI { get; set; }
        public string FormulaD { get; set; }
        public string SubformulaD { get; set; }
        public string Vucetich { get; set; }
        public string Dactiloscopica { get; set; }
        public bool Lunares { get; set; }
        public bool Tatuaje { get; set; }
        public bool Defecto { get; set; }
        public bool Cicatris { get; set; }
        public bool Anteojos { get; set; }
        public int DetenidoId { get; set; }
    }
}
