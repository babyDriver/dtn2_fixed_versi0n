﻿using System;

namespace Entity
{
    public class UsuarioReportes
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int UsuarioId { get; set; }
        public int ReporteId { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int? CreadoPor { get; set; }
    }
}
