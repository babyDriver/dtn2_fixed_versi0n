﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class AgrupadoReporte
    {
        public DateTime Fecha { get; set; }
        public string Detenidooriginal { get; set; }
        public string Expedienteoriginal{ get; set; }
        public int DetenidoorignalId { get; set; }
        public string Detenidoagrupado { get; set; }
        public string Expediente { get; set; }
        public int DetenidoId { get; set; }
        public int ContratoId { get; set; }
    }
}
