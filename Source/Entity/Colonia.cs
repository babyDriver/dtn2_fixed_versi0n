﻿using System;

namespace Entity
{
    public class Colonia
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int IdMunicipio { get; set; }
        public string CodigoPostal { get; set; }
        public string Asentamiento { get; set; }
        public string TipoDeAsentamiento { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string ClaveDeOficina { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
    }
}
