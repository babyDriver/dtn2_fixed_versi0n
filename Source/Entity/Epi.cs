﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Epi
    {
        public string Expediente { get; set; }
        public DateTime Fecha { get; set; }
        public int Edad { get; set; }
        public string Sexo { get; set; }
        public string Conclucion { get; set; }
        public string Nombre { get; set; }
   
    }
}
