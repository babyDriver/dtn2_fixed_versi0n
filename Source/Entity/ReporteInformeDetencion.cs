﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReporteInformeDetencion
    {
        public string Remision { get; set; }
        public string Detenido { get; set; }
        public string Domicilio { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public string Colonia { get; set; }
        public DateTime FechaNacimineto { get; set; }
        public string Sexo { get; set; }
        public string LugarDetencion { get; set; }
        public string Motivo { get; set; }
        public DateTime FechaDetencion { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string CreadoPor { get; set; }
    }
}
