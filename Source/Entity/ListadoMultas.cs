﻿using System;

namespace Entity
{
    public class ListadoMultas
    {
        public int Id { get; set; }
        public Guid CalificacionTracking { get; set; }
        public Guid EstatusTracking { get; set; }
        public string RutaImagen { get; set; }
        public string NombreDetenido { get; set; }
        public string APaternoDetenido { get; set; }
        public string AMaternoDetenido { get; set; }
        public string Expediente { get; set; }        
        public decimal TotalAPagar { get; set; }
        public string NombreCompleto { get; set; }
    }
}
