﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class DomicilioPorSubcontrato
    {
        public int Id { get; set; }
        public int MunicipioId { get; set; }
        public int EstadoId { get; set; }
        public int PaisId { get; set; }
    }
}
