﻿using System;

namespace Entity
{
    public class Historial
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int InternoId { get; set; }
        public string Movimiento { get; set; }
        public DateTime Fecha { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public int ModificadoPor { get; set; }
        public int ContratoId { get; set; }
    }
}
