﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CalificacionIngreso
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public DateTime Fecha { get; set; }
        public int CalificacionId { get; set; }
        public int IngresoId { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public int Folio { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
    }
}
