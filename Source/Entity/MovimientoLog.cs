﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class MovimientoLog
    {
        public int Id { get; set; }
        public int DetalledetencionId { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime FechaAnt { get; set; }
    }
}
