﻿using System;
namespace Entity
{
    public class Amparo
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Autoridad { get; set; }
        public string AutoridadCentro { get; set; }
        public string Actos { get; set; }
        public DateTime FehcaNotificacion { get; set; }
        public string Notificacion { get; set; }
        public DateTime FechaPrevio { get; set; }
        public string Previo { get; set; }
        public DateTime FechaJustificado { get; set; }
        public string Justificado { get; set; }
        public DateTime FechaAudiencia { get; set; }
        public string Audiencia { get; set; }
        public DateTime FechaSuspension { get; set; }
        public string Suspension { get; set; }
        public DateTime FechaDefinitiva { get; set; }
        public string Definitiva { get; set; }
        public DateTime FechaResolucion { get; set; }
        public string Resolucion { get; set; }
        public DateTime FechaAutoejecutoria { get; set; }
        public string Autoejecutoria { get; set; }
        public string Recursos { get; set; }
        public string Observacion { get; set; }
        public string Responsable { get; set; }
        public DateTime FechaElaboracion { get; set; }
        public string Elaboro { get; set; }
        public string Reviso { get; set; }
        public bool Habilitado { get; set; }
        public int DetenidoId { get; set; }
        public int ProcesoId { get; set; }
    }
}
