﻿using System;

namespace Entity
{
    public class Domicilio
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }        
        public string Telefono { get; set; }
        public int PaisId { get; set; }
        public Int32? EstadoId { get; set; }
        public Int32? MunicipioId { get; set; }
        public string Localidad { get; set; }
        public int? ColoniaId { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}
