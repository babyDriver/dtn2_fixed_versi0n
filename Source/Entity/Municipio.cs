﻿using System;

namespace Entity
{
    public class Municipio
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int EstadoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Clave { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public double Salario { get; set; }
        public int Creadopor { get; set; }
    }
}
