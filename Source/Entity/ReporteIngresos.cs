﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReporteIngresos
    {
        public int DetalledetencionId { get; set; }
        public int DetenidoId { get; set; }
        public string Detenido { get; set; }
        public int Remision { get; set; }
        public DateTime Fecharegistro { get; set; }
        public string Folio { get; set; }
        public string Motivo { get; set; }
        public int Horas { get; set; }
        public string Unidad { get; set; }
        public string Responsable { get; set; }
        public string LugarDetencion { get; set; }
        public string ResultadoQuimico { get; set; }
    }
}
