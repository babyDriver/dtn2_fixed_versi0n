﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CertificadoLesionTatuajesDetalle
    {
        public int Id { get; set; }
        public int certificado_LesionTatuajesId { get; set; }
        public int TatuajeId { get; set; }
    }
}
