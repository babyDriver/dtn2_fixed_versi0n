﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ParametroContrato
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public int Parametroid { get; set; }
        public int ContratoId { get; set; }
        public DateTime FechaHora { get; set; }
        public string Valor { get; set; }
        public int CreadoPor { get; set; }
    }
}
