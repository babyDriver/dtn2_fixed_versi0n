﻿using System;

namespace Entity
{
    public class Egreso
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public DateTime Fecha { get; set; }
        public int? Traslado { get; set; }
        public int? MotivoId { get; set; }
        public string Nombre { get; set; }
        public string Puesto { get; set; }
        public int EstatusInternoId { get; set; }
    }
}
