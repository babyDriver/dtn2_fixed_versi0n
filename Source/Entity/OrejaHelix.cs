﻿using System;

namespace Entity
{
    public class OrejaHelix
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public Int32? OriginalO { get; set; }
        public int FormaO { get; set; }
        public Int32? SuperiorH { get; set; }
        public Int32? PosteriorH { get; set; }
        public Int32? AdherenciaH { get; set; }
        public int ContornoH { get; set; }
        public int AntropometriaId { get; set; }
    }
}
