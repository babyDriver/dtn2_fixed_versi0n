﻿using System;

namespace Entity
{
    public class ListadoReporteFichaTecnica
    {
        public int DetalleDetencionId { get; set; }
        public Guid TrackingIdDetalle { get; set; }
        public string Expediente { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public string DescripcionEvento { get; set; }
        public string Motivo { get; set; }
        public string Unidad { get; set; }
        public string Responsable { get; set; }
        public DateTime HoraYFecha { get; set; }
        public string Situacion { get; set; }
        public int TotalHoras { get; set; }
        public string FechasExamen { get; set; }
        public string IndicacionesExamen { get; set; }
        public string Subcontrato { get; set; }
        public string FechaAux { get; set; }
    }
}
