﻿using System;

namespace Entity
{
    public class Observacion
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int InternoId { get; set; }
        public string Observaciones { get; set; }
        public DateTime Fecha { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
    }
}
