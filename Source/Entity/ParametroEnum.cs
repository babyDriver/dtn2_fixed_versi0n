﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public enum ParametroEnum
    {
        TRASLADO_O_SALIDA_SIN_EXAMEN_MÉDICO	=	1	,
	    NOMBRE_DEL_SECRETARIO	=	2	,
	    TÍTULO_DEL_SECRETARIO	=	3	,
	    CONSECUTIVO_INGRESO	=	4	,
	    CONSECUTIVO_EGRESO	=	5	,
	    CONSECUTIVO_RECIBO	=	6	,
	    CONSECUTIVO_CANCELACIONES	=	7	,
	    CONTRASEÑA_SUPERVISOR	=	8	,
	    SALIDA_DEL_DETENIDO_SIN_BIOMETRICO	=	9,
        BOTONES_PERTENENCIA=10,
        PERFILES = 11,
        ADMINISTRAR_EXAMEN_MEDICO = 12
    }
}
