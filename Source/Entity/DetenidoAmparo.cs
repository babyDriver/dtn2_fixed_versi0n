﻿using System;


namespace Entity
{
    public class DetenidoAmparo
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int DetalleDetencionId { get; set; }
        public DateTime Fecha { get; set; }
        public Boolean Activo { get; set; }
    }
}
