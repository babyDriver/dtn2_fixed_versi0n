﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReportesLog
    {
        public int Id { get; set; }
        public int ProcesoId { get; set; }
        public string ProcesoTrackingId { get; set; }
        public string Reporte { get; set; }
        public int CreadoPor { get; set; }
        public DateTime Fechayhora { get; set; }
        public string Modulo { get; set; }
        public int EstatusId { get; set; }

    }
}
