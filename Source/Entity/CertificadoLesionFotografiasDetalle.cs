﻿namespace Entity
{
    public class CertificadoLesionFotografiasDetalle
    {
        public int Id { get; set; }
        public int IdCertificado_lesionfotografias { get; set; }
        public string Fotografia { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
    }
}
