﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class HistorialCalificacion
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int InternoId { get; set; }
        public string Movimiento { get; set; }
        public DateTime Fecha { get; set; }
        public bool Activo { get; set; }
        public string Descripcion { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
    }
}
