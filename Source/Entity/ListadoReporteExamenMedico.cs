﻿using System;

namespace Entity
{
    public class ListadoReporteExamenMedico
    {
        public int IdDetenido { get; set; }
        public int IdDetalle { get; set; }
        public Guid TrackingId { get; set; }
        public Guid TrackingIdDetalle { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string NombreCompleto { get; set; }
        public bool Activo { get; set; }
        public string Expediente { get; set; }
        public bool ActivoExamen { get; set; }
        public int IdExamen { get; set; }
        public DateTime FechaExamen { get; set; }
        public Guid TrackingIdExamen { get; set; }
        public string Sexo { get; set; }
        public string FechaExamenAux { get; set; }
    }
}
