﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReporteUsosistema
    {
        public int IdHistorial { get; set; }
        public DateTime Fecha { get; set; }
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Rol { get; set; }
        public string SubContratos { get; set; }
        public string Movimiento { get; set; }
        public string NombreCompleto { get; set; }
        public  int ContratoId { get; set; }
    }
}
