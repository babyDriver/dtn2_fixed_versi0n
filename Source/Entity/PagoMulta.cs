﻿namespace Entity
{
    public class PagoMulta
    {
        public int Id { get; set; }
        public string Articulo { get; set; }
        public string Motivo { get; set; }
        public decimal MultaSugerida { get; set; }
    }
}
