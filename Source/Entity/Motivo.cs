﻿using System;

namespace Entity
{
    public class Motivo
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int CreadoPor { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
    }
}