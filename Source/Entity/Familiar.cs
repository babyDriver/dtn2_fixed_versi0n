﻿using System;
namespace Entity
{
    public class Familiar
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Estado { get; set; }
        public Catalogo Ocupacion { get; set; }
        public Catalogo Sexo { get; set; }
        public Domicilio Domicilio { get; set; }
        public Catalogo Parentesco { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public Detenido Detenido { get; set; }

    }
}
