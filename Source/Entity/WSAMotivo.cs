﻿using System;

namespace Entity
{
    public class WSAMotivo
    {
        public int IdMotivo { get; set; }
        public string NombreMotivoLlamada { get; set; }
        public string PrioridadAtencionEvento { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool Activo { get; set; }
    }
}
