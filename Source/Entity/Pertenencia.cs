﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Pertenencia
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }        
        public int InternoId { get; set; }
        public string PertenenciaNombre { get; set; }
        public string Observacion { get; set; }
        public int Clasificacion { get; set; }
        public int Bolsa { get; set; }
        public int Cantidad { get; set; }
        public string Fotografia { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int Estatus { get; set; }
        public int CreadoPor { get; set; }
        public int CasilleroId { get; set; }
        public DateTime FechaEntrada { get; set; }
    }
}
