﻿using System;

namespace Entity
{
    public class WSAEvento
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int IdUnidad { get; set; }
        public int IdInstitucion { get; set; }
        public int IdLugar { get; set; }
        public string Folio { get; set; }
        public DateTime FechaEvento { get; set; }
        public DateTime FechaCapturaDetenidos { get; set; }
        public int NumeroDetenidos { get; set; }
        public string NombreResponsable { get; set; }
        public string Descripcion { get; set; }
        public int IdMotivo { get; set; }
    }
}
