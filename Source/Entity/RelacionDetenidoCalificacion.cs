﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class RelacionDetenidoCalificacion
    {
        public string Remision { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public DateTime Fechadetencion { get; set; }
        public string Unidad { get; set; }
        public DateTime Fecharegistro { get; set; }
        public int Totalhoras { get; set; }
        public string Motivodetencion { get; set; }
        public DateTime Fechasalida { get; set; }
        public int SituacionId { get; set; }
        public string Situacion { get; set; }
    }
}
