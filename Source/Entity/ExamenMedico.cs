﻿using System;
namespace Entity
{
    public class ExamenMedico
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int Edad { get; set; }
        public int SexoId { get; set; }
        public string InspeccionOcular { get; set; }
        public string IndicacionesMedicas { get; set; }
        public string Observacion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public bool RiesgoVida { get; set; }
        public bool ConsecuenciasLesiones { get; set; }
        public int DiasSanarLesiones { get; set; }
        public Catalogo TipoExamen { get; set; }
        public Int32 RegistradoPor { get; set; }
        public int DetalleDetencionId { get; set; }
        public DateTime Fecha { get; set; }
        public Boolean Lesion_visible { get; set; }
        public DateTime FechaRegistro { get; set; }
        public Boolean Fallecimiento { get; set; }
    }
}
