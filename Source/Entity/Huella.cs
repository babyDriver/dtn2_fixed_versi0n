﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Huella
    {
        public int Id { get; set; }
        public int DetenidoId { get; set; }
        public string Mano { get; set; }
        public string Dedo { get; set; }
        public string Fotografia { get; set; }
        public bool Activo { get; set; }
        public int Habilitado { get; set; }
    }
}
