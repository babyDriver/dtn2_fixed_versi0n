﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class EventoUnidadResponsable
    {
        public int Id { get; set; }
        public int EventoId { get; set; }
        public int UnidadId { get; set; }
        public int ResponsableId { get; set; }
        public string Unidad { get; set; }
        public string Responsable { get; set; }
    }
}
