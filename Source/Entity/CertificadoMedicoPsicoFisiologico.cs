﻿using System;


namespace Entity
{
   public  class CertificadoMedicoPsicoFisiologico
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public string Codigo { get; set; }
        public int AlientoId { get; set; }
        public int MarchaId { get; set; }
        public int ActitudId { get; set; }
        public int AtencionId { get; set; }
        public int Cavidad_oralId { get; set; }
        public int PupilaId { get; set; }
        public int Reflejo_pupilarId { get; set; }
        public int ConjuntivaId { get; set; }
        public int LenguajeId { get; set; }
        public int RombergId { get; set; }
        public int Ojos_abiertos_dedo_dedoId { get; set; }
        public int Ojos_cerrados_dedo_dedoId { get; set; }
        public int Ojos_abiertos_dedo_narizId { get; set; }
        public int Ojos_cerrados_dedo_narizId { get; set; }
        public string TA { get; set; }
        public string FC { get; set; }
        public string FR { get; set; }
        public string Pulso { get; set; }
        public Int16 No_valorabe { get; set; }
        public int OrientacionId { get; set; }
        public string Observacion_orientacion { get; set; }
        public int ConclusionId { get; set; }
        public Int16 Sustento_toxicologico { get; set; }
        public string Observacion_extra { get; set; }
        public DateTime FechaValoracion { get; set; }
        public int Folio { get; set; }
        public int ContratoId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int Registradopor { get; set; }
        public int Modificadopor { get; set; }
        public DateTime Fechaultimamodificacion { get; set; }
    }
}
