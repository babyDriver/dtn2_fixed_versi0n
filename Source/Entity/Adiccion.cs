﻿using System;
namespace Entity
{
    public class Adiccion
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Descripcion { get; set; }
        public string TipoAdiccion { get; set; }
        public string Recurrencia { get; set; }
        public string TiempoDeAdiccion { get; set; }
        public string Cantidad { get; set; }
        public string Efectos { get; set; }
        public bool Rehabilitado { get; set; }
        public DateTime? FechaRehabilitacion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public Detenido Interno { get; set; }

    }
}
