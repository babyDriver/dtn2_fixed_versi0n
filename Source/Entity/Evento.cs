﻿using System;

namespace Entity
{
    public class Evento
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int LlamadaId { get; set; }
        public string Descripcion { get; set; }
        public string Lugar { get; set; }        
        public string Folio { get; set; }
        public DateTime HoraYFecha { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int ColoniaId { get; set; }
        public int NumeroDetenidos { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public int IdEventoWS { get; set; }
        public int Eventoanioregistro { get; set; }
        public int Eventomesregistro { get; set; }
        public int MotivoId { get; set; }
        public int CreadoPor { get; set; }

    }
}
