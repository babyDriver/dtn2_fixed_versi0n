﻿using System;
namespace Entity
{
    public class Antecedente
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int ProcesoId { get; set; }
        public string Averiguacion { get; set; }
        public DateTime Fecha { get; set; }
        public string Consignado { get; set; }
        public DateTime FechaRadicacion { get; set; }
        public int Numero { get; set; }
        public DateTime FechaEjecucion { get; set; }
        public string Autoridad { get; set; }
        public DateTime FechaEmpezo { get; set; }
        public int Anios { get; set; }
        public int Meses { get; set; }
        public int Dias { get; set; }
        public int TipoId { get; set; }
        public int FueroId { get; set; }
        public int JuzgadoId { get; set; }
        public int CentroId { get; set; }
        public DateTime FechaPrision { get; set; }
        public DateTime FechaSentencia { get; set; }
        public DateTime FechaExternacion { get; set; }
        public string Motivo { get; set; }
        public string Terminos { get; set; }
        public string Observacion { get; set; }
        public bool Habilitado { get; set; }
        public int DetenidoId { get; set; }
    }
}
