﻿using System;

namespace Entity
{
    public class PermisoDefault
    {
        public Int32 RolId { get; set; }
        public Int32 PantallaId { get; set; }
        public Int32 PermisoId { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
    }
}
