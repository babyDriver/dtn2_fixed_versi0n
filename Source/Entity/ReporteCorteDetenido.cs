﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReporteCorteDetenido
    {
        public string Remision { get; set; }
        public string Evento { get; set; }
        public string Nombre { get; set; }
        public int? Edad { get; set; }
        public DateTime? Ingreso { get; set; }
        public string Calificación { get; set; }
        public string Celda { get; set; }
        public DateTime? Cumple { get; set; }
        public int? IdDetenido { get; set; }
    }
}
