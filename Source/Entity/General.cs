﻿using System;

namespace Entity
{
    public class General
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public DateTime FechaNacimineto { get; set; }
        public int NacionalidadId { get; set; }
        public string RFC { get; set; }
        public int EscolaridadId { get; set; }
        public int ReligionId { get; set; }
        public int OcupacionId { get; set; }
        public int EstadoCivilId { get; set; }
        public int EtniaId { get; set; }
        public int SexoId { get; set; }
        public bool EstadoMental { get; set; }
        public bool Inimputable { get; set; }
        public int DetenidoId { get; set; }
        public int LenguanativaId { get; set; }
        public string CURP { get; set; }
        public int Edaddetenido { get; set; }
        public decimal SalarioSemanal { get; set; }
        public int Generalanioregistro { get; set; }
        public int Generalmesregistro { get; set; }
    }
}
