﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ContratoCliente
    {
        public int ContratoId { get; set; }
        public int ClienteId { get; set; }
        public int CreadoPor { get; set; }
    }
}
