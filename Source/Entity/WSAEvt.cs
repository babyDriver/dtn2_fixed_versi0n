﻿using System;

namespace Entity
{
    public class WSAEvt {
		public int Id { get; set; }
		public int EventoId { get; set; }
		public string Referencia { get; set; }
		public string Folio { get; set; }
		public DateTime Fecha { get; set; }
		public int IdInstitucion { get; set; }
		public int IdUnidad { get; set; }
		public int IdOrigen { get; set; }
		public int IdVehiculo { get; set; }
        public int IdDomicilio { get; set; }
        public int IdColonia { get; set; }
        public int IdMotivo { get; set; }
        public int NoDetenidos { get; set; }
		public string Descripcion { get; set; }
        public bool Log { get; set; }
        public DateTime FechaCreacion { get; set; }
		public bool Habilitado { get; set; }

        //virtual d4t4//
		public string No { get; set; }
        public string Calle { get; set; }
        public string CalleRef { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string CodigoPostal { get; set; }
    }
}
