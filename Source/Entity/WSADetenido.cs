﻿using System;

namespace Entity
{
    public class WSADetenido
    {
        public int Id { get; set; }
        public int EventoId { get; set; }
        public Guid TrackingId { get; set; }
        public string Flag { get; set; }
        public int IdentCAD { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Alias { get; set; }
        public DateTime FechaCaptura { get; set; }
        public DateTime FechaNac { get; set; }
        public int Edad { get; set; }
        public int Sexo { get; set; }
        public int Nacionalidad { get; set; }
		public DateTime Fecha { get; set; }
        public bool Habilitado { get; set; }
    }
}
