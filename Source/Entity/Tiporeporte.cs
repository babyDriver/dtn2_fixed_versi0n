﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Tiporeporte
    {
        public int Id { get; set; }
        public string Reporte { get; set; }
        public int Activo { get; set; }
        public int Creadopor { get; set; }
        public int PantallaId { get; set; }
    }
}
