﻿using System;
namespace Entity
{
    public class PruebaExamenMedico
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int ExamenMedicoId { get; set; }
        public int MucosasId { get; set; }
        public int AlientoId { get; set; }
        public int Examen_neurologicoId { get; set; }
        public bool Disartia { get; set; }
        public int ConjuntivasId { get; set; }
        public int MarchaId { get; set; }
        public int PupilasId { get; set; }
        public int CoordinacionId { get; set; }
        public int Reflejos_pupilaresId { get; set; }
        public int TendinososId { get; set; }
        public int RomberqId { get; set; }
        public int ConductaId { get; set; }
        public int LenguajeId { get; set; }
        public int AtencionId { get; set; }
        public int OrientacionId { get; set; }
        public int DiadococinenciaId { get; set; }
        public int DedoId { get; set; }
        public int TalonId { get; set; }
        public string Alcoholimetro { get; set; }
        public string TA { get; set; }
        public string FC { get; set; }
        public string FR { get; set; }
        public string Pulso { get; set; }
        public Int32 RegistradoPor { get; set; }
    }
}
