﻿namespace Entity
{
    public class ListadoReporteCorteDetenidos
    {
        public string Remision { get; set; }
        public string Evento { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public string Ingreso { get; set; }
        public string Calificacion { get; set; }
        public string Celda { get; set; }
        public string Cumple { get; set; }
        public string Motivo { get; set; }
        public string Fecha { get; set; }
        public string Folio { get; set; }
        public string Motivos { get; set; }
        public string HoraDetencion { get; set; }
        public string Unidad { get; set; }
        public string Responsable { get; set; }
        public string Lugar { get; set; }
        public int IdDetalle { get; set; }
    }
}
