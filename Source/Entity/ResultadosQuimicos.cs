﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ResultadosQuimicos
    {
        public int Id { get; set; }
        public string Resultado { get; set; }
        public int Activo { get; set; }
    }
}
