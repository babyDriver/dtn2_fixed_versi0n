﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CrossReference
    {
        public int Id { get; set; }
        public string tcn { get; set; }
        public string tcn_candidate { get; set; }
        public string scores { get; set; }
        public string identification { get; set; }
        public string product { get; set; }
        public string transaction_type { get; set; }
        public string biometric_type { get; set; }
        public DateTime received_datetime { get; set; }
        public string ip { get; set; }
        public string result { get; set; }
        public int ContratoId { get; set; }
        public int DetenidoId { get; set; }
    }
}
