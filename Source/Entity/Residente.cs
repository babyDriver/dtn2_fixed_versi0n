﻿using System;

namespace Entity
{
    public class Residente
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Estancia { get; set; }
        public string Modulo { get; set; }
        public string Pasillo { get; set; }
        public string Dormitorio { get; set; }
        public string Seccion { get; set; }
        public string Celda { get; set; }
        public string Nivel { get; set; }
        public string Cama { get; set; }
        public int EstatusInternoId { get; set; }
    }
}
