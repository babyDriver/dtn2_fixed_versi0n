﻿using System;

namespace Entity
{
    public class Reportes
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int? CreadoPor { get; set; }
        public int? ContratoId { get; set; }
        public string Tipo { get; set; }
        public int PantallaId { get; set; }
    }
}
