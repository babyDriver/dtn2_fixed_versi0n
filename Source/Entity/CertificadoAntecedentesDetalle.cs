﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CertificadoAntecedentesDetalle
    {
        public int Id { get; set; }
        public int certificado_LesionAntecedentesId { get; set; }
        public int AntecedenteId { get; set; }
    }
}
