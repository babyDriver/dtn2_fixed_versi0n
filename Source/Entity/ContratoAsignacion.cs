﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ContratoAsignacion
    {
        public int Id { get; set; }
        public string Contrato { get; set; }
        public DateTime VigenciaInicial { get; set; }
        public DateTime VigenciaFinal { get; set; }
        public int? IdUsuario { get; set; }
        public bool Activo { get; set; }
        public string Tipo { get; set; }
    }
}
