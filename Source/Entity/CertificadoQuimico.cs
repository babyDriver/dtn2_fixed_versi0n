﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CertificadoQuimico
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public int Folio { get; set; }
        public DateTime Fecha_toma { get; set; }
        public DateTime Fecha_proceso { get; set; }
        public int EtanolId { get; set; }
        public int Grado { get; set; }
        public int BenzodiapinaId { get; set; }
        public int AnfetaminaId { get; set; }
        public int CannabisId { get; set; }
        public int CocainaId { get; set; }
        public int ExtasisId { get; set; }
        public Int16 Dimension { get; set; }
        public Int16 Axysm { get; set; }
        public Int16 Architect { get; set; }
        public Int16 VivaE { get; set; }
        public int ContratoId { get; set; }
        public int EquipoautilizarId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int Registradopor { get; set; }
        public int Modificadopor { get; set; }
        public DateTime Fechaultimamodificacion { get; set; }
        public string Foliocertificado { get; set; }
        public int Edad { get; set; }
        public int DetenidoId { get; set; }


    }
}
