﻿namespace Entity.Util
{
    public class Combo
    {
        public string Id { get; set; }
        public string Desc { get; set; }
        public string Selected { get; set; }
        public string AdicionalUno { get; set; }
        public string AdicionalDos { get; set; }
    }
}
