﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Traslado
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public int DetalleDetencionId { get; set; }
        public int DelegacionId { get; set; }
        public DateTime Fechahora { get; set; }
        public int CreadoPor { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; }
        public int TiposalidaId { get; set; }
        public string Fundamento { get; set; }
        public int Horas { get; set; }

    }
}
