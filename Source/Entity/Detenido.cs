﻿using System;
namespace Entity
{
    public class Detenido
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string RutaImagen { get; set; }
        public int DomicilioId { get; set; }
        public int DomicilioNId { get; set; }
        //public Boolean Lesion_visible { get; set; }
        public int Detenidoanioregistro { get; set; }
        public int Detenidomesregistro { get; set; }

    }
}
