﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TipoBiometrico
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; } 
        public int Creadopor { get; set; }

    }
}
