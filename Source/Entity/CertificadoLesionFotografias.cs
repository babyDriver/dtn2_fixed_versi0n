﻿using System;

namespace Entity
{
    public class CertificadoLesionFotografias
    {
        public int Id { get; set; }
        public DateTime FechaHora { get; set; }
        public int Creadopor { get; set; }
    }
}
