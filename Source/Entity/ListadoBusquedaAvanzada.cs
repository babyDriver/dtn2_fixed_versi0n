﻿using System;

namespace Entity
{
    public class ListadoBusquedaAvanzada
    {
        public Guid TrackingId { get; set; }
        public Guid TkgDet { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Expediente { get; set; }
        public string Alias { get; set; }
        public string Evento { get; set; }
        public string DescripcionHechos { get; set; }
        public string Folio { get; set; }
        public string Unidad { get; set; }
        public string Corporacion { get; set; }
        public int Horas { get; set; }
        public string Motivo { get; set; }
        public int Edad { get; set; }
        public string NombreCompleto { get; set; }
    }
}
