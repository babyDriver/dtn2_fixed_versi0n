﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Detenidosporcolonia
    {
        public int MunicipioId { get; set; }
        public string Municipio { get; set; }
        public int ColoniaId { get; set; }
        public string Colonia { get; set; }
        public int Cantidad { get; set; }
        public DateTime Fecha { get; set; }
    }
}
