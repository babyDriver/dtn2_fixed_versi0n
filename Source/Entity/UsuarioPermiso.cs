using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class UsuarioPermiso
    {
        public int UsuarioId { get; set; }
        public int PermisoId { get; set; }
        public bool Activo { get; set; }
        public int PantallaId { get; set; }
        public Guid TrackingId { get; set; }
        public int Id { get; set; }
        public bool Habilitado { get; set; }
    }
}
