﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TipoMotivoDetencion
    {
        public int TipoId { get; set; }
        public string Descripcion { get; set; }
    }
}
