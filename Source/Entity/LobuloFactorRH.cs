﻿using System;

namespace Entity
{
    public class LobuloFactorRH
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public Int32? AdherenciaL { get; set; }
        public Int32? DimensionL { get; set; }
        public int ParticularidadL { get; set; }
        public int TragoL { get; set; }
        public int AntitragoL { get; set; }
        public int SangreF { get; set; }
        public int TipoF { get; set; }
        public int AntropometriaId { get; set; }
    }
}
