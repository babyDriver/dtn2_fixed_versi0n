﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CertificadoLesion
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        //public int Aptitud { get; set; }
        public int Tipo_lesionId { get; set; }
        public string Observaciones_lesion { get; set; }
        public Int16 Lesion_simple_vista { get; set; }
        public Int16 Fallecimiento { get; set; }
        public Int16 Envio_hospital { get; set; }
        public Int16 Peligro_de_vida { get; set; }
        public Int16 Consecuencias_medico_legales { get; set; }
        public int Tipo_alergiaId { get; set; }
        public string Observacion_alergia { get; set; }
        public int Tipo_tatuajeId { get; set; }
        public string Observacion_tatuje { get; set; }
        public string Observacion_general { get; set; }
        public int AntecedentesId { get; set; }
        public string Observaciones_antecedentes { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int Registradopor { get; set; }
        public int Modificadopor { get; set; }
        public DateTime Fechaultimamodificacion { get; set; }
        public Int16 Sinlesion { get; set; }
        public int FotografiasId { get; set; }
        public DateTime Fechavaloracion { get; set; }
    }
}
