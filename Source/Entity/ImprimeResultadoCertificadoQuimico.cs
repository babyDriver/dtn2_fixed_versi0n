﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ImprimeResultadoCertificadoQuimico
    {
        public bool PrintEtanol { get; set; }
        public bool PrintBenzodiazepina { get; set; }
        public bool PrintAnfetamina { get; set; }
        public bool PrintCannabis { get; set; }
        public bool PrintCocaina { get; set; }
        public bool PrintExtasis { get; set; }
    }
}
