﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class CertificadoPsicoFisilogicoOrientacion
    {
        public int Id { get; set; }
        public DateTime FechaHora { get; set; }
        public int Creadopor { get; set; }
    }
}
