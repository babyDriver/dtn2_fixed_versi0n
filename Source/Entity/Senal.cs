﻿using System;

namespace Entity
{
    public class Senal
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int Tipo { get; set; }
        public int Lado { get; set; }
        public string Region { get; set; }
        public int Vista { get; set; }
        public int Cantidad { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public int DetenidoId { get; set; }
        public int Habilitado { get; set; }
        public int Creadopor { get; set; }
        public int? LugarId { get; set; }
    }
}
