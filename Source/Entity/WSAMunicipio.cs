﻿using System;

namespace Entity
{
    public class WSAMunicipio
    {
        public int IdMunicipio { get; set; }
        public string NombreMunicipio { get; set; }
        public int IdEstado { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool Activo { get; set; }
    }
}
