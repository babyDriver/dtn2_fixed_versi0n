﻿using System;

namespace Entity
{
    public class OjoNariz
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int ColorO { get; set; }
        public int FormaO { get; set; }
        public Int32? TamanoO { get; set; }
        public Int32? RaizN { get; set; }
        public Int32? AlturaN { get; set; }
        public Int32? AnchoN { get; set; }
        public int DorsoN { get; set; }
        public int BaseN { get; set; }
        public int AntropometriaId { get; set; }
    }
}
