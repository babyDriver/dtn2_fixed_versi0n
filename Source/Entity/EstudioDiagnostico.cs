﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class EstudioDiagnostico
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public int DetalledetencionId { get; set; }
        public DateTime Fechayhora { get; set; }
        public int TipodediagnosticoId { get; set; }
        public string Detallediagnostico { get; set; }
        public string Observaciones { get; set; }
        public int Creadopor { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; }
    }
}
