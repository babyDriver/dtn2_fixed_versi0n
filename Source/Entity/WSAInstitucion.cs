﻿using System;

namespace Entity
{
    public class WSAInstitucion
    {
        public int IdInstitucion { get; set; }
        public string NombreInstitucion { get; set; }
        public string NombreCorto { get; set; }
        public int IdMunicipio { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool Activo { get; set; }
    }
}
