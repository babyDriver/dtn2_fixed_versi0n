﻿namespace Entity
{
    public class DetenidosActivosSelect
    {
        public int Id { get; set; }
        public string Expediente { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public int IdDetalle { get; set; }
        public string NombreCombpleto { get; set; }
    }
}
