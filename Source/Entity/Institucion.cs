﻿using System;

namespace Entity
{
    public class Institucion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int DomicilioId { get; set; }
        public string Telefono { get; set; }
        public string Encargado { get; set; }
        public string Clave { get; set; }
        public Estado Estado { get; set; }
        public int CupoHombres { get; set; }
        public int CupoMujeres { get; set; }
        public Guid TrackingId { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int Creadopor { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
    }
}