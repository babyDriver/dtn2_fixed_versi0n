﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Link
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int SubcontratoId { get; set; }
        public string Leyenda { get; set; }
        public int Creadopor { get; set; }
    }
}
