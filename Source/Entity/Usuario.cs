using System;

namespace Entity
{
    public class Usuario
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int EmpresaId { get; set; }
        public string Nombre { get; set; }
        public string Avatar { get; set; }
        public string Puesto { get; set; }
        public string Movil { get; set; }
        public string Oficina { get; set; }
        public DateTime UltimaActualizacion { get; set; }
        public string User { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Email { get; set; }
        public int RolId { get; set; }
        //public int CentroId { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public string CUIP { get; set; }
        public Boolean BanderaPassword { get; set; }
    }
}
