﻿using System;

namespace Entity
{
    public class AdicionalExamenMedico
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int ExamenMedicoId { get; set; }
        public int? Lugar { get; set; }
        public string Observacion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public Catalogo Tipo { get; set; }
        public int Creadopor { get; set; }
        public string Clasificacion { get; set; }

    }
}
