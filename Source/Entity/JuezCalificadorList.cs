﻿using System;

namespace Entity
{
    public class JuezCalificadorList
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string RutaImagen { get; set; }
        public string Expediente { get; set; }
        public string NCP { get; set; }
        public int Estatus { get; set; }
        public bool Activo { get; set; }
        public Guid TrackingIdEstatus { get; set; }
        public string EstatusNombre { get; set; }
        public int DetalleDetencionId { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string NombreCompleto { get; set; }
        public int CalificacionId { get; set; }
        public decimal TotalAPagar { get; set; }
        public string Situacion { get; set; }
        public int IngresoId { get; set; }
        public string horas { get; set; }
    }
}
