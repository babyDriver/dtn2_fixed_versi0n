﻿using System;

namespace Entity
{
    public class ListadoReporteOficio
    {
        public Guid EstatusTracking { get; set; }
        public string RutaImagen { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Expediente { get; set; }
        public string Fecha { get; set; }
        public Guid TrackingId { get; set; }
        public string NombreCompleto { get; set; }
        public string Motivo { get; set; }
        public string LugarDetencion { get; set; }
        public string Unidad { get; set; }
        public string Responsable { get; set; }
        public DateTime Edad { get; set; }
        public int EdadDetenido { get; set; }
    }
}
