﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class UsuarioPantalla
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Consultar { get; set; }
        public bool Registrar { get; set; }
        public bool Modificar { get; set; }
        public bool Eliminar { get; set; }
    }
}
