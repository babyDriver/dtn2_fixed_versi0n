﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Biometrico
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public int DetenidoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Rutaimagen { get; set; }
        public int TipobiometricoId { get; set; }
        public string Clave { get; set; }
        public DateTime FechaHora { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; }
        public int Creadopor { get; set; }
    }
}
