﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class InformacionDeDetencion
    {
        public int Id { get; set; }
        public int IdInterno { get; set; }
        public Guid TrackingId { get; set; }
        public int DetalleDetencionId { get; set; }
        public int UnidadId { get; set; }
        public int ResponsableId { get; set; }
        public string Motivo { get; set; }
        public string Descripcion { get; set; }
        public string LugarDetencion { get; set; }
        public string Folio { get; set; }
        public DateTime HoraYFecha { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int ColoniaId { get; set; }
        public int IdEvento { get; set; }
        public int CreadoPor { get; set; }
        public string Personanotifica { get; set; }
        public string Celular { get; set; }
        public string Detalletrayectoria { get; set; }
        public int IdDetenido_Evento { get; set; }
    }
}
