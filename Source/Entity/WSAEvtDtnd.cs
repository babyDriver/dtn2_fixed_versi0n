﻿using System;

namespace Entity
{
    public class WSAEvtDtnd {
		public int Id { get; set; }
		public int IdEvento { get; set; }
		public string Folio { get; set; }
		public DateTime Fecha { get; set; }
		public int IdInstitucion { get; set; }
		public int IdOrigen { get; set; }
		public string Origen { get; set; }
		public int IdMotivo { get; set; }
		public string Calle { get; set; }
		public string CalleRef { get; set; }
		public string Latitud { get; set; }
		public string Longitud { get; set; }
		public string CodigoPostal { get; set; }
		public int IdColonia { get; set; }
		public int IdMunicipio { get; set; }
		public int IdEstado { get; set; }
		public int NoDetenidos { get; set; }
		public string Descripcion { get; set; }
		public string Nombre { get; set; }
		public string Paterno { get; set; }
		public string Materno { get; set; }
		public string Alias { get; set; }
		public DateTime FechaCaptura { get; set; }
		public DateTime FechaNac { get; set; }
		public int Edad { get; set; }
		public int IdSexo { get; set; }
		public int Nacionalidad { get; set; }
		public int Telefono { get; set; }
		public int Papeles { get; set; }
	}
}
