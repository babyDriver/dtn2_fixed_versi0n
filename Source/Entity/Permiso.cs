﻿using System;

namespace Entity
{
    public class Permiso
    {
        public string Rol { get; set; }
        public bool AgregarContrato { get; set; }
        public bool Agregar { get; set; }
        public bool Modificar { get; set; }
        public bool Eliminar { get; set; }
    }
}
