﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReporteEstadisticoPorLesion
    {
        public int DetenidoId { get; set; }
        public string Lesion { get; set; }
        public int Cantidad { get; set; } 
        public int Edad { get; set; }
    }
}
