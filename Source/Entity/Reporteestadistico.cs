﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Reporteestadistico
    {
        public int Id { get; set; }
        public int Tipo_reporteId { get; set; } 
        public string Descripcion { get; set; }
        public int Activo { get; set; }
        public int ReporteId { get; set; }
    }
}
