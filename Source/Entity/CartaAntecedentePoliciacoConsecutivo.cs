﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CartaAntecedentePoliciacoConsecutivo

    {
        public int Id { get; set; }
        public int ContratoId { get; set; }
        public int Consecutivo { get; set; }
        public string Ciudadano { get; set; }
        public DateTime Fecha { get; set; }
        public string Rutafoto { get; set; }
        public string Domicilio { get; set; }
        public string Ciudad { get; set; }
        public int Antecedentes { get; set; }
        public int Anio { get; set; }
        public DateTime FechaAntecedente { get; set; }
        public string Elaboro{ get; set; }
        public string Reviso { get; set; }
        public string ReciboCaja{ get; set; }
        public string TrakingId{ get; set; }
        public int CreadoPor { get; set; }
        public string cuerpo { get; set; }

    }
}
