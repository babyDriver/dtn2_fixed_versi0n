﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TrabajoSocialBox
    {
        public int Id { get; set; }
        public int InternoId { get; set; }
        public Guid TrackingId { get; set; }
        public string Programa { get; set; }
        public decimal PorcentajeHoras { get; set; }

    }
}

