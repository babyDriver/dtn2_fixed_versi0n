﻿using System;

namespace Entity
{
    public class Catalogo
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public TipoDeCatalogo tipo { get; set; }
        public int Creadopor { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
    }
}
