﻿using System;

namespace Entity
{
    public class Modelo
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public Catalogo marca_vehiculo { get; set; }
        public int Creadopor { get; set; }
    }
}
