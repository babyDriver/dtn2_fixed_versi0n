﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class SalidaEfectuada
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public int DetalledetencionId { get; set; }
        public string Observacion { get; set; }
        public string Responsabledeldetenido { get; set; }
        public int Creadopor { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; }
    }
}
