﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class EgresoIngreso
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public DateTime Fecha { get; set; }
        public int Folio { get; set; }
        public string Concepto { get; set; }
        public decimal Total { get; set; }
        public string PersonaQuePaga { get; set; }
        public string Observacion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public bool Egreso { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
    }
}
