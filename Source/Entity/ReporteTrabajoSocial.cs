﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class ReporteTrabajoSocial
    {
        public string Delegacion { get; set; }
        public string Nombre { get; set; }
        public string Alias { get; set; }
        public string Escolaridad { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public int  Edad { get; set; }
        public string EstadoCivil { get; set; }
        public string Domicilio { get; set; }
        public string Telefono { get; set; }
        public string Colonia { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string Nacionalidad { get; set; }
        public string Expediente { get; set; }
        public DateTime Fecha { get; set; }
        public string Motivo { get; set; }
        public string LugarDetencion { get; set; }
        public string Folio { get; set; }
        public string MotivoRehabilitacion { get; set; }
        public string Adiccion { get; set; }
        public string Religion { get; set; }
        public string Pandilla { get; set; }
        public string CuadroPatologico { get; set; }
        public string Observacion { get; set; }
        public string NombreMadre { get; set; }
        public string CelularMadre { get; set; }
        public string NombrePadre { get; set; }
        public string CelularPadre { get; set; }
        public string Tutor { get; set; }
        public string Celulartutor { get; set; }
        public string DomicilioMadre { get; set; }
        public string DomicilioPadre { get; set; }
        public string EscolaridadTS { get; set; }
        public string Notificado { get; set; }
        public string CelularNotificado { get; set; }
    }
}
