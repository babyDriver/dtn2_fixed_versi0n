﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Subcontrato
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int ContratoId { get; set; }
        public string Nombre { get; set; }        
        public DateTime VigenciaInicial { get; set; }
        public DateTime? VigenciaFinal { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public int InstitucionId { get; set; }
        public string Logotipo { get; set; }
        public decimal SalarioMinimo { get; set; }
        public int DivisaId { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Banner { get; set; }
    }
}
