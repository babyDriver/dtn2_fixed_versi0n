﻿using System;

namespace Entity
{
    public class SubcontratoWSAInstitucion
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int IdSubcontrato { get; set; }
        public int IdWSAInstitucion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
    }
}
