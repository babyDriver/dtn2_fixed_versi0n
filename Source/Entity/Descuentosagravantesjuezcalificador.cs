﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Descuentosagravantesjuezcalificador
    {
        public string Remision { get; set; }
        public string Detenido { get; set; }
        public DateTime Fechacalificacion { get; set; }
        public Decimal Total { get; set; }
        public Decimal Descuento{ get; set; }
        public Decimal Agravante{ get; set; }
        public Decimal Totalapagar{ get; set; }
        public string Razon { get; set; }


    }
}
