﻿using System;

namespace Entity
{
    public class WSAUnidad
    {
        public int IdUnidadInstitucion { get; set; }
        public int IdInstitucion { get; set; }
        public string ClaveUnidad { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool Activo { get; set; }
    }
}
