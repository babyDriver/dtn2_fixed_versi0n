﻿using System;

namespace Entity
{
    public class DetalleDetencion
    {
        public int No { get; set; }
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Expediente { get; set; }
        public DateTime? Fecha { get; set; }
        public string ExpedienteAdm { get { return No.ToString(); } set { if (value == "") No.ToString("No"); } }
        public string NCP { get; set; }
        public bool Activo { get; set; }
        public int CentroId { get; set; }
        public int DetenidoId { get; set; }
        public int Estatus { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
        public Boolean Lesion_visible { get; set; }
        public int Detalledetencionanioregistro { get; set; }
        public int Detalldetencionmesregistro { get; set; }
        public int DetalledetencionSexoId { get; set; }
        public string NombreDetenido { get; set; }
        public string APaternoDetenido { get; set; }
        public string AMaternoDetenido { get; set; }
        public int DetalledetencionEdad { get; set; }

}
    
}
