﻿using System;

namespace Entity
{
    public class ListadoExamenMedico
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Expediente { get; set; }
        public string NCP { get; set; }
        public int Estatus { get; set; }
        public bool Activo { get; set; }
        public Guid TrackingIdEstatus { get; set; }
        public string RutaImagen { get; set; }
        public int ExamenId { get; set; }
        public Guid ExamenTrackingId { get; set; }
        public string Registro { get; set; }
        public string Situacion { get; set; }
        public string NombreCompleto { get; set; }
    }
}
