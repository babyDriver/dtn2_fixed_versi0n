﻿namespace Entity
{
    public class ListadoReporteDetencionBoleta
    {
        public int Id { get; set; }
        public string Expediente { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Sexo { get; set; }
        public string NombreCompleto { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public string Colonia { get; set; }
    }
}
