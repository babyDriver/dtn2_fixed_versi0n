﻿using System;

namespace Entity
{
    public class Procedencia
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Remisa { get; set; }
        public string Expedir { get; set; }
        public int EstadoId { get; set; }
        public int CentroId { get; set; }
        public bool Activo { get; set; }
        public int DetenidoId { get; set; }
    }
}
