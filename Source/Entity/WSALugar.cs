﻿using System;

namespace Entity
{
    public class WSALugar
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int IdEstado { get; set; }
        public int IdMunicipio { get; set; }
        public int IdColonia { get; set; }
        public string Numero { get; set; }
        public string EntreCalle { get; set; }
        public string Sector { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Ycalle { get; set; }
    }
}
