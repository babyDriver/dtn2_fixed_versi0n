﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ExpedienteTrabajoSocial
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public int DetalledetencionId { get; set; }
        public int MotivorehabilitacionId { get; set; }
        public int AdiccionId { get; set; }
        public string Pandilla { get; set; }
        public int ReligionId { get; set; }
        public string Cuadropatologico { get; set; }
        public string Observacion { get; set; }
        public int Creadopor { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; }
        public string Nombremadre { get; set; }
        public string Celularmadre { get; set; }
        public string Nombrepadre { get; set; }
        public string Celularpadre { get; set; }
        public string Tutor { get; set; }
        public string Celulartutor { get; set; }
        public int EscolaridadId { get; set; }
        public string Notificado { get; set; }
        public string Celularnotificado { get; set; }
        public string Domiciliomadre { get; set; }
        public string Domiciliopadre { get; set; }
    }
}
