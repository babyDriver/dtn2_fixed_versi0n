﻿using System;

namespace Entity
{
    public class WSAColonia
    {
        public int IdColonia { get; set; }
        public string NombreColonia { get; set; }
        public int IdMunicipio { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool Activo { get; set; }
    }
}
