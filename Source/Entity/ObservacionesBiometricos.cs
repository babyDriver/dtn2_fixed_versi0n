﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class ObservacionesBiometricos
    {
        public int Id { get; set; }
        public int DetenidoId { get; set; }
        public string Observaciones { get; set; }
    }
}
