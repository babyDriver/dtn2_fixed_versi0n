﻿using System;

namespace Entity
{
    public class Otro_Nombre
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public bool Activo { get; set; }
        public int DetenidoId { get; set; }
    }
}
