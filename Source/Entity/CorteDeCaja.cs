﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CorteDeCaja
    {
        public int Id { get; set; }        
        public Guid TrackignId { get; set; }
        public decimal SaldoInicial { get; set; }
        public DateTime Fecha { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public DateTime? FechaFin { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
    }
}
