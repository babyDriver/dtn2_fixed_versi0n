﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class EventoReciente
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int Hora { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int Creadopor { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
    }
}
