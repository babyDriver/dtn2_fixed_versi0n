﻿using System;
namespace Entity
{
    public class NucleoSecundario
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string NombrePareja { get; set; }
        public Int32? EdadPareja { get; set; }
        public Catalogo OcupacionPareja { get; set; }
        public string ActitudesAntisocialesPareja { get; set; }
        public string RelacionAfectivaPareja { get; set; }
        public string Hijos { get; set; }
        public string RelacionHijos { get; set; }
        public string HijoAllegado { get; set; }
        public string ActitudAntisocialHijos { get; set; }
        public string MotivoNoNucleoSecundario { get; set; }
       

    }
}
