﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Unidad
    {
        public int Id { get; set; }
        public string TrackingId { get; set; } 
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int Creadopor { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
        public string Placa { get; set; }
        public int CorporacionId { get; set; }

    }
}
