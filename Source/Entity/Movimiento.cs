﻿using System;

namespace Entity
{
    public class Movimiento
    {
        public int Id { get; set; }
        public string TrackingId { get; set; }
        public int DetalledetencionId { get; set; }
        public int TipomovimientoId { get; set; }
        public DateTime FechaYHora { get; set; }
        public int CeldaId { get; set; }
        public string Observacion { get; set; }
        public string Responsable { get; set; }
        public int InstitucionId { get; set; }
        public int Creadopor { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; }
    }
}
