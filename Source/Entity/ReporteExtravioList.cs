﻿using System;

namespace Entity
{
    public class ReporteExtravioList
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string RutaImagen { get; set; }
        public string Expediente { get; set; }
        public string NCP { get; set; }
        public int Estatus { get; set; }
        public bool Activo { get; set; }
        public Guid TrackingIdEstatus { get; set; }
        public int Edad { get; set; }
        public string AliasInterno { get; set; }
        public DateTime FechaNacimiento { get; set; }
    }
}
