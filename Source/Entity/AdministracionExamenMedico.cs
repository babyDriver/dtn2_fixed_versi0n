﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public  class AdministracionExamenMedico
    {
        public int Id { get; set; }

        public string TrackingId{ get; set; }

        public int DetalledetencionId { get; set; }

        public DateTime Fecha{ get; set; }

        public int CreadoPor{ get; set; }

        public string Justificacion{ get; set; }
}
}
