﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class DetenidosPorTipoLesion
    {
        public DateTime Fecha { get; set; }
        public int TipolesionId { get; set; }
        public string Tipolesion { get; set; }
        public int Cantidad { get; set; }
        public int DetenidoId { get; set; }
        public int Subcontrato { get; set; }
    }
}
