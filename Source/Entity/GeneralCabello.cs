﻿using System;

namespace Entity
{
    public class GeneralCabello
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int ComplexionG { get; set; }
        public int ColorPielG { get; set; }
        public int CaraG { get; set; }
        public int CantidadC { get; set; }
        public int ColorC { get; set; }
        public int FormaC { get; set; }
        public int CalvicieC { get; set; }
        public int ImplementacionC { get; set; }
        public int AntropometriaId { get; set; }
    }
}
