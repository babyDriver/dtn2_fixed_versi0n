﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class DashboarDetenidos
    {
        public int DetenidoId { get; set; }
        public int DetalledetencionId { get; set; }
        public int SexoId  { get; set; }
        public int Estatus { get; set; }
        public int Activo { get; set; }
        public int TrabajosocialId { get; set; }
        public int MovimentoceldaId { get; set; }
        public int TipoMovimientoCelda { get; set; }
        public int EstatusMovimiento { get; set; }
        public int ContratoId { get; set; }
        public DateTime Fecha { get; set; }

    }
}
