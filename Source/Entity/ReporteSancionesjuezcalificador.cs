﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class ReporteSancionesjuezcalificador
    {
        public int Id { get; set; }

        public string TrackingId { get; set; }

        public string Juez { get; set; }

        public string Expediente { get; set; }

        public DateTime Fecha { get; set; }

        public string Detenido { get; set; }

        public decimal Totaldemultas { get; set; }

        public int Totalhoras { get; set; }
               
    }
}
