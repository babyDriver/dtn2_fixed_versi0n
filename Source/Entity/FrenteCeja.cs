﻿using System;

namespace Entity
{
    public class FrenteCeja
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public Int32? AlturaF { get; set; }
        public Int32? AnchoF { get; set; }
        public int InclinacionF { get; set; }
        public int DireccionC { get; set; }
        public int ImplantacionC { get; set; }
        public int FormaC { get; set; }
        public int TamanoC { get; set; }
        public int AntropometriaId { get; set; }
    }
}
