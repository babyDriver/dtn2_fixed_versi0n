﻿using System;

namespace Entity
{
    public class Zona
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Descripcion { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public Guid? RegistradoPor { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public Guid? ModificadoPor { get; set; }
        public bool Activo { get; set; }
    }
}