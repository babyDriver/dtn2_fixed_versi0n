﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReporteBoletaControl
    {
        public string Remision { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Sexo { get; set; }
        public string Domicilio { get; set; }
        public string Escolaridad { get; set; }
        public string Estado { get; set; }
        public string Localidad { get; set; }
        public string Unidad { get; set; }
        public string Responsable { get; set; }
        public string Motivo { get; set; }
        public string LugarDetencion { get; set; }
        public string ColoniaDetencion { get; set; }
        public string Situacion { get; set; }
        public string Califico { get; set; }
        public string TotalHoras { get; set; }
        public string Multa { get; set; }
        public DateTime FechaMedico { get; set; }
        public string Realizador { get; set; }
        public string Diagnostico { get; set; }
    }
}
