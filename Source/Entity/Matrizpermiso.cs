﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Matrizpermiso
    {
        public int UsuarioId { get; set; }
        public int PermisoId { get; set; }
        public int PantallaId { get; set; }
        public int RolId { get; set; }
        public bool Habilitado { get; set; }
    }
}
