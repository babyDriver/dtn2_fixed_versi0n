﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReporteEstadisticoDetenidoPorTipoLesionCL
    {
        public int DetenidoId { get; set; }
        public string Unidad { get; set; }
        public string Lesion { get; set; }
        public DateTime Fecha { get; set; }
        public int Edad { get; set; }
    }
}
