﻿using System;

namespace Entity
{
    public class AlertaWeb
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string UrlWebService { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int CreadoPor { get; set; }
        public DateTime FechaModificacion { get; set; }
        public int ModificadoPor { get; set; }
        public string Denominacion { get; set; }
    }
}
