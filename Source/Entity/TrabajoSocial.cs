﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TrabajoSocial
    {

        public int Id { get; set; }
        public string TrackingId { get; set; }    
        public int DetalledetencionId { get; set; }
        public string Programa { get; set; }
        public decimal PorcentajeHoras { get; set; }
        public DateTime FechaHora { get; set; }
        public int Activo { get; set; }
        public int Habilitado { get; set; }
        public int Creadopor { get; set; }

    }
}
