﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class DatosDetenido
    {

        public string CentroReclusion { get; set; }
        public string Expediente { get; set; }
        public DateTime Fecha { get; set; }
        public string Nombredetenido { get; set; }
        public int Edad { get; set; }
        public string Sexo { get; set; }
        public string EstadoCivil { get; set; }
        public string Escolaridad { get; set; }
        public string Nacionalidad { get; set; }
        public string Domicilio { get; set; }
        public string Colonia { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string Telefono { get; set; }
        public string Unidad { get; set; }
        public string Responsableunidad { get; set; }
        public int InformacionDetencionId { get; set; }
        public string Descripcion { get; set; }
        public string Lugardetencion { get; set; }
        public string Coloniadetencion { get; set; }
        public int CalificacionId { get; set; }
        public int Soloarresto { get; set; }
        public int Totalhoras { get; set; } 
        public decimal Totalapagar { get; set; }
        public string Fundamento { get; set; }
        public string Razon { get; set; }
        public int UnidadId { get; set; }
        public string Situacion { get; set; }
        public string Usuariocalifico { get; set; }
        public string FolioEvento { get; set; }
    }
}
