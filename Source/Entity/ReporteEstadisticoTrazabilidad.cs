﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class ReporteEstadisticoTrazabilidad
    {
        public string Nombre { get; set; }
        public string Remision { get; set; }
        public string Proceso { get; set; }
        public string Subproceso { get; set; }
        public string Usuario { get; set; }
        public string Estatus { get; set; }
        public DateTime Fechayhora { get; set; }
    }
}
