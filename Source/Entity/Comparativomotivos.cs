﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Comparativomotivos
    {
        public string Remision { get; set; }
        public string Detenido { get; set; }
        public string Registro { get; set; }
        public string Motivos { get; set; }
        public string Califico { get; set; }
        public string Motivocalificacion { get; set; }
        public string Foliollamada { get; set; }
        public string Registrollamada { get; set; }
        public string Motivollamada { get; set; }
        public DateTime Fecha { get; set; }
        public int Edad;
    }
}
