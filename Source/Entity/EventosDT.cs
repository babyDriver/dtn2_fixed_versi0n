﻿using System;

namespace Entity
{
    public class EventosDT
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string DescripcionLlamada { get; set; }
        public string Descripcion { get; set; }
        public string LugarDetencion { get; set; }
        public string Asentamiento { get; set; }
        public string CodigoPostal { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public int IdColonia { get; set; }
        public string Folio { get; set; }
        public string HoraYFecha { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
    }
}
