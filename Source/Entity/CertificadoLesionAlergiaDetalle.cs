﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CertificadoLesionAlergiaDetalle
    {
        public int Id { get; set; }
        public int certificado_LesionAlergiaId { get; set; }
        public int AlergiaId { get; set; }
    }
}
