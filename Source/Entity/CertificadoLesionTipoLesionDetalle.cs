﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CertificadoLesionTipoLesionDetalle
    {
        public int Id { get; set; }
        public int Certificado_LesionTipoLesionId { get; set; }
        public int TipoLesionId { get; set; }
    }
}
