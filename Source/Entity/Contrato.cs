﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Contrato
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public int ClienteId { get; set; }
        public string Nombre { get; set; }
        public DateTime VigenciaInicial { get; set; }
        public DateTime VigenciaFinal { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public int InstitucionId { get; set; }
    }
}
