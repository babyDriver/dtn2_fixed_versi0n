﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class DetalleCelda
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public int Capacidad { get; set; }

        public int TipoMovimientoId { get; set; }

        public int Estatus { get; set; }

         
    }
}
