﻿using System;
namespace Entity
{
    public class Alias
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public bool Activo { get; set; }
        public int DetenidoId { get; set; }
        public int Habilitado { get; set; }
        public int Creadopor { get; set; }
    }
}
