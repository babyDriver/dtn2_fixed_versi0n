﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class DevolucionPertenencia
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }        
        public string PertenenciasDe { get; set; }
        public string UsuarioQueRegistro { get; set; }
        public string PersonaQueRecibe { get; set; }
        public string Condicion { get; set; }
        public int Entrega { get; set; }
        public DateTime FechaEntrega { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
    }
}
