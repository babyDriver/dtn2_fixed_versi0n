﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ContratoUsuario
    {
        public int IdContrato { get; set; }
        public int IdUsuario { get; set; }
        public Guid TrackingId { get; set; }
        public int Id { get; set; }
        public int CreadoPor { get; set; }
        public bool Activo { get; set; }
        public string Tipo { get; set; }
    }
}
