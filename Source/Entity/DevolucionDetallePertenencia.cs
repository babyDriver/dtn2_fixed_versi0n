﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class DevolucionDetallePertenencia
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }        
        public int PertenenciaId { get; set; }
        public int DevolucionId { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
    }
}
