﻿using System;

namespace Entity
{
    public class ListadoPertenencia
    {
        public int Id { get; set; }
        public string Expediente { get; set; }
        public int IdEstatusInterno { get; set; }
        public int PertenenciaId { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string Clasificacion { get; set; }
        public string Pertenencia { get; set; }
        public int Cantidad { get; set; }
        public int Bolsa { get; set; }
        public string Estatus { get; set; }
        public int ClasificacionId { get; set; }
        public string NombreUsuario { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Observacion { get; set; }
        public string Fotografia { get; set; }
        public string UsuarioUsuario { get; set; }
    }
}
