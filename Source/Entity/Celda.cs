﻿using System;

namespace Entity
{
    public class Celda
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public int Capacidad { get; set; }
        public int Creadopor{ get; set; }
        public bool Habilitado { get; set; }
        public bool Activo{ get; set; }
        public Catalogo Nivel_Peligrosidad { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
    }
}