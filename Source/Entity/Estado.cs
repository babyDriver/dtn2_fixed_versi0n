﻿using System;

namespace Entity
{
    public class Estado
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Clave { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int IdPais { get; set; }
        public int CreadoPor { get; set; }
    }
}
