﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class PertenenciaLog
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public string Accion { get; set; }
        public int AccionId { get; set; }
        public DateTime Fec_Movto { get; set; }
    }
}

