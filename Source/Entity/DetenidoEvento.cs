﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class DetenidoEvento
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public int SexoId { get; set; }
        public int EventoId { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public DateTime Fechanacimiento { get; set; }
        public int Edad { get; set; }
        public int MotivoId { get; set; }
        public int DetalledetencionId { get; set; }
    }
}
