﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class MotivoDetencionInterno
    {
        public int MotivoDetencionId { get; set; }
        public int InternoId { get; set; }
        public decimal MultaSugerida { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int CreadoPor { get; set; }
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
    }
}
