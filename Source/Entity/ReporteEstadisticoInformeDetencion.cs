﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class ReporteEstadisticoInformeDetencion
    {
        public int Remision { get; set; }
        public DateTime Fecharegistro { get; set; }
        public DateTime Fechadetencion { get; set; }
        public String Nombre { get; set; }
        public DateTime Fechanacimiento { get; set; }
        public int Edad { get; set; }
        public string Sexo { get; set; }
        public string Nacionalidad { get; set; }
        public string Domicilio { get; set; }
        public string Colonia { get; set; }
        public string Ciudad { get; set; }
        public string Estado { get; set; }
        public string Telefono { get; set; }
        public string Seniasparticulares { get; set; }
        public string Alias { get; set; }
        public string Estadocivil { get; set; }
        public double Estatura { get; set; }
        public double peso { get; set; }
        public string Ocupacion { get; set; }
        public string Escolaridad { get; set; }
        public Decimal Sueldo { get; set; }

        public string Noevento { get; set; }
        public string Eventodescripcion { get; set; }
        public string Lugardetencion { get; set; }
        public string Coloniadetencion { get; set; }
        public string Claveunidad { get; set; }
        public string Claveagente { get; set; }
        public string Nombreagente { get; set; }
    }
}
