﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class Usuario_login
    {
        public  int Id { get; set; }
        public int UsuarioId { get; set; }
        public string IPequipo { get; set; }
        public DateTime Fechahora { get; set; }
        public bool Activo { get; set; }
    }
}
