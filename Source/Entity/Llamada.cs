﻿using System;

namespace Entity
{
    public class Llamada
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Involucrado { get; set; }
        public string Descripcion { get; set; }
        public string Lugar { get; set; }
        public int IdColonia { get; set; }        
        public string Folio { get; set; }
        public DateTime HoraYFecha { get; set; }
        public bool Activo { get; set; }
        public bool Habilitado { get; set; }
        public int ContratoId { get; set; }
        public string Tipo { get; set; }
        public int CreadoPor { get; set; }
    }
}
