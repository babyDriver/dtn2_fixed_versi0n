﻿using System;

namespace Entity
{
    public class Informacion
    {
        public int Id { get; set; }
        public Guid TrackingId { get; set; }
        public string Jornadas { get; set; }
        public string Multa { get; set; }
        public string Reparacion { get; set; }
        public DateTime Fecha { get; set; }
        public int Anios { get; set; }
        public int Meses { get; set; }
        public int Dias { get; set; }
        public string Amparo { get; set; }
        public DateTime FechaAmparo { get; set; }
        public int TribunalId { get; set; }
        public string Terminos { get; set; }
        public DateTime FechaInstancia { get; set; }
        public int JuzgadoId { get; set; }
        public string Resolucion { get; set; }
        public string Toca { get; set; }
        public DateTime FechaApelacion { get; set; }
        public DateTime FechaResolucion { get; set; }
        public int TribunalApelacionId { get; set; }
        public string TerminoApelacion { get; set; }
        public string AmparoDirecto { get; set; }
        public DateTime FechaAmparoD { get; set; }
        public DateTime FechaResolucionD { get; set; }
        public int TribunalDirectoId { get; set; }
        public string TerminoDirecto { get; set; }
        public int AutorId { get; set; }
        public int AutoriaId { get; set; }
        public int ClasificacionId { get; set; }
        public int PeligrosidadId { get; set; }
        public int DeterminadaId { get; set; }
        public bool Habilitado { get; set; }
        public int DetenidoId { get; set; }
        public int ProcesoId { get; set; }
    }
}
