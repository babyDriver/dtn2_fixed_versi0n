﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class SubProceso
    {
        public int Id { get; set; }
        public int ProcesoId { get; set; }
        public string Subproceso { get; set; }
        public Int16 Activo { get; set; }
    }
}
