﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application
{
    public class UsuarioAux
    {
        public string Usuario { get; set; }
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Perfil { get; set; }
        public string Empresa { get; set; }
        public string Puesto { get; set; }
        public string Domicilio { get; set; }
        public string Email { get; set; }
        public string Oficina { get; set; }
        public string Movil { get; set; }
        public string Password { get; set; }
        public string Avatar { get; set; }
        public string AvatarOriginal { get; set; }
    }

    public partial class myprofile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static Guid GetUser(string name)
        {
            return (Guid)Membership.GetUser(name).ProviderUserKey;
        }


        [WebMethod]
        public static string GetParametrosTamaño()
        {
            try
            {
                var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                var parametro = ControlParametroContrato.TraerTodos();
                decimal maximo = 0;
                decimal minimo = 0;

                foreach (var item in parametro)
                {
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 13)
                    {
                        minimo = Convert.ToDecimal(item.Valor);
                    }
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 14)
                    {
                        maximo = Convert.ToDecimal(item.Valor);
                    }
                }
                return JsonConvert.SerializeObject(new { exitoso = true, TMax = maximo, Tmin = minimo });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string cargarPerfil()
        {
            try
            {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                var user = ControlUsuario.Obtener(usuario);
                var usuarioms = Membership.GetUser(user.Id);

                object obj = null;

                if (user != null)
                {
                    string rol = System.Web.Security.Roles.GetRolesForUser(user.User)[0];
                    obj = new
                    {
                        Id = user.Id.ToString(),
                        TrackingId = user.UserId.ToString(),
                        Usuario = user.User,
                        Nombre = !string.IsNullOrEmpty(user.Nombre) ? user.Nombre : "",
                        ApPaterno = !string.IsNullOrEmpty(user.ApellidoPaterno) ? user.ApellidoPaterno.Trim() : "",
                        ApMaterno = !string.IsNullOrEmpty(user.ApellidoMaterno) ? user.ApellidoMaterno.Trim() : "",
                        Perfil = !string.IsNullOrEmpty(rol) != null ? rol : "",
                        //Centro = user.CentroId > 0 ? ControlCentroReclusion.ObtenerPorId(user.CentroId).Nombre.Trim() : "",
                        Email = string.IsNullOrEmpty(user.Email) ? string.Empty : user.Email,
                        Avatar = string.IsNullOrEmpty(user.Avatar) ? "~/Content/img/avatars/male.png" : user.Avatar,
                        Oficina = string.IsNullOrEmpty(user.Oficina) ? string.Empty : user.Oficina,
                        Movil = string.IsNullOrEmpty(user.Movil) ? string.Empty : user.Movil,
                        AvatarOriginal = string.IsNullOrEmpty(user.Avatar) ? string.Empty : user.Avatar,
                    };
                }
                return JsonConvert.SerializeObject(obj);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string guardar(string datos)
        {
            try
            {
                MembershipUser usuarioms;
                var user = new Entity.Usuario();
                var unserializedobj = JsonConvert.DeserializeObject<UsuarioAux>(datos);
                   
                user = ControlUsuario.Obtener(Convert.ToInt32(unserializedobj.Id));
                user.Nombre = unserializedobj.Nombre.Trim();
                user.ApellidoPaterno = unserializedobj.ApellidoPaterno.Trim();
                user.ApellidoMaterno = unserializedobj.ApellidoMaterno.Trim();
                user.Movil = unserializedobj.Movil;
                user.Oficina = unserializedobj.Oficina;
                user.Email = unserializedobj.Email.Trim();

                var user_duplicado = ControlUsuario.ObtenerPorUsuario(user.User);

                if (user_duplicado != null && user_duplicado.Id != user.Id)
                    throw new Exception(string.Concat("El nombre de usuario: ", user_duplicado.User, " ya está registrado favor de cambiarlo por otro."));
                if ((!string.IsNullOrEmpty(user.Email)))
                {
                    var email_duplicado = ControlUsuario.ObtenerPorEmail(user.Email);

                    if (email_duplicado != null && email_duplicado.Id != user.Id)
                        throw new Exception(string.Concat("El correo: ", email_duplicado.Email, " ya está registrado favor de cambiarlo por otro."));
                }
                

                if (!string.IsNullOrEmpty(unserializedobj.Avatar))  user.Avatar = unserializedobj.Avatar;
                user.UltimaActualizacion = DateTime.Now;

                usuarioms = Membership.GetUser(unserializedobj.Usuario);
                if (!string.IsNullOrEmpty(unserializedobj.Password) || !string.IsNullOrEmpty(unserializedobj.Email))
                {
                    usuarioms.UnlockUser();
                    usuarioms.Comment = unserializedobj.Nombre.Trim() + " " + unserializedobj.ApellidoPaterno.Trim() +" " + unserializedobj.ApellidoMaterno.Trim();
                    if (!string.IsNullOrEmpty(unserializedobj.Password)) usuarioms.ChangePassword(usuarioms.GetPassword(), unserializedobj.Password);
                    if (!string.IsNullOrEmpty(unserializedobj.Email)) usuarioms.Email = unserializedobj.Email.Trim();
                    Membership.UpdateUser(usuarioms);
                    
                }

                ControlUsuario.Actualizar(user);
                if (!string.IsNullOrEmpty(usuarioms.Email))
                {
                    ControlSmtp.NotificarPorCorreo(user.Nombre, usuarioms.UserName, usuarioms.GetPassword(), unserializedobj.Perfil, usuarioms.Email, string.Empty, string.Empty, Entity.TipoDeMensaje.ModificacionDePerfil, string.Empty, string.Empty);

                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = string.Empty, Id = user.Id.ToString(), TrackingId = user.UserId, Avatar = string.IsNullOrEmpty(user.Avatar) ? "~/Content/img/avatars/male.png" : user.Avatar });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, Id = string.Empty, TrackingId = string.Empty, Avatar = "~/Content/img/avatars/male.png" });
            }


        }



        }
    }
