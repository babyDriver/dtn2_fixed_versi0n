﻿using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;

namespace Web.Application.Epi
{
    public partial class Epi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(23);
            //Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static object PDF(string inicio,string fin)
        {
            var serializedObject = string.Empty;
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 23 });

                if (permiso != null)
                {
                    var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                    var contratoId = 0;
                    var institucionId = 0;
                    var tipo = "";

                    foreach (var k in dataContratos)
                    {
                        if (k.IdUsuario == usId)
                        {
                            contratoId = k.IdContrato;
                            var contrato = ControlContrato.ObtenerPorId(k.IdContrato);
                            if(contrato!=null)
                            {
                                institucionId = contrato.InstitucionId;
                            }
                            
                            tipo = k.Tipo;
                        }
                    }

                    var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    var institucion = ControlInstitucion.ObtenerPorId(institucionId);
                    string[] parametros = new string[3] { contratoUsuarioK.IdContrato.ToString(), inicio, fin };
                    var listado = ControlEpi.ObtenerListadoPorFechas(parametros);

                    //if (listado.Count == 0)
                    //{
                    //    throw new Exception("No hay información para generar el reporte.");

                    //}

                    object[] dataArchivo = null;

                    dataArchivo = ControlPDF.ReporteEpi(listado, parametros);

                    return new { exitoso = true, mensaje = "", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje=ex.Message };
                
            }
        }
        [WebMethod]
        public static string GetFecha()
        {
            var fechainical = DateTime.Now;
            var fechafinal = DateTime.Now;
            var diadelasemana = DateTime.Now.DayOfWeek;
            

            switch (diadelasemana)
            {
                case DayOfWeek.Sunday:
                    fechafinal = fechainical.AddDays(6);
                    fechainical = DateTime.Now;
                    break;
                case DayOfWeek.Monday:
                    
                    fechainical = DateTime.Now.AddDays(-1);
                    fechafinal = DateTime.Now.AddDays(5);
                    break;
                case DayOfWeek.Tuesday:
                    fechainical = DateTime.Now.AddDays(-2);
                    fechafinal = DateTime.Now.AddDays(4);
                    break;
                case DayOfWeek.Wednesday:
                    fechainical = DateTime.Now.AddDays(-3);
                    fechafinal = DateTime.Now.AddDays(3);
                    break;
                case DayOfWeek.Thursday:
                    fechainical = DateTime.Now.AddDays(-4);
                    fechafinal = DateTime.Now.AddDays(2);
                    break;
                case DayOfWeek.Friday:
                    fechainical = DateTime.Now.AddDays(-5);
                    fechafinal = DateTime.Now.AddDays(2);
                    break;
                case DayOfWeek.Saturday:
                    fechainical = DateTime.Now.AddDays(-6);
                    fechafinal = DateTime.Now;
                    break;
            }

            var inicial = fechainical.ToShortDateString() + " 00:00:00";
            var final = fechafinal.ToShortDateString() + " 23:59:59";
            
            return JsonConvert.SerializeObject(new { exitoso = true, FechaI = inicial, FechaF = final });
        } 

        [WebMethod]
        public static object Excel(string inicio, string fin)
        {
            var serializedObject = string.Empty;
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 23 });

                if (permiso != null)
                {
                    var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                    var contratoId = 0;
                    var institucionId = 0;
                    var tipo = "";

                    foreach (var k in dataContratos)
                    {
                        if (k.IdUsuario == usId)
                        {
                            contratoId = k.IdContrato;
                            var contrato = ControlContrato.ObtenerPorId(k.IdContrato);
                            institucionId = contrato.InstitucionId;
                            tipo = k.Tipo;
                        }
                    }

                    var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    var institucion = ControlInstitucion.ObtenerPorId(institucionId);
                    string[] parametros = new string[3] { contratoUsuarioK.IdContrato.ToString(), inicio, fin };
                    var listado = ControlEpi.ObtenerListadoPorFechas(parametros);

                    if (listado.Count == 0)
                    {
                        throw new Exception("No hay información para generar el reporte.");

                    }

                    object[] dataArchivo = null;
                    dataArchivo = ControlEpiExcel.EpiExcel(listado, parametros);
                    return new { exitoso = true, mensaje = "", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };

            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 23 });

                if (permiso != null)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var contrato = contratoUsuarioK.IdContrato;
                        //where.Add(new Where("A.ContratoId", contratoUsuarioK.IdContrato.ToString()));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "cast(D.Expediente as unsigned) Expediente",
                                //"date_format(D.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "date_format(I.HoraYFecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "D.DetalledetencionEdad Edad",
                                "S.Nombre Sexo",
                                //"ifnull(E.Nombre,'') conclucion",
                                "'Estado de ebriedad' conclucion",
                                "D.NombreDetenido Nombre"
                            },
                            from = new Table("servicio_medico", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("detalle_detencion", "D"), "A.DetalledetencionId = D.Id" ),
                                new Join(new Table("informaciondedetencion", "I"), "D.DetenidoId = I.IdInterno" ),
                                new Join(new Table("Sexo", "S"), "D.DetalledetencionSexoId = S.Id" ),
                                new Join(new Table("General", "E"), "D.DetenidoId = E.DetenidoId" )
                            },
                            wheres = where,
                        };

                        DataTablesAux dt = new DataTablesAux(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }
    }
}