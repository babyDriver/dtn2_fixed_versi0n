﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;

namespace Web.Application.Examen_medico
{
    public partial class examenmedico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getPermisos();
            string action = Convert.ToString(Request.QueryString["action"]);
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor != "")
                switch (action)
                {
                    case "add":
                        this.tracking.Value = valor;
                        break;
                    case "update":
                        Entity.ExamenMedico em = ControlExamenMedico.ObtenerPorTrackingId(new Guid(valor));
                        Entity.PruebaExamenMedico pe = ControlPruebaExamenMedico.ObtenerPorExamenMedicoId(em.Id);
                        if (pe != null)
                            this.PruebaTracking.Value = pe.TrackingId.ToString();
                        Entity.DetalleDetencion detencion = ControlDetalleDetencion.ObtenerPorId(em.DetalleDetencionId);
                        this.tracking.Value = detencion.TrackingId.ToString();
                        this.ExamenTracking.Value = valor;

                        break;
                }
            else
                Response.Redirect("examen_medico_list.aspx");


        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Examen médico" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static string pdf2(Guid TrackingId)
        {
            var serializedObject = string.Empty;
            try
            {
                var examenmedico = ControlExamenMedico.ObtenerPorTrackingId(TrackingId);
                var detalledetencion = ControlDetalleDetencion.ObtenerPorId(examenmedico.DetalleDetencionId);
                var interno = ControlDetenido.ObtenerPorId(detalledetencion.DetenidoId);
                var obj = ControlPDFEM.ReporteEM(interno.Id, interno.TrackingId, examenmedico != null ? examenmedico.Id : 0);
                serializedObject = JsonConvert.SerializeObject(obj);
                return serializedObject;
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static Object saveexamen(ExamenAux examen)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var examenNuevo = new Entity.ExamenMedico();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    // string id = examen.Id;
                    // if (id == "") id = "0";
                    int dias = 0;
                    if (examen.DiasSanarLesiones != "")
                        dias = Convert.ToInt32(examen.DiasSanarLesiones);
                    Entity.DetalleDetencion detalle_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.DetalleDetencionTracking));
                    examenNuevo.DetalleDetencionId = detalle_detencion.Id;
                    if (string.IsNullOrEmpty(examen.TrackingId))
                    {
                        mode = @"registró";
                        //var tmpmail = String.Concat(cliente.Nombre, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

                        examenNuevo.Activo = true;
                        examenNuevo.RegistradoPor = usId;
                        examenNuevo.TrackingId = Guid.NewGuid();
                        examenNuevo.Habilitado = true;
                        examenNuevo.Fecha = Convert.ToDateTime(examen.Fecha);
                        examenNuevo.Edad = Convert.ToInt32(examen.Edad);
                        examenNuevo.SexoId = Convert.ToInt32(examen.SexoId);
                        examenNuevo.InspeccionOcular = examen.InspeccionOcular;
                        examenNuevo.IndicacionesMedicas = examen.IndicacionesMedicas;
                        examenNuevo.Observacion = examen.Observacion;
                        examenNuevo.RiesgoVida = Convert.ToBoolean(examen.RiesgoVida);
                        examenNuevo.ConsecuenciasLesiones = Convert.ToBoolean(examen.ConsecuenciasLesiones);
                        examenNuevo.DiasSanarLesiones = dias;
                        examenNuevo.TipoExamen = ControlCatalogo.Obtener(Convert.ToInt32(examen.TipoExamen), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_examen));
                        examenNuevo.Fallecimiento = Convert.ToBoolean(examen.Fallecimiento);
                        examenNuevo.Lesion_visible = Convert.ToBoolean(examen.Lesion_visible);
                        examenNuevo.Id = ControlExamenMedico.Guardar(examenNuevo);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = detalle_detencion.DetenidoId;
                        historial.Movimiento = "Modificación de datos generales en examen médico";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);

                    }
                    else
                    {
                        mode = @"actualizó";

                        Entity.ExamenMedico _examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(examen.TrackingId));

                        string motivoAnterior = _examen != null ? _examen.Observacion : "";
                        string motivoSinRepetir = motivoAnterior != "" ? examen.Observacion.Replace(motivoAnterior, "") : examen.Observacion;

                        examenNuevo.Activo = true;
                        examenNuevo.RegistradoPor = usId;

                        examenNuevo.Habilitado = true;

                        examenNuevo.Edad = Convert.ToInt32(examen.Edad);
                        examenNuevo.SexoId = Convert.ToInt32(examen.SexoId);
                        examenNuevo.InspeccionOcular = examen.InspeccionOcular;
                        examenNuevo.IndicacionesMedicas = examen.IndicacionesMedicas;
                        examenNuevo.Observacion = motivoAnterior + "/" + motivoSinRepetir;
                        examenNuevo.RiesgoVida = Convert.ToBoolean(examen.RiesgoVida);
                        examenNuevo.ConsecuenciasLesiones = Convert.ToBoolean(examen.ConsecuenciasLesiones);
                        examenNuevo.DiasSanarLesiones = dias;
                        examenNuevo.TipoExamen = ControlCatalogo.Obtener(Convert.ToInt32(examen.TipoExamen), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_examen));
                        examenNuevo.Fecha = Convert.ToDateTime(examen.Fecha);
                        examenNuevo.Lesion_visible = Convert.ToBoolean(examen.Lesion_visible);
                        examenNuevo.TrackingId = _examen.TrackingId;
                        examenNuevo.Id = _examen.Id;
                        examenNuevo.Fallecimiento = Convert.ToBoolean(examen.Fallecimiento);
                        ControlExamenMedico.Actualizar(examenNuevo);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = detalle_detencion.DetenidoId;
                        historial.Movimiento = "Modificación de datos generales en examen medico";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    int[] filtro = { detalle_detencion.ContratoId, detalle_detencion.DetenidoId };
                    var hit = ControlCrossreference.GetCrossReferences(filtro);
                    string msg = "";
                    var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                    bool fhit = false;
                    if (hit.Count > 0)
                    {
                        fhit = true;
                        msg = "La información del examen medico se " + mode + " correctamente.   El detenido " + detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno + " tiene un hit";
                    }

                    return new { ishit = fhit, msghit = msg, exitoso = true, mensaje = mode, Id = examenNuevo.Id.ToString(), TrackingId = examenNuevo.TrackingId, DetenidoId = detalle_detencion.DetenidoId.ToString(), Observacion = examenNuevo.Observacion };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static string pdf(int datos)
        {
            var serializedObject = string.Empty;
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Reportes" }).Consultar)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    var interno = ControlDetenido.ObtenerPorId(datos);

                    string[] Id = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(Id);

                    var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);

                    if (examenmedico.Count > 0)
                    {
                        var obj = ControlPDFEM.ReporteEM(interno.Id, interno.TrackingId, examenmedico.LastOrDefault().Id);
                        serializedObject = JsonConvert.SerializeObject(obj);
                    }
                    else
                    {
                        serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El detenido no cuenta con examen médico." });
                    }

                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static Object saveprueba(PruebaAux prueba)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var pruebaNueva = new Entity.PruebaExamenMedico();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    if (prueba.ExamenMedicoTracking == null || prueba.ExamenMedicoTracking == "")
                    {
                        return new { exitoso = false, mensaje = "No existe un exámen médico para esta prueba", Id = "", TrackingId = "" };
                    }

                    Entity.ExamenMedico examenMedico = ControlExamenMedico.ObtenerPorTrackingId(new Guid(prueba.ExamenMedicoTracking));

                    pruebaNueva.ExamenMedicoId = examenMedico.Id;
                    pruebaNueva.RegistradoPor = usId;

                    if (string.IsNullOrEmpty(prueba.TrackingId))
                    {
                        mode = @"registró";
                        pruebaNueva.TrackingId = Guid.NewGuid();

                        pruebaNueva.MucosasId = Convert.ToInt32(prueba.MucosasId);
                        pruebaNueva.AlientoId = Convert.ToInt32(prueba.AlientoId);
                        pruebaNueva.Examen_neurologicoId = Convert.ToInt32(prueba.Examen_neurologicoId);
                        pruebaNueva.Disartia = Convert.ToBoolean(prueba.Disartia);
                        pruebaNueva.ConjuntivasId = Convert.ToInt32(prueba.ConjuntivasId);
                        pruebaNueva.MarchaId = Convert.ToInt32(prueba.MarchaId);
                        pruebaNueva.PupilasId = Convert.ToInt32(prueba.PupilasId);
                        pruebaNueva.CoordinacionId = Convert.ToInt32(prueba.CoordinacionId);
                        pruebaNueva.Reflejos_pupilaresId = Convert.ToInt32(prueba.Reflejos_pupilaresId);
                        pruebaNueva.TendinososId = Convert.ToInt32(prueba.TendinososId);
                        pruebaNueva.RomberqId = Convert.ToInt32(prueba.RomberqId);
                        pruebaNueva.ConductaId = Convert.ToInt32(prueba.ConductaId);
                        pruebaNueva.LenguajeId = Convert.ToInt32(prueba.LenguajeId);
                        pruebaNueva.AtencionId = Convert.ToInt32(prueba.AtencionId);
                        pruebaNueva.OrientacionId = Convert.ToInt32(prueba.OrientacionId);
                        pruebaNueva.DiadococinenciaId = Convert.ToInt32(prueba.DiadococinenciaId);
                        pruebaNueva.DedoId = Convert.ToInt32(prueba.DedoId);
                        pruebaNueva.TalonId = Convert.ToInt32(prueba.TalonId);
                        pruebaNueva.Alcoholimetro = prueba.Alcoholimetro;
                        pruebaNueva.TA = prueba.TA;
                        pruebaNueva.FC = prueba.FC;
                        pruebaNueva.FR = prueba.FR;
                        pruebaNueva.Pulso = prueba.Pulso;

                        pruebaNueva.Id = ControlPruebaExamenMedico.Guardar(pruebaNueva);
                        var detalle = ControlDetalleDetencion.ObtenerPorId(examenMedico.DetalleDetencionId);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = detalle.DetenidoId;
                        historial.Movimiento = "Modificación de datos generales en pruebas del examen medico";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {
                        mode = @"actualizó";

                        Entity.PruebaExamenMedico _prueba = ControlPruebaExamenMedico.ObtenerPorTrackingId(new Guid(prueba.TrackingId));

                        pruebaNueva.MucosasId = Convert.ToInt32(prueba.MucosasId);
                        pruebaNueva.AlientoId = Convert.ToInt32(prueba.AlientoId);
                        pruebaNueva.Examen_neurologicoId = Convert.ToInt32(prueba.Examen_neurologicoId);
                        pruebaNueva.Disartia = Convert.ToBoolean(prueba.Disartia);
                        pruebaNueva.ConjuntivasId = Convert.ToInt32(prueba.ConjuntivasId);
                        pruebaNueva.MarchaId = Convert.ToInt32(prueba.MarchaId);
                        pruebaNueva.PupilasId = Convert.ToInt32(prueba.PupilasId);
                        pruebaNueva.CoordinacionId = Convert.ToInt32(prueba.CoordinacionId);
                        pruebaNueva.Reflejos_pupilaresId = Convert.ToInt32(prueba.Reflejos_pupilaresId);
                        pruebaNueva.TendinososId = Convert.ToInt32(prueba.TendinososId);
                        pruebaNueva.RomberqId = Convert.ToInt32(prueba.RomberqId);
                        pruebaNueva.ConductaId = Convert.ToInt32(prueba.ConductaId);
                        pruebaNueva.LenguajeId = Convert.ToInt32(prueba.LenguajeId);
                        pruebaNueva.AtencionId = Convert.ToInt32(prueba.AtencionId);
                        pruebaNueva.OrientacionId = Convert.ToInt32(prueba.OrientacionId);
                        pruebaNueva.DiadococinenciaId = Convert.ToInt32(prueba.DiadococinenciaId);
                        pruebaNueva.DedoId = Convert.ToInt32(prueba.DedoId);
                        pruebaNueva.TalonId = Convert.ToInt32(prueba.TalonId);
                        pruebaNueva.Alcoholimetro = prueba.Alcoholimetro;
                        pruebaNueva.TA = prueba.TA;
                        pruebaNueva.FC = prueba.FC;
                        pruebaNueva.FR = prueba.FR;
                        pruebaNueva.Pulso = prueba.Pulso;

                        pruebaNueva.TrackingId = _prueba.TrackingId;
                        pruebaNueva.Id = _prueba.Id;
                        ControlPruebaExamenMedico.Actualizar(pruebaNueva);

                        var detalle = ControlDetalleDetencion.ObtenerPorId(examenMedico.DetalleDetencionId);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = detalle.DetenidoId;
                        historial.Movimiento = "Modificación de datos generales en pruebas del examen medico";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    return new { exitoso = true, mensaje = mode, Id = pruebaNueva.Id.ToString(), TrackingId = pruebaNueva.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
        
        [WebMethod]
        public static string getexamen(string trackingid)
        {
            try
            {
                Entity.ExamenMedico _examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));



                return JsonConvert.SerializeObject(new { exitoso = true, obj = _examen, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getpruebaexamen(string trackingid)
        {
            try
            {
                Entity.PruebaExamenMedico _pruebaexamen = ControlPruebaExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));



                return JsonConvert.SerializeObject(new { exitoso = true, obj = _pruebaexamen, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                edad = edad - 1;

            return edad;
        }
        [WebMethod]
        public static string Validateexamenexistente(string trackingid)
        {
            try
            {
                var existe = "";
                Entity.ExamenMedico examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));
                Entity.DetalleDetencion detalleDetencionAux = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                Entity.DetalleDetencion detalleDetencion = new Entity.DetalleDetencion();
                if(examen==null)
                {
                    examen = ControlExamenMedico.ObtenerPorDetalleDetencionId(detalleDetencionAux.Id);
                }

                if (examen != null)
                {
                    detalleDetencion = ControlDetalleDetencion.ObtenerPorId(examen.DetalleDetencionId);
                    existe = "Exitoso";
                }
                else if (detalleDetencionAux != null)
                {
                    detalleDetencion = detalleDetencionAux;
                }
                if(examen==null)
                {
                    existe = "Fallido";
                }

                return JsonConvert.SerializeObject(new { exitoso = true, prueba=existe });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen Medico" }).Consultar)
                {
                    Entity.ExamenMedico examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));
                    Entity.DetalleDetencion detalleDetencionAux = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                    Entity.DetalleDetencion detalleDetencion = new Entity.DetalleDetencion();

                    if (examen != null)
                    {
                        detalleDetencion = ControlDetalleDetencion.ObtenerPorId(examen.DetalleDetencionId);
                    }
                    else if (detalleDetencionAux != null)
                    {
                        detalleDetencion = detalleDetencionAux;
                    }

                    Entity.Detenido interno = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
                    if (interno != null)
                    {
                        if (string.IsNullOrEmpty(interno.RutaImagen))
                        {
                            interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                        }
                    }
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalle = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);

                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalle.CentroId);

                    Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);

                    Entity.Domicilio nacimiento = ControlDomicilio.ObtenerPorId(interno.DomicilioNId);

                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                            var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                            Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                            foreach (var item in detenidosEvento)
                            {
                                if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                    detenidoEvento = item;
                            }

                            if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                            else
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }

                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }

                    var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                    Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;
                    if (edad == 0) edad = detalleDetencion.DetalledetencionEdad;
                    var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                    Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                    object obj = null;
                    object objnacimiento = null;
                    bool tienenacimiento = true;
                    object objdomicilio = null;
                    bool tienedomicilio = true;
                    object objgeneral = null;
                    bool tienegeneral = true;
                    int coloniaId = 0;
                    if (domicilio != null)
                    {
                        if (domicilio.ColoniaId != null)
                            coloniaId = Convert.ToInt32(domicilio.ColoniaId);
                    }

                    string numero = "";
                    string calle = "";

                    if (domicilio != null)
                    {
                        if (domicilio.Numero != null)
                            numero = domicilio.Numero;

                        if (domicilio.Calle != null)
                            calle = domicilio.Calle;
                    }

                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = (detalle) != null ? detalle.Expediente : "",
                        Centro = (institucion != null) ? institucion.Nombre : "No registrado",
                        Fecha = detalle.Fecha != null ? Convert.ToDateTime(detalle.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                        RutaImagen = interno.RutaImagen,
                        Edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        SexoId = sexo != null ? sexo.Id : 0,
                        CalleDomicilio = domicilio != null ? calle + " #" + numero : "Domicilio no registrado",
                        CodigoPostalDomicilio = coloniaId != 0 ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                        Lesion_Visible = detalleDetencion.Lesion_visible.ToString()
                    };

                    if (nacimiento != null)
                    {
                        objnacimiento = new
                        {
                            Id = nacimiento.Id,
                            TrackingId = nacimiento.TrackingId,
                            CalleNacimiento = (nacimiento.Calle) != null ? nacimiento.Calle : "",
                            NumeroNacimiento = nacimiento.Numero != null ? nacimiento.Numero : "",
                            //ColoniaNacimiento = nacimiento.Colonia,
                            //CPNacimiento = nacimiento.CP,
                            TelefonoNacimiento = nacimiento.Telefono != null ? nacimiento.Telefono : "",
                            PaisNacimiento = nacimiento.PaisId != null ? nacimiento.PaisId : 0,
                            EstadoNacimiento = (nacimiento.EstadoId) != null ? nacimiento.EstadoId : 0,
                            MunicipioNacimiento = (nacimiento.MunicipioId) != null ? nacimiento.MunicipioId : 0,
                            LocalidadNacimiento = (nacimiento.Localidad) != null ? nacimiento.Localidad : "",
                            ColoniaIdNacimiento = (nacimiento.ColoniaId) != null ? nacimiento.ColoniaId : 0,
                            CodigoPostalNacimiento = (nacimiento.ColoniaId) != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(nacimiento.ColoniaId)).CodigoPostal : ""
                        };
                    }
                    else
                    {
                        tienenacimiento = false;
                    }

                    if (domicilio != null)
                    {
                        string calle2 = "";
                        string numero2 = "";
                        if (domicilio.Calle != null)
                            calle2 = domicilio.Calle;

                        if (domicilio.Numero != null)
                            numero2 = domicilio.Numero;

                        objdomicilio = new
                        {
                            Id = domicilio.Id,
                            TrackingId = domicilio.TrackingId,

                            CalleDomicilio = domicilio != null ? calle2 + " #" + numero2 : "Domicilio no registrado",
                            NumeroDomicilio = numero2,
                            //ColoniaDomicilio = domicilio.Colonia,
                            //CPDomicilio = domicilio.CP,
                            TelefonoDomicilio = domicilio.Telefono,
                            PaisDomicilio = domicilio.PaisId,
                            EstadoDomicilio = domicilio.EstadoId,
                            MunicipioDomicilio = domicilio.MunicipioId,
                            LocalidadDomicilio = domicilio.Localidad != null ? domicilio.Localidad : "Localidad no registrado",
                            ColoniaIdDomicilio = (domicilio.ColoniaId) != null ? domicilio.ColoniaId : 0,

                        };
                    }
                    else
                    {
                        tienedomicilio = false;
                    }

                    if (general != null)
                    {
                        objgeneral = new
                        {
                            Id = general.Id,
                            TrackingId = general.TrackingId,
                            FechaNacimineto = general.FechaNacimineto != null ? general.FechaNacimineto.ToString("dd/MM/yyyy") : "",
                            Nacionalidad = general.NacionalidadId,
                            RFC = general.RFC,
                            Escolaridad = general.EscolaridadId,
                            Religion = general.ReligionId,
                            Ocupacion = general.OcupacionId,
                            Estado = general.EstadoCivilId,
                            Etnia = general.EtniaId,
                            Sexo = general.SexoId,
                            Mental = general.EstadoMental,
                            Inimputable = general.Inimputable,
                            Edad = edad
                        };
                    }
                    else
                    {
                        tienegeneral = false;
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, nacimiento = objnacimiento, tienenacimiento = tienenacimiento, domicilio = objdomicilio, tienedomicilio = tienedomicilio, general = objgeneral, tienegeneral = tienegeneral });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getSexoEdad(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen Medico" }).Consultar)
                {
                    Entity.ExamenMedico examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));
                    Entity.DetalleDetencion detalleDetencionAux = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                    Entity.DetalleDetencion detalle = new Entity.DetalleDetencion();

                    if (examen != null)
                    {
                        detalle = ControlDetalleDetencion.ObtenerPorId(examen.DetalleDetencionId);
                    }
                    else if (detalleDetencionAux != null)
                    {
                        detalle = detalleDetencionAux;
                    }

                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };

                    Entity.Domicilio nacimiento = ControlDomicilio.ObtenerPorId(interno.DomicilioNId);
                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                            var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                            Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                            foreach (var item in detenidosEvento)
                            {
                                if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                    detenidoEvento = item;
                            }

                            if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                            else
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }

                    }

                    object obj = null;
                    if (edad == 0)
                    {
                        edad = general.Edaddetenido;
                    }
                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        Edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        Sexo = general != null ? general.SexoId : 0,
                        FechaNacimiento = general != null && general.FechaNacimineto != DateTime.MinValue ? general.FechaNacimineto.ToString("dd/MM/yyyy") : ""
                    };

                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, mensaje = "" });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getSexo()
        {
            List<Combo> combo = new List<Combo>();


            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }
   
        [WebMethod]
        public static string getDatosExamen(string trackingExamenId)
        {
            try
            {


                Entity.ExamenMedico examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingExamenId));
                Entity.PruebaExamenMedico prueba = ControlPruebaExamenMedico.ObtenerPorExamenMedicoId(examen.Id);

                object objexamen = null;
                object objpruebaK = null;

                objexamen = new
                {
                    InspeccionOcular = examen.InspeccionOcular.ToString(),
                    IndicacionesMedicas = examen.IndicacionesMedicas.ToString(),
                    Observacion = examen.Observacion,
                    RiesgoVida = examen.RiesgoVida.ToString(),
                    ConsecuenciasLesiones = examen.ConsecuenciasLesiones.ToString(),
                    DiasSanarLesiones = examen.DiasSanarLesiones.ToString(),
                    Tipo = examen.TipoExamen.Id.ToString(),
                    Fecha = examen.Fecha.ToString("dd / MM / yyyy HH: mm:ss"),
                    Lesion_Visible = examen.Lesion_visible.ToString(),
                    Fallecimiento = examen.Fallecimiento.ToString(),
                };
                if (prueba != null)
                {
                    objpruebaK = new
                    {
                        MucosasId = prueba.MucosasId.ToString(),
                        AlientoId = prueba.AlientoId.ToString(),
                        Examen_neurologicoId = prueba.Examen_neurologicoId.ToString(),
                        Disartia = prueba.Disartia.ToString(),
                        ConjuntivasId = prueba.ConjuntivasId.ToString(),
                        MarchaId = prueba.MarchaId.ToString(),
                        PupilasId = prueba.PupilasId.ToString(),
                        CoordinacionId = prueba.CoordinacionId.ToString(),
                        Reflejos_pupilaresId = prueba.Reflejos_pupilaresId.ToString(),
                        TendinososId = prueba.TendinososId.ToString(),
                        RomberqId = prueba.RomberqId.ToString(),
                        ConductaId = prueba.ConductaId.ToString(),
                        LenguajeId = prueba.LenguajeId.ToString(),
                        AtencionId = prueba.AtencionId.ToString(),
                        OrientacionId = prueba.OrientacionId.ToString(),
                        DiadococinenciaId = prueba.DiadococinenciaId.ToString(),
                        DedoId = prueba.DedoId.ToString(),
                        TalonId = prueba.TalonId.ToString(),
                        Alcoholimetro = prueba.Alcoholimetro.ToString(),
                        TA = prueba.TA.ToString(),
                        FC = prueba.FC.ToString(),
                        FR = prueba.FR.ToString(),
                        Pulso = prueba.Pulso.ToString(),
                    };
                }


                return JsonConvert.SerializeObject(new { exitoso = true, objexamen = objexamen, objprueba = objpruebaK });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getListado(string select)
        {
            int tipo = 0;
            switch (select)
            {
                case "mucosas":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.mucosas);
                    break;
                case "aliento":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.aliento);
                    break;
                case "neurologico":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.examen_neurologico);
                    break;

                case "conjuntivas":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.conjuntivas);
                    break;
                case "marcha":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.marcha);
                    break;
                case "pupilas":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.pupilas);
                    break;
                case "coordinacion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.coordinacion);
                    break;
                case "pupilares":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.reflejos_pupilares);
                    break;
                case "osteo":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.reflejos_osteo_tendinosos);
                    break;
                case "romberq":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.romberq);
                    break;
                case "conducta":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.conducta);
                    break;
                case "lenguaje":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.lenguaje);
                    break;
                case "atencion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.atencion);
                    break;
                case "orientacion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.orientacion);
                    break;
                case "diadococinencia":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.diadococinencia);
                    break;
                case "dedo":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.dedo_nariz);
                    break;
                case "talon":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.talon_rodilla);
                    break;
                case "tipo_examen":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_examen);
                    break;
                case "tipo_tatuaje":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_tatuaje);
                    break;
                case "tipo_lesion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_lesion);
                    break;
                case "tipo_intoxicacion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_intoxicacion);
                    break;
                case "lugar":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.lugar);
                    break;
            }
            var combo = new List<Combo>();
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerHabilitados(tipo);

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static DataTable getDataTatuaje(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("T.Activo", "1"));
                        where.Add(new Where("EM.TrackingId", tracking));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                        "T.Id",
                        "T.TrackingId",
                        "LU.Nombre Lugar",
                        "T.LugarId",
                        "T.Observacion",
                        "TT.Nombre Tipo",
                        "T.TipoTatuajeId",
                        "T.Habilitado"

                    },
                            from = new Table("tatuaje", "T"),
                            joins = new List<Join>
                    {
                        new Join(new Table("tipo_tatuaje", "TT"), "TT.Id  = T.TipoTatuajeId"),
                         new Join(new Table("lugar", "LU"), "LU.Id  = T.LugarId"),
                        new Join(new Table("examen_medico", "EM"), "EM.Id  = T.ExamenMedicoId"),
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }
        [WebMethod]
        public static DataTable GetLesionLog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        where.Add(new Where("B.RolId", "<>", "13"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "LU.Nombre Lugar",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("lesionlog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                      new Join(new Table("lugar", "LU"), "LU.Id  = A.LugarId"),
                                new Join(new Table("lesion","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }
        [WebMethod]
        public static DataTable GetIntoxicacionLog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        where.Add(new Where("B.RolId", "<>", "13"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Observacion",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("intoxicacionlog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("intoxicacion","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }
        [WebMethod]
        public static DataTable GetTatuajeLog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        where.Add(new Where("B.RolId", "<>", "13"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "LU.Nombre Lugar",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("tatuajelog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                 new Join(new Table("lugar", "LU"), "LU.Id  = A.LugarId"),
                                new Join(new Table("tatuaje","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);

                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
        [WebMethod]
        public static DataTable getDataLesion(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("L.Activo", "1"));
                        where.Add(new Where("EM.TrackingId", tracking));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                        "L.Id",
                        "L.TrackingId",
                        "LU.Nombre Lugar",
                        "L.LugarId",
                        "L.Observacion",
                        "TL.Nombre Tipo",
                        "L.TipoLesionId",
                        "L.Habilitado"

                    },
                            from = new Table("lesion", "L"),
                            joins = new List<Join>
                    {
                        new Join(new Table("tipo_lesion", "TL"), "TL.Id  = L.TipoLesionId"),
                        new Join(new Table("lugar", "LU"), "LU.Id  = L.LugarId"),
                        new Join(new Table("examen_medico", "EM"), "EM.Id  = L.ExamenMedicoId"),
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static DataTable getDataIntoxicacion(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("I.Activo", "1"));
                        where.Add(new Where("EM.TrackingId", tracking));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                        "I.Id",
                        "I.TrackingId",
                        "I.TipoIntoxicacionId",
                        "I.Observacion",
                        "TI.Nombre Tipo",
                        "I.Habilitado"

                    },
                            from = new Table("intoxicacion", "I"),
                            joins = new List<Join>
                    {
                        new Join(new Table("tipo_intoxicacion", "TI"), "TI.Id  = I.TipoIntoxicacionId"),

                        new Join(new Table("examen_medico", "EM"), "EM.Id  = I.ExamenMedicoId"),
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static Object saveAdicional(AdicionalAux item)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var itemNuevo = new Entity.AdicionalExamenMedico();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    Entity.ExamenMedico examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(item.ExamenTrackingId));
                    itemNuevo.ExamenMedicoId = examen.Id;
                    itemNuevo.Activo = true;
                    itemNuevo.Creadopor = usId;
                    itemNuevo.Habilitado = true;
                    itemNuevo.Clasificacion = item.Clasificacion;
                    itemNuevo.Lugar =Convert.ToInt32(item.Lugar);
                    itemNuevo.Observacion = item.Observacion;
                    if (item.Id != "")
                        itemNuevo.Id = Convert.ToInt32(item.Id);
                    switch (item.Clasificacion)
                    {
                        case "tatuaje":
                            itemNuevo.Tipo = ControlCatalogo.Obtener(Convert.ToInt32(item.Tipo), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_tatuaje));
                            break;
                        case "intoxicacion":
                            itemNuevo.Tipo = ControlCatalogo.Obtener(Convert.ToInt32(item.Tipo), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_intoxicacion));
                            break;
                        case "lesion":
                            itemNuevo.Tipo = ControlCatalogo.Obtener(Convert.ToInt32(item.Tipo), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_lesion));
                            break;
                    }

                    if (string.IsNullOrEmpty(item.TrackingId))
                    {
                        mode = @"registró";
                        itemNuevo.TrackingId = Guid.NewGuid();
                        itemNuevo.Id = ControlAdicionalExamenMedico.Guardar(itemNuevo);
                    }
                    else
                    {
                        mode = @"actualizó";
                        Entity.AdicionalExamenMedico auxitem = new Entity.AdicionalExamenMedico();
                        auxitem.TrackingId = new Guid(item.TrackingId);
                        auxitem.Clasificacion = itemNuevo.Clasificacion;
                        itemNuevo.TrackingId = auxitem.TrackingId;
                        Entity.AdicionalExamenMedico _item = ControlAdicionalExamenMedico.ObtenerPorTrackingId(auxitem);
                        ControlAdicionalExamenMedico.Actualizar(itemNuevo);
                    }

                    return new { exitoso = true, mensaje = mode, Id = itemNuevo.Id.ToString(), TrackingId = itemNuevo.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static string blockuser(AdicionalAux item)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Eliminar)
                {
                    Entity.AdicionalExamenMedico auxitem = new Entity.AdicionalExamenMedico();
                    auxitem.TrackingId = new Guid(item.TrackingId);
                    auxitem.Clasificacion = item.Clasificacion;
                    var examenAux = ControlAdicionalExamenMedico.ObtenerPorTrackingId(auxitem);
                    examenAux.Habilitado = (examenAux.Habilitado) ? false : true;
                    string mensaje = examenAux.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                    ControlAdicionalExamenMedico.Actualizar(examenAux);

                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, mensaje });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveSexoEdad(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    Entity.General general = new Entity.General();
                    Entity.DetalleDetencion detalle_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[2].ToString()));
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;

                    if (detalle_detencion != null)
                    {
                        general = ControlGeneral.ObtenerPorDetenidoId(detalle_detencion.DetenidoId);
                    }


                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {

                        }

                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }




                    if (general != null)
                    {
                        mode = @"actualizó";

                        //var fechaNacimiento = Convert.ToDateTime(datos[0]);
                        //edad = CalcularEdad(fechaNacimiento);
                        edad = Convert.ToInt32(datos[0]);
                        if (edad < 1)
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha de nacimiento debe de ser mayor a un año.", Id = "", TrackingId = "" });


                        //general.FechaNacimineto = fechaNacimiento;
                        //general.Edaddetenido = edad;
                        general.SexoId = Convert.ToInt32(datos[1].ToString());
                        ControlGeneral.Actualizar(general);
                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);

                        //
                        var interno = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                        var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                        var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                        var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                        detalle_detencion.DetalledetencionEdad = edad;
                        ControlDetalleDetencion.Actualizar(detalle_detencion);
                        general.Edaddetenido = edad;
                        ControlGeneral.Actualizar(general);
                        Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                        foreach (var item in detenidosEvento)
                        {
                            if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                detenidoEvento = item;
                        }

                        if (detenidoEvento != null)
                        {
                            detenidoEvento.Edad = edad;
                            ControlDetenidoEvento.Actualizar(detenidoEvento);
                        }
                        //
                    }
                    else
                    {
                        mode = "registró";

                        general = new Entity.General();
                        general.DetenidoId = detalle_detencion.DetenidoId;
                        general.TrackingId = Guid.NewGuid();
                        general.FechaNacimineto = Convert.ToDateTime(datos[0]);
                        general.RFC = "";
                        general.NacionalidadId = 39;
                        general.EscolaridadId = 19;
                        general.ReligionId = 10;
                        general.OcupacionId = 18;
                        general.EstadoCivilId = 8;
                        general.EtniaId = 59;
                        general.SexoId = Convert.ToInt32(datos[1].ToString());
                        general.EstadoMental = false;
                        general.Inimputable = false;
                        general.Id = ControlGeneral.Guardar(general);

                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                        edad = CalcularEdad(general.FechaNacimineto);
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, sexo = sexo.Nombre, edad = edad });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" });
            }
        }

        public class ExamenAux
        {
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string Edad { get; set; }
            public string SexoId { get; set; }
            public string InspeccionOcular { get; set; }
            public string IndicacionesMedicas { get; set; }
            public string Observacion { get; set; }
            public string Activo { get; set; }
            public string Habilitado { get; set; }
            public string RiesgoVida { get; set; }
            public string ConsecuenciasLesiones { get; set; }
            public string DiasSanarLesiones { get; set; }
            public string TipoExamen { get; set; }
            public string Fecha { get; set; }
            public string DetalleDetencionTracking { get; set; }
            public Boolean Lesion_visible { get; set; }
            public string Fallecimiento { get; set; }
        }

        public class PruebaAux
        {
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string ExamenMedicoTracking { get; set; }
            public string MucosasId { get; set; }
            public string AlientoId { get; set; }
            public string Examen_neurologicoId { get; set; }
            public string Disartia { get; set; }
            public string ConjuntivasId { get; set; }
            public string MarchaId { get; set; }
            public string PupilasId { get; set; }
            public string CoordinacionId { get; set; }
            public string Reflejos_pupilaresId { get; set; }
            public string TendinososId { get; set; }
            public string RomberqId { get; set; }
            public string ConductaId { get; set; }
            public string LenguajeId { get; set; }
            public string AtencionId { get; set; }
            public string OrientacionId { get; set; }
            public string DiadococinenciaId { get; set; }
            public string DedoId { get; set; }
            public string TalonId { get; set; }
            public string Alcoholimetro { get; set; }
            public string TA { get; set; }
            public string FC { get; set; }
            public string FR { get; set; }
            public string Pulso { get; set; }
        }
        public class AdicionalAux
        {
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string ExamenMedicoId { get; set; }
            public string Lugar { get; set; }
            public string Observacion { get; set; }
            public string Activo { get; set; }
            public string Habilitado { get; set; }
            public string Tipo { get; set; }
            public string Creadopor { get; set; }
            public string Clasificacion { get; set; }
            public string ExamenTrackingId { get; set; }
        }
    }
}