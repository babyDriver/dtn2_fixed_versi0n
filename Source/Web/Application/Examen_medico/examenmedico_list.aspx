﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="examenmedico_list.aspx.cs" Inherits="Web.Application.Examen_medico.examenmedico_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
   
    <li>Examen médico</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        .dropdown-menu {
            position: relative;
        }
        td.strikeout {
            text-decoration: line-through;
        }
    </style>
    <div class="scroll">
        <div class="row" id="addentry">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 class="page-title txt-color-blueDark" id="titlehead">
                    <img style="width:40px; height:40px; margin-bottom:5px" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/MedicalTest.png" />  <asp:Label ID="Label2" runat="server"></asp:Label>
                    Examen médico
                </h1>
            </div>
        </div>
        <div class="row" id="addexamen" style="display: block;">
            <section style="margin-top:10px;" class="col col-4">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                    <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                    <a  class="btn btn-primary dropdown-toggle " href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/reporte_examenmedico.aspx?Accion='Reporte'" id="rpt">Reportes médicos </a>
                    <%--<div class="btn-group">
                <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown"  >Reportes médicos
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li >
                        <a class="btn-sm " title="Examen médico" href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/reporte_examen_medico.aspx">
                            Examen médico
                        </a>
                    </li>
                </ul>
            </div>--%>
                </div>
            </section>
        </div>
        <p></p>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-examenlist-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><img style="width:20px; height:20px; margin-bottom:5px; filter: invert(100%);" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/MedicalTest.png" />  <asp:Label ID="Label1" runat="server"></asp:Label></span>
                            <h2>Examen médico</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th data-class="expand">#</th>
                                            <th>Fotografía</th>
                                            <th >Nombre</th>
                                            <th>Apellido paterno</th>
                                            <th data-hide="phone,tablet">Apellido materno</th>
                                            <th data-hide="phone,tablet">No. remisión</th>
                                            <th data-hide="phone,tablet">Situación del detenido</th>
                                            <th>Fecha y hora de registro</th>
                                            <th>Usuario que registra</th>
                                            <th data-hide="phone,tablet">Acciones</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4>Añadir examen médico</h4>
                </div>
                <div class="modal-body">
                    <h5>Listado de  detenidos en existencia</h5>
                    <div id="addTable" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basicadd" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">Nombre</th>
                                        <th>No. Remisión</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="add-antecedentes" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4>Antecedentes</h4>
                </div>
                <div class="modal-body">
                    <h5>Antecedentes</h5>
                    <div id="addTable2" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basicadAnt" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                     <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                        <th data-hide="phone,tablet">Fecha</th>
                                        <th data-hide="phone,tablet">Centro</th>
                                        <th data-hide="phone,tablet">No. de remisión</th>
                                        <th data-hide="phone,tablet">NCP</th>
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>                                       
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar señal
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div4" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro de  seña <strong><span id="senaleliminar"></span></strong> será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />

    <div id="photo-arrested" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="<%= ConfigurationManager.AppSettings["relativepath"]  %> #" alt="fotografía del detenido" /> 
                                </div> 
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
      <script type="text/javascript">
          $(document).ready(function () {

              pageSetUp();

              var responsiveHelper_dt_basic = undefined;
              var responsiveHelper_dt_basicadAnt = undefined;
              var responsiveHelper_datatable_fixed_column = undefined;
              var responsiveHelper_datatable_col_reorder = undefined;
              var responsiveHelper_datatable_tabletools = undefined;

              var breakpointDefinition = {
                  desktop: Infinity,
                  tablet: 1024,
                  fablet: 768,
                  phone: 480
              };

              if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                  $("#add").show();
              }
              else {
                  $("#add").hide();
              }

              var rutaDefaultServer = "";

              getRutaDefaultServer();

              function getRutaDefaultServer() {
                  $.ajax({
                      type: "POST",
                      url: "examenmedico_list.aspx/getRutaServer",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              rutaDefaultServer = resultado.rutaDefault;
                          }
                      }
                  });
              }

              window.table = $('#dt_basic').dataTable({
                  "lengthMenu": [10, 20, 50, 100],
                  iDisplayLength: 10,
                  serverSide: true,
                  fixedColumns: true,
                  autoWidth: true,
                  "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                      "t" +
                      "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                  "autoWidth": true,
                  "oLanguage": {
                      "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                  },
                  "preDrawCallback": function () {
                      if (!responsiveHelper_dt_basic) {
                          responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                      }
                  },
                  "rowCallback": function (nRow) {
                      responsiveHelper_dt_basic.createExpandIcon(nRow);
                  },
                  "drawCallback": function (oSettings) {
                      responsiveHelper_dt_basic.respond();
                      $('#dt_basic').waitMe('hide');
                  },
                  ajax: {
                      url: "examenmedico_list.aspx/getdata",
                      method: "POST",
                      contentType: "application/json; charset=utf-8",
                      data: function (d) {
                          startLoading();                          
                          d.emptytable = false;                          
                          d.pages = $('#dt_basic').DataTable().page.info().page || "";
                          return JSON.stringify(d);
                      },
                      dataSrc: "data",
                      dataFilter: function (data) {
                          var json = jQuery.parseJSON(data);
                          json.recordsTotal = json.d.recordsTotal;
                          json.recordsFiltered = json.d.recordsFiltered;
                          json.data = json.d.data;
                          endLoading();
                          return JSON.stringify(json);
                      }
                  },                 
                  columns: [
                      {
                          data: "TrackingId",
                          targets: 0,
                          orderable: false,
                          visible: false,
                          render: function (data, type, row, meta) {
                              return "";
                          }
                      },
                      null,
                      null,
                      {
                          name: "Nombre",
                          data: "Nombre"
                      },
                      {
                          name: "Paterno",
                          data: "Paterno"
                      },
                      {
                          name: "Materno",
                          data: "Materno"
                      },

                      {
                          name: "Expediente",
                          data: "Expediente"
                      },
                      null,
                      null,
                      null,
                      null,
                      {
                          name: "NombreCompleto",
                          data: "NombreCompleto",
                          visible: false

                      }
                  ],
                  columnDefs: [

                      {
                          data: "TrackingId",
                          targets: 0,
                          orderable: false,
                          visible: false,
                          render: function (data, type, row, meta) {
                              return "";
                          }
                      },
                      {
                          targets: 1,
                          orderable: false,
                          render: function (data, type, row, meta) {
                              return meta.row + meta.settings._iDisplayStart + 1;
                          }
                      }, {
                          targets: 2,
                          orderable: false,
                          render: function (data, type, row, meta) {
                              if (row.RutaImagen != null) {
                                  var ext = "." + row.RutaImagen.split('.').pop();
                                  var photo = row.RutaImagen.replace(ext, ".thumb");
                                  var imgAvatar = resolveUrl(photo);
                                  return '<div class="text-center">' +
                                      '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                      '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                      '</a>' +
                                      '<div>';
                              } else {
                                  pathfoto = resolveUrl("~/Content/img/avatars/male.png");
                                  return '<div class="text-center">' +
                                      '<img id="avatar" class="img-thumbnail text-center" alt = "" src = "' + pathfoto + '" height = "10" width = "50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';"/>' +
                                      '<div>';
                              }
                          }
                      },
                      {
                          targets: 7,
                          orderable: false,
                          render: function (data, type, row, meta) {
                              if (row.Situacion != null) {
                                  return row.Situacion;
                              }
                              else {
                                  return "Pendiente";
                              }
                          }
                      },
                      {
                          targets: 8,
                          orderable: false,

                          render: function (data, type, row, meta) {
                              if (row.Registro != null)

                                  return row.Registro;
                              else
                                  return "Sin examen médico efectuado";
                          }
                      },
                      {
                          targets: 9,
                          orderable: false,
                          visible: false,
                          render: function (data, type, row, meta) {


                              return row.Nombre;
                          }
                      },
                      {
                          targets: 10,
                          orderable: false,
                          render: function (data, type, row, meta) {
                              var edit = "edit";
                              var editar = "";
                              var color = "";
                              var txtestatus = "";
                              var icon = "";
                              var habilitar = "";

                              if (row.Habilitado) {
                                  txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                              }
                              else {
                                  txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                              }
                              var editar2 = "";
                              var action9 = "";
                              var action1 = "";
                              var action0 = '<div class="btn-group"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Ver / editar examen <span class="caret"></span></button><ul class="dropdown-menu">';
                              if (row.ExamenId != 0) {
                                  if ($("#ctl00_contenido_KAQWPK").val() == "true") editar2 = '<li><a  class="btn-sm   ' + edit + '" href="examenmedico.aspx?tracking=' + row.ExamenTrackingId + '&action=update' + '" title="Editar registro"><i class="fa fa-edit"></i> Editar</a></li><li class="divider"></li>';
                                  action9 = '<li><a class="btn-sm    examen" href="javascript:void(0);" id="examen" data-id="' + row.ExamenId + '" title="Examen médico"><i class="fa fa-user-md"></i> Examen médico</a></li><li class="divider"></li>';
                              }
                              action1 = '<li><a ctitle="Antecedentes" class="btn-sm" href="javascript:void(0);" id="antecedentes" data-id="' + row.TrackingId + '" ><i class="fa fa-inbox"></i> Antecedentes</a></li></ul></div>';
                              return action0 + editar2 + action9 + action1;

                          }
                      }
                  ]

              });

              dtable = $("#dt_basic").dataTable().api();

              $("#dt_basic_filter input[type='search']")
                  .unbind()
                  .bind("input", function (e) {

                      if (this.value == "") {
                          dtable.search("").draw();
                      }
                      return;
                  });

              var $textarea = $("#dt_basic_filter input[type='search']");

              $("#dt_basic_filter input[type='search']").keypress(function (e) {
                  if (e.charCode === 13) {
                      setTimeout(function () {
                          $('.btn-group').removeClass('open');
                      }, 0);
                      dtable.search($("#dt_basic_filter input[type='search']").val()).draw();
                  }
              }); 

              $("body").on("click", ".add", function () {
                  $("#add-modal").modal("show");
                  window.emptytableadd = false;
                  window.tableadd.api().ajax.reload();

              });

              $("body").on("click", "#examen", function () {

                  var datos = $(this).attr("data-id");
                 
                 pdf(datos);
              });

              $("body").on("click", "#antecedentes", function () {
                  var datos = $(this).attr("data-id");
                  cargatablaantecentes(datos);
                  $("#add-antecedentes").modal("show");

              });


              function cargatablaantecentes(tracking) {
                  responsiveHelper_dt_basicadAnt = undefined;
                  window.table = $('#dt_basicadAnt').dataTable({
                      destroy: true,
                      "lengthMenu": [10, 20, 50, 100],
                      iDisplayLength: 10,
                      serverSide: true,
                      fixedColumns: true,
                      autoWidth: true,
                      "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                          "t" +
                          "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                      "autoWidth": true,
                      "oLanguage": {
                          "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                      },
                      "preDrawCallback": function () {
                          if (!responsiveHelper_dt_basicadAnt) {
                              responsiveHelper_dt_basicadAnt = new ResponsiveDatatablesHelper($('#dt_basicadAnt'), breakpointDefinition);
                          }
                      },
                      "rowCallback": function (nRow) {
                          responsiveHelper_dt_basicadAnt.createExpandIcon(nRow);
                      },
                      "drawCallback": function (oSettings) {
                          responsiveHelper_dt_basicadAnt.respond();
                          $('#dt_basicadAnt').waitMe('hide');
                      },/*
               "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                        $('td', row).eq(7).addClass('strikeout');
                    }
                },*/
                      ajax: {
                          type: "POST",
                          url: "examenmedico_list.aspx/getinterno",
                          contentType: "application/json; charset=utf-8",
                          data: function (parametrosServerSide) {
                              $('#dt_basicadAnt').waitMe({
                                  effect: 'bounce',
                                  text: 'Cargando...',
                                  bg: 'rgba(255,255,255,0.7)',
                                  color: '#000',
                                  sizeW: '',
                                  sizeH: '',
                                  source: ''
                              });
                              parametrosServerSide.emptytable = false;
                              parametrosServerSide.tracking = tracking;
                              return JSON.stringify(parametrosServerSide);
                          }

                      },
                      columns: [
                          {
                              data: "TrackingId",
                              targets: 0,
                              orderable: false,
                              visible: false,
                              render: function (data, type, row, meta) {
                                  return "";
                              }
                          },
                          null,
                          null,
                          {
                              name: "Expediente",
                              data: "Expediente"
                          },
                          null,
                          {
                              name: "Centro",
                              data: "Centro"
                          }
                          ,
                          {
                              name: "Expediente",
                              data: "Expediente",
                              visible: false
                          },
                          {
                              name: "NCP",
                              data: "NCP",
                              visible: false
                          },
                          {
                              name: "EstatusNombre",
                              data: "EstatusNombre"
                          },
                          null
                      ],
                      columnDefs: [

                          {
                              data: "TrackingId",
                              targets: 0,
                              orderable: false,
                              visible: false,
                              render: function (data, type, row, meta) {
                                  return "";
                              }
                          },
                          {
                              targets: 1,
                              orderable: false,
                              render: function (data, type, row, meta) {
                                  return meta.row + meta.settings._iDisplayStart + 1;
                              }
                          },
                          {
                              targets: 2,
                              orderable: false,
                              visible: false,
                              render: function (data, type, row, meta) {
                                  if (row.RutaImagen != null) {
                                      var ext = "." + row.RutaImagen.split('.').pop();
                                      var photo = row.RutaImagen.replace(ext, ".thumb");
                                      var imgAvatar = ResolveUrl(photo);
                                      return '<div class="text-center">' +
                                          '<a href="#" class="photoview" data-foto="' + ResolveUrl(row.RutaImagen) + '" >' +
                                          '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" />' +
                                          '</a>' +
                                          '<div>';
                                  } else { return ''; }
                              }
                          },
                          {
                              targets: 4,
                              orderable: false,
                              render: function (data, type, row, meta) {

                                  return row.Fecha;
                              }
                          },
                          {
                              targets: 9,
                              orderable: false,
                              visible: false,
                              render: function (data, type, row, meta) {
                                  var txtestatus = "";
                                  var icon = "";
                                  var color = "";
                                  var edit = "edit"
                                  var transfer = "tranfer";
                                  var reentry = "reentry";
                                  var registrar = "";
                                  var habilitar = "";
                                  var egreso = "";
                                  var reingreso = "";
                                  if (row.Activo) {
                                      txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                  }
                                  else {
                                      txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled"; transfer = "disabled"; reentry = "disabled";

                                  }

                                  if ($("#ctl00_contenido_KAQWPK").val() == "true")
                                      registrar = '<a class="btn btn-primary btn-circle ' + edit + '" href="entry.aspx?tracking=' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                  else if ($("#ctl00_contenido_WERQEQ").val() == "true")
                                      registrar = '<a class="btn btn-primary btn-circle ' + edit + '" href="entry.aspx?tracking=' + row.TrackingId + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                                  if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-id="' + row.TrackingIdEstatus + '" data-value = "' + row.Expediente + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                                  // if ($("#ctl00_contenido_VYXMBM").val() == "true" & row.Estatus != 2 & $("#ctl00_contenido_KAQWPK").val() == "true") { egreso = '<a class="btn btn-danger btn-circle ' + transfer + '" href="transfer.aspx?tracking=' + row.TrackingId + '" title="Egreso"><i class="glyphicon glyphicon-transfer"></i></a>&nbsp;'; }
                                  //  if ($("#ctl00_contenido_RAWMOV").val() == "true" & row.Estatus == 2 & $("#ctl00_contenido_KAQWPK").val() == "true") reingreso = '<a class="btn btn-warning btn-circle ' + reentry + '" href="reentry.aspx?tracking=' + row.TrackingId + '" title="Reingreso"><i class="glyphicon glyphicon-share-alt"></i></a>&nbsp;';

                                  return registrar + egreso + reingreso + habilitar;

                              }
                          }
                      ]

                  });

              }


              function pdf(datos) {
                  $.ajax({
                      type: "POST",
                      url: "examenmedico_list.aspx/pdf",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          datos: datos
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.success) {
                              open(resultado.file.replace("~", ""));
                          }
                          else {
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                  "Algo salió mal: " + resultado.mensaje + "</div>");
                              setTimeout(hideMessage, hideTime);
                              ShowError("¡Error!", resultado.mensaje);
                          }
                      },
                      error: function () {
                          ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });

              }


              var responsiveHelper_dt_basicadd = undefined;
              var breakpointAddDefinition = {
                  desktop: Infinity,
                  tablet: 1024,
                  fablet: 768,
                  phone: 480
              };
              window.emptytableadd = true;
              window.tableadd = $('#dt_basicadd').dataTable({
                  "lengthMenu": [10, 20, 50, 100],
                  iDisplayLength: 10,
                  serverSide: true,
                  fixedColumns: true,
                  autoWidth: true,
                  "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                      "t" +
                      "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                  "autoWidth": true,
                  "oLanguage": {
                      "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                  },
                  "preDrawCallback": function () {
                      if (!responsiveHelper_dt_basicadd) {
                          responsiveHelper_dt_basicadd = new ResponsiveDatatablesHelper($('#dt_basicadd'), breakpointAddDefinition);
                      }
                  },
                  "rowCallback": function (nRow) {
                      responsiveHelper_dt_basicadd.createExpandIcon(nRow);
                  },
                  "drawCallback": function (oSettings) {
                      responsiveHelper_dt_basicadd.respond();
                      $('#dt_basicadd').waitMe('hide');
                  },
                  ajax: {
                      url: "examenmedico_list.aspx/getdetenidosexistencia",
                      method: "POST",
                      contentType: "application/json; charset=utf-8",
                      data: function (d) {
                          startLoading();
                          d.emptytableadd = window.emptytableadd;
                          d.pages = $('#dt_basicadd').DataTable().page.info().page || "";
                          return JSON.stringify(d);
                      },
                      dataSrc: "data",
                      dataFilter: function (data) {
                          var json = jQuery.parseJSON(data);
                          json.recordsTotal = json.d.recordsTotal;
                          json.recordsFiltered = json.d.recordsFiltered;
                          json.data = json.d.data;
                          endLoading();
                          return JSON.stringify(json);
                      }
                  },                  
                  columns: [
                      null,
                      {
                          name: "NombreCompleto",
                          data: "NombreCompleto",

                      },
                      {
                          name: "Expediente",
                          data: "Expediente",
                          orderable: false
                      },
                      null,
                  ],
                  columnDefs: [{
                      data: "TrackingId",
                      targets: 0,
                      orderable: false,
                      visible: false,
                      render: function (data, type, row, meta) {
                          return "";
                      }
                  },


                  {
                      targets: 3,
                      orderable: false,
                      render: function (data, type, row, meta) {
                          var edit = "edit";
                          var agregar = "";

                          //if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-success btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=1' + '" title="Agregar sentencia/proceso"><i class="glyphicon glyphicon-plus"></i></a>&nbsp;';
                          //if ($("#ctl00_contenido_HQLNBB").val() == "true")
                          agregar = '<a class="btn btn-success  addexamenmedico" href="javascript:void(0);"'
                              + '" data-tracking= "' + row.TrackingIdEstatus + '" title="Añadir examen médico"><i class="glyphicon glyphicon-plus"></i></a>&nbsp;';

                          return agregar;

                      }
                  }

                  ]
              });

              dtable2 = $("#dt_basicadd").dataTable().api();

              $("#dt_basicadd_filter input[type='search']")
                  .unbind()
                  .bind("input", function (e) {

                      if (this.value == "") {
                          dtable2.search("").draw();
                      }
                      return;
                  });              

              $("#dt_basicadd_filter input[type='search']").keypress(function (e) {
                  if (e.charCode === 13) {
                      //setTimeout(function () {
                      //    $('.btn-group').removeClass('open');
                      //}, 0);
                      dtable2.search($("#dt_basicadd_filter input[type='search']").val()).draw();
                  }
              }); 

              $("body").on("click", ".photoview", function (e) {
                  var photo = $(this).attr("data-foto");
                  $("#foto_detenido").attr("src", photo);

                  $("#foto_detenido").error(function () {
                      $(this).unbind("error").attr("src", rutaDefaultServer);
                  });

                  $("#photo-arrested").modal("show");
              });


              function resolveUrl(url) {
                  var baseUrl = "<%= ResolveUrl("~/") %>";
                  if (url.indexOf("~/") == 0) {
                      url = baseUrl + url.substring(2);
                  }
                  return url;
              }

              $("body").on("click", ".blockitem", function () {
                  var itemnameblock = $(this).attr("data-value");
                  var verb = $(this).attr("style");
                  $("#itemnameblock").text(itemnameblock);
                  $("#verb").text(verb);
                  $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                  $("#blockitem-modal").modal("show");
              });


              $("body").on("click", ".addexamenmedico", function () {
                  var id = $(this).attr("data-tracking");
                  startLoading();
                  window.location.href = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Examen_medico/examenmedico.aspx?tracking=" + id + "&action=add";
              });

              $("body").on("click", ".search", function () {
                  window.emptytable = false;
                  window.table.api().ajax.reload();
              });

              $("body").on("click", ".clear", function () {
                  $('#tipo').val("0");
                  $('#lado').val("0");
                  $('#ctl00_contenido_region').val("");
                  $('#vista').val("0");
                  $('#ctl00_contenido_cantidad').val("");
                  $('#ctl00_contenido_descripcion_').val("");

              });

              $("body").on("click", ".delete", function () {
                  var id = $(this).attr("data-id");
                  var nombre = $(this).attr("data-value");
                  $("#senaleliminar").text(nombre);
                  $("#btndelete").attr("data-id", id);
                  $("#delete-modal").modal("show");
              });

              function ResolveUrl(url) {
                  var baseUrl = "<%= ResolveUrl("~/") %>";
                  if (url.indexOf("~/") == 0) {
                      url = baseUrl + url.substring(2);
                  }
                  return url;
              }

              function init() {

              }

              var hideTime = 5000;
              function hideMessage() {
                  $("#ctl00_contenido_lblMessage").html("");
              }

          });
      </script>
</asp:Content>

