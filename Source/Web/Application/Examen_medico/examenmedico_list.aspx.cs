﻿using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;
using System.Linq;
using Entity;

namespace Web.Application.Examen_medico
{
    public partial class examenmedico_list : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor == "")
                Response.Redirect("dashboard.aspx");
        }

        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Examen médico" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {

                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();




                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static string pdf(int datos)
        {
            var serializedObject = string.Empty;
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");
                    Entity.ExamenMedico Examen = ControlExamenMedico.ObtenerPorId(datos);
                    Entity.DetalleDetencion detencion = ControlDetalleDetencion.ObtenerPorId(Examen.DetalleDetencionId);
                    var interno = ControlDetenido.ObtenerPorId(detencion.DetenidoId);

                    string[] Id = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(Id);

                    var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);

                    if (examenmedico.Count > 0)
                    {
                        var obj = ControlPDFEM.ReporteEM(interno.Id, interno.TrackingId, datos);
                        serializedObject = JsonConvert.SerializeObject(obj);
                    }
                    else
                    {
                        serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El detenido no cuenta con examen médico." });
                    }

                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getinterno(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("I.TrackingId", tracking));
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                        string IdsContratos = "(";

                        int i = 0;

                        foreach (var item in dataContratos)
                        {
                            if (i == dataContratos.Count - 1)
                            {
                                IdsContratos += item.IdContrato + ")";
                            }
                            else
                            {
                                IdsContratos += item.IdContrato + ",";
                            }

                            i++;
                        }

                        where.Add(new Where("E.ContratoId", Where.IN, IdsContratos));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);

                        }
                        Query query = new Query
                        {
                            select = new List<string> {
                        "I.Id",
                        "I.TrackingId",
                        "I.Nombre",
                        "I.Paterno",
                        "I.Materno",
                        "I.RutaImagen",
                        "ifnull(ExpedienteA,Expediente) Expediente",
                        "DATE_FORMAT(E.Fecha,'%d/%m/%Y') Fecha",
                        "E.NCP",
                        "E.Estatus",
                        "E.Activo",
                        "E.TrackingId TrackingIdEstatus" ,
                        "ES.Nombre EstatusNombre",
                        "CR.Nombre Centro"
                    },
                            from = new Table("detenido", "I"),
                            joins = new List<Join>
                    {
                        new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                        new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"),
                         new Join(new Table("institucion", "CR"), "CR.Id  = E.CentroId"),

                        new Join(new Table("(Select DetalleDetencionId, Expediente ExpedienteA from historico_agrupado_detenido )", "J"), "J.DetalleDetencionId=E.Id")
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }


        [WebMethod]
        public static object getdetenidosexistencia(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string pages)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytableadd)
                    {
                        List<Entity.ListadoDetenidosExamenMedico> data = new List<Entity.ListadoDetenidosExamenMedico>();
                        List<Entity.ListadoDetenidosExamenMedico> listTotals = new List<Entity.ListadoDetenidosExamenMedico>();

                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                        object[] dataParams = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0
                        };

                        listTotals = ControlListadoDetenidosExamenMedico.ObtenerTodos(dataParams);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value.ToLower());
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.NombreCompleto).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.NombreCompleto).ToList();
                                    break;                                
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                            }
                        }

                        List<Entity.ListadoDetenidosExamenMedico> listFinal = new List<Entity.ListadoDetenidosExamenMedico>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;
                    }
                    else
                    {
                        object json2 = new { data = new List<ListadoDetenidosExamenMedico>(), recordsTotal = new List<ListadoDetenidosExamenMedico>().Count, recordsFiltered = new List<ListadoDetenidosExamenMedico>().Count, };
                        return json2;
                    }
                }
                else
                {
                    object json2 = new { data = new List<ListadoDetenidosExamenMedico>(), recordsTotal = new List<ListadoDetenidosExamenMedico>().Count, recordsFiltered = new List<ListadoDetenidosExamenMedico>().Count, };
                    return json2;
                }
            }
            catch (Exception ex)
            {
                object json2 = new { data = new List<ListadoDetenidosExamenMedico>(), recordsTotal = new List<ListadoDetenidosExamenMedico>().Count, recordsFiltered = new List<ListadoDetenidosExamenMedico>().Count, };
                return json2;
            }
        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string pages)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                    {
                        List<Entity.ListadoExamenMedico> data = new List<Entity.ListadoExamenMedico>();
                        List<Entity.ListadoExamenMedico> listTotals = new List<Entity.ListadoExamenMedico>();                        

                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);                        
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);                        

                        object[] dataParams = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0
                        };

                        listTotals = ControlListadoExamenMedico.ObtenerTodos(dataParams);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value.ToLower());
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                            }
                        }

                        List<Entity.ListadoExamenMedico> listFinal = new List<Entity.ListadoExamenMedico>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;
                    }
                    else
                    {
                        object json2 = new { data = new List<ListadoExamenMedico>(), recordsTotal = new List<ListadoExamenMedico>().Count, recordsFiltered = new List<ListadoExamenMedico>().Count, };
                        return json2;
                    }
                }
                catch (Exception ex)
                {
                    object json2 = new { data = new List<ListadoExamenMedico>(), recordsTotal = new List<ListadoExamenMedico>().Count, recordsFiltered = new List<ListadoExamenMedico>().Count, };
                    return json2;
                }
            }
            else
            {
                object json2 = new { data = new List<ListadoExamenMedico>(), recordsTotal = new List<ListadoExamenMedico>().Count, recordsFiltered = new List<ListadoExamenMedico>().Count, };
                return json2;
            }
        }

        [WebMethod]
        public static string delete(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Eliminar)
                {
                    Guid tracking = new Guid(trackingid);
                    Entity.ExamenMedico item = ControlExamenMedico.ObtenerPorTrackingId(tracking);
                    item.Activo = false;
                    ControlExamenMedico.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }



        [WebMethod]
        public static string blockitem(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Eliminar)
                {
                    Entity.ExamenMedico item = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));
                    item.Activo = item.Activo ? false : true;
                    ControlExamenMedico.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }


    }
}