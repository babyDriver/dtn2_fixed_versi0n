<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="examen_medico.aspx.cs" Inherits="Web.Application.Examen_medico.examen_medico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Examen_medico/examen_medico_list.aspx">Examen médico</a></li>
    <li>Examen médico</li>  
  
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        .select2-selection__choice
        {
                padding: 1px 28px 1px 8px;
                margin: 4px 0 3px 5px;
                position: relative;
                line-height: 18px;
                color: #fff;
                cursor: default;
                border: 1px solid #2a6395;
        }
  
        .dataTables_filter .input-group-addon{
            height: auto;
        }
        /*.select2-container .select2-choice {
    padding: 5px 10px;
    height: 40px;
    width: 132px; 
    font-size: 1.2em;  
}*/
        .select2-container.form-control {
            height: auto !important;
        }
        .same-height-thumbnail{
            height: 300px;
            margin-top:0;
        }
    </style>
    <div class="scroll">
        <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-list-alt"></i>
                Información personal
            </h1>
        </div>
    </div>-->
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
				        <table class="table-responsive">
					        <tbody>
                                <tr>
						            <td colspan="2">
                                        <br />
                                        <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
				                    </td>
							    </tr>
						        <tr>
								    <td colspan="2">
                                        <br />
									    <br />	
								    </td>
                                    <td colspan="2"></td>
							    </tr>
						    </tbody>
				        </table>  
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
                        <table class="table-responsive">
					        <tbody>
                                <tr>
							        <td colspan="2">
								        <table class="table-responsive">
									        <tbody>
                                                <tr>
										            <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
										            <td>&nbsp; <small><span id="nombreInterno"></span></small><br /></td>
									            </tr>
									            <tr>
										            <th><span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                    <td>&nbsp;&nbsp;<small><span id="edadInterno">Fecha de nacimiento no registrada</span></small></td>
									            </tr>
									            <tr>
										            <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
										            <td>&nbsp;  <small><span id="sexoInterno"></span></small><br /></td>
									            </tr>
									            
								            </tbody>
								        </table>
								    </td>
							    </tr>
							    <tr>
								    <td colspan="2"><div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div></td>
								    <td align="left"><div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div></td>
								    <td></td>
							    </tr>
						    </tbody>
                        </table>  
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                        <table class="table-responsive">
                            <tbody>
                                <tr>
							        <td colspan="2">
								        <table class="table-responsive">
									        <tbody>
                                                <tr>
                                                    <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
										            <td>&nbsp; <small><span id="centroInterno"></span></small></td>
									            </tr>
									            <tr>
										            <th> <span><strong style="color: #006ead;">No. de remisión:</strong> </span></th>
										            <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
									            </tr>
									            <tr>
										            <th> <span><strong style="color: #006ead;">Alias:</strong> </span></th>
										            <td>&nbsp; <small><span id="aliasinternotd">No registrado</span></small></td>
									            </tr>
									            
								            </tbody>
								        </table>
								    </td>								
							    </tr>
							    <tr>
								    <td colspan="2"><div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div></td>
								    <td align="left"><div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div></td>
								    <td></td>
							    </tr>
						    </tbody>
                        </table>  
                    </div>
                </article>
                <div></div>
                <br />
            </div>
            <div class="row" style="margin-left:10px">
                <%--<a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>--%>
                <a href="javascript:void(0);" class="btn btn-md btn-warning" id="sexoEdad"><i class="fa fa-pencil"></i>&nbsp;Cambiar edad y sexo</a>
                <br />&nbsp;
            </div>
            <br />
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-examenmedico-2" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-user-md"></i></span>
                        <h2>Examen médico </h2>
                    </header>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <!-- widgetbody -->
                        <ul id="tabs" class="nav nav-tabs ">
                            <li id="tabexamenfisico"class="active">
                                <a id="refexamenfisico" href="#s0" data-toggle="tab" aria-expanded="true">Certificado médico psicofisiológico </a>
                            </li>
                            <li id="tabexamen" class="">
                                <a id="refexamen" href="#s1" data-toggle="tab" aria-expanded="true">Certificado médico lesiones</a>
                            </li>
                            <li id="tabpruebas" class="">
                                <a id="refpruebas" href="#s2" data-toggle="tab" aria-expanded="false">Certificado químico</a>
                            </li>
                        </ul>
                        <div id="content" class="tab-content padding-10">
                              <div class="tab-pane fade active in " id="s0">
                                  <div class="row smart-form">
                                      <section class="col-md-2 col-lg-2 col-xs-2">
                                            <label class="label"   >Fecha y hora de  valoración <a style="color: red">*</a></label>
                                                  <label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker3'>
                                                                <input type="text" name="fechahora" id="fechahora3" class='input-group date alptext' placeholder="Fecha y hora valoración" data-requerido="true"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                        </label>
                                                <i></i>
                                        </section>

                                  </div>
                                  <div class="row smart-form">
                                      <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">
                                          <label class="label">Aliento <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="aliento" style="width: 100%;">
                                                  <option value="0">Seleccione</option>
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa aliento.</b>
                                          </label>
                                      </section>
                                      <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">
                                          <label class="label">Marcha <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="marcha">
                                                  <option value="0">Seleccione</option>
                                              </select>&nbsp;
                                                    <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa marcha.</b>
                                          </label>
                                      </section>
                                      <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">
                                          <label class="label">Actitud <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="idactitud">
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa actitud.</b>
                                          </label>
                                      </section>
                                      <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">
                                          <label class="label">Atención <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="atencion">
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa atención.</b>
                                          </label>
                                      </section>
                                      <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">
                                          <label class="label">Cavidad oral <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="cavidadoral">
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa la cavidad oral.</b>
                                          </label>
                                      </section>
                                  </div>
                                  <div class="row smart-form">
                                   <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">
                                          <label class="label">Pupilas <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="pupilas">
                                                  <option value="0">Seleccione</option>
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa pupilas.</b>
                                          </label>
                                      </section>
                                      <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">

                                          <label class="label">Discurso <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="pupilares">
                                                  <option value="0">Seleccione</option>
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa discurso.</b>
                                          </label>
                                      </section>
                                      <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">
                                          <label class="label">Conjuntivas <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="conjuntivas">
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa conjuntivas.</b>
                                          </label>
                                      </section>
                                      <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right: 10px">
                                          <label class="label">Lenguaje <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="lenguaje">
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa lenguaje.</b>
                                          </label>
                                      </section>
                                      <section class="col-md-2 col-lg-2 col-xs-2">
                                          <label class="label">Romberg <a style="color: red">*</a></label>
                                          <label class="select">
                                              <select runat="server" id="romberq">
                                                  <option value="0">Seleccione</option>
                                              </select>
                                              <i></i>
                                              <b class="tooltip tooltip-bottom-right">Ingresa romberg.</b>
                                          </label>
                                      </section>
                                  </div>
                                  <div class="row smart-form">
                                      <!-- sección de coordinación motriz-->   
                                      <div class="form-group col-md-5 col-lg-5 col-xs-5 " > 
                                           <header>Coordinación motriz</header>
                                            <br />
                                           <div class="col-md-5 col-lg-5 col-xs-5" style="margin-right:10px">
                                                <label class="label"><b>Dedo-dedo</b></label>                                               
                                                <section >                                                    
                                                    <label class="label">Ojos abiertos <a style="color: red">*</a></label>
                                                    <label class="select">
                                                        <select runat="server" id="ojos_abiertos_dedo_dedo">
                                                            <option value="0">Seleccione</option>
                                                        </select>
                                                        <i></i>
                                                        <b class="tooltip tooltip-bottom-right">Ingresa dedo-nariz.</b>
                                                    </label>
                                                </section>
                                                <label class="label"><b>Dedo-nariz</b></label>
                                               
                                                <section >
                                                    <label class="label">Ojos abiertos <a style="color: red">*</a></label>
                                                    <label class="select">
                                                        <select runat="server" id="ojos_abiertos_dedo_nariz">
                                                            <option value="0">Seleccione</option>
                                                        </select>
                                                        <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa dedo-nariz.</b>
                                                    </label>
                                                </section>
                                           </div>
                                           <div class="col-md-5 col-lg-5 col-xs-5">
                                               <label class="label"><b>&nbsp</b></label>
                                               <section >
                                                    <label class="label"> Ojos cerrados <a style="color: red">*</a></label>
                                                    <label class="select">
                                                        <select runat="server" id="ojos_cerrados_dedo_dedo">
                                                            <option value="0">Seleccione</option>
                                                        </select>
                                                        <i></i>
                                                        <b class="tooltip tooltip-bottom-right">Ingresa dedo-nariz.</b>
                                                    </label>
                                                </section>
                                                <label class="label"><b>&nbsp</b></label>
                                                <section >                                              
                                                    <label class="label">Ojos cerrados<a style="color: red">*</a></label>
                                                    <label class="select">
                                                        <select runat="server" id="ojos_cerrados_dedo_nariz">
                                                            <option value="0">Seleccione</option>
                                                        </select>
                                                        <i></i>
                                                     <b class="tooltip tooltip-bottom-right">Ingresa dedo-nariz.</b>
                                                    </label>
                                                 </section>
                                           </div>
                                      </div>

                                      <!-- sección de signos vitales-->   
                                      <div class="form-group col-md-7 col-lg-7 col-xs-7" > 
                                          <header>Signos vitales</header>
                                          <br />     
                                          <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:5px; margin-left:5px">
                                                <label class="label"><b>&nbsp</b></label>
                                                <label class="label">T/A </label>
                                                <label class="input">
                                                    <input type="text" name="ta" maxlength="10" id="ta"  placeholder="" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa T/A.</b>
                                                </label>
                                          </section>
                                          <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:5px;">
                                                <label class="label"><b>&nbsp</b></label>
                                                <label class="label">FR<a style="color: red">*</a></label>
                                                <label class="input">
                                                    <input type="text" name="fr" maxlength="10" id="fr" placeholder="" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la FR.</b>
                                                </label>
                                            </section>
                                          <section class="col-md-1 col-lg-1 col-xs-1" style="margin-right:2px;">
                                                 <label class="label"><b>&nbsp</b></label>
                                                <label class="label">&nbsp</label>
                                                <label class="label">xmin</label>
                                                
                                            </section>
                                          <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:5px;">
                                                 <label class="label"><b>&nbsp</b></label>
                                                <label class="label">FC <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <input type="text" name="fc" maxlength="10" id="fc" placeholder="" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la FC.</b>
                                                    
                                                </label>
                                            </section>
                                           <section class="col-md-1 col-lg-1 col-xs-1" style="margin-right:2px;">
                                                 <label class="label"><b>&nbsp</b></label>
                                                <label class="label">&nbsp</label>
                                                <label class="label">xmin</label>
                                                
                                            </section>
                                            <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:5px;"  >
                                                 <label class="label"><b>&nbsp</b></label>
                                                <label class="label">Pulso <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <input type="text" name="pulso" maxlength="10" id="pulso"  placeholder="" class="alphanumeric alptext" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el pulso.</b>
                                                </label>
                                            </section>
                                          <section class="col-md-1 col-lg-1 col-xs-1" style="margin-right:2px;">
                                                 <label class="label"><b>&nbsp</b></label>
                                                <label class="label">&nbsp</label>
                                                <label class="label">xmin</label>
                                                
                                            </section>
                                          <section class="col-md-11 col-lg-11 col-xs-11">
                                              <label class="label"><b>&nbsp</b></label>
                                               <label class="label"><b>&nbsp</b></label>
                                               
                                              <label class="checkbox" style="color: #3276B1; font-weight: bold;">
                                                  <input type="checkbox" name="novalorable" id="novalorabe" value="true" />
                                                  <i></i>No valorable</label>
                                          </section>
                                          
                                          <div class="col-md-5 col-lg-5 col-xs-5">
                                               <header>Orientación</header>
                                                <br />
                                                <section>
                                                     <label class="label">Orientación </label>
                                                    <select multiple class="select2" style="width:100%" runat="server" id="orientacion">
                                                        <option value="0">Seleccione</option>
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa orientación.</b>
                                                 </section>
                                          </div>
                                          <div class="col-md-1 col-lg-1 col-xs-1"></div>
                                          <div class="col-md-5 col-lg-5 col-xs-5">
                                                <header>Conclusión</header>
                                                <br />  
                                                <section>
                                                    <label class="label">Conclusión </label>
                                                    <select multiple class="select2" runat="server" id="conclusion">
                                                        <option value="0">Seleccione</option>
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa conclusión.</b>
                                                </section>
                                          </div>
                                      </div>
                                  </div>
                         
                                  <div class="row smart-form">
                                        <section class="col-md-12 col-lg-12 col-xs-12">
                                            <label class="checkbox" style="color:#3276B1; font-weight:bold;">
                                                <input type="checkbox" name="sustento" id="sustento" value="true"/>
                                                <i></i>Sustento toxicológico</label>
                                        </section>
                                        <section class="col-md-12 col-lg-12 col-xs-12">
                                                 <label class="label">Observaciones  </label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="3" id="observaciones" placeholder="Observaciones" class="alphanumeric alptext" maxlength="999"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Ingresa observaciones.</b>
                                            </label>
                                         </section>
                                  </div>
                                    
                                  <div class="col-md-12 col-lg-12 col-xs-12">
                                        <br />
                                         <div class="row smart-form">
                                             <footer>
                                                 <a href="examen_medico_list.aspx" class="btn btn-default btn-sm" title="Volver al listado de examen médico"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar&nbsp; </a>
                                                 <a class="btn btn-default btn-sm savecertificadopsicofisiologico" title="Guardar" id="savecertificadopsicofisiologico"><i class="fa fa-save"></i>&nbsp;Guardar &nbsp;</a>
                                             </footer>
                                         </div>
                                    </div>
                                    </div>
                                                     
                            
                            <div class="tab-pane fade " id="s1">
                                <div class="row smart-form">
                                    <!-- sección lesión-->
                                    <div  style="margin-right:20px"  class="col-md-10 col-lg-10 col-xs-10">
                                        <header>Lesiones</header><br />
                                         <section class="col-md-7 col-lg-7 col-xs-7">
                                            <label class="label"   >Fecha y hora de  valoración <a style="color: red">*</a></label>
                                                  <label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker4'>
                                                                <input type="text" name="fechahora" id="fechahora4" class='input-group date alptext' placeholder="Fecha y hora valoración" data-requerido="true"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                        </label>
                                                <i></i>
                                        </section>
                                        <div class="col-md-7 col-lg-7 col-xs-7">
                                         <section>
                                            <label class="label" > Lesión  </label>
                                             <i></i>
                                                <select multiple class="select2" name="tipo_lesion" id="tipo_lesion" runat="server"></select>
                                        </section>
                                        
                                        <section >
                                            <label class="label">Observaciones  </label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="3" id="observacioneslesion" placeholder="Observaciones" class="alphanumeric alptext" maxlength="999" disabled="disabled"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Ingresa observaciones.</b>
                                            </label>
                                        </section>
                                        </div>
                                        <div class="col-md-1 col-lg-1 col-xs-1"></div>
                                        <div class="col-md-4 col-lg-4 col-xs-4">
                                            <section >
                                            <label class="checkbox" style="color:#3276B1; font-weight:bold; font-size-adjust:0.58">
                                                <input type="checkbox" name="sinlesion" id="sinlesion" value="true"/>
                                                <i></i>No hay lesiones visibles a simple vista</label>
                                        </section>
                                            <section >
                                            <label class="checkbox" style="color:#3276B1; font-weight:bold; font-size-adjust:0.58">
                                                <input type="checkbox" name="Lesiones_visibles" id="Lesion_visible" value="true"/>
                                                <i></i>Lesiones visibles a simple vista</label>
                                        </section>
                                        <section >
                                            <label class="checkbox"   style="color:#3276B1; font-weight:bold; font-size:small">
                                                    <input type="checkbox"name="fallecimiento" id="fallecimiento" value="false"/>
                                                    <i></i>Fallecimiento</label>
                                        </section>
                                        <section >
                                            <label class="checkbox"  style="color:#3276B1; font-weight:bold; font-size:small">
                                                    <input type="checkbox"  style="color:#3276B1; font-weight:bold;" name="fallecimiento" id="enviohospital" value="false"/>
                                                    <i></i>Envío a hospital</label>
                                        </section>
                                        <section hidden="hidden">
                                            <label class="checkbox" style="color:#3276B1; font-weight:bold; font-size:small">
                                                    <input type="checkbox" name="peligro_" id="peligro" value="true"/>
                                                    <i></i>Ponen en peligro la vida</label>
                                        </section> 
                                        <section  hidden="hidden" >
                                            <label class="checkbox" style="color:#3276B1; font-weight:bold; font-size:small">
                                                    <input type="checkbox" name="consecuencias_" id="consecuencias" value="false"/>
                                                    <i></i>Dejan consecuencias médico-legales</label>
                                        </section>
                                        </div>
                                       
                                    </div>
                                    <!-- sección alergias-->
                                    <div class="col-md-5 col-lg-5 col-xs-5" style="margin-right:20px">
                                        <header>Alergias</header><br />
                                         <section >
                                            <label class="label"> Alergia  </label>
                                                <select multiple class="select2" name="tipo_alergia" id="tipo_alergia" runat="server" rows="3"></select>
                                                <i></i>
                                        </section> 
                                        <section >
                                            <label class="label">Observaciones  </label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="3" id="observacionesalergia" placeholder="Observaciones" class="alphanumeric alptext" disabled="disabled" maxlength="999"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Observaciones.</b>
                                            </label>
                                        </section>
                                       
                                    </div>
                                    <!-- sección antecedentes-->
                                    <div class="col-md-5 col-lg-5 col-xs-5" >
                                        <header>Antecedentes</header><br />
                                        <section >
                                            <label  class="label"> Antecedente</label>
                                            
                                                <select multiple class="select2" name="antecedentes" id="antecedentes" runat="server"></select>
                                                <i></i>
                                            
                                        </section>
                                      
                                        <section >
                                            <label class="label">Observaciones  </label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="3" id="observacionesAntecedente" placeholder="Observaciones" class="alphanumeric alptext" disabled="disabled" maxlength="999"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Observaciones.</b>
                                            </label>
                                        </section>
                                    </div>
                                    <!-- sección tatuajes-->
                                    <div class="col-md-5 col-lg-5 col-xs-5" style="margin-right:20px">
                                        <header>Tatuajes</header><br />
                                       <section >
                                            <label  class="label"> Tatuaje  </label>
                                            
                                                <select multiple class="select2" name="tipo_tatuaje" id="tipo_tatuaje" runat="server"></select>
                                                <i></i>
                                            
                                        </section>
                                       
                                        <section >
                                            <label class="label">Observaciones </label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="3" id="observacionesTatuaje" placeholder="Observaciones" class="alphanumeric alptext" disabled="disabled" maxlength="999"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Observaciones.</b>
                                            </label>
                                        </section>
                                    </div>
                                     <div class="col-md-5 col-lg-5 col-xs-5">     
                                        <header>Observaciones</header><br />
                                        <section >
                                            <label class="label">Observaciones </label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="8" id="observacionesGlobales" placeholder="Observaciones" class="alphanumeric alptext" maxlength="999"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Observaciones.</b>
                                            </label>
                                        </section>
                                         </div>
                                   
                                       <div class="col-md-10 col-lg-10 col-xs-10">     
                                        <header>Evidencia fotográfica</header><br />
                                        <section >
                                            <a href="javascript:void(0);" class="btn btn-default btn-sm" id="addLesion"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                            <br />
                                            <br />
                                            <table id="dt_basic_lesiones_fotos" class="table table-striped table-bordered table-hover" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>#</th>
                                                        <th>Evidencia fotográfica</th>
                                                        <th>Descripción</th>
                                                        <th data-hide="phone,tablet">Acciones</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </section>
                                    </div>
                                        
                                       
                                    </div>
                        
                              
                                <div class="row smart-form"">
                            
                                     <footer class="col-md-11 col-lg-11 col-xs-11">
                                                 <a href="examen_medico_list.aspx" class="btn btn-default btn-sm" title="Volver al listado de examen médico"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar&nbsp; </a>
                                                 <a class="btn btn-default btn-sm savecertificadolesion" title="Guardar" id="savecertificadolesion"><i class="fa fa-save"></i>&nbsp;Guardar &nbsp;</a>
                                             </footer>
                                    </div>
                                
                            </div>
                            <br />
                            <div class="tab-pane fade" id="s2">
                                <div class="row smart-form">
                                            <section class="col-md-3 col-lg-3 col-xs-3">
                                                <label class="label">Folio <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <input type="text" name="fc" title="" pattern="^\Q\d*?$" maxlength="8" id="Folio"  placeholder="" class="alphanumeric alptext"/>
                                                     <b class="tooltip tooltip-bottom-right">Ingresa folio.</b>
                                                 
                                                </label>
                                            </section>
                                            <section class="col-md-5 col-lg-4 col-xs-4"></section>
                                            <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:10px">
                                                    <label class="label"   >Fecha y hora de toma <a style="color: red">*</a></label>
                                                    <label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker'>
                                                                <input type="text" name="fechahora" id="fechahora" class='input-group date alptext' placeholder="Fecha y hora de toma" data-requerido="true"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                   </label>
                                                    <i></i>
                                            </section>
                                            <section class="col-md-2 col-lg-2 col-xs-2">
                                                <label class="label">Fecha y hora de proceso <a style="color: red">*</a></label>
                                                     <label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker2'>
                                                                <input type="text" name="fechahora" id="fechahora2" class='input-group date alptext' placeholder="Fecha y hora de proceso" data-requerido="true"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                     </label>
                                                     <i></i>
                                            </section>
                                    
                                </div>
                                <div class="row smart-form">
                                    <div class="col-md-7 col-lg-7 col-xs-7" style="margin-right:10px" >
                                         <header>Resultados</header>
                                         <br />
                                     <div class="row" style="margin-left:20px">   
                                    <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:10px" >
                                        <label class="label">Etanol <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select runat="server" id="Etanol" >
                                                        <option value="-1">[Seleccione]</option>
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa etanol.</b>
                                                    </label>
                                    </section>
                                         <section class="col-md-1 col-lg-1 col-xs-1">
                                              <label class="label">&nbsp</label>
                                              <label class="checkbox" style="color: #3276B1; font-weight: bold;">
                                                  <input type="checkbox" name="novalorable" id="printetanol" value="true" />
                                                  <i></i></label>
                                          </section>
                                    <section class="col-md-1 col-lg-1 col-xs-1" id="gradosection" hidden="hidden" >
                                                <label class="label">Grado <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <input type="text" name="fc" maxlength="50" id="grado" title=""pattern='^[0-9]*$' placeholder="" class="alphanumeric alptext"/>
                                                 
                                                </label>
                                    </section>
                                    <section id="mgdlsection" class="col-md-1 col-lg-1 col-xs-1" hidden="hidden" >
                                                 <br />  <br />  
                                                <label class="label">&nbsp mg/dL </label>

                                    </section>
                                    <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:10px" >
                                        <label class="label">Benzodiazepina <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select runat="server" id="Benzodiazepina" >
                                                         <option value="-1">[Seleccione]</option>
                                                       
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa Benzodiazepina.</b>
                                        </label>
                                    </section>
                                          <section class="col-md-1 col-lg-1 col-xs-1">
                                              <label class="label">&nbsp</label>
                                              <label class="checkbox" style="color: #3276B1; font-weight: bold;">
                                                  <input type="checkbox" name="novalorable" id="printbenzodiazepina" value="true" />
                                                  <i></i></label>
                                          </section>
                                    <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:10px" >
                                        <label class="label">Anfetamina <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select runat="server" id="anfetamina" >
                                                         <option value="-1">[Seleccione]</option>
                                                        
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa anfetamina.</b>
                                                    </label>
                                    </section>
                                          <section class="col-md-1 col-lg-1 col-xs-1">
                                              <label class="label">&nbsp</label>
                                              <label class="checkbox" style="color: #3276B1; font-weight: bold;">
                                                  <input type="checkbox" name="novalorable" id="printanfetamina" value="true" />
                                                  <i></i></label>
                                          </section>
                                      </div>
                                        <div class="row" style="margin-left:20px">
                                    

                                    <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:10px" >
                                        <label class="label">Cannabis <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select runat="server" id="cannabis" >
                                                         <option value="-1">[Seleccione]</option>
                                                        
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa canabis
                                                        .</b>
                                                    </label>
                                    </section>
                                            <section class="col-md-1 col-lg-1 col-xs-1">
                                              <label class="label">&nbsp</label>
                                              <label class="checkbox" style="color: #3276B1; font-weight: bold;">
                                                  <input type="checkbox" name="novalorable" id="printcannabis" value="true" />
                                                  <i></i></label>
                                          </section>
                                    <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:10px" >
                                        <label class="label">Cocaína <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select runat="server" id="cocaina" >
                                                         <option value="-1">[Seleccione]</option>
                                                        
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa cocaína
                                                        .</b>
                                                    </label>
                                    </section>
                                            <section class="col-md-1 col-lg-1 col-xs-1">
                                              <label class="label">&nbsp</label>
                                              <label class="checkbox" style="color: #3276B1; font-weight: bold;">
                                                  <input type="checkbox" name="novalorable" id="printcocaina" value="true" />
                                                  <i></i></label>
                                          </section>
                                    <section class="col-md-2 col-lg-2 col-xs-2" style="margin-right:10px" >
                                        <label class="label">Éxtasis <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select runat="server" id="extasis" >
                                                        <option value="-1">[Seleccione]</option>
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa éxtasis
                                                        .</b>
                                        </label>
                                    </section>
                                             <section class="col-md-1 col-lg-1 col-xs-1">
                                              <label class="label">&nbsp</label>
                                              <label class="checkbox" style="color: #3276B1; font-weight: bold;">
                                                  <input type="checkbox" name="novalorable" id="printextasis" value="true" />
                                                  <i></i>&nbsp</label>
                                          </section>
                                           

                                        </div>

                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-4">
                                          <header>Equipo utilizado</header>
                                    <br />
                                        
                                        <section  >
                                        <label class="label">Equipo a utilizar <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select runat="server" id="equipo_a_utilizar" >
                                                        <option value="-1">[Seleccione]</option>
                                                    </select>
                                                    <i></i>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa equipo a utulizar
                                                        .</b>
                                        </label>
                                    </section>
                                    

                                    </div>
                                   
                                   
                                </div>

                                <div class="row smart-form">
                                          <footer>                                        
                                                <a href="examen_medico_list.aspx" class="btn btn-default btn-sm" title="Volver al listado de examen médico"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar &nbsp;</a>
                                                <a class="btn btn-default btn-sm savecertificadoquimico" title="Guardar" id="savecertificadoquimico"><i class="fa fa-save"></i>&nbsp;Guardar&nbsp; </a>
                                            </footer>
                                </div>
                            
                        </div>




                            </div>
                    </div>
                </div>
                <!-- end widgetbody -->
            </article>
            <div class="modal fade" id="form-modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="form-modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <div id="register-form" class="smart-form">
                                <fieldset>
                                    <div class="row">
                                        
                                        <section id="sectionLugar">
                                            <label class="input" style="color: dodgerblue" >Lugar <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-certificate"></i>
                                                <input type="text" name="lugar" id="lugaradicional" runat="server"  placeholder="Lugar" maxlength="256" class="alptext"/>
                                                <b class="tooltip tooltip-bottom-right">Ingrese lugar</b>
                                            </label>
                                        </section>
                                        <section>
                                            <label class="input" style="color: dodgerblue" >Observación</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-certificate"></i>
                                                <input type="text" name="observacion"  id="observacionadicional" runat="server"  placeholder="Observación" maxlength="256" class="alptext"/>
                                                <b class="tooltip tooltip-bottom-right" style="color: dodgerblue">Ingrese observación</b>
                                            </label>
                                        </section>
                                    </div>
                                    <input type="hidden" id="trackingadicional" runat="server" value="" />
                                </fieldset>
                                <footer>
                                    <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                                    <a class="btn btn-sm btn-default saveadicional" id="btnsaveadicional"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="historyTatuajes-modal" class="modal fade" data-backdrop="static" data-kerboard="false" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial tatuajes</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-sm-12">
                        <table id="dt_basichistorytatuajes" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th data-hiden="tablet,fablet,phone">Lugar</th>
                                    <th>Movimiento</th>
                                    <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                    <th>Fecha movimiento</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                        </div>
                </div>
            </div>
    </div>
 </div>
        <div id="historyIntoxicacion-modal" class="modal fade" data-backdrop="static" data-kerboard="false" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial intoxicaciones</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-sm-12">
                        <table id="dt_basichistoryintoxicacion" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th data-hiden="tablet,fablet,phone">Observacion</th>
                                    <th>Movimiento</th>
                                    <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                    <th>Fecha movimiento</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                        </div>
                </div>
            </div>
    </div>
 </div>
        <div id="historyLesion-modal" class="modal fade" data-backdrop="static" data-kerboard="false" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial lesiones</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-sm-12">
                        <table id="dt_basichistoryLesion" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th data-hiden="tablet,fablet,phone">Lugar</th>
                                    <th>Movimiento</th>
                                    <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                    <th>Fecha movimiento</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                        </div>
                </div>
            </div>
    </div>
 </div>
    <div class="modal fade" id="sexoedad-modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class='fa fa-pencil'></i> Cambiar edad y sexo</h4>
                </div>
                <div class="modal-body" style="padding:0 20px;">
                    <div id="sexoedad-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="label" style="color:#3276b1;">Edad<a style="color: red">*</a></label>
                                    <input type="text" title="" class="form-control" maxlength="3" pattern='^[0-9]*$' id="edad"/>
                                </section>
                                <%--<section> 
                                    <label class="label" style="color:#3276b1;">Fecha de nacimiento <a style="color: red">*</a></label>
                                    <div class='input-group date' id='fechadatetimepicker'>
                                        <input type="text" class="form-control" name="Fecha" id="fechaNacimiento" placeholder="Fecha de nacimiento" data-requerido="true"/>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </section>--%>
                                <section>
                                    <label style="color:#3276b1;">Sexo <a style="color: red">*</a></label>
                                    <label class="select">
                                        <select name="rol" id="sexo" runat="server">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelsexomodal"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save" id="btncontinuarsexo"><i class="fa fa-save"></i>&nbsp;Continuar </a>                            
                        </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Confirmar cambiar edad y sexo</h4>
                        </div>
                        <div class="modal-body">
                            <div class="smart-form">
                                <fieldset>
                                    <div class="row">
                                        <section>
                                            <p class="center">¿Está seguro de cambiar la información del detenido?</p>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelconfirm"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                                    <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                    <a class="btn btn-sm btn-default save" id="btnconfirm"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <div id="blockuser-modal" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de<strong>&nbsp<span id="usernameblock"></span></strong>?</p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control alptext" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control alptext" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img  width="240" height="240" align="center" class="img-thumbnail same-height-thumbnail" id="imgInfo" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="lesion-modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal-title"><i class="fa fa-pencil" +=""></i> Evidencia fotográfica</h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-serie" class="smart-form">
                        <div class="row">
                            <fieldset>
                                <section>
                                    <label style="color: dodgerblue" class="input">Evidencia fotográfica <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-file-image-o"></i>
                                        <asp:FileUpload ID="foto" runat="server"/>
                                    </label>
                                </section>
                                <section>
                                    <label class="input" style="color: dodgerblue">Descripción <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" id="descripcionLesion" placeholder="Descripción" maxlength="255" class="alptext"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese la descripción</b>
                                    </label>
                                </section>
                            </fieldset>
                        </div>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelModalLesion"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default" id="btnSaveModalLesion"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            <input type="hidden" id="trackingid" runat="server" />
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="deleteFotografia-modal" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">¿Está seguro que desea <strong><span>eliminar</span></strong> la fotografía?</p>
                            </section>
                        </fieldset>
                        <footer>
                            <input type="hidden" id="fotografiaDelete"/>
                            <a class="btn btn-sm btn-default" id="btnEliminarFotografiaCancel"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btnEliminarFotografia"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="photo-lesion-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Fotografía</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_lesion" class="img-thumbnail text-center" src="<%= ConfigurationManager.AppSettings["relativepath"]  %> #" alt="fotografía de la lesión" /> 
                                </div> 
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="tracking" runat="server" />
    <asp:HiddenField ID="ExamenTracking" runat="server" />
    <asp:HiddenField ID="PruebaTracking" runat="server" />
    <asp:HiddenField ID="trackingUser" runat="server" />
         <input type="hidden" id="Max" value="0" />
        <input type="hidden" id="Min" value="0" />
        <input type="hidden" id="Validar1"  />
        <input type="hidden" id="Validar2"  />
    <input type="hidden" id="FechaRegistro" />
    <input type="hidden"id="idHistoryT"  />
    <input type="hidden" id="idHistoryL" />
    <input type="hidden" id="idhistoryLesion" />
    <input type="hidden" id="serviciotrackingid" />
    <input type="hidden" id="certLesionFotografias" />
    <input type="hidden" id="rolid" runat="server" value="" />
    <input type="hidden" id="sexoId" runat="server" value="" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="validarfecha" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js"></script>

    <script type="text/javascript">
        var responsiveHelper_dt_basiclesionesFotografias = undefined;

        pageSetUp();
        init();

        function init() {
            if ($("#ctl00_contenido_tracking").val() != "") {
                CargarDatos($("#ctl00_contenido_tracking").val());
            }

            
            var param = RequestQueryString("action");
            $("#architec").prop("checked", true);

            if (param == "update") {
                $('#addtatuaje').removeClass('disabled');
                $('#addlesion').removeClass('disabled');
                $('#addintoxicacion').removeClass('disabled');
                CargarDatosExamen();
            }
            else {
                $("#Folio").val("Q");

                CargarListado(0, "mucosas");
                CargarListado(0, "aliento");
                CargarListado(0, "neurologico");
                CargarListado(0, "tipo_examen");
                CargarListado(0, "conjuntivas");
                CargarListado(0, "marcha");
                CargarListado(0, "pupilas");
                CargarListado(0, "coordinacion");
                CargarListado(0, "pupilares");
                CargarListado(0, "osteo");
                CargarListado(0, "romberq");
                CargarListado(0, "conducta");
                CargarListado(0, "lenguaje");
                CargarListado(0, "atencion");
                CargarListado(0, "orientacion");
                CargarListado(0, "diadococinencia");
                CargarListado(0, "ojos_abiertos_dedo_dedo");
                CargarListado(0, "talon");
                CargarListado(0, "ojos_abiertos_dedo_nariz");
                CargarListado(0, "ojos_cerrados_dedo_dedo");
                CargarListado(0, "ojos_cerrados_dedo_nariz");
                CargarListado(0, "tipo_lesion");
                CargarListado(0, "tipo_alergia");
                CargarListado(0, "tipo_tatuaje");
                CargarListado(0, "antecedentes");
                CargarListado(0, "conclusion");
                CargarListado(0, "idactitud");
                CargarListado(0, "cavidadoral");
                CargarListado(0, "Etanol");
                CargarListado(0, "Benzodiazepina");
                CargarListado(0, "anfetamina");
                CargarListado(0, "cannabis");
                CargarListado(0, "cocaina");
                CargarListado(0, "extasis");
                CargarListado(0, "equipo_a_utilizar");
                loadDate();

                setTimeout(BloquearControles(), 500);
            }

            //Cargar opciones select
            $("#novalorabe").change(function () {
                if ($("#novalorabe").is(":checked")) {
                    $("#ta").prop('disabled', true);
                    $("#fc").prop('disabled', true);
                    $("#fr").prop('disabled', true);
                    $("#pulso").prop('disabled', true);
                }
                else {
                    $("#ta").prop('disabled', false);
                    $("#fc").prop('disabled', false);
                    $("#fr").prop('disabled', false);
                    $("#pulso").prop('disabled', false);
                }
            });

            $("#ctl00_contenido_Etanol").change(function () {
                if ($('#ctl00_contenido_Etanol option:selected').html().toUpperCase() == "POSITIVO") {
                    $("#gradosection").show();
                    $("#mgdlsection").show();
                }
                else {
                    $("#gradosection").hide();
                    $("#mgdlsection").hide();
                }
            });

            $("#btncancelsexomodal").parent().css("margin", "10px");
        }

        $('#ctl00_contenido_tipo_lesion').on('select2:opening', function (evt) {
            if (this.disabled) {
                return false;
            }
        });

        $('#ctl00_contenido_orientacion').on('select2:opening', function (evt) {
            if (this.disabled) {
                return false;
            }
        });

        $('#ctl00_contenido_conclusion').on('select2:opening', function (evt) {
            if (this.disabled) {
                return false;
            }
        });

        $('#ctl00_contenido_tipo_alergia').on('select2:opening', function (evt) {
            if (this.disabled) {
                return false;
            }
        });

        $('#ctl00_contenido_tipo_tatuaje').on('select2:opening', function (evt) {
            if (this.disabled) {
                return false;
            }
        });

        $('#ctl00_contenido_antecedentes').on('select2:opening', function (evt) {
            if (this.disabled) {
                return false;
            }
        });

        function BloquearControles() {
            if ($("#ctl00_contenido_rolid").val() == "14") {
                $("#savecertificadolesion").hide();
                $("#savecertificadopsicofisiologico").hide();
                //$("#s0").children.prop('disabled', true);
                $("#serviciotrackingid").attr("disabled", "disabled");
                $('#ctl00_contenido_tracking').attr("disabled", "disabled");
                $('#ctl00_contenido_tracking').attr("disabled", "disabled");
                $('#ctl00_contenido_tipo_lesion').attr("disabled", "disabled");
                $('#observacioneslesion ').attr("disabled", "disabled");
                $('#sinlesion').attr("disabled", "disabled");
                $('#Lesion_visible').attr("disabled", "disabled");
                $('#fallecimiento').attr("disabled", "disabled");
                $('#enviohospital').attr("disabled", "disabled");
                $('#peligro').attr("disabled", "disabled");
                $('#consecuencias').attr("disabled", "disabled");
                $('#ctl00_contenido_tipo_alergia').attr("disabled", "disabled");
                $('#observacionesalergia').attr("disabled", "disabled");
                $('#ctl00_contenido_tipo_tatuaje').attr("disabled", "disabled");
                $('#observacionesTatuaje').attr("disabled", "disabled");
                $('#observacionesGlobales').attr("disabled", "disabled");
                $('#ctl00_contenido_antecedentes').attr("disabled", "disabled");
                $('#observacionesAntecedente').attr("disabled", "disabled");
                ///certifiado psicofiológico
                $("#serviciotrackingid").attr("disabled", "disabled");
                $('#ctl00_contenido_tracking').attr("disabled", "disabled");
                $('#ctl00_contenido_tracking').attr("disabled", "disabled");
                $('#ctl00_contenido_aliento').attr("disabled", "disabled");
                $('#ctl00_contenido_marcha').attr("disabled", "disabled");
                $('#ctl00_contenido_idactitud').attr("disabled", "disabled");
                $("#ctl00_contenido_atencion").attr("disabled", "disabled");
                $('#ctl00_contenido_cavidadoral').attr("disabled", "disabled");
                $("#ctl00_contenido_pupilas").attr("disabled", "disabled");
                $('#ctl00_contenido_pupilares').attr("disabled", "disabled");
                $('#ctl00_contenido_conjuntivas').attr("disabled", "disabled");
                $('#ctl00_contenido_lenguaje').attr("disabled", "disabled");
                $('#ctl00_contenido_romberq').attr("disabled", "disabled");
                $('#ctl00_contenido_ojos_abiertos_dedo_dedo').attr("disabled", "disabled");
                $('#ctl00_contenido_ojos_cerrados_dedo_dedo').attr("disabled", "disabled");
                $('#ctl00_contenido_ojos_abiertos_dedo_nariz').attr("disabled", "disabled");
                $('#ctl00_contenido_ojos_cerrados_dedo_nariz').attr("disabled", "disabled");
                $('#ta').attr("disabled", "disabled");
                $('#fc').attr("disabled", "disabled");
                $('#fr').attr("disabled", "disabled");
                $('#pulso').attr("disabled", "disabled");
                $('#novalorabe').attr("disabled", "disabled");
                $('#ctl00_contenido_orientacion').attr("disabled", "disabled");
                $('#ctl00_contenido_conclusion').attr("disabled", "disabled");
                $('#sustento').attr("disabled", "disabled");
                $('#observaciones').attr("disabled", "disabled");
                $('#fechahora3').attr("disabled", "disabled");
                $('#FehaHoradatetimepicker').attr("disabled", "disabled");
                $('#fechahora4').attr("disabled", "disabled");
                $('#FehaHoradatetimepicker4').attr("disabled", "disabled");
                $('#addLesion').hide();
            }
            else if ($("#ctl00_contenido_rolid").val() == "6") {
                $("#savecertificadoquimico").hide();
                $('#Folio').attr("disabled", "disabled");
                $('#fechahora').attr("disabled", "disabled");
                $('#fechahora2 ').attr("disabled", "disabled");
                $('#ctl00_contenido_Etanol').attr("disabled", "disabled");
                $("#grado").attr("disabled", "disabled");
                $('#ctl00_contenido_Benzodiazepina').attr("disabled", "disabled");
                $("#ctl00_contenido_anfetamina").attr("disabled", "disabled");
                $('#ctl00_contenido_cannabis').attr("disabled", "disabled");
                $('#ctl00_contenido_cocaina').attr("disabled", "disabled");
                $('#ctl00_contenido_extasis').attr("disabled", "disabled");
                $('#ctl00_contenido_equipo_a_utilizar').attr("disabled", "disabled");
                $('#printetanol').attr("disabled", "disabled");
                $('#printbenzodiazepina').attr("disabled", "disabled");
                $('#printanfetamina').attr("disabled", "disabled");
                $('#printcannabis').attr("disabled", "disabled");
                $('#printcocaina').attr("disabled", "disabled");
                $('#printextasis').attr("disabled", "disabled");
                $('#addLesion').show();
            }
        }

        $("#ctl00_contenido_foto").change(function () {
            var s = this.files[0].size;
            var n = (s / 1024);
            var max = $("#Max").val() * 1024;
            var min = $("#Min").val() * 1024;
            if (max > 0) {
                if (n > max) {
                    $("#Validar1").val(false);
                }
                else {
                    $("#Validar1").val(true);
                }
            }
            else {
                $("#Validar1").val(true);
            }
            if (min > 0) {
                if (n < min) {
                    $("#Validar2").val(false);
                }
                else {
                    $("#Validar2").val(true);
                }
            }
            else {
                $("#Validar2").val(true);
            }

            //alert($("#Validar1").val() + ' </br> ' + $("#Validar2").val());

        });

        GetMinMax();
        function validatefecha(info) {
            var valido = true;
            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/ValidateFecha",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async:false,
                data: JSON.stringify({
                    info: info,
                }),
                cache: false,

                success: function (data) {
                    var resultado = JSON.parse(data.d);

                    if (resultado.exitoso) {
                        
                        if (resultado.Validar == false) {
                            valido = false;
                        }
                        var fecha = resultado.fechaRegistro;
                        $("#FechaRegistro").val(fecha);
                        $("#validarfecha").val(valido);
                        
                    }
                    else {
                        $('#main').waitMe('hide');

                        ShowError("¡Error! Algo salió mal", resultado.mensaje);
                    }
                    $('#main').waitMe('hide');
                }
            });
            return valido;
        }
        function GetMinMax() {
            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/GetParametrosTamaño",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,

                success: function (data) {
                    var resultado = JSON.parse(data.d);

                    if (resultado.exitoso) {
                        var max = resultado.TMax;
                        var min = resultado.Tmin;

                        $("#Max").val(max);
                        $("#Min").val(min);
                        // alert(resultado.rutaimagenfrontal);
                        //var imagenAvatar = ResolveUrl(resultado.rutaimagenfrontal);
                        //  imagenfrontalbiometricos = imagenAvatar;

                        //$('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                        //$("#ctl00_contenido_fileUpload1").val(imagenAvatar).clone(true);
                        $('#main').waitMe('hide');
                        //location.reload(true);
                        //CargarDatos(datos.TrackingId);



                    }
                    else {
                        $('#main').waitMe('hide');

                        ShowError("¡Error! Algo salió mal", resultado.mensaje);
                    }
                    $('#main').waitMe('hide');
                }
            });
        }

        $(document).on('keydown', 'input[pattern]', function (e) {
            var input = $(this);
            var oldVal = input.val();
            var regex = new RegExp(input.attr('pattern'), 'g');

            setTimeout(function () {
                var newVal = input.val();
                if (!regex.test(newVal)) {
                    input.val(oldVal);
                }
            }, 0);
        });

        /* Listado tatuajes*/
        var responsiveHelper_dt_basictatuajes = undefined;
        var breakpointAddDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        window.emptytabletatuajes = true;
        window.tabletatuajes = $('#dt_basictatuajes').dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basictatuajes) {
                    responsiveHelper_dt_basictatuajes = new ResponsiveDatatablesHelper($('#dt_basictatuajes'), breakpointAddDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basictatuajes.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basictatuajes.respond();
                $('#dt_basictatuajes').waitMe('hide');
            },
            "createdRow": function (row, data, index) {
                if (!data["Habilitado"]) {
                    $('td', row).eq(0).addClass('strikeout');
                    $('td', row).eq(1).addClass('strikeout');
                    $('td', row).eq(2).addClass('strikeout');
                    $('td', row).eq(3).addClass('strikeout');
                }
            },
            ajax: {
                type: "POST",
                url: "examen_medico.aspx/getDataTatuaje",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosServerSide) {
                    $('#dt_basictatuajes').waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });

                    parametrosServerSide.emptytableadd = false;
                    parametrosServerSide.tracking = $("#ctl00_contenido_ExamenTracking").val();
                    parametrosServerSide.tipo = "tatuaje";
                    return JSON.stringify(parametrosServerSide);
                }
            },
            columns: [
                null,
                null,
                {
                    name: "Tipo",
                    data: "Tipo",
                    orderable: false
                },
                {
                    name: "Lugar",
                    data: "Lugar",
                    orderable: false
                },
                {
                    name: "Observacion",
                    data: "Observacion",
                    orderable: false
                },
                null,
            ],
            columnDefs: [
                {
                    data: "TrackingId",
                    targets: 0,
                    orderable: false,
                    visible: false,
                    render: function (data, type, row, meta) {
                        return "";
                    }
                },
                {
                    targets: 1,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var edit = "edit";
                        var editar = "";
                        var habilitar = "";
                        var icon = "";
                        var color = "";
                        var txtestatus = "";
                        if (row.Habilitado) {
                            txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                        }
                        else {
                            txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                        }

                        if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' edittatuaje" + href="javascript:void(0);" ' + '" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipoTatuajeId + '"data-lugar= "' + row.Lugar + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"data-clasificacion = tatuaje title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;';
                        if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a data-clasificacion = tatuaje class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipoTatuajeId + '"data-lugar= "' + row.Lugar + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                        return editar + habilitar
                            + '<a name="history" class="btn btn-default btn-circle historialT" id="Historialtatuaje" href="javascript:void(0);" data-value="' + row.Lugar + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                    }
                }
            ]
        });

        /* Fin listado tatuajes*/

        /* Listado lesiones*/
        var responsiveHelper_dt_basiclesiones = undefined;
        var breakpointAddDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        window.emptytablelesiones = true;
        window.tablelesiones = $('#dt_basiclesiones').dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basiclesiones) {
                    responsiveHelper_dt_basiclesiones = new ResponsiveDatatablesHelper($('#dt_basiclesiones'), breakpointAddDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basiclesiones.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basiclesiones.respond();
                $('#dt_basiclesiones').waitMe('hide');
            },
            "createdRow": function (row, data, index) {
                if (!data["Habilitado"]) {
                    $('td', row).eq(0).addClass('strikeout');
                    $('td', row).eq(1).addClass('strikeout');
                    $('td', row).eq(2).addClass('strikeout');
                    $('td', row).eq(3).addClass('strikeout');
                }
            },
            ajax: {
                type: "POST",
                url: "examen_medico.aspx/getDataLesion",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosServerSide) {
                    $('#dt_basiclesiones').waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });

                    parametrosServerSide.emptytableadd = false;
                    parametrosServerSide.tracking = $("#ctl00_contenido_ExamenTracking").val();
                    parametrosServerSide.tipo = "lesion";
                    return JSON.stringify(parametrosServerSide);
                }
            },
            columns: [
                null,
                null,
                {
                    name: "Tipo",
                    data: "Tipo",
                    orderable: false
                },
                {
                    name: "Lugar",
                    data: "Lugar",
                    orderable: false
                },
                {
                    name: "Observacion",
                    data: "Observacion",
                    orderable: false
                },
                null,
            ],
            columnDefs: [
                {
                    data: "TrackingId",
                    targets: 0,
                    orderable: false,
                    visible: false,
                    render: function (data, type, row, meta) {
                        return "";
                    }
                },
                {
                    targets: 1,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var edit = "edit";
                        var editar = "";
                        var habilitar = "";
                        var icon = "";
                        var color = "";
                        var txtestatus = "";
                        if (row.Habilitado) {
                            txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                        }
                        else {
                            txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                        }

                        if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' editlesion" + href="javascript:void(0);" ' + '" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipoLesionId + '"data-lugar= "' + row.Lugar + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"data-clasificacion = lesion title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;';
                        if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a data-clasificacion = lesion class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipoLesionId + '"data-lugar= "' + row.Lugar + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                        return editar + habilitar + '<a name="history" class="btn btn-default btn-circle historialLes" href="javascript:void(0);" data-value="' + row.Lugar + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                    }
                }
            ]
        });

        /* Fin listado lesiones*/
        var responsiveHelper_dt_basichistoryLesion = undefined;
        var breackpointHistoryLesDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        window.emptytablehistoryles = true;
        window.tablehistoryLes = $("#dt_basichistoryLesion").dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            order: [[3, 'asc']],
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basichistoryLesion) {
                    responsiveHelper_dt_basichistoryLesion = new ResponsiveDatatablesHelper($("#dt_basichistoryLesion"), breackpointHistoryLesDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basichistoryLesion.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basichistoryLesion.respond();
                $("#dt_basichistoryLesion").waitMe('hide');
            },
            ajax: {
                type: "POST",
                url: "examen_medico.aspx/GetLesionLog",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosserverside) {
                    $("#dt_basichistoryLesion").waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });
                    var centroid = $('#idhistoryLesion').val() || "0";
                    parametrosserverside.centroid = centroid;
                    parametrosserverside.todoscancelados = false;
                    parametrosserverside.emptytable = emptytablehistoryles;
                    return JSON.stringify(parametrosserverside);
                }
            },
            columns: [
                {
                    name: "Lugar",
                    data: "Lugar",
                    ordertable: false
                },
                {
                    name: "Accion",
                    data: "Accion",
                    ordertable: false
                },

                {
                    name: "Creadopor",
                    data: "Creadopor",
                    ordertable: false
                }
                ,
                {
                    name: "Fec_Movto",
                    data: "Fec_Movto",
                    ordertable: false
                }
            ],
        });

        var responsiveHelper_dt_basichistoryIntoxicaciones = undefined;
        var breackpontHistoryIntoxDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        /* Listado intoxicaciones*/
        var responsiveHelper_dt_basicintoxicaciones = undefined;
        var breakpointAddDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        $("body").on("click", ".historialLes", function () {
            var id = $(this).attr("data-id");
            $("#idhistoryLesion").val(id);
            //var descripcion = $(this).attr("data-value");
            //$("#descripcionhistorialLesion").text(descripcion);
            $("#historyLesion-modal").modal("show");
            window.emptytablehistoryles = false;
            window.tablehistoryLes.api().ajax.reload();
        });

        $("body").on("click", ".historialIntox", function () {
            var id = $(this).attr("data-id");
            $("#idHistoryL").val(id);
            //var descripcion = $(this).attr("data-value");
            //$("#descripcionhistorialintoxicacion").text(descripcion);
            $("#historyIntoxicacion-modal").modal("show");
            window.emptytablehistoryintx = false;
            window.tablehistoryintox.api().ajax.reload();
        });

        $('#sinlesion').click(function () {
            if ($(this).is(':checked')) {
                $('#Lesion_visible').attr("disabled", "disabled");
                $('#fallecimiento').attr("disabled", "disabled");
                $('#enviohospital').attr("disabled", "disabled");
                $("#Lesion_visible").prop("checked", false);
                $("#fallecimiento").prop("checked", false);
                $("#enviohospital").prop("checked", false);
                $("#ctl00_contenido_tipo_lesion").attr("disabled", "disabled");
             
                $("#ctl00_contenido_tipo_lesion").val(null).trigger('change');
                $('#observacioneslesion').attr("disabled", "disabled");
                $('#observacioneslesion').val("");

            }
            else {
                $('#Lesion_visible').removeAttr("disabled");
                $('#fallecimiento').removeAttr("disabled");
                $('#enviohospital').removeAttr("disabled");
                $('#ctl00_contenido_tipo_lesion').removeAttr("disabled");
                $('#observacioneslesion').removeAttr("disabled");
            }
        });
       
        window.emptytableintoxicaciones = true;
        window.tableintoxicaciones = $('#dt_basicintoxicaciones').dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basicintoxicaciones) {
                    responsiveHelper_dt_basicintoxicaciones = new ResponsiveDatatablesHelper($('#dt_basicintoxicaciones'), breakpointAddDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basicintoxicaciones.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basicintoxicaciones.respond();
                $('#dt_basicintoxicaciones').waitMe('hide');
            },
            "createdRow": function (row, data, index) {
                if (!data["Habilitado"]) {
                    $('td', row).eq(0).addClass('strikeout');
                    $('td', row).eq(1).addClass('strikeout');
                    $('td', row).eq(2).addClass('strikeout');
                    $('td', row).eq(3).addClass('strikeout');
                }
            },
            ajax: {
                type: "POST",
                url: "examen_medico.aspx/getDataIntoxicacion",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosServerSide) {
                    $('#dt_basicintoxicaciones').waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });

                    parametrosServerSide.emptytableadd = false;
                    parametrosServerSide.tracking = $("#ctl00_contenido_ExamenTracking").val();
                    return JSON.stringify(parametrosServerSide);
                }
            },
            columns: [
                null,
                null,
                {
                    name: "Tipo",
                    data: "Tipo",
                    orderable: false
                },
                {
                    name: "Observacion",
                    data: "Observacion",
                    orderable: false
                },
                null,
            ],
            columnDefs: [
                {
                    data: "TrackingId",
                    targets: 0,
                    orderable: false,
                    visible: false,
                    render: function (data, type, row, meta) {
                        return "";
                    }
                },
                {
                    targets: 1,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    targets: 4,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var edit = "edit";
                        var editar = "";
                        var habilitar = "";
                        var icon = "";
                        var color = "";
                        var txtestatus = "";
                        if (row.Habilitado) {
                            txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                        }
                        else {
                            txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                        }

                        if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' editintoxicacion" + href="javascript:void(0);" ' + '" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipoIntoxicacionId + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"data-clasificacion = intoxicacion title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;';
                        if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a data-clasificacion = intoxicacion class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipointoxicacionId + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                        return editar + habilitar + '<a name="history" class="btn btn-default btn-circle historialIntox" href="javascript:void(0);" data-value="' + row.Observacion + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                    }
                }
            ]
        });

        /* Fin listado intoxicaciones*/
        window.emptytablehistoryintx = true;
        window.tablehistoryintox = $("#dt_basichistoryintoxicacion").dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basichistoryIntoxicaciones) {
                    responsiveHelper_dt_basichistoryIntoxicaciones = new ResponsiveDatatablesHelper($("#dt_basichistoryintoxicacion"), breackpontHistoryIntoxDefinition);
                }

            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basichistoryIntoxicaciones.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basichistoryIntoxicaciones.respond();
                $("#dt_basichistoryintoxicacion").waitMe("hide");
            },
            ajax: {
                type: "POST",
                url: "examen_medico.aspx/GetIntoxicacionLog",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosserverside) {
                    $("#dt_basichistoryintoxicacion").waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });
                    var centroid = $('#idHistoryL').val() || "0";
                    parametrosserverside.centroid = centroid;
                    parametrosserverside.todoscancelados = false;
                    parametrosserverside.emptytable = emptytablehistoryintx;
                    return JSON.stringify(parametrosserverside);
                }
            },
            columns: [
                {
                    name: "Observacion",
                    data: "Observacion",
                    ordertable: false
                },
                {
                    name: "Accion",
                    data: "Accion",
                    ordertable: false
                },

                {
                    name: "Creadopor",
                    data: "Creadopor",
                    ordertable: false
                }
                ,
                {
                    name: "Fec_Movto",
                    data: "Fec_Movto",
                    ordertable: false
                }
            ],

        });

        var responsiveHelper_dt_basichistorytatuajes=undefined;
        var breakpontHistoryTatuajesDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        window.emptytablehistory = true;
        window.tablehistory = $("#dt_basichistorytatuajes").dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            order: [[3, 'asc']],
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basichistorytatuajes) {
                    responsiveHelper_dt_basichistorytatuajes = new ResponsiveDatatablesHelper($("#dt_basichistorytatuajes"), breakpontHistoryTatuajesDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basichistorytatuajes.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basichistorytatuajes.respond();
                $("#dt_basichistorytatuajes").waitMe("hide");
            },
            ajax: {
                type: "POST",
                url: "examen_medico.aspx/GetTatuajeLog",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosserverside) {
                    $("#dt_basichistorytatuajes").waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });
                    var centroid = $('#idHistoryT').val() || "0";
                    parametrosserverside.centroid = centroid;
                    parametrosserverside.todoscancelados = false;
                    parametrosserverside.emptytable = emptytablehistory;
                    return JSON.stringify(parametrosserverside);
                }
            },
            columns: [
                {
                    name: "Lugar",
                    data: "Lugar",
                    ordertable: false
                },
                {
                    name: "Accion",
                    data: "Accion",
                    ordertable: false
                },
                {
                    name: "Creadopor",
                    data: "Creadopor",
                    ordertable: false
                },
                {
                    name: "Fec_Movto",
                    data: "Fec_Movto",
                    ordertable: false
                }
            ],
        });

        $("body").on("click", ".historialT", function () {
            var id = $(this).attr("data-id");
            $('#idHistoryT').val(id);
            //var descripcion = $(this).attr("data-value");
            //$("#descripcionhistorialtatuajes").text(descripcion);
            $("#historyTatuajes-modal").modal("show");
            window.emptytablehistory = false;
            window.tablehistory.api().ajax.reload();
        });

        $("body").on("click", ".blockuser", function () {
            var usernameblock = $(this).attr("data-lugar");
            var verb = $(this).attr("style");
            $("#usernameblock").text(usernameblock);
            $("#verb").text(verb);
            $("#btncontinuar").attr("data-tracking", $(this).attr("data-tracking"));
            $("#btncontinuar").attr("data-clasificacion", $(this).attr("data-clasificacion"));
            $("#blockuser-modal").modal("show");
        });

        $("#btncontinuar").unbind("click").on("click", function () {
            var datos = {       
                    TrackingId : $("#btncontinuar").attr("data-tracking"),                         
                    Clasificacion: $("#btncontinuar").attr("data-clasificacion"),                                      
                };                
                    bloquear(datos);                
            });

        $("#tabexamen").unbind("click").on("click", function () {
            $('#tabexamen').parent().addClass('active');
            $("#refexamen").attr("aria-expanded", "true");
            $("#refpruebas").attr("aria-expanded", "false");
            $('#tabpruebas').parent().removeClass('active');
        });

        $("#tabpruebas").unbind("click").on("click", function () {
            $('#tabpruebas').parent().addClass('active');
            $("#refexamen").attr("aria-expanded", "false");
            $("#refpruebas").attr("aria-expanded", "true");
            $('#tabexamen').parent().removeClass('active');
        });

        $('#fechahora').datetimepicker({
            format: 'DD/MM/YYYY HH:mm:ss'
        });
        if ($("#ctl00_contenido_rolid").val() != "6") {
            $('#FehaHoradatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss'
            });
            $('#FehaHoradatetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss'
            });
           
        }
        if ($("#ctl00_contenido_rolid").val() != "14") {
            $('#FehaHoradatetimepicker3').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss'
            });
             $('#FehaHoradatetimepicker4').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss'
            });
        }
        $("body").on("click", ".savecertificadopsicofisiologico", function () {

            if (validarPsicofisiologico())
            {
                guardarCertificadopisco();
            }
        });

          $("body").on("click", ".saveexamen", function () {
                
                if (validarExamen()) {
                    guardarExamen();
              }
              $("#ctl00_contenido_lblMessage").html("");
        }); 
        //savecertificadoquimico


        $("body").on("click", "#savecertificadolesion", function () {
           
            if (validarCertificadoLesion()) {
                guardarCertificadoLesion();
            }

        });
        function ObtenerValoresCertiLesion() {
            var examen = {
                Id: "",
                ServiciomedicoTrackingId: $("#serviciotrackingid").val(),
                TrackingId: $('#ctl00_contenido_tracking').val(),
                DetalledetencionId: $('#ctl00_contenido_tracking').val(),
                Tipo_lesionId: $('#ctl00_contenido_tipo_lesion').val(),
                Observaciones_lesion: $('#observacioneslesion ').val(),
                Lesion_simple_vista: $('#Lesion_visible').is(':checked'),
                Fallecimiento: $('#fallecimiento').is(':checked'),
                Envio_hospital: $('#enviohospital').is(':checked'),
                Peligro_de_vida: $('#peligro').is(':checked'),
                Consecuencias_medico_legales: $('#consecuencias').is(':checked'),
                Tipo_alergiaId: $('#ctl00_contenido_tipo_alergia').val(),
                Observacion_alergia: $('#observacionesalergia').val(),
                Tipo_tatuajeId: $('#ctl00_contenido_tipo_tatuaje').val(),
                Observacion_tatuje: $('#observacionesTatuaje').val(),
                Observacion_general: $('#observacionesGlobales').val(),
                AntecedentesId: $('#ctl00_contenido_antecedentes').val(),
                Observaciones_antecedentes: $('#observacionesAntecedente').val(),
                Sinlesion: $('#sinlesion').is(':checked'),
                FotografiasId: $("#certLesionFotografias").val(),
                Fechavaloracion:$("#fechahora4").val()
            };
            return examen;
        }

        //$('#ctl00_contenido_tipo_lesion').attr("disabled","disabled");
        //$("#ctl00_contenido_email").focusout(function () {
        //}).blur(function () {

        //    alert("adios lesion");
        // });

        $("#ctl00_contenido_tipo_lesion").change(function () {

            if ($(this).val() == null) {
                $('#observacioneslesion').attr("disabled", "disabled");
                $('#observacioneslesion').val("");
            }
            else
            {
                $('#observacioneslesion').removeAttr("disabled");
            }

        });

         $("#ctl00_contenido_tipo_alergia").change(function () {

            if ($(this).val() == null) {
                $('#observacionesalergia').attr("disabled", "disabled");
                $('#observacionesalergia').val("");
            }
            else
            {
                $('#observacionesalergia').removeAttr("disabled");
            }

        });

         $("#ctl00_contenido_antecedentes").change(function () {

            if ($(this).val() == null) {
                $('#observacionesAntecedente').attr("disabled", "disabled");
                $('#observacionesAntecedente').val("");
            }
            else
            {
                $('#observacionesAntecedente').removeAttr("disabled");
            }

        });

           $("#ctl00_contenido_tipo_tatuaje").change(function () {

            if ($(this).val() == null) {
                $('#observacionesTatuaje').attr("disabled", "disabled");
                $('#observacionesTatuaje').val("");
            }
            else
            {
                $('#observacionesTatuaje').removeAttr("disabled");
            }

        });
        function validarCertificadoLesion() {
            var esvalido = true;

              if ($("#fechahora4").val().split(" ").join("") == "") {
                    ShowError("Fecha y hora de valoración", "El campo fecha y hora de valoración es obligatorio.");
                    //$('#fechahora3').parent().removeClass('state-success').addClass("state-error");
                    //$('#fechahora3').removeClass('valid');
                    esvalido = false;
                }
              else {
                  info = [
                      tracking = $("#ctl00_contenido_tracking").val(),
                      fecha = $("#fechahora4").val()
                  ];
                  var valido = validatefecha(info);

                  valido = $("#validarfecha").val();
                  if (valido == "false") {

                      var f = $("#FechaRegistro").val();
                      ShowError("Fecha y hora de valoración", "La fecha y hora de valoración no puede ser menor a la fecha " + f + ".");
                      //$('#fechahora3').parent().removeClass('state-success').addClass("state-error");
                      //$('#fechahora3').removeClass('valid');
                      esvalido = false;
                  }
                    //$('#fechahora3').parent().removeClass("state-error").addClass('state-success');
                    //$('#fechahora3').addClass('valid');
                }

            if ($('#Lesion_visible').is(':checked') || $('#fallecimiento').is(':checked') || $('#enviohospital').is(':checked')) {
                if ($('#ctl00_contenido_tipo_lesion').val() == null) {
                    ShowError("Lesión", "El campo lesión es obligatorio.");
                    // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
                }
                if ($("#observacioneslesion").val().split(" ").join("") == "") {
                    ShowError("Observacion ", "El campo observación de lesiones es obligatorio.");
                    //$('#observacioneslesion').parent().removeClass('state-success').addClass("state-error");
                    //$('#observacioneslesion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#observacioneslesion').parent().removeClass("state-error").addClass('state-success');
                    //$('#observacioneslesion').addClass('valid');
                }
            }
            else {
                if ($('#ctl00_contenido_tipo_lesion').val() != null) {
                    if ($("#observacioneslesion").val().split(" ").join("") == "") {
                        ShowError("Observacion ", "El campo observación de lesiones es obligatorio.");
                        //$('#observacioneslesion').parent().removeClass('state-success').addClass("state-error");
                        //$('#observacioneslesion').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        //$('#observacioneslesion').parent().removeClass("state-error").addClass('state-success');
                        //$('#observacioneslesion').addClass('valid');
                    }

                }
            }
                if ($('#ctl00_contenido_tipo_alergia').val() != null)
                {
                    if ($("#observacionesalergia").val().split(" ").join("") == "") {
                    ShowError("Observacion ", "El campo observación de alergia es obligatorio.");
                    //$('#observacionesalergia').parent().removeClass('state-success').addClass("state-error");
                    //$('#observacionesalergia').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#observacionesalergia').parent().removeClass("state-error").addClass('state-success');
                    //$('#observacionesalergia').addClass('valid');
                }
                   
                }

            if ($('#ctl00_contenido_antecedentes').val() != null)
                {
                    if ($("#observacionesAntecedente").val().split(" ").join("") == "") {
                    ShowError("Observacion ", "El campo observación de antecedentes es obligatorio.");
                    //$('#observacionesAntecedente').parent().removeClass('state-success').addClass("state-error");
                    //$('#observacionesAntecedente').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#observacionesAntecedente').parent().removeClass("state-error").addClass('state-success');
                    //$('#observacionesAntecedente').addClass('valid');
                }
                   
            }

            if ($('#ctl00_contenido_tipo_tatuaje').val() != null)
                {
                    if ($("#observacionesTatuaje").val().split(" ").join("") == "") {
                    ShowError("Observacion ", "El campo observación de tatuajes es obligatorio.");
                    //$('#observacionesTatuaje').parent().removeClass('state-success').addClass("state-error");
                    //$('#observacionesTatuaje').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#observacionesTatuaje').parent().removeClass("state-error").addClass('state-success');
                    //$('#observacionesTatuaje').addClass('valid');
                }
                   
                }

            

        

            

            return esvalido
        }

            function guardarCertificadoLesion() {
                startLoading();
                var examen = ObtenerValoresCertiLesion();
               
                $.ajax({
                    type: "POST",
                    url: "examen_medico.aspx/savecertificadoLesion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'examen': examen }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $("#observacion").val(resultado.Observacion);
                            $('#ctl00_contenido_ExamenTracking').val(resultado.ServicioMedTracingId);
                            $("#serviciotrackingid").val(resultado.ServicioMedTracingId);
                             var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            if (data.d.ishit) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Atención! </strong>" +
                                    data.d.msghit, "<br /></div>");
                                ShowAlert("¡Atencion!", data.d.msghit)
                                setTimeout(hideMessage, hideTime);
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                    "" + resultado.mensaje + " ", "<br /></div>");
                                setTimeout(hideMessage, hideTime);
                                ShowSuccess("¡Bien hecho!", "  " + resultado.mensaje);
                                
                                $("#serviciotrackingid").val(resultado.ServicioMedTracingId);
                            }
                            $('#addtatuaje').removeClass('disabled');
                            $('#addlesion').removeClass('disabled');
                            $('#addintoxicacion').removeClass('disabled');
                            $('#ocular').parent().removeClass('state-success');
                            $('#indicaciones').parent().removeClass('state-success');
                            $('#observacion').parent().removeClass('state-success');
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
        }

          $("body").on("click", "#savecertificadoquimico", function () {

            if (validarCertificadoquimico())
            {
                guardarCertificadoQuimico();
            }
        });

        function validarCertificadoquimico()
        {
            var esvalido = true;

            if ($("#Folio").val().split(" ").join("") == "") {
                    ShowError("Folio", "El campo folio es obligatorio.");
                    //$('#Folio').parent().removeClass('state-success').addClass("state-error");
                    //$('#Folio').removeClass('valid');
                    esvalido = false;
                }
                else {
                     if ($("#Folio").val().split(" ").join("").length <6) {
                    ShowError("Folio", "El campo folio no tiene el formato correcto.");
                    //$('#Folio').parent().removeClass('state-success').addClass("state-error");
                    //$('#Folio').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#Folio').parent().removeClass("state-error").addClass('state-success');
                    //$('#Folio').addClass('valid');
                }
            }
             

            if ($("#fechahora").val().split(" ").join("") == "") {
                    ShowError("Fecha y hora de toma", "El campo fecha y hora de toma es obligatorio.");
                    //$('#fechahora').parent().removeClass('state-success').addClass("state-error");
                    //$('#fechahora').removeClass('valid');
                    esvalido = false;
                }
            else {
                info = [
                    tracking = $("#ctl00_contenido_tracking").val(),
                    fecha = $("#fechahora").val()
                ];
                var valido = validatefecha(info);

                valido = $("#validarfecha").val();
                if (valido == "false") {

                    var f = $("#FechaRegistro").val();
                    ShowError("Fecha y hora de toma", "La fecha y hora de toma no puede ser menor a la fecha " + f + ".");
                    //$('#fechahora3').parent().removeClass('state-success').addClass("state-error");
                    //$('#fechahora3').removeClass('valid');
                    esvalido = false;
                }
                    //$('#fechahora').parent().removeClass("state-error").addClass('state-success');
                    //$('#fechahora').addClass('valid');
                }

            if ($("#fechahora2").val().split(" ").join("") == "") {
                    ShowError("Fecha y hora de proceso", "El campo fecha y hora de proceso es obligatorio.");
                    //$('#fechahora2').parent().removeClass('state-success').addClass("state-error");
                    //$('#fechahora2').removeClass('valid');
                    esvalido = false;
                }
            else {
                info = [
                    tracking = $("#ctl00_contenido_tracking").val(),
                    fecha = $("#fechahora2").val()
                ];
                var valido = validatefecha(info);

                valido = $("#validarfecha").val();
                if (valido == "false") {

                    var f = $("#FechaRegistro").val();
                    ShowError("Fecha y hora de proceso", "La fecha y hora de proceso no puede ser menor a la fecha " + f + ".");
                    //$('#fechahora3').parent().removeClass('state-success').addClass("state-error");
                    //$('#fechahora3').removeClass('valid');
                    esvalido = false;
                }
                    //$('#fecha2').parent().removeClass("state-error").addClass('state-success');
                    //$('#fecha2').addClass('valid');
                } 


            
                if ($('#ctl00_contenido_Etanol').val() == "0" || $('#ctl00_contenido_Etanol').val() == null) {
                    ShowError("Etanol", "El campo etanol es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
            
            if ($('#ctl00_contenido_Etanol option:selected').html().toUpperCase() == "POSITIVO")
            {
                 
                 if ($("#grado").val().split(" ").join("") == "") {
                    ShowError("Grado", "El campo grado es obligatorio.");
                    //$('#grado').parent().removeClass('state-success').addClass("state-error");
                    //$('#grado').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#grado').parent().removeClass("state-error").addClass('state-success');
                    //$('#grado').addClass('valid');
                } 

              }
           

             if ($('#ctl00_contenido_Benzodiazepina').val() == "0" || $('#ctl00_contenido_Benzodiazepina').val() == null) {
                    ShowError("Benzodiazepina", "El campo benzodiazepina es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_anfetamina').val() == "0" || $('#ctl00_contenido_anfetamina').val() == null) {
                    ShowError("Anfetamina", "El campo anfetamina es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

               if ($('#ctl00_contenido_cannabis').val() == "0" || $('#ctl00_contenido_cannabis').val() == null) {
                    ShowError("Cannabis", "El campo cannabis es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
        if ($('#ctl00_contenido_cocaina').val() == "0" || $('#ctl00_contenido_cocaina').val() == null) {
                    ShowError("Cocaína", "El campo cocaína es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
             if ($('#ctl00_contenido_extasis').val() == "0" || $('#ctl00_contenido_extasis').val() == null) {
                    ShowError("Éxtasis", "El campo éxtasis es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
               if ($('#ctl00_contenido_equipo_a_utilizar').val() == "0" || $('#ctl00_contenido_equipo_a_utilizar').val() == null) {
                    ShowError("Equipo a utilizar", "El campo equipo a utilizar es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            return esvalido;

        }

        function ObtenerValoresCertiQuimico() {                                
                var examen = {
                    
                    Id: "",
                    Folio:$("#Folio").val(),
                    ServiciomedicoTrackingId:$("#serviciotrackingid").val(),
                    TrackingId: $('#ctl00_contenido_tracking').val(),
                    DetalledetencionId:$('#ctl00_contenido_tracking').val(),
                    Fecha_toma : $('#fechahora').val(),
                    Fecha_proceso : $('#fechahora2 ').val(),
                    EtanolId : $('#ctl00_contenido_Etanol').val(),
                    Grado : $("#grado").val(),
                    BenzodiapinaId : $('#ctl00_contenido_Benzodiazepina').val(),
                    AnfetaminaId : $("#ctl00_contenido_anfetamina").val(),
                    CannabisId :  $('#ctl00_contenido_cannabis').val(),
                    CocaínaId : $('#ctl00_contenido_cocaina').val(),      
                    ExtasisId : $('#ctl00_contenido_extasis').val(),
                    EquipoautilizarId: $('#ctl00_contenido_equipo_a_utilizar').val(),
                    PrintEtanol: $('#printetanol').is(':checked'),
                    PrintBenzodiazepina: $('#printbenzodiazepina').is(':checked'),
                    PrintAnfetamina: $('#printanfetamina').is(':checked'),
                    PrintCannabis: $('#printcannabis').is(':checked'),
                    PrintCocaina: $('#printcocaina').is(':checked'),
                    PrintExtasis: $('#printextasis').is(':checked'),
                };
                return examen;
        }
        function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

        function guardarCertificadoQuimico() {
                startLoading();
                var examen = ObtenerValoresCertiQuimico();
               
                $.ajax({
                    type: "POST",
                    url: "examen_medico.aspx/savecertificadoquimico",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'examen': examen }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $("#observacion").val(resultado.Observacion);
                            $('#ctl00_contenido_ExamenTracking').val(resultado.TrackingId);

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            if (data.d.ishit) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Atención! </strong>" +
                                    data.d.msghit, "<br /></div>");
                                ShowAlert("¡Atencion!", data.d.msghit)
                                setTimeout(hideMessage, hideTime);
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                    "" + resultado.mensaje + "", "<br /></div>");
                                setTimeout(hideMessage, hideTime);
                                ShowSuccess("¡Bien hecho!", "  " + resultado.mensaje);
                                $("#Folio").val(resultado.Folio);
                                $("#serviciotrackingid").val(resultado.ServicioMedTracingId);
                            }
                            $('#addtatuaje').removeClass('disabled');
                            $('#addlesion').removeClass('disabled');
                            $('#addintoxicacion').removeClass('disabled');
                            $('#ocular').parent().removeClass('state-success');
                            $('#indicaciones').parent().removeClass('state-success');
                            $('#observacion').parent().removeClass('state-success');
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            
                            if (resultado.alert) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atencion! </strong>" +
                                    "Algo salió mal. " + resultado.mensaje + "</div>");
                                setTimeout(hideMessage, hideTime);
                                ShowAlert("¡Atención! ", resultado.mensaje);

                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                    "Algo salió mal. " + resultado.mensaje + "</div>");
                                setTimeout(hideMessage, hideTime);
                                ShowError("¡Error! Algo salió mal", resultado.mensaje);
                            }
                        }
                    }
                });
        }


        function validarPsicofisiologico() {                
            var esvalido = true;  
            if ($("#fechahora3").val().split(" ").join("") == "") {
                    ShowError("Fecha y hora de valoración", "El campo fecha y hora de valoración es obligatorio.");
                    //$('#fechahora3').parent().removeClass('state-success').addClass("state-error");
                    //$('#fechahora3').removeClass('valid');
                    esvalido = false;
                }
            else {
                info = [
                    tracking = $("#ctl00_contenido_tracking").val(),
                    fecha = $("#fechahora3").val()
                ];
                
                var valido = validatefecha(info);
                
                valido = $("#validarfecha").val();
                
                if (valido == "false") {
                    
                    var f = $("#FechaRegistro").val();
                    ShowError("Fecha y hora de valoración", "La fecha y hora de valoración no puede ser menor a la fecha "+f+".");
                    //$('#fechahora3').parent().removeClass('state-success').addClass("state-error");
                    //$('#fechahora3').removeClass('valid');
                    esvalido = false;
                }
                    //$('#fechahora3').parent().removeClass("state-error").addClass('state-success');
                    //$('#fechahora3').addClass('valid');
                } 

                if ($('#ctl00_contenido_aliento').val() == "0" || $('#ctl00_contenido_aliento').val() == null) {
                    ShowError("Aliento", "El campo aliento es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
                }

                   if ($('#ctl00_contenido_marcha').val() == "0" || $('#ctl00_contenido_marcha').val() == null) {
                    ShowError("Marcha", "El campo marcha es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_idactitud').val() == "0" || $('#ctl00_contenido_idactitud').val() == null) {
                    ShowError("Actitud", "El campo actitud es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
            if ($('#ctl00_contenido_atencion').val() == "0" || $('#ctl00_contenido_atencion').val() == null) {
                    ShowError("Atención", "El campo atención es obligatoria.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

             if ($('#ctl00_contenido_cavidadoral').val() == "0" || $('#ctl00_contenido_cavidadoral').val() == null) {
                    ShowError("Cavidad oral", "El campo cavidad oral es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }


              if ($('#ctl00_contenido_pupilas').val() == "0" || $('#ctl00_contenido_pupilas').val() == null) {
                    ShowError("Pupilas", "El campo pupilas es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

               if ($('#ctl00_contenido_pupilares').val() == "0" || $('#ctl00_contenido_pupilares').val() == null) {
                    ShowError("Discurso", "El campo discurso es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

                if ($('#ctl00_contenido_conjuntivas').val() == "0" || $('#ctl00_contenido_conjuntivas').val() == null) {
                    ShowError("Conjuntivas", "El campo conjuntivas es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

               if ($('#ctl00_contenido_lenguaje').val() == "0" || $('#ctl00_contenido_lenguaje').val() == null) {
                    ShowError("Lenguaje", "El campo lenguaje es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

              if ($('#ctl00_contenido_romberq').val() == "0" || $('#ctl00_contenido_romberq').val() == null) {
                    ShowError("Romberg", "El campo romberg  es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

               if ($('#ctl00_contenido_ojos_abiertos_dedo_dedo').val() == "0" || $('#ctl00_contenido_ojos_abiertos_dedo_dedo').val() == null) {
                    ShowError("Dedo dedo ojos abiertos", "El campo dedo dedo ojos abiertos  es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
            if ($('#ctl00_contenido_ojos_cerrados_dedo_dedo').val() == "0" || $('#ctl00_contenido_ojos_cerrados_dedo_dedo').val() == null) {
                    ShowError("Dedo dedo ojos cerrados", "El campo dedo dedo ojos cerrados  es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
                   if ($('#ctl00_contenido_ojos_abiertos_dedo_nariz').val() == "0" || $('#ctl00_contenido_ojos_abiertos_dedo_nariz').val() == null) {
                    ShowError("Dedo nariz ojos abiertos", "El campo dedo nariz ojos abiertos  es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
            if ($('#ctl00_contenido_ojos_cerrados_dedo_nariz').val() == "0" || $('#ctl00_contenido_ojos_cerrados_dedo_nariz').val() == null) {
                ShowError("Dedo nariz ojos cerrados", "El campo dedo nariz ojos cerrados  es obligatorio.");
                // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                esvalido = false;
            }
            else {
                //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if (!$('#novalorabe').is(':checked'))
            {
                //  if ($("#ta").val().split(" ").join("") == "") {
                //    ShowError("TA", "El campo TA es obligatorio.");
                //    $('#ta').parent().removeClass('state-success').addClass("state-error");
                //    $('#ta').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#ta').parent().removeClass("state-error").addClass('state-success');
                //    $('#ta').addClass('valid');
                //} 
               
                    if ($("#fc").val().split(" ").join("") == "") {
                    ShowError("FC", "El campo FC es obligatorio.");
                    //$('#fc').parent().removeClass('state-success').addClass("state-error");
                    //$('#fc').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#fc').parent().removeClass("state-error").addClass('state-success');
                    //$('#fc').addClass('valid');
                } 

                    if ($("#fr").val().split(" ").join("") == "") {
                    ShowError("FR", "El campo FR es obligatorio.");
                    //$('#fr').parent().removeClass('state-success').addClass("state-error");
                    //$('#fr').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#fr').parent().removeClass("state-error").addClass('state-success');
                    //$('#fr').addClass('valid');
                } 
                   if ($("#pulso").val().split(" ").join("") == "") {
                    ShowError("Pulso", "El campo pulso es obligatorio.");
                    //$('#pulso').parent().removeClass('state-success').addClass("state-error");
                    //$('#pulso').removeClass('valid');
                    esvalido = false;
                }
                else {
                    //$('#pulso').parent().removeClass("state-error").addClass('state-success');
                    //$('#pulso').addClass('valid');
                }


            }


              

                
                return esvalido;
            }

            function guardarCertificadopisco() {
                startLoading();
                var examen = ObtenerValoresCertificadopisco();
               
                $.ajax({
                    type: "POST",
                    url: "examen_medico.aspx/savecertificadopsicofisiologico",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'examen': examen }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $("#observacion").val(resultado.Observacion);
                            $('#ctl00_contenido_ExamenTracking').val(resultado.ServicioMedTracingId);
                            $("#serviciotrackingid").val(resultado.ServicioMedTracingId);

                            var ruta = resolveUrl(resultado.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            if (data.d.ishit) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Atención! </strong>" +
                                    data.d.msghit, "<br /></div>");
                                ShowAlert("¡Atencion!", data.d.msghit)
                                setTimeout(hideMessage, hideTime);
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                    "" + resultado.mensaje + "", "<br /></div>");
                                setTimeout(hideMessage, hideTime);
                                ShowSuccess("¡Bien hecho!",  resultado.mensaje + "");
                                $("#serviciotrackingid").val(resultado.ServicioMedTracingId);
                            }
                            $('#addtatuaje').removeClass('disabled');
                            $('#addlesion').removeClass('disabled');
                            $('#addintoxicacion').removeClass('disabled');
                            $('#ocular').parent().removeClass('state-success');
                            $('#indicaciones').parent().removeClass('state-success');
                            $('#observacion').parent().removeClass('state-success');
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
        }
        function ObtenerValoresCertificadopisco() {                                
                var examen = {
                    
                    Id: "",
                    ServiciomedicoTrackingId:$("#serviciotrackingid").val(),
                    TrackingId: $('#ctl00_contenido_tracking').val(),
                    DetalledetencionId:$('#ctl00_contenido_tracking').val(),
                    AlientoId: $('#ctl00_contenido_aliento').val(),
                    MarchaId: $('#ctl00_contenido_marcha').val(),
                    ActitudId: $('#ctl00_contenido_idactitud').val(),
                    AtencionId: $("#ctl00_contenido_atencion").val(),
                    Cavidad_oralId: $('#ctl00_contenido_cavidadoral').val(),
                    PupilaId: $("#ctl00_contenido_pupilas").val(),
                    Reflejo_pupilarId:  $('#ctl00_contenido_pupilares').val(),
                    ConjuntivaId: $('#ctl00_contenido_conjuntivas').val(),      
                    LenguajeId: $('#ctl00_contenido_lenguaje').val(),
                    RombergId: $('#ctl00_contenido_romberq').val(),
                    Ojos_abiertos_dedo_dedoId: $('#ctl00_contenido_ojos_abiertos_dedo_dedo').val(),
                    Ojos_cerrados_dedo_dedoId: $('#ctl00_contenido_ojos_cerrados_dedo_dedo').val(),
                    Ojos_abiertos_dedo_narizId: $('#ctl00_contenido_ojos_abiertos_dedo_nariz').val(),
                    Ojos_cerrados_dedo_narizId: $('#ctl00_contenido_ojos_cerrados_dedo_nariz').val(),
                    TA: $('#ta').val(),
                    FC: $('#fc').val(),
                    FR: $('#fr').val(),
                    Pulso: $('#pulso').val(),
                    No_valorabe: $('#novalorabe').is(':checked'),
                    OrientacionId: $('#ctl00_contenido_orientacion').val(),
                    ConclusionId: $('#ctl00_contenido_conclusion').val(),
                    Sustento_toxicologico: $('#sustento').is(':checked'),
                    Observacion_extra: $('#observaciones').val(),
                    FechaValoracion: $('#fechahora3').val()

                   
                };
                return examen;
        }


          function validarExamen() {                
                var esvalido = true;                

                if ($('#ctl00_contenido_tipo_examen').val() == "0" || $('#ctl00_contenido_tipo_examen').val() == null) {
                    ShowError("Tipo de examen", "El tipo de examen es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
                }

                   

                if ($("#ocular").val().split(" ").join("") == "") {
                    ShowError("Inspección ocular", "La inspección ocular es obligatoria.");
                    $('#ocular').parent().removeClass('state-success').addClass("state-error");
                    $('#ocular').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ocular').parent().removeClass("state-error").addClass('state-success');
                    $('#ocular').addClass('valid');
                } 

                if ($("#indicaciones").val().split(" ").join("") == "") {
                    ShowError("Indicaciones médicas", "Indicaciones médicas son obligatorias.");
                    $('#indicaciones').parent().removeClass('state-success').addClass("state-error");
                    $('#indicaciones').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#indicaciones').parent().removeClass("state-error").addClass('state-success');
                    $('#indicaciones').addClass('valid');
              } 

               if ($("#observacion").val().split(" ").join("") == "") {
                    ShowError("Observación", "Observación obligatoria.");
                    $('#observacion').parent().removeClass('state-success').addClass("state-error");
                    $('#observacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observacion').parent().removeClass("state-error").addClass('state-success');
                    $('#observacion').addClass('valid');
                } 
                return esvalido;
            }

            function guardarExamen() {
                startLoading();
                var examen = ObtenerValoresExamen();
               
                $.ajax({
                    type: "POST",
                    url: "examen_medico.aspx/saveexamen",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'examen': examen }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $("#observacion").val(resultado.Observacion);
                            $('#ctl00_contenido_ExamenTracking').val(resultado.TrackingId);

                            if (data.d.ishit) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Atención! </strong>" +
                                    data.d.msghit, "<br /></div>");
                                ShowAlert("¡Atencion!", data.d.msghit)
                                setTimeout(hideMessage, hideTime);
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                    "" + resultado.mensaje + " correctamente.", "<br /></div>");
                                setTimeout(hideMessage, hideTime);
                                ShowSuccess("¡Bien hecho!", "" + resultado.mensaje + " correctamente.");
                            }
                            $('#addtatuaje').removeClass('disabled');
                            $('#addlesion').removeClass('disabled');
                            $('#addintoxicacion').removeClass('disabled');
                            $('#ocular').parent().removeClass('state-success');
                            $('#indicaciones').parent().removeClass('state-success');
                            $('#observacion').parent().removeClass('state-success');
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
        }







            function ObtenerValoresExamen() {                                
                var examen = {
                    Edad: 0,
                    SexoId: $('#ctl00_contenido_sexoId').val(),
                    DetalleDetencionTracking: $('#ctl00_contenido_tracking').val(),
                    TipoExamen: $('#ctl00_contenido_tipo_examen').val(),
                    InspeccionOcular: $('#ocular').val(),
                    IndicacionesMedicas: $('#indicaciones').val(),
                    Observacion: $("#observacion").val(),
                    TrackingId: $('#ctl00_contenido_ExamenTracking').val(),
                    DiasSanarLesiones: $("#dias").val(),
                    RiesgoVida:  $('input:radio[name=peligro_]:checked').val(),
                    ConsecuenciasLesiones: $('input:radio[name=consecuencias_]:checked').val(),      
                    Fecha: $('#fecha').val(),
                    Lesion_visible: $('#Lesion_visible').is(':checked'),
                    Fallecimiento:$('#fallecimiento').is(':checked')
                   
                };
                return examen;
        }

         function pdf(datos) {
                $.ajax({
                    type: "POST",
                    url: "examen_medico.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });

            }

         $("body").on("click", ".saveprueba", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validarPruebaExamen()) {
                    guardarPrueba();
                }
        }); 

                function guardarPrueba() {
                startLoading();
                var prueba = ObtenerValoresPrueba();
                
                $.ajax({
                    type: "POST",
                    url: "examen_medico.aspx/saveprueba",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'prueba': prueba }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                           
                            $('#ctl00_contenido_PruebaTracking').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información de pruebas de examen médico se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información de pruebas de examen médico se  " + resultado.mensaje + " correctamente.");                            
                            $('#main').waitMe('hide');
                            $('#ta').parent().removeClass('state-success');
                            $('#alcoholimetro').parent().removeClass('state-success');
                            $('#fc').parent().removeClass('state-success');
                            $('#pulso').parent().removeClass('state-success');
                            $('.input').removeClass('state-success');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            if(resultado.mensaje == "No existe un exámen médico para esta prueba") ShowError("¡Error! Algo salió mal", resultado.mensaje);
                            else ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }
            function ObtenerValoresPrueba() {                                
                var prueba = {

                   
                    ExamenMedicoTracking : $("#ctl00_contenido_ExamenTracking").val(),
                    TrackingId : $("#ctl00_contenido_PruebaTracking").val(),                
                    MucosasId : $("#ctl00_contenido_mucosas").val(),
                    AlientoId : $("#ctl00_contenido_aliento").val(),
                    Examen_neurologicoId : $("#ctl00_contenido_neurologico").val(),
                    Disartia : $("#ctl00_contenido_disartia").val(),
                    ConjuntivasId : $("#ctl00_contenido_conjuntivas").val(),
                    MarchaId : $("#ctl00_contenido_marcha").val(),
                    PupilasId : $("#ctl00_contenido_pupilas").val(),
                    CoordinacionId : $("#ctl00_contenido_coordinacion").val(),
                    Reflejos_pupilaresId : $("#ctl00_contenido_pupilares").val(),
                    TendinososId : $("#ctl00_contenido_osteo").val(),
                    RomberqId : $("#ctl00_contenido_romberq").val(),
                    ConductaId : $("#ctl00_contenido_conducta").val(),
                    LenguajeId : $("#ctl00_contenido_lenguaje").val(),
                    AtencionId : $("#ctl00_contenido_atencion").val(),
                    OrientacionId : $("#ctl00_contenido_orientacion").val(),
                    DiadococinenciaId : $("#ctl00_contenido_diadococinencia").val(),
                    DedoId : $("#ctl00_contenido_dedo").val(),
                    TalonId : $("#ctl00_contenido_talon").val(),
                    Alcoholimetro : $("#alcoholimetro").val(),
                    TA : $("#ta").val(),
                    FC : $("#fc").val(),
                    FR : $("#fr").val(),
                    Pulso : $("#pulso").val()                   
                };
                return prueba;
        }

        function validarPruebaExamen() {
            var esvalido = true;
            
                if ($("#alcoholimetro").val().split(" ").join("") == "") {
                    ShowError("Alcoholímetro", "El alcoholímetro es obligatorio.");
                    $('#alcoholimetro').parent().removeClass('state-success').addClass("state-error");
                    $('#alcoholimetro').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#alcoholimetro').parent().removeClass("state-error").addClass('state-success');
                    $('#alcoholimetro').addClass('valid');
                } 

               if ($("#ta").val().split(" ").join("") == "") {
                    ShowError("TA", "El TA es obligatorio.");
                    $('#ta').parent().removeClass('state-success').addClass("state-error");
                    $('#ta').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ta').parent().removeClass("state-error").addClass('state-success');
                    $('#ta').addClass('valid');
                } 
            
               if ($("#fc").val().split(" ").join("") == "") {
                    ShowError("FC", "El FC es obligatorio.");
                    $('#fc').parent().removeClass('state-success').addClass("state-error");
                    $('#fc').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fc').parent().removeClass("state-error").addClass('state-success');
                    $('#fc').addClass('valid');
            } 
            if ($("#fr").val().split(" ").join("") == "") {
                ShowError("FR", "El FR es obligatorio.");
                $('#fr').parent().removeClass('state-success').addClass("state-error");
                $('#fr').removeClass('valid');
                esvalido = false;
            }
            else {
                $('#fr').parent().removeClass("state-error").addClass('state-success');
                $('#fr').addClass('valid');
            }

            
         

            if ($('#ctl00_contenido_mucosas').val() == "0" || $('#ctl00_contenido_mucosas').val() == null) {
                    ShowError("Mucosas", "Mucosas es obligatorio.");
                    $("#ctl00_contenido_mucosas").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_mucosas").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

              if ($('#ctl00_contenido_aliento').val() == "0" || $('#ctl00_contenido_aliento').val() == null) {
                    ShowError("Aliento", "El aliento es obligatorio.");
                    $("#ctl00_contenido_aliento").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_aliento").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

               if ($('#ctl00_contenido_neurologico').val() == "0" || $('#ctl00_contenido_neurologico').val() == null) {
                    ShowError("Examen neurológico", "El examen neurológico es obligatorio.");
                    $("#ctl00_contenido_neurologico").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_neurologico").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_conjuntivas').val() == "0" || $('#ctl00_contenido_conjuntivas').val() == null) {
                    ShowError("Conjuntivas", "El conjuntivas es obligatorio.");
                    $("#ctl00_contenido_conjuntivas").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_conjuntivas").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_marcha').val() == "0" || $('#ctl00_contenido_marcha').val() == null) {
                    ShowError("Marcha", "El marcha es obligatorio.");
                    $("#ctl00_contenido_marcha").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_marcha").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
          

            if ($('#ctl00_contenido_pupilas').val() == "0" || $('#ctl00_contenido_pupilas').val() == null) {
                    ShowError("Pupilas", "El pupilas es obligatorio.");
                    $("#ctl00_contenido_pupilas").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_pupilas").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_coordinacion').val() == "0" || $('#ctl00_contenido_coordinacion').val() == null) {
                    ShowError("Coordinación", "Coordinación es obligatorio.");
                    $("#ctl00_contenido_coordinacion").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_coordinacion").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_pupilares').val() == "0" || $('#ctl00_contenido_pupilares').val() == null) {
                    ShowError("Discurso", "Discurso es obligatorio.");
                    $("#ctl00_contenido_pupilares").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_pupilares").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_osteo').val() == "0" || $('#ctl00_contenido_osteo').val() == null) {
                    ShowError("Reflejos osteo tendinosos", "Reflejos osteo tendinosos es obligatorio.");
                    $("#ctl00_contenido_osteo").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_osteo").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
           

            if ($('#ctl00_contenido_romberq').val() == "0" || $('#ctl00_contenido_romberq').val() == null) {
                    ShowError("Romberg", "Romberg es obligatorio.");
                    $("#ctl00_contenido_romberq").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_romberq").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            
            if ($('#ctl00_contenido_disartia').val() == "0" || $('#ctl00_contenido_disartia').val() == "-1" || $('#ctl00_contenido_disartia').val() == null) {
                    ShowError("Disartía", "Disartía es obligatorio.");
                    $("#ctl00_contenido_orientacion").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_disartia").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_conducta').val() == "0" || $('#ctl00_contenido_conducta').val() == null) {
                    ShowError("Conducta", "Conducta es obligatorio.");
                    $("#ctl00_contenido_conducta").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_conducta").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
            if ($('#ctl00_contenido_lenguaje').val() == "0" || $('#ctl00_contenido_lenguaje').val() == null) {
                    ShowError("Lenguaje", "Conducta es obligatorio.");
                    $("#ctl00_contenido_lenguaje").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_lenguaje").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
            
            if ($('#ctl00_contenido_atencion').val() == "0" || $('#ctl00_contenido_atencion').val() == null) {
                    ShowError("Atención", "Atención es obligatorio.");
                    $("#ctl00_contenido_atencion").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_atencion").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
        
            
            if ($('#ctl00_contenido_diadococinencia').val() == "0" || $('#ctl00_contenido_diadococinencia').val() == null) {
                    ShowError("Diadococinencia", "Diadococinencia es obligatorio.");
                    $("#ctl00_contenido_diadococinencia").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_diadococinencia").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
         
            if ($('#ctl00_contenido_dedo').val() == "0" || $('#ctl00_contenido_dedo').val() == null) {
                    ShowError("Dedo-nariz", "El dedo-nariz es obligatorio.");
                    $("#ctl00_contenido_dedo").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_dedo").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
         
            if ($('#ctl00_contenido_talon').val() == "0" || $('#ctl00_contenido_talon').val() == null) {
                    ShowError("Talón-rodilla", "El talón-rodilla es obligatorio.");
                    $("#ctl00_contenido_talon").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_talon").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
          
            return esvalido;
        }
        function CargarDatos(trackingid) {
                var param = RequestQueryString("tracking");
                
                if (param != undefined) {
                    trackingid=param
                   
                }
            startLoading();
            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/getdatos",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingid: trackingid,
                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {

                         $('#nombreInterno').text(resultado.obj.Nombre);

                         if (resultado.obj.Lesion_simple_vista == "True")
                            {
                                    $("#Lesion_visible").prop("checked", true);

                              }
                              else {
                                   

                        }
                         //$('#Folio').val(resultado.obj.Expediente);
                        $('#expedienteInterno').text(resultado.obj.Expediente);
                        $('#centroInterno').text(resultado.obj.Centro);
                        $('#edadInterno').text(resultado.obj.Edad);
                        $('#sexoInterno').text(resultado.obj.Sexo);
                        $('#ctl00_contenido_sexoId').val(resultado.obj.SexoId);
                        $('#domicilioInterno').text(resultado.obj.CalleDomicilio);
                        $('#coloniaInterno').text(resultado.obj.coloniaNombre);
                        $('#municipioInterno').text(resultado.obj.municipioNombre);
                       $('#aliasinternotd').text(resultado.obj.aliasdetenido);
                        var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);                        
                        if (imagenAvatar != null && imagenAvatar != "") {
                           
                            $('#avatar').attr("src", imagenAvatar);
                        }
                        else {
                            var fotoNull = ResolveUrl("~/Content/img/avatars/male.png");
                            $('#avatar').attr("src", fotoNull);
                        }                            
                        $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);
                        $('#ctl00_contenido_trackingUser').val(resultado.obj.TrackingId);
                     
                    } else {
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarDatosExamen() {
            startLoading();

            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/getDatosExamen",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingExamenId: $("#ctl00_contenido_ExamenTracking").val()

                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {

                        //Datos exmane medico               
                        loadDate();
                        $("#serviciotrackingid").val($("#ctl00_contenido_ExamenTracking").val());
                        if (resultado.CertificadoLesion != null) {
                            $("#fechahora4").val(resultado.CertificadoLesion.Fechavaloracion);
                            CargarListado(resultado.CertificadoLesion.Tipo_alergiaId, "tipo_alergia");
                            CargarListado(resultado.CertificadoLesion.Tipo_tatuajeId, "tipo_tatuaje");
                            CargarListado(resultado.CertificadoLesion.AntecedentesId, "antecedentes");
                            $("#observacioneslesion").val(resultado.CertificadoLesion.Observaciones_lesion);
                            $("#observacionesalergia").val(resultado.CertificadoLesion.Observacion_alergia);
                            $("#observacionesTatuaje").val(resultado.CertificadoLesion.Observacion_tatuje);
                            $("#observacionesGlobales").val(resultado.CertificadoLesion.Observacion_general);
                            $("#observacionesAntecedente").val(resultado.CertificadoLesion.Observaciones_antecedentes);
                            $("#certLesionFotografias").val(resultado.CertificadoLesion.FotografiasId);
                            $("#dt_basic_lesiones_fotos").DataTable().destroy();
                            responsiveHelper_dt_basiclesionesFotografias = undefined;
                            LoadTablaLesionesFotos();

                            if (resultado.CertificadoLesion.Lesion_simple_vista == "True") {
                                $("#Lesion_visible").prop("checked", true);
                            }
                            else {
                                $("#Lesion_visible").prop("checked", false);
                            }
                            if (resultado.CertificadoLesion.Fallecimiento == "True") {
                                $("#fallecimiento").prop("checked", true);
                            }
                            else {
                                $("#fallecimiento").prop("checked", false);
                            }
                            if (resultado.CertificadoLesion.Envio_hospital == "True") {
                                $("#enviohospital").prop("checked", true);
                            }
                            else {
                                $("#enviohospital").prop("checked", false);
                            }
                            if (resultado.CertificadoLesion.Sinlesion == "True") {
                                $("#sinlesion").prop("checked", true);
                            }
                            else {
                                $("#sinlesion").prop("checked", false);
                            }
                            if ($("#sinlesion").is(':checked')) {
                                $('#Lesion_visible').attr("disabled", "disabled");
                                $('#fallecimiento').attr("disabled", "disabled");
                                $('#enviohospital').attr("disabled", "disabled");
                                $("#Lesion_visible").prop("checked", false);
                                $("#fallecimiento").prop("checked", false);
                                $("#enviohospital").prop("checked", false);
                                $("#ctl00_contenido_tipo_lesion").attr("disabled", "disabled");

                                $("#ctl00_contenido_tipo_lesion").val(null).trigger('change');
                                $('#observacioneslesion').attr("disabled", "disabled");
                                $('#observacioneslesion').val("");
                                CargarListado("0", "tipo_lesion");
                            }
                            else {
                                CargarListado(resultado.CertificadoLesion.Tipo_lesionId, "tipo_lesion");
                                $('#Lesion_visible').removeAttr("disabled");
                                $('#fallecimiento').removeAttr("disabled");
                                $('#enviohospital').removeAttr("disabled");
                                $('#ctl00_contenido_tipo_lesion').removeAttr("disabled");
                                $('#observacioneslesion').removeAttr("disabled");
                            }
                            if ($('#observacionesalergia').val() != "") {
                                $('#observacionesalergia').removeAttr("disabled");
                            }
                            if ($('#observacionesAntecedente').val() != "") {
                                $('#observacionesAntecedente').removeAttr("disabled");
                            }
                            if ($('#observacionesTatuaje').val() != "") {
                                $('#observacionesTatuaje').removeAttr("disabled");
                            }

                        }

                        if (resultado.CertificadoQuimico != null) {
                            $("#Folio").val(resultado.CertificadoQuimico.Folio);
                            if (resultado.CertificadoQuimico.EtanolId != 0) {
                                $("#fechahora2").val(resultado.CertificadoQuimico.Fecha_proceso);
                                $("#fechahora").val(resultado.CertificadoQuimico.Fecha_toma);
                            }
                            $("#fr").val(resultado.CertificadoQuimico.FR);
                            CargarListado(resultado.CertificadoQuimico.EtanolId, "Etanol");

                            if (resultado.CertificadoQuimico.ResultadoEtanol == "POSITIVO") {
                                $("#gradosection").show();
                                $("#mgdlsection").show();
                            }
                            else {
                                $("#gradosection").hide();
                                $("#mgdlsection").hide();
                            }

                            CargarListado(resultado.CertificadoQuimico.BenzodiapinaId, "Benzodiazepina");
                            CargarListado(resultado.CertificadoQuimico.AnfetaminaId, "anfetamina");
                            CargarListado(resultado.CertificadoQuimico.CannabisId, "cannabis");
                            CargarListado(resultado.CertificadoQuimico.CocaínaId, "cocaina");
                            CargarListado(resultado.CertificadoQuimico.ExtasisId, "extasis");
                            CargarListado(resultado.CertificadoQuimico.EquipoautilizarId, "equipo_a_utilizar");
                            //$("#ctl00_contenido_Etanol").val(resultado.CertificadoQuimico.EtanolId);
                            $("#grado").val(resultado.CertificadoQuimico.Grado);
                        }
                        if (resultado.Psicofisiologico != null) {
                            $("#fechahora3").val(resultado.Psicofisiologico.FechaValoracion);
                            CargarListado(resultado.Psicofisiologico.AlientoId, "aliento");
                            CargarListado(resultado.Psicofisiologico.MarchaId, "marcha");
                            CargarListado(resultado.Psicofisiologico.ActitudId, "idactitud");
                            // $("#ctl00_contenido_idactitud").val(resultado.Psicofisiologico.ActitudId);
                            //$("#ctl00_contenido_atencion").val(resultado.Psicofisiologico.AtencionId);
                            CargarListado(resultado.Psicofisiologico.AtencionId, "atencion");
                            CargarListado(resultado.Psicofisiologico.Cavidad_oralId, "cavidadoral");
                            //$("#ctl00_contenido_cavidadoral").val(resultado.Psicofisiologico.Cavidad_oralId);
                            CargarListado(resultado.Psicofisiologico.PupilaId, "pupilas");
                            CargarListado(resultado.Psicofisiologico.Reflejo_pupilarId, "pupilares");
                            CargarListado(resultado.Psicofisiologico.ConjuntivaId, "conjuntivas");
                            CargarListado(resultado.Psicofisiologico.LenguajeId, "lenguaje");
                            CargarListado(resultado.Psicofisiologico.RombergId, "romberq");

                            CargarListado(resultado.Psicofisiologico.Ojos_abiertos_dedo_dedoId, "ojos_abiertos_dedo_dedo");
                            CargarListado(resultado.Psicofisiologico.Ojos_cerrados_dedo_dedoId, "ojos_cerrados_dedo_dedo");
                            CargarListado(resultado.Psicofisiologico.Ojos_abiertos_dedo_narizId, "ojos_abiertos_dedo_nariz");
                            CargarListado(resultado.Psicofisiologico.Ojos_cerrados_dedo_narizId, "ojos_cerrados_dedo_nariz");

                            //$("#ctl00_contenido_ojos_abiertos_dedo_dedo").val(resultado.Psicofisiologico.Ojos_abiertos_dedo_dedoId);
                            //$("#ctl00_contenido_ojos_cerrados_dedo_dedo").val(resultado.Psicofisiologico.Ojos_cerrados_dedo_dedoId);
                            //$("#ctl00_contenido_ojos_abiertos_dedo_nariz").val(resultado.Psicofisiologico.Ojos_abiertos_dedo_narizId );
                            //$("#ctl00_contenido_ojos_cerrados_dedo_nariz").val(resultado.Psicofisiologico.Ojos_cerrados_dedo_narizId);
                            $("#ta").val(resultado.Psicofisiologico.TA);
                            $("#fc").val(resultado.Psicofisiologico.FC);
                            $("#fr").val(resultado.Psicofisiologico.FR);
                            $("#pulso").val(resultado.Psicofisiologico.Pulso);
                            $("#observaciones").val(resultado.Psicofisiologico.Observacion_extra);
                            if (resultado.Psicofisiologico.No_valorabe == "True") {
                                $("#novalorabe").prop("checked", true);
                                $("#ta").prop('disabled', true);
                                $("#fc").prop('disabled', true);
                                $("#fr").prop('disabled', true);
                                $("#pulso").prop('disabled', true);
                            }
                            else {
                                $("#novalorabe").prop("checked", false);
                                $("#ta").prop('disabled', false);
                                $("#fc").prop('disabled', false);
                                $("#fr").prop('disabled', false);
                                $("#pulso").prop('disabled', false);
                            }
                            if (resultado.Psicofisiologico.Sustento_toxicologico == "True") {
                                $("#sustento").prop("checked", true);
                            }
                            else {
                                $("#sustento").prop("checked", false);
                            }
                            CargarListado(resultado.Psicofisiologico.OrientacionId, "orientacion");
                            CargarListado(resultado.Psicofisiologico.ConclusionId, "conclusion");
                        }
                        else {
                            CargarListado("0", "aliento");
                            $("#ctl00_contenido_aliento").val(0);
                            $("#ctl00_contenido_aliento").change();

                            CargarListado("0", "marcha");
                            CargarListado("0", "idactitud");
                            // $("#ctl00_contenido_idactitud").val(resultado.Psicofisiologico.ActitudId);
                            //$("#ctl00_contenido_atencion").val(resultado.Psicofisiologico.AtencionId);
                            CargarListado("0", "atencion");
                            CargarListado("0", "cavidadoral");
                            //$("#ctl00_contenido_cavidadoral").val(resultado.Psicofisiologico.Cavidad_oralId);
                            CargarListado("0", "pupilas");
                            CargarListado("0", "pupilares");
                            CargarListado("0", "conjuntivas");
                            CargarListado("0", "lenguaje");
                            CargarListado("0", "romberq");

                            CargarListado("0", "ojos_abiertos_dedo_dedo");
                            CargarListado("0", "ojos_cerrados_dedo_dedo");
                            CargarListado("0", "ojos_abiertos_dedo_nariz");
                            CargarListado("0", "ojos_cerrados_dedo_nariz");
                            CargarListado("0", "orientacion");

                            CargarListado("0", "conclusion");
                        }

                        BloquearControles();
                    }
                    else {
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }

                    $('#main').waitMe('hide');
                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        
        $(document).on('keydown', 'input[pattern]', function(e){
              var input = $(this);
              var oldVal = input.val();
              var regex = new RegExp(input.attr('pattern'), 'g');

              setTimeout(function(){
                var newVal = input.val();
                if(!regex.test(newVal)){
                  input.val(oldVal); 
                }
              }, 0);
            });

        function CargarDatosPruebas() {
             startLoading();

            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/getDatosPruebas",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingExamenId: $("#ctl00_contenido_PruebaTracking").val()
                    
                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {

                        //Datos Pruebas

                        CargarListado(resultado.objprueba.Mucosas, "mucosas");

                        //Datos exmane medico               
                       
                        $('#ocular').val(resultado.objexamen.InspeccionOcular);
                        $('#indicaciones').val(resultado.objexamen.IndicacionesMedicas);
                        $("#observacion").val(resultado.objexamen.Observacion);
                        $("#dias").val(resultado.objexamen.DiasSanarLesiones);
                        CargarListado(resultado.objexamen.Tipo, "tipo_examen");
                        $('#fecha').val(resultado.objexamen.Fecha);

                        if (resultado.objprueba != null) {
                            $("#alcoholimetro").val(resultado.objprueba.Alcoholimetro);
                            $("#ta").val(resultado.objprueba.TA);
                            $("#fc").val(resultado.objprueba.FC);
                            $("#fr").val(resultado.objprueba.FR);
                            $("#pulso").val(resultado.objprueba.Pulso);
                            CargarListado(resultado.objprueba.MucosasId, "mucosas");
                            CargarListado(resultado.objprueba.AlientoId, "aliento");
                            CargarListado(resultado.objprueba.Examen_neurologicoId, "neurologico");

                            CargarListado(resultado.objprueba.ConjuntivasId, "conjuntivas");
                            CargarListado(resultado.objprueba.MarchaId, "marcha");
                            CargarListado(resultado.objprueba.PupilasId, "pupilas");
                            CargarListado(resultado.objprueba.CoordinacionId, "coordinacion");
                            CargarListado(resultado.objprueba.Reflejos_pupilaresId, "pupilares");
                            CargarListado(resultado.objprueba.TendinososId, "osteo");
                            CargarListado(resultado.objprueba.RomberqId, "romberq");
                            CargarListado(resultado.objprueba.ConductaId, "conducta");
                            CargarListado(resultado.objprueba.LenguajeId, "lenguaje");
                            CargarListado(resultado.objprueba.AtencionId, "atencion");
                            CargarListado(resultado.objprueba.OrientacionId, "orientacion");
                            CargarListado(resultado.objprueba.DiadococinenciaId, "diadococinencia");
                            CargarListado(resultado.objprueba.DedoId, "dedo");
                            CargarListado(resultado.objprueba.TalonId, "talon");
                        }
                      
                      
                     
                    } else {
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

       $("body").on("click", "#addtatuaje", function () {
           $("#ctl00_contenido_lblMessage").html("");
           limpiarAdicional();
		        CargarListado(0, "tipo_tatuaje");                
                $("#ctl00_contenido_observacionadicional").val("");       
                $("#ctl00_contenido_lugaradicional").val(""); 
                $("#btnsaveadicional").attr("data-id", "");
                $("#btnsaveadicional").attr("data-tracking", "");
                $("#btnsaveadicional").attr("data-clasificacion", "tatuaje");
                $("#form-modal-title").empty();
                $("#sectionLugar").show();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar tatuaje");
                $("#form-modal").modal("show");
            });
       
        
         $("body").on("click", "#addlesion", function () {
             $("#ctl00_contenido_lblMessage").html("");
                  limpiarAdicional();
		        CargarListado(0, "tipo_lesion");                
                $("#ctl00_contenido_observacionadicional").val("");       
                $("#ctl00_contenido_lugaradicional").val(""); 
                $("#btnsaveadicional").attr("data-id", "");
                $("#btnsaveadicional").attr("data-tracking", "");
                $("#btnsaveadicional").attr("data-clasificacion", "lesion");
                $("#form-modal-title").empty();
              $("#sectionLugar").show();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar lesión");
                $("#form-modal").modal("show");
            });
        
        
        $("body").on("click", "#addintoxicacion", function () {

            $("#ctl00_contenido_lblMessage").html("");
                 limpiarAdicional();
		        CargarListado(0, "tipo_intoxicacion");                
                $("#ctl00_contenido_observacionadicional").val("");       
                $("#ctl00_contenido_lugaradicional").val(""); 
                $("#btnsaveadicional").attr("data-id", "");
                $("#btnsaveadicional").attr("data-tracking", "");
                $("#btnsaveadicional").attr("data-clasificacion", "intoxicacion");
                $("#form-modal-title").empty();
               $("#sectionLugar").hide();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar intoxicación");
                $("#form-modal").modal("show");
            });

              $("body").on("click", ".edittatuaje", function () {
                  limpiarAdicional();
                $("#ctl00_contenido_lblMessage").html("");
           
                $("#btnsaveadicional").attr("data-id", $(this).attr("data-id"));
                $("#btnsaveadicional").attr("data-tracking", $(this).attr("data-tracking"));
                $("#btnsaveadicional").attr("data-clasificacion", $(this).attr("data-clasificacion"));
                $("#ctl00_contenido_lugaradicional").val($(this).attr("data-lugar"));
                $("#ctl00_contenido_observacionadicional").val($(this).attr("data-observacion"));
                $("#ctl00_contenido_tipoadicional").val($(this).attr("data-tipo"));
                  CargarListado($(this).attr("data-tipo"), "tipo_tatuaje");    
                $("#sectionLugar").show();
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar tatuaje");
                $("#form-modal").modal("show");
        });
                $("body").on("click", ".editintoxicacion", function () {
                  limpiarAdicional();
                $("#ctl00_contenido_lblMessage").html("");
              
                $("#btnsaveadicional").attr("data-id", $(this).attr("data-id"));
                $("#btnsaveadicional").attr("data-tracking", $(this).attr("data-tracking"));
                $("#btnsaveadicional").attr("data-clasificacion", $(this).attr("data-clasificacion"));
                $("#ctl00_contenido_observacionadicional").val($(this).attr("data-observacion"));
                $("#ctl00_contenido_tipoadicional").val($(this).attr("data-tipo"));
                  CargarListado($(this).attr("data-tipo"), "tipo_intoxicacion");    
                $("#sectionLugar").hide();
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar intoxicación");
                $("#form-modal").modal("show");
        });
                $("body").on("click", ".editlesion", function () {
              
                limpiarAdicional();
                $("#ctl00_contenido_lblMessage").html("");
           
                $("#btnsaveadicional").attr("data-id", $(this).attr("data-id"));
                $("#btnsaveadicional").attr("data-tracking", $(this).attr("data-tracking"));
                $("#btnsaveadicional").attr("data-clasificacion", $(this).attr("data-clasificacion"));
                $("#ctl00_contenido_lugaradicional").val($(this).attr("data-lugar"));
                $("#ctl00_contenido_observacionadicional").val($(this).attr("data-observacion"));
                $("#ctl00_contenido_tipoadicional").val($(this).attr("data-tipo"));
                  CargarListado($(this).attr("data-tipo"), "tipo_lesion");    
                $("#sectionLugar").show();
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar lesión");
                $("#form-modal").modal("show");
        });

            $("body").on("click", ".saveadicional", function () {
                              
                var datos = {       
                    Id:$("#btnsaveadicional").attr("data-id"),
                    ExamenTrackingId : $("#ctl00_contenido_ExamenTracking").val(),
                    TrackingId : $("#btnsaveadicional").attr("data-tracking"),               
                    Lugar : $("#ctl00_contenido_lugaradicional").val(),
                    Observacion : $("#ctl00_contenido_observacionadicional").val(),
                    Tipo : $("#ctl00_contenido_tipoadicional").val(),
                    Clasificacion: $("#btnsaveadicional").attr("data-clasificacion"),
     
                                      
                };
                if (validarAdicional($(this).attr("data-clasificacion"))) {
                    saveAdicional(datos);
                }

        });
        function validarAdicional(clasificacion) {
               var esvalido = true;                

                if ($('#ctl00_contenido_tipoadicional').val() == "0" || $('#ctl00_contenido_tipoadicional').val() == null) {
                    ShowError("Tipo", "El campo tipo es obligatorio.");
                    $("#ctl00_contenido_tipoadicional").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_tipoadicional").select({ containerCss: { "border-color": "#bdbdbd" } });
                }

            if (clasificacion != "intoxicacion") {

                if ($("#ctl00_contenido_lugaradicional").val().split(" ").join("") == "") {
                    ShowError("Lugar", "El campo lugar es obligatorio.");
                    $('#ctl00_contenido_lugaradicional').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_lugaradicional').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_lugaradicional').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_lugaradicional').addClass('valid');
                }
            }
            return esvalido;
        }

            function limpiarAdicional() {
                $('#ctl00_contenido_tipoadicional').parent().removeClass('state-success');
                 
                $('#ctl00_contenido_tipoadicional').parent().removeClass("state-error");
                $('#ctl00_contenido_lugaradicional').parent().removeClass('state-success');
                $('#ctl00_contenido_lugaradicional').parent().removeClass("state-error");
                $('#ctl00_contenido_observacionadicional').parent().removeClass('state-success');
                $('#ctl00_contenido_observacionadicional').parent().removeClass("state-error");
            }
           function saveAdicional(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "examen_medico.aspx/saveAdicional",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        item: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            window.emptytablelesiones = false;
                             window.tablelesiones.api().ajax.reload();
                            window.emptytableintoxicaciones = false;
                             window.tableintoxicaciones.api().ajax.reload();
                            window.emptytabletatuajes = false;
                             window.tabletatuajes.api().ajax.reload();
                           

                            $("#form-modal").modal("hide");
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');

                         $('#tabexamen').parent().addClass('active');
                         $("#refexamen").attr("aria-expanded", "true");
                            $("#refpruebas").attr("aria-expanded", "false");
                            $('#tabpruebas').parent().removeClass('active');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
        }
        function bloquear(datos) {            
            startLoading();
                $.ajax({
                    url: "examen_medico.aspx/blockuser",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        item: datos,
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            window.emptytablelesiones = false;
                             window.tablelesiones.api().ajax.reload();
                            window.emptytableintoxicaciones = false;
                             window.tableintoxicaciones.api().ajax.reload();
                            window.emptytabletatuajes = false;
                             window.tabletatuajes.api().ajax.reload();
                            $("#blockuser-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                data.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", data.mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del usuario. . Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }
        function CargarListado(id, select) {
        
            $('#ctl00_contenido_' + select).empty();
            $.ajax({

                type: "POST",
                url: "examen_medico.aspx/getListado",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                  data: JSON.stringify({

                      select: select
                    }),
                cache: false,
                success: function (response) {
                    if ( select == "tipo_intoxicacion" )
                        select = "tipoadicional";
                    var Dropdown = $('#ctl00_contenido_' + select);


                    Dropdown.empty();
                    if (select != "orientacion" && select != "conclusion" && select != "tipo_lesion" && select != "tipo_alergia"&& select != "antecedentes" && select != "tipo_tatuaje")
                    {
                        Dropdown.append(new Option("[Seleccione]", "0"));
                    }
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));

                    });
                    if (id != "") {
                        Dropdown.val(id);
                        if (select != "orientacion" && select != "conclusion" && select != "tipo_lesion" && select != "tipo_alergia" && select != "antecedentes" && select != "tipo_tatuaje")
                        {
                            Dropdown.trigger("change.select");
                        }
                        else {
                            Dropdown.trigger("change.select2");
                    }
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de" + select + ". Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }


        function ResolveUrl(url) {
            var baseUrl = "<%= ResolveUrl("~/") %>";
            if (url.indexOf("~/") == 0) {
                url = baseUrl + url.substring(2);
            }
            return url;
        }

        function loadDate() {

            var date = new Date();
                    var dia = date.getDate();
                    var mes = date.getMonth() + 1;
                    var anio = date.getFullYear();
                    var hora = date.getHours();
                    var minutos = date.getMinutes();
                    var segundos = date.getSeconds();
                                        
                    
                    dia = dia.toString().length == 1 ? "0" + dia : dia;
                    mes = mes.toString().length == 1 ? "0" + mes : mes;
                    hora = hora.toString().length == 1 ? "0" + hora : hora;
                    minutos = minutos.toString().length == 1 ? "0" + minutos : minutos;
                    segundos = segundos.toString().length == 1 ? "0" + segundos : segundos;
                    var fechaCompleta = dia + "/" + mes + "/" + anio + " " + hora + ":" + minutos + ":" + segundos;
                                        
                    $('#fecha').val(fechaCompleta);
        }
        $('#dias').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
        });

        $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
            var param = $("#ctl00_contenido_trackingUser").val();
                CargarDatosModal(param);
        });

        $("body").on("click", "#sexoEdad", function () {
            $("#sexoedad-modal").modal("show");
            $('#ctl00_contenido_sexo').parent().removeClass('state-success');
            $('#ctl00_contenido_sexo').parent().removeClass("state-error");
            $('#edad').parent().removeClass('state-success');
            $('#edad').parent().removeClass("state-error");

            var param = $("#ctl00_contenido_trackingUser").val();
            CargarSexoEdad(param);
        });

        $("body").on("click", "#btncontinuarsexo", function () {
            if (validarSexoEdad()) {
                $("#confirm-modal").modal("show");
            }
        });

        $('#fechadatetimepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        //$('#fechadatetimepicker').data("DateTimePicker").hide();

        function CargarSexoEdad(trackingid) {
            startLoading();
            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/getSexoEdad",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingid: trackingid,
                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {
                        $("#edad").val(resultado.obj.Edad);
                        $("#fechaNacimiento").val(resultado.obj.FechaNacimiento);
                        CargarSexo(resultado.obj.Sexo);
                    }
                    else {
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
             });
        }

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != null && imagenAvatar != "") {
                                $('#imgInfo').attr("src", imagenAvatar);                                
                            }
                            else {
                                var fotoNull = ResolveUrl("~/Content/img/avatars/male.png");
                                $('#imgInfo').attr("src", fotoNull);
                            }                            
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
        }

        $("body").on("click", "#btnconfirm", function () {
            saveSexoEdad();
            $("#confirm-modal").modal("hide");
        });

        function validarSexoEdad() {                
            var esvalido = true;                

            if ($('#ctl00_contenido_sexo').val() == "0" || $('#ctl00_contenido_sexo').val() == null) {
                ShowError("Sexo", "El campo de sexo es obligatorio.");
                $("#ctl00_contenido_sexo").select({ containerCss: { "border-color": "#a90329" } });
                esvalido = false;
            }
            else {
                $('#ctl00_contenido_sexo').parent().removeClass("state-error").addClass('state-success');
                $('#ctl00_contenido_sexo').addClass('valid');
            }

            if ($("#edad").val().split(" ").join("") == "") {
                ShowError("Edad", "El campo edad es obligatorio.");
                $('#edad').parent().removeClass('state-success').addClass("state-error");
                $('#edad').removeClass('valid');
                esvalido = false;
            }
            else {
                $('#edad').parent().removeClass("state-error").addClass('state-success');
                $('#edad').addClass('valid');
            }

            return esvalido;
        }

        function saveSexoEdad() {
            startLoading();

            datos = [
                Edad = $('#edad').val(),
                Sexo = $('#ctl00_contenido_sexo').val(),
                DetalleDetencionTracking = $('#ctl00_contenido_tracking').val()
            ];

            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/saveSexoEdad",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    datos: datos
                }),
                cache: false,
                success: function (data) {
                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {
                        ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                        $("#sexoedad-modal").modal("hide");
                        $("#confirm-modal").modal("hide");

                        $('#sexoInterno').text(resultado.sexo);
                        $('#edadInterno').text(resultado.edad);

                        $('#main').waitMe('hide');
                    }
                    else {
                        ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                    $('#main').waitMe('hide');
                },
                error: function () {
                    ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                    $('#main').waitMe('hide');
                }
            });
        }

        function CargarSexo(setsexo) {
            $('#ctl00_contenido_sexo').empty();
            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/getSexo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_sexo');

                    Dropdown.append(new Option("[Sexo]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));

                    });

                    if (setsexo != "") {

                        Dropdown.val(setsexo);
                        Dropdown.trigger("change.select2");

                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de sexo. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

          function addText(){
          
          $("#observacion").value=$("#observacion").val()
 
           }  

        var hideTime = 5000;
        function hideMessage() {
            $("#ctl00_contenido_lblMessage").html("");
        }

        $("body").on("click", "#addLesion", function () {
            $("#lesion-modal").modal("show");
        });

        $("#btnSaveModalLesion").click(function () {
            if (validarFotografiaLesion()) {
                ObtenerFotografias_Cert();
            }
        })

        $("#btnCancelModalLesion").click(function () {
            LimpiarLesionModal();
            $(this).modal('hide');
        });

        function LimpiarLesionModal() {
            $('#<%= foto.ClientID %>').parent().removeClass('state-success').removeClass("state-error");
            $('#<%= foto.ClientID %>').removeClass('valid');
            $('#<%= foto.ClientID %>').val("");
            $('#descripcionLesion').parent().removeClass('state-success').removeClass("state-error");
            $('#descripcionLesion').removeClass('valid');
            $('#descripcionLesion').val("");
        }

        function validarFotografiaLesion() {
            var esvalido = true;

            if ($("#descripcionLesion").val() == null || $("#descripcionLesion").val() == 0) {
                ShowError("Descripción", "La descripción es obligatoria.");
                $('#descripcionLesion').parent().removeClass('state-success').addClass("state-error");
                $('#descripcionLesion').removeClass('valid');
                esvalido = false;
            }
            else {
                $('#descripcionLesion').parent().removeClass("state-error").addClass('state-success');
                $('#descripcionLesion').addClass('valid');
            }

            var file = $("#<%= foto.ClientID %>").val();
            if (file != null && file != '') {

                if (!validaImagen(file)) {
                    ShowError("Evidencia fotográfica", "Solo se permiten extensiones .jpg, .jpeg o .png.");
                    esvalido = false;
                }
                else {
                    var validar = $("#Validar1").val();
                    var validar2 = $("#Validar2").val();


                    if ($("#Max").val() != "0") {
                        if (validar == 'false') {
                            ShowError("Evidencia fotográfica", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                            esvalido = false;
                        }

                    }
                    if ($("#Min").val() != "0") {
                        var s = $("#Min").val() * 1024;
                        if (validar2 == 'false') {
                            ShowError("Evidencia fotográfica", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                            esvalido = false;
                        }
                    }
                }
            }
            else {
                ShowError("Evidencia fotográfica", "La evidencia fotográfica es obligatoria.");
                $('#<%= foto.ClientID %>').parent().removeClass('state-success').addClass("state-error");
                $('#<%= foto.ClientID %>').removeClass('valid');
            }

            return esvalido;
        }

        function validaImagen(file) {
            var extArray = new Array("jpg", "jpeg", "png", "gif");

            var array = file.split(".");
            var ext = array[array.length - 1].toLowerCase();
            for (var i = 0; i < extArray.length; i++) {
                if (extArray[i] == ext) {
                    return true;
                }
            }
            return false;
        }

        function ObtenerFotografias_Cert() {
            var valor = $("#certLesionFotografias").val();
            if (valor != "" && valor != "0") {
                guardarFotografiaLesion(valor);
            }

            else {
                $.ajax({
                    url: "examen_medico.aspx/GetCertificadoLesionFotografias",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            $("#certLesionFotografias").val(resultado.valor);
                            guardarFotografiaLesion(resultado.valor);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible obtener el certificado de la lesión para subir las fotografias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible obtener el certificado de la lesión para subir las fotografias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
        }

        function guardarFotografiaLesion(idFotografias_cert) {
            var files = $("#<%= foto.ClientID %>").get(0).files;
            var nombreArchivo = "";

            if (files.length > 0) {
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append(files[i].name, files[i]);
                }

                $.ajax({
                    url: "<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Handlers/FileUploadHandler.ashx?action=6&detencion=" + $("#<%= tracking.ClientID %>").val() + "&certificadoFotografia=" + idFotografias_cert + "&before=",
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.exitoso) {
                            nombreArchivo = response.nombreArchivo;
                            SaveFotografia(nombreArchivo);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible subir la fotografía. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    },
                    error: function (err) {
                        ShowError("¡Error!", "No fue posible subir la fotografía. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
        }

        function SaveFotografia(nombreArchivo) {
            var datos = {
                "IdCertificadoLesionFotografia": $("#certLesionFotografias").val(),
                "Fotografia": nombreArchivo,
                "Descripcion": $("#descripcionLesion").val(),
                "TrackingCertificado": $("#serviciotrackingid").val() || ""
            };

            $.ajax({
                url: "examen_medico.aspx/SaveFotografiaLesion",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    cert: datos
                }),
                cache: false,
                success: function (data) {
                    var resultado = JSON.parse(data.d);

                    if (resultado.exitoso) {
                        ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                        $("#lesion-modal").modal("hide");
                        LimpiarLesionModal();

                        $("#dt_basic_lesiones_fotos").DataTable().destroy();
                        responsiveHelper_dt_basiclesionesFotografias = undefined;
                        LoadTablaLesionesFotos();

                        $('#main').waitMe('hide');
                    }
                    else {
                        ShowError("¡Error!", "No fue posible obtener el certificado de la lesión para subir las fotografias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible obtener el certificado de la lesión para subir las fotografias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        var responsiveHelper_dt_basiclesionesFotografias = undefined;
        var breakpointAddDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        var rutaDefaultServer = "";

        getRutaDefaultServer();

        function getRutaDefaultServer() {
            $.ajax({
                type: "POST",
                url: "examen_medico.aspx/GetRutaServer",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {
                        rutaDefaultServer = resultado.rutaDefault;
                    }
                }
            });
        }

        LoadTablaLesionesFotos();
        function LoadTablaLesionesFotos() {
            $('#dt_basic_lesiones_fotos').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basiclesionesFotografias) {
                        responsiveHelper_dt_basiclesionesFotografias = new ResponsiveDatatablesHelper($('#dt_basic_lesiones_fotos'), breakpointAddDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basiclesionesFotografias.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basiclesionesFotografias.respond();
                    $('#dt_basic_lesiones_fotos').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "examen_medico.aspx/GetLesionesFotografias",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic_lesiones_fotos').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytableadd = false;
                        parametrosServerSide.certificadoLesionFotografiasId = $("#certLesionFotografias").val();
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id",
                        visible: false
                    },
                    null,
                    null,
                    {
                        name: "Descripcion",
                        data: "Descripcion"
                    },
                    null,
                ],
                columnDefs: [
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.Fotografia != "" && row.Fotografia != null) {
                                var ext = "." + row.Fotografia.split('.').pop();
                                var photo = row.Fotografia.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.Fotografia) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var deleteButton = "";
                            if ($("#ctl00_contenido_rolid").val() != "14") {
                                if ($("#ctl00_contenido_HQLNBB").val() == "true" && $("#ctl00_contenido_LCADLW").val() == "true")
                                    deleteButton = '<a class="btn btn-danger btn-circle deleteImage" href="javascript:void(0);" data-id="' + row.Id + '" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></a>';
                            }
                            return deleteButton;
                        }
                    }
                ]
            });
        }

        $("body").on("click", ".deleteImage", function () {
            $("#fotografiaDelete").val($(this).attr("data-id"));
            $("#deleteFotografia-modal").modal("show");
        });

        $("#btnEliminarFotografiaCancel").click(function () {
            $("#fotografiaDelete").val("");
            $("#deleteFotografia-modal").modal("hide");
        })

        $("#btnEliminarFotografia").click(function () {
            $.ajax({
                url: "examen_medico.aspx/DeleteFotografiaLesion",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    id: $("#fotografiaDelete").val()
                }),
                cache: false,
                success: function (data) {
                    var resultado = JSON.parse(data.d);

                    if (resultado.exitoso) {
                        ShowSuccess("¡Bien hecho!", resultado.mensaje);
                        $("#deleteFotografia-modal").modal("hide");
                        $("#fotografiaDelete").val("");

                        $("#dt_basic_lesiones_fotos").DataTable().destroy();
                        responsiveHelper_dt_basiclesionesFotografias = undefined;
                        LoadTablaLesionesFotos();

                        $('#main').waitMe('hide');
                    }
                    else {
                        ShowError("¡Error!", "No fue posible obtener el certificado de la lesión para subir las fotografias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible obtener el certificado de la lesión para subir las fotografias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        });

        $("body").on("click", ".photoview", function (e) {
            var photo = $(this).attr("data-foto");
            $("#foto_lesion").attr("src", photo);

            $("#foto_lesion").error(function () {
                $(this).unbind("error").attr("src", rutaDefaultServer);
            });
            $("#photo-lesion-modal").modal("show");
        });

    </script>
</asp:Content>
