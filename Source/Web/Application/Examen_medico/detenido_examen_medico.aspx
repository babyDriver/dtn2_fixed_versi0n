﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="detenido_examen_medico.aspx.cs" Inherits="Web.Application.Examen_medico.detenido_examen_medico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Examen_medico/examen_medico_list.aspx">Examen médico</a></li>
    <li>Examen médico</li>
      <li>Examen médico detenido</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
            <style type="text/css">
                .modal-dialog {
    width: 700px;
    margin: 30px auto;
}

                *{
                    margin: 0;
                    padding: 0;
                }

                .magnify {
                    width: 300px;
                    margin: 50px auto;
                    position: relative;
                }

                /*Lets create the magnifying glass*/
                .large {
                    width: 175px;
                    height: 175px;
                    position: absolute;
                    border-radius: 100%;
                    /*Multiple box shadows to achieve the glass effect*/
                    box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85), 0 0 7px 7px rgba(0, 0, 0, 0.25), inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                    /*Lets load up the large image first*/
                    background: url('../../Content/img/senalesdescripcion.jpg') no-repeat;
                    /*hide the glass by default*/
                    display: none;
                }
                td.strikeout {
            text-decoration: line-through;
        }
                /*To solve overlap bug at the edges during magnification*/
                .small {
                    display: block;
                }

                .img-thumbnail {
                    height: 300px;
                }
            </style>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="glyphicon glyphicon-bookmark"></i>
                Examen médico
            </h1>
        </div>
    </div>
    <asp:Label ID="Label2" runat="server"></asp:Label>
    <div class="row padding-bottom-10">
 
       
        <a style="float: right; margin-left:3px; display: none;font-size:smaller" class="btn bg-color-red txt-color-white  btn-lg " id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i>Pertenencias</a>
        <!--<a style="float: right" class="btn bg-color-green txt-color-white btn-circle btn-lg" id="linkvistas" title="Visitas y abogados" href="javascript:void(0);"><i class="fa fa-suitcase"></i></a>-->
        <a style="float: right; margin-left:3px; display: none;font-size:smaller" class="btn bg-color-yellow txt-color-white  btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i>Antecedentes</a>
        <!--<a style="float: right; margin-left:3px; display: none;" class="btn btn-success btn-circle btn-lg" title="Adicciones" id="linkadicciones" href="javascript:void(0);"><i class=" fa fa-certificate"></i></a>-->
        <!--<a style="float: right; margin-left:3px; display: none;" class="btn bg-color-pink txt-color-white btn-circle btn-lg" id="linkfamiliares" title="Familiares" href="family.aspx"><i class="fa fa-child"></i></a>-->
        <a style="float: right; margin-left:3px; display: none;font-size:smaller" class="btn bg-color-purple txt-color-white  btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i>Señas particulares</a>
        <a style="float: right; margin-left:3px; display: none;font-size:smaller" class="btn btn-warning  btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i>Filiación</a>
        <a style="float: right; margin-left:3px; display: none;font-size:smaller" class="btn btn-info  btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i>Información personal</a>
        <a style="float: right; margin-left:3px; display: none;font-size:smaller" class="btn btn-success txt-color-white  btn-lg " id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i>Informe de detención</a> 
        <a style="float: right; margin-left:3px; display: none;font-size:smaller" class="btn btn-default  btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i>Registro</a>

    </div>
    <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-signal-1" data-widget-editbutton="false" data-widget-custombutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-reorder"></i></span>
                    <h2>Datos personales </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <div class="well">

                             <div class="row show-grid ">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <strong>Centro de reclusión</strong><br />
                                        <small><span id="centroInterno"></span></small>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <strong>Expediente</strong><br />
                                        <small><span id="expedienteInterno"></span></small>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <strong>Nombre(s)</strong><br />
                                        <small><span id="nombreInterno"></span></small>
                                    </div>
                                    <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <strong>Apellido paterno</strong><br />
                                        <small><span id="Span2">Ramos</span></small>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <strong>Apellido materno</strong><br />
                                        <small><span id="Span3">Espinosa</span></small>
                                    </div>--->
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <img id="avatar" class="img-thumbnail" height="300" alt="" runat="server" src="" />
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>

        </article>

        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-signal-2" data-widget-editbutton="false" data-widget-custombutton="false">
                <header>
                    <span class="widget-icon"><i class="glyphicon glyphicon-bookmark"></i></span>
                    <h2>Señas particulares</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body">
                        <p></p>
                        <div class="row" id="addsignal" style="display: none;">
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                <a href="javascript:void(0);"  class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                            </div>
                        </div>
                        <p></p>
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th data-class="expand">#</th>
                                    <th >Tipo</th>
                                    <th data-hide="phone,tablet">Descripción</th>
                                    <th data-hide="phone,tablet">Cantidad</th>
                                    <th data-hide="phone,tablet">Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="row smart-form">
                          <footer>  
                               <a href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>                                                                         
                             
                          </footer>
                    </div>  
                    <br />
                </div>                
            </div>
        </article>

    </div>
    </section>

    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal-title">
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-serie" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section class="col col-4">
                                <section>
                                    <label class="text">Tipo</label>
                                         <label class="input">
                                            <select name="tipo" id="tipo" style="width: 100%" class="input-sm"">
                                                
                                            </select>
                                            <i></i>
                                             </label>
                                    <div class="note note-error" style="color:red">Obligatorio</div>
                                </section>
                                <section>
                                    <label class="text">Lado</label>
                                     <label class="input">
                                            <select name="lado" id="lado" style="width: 100%" class="input-sm">
                                                
                                            </select>
                                            <i></i>
                                         </label>
                                    <div class="note note-error" style="color:red">Obligatorio</div>
                                </section>
                                <section>
                                        <label class="text">Región</label>
                                        <label class="input">
                                                <i class="icon-append fa fa-street-view"></i>
                                                <input type="text" name="region" id="region" runat="server" placeholder="Región" maxlength="2" class="input-region" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la región.</b>
                                            </label>
                                        <div class="note note-error" style="color:red">Obligatorio</div>
                                </section>
                                  <section>
                                    <label class="text">Vista</label>
                                       <label class="input">
                                            <select name="vista" id="vista" style="width: 100%" class="input-sm">
                                                
                                            </select>
                                            <i></i>
                                           </label>
                                    <div class="note note-error" style="color:red">Obligatorio</div>
                                </section>
                                 <section>
                                        <label class="text">Cantidad</label>
                                        <label class="input">
                                                <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                <input type="text" name="cantidad" id="cantidad" runat="server" placeholder="Cantidad" class="integer" maxlength="9" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la cantidad.</b>
                                            </label>
                                        <div class="note note-error" style="color:red">Obligatorio</div>
                                </section>
                                        
                          </section>
                                  <section class="col col-8">
                                      <div class="magnify">
                                          <div class="large"></div>
                                        <img id="Img1" class="small" width="300" runat="server" src="~/Content/img/senalesdescripcion.jpg" />
                                          </div>
                                    </section>
                                 </div>
                            <div class="row">
                                <section>
                                          <section>
                                        <label class="text">Descripción</label>
                                   
                                        <label class="textarea">
                                                        <i class="icon-append fa fa-comment"></i>
                                                        <textarea rows="4" name="descripcion_" id="descripcion_" placeholder="Descripción" class="alphanumeric" maxlength="256" runat="server"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Ingrese descripción</b>


                                        </label>

                                        <div class="note note-error" style="color:red">Obligatorio</div>
                                </section>
                                </section>
                            </div>
                   </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                             <input type="hidden" id="trackingid" runat="server" />
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar señal
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div4" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro de  seña <strong><span id="senaleliminar"></span></strong> será eliminado. ¿Está seguro y desea continuar?

                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div id="blockitem-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
    
    <input type="hidden" id="Ingreso" runat="server" value=""/>
    <input type="hidden" id="Informacion_Personal" runat="server" value=""/>
    <input type="hidden" id="Antropometria" runat="server" value=""/>
    <input type="hidden" id="Adicciones" runat="server" value=""/>
    <input type="hidden" id="Estudios_Criminologicos" runat="server" value=""/>
    <input type="hidden" id="Expediente_Medico" runat="server" value=""/>
    <input type="hidden" id="Familiares" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
      <script type="text/javascript">
          //1-
          $(document).ready(function () {

              pageSetUp();

              const IMAGES = document.querySelectorAll(".thumb");
              let imageActive = false;
              let normalImage;
              let bigImage;

              IMAGES.forEach(image => image.addEventListener("click", () => {
                  handleImage(image);
              }));

              const handleImage = image => {
                  if (!imageActive) openImage(image);
                  else if (imageActive) closeImage(image);
              };

              const openImage = image => {
                  imageActive = true;
                  setImageSrc(image);
                  image.parentNode.classList.add("is-active");
              };

              const closeImage = image => {
                  imageActive = false;
                  setImageSrc(image);
                  image.parentNode.classList.remove("is-active");
              };

              const setImageSrc = image => image.setAttribute("src", getImageSrc(image));

              const getImageSrc = image => {
                  let normalImage = image.getAttribute("data-thumb");
                  let bigImage = image.getAttribute("data-big");

                  if (imageActive) return bigImage;
                  else if (!imageActive) return normalImage;
              };

              init();


              var responsiveHelper_dt_basic = undefined;
              var responsiveHelper_dt_basicAlias = undefined;
              var responsiveHelper_dt_basicNombre = undefined;
              var responsiveHelper_datatable_fixed_column = undefined;
              var responsiveHelper_datatable_col_reorder = undefined;
              var responsiveHelper_datatable_tabletools = undefined;

              var breakpointDefinition = {
                  tablet: 1024,
                  phone: 480
              };

              window.table = $('#dt_basic').dataTable({
                  "lengthMenu": [10, 20, 50, 100],
                  iDisplayLength: 10,
                  serverSide: true,
                  fixedColumns: true,
                  autoWidth: true,
                  "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                  "t" +
                  "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                  "autoWidth": true,
                  "oLanguage": {
                      "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                  },
                  "preDrawCallback": function () {
                      if (!responsiveHelper_dt_basic) {
                          responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                      }
                  },
                  "rowCallback": function (nRow) {
                      responsiveHelper_dt_basic.createExpandIcon(nRow);
                  },
                  "drawCallback": function (oSettings) {
                      responsiveHelper_dt_basic.respond();
                      $('#dt_basic').waitMe('hide');
                  },

                  "createdRow": function (row, data, index) {
                      if (!data["Activo"]) {
                          $('td', row).eq(1).addClass('strikeout');
                          $('td', row).eq(2).addClass('strikeout');
                           $('td', row).eq(3).addClass('strikeout');

                      }
                  },
                  ajax: {
                      type: "POST",
                      url: "signal.aspx/getSenal",
                      contentType: "application/json; charset=utf-8",
                      data: function (parametrosServerSide) {
                          $('#dt_basic').waitMe({
                              effect: 'bounce',
                              text: 'Cargando...',
                              bg: 'rgba(255,255,255,0.7)',
                              color: '#000',
                              sizeW: '',
                              sizeH: '',
                              source: ''
                          });

                          var trackingid = ($('#<%= trackingid.ClientID %>').val());

                          parametrosServerSide.emptytable = false;
                          parametrosServerSide.tracking = trackingid;
                          return JSON.stringify(parametrosServerSide);
                      }

                  },
                  columns: [

                      {
                          data: "TrackingId",
                          targets: 0,
                          orderable: false,
                          visible: false,
                          render: function (data, type, row, meta) {
                              return "";
                          }
                      },
                      null,
                      {
                          name: "Nombre",
                          data: "Nombre"
                      },
                      {
                          name: "Descripcion",
                          data: "Descripcion"
                      },
                      {
                          name: "Cantidad",
                          data: "Cantidad"
                      },
                      null
                  ],
                  columnDefs: [

                      {
                          data: "TrackingId",
                          targets: 0,
                          orderable: false,
                          visible: false,
                          render: function (data, type, row, meta) {
                              return "";
                          }
                      },
                      {
                          targets: 1,
                          orderable: false,
                          render: function (data, type, row, meta) {
                              return meta.row + meta.settings._iDisplayStart + 1;
                          }
                      },
                      {
                          targets: 5,
                          orderable: false,
                          render: function (data, type, row, meta) {
                              var txtestatus = "";
                              var icon = "";
                              var color = "";
                              var edit = "edit";
                              var editar = "";
                              var habilitar = "";

                              if (row.Activo) {
                                  txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                              }
                              else {
                                  txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                              }

                              if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-tipo = "' + row.Tipo + '" data-lado = "' + row.Lado + '" data-region = "' + row.Region + '" data-cantidad = "' + row.Cantidad + '" data-vista = "' + row.Vista + '" data-descripcion = "' + row.Descripcion + '" data-tracking="' + row.TrackingId + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                              if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-value = "' + row.Descripcion + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                              return editar + habilitar;


                          }
                      }
                  ]
              });


              function CargarTipo(set) {
                  $('#tipo').empty();
                  $.ajax({

                      type: "POST",
                      url: "signal.aspx/getTipo",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      cache: false,
                      success: function (response) {
                          var Dropdown = $('#tipo');

                          Dropdown.append(new Option("[Tipo]", "0"));
                          $.each(response.d, function (index, item) {
                              Dropdown.append(new Option(item.Desc, item.Id));

                          });

                          if (set != "") {

                              Dropdown.val(set);
                              Dropdown.trigger("change.select2");

                          }
                      },
                      error: function () {
                          ShowError("Error!", "No fue posible cargar la lista de tipos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }

              function CargarLado(set) {
                  $('#lado').empty();
                  $.ajax({

                      type: "POST",
                      url: "signal.aspx/getLado",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      cache: false,
                      success: function (response) {
                          var Dropdown = $('#lado');

                          Dropdown.append(new Option("[Lado]", "0"));
                          $.each(response.d, function (index, item) {
                              Dropdown.append(new Option(item.Desc, item.Id));

                          });

                          if (set != "") {

                              Dropdown.val(set);
                              Dropdown.trigger("change.select2");

                          }
                      },
                      error: function () {
                          ShowError("Error!", "No fue posible cargar la lista de lados. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }

              function CargarVista(set) {
                  $('#vista').empty();
                  $.ajax({

                      type: "POST",
                      url: "signal.aspx/getVista",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      cache: false,
                      success: function (response) {
                          var Dropdown = $('#vista');

                          Dropdown.append(new Option("[Vista]", "0"));
                          $.each(response.d, function (index, item) {
                              Dropdown.append(new Option(item.Desc, item.Id));

                          });

                          if (set != "") {

                              Dropdown.val(set);
                              Dropdown.trigger("change.select2");

                          }
                      },
                      error: function () {
                          ShowError("Error!", "No fue posible cargar la lista de vista. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }
              $("body").on("click", ".blockitem", function () {
                  var itemnameblock = $(this).attr("data-value");
                  var verb = $(this).attr("style");
                  $("#itemnameblock").text(itemnameblock);
                  $("#verb").text(verb);
                  $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                  $("#blockitem-modal").modal("show");
              });

              $("#btncontinuar").unbind("click").on("click", function () {
                  var id = $(this).attr("data-id");
                  startLoading();
                  $.ajax({
                      url: "signal.aspx/blockitem",
                      type: 'POST',
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: id
                      }),
                      success: function (data) {
                          if (JSON.parse(data.d).exitoso) {
                              window.emtytable = false;
                              window.table.api().ajax.reload();
                              $("#blockitem-modal").modal("hide");
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho!</strong>" +
                                  " El registro  se actualizó correctamente.</div>");
                              ShowSuccess("Bien hecho!", "El registro  se actualizó correctamente.");
                          }
                          else {
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                  "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                              ShowError("Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }
                          $('#main').waitMe('hide');
                      }

                  });
                  window.emptytable = true;
                  window.table.api().ajax.reload();
              });
              $("body").on("click", ".search", function () {
                  window.emptytable = false;
                  window.table.api().ajax.reload();
              });

              $("body").on("click", ".clear", function () {
                  $('#tipo').val("0");
                  $('#lado').val("0");
                  $('#ctl00_contenido_region').val("");
                  $('#vista').val("0");
                  $('#ctl00_contenido_cantidad').val("");
                  $('#ctl00_contenido_descripcion_').val("");

              });

              $("body").on("click", ".add", function () {
                  $("#ctl00_contenido_lblMessage").html("");
                  $("#ctl00_contenido_region").val("");
                  $("#ctl00_contenido_cantidad").val("");
                  $("#ctl00_contenido_descripcion_").val("");
                  CargarLado(0);
                  CargarTipo(0);
                  CargarVista(0);
                  limpiar();
                  $("#btnsave").attr("data-id", "");
                  $("#btnsave").attr("data-tracking", "");
                  $("#modal-title").empty();
                  $("#modal-title").html("Agregar");
                  $("#form-modal").modal("show");



              });

              $("body").on("click", ".edit", function () {
                  limpiar();
                  $("#ctl00_contenido_lblMessage").html("");
                  var id = $(this).attr("data-id");
                  var tracking = $(this).attr("data-tracking");
                  var tipo = $(this).attr("data-tipo");
                  var lado = $(this).attr("data-lado");
                  var vista = $(this).attr("data-vista");
                  $("#btnsave").attr("data-id", id);
                  $("#btnsave").attr("data-tracking", tracking);
                  $("#ctl00_contenido_region").val($(this).attr("data-region"));
                  $("#ctl00_contenido_cantidad").val($(this).attr("data-cantidad"));
                  $("#ctl00_contenido_descripcion_").val($(this).attr("data-descripcion"));
                  $("#modal-title").empty();
                  $("#modal-title").html("Editar");
                  CargarVista(vista);
                  CargarLado(lado);
                  CargarTipo(tipo);


                  $("#form-modal").modal("show");
              });

              $("body").on("click", ".delete", function () {
                  var id = $(this).attr("data-id");
                  var nombre = $(this).attr("data-value");
                  $("#senaleliminar").text(nombre);
                  $("#btndelete").attr("data-id", id);
                  $("#delete-modal").modal("show");
              });

              $("body").on("click", ".save", function () {
                  var id = $("#btnsave").attr("data-id");
                  var tracking = $("#btnsave").attr("data-tracking");

                  if (validar()) {

                      datos = [
                          id = id,
                          tracking = tracking,
                          tipo = $("#tipo").val(),
                          lado = $("#lado").val(),
                          region = $("#ctl00_contenido_region").val(),
                          vista = $("#vista").val(),
                          cantidad = $("#ctl00_contenido_cantidad").val(),
                          descripcion = $("#ctl00_contenido_descripcion_").val(),
                          interno = $('#ctl00_contenido_trackingid').val()
                      ];


                      Save(datos);

                  }


              });

              function limpiar() {

                  $('#tipo').parent().removeClass('state-success');
                  $('#tipo').parent().removeClass("state-error");
                  $('#lado').parent().removeClass('state-success');
                  $('#lado').parent().removeClass("state-error");
                  $('#ctl00_contenido_region').parent().removeClass('state-success');
                  $('#ctl00_contenido_region').parent().removeClass("state-error");
                  $('#vista').parent().removeClass('state-success');
                  $('#vista').parent().removeClass("state-error");
                  $('#ctl00_contenido_cantidad').parent().removeClass('state-success');
                  $('#ctl00_contenido_cantidad').parent().removeClass("state-error");
                  $('#ctl00_contenido_descripcion_').parent().removeClass('state-success');
                  $('#ctl00_contenido_descripcion_').parent().removeClass("state-error");

              }

              function validar() {
                  var esvalido = true;

                  if ($("#ctl00_contenido_region").val().split(" ").join("") == "") {
                      ShowError("Región", "La región es obligatoria.");
                      $('#ctl00_contenido_region').parent().removeClass('state-success').addClass("state-error");
                      $('#ctl00_contenido_region').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#ctl00_contenido_region').parent().removeClass("state-error").addClass('state-success');
                      $('#ctl00_contenido_region').addClass('valid');
                  }


                  if ($("#ctl00_contenido_cantidad").val().split(" ").join("") == "" || $("#ctl00_contenido_cantidad").val() == "0") {
                      ShowError("Cantidad", "La cantidad es obligatoria.");
                      $('#ctl00_contenido_cantidad').parent().removeClass('state-success').addClass("state-error");
                      $('#ctl00_contenido_cantidad').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      if (isNaN($("#ctl00_contenido_cantidad").val())) { 
                      ShowError("Cantidad", "La cantidad es numérica.");
                      $('#ctl00_contenido_cantidad').parent().removeClass('state-success').addClass("state-error");
                      $('#ctl00_contenido_cantidad').removeClass('valid');
                      esvalido = false;}
                      else {
                          $('#ctl00_contenido_cantidad').parent().removeClass("state-error").addClass('state-success');
                          $('#ctl00_contenido_cantidad').addClass('valid');
                      }
                  }

                  if ($("#ctl00_contenido_descripcion_").val().split(" ").join("") == "") {
                      ShowError("Descripción", "La descripción es obligatoria.");
                      $('#ctl00_contenido_descripcion_').parent().removeClass('state-success').addClass("state-error");
                      $('#ctl00_contenido_descripcion_').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#ctl00_contenido_descripcion_').parent().removeClass("state-error").addClass('state-success');
                      $('#ctl00_contenido_descripcion_').addClass('valid');
                  }



                  if ($("#tipo").val() == null || $("#tipo").val() == "0") {
                      ShowError("Tipo", "El tipo es obligatorio.");
                      $('#tipo').parent().removeClass('state-success').addClass("state-error");
                      $('#tipo').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#tipo').parent().removeClass("state-error").addClass('state-success');
                      $('#tipo').addClass('valid');
                  }

                  if ($("#lado").val() == null || $("#lado").val() == "0") {
                      ShowError("Lado", "El lado es obligatorio.");
                      $('#lado').parent().removeClass('state-success').addClass("state-error");
                      $('#lado').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#lado').parent().removeClass("state-error").addClass('state-success');
                      $('#lado').addClass('valid');
                  }

                  if ($("#vista").val() == null || $("#vista").val() == "0") {
                      ShowError("Vista", "La vista es obligatoria.");
                      $('#vista').parent().removeClass('state-success').addClass("state-error");
                      $('#vista').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#vista').parent().removeClass("state-error").addClass('state-success');
                      $('#vista').addClass('valid');
                  }


                  return esvalido;
              }




              function Save(datos) {
                  startLoading();
                  $.ajax({
                      type: "POST",
                      url: "signal.aspx/save",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          datos: datos
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              window.emptytable = false;
                              window.table.api().ajax.reload();
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                  "La serie  se " + resultado.mensaje + " correctamente.", "<br /></div>");
                              $("#form-modal").modal("hide");
                              ShowSuccess("Bien hecho!", "La señal se " + resultado.mensaje + " correctamente.");
                          }
                          else {
                              ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                          }
                          $('#main').waitMe('hide');
                      },
                      error: function () {
                          ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                          $('#main').waitMe('hide');
                      }
                  });
              }

              function CargarDatos(trackingid) {
                  startLoading();
                  $.ajax({
                      type: "POST",
                      url: "signal.aspx/getdatos",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              $('#nombreInterno').text(resultado.obj.Nombre);
                              $('#expedienteInterno').text(resultado.obj.Expediente);
                              $('#centroInterno').text(resultado.obj.Centro);
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                              $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                              $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + resultado.obj.TrackingId);
                           //   $("#linkvistas").attr("href", "visitandlawyer.aspx?tracking=" + resultado.obj.TrackingId);
                              $("#linkestudios").attr("href", "record.aspx?tracking=" + resultado.obj.TrackingId);
                           //   $("#linkadicciones").attr("href", "addiction.aspx?tracking=" + resultado.obj.TrackingId);
                           //   $("#linkfamiliares").attr("href", "family.aspx?tracking=" + resultado.obj.TrackingId);
                              $("#linkseñas").attr("href", "signal.aspx?tracking=" + resultado.obj.TrackingId);
                              $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + resultado.obj.TrackingId);
                              $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + resultado.obj.TrackingId);
                              $("#linkingreso").attr("href", "entry.aspx?tracking=" + resultado.obj.TrackingId);
                              $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + resultado.obj.TrackingId);
                              $('#<%= trackingid.ClientID %>').val(resultado.obj.TrackingId);
                          } else {
                              ShowError("Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }

              function ResolveUrl(url) {
                  var baseUrl = "<%= ResolveUrl("~/") %>";
                  if (url.indexOf("~/") == 0) {
                      url = baseUrl + url.substring(2);
                  }
                  return url;
              }


              $("#btndelete").unbind("click").on("click", function () {
                  var id = $(this).attr("data-id");
                  startLoading();
                  $.ajax({
                      url: "signal.aspx/delete",
                      type: 'POST',
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          id: id
                      }),
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              window.emptytable = false;
                              window.table.api().ajax.reload();
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                  "La señal fue eliminada del catálogo.", "</div>");
                              ShowSuccess("Bien hecho!", "La señal fue eliminada correctamente.");
                          }
                          else {
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                  "Algo salió mal: " + resultado.mensaje + "</div>");
                          }
                          $("#delete-modal").modal("hide");
                          $('#main').waitMe('hide');
                      }
                  });
              });


              function init() {

                  var param = RequestQueryString("tracking");
                  $('#<%= trackingid.ClientID %>').val(param);
                  if (param != undefined) {
                      CargarDatos(param);
                  }

                  if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                      $("#addsignal").show();
                  }

                  if ($("#ctl00_contenido_Ingreso").val() == "true") {
                      $("#linkingreso").show();
                  }
                  if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                      $("#linkinformacion").show();
                  }
                  if ($("#ctl00_contenido_Antropometria").val() == "true") {
                      $("#linkantropometria").show();
                  }
                  if ($("#ctl00_contenido_Familiares").val() == "true") {
                      $("#linkfamiliares").show();
                  }
                  if ($("#ctl00_contenido_Estudios_Criminologicos").val() == "true") {
                      $("#linkestudios").show();
                  }
                  if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                      $("#linkexpediente").show();
                  }
                  if ($("#ctl00_contenido_Adicciones").val() == "true") {
                      $("#linkadicciones").show();
                  }
                  $("#linkseñas").show();
                  $("#linkInfoDetencion").show();
              }

              var native_width = 0;
              var native_height = 0;

              //Now the mousemove function
              $(".magnify").mousemove(function (e) {
                  //When the user hovers on the image, the script will first calculate
                  //the native dimensions if they don't exist. Only after the native dimensions
                  //are available, the script will show the zoomed version.
                  if (!native_width && !native_height) {
                      //This will create a new image object with the same image as that in .small
                      //We cannot directly get the dimensions from .small because of the 
                      //width specified to 200px in the html. To get the actual dimensions we have
                      //created this image object.
                      var image_object = new Image();
                      image_object.src = $(".small").attr("src");

                      //This code is wrapped in the .load function which is important.
                      //width and height of the object would return 0 if accessed before 
                      //the image gets loaded.
                      native_width = image_object.width;
                      console.log(image_object.width);
                      native_height = image_object.height;
                  }
                  else {
                      //x/y coordinates of the mouse
                      //This is the position of .magnify with respect to the document.
                      var magnify_offset = $(this).offset();
                      //We will deduct the positions of .magnify from the mouse positions with
                      //respect to the document to get the mouse positions with respect to the 
                      //container(.magnify)
                      var mx = e.pageX - magnify_offset.left;
                      var my = e.pageY - magnify_offset.top;

                      //Finally the code to fade out the glass if the mouse is outside the container
                      if (mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0) {
                          $(".large").fadeIn(100);
                      }
                      else {
                          $(".large").fadeOut(100);
                      }
                      if ($(".large").is(":visible")) {
                          //The background position of .large will be changed according to the position
                          //of the mouse over the .small image. So we will get the ratio of the pixel
                          //under the mouse pointer with respect to the image and use that to position the 
                          //large image inside the magnifying glass
                          var rx = Math.round(mx / $(".small").width() * native_width - $(".large").width()) * -1.1;
                          var ry = Math.round(my / $(".small").height() * native_height - $(".large").height()) * -1.1;
                          var bgp = rx + "px " + ry + "px";

                          //Time to move the magnifying glass with the mouse
                          var px = mx - $(".large").width() / 2;
                          var py = my - $(".large").height() / 2;
                          //Now the glass moves with the mouse
                          //The logic is to deduct half of the glass's width and height from the 
                          //mouse coordinates to place it with its center at the mouse coordinates

                          //If you hover on the image now, you should see the magnifying glass in action
                          $(".large").css({ left: px, top: py, backgroundPosition: bgp });
                      }
                  }
              });

                $("#ctl00_contenido_cantidad").keyup(function () {
                    this.value = (this.value + '').replace(/[^0-9]/g, '');
                });
          });



       </script>
</asp:Content>
