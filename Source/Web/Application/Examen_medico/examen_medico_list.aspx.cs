﻿using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;
using System.Linq;


namespace Web.Application.Examen_medico
{
    public partial class examen_medico_list : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validatelogin();
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor == "")
                Response.Redirect("dashboard.aspx");

            if (!IsPostBack)
            {
                tbFechaInicial.Text = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                tbFechaFinal.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Examen médico" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                 


                    

                     
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static string pdf(int datos)
        {
            var serializedObject = string.Empty;
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");
                    Entity.ExamenMedico Examen = ControlExamenMedico.ObtenerPorId(datos);
                    Entity.DetalleDetencion detencion = ControlDetalleDetencion.ObtenerPorId(Examen.DetalleDetencionId);
                    var interno = ControlDetenido.ObtenerPorId(detencion.DetenidoId);

                    string[] Id = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(Id);

                    var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);

                    if (examenmedico.Count > 0)
                    {
                        var obj = ControlPDFEM.ReporteEM(interno.Id, interno.TrackingId, datos);
                        serializedObject = JsonConvert.SerializeObject(obj);
                    }
                    else
                    {
                        serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El detenido no cuenta con examen médico." });
                    }

                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static Object pdf(int detenidoId, int examenId)
        {
            var serializedObject = string.Empty;
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");
                    Entity.ServicioMedico servicioMedico = new Entity.ServicioMedico();
                    if (detenidoId == 1)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoPsicofisiologico(servicioMedico);

                        var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var det = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = det.Id;
                        reportesLog.ProcesoTrackingId = det.TrackingId.ToString();
                        reportesLog.Modulo = "Examen médico";
                        reportesLog.Reporte = "Certificado psicofisiológico";
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.EstatusId = 2;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);


                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }
                    if (detenidoId == 2)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoLesion(servicioMedico);

                        var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var det = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = det.Id;
                        reportesLog.ProcesoTrackingId = det.TrackingId.ToString();
                        reportesLog.Modulo = "Examen médico";
                        reportesLog.Reporte = "Certificado de lesiones";
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.EstatusId = 2;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);
                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }
                    if (detenidoId == 3)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        Entity.ImprimeResultadoCertificadoQuimico resultadoCertificadoQuimico = new Entity.ImprimeResultadoCertificadoQuimico();
                        resultadoCertificadoQuimico.PrintEtanol = true;
                        resultadoCertificadoQuimico.PrintBenzodiazepina = true;
                        resultadoCertificadoQuimico.PrintAnfetamina = true;
                        resultadoCertificadoQuimico.PrintCannabis = true;
                        resultadoCertificadoQuimico.PrintCocaina = true;
                        resultadoCertificadoQuimico.PrintExtasis = true;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoQuimico(servicioMedico, resultadoCertificadoQuimico);

                        var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var det = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = det.Id;
                        reportesLog.ProcesoTrackingId = det.TrackingId.ToString();
                        reportesLog.Modulo = "Examen médico";
                        reportesLog.Reporte = "Certificado químico";
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.EstatusId = 2;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);

                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }

                    var detenido = ControlDetenido.ObtenerPorId(detenidoId);

                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                    //var obj = ControlPDFEM.ReporteEM(detenido.Id, detenido.TrackingId, examenId);
                    //serializedObject = JsonConvert.SerializeObject(obj);
                    //return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }


        [WebMethod]
        public static DataTable getinterno(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
            {
                try
                {

                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("I.TrackinngId", tracking));
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                        string IdsContratos = "(";

                        int i = 0;

                        foreach (var item in dataContratos)
                        {
                            if (i == dataContratos.Count - 1)
                            {
                                IdsContratos += item.IdContrato + ")";
                            }
                            else
                            {
                                IdsContratos += item.IdContrato + ",";
                            }

                            i++;
                        }

                        //where.Add(new Where("E.ContratoId", Where.IN, IdsContratos));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);

                        }
                        Query query = new Query
                        {
                            select = new List<string> {
                        "I.Id",
                        "I.trackinngId",
                        //"date_format(I.FechaRegistro, '%Y-%m-%d') FechaRegistro",
                        "date_format(I.FechaRegistro, '%Y-%m-%d %H:%i:%S') FechaRegistro",
                        "ifnull(I.CertificadoQuimicoId,0) CertificadoQuimicoId",
                        "ifnull(I.CertificadoLesionId,0) CertificadoLesionId",
                        "ifnull(I.CertificadoMedicoPsicofisiologicoId,0) CertificadoMedicoPsicofisiologicoId",
                        "ifnull(Observaciones_antecedentes,'') Observaciones",
                        "I.Id ExamenMedicoId",
                        "ifnull(V.Antecedente,'') Antecedente",

                    },
                            from = new Table("servicio_medico", "I"),
                            joins = new List<Join>
                    {
                        new Join(new Table("certificado_lesion", "E"), "I.CertificadoLesionId  = E.Id"),
                      new Join(new Table("vAntecedentesmedico", "V"), "V.certificado_LesionAntecedentesId=E.AntecedentesId"),
                      
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }


        [WebMethod]
        public static DataTable getdetenidosexistencia(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string fechaInicial, string fechaFinal)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);


                        where.Add(new Where("E.Estatus", "<>", "2"));
                        where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("E.Tipo", contratoUsuario.Tipo));
                        var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                        where.Add(new Where("ifnull(Saj.TiposalidaId,0)", "<>", tipo.Id.ToString()));
                        where.Add(new Where("cast(E.fecha as date)", ">=", fechaInicial));
                        where.Add(new Where("cast(E.fecha as date)", "<=", fechaFinal));

                        Query query = new Query
                        {
                            select = new List<string> {
                        "I.Id",
                        "I.TrackingId",
                        "R.Nombre",
                        "I.Paterno",
                        "I.Materno",
                        "E.Expediente",
                        "E.NCP",
                        "E.Estatus",
                        "E.Activo",
                        "E.TrackingId TrackingIdEstatus" ,
                       "I.RutaImagen",
                        "IFNULL(a.alias, '') AliasInterno",
                        "IFNULL(G.fechanacimineto, NOW()) FechaNacimiento"
                    },
                            from = new Table("detenido", "I"),
                            joins = new List<Join>
                    {
                        new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                        new Join(new Table("(select DetenidoId, group_concat(Alias) alias from Alias group by DetenidoId)", "A"), "I.id  = A.DetenidoId", "LEFT OUTER"),
                        new Join(new Table("general", "G"), "I.id  = G.DetenidoId", "LEFT OUTER"),
                        new Join(new Table("(Select Concat(Nombre,' ',Paterno,' ',Materno)  Nombre,Id from detenido)", "R"), "I.id  = R.Id", "LEFT OUTER"),
                        new Join(new Table("salidaefectuadajuez", "Saj"), "E.id=Saj.DetalleDetencionId and Saj.Activo=1", "LEFT")
                    },
                            wheres = where
                        };

                        DataTablesAux dt = new DataTablesAux(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }




        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable,string fechaInicial, string fechaFinal)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                    {


                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                        if (contratoUsuario != null)
                        {
                            where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("E.Tipo", contratoUsuario.Tipo));
                        }

                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Estatus", "<>", "2"));
                        var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                        where.Add(new Where("ifnull(Saj.TiposalidaId,0)", "<>", tipo.Id.ToString()));
                        where.Add(new Where("cast(E.fecha as date)", ">=", fechaInicial));
                        where.Add(new Where("cast(E.fecha as date)", "<=", fechaFinal));

                        Query query = new Query
                        {
                            select = new List<string> 
                            {
                                "I.Id",
                                "I.TrackingId",
                                "I.Nombre",
                                "TRIM(I.Paterno) Paterno",
                                "TRIM(I.Materno) Materno",
                                "CAST(ifnull(ExpedienteoOriginal,Expediente) AS unsigned) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus" ,
                                "I.RutaImagen",
                                "EM.Id ExamenId",
                                "EM.TrackinngId ExamenTrackingId",
                                //"DATE_FORMAT(EM.FechaRegistro,'%Y-%m-%d %H:%i') Evaluacion",
                                "DATE_FORMAT(EM.FechaUltimaModificacion,'%Y-%m-%d %H:%i') Evaluacion",
                                "DATE_FORMAT(ID.HoraYFecha,'%Y-%m-%d %H:%i') Registro",
                                //"DATE_FORMAT(E.Fecha,'%Y-%m-%d %H:%i') Registro",
                                "sd.Nombre Situacion",
                                "concat(I.Nombre,' ',TRIM(Paterno),' ',TRIM(Materno)) as NombreCompleto"
                            },
                            from = new Table("detenido", "I"),
                            joins = new List<Join>
                            {
                                new Join(new Table("informaciondedetencion", "ID"), "I.Id = ID.IdInterno"),
                                new Join(new Table("detalle_detencion", "E"), "I.Id  = E.DetenidoId"),
                                new Join(new Table("servicio_medico", "EM"), "E.Id  = EM.DetalledetencionId"),
                                new Join(new Table("calificacion", "ca"), "ca.InternoId  = E.Id","left"),
                                new Join(new Table("situacion_detenido", "sd"), "sd.Id  = ca.SituacionId"),
                                new Join(new Table("(Select Id,ifnull(Expediente,0) as ExpedienteoOriginal,DetalleDetencionId from historico_agrupado_detenido)", "H"), "H.DetalleDetencionId=E.id"),
                                new Join(new Table("salidaefectuadajuez", "Saj"), "E.id=Saj.DetalleDetencionId and Saj.Activo=1", "LEFT")
                            },
                            wheres = where
                        };

                        DataTablesAux dt = new DataTablesAux(mysqlConnection);
                        var result1 = dt.Generar(query, draw, start, length, search, order, columns);
                        return result1;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string delete(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Eliminar)
                {
                    Guid tracking = new Guid(trackingid);
                    Entity.ExamenMedico item = ControlExamenMedico.ObtenerPorTrackingId(tracking);
                    item.Activo = false;
                    ControlExamenMedico.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }



        [WebMethod]
        public static string blockitem(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Eliminar)
                {
                    Entity.ExamenMedico item = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));
                    item.Activo = item.Activo ? false : true;
                    ControlExamenMedico.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }


    }
}