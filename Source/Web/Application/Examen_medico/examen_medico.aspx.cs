﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;
using System.IO;

namespace Web.Application.Examen_medico
{
    public partial class examen_medico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validatelogin();
            getPermisos();
            string action = Convert.ToString(Request.QueryString["action"]);
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor != "")
                switch (action)
                {
                    case "add":
                        this.tracking.Value = valor;
                        break;
                    case "update":
                        Entity.ServicioMedico em = ControlServicoMedico.ObtenerPorTrackingId(valor);
                        //Entity.PruebaExamenMedico pe = ControlPruebaExamenMedico.ObtenerPorExamenMedicoId(em.Id);
                        //if (pe != null)
                        //    this.PruebaTracking.Value = pe.TrackingId.ToString();
                        Entity.DetalleDetencion detencion = ControlDetalleDetencion.ObtenerPorId(em.DetalledetencionId);
                        this.tracking.Value = detencion.TrackingId.ToString();
                        this.ExamenTracking.Value = valor;

                        break;
                }

            else
                Response.Redirect("examen_medico_list.aspx");
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Examen médico" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                        var user = ControlUsuario.Obtener(usuario);
                        this.rolid.Value = user.RolId.ToString();


                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static string GetParametrosTamaño()
        {
            try
            {
                var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                var parametro = ControlParametroContrato.TraerTodos();
                decimal  maximo = 0;
                decimal minimo = 0;

                foreach (var item in parametro)
                {
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 13)
                    {
                        minimo = Convert.ToDecimal(item.Valor);
                    }
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 14)
                    {
                        maximo = Convert.ToDecimal(item.Valor);
                    }
                }
                return JsonConvert.SerializeObject(new { exitoso = true, TMax = maximo, Tmin = minimo });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static string ValidateFecha(string[] info)
        {
            try
            {
                var detalle = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(info[0]));
                var fecha = Convert.ToDateTime(info[1]);
                var infoDet = ControlInformacionDeDetencion.ObtenerPorInternoId(detalle.DetenidoId);
                //int result = DateTime.Compare(fecha,Convert.ToDateTime(detalle.Fecha));
                int result = DateTime.Compare(fecha,Convert.ToDateTime(infoDet.HoraYFecha));
                Boolean validate = true;
                if (result<0)
                {
                    validate = false;
                }
                //var fechar = Convert.ToDateTime(detalle.Fecha);
                var fechar = Convert.ToDateTime(infoDet.HoraYFecha);
                var h = fechar.Hour.ToString();
                if (fechar.Hour < 10) h = "0" + h;
                var m = fechar.Minute.ToString();
                if (fechar.Minute < 10) m = "0" + m;
                var s = fechar.Second.ToString();
                if (fechar.Second < 10) s = "0" + s;
                var registro = fechar.ToShortDateString() + " " + h + ":" + m + ":" + s;
                return JsonConvert.SerializeObject(new { exitoso = true, Validar = validate, fechaRegistro = registro }); 
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }
        
        [WebMethod]
        public static object savecertificadoquimico(CertificadoquimicoAux examen)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var examenNuevo = new Entity.ExamenMedico();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    // string id = examen.Id;
                    //// if (id == "") id = "0";
                    //int dias = 0;
                    //if (examen.DiasSanarLesiones != "")
                    //    dias = Convert.ToInt32(examen.DiasSanarLesiones);
                    //Entity.DetalleDetencion detalle_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.DetalleDetencionTracking));
                    //examenNuevo.DetalleDetencionId = detalle_detencion.Id;
                    if (string.IsNullOrEmpty(examen.ServiciomedicoTrackingId))
                    {
                        mode = @"registró";
                        var detalledet = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.TrackingId));
                        Entity.CertificadoQuimico certificadoQuimico = new Entity.CertificadoQuimico();

                        certificadoQuimico = ControlCertificadoQuimico.ObtenerPorFolio(new string[] {examen.Folio,detalledet.ContratoId.ToString() });
                        //if (certificadoQuimico != null)
                        //{
                        //    return new { exitoso = false, alert = true, mensaje = "Ya existe un certificado químico con el folio: " + examen.Folio };
                        //}

                        certificadoQuimico = new Entity.CertificadoQuimico();
                        certificadoQuimico.TrackingId = Guid.NewGuid().ToString(); ;
                        certificadoQuimico.Fecha_toma =Convert.ToDateTime( examen.Fecha_toma);
                        certificadoQuimico.Fecha_proceso = Convert.ToDateTime(examen.Fecha_proceso);
                        certificadoQuimico.EtanolId = Convert.ToInt32(examen.EtanolId);
                        certificadoQuimico.Grado = Convert.ToInt32(examen.Grado!=""?examen.Grado:"0");
                        certificadoQuimico.BenzodiapinaId = Convert.ToInt32(examen.BenzodiapinaId);
                        certificadoQuimico.AnfetaminaId = Convert.ToInt32(examen.AnfetaminaId);
                        certificadoQuimico.CannabisId = Convert.ToInt32(examen.CannabisId);
                        certificadoQuimico.CocainaId = Convert.ToInt32(examen.CocaínaId);
                        certificadoQuimico.ExtasisId = Convert.ToInt32(examen.ExtasisId);
                        certificadoQuimico.EquipoautilizarId = Convert.ToInt32(examen.EquipoautilizarId);

                        certificadoQuimico.ContratoId = detalledet.ContratoId;
                        certificadoQuimico.FechaRegistro = DateTime.Now;
                        certificadoQuimico.Registradopor=usId;
                        certificadoQuimico.Foliocertificado = examen.Folio;



                        Entity.ServicioMedico servicioMedico = new Entity.ServicioMedico();

                        servicioMedico.CertificadoQuimicoId = ControlCertificadoQuimico.Guardar(certificadoQuimico);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = certificadoQuimico.Fecha_proceso;
                        historial.Habilitado = true;
                        historial.InternoId = detalledet.DetenidoId;
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Movimiento = "Modificación de datos generales en certificado químico";
                        historial.TrackingId = Guid.NewGuid();
                        //historial.Id = ControlHistorial.Guardar(historial);

                        historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = certificadoQuimico.Fecha_proceso;
                        historial.Habilitado = true;
                        historial.InternoId = detalledet.DetenidoId;
                        historial.ContratoId = subcontrato.Id;
                        historial.Movimiento = "Modificación de datos generales en certificado químico fecha de proceso";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);
                        servicioMedico.DetalledetencionId = detalledet.Id;
                        int serviciomedicoID = 0;
                        servicioMedico.Activo = 1;
                        servicioMedico.TrackinngId = Guid.NewGuid().ToString();
                        servicioMedico.RegistradoPor = usId;
                        servicioMedico.FechaRegistro = DateTime.Now;
                        servicioMedico.Habilitado = 1;
                        serviciomedicoID = ControlServicoMedico.Guardar(servicioMedico);

                        var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedicoID);
                        if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                        {
                            var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                            historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                            historialServicioMedicoObj.IdHistorial = historial.Id;
                            historialServicioMedicoObj.IdServicioMedico = serviciomedicoID;
                            ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                        }
                        historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = certificadoQuimico.Fecha_toma;
                        historial.Habilitado = true;
                        historial.InternoId = detalledet.DetenidoId;
                        historial.ContratoId = subcontrato.Id;
                        historial.Movimiento = "Modificación de datos generales en certificado químico fecha de toma";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);
                        historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedicoID);
                        if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                        {
                            var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                            historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                            historialServicioMedicoObj.IdHistorial = historial.Id;
                            historialServicioMedicoObj.IdServicioMedico = serviciomedicoID;
                            ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                        }
                        

                        
                        

                        

                        var certquim = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);
                        string msj = "El certificado químico se registró  correctamente.";


                        Entity.ImprimeResultadoCertificadoQuimico resultado = new Entity.ImprimeResultadoCertificadoQuimico();
                        resultado.PrintEtanol = Convert.ToBoolean(examen.PrintEtanol);
                        resultado.PrintBenzodiazepina = Convert.ToBoolean(examen.PrintBenzodiazepina);
                        resultado.PrintAnfetamina = Convert.ToBoolean(examen.PrintAnfetamina);
                        resultado.PrintCannabis = Convert.ToBoolean(examen.PrintCannabis);
                        resultado.PrintCocaina = Convert.ToBoolean(examen.PrintCocaina);
                        resultado.PrintExtasis = Convert.ToBoolean(examen.PrintExtasis);

                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoQuimico(servicioMedico,resultado);

                        var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = detenido.Id;
                        reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                        reportesLog.Modulo = "Examen médico";
                        reportesLog.Reporte = "Certificado químico";
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.EstatusId = 1;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);

                        return new {  exitoso = true, mensaje = msj, ServicioMedTracingId=servicioMedico.TrackinngId,Folio= examen.Folio.ToString() , Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                        //var tmpmail = String.Concat(cliente.Nombre, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

                    }
                    else
                    {
                        mode = @"actualizó";

                        var serviciomedico = ControlServicoMedico.ObtenerPorTrackingId(examen.ServiciomedicoTrackingId);

                        //int certificadoquimicoId=serviciomedico!=null?ser

                        if (serviciomedico.CertificadoQuimicoId == 0)
                        {
                            mode = @"registró";


                            var detalledet = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.TrackingId));
                            Entity.CertificadoQuimico certificadoQuimico = new Entity.CertificadoQuimico();
                            certificadoQuimico = ControlCertificadoQuimico.ObtenerPorFolio(new string[] { examen.Folio, detalledet.ContratoId.ToString() });
                            if (certificadoQuimico != null)
                            {

                                return new { exitoso = false,alert=true,mensaje = "Ya existe un certificado químico con el folio: " + examen.Folio };
                            }
                            certificadoQuimico = new Entity.CertificadoQuimico();



                            certificadoQuimico.TrackingId = Guid.NewGuid().ToString(); ;
                            certificadoQuimico.Fecha_toma = Convert.ToDateTime(examen.Fecha_toma);
                            certificadoQuimico.Fecha_proceso = Convert.ToDateTime(examen.Fecha_proceso);
                            certificadoQuimico.EtanolId = Convert.ToInt32(examen.EtanolId);
                            certificadoQuimico.Grado = Convert.ToInt32(!string.IsNullOrEmpty(examen.Grado)?examen.Grado:"0");
                            certificadoQuimico.BenzodiapinaId = Convert.ToInt32(examen.BenzodiapinaId);
                            certificadoQuimico.AnfetaminaId = Convert.ToInt32(examen.AnfetaminaId);
                            certificadoQuimico.CannabisId = Convert.ToInt32(examen.CannabisId);
                            certificadoQuimico.CocainaId = Convert.ToInt32(examen.CocaínaId);
                            certificadoQuimico.ExtasisId = Convert.ToInt32(examen.ExtasisId);
                            certificadoQuimico.EquipoautilizarId = Convert.ToInt32(examen.EquipoautilizarId);
                            certificadoQuimico.ContratoId = detalledet.ContratoId;

                            certificadoQuimico.ContratoId = detalledet.ContratoId;
                            certificadoQuimico.FechaRegistro = DateTime.Now;
                            certificadoQuimico.Registradopor = usId;

                     
                            certificadoQuimico.Foliocertificado = examen.Folio;
                            serviciomedico.CertificadoQuimicoId = ControlCertificadoQuimico.Guardar(certificadoQuimico);
                            serviciomedico.Activo = 1;

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = certificadoQuimico.Fecha_proceso;
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.Movimiento = "Modificación de datos generales en certificado químico";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                           // historial.Id = ControlHistorial.Guardar(historial);

                            

                            historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = certificadoQuimico.Fecha_toma;
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.ContratoId = subcontrato.Id;
                            historial.Movimiento = "Modificación de datos generales en certificado químico fecha de toma";
                            historial.TrackingId = Guid.NewGuid();
                            historial.Id = ControlHistorial.Guardar(historial);

                            serviciomedico.ModificadoPor = usId;
                            serviciomedico.FechaUltimaModificacion = DateTime.Now;
                            serviciomedico.Habilitado = 1;


                            serviciomedico.DetalledetencionId = detalledet.Id;

                            ControlServicoMedico.Actualizar(serviciomedico);

                            var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedico.Id);
                            if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                            {
                                var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                                historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                                historialServicioMedicoObj.IdHistorial = historial.Id;
                                historialServicioMedicoObj.IdServicioMedico = serviciomedico.Id;
                                ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                            }
                            historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = certificadoQuimico.Fecha_proceso;
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.ContratoId = subcontrato.Id;
                            historial.Movimiento = "Modificación de datos generales en certificado químico fecha de proceso";
                            historial.TrackingId = Guid.NewGuid();
                            historial.Id = ControlHistorial.Guardar(historial);

                            historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedico.Id);
                            if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                            {
                                var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                                historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                                historialServicioMedicoObj.IdHistorial = historial.Id;
                                historialServicioMedicoObj.IdServicioMedico = serviciomedico.Id;
                                ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                            }

                            var certquim = ControlCertificadoQuimico.ObtenerPorId(serviciomedico.CertificadoQuimicoId);
                            string msj = "El certificado químico se registró correctamente.";

                            Entity.ImprimeResultadoCertificadoQuimico resultado = new Entity.ImprimeResultadoCertificadoQuimico();
                            resultado.PrintEtanol = Convert.ToBoolean(examen.PrintEtanol);
                            resultado.PrintBenzodiazepina = Convert.ToBoolean(examen.PrintBenzodiazepina);
                            resultado.PrintAnfetamina = Convert.ToBoolean(examen.PrintAnfetamina);
                            resultado.PrintCannabis = Convert.ToBoolean(examen.PrintCannabis);
                            resultado.PrintCocaina = Convert.ToBoolean(examen.PrintCocaina);
                            resultado.PrintExtasis = Convert.ToBoolean(examen.PrintExtasis);

                            object[] dataArchivo = null;
                            dataArchivo = ControlPDFServicioMedico.GeneraCertificadoQuimico(serviciomedico,resultado);
                            var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(serviciomedico.DetalledetencionId);
                            var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                            Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                            reportesLog.ProcesoId = detenido.Id;
                            reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                            reportesLog.Modulo = "Examen médico";
                            reportesLog.Reporte = "Certificado químico";
                            reportesLog.Fechayhora = DateTime.Now;
                            reportesLog.EstatusId = 1;
                            reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                            ControlReportesLog.Guardar(reportesLog);
                            return new { exitoso = true, mensaje = msj, ServicioMedTracingId = serviciomedico.TrackinngId, Folio = examen.Folio.ToString(), Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                        }
                        else

                        {
                            var detalledet = ControlDetalleDetencion.ObtenerPorId(serviciomedico.DetalledetencionId);
                            var certQuimi = ControlCertificadoQuimico.ObtenerPorId(serviciomedico.CertificadoQuimicoId);

                            var certificadoQuimico = ControlCertificadoQuimico.ObtenerPorFolio(new string[] { examen.Folio, detalledet.ContratoId.ToString() });
                            // we4pon
                            if (certificadoQuimico != null)
                            {
                                if (certificadoQuimico.Id != certQuimi.Id)
                                {

                                    return new { exitoso = false, alert = true, mensaje = "Ya existe un certificado químico con el folio: " + examen.Folio };
                                }
                            }
                            certQuimi.Fecha_toma = Convert.ToDateTime(examen.Fecha_toma);
                            certQuimi.Fecha_proceso = Convert.ToDateTime(examen.Fecha_proceso);
                            certQuimi.EtanolId = Convert.ToInt32(examen.EtanolId);
                            certQuimi.Grado = Convert.ToInt32(examen.Grado!=""?examen.Grado:"0");
                            certQuimi.BenzodiapinaId = Convert.ToInt32(examen.BenzodiapinaId);
                            certQuimi.AnfetaminaId = Convert.ToInt32(examen.AnfetaminaId);
                            certQuimi.CannabisId = Convert.ToInt32(examen.CannabisId);
                            certQuimi.CocainaId = Convert.ToInt32(examen.CocaínaId);
                            certQuimi.ExtasisId = Convert.ToInt32(examen.ExtasisId);
                            certQuimi.EquipoautilizarId = Convert.ToInt32(examen.EquipoautilizarId);
                            certQuimi.Foliocertificado = examen.Folio;

                            certQuimi.Fechaultimamodificacion = DateTime.Now;
                            certQuimi.Modificadopor = usId;

                            ControlCertificadoQuimico.Actualizar(certQuimi);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = certQuimi.Fecha_proceso;
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.Movimiento = "Modificación de datos generales en certificado químico";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            //historial.Id = ControlHistorial.Guardar(historial);
                            

                            historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = certificadoQuimico.Fecha_toma;
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.ContratoId = subcontrato.Id;
                            historial.Movimiento = "Modificación de datos generales en certificado químico fecha de toma";
                            historial.TrackingId = Guid.NewGuid();
                            historial.Id = ControlHistorial.Guardar(historial);

                            var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedico.Id);
                            if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                            {
                                var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                                historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                                historialServicioMedicoObj.IdHistorial = historial.Id;
                                historialServicioMedicoObj.IdServicioMedico = serviciomedico.Id;
                                ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                            }


                            historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = certificadoQuimico.Fecha_proceso;
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.ContratoId = subcontrato.Id;
                            historial.Movimiento = "Modificación de datos generales en certificado químico fecha de proceso";
                            historial.TrackingId = Guid.NewGuid();
                            historial.Id = ControlHistorial.Guardar(historial);

                            historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedico.Id);
                            if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                            {
                                var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                                historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                                historialServicioMedicoObj.IdHistorial = historial.Id;
                                historialServicioMedicoObj.IdServicioMedico = serviciomedico.Id;
                                ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                            }

                            string msj = "El certificado químico se actualizó correctamente.";
                            Entity.ImprimeResultadoCertificadoQuimico resultado = new Entity.ImprimeResultadoCertificadoQuimico();
                            resultado.PrintEtanol = Convert.ToBoolean(examen.PrintEtanol);
                            resultado.PrintBenzodiazepina = Convert.ToBoolean(examen.PrintBenzodiazepina);
                            resultado.PrintAnfetamina = Convert.ToBoolean(examen.PrintAnfetamina);
                            resultado.PrintCannabis = Convert.ToBoolean(examen.PrintCannabis);
                            resultado.PrintCocaina = Convert.ToBoolean(examen.PrintCocaina);
                            resultado.PrintExtasis = Convert.ToBoolean(examen.PrintExtasis);
                            object[] dataArchivo = null;
                            dataArchivo = ControlPDFServicioMedico.GeneraCertificadoQuimico(serviciomedico,resultado);

                            var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(serviciomedico.DetalledetencionId);
                            var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                            Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                            reportesLog.ProcesoId = detenido.Id;
                            reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                            reportesLog.Modulo = "Examen médico";
                            reportesLog.Reporte = "Certificado químico";
                            reportesLog.Fechayhora = DateTime.Now;
                            reportesLog.EstatusId = 2;
                            reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                            ControlReportesLog.Guardar(reportesLog);

                            return new { exitoso = true, mensaje = msj, ServicioMedTracingId = serviciomedico.TrackinngId, Folio = examen.Folio.ToString(), Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                        }
                    }

                    //int[] filtro = { detalle_detencion.ContratoId, detalle_detencion.DetenidoId };
                    //var hit = ControlCrossreference.GetCrossReferences(filtro);
                    string msg = "";
                    //var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                    bool fhit = false;
                    //if (hit.Count > 0)
                    //{
                    //    fhit = true;
                    //    msg = "La información del examen medico se " + mode + " correctamente.   El detenido " + detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno + " tiene un hit";
                    //}

                    return new { ishit = "", msghit = msg, exitoso = true, mensaje = mode, Id = examenNuevo.Id.ToString(), TrackingId = examenNuevo.TrackingId, DetenidoId ="0", Observacion = examenNuevo.Observacion };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object savecertificadoLesion(CertificadoLesionaux examen)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var examenNuevo = new Entity.ExamenMedico();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    // string id = examen.Id;
                    //// if (id == "") id = "0";
                    //int dias = 0;
                    //if (examen.DiasSanarLesiones != "")
                    //    dias = Convert.ToInt32(examen.DiasSanarLesiones);
                    //Entity.DetalleDetencion detalle_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.DetalleDetencionTracking));
                    //examenNuevo.DetalleDetencionId = detalle_detencion.Id;
                   
                    var usuario = ControlUsuario.Obtener(usId);
                    var roles = ControlRol.ObtenerPorId(usuario.RolId);
                    if(roles.name== "Químico")
                    {
                        throw new Exception("El usuario no cuenta con los permisos para generar el certificado de lesiones.");
                    }
                    var observaciones_antecedentes = examen.Observaciones_antecedentes;
                    if (!string.IsNullOrEmpty(observaciones_antecedentes) && observaciones_antecedentes.Trim().Last() != '.')
                        observaciones_antecedentes = observaciones_antecedentes.Trim() + ".";

                    var observaciones_lesion = examen.Observaciones_lesion;
                    if (!string.IsNullOrEmpty(observaciones_lesion) && observaciones_lesion.Trim().Last() != '.')
                        observaciones_lesion = observaciones_lesion.Trim() + ".";

                    var observacion_alergia = examen.Observacion_alergia;
                    if (!string.IsNullOrEmpty(observacion_alergia) && observacion_alergia.Trim().Last() != '.')
                        observacion_alergia = observacion_alergia.Trim() + ".";

                    var observacion_general = examen.Observacion_general;
                    if (!string.IsNullOrEmpty(observacion_general) && observacion_general.Trim().Last() != '.')
                        observacion_general = observacion_general.Trim() + ".";

                    var observacion_tatuje = examen.Observacion_tatuje;
                    if (!string.IsNullOrEmpty(observacion_tatuje) && observacion_tatuje.Trim().Last() != '.')
                        observacion_tatuje = observacion_tatuje.Trim() + ".";

                    if (string.IsNullOrEmpty(examen.ServiciomedicoTrackingId))
                    {
                        mode = @"registró";
                        var detalledet = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.TrackingId));

                        Entity.ServicioMedico servicioMedico = new Entity.ServicioMedico();
                        Entity.CertificadoLesion certificadoLesion = new Entity.CertificadoLesion();
                        Entity.CertificadoLesionTipoLesion certificadoLesionTipoLesion = new Entity.CertificadoLesionTipoLesion();

                        certificadoLesionTipoLesion.Creadopor = usId;
                        certificadoLesionTipoLesion.FechaHora = DateTime.Now;

                        int idtipolesion=0;

                        List<string> tipolesionid = new List<string>();
                        if (examen.Tipo_lesionId != null)
                        {
                            idtipolesion = ControlCertificadoLesionTipoLesion.Guardar(certificadoLesionTipoLesion);
                            foreach (var item in examen.Tipo_lesionId)
                            {
                                Entity.CertificadoLesionTipoLesionDetalle tipoLesionDetalle = new Entity.CertificadoLesionTipoLesionDetalle();
                                tipoLesionDetalle.Certificado_LesionTipoLesionId = idtipolesion;
                                tipoLesionDetalle.TipoLesionId = Convert.ToInt32(item);
                                ControlCertificadoLesionTipoLesionDetalle.Guardar(tipoLesionDetalle);

                            }
                        }
                        certificadoLesion.Tipo_lesionId = idtipolesion;
                        certificadoLesion.TrackingId = Guid.NewGuid().ToString();
                        
                        certificadoLesion.Observaciones_lesion = observaciones_lesion;

                        if (examen.Tipo_alergiaId !=null)
                        {
                                Entity.CertificadoLesionAlergia certificadoLesionAlergia = new  Entity.CertificadoLesionAlergia();
                                certificadoLesionAlergia.Creadopor = usId;
                                certificadoLesionAlergia.FechaHora = DateTime.Now;

                            int idAlergia=ControlCertificadoLesionAlergia.Guardar(certificadoLesionAlergia);
                            foreach (var item in examen.Tipo_alergiaId)
                            {
                                Entity.CertificadoLesionAlergiaDetalle certificadoLesionAlergiaDetalle = new Entity.CertificadoLesionAlergiaDetalle();

                                certificadoLesionAlergiaDetalle.AlergiaId = Convert.ToInt32(item);
                                certificadoLesionAlergiaDetalle.certificado_LesionAlergiaId = idAlergia;
                                ControlCertificadoLesionAlergiaDetalle.Guardar(certificadoLesionAlergiaDetalle);
                                    
                            }
                            certificadoLesion.Tipo_alergiaId = idAlergia;
                            
                        }
                        certificadoLesion.Observacion_alergia = observacion_alergia;
                        if (examen.AntecedentesId != null)
                        {

                            Entity.CertificadoLesionAntecedentes antecedentes = new Entity.CertificadoLesionAntecedentes();
                            antecedentes.Creadopor = usId;
                            antecedentes.FechaHora = DateTime.Now;
                            int antecedenteid =ControlCertificadoLesionAntecedentes.Guardar(antecedentes);
                            foreach (var item in examen.AntecedentesId)
                            {
                                Entity.CertificadoAntecedentesDetalle certificadoLesionAlergiaDetalle = new Entity.CertificadoAntecedentesDetalle();

                                certificadoLesionAlergiaDetalle.AntecedenteId = Convert.ToInt32(item);
                                certificadoLesionAlergiaDetalle.certificado_LesionAntecedentesId = antecedenteid;
                                ControlCertificadoLesionAntecedentesDetalle.Guardar(certificadoLesionAlergiaDetalle);
                            }

                            certificadoLesion.AntecedentesId = antecedenteid;
                            
                        }
                        certificadoLesion.Observaciones_antecedentes = observaciones_antecedentes;
                        if (examen.Tipo_tatuajeId != null)
                        {

                            Entity.CertificadoLesionTatuajes tatuajes = new Entity.CertificadoLesionTatuajes();
                            tatuajes.Creadopor = usId;
                            tatuajes.FechaHora = DateTime.Now;
                            int tatuajeid =ControlCertificadoLesionTatuajes.Guardar(tatuajes);
                            foreach (var item in examen.Tipo_tatuajeId)
                            {
                                Entity.CertificadoLesionTatuajesDetalle certificadoLesiontatuajeDetalle = new Entity.CertificadoLesionTatuajesDetalle();

                                certificadoLesiontatuajeDetalle.TatuajeId = Convert.ToInt32(item);
                                certificadoLesiontatuajeDetalle.certificado_LesionTatuajesId = tatuajeid;
                                ControlCertificadoLesionTatuajesDetalle.Guardar(certificadoLesiontatuajeDetalle);
                            }

                            certificadoLesion.Tipo_tatuajeId = tatuajeid;
                            
                        }

                        // INCLUDE NEW FIELD
                        //certificadoLesion.Aptitud = Convert.ToInt16(examen.Aptitud == "True" ? 1 : 0);

                        certificadoLesion.Observacion_tatuje = observacion_tatuje;
                        certificadoLesion.Lesion_simple_vista =Convert.ToInt16( examen.Lesion_simple_vista == "True" ? 1 : 0);
                        detalledet.Lesion_visible = Convert.ToBoolean(examen.Lesion_simple_vista);
                        ControlDetalleDetencion.Actualizar(detalledet);
                        certificadoLesion.Peligro_de_vida = Convert.ToInt16(examen.Peligro_de_vida == "True" ? 1 : 0);
                        certificadoLesion.Fallecimiento = Convert.ToInt16(examen.Fallecimiento == "True" ? 1 : 0);
                        certificadoLesion.Consecuencias_medico_legales = Convert.ToInt16(examen.Consecuencias_medico_legales == "True" ? 1 : 0);
                        certificadoLesion.Envio_hospital = Convert.ToInt16(examen.Envio_hospital == "True" ? 1 : 0);
                        certificadoLesion.Observacion_general = observacion_general;
                        certificadoLesion.FechaRegistro = DateTime.Now;
                        certificadoLesion.Registradopor = usId;
                        certificadoLesion.Sinlesion = Convert.ToInt16(examen.Sinlesion == "True" ? 1 : 0);
                        certificadoLesion.FotografiasId = string.IsNullOrWhiteSpace(examen.FotografiasId) ? 0 : int.Parse(examen.FotografiasId);
                        certificadoLesion.Fechavaloracion = Convert.ToDateTime(examen.Fechavaloracion);
                        servicioMedico.CertificadoLesionId = ControlCertificadoLesiones.Guardar(certificadoLesion);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = certificadoLesion.FechaRegistro;
                        historial.Habilitado = true;
                        historial.InternoId = detalledet.DetenidoId;
                        historial.Movimiento = "Modificación de datos generales en certificado de lesiones";
                        historial.TrackingId = Guid.NewGuid();
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);

                        servicioMedico.Activo = 1;
                        servicioMedico.TrackinngId = Guid.NewGuid().ToString();
                        servicioMedico.RegistradoPor = usId;
                        servicioMedico.FechaRegistro = DateTime.Now;
                        servicioMedico.Habilitado = 1;


                        servicioMedico.DetalledetencionId = detalledet.Id;
                        int serviciomedicoID = 0;
                        serviciomedicoID = ControlServicoMedico.Guardar(servicioMedico);

                        var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedicoID);
                        if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                        {
                            var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                            historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                            historialServicioMedicoObj.IdHistorial = historial.Id;
                            historialServicioMedicoObj.IdServicioMedico = serviciomedicoID;
                            ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                        }

                        string msj = "El certificado de lesiones se registró  correctamente.";
                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoLesion(servicioMedico);

                        var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = detenido.Id;
                        reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                        reportesLog.Modulo = "Examen médico";
                        reportesLog.Reporte = "Certificado de lesiones";
                        reportesLog.EstatusId = 1;
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);

                        return new { exitoso = true, mensaje = msj, ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                        //var tmpmail = String.Concat(cliente.Nombre, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

                    }
                    else
                    {
                        mode = @"actualizó";

                        var serviciomedico = ControlServicoMedico.ObtenerPorTrackingId(examen.ServiciomedicoTrackingId);

                        //int certificadoquimicoId=serviciomedico!=null?ser

                        if (serviciomedico.CertificadoLesionId == 0)
                        {
                            mode = @"registró";
                            var detalledet = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.TrackingId));

                            Entity.ServicioMedico servicioMedico = new Entity.ServicioMedico();
                            Entity.CertificadoLesion certificadoLesion = new Entity.CertificadoLesion();
                            Entity.CertificadoLesionTipoLesion certificadoLesionTipoLesion = new Entity.CertificadoLesionTipoLesion();
                            servicioMedico = ControlServicoMedico.ObtenerPorTrackingId(examen.ServiciomedicoTrackingId);

                            certificadoLesionTipoLesion.Creadopor = usId;
                            certificadoLesionTipoLesion.FechaHora = DateTime.Now;

                            int idtipolesion = ControlCertificadoLesionTipoLesion.Guardar(certificadoLesionTipoLesion);


                            List<string> tipolesionid = new List<string>();
                            if (examen.Tipo_lesionId != null)
                            {
                                foreach (var item in examen.Tipo_lesionId)
                                {
                                    Entity.CertificadoLesionTipoLesionDetalle tipoLesionDetalle = new Entity.CertificadoLesionTipoLesionDetalle();
                                    tipoLesionDetalle.Certificado_LesionTipoLesionId = idtipolesion;
                                    tipoLesionDetalle.TipoLesionId = Convert.ToInt32(item);
                                    ControlCertificadoLesionTipoLesionDetalle.Guardar(tipoLesionDetalle);
                                }

                                certificadoLesion.Tipo_lesionId = idtipolesion;
                            }
                            certificadoLesion.TrackingId = Guid.NewGuid().ToString();

                            certificadoLesion.Observaciones_lesion = observaciones_lesion;
                            //examen.Tipo_alergiaId = examen.Tipo_alergiaId[0]

                            if (examen.Tipo_alergiaId != null)
                            {
                                Entity.CertificadoLesionAlergia certificadoLesionAlergia = new Entity.CertificadoLesionAlergia();
                                certificadoLesionAlergia.Creadopor = usId;
                                certificadoLesionAlergia.FechaHora = DateTime.Now;

                                int idAlergia = ControlCertificadoLesionAlergia.Guardar(certificadoLesionAlergia);
                                foreach (var item in examen.Tipo_alergiaId)
                                {
                                    Entity.CertificadoLesionAlergiaDetalle certificadoLesionAlergiaDetalle = new Entity.CertificadoLesionAlergiaDetalle();

                                    certificadoLesionAlergiaDetalle.AlergiaId = Convert.ToInt32(item);
                                    certificadoLesionAlergiaDetalle.certificado_LesionAlergiaId = idAlergia;
                                    ControlCertificadoLesionAlergiaDetalle.Guardar(certificadoLesionAlergiaDetalle);

                                }
                                certificadoLesion.Tipo_alergiaId = idAlergia;
                                
                            }
                            certificadoLesion.Observacion_alergia = observacion_alergia;
                            if (examen.AntecedentesId != null)
                            {

                                Entity.CertificadoLesionAntecedentes antecedentes = new Entity.CertificadoLesionAntecedentes();
                                antecedentes.Creadopor = usId;
                                antecedentes.FechaHora = DateTime.Now;
                                int antecedenteid = ControlCertificadoLesionAntecedentes.Guardar(antecedentes);
                                foreach (var item in examen.AntecedentesId)
                                {
                                    Entity.CertificadoAntecedentesDetalle certificadoLesionAlergiaDetalle = new Entity.CertificadoAntecedentesDetalle();

                                    certificadoLesionAlergiaDetalle.AntecedenteId = Convert.ToInt32(item);
                                    certificadoLesionAlergiaDetalle.certificado_LesionAntecedentesId = antecedenteid;
                                    ControlCertificadoLesionAntecedentesDetalle.Guardar(certificadoLesionAlergiaDetalle);
                                }

                                certificadoLesion.AntecedentesId = antecedenteid;
                                
                            }
                            certificadoLesion.Observaciones_antecedentes = observaciones_antecedentes;
                            if (examen.Tipo_tatuajeId != null)
                            {

                                Entity.CertificadoLesionTatuajes tatuajes = new Entity.CertificadoLesionTatuajes();
                                tatuajes.Creadopor = usId;
                                tatuajes.FechaHora = DateTime.Now;
                                int tatuajeid = ControlCertificadoLesionTatuajes.Guardar(tatuajes);
                                foreach (var item in examen.Tipo_tatuajeId)
                                {
                                    Entity.CertificadoLesionTatuajesDetalle certificadoLesiontatuajeDetalle = new Entity.CertificadoLesionTatuajesDetalle();

                                    certificadoLesiontatuajeDetalle.TatuajeId = Convert.ToInt32(item);
                                    certificadoLesiontatuajeDetalle.certificado_LesionTatuajesId = tatuajeid;
                                    ControlCertificadoLesionTatuajesDetalle.Guardar(certificadoLesiontatuajeDetalle);
                                }

                                certificadoLesion.Tipo_tatuajeId = tatuajeid;
                                
                            }

                            // INCLUDE NEW FIELD
                            //certificadoLesion.Aptitud = Convert.ToInt16(examen.Aptitud == "True" ? 1 : 0);
                            
                            certificadoLesion.Observacion_tatuje = observacion_tatuje;
                            certificadoLesion.Lesion_simple_vista = Convert.ToInt16(examen.Lesion_simple_vista == "True" ? 1 : 0);
                            detalledet.Lesion_visible = Convert.ToBoolean(examen.Lesion_simple_vista);
                            ControlDetalleDetencion.Actualizar(detalledet);
                            certificadoLesion.Peligro_de_vida = Convert.ToInt16(examen.Peligro_de_vida == "True" ? 1 : 0);
                            certificadoLesion.Fallecimiento = Convert.ToInt16(examen.Fallecimiento == "True" ? 1 : 0);
                            certificadoLesion.Consecuencias_medico_legales = Convert.ToInt16(examen.Consecuencias_medico_legales == "True" ? 1 : 0);
                            certificadoLesion.Envio_hospital = Convert.ToInt16(examen.Envio_hospital == "True" ? 1 : 0);
                            certificadoLesion.Observacion_general = observacion_general;
                            certificadoLesion.FechaRegistro = DateTime.Now;
                            certificadoLesion.Registradopor = usId;
                            certificadoLesion.Sinlesion = Convert.ToInt16(examen.Sinlesion == "True" ? 1 : 0);
                            certificadoLesion.FotografiasId = string.IsNullOrWhiteSpace(examen.FotografiasId) ? 0 : int.Parse(examen.FotografiasId);
                            certificadoLesion.Fechavaloracion = Convert.ToDateTime(examen.Fechavaloracion);
                            servicioMedico.CertificadoLesionId = ControlCertificadoLesiones.Guardar(certificadoLesion);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = certificadoLesion.FechaRegistro;
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.Movimiento = "Modificación de datos generales en certificado de lesiones";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);

                            //serviciomedico.CertificadoQuimicoId = ControlCertificadoQuimico.Guardar(certificadoQuimico);
                            servicioMedico.Activo = 1;

                            servicioMedico.ModificadoPor = usId;
                            servicioMedico.FechaUltimaModificacion = DateTime.Now;
                            servicioMedico.Habilitado = 1;


                            servicioMedico.DetalledetencionId = detalledet.Id;

                            ControlServicoMedico.Actualizar(servicioMedico);

                            var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedico.Id);
                            if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                            {
                                var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                                historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                                historialServicioMedicoObj.IdHistorial = historial.Id;
                                historialServicioMedicoObj.IdServicioMedico = serviciomedico.Id;
                                ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                            }

                            //var certquim = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);
                            string msj = "El certificado de lesiones se registró correctamente.";


                            object[] dataArchivo = null;
                            dataArchivo = ControlPDFServicioMedico.GeneraCertificadoLesion(servicioMedico);

                            var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                            var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                            Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                            reportesLog.ProcesoId = detenido.Id;
                            reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                            reportesLog.Modulo = "Examen médico";
                            reportesLog.Reporte = "Certificado de lesiones";
                            reportesLog.Fechayhora = DateTime.Now;
                            reportesLog.EstatusId = 1;
                            reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                            ControlReportesLog.Guardar(reportesLog);
                            return new { exitoso = true, mensaje = msj, ServicioMedTracingId = serviciomedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                            //return new { exitoso = true, mensaje = msj, ServicioMedTracingId = serviciomedico.TrackinngId, Folio = certquim.Folio.ToString() };
                        }
                        else

                        {
                            var detalledet = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.TrackingId));

                            Entity.ServicioMedico servicioMedico = new Entity.ServicioMedico();
                            Entity.CertificadoLesion certificadoLesion = new Entity.CertificadoLesion();
                            Entity.CertificadoLesionTipoLesion certificadoLesionTipoLesion = new Entity.CertificadoLesionTipoLesion();
                            servicioMedico = ControlServicoMedico.ObtenerPorTrackingId(examen.ServiciomedicoTrackingId);
                            certificadoLesion = ControlCertificadoLesiones.ObtenerPorId(serviciomedico.CertificadoLesionId);
                            certificadoLesionTipoLesion.Creadopor = usId;
                            certificadoLesionTipoLesion.FechaHora = DateTime.Now;

                            int idtipolesion = ControlCertificadoLesionTipoLesion.Guardar(certificadoLesionTipoLesion);


                            List<string> tipolesionid = new List<string>();
                            if (examen.Tipo_lesionId != null)
                            {
                                foreach (var item in examen.Tipo_lesionId)
                                {
                                    Entity.CertificadoLesionTipoLesionDetalle tipoLesionDetalle = new Entity.CertificadoLesionTipoLesionDetalle();
                                    tipoLesionDetalle.Certificado_LesionTipoLesionId = idtipolesion;
                                    tipoLesionDetalle.TipoLesionId = Convert.ToInt32(item);
                                    ControlCertificadoLesionTipoLesionDetalle.Guardar(tipoLesionDetalle);
                                    certificadoLesion.Tipo_lesionId = idtipolesion;
                                }
                            }
                            
                            //certificadoLesion.TrackingId = Guid.NewGuid().ToString();

                            certificadoLesion.Observaciones_lesion = observaciones_lesion;

                            if (examen.Tipo_alergiaId != null)
                            {
                                Entity.CertificadoLesionAlergia certificadoLesionAlergia = new Entity.CertificadoLesionAlergia();
                                certificadoLesionAlergia.Creadopor = usId;
                                certificadoLesionAlergia.FechaHora = DateTime.Now;

                                int idAlergia = ControlCertificadoLesionAlergia.Guardar(certificadoLesionAlergia);
                                foreach (var item in examen.Tipo_alergiaId)
                                {
                                    Entity.CertificadoLesionAlergiaDetalle certificadoLesionAlergiaDetalle = new Entity.CertificadoLesionAlergiaDetalle();

                                    certificadoLesionAlergiaDetalle.AlergiaId = Convert.ToInt32(item);
                                    certificadoLesionAlergiaDetalle.certificado_LesionAlergiaId = idAlergia;
                                    ControlCertificadoLesionAlergiaDetalle.Guardar(certificadoLesionAlergiaDetalle);

                                }
                                certificadoLesion.Tipo_alergiaId = idAlergia;
                                

                            }
                            certificadoLesion.Observacion_alergia = observacion_alergia;
                            if (examen.AntecedentesId != null)
                            {

                                Entity.CertificadoLesionAntecedentes antecedentes = new Entity.CertificadoLesionAntecedentes();
                                antecedentes.Creadopor = usId;
                                antecedentes.FechaHora = DateTime.Now;
                                int antecedenteid = ControlCertificadoLesionAntecedentes.Guardar(antecedentes);
                                foreach (var item in examen.AntecedentesId)
                                {
                                    Entity.CertificadoAntecedentesDetalle certificadoLesionAlergiaDetalle = new Entity.CertificadoAntecedentesDetalle();

                                    certificadoLesionAlergiaDetalle.AntecedenteId = Convert.ToInt32(item);
                                    certificadoLesionAlergiaDetalle.certificado_LesionAntecedentesId = antecedenteid;
                                    ControlCertificadoLesionAntecedentesDetalle.Guardar(certificadoLesionAlergiaDetalle);
                                }

                                certificadoLesion.AntecedentesId = antecedenteid;
                                
                            }
                            certificadoLesion.Observaciones_antecedentes = observaciones_antecedentes;

                            if (examen.Tipo_tatuajeId != null)
                            {

                                Entity.CertificadoLesionTatuajes tatuajes = new Entity.CertificadoLesionTatuajes();
                                tatuajes.Creadopor = usId;
                                tatuajes.FechaHora = DateTime.Now;
                                int tatuajeid = ControlCertificadoLesionTatuajes.Guardar(tatuajes);
                                foreach (var item in examen.Tipo_tatuajeId)
                                {
                                    Entity.CertificadoLesionTatuajesDetalle certificadoLesiontatuajeDetalle = new Entity.CertificadoLesionTatuajesDetalle();

                                    certificadoLesiontatuajeDetalle.TatuajeId = Convert.ToInt32(item);
                                    certificadoLesiontatuajeDetalle.certificado_LesionTatuajesId = tatuajeid;
                                    ControlCertificadoLesionTatuajesDetalle.Guardar(certificadoLesiontatuajeDetalle);
                                }

                                certificadoLesion.Tipo_tatuajeId = tatuajeid;
                                
                            }
                            certificadoLesion.Observacion_tatuje = observacion_tatuje;
                            certificadoLesion.Lesion_simple_vista = Convert.ToInt16(examen.Lesion_simple_vista == "True" ? 1 : 0);
                            detalledet.Lesion_visible = Convert.ToBoolean(examen.Lesion_simple_vista);
                            ControlDetalleDetencion.Actualizar(detalledet);
                            certificadoLesion.Peligro_de_vida = Convert.ToInt16(examen.Peligro_de_vida == "True" ? 1 : 0);
                            certificadoLesion.Fallecimiento = Convert.ToInt16(examen.Fallecimiento == "True" ? 1 : 0);
                            certificadoLesion.Consecuencias_medico_legales = Convert.ToInt16(examen.Consecuencias_medico_legales == "True" ? 1 : 0);
                            certificadoLesion.Envio_hospital = Convert.ToInt16(examen.Envio_hospital == "True" ? 1 : 0);
                            certificadoLesion.Observacion_general = observacion_general;
                            certificadoLesion.Modificadopor = usId;
                            certificadoLesion.Fechaultimamodificacion = DateTime.Now;
                            certificadoLesion.Sinlesion = Convert.ToInt16(examen.Sinlesion == "True" ? 1 : 0);
                            certificadoLesion.FotografiasId = string.IsNullOrWhiteSpace(examen.FotografiasId) ? 0 : int.Parse(examen.FotografiasId);
                            certificadoLesion.Fechavaloracion = Convert.ToDateTime(examen.Fechavaloracion);
                            ControlCertificadoLesiones.Actualizar(certificadoLesion);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = certificadoLesion.Fechaultimamodificacion;
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.Movimiento = "Modificación de datos generales en certificado de lesiones";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);

                            //serviciomedico.CertificadoQuimicoId = ControlCertificadoQuimico.Guardar(certificadoQuimico);
                            servicioMedico.Activo = 1;

                            servicioMedico.ModificadoPor = usId;
                            servicioMedico.FechaUltimaModificacion = DateTime.Now;
                            servicioMedico.Habilitado = 1;

                            servicioMedico.DetalledetencionId = detalledet.Id;

                            ControlServicoMedico.Actualizar(servicioMedico);

                            var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedico.Id);
                            if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                            {
                                var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                                historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                                historialServicioMedicoObj.IdHistorial = historial.Id;
                                historialServicioMedicoObj.IdServicioMedico = serviciomedico.Id;
                                ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                            }

                            //var certquim = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);
                            string msj = "El certificado de lesiones se actualizó correctamente.";


                            object[] dataArchivo = null;
                            dataArchivo = ControlPDFServicioMedico.GeneraCertificadoLesion(servicioMedico);

                            var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                            var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                            Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                            reportesLog.ProcesoId = detenido.Id;
                            reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                            reportesLog.Modulo = "Examen médico";
                            reportesLog.Reporte = "Certificado de lesiones";
                            reportesLog.Fechayhora = DateTime.Now;
                            reportesLog.EstatusId = 2;
                            reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                            ControlReportesLog.Guardar(reportesLog);

                            return new { exitoso = true, mensaje = msj, ServicioMedTracingId = serviciomedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                        }
                    }

                    //int[] filtro = { detalle_detencion.ContratoId, detalle_detencion.DetenidoId };
                    //var hit = ControlCrossreference.GetCrossReferences(filtro);
                    string msg = "";
                    //var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                    bool fhit = false;
                    //if (hit.Count > 0)
                    //{
                    //    fhit = true;
                    //    msg = "La información del examen medico se " + mode + " correctamente.   El detenido " + detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno + " tiene un hit";
                    //}

                    return new { ishit = "", msghit = msg, exitoso = true, mensaje = mode, Id = examenNuevo.Id.ToString(), TrackingId = examenNuevo.TrackingId, DetenidoId = "0", Observacion = examenNuevo.Observacion };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object savecertificadopsicofisiologico(CertificadoPsicofisiologicoaux examen)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var examenNuevo = new Entity.ExamenMedico();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    
                    var usuario = ControlUsuario.Obtener(usId);
                    var roles = ControlRol.ObtenerPorId(usuario.RolId);
                    var p = "";
                    if (roles.name == "Químico")
                    {
                        throw new Exception("El usuario no cuenta con los permisos para generar el certificado de lesiones.");
                    }
                    // string id = examen.Id;
                    //// if (id == "") id = "0";
                    //int dias = 0;
                    //if (examen.DiasSanarLesiones != "")
                    //    dias = Convert.ToInt32(examen.DiasSanarLesiones);
                    //Entity.DetalleDetencion detalle_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.DetalleDetencionTracking));
                    //examenNuevo.DetalleDetencionId = detalle_detencion.Id;

                    var observacion_extra = examen.Observacion_extra;
                    if (!string.IsNullOrEmpty(observacion_extra) && observacion_extra.Trim().Last() != '.')
                        observacion_extra = observacion_extra.Trim() + ".";

                    if (string.IsNullOrEmpty(examen.ServiciomedicoTrackingId))
                    {
                        mode = @"registró";

                        Entity.CertificadoPsicoFisilogicoOrientacion certificadoPsicoFisilogicoOrientacion = new Entity.CertificadoPsicoFisilogicoOrientacion();
                        if (examen.OrientacionId != null)
                        {
                            certificadoPsicoFisilogicoOrientacion.Creadopor = usId;
                            certificadoPsicoFisilogicoOrientacion.FechaHora = DateTime.Now;
                            certificadoPsicoFisilogicoOrientacion.Id = ControlCertificadoPsicoFisologicoOrientacion.Guardar(certificadoPsicoFisilogicoOrientacion);

                            foreach (var orientacionid in examen.OrientacionId)
                            {
                                Entity.CertificadoPsicoFisiologicoOrientacionDetalle certificadoPsicoFisiologicoConclusionDetalle = new Entity.CertificadoPsicoFisiologicoOrientacionDetalle();
                                certificadoPsicoFisiologicoConclusionDetalle.certificado_psicofisiologicoorientacionId = certificadoPsicoFisilogicoOrientacion.Id;
                                certificadoPsicoFisiologicoConclusionDetalle.OrientacionId = Convert.ToInt32(orientacionid);
                                certificadoPsicoFisiologicoConclusionDetalle.Creadopor = usId;
                                certificadoPsicoFisiologicoConclusionDetalle.FechaHora = DateTime.Now;
                                ControlCertificadoPsicoFisologicoOrientacionDetalle.Guardar(certificadoPsicoFisiologicoConclusionDetalle);
                            }
                        }

                        Entity.CertificadoPsicoFisilogicoConclusion certificadoPsicoFisilogicoConclusion = new Entity.CertificadoPsicoFisilogicoConclusion();
                        if (examen.ConclusionId != null)
                        {
                            certificadoPsicoFisilogicoConclusion.Creadopor = usId;
                            certificadoPsicoFisilogicoConclusion.FechaHora = DateTime.Now;
                            certificadoPsicoFisilogicoConclusion.Id = ControlCertificadoPsicoFisologicoConclusion.Guardar(certificadoPsicoFisilogicoConclusion);

                            foreach (var conclusionid in examen.ConclusionId)
                            {
                                Entity.CertificadoPsicoFisiologicoConclusionDetalle certificadoPsicoFisiologicoConclusionDetalle = new Entity.CertificadoPsicoFisiologicoConclusionDetalle();
                                certificadoPsicoFisiologicoConclusionDetalle.certificado_psicofisiologicoConclusionId = certificadoPsicoFisilogicoConclusion.Id;
                                certificadoPsicoFisiologicoConclusionDetalle.ConclusionId = Convert.ToInt32(conclusionid);
                                certificadoPsicoFisiologicoConclusionDetalle.Creadopor = usId;
                                certificadoPsicoFisiologicoConclusionDetalle.FechaHora = DateTime.Now;
                                ControlCertificadoPsicoFisologicoConclusionDetalle.Guardar(certificadoPsicoFisiologicoConclusionDetalle);
                            }
                        }

                        Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsico = new Entity.CertificadoMedicoPsicoFisiologico();
                        certificadoMedicoPsico.TrackingId = Guid.NewGuid().ToString();
                        certificadoMedicoPsico.AlientoId = Convert.ToInt32(examen.AlientoId);
                        certificadoMedicoPsico.MarchaId = Convert.ToInt32(examen.MarchaId);
                        certificadoMedicoPsico.ActitudId = Convert.ToInt32(examen.ActitudId);
                        certificadoMedicoPsico.AtencionId = Convert.ToInt32(examen.AtencionId);
                        certificadoMedicoPsico.Cavidad_oralId = Convert.ToInt32(examen.Cavidad_oralId);
                        certificadoMedicoPsico.PupilaId = Convert.ToInt32(examen.PupilaId);
                        certificadoMedicoPsico.Reflejo_pupilarId = Convert.ToInt32(examen.Reflejo_pupilarId);
                        certificadoMedicoPsico.ConjuntivaId = Convert.ToInt32(examen.ConjuntivaId);
                        certificadoMedicoPsico.LenguajeId = Convert.ToInt32(examen.LenguajeId);
                        certificadoMedicoPsico.RombergId = Convert.ToInt32(examen.RombergId);
                        certificadoMedicoPsico.Ojos_abiertos_dedo_dedoId = Convert.ToInt32(examen.Ojos_abiertos_dedo_dedoId);
                        certificadoMedicoPsico.Ojos_cerrados_dedo_dedoId = Convert.ToInt32(examen.Ojos_cerrados_dedo_dedoId);
                        certificadoMedicoPsico.Ojos_abiertos_dedo_narizId = Convert.ToInt32(examen.Ojos_abiertos_dedo_narizId);
                        certificadoMedicoPsico.Ojos_cerrados_dedo_narizId = Convert.ToInt32(examen.Ojos_cerrados_dedo_narizId);
                        certificadoMedicoPsico.TA = examen.TA;
                        certificadoMedicoPsico.FC = examen.FC;
                        certificadoMedicoPsico.FR = examen.FR;
                        certificadoMedicoPsico.Pulso = examen.Pulso;
                        certificadoMedicoPsico.No_valorabe = Convert.ToInt16(Convert.ToBoolean(examen.No_valorabe));
                        certificadoMedicoPsico.OrientacionId = certificadoPsicoFisilogicoOrientacion.Id;
                        certificadoMedicoPsico.ConclusionId = certificadoPsicoFisilogicoConclusion.Id;
                        certificadoMedicoPsico.Sustento_toxicologico = Convert.ToInt16(Convert.ToBoolean(examen.Sustento_toxicologico));
                        certificadoMedicoPsico.Observacion_orientacion = examen.Observacion_orientacion;
                        certificadoMedicoPsico.FechaValoracion = Convert.ToDateTime(examen.FechaValoracion);
                        certificadoMedicoPsico.Observacion_extra = observacion_extra;
                        certificadoMedicoPsico.FechaRegistro = DateTime.Now;
                        certificadoMedicoPsico.Registradopor = usId;
                        var detalledet = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.TrackingId));
                        certificadoMedicoPsico.ContratoId = detalledet.ContratoId;

                        Entity.ServicioMedico servicioMedico = new Entity.ServicioMedico();
                        servicioMedico.CertificadoMedicoPsicofisiologicoId = ControlCertificadoPsicoFisiologico.Guardar(certificadoMedicoPsico);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = Convert.ToDateTime(examen.FechaValoracion);
                        historial.Habilitado = true;
                        historial.InternoId = detalledet.DetenidoId;
                        historial.Movimiento = "Modificación de datos generales en certificado psicofisiológico";
                        historial.TrackingId = Guid.NewGuid();
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);

                        servicioMedico.Activo = 1;
                        servicioMedico.TrackinngId = Guid.NewGuid().ToString();
                        servicioMedico.RegistradoPor = usId;
                        servicioMedico.FechaRegistro = DateTime.Now;
                        servicioMedico.Habilitado = 1;
                        servicioMedico.DetalledetencionId = detalledet.Id;

                        int serviciomedicoID = ControlServicoMedico.Guardar(servicioMedico);

                        var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedicoID);
                        if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                        {
                            var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                            historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                            historialServicioMedicoObj.IdHistorial = historial.Id;
                            historialServicioMedicoObj.IdServicioMedico = serviciomedicoID;
                            ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                        }

                        string msj = "El certificado médico psicofisiológico se registró correctamente.";

                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoPsicofisiologico(servicioMedico);

                        var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = detenido.Id;
                        reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                        reportesLog.Modulo = "Examen médico";
                        reportesLog.Reporte = "Certificado psicofisiológico";
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.EstatusId = 1;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);
                        return new { exitoso = true, mensaje = msj, ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                        //var tmpmail = String.Concat(cliente.Nombre, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

                    }
                    else
                    {
                        //rep4ir
                        mode = @"actualizó";
                        var serviciomedico = ControlServicoMedico.ObtenerPorTrackingId(examen.ServiciomedicoTrackingId);

                        //int certificadoquimicoId=serviciomedico!=null?ser

                        if (serviciomedico.CertificadoMedicoPsicofisiologicoId == 0)
                        {
                            mode = @"registró";

                            Entity.CertificadoPsicoFisilogicoOrientacion certificadoPsicoFisilogicoOrientacion = new Entity.CertificadoPsicoFisilogicoOrientacion();
                            if (examen.OrientacionId != null)
                            {
                                certificadoPsicoFisilogicoOrientacion.Creadopor = usId;
                                certificadoPsicoFisilogicoOrientacion.FechaHora = DateTime.Now;
                                certificadoPsicoFisilogicoOrientacion.Id = ControlCertificadoPsicoFisologicoOrientacion.Guardar(certificadoPsicoFisilogicoOrientacion);

                                foreach (var orientacionid in examen.OrientacionId)
                                {
                                    Entity.CertificadoPsicoFisiologicoOrientacionDetalle certificadoPsicoFisiologicoConclusionDetalle = new Entity.CertificadoPsicoFisiologicoOrientacionDetalle();
                                    certificadoPsicoFisiologicoConclusionDetalle.certificado_psicofisiologicoorientacionId = certificadoPsicoFisilogicoOrientacion.Id;
                                    certificadoPsicoFisiologicoConclusionDetalle.OrientacionId = Convert.ToInt32(orientacionid);
                                    certificadoPsicoFisiologicoConclusionDetalle.Creadopor = usId;
                                    certificadoPsicoFisiologicoConclusionDetalle.FechaHora = DateTime.Now;
                                    ControlCertificadoPsicoFisologicoOrientacionDetalle.Guardar(certificadoPsicoFisiologicoConclusionDetalle);
                                }
                            }

                            Entity.CertificadoPsicoFisilogicoConclusion certificadoPsicoFisilogicoConclusion = new Entity.CertificadoPsicoFisilogicoConclusion();
                            if (examen.ConclusionId != null)
                            {
                                certificadoPsicoFisilogicoConclusion.Creadopor = usId;
                                certificadoPsicoFisilogicoConclusion.FechaHora = DateTime.Now;
                                certificadoPsicoFisilogicoConclusion.Id = ControlCertificadoPsicoFisologicoConclusion.Guardar(certificadoPsicoFisilogicoConclusion);

                                foreach (var conclusionid in examen.ConclusionId)
                                {
                                    Entity.CertificadoPsicoFisiologicoConclusionDetalle certificadoPsicoFisiologicoConclusionDetalle = new Entity.CertificadoPsicoFisiologicoConclusionDetalle();
                                    certificadoPsicoFisiologicoConclusionDetalle.certificado_psicofisiologicoConclusionId = certificadoPsicoFisilogicoConclusion.Id;
                                    certificadoPsicoFisiologicoConclusionDetalle.ConclusionId = Convert.ToInt32(conclusionid);
                                    certificadoPsicoFisiologicoConclusionDetalle.Creadopor = usId;
                                    certificadoPsicoFisiologicoConclusionDetalle.FechaHora = DateTime.Now;
                                    ControlCertificadoPsicoFisologicoConclusionDetalle.Guardar(certificadoPsicoFisiologicoConclusionDetalle);
                                }
                            }

                            Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsico = new Entity.CertificadoMedicoPsicoFisiologico();
                            certificadoMedicoPsico.TrackingId = Guid.NewGuid().ToString();
                            certificadoMedicoPsico.AlientoId = Convert.ToInt32(examen.AlientoId);
                            certificadoMedicoPsico.MarchaId = Convert.ToInt32(examen.MarchaId);
                            certificadoMedicoPsico.ActitudId = Convert.ToInt32(examen.ActitudId);
                            certificadoMedicoPsico.AtencionId = Convert.ToInt32(examen.AtencionId);
                            certificadoMedicoPsico.Cavidad_oralId = Convert.ToInt32(examen.Cavidad_oralId);
                            certificadoMedicoPsico.PupilaId = Convert.ToInt32(examen.PupilaId);
                            certificadoMedicoPsico.Reflejo_pupilarId = Convert.ToInt32(examen.Reflejo_pupilarId);
                            certificadoMedicoPsico.ConjuntivaId = Convert.ToInt32(examen.ConjuntivaId);
                            certificadoMedicoPsico.LenguajeId = Convert.ToInt32(examen.LenguajeId);
                            certificadoMedicoPsico.RombergId = Convert.ToInt32(examen.RombergId);
                            certificadoMedicoPsico.Ojos_abiertos_dedo_dedoId = Convert.ToInt32(examen.Ojos_abiertos_dedo_dedoId);
                            certificadoMedicoPsico.Ojos_cerrados_dedo_dedoId = Convert.ToInt32(examen.Ojos_cerrados_dedo_dedoId);
                            certificadoMedicoPsico.Ojos_abiertos_dedo_narizId = Convert.ToInt32(examen.Ojos_abiertos_dedo_narizId);
                            certificadoMedicoPsico.Ojos_cerrados_dedo_narizId = Convert.ToInt32(examen.Ojos_cerrados_dedo_narizId);
                            certificadoMedicoPsico.TA = examen.TA;
                            certificadoMedicoPsico.FC = examen.FC;
                            certificadoMedicoPsico.FR = examen.FR;
                            certificadoMedicoPsico.Pulso = examen.Pulso;
                            certificadoMedicoPsico.No_valorabe = Convert.ToInt16(Convert.ToBoolean(examen.No_valorabe));
                            certificadoMedicoPsico.OrientacionId = certificadoPsicoFisilogicoOrientacion.Id;
                            certificadoMedicoPsico.ConclusionId = certificadoPsicoFisilogicoConclusion.Id;
                            certificadoMedicoPsico.Sustento_toxicologico = Convert.ToInt16(Convert.ToBoolean(examen.Sustento_toxicologico));
                            certificadoMedicoPsico.Observacion_orientacion = examen.Observacion_orientacion;
                            certificadoMedicoPsico.Observacion_extra = observacion_extra;
                            certificadoMedicoPsico.FechaValoracion = Convert.ToDateTime(examen.FechaValoracion);
                            var detalledet = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.TrackingId));
                            certificadoMedicoPsico.ContratoId = detalledet.ContratoId;
                            certificadoMedicoPsico.FechaRegistro = DateTime.Now;
                            certificadoMedicoPsico.Registradopor = usId;
                            serviciomedico.CertificadoMedicoPsicofisiologicoId = ControlCertificadoPsicoFisiologico.Guardar(certificadoMedicoPsico);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = Convert.ToDateTime(examen.FechaValoracion);
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.Movimiento = "Modificación de datos generales en certificado psicofisiológico";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);

                            serviciomedico.Activo = 1;
                            serviciomedico.ModificadoPor = usId;
                            serviciomedico.FechaUltimaModificacion = DateTime.Now;
                            serviciomedico.Habilitado = 1;
                            serviciomedico.DetalledetencionId = detalledet.Id;

                            ControlServicoMedico.Actualizar(serviciomedico);

                            var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedico.Id);
                            if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                            {
                                var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                                historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                                historialServicioMedicoObj.IdHistorial = historial.Id;
                                historialServicioMedicoObj.IdServicioMedico = serviciomedico.Id;
                                ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                            }

                            string msj = "Se registró el certificado médico psicofisiológico correctamente.";

                            object[] dataArchivo = null;
                            dataArchivo = ControlPDFServicioMedico.GeneraCertificadoPsicofisiologico(serviciomedico);
                            var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(serviciomedico.DetalledetencionId);
                            var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                            Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                            reportesLog.ProcesoId = detenido.Id;
                            reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                            reportesLog.Modulo = "Examen médico";
                            reportesLog.Reporte = "Certificado psicofisiológico";
                            reportesLog.Fechayhora = DateTime.Now;
                            reportesLog.EstatusId = 1;
                            reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                            ControlReportesLog.Guardar(reportesLog);
                            return new { exitoso = true, mensaje = msj, ServicioMedTracingId = serviciomedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                        }
                        else
                        {
                            Entity.CertificadoPsicoFisilogicoOrientacion certificadoPsicoFisilogicoOrientacion = new Entity.CertificadoPsicoFisilogicoOrientacion();
                            if (examen.OrientacionId != null)
                            {
                                
                                certificadoPsicoFisilogicoOrientacion.Creadopor = usId;
                                certificadoPsicoFisilogicoOrientacion.FechaHora = DateTime.Now;
                                certificadoPsicoFisilogicoOrientacion.Id = ControlCertificadoPsicoFisologicoOrientacion.Guardar(certificadoPsicoFisilogicoOrientacion);

                                foreach (var orientacionid in examen.OrientacionId)
                                {
                                    Entity.CertificadoPsicoFisiologicoOrientacionDetalle certificadoPsicoFisiologicoConclusionDetalle = new Entity.CertificadoPsicoFisiologicoOrientacionDetalle();
                                    certificadoPsicoFisiologicoConclusionDetalle.certificado_psicofisiologicoorientacionId = certificadoPsicoFisilogicoOrientacion.Id;
                                    certificadoPsicoFisiologicoConclusionDetalle.OrientacionId = Convert.ToInt32(orientacionid);
                                    certificadoPsicoFisiologicoConclusionDetalle.Creadopor = usId;
                                    certificadoPsicoFisiologicoConclusionDetalle.FechaHora = DateTime.Now;
                                    ControlCertificadoPsicoFisologicoOrientacionDetalle.Guardar(certificadoPsicoFisiologicoConclusionDetalle);
                                }
                            }

                            Entity.CertificadoPsicoFisilogicoConclusion certificadoPsicoFisilogicoConclusion = new Entity.CertificadoPsicoFisilogicoConclusion();
                            if (examen.ConclusionId != null)
                            {
                                certificadoPsicoFisilogicoConclusion.Creadopor = usId;
                                certificadoPsicoFisilogicoConclusion.FechaHora = DateTime.Now;
                                certificadoPsicoFisilogicoConclusion.Id = ControlCertificadoPsicoFisologicoConclusion.Guardar(certificadoPsicoFisilogicoConclusion);

                                foreach (var conclusionid in examen.ConclusionId)
                                {
                                    Entity.CertificadoPsicoFisiologicoConclusionDetalle certificadoPsicoFisiologicoConclusionDetalle = new Entity.CertificadoPsicoFisiologicoConclusionDetalle();
                                    certificadoPsicoFisiologicoConclusionDetalle.certificado_psicofisiologicoConclusionId = certificadoPsicoFisilogicoConclusion.Id;
                                    certificadoPsicoFisiologicoConclusionDetalle.ConclusionId = Convert.ToInt32(conclusionid);
                                    certificadoPsicoFisiologicoConclusionDetalle.Creadopor = usId;
                                    certificadoPsicoFisiologicoConclusionDetalle.FechaHora = DateTime.Now;
                                    ControlCertificadoPsicoFisologicoConclusionDetalle.Guardar(certificadoPsicoFisiologicoConclusionDetalle);
                                }
                            }

                            Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsico = new Entity.CertificadoMedicoPsicoFisiologico();

                            certificadoMedicoPsico = ControlCertificadoPsicoFisiologico.ObtenerPorId(serviciomedico.CertificadoMedicoPsicofisiologicoId);
                            //certificadoMedicoPsico.TrackingId = Guid.NewGuid().ToString();
                            certificadoMedicoPsico.AlientoId = Convert.ToInt32(examen.AlientoId);
                            certificadoMedicoPsico.MarchaId = Convert.ToInt32(examen.MarchaId);
                            certificadoMedicoPsico.ActitudId = Convert.ToInt32(examen.ActitudId);
                            certificadoMedicoPsico.AtencionId = Convert.ToInt32(examen.AtencionId);
                            certificadoMedicoPsico.Cavidad_oralId = Convert.ToInt32(examen.Cavidad_oralId);
                            certificadoMedicoPsico.PupilaId = Convert.ToInt32(examen.PupilaId);
                            certificadoMedicoPsico.Reflejo_pupilarId = Convert.ToInt32(examen.Reflejo_pupilarId);
                            certificadoMedicoPsico.ConjuntivaId = Convert.ToInt32(examen.ConjuntivaId);
                            certificadoMedicoPsico.LenguajeId = Convert.ToInt32(examen.LenguajeId);
                            certificadoMedicoPsico.RombergId = Convert.ToInt32(examen.RombergId);
                            certificadoMedicoPsico.Ojos_abiertos_dedo_dedoId = Convert.ToInt32(examen.Ojos_abiertos_dedo_dedoId);
                            certificadoMedicoPsico.Ojos_cerrados_dedo_dedoId = Convert.ToInt32(examen.Ojos_cerrados_dedo_dedoId);
                            certificadoMedicoPsico.Ojos_abiertos_dedo_narizId = Convert.ToInt32(examen.Ojos_abiertos_dedo_narizId);
                            certificadoMedicoPsico.Ojos_cerrados_dedo_narizId = Convert.ToInt32(examen.Ojos_cerrados_dedo_narizId);
                            certificadoMedicoPsico.TA = examen.TA;
                            certificadoMedicoPsico.FC = examen.FC;
                            certificadoMedicoPsico.FR = examen.FR;
                            certificadoMedicoPsico.Pulso = examen.Pulso;
                            certificadoMedicoPsico.No_valorabe = Convert.ToInt16(Convert.ToBoolean(examen.No_valorabe));
                            certificadoMedicoPsico.OrientacionId = certificadoPsicoFisilogicoOrientacion.Id;
                            certificadoMedicoPsico.ConclusionId = certificadoPsicoFisilogicoConclusion.Id;
                            certificadoMedicoPsico.Sustento_toxicologico = Convert.ToInt16(Convert.ToBoolean(examen.Sustento_toxicologico));
                            certificadoMedicoPsico.Observacion_orientacion = examen.Observacion_orientacion;
                            certificadoMedicoPsico.Observacion_extra = observacion_extra;
                            certificadoMedicoPsico.FechaValoracion = Convert.ToDateTime(examen.FechaValoracion);
                            var detalledet = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.TrackingId));
                            certificadoMedicoPsico.ContratoId = detalledet.ContratoId;
                            certificadoMedicoPsico.Fechaultimamodificacion = DateTime.Now;
                            certificadoMedicoPsico.Modificadopor = usId;
                            ControlCertificadoPsicoFisiologico.Actualizar(certificadoMedicoPsico);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = Convert.ToDateTime(examen.FechaValoracion);
                            historial.Habilitado = true;
                            historial.InternoId = detalledet.DetenidoId;
                            historial.Movimiento = "Modificación de datos generales en certificado psicofisiológico";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);

                            serviciomedico.Activo = 1;
                            serviciomedico.ModificadoPor = usId;
                            serviciomedico.FechaUltimaModificacion = DateTime.Now;
                            serviciomedico.Habilitado = 1;
                            serviciomedico.DetalledetencionId = detalledet.Id;

                            ControlServicoMedico.Actualizar(serviciomedico);

                            var historialServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdServicioMedico(serviciomedico.Id);
                            if (!historialServicioMedico.Any(x => x.IdHistorial == historial.Id))
                            {
                                var historialServicioMedicoObj = new Entity.HistorialServicioMedico();
                                historialServicioMedicoObj.TrackingId = Guid.NewGuid();
                                historialServicioMedicoObj.IdHistorial = historial.Id;
                                historialServicioMedicoObj.IdServicioMedico = serviciomedico.Id;
                                ControlHistorialServicioMedico.Guardar(historialServicioMedicoObj);
                            }

                            string msj = "Se actualizó el certificado médico psicofisiológico correctamente.";

                            object[] dataArchivo = null;
                            dataArchivo = ControlPDFServicioMedico.GeneraCertificadoPsicofisiologico(serviciomedico);
                            var detalle_detencion = ControlDetalleDetencion.ObtenerPorId(serviciomedico.DetalledetencionId);
                            var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                            Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                            reportesLog.ProcesoId = detenido.Id;
                            reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                            reportesLog.Modulo = "Examen médico";
                            reportesLog.Reporte = "Certificado psicofisiológico";
                            reportesLog.Fechayhora = DateTime.Now;
                            reportesLog.EstatusId = 2;
                            reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                            ControlReportesLog.Guardar(reportesLog);
                            return new { exitoso = true, mensaje = msj, ServicioMedTracingId = serviciomedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };




                        }
                    }
                    //int[] filtro = { detalle_detencion.ContratoId, detalle_detencion.DetenidoId };
                    //var hit = ControlCrossreference.GetCrossReferences(filtro);
                    string msg = "";
                    //var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                    bool fhit = false;
                    //if (hit.Count > 0)
                    //{
                    //    fhit = true;
                    //    msg = "La información del examen medico se " + mode + " correctamente.   El detenido " + detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno + " tiene un hit";
                    //}

                    return new { ishit = "", msghit = msg, exitoso = true, mensaje = mode, Id = examenNuevo.Id.ToString(), TrackingId = examenNuevo.TrackingId, DetenidoId = "0", Observacion = examenNuevo.Observacion };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object saveexamen(ExamenAux examen)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var examenNuevo = new Entity.ExamenMedico();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    // string id = examen.Id;
                    // if (id == "") id = "0";
                    int dias = 0;
                    if (examen.DiasSanarLesiones != "")
                        dias = Convert.ToInt32(examen.DiasSanarLesiones);
                    Entity.DetalleDetencion detalle_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(examen.DetalleDetencionTracking));
                    examenNuevo.DetalleDetencionId = detalle_detencion.Id;
                    if (string.IsNullOrEmpty(examen.TrackingId))
                    {
                        mode = @"registró";
                        //var tmpmail = String.Concat(cliente.Nombre, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

                        examenNuevo.Activo = true;
                        examenNuevo.RegistradoPor = usId;
                        examenNuevo.TrackingId = Guid.NewGuid();
                        examenNuevo.Habilitado = true;
                        examenNuevo.Fecha = Convert.ToDateTime(examen.Fecha);
                        examenNuevo.Edad = Convert.ToInt32(examen.Edad);
                        examenNuevo.SexoId = Convert.ToInt32(examen.SexoId);
                        examenNuevo.InspeccionOcular = examen.InspeccionOcular;
                        examenNuevo.IndicacionesMedicas = examen.IndicacionesMedicas;
                        examenNuevo.Observacion = examen.Observacion;
                        examenNuevo.RiesgoVida = Convert.ToBoolean(examen.RiesgoVida);
                        examenNuevo.ConsecuenciasLesiones = Convert.ToBoolean(examen.ConsecuenciasLesiones);
                        examenNuevo.DiasSanarLesiones = dias;
                        examenNuevo.TipoExamen = ControlCatalogo.Obtener(Convert.ToInt32(examen.TipoExamen), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_examen));
                        examenNuevo.Fallecimiento = Convert.ToBoolean(examen.Fallecimiento);
                        examenNuevo.Lesion_visible = Convert.ToBoolean(examen.Lesion_visible);
                        examenNuevo.Id = ControlExamenMedico.Guardar(examenNuevo);
                    }
                    else
                    {
                        mode = @"actualizó";

                        Entity.ExamenMedico _examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(examen.TrackingId));

                        string motivoAnterior = _examen != null ? _examen.Observacion : "";
                        string motivoSinRepetir = motivoAnterior != "" ? examen.Observacion.Replace(motivoAnterior, "") : examen.Observacion;

                        examenNuevo.Activo = true;
                        examenNuevo.RegistradoPor = usId;

                        examenNuevo.Habilitado = true;

                        examenNuevo.Edad = Convert.ToInt32(examen.Edad);
                        examenNuevo.SexoId = Convert.ToInt32(examen.SexoId);
                        examenNuevo.InspeccionOcular = examen.InspeccionOcular;
                        examenNuevo.IndicacionesMedicas = examen.IndicacionesMedicas;
                        examenNuevo.Observacion = motivoAnterior + "/" + motivoSinRepetir;
                        examenNuevo.RiesgoVida = Convert.ToBoolean(examen.RiesgoVida);
                        examenNuevo.ConsecuenciasLesiones = Convert.ToBoolean(examen.ConsecuenciasLesiones);
                        examenNuevo.DiasSanarLesiones = dias;
                        examenNuevo.TipoExamen = ControlCatalogo.Obtener(Convert.ToInt32(examen.TipoExamen), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_examen));
                        examenNuevo.Fecha = Convert.ToDateTime(examen.Fecha);
                        examenNuevo.Lesion_visible = Convert.ToBoolean(examen.Lesion_visible);
                        examenNuevo.TrackingId = _examen.TrackingId;
                        examenNuevo.Id = _examen.Id;
                        examenNuevo.Fallecimiento = Convert.ToBoolean(examen.Fallecimiento);
                        ControlExamenMedico.Actualizar(examenNuevo);
                    }

                    int[] filtro = { detalle_detencion.ContratoId, detalle_detencion.DetenidoId };
                    var hit = ControlCrossreference.GetCrossReferences(filtro);
                    string msg = "";
                    var detenido = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                    bool fhit = false;
                    if (hit.Count > 0)
                    {
                        fhit = true;
                        msg = "La información del examen médico se "+mode+ " correctamente.   El detenido " + detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno + " tiene un hit";
                    }

                    return new { ishit = fhit, msghit = msg, exitoso = true, mensaje = mode, Id = examenNuevo.Id.ToString(), TrackingId = examenNuevo.TrackingId,DetenidoId= detalle_detencion.DetenidoId.ToString(), Observacion = examenNuevo.Observacion };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static string pdf(int datos)
        {
            var serializedObject = string.Empty;
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Reportes" }).Consultar)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    var interno = ControlDetenido.ObtenerPorId(datos);

                    string[] Id = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(Id);

                    var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);

                    if (examenmedico.Count > 0)
                    {
                        var obj = ControlPDFEM.ReporteEM(interno.Id, interno.TrackingId, examenmedico.LastOrDefault().Id);
                        serializedObject = JsonConvert.SerializeObject(obj);
                    }
                    else
                    {
                        serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El detenido no cuenta con examen médico." });
                    }

                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static object saveprueba(PruebaAux prueba)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var pruebaNueva = new Entity.PruebaExamenMedico();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    if (prueba.ExamenMedicoTracking == null || prueba.ExamenMedicoTracking == "")
                    {
                        return new { exitoso = false, mensaje = "No existe un exámen médico para esta prueba", Id = "", TrackingId = "" };
                    }

                    Entity.ExamenMedico examenMedico = ControlExamenMedico.ObtenerPorTrackingId(new Guid(prueba.ExamenMedicoTracking));

                    pruebaNueva.ExamenMedicoId = examenMedico.Id;
                    pruebaNueva.RegistradoPor = usId;

                    if (string.IsNullOrEmpty(prueba.TrackingId))
                    {
                        mode = @"registró";
                        pruebaNueva.TrackingId = Guid.NewGuid();

                        pruebaNueva.MucosasId = Convert.ToInt32(prueba.MucosasId);
                        pruebaNueva.AlientoId = Convert.ToInt32(prueba.AlientoId);
                        pruebaNueva.Examen_neurologicoId = Convert.ToInt32(prueba.Examen_neurologicoId);
                        pruebaNueva.Disartia = Convert.ToBoolean(prueba.Disartia);
                        pruebaNueva.ConjuntivasId = Convert.ToInt32(prueba.ConjuntivasId);
                        pruebaNueva.MarchaId = Convert.ToInt32(prueba.MarchaId);
                        pruebaNueva.PupilasId = Convert.ToInt32(prueba.PupilasId);
                        pruebaNueva.CoordinacionId = Convert.ToInt32(prueba.CoordinacionId);
                        pruebaNueva.Reflejos_pupilaresId = Convert.ToInt32(prueba.Reflejos_pupilaresId);
                        pruebaNueva.TendinososId = Convert.ToInt32(prueba.TendinososId);
                        pruebaNueva.RomberqId = Convert.ToInt32(prueba.RomberqId);
                        pruebaNueva.ConductaId = Convert.ToInt32(prueba.ConductaId);
                        pruebaNueva.LenguajeId = Convert.ToInt32(prueba.LenguajeId);
                        pruebaNueva.AtencionId = Convert.ToInt32(prueba.AtencionId);
                        pruebaNueva.OrientacionId = Convert.ToInt32(prueba.OrientacionId);
                        pruebaNueva.DiadococinenciaId = Convert.ToInt32(prueba.DiadococinenciaId);
                        pruebaNueva.DedoId = Convert.ToInt32(prueba.DedoId);
                        pruebaNueva.TalonId = Convert.ToInt32(prueba.TalonId);
                        pruebaNueva.Alcoholimetro = prueba.Alcoholimetro;
                        pruebaNueva.TA = prueba.TA;
                        pruebaNueva.FC = prueba.FC;
                        pruebaNueva.FR = prueba.FR;
                        pruebaNueva.Pulso = prueba.Pulso;

                        pruebaNueva.Id = ControlPruebaExamenMedico.Guardar(pruebaNueva);
                    }
                    else
                    {
                        mode = @"actualizó";

                        Entity.PruebaExamenMedico _prueba = ControlPruebaExamenMedico.ObtenerPorTrackingId(new Guid(prueba.TrackingId));

                        pruebaNueva.MucosasId = Convert.ToInt32(prueba.MucosasId);
                        pruebaNueva.AlientoId = Convert.ToInt32(prueba.AlientoId);
                        pruebaNueva.Examen_neurologicoId = Convert.ToInt32(prueba.Examen_neurologicoId);
                        pruebaNueva.Disartia = Convert.ToBoolean(prueba.Disartia);
                        pruebaNueva.ConjuntivasId = Convert.ToInt32(prueba.ConjuntivasId);
                        pruebaNueva.MarchaId = Convert.ToInt32(prueba.MarchaId);
                        pruebaNueva.PupilasId = Convert.ToInt32(prueba.PupilasId);
                        pruebaNueva.CoordinacionId = Convert.ToInt32(prueba.CoordinacionId);
                        pruebaNueva.Reflejos_pupilaresId = Convert.ToInt32(prueba.Reflejos_pupilaresId);
                        pruebaNueva.TendinososId = Convert.ToInt32(prueba.TendinososId);
                        pruebaNueva.RomberqId = Convert.ToInt32(prueba.RomberqId);
                        pruebaNueva.ConductaId = Convert.ToInt32(prueba.ConductaId);
                        pruebaNueva.LenguajeId = Convert.ToInt32(prueba.LenguajeId);
                        pruebaNueva.AtencionId = Convert.ToInt32(prueba.AtencionId);
                        pruebaNueva.OrientacionId = Convert.ToInt32(prueba.OrientacionId);
                        pruebaNueva.DiadococinenciaId = Convert.ToInt32(prueba.DiadococinenciaId);
                        pruebaNueva.DedoId = Convert.ToInt32(prueba.DedoId);
                        pruebaNueva.TalonId = Convert.ToInt32(prueba.TalonId);
                        pruebaNueva.Alcoholimetro = prueba.Alcoholimetro;
                        pruebaNueva.TA = prueba.TA;
                        pruebaNueva.FC = prueba.FC;
                        pruebaNueva.FR = prueba.FR;
                        pruebaNueva.Pulso = prueba.Pulso;

                        pruebaNueva.TrackingId = _prueba.TrackingId;
                        pruebaNueva.Id = _prueba.Id;
                        ControlPruebaExamenMedico.Actualizar(pruebaNueva);
                    }

                    return new { exitoso = true, mensaje = mode, Id = pruebaNueva.Id.ToString(), TrackingId = pruebaNueva.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static string getexamen(string trackingid)
        {
            try
            {
                Entity.ExamenMedico _examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));



                return JsonConvert.SerializeObject(new { exitoso = true, obj = _examen, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getpruebaexamen(string trackingid)
        {
            try
            {
                Entity.PruebaExamenMedico _pruebaexamen = ControlPruebaExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));



                return JsonConvert.SerializeObject(new { exitoso = true, obj = _pruebaexamen, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen Medico" }).Consultar)
                {
                    Entity.ServicioMedico examen = ControlServicoMedico.ObtenerPorTrackingId(trackingid);
                    Entity.DetalleDetencion detalleDetencionAux = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                    Entity.DetalleDetencion detalleDetencion = new Entity.DetalleDetencion();

                    if (examen != null)
                    {
                        detalleDetencion = ControlDetalleDetencion.ObtenerPorId(examen.DetalledetencionId);
                    }
                    else if (detalleDetencionAux != null)
                    {
                        detalleDetencion = detalleDetencionAux;
                    }

                    

                    Entity.Detenido interno = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);

                    var alias = ControlAlias.ObteneTodos();
                    string cadalias = "";
                    foreach (var item in alias.Where(x=>x.DetenidoId==interno.Id && x.Habilitado==1 && x.Activo))
                    {
                        cadalias += item.Nombre + ",";
                    }
                    if (cadalias.Length > 0)
                    {
                        cadalias = cadalias.Substring(0, cadalias.Length - 1);
                    }

                 

                    if (interno != null)
                    {
                        if (string.IsNullOrEmpty(interno.RutaImagen))
                        {
                            interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                        }
                    }
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalle = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);

                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalle.CentroId);

                    Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);

                    Entity.Domicilio nacimiento = ControlDomicilio.ObtenerPorId(interno.DomicilioNId);

                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                            var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                            Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                            foreach (var item in detenidosEvento)
                            {
                                if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                    detenidoEvento = item;
                            }

                            if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                            else
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }

                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }
                    edad = detalleDetencion.DetalledetencionEdad;
                    var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                    Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                    var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                    Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                    object obj = null;
                    object objnacimiento = null;
                    bool tienenacimiento = true;
                    object objdomicilio = null;
                    bool tienedomicilio = true;
                    object objgeneral = null;
                    bool tienegeneral = true;
                    int coloniaId = 0;
                    if (domicilio != null)
                    {
                        if(domicilio.ColoniaId != null)
                            coloniaId = Convert.ToInt32(domicilio.ColoniaId);
                    }

                    string numero = "";
                    string calle = "";

                    if (domicilio != null)
                    {
                        if (domicilio.Numero != null)
                            numero = domicilio.Numero;

                        if (domicilio.Calle != null)
                            calle = domicilio.Calle;
                    }

                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = (detalle) != null ? detalle.Expediente : "",
                        Centro = (institucion != null) ? institucion.Nombre : "No registrado",
                        Fecha = detalle.Fecha != null ? Convert.ToDateTime(detalle.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                        RutaImagen = interno.RutaImagen,
                        Edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        SexoId = sexo != null ? sexo.Id : 0,
                        CalleDomicilio = domicilio != null ? calle + " #" + numero : "Domicilio no registrado",
                        CodigoPostalDomicilio = coloniaId != 0 ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                        aliasdetenido = cadalias,
                        Lesion_simple_vista = !detalle.Lesion_visible?"False":"True",
                    };

                    if (nacimiento != null)
                    {
                        objnacimiento = new
                        {
                            Id = nacimiento.Id,
                            TrackingId = nacimiento.TrackingId,
                            CalleNacimiento = (nacimiento.Calle) != null ? nacimiento.Calle : "",
                            NumeroNacimiento = nacimiento.Numero != null ? nacimiento.Numero : "",
                            //ColoniaNacimiento = nacimiento.Colonia,
                            //CPNacimiento = nacimiento.CP,
                            TelefonoNacimiento = nacimiento.Telefono != null ? nacimiento.Telefono : "",
                            PaisNacimiento = nacimiento.PaisId != null ? nacimiento.PaisId : 0,
                            EstadoNacimiento = (nacimiento.EstadoId) != null ? nacimiento.EstadoId : 0,
                            MunicipioNacimiento = (nacimiento.MunicipioId) != null ? nacimiento.MunicipioId : 0,
                            LocalidadNacimiento = (nacimiento.Localidad) != null ? nacimiento.Localidad : "",
                            ColoniaIdNacimiento = (nacimiento.ColoniaId) != null ? nacimiento.ColoniaId : 0,
                            CodigoPostalNacimiento = (nacimiento.ColoniaId) != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(nacimiento.ColoniaId)).CodigoPostal : ""
                        };
                    }
                    else
                    {
                        tienenacimiento = false;
                    }

                    if (domicilio != null)
                    {
                        string calle2 = "";
                        string numero2 = "";
                        if (domicilio.Calle != null)
                            calle2 = domicilio.Calle;

                        if (domicilio.Numero != null)
                            numero2 = domicilio.Numero;

                        objdomicilio = new
                        {
                            Id = domicilio.Id,
                            TrackingId = domicilio.TrackingId,

                            CalleDomicilio = domicilio != null ? calle2 + " #" + numero2 : "Domicilio no registrado",
                            NumeroDomicilio = numero2,
                            //ColoniaDomicilio = domicilio.Colonia,
                            //CPDomicilio = domicilio.CP,
                            TelefonoDomicilio = domicilio.Telefono,
                            PaisDomicilio = domicilio.PaisId,
                            EstadoDomicilio = domicilio.EstadoId,
                            MunicipioDomicilio = domicilio.MunicipioId,
                            LocalidadDomicilio = domicilio.Localidad != null ? domicilio.Localidad : "Localidad no registrada",
                            ColoniaIdDomicilio = (domicilio.ColoniaId) != null ? domicilio.ColoniaId : 0,

                        };
                    }
                    else
                    {
                        tienedomicilio = false;
                    }

                    if (general != null)
                    {
                        objgeneral = new
                        {
                            Id = general.Id,
                            TrackingId = general.TrackingId,
                            FechaNacimineto = general.FechaNacimineto != null ? general.FechaNacimineto.ToString("dd/MM/yyyy") : "",
                            Nacionalidad = general.NacionalidadId,
                            RFC = general.RFC,
                            Escolaridad = general.EscolaridadId,
                            Religion = general.ReligionId,
                            Ocupacion = general.OcupacionId,
                            Estado = general.EstadoCivilId,
                            Etnia = general.EtniaId,
                            Sexo = general.SexoId,
                            Mental = general.EstadoMental,
                            Inimputable = general.Inimputable,
                            Edad = edad
                        };
                    }
                    else
                    {
                        tienegeneral = false;
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, nacimiento = objnacimiento, tienenacimiento = tienenacimiento, domicilio = objdomicilio, tienedomicilio = tienedomicilio, general = objgeneral, tienegeneral = tienegeneral });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getSexoEdad(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen Medico" }).Consultar)
                {
                    Entity.ExamenMedico examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingid));
                    Entity.DetalleDetencion detalleDetencionAux = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                    Entity.DetalleDetencion detalle = new Entity.DetalleDetencion();

                    if (examen != null)
                    {
                        detalle = ControlDetalleDetencion.ObtenerPorId(examen.DetalleDetencionId);
                    }
                    else if (detalleDetencionAux != null)
                    {
                        detalle = detalleDetencionAux;
                    }

                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };

                    Entity.Domicilio nacimiento = ControlDomicilio.ObtenerPorId(interno.DomicilioNId);
                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                            var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                            Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                            foreach (var item in detenidosEvento)
                            {
                                if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                    detenidoEvento = item;
                            }

                            if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                            else
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }

                    }

                    object obj = null;
                    if (edad == 0)
                    {
                        edad = general.Edaddetenido;
                    }
                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        Edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        Sexo = general != null ? general.SexoId : 0,
                        FechaNacimiento = general != null && general.FechaNacimineto != DateTime.MinValue ? general.FechaNacimineto.ToString("dd/MM/yyyy") : ""
                    };

                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, mensaje = "" });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getSexo()
        {
            List<Combo> combo = new List<Combo>();


            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getDatosExamen(string trackingExamenId)
        {
            try
            {
                //Entity.ExamenMedico examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(trackingExamenId));
                //Entity.PruebaExamenMedico prueba = ControlPruebaExamenMedico.ObtenerPorExamenMedicoId(examen.Id);

                Entity.ServicioMedico servicioMedico = ControlServicoMedico.ObtenerPorTrackingId(trackingExamenId);

                Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsico = new Entity.CertificadoMedicoPsicoFisiologico();
                Entity.CertificadoLesion certificadoLesion = new Entity.CertificadoLesion();
                Entity.CertificadoQuimico certificadoQuimico = new Entity.CertificadoQuimico();
                CertificadoquimicoAux Certificadoquimico = new CertificadoquimicoAux();
                CertificadoPsicofisiologicoaux psicofisiologico = new CertificadoPsicofisiologicoaux();
                CertificadoLesionaux certificadoLesionaux = new CertificadoLesionaux();
                //charge values
                if (servicioMedico !=null)
                    {
                     certificadoMedicoPsico = ControlCertificadoPsicoFisiologico.ObtenerPorId(servicioMedico.CertificadoMedicoPsicofisiologicoId);
                     certificadoLesion = ControlCertificadoLesiones.ObtenerPorId(servicioMedico.CertificadoLesionId);
                     certificadoQuimico = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);
                    var detalledetencion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);

                    if (servicioMedico.CertificadoQuimicoId!=0)
                    {
                        Certificadoquimico.Folio = certificadoQuimico.Foliocertificado!=null?certificadoQuimico.Foliocertificado.ToString():"";
                        Certificadoquimico.Fecha_proceso = certificadoQuimico.Fecha_proceso.ToString("dd/MM/yyyy HH:mm:ss");
                        Certificadoquimico.Fecha_toma = certificadoQuimico.Fecha_toma.ToString("dd/MM/yyyy HH:mm:ss");
                        Certificadoquimico.EtanolId = certificadoQuimico.EtanolId.ToString();

                        var opcionComunEtanol = ControlCatalogo.Obtener(certificadoQuimico.EtanolId, Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun));
                        Certificadoquimico.ResultadoEtanol = opcionComunEtanol.Nombre.ToUpper();
                        Certificadoquimico.Grado = certificadoQuimico.Grado.ToString();
                        Certificadoquimico.BenzodiapinaId = certificadoQuimico.BenzodiapinaId.ToString();
                        Certificadoquimico.AnfetaminaId = certificadoQuimico.AnfetaminaId.ToString();
                        Certificadoquimico.CannabisId = certificadoQuimico.CannabisId.ToString();
                        Certificadoquimico.CocaínaId = certificadoQuimico.CocainaId.ToString();
                        Certificadoquimico.ExtasisId = certificadoQuimico.ExtasisId.ToString();
                        Certificadoquimico.EquipoautilizarId = certificadoQuimico.EquipoautilizarId.ToString();

                        Certificadoquimico.Dimension = certificadoQuimico.Dimension == 1 ? "True" : "False";
                        Certificadoquimico.Axysm = certificadoQuimico.Axysm == 1 ? "True" : "False";
                        Certificadoquimico.Architect = certificadoQuimico.Architect == 1 ? "True" : "False";
                        Certificadoquimico.VivaE = certificadoQuimico.VivaE == 1 ? "True" : "False";
                    }
                    else
                    {
                        certificadoQuimico = new Entity.CertificadoQuimico();
                        Certificadoquimico.Folio = "Q";
                        Certificadoquimico.Fecha_proceso = certificadoQuimico.Fecha_proceso.ToString();
                        Certificadoquimico.Fecha_toma = certificadoQuimico.Fecha_toma.ToString();
                        Certificadoquimico.EtanolId = certificadoQuimico.EtanolId.ToString();

                        //var opcionComunEtanol = ControlCatalogo.Obtener(certificadoQuimico.EtanolId, Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun));
                        Certificadoquimico.ResultadoEtanol ="";
                        Certificadoquimico.Grado = certificadoQuimico.Grado.ToString();
                        Certificadoquimico.BenzodiapinaId = certificadoQuimico.BenzodiapinaId.ToString();
                        Certificadoquimico.AnfetaminaId = certificadoQuimico.AnfetaminaId.ToString();
                        Certificadoquimico.CannabisId = certificadoQuimico.CannabisId.ToString();
                        Certificadoquimico.CocaínaId = certificadoQuimico.CocainaId.ToString();
                        Certificadoquimico.ExtasisId = certificadoQuimico.ExtasisId.ToString();
                        Certificadoquimico.EquipoautilizarId = certificadoQuimico.EquipoautilizarId.ToString();

                        Certificadoquimico.Dimension = certificadoQuimico.Dimension == 1 ? "True" : "False";
                        Certificadoquimico.Axysm = certificadoQuimico.Axysm == 1 ? "True" : "False";
                        Certificadoquimico.Architect = certificadoQuimico.Architect == 1 ? "True" : "False";
                        Certificadoquimico.VivaE = certificadoQuimico.VivaE == 1 ? "True" : "False";

                    }
                    if (servicioMedico.CertificadoLesionId != 0)
                    {
                        //certificadoLesionaux.Aptitud = certificadoLesion.Aptitud== 1 ? "True" : "False";
                        var tipolesiondetalle = ControlCertificadoLesionTipoLesionDetalle.ObteneroPortipoLesion(certificadoLesion.Tipo_lesionId);
                        List<string> tipolesionid = new List<string>();
                        foreach (var item in tipolesiondetalle)
                        {
                            tipolesionid.Add(item.TipoLesionId.ToString());
                        }

                        certificadoLesionaux.Lesion_simple_vista= certificadoLesion.Lesion_simple_vista == 1 ? "True" : "False";
                        certificadoLesionaux.Fallecimiento = certificadoLesion.Fallecimiento == 1 ? "True" : "False";
                        certificadoLesionaux.Envio_hospital = certificadoLesion.Envio_hospital == 1 ? "True" : "False";
                        certificadoLesionaux.Observaciones_antecedentes = certificadoLesion.Observaciones_antecedentes;
                        certificadoLesionaux.Observacion_alergia = certificadoLesion.Observacion_alergia;
                        certificadoLesionaux.Observacion_general = certificadoLesion.Observacion_general;
                        certificadoLesionaux.Observacion_tatuje = certificadoLesion.Observacion_tatuje;
                        certificadoLesionaux.Observaciones_lesion = certificadoLesion.Observaciones_lesion;
                        certificadoLesionaux.Tipo_lesionId = tipolesionid.ToArray();
                        certificadoLesionaux.Sinlesion = certificadoLesion.Sinlesion == 1 ? "True" : "False";
                        certificadoLesionaux.FotografiasId = certificadoLesion.FotografiasId.ToString();
                        certificadoLesionaux.Fechavaloracion = certificadoLesion.Fechavaloracion.ToString("dd/MM/yyyy HH:mm:ss");
                        var alergiadetalle = ControlCertificadoLesionAlergiaDetalle.ObteneroPortipoLesion(certificadoLesion.Tipo_alergiaId);

                        List<string> alergiaId = new List<string>();

                        foreach (var item in alergiadetalle)
                        {
                            alergiaId.Add(item.AlergiaId.ToString());

                        }
                        certificadoLesionaux.Tipo_alergiaId = alergiaId.ToArray();

                        var tatuajesDetalle = ControlCertificadoLesionTatuajesDetalle.ObteneroPortipoAntecedenteIdn(certificadoLesion.Tipo_tatuajeId);
                        List<string> tatuajeid = new List<string>();

                        foreach (var item in tatuajesDetalle)
                        {
                            tatuajeid.Add(item.TatuajeId.ToString());

                        }
                        certificadoLesionaux.Tipo_tatuajeId = tatuajeid.ToArray();

                        var antecentesdetalle = ControlCertificadoLesionAntecedentesDetalle.ObteneroPortipoAntecedenteIdn(certificadoLesion.AntecedentesId);
                        List<string> antecedenteid = new List<string>();

                        foreach (var item in antecentesdetalle)
                        {
                            antecedenteid.Add(item.AntecedenteId.ToString());

                        }
                        certificadoLesionaux.AntecedentesId = antecedenteid.ToArray();
                    }
                    if (servicioMedico.CertificadoMedicoPsicofisiologicoId != 0)
                    {
                        psicofisiologico.FechaValoracion = certificadoMedicoPsico.FechaValoracion.ToString("dd/MM/yyyy HH:mm:ss");
                        psicofisiologico.AlientoId = certificadoMedicoPsico.AlientoId.ToString();
                        psicofisiologico.MarchaId = certificadoMedicoPsico.MarchaId.ToString();
                        psicofisiologico.ActitudId = certificadoMedicoPsico.ActitudId.ToString();
                        psicofisiologico.AtencionId = certificadoMedicoPsico.AtencionId.ToString();
                        psicofisiologico.Cavidad_oralId = certificadoMedicoPsico.Cavidad_oralId.ToString();
                        psicofisiologico.PupilaId = certificadoMedicoPsico.PupilaId.ToString();
                        psicofisiologico.Reflejo_pupilarId = certificadoMedicoPsico.Reflejo_pupilarId.ToString();
                        psicofisiologico.ConjuntivaId = certificadoMedicoPsico.ConjuntivaId.ToString();
                        psicofisiologico.LenguajeId = certificadoMedicoPsico.LenguajeId.ToString();
                        psicofisiologico.RombergId = certificadoMedicoPsico.RombergId.ToString();
                        psicofisiologico.Ojos_abiertos_dedo_dedoId = certificadoMedicoPsico.Ojos_abiertos_dedo_dedoId.ToString();
                        psicofisiologico.Ojos_cerrados_dedo_dedoId = certificadoMedicoPsico.Ojos_cerrados_dedo_dedoId.ToString();
                        psicofisiologico.Ojos_abiertos_dedo_narizId = certificadoMedicoPsico.Ojos_abiertos_dedo_narizId.ToString();
                        psicofisiologico.Ojos_cerrados_dedo_narizId = certificadoMedicoPsico.Ojos_cerrados_dedo_narizId.ToString();
                        psicofisiologico.TA = certificadoMedicoPsico.TA.ToString();
                        psicofisiologico.FC = certificadoMedicoPsico.FC.ToString();
                        psicofisiologico.FR = certificadoMedicoPsico.FR.ToString();
                        psicofisiologico.Pulso = certificadoMedicoPsico.Pulso.ToString();
                        psicofisiologico.No_valorabe = certificadoMedicoPsico.No_valorabe == 1 ? "True" : "False";
                        psicofisiologico.Observacion_extra = certificadoMedicoPsico.Observacion_extra;
                        var certifficadofiscoorientacion = ControlCertificadoPsicoFisologicoOrientacionDetalle.Obetener(certificadoMedicoPsico.OrientacionId);
                        List<string> orientacionId = new List<string>();
                        foreach (var item in certifficadofiscoorientacion)

                        {
                            orientacionId.Add(item.OrientacionId.ToString());
                        }
                        psicofisiologico.OrientacionId = orientacionId.ToArray();
                        psicofisiologico.Sustento_toxicologico = certificadoMedicoPsico.Sustento_toxicologico == 1 ? "True" : "False";
                        psicofisiologico.Observacion_extra = certificadoMedicoPsico.Observacion_extra != null ? certificadoMedicoPsico.Observacion_extra : "";

                        var certificadoconclusion = ControlCertificadoPsicoFisologicoConclusionDetalle.Obtener(certificadoMedicoPsico.ConclusionId);
                        List<string> conclusionid = new List<string>();

                        foreach (var item in certificadoconclusion)
                        {
                            conclusionid.Add(item.ConclusionId.ToString());
                        }

                        psicofisiologico.ConclusionId = conclusionid.ToArray();
                    }
                }

                if(psicofisiologico.ActitudId==null)
                {
                    psicofisiologico = null;

                }

                object objexamen = null;
                object objpruebaK = null;

                //objexamen = new
                //{
                //    InspeccionOcular = examen.InspeccionOcular.ToString(),
                //    IndicacionesMedicas = examen.IndicacionesMedicas.ToString(),
                //    Observacion = examen.Observacion,
                //    RiesgoVida = examen.RiesgoVida.ToString(),
                //    ConsecuenciasLesiones = examen.ConsecuenciasLesiones.ToString(),
                //    DiasSanarLesiones = examen.DiasSanarLesiones.ToString(),
                //    Tipo = examen.TipoExamen.Id.ToString(),
                //    Fecha = examen.Fecha.ToString(),
                //    Lesion_Visible = examen.Lesion_visible.ToString(),
                //    Fallecimiento = examen.Fallecimiento.ToString(),
                //};
                //if (prueba != null)
                //{
                //    objpruebaK = new
                //    {
                //        MucosasId = prueba.MucosasId.ToString(),
                //        AlientoId = prueba.AlientoId.ToString(),
                //        Examen_neurologicoId = prueba.Examen_neurologicoId.ToString(),
                //        Disartia = prueba.Disartia.ToString(),
                //        ConjuntivasId = prueba.ConjuntivasId.ToString(),
                //        MarchaId = prueba.MarchaId.ToString(),
                //        PupilasId = prueba.PupilasId.ToString(),
                //        CoordinacionId = prueba.CoordinacionId.ToString(),
                //        Reflejos_pupilaresId = prueba.Reflejos_pupilaresId.ToString(),
                //        TendinososId = prueba.TendinososId.ToString(),
                //        RomberqId = prueba.RomberqId.ToString(),
                //        ConductaId = prueba.ConductaId.ToString(),
                //        LenguajeId = prueba.LenguajeId.ToString(),
                //        AtencionId = prueba.AtencionId.ToString(),
                //        OrientacionId = prueba.OrientacionId.ToString(),
                //        DiadococinenciaId = prueba.DiadococinenciaId.ToString(),
                //        DedoId = prueba.DedoId.ToString(),
                //        TalonId = prueba.TalonId.ToString(),
                //        Alcoholimetro = prueba.Alcoholimetro.ToString(),
                //        TA = prueba.TA.ToString(),
                //        FC = prueba.FC.ToString(),
                //        FR = prueba.FR.ToString(),
                //        Pulso = prueba.Pulso.ToString(),
                //    };
                //}


                return JsonConvert.SerializeObject(new { exitoso = true,CertificadoQuimico= Certificadoquimico, Psicofisiologico= psicofisiologico,CertificadoLesion= certificadoLesionaux, objexamen = objexamen, objprueba = objpruebaK });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getlistadolesiones(string select)
        {

            var combo = new List<Combo>();
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32( Entity.TipoDeCatalogo.tipo_lesion));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        //s0lve_type
        [WebMethod]
        public static List<Combo> getListado(string select)
        {
            int tipo = 0;
            switch (select)
            {
                case "mucosas":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.mucosas); break;
                case "aliento":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.aliento); break;
                case "neurologico":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.examen_neurologico); break;
                case "conjuntivas":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.conjuntivas); break;
                case "marcha":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.marcha); break;
                case "pupilas":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.pupilas); break;
                case "coordinacion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.coordinacion); break;
                case "pupilares":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.reflejos_pupilares); break;
                case "osteo":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.reflejos_osteo_tendinosos); break;
                case "romberq":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.romberq); break;
                case "conducta":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.conducta); break;
                case "lenguaje":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.lenguaje); break;
                case "atencion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.atencion); break;
                case "orientacion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.orientacion); break;
                case "diadococinencia":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.diadococinencia); break;
                case "ojos_abiertos_dedo_dedo":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.ojos_abiertos_dedo_dedo); break;
                case "ojos_abiertos_dedo_nariz":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.ojos_abiertos_dedo_nariz); break;
                case "ojos_cerrados_dedo_dedo":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.ojos_cerrados_dedo_dedo); break;
                case "ojos_cerrados_dedo_nariz":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.ojos_cerrados_dedo_nariz); break;
                case "talon":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.talon_rodilla); break;
                case "tipo_examen":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_examen); break;
                case "tipo_tatuaje":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_tatuaje); break;
                case "tipo_lesion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_lesion); break;
                case "tipo_alergia":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_alergia); break;
                case "tipo_intoxicacion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.tipo_intoxicacion); break;
                case "antecedentes":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.Antecedentes); break;
                case "conclusion":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.Conclusion); break;
                case "idactitud":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.actitud); break;
                case "cavidadoral":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.cavidad_oral); break;
                case "Etanol":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun); break;
                case "Benzodiazepina":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun); break;
                case "anfetamina":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun); break;
                case "cannabis":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun); break;
                case "cocaina":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun); break;
                case "extasis":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun); break;
                case "equipo_a_utilizar":
                    tipo = Convert.ToInt32(Entity.TipoDeCatalogo.equipo_a_utilizar); break;
            }

            var combo = new List<Combo>();
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerHabilitados(tipo);

            if (listado.Count > 0)
            {
                foreach (var item in listado)
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            var listaOrdenada = combo.OrderBy(x => x.Desc).ToList();
            if (tipo == Convert.ToInt32(Entity.TipoDeCatalogo.tipo_lesion) || tipo == Convert.ToInt32(Entity.TipoDeCatalogo.tipo_alergia)
                || tipo == Convert.ToInt32(Entity.TipoDeCatalogo.Antecedentes) || tipo == Convert.ToInt32(Entity.TipoDeCatalogo.tipo_tatuaje))
            {
                var sinDato = listaOrdenada.FirstOrDefault(x => x.Desc.ToUpper().Contains("SIN DATO"));
                if (sinDato != null)
                {
                    listaOrdenada.Remove(sinDato);
                    listaOrdenada.Add(sinDato);
                }
            }
            else
            {
                var noValorable = listaOrdenada.FirstOrDefault(x => x.Desc.ToUpper().Contains("NO VALORABLE"));
                if (noValorable != null)
                {
                    listaOrdenada.Remove(noValorable);
                    listaOrdenada.Add(noValorable);
                }
            }
            return listaOrdenada.ToList();
        }

        [WebMethod]
        public static DataTable getDataTatuaje(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("T.Activo", "1"));
                        where.Add(new Where("EM.TrackingId", tracking));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                        "T.Id",
                        "T.TrackingId",
                        "T.Lugar",
                        "T.Observacion",
                        "TT.Nombre Tipo",
                        "T.TipoTatuajeId",
                        "T.Habilitado"

                    },
                            from = new Table("tatuaje", "T"),
                            joins = new List<Join>
                    {
                        new Join(new Table("tipo_tatuaje", "TT"), "TT.Id  = T.TipoTatuajeId"),

                        new Join(new Table("examen_medico", "EM"), "EM.Id  = T.ExamenMedicoId"),
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static DataTable GetLesionLog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        where.Add(new Where("B.RolId", "<>", "13"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Lugar",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("lesionlog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("lesion","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static DataTable GetIntoxicacionLog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        where.Add(new Where("B.RolId", "<>", "13"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Observacion",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("intoxicacionlog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("intoxicacion","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static DataTable GetTatuajeLog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid)!=0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        where.Add(new Where("B.RolId", "<>", "13"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Lugar",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("tatuajelog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("tatuaje","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt= dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);

                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }
            }
            catch(Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static DataTable getDataLesion(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("L.Activo", "1"));
                        where.Add(new Where("EM.TrackingId", tracking));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                        "L.Id",
                        "L.TrackingId",
                        "L.Lugar",
                        "L.Observacion",
                        "TL.Nombre Tipo",
                        "L.TipoLesionId",
                        "L.Habilitado"

                    },
                            from = new Table("lesion", "L"),
                            joins = new List<Join>
                    {
                        new Join(new Table("tipo_lesion", "TL"), "TL.Id  = L.TipoLesionId"),

                        new Join(new Table("examen_medico", "EM"), "EM.Id  = L.ExamenMedicoId"),
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static DataTable getDataIntoxicacion(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("I.Activo", "1"));
                        where.Add(new Where("EM.TrackingId", tracking));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                        "I.Id",
                        "I.TrackingId",
                        "I.TipoIntoxicacionId",
                        "I.Observacion",
                        "TI.Nombre Tipo",
                        "I.Habilitado"

                    },
                            from = new Table("intoxicacion", "I"),
                            joins = new List<Join>
                    {
                        new Join(new Table("tipo_intoxicacion", "TI"), "TI.Id  = I.TipoIntoxicacionId"),

                        new Join(new Table("examen_medico", "EM"), "EM.Id  = I.ExamenMedicoId"),
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static object saveAdicional(AdicionalAux item)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {

                    var itemNuevo = new Entity.AdicionalExamenMedico();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    Entity.ExamenMedico examen = ControlExamenMedico.ObtenerPorTrackingId(new Guid(item.ExamenTrackingId));
                    itemNuevo.ExamenMedicoId = examen.Id;
                    itemNuevo.Activo = true;
                    itemNuevo.Creadopor = usId;
                    itemNuevo.Habilitado = true;
                    itemNuevo.Clasificacion = item.Clasificacion;
                    itemNuevo.Lugar = Convert.ToInt32(item.Lugar); 
                    itemNuevo.Observacion = item.Observacion;
                    if (item.Id != "")
                        itemNuevo.Id = Convert.ToInt32(item.Id);
                    switch (item.Clasificacion)
                    {
                        case "tatuaje":
                            itemNuevo.Tipo = ControlCatalogo.Obtener(Convert.ToInt32(item.Tipo), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_tatuaje));
                            break;
                        case "intoxicacion":
                            itemNuevo.Tipo = ControlCatalogo.Obtener(Convert.ToInt32(item.Tipo), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_intoxicacion));
                            break;
                        case "lesion":
                            itemNuevo.Tipo = ControlCatalogo.Obtener(Convert.ToInt32(item.Tipo), Convert.ToInt32(Entity.TipoDeCatalogo.tipo_lesion));
                            break;
                    }

                    if (string.IsNullOrEmpty(item.TrackingId))
                    {
                        mode = @"registró";
                        itemNuevo.TrackingId = Guid.NewGuid();
                        itemNuevo.Id = ControlAdicionalExamenMedico.Guardar(itemNuevo);
                    }
                    else
                    {
                        mode = @"actualizó";
                        Entity.AdicionalExamenMedico auxitem = new Entity.AdicionalExamenMedico();
                        auxitem.TrackingId = new Guid(item.TrackingId);
                        auxitem.Clasificacion = itemNuevo.Clasificacion;
                        itemNuevo.TrackingId = auxitem.TrackingId;
                        Entity.AdicionalExamenMedico _item = ControlAdicionalExamenMedico.ObtenerPorTrackingId(auxitem);
                        ControlAdicionalExamenMedico.Actualizar(itemNuevo);
                    }

                    return new { exitoso = true, mensaje = mode, Id = itemNuevo.Id.ToString(), TrackingId = itemNuevo.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static string blockuser(AdicionalAux item)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Eliminar)
                {
                    Entity.AdicionalExamenMedico auxitem = new Entity.AdicionalExamenMedico();
                    auxitem.TrackingId = new Guid(item.TrackingId);
                    auxitem.Clasificacion = item.Clasificacion;
                    var examenAux = ControlAdicionalExamenMedico.ObtenerPorTrackingId(auxitem);
                    examenAux.Habilitado = (examenAux.Habilitado) ? false : true;
                    string mensaje = examenAux.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                    ControlAdicionalExamenMedico.Actualizar(examenAux);

                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, mensaje });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveSexoEdad(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    Entity.General general = new Entity.General();
                    Entity.DetalleDetencion detalle_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[2].ToString()));
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;

                    if (detalle_detencion != null)
                    {
                        general = ControlGeneral.ObtenerPorDetenidoId(detalle_detencion.DetenidoId);
                    }


                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            
                        }

                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }

                    detalle_detencion.DetalledetencionEdad = Convert.ToInt32(datos[0]);
                    detalle_detencion.DetalledetencionSexoId = Convert.ToInt32(datos[1]);
                    ControlDetalleDetencion.Actualizar(detalle_detencion);


                    if (general != null)
                    {
                        mode = @"actualizó";

                        //var fechaNacimiento = Convert.ToDateTime(datos[0]);
                        //edad = CalcularEdad(fechaNacimiento);
                        edad = Convert.ToInt32(datos[0]);
                        if(edad < 1)
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La edad del detenido debe de ser mayor a un año.", Id = "", TrackingId = "" });


                        //general.FechaNacimineto = fechaNacimiento;
                        //general.Edaddetenido = edad;
                        general.SexoId = Convert.ToInt32(datos[1].ToString());
                        general.Edaddetenido = edad;
                        ControlGeneral.Actualizar(general);
                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                        
                        //
                        var interno = ControlDetenido.ObtenerPorId(detalle_detencion.DetenidoId);
                        var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                        var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                        var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                        Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                        foreach (var item in detenidosEvento)
                        {
                            if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                detenidoEvento = item;
                        }

                        if (detenidoEvento != null)
                        {
                            detenidoEvento.Edad = edad;
                            ControlDetenidoEvento.Actualizar(detenidoEvento);
                        }
                        //
                    }
                    else
                    {
                        mode = "registró";

                        general = new Entity.General();
                        general.DetenidoId = detalle_detencion.DetenidoId;
                        general.TrackingId = Guid.NewGuid();
                        general.FechaNacimineto = Convert.ToDateTime(datos[0]);
                        general.RFC = "";
                        general.NacionalidadId = 39;
                        general.EscolaridadId = 19;
                        general.ReligionId = 10;
                        general.OcupacionId = 18;
                        general.EstadoCivilId = 8;
                        general.EtniaId = 59;
                        general.Edaddetenido = Convert.ToInt32(datos[0]);
                        general.SexoId = Convert.ToInt32(datos[1].ToString());
                        general.EstadoMental = false;
                        general.Inimputable = false;
                        general.Id = ControlGeneral.Guardar(general);

                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                        edad = general.Edaddetenido;
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, sexo = sexo.Nombre, edad = edad });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" });
            }
        }

        [WebMethod]
        public static string GetCertificadoLesionFotografias()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var usuario = ControlUsuario.Obtener(usId);
            if(usuario != null)
            {
                var certificadoLesionFotografias = new Entity.CertificadoLesionFotografias();
                certificadoLesionFotografias.Creadopor = usuario.Id;
                certificadoLesionFotografias.FechaHora = DateTime.Now;
                var idCert = ControlCertificadoLesionFotografias.Guardar(certificadoLesionFotografias);

                return JsonConvert.SerializeObject(new { exitoso = true, valor = idCert });
            }
            else
            {
                return JsonConvert.SerializeObject(new { exitoso = false });
            }
        }

        [WebMethod]
        public static string SaveFotografiaLesion(CertificadoLesionFotografiaDetalleAux cert)
        {
            Entity.CertificadoLesionFotografiasDetalle certDetalle = new Entity.CertificadoLesionFotografiasDetalle();
            var certificadoFotografia = ControlCertificadoLesionFotografias.ObtenerPorId(int.Parse(cert.IdCertificadoLesionFotografia));
            if(certificadoFotografia != null)
            {
                var mode = "";
                if (Convert.ToInt32(cert.Id) == 0)
                {
                    mode = "registró";
                    certDetalle.IdCertificado_lesionfotografias = int.Parse(cert.IdCertificadoLesionFotografia);
                    certDetalle.Fotografia = cert.Fotografia;
                    certDetalle.Descripcion = cert.Descripcion;

                    var idCertDetalle = ControlCertificadoLesionFotografiasDetalle.Guardar(certDetalle);

                    if (idCertDetalle == -1)
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se pudo guardar la fotografía de la lesión." });
                    }
                }
                else
                {
                    mode = "modificó";
                    var certDetalleAux = ControlCertificadoLesionFotografiasDetalle.ObtenerPorId(int.Parse(cert.Id));
                    if (certDetalleAux != null)
                    {
                        certDetalle.Id = int.Parse(cert.Id);
                        certDetalle.IdCertificado_lesionfotografias = int.Parse(cert.IdCertificadoLesionFotografia);
                        certDetalle.Fotografia = cert.Fotografia;
                        certDetalle.Descripcion = cert.Descripcion;

                        ControlCertificadoLesionFotografiasDetalle.Actualizar(certDetalle);
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se pudo guardar la fotografía de la lesión." });
                    }
                }
                if(!string.IsNullOrEmpty(cert.TrackingCertificado))
                {
                    var medico = ControlServicoMedico.ObtenerPorTrackingId(cert.TrackingCertificado);
                    if (medico != null)
                    {
                        var certificado = ControlCertificadoLesiones.ObtenerPorId(medico.CertificadoLesionId);
                        if(certificado != null){
                            certificado.FotografiasId = int.Parse(cert.IdCertificadoLesionFotografia);
                            ControlCertificadoLesiones.Actualizar(certificado);
                        }
                            

                    }
                }
                
                
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode });
            }
            else
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se pudo guardar la fotografía de la lesión." });
            }
        }

        [WebMethod]
        public static string DeleteFotografiaLesion(string id)
        {
            Entity.CertificadoLesionFotografiasDetalle certDetalle = ControlCertificadoLesionFotografiasDetalle.ObtenerPorId(Convert.ToInt32(id));
            if (certDetalle != null)
            {
                var certificadoFotografia = ControlCertificadoLesionFotografias.ObtenerPorId(certDetalle.IdCertificado_lesionfotografias);
                if (certificadoFotografia != null)
                {
                    certDetalle.Activo = false;
                    ControlCertificadoLesionFotografiasDetalle.Actualizar(certDetalle);

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "La fotografía de la lesión se eliminó exitosamente." });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se pudo eliminar la fotografía de la lesión." });
                }
            }
            else
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se pudo eliminar la fotografía de la lesión." });
            }
        }

        [WebMethod]
        public static DataTable GetLesionesFotografias(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string certificadoLesionFotografiasId)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" }).Registrar)
                {
                    if (!emptytableadd)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        if (string.IsNullOrWhiteSpace(certificadoLesionFotografiasId)) certificadoLesionFotografiasId = "0";

                        List<Where> where = new List<Where>();
                        where.Add(new Where("IdCertificado_lesionfotografias", certificadoLesionFotografiasId));
                        where.Add(new Where("Activo", "1"));

                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "Id",
                                "IdCertificado_lesionfotografias CertificadoFotografia",
                                "Fotografia",
                                "Descripcion"
                            },
                            from = new Table("certificado_lesionfotografiasdetalle"),
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var data = dt.Generar(query, draw, start, length, search, order, columns);
                        return data;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = string.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        public class ExamenAux
        {
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string Edad { get; set; }
            public string SexoId { get; set; }
            public string InspeccionOcular { get; set; }
            public string IndicacionesMedicas { get; set; }
            public string Observacion { get; set; }
            public string Activo { get; set; }
            public string Habilitado { get; set; }
            public string RiesgoVida { get; set; }
            public string ConsecuenciasLesiones { get; set; }
            public string DiasSanarLesiones { get; set; }
            public string TipoExamen { get; set; }
            public string Fecha { get; set; }
            public string DetalleDetencionTracking { get; set; }
            public Boolean Lesion_visible { get; set; }
            public string Fallecimiento { get; set; }
        }

        public class CertificadoquimicoAux
        {
            public string ServiciomedicoId { get; set; }
            public string ServiciomedicoTrackingId { get; set; }
            public string DetenidoTrackingId { get; set; }
            public string DetalledetencionId { get; set; }
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string Folio { get; set; }
            public string Fecha_toma { get; set; }
            public string Fecha_proceso { get; set; }
            public string EtanolId { get; set; }
            public string Grado { get; set; }
            public string BenzodiapinaId { get; set; }
            public string AnfetaminaId { get; set; }
            public string CannabisId { get; set; }
            public string CocaínaId { get; set; }
            public string ExtasisId { get; set; }
            public string Dimension { get; set; }
            public string Axysm { get; set; }
            public string Architect { get; set; }
            public string VivaE { get; set; }
            public string ResultadoEtanol { get; set; }
            public string EquipoautilizarId { get; set; }
            public string PrintEtanol { get; set; }
            public string PrintBenzodiazepina { get; set; }
            public string PrintAnfetamina { get; set; }
            public string PrintCannabis { get; set; }
            public string PrintCocaina { get; set; }
            public string PrintExtasis { get; set; }
        }

        public class CertificadoLesionaux
        {
            public string ServiciomedicoId { get; set; }
            public string ServiciomedicoTrackingId { get; set; }
            public string DetenidoTrackingId { get; set; }
            public string DetalledetencionId { get; set; }
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string Aptitud { get; set; }
            public string[] Tipo_lesionId { get; set; }
            public string Observaciones_lesion { get; set; }
            public string Lesion_simple_vista { get; set; }
            public string Fallecimiento { get; set; }
            public string Envio_hospital { get; set; }
            public string Peligro_de_vida { get; set; }
            public string Consecuencias_medico_legales { get; set; }
            public string[] Tipo_alergiaId { get; set; }
            public string Observacion_alergia { get; set; }
            public string[] Tipo_tatuajeId { get; set; }
            public string Observacion_tatuje { get; set; }
            public string Observacion_general { get; set; }
            public string[] AntecedentesId { get; set; }
            public string Observaciones_antecedentes { get; set; }
            public string Sinlesion { get; set; }
            public string FotografiasId { get; set; }
            public string Fechavaloracion { get; set; }
        }

        public class CertificadoPsicofisiologicoaux

        {
            public string ServiciomedicoId { get; set; }
            public string ServiciomedicoTrackingId { get; set; }
            public string DetenidoTrackingId { get; set; }
            public string DetalledetencionId{ get; set; }
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string AlientoId { get; set; }
            public string MarchaId { get; set; }
            public string ActitudId { get; set; }
            public string AtencionId { get; set; }
            public string Cavidad_oralId { get; set; }
            public string PupilaId { get; set; }
            public string Reflejo_pupilarId { get; set; }
            public string ConjuntivaId { get; set; }
            public string LenguajeId { get; set; }
            public string RombergId { get; set; }
            public string Ojos_abiertos_dedo_dedoId { get; set; }
            public string Ojos_cerrados_dedo_dedoId { get; set; }
            public string Ojos_abiertos_dedo_narizId { get; set; }
            public string Ojos_cerrados_dedo_narizId { get; set; }
            public string TA { get; set; }
            public string FC { get; set; }
            public string FR { get; set; }
            public string Pulso { get; set; }
            public string No_valorabe { get; set; }
            public string [] OrientacionId { get; set; }
            public string Observacion_orientacion { get; set; }
            public string [] ConclusionId { get; set; }
            public string Sustento_toxicologico { get; set; }
            public string Observacion_extra { get; set; }
            public string FechaValoracion { get; set; }

        }

        public class PruebaAux
        {
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string ExamenMedicoTracking { get; set; }
            public string MucosasId { get; set; }
            public string AlientoId { get; set; }
            public string Examen_neurologicoId { get; set; }
            public string Disartia { get; set; }
            public string ConjuntivasId { get; set; }
            public string MarchaId { get; set; }
            public string PupilasId { get; set; }
            public string CoordinacionId { get; set; }
            public string Reflejos_pupilaresId { get; set; }
            public string TendinososId { get; set; }
            public string RomberqId { get; set; }
            public string ConductaId { get; set; }
            public string LenguajeId { get; set; }
            public string AtencionId { get; set; }
            public string OrientacionId { get; set; }
            public string DiadococinenciaId { get; set; }
            public string DedoId { get; set; }
            public string TalonId { get; set; }
            public string Alcoholimetro { get; set; }
            public string TA { get; set; }
            public string FC { get; set; }
            public string FR { get; set; }
            public string Pulso { get; set; }
        }

        public class AdicionalAux
        {
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string ExamenMedicoId { get; set; }
            public string Lugar { get; set; }
            public string Observacion { get; set; }
            public string Activo { get; set; }
            public string Habilitado { get; set; }
            public string Tipo { get; set; }
            public string Creadopor { get; set; }
            public string Clasificacion { get; set; }
            public string ExamenTrackingId { get; set; }
        }

        public class CertificadoLesionFotografiaDetalleAux
        {
            public string Id { get; set; }
            public string IdCertificadoLesionFotografia { get; set; }
            public string Fotografia { get; set; }
            public string Descripcion { get; set; }
            public string TrackingCertificado { get; set; }
        }
    }
}