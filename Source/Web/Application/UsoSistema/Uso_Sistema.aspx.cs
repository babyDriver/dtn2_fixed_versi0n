﻿using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;
using System.Linq;
using Entity.Util;

namespace Web.Application.UsoSistema
{
    public partial class Uso_Sistema : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
        }
        [WebMethod]
        public static List<Combo> GetSubcontratos()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            List<Combo> lista = new List<Combo>();
            var usuario = ControlUsuario.Obtener(usId);
            var subcontratos = ControlSubcontrato.ObtenerByUsuarioId(usuario.Id).Where(x=> x.Activo);
            foreach( var item in subcontratos)
            {

                lista.Add(new Combo { Id = item.Id.ToString(), Desc = item.Nombre });
            }
            return lista;
        }
        [WebMethod]
        public static object pdf(string Inicio, string Fin,int filtro,int subcontratoid)
        {
            var serializedObject = string.Empty;

            

            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 6 });
                var permitido = true;
                if (permitido)
                {
                    

                    Inicio = Inicio + " 00:00:00";
                    Fin = Fin + " 23:59:59";

                    string[] parametros = new string[2] { Inicio, Fin };

                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                   

                    var listadoreporte = ControlReporteUsosistema.Obtenerporfechas(parametros);
                    //var listasanciones = ControlReporteSancionesJuezCalificador.GetreporteSanciones(parametros);

                    if (listadoreporte.Count == 0)
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados" });
                    }
                    if (subcontratoid != 0)
                    {
                        subcontrato = ControlSubcontrato.ObtenerPorId(subcontratoid);
                        listadoreporte = listadoreporte.Where(x => x.ContratoId == subcontrato.Id).ToList();
                    }
                    listadoreporte = listadoreporte.Where(x => x.SubContratos.Contains(subcontrato.Nombre)).ToList();
                    if (filtro==1)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("barandilla")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }

                    }
                    else if(filtro==2)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("certificado")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if(filtro==3)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("Biometrico")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if(filtro==4)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("calif") || x.Movimiento.Contains("salida") || x.Movimiento.Contains("Tras")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if (filtro == 5)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("formato")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }

                    else if (filtro == 6)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("llamada")|| x.Movimiento.Contains("evento")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if (filtro == 7)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("perte") ).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if (filtro == 8)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("celda")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if (filtro == 9)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("observa")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if (filtro == 10)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("pago de multa")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if (filtro == 11)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("social")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    else if (filtro == 12)
                    {
                        listadoreporte = listadoreporte.Where(x => x.Movimiento.Contains("amparo")).ToList();
                        if (listadoreporte.Count == 0)
                        {
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados para este modulo" });
                        }
                    }
                    //var obj = ControlPDFBarandilla.GeneraReporteIngresos(listadoreporteingresos.LastOrDefault().DetalledetencionId,listadoreporteingresos,parametros);
                    //serializedObject = JsonConvert.SerializeObject(obj[1]);
                    //return serializedObject;

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFBarandilla.GenerarReporteUsoSistema(listadoreporte, parametros);
                    return new { exitoso = true, mensaje = "", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string inicial, string final, int filtro, int subcontratoid)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 6 });
            var permitido = true;
            if (permitido)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        List<Join> join = new List<Join>();
                        //where.Add(new Where("ES.Activo", "1"));
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        if (subcontratoid!=0)
                        {
                            subcontrato = ControlSubcontrato.ObtenerPorId(subcontratoid);
                            where.Add(new Where("A.ContratoId", subcontrato.Id.ToString()));
                        }
                        where.Add(new Where("A.SubContratos", Where.LIKE, "%"+subcontrato.Nombre+"%"));

                        if (!string.IsNullOrEmpty(inicial) && !string.IsNullOrEmpty(final))
                        {
                            var fechaFinal = Convert.ToDateTime(final);
                            fechaFinal = fechaFinal.AddDays(1);

                            where.Add(new Where("A.Fecha", Where.GREATEREQUAL, inicial));
                            where.Add(new Where("A.Fecha", Where.LESSEQUAL, fechaFinal.ToString("yyyy-MM-dd")));
                        }
                        if(filtro==1)
                        {
                            where.Add(new Where("A.Movimiento",Where.LIKE, "%barandilla%"));
                        }
                        else if(filtro==2)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%certificado%"));
                        }
                        else if (filtro == 3)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%Biometrico%"));
                        }
                        else if (filtro == 4)
                        {
                            join.Add(new Join(new Table($"(select Id from historial D where D.movimiento like '%calif%' or D.movimiento like '%salida%' or D.Movimiento like '%tras%')", "B"), "A.IdHistorial  = B.Id","INNER"));
                        }
                        else if (filtro == 5)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%formato%"));
                        }
                        else if (filtro == 6)
                        {
                            join.Add(new Join(new Table($"(select C.Id from historial C where C.Movimiento like '%evento%' or C.movimiento like '%llamada%')", "B"), "A.IdHistorial  = B.Id", "INNER"));
                        }
                        else if (filtro == 7)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%perte%"));
                        }
                        else if (filtro == 8)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%celda%"));
                        }
                        else if (filtro == 9)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%observa%"));
                        }
                        else if (filtro == 10)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%pago de multa%"));
                        }
                        else if (filtro == 11)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%social%"));
                        }
                        else if (filtro == 12)
                        {
                            where.Add(new Where("A.Movimiento", Where.LIKE, "%amparo%"));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {

                                "A.Usuario",
                                "A.Nombre",
                                "date_format(A.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "A.ApellidoPaterno",
                                "A.ApellidoMaterno",
                                "A.rol",
                                "A.SubContratos",
                                "A.Movimiento",
                                "A.NombreCompleto",

                            },
                            from = new Table("VhistoricoUsuarios", "A"),
                            joins = join,
                            wheres = where,
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }
    }
}