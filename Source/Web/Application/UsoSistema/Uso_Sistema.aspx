﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="Uso_Sistema.aspx.cs" Inherits="Web.Application.UsoSistema.Uso_Sistema" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Uso del sistema</li>
    

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #content {
            height: 600px;
        }
        div.scroll {
            overflow: auto;
            overflow-x: hidden;
        }
    </style>
    <div class="scroll">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                  <i class="fa fa-file-pdf-o "></i>
                  Uso del sistema
                </h1>
            </div>
        </div>
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <section id="widget-grid-sanciones" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-sanciones-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                        <header>
                            <%-- <span class="widget-icon"></span> --%> <i style="float:left; margin-top:10px; margin-left:5px;" class="fa fa-file-pdf-o"></i>
                            <h2>Uso del sistema</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div id="smart-form-register-entry" class="smart-form">
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-2">
                                                <label class="label">Fecha de inicio <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <label class='input-group date'>
                                                        <input type="text" name="inicio" id="inicio" placeholder="Fecha de inicio" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha de inicio.</b>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </label>
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Fecha final <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <label class='input-group date' >
                                                        <input type="text" name="final" id="final" placeholder="Fecha final" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha final.</b>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </label>
                                                </label>
                                            </section>
                                             <section class="col col-3">
                                                 <label class="label">Módulo</label>
                                                 <select class="select2" id="filtro" style="padding:5px;margin-top:22px;" >
                                                     <option value="0">Todos</option>
                                                     <option value="5">Formato de referencia</option>
                                                     <option value="6">Llamadas y eventos /eventos /llamadas</option>
                                                     <option value="1">Registro en barandilla</option>
                                                     <option value="2">Examen médico</option>
                                                     <option value="7">Control de pertenencias/evidencias</option>
                                                     <option value="4">Juez calificador</option>
                                                     <option value="12">Detenidos en amparo</option>
                                                     <option value="8">Control de celdas</option>
                                                     <option value="9">Defensor público</option>
                                                     <option value="10">Caja</option>
                                                     <option value="11">Trabajo social</option>
                                                 </select>
                                             </section>
                                            <section class="col col-3">
                                                <label class="label">Subcontrato</label>
                                                <select class="select2" id="subcontrato" style="padding:5px;margin-top:22px;">

                                                </select>
                                            </section>
                                            <section class="col col-2">
                                                 <a class="btn btn-success btn-md reportePdf" style="margin-top:22px;padding:5px;height:35px; margin-right:15px;"><i class="glyphicon glyphicon-file"></i> Generar PDF</a>
                                            </section>
                                        </div>
                                    </fieldset>
                                </div>
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Usuario</th>
                                            <th>Nombre </th>
                                            <th >Apellido paterno</th>
                                            <th>Apellido materno</th>
                                            <th>Perfil</th>
                                            <th>Subcontratos</th>
                                            <th>Registros</th>
                                        </tr>
                                    </thead>
                                </table>
                                

                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <input type="hidden" id="Hidden1" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            loadContracts("0");
            function loadContracts(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "Uso_Sistema.aspx/GetSubcontratos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#subcontrato');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de contratos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            $('#ctl00_contenido_inicio').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });
            $('#ctl00_contenido_final').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });

            $("body").on("click", ".reportePdf", function () {
                $("#ctl00_contenido_lblMessage").html("");

                var valido = validarFechas();
                if (valido) {
                    pdf();
                }
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }
            fechasiniciales();
            function fechasiniciales() {
                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth() + 1;
                var yyyy = hoy.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (mm < 10) {
                    mm = '0' + mm;
                }
                var fecha1 = dd + "/" + mm + "/" + yyyy;
                

                $("#ctl00_contenido_inicio").val(fecha1);
                $("#ctl00_contenido_final").val(fecha1);
            }

            function validarFechas() {
                var esvalido = true;
                if ($("#ctl00_contenido_inicio").val() == "" || $("#ctl00_contenido_inicio").val() == null) {
                    ShowError("Fecha inicial", "Por favor ingrese el valor en la fecha inicial.");
                    $('#ctl00_contenido_inicio').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_inicio').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_inicio').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_inicio').addClass('valid');
                }
                if ($("#ctl00_contenido_final").val() == "" || $("#ctl00_contenido_final").val() == null) {
                    ShowError("Fecha final", "Por favor ingrese el valor en la fecha final.");
                    $('#ctl00_contenido_final').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_final').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_final').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_final').addClass('valid');
                }
                var inicio = $("#ctl00_contenido_inicio").val() == null ? "" : $("#ctl00_contenido_inicio").val();
                var fin = $("#ctl00_contenido_final").val() == null ? "" : $("#ctl00_contenido_final").val();
                if (inicio != "") {
                    inicio = inicio.split("/");
                    inicio = inicio[2] + "/" + inicio[1] + "/" + inicio[0];

                }
                if (fin != "") {
                    fin = fin.split("/");
                    fin = fin[2] + "/" + fin[1] + "/" + fin[0];
                }
                if (inicio != "" & fin != "") {
                    if (fin < inicio) {
                        ShowError("Fecha final", "La fecha final deberá ser mayor que la fecha inicial.");
                        $('#ctl00_contenido_final').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_final').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_final').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_final').addClass('valid');
                    }
                }
                return esvalido;
            }

            function pdf() {
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/UsoSistema/Uso_Sistema.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        Inicio: $("#ctl00_contenido_inicio").val(),
                        Fin: $("#ctl00_contenido_final").val(),
                        filtro: $("#filtro").val(),
                        subcontratoid: $('#subcontrato').val()||"0"
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = data.d;

                        if (resultado.exitoso) {
                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + data.d.mensaje + "</div>");
                            ShowError("¡Error!", data.d.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            $("#ctl00_contenido_inicio").change(function () {

                var valido = validarFechas();
                if (valido) {
                    window.table.api().ajax.reload();
                }

            });
            $('#subcontrato').change(function () {

                var valido = validarFechas();
                if (valido) {
                    window.table.api().ajax.reload();
                }

            });

            $("#filtro").change(function () {

                var valido = validarFechas();
                if (valido) {
                    window.table.api().ajax.reload();
                }

            });

            $("#ctl00_contenido_final").change(function () {

                var valido = validarFechas();
                if (valido) {
                    window.table.api().ajax.reload();
                }

            });

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "order": [[6, "desc"], [7, "desc"]],
                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"] %>Application/UsoSistema/Uso_Sistema.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var inicio = $("#ctl00_contenido_inicio").val() == null ? "" : $("#ctl00_contenido_inicio").val();
                        var fin = $("#ctl00_contenido_final").val() == null ? "" : $("#ctl00_contenido_final").val();
                        if (inicio != "") {
                            inicio = inicio.split("/");
                            inicio = inicio[2] + "/" + inicio[1] + "/" + inicio[0];

                        }
                        if (fin != "") {
                            fin = fin.split("/");
                            fin = fin[2] + "/" + fin[1] + "/" + fin[0];
                        }
                        var valor = $("#filtro").val();
                        var sub = $('#subcontrato').val()||"0";

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.inicial = inicio;
                        parametrosServerSide.final = fin;
                        parametrosServerSide.filtro = valor;
                        parametrosServerSide.subcontratoid = sub;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },
                    {
                        name: "Usuario",
                        data: "Usuario"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "ApellidoPaterno",
                        data: "ApellidoPaterno"
                    },
                    {
                        name: "ApellidoMaterno",
                        data: "ApellidoMaterno"
                    },
                    {
                        name: "rol",
                        data: "rol"
                    },
                    {
                        name: "SubContratos",
                        data: "SubContratos"
                    },
                    {
                        name: "Movimiento",
                        data: "Movimiento"
                    }
                ],
                
            });



            function getalerta() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "ReporteIngresos.aspx/GetAlertaWeb",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.d != null) {
                            data = data.d;

                            var alerta1 = data.Denominacion;
                            var alerta2 = data.AlertaWerb;

                            //$("#Denominacin").val(data.Denominacion);

                            //$("#linkEvento").html('<i class="glyphicon glyphicon-folder-open"></i>&nbsp; ' + alerta1 + ' / evento');
                            //$("#alerta1").html('Fecha ' + alerta2);
                            $("#alerta2").html('Folio ' + alerta2);
                            $("#alerta3").html('Motivo ' + alerta2);
                            //$("#alerta2").html('<i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>' + alerta1);
                            //$("#alerta3").html(' <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de ' + alerta2);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de alerta web. Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
            }

        });
    </script>
</asp:Content>
