﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static IronPython.Modules._ast;
using System.Web.UI;
using Entity.Util;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using static Web.Application.Registry.Defeat;
using System.Net.Http;
using System.Text;
using Entity;
using static IronPython.Modules.PythonDateTime;
using System.Web.UI.WebControls.WebParts;
using System.Drawing;
using DT;

namespace Web.Application.Registry
{
    public class RandomPetition : Provider
    {
        //calpis || miso
        private List<Combo> seriable = new List<Combo>();
        const string _url = "http://c5-amb-postproduccion.promad.com.mx:9197" + "/api";
        int nDet, nEvt = new int(); string reason = ""; string origin = ""; 
        int[] result = { };
        private JArray paramsArray = new JArray();
        
        public List<Combo> EventCombo(string id/*declare combo*/)
        {
            try
            {
                _nameSpace = _url + "/despachador-eventos/obtenerEventos";
                _json = new JavaScriptSerializer().Serialize(new // adjoining
                {
                    uuid = 5,
                    idRolUsuario = 33,
                    idUsuario = 6
                });
                var jObj = factory(_nameSpace, _json);
                JArray paramsArray = (JArray)jObj["eventos"];

                foreach (JToken i in paramsArray)
                {
                    Entity.EventoReciente eventoReciente = ControlEventoReciente.ObtenerPorId(Convert.ToInt32(id));
                    DateTime fecha = DateTime.Now;
                    DateRange range = new DateRange(fecha, eventoReciente.Hora + 1);
                    int ii = 0;
                    ii = (int)i["idInstitucion"];
                    //Entity.WSAInstitucion ins_wsa = ControlWSAInstitucion.ObtenerPorId(ii);
                    //if (range.Includes(isoDateFormat))
                        seriable.Add(new Combo { Desc = (string)i["folio"], Id = (string)i["idEvento"], AdicionalUno = (string)i["idInstAsignada"] });

                }
                
            }
            catch { }
            return seriable;
        }

        public List<Combo> DtnCombo(string id/*declare combo*/)
        {
            if (id != "0")
            {
                try
                {
                    // detained
                    _nameSpace = _url + "/personainvolucrada/getRegistros";
                    _json = new JavaScriptSerializer().Serialize(new // adjoining
                    {
                        uuid = 5,
                        idEvento = id
                    });
                    var jObj = factory(_nameSpace, _json);
                    JArray paramsArray = new JArray();
                    paramsArray = (JArray)jObj["items"];

                    foreach (JToken i in paramsArray)
                    {
                        if ((string)i["nombre_tipo_persona"] == "DETENIDO")
                            seriable.Add(new Combo { Desc = (string)i["nombre"] + " " + (string)i["apellido_paterno"] + " " + (string)i["apellido_materno"], Id = id, AdicionalUno = (string)i["id_persona_involucrada"] });
                    }
                }
                catch { }
            }
            return seriable;
        }

        public string saveNonReal(string evt, int institute, int dtnValue)
        {
            string feedback = "";
            _nameSpace = _url + "/informacionincidente/obtenerInformacionIncidente";
            try
            {
                for (int i = 208000; i < 208500; i++)
                {
                    using (var client = new HttpClient())
                    {
                        _json = new JavaScriptSerializer().Serialize(new
                        {
                            uuid = 5,
                            idEvento = evt,
                            idInstitucion = institute
                        });
                        var response = client.PostAsync(new Uri(_nameSpace), new StringContent(_json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult(); ;

                        if (response.IsSuccessStatusCode)
                        {
                            feedback = i.ToString();
                            break;
                        }
                    }
                } 
            }
            catch { }
            return feedback;
        }

        public string saveObject(string evt, int ivalue, int dvalue)
        {
            string feedback = "";
            string key = "";
            int[] short_id = { };
            //string[] decorator = new string[2];
            RandomCatalogue decorator = new RandomCatalogue();
            RandomObjects instance = new RandomObjects();
            instance.evt = Convert.ToInt32(evt);
            instance.dtn = dvalue;
            //c1nd3r3l4
            _nameSpace = _url + "/informacionincidente/obtenerInformacionIncidente";
            _json = new JavaScriptSerializer().Serialize(new
            {
                uuid = 5,
                idEvento = evt,
                idInstitucion = ivalue
            });
            
            try
            {
                var jObj = factory(_nameSpace, _json);
                JArray paramsArray = new JArray();
                JArray paramsOther = new JArray();
                Entity.WSAEvt wsa = new Entity.WSAEvt();
                wsa.Log = false;
                wsa.IdVehiculo = 0;
                var evnt = (JObject)jObj["datoEvento"];
                paramsArray.Add(evnt);
                paramsOther = (JArray)jObj["descripcion"];
                RandomAddress address = new RandomAddress();
                //edata
                foreach (JToken i in paramsArray)
                {
                    wsa.EventoId = Convert.ToInt32(evt);
                    key = (string)i["folio"];
                    wsa.Folio = key;
                    wsa.Fecha = Convert.ToDateTime((string)i["fecha_inicio"]); ;
                    wsa.IdOrigen = (int)i["id_origen"]; 
                    wsa.IdMotivo = (int)i["id_motivo"];
                    origin = (string)i["nombre_origen"];
                    reason = (string)i["nombre_motivo"];
                    //Append((int)i["id_colonia"]).ToArray()
                    address.sIdentifier = (int)i["id_colonia"];
                    address.suburb = (string)i["nombre_colonia"];
                    address.tIdentifier = (int)i["id_municipio"];
                    address.town = (string)i["nombre_municipio"];
                    address.stIdentifier = (int)i["id_estado"];
                    address.state = (string)i["nombre_estado"];
                    wsa.Latitud = (string)i["latitud"];
                    address.lat = (string)i["latitud"];
                    wsa.Longitud = (string)i["longitud"];
                    address.lon = (string)i["longitud"];                    
                    wsa.CodigoPostal = (string)i["codigo_postal"];
                    address.postalCode = (string)i["codigo_postal"];
                    address.location = (string)i["calle"] + " " + (string)i["entre_calle_2"] + " " + (string)i["numero"];
                    wsa.Calle = (string)i["calle"];
                    address.street = (string)i["calle"];
                    // itinerary operator        
                    wsa.CalleRef = ((string)i["entre_calle_1"] is null && (string)i["entre_calle_2"] != null) ? (string)i["entre_calle_2"] : (string)i["entre_calle_1"] ;
                    address.refStreet = wsa.CalleRef;
                    wsa.No = (string)i["numero"];
                    address.no = (string)i["numero"];
                    int[] result = address.saveAddress();
                    wsa.IdColonia = result[0];
                    //re dø function
                    wsa.IdDomicilio = result[1];
                    wsa.NoDetenidos = nDet;
                            
                }
                
                foreach (JToken i in paramsOther)
                {
                    string attemp = (string)i["descripcion"];
                    if (attemp != "")
                        wsa.Descripcion = attemp;
                }
                decorator.data = reason; decorator.save("r", wsa.IdMotivo);
                paramsOther = (JArray)jObj["instituciones"];

                foreach (JToken i in paramsOther)
                {
                    short_id = new int[] { (int)i["id_institucion"], (int)i["id_recurso"] };
                    //Array.Resize(ref decorator, 3);
                    decorator.data = (string)i["nombre_institucion"];
                    decorator.data_support = (string)i["nombre"];
                    decorator.save("i", short_id[0]);
                    decorator.data = (string)i["nombre_recurso"];  
                    decorator.data_desc = (string)i["nombre_tipo_recurso"];
                    decorator.data_id = short_id[0].ToString();
                    decorator.save("u", short_id[1]);
                    wsa.IdInstitucion = short_id[0]; //wsa.RecursoId = oopart[1];
                }
                int id = ControlWSAEvt.Guardar(wsa);
                instance.location = address.location; instance.suburb = address.sIdentifier;
                nEvt = instance.MakeEvent(wsa, (id > 0) ? id : 0);
                int evalue = Convert.ToInt32(evt);
                //saveDtn
                var dtn = new RandomDtn(evalue, dvalue);
                feedback = dtn.factory();
                bool rule = Convert.ToBoolean(short_id.Length);
                instance.MakeDetained(nEvt, dtn.name, dtn.first, dtn.last, dtn.nick, dtn.date, dtn.age, 0, dtn.sex, key, rule ? short_id[0] : 0, reason);

            }
            catch (Exception ex)
            {
                feedback = ex.ToString();
            }
            return feedback;
        }

        public DataTable defaultDT(int start, int length)
        {
            DT.DataTable defaultTable = new DT.DataTable();
            try
            {
                List<object> dataSet = new List<object>();
                var alertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
                int r = 0, s = 0, l = 0;
                using (var client = new HttpClient())  // instance HttpClient
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        uuid = 5,
                        idRolUsuario = 33,
                        idUsuario = 6
                    });

                    var _url = "http://c5-amb-postproduccion.promad.com.mx:9197/api/despachador-eventos/obtenerEventos";
                    var response = client.PostAsync(new Uri(_url), new StringContent(json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult(); ;
                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                        var jObj = JObject.Parse(responseContent);
                        JArray paramsArray = (JArray)jObj["eventos"];
                        foreach (JToken i in paramsArray)
                        {
                            r++; s++;
                            if (s > start)
                            {
                                if (l < length)
                                {
                                    Object obj = new
                                    {
                                        Institucion = (string)i["institucion"],
                                        Descripcion = (string)i["motivo"],
                                        //4lter
                                        Coordenadas = (string)i["latitud"] + "/" + (string)i["longitud"],
                                        CodigoPostal = (string)i["zps"],
                                        Asentamiento = (string)i["sector"],
                                        Municipio = (string)i["nombreMunicipio"],
                                        Estado = "CDMX",
                                        Pais = "Mexico",
                                        Folio = (string)i["folio"],
                                        HoraYFecha = (string)i["fechaInicio"],
                                        //unte3st
                                        Identifiers = (int)i["idInstitucion"] + '/' + (int)i["idEvento"]
                                    };
                                    dataSet.Add(obj);
                                    l++;
                                }
                            }
                        }
                    }
                }
                //return JsonConvert.SerializeObject(dataSet);
                defaultTable.data = dataSet;
                defaultTable.recordsFiltered = r;
                defaultTable.recordsTotal = r;
            }
            catch { }
            return defaultTable;
        }
    }
}