﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;

namespace Web.Application.Registry
{
    public partial class huella : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor != "")
                this.tracking.Value = valor;
            else
                Response.Redirect("entrylist.aspx");

            if (Request.QueryString["editable"] == "1")
            {
                editable.Value = "1";
            }
            else
            {
                editable.Value = "0";
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                        //Acceso a Informacion_Personal
                        parametros[1] = "Registro en barandilla";
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);

                        //Acceso a Adicciones
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Adicciones.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Antropometria
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Antropometria.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Señas_Particulares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Señas_Particulares.Value = permisos.Consultar.ToString().ToLower();
                        }



                        //Acceso a Familiares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Familiares.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Expediente_Medico
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Expediente_Medico.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Ingreso
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Ingreso.Value = permisos.Consultar.ToString().ToLower();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static string saveObservaciones(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    var detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[0]));

                    var observacionBiometrico = ControlObservacionesBiometricos.GetByDetenidId(detenido.Id);
                    string msj = "";
                    if (observacionBiometrico == null)
                    {
                        Entity.ObservacionesBiometricos observaciones = new Entity.ObservacionesBiometricos();
                        observaciones.DetenidoId = detenido.Id;
                        observaciones.Observaciones = datos[1];
                        observaciones.Id = ControlObservacionesBiometricos.Guardar(observaciones);
                        msj = "La información se registró correctamente.";
                    }
                    else
                    {
                        observacionBiometrico.Observaciones = datos[1];
                        ControlObservacionesBiometricos.Actualizar(observacionBiometrico);
                        msj = "La información se actualizó correctamente";
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje=msj });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }

            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
       
           
        }


        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    Entity.Huella huella = new Entity.Huella();
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[1]));
                    string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido" });

                    huella.Mano = datos[2].ToString();
                    huella.Dedo = datos[3].ToString();
                    huella.Fotografia = datos[4].ToString();

                    var id = datos[0].ToString();

                    if (id == "")
                        id = "0";

                    List<Entity.Huella> huellaRepetida = ControlHuella.ObtenerPorDetenidoId(interno.Id);

                    if (huellaRepetida != null)
                    {
                        foreach (var h in huellaRepetida)
                        {
                            if (h.Mano == huella.Mano && h.Dedo == huella.Dedo && h.Id != Convert.ToInt32(id))
                                throw new Exception(string.Concat("Al parecer ya está registrada esta huella"));
                        }
                    }

                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        if(huella.Fotografia == "")
                        {
                            huella.Fotografia = datos[5].ToString();
                        }
                        mode = "actualizó";
                        huella.DetenidoId = interno.Id;
                        huella.Activo = true;
                        huella.Habilitado = 1;
                        huella.Id = Convert.ToInt32(datos[0]);
                        ControlHuella.Actualizar(huella);
                    }
                    else
                    {
                        mode = "registró";
                        huella.DetenidoId = interno.Id;
                        huella.Activo = true;
                        huella.Id = ControlHuella.Guardar(huella);
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = huella.Id });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string delete(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
                {
                    Entity.Huella item = ControlHuella.ObtenerPorId(Convert.ToInt32(trackingid));
                    string[] internoestatus = { item.DetenidoId.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido" });

                    item.Habilitado = item.Habilitado==1 ? 0 : 1;
                    ControlHuella.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = item.Habilitado == 1 ? "Registro habilitado con éxito." : "Registro deshabilitado con éxito." });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getInternoId(string datos)
        {
            try
            {
                var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos));

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Ok.", Id = interno.Id });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static Object GeneraPdF(string trackingId)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                {
                    var ficha = ControlFichaBiometrico.ObtenerFichaBiometrico(Convert.ToInt32(trackingId));
                    var biometricos = ControlBiometricos.GetByDetenidoId(ficha.DetenidoId);
                    if(biometricos.Count == 0) return new { exitoso = false, mensaje = "El detenido no cuenta con biométricos", Id = "", TrackingId = "" };
                    var detenido=ControlDetenido.ObtenerPorId(Convert.ToInt32(trackingId));
                    object[] dataArchivo = null;

                    dataArchivo = ControlPDFBiometricos.generarReporte(ficha);
                    Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                    reportesLog.ProcesoId = ficha.DetenidoId;
                    reportesLog.ProcesoTrackingId = ficha.DetenidoId.ToString();
                    reportesLog.Modulo = "Registro en barandilla";
                    reportesLog.Reporte = "Ficha de biométricos";
                    reportesLog.Fechayhora = DateTime.Now;
                    reportesLog.EstatusId = 2;
                    reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    ControlReportesLog.Guardar(reportesLog);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "" };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }

            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }

        }

        [WebMethod]
        public static DataTable getlist(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking,string opcion)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                    {
                        if (!emptytable)
                        {


                            List<Where> where = new List<Where>();

                            where.Add(new Where("P.Activo", "1"));
                            where.Add(new Where("I.TrackingId", tracking));
                            if (opcion != null)
                            {
                                if (opcion != "0")
                                {
                                    where.Add(new Where("P.TipobiometricoId", opcion));
                                }
                            }

                            Query query = new Query
                            {
                                select = new List<string>{
                                "P.Id",
                                "P.DetenidoId",
                                "P.Nombre Mano",
                                "P.Descripcion Dedo",
                                "P.Rutaimagen RutaImagen",
                                "P.Activo",
                                "P.Habilitado",
                            },
                                from = new Table("Biometrico", "P"),
                                joins = new List<Join> {
                                    new Join(new DT.Table("detenido", "I"), "P.DetenidoId = I.Id", "LEFT")
                            },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
                finally
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            return edad;
        }


        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                {
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                    if (detalleDetencion == null) detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();

                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                    Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else if (general.Edaddetenido != 0)
                            edad = general.Edaddetenido;
                        else
                        {
                            var infDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infDetencion.IdDetenido_Evento);

                            if (detenidoEvento != null)
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }
                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }
                    var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                    Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                    var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                    Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                    object obj = null;
                    if (interno != null)
                    {
                        if (string.IsNullOrEmpty(interno.RutaImagen))
                        {
                            interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                        }
                    }

                    var observacionesBiometricos = ControlObservacionesBiometricos.GetByDetenidId(interno.Id);

                    string observacionesBio = "";
                    if (observacionesBiometricos != null)
                    {
                        observacionesBio = observacionesBiometricos.Observaciones;
                    }

                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = detalleDetencion.Expediente,
                        Centro = institucion != null ? institucion.Nombre : "Institución no registrada",
                        Fecha = detalleDetencion.Fecha != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                        RutaImagen = interno.RutaImagen,
                        Edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                        ContratoId = detalleDetencion.ContratoId.ToString(),
                        Observacionebiometrico= observacionesBio,
                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion estatusinterno = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(estatusinterno.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                
                int edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = domicilio.Localidad + ", " + domicilio.Calle + " " + domicilio.Numero;

                }

                object obj = null;

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                    Situacion = "PENDIENTE",
                    Domicilio = strDomicilio,

                    RutaImagen = interno.RutaImagen,
                    Registro = estatusinterno.Fecha,
                    Salida = "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}