﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using Entity;

namespace Web.Application.Registry
{
    public partial class eventsform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            if (idContrato == 0) Response.Redirect("../dashboard.aspx");
            // validatelogin();
            // feedback
            // string valor = Convert.ToString(Request.QueryString["tracking"]);
            // this.tracking.Value = valor;

        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }

        //null purpos3
        [WebMethod]
        public static List<Combo> getSexogrid()
        {
            var combo = new List<Combo>();
            var c = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));

            foreach (var item in c)
            {
                if (item.Activo && item.Habilitado)
                {
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getMotivogrid()
        {
            var combo = new List<Combo>();
            var c = ControlWSAMotivo.ObtenerTodos();

            foreach (var item in c)
            {
                if (item.Activo )
                {
                    combo.Add(new Combo { Desc = item.NombreMotivoLlamada, Id = item.IdMotivo.ToString() });
                }
            }

            return combo;
        }


        //fill data
        [WebMethod]
        public static string getDataNuevo(string bandera)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
            {
                try
                {
                    List<object> list = new List<object>();
                    // initialize
                    if (Convert.ToBoolean(bandera) == false)
                    {
                        object obj = new
                        {
                            Check = "",
                            Nombre = "",
                            Apellidopaterno = "",
                            Apellidomaterno = "",
                            Sexo = "",
                            Edad = "",
                            Motivo = ""
                        };
                        list.Add(obj);
                    }
                    else
                    {
                        object obj = new
                        {
                            Nombre = "",
                            TipoVictima = "",
                            DanioPerjuicio = ""
                        };
                        list.Add(obj);
                    }
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
                }
                catch (Exception ex)
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
                }
            }
            else
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "error" });
            }
        }


        [WebMethod]
        public static object GetAlertaWeb()
        {
            try
            {
                var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
                var denoma = "Alerta web";
                var alerta = "alerta web";
                if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
                {
                    denoma = datosAlertaWeb.Denominacion;
                    alerta = denoma;
                }
                object data = new
                {

                    Denominacion = denoma,
                    AlertaWerb = alerta
                };

                return data;
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static Object getEventoByTrackingId(string tracking)
        {
            try
            {                
                var data = ControlEvento.ObtenerByTrackingId(new Guid(tracking));
                var colonia = ControlColonia.ObtenerPorId(data.ColoniaId);
                var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                var estado = ControlEstado.Obtener(municipio.EstadoId);
                var pais = ControlCatalogo.Obtener(estado.IdPais, 45);
                string relacionado = string.Empty;

                EventoAux obj = new EventoAux();
                obj.CodigoPostal = colonia.CodigoPostal;
                obj.Descripcion = data.Descripcion;
                obj.Folio = data.Folio;
                obj.HoraYFecha = data.HoraYFecha.ToString("dd/MM/yyyy HH:mm:ss");
                obj.Id = data.Id.ToString();
                obj.ColoniaId = data.ColoniaId.ToString();
                obj.IdEstado = estado.Id.ToString();
                obj.IdMunicipio = municipio.Id.ToString();
                obj.IdPais = pais.Id.ToString();
                obj.IdLlamada = data.LlamadaId.ToString();
                obj.Lugar = data.Lugar;
                obj.TrackingId = data.TrackingId.ToString();
                obj.NumeroDetenidos = data.NumeroDetenidos.ToString();
                obj.Latitud = data.Latitud.ToString();
                obj.Longitud = data.Longitud.ToString();
                obj.MotivoId = data.MotivoId.ToString(); ;

                // Se obtiene el la unidad del evento de alerta web que se registró si es que el evento está ligado a uno.
                if (data.IdEventoWS > 0)
                {
                    var eventoAw = ControlWSAEvento.ObtenerPorId(data.IdEventoWS);

                    if (eventoAw != null)
                    {
                        obj.UnidadIdAW = eventoAw.IdUnidad.ToString();
                        relacionado = "1";
                    }
                    
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", obj, relacionado });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static object getpositiobycontract(string LlamadaId)
        {
            try
            {
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                Entity.Subcontrato subcontrato = new Entity.Subcontrato();

               

                subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);


                postioncontract obj = new postioncontract();
                obj.Latitud = subcontrato.Latitud;
                obj.Longitud = subcontrato.Longitud;

                

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", obj });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static object getCall(string LlamadaId)
        {
            try
            {
                Entity.Llamada llamada = ControlLlamada.ObtenerById(Convert.ToInt32(LlamadaId));
                EventoAux obj = new EventoAux();

                if (llamada != null)
                {
                    var colonia = ControlColonia.ObtenerPorId(llamada.IdColonia);
                    var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                    var estado = ControlEstado.Obtener(municipio.EstadoId);
                    var pais = ControlCatalogo.Obtener(estado.IdPais, 45);

                    obj.CodigoPostal = colonia.CodigoPostal;
                    obj.Descripcion = llamada.Descripcion;
                    obj.ColoniaId = llamada.IdColonia.ToString();
                    obj.IdEstado = estado.Id.ToString();
                    obj.IdMunicipio = municipio.Id.ToString();
                    obj.IdPais = pais.Id.ToString();
                    obj.Lugar = llamada.Lugar;
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", obj });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string getListadoMunicipio()
        {
            try
            {
                List<Entity.Municipio> list = ControlMunicipio.ObtenerTodo();
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", data = list });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string getListadoSectores()
        {
            try
            {
                List<Entity.Catalogo> list = ControlCatalogo.ObtenerHabilitados(101);
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", data = list });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string getListadoLlamadas()
        {
            try
            {
                List<Entity.Llamada> list = ControlLlamada.ObtenerTodos();
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", data = list });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string save(Dictionary<string, string> csObj)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
                {
                    var item = new Entity.Evento();
                    var mode = string.Empty;
                    int eventoId = 0;
                    int folio = 0;
                    Entity.Evento evento;
                    string folioAux = string.Empty;

                    //rep4ir
                    //var eventos = ControlEvento.ObtenerEventos();
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    Entity.Contador contador = ControlEvento.ObtenerFolio(contratoUsuario.IdContrato);
                    folio = contador.total + 1;
                    Entity.Subcontrato subcontrato = new Entity.Subcontrato();

                    //if (eventos.Where(x=> x.ContratoId==contratoUsuario.IdContrato).Count() > 0)
                    //{
                    //    evento = eventos.Last();
                    //    folio = Convert.ToInt32(evento.Folio) + 1;
                    //}
                    //else
                    //{
                    //    folio = 1;
                    //}

                    subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

                    Entity.Institucion institucion = new Entity.Institucion();
                    Entity.Domicilio domicilio = new Entity.Domicilio();

                    if (subcontrato != null)
                        institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

                    if (institucion != null)
                        domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

                    var fecha = Convert.ToDateTime(csObj["fecha"]);
                    fecha = fecha.AddSeconds(-fecha.Second);
                    var fechaNow = DateTime.Now;
                    fecha = fecha.AddSeconds(-60);
                    fechaNow = fechaNow.AddSeconds(-fechaNow.Second);
                    fecha = fecha.AddMinutes(-10);
                    if (fecha > DateTime.Now) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha y hora no puede ser mayor a la fecha actual", fallo = "fecha" });

                    ///
                    item.Descripcion = csObj["descripcion"];
                    //item.Colonia = Convert.ToInt32(csObj["colonia"]);
                    item.HoraYFecha = Convert.ToDateTime(csObj["fecha"]);
                    item.LlamadaId = Convert.ToInt32(csObj["llamada"]);
                    item.Lugar = csObj["lugar"];
                    item.ColoniaId = Convert.ToInt32(csObj["colonia"]);
                    item.NumeroDetenidos = Convert.ToInt32(csObj["numeroDetenidos"]);
                    item.Activo = true;
                    item.Habilitado = true;
                    item.ContratoId = contratoUsuario.IdContrato;
                    item.Tipo = contratoUsuario.Tipo;
                    item.Latitud = csObj["latitud"];
                    item.Longitud = csObj["longitud"];
                    item.MotivoId = Convert.ToInt32(csObj["motivoevento"]);
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    

                    if (csObj["tracking"] != "")
                    {
                        evento = ControlEvento.ObtenerByTrackingId(new Guid(csObj["tracking"]));
                        eventoId = evento.Id;

                        mode = "actualizó";
                        item.Eventoanioregistro = evento.Eventoanioregistro;
                        item.Eventomesregistro = evento.Eventomesregistro;
                        item.TrackingId = new Guid(csObj["tracking"]);
                        //folio en realidad es el id del evento
                        item.Folio = evento.Folio;
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación del evento";
                        historial.CreadoPor = usuario.Id;
                       
                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);
                        ControlEvento.Actualizar(item);
                        folioAux = evento.Folio;
                    }
                    else
                    {
                        item.Eventoanioregistro = DateTime.Now.Year;
                        item.Eventomesregistro = DateTime.Now.Month;
                        item.TrackingId = Guid.NewGuid();
                        //patch
                        item.IdEventoWS = 0;
                        item.Folio = Convert.ToString(folio);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro del evento";
                        historial.CreadoPor = usuario.Id;
                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);
                        item.CreadoPor = usId;
                        eventoId = ControlEvento.Guardar(item);
                        var _evento = ControlEvento.ObtenerById(eventoId);
                        folioAux = _evento.Folio;
                        mode = "registró";
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, EventoId = eventoId.ToString(), tracking = item.TrackingId.ToString(), folio = folioAux });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });

                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getCalls()
        {
            List<Combo> combo = new List<Combo>();

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            var calls = ControlLlamada.ObtenerTodos();

            if (calls.Count > 0)
            {
                foreach (var rol in calls)
                {
                    if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Descripcion.Replace("\"",""), Id = rol.Id.ToString() });
                    }
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCountries()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.pais));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getStates(string idPais)
        {
            List<Combo> combo = new List<Combo>();
            var data = ControlEstado.ObtenerPorPais(Convert.ToInt32(idPais));

            if (data.Count > 0)
            {
                foreach (var rol in data)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCities(string idEstado)
        {
            List<Combo> combo = new List<Combo>();
            var data = ControlMunicipio.ObtenerPorEstado(Convert.ToInt32(idEstado));

            if (data.Count > 0)
            {
                foreach (var rol in data)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getNeighborhoods(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();

            var colonias = ControlColonia.ObtenerPorMunicipioId(Convert.ToInt32(idMunicipio));

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias)
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getSexo()
        {
            List<Combo> combo = new List<Combo>();
            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));

            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getTipo()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.tipo_victima));

            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        public static List<Combo> getTipoGrid() {

            var combo = new List<Combo>();
            List<Entity.Catalogo> c = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.tipo_victima));

            foreach (var rol in c) {
                combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }


        [WebMethod]
        public static string getZipCode(string idColonia)
        {
            try
            {
                var colonia = ControlColonia.ObtenerPorId(Convert.ToInt32(idColonia));

                string codigoPostal = "";

                if (colonia != null)
                {
                    codigoPostal = colonia.CodigoPostal;
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", cp = codigoPostal });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, });
            }
        }

        [WebMethod]
        public static string getFolio()
        {
            try
            {
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                int folio = 0;
                Entity.Contador contador = ControlEvento.ObtenerFolio(contratoUsuario.IdContrato);
                folio = contador.total + 1;
                //var eventos = ControlEvento.ObtenerEventos() && eventos.Where(x=> x.ContratoId==contratoUsuario.IdContrato).Count() + 1;                

                return JsonConvert.SerializeObject(new { exitoso = true, folio });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = true, folio="1", });
            }
        }

        [WebMethod]
        public static List<Combo> getUnidades()
        {
            List<Combo> combo = new List<Combo>();
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
            List<Entity.Catalogo> unidades = ControlCatalogo.ObtenerTodo(82);
            //List<Entity.Catalogo> unidades = ControlCatalogo.ObtenerTodo(82);

            if (unidades.Count > 0)
            {
                foreach (var rol in unidades)
                {
                    //if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    if (rol.ContratoId == contratoUsuario.IdContrato && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
                    }
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getResponsables()
        {
            List<Combo> combo = new List<Combo>();

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            List<Entity.Catalogo> responsables = ControlCatalogo.ObtenerTodo(83);

            if (responsables.Count > 0)
            {
                foreach (var rol in responsables)
                {
                    if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Descripcion + "-" + rol.Nombre, Id = rol.Id.ToString() });
                    }
                }
            }

            return combo;
        }

        [WebMethod]
        public static string saveUnidad(Dictionary<string, string> csObj)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
                {
                    var item = new Entity.EventoUnidadResponsable();
                    var evento = new Entity.Evento();
                    var mode = string.Empty;

                    item.EventoId = Convert.ToInt32(csObj["EventoId"]);
                    item.UnidadId = Convert.ToInt32(csObj["unidad"]);
                    item.ResponsableId = Convert.ToInt32(csObj["responsable"]);

                    var unidadresponsable = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { item.UnidadId, item.EventoId });

                    if (unidadresponsable != null)
                    {
                        if (unidadresponsable.Id != 0)
                            return JsonConvert.SerializeObject(new { exitoso = false, alerta = true, mensaje = "La unidad ya se encuentra registrada en el evento" });
                    }

                    if (csObj["EventoId"] != "")
                    {
                        ControlEventoUnidadResponsable.Guardar(item);
                        mode = "registró";

                        evento = ControlEvento.ObtenerById(item.EventoId);
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, tracking = evento.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveDetenido(Dictionary<string, string> csObj, List<DetenidoEventoData> list)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
                {
                    var eventodet = ControlEvento.ObtenerById(Convert.ToInt32(csObj["EventoId"]));
                    string detenidosinguardar = "";
                    var membershipUser2 = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    List<Entity.DetenidoEvento> ListDetenidoaguardar = new List<Entity.DetenidoEvento>();
                    int i = 0;
                    foreach (var detenidoEventoData in list)
                    {
                        Entity.DetenidoEvento Detevento = new Entity.DetenidoEvento();
                        Detevento.Activo = true;
                        Detevento.CreadoPor= Convert.ToInt32(membershipUser2.ProviderUserKey);
                        Detevento.Edad = Convert.ToInt32(detenidoEventoData.Edad);
                        Detevento.EventoId = Convert.ToInt32(detenidoEventoData.EventoId);
                        Detevento.Habilitado = true;
                        Detevento.Nombre = detenidoEventoData.Nombre;
                        Detevento.Paterno = detenidoEventoData.Paterno;
                        Detevento.Materno = detenidoEventoData.Materno;
                        Detevento.SexoId =Convert.ToInt32( detenidoEventoData.SexoId);
                        Detevento.MotivoId = Convert.ToInt32(detenidoEventoData.Motivo);

                        var detenidos2 = ControlDetenidoEvento.ObtenerPorEventoId(Convert.ToInt32(csObj["EventoId"]));
                        if (  detenidos2.Count()+i>= eventodet.NumeroDetenidos)
                        {
                            detenidosinguardar += detenidoEventoData.Nombre+" "+ detenidoEventoData.Paterno+" " +detenidoEventoData.Materno + ",";


                        }
                        else
                        {
                            ListDetenidoaguardar.Add(Detevento);

                        }
                        i++;

                    }
                    Boolean alertadet = false;

                    if (detenidosinguardar != "")
                    {
                        alertadet = true;
                        detenidosinguardar = " Se llegó al límite de detenidos los siguientes detenidos no se guardaron:  " + detenidosinguardar;
                        detenidosinguardar = detenidosinguardar.Substring(0,detenidosinguardar.Length - 1);
                        detenidosinguardar = detenidosinguardar + ".";
                    }

                    if (ListDetenidoaguardar.Count()==0)
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "Se ha llegado al límite de detenidos en el evento." });

                    }
                    else
                    {
                        foreach (var item in ListDetenidoaguardar)
                        {
                            item.TrackingId = Guid.NewGuid();
                            ControlDetenidoEvento.Guardar(item);

                        }

                    }
                    




                    

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "registro",Alertadetenido=alertadet,Mensajealerta=detenidosinguardar });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        //[WebMethod]
        //public static string saveVictima(Dictionary<string, string> csObj, List<VictimaEventoData> list)
        //{
        //    try
        //    {
        //        if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
        //        {
        //            string victimaunsafe = "";
        //            var membershipUser2 = Membership.GetUser(HttpContext.Current.User.Identity.Name);

        //            List<Entity.DetenidoEvento> ListDetenidoaguardar = new List<Entity.DetenidoEvento>();
        //            int i = 0;
        //            foreach (var detenidoEventoData in list)
        //            {
        //                Entity.DetenidoEvento Detevento = new Entity.DetenidoEvento();
        //                Detevento.Activo = true;
        //                Detevento.CreadoPor= Convert.ToInt32(membershipUser2.ProviderUserKey);
        //                Detevento.Edad = Convert.ToInt32(detenidoEventoData.Edad);
        //                Detevento.EventoId = Convert.ToInt32(detenidoEventoData.EventoId);
        //                Detevento.Habilitado = true;
        //                Detevento.Nombre = detenidoEventoData.Nombre;
        //                Detevento.Paterno = detenidoEventoData.Paterno;
        //                Detevento.Materno = detenidoEventoData.Materno;
        //                Detevento.SexoId =Convert.ToInt32( detenidoEventoData.SexoId);
        //                Detevento.MotivoId = Convert.ToInt32(detenidoEventoData.Motivo);

        //                var detenidos2 = ControlDetenidoEvento.ObtenerPorEventoId(Convert.ToInt32(csObj["EventoId"]));
        //                // condition
        //                victimaunsafe += detenidoEventoData.Nombre+" "+ detenidoEventoData.Paterno+" " +detenidoEventoData.Materno + ",";

        //                else
        //                {
        //                    ListDetenidoaguardar.Add(Detevento);

        //                }
        //                i++;

        //            }
        //            Boolean alertadet = false;

        //            if (victimaunsafe != "")
        //            {
        //                alertadet = true;
        //                victimaunsafe = " Se llegó al límite de detenidos los siguientes detenidos no se guardaron:  " + detenidosinguardar;
        //                victimaunsafe = detenidosinguardar.Substring(0,detenidosinguardar.Length - 1);
        //                victimaunsafe = detenidosinguardar + ".";
        //            }

        //            if (ListDetenidoaguardar.Count()==0)
        //            {
        //                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "Se ha llegado al límite de detenidos en el evento." });

        //            }
        //            else
        //            {
        //                foreach (var item in ListDetenidoaguardar)
        //                {
        //                    item.TrackingId = Guid.NewGuid();
        //                    ControlDetenidoEvento.Guardar(item);

        //                }

        //            }
        //            return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "registro",Alertadetenido=alertadet,Mensajealerta=detenidosinguardar });
        //        }
        //        else
        //        {
        //            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
        //    }
        //}

        [WebMethod]
        public static DataTable getUnidadadesTabla(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string tracking, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        Entity.Evento evento = new Entity.Evento();

                        if (!string.IsNullOrEmpty(tracking))
                        {
                            evento = ControlEvento.ObtenerByTrackingId(new Guid(tracking));
                            where.Add(new Where("E.Id", Convert.ToString(evento.Id)));
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                            "U.Nombre Unidad",
                            "concat(R.Descripcion, '-', R.Nombre) ClaveResponsable",
                            "ifnull(C.Nombre,'') Corporacion"
                        },
                            from = new Table("evento_unidad_responsable", "EU"),
                            joins = new List<Join>
                            {
                                new Join(new Table("eventos", "E"), "EU.EventoId = E.Id", "inner"),
                                new Join(new Table("unidad", "U"), "EU.UnidadId = U.Id", "inner"),
                                new Join(new Table("responsable_unidad", "R"), "EU.ResponsableId = R.Id", "inner"),
                                new Join(new Table("Corporacion", "C"), "C.Id = U.CorporacionId", "left")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static DataTable getDetenidosTabla(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
                {
                    if (!emptytable)
                    {
                        if (tracking == "")
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        Entity.Evento evento = ControlEvento.ObtenerByTrackingId(new Guid(tracking));

                        where.Add(new Where("D.EventoId", evento.Id.ToString()));

                        Query query = new Query
                        {
                            select = new List<string> {
                                "D.Nombre",
                                "D.Paterno",
                                "D.Materno",
                                "S.Nombre Sexo",
                                "ifnull(D.Edad,0) as Edad",
                                "concat(D.Nombre,' ',TRIM(Paterno),' ',TRIM(Materno)) as NombreCompleto"
                                //"ifnull(TIMESTAMPDIFF(YEAR,Fechanacimiento,CURDATE()),0) AS Edad"
                            },
                            from = new Table("detenido_evento", "D"),
                            joins = new List<Join>
                            {
                                new Join(new Table("sexo", "S"), "D.SexoId = S.Id"),
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        // DataTable _dt = new DataTable();
                        // _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para realizar la acción.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }
        
        [WebMethod]
        public static DataTable getEventoVictimas(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
                {
                    if (!emptytable)
                    {
                        if (tracking == "")
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        Entity.Evento evento = ControlEvento.ObtenerByTrackingId(new Guid(tracking));

                        where.Add(new Where("Ev.EventoId", evento.Id.ToString()));

                        Query query = new Query
                        {
                            select = new List<string> {
                                "V.Nombre",
                                "Tv.Nombre",
                                //"V.Reparacion",
                                "V.DanioPerjuicio",
                                "ifnull(Ev.FechaHora,0) as Fecha",
                            },
                            from = new Table("evento_victima", "Ev"),
                            joins = new List<Join>
                            {
                                //new Join(new Table("evento_victima", "Ev"), evento.Id.ToString() + " = Ev.eventoId"),
                                new Join(new Table("victimas", "V"), "Ev.VictimaId = V.Id"),
                                new Join(new Table("tipo_victima", "Tv"), "Ev.TipoVictima = Tv.Id"),
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        // DataTable _dt = new DataTable();
                        // _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para realizar la acción.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
       
        [WebMethod]
        public static object getbyid(string _id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
                {
                    //int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var contract = ControlMotivoDetencion.ObtenerPorId(Convert.ToInt32(_id));
                    object obj = null;

                    var detenidoevento = ControlDetenidoEvento.ObtenerPorId(Convert.ToInt32(_id));
                    var sexo = ControlCatalogo.Obtener(detenidoevento.SexoId, Convert.ToInt32(Entity.TipoDeCatalogo.sexo));
                    obj = new
                    {
                        Sexo = sexo != null ? sexo.Nombre : "Sexo no registrado",
                        Edad = detenidoevento.Edad >= 105 ? "0" : detenidoevento.Edad.ToString()
                    };

                    return obj;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }

        }

        [WebMethod]
        public static List<Combo> getUnidadesAW(string institucionId)
        {
            List<Combo> combo = new List<Combo>();

            /*int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);*/
            List<Entity.WSAUnidad> unidad = new List<Entity.WSAUnidad>();

            if (!string.IsNullOrWhiteSpace(institucionId))
            {
                unidad = ControlWSAUnidad.ObtenerTodosPorIdInstitucion(Convert.ToInt32(institucionId));
            }

            if (unidad.Count > 0)
            {
                foreach (var rol in unidad)
                {
                    combo.Add(new Combo { Desc = rol.ClaveUnidad, Id = rol.IdUnidadInstitucion.ToString() });
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getInstitucionesAW()
        {
            List<Combo> combo = new List<Combo>();
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
            Entity.Subcontrato subcontrato = new Entity.Subcontrato();
            List<Entity.WSAInstitucion> instituciones = new List<Entity.WSAInstitucion>();
            subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            //subcontrato = ControlSubcontrato.ObtenerPorId(idContrato);

            if (subcontrato != null)
            {
                // patch
                var subcontratoWSAInstituciones = ControlSubcontratoWSAInstitucion.ObtenerPorSubcontratoId(subcontrato.Id);
                if(subcontratoWSAInstituciones.Count > 0)
                {
                    foreach (var item in subcontratoWSAInstituciones)
                    {
                        //var institucionWeb = ControlWSAInstitucion.ObtenerPorId(item.IdWSAInstitucion);
                        instituciones.Add(ControlWSAInstitucion.ObtenerPorId(item.IdWSAInstitucion));
                    }
                }
            }

            if (instituciones.Count > 0)
            {
                foreach (var rol in instituciones)
                {
                    combo.Add(new Combo { Desc = rol.NombreInstitucion, Id = rol.IdInstitucion.ToString() });
                }
            }

            return combo;
        }

        [WebMethod]
        public static string getEventosAW(string idUnidadInstitucion)
        {
            List<Combo> combo = new List<Combo>();
            List<object> lista = new List<object>();
            Entity.WSAUnidad unidad = new Entity.WSAUnidad();
            EventoAWAux evento = new EventoAWAux();
            string longitud = "";
            string latitud = "";

            unidad = ControlWSAUnidad.ObtenerPorId(Convert.ToInt32(idUnidadInstitucion));

            if (unidad != null) {
                
                try 
                {
                    evento = callWebServicesEventos(unidad.IdUnidadInstitucion, unidad.IdInstitucion);
                }

                catch (Exception ex)
                { 
                
                }
                
                if (evento != null)
                    {
                        var fechaEventoWS = Convert.ToDateTime(evento.fechaCapturaDetenidos);
                        var diffFecha = DateTime.Now - fechaEventoWS;
                        if (diffFecha.Days == 0 && diffFecha.Hours < 3)
                        {
                            string detenidos = string.Empty;
                            Entity.WSAMotivo motivo = new Entity.WSAMotivo();
                            Entity.WSAEstado estado = new Entity.WSAEstado();
                            Entity.WSAMunicipio municipio = new Entity.WSAMunicipio();
                            Entity.WSAColonia colonia = new Entity.WSAColonia();

                            motivo = ControlWSAMotivo.ObtenerPorId(Convert.ToInt32(evento.idMotivoEvento));
                            estado = ControlWSAEstado.ObtenerPorId(Convert.ToInt32(evento.lugar.idEstado));
                            municipio = ControlWSAMunicipio.ObtenerPorId(Convert.ToInt32(evento.lugar.idMunicipio));
                            colonia = ControlWSAColonia.ObtenerPorId(Convert.ToInt32(evento.lugar.idColonia));

                            if (evento.detenidos != null)
                            {
                                int contador = 1;

                                foreach (var detenido in evento.detenidos)
                                {
                                    detenidos = detenidos.Trim() + " " + contador + ") " + detenido.nombre + " " + detenido.apellidoPaterno + " " + detenido.apellidoMaterno + "/" + detenido.sexo + "/" + detenido.edad + "\n";
                                    contador++;
                                }
                            }

                            object obj = new
                            {
                                Folio = evento.folio,
                                Fecha = evento.fechaEvento,
                                Motivo = motivo != null ? motivo.NombreMotivoLlamada : "Sin motivo",
                                NumeroDetenidos = evento.numeroDetenidos,
                                Estado = estado != null ? estado.NombreEstado : "Sin estado",
                                Municipio = municipio != null ? municipio.NombreMunicipio : "Sin municipio",
                                Colonia = colonia != null ? colonia.NombreColonia : "Sin colonia",
                                Numero = evento.lugar.numero != "" ? evento.lugar.numero : "Sin número",
                                EntreCalle = evento.lugar.entreCalle != "" ? evento.lugar.entreCalle : "" + " " + evento.lugar.ycalle != "" ? evento.lugar.ycalle : "",
                                Responsable = evento.nombreResponsable != "" ? evento.nombreResponsable : "",
                                Descripcion = evento.descripcion != "" ? evento.descripcion : "",
                                Detenidos = detenidos,
                                UnidadId = evento.idUnidad
                            };

                            lista.Add(obj);

                            latitud = evento.lugar.latitud;
                            longitud = evento.lugar.longitud;
                        }
                    }
                }

                return JsonConvert.SerializeObject(new { latitud = latitud, longitud = longitud, lista = lista });
            }


        [WebMethod]
        public static string relacionarLlamadaAW(Dictionary<string, string> csObj)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
                {
                    Entity.Evento evento;
                    EventoAWAux eventoAWAux = new EventoAWAux();

                    int eventoWSId = 0;
                    var mode = string.Empty;
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    //re1nv3nt3d
                    eventoAWAux = callWebServicesEventos(Convert.ToInt32(csObj["unidadId"]), Convert.ToInt32(csObj["institucionId"]));
                    evento = ControlEvento.ObtenerByTrackingId(new Guid(csObj["tracking"]));

                    if (csObj["tracking"] != "")
                    {
                        Entity.WSALugar lugarWSA = new Entity.WSALugar();
                        Entity.WSAEvento eventoWSA = new Entity.WSAEvento();
                        Entity.WSADetenido detenidoWSA = new Entity.WSADetenido();
                        int lugarId = 0;

                        lugarWSA.TrackingId = Guid.NewGuid();
                        lugarWSA.IdEstado = Convert.ToInt32(eventoAWAux.lugar.idEstado);
                        lugarWSA.IdMunicipio = Convert.ToInt32(eventoAWAux.lugar.idMunicipio);
                        lugarWSA.IdColonia = Convert.ToInt32(eventoAWAux.lugar.idColonia);
                        lugarWSA.Numero = eventoAWAux.lugar.numero;
                        lugarWSA.EntreCalle = eventoAWAux.lugar.entreCalle;
                        lugarWSA.Sector = eventoAWAux.lugar.sector;
                        lugarWSA.Latitud = eventoAWAux.lugar.latitud;
                        lugarWSA.Longitud = eventoAWAux.lugar.longitud;
                        lugarWSA.Ycalle = eventoAWAux.lugar.ycalle;
                        lugarId = ControlWSALugar.Guardar(lugarWSA);

                        eventoWSA.TrackingId = Guid.NewGuid();
                        eventoWSA.IdUnidad = Convert.ToInt32(eventoAWAux.idUnidad);
                        eventoWSA.IdInstitucion = Convert.ToInt32(eventoAWAux.idInstitucion);
                        eventoWSA.IdLugar = lugarId;
                        eventoWSA.Folio = eventoAWAux.folio;
                        eventoWSA.FechaEvento = Convert.ToDateTime(eventoAWAux.fechaEvento);
                        eventoWSA.FechaCapturaDetenidos = Convert.ToDateTime(eventoAWAux.fechaCapturaDetenidos);
                        eventoWSA.NumeroDetenidos = Convert.ToInt32(eventoAWAux.numeroDetenidos);
                        eventoWSA.NombreResponsable = eventoAWAux.nombreResponsable;
                        eventoWSA.Descripcion = eventoAWAux.descripcion;
                        eventoWSA.IdMotivo = Convert.ToInt32(eventoAWAux.idMotivoEvento);
                        eventoWSId = ControlWSAEvento.Guardar(eventoWSA);

                        if (eventoAWAux.detenidos != null)
                        {
                            //foreach (var detenido in eventoAWAux.detenidos)
                            //{
                            //    detenidoWSA.TrackingId = Guid.NewGuid();
                            //    detenidoWSA.Nombre = detenido.nombre;
                            //    detenidoWSA.ApellidoPaterno = detenido.apellidoPaterno;
                            //    detenidoWSA.ApellidoMaterno = detenido.apellidoMaterno;
                            //    detenidoWSA.FechaNacimiento = Convert.ToDateTime(detenido.fechaNacimiento);
                            //    detenidoWSA.Edad = Convert.ToInt32(detenido.edad);
                            //    detenidoWSA.Sexo = detenido.sexo;
                            //    detenidoWSA.Estatura = detenido.estatura;
                            //    detenidoWSA.IdEventoWS = eventoWSId;
                            //    ControlWSADetenido.Guardar(detenidoWSA);
                            //}
                        }

                        evento.IdEventoWS = eventoWSId;
                        evento.Latitud = eventoAWAux.lugar.latitud;
                        evento.Longitud = eventoAWAux.lugar.longitud;
                        mode = "actualizó";
                        ControlEvento.Actualizar(evento);
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode});
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });

                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        public static EventoAWAux callWebServicesEventos(int idUnidadInstitucion, int idInstitucion)
        {
            //List<EventoAWAux> evento = new List<EventoAWAux>();
            EventoAWAux evento = new EventoAWAux();

            var alertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
            if (alertaWeb != null)
            {
                // Llamada al web services por medio de HttpRequest
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create($@"{alertaWeb.UrlWebService}operaciones/solicitaInfoUnidad");
                // HttpWebRequest request = (HttpWebRequest)WebRequest.Create($@"{alertaWeb.UrlWebServuce}operaciones/solicitaInfoUnidad");
                // Original      http://192.168.113.32:9091/ControlDetenidos/operaciones/solicitaInfoUnidad

                request.ContentType = "application/json";
                request.Method = "POST";
                request.Timeout = 2000;

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    if (streamWriter != null)
                    {
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            usuario = alertaWeb.Username,
                            password = alertaWeb.Password,
                            idUnidadInstitucion = idUnidadInstitucion,
                            idInstitucion = idInstitucion
                        });

                        streamWriter.Write(json);
                    }                       
                }

                HttpWebResponse response = null;

                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                }
                catch (Exception ex)
                {
                    return null;
                }

                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    evento = JsonConvert.DeserializeObject<EventoAWAux>(result);
                }

                if (evento != null)
                {
                    return evento;
                }

                return evento;
            }
            else return null;
        }

        [WebMethod]
        public static object getPaisEstadoMunicipio()
        {
            try
            {
                Entity.Institucion institucion = new Entity.Institucion();
                Entity.Domicilio domicilio = new Entity.Domicilio();
                Entity.Subcontrato subcontrato = new Entity.Subcontrato();
                string paisId = "0";
                string estadoId = "0";
                string municipioId = "0";
                
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                if(contratoUsuario != null)
                    subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

                if(subcontrato != null)
                    institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

                if (institucion != null)
                    domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

                if(domicilio != null)
                {
                    paisId = domicilio.PaisId.ToString();
                    estadoId = domicilio.EstadoId.ToString();
                    municipioId = domicilio.MunicipioId.ToString();
                }

                var pais = ControlCatalogo.Obtener(domicilio.PaisId, 45);
                var estado = ControlEstado.Obtener(domicilio.EstadoId ?? 0);
                var municipio = ControlMunicipio.Obtener(domicilio.MunicipioId ?? 0);
                if (pais == null || estado == null || municipio == null) throw new Exception("No se encontró parte del domicilio del subcontrato.");

                var textoLocalizacion = $"{pais.Nombre[0].ToString().ToUpper() + pais.Nombre.Substring(1).ToLower()}, {estado.Nombre[0].ToString().ToUpper() + estado.Nombre.Substring(1).ToLower()}-{municipio.Nombre[0].ToString().ToUpper() + municipio.Nombre.Substring(1).ToLower()}";

                return JsonConvert.SerializeObject(new { exitoso = true, Pais = paisId, Estado = estadoId, Municipio = municipioId, Localizacion = textoLocalizacion });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getEvento(string tracking)
        {
            List<Combo> combo = new List<Combo>();
            Entity.Evento evento;

            if (tracking != null)
            {
                evento = ControlEvento.ObtenerByTrackingId(new Guid(tracking));

                if (evento != null)
                {
                    combo.Add(new Combo { Desc = evento.Folio + " " + evento.Descripcion, Id = evento.Id.ToString() });
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getDetenidos(string id)
        {
            List<Combo> combo = new List<Combo>();
            List<Entity.DetenidoEvento> detenidos;
            Entity.Evento evento = new Entity.Evento();

            evento = ControlEvento.ObtenerByTrackingId(new Guid(id));

            detenidos = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);

            if (detenidos.Count > 0)
            {
                foreach (var rol in detenidos)
                {
                    combo.Add(new Combo { Desc = rol.Nombre + " " + rol.Paterno + " " + rol.Materno, Id = rol.Id.ToString() });
                }
            }

            return combo;
        }

        //twins
        [WebMethod]
        public static string saveDetenidoBarandilla(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    Entity.Evento evento = new Entity.Evento();
                    Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                    Entity.Detenido detenido = new Entity.Detenido();
                    Entity.DetalleDetencion estatus = new Entity.DetalleDetencion();
                    Entity.DetalleDetencion estatusTemp = new Entity.DetalleDetencion();
                    Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                    Entity.Institucion institucion = new Entity.Institucion();
                    Entity.General general = new Entity.General();
                    List<Entity.Detenido> listaDetenido = new List<Entity.Detenido>();

                    int expediente = 0;
                    int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                    var listadetalleDetencion = ControlDetalleDetencion.ObteneTodos();
                    var contratoUsuarioAux = ControlContratoUsuario.ObtenerPorId(idContrato);
                    var mode = string.Empty;
                    bool alerta = false;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    string nombreDetenido = string.Empty;

                    var detenidoAux = ControlDetenido.ObtenerPorId(Convert.ToInt32(datos[1]));
                    detenidoEvento = ControlDetenidoEvento.ObtenerPorId(Convert.ToInt32(datos[1]));
                    evento = ControlEvento.ObtenerById(Convert.ToInt32(datos[0]));

                    detenido.Nombre = detenidoEvento.Nombre;
                    detenido.Paterno = detenidoEvento.Paterno;
                    detenido.Materno = detenidoEvento.Materno;
                    detenido.Detenidoanioregistro = DateTime.Now.Year;
                    detenido.Detenidomesregistro = DateTime.Now.Month;

                    estatus.CentroId = 0;
                    estatus.Fecha = DateTime.Now;
                    estatus.NCP = "npc";
                    estatus.Activo = true;
                    estatus.ExpedienteAdm = "";
                    estatus.Estatus = 1;
                    estatus.ContratoId = contratoUsuarioAux.IdContrato;
                    estatus.Tipo = contratoUsuarioAux.Tipo;
                    estatus.Detalledetencionanioregistro = DateTime.Now.Year;
                    estatus.Detalldetencionmesregistro = DateTime.Now.Month;
                    estatus.NombreDetenido = detenido.Nombre;
                    estatus.APaternoDetenido = detenido.Paterno;
                    estatus.AMaternoDetenido = detenido.Materno;
                    estatus.DetalledetencionSexoId= detenidoEvento.SexoId;
                    estatus.DetalledetencionEdad = detenidoEvento.Edad;

                    if (detenido != null)
                    {
                        var listaDetalleDetencionFiltrada = listadetalleDetencion.Where(x => x.ContratoId == contratoUsuarioAux.IdContrato && x.Tipo == contratoUsuarioAux.Tipo);

                        if (listaDetalleDetencionFiltrada.Count() > 0)
                        {
                            estatusTemp = listaDetalleDetencionFiltrada.Last();
                            expediente = Convert.ToInt32(estatusTemp.Expediente) + 1;
                        }
                        else
                            expediente = 1;

                        if (contratoUsuarioAux.Tipo == "contrato")
                        {
                            institucion = contratoUsuarioAux != null ? ControlInstitucion.ObtenerPorId(ControlContrato.ObtenerPorId(contratoUsuarioAux.IdContrato).InstitucionId) : null;
                        }
                        else if (contratoUsuarioAux.Tipo == "subcontrato")
                        {
                            institucion = contratoUsuarioAux != null ? ControlInstitucion.ObtenerPorId(ControlSubcontrato.ObtenerPorId(contratoUsuarioAux.IdContrato).InstitucionId) : null;
                        }

                        mode = "registró";

                        nombreDetenido = detenido.Nombre + ' ' + detenido.Paterno + ' ' + detenido.Materno;
                        listaDetenido = ControlDetenido.ObtenePorNombre(nombreDetenido);

                        if (listaDetenido.Count > 0)
                        {
                            mode = "Se detectaron a detenidos con información similar, el registro se realizó correctamente.";
                            alerta = true;
                        }

                        detenido.TrackingId = Guid.NewGuid();
                        detenido.Id = ControlDetenido.Guardar(detenido);

                        estatus.Expediente = Convert.ToString(expediente);
                        estatus.DetenidoId = detenido.Id;
                        estatus.CentroId = institucion.Id;
                        estatus.TrackingId = Guid.NewGuid();
                        estatus.Id = ControlDetalleDetencion.Guardar(estatus);

                        infoDetencion.Activo = true;
                        infoDetencion.IdInterno = detenido.Id;
                        infoDetencion.CreadoPor = usId;
                        infoDetencion.Descripcion = evento.Descripcion;
                        infoDetencion.Folio = evento.Folio;
                        infoDetencion.Habilitado = true;
                        infoDetencion.HoraYFecha = Convert.ToDateTime(evento.HoraYFecha);
                        infoDetencion.ColoniaId = Convert.ToInt32(evento.ColoniaId);
                        infoDetencion.ResponsableId = 0;
                        infoDetencion.UnidadId = 0;
                        infoDetencion.LugarDetencion = evento.Lugar;
                        infoDetencion.Motivo = evento.Descripcion;
                        infoDetencion.TrackingId = Guid.NewGuid();
                        infoDetencion.IdEvento = Convert.ToInt32(evento.Id);
                        infoDetencion.IdDetenido_Evento = detenidoEvento.Id;

                        general = new Entity.General();
                        general.DetenidoId = detenido.Id;
                        general.TrackingId = Guid.NewGuid();
                        //general.FechaNacimineto = DateTime.MinValue;
                        general.RFC = "";
                        
                        general.NacionalidadId = 38;
                        general.EscolaridadId = 19;
                        general.ReligionId = 0;
                        general.OcupacionId = 18;
                        general.EstadoCivilId = 0;
                        general.EtniaId = 0;
                        general.SexoId = detenidoEvento.SexoId;
                        general.EstadoMental = false;
                        general.Inimputable = false;
                        general.Edaddetenido = detenidoEvento.Edad;
                        general.Generalanioregistro = DateTime.Now.Year;
                        general.Generalmesregistro = DateTime.Now.Month;
                       
                        //general.FechaNacimineto = detenidoEvento.Fechanacimiento;
                        general.Id = ControlGeneral.Guardar(general);

                        infoDetencion.Id = ControlInformacionDeDetencion.Guardar(infoDetencion);

                        if (estatus.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = estatus.DetenidoId;
                            historial.Movimiento = "Registro del detenido en barandilla";
                            historial.TrackingId = detenido.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, alerta = alerta, id = detenido.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }

    public class postioncontract
    {
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
    public class DetenidoEventoData
    {
        public string EventoId { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string SexoId { get; set; }
        public string Edad { get; set; }
        public string Motivo { get; set; }


    }

    public class VictimaEventoData
    {
        public string EventoId { get; set; }
        public string Nombre { get; set; }
        public string TipoId { get; set; }
        public string DanioPerjuicio { get; set; }

    }

    public class EventoPibote
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string Folio { get; set; }
        public string HoraYFecha { get; set; }
        public int EstadoId { get; set; }
        public int MunicipioId { get; set; }
        public int ColoniaId { get; set; }
        public int MotivoId { get; set; }
        public int OrigenId { get; set; }
        public int InstitucionId { get; set; }
        public string CodigoPostal { get; set; }    
        public string Lugar { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public int NDet { get; set; }
        public int RecursoId { get; set; }
        public string Record { get; set; }
    }
    public class EventoAux
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string IdPais { get; set; }
        public string IdEstado { get; set; }
        public string IdMunicipio { get; set; }
        public string IdLlamada { get; set; }
        public string ColoniaId { get; set; }
        public string CodigoPostal { get; set; }
        public string Descripcion { get; set; }
        public string Folio { get; set; }
        public string HoraYFecha { get; set; }        
        public string Lugar { get; set; }
        public string NumeroDetenidos { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string UnidadIdAW { get; set; }
        public string MotivoId { get; set; }
    }

    public class OutUnset
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string IdPais { get; set; }
        public string IdEstado { get; set; }
        public string IdMunicipio { get; set; }
        public string ColoniaId { get; set; }
        public string IdLlamada { get; set; }
        public string CodigoPostal { get; set; }
        public string Descripcion { get; set; }
        public string Folio { get; set; }
        public string HoraYFecha { get; set; }        
        public string Lugar { get; set; }
        public string NumeroDetenidos { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string UnidadIdAW { get; set; }
        public string MotivoId { get; set; }
    }

    public class EventoAWAux
    {
        public string idUnidad { get; set; }
        public string idInstitucion { get; set; }
        public LugarAWAux lugar { get; set; }
        public string folio { get; set; }
        public string fechaEvento { get; set; }
        public string fechaCapturaDetenidos { get; set; }
        public string idMotivoEvento { get; set; }
        public string numeroDetenidos { get; set; }
        public List<DetenidoAWAux> detenidos { get; set; }
        public string nombreResponsable { get; set; }
        public string descripcion { get; set; }
    }

    public class LugarAWAux
    {
        public string idEstado { get; set; }
        public string idMunicipio { get; set; }
        public string idColonia { get; set; }
        public string numero { get; set; }
        public string entreCalle { get; set; }
        public string sector { get; set; }
        public string latitud { get; set; }
        public string longitud { get; set; }
        public string ycalle { get; set; }
    }

    public class DetenidoAWAux
    {
        public string nombre { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public string fechaNacimiento { get; set; }
        public string edad { get; set; }
        public string sexo { get; set; }
        public string estatura { get; set; }
    }
}