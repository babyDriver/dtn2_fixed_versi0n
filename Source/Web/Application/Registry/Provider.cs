﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Web.Application.Registry
{
    public class Provider  //<T> where T : object
    {
        //t4k3 of .conf
        private static JObject _jObj = new JObject();
        public string _nameSpace = "";
        public string _json = "";

        public JObject factory(string _nameSpace, string _json) 
        {
            using (var client = new HttpClient())  // instance HttpClient
            {
                var response = client.PostAsync(new Uri(_nameSpace), new StringContent(_json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult();
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    _jObj = JObject.Parse(responseContent);

                }
            }
            return _jObj;
        }
    }
}