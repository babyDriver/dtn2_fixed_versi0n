﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="True" CodeBehind="huella.aspx.cs" Inherits="Web.Application.Registry.huella" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Biométricos</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        .scroll2 {
            height: 20vw;
            overflow: auto;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="glyphicon glyphicon-edit"></i>
                Biométricos
            </h1>
        </div>
    </div>-->
    <asp:Label ID="Label2" runat="server"></asp:Label>
    <section id="widget-grid" class="">

          <div class="row padding-bottom-10">
            <div class="menu-container" style="display: inline-block; float: right">
                <a style="display: none;font-size:smaller" class="btn btn-default btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
                <a style="display: none;font-size:smaller" class="btn btn-success txt-color-white btn-lg" id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a> 
                <a style="display: none;font-size:smaller" class="btn btn-info btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
                <a style="font-size:smaller" class="btn btn-primary txt-color-white btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
                <a style="display: none;font-size:smaller" class="btn btn-warning btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-purple txt-color-white btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-yellow txt-color-white btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-red txt-color-white btn-lg" id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
                <a style="margin-right:30px; font-size:smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa fa-paw"></i> Biométricos</a>
            </div>
        </div>



        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
      
        <div class="scroll2" id="ScrollableContent">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <label style="float: right"> Mostrar: &nbsp</label>
                <div class="jarviswidget jarviswidget-color-darken" id="wid-huella-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="glyphicon glyphicon-bookmark"></i></span>
                        <h2>Biométricos</h2>
                    </header>
                    <div>
                        <select class="mostrar" style="float: right">
                            <option value="0" selected="selected">Todos</option>
                            <option value="1">Facial</option>
                            <option value="2">Iris</option> 
                            <option value="3">Huellas</option>
                            <option value="4">Huellas roladas</option>
                        </select>
                        <label style="float: right"> Tipo de biométrico: &nbsp</label>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body">
                        
                            <div class="row" id="addhuella">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                    <%--<a href="javascript:void(0);"  class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>--%>
                                    <a href="javascript:void(0);"  class="btn btn-md btn-default biometrico" id="biometrico"><i class="fa fa-plus"></i>&nbsp;KIB </a>
                                    <a href="javascript:void(0);"  class="btn btn-md btn-default biometrico" id="fichabiometrico"><i class="fa fa-file-pdf-o "></i>&nbsp;Generar ficha de biométricos </a>
                                </div>
                            </div>
                            <br />
                            <div class="row smart-form">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <section>
                                        <label class="label" style="color:#3276b1;font-weight:bold; ">Observaciones  </label>
                                        <label class="textarea">
                                            <i class="icon-append fa fa-comment"></i>
                                            <textarea rows="3" id="observaciones" placeholder="Observaciones" class="alphanumeric alptext" maxlength="999"  ></textarea>
                                            <b class="tooltip tooltip-bottom-right">Ingresa observaciones.</b>
                                        </label>
                                        <%--<a style="float: right; margin-left:3px;  display: block;font-size:smaller"   class="btn btn-default btnsaveobservacion" id="btnsaveobservacion"><i class="fa fa-save"></i>&nbsp;Guardar </a>--%>
                                    </section>
                                </div>
                                <footer>
                                    <a href="javascript:void(0);" class="btn btn-default btnsaveobservacion" id="SaveObservacion" title="Guardar"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                </footer>
                            </div>
                        </div>
                        <div></div>
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th data-class="expand">#</th>
                                    <th>Fotografía</th>
                                    <th>Tipo de biométrico</th>
                                    <th>Descripción</th>
                                    <%--<th data-hide="phone,tablet">Acciones</th>--%>
                                </tr>
                            </thead>
                        </table>
                        <div class="row smart-form">
                            <footer>
                                <div class="row" style="display: inline-block; float: right">
                                    <%--<a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default" id="ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>--%>
                                    <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a> 
                                </div>
                            </footer>
                        </div>  
                        <br />
                    </div>
                </div>
            </article>
        </div>
    </section>

    <div class="modal fade" id="form-modal" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal-title"><i class="fa fa-pencil" +=""></i> Editar registro</h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-serie" class="smart-form">
                        <div class="row">
                            <fieldset>
                                <section>
                                    <label style="color: dodgerblue" class="label">Mano <a style="color: red">*</a></label>
                                    <label class="select">
                                    <select name="mano" id="mano" style="width: 100%" class="input-sm"">
                                        <option value="izquierda">Izquierda</option>
                                        <option value="derecha">Derecha</option>
                                    </select>
                                    <i></i>
                                    </label>
                                </section>
                                <section>
                                    <label style="color: dodgerblue" class="label">Dedo <a style="color: red">*</a></label>
                                    <label class="select">
                                        <select name="dedo" id="dedo" style="width: 100%" class="input-sm">
                                            <option value="pulgar">Pulgar</option>
                                            <option value="indice">Índice</option>
                                            <option value="medio">Medio</option>
                                            <option value="anular">Anular</option>
                                            <option value="meñique">Meñique</option>
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                                <section>
                                    <label style="color: dodgerblue" class="input">Fotografía</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-file-image-o"></i>
                                        <asp:FileUpload ID="foto" runat="server"/>
                                    </label>
                                </section>
                            </fieldset>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                            <input type="hidden" id="trackingid" runat="server" />
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro que desea <strong><span id="verb"></span></strong>&nbsp;el registro?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>&nbsp;
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar huella
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro <strong><span id="itemeliminar"></span></strong>&nbsp;será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="photo-arrested" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="photo-arrested-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="#" alt="fotografía huella dactilar" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control alptext" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="tracking" runat="server" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="Ingreso" runat="server" value="" />
    <input type="hidden" id="Informacion_Personal" runat="server" value="" />
    <input type="hidden" id="Antropometria" runat="server" value="" />
    <input type="hidden" id="Señas_Particulares" runat="server" value="" />
    <input type="hidden" id="Adicciones" runat="server" value="" />
    <input type="hidden" id="Expediente_Medico" runat="server" value="" />
    <input type="hidden" id="Familiares" runat="server" value="" />
    <input type="hidden" id="idInterno" value="" />
    <input type="hidden" id="idsubcontrato" value="" />
    <input type="hidden" id="editable" runat="server" value="" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/protocolcheck.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            var sectionNumber = $("#lstTabs .active").children().attr("id").substring(7);
            if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                document.getElementById("linkexpediente").click();
                switch (sectionNumber) {
                    case "linkTab1": document.getElementById("linkTab1").click(); break;
                    
                }
                //SaveObservacion
            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                document.getElementsByClassName("SaveObservacion")[0].click();
                switch (sectionNumber) {
                    case "linkTab1": document.getElementById("SaveObservacion").click(); break;
                    
                }
            }
        });

        $(document).ready(function () {

            var urlIzquierda = '';
            var urlDerecha = '';

            $('body').on("keydown", function (e) {

                if (e.ctrlKey && e.which === 39) {
                    window.location.href = urlDerecha;
                }

                if (e.ctrlKey && e.which === 37) {
                    window.location.href = urlIzquierda;
                }

                if (e.ctrlKey && e.which === 71) {
                    SaveObservaciones();
                }
            });

            $("#ScrollableContent").css("height", "calc(100vh - 443px)");

            $("#datospersonales-modal").on("hidden.bs.modal", function () {
                document.getElementsByClassName("detencion")[0].focus();
            });

            $("body").on("click", ".add", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_mano").val("");
                $("#ctl00_contenido_dedo").val("");
                $("#ctl00_contenido_foto_").val("");
                $("#modal-title").html("Agregar huella");
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#form-modal").modal("show");
                LimpiarDatos();
            });

            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            init();
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                var tipofoto = $(this).attr("data-mano");
                $("#foto_detenido").attr("src", photo);
                $("#photo-arrested-title").html(tipofoto);

                $("#photo-arrested").modal("show");
            });

            $("#ligarNuevoEvento").hide();
            if ($("#<%= editable.ClientID %>").val() == "0") {
                $("#biometrico").hide();
                $("#ligarNuevoEvento").show();
            }

            window.emptytable = true;

            var rutaDefaultServer = "";

            getRutaDefaultServer();

            function getRutaDefaultServer() {
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;
                        }
                    }
                });
            }
            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {

                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');

                    }
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },

                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/huella.aspx/getlist",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.tracking = RequestQueryString("tracking");
                        parametrosServerSide.emptytable = false;        //window.emptytable;
                        parametrosServerSide.opcion = $(".mostrar").val();
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    null,
                    null,
                    null,
                    {
                        name: "Mano",
                        data: "Mano"
                    },
                    {
                        name: "Dedo",
                        data: "Dedo"
                    }//,
                    //null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        data: "Id",
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        width: "40px",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != "" && row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-mano="' + row.Dedo + '" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 3,
                        width: "250px",
                        render: function (data, type, row, meta) {
                            return data;
                        }
                    }
                    //{
                    //    targets: 5,
                    //    orderable: false,
                    //    render: function (data, type, row, meta) {                            
                    //        var txtestatus = "";
                    //        var icon = "";
                    //        var color = "";
                    //        var edit = "edit";
                    //        block = "";
                    //        var registro = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-mano="' + row.Mano + '" data-dedo="' + row.Dedo + '" data-activo= "' + row.Activo + '" data-fotografia="' + row.Fotografia + '" title = "Editar" > <i class="glyphicon glyphicon-pencil"></i></a>&nbsp; ';
                    //        if (row.Habilitado==1) {
                    //            txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                    //        }
                    //        else {
                    //            txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled"; transfer = "disabled"; reentry = "disabled";
                    //        }
                    //        if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                    //            registro = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id +'" style="display:none" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;'
                    //        }
                    //        if ($("#ctl00_contenido_LCADLW").val() == "true") block = '<a class="btn btn-' + color + ' btn-circle blockitem" id="block" href="javascript:void(0);" data-id="' + row.Id + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                    //        return registro + block;
                    //    }
                    //}
                ]
            });

            var file = document.getElementById($("#ctl00_contenido_foto").val());
            if (file != null && file != '') {
                if (!validaImagen(file)) {
                    ShowError("Logo", "Solo se permiten extensiones .jpg, .jpeg o .png");
                    esvalido = false;

                }
            }
            $(".mostrar").change(function () {
                window.table.api().ajax.reload();
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";


                if (url.indexOf("~/") == 0) {

                    url = baseUrl + url.substring(2);
                }
                return url;
            }
            function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }

                }
                return false;
            }
            $("body").on("click", "#biometrico", function () {

                OpenKib($("#idsubcontrato").val(), $("#idInterno").val());

            });

            $("body").on("click", "#fichabiometrico", function () {
                ImprimeTrabajoSocial();
            });

            function OpenKib(DetenidoId, subcontratoId) {

                window.openExternal("KIBSTATION:" + DetenidoId + "," + subcontratoId, function () {
                    alert("Ocurrió un error al abrir el recurso");
                });
            }

            $("body").on("click", ".save", function () {
                if (validar()) {
                    guardar();
                }
            });
            $("body").on("click", ".btnsaveobservacion", function () {
                SaveObservaciones();
            });

            $("body").on("click", ".delete", function () {
                var id = $(this).attr("data-tracking");
                var nombre = $(this).attr("data-value");
                $("#btndelete").attr("data-id", id);
                $("#delete-modal").modal("show");
            });

            onclick = "OpenKib('987654321')"

            $("body").on("click", ".blockitem", function () {

                var itemnameblock = $(this).attr("data-value");

                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "huella.aspx/delete",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                JSON.parse(data.d).mensaje + ".</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", JSON.parse(data.d).mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });

            function limpiar() {
                $("#mano").val("");
                $("#dedo").val("");
                $("#ctl00_contenido_foto").val("");
            }

            $("body").on("click", ".edit", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var img = ResolveUrl($(this).attr("data-fotografia"));
                $("#mano").val($(this).attr("data-mano"));
                $("#dedo").val($(this).attr("data-dedo"));

                $("#btnsave").attr("data-id", id);
                $("#btnsave").attr("data-fotografia", $(this).attr("data-fotografia"));

                $("#form-modal-title").empty();
                $("#form-modal-title").html('<i class="fa fa-pencil" +=""></i> Editar registro');
                $("#form-modal").modal("show");
            });

            function LimpiarDatos() {
                $("#mano").val(0);
                $('#mano').parent().removeClass('state-success');
                $('#mano').parent().removeClass("state-error");
                $("#dedo").val(0);
                $('#dedo').parent().removeClass('state-success');
                $('#dedo').parent().removeClass("state-error");
            }

            ///
            function ImprimeTrabajoSocial() {
                startLoading();
                var trackingId = $("#idInterno").val();
                $.ajax({
                    type: "POST",
                    url: "huella.aspx/GeneraPdF",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'trackingId': trackingId
                    }),
                    cache: false,
                    success: function (data) {
                        if (data.d.exitoso) {


                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "Se genero correctamente el reporte.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "Se genero correctamente el reporte .");

                            var ruta = ResolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                        }
                        else {
                            if (data.d.mensaje == "No se encontraron resultados") {
                                ShowAlert("¡Atención!", data.d.mensaje);
                            }
                            else {
                                ShowError("Error! Algo salió mal", data.d.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                            }
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + data.d.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }
            ///



            function validar() {
                var esvalido = true;
                //validaImagen();

                if ($("#mano").val() == null || $("#mano").val() == 0) {
                    ShowError("Mano", "Selecciona una mano");
                    $('#mano').parent().removeClass('state-success').addClass("state-error");
                    $('#mano').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#mano').parent().removeClass("state-error").addClass('state-success');
                    $('#mano').addClass('valid');
                }

                if ($("#dedo").val() == null || $("#dedo").val() == 0) {
                    ShowError("Dedo", "Selecciona un dedo");
                    $('#dedo').parent().removeClass('state-success').addClass("state-error");
                    $('#dedo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#dedo').parent().removeClass("state-error").addClass('state-success');
                    $('#dedo').addClass('valid');
                }

                var file = $("#ctl00_contenido_foto").val();
                if (file != null && file != '') {

                    if (!validaImagen(file)) {
                        ShowError("Logo", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        esvalido = false;

                    }
                }

                return esvalido;
            }

            function guardar() {

                var files = $("#ctl00_contenido_foto").get(0).files;
                var nombreAvatar = "";

                if (files.length > 0) {

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Handlers/FileUploadHandler.ashx?action=2&before=",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (Results) {
                            if (Results.exitoso) {
                                nombreAvatar = Results.nombreArchivo;
                                Save(nombreAvatar);

                            }
                            else {
                                ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                            }
                        },
                        error: function (err) {
                            ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else {
                    Save(nombreAvatar);
                }
            }

            function Save(rutaAvatar) {
                startLoading();

                var datos = ObtenerValores(rutaAvatar);

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/huella.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modal").modal("hide");
                            $('#main').waitMe('hide');
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                            LimpiarDatos();
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }


            function SaveObservaciones() {
                startLoading();

                var datos = ObtenerValoresObsercaciones();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/huella.aspx/saveObservaciones",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modal").modal("hide");
                            $('#main').waitMe('hide');
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "" + resultado.mensaje + "", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", resultado.mensaje);
                            LimpiarDatos();
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }
            //ObtenerValoresObsercaciones
            function ObtenerValoresObsercaciones() {
                var param = RequestQueryString("tracking");


                datos = [
                    TrackingId = param,
                    Observaciones = $("#observaciones").val()
                ];
                return datos;
            }
            function ObtenerValores(rutaAvatar) {
                var param = RequestQueryString("tracking");
                var id = $("#btnsave").attr("data-id");
                var img = $("#btnsave").attr("data-fotografia");

                datos = [
                    Id = id,
                    TrackingId = param,
                    Mano = $("#mano").val(),
                    Dedo = $("#dedo").val(),
                    Fotografia = rutaAvatar,
                    Imagen = img
                ];
                return datos;
            }

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    CargarDatos(param);
                    loadInternoId(param);
                }

                //if ($("#ctl00_contenido_LCADLW").val() == "true")
                //    $("#block").style("disbaled");

                if ($("#ctl00_contenido_KAQWPK").val() == "false")
                    $("#btnsave").hide();
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#add_").show();
                }
                else {
                    $("textarea,input, select").each(function (index, element) { $(this).attr('disabled', true); });
                }

                if ($("#ctl00_contenido_Ingreso").val() == "true") {
                    $("#linkingreso").show();
                }
                if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                    $("#linkinformacion").show();
                }
                if ($("#ctl00_contenido_Antropometria").val() == "true") {
                    $("#linkantropometria").show();
                }
                if ($("#ctl00_contenido_Señas_Particulares").val() == "true") {
                    $("#linkseñas").show();
                }
                if ($("#ctl00_contenido_Familiares").val() == "true") {
                    $("#linkfamiliares").show();
                }
                if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                    $("#linkexpediente").show();
                }
                //jc
                //if ($("#ctl00_contenido_Adicciones").val() == "true") {
                //    $("#linkinformacion").show();
                //}
                //if ($("#ctl00_contenido_Adicciones").val() == "true") {
                //    $("#linkInfoDetencion").show();
                //}
                $("#linkestudios").show();
                $("#linkInfoDetencion").show();
                $("#linkinformacion").show();
            }

            function loadInternoId(param) {

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/huella.aspx/getInternoId",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: param,
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        if (resultado.exitoso) {
                            $("#idInterno").val(resultado.Id);
                        }
                    },
                    error: function () {
                        ShowError("¡¡Error!", "Ocurrió un error. No fue posible cargar el Id del interno. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/huella.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            $('#edadInterno').text(resultado.obj.Edad);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);
                            $('#sexoInterno').text(resultado.obj.Sexo);
                            $('#municipioInterno').text(resultado.obj.municipioNombre);
                            $('#domicilioInterno').text(resultado.obj.domiclio);
                            $('#coloniaInterno').text(resultado.obj.coloniaNombre);
                            $('#idsubcontrato').val(resultado.obj.ContratoId);
                            $('#idInterno').val(resultado.obj.Id);
                            $('#observaciones').val(resultado.obj.Observacionebiometrico);


                            $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            urlIzquierda = "belongings.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val();
                            $("#linkvistas").attr("href", "huella.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkestudios").attr("href", "record.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkseñas").attr("href", "signal.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkingreso").attr("href", "entry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            urlDerecha = "entry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val();
                            $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información del interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
                var param = RequestQueryString("tracking");
                CargarDatosModal(param);
            });

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 443px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');

                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 216px)");
            });

        });
    </script>
</asp:Content>