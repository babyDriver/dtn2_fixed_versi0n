﻿using Business;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Application.Registry
{
    public class RandomCatalogue
    {
        public string data = "";
        public string data_id = "";
        public string data_desc = "";
        public string data_support = "";
        private DateTime t = DateTime.Now;
        private int _idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);

        public bool save(string flag, int id)
        {
            bool status = false;
            try
            {
                if (flag == "i")
                {
                    var attemp = ControlWSAInstitucion.ObtenerPorId(id);
                    if (attemp is null)
                    {
                        attemp = new Entity.WSAInstitucion();
                        attemp.IdInstitucion = id;
                        attemp.NombreInstitucion = data;
                        attemp.NombreCorto = data_support;
                        attemp.FechaModificacion = DateTime.Now;
                        attemp.Activo = true;
                        ControlWSAInstitucion.Guardar(attemp);
                    }
                    status = true;
                }
                else if (flag == "u")
                {
                    var attemp = ControlUnidad.ObtenerById(id);
                    if (attemp is null)
                    {
                        attemp = new Entity.Unidad();
                        attemp.Id = id;
                        attemp.Nombre = data;
                        attemp.Descripcion = data_desc;
                        attemp.CorporacionId = Convert.ToInt32(data_id);
                        attemp.ContratoId = _idContrato;
                        attemp.Activo = true;
                        ControlUnidad.Guardar(attemp);
                    }
                }
                //else if (flag == "non_f")
                //{
                    
                //    var attemp_repeat = ControlWSAMunicipio.ObtenerPorId(x);

                //    if (attemp_repeat is null)
                //    {          
                //        attemp_repeat = new WSAMunicipio();
                //        attemp_repeat.IdMunicipio = x;
                //        attemp_repeat.NombreMunicipio = data_support;
                //        attemp_repeat.FechaModificacion = t;
                //        attemp_repeat.Activo = true;
                //        ControlWSAMunicipio.Guardar(attemp_repeat);
                //    }
                //}
                else
                {
                    var attemp = ControlWSAMotivo.ObtenerPorId(id);

                    if (attemp is null)
                    {
                        attemp = new WSAMotivo();
                        attemp.IdMotivo = id;
                        attemp.NombreMotivoLlamada = data;
                        attemp.FechaModificacion = t;
                        attemp.Activo = true;
                        ControlWSAMotivo.Guardar(attemp);
                    }
                    //
                    status = true;
                }
            }
            catch (Exception ex) { }
            return status;
        }

    }
}