﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;
using Entity;

namespace Web.Application.Registry
{
    public partial class personalinformation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor == "")
                Response.Redirect("entrylist.aspx");

            if (Request.QueryString["editable"] == "1")
            {
                editable.Value = "1";
            }
            else
            {
                editable.Value = "0";
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }

        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                        //Acceso a Ingreso
                        parametros[1] = "Registro en barandilla";
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Ingreso.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Adicciones
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Adicciones.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Antropometria
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Antropometria.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Señas_Particulares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Señas_Particulares.Value = permisos.Consultar.ToString().ToLower();
                        }



                        //Acceso a Familiares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Familiares.Value = permisos.Consultar.ToString().ToLower();
                        }


                        //Acceso a Estudios_Criminologicos
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Estudios_Criminologicos.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Expediente_Medico
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Expediente_Medico.Value = permisos.Consultar.ToString().ToLower();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }


        // here is jhønny
        [WebMethod]
        public static string getdatos(string value)
        {
            try
            {
                object obj = null;
                object objnacimiento = null;
                bool tienenacimiento = true;
                object objdomicilio = null;
                bool tienedomicilio = true;
                object objgeneral = null;
                bool tienegeneral = true;
                bool tienedomsc = false;
                object objdomsc = null;
                int id = Convert.ToInt32(value);
                Entity.Detenido detenido = new Entity.Detenido();
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                {
                    //var detencion = ControlDetalleDetencion.ObtenerPorId(id);
                    Entity.Detenido interno = ControlDetenido.ObtenerPorId(id);

                    if (interno != null)
                    {
                        string[] datos = { interno.Id.ToString(), true.ToString() };
                        Entity.DetalleDetencion detail = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                        if (detail == null) detail = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                        Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detail.CentroId);
                        Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                        Entity.Domicilio nacimiento = ControlDomicilio.ObtenerPorId(interno.DomicilioNId);
                        Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                        Entity.DomicilioPorSubcontrato domicilioSC = ControlDomicilioPorSubcontrato.ObtenerporId(interno.Id); int edad = 0;                        
                        var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                        Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                        var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                        Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                        if (interno != null)
                        {
                            if (string.IsNullOrEmpty(interno.RutaImagen))
                            {
                                interno.RutaImagen = string.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                            }
                        }
                        obj = new
                        {
                            Id = interno.Id.ToString(),
                            TrackingId = interno.TrackingId.ToString(),
                            Expediente = (detail.Expediente) != null ? detail.Expediente : "",
                            Centro = institucion != null ? institucion.Nombre : "Institución no registrada",
                            Fecha = detail.Fecha != null ? Convert.ToDateTime(detail.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                            Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                            RutaImagen = interno.RutaImagen,
                        };

                        if (nacimiento != null)
                        {
                            objnacimiento = new
                            {
                                Id = nacimiento.Id,
                                TrackingId = nacimiento.TrackingId,
                                CalleNacimiento = nacimiento.Calle,
                                NumeroNacimiento = nacimiento.Numero,
                                //ColoniaNacimiento = nacimiento.Colonia,
                                //CPNacimiento = nacimiento.CP,
                                TelefonoNacimiento = nacimiento.Telefono,
                                PaisNacimiento = nacimiento.PaisId,
                                EstadoNacimiento = nacimiento.EstadoId,
                                MunicipioNacimiento = nacimiento.MunicipioId,
                                LocalidadNacimiento = nacimiento.Localidad,
                                ColoniaIdNacimiento = (nacimiento.ColoniaId) != null ? nacimiento.ColoniaId : 0,
                                CodigoPostalNacimiento = (nacimiento.ColoniaId) != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(nacimiento.ColoniaId)).CodigoPostal : ""
                            };
                        }
                        else
                        {
                            tienenacimiento = false;
                        }

                        if (domicilio != null)
                        {
                            objdomicilio = new
                            {
                                Id = domicilio.Id,
                                TrackingId = domicilio.TrackingId,

                                CalleDomicilio = domicilio != null ? domicilio.Calle : "Domicilio no registrado",
                                NumeroDomicilio = domicilio.Numero,
                                //ColoniaDomicilio = domicilio.Colonia,
                                //CPDomicilio = domicilio.CP,
                                TelefonoDomicilio = domicilio.Telefono,
                                PaisDomicilio = domicilio.PaisId,
                                EstadoDomicilio = domicilio.EstadoId,
                                MunicipioDomicilio = domicilio.MunicipioId,
                                LocalidadDomicilio = domicilio.Localidad != null ? domicilio.Localidad : "Localidad no registrado",
                                ColoniaIdDomicilio = (domicilio.ColoniaId) != null ? domicilio.ColoniaId : 0,
                                CodigoPostalDomicilio = (domicilio.ColoniaId) != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "",
                                municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                                coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                                LatitudDomicilio = string.IsNullOrEmpty(domicilio.Latitud) ? string.Empty : domicilio.Latitud,
                                LongitudDomicilio = string.IsNullOrEmpty(domicilio.Longitud) ? string.Empty : domicilio.Longitud
                            };
                        }
                        else
                        {
                            tienedomicilio = false;
                            if (domicilioSC != null)
                            {
                                tienedomsc = true;
                                objdomsc = new
                                {
                                    Id = domicilioSC.Id,
                                    MunicipioId = domicilioSC.MunicipioId,
                                    EstadoId = domicilioSC.EstadoId,
                                    PaisId = domicilioSC.PaisId
                                };

                            }
                        }
                        //if (general != null)
                        //{
                        //    if (general.FechaNacimineto != DateTime.MinValue)
                        //        edad = CalcularEdad(general.FechaNacimineto);
                        //    else if (general.Edaddetenido != 0)
                        //        edad = general.Edaddetenido;
                        //    else
                        //    {
                        //        var infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                        //        var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);

                        //        if (detenidoEvento != null)
                        //        {
                        //            edad = detenidoEvento.Edad;
                        //        }
                        //    }
                        //}

                        Entity.Catalogo sexo = new Entity.Catalogo();

                        if (general != null)
                        {
                            var nacion = ControlCatalogo.Obtener("MEXICANA", Convert.ToInt32(TipoDeCatalogo.nacionalidad));
                            sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                            var nacionalidad = ControlCatalogo.Obtener(general.NacionalidadId, Convert.ToInt32(TipoDeCatalogo.nacionalidad));
                            var idnacion = nacion.Id;
                            if (nacionalidad != null) idnacion = nacionalidad.Id;
                            var _etnia = ControlCatalogo.Obtener(general.EtniaId, Convert.ToInt32(Entity.TipoDeCatalogo.etnia));
                            objgeneral = new
                            {
                                Id = general.Id,
                                TrackingId = general.TrackingId,
                                FechaNacimineto = general.FechaNacimineto != DateTime.MinValue ? general.FechaNacimineto.ToString("dd/MM/yyyy") : "",
                                Nacionalidad = general.NacionalidadId != 39 ? idnacion : nacion.Id,
                                RFC = general.RFC,
                                Escolaridad = general.EscolaridadId,
                                Religion = general.ReligionId,
                                Ocupacion = general.OcupacionId,
                                Estado = general.EstadoCivilId,
                                Etnia = _etnia != null ? _etnia.Id : 0,
                                Sexo = sexo != null ? sexo.Nombre : "Sexo no registrado",
                                Mental = general.EstadoMental,
                                Inimputable = general.Inimputable,
                                Edad = edad > 0 ? Convert.ToString(edad) : "Fecha de nacimiento no registrada",
                                SexoId = sexo != null ? sexo.Id : 0,
                                LenguaNativaId = general.LenguanativaId.ToString(),
                                CURP = general.CURP != null ? general.CURP : "",
                                SalarioSemanal = general.SalarioSemanal.ToString("$ 0,0,0.00", CultureInfo.InvariantCulture)
                            };
                        }
                        else
                        {
                            string[] sex_name = {"No Aplica", "MASCULINO", "FEMENINO" };
                            int s = detail.DetalledetencionSexoId;
                            string rfc_fake = detail.APaternoDetenido.Substring(0, 2) + 
                                detail.AMaternoDetenido.Substring(0, 1) + detail.NombreDetenido.Substring(0, 1);
                            DateTime bornTime = DateTime.Now;
                            objgeneral = new
                            {
                                Nacionalidad = 39,
                                RFC = rfc_fake,
                                Sexo = s != 0 ? sex_name[s] : "Sexo no registrado",
                                Inimputable = 0,
                                FechaNacimiento = bornTime.AddYears(-edad),
                                Edad = detail.DetalledetencionEdad,
                                SexoId = s,
                                //CURP = general.CURP != null ? general.CURP : "",
                            };
                            //tienegeneral = false;
                        }
                        //detencion = ControlDetalleDetencion.ObtenerPorDetenidoId(detencion.DetenidoId).LastOrDefault();
                    }
                    //var dets = ControlDetalleDetencion.ObtenerPorDetenidoId(detencion.Id);
                    //var detencion = ControlDetalleDetencion.ObtenerPorDetenidoId(detenido.Id);
                    


                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, nacimiento = objnacimiento, tienenacimiento = tienenacimiento, domicilio = objdomicilio, tienedomicilio = tienedomicilio, general = objgeneral, tienegeneral = tienegeneral, tienedomsc = tienedomsc, objdomsc = objdomsc });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }

        [WebMethod]
        public static List<Combo> getPais()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.pais));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getMunicipio(string estadoId)
        {
            var combo = new List<Combo>();
            List<Entity.Municipio> listado = ControlMunicipio.ObtenerPorEstado(Convert.ToInt32(estadoId));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getNacionalidad()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.nacionalidad));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getEscolaridad()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.escolaridad));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getReligion()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.religion));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getOcupacion()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.ocupacion));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getEstadoCivil()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.estado_civil));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getEtnia()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.etnia));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getSexo()
        {
            List<Combo> combo = new List<Combo>();


            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getlenguanativa()
        {
            List<Combo> combo = new List<Combo>();


            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.lenguanativa));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getInstitucion(string estadoid)
        {
            List<Combo> combo = new List<Combo>();
            if (estadoid != null && estadoid != "" && estadoid != "null")
            {
                List<Entity.Institucion> instituciones = ControlInstitucion.ObtenerPorEstadoId(Convert.ToInt32(estadoid));
                if (instituciones.Count > 0)
                {
                    foreach (var rol in instituciones)
                        combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getEstado(string paisId)
        {
            var combo = new List<Combo>();
            List<Entity.Estado> listado = ControlEstado.ObtenerPorPais(Convert.ToInt32(paisId));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static DataTable getProcedencia(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                    {
                        if (!emptytable)
                        {


                            List<Where> where = new List<Where>();
                            // var detencion = ControlDetalleDetencion.ObtenerPorTrackingId();
                            Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(tracking));

                            // where.Add(new Where("P.Activo", "1"));
                            where.Add(new Where("P.InternoId", interno.Id.ToString()));

                            Query query = new Query
                            {
                                select = new List<string> {
                        "P.Id",
                        "P.TrackingId",
                        "P.Remisa",
                        "P.Expedir",
                        "P.EstadoId",
                        "P.CentroId",
                        "P.Activo",
                        "E.Nombre Estado",
                        "C.Nombre Centro"
                    },
                                from = new Table("procedencia", "P"),
                                joins = new List<Join>
                    {
                        new Join(new Table("estado", "E"), "P.EstadoId = E.id"),
                        new Join(new Table("institucion", "C"), "C.id  = P.CentroId"),
                    },
                                wheres = where
                            };


                            DataTables dt = new DataTables(mysqlConnection);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para realizar la acción.", draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
                finally
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static DataTable getaliaslog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            using(MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString)){
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                    {
                        if (!emptytable)
                        {

                            List<Where> where = new List<Where>();
                            List<Order> orderby = new List<Order>();
                            if (Convert.ToInt32(centroid) != 0)
                            {
                                where.Add(new Where("A.Id", centroid.ToString()));
                            }
                            if (Convert.ToBoolean(todoscancelados))
                            {
                                where.Add(new Where("C.Activo", "0"));
                            }

                            Query query = new Query
                            {
                                select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Alias",
                                "A.DetenidoId",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                                from = new Table("aliaslog", "A"),
                                joins = new List<Join>
                            {
                                new Join(new Table("vUsuarios","D"),"D.id=A.Creadopor","LEFT"),
                                new Join(new Table("Usuario","B"),"B.id=D.Usuarioid","LEFT"),
                                new Join(new Table("alias","C"),"C.Id=A.Id"),

                            },
                                wheres = where

                            };
                            DataTables dt = new DataTables(mysqlconection);
                            DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                            ///


                            ///
                            return _dt;
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
                finally
                {
                    if (mysqlconection.State == System.Data.ConnectionState.Open)
                        mysqlconection.Close();
                }
            }
        }

        [WebMethod]
        public static DataTable getAlias(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                    {
                        if (!emptytable)
                        {


                            List<Where> where = new List<Where>();

                            var detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(tracking));
                            Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(tracking));
                            //  where.Add(new Where("A.Activo", "1"));
                            where.Add(new Where("A.DetenidoId", interno.Id.ToString()));

                            Query query = new Query
                            {
                                select = new List<string> {
                                "A.Id",
                                "A.TrackingId",
                                "A.Alias",
                                "A.Activo"
                            },
                                from = new Table("alias", "A"),
                                joins = new List<Join>
                                {

                                },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para realizar la acción.", draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
                finally
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static DataTable getNombre(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                    {
                        if (!emptytable)
                        {

                            List<Where> where = new List<Where>();

                            var detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(tracking));
                            Entity.Detenido interno = ControlDetenido.ObtenerPorId(detencion.DetenidoId);
                            // where.Add(new Where("O.Activo", "1"));
                            where.Add(new Where("O.DetenidoId", interno.Id.ToString()));

                            Query query = new Query
                            {
                                select = new List<string> {
                        "O.Id",
                        "O.TrackingId",
                        "O.Nombre",
                        "O.Paterno",
                        "O.Materno",
                        "O.Activo"
                    },
                                from = new Table("otro_nombre", "O"),
                                joins = new List<Join>
                                {

                                },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para realizar la acción.", draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
                finally
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static string saveGeneral(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    string mode = string.Empty;

                    var añoNacimiento = int.Parse(datos[2].Substring(6));
                    bool esFebrero29 = datos[2].Substring(0, 5) == "29/02" ? true : false;
                    if (esFebrero29 && !DateTime.IsLeapYear(añoNacimiento))
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha de nacimiento no corresponde a un año bisiesto." });

                    var fechaNacimiento = Convert.ToDateTime(datos[2].ToString());
                    if (fechaNacimiento > DateTime.Now.AddYears(-1))
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha de nacimiento debe ser de un año anterior a la fecha de hoy." });
                    if (datos[3].ToString().Length > 13)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El RFC debe contar con un máximo de 13 caracteres." });
                    Regex validarRFC_CURP = new Regex("[^A-Z0-9]+");
                    if (validarRFC_CURP.IsMatch(datos[3].ToString().ToUpper()))
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El RFC no puede contener caracteres especiales." });
                    if (validarRFC_CURP.IsMatch(datos[15].ToString().ToUpper()))
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La CURP no puede contener caracteres especiales." });
                   
                    //f1x3d
                    Entity.General general = new Entity.General();
                    general.FechaNacimineto = Convert.ToDateTime(datos[2].ToString());
                    general.RFC = datos[3].ToString().ToUpper();
                    general.NacionalidadId = Convert.ToInt32(datos[4].ToString());
                    general.EscolaridadId = Convert.ToInt32(datos[5].ToString());
                    general.ReligionId = Convert.ToInt32(!String.IsNullOrEmpty(datos[6]) ? Convert.ToInt32(datos[6]) : 0);
                    general.OcupacionId = Convert.ToInt32(datos[7].ToString());
                    general.EstadoCivilId = Convert.ToInt32(datos[8].ToString());
                    general.EtniaId = Convert.ToInt32(!String.IsNullOrEmpty(datos[9]) ? Convert.ToInt32(datos[9]) : 0);
                    general.SexoId = Convert.ToInt32(datos[10].ToString());
                    general.EstadoMental = Convert.ToBoolean(datos[11].ToString());
                    general.Inimputable = Convert.ToBoolean(datos[12].ToString());
                    general.LenguanativaId = Convert.ToInt32(!String.IsNullOrEmpty(datos[14]) ? Convert.ToInt32(datos[14]) : 0);
                    general.CURP = Convert.ToString(datos[15].ToString());
                    general.SalarioSemanal = datos[16] != "" ? Convert.ToDecimal(datos[16].ToString()) : 0;
                    general.Edaddetenido = CalcularEdad(general.FechaNacimineto);

                    var detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[13]));
                    var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[13]));
                    var detalleDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                    var evento = ControlEvento.ObtenerById(detalleDetencion.IdEvento);
                    var infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                    var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);

                    string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion2 = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion2 == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    general.DetenidoId = interno.Id;

                    var mostrarAlertaEdad = false;
                    //if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                    if (detenidoEvento != null)
                    {
                        var edadBarandilla = CalcularEdad(general.FechaNacimineto);
                        var fechaEvento = evento.HoraYFecha;
                        var añosTrascurridosDesdeEvento = CalcularEdad(fechaEvento);
                        var edadRegistradaEvento = detenidoEvento.Edad;
                        if (edadBarandilla != edadRegistradaEvento + añosTrascurridosDesdeEvento) mostrarAlertaEdad = true;
                    }
                    
                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        mode = "actualizaron";
                        var genera2 = ControlGeneral.ObtenerPorDetenidoId(detalleDetencion2.DetenidoId);
                        if (general.FechaNacimineto != DateTime.MinValue)
                        {
                            general.Edaddetenido = CalcularEdad(general.FechaNacimineto);
                        }
                        else
                        {
                            general.Edaddetenido = genera2.Edaddetenido;
                        }
                        
                        general.Generalanioregistro = genera2.Generalanioregistro;
                        general.Generalmesregistro = genera2.Generalmesregistro;
                        general.Id = Convert.ToInt32(datos[0]);
                        general.TrackingId = new Guid(datos[1]);

                        ControlGeneral.Actualizar(general);
                        detalleDetencion2.DetalledetencionSexoId = general.SexoId;
                        detalleDetencion2.DetalledetencionEdad = general.Edaddetenido;
                        ControlDetalleDetencion.Actualizar(detalleDetencion2);


                        if (general.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Movimiento = "Modificación de datos generales en barandilla";
                            historial.TrackingId = Guid.NewGuid();
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        mode = "registró";
                        general.TrackingId = Guid.NewGuid();
                        general.Edaddetenido = DateTime.Today.AddTicks(-general.FechaNacimineto.Ticks).Year - 1;
                        general.Id = ControlGeneral.Guardar(general);

                        detalleDetencion2.DetalledetencionSexoId = general.SexoId;
                        detalleDetencion2.DetalledetencionEdad = general.Edaddetenido;
                        ControlDetalleDetencion.Actualizar(detalleDetencion2);

                        if (general.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Registro de datos generales en barandilla";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    if (mostrarAlertaEdad) return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = general.Id, TrackingId = general.TrackingId, mensaje2 = "La fecha de nacimiento no coincide con la edad registrada en el evento." });
                    else return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = general.Id, TrackingId = general.TrackingId, mensaje2="" });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    string mode = string.Empty;
                    Entity.Procedencia procedencia = new Entity.Procedencia();
                    procedencia.Remisa = datos[2].ToString();
                    procedencia.Expedir = datos[3].ToString();
                    procedencia.CentroId = Convert.ToInt32(datos[4].ToString());
                    procedencia.EstadoId = Convert.ToInt32(datos[5].ToString());
                    procedencia.Activo = true;

                    var detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[6]));
                    Entity.Detenido interno = ControlDetenido.ObtenerPorId(detencion.DetenidoId);
                    string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    procedencia.DetenidoId = interno.Id;

                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        mode = "actualizó";
                        procedencia.Id = Convert.ToInt32(datos[0]);
                        procedencia.TrackingId = new Guid(datos[1]);
                        //  ControlProcendencia.Actualizar(procedencia);
                    }
                    else
                    {
                        mode = "registró";
                        procedencia.TrackingId = Guid.NewGuid();
                        // procedencia.Id = ControlProcendencia.Guardar(procedencia);
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = procedencia.Id, TrackingId = procedencia.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string delete(int id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
                {

                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveAlias(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla " }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    string mode = string.Empty;
                    Entity.Alias alias = new Entity.Alias();
                    alias.Nombre = datos[2].ToString();
                    alias.Activo = true;
                    Entity.Alias alias2 = new Entity.Alias();
                    if (!string.IsNullOrEmpty(datos[1]))
                    {
                        alias2 = ControlAlias.ObtenerPorTrackingId(new Guid(datos[1]));
                    }
                    var detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[3].ToString()));
                    if (detencion == null)
                    {
                        var det = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[3].ToString()));
                        detencion = ControlDetalleDetencion.ObtenerPorDetenidoId(det.Id).LastOrDefault();
                    }
                    Entity.Detenido interno = ControlDetenido.ObtenerPorId(detencion.DetenidoId);
                    string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    alias.DetenidoId = interno.Id;

                    var aliasrepetido = ControlAlias.ObtenerPorAlias(alias);

                    var id = datos[0].ToString();

                    if (id == "")
                        id = "0";

                    if (aliasrepetido != null && aliasrepetido.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("Al parecer ya existe un alias ", alias.Nombre, ". Verifique la información y vuelva a intentarlo."));



                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        mode = "actualizó";
                        alias.Habilitado = alias2 != null ? alias2.Habilitado : alias.Habilitado;
                        alias.Id = Convert.ToInt32(datos[0]);
                        alias.TrackingId = new Guid(datos[1]);
                        ControlAlias.Actualizar(alias);

                        if (alias.Id > 0)
                        {
                            //Entity.Historial historial = new Entity.Historial();
                            //historial.Activo = true;
                            //historial.CreadoPor = usId;
                            //historial.Fecha = DateTime.Now;
                            //historial.Habilitado = true;
                            //historial.InternoId = alias.DetenidoId;
                            //historial.Movimiento = "Modificación de alias en barandilla";
                            //historial.TrackingId = alias.TrackingId;
                            //historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        mode = "registró";
                        alias.TrackingId = Guid.NewGuid();
                        alias.Creadopor = usId;
                        alias.Id = ControlAlias.Guardar(alias);

                        if (alias.Id > 0)
                        {
                            //Entity.Historial historial = new Entity.Historial();
                            //historial.Activo = true;
                            //historial.CreadoPor = usId;
                            //historial.Fecha = DateTime.Now;
                            //historial.Habilitado = true;
                            //historial.InternoId = alias.DetenidoId;
                            //historial.Movimiento = "Registro de alias en barandilla";
                            //historial.TrackingId = alias.TrackingId;
                            //historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = alias.Id, TrackingId = alias.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static object getpositiobycontract(string LlamadaId)
        {
            try
            {
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                EventoAWAux eventoAux = new EventoAWAux();
                Entity.Subcontrato subcontrato = new Entity.Subcontrato();



                subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);


                postioncontract obj = new postioncontract();
                obj.Latitud = subcontrato.Latitud;
                obj.Longitud = subcontrato.Longitud;



                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", obj });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string deleteAlias(int id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
                {
                    Entity.Alias alias = ControlAlias.ObtenerPorId(id);
                    string[] internoestatus = { alias.DetenidoId.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    alias.Activo = false;
                    ControlAlias.Actualizar(alias);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveNombre(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    string mode = string.Empty;
                    Entity.Otro_Nombre nombre = new Entity.Otro_Nombre();
                    nombre.Nombre = datos[2].ToString();
                    nombre.Paterno = datos[3].ToString();
                    nombre.Materno = datos[4].ToString();
                    nombre.Activo = true;

                    var detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[5]));
                    Entity.Detenido interno = ControlDetenido.ObtenerPorId(detencion.DetenidoId);
                    string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    nombre.DetenidoId = interno.Id;

                    string[] parametros = { nombre.Nombre, nombre.Paterno, nombre.Materno, nombre.DetenidoId.ToString() };
                    var nombrerepetido = ControlOtroNombre.ObtenerPorParametros(parametros);

                    var id = datos[0].ToString();

                    if (id == "")
                        id = "0";

                    if (nombrerepetido != null && nombrerepetido.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("Al parecer ya existe un nombre ", nombre.Nombre + " " + nombre.Paterno + " " + nombre.Materno, ". Verifique la información y vuelva a intentarlo."));



                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        mode = "actualizó";
                        nombre.Id = Convert.ToInt32(datos[0]);
                        nombre.TrackingId = new Guid(datos[1]);
                        ControlOtroNombre.Actualizar(nombre);
                    }
                    else
                    {
                        mode = "registró";
                        nombre.TrackingId = Guid.NewGuid();
                        nombre.Id = ControlOtroNombre.Guardar(nombre);
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = nombre.Id, TrackingId = nombre.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string deleteNombre(int id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
                {
                    Entity.Otro_Nombre nombre = ControlOtroNombre.ObtenerPorId(id);
                    string[] internoestatus = { nombre.DetenidoId.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    nombre.Activo = false;
                    ControlOtroNombre.Actualizar(nombre);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveDN(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    string mode = string.Empty;
                    //guardar domicilio
                    var detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[17]));
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[17]));
                    string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    Entity.Domicilio domicilio = new Entity.Domicilio();
                    domicilio.Calle = datos[2].ToString();
                    domicilio.Numero = datos[3].ToString();
                    //domicilio.Colonia = datos[4].ToString();
                    //domicilio.CP = Convert.ToInt32(datos[5].ToString());
                    domicilio.Telefono = datos[4].ToString();
                    domicilio.PaisId = Convert.ToInt32(datos[5].ToString());
                    if (domicilio.PaisId == 73)
                    {
                        domicilio.EstadoId = Convert.ToInt32(datos[6].ToString());
                        domicilio.MunicipioId = Convert.ToInt32(datos[7].ToString());
                        domicilio.ColoniaId = Convert.ToInt32(datos[20].ToString());
                    }
                    else
                    {
                        domicilio.Localidad = datos[18].ToString();
                    }
                    domicilio.Latitud = datos[22].ToString();
                    domicilio.Longitud = datos[23].ToString();

                    if (domicilio.Latitud.Length > 15)
                    {
                        domicilio.Latitud = domicilio.Latitud.Substring(0, 14);
                    }

                    if (domicilio.Longitud.Length > 15)
                    {
                        domicilio.Longitud = domicilio.Longitud.Substring(0, 15);
                    }
                    //guardar lugar nacimiento
                    Entity.Domicilio nacimiento = new Entity.Domicilio();
                    nacimiento.Calle = datos[11].ToString();
                    nacimiento.Numero = datos[12].ToString();
                    //nacimiento.Colonia = datos[15].ToString();
                    //nacimiento.CP = Convert.ToInt32(datos[16].ToString());
                    nacimiento.Telefono = datos[13].ToString();
                    nacimiento.PaisId = Convert.ToInt32(datos[14].ToString());
                    if (nacimiento.PaisId == 73)
                    {
                        nacimiento.EstadoId = Convert.ToInt32(datos[15] != null ? datos[15].ToString() : "0");
                        nacimiento.MunicipioId = Convert.ToInt32(datos[16] != null ? datos[16].ToString() : "0");
                        nacimiento.ColoniaId = Convert.ToInt32(datos[21] != null ? datos[21].ToString() : "0");
                    }
                    else
                    {
                        nacimiento.Localidad = datos[19].ToString();
                    }


                    if (!string.IsNullOrEmpty(datos[0].ToString()) && !string.IsNullOrEmpty(datos[9].ToString()))
                    {
                        mode = "actualizó";
                        domicilio.Id = Convert.ToInt32(datos[0]);
                        domicilio.TrackingId = new Guid(datos[1]);
                        ControlDomicilio.Actualizar(domicilio);
                        nacimiento.Id = Convert.ToInt32(datos[9]);
                        nacimiento.TrackingId = new Guid(datos[10]);
                        ControlDomicilio.Actualizar(nacimiento);
                        interno.DomicilioId = domicilio.Id;
                        interno.DomicilioNId = nacimiento.Id;
                        ControlDetenido.Actualizar(interno);

                        if (domicilio.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = interno.Id;
                            historial.Movimiento = "Modificación de domicilio actual en barandilla";
                            historial.TrackingId = domicilio.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }

                        if (nacimiento.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Modificación de domicilio de nacimiento en barandilla";
                            historial.TrackingId = nacimiento.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        mode = "guardo";
                        domicilio.TrackingId = Guid.NewGuid();
                        domicilio.Id = ControlDomicilio.Guardar(domicilio);
                        nacimiento.TrackingId = Guid.NewGuid();
                        nacimiento.Id = ControlDomicilio.Guardar(nacimiento);
                        interno.DomicilioId = domicilio.Id;
                        interno.DomicilioNId = nacimiento.Id;
                        ControlDetenido.Actualizar(interno);

                        if (domicilio.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Registro de domicilio actual en barandilla";
                            historial.TrackingId = domicilio.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }

                        if (nacimiento.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Registro de domicilio de nacimiento en barandilla";
                            historial.TrackingId = nacimiento.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Iddomicilio = domicilio.Id, TrackingIddomicilio = domicilio.TrackingId, Idnacimiento = nacimiento.Id, TrackingIdnacimiento = nacimiento.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                DateTime nacimiento;
                int edad = 0;
                if (general != null)
                {
                    nacimiento = general.FechaNacimineto;
                    edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = domicilio.Localidad + ", " + domicilio.Calle + " " + domicilio.Numero;

                }

                object obj = null;

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = "PENDIENTE",
                    Domicilio = strDomicilio,

                    RutaImagen = interno.RutaImagen,
                    Registro = detalleDetencion.Fecha,
                    Salida = "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitemprocedencia(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
                {
                    // Entity.Procedencia item = ControlProcendencia.ObtenerPorTrackingId(new Guid(trackingid));
                    // item.Activo = item.Activo ? false : true;
                    // ControlProcendencia.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitemalias(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
                {
                    Entity.Alias item = ControlAlias.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] internoestatus = { item.DetenidoId.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    item.Activo = item.Activo ? false : true;
                    item.Habilitado = item.Activo ? 1 : 0;
                    ControlAlias.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = item.Activo ? "Registro habilitado con éxito." : "Registro deshabilitado con éxito." });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitemnombre(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
                {
                    Entity.Otro_Nombre item = ControlOtroNombre.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] internoestatus = { item.DetenidoId.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    item.Activo = item.Activo ? false : true;
                    ControlOtroNombre.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getNeighborhoods(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();

            var colonias = ControlColonia.ObtenerPorMunicipioId(Convert.ToInt32(idMunicipio));

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias)
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getZipCode(string idColonia)
        {
            try
            {
                var colonia = ControlColonia.ObtenerPorId(Convert.ToInt32(idColonia));

                string codigoPostal = "";

                if (colonia != null)
                {
                    codigoPostal = colonia.CodigoPostal;
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", cp = codigoPostal });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, });
            }
        }
    }
}