using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Text;
using System.Data;
using System.Web;
using System.Linq;
using Entity.Util;
using System.Web.UI;

namespace Web.Application.Registry
{
    public partial class InformacionDetenido : System.Web.UI.Page
    {
        private MySqlDataReader reader
        {
            get
            {
                return (MySqlDataReader)
                  ViewState["DATOSTMP"];
            }
            set
            {
                ViewState["DATOSTMP"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {                
                getPermisos();
                tbFechaInicio.Text = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                tbFechaFin.Text = DateTime.Now.ToString("yyyy-MM-dd");
                InitPage();
            }
        }
        

        protected void Btn_Click(object sender, EventArgs e)
        {
            // create a table
            getInterno();
            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "requestHandler();", true);
        }


        protected void Btn_Data(object sender, EventArgs e)
        {
            var btn = (System.Web.UI.WebControls.Button)sender;
            string[] arg = new string[2];
            arg = btn.CommandArgument.ToString().Split(';');
            int id = Int32.Parse(arg[0]);
            int idAux = Int32.Parse(arg[1]);
            getDescripcion(id, idAux);
            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "fill", "showDescription();", true);

        }

        public void InitPage()
        {
            cargarOcupacion();
            cargarNacionalidad();
            cargarSexo();
            cargarPais();
            cargarEstado();
            cargarMunicipio();
            cargarColonia();
            getInterno();
        }


        // create a new variable type permission
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Información de Detenido" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);

                string[] parametrosReingreso = { usuario.ToString(), "Información de Detenido" };
                var permisosReingreso = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametrosReingreso);

                string[] parametrosEgreso = { usuario.ToString(), "Información de Detenido" };
                var permisosEgreso = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametrosEgreso);

                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //Consultar
                        this.WERQEQ.Value = permisos.Consultar.ToString().ToLower();

                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();

                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                        //setear hidden reingreso
                        if (permisosReingreso != null)
                            this.RAWMOV.Value = permisosReingreso.Consultar.ToString().ToLower();
                        //setear hidden egreso
                        if (parametrosEgreso != null)
                            this.VYXMBM.Value = permisosEgreso.Consultar.ToString().ToLower();
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }


        [WebMethod]
        public void getInterno() /*int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable*/

        {
            //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Información de Detenido" }).Consultar)
            
            try
            {
                MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                List<Where> where = new List<Where>();

                var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                //Filtros
                var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var contrato = contratoUsuarioK.IdContrato;
                //-----Filtro contratos-------------------------
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                //buscar el contrato del subcontrato actual
                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuarioK.IdContrato);
                List<Entity.Subcontrato> listaSubcontratos = ControlSubcontrato.ObtenerByContratoId(subcontrato.ContratoId);
                List<Entity.Subcontrato> listaSubcontratosActivos = new List<Entity.Subcontrato>();
                //extraer solo los subcontratos activos                                            

                foreach (var item in listaSubcontratos)
                {
                    if (item.Activo)
                    {
                        listaSubcontratosActivos.Add(item);
                    }
                }

                string IdsContratos = "(";

                int i = 0;

                foreach (var item in listaSubcontratosActivos)
                {
                    if (i == listaSubcontratosActivos.Count - 1)
                    {
                        IdsContratos += item.Id + ")";
                    }
                    else
                    {
                        IdsContratos += item.Id + ",";
                    }

                    i++;
                }

                where.Add(new Where("E.ContratoId", Where.IN, IdsContratos));
                where.Add(new Where("E.Tipo", "subcontrato"));
                //where.Add(new Where("E.ContratoId", "=", contrato.ToString()));
                //where.Add(new Where("E.Tipo", contratoUsuarioK.Tipo));
                //----------------------------------------------

                where.Add(new Where("cast(E.Fecha as DATE)", ">=", tbFechaInicio.Text));
                where.Add(new Where("cast(E.Fecha as DATE)", "<=", tbFechaFin.Text));

                if (nombre.Text != "")
                    where.Add(new Where("trim(I.Nombre)", Where.LIKE, "%" + nombre.Text + "%"));

                if (paterno.Text != "")
                    where.Add(new Where("trim(I.Paterno)", Where.LIKE, "%" + paterno.Text + "%"));

                if (materno.Text != "")
                    where.Add(new Where("trim(I.Materno)", Where.LIKE, "%" + materno.Text + "%"));

                if (rfc.Text != "")
                    where.Add(new Where("G.RFC", "=", rfc.Text));

                if (edad.Text != "")
                    where.Add(new Where("V.Edad", "=", edad.Text));

                if (sexo.SelectedIndex.ToString() != "0")
                    where.Add(new Where("G.SexoId", "=", sexo.SelectedValue));

                if (ocupacion.SelectedIndex.ToString() != "0")
                    where.Add(new Where("G.OcupacionId", "=", ocupacion.SelectedValue));

                if (nacionalidad.SelectedIndex.ToString() != "0")
                    where.Add(new Where("G.NacionalidadId", "=", nacionalidad.SelectedValue));

                if (calle.Text != "")
                    where.Add(new Where("D.Calle", "=", calle.Text));

                if (colonia.SelectedIndex.ToString() != "0")
                    where.Add(new Where("D.ColoniaId", "=", colonia.SelectedValue));

                if (ipais.SelectedIndex.ToString() != "0")
                    where.Add(new Where("D.PaisId", "=", ipais.SelectedValue));

                if (iestado.SelectedIndex.ToString() != "0")
                    where.Add(new Where("D.EstadoId", "=", iestado.SelectedValue));

                if (imunicipio.SelectedIndex.ToString() != "0")
                    where.Add(new Where("D.MunicipioId", "=", imunicipio.SelectedValue));

                if (telefono.Text != "")
                    where.Add(new Where("D.Telefono", "=", telefono.Text));

                if (alias.Text != "")
                    where.Add(new Where("A.Alias", "=", alias.Text));

                if (peso.Text != "")
                    where.Add(new Where("An.Peso", "=", peso.Text));

                if (estatura.Text != "")
                    where.Add(new Where("An.Estatura", "=", estatura.Text));

                //-----------------------------------------------------

                Query query = new Query
                {
                    select = new List<string> {
                                "I.Id",
                                "E.Id idAux",
                                "trim(I.Nombre) Nombre",
                                "trim(I.Paterno) Paterno",
                                "trim(I.Materno) Materno",
                                "concat(TRIM(I.Nombre),' ',TRIM(Paterno),' ',TRIM(Materno)) as NombreCompleto",
                                "I.RutaImagen RutaImagen",
                                "IF(TRIM(I.RutaImagen)<>' ' && I.RutaImagen IS NOT NULL, REPLACE(I.RutaImagen, '.jpg', '.thumb'), '/Content/img/avatars/male.png') as RutaThumb",
                                "cast(E.Expediente as unsigned) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus",
                                "ES.Nombre EstatusNombre",
                                "V.Edad Edad"
                            },
                    from = new Table("detenido", "I"),
                    joins = new List<Join>
                            {
                                new Join(new Table("detalle_detencion", "E"), "I.Id  = E.DetenidoId"),
                                new Join(new Table("estatus", "ES"), "ES.Id  = E.Estatus"),
                                //new Join(new Table("contrato", "c"), "c.Id  = E.ContratoId"),
                                new Join(new Table("general", "G"), "G.DetenidoId = I.Id"),
                                new Join(new Table("domicilio", "D"), "I.DomicilioId = D.Id"),
                                new Join(new Table("alias", "A"), "A.DetenidoId = I.Id"),
                                new Join(new Table("antropometria", "An"), "An.DetenidoId = I.Id"),
                                new Join(new Table("vinfodetenido_edad", "V"), "I.Id = V.Id")
                            },
                    wheres = where,
                    groupBy = "E.Expediente",
                    
                };

                //DataTables dt = new DataTables(mysqlConnection);
                //var data = dt.Generar(query, draw, start, length, search, order, columns);
                
                bool isEmpty = where.Count() <= 4;
                if (!isEmpty)
                {
                    mysqlConnection.Open();
                    MySqlCommand cmd = new MySqlCommand(query.GenerarConsulta());
                    cmd.Connection = mysqlConnection;

                    MySqlDataReader dr = cmd.ExecuteReader();

                    /*MySqlDataAdapter adapter = new MySqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);*/
                    DataSet ds = new DataSet();
                    System.Data.DataTable dataTable = new System.Data.DataTable();
                    dataTable.Load(dr);
                    dataTable = dataTable.AsEnumerable().Skip(0).Take(1000).CopyToDataTable();
                    ds.EnforceConstraints = false;
                    ds.Tables.Add(dataTable);
                    dt_basic.DataSource = ds;
                    dt_basic.DataBind();
                    cmd.Dispose();
                    dr.Close();
                    mysqlConnection.Close();
                }
                else
                {
                    DataSet ds = new DataSet();
                    System.Data.DataTable tbl = new System.Data.DataTable();
                    tbl.Columns.Add("Id");
                    tbl.Columns.Add("RutaImagen");
                    tbl.Columns.Add("RutaThumb");
                    tbl.Columns.Add("Nombre");
                    tbl.Columns.Add("Paterno");
                    tbl.Columns.Add("Materno");
                    tbl.Columns.Add("Expediente");
                    tbl.Columns.Add("EstatusNombre");
                    ds.Tables.Add(tbl);
                    dt_basic.DataSource = ds;
                    dt_basic.DataBind();
                    //return DataTables.ObtenerDataTableVacia(null, 1);
                }
            }
            catch (Exception ex)
            {
                //return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                Response.Write(ex.Message);
            }

            //else
            //return DataTables.ObtenerDataTableVacia("", draw);
        }

        // crear dropdownlist
        #region Dropdowns
        protected void OnSelectedIndexChangedFirst(object sender, EventArgs e)
        {
            cargarEstado();
            cargarMunicipio();
            cargarColonia();
        }

        protected void OnSelectedIndexChangedSecond(object sender, EventArgs e)
        {
            cargarMunicipio();
            cargarColonia();
        }

        protected void OnSelectedIndexChangedThird(object sender, EventArgs e)
        {
            cargarColonia();
        }
        public void cargarOcupacion()
        {

            try
            {
                ocupacion.DataSource = getOcupacion();
                ocupacion.DataValueField = "Id";
                ocupacion.DataTextField = "Desc";
                ocupacion.DataBind();
                ocupacion.Items.Insert(0, "[Ocupacion]");
            }

            catch (Exception e)
            {
                throw new ArgumentException("Error!, No fue posible cargar la lista de ocupaciones.", e);
            }

        }

        public static List<Combo> getOcupacion()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.ocupacion));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        public void cargarNacionalidad()
        {

            try
            {
                nacionalidad.DataSource = getNacionalidad();
                nacionalidad.DataValueField = "Id";
                nacionalidad.DataTextField = "Desc";
                nacionalidad.DataBind();
                nacionalidad.Items.Insert(0, "[Nacionalidad]");
            }

            catch (Exception e)
            {
                throw new ArgumentException("Error!, No fue posible cargar la lista de nacionalidades.", e);
            }

        }

        public static List<Combo> getNacionalidad()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.nacionalidad));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        public void cargarColonia()
        {

            try
            {
                string var;
                if (imunicipio.SelectedValue.ToString() == "[Municipio]") { var = "0"; }
                else { var = imunicipio.SelectedValue.ToString(); }
                colonia.DataSource = getColonia(var);
                colonia.DataValueField = "Id";
                colonia.DataTextField = "Desc";
                colonia.DataBind();
                colonia.Items.Insert(0, "[Colonia]");
            }

            catch (Exception e)
            {
                throw new ArgumentException("Error!, No fue posible cargar la lista de colonias.", e);
            }

        }


        [WebMethod]
        public static List<Combo> getColonia(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();

            var colonias = ControlColonia.ObtenerPorMunicipioId(Convert.ToInt32(idMunicipio));

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias)
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;

        }

        public void cargarPais()
        {

            try
            {
                ipais.DataSource = getPais();
                ipais.DataValueField = "Id";
                ipais.DataTextField = "Desc";
                ipais.DataBind();
                ipais.Items.Insert(0, "[Pais]");
            }

            catch (Exception e)
            {
                throw new ArgumentException("Error!, No fue posible cargar la lista de paises.", e);
            }

        }

        [WebMethod]
        public static List<Combo> getPais()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.pais));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        public void cargarEstado()
        {

            try
            {
                iestado.DataSource = getEstado(ipais.SelectedIndex.ToString());
                iestado.DataValueField = "Id";
                iestado.DataTextField = "Desc";
                iestado.DataBind();
                iestado.Items.Insert(0, "[Estado]");
            }

            catch (Exception e)
            {
                throw new ArgumentException("Error!, No fue posible cargar la lista de estados.", e);
            }

        }

        [WebMethod]
        public static List<Combo> getEstado(string paisId)
        {
            var combo = new List<Combo>();
            List<Entity.Estado> listado = ControlEstado.ObtenerPorPais(Convert.ToInt32(paisId));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        public void cargarMunicipio()
        {

            try
            {
                imunicipio.DataSource = getMunicipio(iestado.SelectedIndex.ToString());
                imunicipio.DataValueField = "Id";
                imunicipio.DataTextField = "Desc";
                imunicipio.DataBind();
                imunicipio.Items.Insert(0, "[Municipio]");
            }

            catch (Exception e)
            {
                throw new ArgumentException("Error!, No fue posible cargar la lista de municipios.", e);
            }

        }

        [WebMethod]
        public static List<Combo> getMunicipio(string estadoId)
        {
            var combo = new List<Combo>();

            List<Entity.Municipio> listado = ControlMunicipio.ObtenerPorEstado(Convert.ToInt32(estadoId));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        public void cargarSexo()
        {

            try
            {
                sexo.DataSource = getSexo();
                sexo.DataValueField = "Id";
                sexo.DataTextField = "Desc";
                sexo.DataBind();
                sexo.Items.Insert(0, "[Sexo]");
            }

            catch (Exception e)
            {
                throw new ArgumentException("Error!, No fue posible cargar la lista de sexos.", e);
            }

        }

        [WebMethod]
        public static List<Combo> getSexo()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        #endregion Dropdowns

        public class DataTableParameter
        {
            public int draw { get; set; }
            public int length { get; set; }
            public int start { get; set; }
            public string search { get; set; }
        }

        // do nothing
        [WebMethod]
        public void obtener(string info)
        {

            DataTableParameter dtp = JsonConvert.DeserializeObject<DataTableParameter>(info);
            System.Data.DataTable dataTableFilter = new System.Data.DataTable();
            DataSet ds = new DataSet();
            dataTableFilter.Load(reader);
            dataTableFilter = dataTableFilter.AsEnumerable().Skip(dtp.start).Take(dtp.length).CopyToDataTable();
            ds.Tables.Add(dataTableFilter);

        }

        public void getDescripcion(int id, int idAux)
        {
            try
            {

                Entity.Detenido interno = ControlDetenido.ObtenerPorId(id);
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorId(idAux);
                //string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                Entity.InformacionDeDetencion info = ControlInformacionDeDetencion.ObtenerPorInternoId(id);
                Entity.Antropometria antropometria = ControlAntropometria.ObtenerPorDetenidoId(id);
                Entity.Evento evento = ControlEvento.ObtenerById(info.IdEvento);

                #region Nuevo Requerimiento
                //Verificar si esta Egresado y si lo esta, verificar que no pasen mas de 120 horas desde su salida
                //ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).FirstOrDefault();
                var estatus = detalleDetencion.Estatus;
                string strNombre = "";
                strNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                nombreInterno.Text = strNombre;

                if (estatus == 2)//Egreso
                {
                    var fechaEntrada = (DateTime)detalleDetencion.Fecha;
                    var totalHoras = 0;
                    Entity.Calificacion calificacionDetenido = ControlCalificacion.ObtenerPorInternoId(interno.Id);
                    if (calificacionDetenido != null)
                    {
                        totalHoras = calificacionDetenido.TotalHoras;
                    }
                    var fechaSalidaDetenido = fechaEntrada.AddHours(totalHoras);
                    var fechaSalidaDetenido_Horas_Sumadas = fechaSalidaDetenido.AddHours(120.00);

                    if (fechaSalidaDetenido_Horas_Sumadas < DateTime.Now)//Si la fecha de salida + 120 horas es anterior a la fecha actual no debera mostrar info del detenido
                    {
                        throw new Exception("Información no disponible");
                    }

                }
                #endregion Nuevo Requerimiento

                var calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                DateTime nacimiento;
                int edad = 0;
                if (general != null)
                {
                    nacimiento = general.FechaNacimineto;
                    edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = domicilio.Localidad + ", " + domicilio.Calle + " " + domicilio.Numero;

                }

                if (info != null)
                {
                    horaRegistro.Text = info.HoraYFecha.ToString();
                    var detenidoevento = ControlDetenidoEvento.ObtenerPorId(info.IdDetenido_Evento);
                    if (detenidoevento != null)
                    {
                        var wsa = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                        if (wsa != null)
                        {
                            motivoInterno.Text = wsa.NombreMotivoLlamada;
                        }
                    }

                }

                if (antropometria != null)
                {
                    pesoInterno.Text = antropometria.Peso.ToString();
                    estaturaInterno.Text = antropometria.Estatura.ToString();
                }

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = evento.HoraYFecha;
                fechaSalidaAdd = DateTime.MinValue;

                if (calificacion != null)
                {
                    var horas = calificacion.TotalHoras;
                    fechaSalidaAdd = fechaSalida.AddHours(horas);
                    salida.Text = fechaSalidaAdd.ToString();
                }

                //var hist = ControlHistorial.ObtenerPorDetenido(detalleDetencion.Id).Where(x => x.Movimiento.ToLower().Contains("salida") || x.Movimiento.ToLower().Contains("traslado"));
                //foreach (var item in hist)
                //{
                //    fechaSalidaAdd = item.Fecha;
                //}

                if (institucion != null)
                    centro.Text = institucion.Nombre;

            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error al cargar datos", ex);
            }
        }
    }
}
