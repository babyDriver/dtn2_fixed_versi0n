﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using static IronPython.Runtime.Profiler;

namespace Web.Application.Registry
{
    //saveAddress(address[0], address[1], address[2], suburb, town, wsa.Latitud, wsa.Longitud, wsa.Calle, wsa.CalleRef, wsa.CodigoPostal, wsa.No, "55 5555 5555");
    public class RandomAddress
    {
        public int sIdentifier, tIdentifier, stIdentifier;
        public string suburb;
        public string town;
        public string state;
        public string lat;
        public string lon;
        public string location;
        public string street;
        public string refStreet;
        public string postalCode;
        public string no;
        public string phone;
        
        public int[] saveSuburb(string ivalue)
        {
            int[] result = { };
            Guid data_unique = Guid.NewGuid();
            //string ivalue = sIdentifier.ToString();
            try
            {
                RandomCatalogue randomCatalogue = new RandomCatalogue();
                var attemp = ControlColonia.ObtenerPorMunicipioCodigoColonia(ivalue);
                if (attemp is null)
                {
                    //st▲t3 z
                    //var attemp_base = new Entity.Estado();
                    //attemp_base.Nombre = state;
                    //attemp_base.Descripcion = state;
                    //attemp_base.TrackingId = Guid.NewGuid();
                    //attemp_base.IdPais = 73;
                    //attemp_base.Habilitado = true;
                    //attemp_base.Activo = true;
                    //int st = ControlEstado.Guardar(attemp_base);
                    //tøwn zøn3
                    var attemp_repeat = new Entity.Municipio();
                    attemp_repeat.TrackingId = Guid.NewGuid();
                    attemp_repeat.Nombre = town; attemp_repeat.Descripcion = town;
                    attemp_repeat.Clave = tIdentifier.ToString();
                    attemp_repeat.EstadoId = 9;
                    attemp_repeat.Habilitado = true; attemp_repeat.Activo = true;
                    int t = ControlMunicipio.Guardar(attemp_repeat);
                    //C▲d z
                    attemp = new Entity.Colonia();
                    attemp.TrackingId = data_unique;
                    attemp.ClaveDeOficina = sIdentifier.ToString();
                    attemp.CodigoPostal = postalCode;
                    attemp.IdMunicipio = t;
                    attemp.Municipio = town;
                    attemp.Asentamiento = suburb;
                    attemp.TipoDeAsentamiento = "Colonia urbana";
                    attemp.Estado = string.IsNullOrEmpty(state) ? "EDOMEX" : state;
                    attemp.Habilitado = true;
                    attemp.Activo = true;
                    int s = ControlColonia.Guardar(attemp);
                    result = new int[] { s, t };
                }
                else
                    result = new int[] { attemp.Id, attemp.IdMunicipio };
                //var attempt = ControlMunicipio.ObtenerPorClave(tIdentifier);

            }

            catch { }

            return result;
        }

        public int[] saveAddress()
        {
            int[] idAddress = new int[2];
            Guid data_unique = new Guid();
            Entity.Domicilio address = new Entity.Domicilio();
            try
            {
                address.TrackingId = data_unique;
                //simpl4
                int[] x = saveSuburb(sIdentifier.ToString()).ToArray();
                address.ColoniaId = x[0];
                idAddress[0] = x[0];
                address.MunicipioId = x[1];
                address.PaisId = 72;
                address.EstadoId = 9;
                address.Longitud = lon;
                address.Latitud = lat;
                address.Calle = street;
                address.Localidad = location;
                //add calleRef
                if (!string.IsNullOrEmpty(refStreet))
                {
                    address.Calle += ';' + refStreet;
                }
                address.Numero = no;
                //fixed p. c0d3
                address.Localidad = postalCode;
                address.Telefono = phone;
                idAddress[1] = ControlDomicilio.Guardar(address);
            }
            catch (Exception ex) { }
            return idAddress;
        }    
    }
}