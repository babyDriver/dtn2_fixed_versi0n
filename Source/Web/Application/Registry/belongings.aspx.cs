﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace Web.Application.Registry
{
    public partial class belongings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          //  validatelogin();
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor != "")
                this.tracking.Value = valor;
            else
                Response.Redirect("entrylist.aspx");

            if (Request.QueryString["editable"] == "0")
            {
                editable.Value = "0";
            }
            else
            {
                editable.Value = "1";
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                        //Acceso a Informacion_Personal
                        parametros[1] = "Registro en barandilla";
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Informacion_Personal.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Adicciones
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Adicciones.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Antropometria
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Antropometria.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Señas_Particulares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Señas_Particulares.Value = permisos.Consultar.ToString().ToLower();
                        }



                        //Acceso a Familiares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Familiares.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Expediente_Medico
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Expediente_Medico.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Ingreso
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Ingreso.Value = permisos.Consultar.ToString().ToLower();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static string GetParametrosTamaño()
        {
            try
            {
                var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                var parametro = ControlParametroContrato.TraerTodos();
                decimal maximo = 0;
                decimal minimo = 0;

                foreach (var item in parametro)
                {
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 13)
                    {
                        minimo = Convert.ToDecimal(item.Valor);
                    }
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 14)
                    {
                        maximo = Convert.ToDecimal(item.Valor);
                    }
                }
                return JsonConvert.SerializeObject(new { exitoso = true, TMax = maximo, Tmin = minimo });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getPertenenciasComunes(string interno)
        {
            try
            {
                List<object> list = new List<object>();
                var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));                
                List<Entity.Catalogo> pertenenciasComunes = ControlCatalogo.ObtenerHabilitados(123);
                string[] ids = new string[] {interno,"true" };
                var detalledetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(ids);

                List<Entity.Pertenencia> pertenenciasRegistradas = ControlPertenencia.ObtenerPorInterno(Convert.ToInt32(detalledetencion.Id));
                string fecha = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                foreach (var item in pertenenciasComunes)
                {                    
                    object obj = new
                    {
                        Id = "",
                        TrackingId = "",
                        item.Nombre,
                        Fecha = fecha,
                        Observacion = "",
                        Cantidad = "",
                        Bolsa = "",
                        ClasificacionId = "",
                        CasilleroId = "",
                        Fotografia = "",
                        Casillero = "",
                        Clasificacion = ""
                    };

                    list.Add(obj);                    
                }

                foreach(var item in pertenenciasRegistradas)
                {
                    if(item.Habilitado && item.Activo)
                    {
                        object obj = new
                        {
                            Id = item.Id,
                            TrackingId = item.TrackingId.ToString(),
                            Nombre = item.PertenenciaNombre,
                            Fecha = fecha,
                            Observacion = item.Observacion,
                            Cantidad = item.Cantidad,
                            Bolsa = item.Bolsa,
                            ClasificacionId = item.Clasificacion,
                            CasilleroId = item.CasilleroId,
                            Fotografia = item.Fotografia,
                            Casillero = ControlCasillero.ObtenerPorId(item.CasilleroId).Nombre,
                            Clasificacion = ControlClasificacionEvidencia.ObtenerPorId(item.Clasificacion).Nombre
                        };

                        list.Add(obj);
                    }                                        
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getPertenenciasByPertenenciaId(string interno,string pertenenciaId)
        {
            try
            {
                List<object> list = new List<object>();
                var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                List<Entity.Catalogo> pertenenciasComunes = ControlCatalogo.ObtenerHabilitados(123);
                string[] ids = new string[] { interno, "true" };
                var detalledetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(ids);

                List<Entity.Pertenencia> pertenenciasRegistradas = ControlPertenencia.ObtenerPorInterno(Convert.ToInt32(detalledetencion.Id));
                string fecha = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
       

                foreach (var item in pertenenciasRegistradas.Where(x=>x.Id== Convert.ToInt32(pertenenciaId)))
                {
                    if (item.Habilitado && item.Activo)
                    {
                        object obj = new
                        {
                            Id = item.Id,
                            TrackingId = item.TrackingId.ToString(),
                            Nombre = item.PertenenciaNombre,
                            Fecha = fecha,
                            Observacion = item.Observacion,
                            Cantidad = item.Cantidad,
                            Bolsa = item.Bolsa,
                            ClasificacionId = item.Clasificacion,
                            CasilleroId = item.CasilleroId,
                            Fotografia = item.Fotografia,
                            Casillero = ControlCasillero.ObtenerPorId(item.CasilleroId).Nombre
                        };

                        list.Add(obj);
                    }
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string save(List<PertenenciaData> pertenencias, string interno)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    if (usuario.RolId == 13)
                    {
                        usuario = ControlUsuario.Obtener(8);
                    }

                    string[] internoestatus = { interno, true.ToString() };                    
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido" });

                    List<string> errores = new List<string>();
                    List<Business.PertenenciaAux> dataReporte = new List<Business.PertenenciaAux>();

                    foreach (var item in pertenencias)
                    {
                        var pertenenciaTemp = item.Pertenencia;
                        pertenenciaTemp = Regex.Replace(pertenenciaTemp, "[\"'´`]", "");
                        pertenenciaTemp = Regex.Replace(pertenenciaTemp, "<script>", "");
                        pertenenciaTemp = Regex.Replace(pertenenciaTemp, "hacked", "");
                        pertenenciaTemp = Regex.Replace(pertenenciaTemp, "[\"'´`]", "");

                        var observacionTemp = item.Observacion;
                        observacionTemp = Regex.Replace(observacionTemp, "[\"'´`]", "");
                        observacionTemp = Regex.Replace(observacionTemp, "<script>", "");
                        observacionTemp = Regex.Replace(observacionTemp, "hacked", "");
                        observacionTemp = Regex.Replace(observacionTemp, "[\"'´`]", "");

                        Entity.Pertenencia pertenencia = new Entity.Pertenencia();
                        pertenencia.Bolsa = Convert.ToInt32(item.Bolsa);
                        pertenencia.Cantidad = Convert.ToInt32(item.Cantidad);

                        if (pertenencia.Bolsa < 0)
                        {                            
                            throw new Exception(string.Concat("No se permite un valor negativo para bolsa."));
                        }

                        if (pertenencia.Cantidad < 0)
                        {
                            throw new Exception(string.Concat("No se permite un valor negativo para cantidad."));
                        }                        

                        pertenencia.Clasificacion = Convert.ToInt32(item.Clasificacion);
                        pertenencia.CreadoPor = usuario.Id;
                        pertenencia.Estatus = 4;
                        pertenencia.Fotografia = item.Fotografia;
                        if (string.IsNullOrEmpty(pertenencia.Fotografia))
                        {
                            pertenencia.Fotografia = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/object.png");

                        }
                        pertenencia.InternoId = detalleDetencion.Id;
                        pertenencia.Observacion = observacionTemp;
                        pertenencia.PertenenciaNombre = pertenenciaTemp;
                        pertenencia.CasilleroId = Convert.ToInt32(item.Casillero);

                        var casilleroNuevo = ControlCasillero.ObtenerPorId(Convert.ToInt32(item.Casillero));

                        if (!string.IsNullOrEmpty(item.Id))
                        {
                            var p = ControlPertenencia.ObtenerPorId(Convert.ToInt32(item.Id));
                            Entity.Casillero c = null;
                            if (p.CasilleroId != 0)
                            {
                                c = ControlCasillero.ObtenerPorId(p.CasilleroId);
                                c.Disponible = c.Disponible + p.Bolsa;
                            }

                            if (Convert.ToInt32(item.Casillero) != p.CasilleroId)
                            {
                                if (pertenencia.Bolsa > casilleroNuevo.Disponible)
                                {
                                    throw new Exception(string.Concat("No hay espacio suficiente en el casillero para la cantidad de bolsas"));
                                }
                            }
                            else
                            {
                                if (pertenencia.Bolsa > c.Disponible)
                                {
                                    throw new Exception(string.Concat("No hay espacio suficiente en el casillero para la cantidad de bolsas"));
                                }
                            }

                            mode = "actualizó";
                            pertenencia.Habilitado = true;
                            pertenencia.Activo = true;
                            pertenencia.Id = Convert.ToInt32(item.Id);
                            pertenencia.TrackingId = new Guid(item.TrackingId);
                            ControlPertenencia.Actualizar(pertenencia);

                            pertenencia = ControlPertenencia.ObtenerPorId(pertenencia.Id);
                            dataReporte.Add(new Business.PertenenciaAux()
                            {
                                Id = pertenencia.Id.ToString(),
                                Nombre = pertenencia.PertenenciaNombre,
                                FechaEntrada = DateTime.Now.ToString(),
                                Bolsa = pertenencia.Bolsa.ToString(),
                                Cantidad = pertenencia.Cantidad.ToString(),
                                Casillero = ControlCasillero.ObtenerPorId(pertenencia.CasilleroId).Nombre,
                                Clasificacion = ControlCatalogo.Obtener(pertenencia.Clasificacion, 85).Nombre,
                                Estatus = pertenencia.Estatus.ToString(),
                                Observacion = pertenencia.Observacion
                            });

                            if (c != null)
                                ControlCasillero.Actualizar(c);

                            var casilleroNuevo2 = ControlCasillero.ObtenerPorId(Convert.ToInt32(item.Casillero));
                            casilleroNuevo2.Disponible = casilleroNuevo2.Disponible - pertenencia.Bolsa;
                            ControlCasillero.Actualizar(casilleroNuevo2);
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usuario.Id;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Movimiento = "Modificación de datos generales en control de pertenencias en barandilla";
                            historial.TrackingId = Guid.NewGuid();
                            historial.Id = ControlHistorial.Guardar(historial);

                        }
                        else
                        {
                            mode = "registró";
                            if (pertenencia.Bolsa > casilleroNuevo.Disponible)
                            {
                                throw new Exception(string.Concat("No hay espacio suficiente en el casillero para la cantidad de bolsas"));
                            }

                            pertenencia.Habilitado = true;
                            pertenencia.Activo = true;
                            pertenencia.TrackingId = Guid.NewGuid();
                            pertenencia.Id = ControlPertenencia.Guardar(pertenencia);

                            pertenencia = ControlPertenencia.ObtenerPorId(pertenencia.Id);
                            dataReporte.Add(new Business.PertenenciaAux()
                            {
                                Id = pertenencia.Id.ToString(),
                                Nombre = pertenencia.PertenenciaNombre,
                                FechaEntrada = DateTime.Now.ToString(),
                                Bolsa = pertenencia.Bolsa.ToString(),
                                Cantidad = pertenencia.Cantidad.ToString(),
                                Casillero = ControlCasillero.ObtenerPorId(pertenencia.CasilleroId).Nombre,
                                Clasificacion = ControlCatalogo.Obtener(pertenencia.Clasificacion, 85).Nombre,
                                Estatus = pertenencia.Estatus.ToString(),
                                Observacion = pertenencia.Observacion
                            });

                            if (pertenencia.Id <= 0)
                            {
                                errores.Add("Hubo un error al intentar registrar la pertenencia con el nombre de " + pertenencia.PertenenciaNombre+", ");
                            }

                            casilleroNuevo.Disponible = casilleroNuevo.Disponible - pertenencia.Bolsa;
                            ControlCasillero.Actualizar(casilleroNuevo);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usuario.Id;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Movimiento = "Modificación de datos generales en control de pertenencias en barandilla";
                            historial.TrackingId = Guid.NewGuid();
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    var detenido = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
                    var domicilio = ControlDomicilio.ObtenerPorId(detenido.DomicilioId);

                    string nombreInterno = detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno;
                    string colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).Asentamiento : "" : "";
                    string cp = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "" : "";
                    string calle = (domicilio != null) ? (domicilio.Calle != null) ? domicilio.Calle : "" : "";
                    string numero = (domicilio != null) ? (domicilio.Numero != null) ? domicilio.Numero : "" : "";
                    string domicilioCompleto = "Calle: " + calle + " #" + numero + " Colonia:" + colonia + " C.P." + cp;
                    string bolsa = pertenencias[0].Bolsa;

                    string recibio = usuario.User;

                    //Generar PDF
                    object[] obj = ControlPDF.GenerarReciboDevolucionesPDF(dataReporte, nombreInterno, detalleDetencion.Expediente, domicilioCompleto, recibio, "", "", bolsa, nombreInterno, detenido.Id);
                    Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                    reportesLog.ProcesoId = detenido.Id;
                    reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                    reportesLog.Modulo = "Registro en barandilla";
                    reportesLog.Reporte = "Recibo de pertenencias";
                    reportesLog.Fechayhora = DateTime.Now;
                    reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    reportesLog.EstatusId = 1;
                    ControlReportesLog.Guardar(reportesLog);

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, errores = errores, ubicacionArchivo = obj[1].ToString() });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitem(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
                {
                    Entity.Pertenencia item = ControlPertenencia.ObtenerPorTrackingId(new Guid(trackingid));
                    item.Habilitado = item.Habilitado ? false : true;
                    ControlPertenencia.Actualizar(item);
                    string message = "";
                    message = item.Habilitado ? "Registro habilitado con éxito." : "Registro deshabilitado con éxito.";
                    return JsonConvert.SerializeObject(new { exitoso = true, message });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, messaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, message = ex.Message });
            }
        }

        [WebMethod]
        public static string delete(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Eliminar)
                {
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getlist(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(tracking));
                        string[] datosInterno = new string[]
                        {
                            interno.Id.ToString(),
                            true.ToString()
                        };
                        var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datosInterno);

                        List<Where> where = new List<Where>();

                        where.Add(new Where("P.Activo", "1"));
                        where.Add(new Where("ES.TrackingId", detalleDetencion.TrackingId.ToString()));

                        Query query = new Query
                        {
                            select = new List<string>{
                                "P.Id",
                                "P.TrackingId",                                
                                "P.InternoId",
                                "P.Pertenencia",
                                "P.Observacion",
                                "P.Clasificacion",
                                "P.Bolsa",
                                "P.Cantidad", 
                                "P.Fotografia",
                                "P.Activo",                       
                                "P.Habilitado", 
                                "P.Estatus",
                                "P.CreadoPor",
                                "P.CasilleroId"
                            },
                            from = new Table("Pertenencia", "P"),
                            joins = new List<Join> {
                                    new Join(new DT.Table("detalle_detencion", "ES"), "P.InternoId = ES.Id", "LEFT")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            return edad;
        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Consultar)
                {
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                    if (detalleDetencion == null) detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();

                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                    Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    Entity.Catalogo sexo=new Entity.Catalogo();
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else if (general.Edaddetenido != 0)
                            edad = general.Edaddetenido;
                        else
                        {
                            var infDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infDetencion.IdDetenido_Evento);

                            if (detenidoEvento != null)
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }
                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }
                    var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                    Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                    var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                    Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                    object obj = null;
                    if (interno != null)
                    {
                        if (string.IsNullOrEmpty(interno.RutaImagen))
                        {
                            interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                        }
                    }

                    string calleAux = domicilio != null ? string.IsNullOrEmpty(domicilio.Calle) ? string.Empty : domicilio.Calle : string.Empty;
                    string numAux = domicilio != null ? string.IsNullOrEmpty(domicilio.Numero) ? string.Empty : domicilio.Calle : string.Empty;
                    string domicilioAux = string.IsNullOrEmpty(calleAux) ? string.Empty : calleAux;
                    domicilioAux += string.IsNullOrEmpty(numAux) ? string.Empty : " #" + numAux;

                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = detalleDetencion.Expediente,
                        Centro = institucion != null ? institucion.Nombre : "Institución no registrada",
                        Fecha = detalleDetencion.Fecha != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                        RutaImagen = interno.RutaImagen,
                        Edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        Sexo = sexo.Nombre!=null?sexo.Nombre:"Sexo no registrado",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        domiclio = string.IsNullOrEmpty(domicilioAux) ? "Domicilio no registrado" : domicilioAux,
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj});
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getContracts()
        {
            var combo = new List<Combo>();
            var c = ControlContrato.ObtenerTodos();

            foreach (var item in c)
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getInternos(string tkg)
        {
            var combo = new List<Combo>();
            Guid tkgDetenido;
            Guid.TryParse(tkg, out tkgDetenido);
            var c = ControlDetenido.ObtenerPorTrackingId(tkgDetenido);

            if(c != null)
            {
                combo.Add(new Combo { Desc = c.Nombre + " " + c.Paterno + " " + c.Materno, Id = c.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getInternoId(string datos)
        {
            try
            {
                var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos));

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Ok.", Id = interno.Id });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getEstatusPertenencia()
        {
            var combo = new List<Combo>();
            var c = ControlEstatusPertenencia.ObtenerTodos();

            foreach (var item in c)
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getClasificacion()
        {
            var combo = new List<Combo>();
            var c = ControlClasificacionEvidencia.ObtenerTodos();

            foreach (var item in c)
            {
                if(item.Habilitado)
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }
        [WebMethod]
        public static List<Combo> getCasillerosAll()
        {
            var combo = new List<Combo>();
            var c = ControlCasillero.ObtenerTodos();

            var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

            foreach (var item in c)
            {
                if (item.Activo && item.ContratoId == contratoUsuarioK.IdContrato)
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCasilleros()
        {
            var combo = new List<Combo>();
            var c = ControlCasillero.ObtenerTodos();

            var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
           
            foreach (var item in c)
            {
                if (item.Activo && item.ContratoId == contratoUsuarioK.IdContrato && item.Disponible > 0)
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                
                int edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = domicilio.Localidad + ", " + domicilio.Calle + " " + domicilio.Numero;

                }

                object obj = null;

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                    Situacion = "PENDIENTE",
                    Domicilio = strDomicilio,

                    RutaImagen = interno.RutaImagen,
                    Registro = detalleDetencion.Fecha,
                    Salida = "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }

    public class PertenenciaData
    {
        public string Pertenencia { get; set; }
        public string Observacion { get; set; }
        public string Bolsa { get; set; }
        public string Cantidad { get; set; }
        public string Clasificacion { get; set; }
        public string Casillero { get; set; }
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string Fotografia { get; set; }
    }
}