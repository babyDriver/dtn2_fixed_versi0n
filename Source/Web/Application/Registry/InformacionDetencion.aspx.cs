﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Registry
{
    public partial class InformacionDetencion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();

            if (Request.QueryString["editable"] == "1")
            {
                editable.Value = "1";
            }
            else
            {
                editable.Value = "0";
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                        //Acceso a Informacion_Personal
                        parametros[1] = "Registro en barandilla";
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);                        

                        //Acceso a Adicciones
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Adicciones.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Antropometria
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Antropometria.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Señas_Particulares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Señas_Particulares.Value = permisos.Consultar.ToString().ToLower();
                        }



                        //Acceso a Familiares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Familiares.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Expediente_Medico
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Expediente_Medico.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Ingreso
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Ingreso.Value = permisos.Consultar.ToString().ToLower();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static object GetAlertaWeb()
        {
            try
            {
                var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
                var denoma = "Alerta web";
                var alerta = "alerta web";
                if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
                {
                    denoma = datosAlertaWeb.Denominacion;
                    alerta = denoma;
                }
                object data = new
                {

                    Denominacion = denoma,
                    AlertaWerb = alerta
                };

                return data;
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }


        [WebMethod]
        public static List<Combo> getCountries()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.pais));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getStates(string idPais)
        {
            List<Combo> combo = new List<Combo>();
            var data = ControlEstado.ObtenerPorPais(Convert.ToInt32(idPais));

            if (data.Count > 0)
            {
                foreach (var rol in data)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCities(string idEstado)
        {
            List<Combo> combo = new List<Combo>();
            var data = ControlMunicipio.ObtenerPorEstado(Convert.ToInt32(idEstado));

            if (data.Count > 0)
            {
                foreach (var rol in data)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getNeighborhoods(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();

            var colonias = ControlColonia.ObtenerPorMunicipioId(Convert.ToInt32(idMunicipio));

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias)
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getZipCode(string idColonia)
        {
            try
            {
                var colonia = ControlColonia.ObtenerPorId(Convert.ToInt32(idColonia));

                string codigoPostal = "";

                if (colonia != null)
                {
                    codigoPostal = colonia.CodigoPostal;
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", cp = codigoPostal });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, });
            }
        }

        [WebMethod]
        public static List<Combo> getCalls()
        {
            List<Combo> combo = new List<Combo>();

            var calls = ControlLlamada.ObtenerTodos();

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            if (calls.Count > 0)
            {
                foreach (var rol in calls)
                    if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Descripcion, Id = rol.Id.ToString() });
                    }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getEvents(string idLlamada, string idEvento)
        {
            List<Combo> combo = new List<Combo>();

            int lamadaid = idLlamada != "" ? Convert.ToInt32(idLlamada) : 0;
            List<Entity.Evento> events ;
            if (lamadaid > 0)

                events = ControlEvento.ObtenerPorLlamadaId(Convert.ToInt32(idLlamada));
            else {
               // var evento= ControlEvento.ObtenerById(Convert.ToInt32(idEvento));
                events = new List<Entity.Evento>
                 {
                     ControlEvento.ObtenerById(Convert.ToInt32(idEvento))
                 };



            }

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            if (events.Count > 0)
            {
                foreach (var rol in events)
                    if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Descripcion, Id = rol.Id.ToString() });
                    }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getUnits(string id)
        {
            List<Combo> combo = new List<Combo>();

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
            
            var units = ControlUnidad.ObtenerPorEventoId(Convert.ToInt32(id));       

            if (units.Count > 0)
            {
                foreach (var rol in units)
                    if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
                    }
            }

            return combo;
        }

        [WebMethod]
        public static DataTable getUnidadadesTabla(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string tracking, bool emptytable)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    if (!emptytable)
                    {
                        List<Where> where = new List<Where>();
                        Entity.Evento evento = new Entity.Evento();

                        if (!string.IsNullOrEmpty(tracking))
                        {
                            var guidId = new Guid(tracking);
                            evento = ControlEvento.ObtenerByTrackingId(guidId);
                            where.Add(new Where("E.Id", Convert.ToString(evento.Id)));
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                        "E.Id EventoId",
                        "U.Id UnidadId",
                        "U.Nombre Unidad",
                        "R.Id ResponsableId",
                        "R.Nombre Responsable",
                        "ifnull(C.Nombre,'') Corporacion"
                    },
                            from = new DT.Table("evento_unidad_responsable", "EU"),
                            joins = new List<Join>
                        {
                            new Join(new DT.Table("eventos", "E"), "EU.EventoId = E.Id", "inner"),
                            new Join(new DT.Table("unidad", "U"), "EU.UnidadId = U.Id", "inner"),
                            new Join(new DT.Table("responsable_unidad", "R"), "EU.ResponsableId = R.Id", "inner"),
                            new Join(new DT.Table("Corporacion", "C"), "C.Id = U.CorporacionId", "left")
                        },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var returned = dt.Generar(query, draw, start, length, search, order, columns);
                        return returned;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }

                finally
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static List<Combo> getResponsable(object[] datos)
        {
            List<Combo> combo = new List<Combo>();

            var responsable = ControlResponsable.ObtenerPorUnidadIdEventoId(datos);

            if (responsable != null)
            {
                combo.Add(new Combo { Desc = responsable.Nombre, Id = responsable.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static Object getEventById(string id)
        {
            try
            {
                var evento = ControlEvento.ObtenerById(Convert.ToInt32(id));
                var colonia = ControlColonia.ObtenerPorId(evento.ColoniaId);
                var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                var estado = ControlEstado.Obtener(municipio.EstadoId);
                var pais = ControlCatalogo.Obtener(estado.IdPais, 45);

                object obj = new
                {
                    evento.TrackingId,
                    evento.Descripcion,
                    evento.Lugar,
                    evento.Folio,
                    HoraYFecha = evento.HoraYFecha.ToString("dd/MM/yyyy HH:mm:ss"),
                    PaisId = pais.Id,
                    EstadoId = estado.Id,
                    MunicipioId = municipio.Id,
                    ColoniaId = colonia.Id,
                    CodigoPostal = colonia.CodigoPostal,
                    EventoIdAW = evento.IdEventoWS
                };

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", obj });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }            
        }

        [WebMethod]
        public static Object save(InfoDetencionAux datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    var infoDetencion = new Entity.InformacionDeDetencion();
                    var detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(datos.TrackingIdDetenido));
                    string[] datadetenido = new string[]
                    {
                        detenido.Id.ToString(),
                        true.ToString()
                    };
                    var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datadetenido);
                    if(detalleDetencion == null)
                        return new { exitoso = false, mensaje = "No puede modificar la información del detenido" };

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                                       
                    string id = datos.Id;
                    if (id == "") id = "0";                                      

                    if (string.IsNullOrEmpty(datos.Id))
                    {
                        mode = @"registro";

                        infoDetencion.Activo = true;
                        infoDetencion.IdInterno = detenido.Id;
                        infoDetencion.CreadoPor = usId;
                        infoDetencion.Descripcion = datos.Descripcion;
                        infoDetencion.Folio = datos.Folio;
                        infoDetencion.Habilitado = true;
                        infoDetencion.HoraYFecha = DateTime.Now; //Convert.ToDateTime(datos.HoraYFecha);
                        infoDetencion.ColoniaId = Convert.ToInt32(datos.ColoniaId);
                        infoDetencion.DetalleDetencionId = detalleDetencion.Id;
                        infoDetencion.ResponsableId = Convert.ToInt32(datos.ResponsableId);
                        infoDetencion.UnidadId = Convert.ToInt32(datos.UnidadId);
                        infoDetencion.LugarDetencion = datos.Lugar;
                        infoDetencion.Motivo = datos.Motivo;
                        infoDetencion.TrackingId = Guid.NewGuid();
                        infoDetencion.IdEvento = Convert.ToInt32(datos.EventoId);
                        infoDetencion.Personanotifica = Convert.ToString(datos.Personanotifica);
                        infoDetencion.Celular = Convert.ToString(datos.Celular);

                        infoDetencion.Id = ControlInformacionDeDetencion.Guardar(infoDetencion);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = detalleDetencion.Id;
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Movimiento = "Modificación de datos generales en informe de detención";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {
                        mode = @"actualizo";

                        //var descripcionAnterior = ControlInformacionDeDetencion.ObtenerPorId(Convert.ToInt32(datos.Id));
                        //string motivoAnterior = descripcionAnterior != null ? descripcionAnterior.Motivo : "";
                        //string motivoSinRepetir = motivoAnterior != "" ? datos.Motivo.Replace(motivoAnterior, "") : datos.Motivo;                        

                        infoDetencion.Id = Convert.ToInt32(datos.Id);
                        infoDetencion.TrackingId = new Guid(datos.TrackingInfo);
                        infoDetencion.Activo = true;
                        infoDetencion.CreadoPor = usId;
                        infoDetencion.Descripcion = datos.Descripcion;
                        infoDetencion.Folio = datos.Folio;
                        infoDetencion.Habilitado = true;
                        infoDetencion.HoraYFecha = Convert.ToDateTime(datos.HoraYFecha);
                        infoDetencion.ColoniaId = Convert.ToInt32(datos.ColoniaId);
                        infoDetencion.DetalleDetencionId = detalleDetencion.Id;
                        infoDetencion.ResponsableId = Convert.ToInt32(datos.ResponsableId);
                        infoDetencion.UnidadId = Convert.ToInt32(datos.UnidadId);
                        infoDetencion.LugarDetencion = datos.Lugar;
                        //infoDetencion.Motivo = motivoAnterior + "/" + motivoSinRepetir;
                        infoDetencion.Motivo = datos.Motivo;
                        infoDetencion.IdEvento = Convert.ToInt32(datos.EventoId);
                        infoDetencion.Personanotifica = Convert.ToString(datos.Personanotifica);
                        infoDetencion.Celular = Convert.ToString(datos.Celular).Replace("(","").Replace(")","").Replace("-","").Replace(" ","");
                        ControlInformacionDeDetencion.Actualizar(infoDetencion);

                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = detalleDetencion.Id;
                        historial.Movimiento = "Modificación de datos generales en informe  de detención";
                        historial.TrackingId = Guid.NewGuid();
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);

                    }

                    return new { exitoso = true, mensaje = mode, Id = infoDetencion.Id.ToString(), TrackingId = infoDetencion.TrackingId, Motivo = infoDetencion.Motivo };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            return edad;
        }

        [WebMethod]
        public static List<Combo> getcorporacion(string unidad)
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

            var unidadentity = ControlUnidad.ObtenerById(Convert.ToInt32(unidad));
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.corporacion));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo.Where(x => x.Id == unidadentity.CorporacionId))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {

                //(trackingid.Trim());
                var detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                if (detencion == null)
                {
                    var detenido_ = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    //detencion = ControlDetalleDetencion.ObtenerPorDetenidoId(detenido_.Id).LastOrDefault();
                }

                Entity.Detenido interno = ControlDetenido.ObtenerPorId(detencion.DetenidoId);
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = new Entity.DetalleDetencion();
                detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                if (detalleDetencion == null) detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                try
                {
                    infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                }
                catch (Exception ex)
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
                }
                var colonia = (infoDetencion != null) ? ControlColonia.ObtenerPorId(infoDetencion.ColoniaId) : null;
                var municipio = (infoDetencion != null) ? ControlMunicipio.Obtener(colonia!=null?colonia.IdMunicipio:0) : null;
                var estado = (infoDetencion != null) ? ControlEstado.Obtener(municipio!=null? municipio.EstadoId:0) : null;
                var pais = (infoDetencion != null) ? ControlCatalogo.Obtener(estado!=null?estado.IdPais:0, 45) : null;
                var evento = (infoDetencion != null) ? ControlEvento.ObtenerById(infoDetencion.IdEvento) : null;
                var antropometria = (infoDetencion != null) ? ControlAntropometria.ObtenerPorDetenidoId(interno.Id) : null;

                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);

                Entity.Domicilio nacimiento = ControlDomicilio.ObtenerPorId(interno.DomicilioNId);
                Entity.Catalogo sexo = new Entity.Catalogo();
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                int edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);
                    else if (general.Edaddetenido != 0)
                        edad = general.Edaddetenido;
                    else
                    {
                        var infDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                        var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infDetencion.IdDetenido_Evento);

                        if (detenidoEvento != null)
                        {
                            edad = detenidoEvento.Edad;
                        }
                    }
                    sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                }

                // f1x3d
                //var llamada = ControlLlamada.ObtenerById(evento.LlamadaId);
                var llamada = new Entity.Llamada();
                var unidad = ControlCatalogo.Obtener(infoDetencion.UnidadId, 82);
                var responsable = ControlCatalogo.Obtener(infoDetencion.ResponsableId, 83);

                var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                Entity.Municipio municipio2 = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                Entity.Colonia colonia2 = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;
                object obj = null;

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = evento.HoraYFecha;

                if (calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                }
                else
                {
                    //fechaSalidaAdd = fechaSalida.AddHours(36);
                    var hist = ControlHistorial.ObtenerPorDetenido(detalleDetencion.Id).Where(x=>x.Movimiento.ToLower().Contains("salida") || x.Movimiento.ToLower().Contains("traslado"));
                    fechaSalidaAdd = DateTime.MinValue;
                    foreach (var item in hist)
                    {
                        fechaSalidaAdd = item.Fecha;
                    }
                    
                }
                if (interno != null)
                {
                    if (string.IsNullOrEmpty(interno.RutaImagen))
                    {
                        interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                    }
                }
                var informacion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                var evento2 = ControlEvento.ObtenerById(informacion.IdEvento);
                var eventounidadresponsable = ControlEventoUnidadResponsable.ObtenerByEventoId(evento2.Id);
                var unidadprueba = "";
                var responsableprueba = "";
                var unidad2 = new Entity.Unidad();
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);
                var m = "";
                if (detenidoevento!=null)
                {
                    var motivo = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);

                    if (motivo != null)
                    {
                        m = motivo.NombreMotivoLlamada;
                    }
                }
                
                
                if (eventounidadresponsable != null)
                {
                    unidad2 = ControlUnidad.ObtenerById(eventounidadresponsable.UnidadId);
                    var responsable2 = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { unidad2.Id, evento.Id });
                    unidadprueba = unidad2.Nombre;
                    responsableprueba = responsable2 !=null ? responsable2.Descripcion+"-"+responsable2.Nombre :"";
                }

                var Eur = ControlEventoUnidadResponsable.ObtenerTodosByEventoId(evento.Id);
                var unidades = "";
                var l = 1;
                var responsables = "";
                var m1 = 1;
                foreach (var item in Eur)
                {
                    var uni = ControlUnidad.ObtenerById(item.UnidadId);
                    if (l == 1)
                    {
                        unidades = uni.Nombre;
                    }
                    else
                    {
                        unidades = unidades + ", " + uni.Nombre;
                    }
                    l++;
                    var responsablel = ControlCatalogo.Obtener(item.ResponsableId, Convert.ToInt32(Entity.TipoDeCatalogo.responsable_unidad));
                    if (m1 == 1)
                    {
                        responsables =responsablel.Descripcion+"-"+ responsablel.Nombre;
                    }
                    else
                    {
                        responsables = responsables + ", " + responsablel.Descripcion + "-" + responsablel.Nombre;
                    }
                    m1++;
                }
                obj = new
                {
                    Id = interno != null ? interno.Id.ToString() : "",
                    TrackingId = interno != null ? interno.TrackingId.ToString() : "",
                    Expediente = (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                    Centro = institucion != null ? institucion.Nombre : "Centro no registrado",
                    Fecha = detalleDetencion != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd-MM-yyyy HH:mm:ss") : "",
                    Nombre = (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                    RutaImagen = (interno != null) ? interno.RutaImagen != null ? interno.RutaImagen : "" : "",
                    UnidadId = infoDetencion != null ? infoDetencion.UnidadId : 0,
                    ResponsableId = infoDetencion != null ? infoDetencion.ResponsableId : 0,
                    Motivo = m,
                    Descripcion = (infoDetencion != null) ? infoDetencion.Motivo : "",
                    Folio = (infoDetencion != null) ? infoDetencion.Folio : "",
                    Lugar = (infoDetencion != null) ? infoDetencion.LugarDetencion : "",
                    FechaDetencion = (detalleDetencion != null) ? ((DateTime)detalleDetencion.Fecha).ToString("dd-MM-yyyy HH:mm:ss") : "",
                    PaisId = (pais != null) ? pais.Id : 0,
                    EstadoId = (estado != null) ? estado.Id : 0,
                    MunicipioId = (municipio != null) ? municipio.Id : 0,
                    ColoniaId = (colonia != null) ? colonia.Id : 0,
                    CodigoPostal = (colonia != null) ? colonia.CodigoPostal : "",
                    IdInfo = (infoDetencion != null) ? infoDetencion.Id.ToString() : "",
                    TrackingInfo = (infoDetencion != null) ? infoDetencion.TrackingId.ToString() : "",
                    LlamadaId = (evento != null) ? evento.LlamadaId : 0,
                    EventoId = (evento != null) ? evento.Id : 0,
                    Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                    Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                    municipioNombre = municipio2 != null ? municipio2.Nombre : "Municipio no registrado",
                    domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                    coloniaNombre = colonia2 != null ? colonia2.Asentamiento : "Colonia no registrada",
                    LlamadaNombre = llamada != null ? llamada.Descripcion : "",
                    EventoNombre = (evento != null) ? evento.Descripcion : "",
                    UnidadNombre = (unidad != null) ? unidad.Nombre : "",
                    ResponsableNombre = (responsable != null) ? responsable.Descripcion+"-"+responsable.Nombre : "",
                    PaisNombre = (pais != null) ? pais.Nombre : "",
                    EstadoNombre = (estado != null) ? estado.Nombre : "",
                    MunicipioNombre = (municipio != null) ? municipio.Nombre : "",
                    ColoniaNombre = (colonia != null) ? colonia.Asentamiento : "",
                    Estatura = (antropometria != null) ? antropometria.Estatura.ToString() : "",
                    Peso = (antropometria != null) ? antropometria.Peso.ToString() : "",
                    FechaSalida = fechaSalidaAdd != DateTime.MinValue ? fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    Personanotifica = infoDetencion.Personanotifica != null ? infoDetencion.Personanotifica : "",
                    Celular = infoDetencion.Celular != null ? infoDetencion.Celular : "",
                    Lugares = informacion.LugarDetencion,
                    Unidad1 = unidades,
                    Responsable1 = responsables,
                    Motivo_evento = evento2 != null ? evento2.Descripcion : ""

                };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                //}
                //else
                //{
                //    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                //}
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getEvento(string detenidoId)
        {
            List<object> lista = new List<object>();
            Entity.Evento evento = new Entity.Evento();
            Entity.Detenido detenido = new Entity.Detenido();
            Entity.InformacionDeDetencion informacionDetencion = new Entity.InformacionDeDetencion();
            Entity.Colonia colonia = new Entity.Colonia();

            detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(detenidoId));

            if (detenido != null)
            {
                informacionDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);

            }

            if (informacionDetencion != null)
            {
                evento = ControlEvento.ObtenerById(informacionDetencion.IdEvento);
            }

            if (evento != null)
            {
                string localizacion = string.Empty;

                colonia = ControlColonia.ObtenerPorId(evento.ColoniaId);

                localizacion = evento.Lugar;

                if (colonia != null)
                {
                    localizacion += " " + colonia.Municipio + " " + colonia.Estado + " C.P. " + colonia.CodigoPostal;
                }

                object obj = new
                {
                    Descripcion = evento.Descripcion != "" ? evento.Descripcion : "Sin descripción",
                    FolioEvento = evento.Folio,
                    FechaHora = evento.HoraYFecha,
                    Localizacion = localizacion != "" ? localizacion : "Sin lugar de detención"
                };

                lista.Add(obj);
            }

            return JsonConvert.SerializeObject(new { lista = lista });
        }

        [WebMethod]
        public static string getLugarEvento(string detenidoId)
        {
            List<object> lista = new List<object>();
            Entity.Evento evento = new Entity.Evento();
            Entity.Detenido detenido = new Entity.Detenido();
            Entity.InformacionDeDetencion informacionDetencion = new Entity.InformacionDeDetencion();
            Entity.Colonia colonia = new Entity.Colonia();

            detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(detenidoId));

            if (detenido != null)
            {
                informacionDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);

            }

            if (informacionDetencion != null)
            {
                evento = ControlEvento.ObtenerById(informacionDetencion.IdEvento);
            }

            if (evento != null)
            {
                object obj = new
                {
                    NumeroDetenidos = evento.NumeroDetenidos != 0 ? evento.NumeroDetenidos.ToString() : "Sin número de detenidos",
                    Latitud = evento.Latitud,
                    Longitud = evento.Longitud,
                };

                lista.Add(obj);
            }

            return JsonConvert.SerializeObject(new { lista = lista });
        }

        [WebMethod]
        public static string getEventoAW(string detenidoId)
        {
            List<object> lista = new List<object>();
            Entity.Evento evento = new Entity.Evento();
            Entity.Detenido detenido = new Entity.Detenido();
            Entity.InformacionDeDetencion informacionDetencion = new Entity.InformacionDeDetencion();
            Entity.WSAEvento eventoAW = new Entity.WSAEvento();
            Entity.WSALugar lugarEventoAW = new Entity.WSALugar();
            Entity.WSAColonia coloniaAW = new Entity.WSAColonia();
            string localizacion = string.Empty;

            detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(detenidoId));

            if (detenido != null)
            {
                informacionDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);

            }

            if (informacionDetencion != null)
            {
                evento = ControlEvento.ObtenerById(informacionDetencion.IdEvento);
            }

            if (evento != null)
            {
                eventoAW = ControlWSAEvento.ObtenerPorId(evento.IdEventoWS);

                if (eventoAW != null)
                {
                    lugarEventoAW = ControlWSALugar.ObtenerPorId(eventoAW.IdLugar);

                    if (lugarEventoAW != null)
                    {
                        coloniaAW = ControlWSAColonia.ObtenerPorId(lugarEventoAW.IdColonia);

                        if (coloniaAW != null)
                        {
                            Entity.WSAMunicipio municipioWSA = new Entity.WSAMunicipio();
                            Entity.WSAEstado estadoWSA = new Entity.WSAEstado();

                            municipioWSA = ControlWSAMunicipio.ObtenerPorId(coloniaAW.IdMunicipio);
                            estadoWSA = ControlWSAEstado.ObtenerPorId(lugarEventoAW.IdEstado);

                            localizacion = coloniaAW.NombreColonia + " " + lugarEventoAW.Numero + " " + municipioWSA.NombreMunicipio + " " + estadoWSA.NombreEstado;
                        }
                    }


                }

                object obj = new
                {
                    FolioEvento = eventoAW != null ? eventoAW.Folio : "",
                    Descripcion = eventoAW != null ? eventoAW.Descripcion : "",
                    Fecha = eventoAW != null ? eventoAW.FechaEvento.ToShortDateString() : "",
                    Lugar = localizacion != "" ? localizacion : "",
                    NombreResponsable = eventoAW != null ? eventoAW.NombreResponsable : "",
                    NumeroDetenidos = eventoAW != null ? eventoAW.NumeroDetenidos.ToString() : ""
                };

                lista.Add(obj);
            }

            return JsonConvert.SerializeObject(new { lista = lista });
        }

        [WebMethod]
        public static string getLugarEventoAW(string detenidoId)
        {
            List<object> lista = new List<object>();
            Entity.Evento evento = new Entity.Evento();
            Entity.Detenido detenido = new Entity.Detenido();
            Entity.InformacionDeDetencion informacionDetencion = new Entity.InformacionDeDetencion();
            Entity.WSAEvento eventoAW = new Entity.WSAEvento();
            Entity.WSALugar lugarEventoAW = new Entity.WSALugar();
            Entity.WSAColonia coloniaAW = new Entity.WSAColonia();
            string entreCalle = string.Empty;

            detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(detenidoId));

            if (detenido != null)
            {
                informacionDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);

            }

            if (informacionDetencion != null)
            {
                evento = ControlEvento.ObtenerById(informacionDetencion.IdEvento);
            }

            if (evento != null)
            {
                eventoAW = ControlWSAEvento.ObtenerPorId(evento.IdEventoWS);

                if (eventoAW != null)
                {
                    lugarEventoAW = ControlWSALugar.ObtenerPorId(eventoAW.IdLugar);

                    if (lugarEventoAW != null)
                    {
                        entreCalle = lugarEventoAW.EntreCalle != null ? lugarEventoAW.EntreCalle + lugarEventoAW.Ycalle != null ? " y " + lugarEventoAW.Ycalle : "" : "";
                    }
                }

                object obj = new
                {
                    Sector = lugarEventoAW.Sector != null ? lugarEventoAW.Sector : "",
                    EntreCalle = entreCalle,
                    Latitud = lugarEventoAW.Latitud != null ? lugarEventoAW.Latitud : "",
                    Longitud = lugarEventoAW.Longitud != null ? lugarEventoAW.Longitud : ""
                };
                lista.Add(obj);
            }

            return JsonConvert.SerializeObject(new { lista = lista });
        }

    }

    public class InfoDetencionAux
    {
        public string TrackingIdDetenido { get; set; }
        public string UnidadId { get; set; }
        public string ResponsableId { get; set; }
        public string Motivo { get; set; }
        public string Descripcion { get; set; }
        public string Lugar { get; set; }
        public string Folio { get; set; }
        public string HoraYFecha { get; set; }
        public string ColoniaId { get; set; }
        public string Id { get; set; }
        public string TrackingInfo { get; set; }
        public string EventoId { get; set; }
        public string Personanotifica { get; set; }
        public string Celular { get; set; }
    }
}