﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Security;
using System.Configuration;
using System.Linq;

namespace Web.Application.Registry
{
    public partial class anthropometry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor == "")
                Response.Redirect("entrylist.aspx");

            if (Request.QueryString["editable"] == "1")
            {
                editable.Value = "1";
            }
            else
            {
                editable.Value = "0";
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();


                        //Acceso a ingreso
                        parametros[1] = "Registro en barandilla";
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Ingreso.Value = permisos.Consultar.ToString().ToLower();
                        }



                        //Acceso a Informacion_Personal
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Informacion_Personal.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Adicciones
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Adicciones.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Señas_Particulares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Señas_Particulares.Value = permisos.Consultar.ToString().ToLower();
                        }



                        //Acceso a Familiares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Familiares.Value = permisos.Consultar.ToString().ToLower();
                        }


                        //Acceso a Estudios_Criminologicos
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Estudios_Criminologicos.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Expediente_Medico
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Expediente_Medico.Value = permisos.Consultar.ToString().ToLower();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static List<Combo> getAdherencia()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.adherencia));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getTamano()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.tamaño));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static string saveGeneral(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[1]));
                    string[] datos2 = { interno.Id.ToString(), true.ToString() };
                    var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos2);
                    if(detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    Entity.Antropometria antropometria = new Entity.Antropometria();

                    antropometria.DetenidoId = interno.Id;
                    antropometria.Estatura = Convert.ToDecimal(datos[2].ToString());
                    antropometria.Peso = Convert.ToDecimal(datos[3].ToString());


                    if (Convert.ToInt32(antropometria.Estatura) <= 0 && Convert.ToInt32(antropometria.Peso) <= 0)
                    {
                        throw new Exception("El valor del peso y estatura debe ser mayor a cero.");
                    }
                    else
                    {
                        if (!(Convert.ToInt32(antropometria.Peso) > 0))
                            throw new Exception("El valor del peso debe ser mayor a cero.");
                        if (!(Convert.ToInt32(antropometria.Estatura) > 0))
                            throw new Exception("El valor de la estatura debe ser mayor a cero.");
                    }
                    
                    //antropometria.FormulaI = datos[4];
                    //antropometria.SubformulaI = datos[5];
                    //antropometria.FormulaD = datos[6];
                    //antropometria.SubformulaD = datos[7];
                    //antropometria.Vucetich = datos[8];
                    antropometria.FormulaI = "";
                    antropometria.SubformulaI = "";
                    antropometria.FormulaD = "";
                    antropometria.SubformulaD = "";
                    antropometria.Vucetich = "";
                    antropometria.Dactiloscopica = datos[4];
                    antropometria.Lunares = Convert.ToBoolean(datos[5]);
                    antropometria.Tatuaje = Convert.ToBoolean(datos[6]);
                    antropometria.Defecto = Convert.ToBoolean(datos[7]);
                    antropometria.Cicatris = Convert.ToBoolean(datos[8]);
                    antropometria.Anteojos = Convert.ToBoolean(datos[9]);


                    if (!string.IsNullOrEmpty(datos[10].ToString()))
                    {
                        mode = "actualizó";

                        antropometria.Id = Convert.ToInt32(datos[10]);
                        antropometria.TrackingId = new Guid(datos[11]);


                        ControlAntropometria.Actualizar(antropometria);

                        if (antropometria.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Modificación de datos de antropometría en barandilla";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        mode = "registró";


                        antropometria.TrackingId = Guid.NewGuid();
                        antropometria.Id = ControlAntropometria.Guardar(antropometria);

                        if (antropometria.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Registro de datos de antropometría en barandilla";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = interno.Id, TrackingId = interno.TrackingId, IdA = antropometria.Id, TrackingIdA = antropometria.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Modificar)
                {
                    var mode = string.Empty;

                    if (!string.IsNullOrEmpty(datos[1].ToString()))
                    {
                        Entity.Antropometria antropometria = ControlAntropometria.ObtenerPorTrackingId(new Guid(datos[1]));
                        string[] datos2 = { antropometria.DetenidoId.ToString(), true.ToString() };
                        var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos2);
                        if (detalleDetencion == null)
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                        Entity.GeneralCabello generalCabello = new Entity.GeneralCabello();
                        Entity.FrenteCeja frenteCeja = new Entity.FrenteCeja();
                        Entity.OjoNariz ojoNariz = new Entity.OjoNariz();
                        Entity.BocaLabioMenton bocaLabioMenton = new Entity.BocaLabioMenton();
                        Entity.OrejaHelix orejaHelix = new Entity.OrejaHelix();
                        Entity.LobuloFactorRH lobuloFactorRH = new Entity.LobuloFactorRH();
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        generalCabello.ComplexionG = Convert.ToInt32(datos[2]);
                        generalCabello.ColorPielG = Convert.ToInt32(datos[3]);
                        generalCabello.CaraG = Convert.ToInt32(datos[4]);
                        generalCabello.CantidadC = Convert.ToInt32(datos[5]);
                        generalCabello.ColorC = Convert.ToInt32(datos[6]);
                        generalCabello.FormaC = Convert.ToInt32(datos[7]);
                        generalCabello.CalvicieC = Convert.ToInt32(datos[8]);
                        generalCabello.ImplementacionC = Convert.ToInt32(datos[9]);


                        if (Convert.ToInt32(datos[10]) != 0)
                            frenteCeja.AlturaF = Convert.ToInt32(datos[10]);
                        else
                            frenteCeja.AlturaF = null;

                        if (Convert.ToInt32(datos[11]) != 0)
                            frenteCeja.AnchoF = Convert.ToInt32(datos[11]);
                        else
                            frenteCeja.AnchoF = null;

                        frenteCeja.InclinacionF = Convert.ToInt32(datos[12]);
                        frenteCeja.DireccionC = Convert.ToInt32(datos[13]);
                        frenteCeja.ImplantacionC = Convert.ToInt32(datos[14]);
                        frenteCeja.FormaC = Convert.ToInt32(datos[15]);
                        frenteCeja.TamanoC = Convert.ToInt32(datos[16]);


                        ojoNariz.ColorO = Convert.ToInt32(datos[17]);
                        ojoNariz.FormaO = Convert.ToInt32(datos[18]);

                        if (Convert.ToInt32(datos[19]) != 0)
                            ojoNariz.TamanoO = Convert.ToInt32(datos[19]);
                        else
                            ojoNariz.TamanoO = null;

                        if (Convert.ToInt32(datos[20]) != 0)
                            ojoNariz.RaizN = Convert.ToInt32(datos[20]);
                        else
                            ojoNariz.RaizN = null;
                        if (Convert.ToInt32(datos[21]) != 0)
                            ojoNariz.AlturaN = Convert.ToInt32(datos[21]);
                        else
                            ojoNariz.AlturaN = null;
                        if (Convert.ToInt32(datos[22]) != 0)
                            ojoNariz.AnchoN = Convert.ToInt32(datos[22]);
                        else
                            ojoNariz.AnchoN = null;

                        ojoNariz.DorsoN = Convert.ToInt32(datos[23]);
                        ojoNariz.BaseN = Convert.ToInt32(datos[24]);

                        if (Convert.ToInt32(datos[25]) != 0)
                            bocaLabioMenton.TamanoB = Convert.ToInt32(datos[25]);
                        else
                            bocaLabioMenton.TamanoB = null;

                        bocaLabioMenton.ComisuraB = Convert.ToInt32(datos[26]);
                        if (Convert.ToInt32(datos[27]) != 0)
                            bocaLabioMenton.AlturaL = Convert.ToInt32(datos[27]);
                        else
                            bocaLabioMenton.AlturaL = null;

                        bocaLabioMenton.EspesorL = Convert.ToInt32(datos[28]);
                        bocaLabioMenton.ProminenciaL = Convert.ToInt32(datos[29]);
                        bocaLabioMenton.TipoM = Convert.ToInt32(datos[30]);
                        bocaLabioMenton.FormaM = Convert.ToInt32(datos[31]);
                        bocaLabioMenton.InclinacionM = Convert.ToInt32(datos[32]);

                        if (Convert.ToInt32(datos[33]) != 0)
                            orejaHelix.OriginalO = Convert.ToInt32(datos[33]);
                        else
                            orejaHelix.OriginalO = null;

                        orejaHelix.FormaO = Convert.ToInt32(datos[34]);

                        if (Convert.ToInt32(datos[35]) != 0)
                            orejaHelix.SuperiorH = Convert.ToInt32(datos[35]);
                        else
                            orejaHelix.SuperiorH = null;
                        if (Convert.ToInt32(datos[36]) != 0)
                            orejaHelix.PosteriorH = Convert.ToInt32(datos[36]);
                        else
                            orejaHelix.PosteriorH = null;

                        if (Convert.ToInt32(datos[37]) != 0)
                            orejaHelix.AdherenciaH = Convert.ToInt32(datos[37]);
                        else
                            orejaHelix.AdherenciaH = null;

                        orejaHelix.ContornoH = Convert.ToInt32(datos[38]);

                        if (Convert.ToInt32(datos[39]) != 0)
                            lobuloFactorRH.AdherenciaL = Convert.ToInt32(datos[39]);
                        else
                            lobuloFactorRH.AdherenciaL = null;

                        if (Convert.ToInt32(datos[40]) != 0)
                            lobuloFactorRH.DimensionL = Convert.ToInt32(datos[40]);
                        else
                            lobuloFactorRH.DimensionL = null;
                        lobuloFactorRH.ParticularidadL = Convert.ToInt32(datos[41]);
                        lobuloFactorRH.TragoL = Convert.ToInt32(datos[42]);
                        lobuloFactorRH.AntitragoL = Convert.ToInt32(datos[43]);
                        lobuloFactorRH.SangreF = Convert.ToInt32(datos[44]);
                        lobuloFactorRH.TipoF = Convert.ToInt32(datos[45]);


                        if (!string.IsNullOrEmpty(datos[46].ToString()))
                        {
                            mode = "actualizó";


                            Entity.GeneralCabello recuperargeneralCabello = ControlGeneralCabello.ObtenerPorAntropometriaId(antropometria.Id);
                            Entity.FrenteCeja recuperarfrenteCeja = ControlFrenteCeja.ObtenerPorAntropometriaId(antropometria.Id);
                            Entity.OjoNariz recuperarojoNariz = ControlOjoNariz.ObtenerPorAntropometriaId(antropometria.Id);
                            Entity.BocaLabioMenton recuperarbocaLabioMenton = ControlBocaLabioMenton.ObtenerPorAntropometriaId(antropometria.Id);
                            Entity.OrejaHelix recuperarorejaHelix = ControlOrejaHelix.ObtenerPorAntropometriaId(antropometria.Id);
                            Entity.LobuloFactorRH recuperarlobuloFactorRH = ControlLobuloFactorRH.ObtenerPorAntropometriaId(antropometria.Id);

                            generalCabello.AntropometriaId = antropometria.Id;
                            generalCabello.Id = recuperargeneralCabello.Id;
                            generalCabello.TrackingId = recuperargeneralCabello.TrackingId;

                            frenteCeja.AntropometriaId = antropometria.Id;
                            frenteCeja.Id = recuperarfrenteCeja.Id;
                            frenteCeja.TrackingId = recuperarfrenteCeja.TrackingId;

                            ojoNariz.AntropometriaId = antropometria.Id;
                            ojoNariz.Id = recuperarojoNariz.Id;
                            ojoNariz.TrackingId = recuperarojoNariz.TrackingId;

                            bocaLabioMenton.AntroprometriaId = antropometria.Id;
                            bocaLabioMenton.Id = recuperarbocaLabioMenton.Id;
                            bocaLabioMenton.TrackingId = recuperarbocaLabioMenton.TrackingId;

                            orejaHelix.AntropometriaId = antropometria.Id;
                            orejaHelix.Id = recuperarorejaHelix.Id;
                            orejaHelix.TrackingId = recuperarorejaHelix.TrackingId;

                            lobuloFactorRH.AntropometriaId = antropometria.Id;
                            lobuloFactorRH.Id = recuperarlobuloFactorRH.Id;
                            lobuloFactorRH.TrackingId = recuperarlobuloFactorRH.TrackingId;

                            ControlGeneralCabello.Actualizar(generalCabello);
                            ControlFrenteCeja.Actualizar(frenteCeja);
                            ControlOjoNariz.Actualizar(ojoNariz);
                            ControlBocaLabioMenton.Actualizar(bocaLabioMenton);
                            ControlOrejaHelix.Actualizar(orejaHelix);
                            ControlLobuloFactorRH.Actualizar(lobuloFactorRH);

                            if (antropometria.Id > 0)
                            {
                                Entity.Historial historial = new Entity.Historial();
                                historial.Activo = true;
                                historial.CreadoPor = usId;
                                historial.Fecha = DateTime.Now;
                                historial.Habilitado = true;
                                historial.InternoId = detalleDetencion.Id;
                                historial.Movimiento = "Modificación de rasgos generales en barandilla";
                                historial.TrackingId = detalleDetencion.TrackingId;
                                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                                historial.ContratoId = subcontrato.Id;
                                historial.Id = ControlHistorial.Guardar(historial);
                            }
                        }
                        else
                        {
                            mode = "registró";

                            generalCabello.AntropometriaId = antropometria.Id;
                            generalCabello.TrackingId = Guid.NewGuid();
                            generalCabello.Id = ControlGeneralCabello.Guardar(generalCabello);

                            frenteCeja.AntropometriaId = antropometria.Id;
                            frenteCeja.TrackingId = Guid.NewGuid();

                            frenteCeja.Id = ControlFrenteCeja.Guardar(frenteCeja);

                            ojoNariz.AntropometriaId = antropometria.Id;
                            ojoNariz.TrackingId = Guid.NewGuid();
                            ojoNariz.Id = ControlOjoNariz.Guardar(ojoNariz);

                            bocaLabioMenton.AntroprometriaId = antropometria.Id;
                            bocaLabioMenton.TrackingId = Guid.NewGuid();
                            bocaLabioMenton.Id = ControlBocaLabioMenton.Guardar(bocaLabioMenton);

                            orejaHelix.AntropometriaId = antropometria.Id;
                            orejaHelix.TrackingId = Guid.NewGuid();
                            orejaHelix.Id = ControlOrejaHelix.Guardar(orejaHelix);

                            lobuloFactorRH.AntropometriaId = antropometria.Id;
                            lobuloFactorRH.TrackingId = Guid.NewGuid();
                            lobuloFactorRH.Id = ControlLobuloFactorRH.Guardar(lobuloFactorRH);

                            if (antropometria.Id > 0)
                            {
                                Entity.Historial historial = new Entity.Historial();
                                historial.Activo = true;
                                historial.CreadoPor = usId;
                                historial.Fecha = DateTime.Now;
                                historial.Habilitado = true;
                                historial.InternoId = detalleDetencion.Id;
                                historial.Movimiento = "Registro de rasgos generales en barandilla";
                                historial.TrackingId = detalleDetencion.TrackingId;
                                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                                historial.ContratoId = subcontrato.Id;
                                historial.Id = ControlHistorial.Guardar(historial);
                            }
                        }

                        return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, IdA = antropometria.Id, TrackingIdA = antropometria.TrackingId, Id = generalCabello.Id, TrackingId = generalCabello.TrackingId, });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "Primero debe de guardarse la sección de Antropometría." });
                    }
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            return edad;
        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {
                int value_dtn = Convert.ToInt32(trackingid.Trim());
                Entity.Detenido interno = ControlDetenido.ObtenerPorId(value_dtn);
                if (interno == null)
                {
                    //var detalle_det = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                    //interno = ControlDetenido.ObtenerPorId(detalle_det.DetenidoId)
                }
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalle_det = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                if (detalle_det == null) detalle_det = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalle_det.CentroId);
                Entity.Antropometria antropometria = ControlAntropometria.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Catalogo sexo = new Entity.Catalogo();
                int edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);
                    else if (general.Edaddetenido != 0)
                        edad = general.Edaddetenido;
                    else
                    {
                        var infDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                        var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infDetencion.IdDetenido_Evento);

                        if (detenidoEvento != null)
                        {
                            edad = detenidoEvento.Edad;
                        }
                    }
                    sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                }
                if (interno != null)
                {
                    if (string.IsNullOrEmpty(interno.RutaImagen))
                    {
                        interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                    }
                }
                var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                bool tienedatos = false;
                bool tienedatosA = false;

                object objA = null;
                object obj = null;

                if (antropometria != null)
                {

                    objA = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = detalle_det.Expediente,
                        Centro = institucion != null ? institucion.Nombre : "Institución no registrada",
                        Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                        RutaImagen = interno.RutaImagen,
                        estaturam = antropometria.Estatura.ToString(),
                        peso = antropometria.Peso.ToString(),
                        formulai = antropometria.FormulaI,
                        subformulai = antropometria.SubformulaI,
                        formulad = antropometria.FormulaD,
                        subformulad = antropometria.SubformulaD,
                        vucetich = antropometria.Vucetich,
                        dactiloscopica = antropometria.Dactiloscopica,
                        lunar = antropometria.Lunares,
                        tatuaje = antropometria.Tatuaje,
                        defecto = antropometria.Defecto,
                        cicatris = antropometria.Cicatris,
                        anteojos = antropometria.Anteojos,
                        IdA = antropometria.Id,
                        TrackingA = antropometria.TrackingId,
                        edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento:"Colonia no registrada"
                    };

                    tienedatosA = true;

                    Entity.GeneralCabello generalCabello = ControlGeneralCabello.ObtenerPorAntropometriaId(antropometria.Id);
                    Entity.FrenteCeja frenteCeja = ControlFrenteCeja.ObtenerPorAntropometriaId(antropometria.Id);
                    Entity.OjoNariz ojoNariz = ControlOjoNariz.ObtenerPorAntropometriaId(antropometria.Id);
                    Entity.BocaLabioMenton bocaLabioMenton = ControlBocaLabioMenton.ObtenerPorAntropometriaId(antropometria.Id);
                    Entity.OrejaHelix orejaHelix = ControlOrejaHelix.ObtenerPorAntropometriaId(antropometria.Id);
                    Entity.LobuloFactorRH lobuloFactorRH = ControlLobuloFactorRH.ObtenerPorAntropometriaId(antropometria.Id);

                    if (generalCabello != null)
                    {
                        obj = new
                        {
                            Id = generalCabello.Id,
                            Tracking = generalCabello.TrackingId,
                            radiocomplexion = generalCabello.ComplexionG,
                            colorpiel = generalCabello.ColorPielG,
                            cara = generalCabello.CaraG,
                            cabellocantidad = generalCabello.CantidadC,
                            piel = generalCabello.ColorC,
                            formacabello = generalCabello.FormaC,
                            calvicie = generalCabello.CalvicieC,
                            implementacioncabello = generalCabello.ImplementacionC,
                            frente = frenteCeja.InclinacionF,
                            direccion = frenteCeja.DireccionC,
                            implementacioncejas = frenteCeja.ImplantacionC,
                            formacejas = frenteCeja.FormaC,
                            tamanocejas = frenteCeja.TamanoC,
                            ojoscalor = ojoNariz.ColorO,
                            implementacion = ojoNariz.FormaO,
                            narizdorso = ojoNariz.DorsoN,
                            narizbase = ojoNariz.BaseN,
                            boca = bocaLabioMenton.ComisuraB,
                            espesor = bocaLabioMenton.EspesorL,
                            prominencia = bocaLabioMenton.ProminenciaL,
                            mentontipo = bocaLabioMenton.TipoM,
                            mentonforma = bocaLabioMenton.FormaM,
                            inclinacionm = bocaLabioMenton.InclinacionM,
                            oreja = orejaHelix.FormaO,
                            contorno = orejaHelix.ContornoH,
                            lobulo = lobuloFactorRH.ParticularidadL,
                            trago = lobuloFactorRH.TragoL,
                            antitrago = lobuloFactorRH.AntitragoL,
                            sangre = lobuloFactorRH.SangreF,
                            tipo = lobuloFactorRH.TipoF,
                            frentealtura = frenteCeja.AlturaF != null ? frenteCeja.AlturaF : 0,
                            frenteancho = frenteCeja.AnchoF != null ? frenteCeja.AnchoF : 0,
                            ojoastamano = ojoNariz.TamanoO != null ? ojoNariz.TamanoO : 0,
                            narizraiz = ojoNariz.RaizN != null ? ojoNariz.RaizN : 0,
                            narizaltura = ojoNariz.AlturaN != null ? ojoNariz.AlturaN : 0,
                            narizancho = ojoNariz.AnchoN != null ? ojoNariz.AnchoN : 0,
                            bocatamano = bocaLabioMenton.TamanoB != null ? bocaLabioMenton.TamanoB : 0,
                            labiosaltura = bocaLabioMenton.AlturaL != null ? bocaLabioMenton.AlturaL : 0,
                            original = orejaHelix.OriginalO != null ? orejaHelix.OriginalO : 0,
                            superior = orejaHelix.SuperiorH != null ? orejaHelix.SuperiorH : 0,
                            posterior = orejaHelix.PosteriorH != null ? orejaHelix.PosteriorH : 0,
                            dimension = lobuloFactorRH.DimensionL != null ? lobuloFactorRH.DimensionL : 0,
                            helixadherencia = orejaHelix.AdherenciaH != null ? orejaHelix.AdherenciaH : 0,
                            labuloadherencia = lobuloFactorRH.AdherenciaL != null ? lobuloFactorRH.AdherenciaL : 0,

                        };
                        tienedatos = true;
                    }

                }
                else
                {
                    objA = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = detalle_det.Expediente,
                        Centro = institucion != null ? institucion.Nombre : "Centro no registrado",
                        Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                        RutaImagen = interno.RutaImagen,
                        edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        domiclio=domicilio!=null? domicilio.Calle + " #"+ domicilio.Numero.ToString():"Domicilio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                    };
                }


                return JsonConvert.SerializeObject(new { exitoso = true, Editar = tienedatos, obj = obj, objA = objA, EditarA = tienedatosA });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                int edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = domicilio.Localidad + ", " + domicilio.Calle + " " + domicilio.Numero;

                }

                object obj = null;

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                    Situacion = "PENDIENTE",
                    Domicilio = strDomicilio,

                    RutaImagen = interno.RutaImagen,
                    Registro = detalleDetencion.Fecha,
                    Salida = "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}