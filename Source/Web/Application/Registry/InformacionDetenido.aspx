<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="InformacionDetenido.aspx.cs" Inherits="Web.Application.Registry.InformacionDetenido" MaintainScrollPositionOnPostBack="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Información de detenidos</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
            <style type="text/css">
         td.strikeout {
            text-decoration: line-through;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <div class="scroll">
        <!--
       <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
              Información de detenidos
            </h1>
        </div>
    </div>-->
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    
    <section id="widget-grid-0" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken"  id="wid-entrya-1" data-widget-editbutton="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="glyphicon  glyphicon-search"></i></span>
                    <h2>Búsqueda de detenidos</h2>
                </header>

                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <div id="smart-form-register-entry" class="smart-form">
                            <header>
                                Criterios de búsqueda
                            </header>
                            <fieldset>
                                <div class="row"> 
                                    
                                    <!--copiar y pegar de vuelta-->
                                    <section class="col col-4"> 
                                        <label for="tbFechaInicio">Fecha Inicial:</label>
                                        <asp:TextBox ID="tbFechaInicio" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
                                     </section>

                                    <section class="col col-4"> 
                                        <label for="tbFechaFin">Fecha Final:</label>
                                        <asp:TextBox ID="tbFechaFin" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
                                     </section>

                                    <section class="col col-4">
                                        <label class="label">Nombre</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <asp:TextBox runat="server" name="Nombre" ID="nombre" placeholder="Nombre" Style="text-transform: capitalize;" pattern="^[A-Za-z]+$" maxlength="256" class="alphanumeric alptext"/>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Apellido paterno</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <asp:TextBox runat="server" name="Paterno" id="paterno" placeholder="Apellido paterno" Style="text-transform: capitalize;" pattern="^[A-Za-z]+$" maxlength="1000" class="alphanumeric alptext"/>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Apellido materno</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <asp:TextBox runat="server" name="Apellido Materno" id="materno" placeholder="Apellido materno" Style="text-transform: capitalize;" pattern="^[A-Za-z]+$" maxlength="1000" class="alphanumeric alptext"/>
                                        </label>
                                    </section>
                                    
                                    <section class="col col-4">
                                        <label class="label">RFC</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <asp:TextBox runat="server" name="rfc" id="rfc" Style="text-transform: uppercase;" placeholder="RFC" maxlength="1000" class="alphanumeric"/>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Edad</label>
                                        <label class="input">
                                            <asp:TextBox runat="server" type="number" name="edad" id="edad" maxlenght="3" placeholder="Edad" class="alptext"/>
                                        </label>
                                    </section>
                                    
                                    <section class="col col-4">
                                        <label class="label">Sexo</label>
                                        <label class="select">
                                            <asp:DropDownList  runat="server" name="sexo" id="sexo">
                                            </asp:DropDownList>
                                            <i></i>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Ocupación</label>
                                        <label class="select">
                                            <asp:DropDownList  runat="server" name="ocupacion" id="ocupacion">
                                            </asp:DropDownList>
                                            <i></i>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Nacionalidad</label>
                                        <label class="select">
                                            <asp:DropDownList  runat="server" name="nacionalidad" id="nacionalidad">
                                            </asp:DropDownList>
                                            <i></i>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Calle</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <asp:TextBox runat="server" name="calle" id="calle" placeholder="Calle" maxlength="1000" class="alphanumeric alptext"/>
                                        </label>
                                    </section>
                                    
                                    <asp:scriptmanager id="scriptmanager0" runat="server" EnablePartialRendering="true" >
                                    </asp:scriptmanager>
                                    <asp:Updatepanel id="updatepanel0" runat="server" enablepartialrendering="true" >
                                        <contenttemplate>

                                            <section class="col col-4">
                                                <label class="label">Pais</label>
                                                <label class="select">
                                                    <asp:DropDownList runat="server" name="ipais" id="ipais" AutoPostBack="true" onselectedindexchanged="OnSelectedIndexChangedFirst">
                                                    </asp:DropDownList>
                                                    <i></i>
                                                </label>
                                            </section>

                                            <section class="col col-4">
                                                <label class="label">Estado</label>
                                                <label class="select">
                                                    <asp:DropDownList  runat="server" name="iestado" id="iestado" AutoPostBack="true" onselectedindexchanged="OnSelectedIndexChangedSecond"> 
                                                    </asp:DropDownList>
                                                    <i></i>
                                                </label>
                                            </section>

                                            <section class="col col-4">
                                                <label class="label">Municipio</label>
                                                <label class="select">
                                                    <asp:DropDownList  runat="server" name="imunicipio" id="imunicipio" AutoPostBack="true" onselectedindexchanged="OnSelectedIndexChangedThird">
                                                    </asp:DropDownList>
                                                    <i></i>
                                                </label>                                            
                                            </section>                                            

                                            <section class="col col-4">
                                               <label class="label">Colonia</label>
                                               <label class="select">
                                                   <asp:DropDownList  runat="server" name="colonia" id="colonia" AutoPostBack="true">
                                                   </asp:DropDownList>
                                                   <i></i>
                                               </label>
                                            </section>    
                                            
                                        </ContentTemplate>                                        
                                    </asp:Updatepanel>  

                                    <section class="col col-4">
                                        <label class="label">Teléfono</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-phone"></i>
                                            <asp:TextBox runat="server" type="phone" name="telefono" id="telefono" placeholder="Teléfono" data-mask="(999) 999-9999"/>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Alias</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <asp:TextBox runat="server" name="alias" id="alias" placeholder="Alias" maxlength="1000" class="alphanumeric alptext"/>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Estatura</label>
                                        <label class="input">
                                            <asp:TextBox runat="server" type="text" onkeypress="if(event.keyCode != 46 && event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57))event.returnValue=false;" 
                                                name="estatura" id="estatura" placeholder="Estatura" maxlength="1000" class="alphanumeric alptext"/>
                                        </label>
                                    </section>

                                    <section class="col col-4">
                                        <label class="label">Peso</label>
                                        <label class="input">
                                            <asp:TextBox runat="server" type="text" onkeypress="if(event.keyCode != 46 && event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57))event.returnValue=false;" 
                                                name="peso" id="peso" placeholder="Peso" maxlength="1000" class="alphanumeric alptext"/>
                                        </label>
                                    </section>
                                </div>

                            </fieldset>        
                            <footer>                                                             
                                <!--OnClientClick="return partialPostback();" after class-->
                                                                
                                <asp:Button runat="server" OnClick="Btn_Click" ID="save_" Text="Buscar" class="fa fa-search" />                                   
                            </footer>
                                
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Detenidos </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <!-- EnablePartialRendering="true" at the end-->
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateModel="Conditional" >
                            <ContentTemplate>
                            <style>                                
                                .repeater-table-hidden
                                {
                                    display: none;
                                }
                            </style>
                            <asp:Repeater runat= "server" id="dt_basic" >
                                <HeaderTemplate>
                                    <!--Create table-->
                                    <table id="dataTable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <!-- data-class="expand" after th-->
                                            <th>#</th>
                                            <th>Fotografía</th>
                                             <!-- data-hide="phone,tablet" after th-->
                                            <th>Nombre</th>
                                            <th>Apellido paterno</th>
                                            <th>Apellido materno</th>
                                            <th>No. remisión</th>
                                            <th>Estatus</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>    
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <!--style="display:none" after runat-->
                                    <asp:Panel ID="divSearch" runat="server" >
                                    <tr>
                                        <td></td>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td>
                                            <div class="text-center">
                                            <a href="#" id="shoot" class="photoview" data-foto="<%#Eval ("RutaImagen") %>" >
                                                <!-- onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" after width-->                                                                                                                    
                                                <img id="avatar" class="img-thumbnail text-center" alt="" src="<%#Eval ("RutaThumb") %>" height="10" width="50" />
                                            </a>
                                            </div>
                                        </td>
                                        <td><%#Eval ("Nombre") %></td>
                                        <td><%#Eval ("Paterno") %></td>
                                        <td><%#Eval ("Materno") %></td>
                                        <td><%#Eval ("Expediente") %></td>
                                        <td><%#Eval ("EstatusNombre") %></td>
                                        <td>
                                            <%-- return showDescription()--%>
                                            <div class="text-center">
                                                <asp:Button runat="server" class="btn btn-success datos" ID="data_" OnClick="Btn_Data" Text="Ver información" CommandArgument='<%# Eval("Id") + ";" + Eval("idAux") %>'  ></asp:Button>                                            
                                            </div>
                                            <%-- ajaxToolkit:ModalPopupExtender ++ href="javascript:void(0);" after ID && </i>&nbsp; after > (close tag)--%>
                                        </td>
                                    </tr>
                                    </asp:Panel>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                                </asp:Repeater>
                                </ContentTemplate>  
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="save_" EventName="Click" />
                                </Triggers>
                        </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>    
    
    <!--que carajos hacen estas etiquetas-->
     <div id="dvMain">
        <input type="hidden" id="hidden_place" />
     </div>
     <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="VYXMBM" runat="server" value=""/>
     <input type="hidden" id="RAWMOV" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>
    
      <div id="photo-arrested" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <!--src="#" after class-->
                                    <img id="foto_detenido" class="img-thumbnail text-center" alt="fotografía del detenido" />
                                </div>
                            </section>
                        </fieldset>
                        <footertemplate>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footertemplate>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
        <ContentTemplate> 
        <!-- Modal Testing -->
        <asp:Panel id="PanelTest" runat="server">
        <div id="datospersonales-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="refresh">
                        &times;
                    </button>
                    
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <asp:TextBox runat="server" class="form-control" ID="nombreInterno" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <asp:TextBox runat="server" class="form-control" type="datetime" ID="horaRegistro" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right; padding-right: 1px">Motivo de ingreso:</label>
                            <div class="col-md-8">
                                <asp:TextBox runat="server" class="form-control" ID="motivoInterno" disabled="disabled"></asp:TextBox><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <asp:TextBox runat="server" class="form-control" ID="centro" disabled="disabled"></asp:TextBox><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <asp:TextBox runat="server" class="form-control" type="datetime" ID="salida" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <asp:TextBox runat="server" class="form-control" type="text" ID="estaturaInterno" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <asp:TextBox runat="server" class="form-control" type="text" ID="pesoInterno" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>                        
                   <div class="col-md-4">
                       <!--improve performance-->
                       <img id="Img1" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" class="img-thumbnail same-height-thumbnail" height="240" width="240" /><br /><br />
                   </div>
                   <%-- <div class="row">
                        <table id="dt_basic2" class="table table-striped table-bordered table-hover"  width="100%">
                                <thead>
                                    <tr>
                                        <th>Fecha y hora de detención</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                        <th >Motivo de detención</th>
                                        <th>Lugar de detención</th>
                                        <th data-hide="phone,tablet">Institución</th>
                                        <th data-hide="phone,tablet">Situación</th>
                                        <th data-hide="phone,tablet">Fecha y hora salida</th>
                                        <th data-hide="phone,tablet">Tipo de salida</th>
                                        <th data-hide="phone,tablet">Fundamento</th>
                                    </tr>
                                </thead>                               
                            </table>
                   </div>--%>
                </div>
            </div>
        </div>
    </div>
        </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>

    <script type="text/javascript">

        function partialPostback() {
            //ShowSuccess("¡Datos cargados!"); 
            //alert('!Error¡ No fue posible cargar los datos del detenido')
        };

        function requestHandler() {

            if (validar()) {

                var rows = $('#dataTable').length;
                if (rows == 1) {

                    $('#dataTable').dataTable().fnDestroy();

                    $('#dataTable').dataTable({
                        //"fnRowCallback": function () {
                        //    $('#dataTable').waitMe('hide');
                        //}                        
                        stateSave: true
                    });

                }
            }

            else {
                ShowAlert("¡Aviso!", "Debe ingresar al menos un parámetro de búsqueda");
                //$('#dataTable').dataTable();                
            }

        }


        function validar() { //barrido de campos

            if (
                ($("#ctl00_contenido_nombre").val() != "" ||
                    $("#ctl00_contenido_paterno").val() != "" ||
                    $("#ctl00_contenido_materno").val() != "" ||
                    $("#ctl00_contenido_rfc").val() != "" ||
                    $("#ctl00_contenido_edad").val() != "" ||
                    $("#ctl00_contenido_calle").val() != "" ||
                    $("#ctl00_contenido_telefono").val() != "" ||
                    $("#ctl00_contenido_alias").val() != "" ||
                    $("#ctl00_contenido_estatura").val() != "" ||
                    $("#ctl00_contenido_peso").val() != "" ||
                    $("#ctl00_contenido_sexo").val() != "[Sexo]" ||
                    $("#ctl00_contenido_ocupacion").val() != "[Ocupacion]" ||
                    $("#ctl00_contenido_nacionalidad").val() != "[Nacionalidad]" ||
                    $("#ctl00_contenido_ipais").val() != "[Pais]" ||
                    $("#ctl00_contenido_iestado").val() != "[Estado]" ||
                    $("#ctl00_contenido_imunicipio").val() != "[Municipio]" ||
                    $("#ctl00_contenido_colonia").val() != "[Colonia]")) {
                return true;
            }
            else {
                return false;
            }
        }

        function validarAux() {

            if (
                $("#ctl00_contenido_nombreInterno").val() != "" ||
                $("#ctl00_contenido_horaRegistro").val() != "" ||
                $("#ctl00_contenido_motivoInterno").val() != "" ||
                $("#ctl00_contenido_centro").val() != "" ||
                $("#ctl00_contenido_salida").val() != "" ||
                $("#ctl00_contenido_estaturaInterno").val() != "" ||
                $("#ctl00_contenido_pesoInterno").val() != "") {
                return true;
            }
            else {
                return false;
            }
        }


        function showDescription() {
            //$.when(loadBasement()).then(function () {
            //});
            loadBasement(replicate);
        }


        function loadBasement(callbackFn) {
            $("#datospersonales-modal").modal("show"); 
            callbackFn();
        }

        function replicate() {
            $('#dataTable').dataTable({
                stateSave: true,
                //serverSide: true
            });
        }

    </script>
                    
    <script type="text/javascript">

        $(document).ready(function () {

            localStorage.clear();
            var req = Sys.WebForms.PageRequestManager.getInstance();
            req.add_endRequest(function () {
                //$("#dataTable").dataTable(
                //    //serverSide: true,
                //)

                $('#dataTable').waitMe('hide');

            })

        });

        $('#dataTable').on('page.dt', function () {
            alert('!Error¡ No fue posible cargar los datos del detenido')
            $.ajax({
                type: "POST",
                url: "InformacionDetenido.aspx/obtener",
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                data: function (info) {
                    //enviamos al servidor
                    return JSON.stringify({ ClientParameters: JSON.stringify(table.page.info()) });
                }
            });
        })

        $("#btn").click(function (e) { e.preventDefault(); })
        

        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            var rutaDefaultServer = "";

            getRutaDefaultServer();

            // obtener ruta de la imagen
            function getRutaDefaultServer() {
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;
                        }
                    }
                });
            }
            

            $("body").on("click", "#ctl00_contenido_save_", function (e) {
                $('#dataTable').waitMe({
                    effect: 'bounce',
                    text: 'Cargando...:o.o:',
                    bg: 'rgba(255,255,255,0.7)',
                    color: '#000',
                    sizeW: '',
                    sizeH: '',
                    source: ''
                });
            });


            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            
            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }
            

        });
    </script>
</asp:Content>
