﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static IronPython.Modules._ast;
using System.Web.UI;
using Entity;
using System.Web.UI.WebControls;
using static iText.Kernel.Pdf.Colorspace.PdfShading;

namespace Web.Application.Registry
{
    public class RandomObjects
    {
        public int evt, dtn, institute, suburb = new int();
        public string location = "";
        private int contratoN = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"])).IdContrato;

        
        public int MakeDetained(int ctrlEvt, string name, string first, string second, string nick, DateTime dateZero, int age, int weigth, int sex, string evtKey, int policePlate, string reason)
        {
            DateTime date = DateTime.Now;
            int feedback = 0;
            Guid unique_value = Guid.NewGuid();
            //receptaclefin
            try
            {
                //Entity.DetenidoEvento recicle = ControlDetenidoEvento.Obtain(Convert.ToInt16(id)).FirstOrDefault();
                Entity.WSADetenido wsa = new Entity.WSADetenido();
                Entity.DetalleDetencion dtail = new Entity.DetalleDetencion();
                var det = new Entity.Detenido();
                var infoDtn = new Entity.InformacionDeDetencion();             
                var binnacle = new Entity.Historial();
                binnacle.Movimiento = "Registro desde CAD";
                binnacle.Habilitado = true; binnacle.Activo = true;
                infoDtn.Descripcion = "Registrado desde CAD";
                infoDtn.Activo = true; infoDtn.Habilitado = true;
                DateTime finite = DateTime.Now;
                //here is Jhønny
                wsa.TrackingId = unique_value;
                wsa.EventoId = evt;
                wsa.IdentCAD = ctrlEvt;
                wsa.Nombre = name;
                wsa.Paterno = first;
                wsa.Materno = second;
                wsa.Alias = (nick == null) ? "Sin alias" : nick.ToString();
                wsa.FechaCaptura = date;
                wsa.Edad = age;
                wsa.Sexo = sex;
                dtail.TrackingId = unique_value;
                dtail.No = dtn; //invoiceDtn
                dtail.NombreDetenido = name;
                dtail.APaternoDetenido = first;
                dtail.AMaternoDetenido = second;
                dtail.Fecha = date;
                dtail.CentroId = institute;
                dtail.DetalledetencionEdad = age;
                dtail.DetalledetencionSexoId = sex;
                //default d4t4
                dtail.TrackingId = unique_value;
                dtail.Activo = true;
                dtail.Estatus = 1;
                dtail.Fecha = finite;
                dtail.Expediente = evt.ToString();
                dtail.NCP = "cad";
                dtail.ContratoId = contratoN;
                dtail.Tipo = "subcontrato";
                det.TrackingId = unique_value;
                det.Nombre = name;
                det.Paterno = first;
                det.Materno = second;
                det.RutaImagen = "";
                det.Detenidoanioregistro = finite.Year;
                det.Detenidomesregistro = finite.Month + ';' + finite.Day;
                det.DomicilioId = 0;
                infoDtn.TrackingId = unique_value;
                infoDtn.IdEvento = ctrlEvt;
                infoDtn.IdDetenido_Evento = dtn;
                infoDtn.UnidadId = policePlate;
                infoDtn.Motivo = reason;
                infoDtn.HoraYFecha = dateZero;
                infoDtn.LugarDetencion = location;
                infoDtn.ColoniaId = suburb;
                infoDtn.Personanotifica = "N/A";
                infoDtn.Folio = evtKey;
                infoDtn.Detalletrayectoria = "Sin descripcion de los hechos";
                //Entity.DetalleDetencion i = new Entity.DetalleDetencion();
                //wsa.IdNacionalidad = (int)i["n"];
                //wsa.Estatura = (int)i["estatura_cm"];
                //dtn.EventoId = Convert.ToInt32(id);
                //dtn.Nombre = name;
                //dtn.Paterno = first;
                //dtn.Materno = second;
                //dtn.SexoId = sex;
                //dtn.Edad = age;
                //int dtn_saga = ControlDetenidoEvento.Guardar(dtn);
                //Entity.General dataPersonal = new Entity.General();
                //dataPersonal.Edaddetenido = age;
                //dataPersonal.SexoId = sex;
                //dataPersonal.NacionalidadId = 96;
                int disturb =  ControlDetenido.Guardar(det);
                dtail.DetenidoId = disturb;
                int perturb = ControlDetalleDetencion.Guardar(dtail);
                infoDtn.IdInterno = disturb;
                infoDtn.DetalleDetencionId = perturb;
                int creature = ControlInformacionDeDetencion.Guardar(infoDtn);
                binnacle.Fecha = finite;
                binnacle.InternoId = disturb;
                //ControlHistorial.Guardar(binnacle);
                ControlWSADetenido.Guardar(wsa);
                feedback = 1;

                if (DtnExist(dtn))
                    feedback = 2;
            } catch { }
            return feedback;
        }
        
        public int MakeEvent(WSAEvt evt, int i)
        {
            int feedback = new int();
            try
            {
                Entity.Evento node = new Entity.Evento();
                node.IdEventoWS = i;
                node.MotivoId = evt.IdMotivo;
                node.Lugar = location;
                //shit
                node.ColoniaId = evt.IdColonia;
                node.Longitud = evt.Longitud;
                node.Latitud = evt.Latitud;
                node.NumeroDetenidos = evt.NoDetenidos;
                node.Descripcion = evt.Descripcion;
                node.HoraYFecha = evt.Fecha;
                node.Habilitado = true;
                feedback = ControlEvento.Guardar(node);
            }
            catch { }
            return feedback;
        }

        private bool DtnExist(int value)
        {   

            var factory = ControlInformacion.ObtenerPorDetenidoId(value);
            if (factory is null)
                return false;
            return true;
        }


        //tlauncher
        //public T Nodo<T>(T)
        //{
        //    T item = new T();
        //}

    }
}