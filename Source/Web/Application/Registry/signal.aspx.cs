﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;
using System.Linq;

namespace Web.Application.Registry
{
    public partial class signal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor == "")
                Response.Redirect("entrylist.aspx");

            if (Request.QueryString["editable"] == "1")
            {
                editable.Value = "1";
            }
            else
            {
                editable.Value = "0";
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                        //Acceso a Ingreso
                        parametros[1] = "Registro en barandilla";
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Ingreso.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Adicciones
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Adicciones.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Antropometria
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Antropometria.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Familiares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Familiares.Value = permisos.Consultar.ToString().ToLower();
                        }


                        //Acceso a Estudios_Criminologicos
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Estudios_Criminologicos.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Expediente_Medico
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Expediente_Medico.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Informacion_Personal
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Informacion_Personal.Value = permisos.Consultar.ToString().ToLower();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[8]));
                    string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    Entity.Senal senal = new Entity.Senal();

                    senal.Tipo = Convert.ToInt32(datos[2].ToString());
                    senal.Lado = Convert.ToInt32(datos[3].ToString());

                    int idLugar = Convert.ToInt32(datos[9]);
                    var lugar = ControlCatalogo.Obtener(idLugar, Convert.ToInt32(Entity.TipoDeCatalogo.lugar));
                    if(lugar!=null)
                    {
                        senal.LugarId = lugar.Id;
                        senal.Region = lugar.Nombre;
                    }
                    
                    senal.Vista = Convert.ToInt32(datos[5].ToString());
                    senal.Cantidad = Convert.ToInt32(datos[6].ToString());
                    senal.Descripcion = datos[7].ToString();
                    senal.Activo = true;
                    senal.DetenidoId = interno.Id;
                    senal.Habilitado = 1;

                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        mode = "actualizó";
                        senal.Id = Convert.ToInt32(datos[0]);
                        senal.TrackingId = new Guid(datos[1]);
                        ControlSenal.Actualizar(senal);

                        if (senal.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Modificación de señas particulares en barandilla";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        mode = "agregó";
                        senal.TrackingId = Guid.NewGuid();
                        senal.Creadopor = usId;
                        senal.Id = ControlSenal.Guardar(senal);

                        if (senal.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Registro de señas particulares en barandilla";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = interno.Id, TrackingId = interno.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }


        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            return edad;
        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {
                //int value = Convert.ToInt32(trackingid.Trim());
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                if (detalleDetencion == null) detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Catalogo sexo = new Entity.Catalogo();

                int edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);
                    else if (general.Edaddetenido != 0)
                        edad = general.Edaddetenido;
                    else
                    {
                        var infDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                        var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infDetencion.IdDetenido_Evento);

                        if (detenidoEvento != null)
                        {
                            edad = detenidoEvento.Edad;
                        }
                    }
                    sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                }
                var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;
                if (interno != null)
                {
                    if (string.IsNullOrEmpty(interno.RutaImagen))
                    {
                        interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                    }
                }

                object obj = null;

                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = detalleDetencion.Expediente,
                        Centro = institucion != null ? institucion.Nombre : "Institución no registrada",
                        Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                        RutaImagen = interno.RutaImagen,
                        Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                        Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        municipioNombre = municipio != null ? municipio.Nombre : "Muncipio no registrado",
                        domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                    };

                return JsonConvert.SerializeObject(new { exitoso = true,obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string delete(int id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Eliminar)
                {
                    Entity.Senal senal = ControlSenal.ObtenerPorId(id);
                    string[] internoestatus = { senal.DetenidoId.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    senal.Activo = false;
                    ControlSenal.Actualizar(senal);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static List<Combo> GetLugar()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.lugar)).Where(x=> x.Activo).ToList();
            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }
        [WebMethod]
        public static List<Combo> getTipo()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.tipo_senal));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getLado()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.lado_senal));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getVista()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.vista_senal));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }
        [WebMethod]
        public  static DataTable getsenallog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                       
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Tipo",
                                "A.Lado",
                                "A.Vista",
                                "A.Descripcion",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("senaleslog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("vusuarios","V"),"V.id=A.Creadopor","LEFT"),
                                new Join(new Table("Usuario","B"),"B.id=V.UsuarioId","LEFT"),
                                new Join(new Table("senales","C"),"C.Id=A.Id"),
                               
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);

                       

                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }

                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }

            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }
        [WebMethod]
        public static DataTable getSenal(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        int value = Convert.ToInt32(tracking.Trim());
                        List<Where> where = new List<Where>();
                        Entity.Detenido interno = ControlDetenido.ObtenerPorId(value);

                        // where.Add(new Where("S.Activo", "1"));
                        where.Add(new Where("S.DetenidoId", interno.Id.ToString()));

                        Query query = new Query
                        {
                            select = new List<string> {
                        "S.Id",
                        "S.TrackingId",
                        "S.Tipo",
                        "S.Lado",
                        "S.Region",
                        "S.Cantidad",
                        "S.Descripcion",
                        "S.Activo",
                        "S.Vista",
                        "T.Nombre",
                        "ifnull(S.LugarId,0) LugarId"
                    },
                            from = new Table("senales", "S"),
                            joins = new List<Join>
                            {
                                new Join(new Table("tipo_senal", "T"), "T.Id  = S.Tipo"),
                            },
                           
                            wheres = where
                        };


                        DataTables dt = new DataTables(mysqlConnection);
                        //DataTable _dt= dt.Generar(query, draw, start, length, search, order, columns);
                        var data = dt.Generar(query, draw, start, length, search, order, columns);
                        return data;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para realizar la acción.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static string blockitem(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Eliminar)
                {
                    Entity.Senal item = ControlSenal.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] internoestatus = { item.DetenidoId.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    item.Activo = item.Activo ? false : true;
                    item.Habilitado = item.Activo ? 1 : 0;
                    string mensaje = item.Activo ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                    ControlSenal.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                DateTime nacimiento;
                int edad = 0;
                if (general != null)
                {
                    nacimiento = general.FechaNacimineto;
                    edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = domicilio.Localidad + ", " + domicilio.Calle + " " + domicilio.Numero;

                }

                object obj = null;

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = "PENDIENTE",
                    Domicilio = strDomicilio,

                    RutaImagen = interno.RutaImagen,
                    Registro = detalleDetencion.Fecha,
                    Salida = "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }


    }
}