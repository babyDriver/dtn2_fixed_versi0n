﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Registry
{
    public partial class events : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
            getPermisos();


            
        }

        [WebMethod]
        public static List<Combo> getYears()
        {
            var combo = new List<Combo>();
            List<string> data = new List<string>();

            var list = ControlYearsEvent.ObtenerTodos();

            foreach(var item in list)
            {
                combo.Add(new Combo { Desc = item.Year.ToString(), Id = item.Year.ToString() });
            }

            return combo;
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Llamadas y eventos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static string eliminarByTrackingId(string tracking)
        {
            try
            {
                ControlEvento.Desactivar(new Guid(tracking));
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente" });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string blockitem(string id)
        {
            try
            {
                Entity.Evento evento = ControlEvento.ObtenerByTrackingId(new Guid(id));
                evento.Habilitado = evento.Habilitado ? false : true;
                string mensaje = evento.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                ControlEvento.Actualizar(evento);

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getllamadas(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string year)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
            {
                using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
                {
                    try
                    {
                        if (!emptytable)
                        {
                                                        List<Where> where = new List<Where>();
                            var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                            int usuario;

                            if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                            else throw new Exception("No es posible obtener información del usuario en el sistema");

                            where.Add(new Where("l.Activo", "1"));
                            where.Add(new Where("L.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("L.Tipo", contratoUsuario.Tipo));
                            where.Add(new Where("L.Eventoanioregistro", year));

                            Query query = new Query
                            {
                                select = new List<string> {
                                "L.Id",
                                "L.TrackingId",
                                "S.Descripcion DescripcionLlamada",
                                "TRIM(L.Descripcion) Descripcion",
                                "L.LugarDetencion",
                                "C.Asentamiento",
                                "C.CodigoPostal",
                                "M.Nombre Municipio",
                                "E.Nombre Estado",
                                "P.Nombre Pais",
                                "L.ColoniaId IdColonia",
                                "CAST(L.Folio AS unsigned) Folio",
                                //"L.HoraYFecha",
                                "date_format(L.HoraYFecha, '%Y-%m-%d %H:%i:%S') HoraYFecha",
                                "L.Activo",
                                "L.Habilitado"
                            },
                                from = new DT.Table("eventos", "L"),
                                joins = new List<Join> {
                                new Join(new DT.Table("Llamadas", "S"), "S.Id = L.LlamadaId"),
                                new Join(new DT.Table("Colonia", "C"), "C.Id = L.ColoniaId"),
                                new Join(new DT.Table("Municipio", "M"), "M.Id = C.IdMunicipio"),
                                new Join(new DT.Table("Estado", "E"), "E.Id = M.EstadoId"),
                                new Join(new DT.Table("Pais", "P"), "P.Id = E.PaisId")
                            },
                                wheres = where

                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            var data = dt.Generar(query, draw, start, length, search, order, columns);
                            return data;
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    catch (Exception ex)
                    {
                        return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                    }
                    finally
                    {
                        if (mysqlConnection.State == System.Data.ConnectionState.Open)
                            mysqlConnection.Close();
                    }
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }
    }
}