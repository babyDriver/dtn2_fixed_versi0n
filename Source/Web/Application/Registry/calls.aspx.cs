﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Registry
{
    public partial class calls : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Llamadas y eventos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static string eliminarByTrackingId(string tracking)
        {
            try
            {
                ControlLlamada.Desactivar(new Guid(tracking));
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente" });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static DataTable getllamadas(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");
                        
                        where.Add(new Where("L.Activo", "1"));
                        where.Add(new Where("L.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("L.Tipo", contratoUsuario.Tipo));

                        Query query = new Query
                        {
                            select = new List<string>{
                        "L.Id",
                        "L.TrackingId",
                        "replace(L.Involucrado,'\"','') Involucrado",
                        "replace(L.Descripcion,'\"','') Descripcion" ,
                        "replace(L.LugarDetencion,'\"','') LugarDetencion",
                        "C.Asentamiento",
                        "C.CodigoPostal",
                        "M.Nombre Municipio",
                        "E.Nombre Estado",
                        "P.Nombre Pais",
                        "L.IdColonia",
                        "L.Folio",
                        //"L.HoraYFecha",
                        "date_format(L.HoraYFecha, '%Y-%m-%d %H:%i:%S') HoraYFecha",
                        "L.Activo",
                        "L.Habilitado"
                        },
                            from = new DT.Table("llamadas", "L"),
                            joins = new List<Join> {
                                    new Join(new DT.Table("Colonia", "C"), "C.Id = L.IdColonia"),
                                    new Join(new DT.Table("Municipio", "M"), "M.Id = C.IdMunicipio"),
                                    new Join(new DT.Table("Estado", "E"), "E.Id = M.EstadoId"),
                                    new Join(new DT.Table("Pais", "P"), "P.Id = E.PaisId")
                            },
                            wheres = where
                        };                        

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string blockitem(string id)
        {
            try
            {
                Entity.Llamada llamada = ControlLlamada.ObtenerByTrackingId(new Guid(id));
                llamada.Habilitado = llamada.Habilitado ? false : true;
                string mensaje = llamada.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                ControlLlamada.Actualizar(llamada);
                
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}