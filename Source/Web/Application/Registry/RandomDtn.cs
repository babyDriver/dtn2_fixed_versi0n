﻿using Aspose.Foundation.UriResolver.RequestResponses;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Web.Application.Registry
{
    public class RandomDtn : Detained
    {
        static int hiddeEvt;
        int dValue;
        int nDet;
        int noinvolve;
        public RandomDtn(int eValue, int dValue)
        {
            this.dValue = dValue;
            hiddeEvt = eValue;
        }
        //t4k3 of .conf
        public static string _url = "http://c5-amb-postproduccion.promad.com.mx:9197" + "/api";
        string _nameSpace = _url + "/personainvolucrada/getRegistros";
        string _json = new JavaScriptSerializer().Serialize(new // adjoining
        {
            uuid = 5,
            idEvento = hiddeEvt
        });
        public string factory()
        {
            string feedback = "";
            var jObj = new Provider().factory(_nameSpace, _json);
            JArray paramsArray = new JArray();
            // l0g1c                      
            paramsArray = (JArray)jObj["items"];
            foreach (JToken i in paramsArray)
            {
                if ((string)i["nombre_tipo_persona"] == "DETENIDO" && dValue == (int)i["id_persona_involucrada"])
                {
                    nDet++; // # dtnd
                    noinvolve = (int)i["id_persona_involucrada"];
                    name = (string)i["nombre"];
                    first = (string)i["apellido_paterno"];
                    last = (string)i["apellido_materno"];
                    nick = (string)i["alias"];
                    date = Convert.ToDateTime((string)i["fecha_inicio"]);
                    age = (int)i["edad"];
                    sex = (int)i["id_sexo"];
                    if (noinvolve == dValue)
                    feedback = name + ' ' + first + " " + last;
                }
            }

            return feedback;
        }
    }    
}