﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Configuration;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Web.Application.Registry
{
    public partial class entry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validatelogin();
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor == "")
                Response.Redirect("entrylist.aspx");

            if (Request.QueryString["editable"] == "0")
            {
                editable.Value = "0";
            }
            else
            {
                editable.Value = "1";
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var usuario_ = ControlUsuario.Obtener(usuario);
                if (usuario_.RolId == 3)
                {
                    string cuip = usuario_.CUIP != null ? usuario_.CUIP : "sin dato";
                    this.idcuip.InnerHtml = "<strong>CUIP: " + cuip + "</strong>";

                }

                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        var parametro = ControlParametroContrato.TraerTodos();
                        Entity.ParametroContrato parametroContrato = new Entity.ParametroContrato();
                        foreach (var item in parametro.Where(x=> x.ContratoId== subcontrato.Id && x.Parametroid==Convert.ToInt32( Entity.ParametroEnum.CONTRASEÑA_SUPERVISOR)))
                        {
                            parametroContrato = item;
                        }

                        this.PCBRIG.Value = parametroContrato.Valor != null ? parametroContrato.Valor : "2386";


                        // perfiles ws4





                        //Acceso a Informacion_Personal
                        parametros[1] = "Registro en barandilla";
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Informacion_Personal.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Adicciones
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Adicciones.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Antropometria
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Antropometria.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Señas_Particulares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Señas_Particulares.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Familiares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Familiares.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Estudios_Criminologicos
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Estudios_Criminologicos.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Expediente_Medico
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Expediente_Medico.Value = permisos.Consultar.ToString().ToLower();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static string GetParametrosTamaño()
        {
            try
            {
                var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                var parametro = ControlParametroContrato.TraerTodos();
                decimal maximo = 0;
                decimal minimo = 0;
                
                foreach(var item in parametro)
                {
                    if(item.ContratoId==contratoususario.IdContrato && item.Parametroid==13)
                    {
                        minimo = Convert.ToDecimal(item.Valor);
                    }
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 14)
                    {
                        maximo = Convert.ToDecimal(item.Valor);
                    }
                }
                return JsonConvert.SerializeObject(new { exitoso =true,TMax=maximo,Tmin=minimo});
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        
        [WebMethod]
        public static List<Combo> getInstitucion(string data)
        {
            List<Combo> combo = new List<Combo>();

            string val = "";
            if (data != "")
            {
                val = data;
            }
            else
            {
                val = "0";
            }

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var ususarocontrato = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(val));
            var institucion = new Entity.Institucion();
            if(contratoUsuario==null)
            {
                contratoUsuario = ususarocontrato.FirstOrDefault(x=> x.Activo==true);

            }

            if (contratoUsuario.Tipo == "contrato")
            {
                institucion = contratoUsuario != null ? ControlInstitucion.ObtenerPorId(ControlContrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId) : null;
            }
            else if (contratoUsuario.Tipo == "subcontrato")
            {
                institucion = contratoUsuario != null ? ControlInstitucion.ObtenerPorId(ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId) : null;
            }


            if (institucion != null)
            {
                combo.Add(new Combo { Desc = institucion.Nombre, Id = institucion.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string saveRutaBiometrico(string trackingId)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    var detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingId));
                    string Mensaje = "";
                    string msg="";
                    //biometrik
                    Mensaje = ControlBiometricos.GuardarRutasBiometrico(detenido.Id);
                    if (Mensaje == "OK")
                    {
                        try
                        {
                            var detalle = ControlDetalleDetencion.ObtenerPorDetenidoId(detenido.Id).LastOrDefault(x => x.Activo);
                            int[] filtro = { detalle.ContratoId, detenido.Id };
                            var hit = ControlCrossreference.GetCrossReferences(filtro);

                            if (hit.Count > 0)
                            {
                                msg = "El detenido "+detenido.Nombre+" "+detenido.Paterno+ " "+detenido.Materno+" tiene un hit";
                            }

                            var listabiometricos = ControlBiometricos.GetByDetenidoId(detenido.Id);
                            string rutaimagen = "";
                            if (listabiometricos.Count > 0)
                            {
                                rutaimagen = listabiometricos.FirstOrDefault().Rutaimagen;
                            }
                            var historial = new Entity.Historial();
                            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.TrackingId = Guid.NewGuid();
                            historial.InternoId = detalle.DetenidoId;
                            historial.Movimiento = "Registro del Biometrico del detenido";
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);

                            return JsonConvert.SerializeObject(new { exitoso = true, mensaje = msg ,rutaimagenfrontal=rutaimagen});
                        }
                        catch (Exception ex)
                        {
                            var error = ex;
                        }
                        return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "" });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = Mensaje });

                    }
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }


        [WebMethod]
        public static string RelacionarPerfil(string[] Datos)
        {
          
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                var detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(Datos[0]));

                detenido.Nombre = Datos[1];
                detenido.Paterno = Datos[2];
                detenido.Materno = Datos[3];

                var general = ControlGeneral.ObtenerPorDetenidoId(detenido.Id);

                general.FechaNacimineto = Convert.ToDateTime(Datos[4]);


                ControlDetenido.Actualizar(detenido);
                ControlGeneral.Actualizar(general);





                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "" });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "" });

                    }
                
               

        }

        //biometrik
        [WebMethod] 
        public static string validaBiometricosalGuardar(string value)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    Guid bias = new Guid(value); //(int)Convert.ToInt32
                    var detenido = ControlDetenido.ObtenerPorTrackingId(bias);
                    var biometricos = ControlBiometricos.GetByDetenidoId(detenido.Id);
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "" });

                    //if (biometricos.Count > 0)
                    //{
                    //    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "" });
                    //}
                    //else
                    //{
                    //    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Alerta" });
                    //}

                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

             }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });

            }
        }


        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Modificar)
                {
                    if (!string.IsNullOrEmpty(datos[0]))
                    {
                        var internoEgresado = ControlDetenido.ObtenerPorId(int.Parse(datos[0]));
                        string[] datos2 = { internoEgresado.Id.ToString(), true.ToString() };
                        var detalleDetencion2 = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos2);
                        if (detalleDetencion2 == null)
                            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });
                    }

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoususario.IdContrato);
                    Entity.Detenido interno = new Entity.Detenido();
                    Entity.DetalleDetencion detail = new Entity.DetalleDetencion();
                    Entity.DetalleDetencion detalleDetencionTemp = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[13]));
                    int contractId = 0;
                    string contractType = "";
                    if (detalleDetencionTemp != null)
                    {
                        contractId = detalleDetencionTemp.ContratoId;
                        contractType = detalleDetencionTemp.Tipo;
                    }
                    if (!detalleDetencionTemp.Activo)
                    {
                        detalleDetencionTemp = null;
                    }
                    var internoAux = (detalleDetencionTemp != null) ? ControlDetenido.ObtenerPorId(detalleDetencionTemp.DetenidoId) : null;
                    var listaDetalleDetencion = ControlDetalleDetencion.ObteneTodos();
                    int expediente = 0;
                    interno.Nombre = datos[5].ToString();
                    interno.Paterno = datos[6].ToString();
                    interno.Materno = datos[7].ToString();
                    interno.RutaImagen = datos[9].ToString();

                    //interno.ContratoId = Convert.ToInt32(datos[14].ToString());
                    detail.CentroId = subcontrato.InstitucionId;
                    detail.Fecha = Convert.ToDateTime(datos[4].ToString());
                    detail.NCP = "npc";
                    detail.Activo = true;
                    detail.ExpedienteAdm = "";
                    detail.Estatus = 1;
                    var contratoUsuarioAux = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(contratoususario.IdContrato.ToString()));
                    detail.ContratoId = contractId != 0 ? contractId : detalleDetencionTemp.ContratoId;
                    detail.Tipo = contractType != "" ? contractType : detalleDetencionTemp.Tipo;
                    detail.Detalledetencionanioregistro = detalleDetencionTemp.Detalledetencionanioregistro;
                    detail.Detalldetencionmesregistro = detalleDetencionTemp.Detalldetencionmesregistro;
                    detail.DetalledetencionEdad = detalleDetencionTemp.DetalledetencionEdad;
                    detail.DetalledetencionSexoId = detalleDetencionTemp.DetalledetencionSexoId;
                    detail.NombreDetenido = interno.Nombre;
                    detail.APaternoDetenido = interno.Paterno;
                    detail.AMaternoDetenido = interno.Materno;
                    Entity.Residente residente = new Entity.Residente();
                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        //Validación expediente único
                        if (detalleDetencionTemp != null)
                        {
                            if (detalleDetencionTemp.ContratoId == contratoususario.IdContrato)
                            {
                                if (detalleDetencionTemp.DetenidoId != Convert.ToInt32(datos[0]))
                                {
                                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El número de remisión ya existe" });
                                }
                            }
                        }

                        mode = "actualizó";
                        interno.Id = Convert.ToInt32(datos[0]);
                        interno.TrackingId = new Guid(datos[1]);
                        interno.Detenidoanioregistro = internoAux.Detenidoanioregistro;
                        interno.Detenidomesregistro = internoAux.Detenidomesregistro;
                        detail.DetenidoId = interno.Id;
                        detail.Id = Convert.ToInt32(datos[12]);
                        detail.TrackingId = new Guid(datos[13]);
                        detail.Lesion_visible = Convert.ToBoolean(datos[15]);
                        detail.Estatus = detalleDetencionTemp.Estatus;
                        detail.Detalledetencionanioregistro = detalleDetencionTemp.Detalledetencionanioregistro;
                        detail.Detalldetencionmesregistro = detalleDetencionTemp.Detalldetencionmesregistro;
                        detail.DetalledetencionEdad = detalleDetencionTemp.DetalledetencionEdad;
                        detail.DetalledetencionSexoId = detalleDetencionTemp.DetalledetencionSexoId;
                     
                        string exp="";
                        if (detalleDetencionTemp == null)
                        {
                            exp = datos[2];

                        }
                        else
                        {
                            exp = detalleDetencionTemp.Expediente != null ? detalleDetencionTemp.Expediente : detalleDetencionTemp.ExpedienteAdm;
                        }
                        detail.Expediente = exp;
                        //recuperar el domicilioid y domicilionid
                        Entity.Detenido recuperar = ControlDetenido.ObtenerPorId(interno.Id);
                        interno.DomicilioId = recuperar.DomicilioId;
                        interno.DomicilioNId = recuperar.DomicilioNId;
                        //if(interno.RutaImagen == "")
                        //    interno.RutaImagen = recuperar.RutaImagen;
                       
                        ControlDetenido.Actualizar(interno);
                        ControlDetalleDetencion.Actualizar(detail);

                        if(detail.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detail.DetenidoId;
                            historial.Movimiento = "Modificación de datos del detenido en barandilla";
                            historial.TrackingId = detail.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato1 = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato1.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }                        
                      //  residente = GuardarResidente(estatus.Id, datos);
                    }
                    else
                    {
                        //Validación expediente único
                        if (detalleDetencionTemp != null && internoAux != null)
                        {
                            if (detalleDetencionTemp.ContratoId == detail.ContratoId)
                            {
                                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El número de remisión ya existe" });
                            }
                        }

                        var listaDetalleDetencionFiltrada = listaDetalleDetencion.Where(x => x.ContratoId == contratoUsuarioAux.IdContrato && x.Tipo == contratoUsuarioAux.Tipo);

                        if (listaDetalleDetencionFiltrada.Count() > 0)
                        {                            
                            detalleDetencionTemp = listaDetalleDetencionFiltrada.Last();
                            expediente = Convert.ToInt32(detalleDetencionTemp.Expediente) + 1;
                        }
                        else
                            expediente = 1;

                        mode = "registró";
                        interno.TrackingId = Guid.NewGuid();                        
                        interno.Id = ControlDetenido.Guardar(interno);
                        detail.Expediente = Convert.ToString(expediente);
                        detail.DetenidoId = interno.Id;
                        detail.TrackingId = Guid.NewGuid();
                        detail.Detalledetencionanioregistro = DateTime.Now.Year;
                        detail.Detalldetencionmesregistro = DateTime.Now.Month;
                        detail.Id = ControlDetalleDetencion.Guardar(detail);

                        if(detail.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detail.DetenidoId;
                            historial.Movimiento = "Registro del detenido en barandilla";
                            historial.TrackingId = detail.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato1 = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato1.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }                        

                        // residente = GuardarResidente(estatus.Id, datos);
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = interno.Id, TrackingId = detail.Id, IdEstatus = detail.Id, TrackingIdEstatus = detail.TrackingId, IdResidente = residente.Id, TrackingIdResidente = residente.TrackingId, RutaImagen = interno.RutaImagen });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        
        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }


        [WebMethod]
        public static string getPerfiles(string trackingId)
        {
            //int id = Convert.ToInt32(trackingId);
            //var det_detencion = ControlDetalleDetencion.ObtenerPorId(id);
            var det_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingId));
            var cross_references = ControlCrossreference.GetCrossReferences(new int[] {det_detencion.ContratoId,det_detencion.DetenidoId });

            PerfilesGamasis.WsGetPerson wsGetPerson = new PerfilesGamasis.WsGetPerson();
            PerfilesGamasis.ParametersBO parametersBO = new PerfilesGamasis.ParametersBO();
            parametersBO.GetMatcherTemplate = true;
            parametersBO.GetAlfas = true;
            parametersBO.GetFaces = true;
            parametersBO.GetFingers = true;
            parametersBO.GetGeolocation = true;
            parametersBO.GetIrises = true;
            parametersBO.GetPalms = true;
            parametersBO.GetSignatures = true;
            parametersBO.GetTemplates = true;
            parametersBO.GetVoices = true;

            if(cross_references.Count==0)
            {
                return JsonConvert.SerializeObject(new { exitoso = false });

            }
            var response = wsGetPerson.GetPerson(cross_references.LastOrDefault().tcn_candidate, parametersBO);

            if (response.Ok)
            {

                string rutaDefault = "";

                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/Perfiles/"+ response.Person.Alfas.Tcn+".png");
          
                var ruta = System.Web.HttpContext.Current.Server.MapPath(rutaDefault);

                System.IO.MemoryStream ms = new System.IO.MemoryStream(response.Person.Faces[2].Biodata);
                System.Drawing.Bitmap b = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms);
                System.Drawing.Bitmap bit = new System.Drawing.Bitmap(b);
                bit.Save(ruta, System.Drawing.Imaging.ImageFormat.Jpeg);
                CreaThumb(ruta);                

                object obj = new {
                    Nombre = response.Person.Alfas.Name,
                    Paterno = response.Person.Alfas.Lastname,
                    Materno=response.Person.Alfas.Second_lastname,
                    FechaNacimiento=response.Person.Alfas.Birth_date,
                    RutaImagen = rutaDefault

                };
                List<object> list = new List<object>();

                list.Add(obj);

                return JsonConvert.SerializeObject(new { exitoso = true, list = list });

            }
            foreach (var item in response.Person.Faces)
            {
                int i = 0;
                System.IO.MemoryStream ms = new System.IO.MemoryStream(item.Biodata);
                System.Drawing.Bitmap b = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms);

                System.Drawing.Bitmap bit = new System.Drawing.Bitmap(b);
                bit.Save(@"C:\Users\cgomez\Documents\dumps\foto.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            return JsonConvert.SerializeObject(new { exitoso = true });

        }

        public static bool CreaThumb(string filepath)
        {
            try
            {
                //url file output
                string output = string.Empty;
                // Load image.
                System.Drawing.Image image = System.Drawing.Image.FromFile(filepath);

                // Compute thumbnail size.
                Size thumbnailSize = GetThumbnailSize(image);

                // Get thumbnail.
                System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero);

                string extension = string.Empty;
                extension = Path.GetExtension(filepath);

                // Save thumbnail.
                if (extension == ".JPG" || extension == ".jpg" || extension == ".JPEG" || extension == ".jpeg")
                {
                    output = filepath.Replace(".jpg", ".thumb").Replace(".jpeg", ".thumb").Replace(".JPG", ".thumb").Replace(".JPEG", ".thumb");
                    thumbnail.Save(output);
                }
                else if (extension == ".PNG" || extension == ".png")
                {
                    output = filepath.Replace(".png", ".thumb").Replace(".PNG", ".thumb");
                    thumbnail.Save(output, ImageFormat.Png);
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 40;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }

        [WebMethod]
        public static string getdatos(string value)
        {
            //int dtn = Convert.ToInt32(value);
            try
            {
                //r3viv4l
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla"  }).Consultar)
                {
                    //string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    //detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    //Entity.Historial historial = ControlHistorial.ObtenerPorDetenido(detalleDetencion.DetenidoId).LastOrDefault();
                    Entity.DetalleDetencion detalleDetencion = new Entity.DetalleDetencion();
                    detalleDetencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(value));
                    Entity.Detenido interno = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
                    if (detalleDetencion == null)
                    {
                        detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                    }

                   // Entity.Residente residente = ControlResidente.ObtenerPorEstatusInternoId(estatus.Id);
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    object obj = null;

                    object[] dataConUsuario = null;                    

                    int contratoId = 0;
                    if(detalleDetencion.Tipo == "contrato")
                    {
                        dataConUsuario = new object[]
                        {
                            usId,
                            detalleDetencion.ContratoId,
                            "contrato"
                        };

                        contratoId = ControlContratoUsuario.ObtenerPorContratoYUsuario(dataConUsuario).IdContrato;
                    }
                    else if(detalleDetencion.Tipo == "subcontrato")
                    {
                        dataConUsuario = new object[]
                        {
                            usId,
                            detalleDetencion.ContratoId,
                            "subcontrato"
                        };

                        contratoId = ControlContratoUsuario.ObtenerPorContratoYUsuario(dataConUsuario).IdContrato;
                    }

                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = detalleDetencion.Expediente,
                        CentroId = detalleDetencion.CentroId,
                        Fecha = detalleDetencion.Fecha != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd/MM/yyyy HH:mm:ss") : "",
                        //Fecha = historial.Fecha != null ? Convert.ToDateTime(historial.Fecha).ToString("dd/MM/yyyy HH:mm:ss") : "",
                        Nombre = interno.Nombre,
                        Paterno = interno.Paterno,
                        Materno = interno.Materno,
                        NCP = detalleDetencion.NCP,
                        RutaImagen = interno.RutaImagen,
                        /* Estancia = residente.Estancia,
                        Modulo = residente.Modulo,
                        Pasillo = residente.Pasillo,
                        Dormitorio = residente.Dormitorio,
                        Seccion = residente.Seccion,
                        Celda = residente.Celda,
                        Nivel = residente.Nivel,
                        Cama = residente.Cama,
                        IdResidente = residente.Id,
                        TrackingIdResidente = residente.TrackingId,*/
                        IdEstatus = detalleDetencion.Id,
                        TrackingIdEstatus = detalleDetencion.TrackingId,
                        ContratoId = contratoId,
                        Lesion_visible=detalleDetencion.Lesion_visible.ToString()
                    };

                   
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
           
              
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getContracts()
        {
            int id = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var combo = new List<Combo>();
            var c = ControlContratoUsuario.ObtenerPorUsuarioId(id);

            var cAux = c.Where(x => x.Activo == true).ToList();

            foreach (var item in cAux)
            {
                var contrato = new Entity.Contrato();
                var subcontrato = new Entity.Subcontrato();
                
                if (item.Tipo == "contrato")
                {
                    contrato = ControlContrato.ObtenerPorId(item.IdContrato);
                }
                else if(item.Tipo == "subcontrato")
                {
                    subcontrato = ControlSubcontrato.ObtenerPorId(item.IdContrato);
                }

                if(item.Tipo == "contrato")
                {
                    combo.Add(new Combo { Desc = contrato.Nombre + " - " + item.Tipo, Id = item.Id.ToString() });
                }
                else if(item.Tipo == "subcontrato")
                {
                    combo.Add(new Combo { Desc = subcontrato.Nombre + " - " + item.Tipo, Id = item.Id.ToString() });
                }                
            }

            return combo;
        }

        [WebMethod]
        public static string getActualContractId()
        {
            try
            {
                return JsonConvert.SerializeObject(new { exitoso = true, contratoId = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]) });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}