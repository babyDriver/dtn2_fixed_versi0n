﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="signaledit.aspx.cs" Inherits="Web.Application.Registry.signaledit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro</a></li>
    <li>Señas particulares</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="glyphicon glyphicon-bookmark"></i>
                Señas particulares
            </h1>
        </div>
    </div>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>


    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-study-1" data-widget-editbutton="false" data-widget-custombutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-reorder"></i></span>
                    <h2>Datos personales </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <div class="well">

                            <div class="row show-grid ">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <strong>Institución a disposición</strong><br />
                                        <small><span id="centro"></span></small>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <strong>Expediente</strong><br />
                                        <small><span id="expediente"></span></small>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <strong>Nombre(s)</strong><br />
                                        <small><span id="Span1"></span></small>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <strong>Apellido paterno</strong><br />
                                        <small><span id="Span2"></span></small>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <strong>Apellido materno</strong><br />
                                        <small><span id="Span3"></span></small>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <img id="avatar" class="img-thumbnail" alt="" runat="server" src="~/Content/img/Imagenx.jpg" />
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>

        </article>

        <article class="col-sm-12 col-md-12 col-lg-12">
             <div class="jarviswidget jarviswidget-color-darken" id="wid-signal-2" data-widget-editbutton="false" data-widget-custombutton="false">
      
                <header>
                    <span class="widget-icon"><i class="glyphicon glyphicon-bookmark"></i></span>
                    <h2>Señas particulares</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <div id="smart-form-register" class="smart-form">
                            
                            <fieldset>
                                <legend class="hide">&nbsp;</legend>
                                <div class="row">

                                    <section class="col col-18">


                                        <img id="Img2" class="img-thumbnail" alt="" runat="server" src="~/Content/img/senalesdescripcion.jpg" />


                                    </section>


                                    <section class="col col-4">

                                        <section class="col col-8">
                                            <select name="tipo" id="tipo" style="width: 100%" class="input-sm"">
                                                <option value="0">[Tipo]</option>
                                            </select>
                                            <i></i>
                                        </section>


                                        <section class="col col-8">
                                            <select name="lado" id="lado" style="width: 100%" class="input-sm">
                                                <option value="0">[Lado]</option>
                                            </select>
                                            <i></i>
                                        </section>

                                        <section class="col col-5">
                                            <label class="input">
                                                <i class="icon-append fa fa-street-view"></i>
                                                <input type="text" name="region" id="region" runat="server" placeholder="Región" maxlength="2" class="input-region" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la región.</b>
                                            </label>
                                        </section>

                                        <section class="col col-8">
                                            <select name="vista" id="vista" style="width: 100%" class="input-sm"">
                                                <option value="0">[Vista]</option>
                                            </select>
                                            <i></i>
                                        </section>

                                        <section class="col col-5">
                                            <label class="input">
                                                <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                <input type="text" name="cantidad" id="cantidad" runat="server" placeholder="Cantidad" maxlength="2" class="input-cantidad" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la cantidad.</b>
                                            </label>
                                        </section>

                                        <section class="col col-10">
                                            <label class="input">
                                                <i class="icon-append fa fa-newspaper-o"></i>
                                                <input type="text" name="descripcion" id="descripcion" runat="server" placeholder="Descripción" maxlength="250" class="input-descripcion" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la descripción.</b>
                                            </label>

                                        </section>

                                    </section>

                                    <section class="col col-8">


                                        <img id="Img1" class="img-thumbnail" alt="" runat="server" src="~/Content/img/senales.jpg" />


                                    </section>




                                </div>

                            </fieldset>
                            <footer>
                                <a class="btn btn-sm btn-default" href="signal.aspx"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                                <a class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            </footer>
                            <input id="other" type="hidden" name="other" />
                        </div>
                    </div>
                </div>
            </div>
        </article>

    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
  
    </script>
</asp:Content>
