﻿//using IronPython.Hosting;
using System.Web.Services.Description;
using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Text;
using System.Web;
using System.Linq;
using Entity.Util;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.Scripting.Hosting;
using System.Runtime.InteropServices.ComTypes;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Web.DynamicData;
using System.Threading;
using System.Security.Policy;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using SelectPdf;
using Web.Classes;

namespace Web.Application.Registry
{
	public class DEtenidoFiltroAux
	{
		public string Paterno { get; set; }
		public string Materno { get; set; }
		public string Nombre { get; set; }
		public string Alias { get; set; }
		public string Id { get; set; }
	}

	public partial class entrylist : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
			if (idContrato == 0)
			{
				Response.Redirect("../dashboard.aspx", true);
			}
			validatelogin();
			getPermisos();
			if (!IsPostBack)
			{
				tbFechaInicial.Text = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
				tbFechaFinal.Text = DateTime.Now.ToString("yyyy-MM-dd");
			}
		}
		public static void validatelogin()
		{
			int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
			var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

			string ip = "";
			ip = HttpContext.Current.Session["ipusuario"].ToString();
			if (ip != userlogin.IPequipo)
			{
				FormsAuthentication.SignOut();
				FormsAuthentication.RedirectToLoginPage();
				System.Web.HttpContext.Current.Session.RemoveAll();
				return;

			}

		}
		public void getPermisos()
		{
			var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
			int usuario;
			if (membershipUser != null)
			{
				usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
				string[] parametros = { usuario.ToString(), "Registro en barandilla" };
				var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);

				string[] parametrosReingreso = { usuario.ToString(), "Registro en barandilla" };
				var permisosReingreso = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametrosReingreso);

				string[] parametrosEgreso = { usuario.ToString(), "Registro en barandilla" };
				var permisosEgreso = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametrosEgreso);

				if (permisos == null)
				{
					//No cuenta con permisos
					Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
				}
				else
				{
					if (!permisos.Consultar)
						//No cuenta con permisos de consultar el  apartado
						Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
					else
					{
						//Consultar
						this.WERQEQ.Value = permisos.Consultar.ToString().ToLower();

						//setear hidden registrar
						this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();

						//setear hidden modificar
						this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
						//setear hidden habilitar
						this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

						//setear hidden reingreso
						if (permisosReingreso != null)
							this.RAWMOV.Value = permisosReingreso.Consultar.ToString().ToLower();
						//setear hidden egreso
						if (parametrosEgreso != null)
							this.VYXMBM.Value = permisosEgreso.Consultar.ToString().ToLower();
					}
				}
			}
			else
			{
				Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
			}
		}


		[WebMethod]
		public static List<Combo> getSexogrid()
		{
			var combo = new List<Combo>();
			var c = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));

			foreach (var item in c)
			{
				if (item.Activo && item.Habilitado)
				{
					combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
				}
			}

			return combo;
		}

		[WebMethod]
		public static List<Combo> getMotivogrid()
		{
			var combo = new List<Combo>();
			var c = ControlWSAMotivo.ObtenerTodos();

			foreach (var item in c)
			{
				if (item.Activo)
				{
					combo.Add(new Combo { Desc = item.NombreMotivoLlamada, Id = item.IdMotivo.ToString() });
				}
			}

			return combo;
		}
		[WebMethod]
		public static string getDataDetenidoNuevo(string interno)
		{
			if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
			{
				try
				{
					List<object> list = new List<object>();
					object obj = new
					{
						Check = "",
						Nombre = "",
						Apellidopaterno = "",
						Apellidomaterno = "",
						Sexo = "",
						Edad = "",
						Motivo = ""
					};

					list.Add(obj);


					return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
				}
				catch (Exception ex)
				{
					return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
				}
			}
			else
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "error" });
			}
		}

		[WebMethod]
		public static object GetAlertaWeb()
		{
			try
			{
				var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
				var denoma = "Alerta web";
				var alerta = "alerta web";
				if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
				{
					denoma = datosAlertaWeb.Denominacion;
					alerta = denoma;
				}
				object data = new
				{

					Denominacion = denoma,
					AlertaWerb = alerta
				};

				return data;
			}
			catch (Exception ex)
			{
				return new { exitoso = false, mensaje = ex.Message };
			}
		}


		[WebMethod]
        // string age, string sex,string origin,string charge

        public static void pdfBeta(IPHData reportData)
			//, string fakeName, string fakeFirst, string fakeLast,string nick, string narrative, string minute, string hour, string day, string month, string year, string col, string mun, string zp, string street, string reference, string age) //string[] datos

		{
			
            string filePath = "http://localhost:11261/Content/part/modal.html";
			filePath = reportData.referencia;
			filePath = "http://localhost:11261/Content/part/modal.html";
			string output = @"C:\demo\framberry.pdf";
			//var html = File.ReadAllText("http://localhost:11261/Content/part/modal.html");
            SelectPdf.HtmlToPdf quimera = new SelectPdf.HtmlToPdf();
			//SelectPdf.PdfDocument pdf =  quimera.ConvertUrl(filePath);
			SelectPdf.PdfDocument pdf = quimera.ConvertHtmlString("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\r\n<html class=\"supernova\"><head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<meta property=\"og:title\" content=\"Cybercrime Report Template\" >\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />\r\n<meta name=\"HandheldFriendly\" content=\"true\" />\r\n<title>Cybercrime Report Template</title>\r\n<!-- SCRIPT Z0N3 -->\r\n    <!-- <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script> -->\r\n    <!-- <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' /> -->\r\n\t\r\n\t<!-- CSS Z0N3 -->\r\n    <style type=\"text/css\">\r\n        #mapCurrent {\r\n            height: 22vw;\r\n            width: 80%;\r\n        }\r\n\t\t.select {\r\n\t\t\tbackground: transparent url('assets/Icon ionic-ios-arrow-down.png') 0% 0% no-repeat padding-box;\r\n\t\t}\r\n\t\t#tenant-modal {\r\n\t\t\ttop: 0px;\r\n\t\t\tleft: 0px;\r\n\t\t\twidth: 1440px;\r\n\t\t\theight: 4074px;\r\n\t\t\tbackground: #8A1634 0% 0% no-repeat padding-box;\r\n\t\t}\r\n\t\t#layer {\r\n\t\t\twidth: 600px;\r\n\t\t\tposition: absolute;\r\n\t\t\tfloat: left;    \r\n\t\t\tmargin: 75px 0 0 -135px;\r\n\t\t\topacity: 0.52;\t\t\r\n\t\t}\r\n\t\t#layer_out {\r\n\t\t\twidth: 600px;\r\n\t\t\tposition: absolute;\r\n\t\t\tfloat: left;    \r\n\t\t\tmargin: 75px 0 0 770px;\r\n\t\t\topacity: 0.52;\t\t\r\n\t\t}\r\n\t\t#IPH_evt{\r\n\t\t\r\n\t\t}\r\n\t\t#borderLeft {\r\n\t\t\r\n\t\t\tposition: fixed;\r\n\t\t\tfloat: left;    \r\n\t\t\tmargin: 75px 0 0 0px;\r\n\t\t\topacity: 0.92;\t\r\n\t\t\tdisplay: none; \t\r\n\t\t}\r\n\t\t#borderRight {\r\n\t\t\tposition: fixed;\r\n\t\t\tfloat: left;    \r\n\t\t\tmargin: 75px 0 0 0px;\r\n\t\t\topacity: 0.92;\t\r\n\t\t\tdisplay: none; \t\r\n\t\t}\r\n\t\t#middle_border {\r\n\t\t\tposition: static;\r\n\t\t\tfloat: right;    \r\n\t\t\tmargin: 75px 3900px 0 -135px;\r\n\t\t\topacity: 0.52;\r\n\t\t\tdisplay:none; \t\r\n\t\t}\t\t\r\n\t\t.figcaption{\r\n\t\t}\r\n\t\t\r\n\t\t#inside{\r\n\t\t\tposition: absolute;\r\n\t\t\theight: 1900px;\r\n\t\t\ttop: 34px;\r\n\t\t\tleft: 14%;right\t: 14%;\r\n\t\t\tbackground: #ffffff 0% 0% no-repeat padding-box;\r\n\t\t\tborder-radius: 10px 10px 10px 10px;\r\n\t\t\topacity: 1;\r\n\t\t}\r\n\t\t\r\n\t\t#overside{\r\n\t\t\tposition: absolute;\r\n\t\t\theight: 1909px;\r\n\t\t\twidth: 1043px;\r\n\t\t\tleft: 0px;\r\n\t\t\ttop: 2044px;\r\n\t\t\tbackground: #ffffff 0% 0% no-repeat padding-box;\r\n\t\t\tborder-radius: 10px 10px 10px 10px;\r\n\t\t\topacity: 1;\r\n\t\t}\r\n\t</style>\r\n\r\n</head>\r\n<body>\r\n\r\n\t<!-- CSS Z0N3 -->\r\n    <style type=\"text/css\">\r\n        #mapCurrent {\r\n            height: 22vw;\r\n            width: 80%;\r\n        }\r\n\t\t.select {\r\n\t\t\tbackground: transparent url('assets/Icon ionic-ios-arrow-down.png') 0% 0% no-repeat padding-box;\r\n\t\t}\r\n\t\t#layer {\r\n\t\t\twidth: 600px;\r\n\t\t\tposition: absolute;\r\n\t\t\tfloat: left;    \r\n\t\t\tmargin: 75px 0 0 -135px;\r\n\t\t\topacity: 0.52;\t\t\r\n\t\t}\r\n\t\t#borderLeft {\r\n\t\t\tposition: fixed;\r\n\t\t\tfloat: left;    \r\n\t\t\tmargin: 75px 0 0 0px;\r\n\t\t\topacity: 0.92;\t\r\n\t\t\tdisplay: none; \t\r\n\t\t}\r\n\t\t#borderRight {\r\n\t\t\tposition: fixed;\r\n\t\t\tfloat: left;    \r\n\t\t\tmargin: 75px 0 0 0px;\r\n\t\t\topacity: 0.92;\t\r\n\t\t\tdisplay: none; \t\r\n\t\t}\r\n\t</style>\r\n<div class=\"modal fade\" id =\"tenant-modal\" tabindex=\"-1\" data-backdrop=\"static\" data-keyboard=\"false\" role=\"dialog\">\r\n\t<img src=\"assets/layer_zero.png\" alt=\"background\" width=\"700\" height=\"700\" id=\"layer\"/>\r\n\t<img src=\"assets/layer_zero.png\" alt=\"background\" width=\"700\" height=\"700\" id=\"layer_out\"/>\r\n\t<img src=\"assets/back_l.png\" alt=\"background_zero\" width=\"700\" height=\"700\" id=\"borderLeft\"/>\r\n\t<img src=\"assets/back_r.png\" alt=\"background_zero\" width=\"700\" height=\"700\" id=\"borderRight\"/>\t\t\r\n\r\n    <div class=\"modal-header\" id=\"opera\">\r\n    </div>\r\n    <div class=\"container top-buffer-submenu\" id=\"3vt\">\r\n\t<input type=\"hidden\" name=\"formID\" value=\"40754031698357\" />\r\n\t<div class=\"form-all\">\r\n\t<!-- glob4l -->\r\n\t\r\n    <!-- <link href=\"assets/formCss.css\" rel=\"stylesheet\" type=\"text/css\" /> -->\r\n    <link type=\"text/css\" rel=\"stylesheet\" href=\"http://localhost:11261/Content/part/assets/formCss.css\" />\r\n\t<link type=\"text/css\" rel=\"stylesheet\" href=\"http://localhost:11261/Content/part/assets/file-style.css\" />\r\n\t<link type=\"text/css\" rel=\"stylesheet\" href=\"http://localhost:11261/Content/part/assets/stylo.css\" /> \r\n    <link type = 'text/css' media = 'print' rel = 'stylesheet' href = 'http://localhost:11261/Content/part/assets/machine.css'  />\r\n   \r\n    <div class=\"cont\">\r\n      <input type=\"text\" id=\"input_language\" name=\"input_language\" style=\"display:none\" />\r\n      <div class=\"language-dd\" id=\"langDd\" style=\"display:none\">\r\n        <div class=\"dd-placeholder lang-emp\">\r\n          Language\r\n        </div>\r\n        <ul class=\"lang-list dn\" id=\"langList\">\r\n          <li data-lang=\"en\" class=\"en\">\r\n            English (US)\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n<div class=\"modal-content\" id =\"inside\" class=\"page-border\">\r\n    <ul class=\"form-section page-section\" id=\"IPH_main\">\r\n      <li id=\"cid_1\" class=\"form-input-wide\" data-type=\"control_head\">\r\n        <div class=\"form-header-group\">\r\n          <div class=\"header-text\">\r\n            <h2 id=\"header_1\" class=\"form-header\">\r\n              Informe  Policial Homologado\r\n            </h2>\r\n            <div id=\"subHeader_1\" class=\"form-subHeader\">\r\n              <!-- To report an incident, please provide the following information -->\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </li>\r\n\t  \r\n\t  <li class=\"form-line\" data-type=\"control_fullname\" id=\"id_4\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_plate\" for=\"plate\"> No. de referencia </label>\r\n        <div id=\"content_plate\" class=\"form-input jf-required\">\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_ref_0\" onkeyup=\"movetoNext(this, 'input_ref_1', event)\"  maxlength = \"1\"/>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_ref_1\" onkeyup=\"movetoNext(this, 'input_ref_2', event)\" maxlength = \"1\"/>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_ref_2\" onkeyup=\"movetoNext(this, 'input_ref_3', event)\" maxlength = \"1\"/>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_ref_3\" onkeyup=\"movetoNext(this, 'input_ref_4', event)\" maxlength = \"1\"/>\r\n          </span>                                                                                                 \r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">                                    \r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_ref_4\" onkeyup=\"movetoNext(this, 'input_ref_5', event)\" maxlength = \"1\"/>\r\n          </span>                                                                                                 \r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">                                    \r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_ref_5\" onkeyup=\"movetoNext(this, 'input_ref_6', event)\" maxlength = \"1\"/>\r\n          </span>                                                                                                 \r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">                                    \r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_ref_6\" onkeyup=\"movetoNext(this, 'input_ref_7', event)\" maxlength = \"1\"/>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_ref_7\" maxlength = \"1\"/>\r\n          </span>\r\n        </div>\r\n      </li>\r\n\r\n      \r\n\t\t<li class=\"form-line allowTime form-field-hidden\" data-type=\"control_datetime\" id=\"id_113\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_3\" for=\"input_37\"> Fecha del incidente </label>\r\n\t\t\t<div id=\"cid_3\" class=\"form-input jf-required\">\r\n\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t<input class=\"currentDate form-textbox validate[limitDate]\" id=\"input_d\" name=\"q3_dateAnd[day]\" type=\"tel\" size=\"2\" maxlength=\"2\" />\r\n\t\t\t\t<span class=\"date-separate\">\r\n\t\t\t\t\t&nbsp;-\r\n\t\t\t\t</span>\r\n\t\t\t\t<label class=\"form-sub-label\" for=\"day_3\" id=\"sublabel_day\" style=\"min-height: 13px;\"> Dia </label>\r\n\t\t\t</span>\r\n\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t<input class=\"form-textbox validate[limitDate]\" id=\"input_m\" name=\"q3_dateAnd[month]\" type=\"tel\" size=\"6\" maxlength=\"10\"/>\r\n\t\t\t\t<span class=\"date-separate\">\r\n\t\t\t\t&nbsp;-\r\n\t\t\t\t</span>\r\n\t\t\t\t<label class=\"form-sub-label\" for=\"month_3\" id=\"sublabel_month\" style=\"min-height: 13px;\"> Mes </label>\r\n\t\t\t</span>\r\n\t\t\t\r\n\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t<input class=\"form-textbox validate[limitDate]\" id=\"input_y\" name=\"q3_dateAnd[year]\" type=\"tel\" size=\"4\" maxlength=\"4\" />\r\n\t\t\t\t<label class=\"form-sub-label\" for=\"year_3\" id=\"label_y\" style=\"min-height: 13px;\"> Anio </label>\r\n\t\t\t</span>\r\n\t\t\t<span style='white-space: nowrap; display: inline-block;' class='allowTime-container'>\r\n            <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t<div id=\"at_3\">\r\n\t\t\t\t-\r\n\t\t\t\t</div>\r\n\t\t\t\t<label class=\"form-sub-label\" for=\"at_3\" style=\"min-height: 13px;\">  </label>\r\n            </span>\r\n            <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n              <select class=\"currentTime time-dropdown form-dropdown validate[limitDate]\" id=\"input_hh\" name=\"q3_dateAnd[hour]\">\r\n                <option>  </option>\r\n                <%--<option selected value=\"12\"> 12 </option>--%>\r\n              </select>\r\n              <span class=\"date-separate\">\r\n                &nbsp;:\r\n              </span>\r\n              <label class=\"form-sub-label\" for=\"hour_3\" id=\"sublabel_hour\" style=\"min-height: 13px;\"> Hora </label>\r\n            </span>\r\n            <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n              <select class=\"time-dropdown form-dropdown validate[limitDate]\" id=\"input_mm\" name=\"q3_dateAnd[min]\">\r\n                <option>  </option>\r\n                <option value=\"1\"> 1 </option> <option value=\"2\"> 2 </option> <option value=\"3\"> 3 </option> <option value=\"4\"> 4 </option> <option value=\"5\"> 5 </option>\r\n                <option value=\"6\"> 6 </option> <option value=\"7\"> 7 </option> <option value=\"8\"> 8 </option> <option value=\"9\"> 9 </option> <option value=\"10\"> 10 </option>\r\n                <option value=\"11\"> 11 </option> \r\n                <option value=\"12\"> 12 </option> \r\n                <option value=\"13\"> 13 </option> \r\n                <option value=\"14\"> 14 </option> \r\n                <option value=\"15\"> 15 </option>\r\n                <option value=\"16\"> 16 </option> \r\n                <option value=\"17\"> 17 </option> \r\n                <option value=\"18\"> 18 </option> \r\n                <option value=\"19\"> 19 </option> \r\n                <option value=\"20\"> 20 </option>\r\n                <option value=\"21\"> 21 </option> \r\n                <option value=\"22\"> 22 </option> \r\n                <option value=\"23\"> 23 </option> \r\n                <option value=\"24\"> 24 </option> \r\n                <option value=\"25\"> 25 </option>\r\n                <option value=\"26\"> 26 </option> \r\n                <option value=\"27\"> 27 </option> \r\n                <option value=\"28\"> 28 </option> \r\n                <option value=\"29\"> 29 </option> \r\n                <option value=\"30\"> 30 </option>\r\n                <option value=\"31\"> 31 </option> \r\n                <option value=\"32\"> 32 </option> \r\n                <option value=\"33\"> 33 </option> \r\n                <option value=\"34\"> 34 </option> \r\n                <option value=\"35\"> 35 </option>\r\n                <option value=\"36\"> 36 </option> \r\n                <option value=\"37\"> 37 </option> \r\n                <option value=\"38\"> 38 </option> \r\n                <option value=\"39\"> 39 </option> \r\n                <option value=\"40\"> 40 </option>\r\n                <option value=\"41\"> 41 </option> \r\n                <option value=\"42\"> 42 </option> \r\n                <option value=\"43\"> 43 </option> \r\n                <option value=\"44\"> 44 </option> \r\n                <option value=\"45\"> 45 </option>\r\n                <option value=\"46\"> 46 </option> \r\n                <option value=\"47\"> 47 </option> \r\n                <option value=\"48\"> 48 </option> \r\n                <option value=\"49\"> 49 </option>\r\n                <option value=\"50\"> 50 </option>\r\n                <option value=\"51\"> 51 </option> \r\n                <option value=\"52\"> 52 </option> \r\n                <option value=\"53\"> 53 </option> \r\n                <option value=\"54\"> 54 </option> \r\n                <option value=\"55\"> 55 </option>\r\n                <option value=\"56\"> 56 </option> \r\n                <option value=\"57\"> 57 </option> \r\n                <option value=\"58\"> 58 </option> \r\n                <option value=\"59\"> 59 </option>\r\n              </select>\r\n              <label class=\"form-sub-label\" for=\"min_3\" id=\"sublabel_minutes\" style=\"min-height: 13px;\"> Minutos </label>\r\n            </span>\r\n          <!-- <span class=\"form-sub-label-container\" style=\"vertical-align: top;\"> -->\r\n            <!-- <img class=\"showAutoCalendar\" alt=\"Pick a Date\" id=\"input_3_pick\" src=\"https://cdn.jotfor.ms/images/calendar.png\" style=\"vertical-align:middle;\" /> -->\r\n            <!-- <label class=\"form-sub-label\" for=\"input_3_pick\" style=\"min-height: 13px;\">  </label> -->\r\n          <!-- </span> -->\r\n        </div>\r\n\t\t</li>\r\n      \r\n      <li class=\"form-line\" data-type=\"control_fullname\" id=\"id_4\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_4\" for=\"input_4\"> Puesto a disposici&oacute;n por </label>\r\n        <div id=\"cid_4\" class=\"form-input jf-required\">\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox validate[required] \" type=\"text\" size=\"15\" name=\"q4_incidentReported[first]\" id=\"first\" />\r\n            <label class=\"form-sub-label\" for=\"first_4\" id=\"sublabel_first\" style=\"min-height: 13px;\"> Nombre </label>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"15\" name=\"q4_incidentReported[last]\" id=\"last\" />\r\n            <label class=\"form-sub-label\" for=\"last_4\" id=\"sublabel_last\" style=\"min-height: 13px;\"> Apellido Paterno </label>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"15\" name=\"q4_incidentReported[last]\" id=\"second\" />\r\n            <label class=\"form-sub-label\" for=\"last_4\" id=\"sublabel_second\" style=\"min-height: 13px;\"> Apellido Materno </label>\r\n          </span>\r\n        </div>\r\n      </li>\r\n\t  <!-- c4se police -->\r\n      <li class=\"form-line jf-required\" data-type=\"control_checkbox\" id=\"id_institute\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_ins\" for=\"input_ins\">\r\n          Institución policiaca\r\n        </label>\r\n\t\t<!-- class=\"form-multiple-column\"  -->\r\n          <div id=\"cid_institute\" class=\"form-input jf-required\" data-columnCount=\"3\">\r\n            <span class=\"form-radio-item\" style=\"clear:left;\">\r\n\t\t\t\t<span class=\"dragger-item\"></span>\r\n\t\t\t\t<input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_goverment\" name=\"police\" value=\"0\" style=\"min-height: 13px;\"/>\r\n\t\t\t\t<label id=\"label_institute_0\" for=\"institute_1\" style=\"margin-top: 4px;\"> Federal </label>\r\n            </span>\r\n\t\t\t<span class=\"form-radio-item\" >\r\n\t\t\t&nbsp;&nbsp;&nbsp;\r\n              <span class=\"dragger-item\"></span>\r\n              <input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_policeCity\" name=\"police\" value=\"1\"/>\r\n              <label id=\"label_institute_\" for=\"institute_1\" style=\"margin-top: 4px;\"> Estatal </label>\r\n            </span>\r\n\t\t\t<span class=\"form-radio-item\" >\r\n\t\t\t&nbsp;&nbsp;&nbsp;\r\n              <span class=\"dragger-item\"></span>\r\n              <input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_localPolice\" name=\"police\" value=\"1\"/>\r\n              <label id=\"label_institute_2\" for=\"institute_1\" style=\"margin-top: 4px;\"> Municipal </label>\r\n            </span>\r\n            <span class=\"form-radio-item\" style=\"min-height: 13px;\">\r\n\t\t\t&nbsp;&nbsp;&nbsp;<span class=\"dragger-item\"></span>\r\n              <input type=\"radio\" class=\"form-radio \" name=\"police\" id=\"input_iother\" value=\"other\" />\r\n              <input type=\"text\" class=\"form-radio-other-input form-textbox\" name=\"institute_3[other]\" placeholder=\"Otra\" size=\"15\" id=\"institute_3_other\" />\r\n              <br />\r\n            </span>\r\n          </div>\r\n      </li>\r\n      \r\n      <li class=\"form-line\" data-type=\"control_email\" id=\"id_11\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_111\" for=\"input_111\"> Cargo / grado </label>\r\n        <div id=\"cid_11\" class=\"form-input form-label-left jf-required\">\r\n            <select class=\"form-dropdown validate[required]\" style=\"width:204px;\" name=\"q8_address8[country]\" id=\"input_8_country\">\r\n              <option value=\"\" selected> Favor de elegir</option>\r\n              <option value=\"Oficer\"> Oficial </option>        \r\n              <option value=\"Judge\"> Juez </option>        \r\n\t\t\t</select>\r\n\t\t</div>\t\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\tUnidad \t\t\r\n\t\t\t&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n\t\t\t<input type=\"checkbox\" id=\"flag_unit\" /> \r\n\t\t\t<br/> \r\n\t\t\t<input class=\"form-textbox\" type=\"text\" size=\"13\" name=\"unit_group\" id=\"input_unit\" />\r\n\t\t</label>       \r\n\t\t   \r\n        \r\n\t  </li>  \r\n\t  \r\n\t  <!-- <li class=\"form-line\" data-type=\"control_email\" id=\"id_11\">\t   -->\r\n\t\t<!-- <label class=\"form-label form-label-right form-label-auto\"> -->\r\n\t\t\t\t<!-- <!-- <input type=\"checkbox\" id=\"pixel\"> -->\r\n\t\t\t\t<!-- Documentaci&oacute;n complementaria -->\r\n\t\t\t\r\n\t\t<!-- </label> -->\r\n\t\t<!-- <div class=\"form-input form-label-right\"> -->\r\n\t\t\t<!-- <input id=\"file-01\" type=\"file\" size=\"3\"/> -->\r\n\t\t<!-- </div> -->\r\n\t\t\t\r\n      <!-- </li> -->\r\n      \r\n\t  <li class=\"form-line\" data-type=\"control_widget\" id=\"id_37\">\r\n        <div id=\"cid_37\" class=\" jf-required\">\r\n          <div style=\"width:100%; text-align:Left;\">\r\n            <div class=\"direct-embed-widgets \" data-type=\"direct-embed\" style=\"width:1px;height: 1px;\">\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </li>\r\n      <li id=\"cid_28\" class=\"form-input-wide\" data-type=\"control_pagebreak\">\r\n        <div class=\"form-pagebreak\" id=\"btn_zone0\">\r\n          <div class=\"form-pagebreak-back-container\">\r\n            <!-- <button type=\"button\" class=\"form-pagebreak-back \" id=\"form-pagebreak-back_0\"> -->\r\n              <!-- Regresar -->\r\n            <!-- </button> -->\r\n          </div>\r\n      </li>\r\n\t  <div style=\"display:none;\" >\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t\t</br></br></br></br>\r\n\t  </div>\r\n    </ul>\r\n    <ul class=\"form-section page-section\" style=\"display:none;\" id=\"IPH_evt\">\r\n\r\n      <li id=\"cid_8\" class=\"form-input-wide\" data-type=\"control_head\">\r\n        <div class=\"form-header-group\">\r\n          <div class=\"header-text\">\r\n            <h2 id=\"header_8\" class=\"form-header\">\r\n             Informaci&oacute;n del evento\r\n            </h2>\r\n            <div id=\"subHeader_8\" class=\"form-subHeader\">\r\n              Datos del evento\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </li>\r\n\t  \r\n\t  <!-- xxxx -->\r\n\t  \r\n\t  \r\n      <li class=\"form-line\" data-type=\"control_checkbox\" id=\"id_involve\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_32\" for=\"input_32\"> Origen </label>\r\n        <div id=\"cid_32\" class=\"form-input jf-required\">\r\n\t\t\t<div class=\"form-single-column\">\r\n\t\t\t\t<span class=\"form-checkbox-item\" style=\"clear:left;\">\r\n\t\t\t\t\t<span class=\"form-sub-label-container dragger-item\" style=\"vertical-align: top;\">\r\n\t\t\t\t\t\t<select class=\"currentTime time-dropdown form-dropdown validate[limitDate]\" id=\"hour_3\" name=\"q3_dateAnd[hour]\" style=\"min-height: 26px;\">\r\n\t\t\t\t\t\t\t<option> Solo uno </option>\r\n\t\t\t\t\t\t\t<option value=\"0\"> Denuncia </option>\r\n\t\t\t\t\t\t\t<option value=\"1\"> Llamada </option>\r\n\t\t\t\t\t\t\t<option value=\"2\"> Flagrancia </option>\r\n\t\t\t\t\t\t</select>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t\t\r\n\t\t        <div data-type=\"control_checkbox\" id=\"id_32\"> \r\n\t\t\t\t\t&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n\t\t\t\t\t<input type=\"checkbox\" class=\"form-checkbox\" id=\"flag_phone\" name=\"q32_input32[]\" value=\"Report anonymously\" />\r\n\t\t\t\t\t<label for=\"phon3\"> Proporciono tel. </label>\r\n\t\t\t\t</div>\r\n                <div id=\"phone_zone\" class=\"form-input jf-required\"> \r\n\t\t\t\t\t&nbsp;&nbsp;&nbsp;&nbsp;\r\n\t\t\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\"> \r\n\t\t\t\t\t\t<input class=\"form-textbox validate[required]\" type=\"tel\" name=\"q12_phoneor[area]\" id=\"input_12_area\" size=\"3\"/> \r\n\t\t\t\t\t\t<span class=\"phone-separate\"> \r\n\t\t\t\t\t\t&nbsp;- \r\n\t\t\t\t\t\t</span> \r\n\t\t\t\t\t\t<label class=\"form-sub-label\" for=\"input_12_area\" id=\"sublabel_area\" style=\"min-height: 13px;\"> lada </label> \r\n\t\t\t\t\t</span> \r\n\t\t\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\"> \r\n\t\t\t\t\t\t<input class=\"form-textbox validate[required]\" type=\"tel\" name=\"q12_phoneor[phone]\" id=\"input_12_phone\" size=\"13\"/> \r\n\t\t\t\t\t\t<label class=\"form-sub-label\" for=\"input_12_phone\" id=\"sublabel_phone\" style=\"min-height: 12px;\"> n&uacute;mero </label> \r\n\t\t\t\t\t</span> \r\n\t\t\t\t</div> \r\n\t\t\t\t</span>\r\n\t\t\t</div>\r\n\t\t\t</div>\r\n\r\n        <!-- </div> -->\r\n      </li>\r\n        <!-- columbus_zon3 -->\r\n\t  <!-- id_8 -->\r\n      <li class=\"form-line jf-required form-field-hidden\" data-type=\"control_address\" id=\"id_88\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_88\" for=\"input_8\">\r\n          Direcci&oacute;n\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div id=\"cid_88\" class=\"form-input jf-required\">\r\n          <table summary=\"\" undefined class=\"form-address-table\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n            <tr>\r\n              <!-- <td colspan=\"2\"> -->\r\n                <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox validate[required]\" type=\"text\" name=\"addr_line1\" id=\"input_street\" size=\"24\"/>\r\n                  <label class=\"form-sub-label\" for=\"input_8_addr_line1\"> Calle </label>\r\n                </span>\r\n\t\t\t\t<span class=\"phone-separate\">\r\n\t\t\t\t&emsp;\r\n\t\t\t\t</span>\r\n\t\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox\" type=\"text\" name=\"addr_line2\" id=\"input_streetRef\" size=\"24\"/>\r\n                  <label class=\"form-sub-label\" for=\"input_8_addr_line2\"> Referencia </label>\r\n                </span>\r\n              <!-- </td> -->\r\n            </tr>\r\n            <tr>\r\n              <td size=\"10\" >\r\n                <span class=\"form-sub-label-container\" style=\"verticalle=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox validate[required]\" type=\"text\" name=\"q8_address8[postal]\" id=\"input_zp\"/>\r\n                  <label class=\"form-sub-label\" for=\"input_8_postal\" id=\"label_zp\"> C&oacute;digo Postal </label>\r\n                </span>\r\n              </td>\r\n              <td>\r\n                <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox validate[required]\" type=\"text\" name=\"q8_address8[city]\" id=\"input_col\" size=\"21\" />\r\n                  <label class=\"form-sub-label\" for=\"input_8_city\" id=\"label_col\" style=\"min-height: 13px;\"> Colonia </label>\r\n                </span>\r\n              </td>\r\n            </tr>\r\n            <tr>\r\n\t\t\t\t<td>\r\n\t\t\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t\t<input class=\"form-textbox validate[required]\" type=\"text\" name=\"q8_address8[state]\" id=\"input_mun\" size=\"23\" />\r\n\t\t\t\t\t<label class=\"form-sub-label\" for=\"input_8_state\" id=\"label_mun\" style=\"min-height: 3px;\"> Alcaldia / Municipio </label>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</td>\r\n            \r\n            <td>\r\n\t\t\t<span class=\"phone-separate\">\r\n              &emsp;\r\n            </span>\r\n                <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <select class=\"form-dropdown validate[required]\" defaultcountry=\"\" name=\"q8_address8[country]\" id=\"input_8_country\">\r\n                    <option value=\"\"> Selecciona el estado </option>\r\n                    <option value=\"1\"> Aguascalientes </option>\r\n                    <option value=\"2\"> Baja California </option>\r\n                    <option value=\"3\"> Baja California Sur </option>\r\n                    <option value=\"4\"> Campeche </option>\r\n                    <option value=\"5\"> Chiapas </option>\r\n                    <option value=\"6\"> Chihuahua </option>\r\n                    <option value=\"7\"> Coahuila </option>\r\n                    <option value=\"8\"> Colima </option>\r\n                    <option value=\"9\" selected> CDMX </option>\r\n                    <option value=\"10\"> Durango </option>\r\n                    <option value=\"11\"> Guanajuato </option>\r\n                    <option value=\"12\"> Guerrero </option>\r\n                    <option value=\"13\"> Hidalgo </option>\r\n                    <option value=\"14\"> Jalisco </option>\r\n                    <option value=\"15\"> M&eacute;xico </option>\r\n                    <option value=\"16\"> Michoac&aacute;n </option>\r\n                    <option value=\"17\"> Morelos </option>\r\n                    <option value=\"18\"> Nayarit </option>\r\n                    <option value=\"19\"> Nuevo Le&oacute;n </option>\r\n                    <option value=\"20\"> Oaxaca </option>\r\n                    <option value=\"21\"> Puebla </option>\r\n                    <option value=\"22\"> Quer&eacute;taro </option>\r\n                    <option value=\"23\"> Quintana Roo </option>\r\n                    <option value=\"24\"> San Luis Potos&iacute; </option>\r\n                    <option value=\"25\"> Sinaloa </option>\r\n                    <option value=\"26\"> Sonora </option>\r\n                    <option value=\"27\"> Tabasco </option>\r\n                    <option value=\"28\"> Tamaulipas </option>\r\n                    <option value=\"29\"> Tlaxcala </option>\r\n                    <option value=\"30\"> Veracruz </option>\r\n                    <option value=\"31\"> Yucat&aacute;n </option>\r\n                    <option value=\"32\"> Zacatecas </option>\r\n                  </select>\r\n                  <label class=\"form-sub-label\" for=\"input_8_country\" id=\"sublabel_8_country\" style=\"min-height: 13px;\"> Estado </label>\r\n                </span>\r\n              </td>\r\n            </tr>\r\n          </table>\r\n        </div>\r\n      \r\n\t  </li>\r\n\t  \t  \r\n\t  <!-- cupon -->\r\n      <!-- <li class=\"form-line jf-required form-field-hidden\" data-type=\"control_textbox\" id=\"id_110\"> -->\r\n\t\t<!-- <label class=\"form-label form-label-left form-label-auto\" id=\"label_10\" for=\"input_10\"> -->\r\n          <!-- Datos del contacto -->\r\n          <!-- <span class=\"form-required\"> -->\r\n            <!-- * -->\r\n          <!-- </span> -->\r\n        <!-- </label> -->\r\n        <!-- <div id=\"cid_110\" class=\"form-input jf-required\"> -->\r\n          <!-- <input type=\"text\" class=\" form-textbox validate[required, Email]\" data-type=\"input-textbox\" id=\"input_10\" name=\"q10_emailAddress\" size=\"20\" value=\"\" /> -->\r\n        <!-- </div> -->\r\n      <!-- </li> -->\r\n\t  \r\n\t  <!-- id_33 -->\r\n      <li class=\"form-line form-field-hidden\" data-type=\"control_textarea\" id=\"id_333\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_333\" for=\"input_333\"> Narrativa de los hechos </label>\r\n        <div id=\"cid_333\" class=\"form-input jf-required\">\r\n          <textarea id=\"input_narrative\" class=\"form-textarea\" name=\"q33_wheneverPossible33\" cols=\"36\" rows=\"7\"></textarea>\r\n\t\t  <label class=\"form-sub-label\" for=\"input_14\" style=\"min-height: 13px;\"> Trata de ser exacto (con los detalles) </label>\r\n        </div>\r\n      </li>\r\n      <li class=\"form-line jf-required form-field-hidden\" data-type=\"control_address\" id=\"id_88\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_88\" for=\"input_8\">\r\n          Coordenadas\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div id=\"cid_88\" class=\"form-input jf-required\">\r\n            <tr>\r\n              <!-- <td colspan=\"2\"> -->\r\n                <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox validate[required]\" type=\"text\" name=\"addr_line1\" id=\"input_lat\" size=\"17\"/>\r\n                  <label class=\"form-sub-label\" for=\"input_8_addr_line1\"> Latitud </label>\r\n                </span>\r\n\t\t\t\t<span class=\"phone-separate\">\r\n\t\t\t\t&amp; &nbsp;\r\n\t\t\t\t</span>\r\n\t\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox\" type=\"text\" name=\"addr_line2\" id=\"input_long\" size=\"17\"/>\r\n                  <label class=\"form-sub-label\" for=\"input_8_addr_line2\"> longitud </label>\r\n                </span>\r\n              <!-- </td> -->\r\n            </tr>\r\n\t\t</div>\r\n\t  </li>\r\n      <li class=\"form-line form-field-hidden\" data-type=\"control_textbox\" id=\"id_suc\">\r\n        <div id=\"mapCurrent\" ></div>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t<!-- ref = {mapContainer} -->\r\n        <pre id='coordinates' class='coordinates'></pre>\t\t\r\n        <!-- <label class=\"form-label form-label-left form-label-auto\" id=\"label_7\" for=\"input_7\"> Relación </label> -->\r\n      </li>\r\n      \r\n    </ul>\r\n\t<div/>\r\n\t<div class=\"modal-content\" id =\"overside\" class=\"page-border\">\r\n\r\n    <ul class=\"form-section page-section\" style=\"display:none;\" id=\"IPH_dtn\">\r\n\r\n      <li id=\"cid_13\" class=\"form-input-wide\" data-type=\"control_head\">\r\n        <div class=\"form-header-group\">\r\n          <div class=\"header-text\">\r\n            <h2 id=\"header_13\" class=\"form-header\">\r\n             Informaci&oacute;n del detenido\r\n            </h2>\r\n            <div id=\"subHeader_13\" class=\"form-subHeader\">\r\n              Datos del ignoto\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </li>\r\n\t  <li class=\"form-line\" data-type=\"control_fullname\" id=\"id_4\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_4\" for=\"input_4\"> Detenido/ignoto </label>\r\n        <div id=\"cid_4\" class=\"form-input jf-required\">\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox validate[required] \" type=\"text\" size=\"15\" name=\"q4_incidentReported[first]\" id=\"input_name\" />\r\n            <asp:label class=\"form-sub-label\" for=\"first_4\" id=\"label_first\" style=\"min-height: 13px;\" runat=\"server\"> Nombre de pila </asp:label>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"15\" name=\"q4_incidentReported[last]\" id=\"input_first\" />\r\n            <asp:label class=\"form-sub-label\" for=\"last_4\" id=\"dtn_last\" style=\"min-height: 13px;\" runat=\"server\"> Apellido Paterno </asp:label>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"15\" name=\"q4_incidentReported[last]\" id=\"input_last\" />\r\n            <asp:label class=\"form-sub-label\" for=\"last_4\" id=\"dtn_second\" style=\"min-height: 13px;\" runat=\"server\"> Apellido Materno </asp:label>\r\n          </span>\r\n        </div>\r\n      </li>\r\n\t\t\r\n      <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_age\">\t  \r\n        <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\t\t\r\n            <label class=\"form-label form-label-left form-label-auto\" id=\"label_433\" for=\"input_433\">\r\n\t\t    \tEdad\r\n\t\t    </label>\r\n            <div id=\"cid_14\" class=\"form-input \">\r\n                <select class=\"form-dropdown\" defaultcountry=\"\" name=\"q8_address8[country]\" id=\"i_age\">\r\n                    <option value=\"\" selected> Selecciona la edad </option>\r\n                    <option value=\"0\"> Menor de edad </option>\r\n                    <option value=\"1\"> 18 </option>\r\n                    <option value=\"2\"> 19 </option>\r\n                    <option value=\"3\"> 20 </option>\r\n                    <option value=\"4\"> 21 </option>\r\n                    <option value=\"5\"> 22 </option>\r\n                    <option value=\"6\"> 23 </option>\r\n                    <option value=\"7\"> 24 </option>\r\n                    <option value=\"8\"> 25 </option>\r\n                    <option value=\"9\"> 26 </option>\r\n                    <option value=\"10\"> 27 </option>\r\n                    <option value=\"11\"> 28 </option>\r\n                    <option value=\"12\"> 29 </option>\r\n                    <option value=\"13\"> 30 </option>\r\n                    <option value=\"14\"> 31 </option>\r\n                    <option value=\"15\"> 32 </option>\r\n                    <option value=\"16\"> 33 </option>\r\n                    <option value=\"17\"> 34 </option>\r\n\t\t\t  </select>\r\n\t\t    </div>   \r\n            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \r\n        </span>\r\n        <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <label class=\"form-label \" id=\"label_nickname\" for=\"lbl_nick\">\r\n\t\t    \tAlias\r\n\t\t    </label> &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;         \r\n           \r\n            <input class=\"form-textbox\" type=\"text\" id=\"input_nickname\" name=\"nickname\" size =\"12\"/> \r\n        </span>\r\n\r\n\t  </li>        \r\n\t\t\r\n      <!-- <li class=\"form-line form-field-hidden\" style=\"display:none;\" data-type=\"control_datetime\" id=\"id_3\"> -->\r\n      <li class=\"form-line allowTime form-field-hidden\" data-type=\"control_datetime\" id=\"id_borndate\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_41\" for=\"input_41\">\r\n          Proporciono fecha <br/> de nacimiento\r\n        </label>\r\n        <div id=\"id_41\" class=\"form-input jf-required\" data-columnCount=\"2\">\r\n            <span class=\"form-check-item\">\r\n              <input type=\"checkbox\" class=\"form-Check\" id=\"flag_birth\" name=\"revenge\" value=\"Yes\" />\r\n            </span>\r\n        </div>\r\n\t\t<div id=\"input_birth3\" class=\"form-input jf-required\">\r\n\t\t\t&nbsp;&nbsp;&nbsp;&nbsp;\r\n\t\t\t<td>\r\n\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t<input class=\"currentDate form-textbox validate[limitDate]\" id=\"day_inpugned\" name=\"q3_dateAnd[day]\" type=\"tel\" size=\"2\" data-maxlength=\"2\"/>\r\n\t\t\t\t<span class=\"date-separate\">\r\n\t\t\t\t&nbsp;-\r\n\t\t\t\t</span>\r\n\t\t\t\t<label class=\"form-sub-label\" for=\"day_3\" id=\"sublabel_day\" style=\"min-height: 13px;\"> Dia </label>\r\n\t\t\t</span>\r\n\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t<input class=\"form-textbox validate[limitDate]\" id=\"month_inpugned\" name=\"q3_dateAnd[month]\" type=\"tel\" size=\"2\" data-maxlength=\"2\"/>\r\n\t\t\t\t<span class=\"date-separate\">\r\n\t\t\t\t&nbsp;-\r\n\t\t\t\t</span>\r\n\t\t\t\t<label class=\"form-sub-label\" for=\"month_3\" id=\"sublabel_month\" style=\"min-height: 13px;\"> Mes </label>\r\n\t\t\t</span>\r\n\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t<input class=\"form-textbox validate[limitDate]\" id=\"year_inpugned\" name=\"q3_dateAnd[year]\" type=\"tel\" size=\"4\" data-maxlength=\"4\" value=\"2023\" />\r\n\t\t\t\t<label class=\"form-sub-label\" for=\"year_3\" id=\"sublabel_year\" style=\"min-height: 13px;\"> Año </label>\r\n\t\t\t</span>\r\n\t\t\t</td>\r\n\t\t\t<td>\r\n\t\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top; visibility: hidden;\">\r\n\t\t\t\t\t<img class=\"showAutoCalendar\" alt=\"Pick a Date\" id=\"input_3_pick\" src=\"https://cdn.jotfor.ms/images/calendar.png\" style=\"vertical-align:middle;\" />\r\n\t\t\t\t\t<label class=\"form-sub-label\" for=\"input_3_pick\" style=\"min-height: 13px;\">  </label>\r\n\t\t\t\t</span>\r\n\t\t\t</td>\r\n        </div>\r\n      </li>\r\n            \r\n\t  <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_433\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_433\" for=\"input_433\">\r\n          Sexo\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div class=\"form-radio-item\" data-columnCount=\"2\">\r\n\t\t\t<input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_male\" name=\"gender\" value=\"M\" />\r\n            <label id=\"label_input_m\" for=\"input_m\" style=\"margin-top:6px\"> Hombre </label>\r\n\t\t\t&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n            <input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_female\" name=\"gender\" value=\"F\" />\r\n            <label id=\"label_input_f\" for=\"input_f\" style=\"margin-top:6px\"> Mujer </label>\r\n\t\t</div>\t\t\r\n      </li>\r\n      \r\n      <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_433\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_433\" for=\"input_433\">\r\n          Nacionalidad\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div class=\"form-radio-item\" data-columnCount=\"2\">\r\n\t\t\t<input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_mexican\" name=\"nation\" />\r\n            <label id=\"label_input_national\" for=\"input_m\" style=\"margin-top:6px\"> Mexicana </label>\r\n\t\t\t&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n            <input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_forager\" name=\"nation\" />\r\n            <label id=\"label_input_non_national\" for=\"input_f\" style=\"margin-top:6px\"> Extranjera </label>\r\n\t\t</div>\t\t\r\n      </li>\r\n\t  \r\n      <!-- <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_20\"> -->\r\n        <!-- <label class=\"form-label form-label-left form-label-auto\" id=\"label_20\" for=\"input_20\"> -->\r\n          <!-- Tipo de documentaci&oacute;n -->\r\n        <!-- </label> -->\r\n        <!-- <div id=\"cid_20\" class=\"form-input jf-required\"> -->\r\n          <!-- <div class=\"form-multiple-column\" data-columnCount=\"2\"> -->\r\n\t\t  <!-- <div class=\"form-single-column\"> -->\r\n\t\t  <!-- <span class=\"form-checkbox-item\" style=\"clear:left;\"> -->\r\n              <!-- <span class=\"dragger-item\"style=\"min-height: 23px;\"> -->\r\n\t\t\t\t<!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_19_0\" name=\"q19_infectionVector19[]\" value=\"Spam/Phishing/Spear Phishing\"/> -->\r\n\t\t\t\t<!-- <label id=\"label_input_19_0\" for=\"input_19_0\" > INE </label>\t\t\t   -->\r\n\t\t\t  <!-- </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->\r\n\t\t\t  <!-- <span class=\"dragger-item\"style=\"min-height: 23px;\"> -->\r\n\t\t\t\t<!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_19_1\" name=\"q19_infectionVector19[]\" value=\"Phone scam (eg.: Fake Microsoft call)\" /> -->\r\n\t\t\t\t<!-- <label id=\"label_input_19_1\" for=\"input_19_1\"> Licencia </label> -->\r\n\t\t\t  <!-- </span> -->\r\n              <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->\r\n\t\t\t  <!-- <span class=\"dragger-item\"style=\"min-height: 23px;\"> -->\r\n\t\t\t\t<!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_19_2\" name=\"q19_infectionVector19[]\" value=\"Chat room (eg.: IRC/Forum/Newsgroup/Reddit/...)\" /> -->\r\n\t\t\t\t<!-- <label id=\"label_input_19_2\" for=\"input_19_2\"> Pasaporte </label> -->\r\n              <!-- </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->\r\n\t\t\t  <!-- <span class=\"dragger-item\"> -->\r\n\t\t\t\t<!-- <input type=\"checkbox\" class=\"form-checkbox-other form-checkbox\" name=\"q19_infectionVector19[other]\" id=\"check_other\" value=\"other\" />\t\t\t   -->\r\n\t\t\t\t<!-- <input type=\"text\" class=\"form-checkbox-other-input form-textbox\" name=\"q19_infectionVector19[other]\" placeholder=\"Otro\" size=\"15\" id=\"input_other\" /> -->\r\n            <!-- </span> -->\r\n            <!-- </span> -->\r\n\t\t\t<!-- </div> -->\r\n          <!-- </div> -->\r\n        <!-- </div> -->\r\n      <!-- </li> -->\r\n      \r\n\t  <!-- id_8 -->\r\n      <li class=\"form-line jf-required form-field-hidden\" data-type=\"control_address\" id=\"id_88\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_88\" for=\"input_8\">\r\n          Direcci&oacute;n\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div id=\"cid_88\" class=\"form-input jf-required\">\r\n          <table summary=\"\" undefined class=\"form-address-table\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n            <tr>\r\n              <!-- <td colspan=\"2\"> -->\r\n                <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox validate[required]\" type=\"text\" name=\"addr_street0\" id=\"input_street0\" size=\"24\"/>\r\n                  <label class=\"form-sub-label\" for=\"dtn_dir\"> Calle </label>\r\n                </span>\r\n\t\t\t\t<span class=\"phone-separate\">\r\n\t\t\t\t&emsp;\r\n\t\t\t\t</span>\r\n\t\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox\" type=\"text\" name=\"addr_line2\" id=\"addr_streetRef0\" size=\"24\"/>\r\n                  <label class=\"form-sub-label\" for=\"dtn_dir\"> Referencia </label>\r\n                </span>\r\n              <!-- </td> -->\r\n            </tr>\r\n            <tr>\r\n              <td size=\"10\" >\r\n                <span class=\"form-sub-label-container\" style=\"verticalle=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox validate[required]\" type=\"text\" name=\"q8_address8[postal]\" id=\"input_zp0\"/>\r\n                  <label class=\"form-sub-label\" for=\"input_8_postal\" id=\"label_zp\"> C&oacute;digo Postal </label>\r\n                </span>\r\n              </td>\r\n              <td>\r\n                <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <input class=\"form-textbox validate[required]\" type=\"text\" name=\"q8_address8[city]\" id=\"input_col0\" size=\"21\" />\r\n                  <label class=\"form-sub-label\" for=\"input_8_city\" id=\"label_col\" style=\"min-height: 13px;\"> Colonia </label>\r\n                </span>\r\n              </td>\r\n            </tr>\r\n            <tr>\r\n\t\t\t\t<td>\r\n\t\t\t\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n\t\t\t\t\t<input class=\"form-textbox validate[required]\" type=\"text\" name=\"q8_address8[state]\" id=\"input_mun0\" size=\"23\" />\r\n\t\t\t\t\t<label class=\"form-sub-label\" for=\"input_8_state\" id=\"label_mun\" style=\"min-height: 3px;\"> Alcaldia / Municipio </label>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</td>\r\n            \r\n            <td>\r\n\t\t\t<span class=\"phone-separate\">\r\n              &emsp;\r\n            </span>\r\n                <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n                  <select class=\"form-dropdown validate[required]\" defaultcountry=\"\" name=\"state[country]\" id=\"input_state0\">\r\n                    <option value=\"\"> Selecciona el estado </option>\r\n                    <option value=\"1\"> Aguascalientes </option>\r\n                    <option value=\"2\"> Baja California </option>\r\n                    <option value=\"3\"> Baja California Sur </option>\r\n                    <option value=\"4\"> Campeche </option>\r\n                    <option value=\"5\"> Chiapas </option>\r\n                    <option value=\"6\"> Chihuahua </option>\r\n                    <option value=\"7\"> Coahuila </option>\r\n                    <option value=\"8\"> Colima </option>\r\n                    <option value=\"9\" selected> CDMX </option>\r\n                    <option value=\"10\"> Durango </option>\r\n                    <option value=\"11\"> Guanajuato </option>\r\n                    <option value=\"12\"> Guerrero </option>\r\n                    <option value=\"13\"> Hidalgo </option>\r\n                    <option value=\"14\"> Jalisco </option>\r\n                    <option value=\"15\"> M&eacute;xico </option>\r\n                    <option value=\"16\"> Michoac&aacute;n </option>\r\n                    <option value=\"17\"> Morelos </option>\r\n                    <option value=\"18\"> Nayarit </option>\r\n                    <option value=\"19\"> Nuevo Le&oacute;n </option>\r\n                    <option value=\"20\"> Oaxaca </option>\r\n                    <option value=\"21\"> Puebla </option>\r\n                    <option value=\"22\"> Quer&eacute;taro </option>\r\n                    <option value=\"23\"> Quintana Roo </option>\r\n                    <option value=\"24\"> San Luis Potos&iacute; </option>\r\n                    <option value=\"25\"> Sinaloa </option>\r\n                    <option value=\"26\"> Sonora </option>\r\n                    <option value=\"27\"> Tabasco </option>\r\n                    <option value=\"28\"> Tamaulipas </option>\r\n                    <option value=\"29\"> Tlaxcala </option>\r\n                    <option value=\"30\"> Veracruz </option>\r\n                    <option value=\"31\"> Yucat&aacute;n </option>\r\n                    <option value=\"32\"> Zacatecas </option>\r\n                  </select>\r\n                  <label class=\"form-sub-label\" for=\"input_8_country\" id=\"sublabel_8_country\" style=\"min-height: 13px;\"> Estado </label>\r\n                </span>\r\n              </td>\r\n            </tr>\r\n          </table>\r\n        </div>\r\n      \r\n\t  <li class=\"form-line form-field-hidden\" style=\"display:none;\" data-type=\"control_checkbox\" id=\"id_19\">\r\n        <div id=\"cid_19\" class=\"form-input jf-required\">          \r\n\t\t  \r\n        </div>\r\n      </li>\r\n\r\n      <li class=\"form-line form-field-hidden\" style=\"display:none;\" data-type=\"control_textbox\" id=\"id_15\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_15\" for=\"input_15\"> Apodo </label>\r\n        <div id=\"cid_15\" class=\"form-input jf-required\">\r\n          <input type=\"text\" class=\" form-textbox validate[AlphaNumeric]\" data-type=\"input-textbox\" id=\"input_15\" name=\"q15_monetaryLoss15\" size=\"25\" value=\"\" />\r\n        </div>\r\n      </li>\r\n      <!-- <li class=\"form-line form-field-hidden\" style=\"display:none;\" data-type=\"control_checkbox\" id=\"id_16\"> -->\r\n        <!-- <label class=\"form-label form-label-left form-label-auto\" id=\"label_16\" for=\"input_16\"> Means of payment you used </label> -->\r\n        <!-- <div id=\"cid_16\" class=\"form-input jf-required\"> -->\r\n          <!-- <div class=\"form-single-column\"> -->\r\n            <!-- <span class=\"form-checkbox-item\" style=\"clear:left;\"> -->\r\n              <!-- <span class=\"dragger-item\"> -->\r\n              <!-- </span> -->\r\n              <!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_16_0\" name=\"q16_meansOf[]\" value=\"Cash/Cashier's Check\" /> -->\r\n              <!-- <label id=\"label_input_16_0\" for=\"input_16_0\"> Cash/Cashier's Check </label> -->\r\n            <!-- </span> -->\r\n            <!-- <span class=\"form-checkbox-item\" style=\"clear:left;\"> -->\r\n              <!-- <span class=\"dragger-item\"> -->\r\n              <!-- </span> -->\r\n              <!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_16_1\" name=\"q16_meansOf[]\" value=\"Check/Debit Card\" /> -->\r\n              <!-- <label id=\"label_input_16_1\" for=\"input_16_1\"> Check/Debit Card </label> -->\r\n            <!-- </span> -->\r\n            <!-- <span class=\"form-checkbox-item\" style=\"clear:left;\"> -->\r\n              <!-- <span class=\"dragger-item\"> -->\r\n              <!-- </span> -->\r\n              <!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_16_2\" name=\"q16_meansOf[]\" value=\"Credit Card\" /> -->\r\n              <!-- <label id=\"label_input_16_2\" for=\"input_16_2\"> Credit Card </label> -->\r\n            <!-- </span> -->\r\n            <!-- <span class=\"form-checkbox-item\" style=\"clear:left;\"> -->\r\n              <!-- <span class=\"dragger-item\"> -->\r\n              <!-- </span> -->\r\n              <!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_16_3\" name=\"q16_meansOf[]\" value=\"Money Order\" /> -->\r\n              <!-- <label id=\"label_input_16_3\" for=\"input_16_3\"> Money Order </label> -->\r\n            <!-- </span> -->\r\n            <!-- <span class=\"form-checkbox-item\" style=\"clear:left;\"> -->\r\n              <!-- <span class=\"dragger-item\"> -->\r\n              <!-- </span> -->\r\n              <!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_16_4\" name=\"q16_meansOf[]\" value=\"Wire Transfer\" /> -->\r\n              <!-- <label id=\"label_input_16_4\" for=\"input_16_4\"> Wire Transfer </label> -->\r\n            <!-- </span> -->\r\n            <!-- <span class=\"form-checkbox-item\" style=\"clear:left;\"> -->\r\n              <!-- <span class=\"dragger-item\"> -->\r\n              <!-- </span> -->\r\n              <!-- <input type=\"checkbox\" class=\"form-checkbox\" id=\"input_16_5\" name=\"q16_meansOf[]\" value=\"Online Payment (PayPal, uKash, MoneyPak, ...)\" /> -->\r\n              <!-- <label id=\"label_input_16_5\" for=\"input_16_5\"> Online Payment (PayPal, uKash, MoneyPak, ...) </label> -->\r\n            <!-- </span> -->\r\n            <!-- <span class=\"form-checkbox-item\" style=\"clear:left\"> -->\r\n              <!-- <input type=\"checkbox\" class=\"form-checkbox-other form-checkbox\" name=\"q16_meansOf[other]\" id=\"other_16\" value=\"other\" /> -->\r\n              <!-- <input type=\"text\" class=\"form-checkbox-other-input form-textbox\" name=\"q16_meansOf[other]\" data-otherHint=\"Other\" size=\"15\" id=\"input_16\" /> -->\r\n              <!-- <br /> -->\r\n            <!-- </span> -->\r\n          <!-- </div> -->\r\n        <!-- </div> -->\r\n      <!-- </li> -->\r\n\t  <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_17\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_17\" for=\"input_17\">\r\n          \r\n          ¿Presenta alg&uacute;n padecimiento?\r\n\t\t  <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t\t&nbsp;\r\n\t\t<label class=\"form-label form-label-left form-label-auto\" id=\"label_17\" for=\"input_17\">\r\n          ¿Pertenece a alg&uacute;n grupo vulnerable?\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>        \r\n        \r\n      </li>\r\n\t  \r\n\t  \t  \t  \r\n\t  <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_29\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_29\" for=\"input_29\">\r\n          ¿Cuenta con lesiones visibles?\r\n          \r\n\t\t  <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div id=\"cid_29\" class=\"form-input jf-required\">\r\n          <div class=\"form-multiple-column\" data-columnCount=\"2\">\r\n              <span class=\"dragger-item\">\r\n              </span>\r\n              <input type=\"radio\" class=\"form-radio validate[required]\" id=\"x_true\" name=\"data_pain\" value=\"Yes\" />\r\n              <label id=\"label_chck_\" for=\"input_00\"> Si </label>\r\n              <span class=\"dragger-item\">\r\n              </span>              \r\n              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n              <input type=\"radio\" class=\"form-radio validate[required]\" id=\"x_false\" name=\"data_pain\" value=\"No\" />\r\n              <label id=\"label_chck_\" for=\"input_00\"> No </label>\r\n\r\n          </div>\r\n        </div>\r\n      </li>\r\n\t  <!-- <li class=\"form-line form-field-hidden\" style=\"display:none;\" data-type=\"control_text\" id=\"id_38\"> -->\r\n        <!-- <div id=\"cid_38\" class=\"form-input-wide\"> -->\r\n          <!-- <div id=\"text_38\" class=\"form-html\"> -->\r\n            <!-- <p>Please refer to the following websites if you would also like to report this seperately:</p> -->\r\n            <!-- <p><a href=\"https://www.europol.europa.eu/content/report-cybercrime-online\" rel=\"nofollow\" target=\"_blank\">Report Cybercrime Online (EU)</a></p> -->\r\n            <!-- <p><a href=\"https://complaint.ic3.gov/default.aspx\" rel=\"nofollow\" target=\"_blank\">IC3 Complaint Referral Form (US)</a></p> -->\r\n            <!-- <p>In case you do not want to report this to a specific law enforcement agency seperately, just finish up this form. If you are willing, it is possible to share any information through Criminal Intelligence teams - this can be completely anonymous, similar to this form. <br /><br />Be sure to contact your CERT or local police department to ask if they have such a team or anonymous reporting possiblity (see also links above). <br /><br />You can find a list of CERTs here:<br /><a href=\"http://www.cert.org/incident-management/national-csirts/national-csirts.cfm\" rel=\"nofollow\" target=\"_blank\">CERTs by Country - Interactive Map <br /><br />List of National CSIRTs </a></p> -->\r\n            <!-- <p><a href=\"http://www.apcert.org/about/structure/members.html\" rel=\"nofollow\" target=\"_blank\">APCERT team members</a></p> -->\r\n          <!-- </div> -->\r\n        <!-- </div> -->\r\n      <!-- </li> -->    \r\n      \r\n\t  <li style=\"display:none\">\r\n        Should be Empty:\r\n        <input type=\"text\" name=\"website\" value=\"\" />\r\n      </li>\r\n    </ul>\r\n\t<ul class=\"form-section page-section\" style=\"display:none;\" id=\"IPH_vehicle\">\r\n\r\n      <li id=\"cid_13\" class=\"form-input-wide\" data-type=\"control_head\">\r\n        <div class=\"form-header-group\">\r\n          <div class=\"header-text\">\r\n            <h2 id=\"header_13\" class=\"form-header\">\r\n             Informaci&oacute;n del vehiculo\r\n            </h2>\r\n            <div id=\"subHeader_13\" class=\"form-subHeader\">\r\n              Datos del vehiculo\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </li>\r\n\t  <li class=\"form-line\" data-type=\"control_fullname\" id=\"id_4\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_plate\" for=\"plate\"> Placa / matricula </label>\r\n        <div id=\"content_plate\" class=\"form-input jf-required\">\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_plate_0\" onkeyup=\"movetoNext(this, 'input_plate_1', event)\"  maxlength = \"1\"/>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_plate_1\" onkeyup=\"movetoNext(this, 'input_plate_2', event)\" maxlength = \"1\"/>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_plate_2\" onkeyup=\"movetoNext(this, 'input_plate_3', event)\" maxlength = \"1\"/>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_plate_3\" onkeyup=\"movetoNext(this, 'input_plate_4', event)\" maxlength = \"1\"/>\r\n          </span>                                                                                                 \r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">                                    \r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_plate_4\" onkeyup=\"movetoNext(this, 'input_plate_5', event)\" maxlength = \"1\"/>\r\n          </span>                                                                                                 \r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">                                    \r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_plate_5\" onkeyup=\"movetoNext(this, 'input_plate_6', event)\" maxlength = \"1\"/>\r\n          </span>                                                                                                 \r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">                                    \r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_plate_6\" onkeyup=\"movetoNext(this, 'input_plate_7', event)\" maxlength = \"1\"/>\r\n          </span>\r\n          <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\r\n            <input class=\"form-textbox\" type=\"text\" size=\"1\" name=\"plate\" id=\"input_plate_7\" maxlength = \"1\"/>\r\n          </span>\r\n        </div>\r\n      </li>\r\n\t\t\r\n      <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_age\">\t  \r\n        <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">\t\t\r\n            <label class=\"form-label\" id=\"label_433\" for=\"input_433\">\r\n\t\t    \tMarca\r\n\t\t    </label>\r\n            <div id=\"cid_14\" class=\"form-input \">\r\n                <select class=\"form-dropdown\" defaultcountry=\"\" name=\"q8_address8[country]\" id=\"i_marca0\">\r\n                    <option value=\"\" selected> Selecciona la marca </option>\r\n                    <option value=\"0\"> Audi </option>\r\n                    <option value=\"1\"> BMW </option>\r\n                    <option value=\"17\"> Golf </option>\r\n\t\t\t  </select>\r\n\t\t\t&nbsp; &nbsp; &nbsp; &nbsp; \r\n\t\t    </div>   \r\n        </span>\r\n        <span class=\"form-sub-label-container\" style=\"vertical-align: top;\">            \r\n\t\t\t<label class=\"form-label \" id=\"label_nickname\" for=\"lbl_nick\">\r\n\t\t\t\tSubmarca\r\n\t\t    </label>\r\n            <div id=\"cid_14\" class=\"form-input \">\r\n                <select class=\"form-dropdown\" defaultcountry=\"\" name=\"q8_address8[country]\" id=\"i_model0\">\r\n                    <option value=\"\" selected> Selecciona la marca </option>\r\n                    <option value=\"0\"> Cupe </option>\r\n                    <option value=\"1\"> Hashback </option>\r\n                    <option value=\"17\"> Van </option>\r\n\t\t\t  </select>\r\n\t\t\t        &nbsp; &nbsp; &nbsp; &nbsp; \r\n\t\t    </div>\r\n        </span>\r\n\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">            \r\n\t\t\t<label class=\"form-label f\" id=\"label_nickname\" for=\"lbl_nick\">\r\n\t\t\t\tModelo\r\n\t\t    </label>\r\n            <div id=\"cid_14\" class=\"form-input form-label-auto\" >\r\n                <select class=\"form-dropdown\" defaultcountry=\"\" name=\"mdel\" id=\"i_model0\" style='width:59px;'>\r\n                    <option value=\"\" selected> A&ntilde;o </option>\r\n                    <option value=\"0\"> Bef0re </option>\r\n                    <option value=\"1\"> 2022 </option>\r\n                    <option value=\"17\"> 2023 </option>\r\n\t\t\t  </select>\r\n\t\t\t        &nbsp; &nbsp; &nbsp; &nbsp; \r\n\t\t    </div>   \r\n        </span>\r\n\r\n\t\t<span class=\"form-sub-label-container\" style=\"vertical-align: top;\">            \r\n\t\t\t<label class=\"form-label f\" id=\"label_nickname\" for=\"lbl_nick\">\r\n\t\t\t\tColor\r\n\t\t    </label>\r\n            <div id=\"cid_14\" class=\"form-input form-label-auto\">\r\n                <select class=\"form-dropdown\" defaultcountry=\"\" name=\"q8_address8[country]\" id=\"i_color\" style='width:79px;'>\r\n                    <option value=\"\" selected> Selecciona el color </option>\r\n                    <option value=\"0\"> Azul </option>\r\n                    <option value=\"1\"> Amarillo </option>\r\n                    <option value=\"2\"> Rojo </option>\r\n                    <option value=\"17\"> Negro </option>\r\n\t\t\t  </select>\r\n\t\t    </div>   \r\n        </span>\r\n\r\n\t  </li>        \r\n\t\t\r\n      <!-- <li class=\"form-line form-field-hidden\" style=\"display:none;\" data-type=\"control_datetime\" id=\"id_3\"> -->\r\n            \r\n      <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_433\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_433\" for=\"input_433\">\r\n          Procedencia\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div class=\"form-radio-item\" data-columnCount=\"2\">\r\n\t\t\t<input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_mexican\" name=\"nation\" />\r\n            <label id=\"label_input_m\" for=\"input_m\" style=\"margin-top:6px\"> Nacional </label>\r\n\t\t\t&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n            <input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_forager\" name=\"nation\" />\r\n            <label id=\"label_input_f\" for=\"input_f\" style=\"margin-top:6px\"> Extranjero </label>\r\n\t\t</div>\t\t\r\n\t  </li>\r\n      <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_433\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_433\" for=\"input_433\">\r\n          Tipo\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div class=\"form-radio-item\" data-columnCount=\"2\">\r\n\t\t\t<input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_type_terrain\" name=\"type\" />\r\n            <label id=\"label_tterrain\" for=\"input_m\" style=\"margin-top:6px\"> Terrestre </label>\r\n\t\t\t&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n            <input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_type_other\" name=\"type\" />\r\n            <label id=\"label_tother\" for=\"input_f\" style=\"margin-top:6px\"> Otro </label>\r\n\t\t</div>\t\t\r\n\t  </li>\r\n      \r\n\t  <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_20\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_20\" for=\"input_20\">\r\n          Tipo de uso\r\n        </label>\r\n        <div id=\"cid_20\" class=\"form-input jf-required\">\r\n          <div class=\"form-multiple-column\" data-columnCount=\"2\">\r\n\t\t  <div class=\"form-single-column\">\r\n\t\t  <span class=\"form-checkbox-item\" style=\"clear:left;\">\r\n              <span class=\"dragger-item\"style=\"min-height: 23px;\">\r\n\t\t\t\t<input type=\"checkbox\" class=\"form-checkbox\" id=\"input_car_type_p\" name=\"car_type\" value=\"Spam/Phishing/Spear Phishing\"/>\r\n\t\t\t\t<label for=\"input_19_0\" > Particular </label>\t\t\r\n\t\t\t  </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n\t\t\t  <span class=\"dragger-item\"style=\"min-height: 23px;\">\r\n\t\t\t\t<input type=\"checkbox\" class=\"form-checkbox\" id=\"input_car_type_t\" name=\"car_type\" value=\"Phone scam (eg.: Fake Microsoft call)\" />\r\n\t\t\t\t<label for=\"input_19_1\"> Transporte </label>\r\n\t\t\t  </span>\r\n              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n\t\t\t  <span class=\"dragger-item\"style=\"min-height: 23px;\">\r\n\t\t\t\t<input type=\"checkbox\" class=\"form-checkbox\" id=\"input_car_type_c\" name=\"car_type\" value=\"Chat room (eg.: IRC/Forum/Newsgroup/Reddit/...)\" />\r\n\t\t\t\t<label for=\"input_19_2\"> Carga </label>\r\n              </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n            </span>\r\n\t\t\t</div>\r\n          </div>\r\n        </div>\r\n      </li>\r\n\t  <li class=\"form-line form-field-hidden\" data-type=\"control_textbox\" id=\"id_active\" style=\"display:none\">\r\n        <div id=\"mapCurrent\" ></div>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t</br>\r\n\t\t<!-- ref = {mapContainer} -->\r\n        <pre id='coordinates' class='coordinates'></pre>\t\t\r\n        <!-- <label class=\"form-label form-label-left form-label-auto\" id=\"label_7\" for=\"input_7\"> Relación </label> -->\r\n      </li>\r\n      <li class=\"form-line form-field-hidden\" data-type=\"control_textbox\" id=\"id_15\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_15\" for=\"input_15\"> Destino </label>\r\n        <div id=\"cid_15\" class=\"form-input jf-required\">\r\n          <textarea id=\"input_narrative\" class=\"form-textarea\" name=\"q33_wheneverPossible33\" cols=\"40\" rows=\"1\"></textarea>\r\n        </div>\r\n      </li>\r\n      <li class=\"form-line form-field-hidden\" data-type=\"control_textbox\" id=\"id_15\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_15\" for=\"input_15\"> Observaciones </label>\r\n        <div id=\"cid_15\" class=\"form-input jf-required\">\r\n          <textarea id=\"input_narrative\" class=\"form-textarea\" name=\"q33_wheneverPossible33\" cols=\"40\" rows=\"3\"></textarea>\r\n        </div>\r\n      </li>\r\n         \r\n\t  <li class=\"form-line jf-required\" data-type=\"control_radio\" id=\"id_17\">\r\n        <label class=\"form-label form-label-left form-label-auto\" id=\"label_17\" for=\"input_17\">\r\n          ¿Cuenta con seguro?\r\n          <span class=\"form-required\">\r\n            *\r\n          </span>\r\n        </label>\r\n        <div id=\"cid_17\" class=\"form-input jf-required\">\r\n          <div class=\"form-multiple-column\" data-columnCount=\"2\">\r\n              <span class=\"dragger-item\">\r\n              </span>\r\n              <input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_17_0\" name=\"q17_haveYou\" value=\"Yes\" />\r\n              <label id=\"label_input_17_0\" for=\"input_17_0\"> Si </label>\r\n              <span class=\"dragger-item\">\r\n              </span>\r\n              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n              <input type=\"radio\" class=\"form-radio validate[required]\" id=\"input_17_1\" name=\"q17_haveYou\" value=\"No\" />\r\n              <label id=\"label_input_17_1\" for=\"input_17_1\"> No </label>\r\n          </div>\r\n        </div>\r\n      </li>\r\n      <li class=\"form-line form-field-hidden\" style=\"display:none;\" data-type=\"control_text\" id=\"id_38\">\r\n        <div id=\"cid_38\" class=\"form-input-wide\">\r\n          <div id=\"text_38\" class=\"form-html\">\r\n            <p>Please refer to the following websites if you would also like to report this seperately:</p>\r\n            <p><a href=\"https://www.europol.europa.eu/content/report-cybercrime-online\" rel=\"nofollow\" target=\"_blank\">Report Cybercrime Online (EU)</a></p>\r\n            <p><a href=\"https://complaint.ic3.gov/default.aspx\" rel=\"nofollow\" target=\"_blank\">IC3 Complaint Referral Form (US)</a></p>\r\n            <p>In case you do not want to report this to a specific law enforcement agency seperately, just finish up this form. If you are willing, it is possible to share any information through Criminal Intelligence teams - this can be completely anonymous, similar to this form. <br /><br />Be sure to contact your CERT or local police department to ask if they have such a team or anonymous reporting possiblity (see also links above). <br /><br />You can find a list of CERTs here:<br /><a href=\"http://www.cert.org/incident-management/national-csirts/national-csirts.cfm\" rel=\"nofollow\" target=\"_blank\">CERTs by Country - Interactive Map <br /><br />List of National CSIRTs </a></p>\r\n            <p><a href=\"http://www.apcert.org/about/structure/members.html\" rel=\"nofollow\" target=\"_blank\">APCERT team members</a></p>\r\n          </div>\r\n        </div>\r\n      </li>     \r\n      <li style=\"display:none\">\r\n        Should be Empty:\r\n        <input type=\"text\" name=\"website\" value=\"\" />\r\n      </li>\r\n    </ul>\t\r\n\t</div>\r\n\t</div>\r\n  \r\n  <input type=\"hidden\" id=\"simple_spc\" name=\"simple_spc\" value=\"40754031698357\" />\r\n  <!-- <script type=\"text/javascript\"> -->\r\n      <!-- document.getElementById(\"si\" + \"mple\" + \"_spc\").value = \"40754031698357-40754031698357\"; -->\r\n  <!-- </script> -->\r\n\r\n\t</div>\t\r\n\t</div>\r\n\r\n\r\n    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>\r\n    <!-- don´t push\r\n\t<script src=\"https://framework-gb.cdn.gob.mx/qa/gobmx.js\"></script> -->\r\n    <!-- <script src=\"https://framework-gb.cdn.gob.mx/qa/assets/scripts/jquery-ui-datepicker.js\"></script> -->\r\n\t<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>\r\n    <script type=\"text/javascript\">\r\n\r\n\tfunction perforate(){$.ajax({\r\n                    type: \"POST\",\r\n                    url: \"http://localhost:11261/Application/Registry/entrylist.aspx/getIPHData\",\r\n                    contentType: \"application/json; charset=utf-8\",\r\n                    dataType: \"json\",\r\n                    data: JSON.stringify({ _id: 33}),\r\n                    cache: false,\r\n                    success: function (data) {\r\n                        var result = JSON.parse(data.d);\r\n                        $(\"#input_name\").val(result.Nombre);\r\n                        $(\"#input_fisrt\").val(result.Paterno);\r\n                        $(\"#input_last\").val(result.Materno);\r\n                        $(\"#input_nickname\").val(result.Alias);\r\n                        $(\"#input_lat\").val(result.Lat);\r\n                        $(\"#input_long\").val(result.Long);\r\n                        $(\"#input_narrative\").val(result.Desc);\r\n                        $(\"#input_hh\").val(result.Hour);\r\n                        $(\"#input_mm\").val(result.Min);\r\n                        $(\"#input_y\").val(result.Year);\r\n                        $(\"#input_m\").val(result.Month);\r\n                        $(\"#input_d\").val(result.Day);\r\n                        $(\"#input_col\").val(result.Col);\r\n                        $(\"#input_mun\").val(result.Mun);\r\n                        $(\"#input_zp\").val(result.ZP);\r\n                        $(\"#input_street\").val(result.St);\r\n                        $(\"#input_streetRef\").val(result.Ref);\r\n                        // add to slct3d\r\n                        $(\"#input_origin\").add(result.Origin, 3);\r\n                        if (result.Sexo == 2) { $(\"#input_female\").attr('checked', 'checked') }\r\n                        else $(\"#input_male\").attr('checked', 'checked');\r\n                        $(\"#input_mexican\").attr('checked', 'checked');\r\n                        $(\"#input_policeCity\").attr('checked', 'checked');\r\n                        $(\"#i_age\").val(result.Edad);       \r\n\r\n                    },\r\n                    error: function () {\r\n                        //ShowError(\"¡Error!\", \"No fue posible cargar la lista de detenidos. Si el problema persiste, por favor, consulte al personal de soporte técnico.\");\r\n                    }\r\n                });\r\n}\r\n        \r\n\t\t$(function () {\r\n\t\t\tperforate();\r\n\t\t\t$(\"#input_name\").val(\"DETENIDO PARA KIRWAN\");\r\n\t\t\t$(\"#input_first\").val(\"TEST\");\r\n\t\t\t$(\"#input_street0\").val(\"Nautla\");\r\n\t\t\t$(\"#input_last\").val(\"PROMAD\");\r\n\t\t\t$(\"#input_male\").prop('checked', true);\r\n\t\t\t$(\"#input_mexican\").prop('checked', true);\r\n\t\t\t\r\n            new violent(\"_0\", \"_evt\",\"_main\");\r\n\t\t\tnew violent(\"_1\", \"_dtn\", \"_evt\");\r\n\t\t\tnew violent(\"_2\", \"_vehicle\", \"_dtn\");\r\n\t\t\t$(\"#IPH_evt\").show();\r\n\t\t\t$(\"#IPH_dtn\").show();\r\n\t\t\t$(\"#IPH_vehicle\").show();\r\n            press_dtn();\r\n            //provide(\"no_nause\", \"checkB\")\r\n        });\r\n        press_pdf(); // printer form\r\n\t\t\r\n        function press_pdf() {\r\n            $(\"#input_print\").on(\"click\", function () {\r\n                $(\"#btn_zone0\").hide();\r\n                $(\"#btn_zone1\").hide();\r\n                $(\"#btn_zone2\").hide();\r\n                $(\"#btn_zone3\").hide();\r\n                window.print();\r\n            });\r\n        }\r\n\r\n        function violent(_btn, _nxt, _bck) {\r\n\t\t\t_feat = _btn;\r\n            _btn = \"#form-pagebreak-back\" + _feat;\r\n            _btnDiff = \"#form-pagebreak-next\" + _feat;\r\n            _bck = \"#IPH\" + _bck;\r\n            _nxt = \"#IPH\" + _nxt;\r\n\t\t\t\t$(_btnDiff).on(\"click\", function () {\r\n\t\t\t\t\t$(_bck).hide();\r\n\t\t\t\t\t$(_nxt).show();\r\n\t\t\t\t});\r\n\t\t\t\t$(_btn).on(\"click\", function () {\r\n\t\t\t\t\t$(_nxt).hide();\r\n\t\t\t\t\t$(_bck).show();\r\n\t\t\t\t});\r\n        }\r\n\r\n\t\t\r\n\t\tfunction provide(_input, _flag) {\r\n            _input = \"#\" + _input;\r\n            $(_input).hide();\r\n            _flag = \"#\" + _flag\r\n            $(_flag).change(function () {\r\n                if (this.checked) {\r\n                    $(_input).show();\r\n                }\r\n                else {\r\n                    $(_input).hide();\r\n                }\r\n            });\r\n        }\r\n\r\n\r\n        function press_car() {\r\n            $(\"#input_car_type_c\").change(function () {\r\n\t\t\t\tif (this.checked) {\r\n\t\t\t\t\t$(\"#input_car_type_p\").prop('checked', false);\r\n\t\t\t\t\t$(\"#input_car_type_t\").prop('checked', false);\r\n\t\t\t\t}\r\n            });\r\n            $(\"#input_car_type_t\").change(function () {\r\n\t\t\t\tif (this.checked) {\r\n\t\t\t\t\t$(\"#input_car_type_p\").prop('checked', false);\r\n\t\t\t\t\t$(\"#input_car_type_c\").prop('checked', false);\r\n\t\t\t\t}\r\n            });\r\n            $(\"#input_car_type_p\").change(function () {\r\n\t\t\t\tif (this.checked) {\r\n\t\t\t\t\t$(\"#input_car_type_c\").prop('checked', false);\r\n\t\t\t\t\t$(\"#input_car_type_t\").prop('checked', false);\r\n\t\t\t\t}\r\n            });\r\n            \r\n        }\r\n\r\n        function movetoNext(k, nextFieldID, EVENT) {\r\n            if (k.value.length >= k.maxLength) {\r\n                event.key;\r\n                k.value = EVENT.key;\r\n                document.getElementById(nextFieldID).focus();\r\n            }\r\n        }\r\n\r\n        //cartridge\r\n\r\n        function init() {\r\n            new provide(\"input_birth3\", \"flag_birth\");\r\n\t\t\tnew provide(\"input_unit\", \"flag_unit\");\r\n\t\t\tnew provide(\"pixel_zone\", \"flag_pixel\");\r\n\t\t\tnew provide(\"retention_zone\", \"flag_retention\");\r\n\t\t\tnew provide(\"phone_zone\", \"flag_phone\");\r\n            new provide(\"input_involve\", \"flag_involve\");\r\n            //$(\"#input_birth3\").prop('checked', false);\r\n            press_car();\r\n            active(\"ailment\");\r\n            active(\"injury\");\r\n        }\r\n        init()\r\n\r\n        function active(_icheck) {\r\n\r\n            _icheck = '#' + _icheck;\r\n            $(_icheck + '_false').prop('checked', true);\r\n            $(_icheck + '_true').change(function () {\r\n                if (this.checked) {\r\n                    $(_icheck).show();\r\n                }\r\n            });\r\n            $(_icheck + '_false').change(function () {\r\n\r\n                if (this.checked) {\r\n                    $(_icheck).hide();\r\n                }\r\n            });\r\n        };\r\n\t\t\r\n        <!-- function CargarMapa() { -->\r\n            <!-- //  if ($('#latitud').val() == \"\" || $('#longitud').val() == \"\") { -->\r\n\r\n            <!-- mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw'; -->\r\n            <!-- var coordinates = document.getElementById('coordinates'); -->\r\n            <!-- function onDragEnd() { -->\r\n                <!-- var lngLat = marker.getLngLat(); -->\r\n                <!-- coordinates.style.display = 'block'; -->\r\n                <!-- coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat; -->\r\n                <!-- let x = lngLat.lng; -->\r\n                <!-- let y = x; -->\r\n                <!-- x = x.toString(); -->\r\n                <!-- x = x.substring(0, x.length - 3); -->\r\n                <!-- $('#longitud').val(x); -->\r\n\r\n                <!-- x = lngLat.lat; -->\r\n                <!-- let z = x; -->\r\n                <!-- x = x.toString(); -->\r\n                <!-- x = x.substring(0, x.length - 3); -->\r\n                <!-- $('#latitud').val(x); -->\r\n            <!-- } -->\r\n            <!-- var latitud = -89.61086650942; -->\r\n            <!-- var longitud = 20.97689912377; -->\r\n\r\n        <!-- var map = new mapboxgl.Map({ -->\r\n                <!-- container: mapCurrent, -->\r\n                <!-- style: 'mapbox://styles/mapbox/streets-v11', -->\r\n                <!-- center: [longitud, latitud], -->\r\n                <!-- zoom: 16 -->\r\n            <!-- }); -->\r\n            <!-- var marker = new mapboxgl.Marker({ -->\r\n                <!-- draggable: true -->\r\n            <!-- }) -->\r\n\r\n                <!-- .setLngLat([longitud, latitud]) -->\r\n                <!-- .addTo(map); -->\r\n            <!-- map.addControl(new mapboxgl.NavigationControl()); -->\r\n            <!-- marker.on('dragend', onDragEnd); -->\r\n\r\n            <!-- //else {\t\t -->\r\n            <!-- //    var mymap = L.map('mapid').setView([$('#latitud').val(), $('#longitud').val()], zoom = 16, 13, 16); var marker = L.marker([$('#latitud').val(), $('#longitud').val()]).addTo(mymap);\t\t -->\r\n            <!-- //} -->\r\n        <!-- } -->\r\n\r\n        //CargarMapa();\r\n\r\n    </script>\r\n\r\n</div>\r\n\r\n</html>\r\n<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>");
			pdf.Save(output); 
            // Close current reference
            pdf.Close();
            Console.WriteLine("PDF updated successfully.");

        }

		[WebMethod]
		public static string pdfBoleta(string data)
		{
			//br34k
			var serializedObject = string.Empty;
			try
			{
				if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
				{
                    //var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];
                    //int usuario;
                    //if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    //else throw new Exception("No es posible obtener información del usuario en el sistema");

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
					//int InternoId = Convert.ToInt32(datos[0]);
					int InternoId = Convert.ToInt32(data);
					var obj = ControlPDFBarandilla.GenerarReporteBoleta(InternoId);
					var detenido = ControlDetenido.ObtenerPorId(InternoId);
					Entity.ReportesLog reportesLog = new Entity.ReportesLog();
					reportesLog.ProcesoId = InternoId;
					reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
					reportesLog.Modulo = "Registro en barandilla";
					reportesLog.Reporte = "Boleta de control";
					reportesLog.Fechayhora = DateTime.Now;
					reportesLog.EstatusId = 2;
					reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
					ControlReportesLog.Guardar(reportesLog);
					serializedObject = JsonConvert.SerializeObject(obj);
					return serializedObject;
				}
				else
				{
					return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
				}
			}
			catch (Exception ex)
			{
				return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
			}
		}

		[WebMethod]
		public static string getRutaServer()
		{
			string rutaDefault = "";

			try
			{
				rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
				return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
			}
		}

		[WebMethod]
		public static DT.DataTable getinterno(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string anio, string fechaInicio, string fechaFinal)
		{
			DateTime newAnio = Convert.ToDateTime(fechaFinal);
			DateTime oldAnio = Convert.ToDateTime(fechaInicio);
			if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
			{
				try
				{
					if (!emptytable)
					{
						if (string.IsNullOrEmpty(anio))
						{
							anio = DateTime.Now.Year.ToString();
						}
						var listaanios = ControlDashBoardAnioTrabajo.GetAniosTrabajo();

						List<int> Anios = new List<int>();
						foreach (var item in listaanios)
						{
							Anios.Add(item.Aniotrabajo);
						}

						if (Convert.ToInt32(anio) > Anios.Max())
						{
							anio = Anios.Max().ToString();
						}

						//order.FirstOrDefault().column = 8;
						//order.FirstOrDefault().dir = "desc";
						MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

						List<Where> where = new List<Where>();
						List<Where> where2 = new List<Where>();
						var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];
						var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
						int usuario;

						if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
						else throw new Exception("No es posible obtener información del usuario en el sistema");

						//where.Add(new Where("I.ContratoId", Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]).ToString()));
						int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
						Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

						//Prueba de IN

						int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

						//var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
						//var soloContratos = dataContratos.Where(x => x.Tipo == "contrato" && x.Activo);
						//var soloSubcontratos = dataContratos.Where(x => x.Tipo == "subcontrato" & x.Activo);
						//string IdsContratos = "(";
						//string IdsContratos2 = "(";

						//int i = 0;
						//int j = 0;

						//foreach (var item in soloContratos)
						//{
						//    if(item.Tipo == "contrato")
						//    {
						//        if (i == soloContratos.Count() - 1)
						//        {
						//            IdsContratos += item.IdContrato + ")";
						//        }
						//        else
						//        {
						//            IdsContratos += item.IdContrato + ",";
						//        }
						//    }                            

						//    i++;
						//}

						//foreach (var item in soloSubcontratos)
						//{
						//    if (item.Tipo == "subcontrato")
						//    {
						//        if (j == soloSubcontratos.Count() - 1)
						//        {
						//            IdsContratos2 += item.IdContrato + ")";
						//        }
						//        else
						//        {
						//            IdsContratos2 += item.IdContrato + ",";
						//        }
						//    }

						//    j++;
						//}

						//Primer where

						where.Add(new Where("Det.ContratoId", contratoUsuario.IdContrato.ToString()));
						where.Add(new Where("Det.Tipo", contratoUsuario.Tipo));
						where.Add(new Where("cast(Det.Fecha as DATE)", ">=", fechaInicio));
						where.Add(new Where("cast(Det.Fecha as DATE)", "<=", fechaFinal));
						//Segundo where
						//where2.Add(new Where("E.ContratoId", Where.IN, IdsContratos2));
						//where2.Add(new Where("E.Tipo", "=", "subcontrato"));

						//if ((!string.Equals(perfil, "Administrador")))
						//{
						//    var user = ControlUsuario.Obtener(usuario);
						//    //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
						//}
						Query query = new Query
						{
							select = new List<string> {
								"distinct Det.Id Dtn",
								"Det.TrackingId",
								"Det.NombreDetenido Nombre",
								"TRIM(Det.APaternoDetenido) Paterno",
								"TRIM(Det.AMaternoDetenido) Materno",
								"'' RutaImagen",
								"CAST(ifnull(Det.Expediente, 'no asignado') AS unsigned) Expediente",
								"Det.DetenidoId",
								"Det.NCP",
								"Det.Estatus",
								"Det.Activo",
								"'' TrackingIdEstatus" ,
								"Det.ContratoId",
								"Det.Tipo",
								"ES.Nombre EstatusNombre",
								"'' AgrupadoId",
								"concat(Det.NombreDetenido,' ',TRIM(APaternoDetenido),' ',TRIM(AMaternoDetenido)) as NombreCompleto",
								//l1n3
                                //"DATE_FORMAT(HS.Fecha,'%Y-%m-%d %H:%i:%S') Fecha"
                                "DATE_FORMAT(Det.Fecha,'%Y-%m-%d') Fecha"
						},
							//from = new Table($"(select Id,TrackingId,Nombre,Paterno,Materno,RutaImagen from detenido where Detenidoanioregistro>={oldAnio.Year} and Detenidoanioregistro<={newAnio.Year})", "I"),
							from = new DT.Table("detalle_detencion", "Det"),
							joins = new List<Join>
							{
								//new Join(new Table($"(select Id, NCP, Estatus, Activo, TrackingId, DetenidoId, Expediente, ContratoId, Tipo, Fecha from detalle_detencion where year(Fecha) >= {oldAnio.Year} and year(Fecha) <= {newAnio.Year})", "E"), "I.id  = E.DetenidoId"),
								//new Join(new Table ("detalle_detencion", "det")),
								new Join(new DT.Table("estatus", "ES"), "ES.id  = Det.Estatus"),                                
								//new Join(new Table("historial", "HS"), "HS.InternoId  = E.DetenidoId"),
                                //new Join(new Table("(Select Id,ifnull(Expediente,0) as ExpedienteoOriginal,DetalleDetencionId from historico_agrupado_detenido)", "H"), "H.DetalleDetencionId=E.id")
                                //new Join(new Table("contrato", "c"), "c.id  = E.ContratoId")                                
                            },
							wheres = where
						};

						DataTablesAux dt = new DataTablesAux(mysqlConnection);
						var result1 = dt.Generar(query, draw, start, length, search, order, columns);
						return result1;
					}
					else
					{
						return DataTables.ObtenerDataTableVacia(null, draw);
					}
				}
				catch (Exception ex)
				{
					return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
				}
			}
			else
			{
				return DataTables.ObtenerDataTableVacia("", draw);
			}
		}


		[WebMethod]
		public static DT.DataTable getinternoFiltro(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, DEtenidoFiltroAux Filtro)
		{
			if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
			{
				try
				{
					if (!emptytable)
					{

						MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

						List<Where> where = new List<Where>();
						List<Where> where2 = new List<Where>();
						var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

						var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
						int usuario;

						if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
						else throw new Exception("No es posible obtener información del usuario en el sistema");

						//where.Add(new Where("I.ContratoId", Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]).ToString()));
						int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
						Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

						//Prueba de IN

						int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

						//var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
						//var soloContratos = dataContratos.Where(x => x.Tipo == "contrato" && x.Activo);
						//var soloSubcontratos = dataContratos.Where(x => x.Tipo == "subcontrato" & x.Activo);
						//string IdsContratos = "(";
						//string IdsContratos2 = "(";

						//int i = 0;
						//int j = 0;

						//foreach (var item in soloContratos)
						//{
						//    if(item.Tipo == "contrato")
						//    {
						//        if (i == soloContratos.Count() - 1)
						//        {
						//            IdsContratos += item.IdContrato + ")";
						//        }
						//        else
						//        {
						//            IdsContratos += item.IdContrato + ",";
						//        }
						//    }                            

						//    i++;
						//}

						//foreach (var item in soloSubcontratos)
						//{
						//    if (item.Tipo == "subcontrato")
						//    {
						//        if (j == soloSubcontratos.Count() - 1)
						//        {
						//            IdsContratos2 += item.IdContrato + ")";
						//        }
						//        else
						//        {
						//            IdsContratos2 += item.IdContrato + ",";
						//        }
						//    }

						//    j++;
						//}

						//Primer where

						where.Add(new Where("Det.ContratoId", contratoUsuario.IdContrato.ToString()));
						where.Add(new Where("Det.Tipo", contratoUsuario.Tipo));

						if (!string.IsNullOrEmpty(Filtro.Nombre))
						{
							where.Add(new Where("I.Nombre", "LIKE", "%" + Filtro.Nombre + "%"));
						}

						if (!string.IsNullOrEmpty(Filtro.Paterno))
						{
							where.Add(new Where("I.Paterno", Where.LIKE, "%" + Filtro.Paterno + "%"));
						}
						if (!string.IsNullOrEmpty(Filtro.Materno))
						{
							where.Add(new Where("I.Materno", Where.LIKE, "%" + Filtro.Materno + "%"));
						}
						if (!string.IsNullOrEmpty(Filtro.Alias))
						{
							where.Add(new Where("A.Alias", Where.LIKE, "%" + Filtro.Alias + "%"));
						}
						if (Filtro.Id == "-1")
						{
							where.Add(new Where("I.Id", Filtro.Id));
						}

						Query query = new Query
						{
							select = new List<string> {
								"I.Id",
								"I.DetenidoId",
								"I.TrackingId",
								"I.Nombre",
								"TRIM(I.Paterno) Paterno",
								"TRIM(I.Materno) Materno",
								"I.RutaImagen",
								"CAST(E.Expediente as unsigned) Expediente",
								"E.NCP",
								"E.Estatus",
								"E.Activo",
								"E.TrackingId TrackingIdEstatus" ,
								"ES.Nombre EstatusNombre",
								"ifnull(A.Alias,'') Alias",
								"concat(E.NombreDetenido,' ',trim(APaternoDetenido),' ',trim(AMaternoDetenido)) NombreCompleto"
							},
							from = new DT.Table("detenido", "I"),
							joins = new List<Join>
							{
								new Join(new DT.Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
								new Join(new DT.Table("estatus", "ES"), "ES.id  = E.Estatus"),
								new Join(new DT.Table("detenidosalias", "A"), "I.id  = A.detenidoid")
                                //new Join(new Table("contrato", "c"), "c.id  = E.ContratoId")                                
                            },
							wheres = where
						};

						DataTables dt = new DataTables(mysqlConnection);


						var result1 = dt.Generar(query, draw, start, length, search, order, columns);

						return result1;
					}
					else
					{
						return DataTables.ObtenerDataTableVacia(null, draw);
					}
				}
				catch (Exception ex)
				{
					return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
				}
			}
			else
			{
				return DataTables.ObtenerDataTableVacia("", draw);
			}
		}

		//wtf
		[WebMethod]
		public static string blockitem(string value)
		{
			if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Eliminar)
			{
				try
				{
					int brief = Convert.ToInt32(value);
					//Entity.Detenido itemBase = ControlDetenido.ObtenerPorId(brief);
					Entity.DetalleDetencion item = ControlDetalleDetencion.ObtenerPorDetenidoId(brief).LastOrDefault();
                    item.Activo = item.Activo ? false : true;
					ControlDetalleDetencion.Actualizar(item);
					return JsonConvert.SerializeObject(new { exitoso = true, mensaje = item.Activo ? "Registro habilitado con éxito." : "Registro deshabilitado con éxito." });
				}
				catch (Exception ex)
				{
					return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
				}
			}
			else
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
			}
		}

		[WebMethod]
		public static List<Combo> getEventoReciente()
		{
			List<Combo> combo = new List<Combo>();
			int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);

			Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
			List<Entity.EventoReciente> eventosReciente = ControlEventoReciente.ObtenerTodos();


			if (eventosReciente.Count > 0)
			{
				foreach (var rol in eventosReciente)
				{
					if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
					{
						combo.Add(new Combo { Desc = rol.Hora > 1 ? rol.Hora + " hrs" : rol.Hora + " hr", Id = rol.Id.ToString() });

					}
				}
			}

			return combo;
		}

		[WebMethod]
		public static string delay(string id, string n)
		{
            //	string py_source = @"C:\Users\Alberto Kirwan\Documents\c1g4r3tt\control_dtnds_foward\ctrldtnds\Source\Web\Content\python_zone\diagnostic.py";
            //  ScriptRuntime run_script = Python.CreateRuntime();
            //  dynamic industrie = run_script.UseFile(py_source);
            //  industrie.run(000, 24919);

            //umpteenth c4s3
            RandomPetition randomPetition = new RandomPetition();
            string[] phrase = id.Split('&');
            string feedback = randomPetition.saveObject(phrase[0], Convert.ToInt32(phrase[1]), Convert.ToInt32(n));
            //string feedback = randomPetition.saveNonReal(phrase[0], Convert.ToInt32(phrase[1]), Convert.ToInt32(n));

            return feedback;
		}


		[WebMethod]
		public static List<Combo> getEventsCase(string id, int u)
		{
			List<Combo> combo = new List<Combo>();

			if (!string.IsNullOrEmpty(id))
				combo = getRecurrence(id, u);

			return combo;

		}

		// un1t_t3$t
		public static List<Combo> getRecurrence(string id, int value)
        {
            List<Combo> combo = new List<Combo>();
            try
			{
				bool non_real_test = true;
				RandomPetition randomPetition = new RandomPetition();
				if (non_real_test)
				{
					if (value == 1 || value == 0)
					{
						if (value == 0) //event				
							combo = randomPetition.EventCombo(id);
						else //detained
							combo = randomPetition.DtnCombo(id);
					}
				}
				else
                {
                    if (value == 0) //event				
                        combo.Add(new Combo { Desc = "Virus Tyrant research", Id = "25091", AdicionalUno = "208506" });
                    else //detained
                        combo.Add(new Combo { Desc = "Leon S. Kennedy", Id = "25091", AdicionalUno = "2865" });
				}
            }
			catch { }
			return combo;
		}

		[WebMethod]
		public static List<Combo> getDetenidos(string id)
		{
			List<Combo> combo = new List<Combo>();
			List<Entity.DetenidoEvento> detenidos;

			detenidos = ControlDetenidoEvento.ObtenerPorEventoId(Convert.ToInt32(id));

			if (detenidos.Count > 0)
			{
				foreach (var rol in detenidos)
				{
					combo.Add(new Combo { Desc = rol.Nombre + " " + rol.Paterno + " " + rol.Materno, Id = rol.Id.ToString() });
				}
			}

			return combo;
		}

		[WebMethod]
		public static string AgrupaDetenidos(string[] datos, string idoriginal)
		{
			try
			{
				string[] internod = new string[2] {
					idoriginal.ToString(), "true"
				};
				var detalledetencionoriginal = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
				foreach (var item in datos)
				{
					string[] id = new string[2] { item.ToString(), "true" };
					var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(item)).LastOrDefault();
					if (detalleDetencion == null)
					{
						var _listadoAgrupados = ControlHistoricoAgrupadoDetenido.HistoricosAgrupoadoPorDetenido(Convert.ToInt32(item));
						detalleDetencion = ControlDetalleDetencion.ObtenerPorId(_listadoAgrupados.LastOrDefault().DetalleDetencionId);
					}

					detalleDetencion.DetenidoId = Convert.ToInt32(idoriginal);
					detalleDetencion.Activo = false;

					Entity.Historial historial = new Entity.Historial();
					int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

					historial.CreadoPor = usId;
					historial.Activo = true;
					historial.Fecha = DateTime.Now;
					historial.Habilitado = true;
					var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
					var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
					historial.ContratoId = subcontrato.Id;
					historial.Movimiento = "Agrupación de registros de detenidos";
					historial.TrackingId = Guid.NewGuid();
					historial.InternoId = Convert.ToInt32(idoriginal);
					ControlHistorial.Guardar(historial);

					var listadoagrupado = ControlHistoricoAgrupadoDetenido.HistoricosAgrupoadoPorDetenidooriginal(Convert.ToInt32(idoriginal));

					Entity.HistoricoAgrupadoDetenido historicoAgrupadoDetenido = new Entity.HistoricoAgrupadoDetenido();
					historicoAgrupadoDetenido.FechaHora = DateTime.Now;
					historicoAgrupadoDetenido.DetenidoOriginalId = Convert.ToInt32(idoriginal);
					historicoAgrupadoDetenido.DetalleDetencionId = detalleDetencion.Id;
					historicoAgrupadoDetenido.DetenidoId = Convert.ToInt32(item);
					historicoAgrupadoDetenido.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
					historicoAgrupadoDetenido.Expediente = detalleDetencion.Expediente;

					if (listadoagrupado.Count > 0)
					{
						if (listadoagrupado.LastOrDefault().Expedienteoriginal != detalleDetencion.Expediente)
						{
							historicoAgrupadoDetenido.Expedienteoriginal = listadoagrupado.LastOrDefault().Expedienteoriginal;
						}
						else
						{
							historicoAgrupadoDetenido.Expedienteoriginal = detalledetencionoriginal.Expediente;
						}
					}
					else
					{
						historicoAgrupadoDetenido.Expedienteoriginal = detalledetencionoriginal.Expediente;
					}
					ControlHistoricoAgrupadoDetenido.Guardar(historicoAgrupadoDetenido);

					detalleDetencion.Expediente = detalledetencionoriginal.Expediente;
					detalleDetencion.Estatus = 2;

					ControlDetalleDetencion.Actualizar(detalleDetencion);

				}

				var listadoAgrupados = ControlHistoricoAgrupadoDetenido.HistoricosAgrupoadoPorDetenidooriginal(Convert.ToInt32(idoriginal));

				List<int> Expedientes = new List<int>();
				List<Entity.Biometrico> Biometrico = new List<Entity.Biometrico>();

				foreach (var agrupado in listadoAgrupados)
				{
					Expedientes.Add(Convert.ToInt32(agrupado.Expediente));
					var _istbiometric = ControlBiometricos.GetByDetenidoId(agrupado.DetenidoId);
					if (_istbiometric.Count > 0)
					{
						Biometrico.Add(_istbiometric.FirstOrDefault(X => X.Clave == "Face_0.jpg"));
					}

				}
				var BiometricoOrigaldDet = ControlBiometricos.GetByDetenidoId(Convert.ToInt32(idoriginal));

				if (BiometricoOrigaldDet.Count > 0)
				{
					Biometrico.Add(BiometricoOrigaldDet.FirstOrDefault());
				}
				Expedientes.Add(Convert.ToInt32(detalledetencionoriginal.Expediente));

				int expediente = Expedientes.Max();
				detalledetencionoriginal.Expediente = expediente.ToString();
				ControlDetalleDetencion.Actualizar(detalledetencionoriginal);

				if (Biometrico.Count > 0)
				{
					Entity.Biometrico Biometric = new Entity.Biometrico();
					List<DateTime> fechas = new List<DateTime>();
					foreach (var item2 in Biometrico)
					{
						fechas.Add(item2.FechaHora);

					}
					DateTime FechaMax = fechas.Max();

					Biometric = Biometrico.Where(x => x.FechaHora == FechaMax).LastOrDefault();

					var biometricoReciente = ControlBiometricos.GetByDetenidoId(Biometric.DetenidoId);

					string ruta = string.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/detenido/" + detalledetencionoriginal.ContratoId + "/" + detalledetencionoriginal.DetenidoId + "");
					string path = System.Web.HttpContext.Current.Server.MapPath(ruta);


					string cad = "";
					cad = (DateTime.Now.ToShortDateString() + DateTime.Now.ToLongTimeString()).Replace("/", "").Replace(":", "");
					cad = cad.Substring(0, 14);
					string path2 = System.Web.HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/detenido/" + detalledetencionoriginal.ContratoId + "/_" + detalledetencionoriginal.DetenidoId + "_" + cad));
					if (Directory.Exists(path))
					{
						Directory.Move(path, path2);
						Directory.CreateDirectory(path);


					}
					else
					{
						Directory.CreateDirectory(path);
					}

					int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
					foreach (var biome in biometricoReciente)
					{
						string _path = System.Web.HttpContext.Current.Server.MapPath(biome.Rutaimagen);
						if (File.Exists(_path))
						{
							File.Copy(_path, path + "\\" + biome.Clave);
							CreaThumb(path + "\\" + biome.Clave);
							Entity.Biometrico biometriconuevo = new Entity.Biometrico();
							biometriconuevo.Activo = 1;
							biometriconuevo.Clave = biome.Clave;
							biometriconuevo.Creadopor = usId;
							biometriconuevo.Descripcion = biome.Descripcion;
							biometriconuevo.DetenidoId = Convert.ToInt32(idoriginal);
							biometriconuevo.FechaHora = DateTime.Now;
							biometriconuevo.Habilitado = 1;
							biometriconuevo.Nombre = biome.Nombre;
							biometriconuevo.Rutaimagen = ruta + "/" + biome.Clave;
							biometriconuevo.TipobiometricoId = biome.TipobiometricoId;
							biometriconuevo.TrackingId = Guid.NewGuid().ToString();
							ControlBiometricos.Guardar(biometriconuevo);

						}
					}
				}



				return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "registro", alerta = "" });
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });

			}
		}

		public static bool CreaThumb(string filepath)
		{
			try
			{
				//url file output
				string output = string.Empty;
				// Load image.
				System.Drawing.Image image = System.Drawing.Image.FromFile(filepath);

				// Compute thumbnail size.
				Size thumbnailSize = GetThumbnailSize(image);

				// Get thumbnail.
				System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero);

				string extension = string.Empty;
				extension = Path.GetExtension(filepath);

				// Save thumbnail.
				if (extension == ".JPG" || extension == ".jpg" || extension == ".JPEG" || extension == ".jpeg" || extension == ".bmp" || extension == ".BMP")
				{
					output = filepath.Replace(".jpg", ".thumb").Replace(".jpeg", ".thumb").Replace(".JPG", ".thumb").Replace(".JPEG", ".thumb").Replace(".bmp", ".thumb").Replace(".BMP", ".thumb");
					thumbnail.Save(output);
				}
				else if (extension == ".PNG" || extension == ".png")
				{
					output = filepath.Replace(".png", ".thumb").Replace(".PNG", ".thumb");
					thumbnail.Save(output, ImageFormat.Png);
				}

				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		private static Size GetThumbnailSize(System.Drawing.Image original)
		{
			// Maximum size of any dimension.
			const int maxPixels = 40;

			// Width and height.
			int originalWidth = original.Width;
			int originalHeight = original.Height;

			// Compute best factor to scale entire image based on larger dimension.
			double factor;
			if (originalWidth > originalHeight)
			{
				factor = (double)maxPixels / originalWidth;
			}
			else
			{
				factor = (double)maxPixels / originalHeight;
			}

			// Return thumbnail size.
			return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
		}
		private static int CalcularEdad(DateTime fechaNac)
		{
			int edad = 0;
			edad = DateTime.Now.Year - fechaNac.Year;
			if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
			{
				if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
					edad = edad - 1;
			}
			else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
			{
				if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
					edad = edad - 1;
			}
			else
			{
				if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
					edad = edad - 1;
			}
			return edad;
		}

		[WebMethod]
		public static object getIPHData(string _id)
		{
			//pdfBeta();
            int value = Convert.ToInt32(_id);
            object fakeValue = loadData(value);
			return fakeValue;
		}

		public static object loadData(int i)
        {
            object obj = null;
            try
            {
                //try catch
                //var detenidoevento = ControlDetenidoEvento.ObtenerPorId(Convert.ToInt32(_id));

                if (i != 0)
                {
                    Entity.WSAEvtDtnd EvtDtnd = new Entity.WSAEvtDtnd();
                    EvtDtnd = ControlWSAEvtD.ObtenerPorId(i);
                    int age = new int();
                    if (EvtDtnd.Edad < 18) age = 0;
                    else age = EvtDtnd.Edad - 17;
                    string[] nMonth = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
					obj = new
					{
						Nombre = EvtDtnd.Nombre,
						Paterno = EvtDtnd.Paterno,
						Materno = EvtDtnd.Materno,
						Alias = EvtDtnd.Alias,
						Sex = EvtDtnd.IdSexo,
						Age = age,
						//event_damb
						Origin = EvtDtnd.Origen,
						Min = EvtDtnd.Fecha.Minute,
						Hour = EvtDtnd.Fecha.Hour,
						Day = EvtDtnd.Fecha.Day,
						Month = EvtDtnd.Fecha.Month,
						Year = EvtDtnd.Fecha.Year,
						Lat = EvtDtnd.Latitud,
						Long = EvtDtnd.Longitud,
						ZP = EvtDtnd.CodigoPostal,
						St = EvtDtnd.Calle,
						//ControlMunicipio.ObtenerPorClave(EvtDtnd.IdMunicipio).NombreMunicipio.ToString(),
						Mun = "Coyoacan",
                        //ControlColonia.ObtenerPorId(EvtDtnd.IdColonia).Asentamiento.ToString(),
                        Col = "AMPLIACION CANDELARIA",
                        Desc = EvtDtnd.Descripcion != null ? EvtDtnd.Descripcion : "",

                    };

                    return JsonConvert.SerializeObject(obj);
                }
                else
                {
                    return "No cuenta con permisos ó no existen datos.";
                    //return obj;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        } 

		[WebMethod]
		public static Object getbyid(string _id, string _detained) //criteri4
        {
			try
			{
				object obj = null;

				//var detenidoevento = ControlDetenidoEvento.ObtenerPorId(Convert.ToInt32(_id));
				var sex = new Entity.Catalogo();
				int age = new int();
				if (!String.IsNullOrEmpty(_id) && _id != "0")
				{
					var _url = "http://c5-amb-postproduccion.promad.com.mx:9197/api/personainvolucrada/getRegistros";
					var json = new JavaScriptSerializer().Serialize(new // adjoining
					{
						uuid = 5,
						idEvento = _id
					});

					using (var client = new HttpClient())  // instance HttpClient
					{
						var response = client.PostAsync(new Uri(_url), new StringContent(json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult(); ;
						if (response.IsSuccessStatusCode)
						{
							var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
							var jObj = JObject.Parse(responseContent);
							JArray paramsArray = new JArray();
							paramsArray = (JArray)jObj["items"];

							foreach (JToken i in paramsArray)
							{
								if ((string)i["nombre_tipo_persona"] == "DETENIDO")
									sex = ControlCatalogo.Obtener(Convert.ToInt16((string)i["id_sexo"]), Convert.ToInt32(Entity.TipoDeCatalogo.sexo));
									age = Convert.ToInt16((string)i["edad"]);
								if ((string)i["id_persona_involucrada"] == _detained)
								{
									obj = new
									{
										Sexo = sex != null ? sex.Nombre : "Sexo no registrado",
										Edad = age != 0 ? age.ToString() : ""
									};
								}
							}
						}
					}
					return obj;
				}
				else
				{
					return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
				}
			}
			catch (Exception ex)
			{
				return new { exitoso = false, mensaje = ex.Message };
			}
		}

		[WebMethod]
		public static string save(string[] datos)
		{
			//br34k
			try
			{
				if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
				{
					Entity.Evento evento = new Entity.Evento();
					Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
					Entity.Detenido detenido = new Entity.Detenido();
					Entity.DetalleDetencion estatus = new Entity.DetalleDetencion();
					Entity.DetalleDetencion estatusTemp = new Entity.DetalleDetencion();
					Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
					Entity.Institucion institucion = new Entity.Institucion();
					Entity.General general = new Entity.General();
					List<Entity.Detenido> listaDetenido = new List<Entity.Detenido>();

					int expediente = 0;
					int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
					var listadetalleDetencion = ControlDetalleDetencion.ObteneTodos();
					var contratoUsuarioAux = ControlContratoUsuario.ObtenerPorId(idContrato);
					var mode = string.Empty;
					bool alerta = false;
					int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
					string nombreDetenido = string.Empty;

					var detenidoAux = ControlDetenido.ObtenerPorId(Convert.ToInt32(datos[1]));
					detenidoEvento = ControlDetenidoEvento.ObtenerPorId(Convert.ToInt32(datos[1]));
					//evento = ControlEvento.ObtenerById(Convert.ToInt32(datos[0]));

					detenido.Nombre = detenidoEvento.Nombre;
					detenido.Paterno = detenidoEvento.Paterno;
					detenido.Materno = detenidoEvento.Materno;
					detenido.RutaImagen = datos[2].ToString();
					detenido.Detenidoanioregistro = DateTime.Now.Year;
					detenido.Detenidomesregistro = DateTime.Now.Month;
					estatus.CentroId = 0;
					estatus.Fecha = DateTime.Now;
					estatus.NCP = "npc";
					estatus.Activo = true;
					estatus.ExpedienteAdm = "";
					estatus.Estatus = 1;
					estatus.ContratoId = contratoUsuarioAux.IdContrato;
					estatus.Tipo = contratoUsuarioAux.Tipo;
					estatus.Detalledetencionanioregistro = DateTime.Now.Year;
					estatus.Detalldetencionmesregistro = DateTime.Now.Month;
					estatus.NombreDetenido = detenido.Nombre;
					estatus.APaternoDetenido = detenido.Paterno;
					estatus.AMaternoDetenido = detenido.Materno;
					estatus.DetalledetencionSexoId = detenidoEvento.SexoId;
					estatus.DetalledetencionEdad = detenidoEvento.Edad;

					if (detenido != null)
					{
						var listaDetalleDetencionFiltrada = listadetalleDetencion.Where(x => x.ContratoId == contratoUsuarioAux.IdContrato && x.Tipo == contratoUsuarioAux.Tipo);

						if (listaDetalleDetencionFiltrada.Count() > 0)
						{
							estatusTemp = listaDetalleDetencionFiltrada.Last();
							expediente = Convert.ToInt32(estatusTemp.Expediente) + 1;
						}
						else
							expediente = 1;

						if (contratoUsuarioAux.Tipo == "contrato")
						{
							institucion = contratoUsuarioAux != null ? ControlInstitucion.ObtenerPorId(ControlContrato.ObtenerPorId(contratoUsuarioAux.IdContrato).InstitucionId) : null;
						}
						else if (contratoUsuarioAux.Tipo == "subcontrato")
						{
							institucion = contratoUsuarioAux != null ? ControlInstitucion.ObtenerPorId(ControlSubcontrato.ObtenerPorId(contratoUsuarioAux.IdContrato).InstitucionId) : null;
						}

						mode = "registró";

						nombreDetenido = detenido.Nombre + ' ' + detenido.Paterno + ' ' + detenido.Materno;
						listaDetenido = ControlDetenido.ObtenePorNombre(nombreDetenido);

						if (listaDetenido.Count > 0)
						{
							mode = "Se detectaron a detenidos con información similar, el registro se realizó correctamente.";
							alerta = true;
						}

						detenido.TrackingId = Guid.NewGuid();
						detenido.Id = ControlDetenido.Guardar(detenido);

						estatus.Expediente = Convert.ToString(expediente);
						estatus.DetenidoId = detenido.Id;
						estatus.CentroId = institucion.Id;
						estatus.TrackingId = Guid.NewGuid();
						estatus.Id = ControlDetalleDetencion.Guardar(estatus);

						infoDetencion.Activo = true;
						infoDetencion.IdInterno = detenido.Id;
						infoDetencion.CreadoPor = usId;
						infoDetencion.Descripcion = evento.Descripcion;
						infoDetencion.Folio = evento.Folio;
						infoDetencion.Habilitado = true;
						infoDetencion.HoraYFecha = Convert.ToDateTime(evento.HoraYFecha);
						infoDetencion.ColoniaId = Convert.ToInt32(evento.ColoniaId);
						infoDetencion.ResponsableId = 0;
						infoDetencion.UnidadId = 0;
						infoDetencion.LugarDetencion = evento.Lugar;
						infoDetencion.Motivo = evento.Descripcion;
						infoDetencion.TrackingId = Guid.NewGuid();
						infoDetencion.IdEvento = Convert.ToInt32(evento.Id);
						infoDetencion.IdDetenido_Evento = detenidoEvento.Id;

						var nacionalidad = ControlCatalogo.Obtener("Mexicana", Convert.ToInt32(Entity.TipoDeCatalogo.nacionalidad));
						general = new Entity.General();
						general.DetenidoId = detenido.Id;
						general.TrackingId = Guid.NewGuid();
						general.FechaNacimineto = DateTime.MinValue;
						general.RFC = "";
						general.NacionalidadId = 38;
						general.EscolaridadId = 19;
						general.ReligionId = 0;
						general.OcupacionId = 18;
						general.EstadoCivilId = 0;
						general.EtniaId = 0;
						general.SexoId = detenidoEvento.SexoId;
						//general.FechaNacimineto = detenidoEvento.Fechanacimiento;
						general.Edaddetenido = detenidoEvento.Edad;
						general.EstadoMental = false;
						general.Inimputable = false;
						general.Generalanioregistro = DateTime.Now.Year;
						general.Generalmesregistro = DateTime.Now.Month;
						general.Id = ControlGeneral.Guardar(general);

						infoDetencion.Id = ControlInformacionDeDetencion.Guardar(infoDetencion);

						if (estatus.Id > 0)
						{
							Entity.Historial historial = new Entity.Historial();
							historial.Activo = true;
							//historial.CreadoPor = contratoUsuarioAux.IdContrato;
							historial.CreadoPor = usId;
							historial.Fecha = DateTime.Now;
							historial.Habilitado = true;
							historial.InternoId = detenido.Id;
							historial.Movimiento = "Registro del detenido en barandilla";
							historial.TrackingId = detenido.TrackingId;
							var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
							var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
							historial.ContratoId = subcontrato.Id;
							historial.Id = ControlHistorial.Guardar(historial);
						}
					}

					return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, alerta = alerta, id = detenido.TrackingId });
				}
				else
				{
					return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
				}

			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
			}
		}

		// fill data
		[WebMethod]
		public static DT.DataTable getllamadas(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
		{
			if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
			{
				try
				{
					if (!emptytable)
					{

						MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

						List<Where> where = new List<Where>();
						var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];
						var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

						var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
						int usuario;

						if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
						else throw new Exception("No es posible obtener información del usuario en el sistema");

						where.Add(new Where("l.Activo", "1"));
						where.Add(new Where("L.ContratoId", contratoUsuario.IdContrato.ToString()));
						where.Add(new Where("L.Tipo", contratoUsuario.Tipo));

						Query query = new Query
						{
							select = new List<string> {
								"L.Id",
								"L.TrackingId",
								"S.Descripcion DescripcionLlamada",
								"L.Descripcion",
								"L.LugarDetencion",
								"C.Asentamiento",
								"C.CodigoPostal",
								"M.Nombre Municipio",
								"E.Nombre Estado",
								"P.Nombre Pais",
								"L.ColoniaId IdColonia",
								"CAST(L.Folio AS unsigned) Folio",
                                //"L.HoraYFecha",
                                "date_format(L.HoraYFecha, '%Y-%m-%d %H:%i:%S') HoraYFecha",
								"L.Activo",
								"L.Habilitado"
							},
							from = new DT.Table("eventos", "L"),
							joins = new List<Join> {
								new Join(new DT.Table("Llamadas", "S"), "S.Id = L.LlamadaId"),
								new Join(new DT.Table("Colonia", "C"), "C.Id = L.ColoniaId"),
								new Join(new DT.Table("Municipio", "M"), "M.Id = C.IdMunicipio"),
								new Join(new DT.Table("Estado", "E"), "E.Id = M.EstadoId"),
								new Join(new DT.Table("Pais", "P"), "P.Id = E.PaisId")
							},
							wheres = where

						};

						DataTables dt = new DataTables(mysqlConnection);
						var data = dt.Generar(query, draw, start, length, search, order, columns);
						return data;
					}
					else
					{
						return DataTables.ObtenerDataTableVacia(null, draw);
					}
				}
				catch (Exception ex)
				{
					return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
				}
			}
			else
			{
				return DataTables.ObtenerDataTableVacia("", draw);
			}
		}

		[WebMethod]
		public static string blockitemEventsList(string id)
		{
			try
			{
				Entity.Evento evento = ControlEvento.ObtenerByTrackingId(new Guid(id));
				evento.Habilitado = evento.Habilitado ? false : true;
				string mensaje = evento.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
				ControlEvento.Actualizar(evento);
				return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
			}
		}

		[WebMethod]
		public static DT.DataTable getUnidadadesTabla(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string tracking, bool emptytable)
		{
			try
			{
				if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
				{
					if (!emptytable)
					{
						MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

						List<Where> where = new List<Where>();
						Entity.Evento evento = new Entity.Evento();

						if (!string.IsNullOrEmpty(tracking))
						{
							evento = ControlEvento.ObtenerByTrackingId(new Guid(tracking));
							where.Add(new Where("E.Id", Convert.ToString(evento.Id)));
						}
						else
						{
							return DataTables.ObtenerDataTableVacia(null, draw);
						}

						Query query = new Query
						{
							select = new List<string>
							{
								"U.Nombre Unidad",
								"concat(R.Descripcion, '-', R.Nombre) ClaveResponsable",
								"ifnull(C.Nombre,'') Corporacion"
							},
							from = new DT.Table("evento_unidad_responsable", "EU"),
							joins = new List<Join>
							{
								new Join(new DT.Table("eventos", "E"), "EU.EventoId = E.Id", "inner"),
								new Join(new DT.Table("unidad", "U"), "EU.UnidadId = U.Id", "inner"),
								new Join(new DT.Table("responsable_unidad", "R"), "EU.ResponsableId = R.Id", "inner"),
								new Join(new DT.Table("Corporacion", "C"), "C.Id = U.CorporacionId", "left")
							},
							wheres = where
						};

						DataTables dt = new DataTables(mysqlConnection);
						var data = dt.Generar(query, draw, start, length, search, order, columns);
						return data;
					}
					else
					{
						return DataTables.ObtenerDataTableVacia(null, draw);
					}
				}
				else
				{
					return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
				}
			}
			catch (Exception ex)
			{
				return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
			}
		}

		[WebMethod]
		public static DT.DataTable getDetenidosTabla(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
		{
			//br34k
			try
			{
				if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Consultar)
				{
					if (!emptytable)
					{
						if (tracking == "")
						{
							return DataTables.ObtenerDataTableVacia(null, draw);
						}

						MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

						List<Where> where = new List<Where>();
						Entity.Evento evento = ControlEvento.ObtenerByTrackingId(new Guid(tracking));

						where.Add(new Where("D.EventoId", evento.Id.ToString()));

						Query query = new Query
						{
							select = new List<string>
							{
								"D.Nombre",
								"D.Paterno",
								"D.Materno",
								"S.Nombre Sexo",
								"ifnull(D.Edad,0) as Edad",
								"concat(D.Nombre,' ',TRIM(Paterno),' ',TRIM(Materno)) as NombreCompleto",
								"D.Id",
								"D.EventoId"
                                //"ifnull(TIMESTAMPDIFF(YEAR,Fechanacimiento,CURDATE()),0) AS Edad"
                            },
							from = new DT.Table("detenido_evento", "D"),
							joins = new List<Join>
							{
								new Join(new DT.Table("sexo", "S"), "D.SexoId = S.Id"),
							},
							wheres = where
						};

						DataTables dt = new DataTables(mysqlConnection);
						var data = dt.Generar(query, draw, start, length, search, order, columns);
						return data;
					}
					else
					{
						return DataTables.ObtenerDataTableVacia(null, draw);
					}
				}
				else
				{
					return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para realizar la acción.", draw);
				}
			}
			catch (Exception ex)
			{
				return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
			}
		}

		[WebMethod]
		public static object getCall(string LlamadaId)
		{
			try
			{
				Entity.Llamada llamada = ControlLlamada.ObtenerById(Convert.ToInt32(LlamadaId));
				EventoAux obj = new EventoAux();

				if (llamada != null)
				{
					var colonia = ControlColonia.ObtenerPorId(llamada.IdColonia);
					var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
					var estado = ControlEstado.Obtener(municipio.EstadoId);
					var pais = ControlCatalogo.Obtener(estado.IdPais, 45);

					obj.CodigoPostal = colonia.CodigoPostal;
					obj.Descripcion = llamada.Descripcion;
					obj.ColoniaId = llamada.IdColonia.ToString();
					obj.IdEstado = estado.Id.ToString();
					obj.IdMunicipio = municipio.Id.ToString();
					obj.IdPais = pais.Id.ToString();
					obj.Lugar = llamada.Lugar;
				}

				return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", obj });
			}
			catch (Exception e)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
			}
		}

		[WebMethod]
		public static string getFolio()
		{
			try
			{
				var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
				int folio = 0;
				var eventos = ControlEvento.ObtenerEventos();

				if (eventos.Count > 0)
				{
					folio = eventos.Where(x => x.ContratoId == contratoUsuario.IdContrato).Count() + 1;
				}
				else
				{
					folio = 1;
				}

				return JsonConvert.SerializeObject(new { exitoso = true, folio });
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = true, folio = "1", });
			}
		}

		[WebMethod]
		public static List<Combo> getNeighborhoods(string idMunicipio)
		{
			List<Combo> combo = new List<Combo>();

			var colonias = ControlColonia.ObtenerPorMunicipioId(Convert.ToInt32(idMunicipio));

			if (colonias.Count > 0)
			{
				foreach (var rol in colonias.Where(x => x.Activo))
					combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
			}

			return combo;
		}

		[WebMethod]
		public static List<Combo> getUnidades()
		{
			List<Combo> combo = new List<Combo>();

			int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
			Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

			List<Entity.Catalogo> unidades = ControlCatalogo.ObtenerTodo(82);

			if (unidades.Count > 0)
			{
				foreach (var rol in unidades)
				{
					if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
					{
						combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
					}
				}
			}

			return combo;
		}

		[WebMethod]
		public static List<Combo> getSexo()
		{
			List<Combo> combo = new List<Combo>();


			List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));


			if (catalogo.Count > 0)
			{
				foreach (var rol in catalogo)
					combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
			}

			return combo;
		}

		[WebMethod]
		public static List<Combo> getResponsables()
		{
			List<Combo> combo = new List<Combo>();

			int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
			Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

			List<Entity.Catalogo> responsables = ControlCatalogo.ObtenerTodo(83);

			if (responsables.Count > 0)
			{
				foreach (var rol in responsables)
				{
					if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
					{
						combo.Add(new Combo { Desc = rol.Descripcion + "-" + rol.Nombre, Id = rol.Id.ToString() });
					}
				}
			}

			return combo;
		}

		[WebMethod]
		public static string getZipCode(string idColonia)
		{
			try
			{
				var colonia = ControlColonia.ObtenerPorId(Convert.ToInt32(idColonia));
				string codigoPostal = "";
				if (colonia != null)
				{
					codigoPostal = colonia.CodigoPostal;
				}

				return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", cp = codigoPostal });
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, });
			}
		}


		//surrender
		[WebMethod]
		public static EventoPibote fakeEvent(string id, string dependenceId) {
			string record = "";
			RandomCatalogue decorator = new RandomCatalogue();
            EventoPibote obj = new EventoPibote();
            try
            {
                string _url = "http://c5-amb-postproduccion.promad.com.mx:9197" + "/api";
                string[] extends = { "/personainvolucrada/getRegistros", "/informacionincidente/obtenerInformacionIncidente" };
				string delay = _url + extends[0];
                int nDet = 0;
				string suburb = "";
				string town = "";
				string reason = "";
				string origin = "";
                JArray paramsOther = new JArray();
                JArray paramsArray = new JArray();
                //RandomPetition randomPetition = new RandomPetition();
                string json = new JavaScriptSerializer().Serialize(new // adjoining
                {
                    uuid = 5,
                    idEvento = id
                });
                using (var client = new HttpClient())
                {
                    var response = client.PostAsync(new Uri(delay), new StringContent(json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult(); ;
                    //br34k
                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                        var jObj = JObject.Parse(responseContent);
                        paramsArray = (JArray)jObj["items"];
                        foreach (JToken i in paramsArray)
                        {
                            if ((string)i["nombre_tipo_persona"] == "DETENIDO")
                            {
                                nDet++; 
                                int n = (int)i["id_persona_involucrada"];
                                string name = (string)i["nombre"];
                                string first = (string)i["apellido_paterno"];
                                string second = (string)i["apellido_materno"];
                                string nick = (string)i["alias"];
                                int age = (int)i["edad"];
                                int sex = (int)i["id_sexo"];
                                RandomObjects instance = new RandomObjects();
                            }
                        }
                    }
                    delay = _url + extends[1];
                    json = new JavaScriptSerializer().Serialize(new
                    {
                        uuid = 5,
                        idEvento = id,
                        idInstitucion = dependenceId
                    });
                    response = client.PostAsync(new Uri(delay), new StringContent(json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult(); ;

					if (response.IsSuccessStatusCode)
					{
						var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
						var jObj = JObject.Parse(responseContent);
						var evnt = (JObject)jObj["datoEvento"];
						paramsArray.Add(evnt);
						paramsOther = (JArray)jObj["descripcion"];
						foreach (JToken i in paramsArray)
						{
							obj.Id = id;
							obj.Folio = (string)i["folio"];
							obj.HoraYFecha = (string)i["fecha_inicio"];
                            obj.OrigenId = (int)i["id_origen"];
                            obj.MotivoId = (int)i["id_motivo"];
							obj.NDet = nDet;
							obj.Latitud = (string)i["latitud"];
							obj.Longitud = (string)i["longitud"];
							obj.CodigoPostal = (string)i["codigo_postal"];
							obj.Lugar = (string)i["calle"] + " " + (string)i["entre_calle_2"] + " " + (string)i["numero"];
							obj.ColoniaId = (int)i["id_colonia"];
							obj.MunicipioId = (int)i["id_municipio"];
							obj.EstadoId = (int)i["id_estado"];
                            //origin = (string)i["nombre_origen"];
                            reason = (string)i["nombre_motivo"];
							town = (string)i["nombre_municipio"];
							suburb = (string)i["nombre_colonia"];
						}

						decorator.data_id = obj.MunicipioId.ToString();
						decorator.data_support = town;
                        decorator.data = suburb;
                        decorator.save("st", obj.ColoniaId);
						decorator.data = reason ;
                        decorator.save("r", obj.MotivoId);
                        foreach (JToken i in paramsOther)
                        {
                            string log = (string)i["descripcion"];
                            if (log != "")
                                obj.Descripcion = log;
                        }
                        paramsOther = (JArray)jObj["instituciones"];
                        foreach (JToken i in paramsOther)
                        {
                            int[] short_id = new int[] { (int)i["id_institucion"], (int)i["id_recurso"] };
                            decorator.data = (string)i["nombre_institucion"];
                            decorator.data_support = (string)i["nombre"];
                            decorator.save("i", short_id[0]);
                            decorator.data = (string)i["nombre_recurso"];  
                            decorator.data_desc = (string)i["nombre_tipo_recurso"];
                            decorator.data_id = short_id[0].ToString();
                            decorator.save("u", short_id[1]);
                            obj.InstitucionId = short_id[0]; obj.RecursoId = short_id[1];
                        }                        
                    }
                }
				record = "Enhorabuena, registro generado exitosamente!";
            }

            catch (Exception e) { 
				record = "Ha sucedido un error " + e.ToString();	
			}
			obj.Record = record;
			return obj;
        }

        //suspe$e
        [WebMethod]
		public static object getEventoByKeys(string ID, string OtherId)
		{
			try
			{
				//surrender | emergency
				var data = fakeEvent(ID, OtherId);
				int relacionado = new int();
				//var colonia = ControlColonia.ObtenerPorId(data.ColoniaId);
				//var municipio = ControlMunicipio.Obtener(data.MunicipioId);
				//var estado = ControlEstado.Obtener(data.EstadoId);
				//var pais = ControlCatalogo.Obtener(estado.IdPais, 45);
				//data.HoraYFecha = data.HoraYFecha.ToString("dd/MM/yyyy HH:mm:ss");			
				
				if (data.InstitucionId > 0)
					relacionado = 1;

				return JsonConvert.SerializeObject(new { exitoso = true, mensaje = data.Record, data, relacionado });
			}
			catch (Exception e)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
			}
		}

		[WebMethod]
		public static List<Combo> getCalls()
		{
			List<Combo> combo = new List<Combo>();

			int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
			Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

			var calls = ControlLlamada.ObtenerTodos();
			if (calls.Count > 0)
			{
				foreach (var rol in calls)
				{
					if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
					{
						combo.Add(new Combo { Desc = rol.Descripcion.Replace("\"", ""), Id = rol.Id.ToString() });
					}
				}
			}

			return combo;
		}

		[WebMethod]
		public static List<Combo> getUnidadesAW(string institucionId)
		{
			List<Combo> combo = new List<Combo>();

			int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
			Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
			List<Entity.WSAUnidad> unidad = new List<Entity.WSAUnidad>();

			if (!string.IsNullOrWhiteSpace(institucionId))
			{
				unidad = ControlWSAUnidad.ObtenerTodosPorIdInstitucion(Convert.ToInt32(institucionId));
			}

			if (unidad.Count > 0)
			{
				foreach (var rol in unidad)
				{
					combo.Add(new Combo { Desc = rol.ClaveUnidad, Id = rol.IdUnidadInstitucion.ToString() });
				}
			}

			return combo;
		}

		[WebMethod]
		public static List<Combo> getInstitucionesAW()
		{
			List<Combo> combo = new List<Combo>();

			int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
			Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
			Entity.Subcontrato subcontrato = new Entity.Subcontrato();
			List<Entity.WSAInstitucion> instituciones = new List<Entity.WSAInstitucion>();

			subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

			if (subcontrato != null)
			{
				var subcontratoWSAInstituciones = ControlSubcontratoWSAInstitucion.ObtenerPorSubcontratoId(subcontrato.Id);
				if (subcontratoWSAInstituciones.Count > 0)
				{
					foreach (var item in subcontratoWSAInstituciones)
					{
						var institucionWeb = ControlWSAInstitucion.ObtenerPorId(item.IdWSAInstitucion);
						instituciones.Add(institucionWeb);
					}
				}
			}

			if (instituciones.Count > 0)
			{
				foreach (var rol in instituciones)
				{
					combo.Add(new Combo { Desc = rol.NombreInstitucion, Id = rol.IdInstitucion.ToString() });
				}
			}

			return combo;
		}

		[WebMethod]
		public static string getEventosAW(string idUnidadInstitucion)
		{
			// List<Combo> combo = new List<Combo>();
			List<object> lista = new List<object>();
			Entity.WSAUnidad unidad = new Entity.WSAUnidad();
			EventoAWAux evento = new EventoAWAux();
			string longitud = "";
			string latitud = "";

			unidad = ControlWSAUnidad.ObtenerPorId(Convert.ToInt32(idUnidadInstitucion));

			if (unidad != null)
			{
				//string evt = callWebServicesCAD(0,0,0);

				/*evento = callWebServicesEventos(unidad.IdUnidadInstitucion, unidad.IdInstitucion);
                if (evento != null)
                {
                    var fechaEventoWS = Convert.ToDateTime(evento.fechaCapturaDetenidos);
                    var diffFecha = DateTime.Now - fechaEventoWS;
                    if (diffFecha.Days == 0 && diffFecha.Hours < 3)
                    {
                        string detenidos = string.Empty;
                        Entity.WSAMotivo motivo = new Entity.WSAMotivo();
                        Entity.WSAEstado estado = new Entity.WSAEstado();
                        Entity.WSAMunicipio municipio = new Entity.WSAMunicipio();
                        Entity.WSAColonia colonia = new Entity.WSAColonia();

                        motivo = ControlWSAMotivo.ObtenerPorId(Convert.ToInt32(evento.idMotivoEvento));
                        estado = ControlWSAEstado.ObtenerPorId(Convert.ToInt32(evento.lugar.idEstado));
                        municipio = ControlWSAMunicipio.ObtenerPorId(Convert.ToInt32(evento.lugar.idMunicipio));
                        colonia = ControlWSAColonia.ObtenerPorId(Convert.ToInt32(evento.lugar.idColonia));

                        if (evento.detenidos != null)
                        {
                            int contador = 1;
                            foreach (var detenido in evento.detenidos)
                            {
                                detenidos = detenidos.Trim() + " " + contador + ") " + detenido.nombre + " " + detenido.apellidoPaterno + " " + detenido.apellidoMaterno + "/" + detenido.sexo + "/" + detenido.edad + "\n";
                                contador++;
                            }
                        }

                        object obj = new
                        {
                            Folio = evento.folio,
                            Fecha = evento.fechaEvento,
                            Motivo = motivo != null ? motivo.NombreMotivoLlamada : "Sin motivo",
                            NumeroDetenidos = evento.numeroDetenidos,
                            Estado = estado != null ? estado.NombreEstado : "Sin estado",
                            Municipio = municipio != null ? municipio.NombreMunicipio : "Sin municipio",
                            Colonia = colonia != null ? colonia.NombreColonia : "Sin colonia",
                            Numero = evento.lugar.numero != "" ? evento.lugar.numero : "Sin número",
                            EntreCalle = evento.lugar.entreCalle != "" ? evento.lugar.entreCalle : "" + " " + evento.lugar.ycalle != "" ? evento.lugar.ycalle : "",
                            Responsable = evento.nombreResponsable != "" ? evento.nombreResponsable : "",
                            Descripcion = evento.descripcion != "" ? evento.descripcion : "",
                            Detenidos = detenidos,
                            UnidadId = evento.idUnidad
                        };

                        lista.Add(obj);

                        latitud = evento.lugar.latitud;
                        longitud = evento.lugar.longitud;
                    }
                }*/
			}

			// unset
			return JsonConvert.SerializeObject(new { latitud = latitud, longitud = longitud, lista = lista });
		}

		[WebMethod]
		public static DataTable callWebServicesCAD(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
		//br34k
		{
			// declare on the .config
			bool non_real_test = true;
            int r = 0, s = 0, l = 0;
            DataTable defaultTable = new DT.DataTable();
            List<object> dataSet = new List<object>();
            var alertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
            try
			{
				if (non_real_test)
				{
					using (var client = new HttpClient())  // instance HttpClient
					{
						string json = new JavaScriptSerializer().Serialize(new
						{
							uuid = 5,
							idRolUsuario = 33,
							idUsuario = 6
						});

						var _url = "http://c5-amb-postproduccion.promad.com.mx:9197/api/despachador-eventos/obtenerEventos";
						var response = client.PostAsync(new Uri(_url), new StringContent(json, Encoding.UTF8, "application/json")).GetAwaiter().GetResult(); ;
						if (response.IsSuccessStatusCode)
						{
							var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
							var jObj = JObject.Parse(responseContent);
							JArray paramsArray = (JArray)jObj["eventos"];
							foreach (JToken i in paramsArray)
							{
								r++; s++;
								if (s > start)
								{
									if (l < length)
									{
										Object obj = new
										{
											Institucion = (string)i["institucion"],
											Descripcion = (string)i["motivo"],
											//4lter
											Coordenadas = (string)i["latitud"] + "/" + (string)i["longitud"],
											CodigoPostal = (string)i["zps"],
											Asentamiento = (string)i["sector"],
											Municipio = (string)i["nombreMunicipio"],
											Estado = "CDMX",
											Pais = "Mexico",
											Folio = (string)i["folio"],
											HoraYFecha = (string)i["fechaInicio"],
											//unte3st
											Identifiers = (int)i["idInstitucion"] + '/' + (int)i["idEvento"],
											Habilitado = 1
										};
										dataSet.Add(obj);
										l++;
									}
								}
							}
						}
					}
				}
				//return JsonConvert.SerializeObject(dataSet); }
				else
				{
					//JArray paramsArray = (JArray)jObj["eventos"];
					r++; s++;
					if (s > start)
					{
						if (l < length)
						{
							Object obj = new
							{
								Institucion = "S.T.A.R.S",
								Descripcion = "Evidence reasøn",
								//4lter
								Coordenadas = "",
								CodigoPostal = "00182",
								Asentamiento = "Racoon City P.D.",
								Municipio = "",
								Estado = "CDMX",
								Pais = "Mexico",
								Folio = "0635",
								HoraYFecha = DateTime.Now.ToString(),
								//unte3st
								Identifiers = "",
								Habilitado = 1
							};
							dataSet.Add(obj);
							l++;
						
						}
					}
				}
			}
			catch { }

            //return JsonConvert.SerializeObject(dataSet); }
            defaultTable.data = dataSet;
            defaultTable.recordsFiltered = r;
            defaultTable.recordsTotal = r;

            return defaultTable;
		}


		[WebMethod]
		public static List<Combo> getEvento(string tracking)
		{
			List<Combo> combo = new List<Combo>();
			Entity.Evento evento;
			if (tracking != null)
			{
				evento = ControlEvento.ObtenerByTrackingId(new Guid(tracking));

				if (evento != null)
				{
					combo.Add(new Combo { Desc = evento.Folio + " " + evento.Descripcion, Id = evento.Id.ToString() });
				}
			}

			return combo;
		}

		
		[WebMethod]
		public static string saveDetenidoBarandilla(string[] datos)
		{
			try
			{
				if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    int expediente = 0;
                    Entity.Evento evento = new Entity.Evento();
					Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
					Entity.DetalleDetencion estatusTemp = new Entity.DetalleDetencion();
					Entity.Institucion institucion = new Entity.Institucion();
					List<Entity.Detenido> listaDetenido = new List<Entity.Detenido>();
					var mode = string.Empty;
					Boolean alerta = false;
					int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
					string nombreDetenido = string.Empty;

					var detenidoAux = ControlDetenido.ObtenerPorId(Convert.ToInt32(datos[1]));
					detenidoEvento = ControlDetenidoEvento.ObtenerPorId(Convert.ToInt32(datos[1]));
					evento = ControlEvento.ObtenerById(Convert.ToInt32(datos[0]));

					//dtn
                    Entity.Detenido detenido = new Entity.Detenido();
                    detenido.TrackingId = Guid.NewGuid();
                    detenido.Nombre = detenidoEvento.Nombre;
					detenido.Paterno = detenidoEvento.Paterno;
					detenido.Materno = detenidoEvento.Materno;
					detenido.Detenidoanioregistro = DateTime.Now.Year;
					detenido.Detenidomesregistro = DateTime.Now.Month;

                    //if (contratoUsuarioAux.Tipo == "contrato")
                    //	institucion = contratoUsuarioAux != null ? ControlInstitucion.ObtenerPorId(ControlContrato.ObtenerPorId(contratoUsuarioAux.IdContrato).InstitucionId) : null;
                    int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]); 
					institucion = idContrato != 0 ? ControlInstitucion.ObtenerPorId(ControlSubcontrato.ObtenerPorId(idContrato).InstitucionId) : null;

                    //det/\il
                    Entity.DetalleDetencion estatus = new Entity.DetalleDetencion();
                    estatus.TrackingId = Guid.NewGuid();
                    estatus.CentroId = 0;
					estatus.Fecha = DateTime.Now;
					estatus.NCP = "ncp";
                    estatus.CentroId = institucion.Id;
                    estatus.Expediente = Convert.ToString(expediente);
                    estatus.Activo = true;
					estatus.ExpedienteAdm = "";
					estatus.Estatus = 1;
					estatus.ContratoId = idContrato;
					estatus.Tipo = "subcontrato";
					estatus.Detalledetencionanioregistro = DateTime.Now.Year;
					estatus.Detalldetencionmesregistro = DateTime.Now.Month;
					estatus.NombreDetenido = detenido.Nombre;
					estatus.APaternoDetenido = detenido.Paterno;
					estatus.AMaternoDetenido = detenido.Materno;
					estatus.DetalledetencionSexoId = detenidoEvento.SexoId;
					estatus.DetalledetencionEdad = detenidoEvento.Edad;

					if (detenido != null)
					{
                        // collect patch
                        var listadetalleDetencion = ControlDetalleDetencion.ObteneTodos();
						var listaDetalleDetencionFiltrada = listadetalleDetencion.Where(x => x.ContratoId == idContrato);
						//&& x.Tipo == contratoUsuarioAux.Tipo);

						//r3 d0
						if (listaDetalleDetencionFiltrada.Count() > 0)
						{
							estatusTemp = listaDetalleDetencionFiltrada.Last();
							expediente = Convert.ToInt32(estatusTemp.Expediente) + 1;
						}
						else expediente = 1;

						mode = "registró";
						nombreDetenido = detenido.Nombre + ' ' + detenido.Paterno + ' ' + detenido.Materno;
						listaDetenido = ControlDetenido.ObtenePorNombre(nombreDetenido);

						if (listaDetenido.Count > 0)
						{
							mode = "Se detectaron a detenidos con información similar, el registro se realizó correctamente.";
							alerta = true;
						}

						detenido.Id = ControlDetenido.Guardar(detenido);
						estatus.DetenidoId = detenido.Id;
						estatus.Id = ControlDetalleDetencion.Guardar(estatus);

						//infø
                        Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                        infoDetencion.Activo = true;
						infoDetencion.IdInterno = detenido.Id;
						infoDetencion.CreadoPor = usId;
						infoDetencion.Descripcion = evento.Descripcion;
						infoDetencion.Folio = evento.Folio;
						infoDetencion.Habilitado = true;
						infoDetencion.HoraYFecha = Convert.ToDateTime(evento.HoraYFecha);
						infoDetencion.ColoniaId = Convert.ToInt32(evento.ColoniaId);
						infoDetencion.ResponsableId = 0;
						infoDetencion.UnidadId = 0;
						infoDetencion.LugarDetencion = evento.Lugar;
						infoDetencion.Motivo = evento.Descripcion;
						infoDetencion.TrackingId = Guid.NewGuid();
						infoDetencion.IdEvento = Convert.ToInt32(evento.Id);
						infoDetencion.IdDetenido_Evento = detenidoEvento.Id;
                        infoDetencion.Id = ControlInformacionDeDetencion.Guardar(infoDetencion);

                        //unsafe
                        Entity.General general = new Entity.General();
						general.DetenidoId = detenido.Id;
						general.TrackingId = Guid.NewGuid();
						general.RFC = "";
						general.NacionalidadId = 38;
						general.EscolaridadId = 19;
						general.ReligionId = 0;
						general.OcupacionId = 18;
						general.EstadoCivilId = 0;
						general.EtniaId = 0;
						general.SexoId = detenidoEvento.SexoId;
						general.EstadoMental = false;
						general.Inimputable = false;
						general.Edaddetenido = detenidoEvento.Edad;
						general.Generalanioregistro = DateTime.Now.Year;
						general.Generalmesregistro = DateTime.Now.Month;
						//general.FechaNacimineto = detenidoEvento.Fechanacimiento;
						general.Id = ControlGeneral.Guardar(general);

						if (estatus.Id > 0)
						{
							Entity.Historial historial = new Entity.Historial();
							historial.Activo = true;
							historial.CreadoPor = usId;
							historial.Fecha = DateTime.Now;
							historial.Habilitado = true;
							historial.InternoId = detenido.Id;
							historial.Movimiento = "Registro del detenido en barandilla";
							historial.TrackingId = detenido.TrackingId;
							var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
							var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
							historial.ContratoId = subcontrato.Id;
							historial.Id = ControlHistorial.Guardar(historial);
						}
					}

					return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, alerta = alerta, id = detenido.TrackingId });
				}
				else
				{
					return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
				}
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
			}
		}


		[WebMethod]
		public static string saveEvento(Dictionary<string, string> csObj)
		{
			try
			{
				if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
				{
					var item = new Entity.Evento();
					var mode = string.Empty;
					int eventoId = 0;
					int folio = 0;
					Entity.Evento evento;
					string folioAux = string.Empty;

					var eventos = ControlEvento.ObtenerEventos();
					var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
					EventoAWAux eventoAux = new EventoAWAux();
					Entity.Subcontrato subcontrato = new Entity.Subcontrato();

					if (eventos.Where(x => x.ContratoId == contratoUsuario.IdContrato).Count() > 0)
					{
						evento = eventos.Last();
						folio = Convert.ToInt32(evento.Folio) + 1;
					}
					else
					{
						folio = 1;
					}

					subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

					//
					Entity.Institucion institucion = new Entity.Institucion();
					Entity.Domicilio domicilio = new Entity.Domicilio();

					if (subcontrato != null)
						institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

					if (institucion != null)
						domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

					var fecha = Convert.ToDateTime(csObj["fecha"]);
					fecha = fecha.AddSeconds(-fecha.Second);
					var fechaNow = DateTime.Now;
					fechaNow = fechaNow.AddSeconds(-fechaNow.Second);
					if (fecha > DateTime.Now) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha y hora no puede ser mayor a la fecha actual", fallo = "fecha" });


					///
					item.Descripcion = csObj["descripcion"];
					//item.Colonia = Convert.ToInt32(csObj["colonia"]);
					item.HoraYFecha = Convert.ToDateTime(csObj["fecha"]);
					item.LlamadaId = Convert.ToInt32(csObj["llamada"]);
					item.Lugar = csObj["lugar"];
					item.ColoniaId = Convert.ToInt32(csObj["colonia"]);
					item.NumeroDetenidos = Convert.ToInt32(csObj["numeroDetenidos"]);
					item.Activo = true;
					item.Habilitado = true;
					item.ContratoId = contratoUsuario.IdContrato;
					item.Tipo = contratoUsuario.Tipo;
					item.Latitud = csObj["latitud"];
					item.Longitud = csObj["longitud"];
					item.MotivoId = Convert.ToInt32(csObj["motivoevento"]);

					if (csObj["tracking"] != "")
					{
						evento = ControlEvento.ObtenerByTrackingId(new Guid(csObj["tracking"]));
						eventoId = evento.Id;
						item.Eventoanioregistro = evento.Eventoanioregistro;
						item.Eventomesregistro = evento.Eventomesregistro;
						mode = "actualizó";
						item.TrackingId = new Guid(csObj["tracking"]);
						item.Folio = evento.Folio;
						ControlEvento.Actualizar(item);
						folioAux = evento.Folio;
					}
					else
					{
						item.Eventoanioregistro = DateTime.Now.Year;
						item.Eventomesregistro = DateTime.Now.Month;
						item.TrackingId = Guid.NewGuid();
						item.IdEventoWS = 0;
						item.Folio = Convert.ToString(folio);
						eventoId = ControlEvento.Guardar(item);
						var _evento = ControlEvento.ObtenerById(eventoId);
						folioAux = _evento.Folio;
						mode = "registró";
					}

					return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, EventoId = eventoId.ToString(), tracking = item.TrackingId.ToString(), folio = folioAux });
				}
				else
				{
					return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });

				}
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
			}
		}

		[WebMethod]
		public static string saveUnidad(Dictionary<string, string> csObj)
		{
			try
			{
				if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
				{
					var item = new Entity.EventoUnidadResponsable();
					var evento = new Entity.Evento();
					var mode = string.Empty;

					item.EventoId = Convert.ToInt32(csObj["EventoId"]);
					item.UnidadId = Convert.ToInt32(csObj["unidad"]);
					item.ResponsableId = Convert.ToInt32(csObj["responsable"]);

					var unidadresponsable = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { item.UnidadId, item.EventoId });

					if (unidadresponsable != null)
					{
						if (unidadresponsable.Id != 0)
							return JsonConvert.SerializeObject(new { exitoso = false, alerta = true, mensaje = "La unidad ya se encuentra registrada en el evento" });
					}

					if (csObj["EventoId"] != "")
					{
						ControlEventoUnidadResponsable.Guardar(item);
						mode = "registró";

						evento = ControlEvento.ObtenerById(item.EventoId);
					}

					return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, tracking = evento.TrackingId });
				}
				else
				{
					return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
				}
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
			}
		}


		[WebMethod]
		public static string saveDetenido(Dictionary<string, string> csObj, List<DetenidoEventoData> list)
		{
			try
			{
				if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
				{
					var eventodet = ControlEvento.ObtenerById(Convert.ToInt32(csObj["EventoId"]));
					string detenidosinguardar = "";
					var membershipUser2 = Membership.GetUser(HttpContext.Current.User.Identity.Name);

					List<Entity.DetenidoEvento> ListDetenidoaguardar = new List<Entity.DetenidoEvento>();
					int i = 0;
					foreach (var detenidoEventoData in list)
					{
						Entity.DetenidoEvento Detevento = new Entity.DetenidoEvento();
						Detevento.Activo = true;
						Detevento.CreadoPor = Convert.ToInt32(membershipUser2.ProviderUserKey);
						Detevento.Edad = Convert.ToInt32(detenidoEventoData.Edad);
						Detevento.EventoId = Convert.ToInt32(detenidoEventoData.EventoId);
						Detevento.Habilitado = true;
						Detevento.Nombre = detenidoEventoData.Nombre;
						Detevento.Paterno = detenidoEventoData.Paterno;
						Detevento.Materno = detenidoEventoData.Materno;
						Detevento.SexoId = Convert.ToInt32(detenidoEventoData.SexoId);
						Detevento.MotivoId = Convert.ToInt32(detenidoEventoData.Motivo);

						var detenidos2 = ControlDetenidoEvento.ObtenerPorEventoId(Convert.ToInt32(csObj["EventoId"]));
						if (detenidos2.Count() + i >= eventodet.NumeroDetenidos)
						{
							detenidosinguardar += detenidoEventoData.Nombre + " " + detenidoEventoData.Paterno + " " + detenidoEventoData.Materno + ",";


						}
						else
						{
							ListDetenidoaguardar.Add(Detevento);

						}
						i++;

					}
					Boolean alertadet = false;

					if (detenidosinguardar != "")
					{
						alertadet = true;
						detenidosinguardar = " Se llegó al límite de detenidos los siguientes detenidos no se guardaron:  " + detenidosinguardar;
						detenidosinguardar = detenidosinguardar.Substring(0, detenidosinguardar.Length - 1);
						detenidosinguardar = detenidosinguardar + ".";
					}

					if (ListDetenidoaguardar.Count() == 0)
					{
						return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "Se ha llegado al límite de detenidos en el evento." });

					}
					else
					{
						foreach (var item in ListDetenidoaguardar)
						{
							item.TrackingId = Guid.NewGuid();
							ControlDetenidoEvento.Guardar(item);

						}

					}

					return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "registro", Alertadetenido = alertadet, Mensajealerta = detenidosinguardar });
				}
				else
				{
					return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
				}
			}
			catch (Exception ex)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
			}
		}

		//[WebMethod]
		//public static string saveDetenido(Dictionary<string, string> csObj)
		//{
		//    try
		//    {
		//        if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
		//        {
		//            var item = new Entity.DetenidoEvento();
		//            var mode = string.Empty;
		//            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
		//            int usuario;

		//            if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
		//            else throw new Exception("No es posible obtener información del usuario en el sistema");

		//            item.EventoId = Convert.ToInt32(csObj["EventoId"]);
		//            item.Nombre = csObj["nombreDetenido"];
		//            item.Paterno = csObj["paternoDetenido"];
		//            item.Materno = csObj["maternoDetenido"];
		//            item.SexoId = Convert.ToInt32(csObj["sexoDetenido"]);
		//            //item.Fechanacimiento =Convert.ToDateTime( csObj["FechaNacimiento"]);
		//            item.Edad = Convert.ToInt32(csObj["Edad"]);
		//            item.Activo = true;
		//            item.Habilitado = true;
		//            item.CreadoPor = usuario;

		//            //if (CalcularEdad(item.Fechanacimiento) < 1)
		//            //{
		//            //    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = " \n La fecha de nacimiento debe de ser mayor a un año" });
		//            //}
		//            if (item.Edad < 1) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La edad debe ser mayor a un año." });
		//            else if (item.Edad > 105) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La edad debe ser menor a 105 años." });

		//            var detenidos = ControlDetenidoEvento.ObtenerPorEventoId(item.EventoId);
		//            var evento = ControlEvento.ObtenerById(item.EventoId);

		//            if (evento.NumeroDetenidos == detenidos.Count)
		//            {
		//                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "Se ha llegado al límite de detenidos en el evento." });
		//            }

		//            if (csObj["EventoId"] != "")
		//            {
		//                item.TrackingId = Guid.NewGuid();
		//                ControlDetenidoEvento.Guardar(item);
		//                mode = "registró";
		//            }

		//            return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode });
		//        }
		//        else
		//        {
		//            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
		//    }
		//}


		[WebMethod]
		public static Object getPaisEstadoMunicipio()
		{
			try
			{
				Entity.Institucion institucion = new Entity.Institucion();
				Entity.Domicilio domicilio = new Entity.Domicilio();
				Entity.Subcontrato subcontrato = new Entity.Subcontrato();
				string paisId = "0";
				string estadoId = "0";
				string municipioId = "0";

				var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

				if (contratoUsuario != null)
					subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

				if (subcontrato != null)
					institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

				if (institucion != null)
					domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

				if (domicilio != null)
				{
					paisId = domicilio.PaisId.ToString();
					estadoId = domicilio.EstadoId.ToString();
					municipioId = domicilio.MunicipioId.ToString();
				}

				var pais = ControlCatalogo.Obtener(domicilio.PaisId, 45);
				var estado = ControlEstado.Obtener(domicilio.EstadoId ?? 0);
				var municipio = ControlMunicipio.Obtener(domicilio.MunicipioId ?? 0);
				if (pais == null || estado == null || municipio == null) throw new Exception("No se encontró parte del domicilio del subcontrato.");

				var textoLocalizacion = $"{pais.Nombre[0].ToString().ToUpper() + pais.Nombre.Substring(1).ToLower()}, {estado.Nombre[0].ToString().ToUpper() + estado.Nombre.Substring(1).ToLower()}-{municipio.Nombre[0].ToString().ToUpper() + municipio.Nombre.Substring(1).ToLower()}";

				return JsonConvert.SerializeObject(new { exitoso = true, Pais = paisId, Estado = estadoId, Municipio = municipioId, Localizacion = textoLocalizacion });
			}
			catch (Exception e)
			{
				return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
			}
		}

		[WebMethod]
		public static List<Combo> getDetenidosEventoReciente(string id)
		{
			List<Combo> combo = new List<Combo>();
			List<Entity.DetenidoEvento> detenidos;
			Entity.Evento evento = new Entity.Evento();

			evento = ControlEvento.ObtenerByTrackingId(new Guid(id));

			detenidos = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);

			if (detenidos.Count > 0)
			{
				foreach (var rol in detenidos)
				{
					combo.Add(new Combo { Desc = rol.Nombre + " " + rol.Paterno + " " + rol.Materno, Id = rol.Id.ToString() });
				}
			}

			return combo;
		}

		[WebMethod]
		public static List<Combo> getEventoEventoReciente(string tracking)
		{
			List<Combo> combo = new List<Combo>();
			Entity.Evento evento;

			if (tracking != null)
			{
				evento = ControlEvento.ObtenerByTrackingId(new Guid(tracking));

				if (evento != null)
				{
					combo.Add(new Combo { Desc = evento.Folio + " " + evento.Descripcion, Id = evento.Id.ToString() });
				}
			}

			return combo;
		}

		//unused method

		[WebMethod]
		public static List<Combo> LoadDetenidosAniosTrabajo()
		{
			List<Combo> combo = new List<Combo>();
			var listaanios = ControlDashBoardAnioTrabajo.GetAniosTrabajo();

			List<int> Anios = new List<int>();
			foreach (var item in listaanios)
			{
				Anios.Add(item.Aniotrabajo);
			}

			if (DateTime.Now.Year > Anios.Max())
			{
				Entity.DasboardAnioTrabajo dasboardAnioTrabajo = new Entity.DasboardAnioTrabajo();
				dasboardAnioTrabajo.Aniotrabajo = DateTime.Now.Year;
				listaanios.Add(dasboardAnioTrabajo);
			}
			foreach (var item in listaanios.OrderByDescending(x => x.Aniotrabajo))
			{
				combo.Add(new Combo { Desc = item.Aniotrabajo.ToString(), Id = item.Aniotrabajo.ToString() });
			}

			return combo;
		}

        protected void input_print_Click(object sender, EventArgs e)
        {
			string referencia = input_ref_0.Text + input_ref_1.Text + input_ref_2.Text + input_ref_3.Text + input_ref_4.Text + input_ref_5.Text + input_ref_6.Text + input_ref_7.Text;
			string fecha_incidente = input_d.Text+"/"+ input_m.Text +"/"+ input_y.Text+" "+input_hh.Text+":"+input_mm.Text;
			string institución_policiaca = "Federal";
			if (input_goverment.Checked)
				institución_policiaca = "Federal";
			else
				if (input_policeCity.Checked)
				institución_policiaca = "Estatal";
			else
				if (input_localPolice.Checked)
				institución_policiaca = "Municipal";
			else
				if (input_iother.Checked)
				institución_policiaca = institute_3_other.Text;
			string puesta_a_disposición =name_disposicion.Text+" "+first_disposicion.Text+" "+last_disposicion.Text;
			string grado = input_position.Text;// input_position.SelectedValue;
			IPHData ReportData = new IPHData();
			ReportData.referencia = referencia;
			ReportData.tipo_formato = cmb_format_type.SelectedItem.Text;
			ReportData.descripcion_vehiculo = flag_vehicle.Checked ? "Si" : "No";
			ReportData.fecha_incidente = fecha_incidente;
			ReportData.dispuesto_por = puesta_a_disposición;
			ReportData.institución_policiaca = institución_policiaca;
			ReportData.cargo = grado;
			ReportData.unidad = input_unit.Text;
			ReportData.autoridad_alterna = flag_shift_reception.Checked ? input_shift_autority.Text : "";
			ReportData.adscripcion_alterna = flag_shift_reception.Checked ? cmb_secondment.SelectedItem.Text : "";
			ReportData.cargo_alterno = flag_shift_reception.Checked ? cmb_shift_post.SelectedItem.Text : "";
			ReportData.origen_evento = cmb_event_origin.SelectedItem.Text;
			ReportData.telefono_evento = flag_phone.Checked ? event_lada.Text+"-" +event_phone.Text : "";
			ReportData.direccion_evento = input_street_event.Text +(input_streetRef_event.Text ==""?"": " por "+input_streetRef_event.Text)
				+",Código Postal: "+input_zp_event.Text+" , en la colonia: "+input_neighborhood_event.Text+", en "+
				(cmb_state_event.SelectedItem.Text=="CDMX"?"la Alcaldía de: ":"el municipio de:")+input_mun_event.Text+" en "+cmb_state_event.SelectedItem.Text;
			ReportData.latitud_evento = input_lat_event.Text;
			ReportData.longitud_evento= input_long_event.Text;
			ReportData.nombre_detenido = input_name.Text + " " + input_first.Text + " " + input_last.Text;
			ReportData.edad_detenido = cmb_age.Text;
			ReportData.alias_detenido = input_nickname.Text;
			ReportData.fecha_nacimiento_detenido = day__birthday.Text + "/" + month_birthday.Text + "/" + year_birthday.Text;
			ReportData.sexo_de_detenido = radio_male.Checked ? "Hombre" : "Mujer";
			ReportData.nacionalidad_detenido = radio_mexican_nat.Checked ? "Mexicana" : "Extranjera";
			ReportData.direccion_detenido = input_street_arrested.Text + (input_streetRef_arrested.Text == "" ? " " : " por " + input_streetRef_arrested.Text) + ", " +
				input_zp_arrested.Text+","+input_col_arrested.Text+", en "+(cmb_state_arrested.SelectedItem.Text=="CDMX"?"la Alcaldía de ":"el municipio de ") + input_mun_arrested.Text
				+" en "+cmb_state_arrested.SelectedItem.Text;
			ReportData.padecimientos_detenido = radio_injury_false.Checked ?"No": injury.Text ;
			ReportData.grupo_vulnerabilidad_detenido = radio_ailment_true.Checked ? "No" : input_grupo_vulnerable.Text;
			ReportData.lesiones_detenido = radio_lesiones_true.Checked ? "Si" : "No";
			ReportData.placa_vehiculo = input_plate_0.Text + input_plate_1.Text + input_plate_2.Text +
				input_plate_3.Text + input_plate_4.Text + input_plate_5.Text + input_plate_6.Text + input_plate_7.Text;
			ReportData.marca_vehiculo = cmb_brand_vehicle.SelectedItem.Text;
			ReportData.submarca_vehiculo = cmb_subBrand_vehicle.SelectedItem.Text;
			ReportData.modelo_vehiculo = cmb_model_vehicle.SelectedItem.Text;
			ReportData.color_vehiculo = cmb_color_vehicle.SelectedItem.Text;
			ReportData.fecha_retencion_vehiculo = flag_retention.Checked ? (day_inpugned_vehicle.Text+"/"+
				month_inpugned_vehicle.Text+"/"+year_inpugned_vehicle.Text) : "";
			ReportData.procedencia_vehiculo = radio_mexican_vehicle.Checked ? "Nacional" : "Extranjero";
			ReportData.tipo_vehiculo = radio_type_terrain_vehicle.Checked ? "Terrestre" : "Otro";
			ReportData.uso_vehiculo = "Uso:" + (check_car_type_p.Checked ? " Particular " : "") + (check_car_type_t.Checked ? " Transporte" : "") + (check_car_type_c.Checked ? " Carga" : "");
			ReportData.destino_vehiculo = input_desti_vehicle.Text;
			ReportData.observaciones_vehiculo = input_obs_vehicle.Text;
			ReportData.seguro_vehiculo = radio_secure_vehicle_yes.Checked ? "Si" : "No";
			pdfBeta(ReportData);
        }

       
    }
}