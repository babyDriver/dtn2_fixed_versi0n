﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Linq;

namespace Web.Application.Registry
{
    public partial class callform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static string getFolio()
        {
            try
            {
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                List<Entity.Llamada> llamadas = ControlLlamada.ObtenerTodos();
                int folio = 0;

                if (llamadas == null)
                    folio = 1;
                else
                    folio = llamadas.Where(x=> x.ContratoId== contratoUsuario.IdContrato).Count() + 1;

                return JsonConvert.SerializeObject(new { exitoso = true, Numeroticket = folio.ToString() });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }

        [WebMethod]
        public static Object getLlamadaByTrackingId(string tracking)
        {
            try
            {
                var data = ControlLlamada.ObtenerByTrackingId(new Guid(tracking));

                var colonia = ControlColonia.ObtenerPorId(data.IdColonia);
                var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                var estado = ControlEstado.Obtener(municipio.EstadoId);
                var pais = ControlCatalogo.Obtener(estado.IdPais, 45);

                LLamadaAux obj = new LLamadaAux();
                obj.CodigoPostal = colonia.CodigoPostal;
                obj.Descripcion = data.Descripcion;
                obj.Folio = data.Folio;
                obj.HoraYFecha = data.HoraYFecha.ToString("dd/MM/yyyy HH:mm:ss");
                obj.Id = data.Id.ToString();
                obj.IdColonia = data.IdColonia.ToString();
                obj.IdEstado = estado.Id.ToString();
                obj.IdMunicipio = municipio.Id.ToString();
                obj.IdPais = pais.Id.ToString();
                obj.Involucrado = data.Involucrado;
                obj.Lugar = data.Lugar;
                obj.TrackingId = data.TrackingId.ToString();

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", obj });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string getListadoMunicipio()
        {
            try
            {
                List<Entity.Municipio> list = ControlMunicipio.ObtenerTodo();
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", data = list });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string getListadoSectores()
        {
            try
            {
                List<Entity.Catalogo> list = ControlCatalogo.ObtenerHabilitados(101);
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", data = list });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        [WebMethod]
        public static string save(Dictionary<string, string> csObj)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Llamadas y eventos" }).Modificar)
                {
                    var item = new Entity.Llamada();
                    var mode = string.Empty;
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    var fecha = Convert.ToDateTime(csObj["fecha"]);
                    if (fecha > DateTime.Now) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha y hora no puede ser mayor a la fecha actual", fallo = "fecha" });

                    item.Descripcion = csObj["descripcion"];
                    item.Folio = csObj["folio"];
                    item.HoraYFecha = fecha;
                    item.Involucrado = csObj["involucrado"];
                    item.Lugar = csObj["lugar"];
                    item.IdColonia = Convert.ToInt32(csObj["colonia"]);
                    item.Activo = true;
                    item.Habilitado = true;
                    item.ContratoId = contratoUsuario.IdContrato;
                    item.Tipo = contratoUsuario.Tipo;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    string Folio = "";
                    if (csObj["tracking"] != "")
                    {
                        mode = "actualizó";
                        item.TrackingId = new Guid(csObj["tracking"]);
                        var call = ControlLlamada.ObtenerByTrackingId(item.TrackingId);
                        item.Folio = call.Folio;
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de la llamada";
                        historial.CreadoPor = usuario.Id;
                        var contratoUsuario1 = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario1.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);
                        ControlLlamada.Actualizar(item);
                        Folio = item.Folio;
                    }
                    else
                    {
                        item.TrackingId = Guid.NewGuid();
                        int llamadaId = 0;
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de la llamada";
                        historial.CreadoPor = usuario.Id;
                        var contratoUsuario1 = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario1.IdContrato);
                        historial.ContratoId = subcontrato.Id; 
                        historial.Id = ControlHistorial.Guardar(historial);
                        item.CreadoPor = usId;
                        llamadaId =ControlLlamada.Guardar(item);
                        var lamadainsertada = ControlLlamada.ObtenerById(llamadaId);
                        mode = "registró";
                        Folio = lamadainsertada.Folio;
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode,folio= Folio });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });

                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getCountries()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.pais));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getStates(string idPais)
        {
            List<Combo> combo = new List<Combo>();
            var data = ControlEstado.ObtenerPorPais(Convert.ToInt32(idPais));

            if (data.Count > 0)
            {
                foreach (var rol in data)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCities(string idEstado)
        {
            List<Combo> combo = new List<Combo>();
            var data = ControlMunicipio.ObtenerPorEstado(Convert.ToInt32(idEstado));

            if (data.Count > 0)
            {
                foreach (var rol in data)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getNeighborhoods(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();

            var colonias = ControlColonia.ObtenerPorMunicipioId(Convert.ToInt32(idMunicipio));

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias.Where(x=> x.Activo))
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getZipCode(string idColonia)
        {
            try
            {
                var colonia = ControlColonia.ObtenerPorId(Convert.ToInt32(idColonia));

                string codigoPostal = "";

                if (colonia != null)
                {
                    codigoPostal = colonia.CodigoPostal;
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", cp = codigoPostal });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, });
            }
        }
    }

    public class LLamadaAux
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string IdPais { get; set; }
        public string IdEstado { get; set; }
        public string IdMunicipio { get; set; }
        public string IdColonia { get; set; }
        public string CodigoPostal { get; set; }
        public string Descripcion { get; set; }
        public string Folio { get; set; }
        public string HoraYFecha { get; set; }
        public string Involucrado { get; set; }
        public string Lugar { get; set; }
    }
}