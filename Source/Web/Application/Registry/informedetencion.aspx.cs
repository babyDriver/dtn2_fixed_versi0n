﻿using Business;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Linq;

namespace Web.Application.Registry
{
    public partial class informedetencion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
            string valor = Convert.ToString(Request.QueryString["tracking"]);
            if (valor == "")
                Response.Redirect("entrylist.aspx");

            if (Request.QueryString["editable"] == "1")
            {
                editable.Value = "1";
            }
            else
            {
                editable.Value = "0";
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }

        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                        //Acceso a Ingreso
                        parametros[1] = "Registro en barandilla";
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (parametros != null)
                        {
                            this.Ingreso.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Adicciones
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Adicciones.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Antropometria
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Antropometria.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Señas_Particulares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Señas_Particulares.Value = permisos.Consultar.ToString().ToLower();
                        }



                        //Acceso a Familiares
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Familiares.Value = permisos.Consultar.ToString().ToLower();
                        }


                        //Acceso a Estudios_Criminologicos
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Estudios_Criminologicos.Value = permisos.Consultar.ToString().ToLower();
                        }

                        //Acceso a Expediente_Medico
                        permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                        if (permisos != null)
                        {
                            this.Expediente_Medico.Value = permisos.Consultar.ToString().ToLower();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static string getinfo(string TrackingId)
        {

            try
            {
                
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    var infoDetencion = new Entity.InformeUsoFuerza();
                    int value = Convert.ToInt32(TrackingId.Trim());
                    var detenido = ControlDetenido.ObtenerPorId(value);

                    if (detenido == null)
                    {
                        var det_detencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(TrackingId));
                        detenido = ControlDetenido.ObtenerPorId(det_detencion.DetenidoId);
                    }
                    string[] datadetenido = new string[]
                    {
                        detenido.Id.ToString(),
                        true.ToString()
                    };
                    var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datadetenido);
                    if (detalleDetencion == null) detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(detenido.Id).LastOrDefault();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    var listainforme = ControlInformeUsoFuerza.GetAll();

                    foreach (var item in listainforme.Where(x=> x.DetalledetencionId==detalleDetencion.Id))
                    {
                        infoDetencion = item;
                    }


                    object obj = null;
                    obj = new
                    {
                        Id = infoDetencion!=null? infoDetencion.Id.ToString():"",
                        TrackingId = infoDetencion.TrackingId != null ? infoDetencion.TrackingId.ToString() : "",
                        Primerapellido = infoDetencion.Primerapellido != null ? infoDetencion.Primerapellido.ToString() : "",
                        Segundoapellido = infoDetencion.Segundoapellido != null ? infoDetencion.Segundoapellido.ToString() : "",
                        Nombre = infoDetencion.Nombre != null ? infoDetencion.Nombre.ToString() : "",
                        Adscripcion = infoDetencion.Adscripcion != null ? infoDetencion.Adscripcion.ToString() : "",
                        Cargo = infoDetencion.Cargo != null ? infoDetencion.Cargo.ToString() : "",
                        Autoridadeslesionadas = infoDetencion.Id != 0 ? infoDetencion.Autoridadeslesionadas.ToString() : "",
                        Autoridadesfallecidas = infoDetencion.Id != 0 ? infoDetencion.Autoridadesfallecidas.ToString() : "",
                        Personaslesionadas = infoDetencion.Id != 0 ? infoDetencion.Personaslesionadas.ToString() : "",
                        Personasfallecidas = infoDetencion.Id != 0 ? infoDetencion.Personasfallecidas.ToString() : "",
                        Reduccionfisicademovimientos = infoDetencion.Reduccionfisicademovimientos==true?"true":"false",
                        Utilizaciondearmasincapacitantemenosletales = infoDetencion.Utilizaciondearmasincapacitantemenosletales == true ? "true" : "false",
                        Utilizaciondearmasdefuego = infoDetencion.Utilizaciondearmasdefuego == true ? "true" : "false",
                        DescripcionConducta = infoDetencion.DescripcionConducta != null ? infoDetencion.DescripcionConducta.ToString() : "",
                        Brindosolicitoasistenciamedica = infoDetencion.Brindosolicitoasistenciamedica == true ? "true" : "false",
                        Especifique = infoDetencion.Especifique != null ? infoDetencion.Especifique.ToString() : "",
                        Firma = infoDetencion.Firma != null ? infoDetencion.Firma.ToString() : "",
                        Firma2 = infoDetencion.Firma2 != null ? infoDetencion.Firma2.ToString() : "",

                    };

                    return JsonConvert.SerializeObject(new { exitoso = true,Obj= obj });

                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {

                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }


        }

        [WebMethod]
        public static Object save(informedetencionAux Informe)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Modificar)
                {
                    var infoDetencion = new Entity.InformeUsoFuerza();
                    var detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(Informe.TrackingIdDetenido));
                    string[] datadetenido = new string[]
                    {
                        detenido.Id.ToString(),
                        true.ToString()
                    };
                    var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datadetenido);
                    if (detalleDetencion == null)
                        return new { exitoso = false, mensaje = "No puede modificar la información del detenido." };

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = Informe.Id;
                    if (id == "" || id == "0") Informe.Id = "";

                    if (string.IsNullOrEmpty(Informe.Id))
                    {
                        mode = @"registro";

                        infoDetencion.Primerapellido = Informe.Primerapellido;
                        infoDetencion.Segundoapellido = Informe.Segundoapellido;
                        infoDetencion.Nombre = Informe.Nombre;
                        infoDetencion.Adscripcion = Informe.Adscripcion;
                        infoDetencion.Cargo = Informe.Cargo;
                        infoDetencion.Fechahora = DateTime.Now;
                        infoDetencion.Activo = 1;
                        infoDetencion.DetalledetencionId = detalleDetencion.Id;
                        infoDetencion.Creadopor = usId;
                        infoDetencion.Autoridadeslesionadas =Convert.ToInt32(Informe.Autoridadeslesionadas);
                        infoDetencion.Autoridadesfallecidas = Convert.ToInt32(Informe.Autoridadesfallecidas);
                        infoDetencion.Personaslesionadas = Convert.ToInt32(Informe.Personaslesionadas);
                        infoDetencion.Personasfallecidas = Convert.ToInt32(Informe.Personasfallecidas);
                        infoDetencion.Reduccionfisicademovimientos = Convert.ToBoolean(Informe.Reduccionfisicademovimientos.ToLower());
                        infoDetencion.Utilizaciondearmasincapacitantemenosletales = Convert.ToBoolean(Informe.Utilizaciondearmasincapacitantemenosletales.ToLower());
                        infoDetencion.Utilizaciondearmasdefuego = Convert.ToBoolean(Informe.Utilizaciondearmasdefuego.ToLower());
                        infoDetencion.DescripcionConducta = Informe.DescripcionConducta;
                        infoDetencion.Brindosolicitoasistenciamedica = Convert.ToBoolean(Informe.Brindosolicitoasistenciamedica.ToLower());
                        infoDetencion.DescripcionConducta = Informe.DescripcionConducta;
                        infoDetencion.Firma = Informe.Firma;
                        infoDetencion.Firma2 = Informe.Firma2;
                        infoDetencion.Especifique = Informe.Especifique;


                        infoDetencion.Habilitado = 1;
                        
                        infoDetencion.TrackingId = Guid.NewGuid().ToString();
                        

                        infoDetencion.Id = ControlInformeUsoFuerza.Guardar(infoDetencion);
                        Informe.Id = infoDetencion.Id.ToString();
                    }
                    else
                    {
                        mode = @"actualizo";

                        infoDetencion.Id = Convert.ToInt32(Informe.Id);
                        infoDetencion.TrackingId = new Guid(Informe.TrackingId).ToString();
                        infoDetencion.Primerapellido = Informe.Primerapellido;
                        infoDetencion.Segundoapellido = Informe.Segundoapellido;
                        infoDetencion.Nombre = Informe.Nombre;
                        infoDetencion.Adscripcion = Informe.Adscripcion;
                        infoDetencion.Cargo = Informe.Cargo;
                        infoDetencion.Fechahora = DateTime.Now;
                        infoDetencion.Activo = 1;
                        infoDetencion.DetalledetencionId = Convert.ToInt32( detalleDetencion.Id);
                        infoDetencion.Habilitado = 1;
                        infoDetencion.Creadopor = usId;
                        infoDetencion.Autoridadeslesionadas = Convert.ToInt32(Informe.Autoridadeslesionadas);
                        infoDetencion.Autoridadesfallecidas = Convert.ToInt32(Informe.Autoridadesfallecidas);
                        infoDetencion.Personaslesionadas = Convert.ToInt32(Informe.Personaslesionadas);
                        infoDetencion.Personasfallecidas = Convert.ToInt32(Informe.Personasfallecidas);
                        infoDetencion.Reduccionfisicademovimientos = Convert.ToBoolean(Informe.Reduccionfisicademovimientos.ToLower());
                        infoDetencion.Utilizaciondearmasincapacitantemenosletales = Convert.ToBoolean(Informe.Utilizaciondearmasincapacitantemenosletales.ToLower());
                        infoDetencion.Utilizaciondearmasdefuego = Convert.ToBoolean(Informe.Utilizaciondearmasdefuego.ToLower());
                        infoDetencion.DescripcionConducta = Informe.DescripcionConducta;
                        infoDetencion.Brindosolicitoasistenciamedica = Convert.ToBoolean(Informe.Brindosolicitoasistenciamedica.ToLower());
                        infoDetencion.DescripcionConducta = Informe.DescripcionConducta;
                        infoDetencion.Firma = Informe.Firma;
                        infoDetencion.Firma2 = Informe.Firma2;
                        infoDetencion.Especifique = Informe.Especifique;
                        ControlInformeUsoFuerza.Actualizar(infoDetencion);
                    }

                    //ControlPDFInformeUsodelaFuerza.GeneraInformeusodelafuerza();
                    if (mode== "registro")
                    {
                        return new { exitoso = true, mensaje = "La información se registró correctamente", Id = Informe.Id };
                    }
                    else
                    {
                        return new { exitoso = true, mensaje = "La información se actualizó correctamente", Id = Informe.Id };

                    }
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }
    }


    public class informedetencionAux
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string Primerapellido { get; set; }
        public string Segundoapellido { get; set; }
        public string Nombre { get; set; }
        public string Adscripcion { get; set; }
        public string Cargo { get; set; }
        public string DetalledetencionId { get; set; }
        public string Fechahora { get; set; }
        public string Activo { get; set; }
        public string Habilitado { get; set; }
        public string Creadopor { get; set; }
        public string Autoridadeslesionadas { get; set; }
        public string Autoridadesfallecidas { get; set; }
        public string Personaslesionadas { get; set; }
        public string Personasfallecidas { get; set; }
        public string Reduccionfisicademovimientos { get; set; }
        public string Utilizaciondearmasincapacitantemenosletales { get; set; }
        public string Utilizaciondearmasdefuego { get; set; }
        public string DescripcionConducta { get; set; }
        public string Brindosolicitoasistenciamedica { get; set; }
        public string Especifique { get; set; }
        public string Firma { get; set; }
        public string Firma2 { get; set; }
        public string TrackingIdDetenido { get; set; }
    }
}