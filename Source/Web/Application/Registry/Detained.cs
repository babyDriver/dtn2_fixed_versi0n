﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Application.Registry
{
    public class Detained
    {
        public string name { get; set; }
        public string first { get; set; }
        public string last { get; set; }
        public string nick { get; set; }
        public int age { get; set; }
        public int sex { get; set; }
        public DateTime date { get; set; }
    }
}