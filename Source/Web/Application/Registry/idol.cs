﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Web.Application.Registry
{
    public static class Defeat
    {
        public interface IRange<T>
        {
            T Start { get; }
            T End { get; }
            bool Includes(T value);
            bool Includes(IRange<T> range);
        }

        public class DateRange : IRange<DateTime>
        {
            public DateRange(DateTime now, int hour)
            {
                Start = now.AddHours(-hour); //init
                End = now;
            }

            public DateTime Start { get; private set; }
            public DateTime End { get; private set; }

            public bool Includes(string value)
            {
                string[] part = value.Split('/');
                DateTime dt = new DateTime(Convert.ToInt16(part[2].Substring(0, 4)), Convert.ToInt16(part[0]), Convert.ToInt16(part[1]));
                return (Start <= dt) && (dt <= End);
            }

            public bool Includes(IRange<DateTime> range)
            {
                return (Start <= range.Start) && (range.End <= End);
            }

            bool IRange<DateTime>.Includes(DateTime value)
            {
                throw new NotImplementedException();
            }
        }
    }
}