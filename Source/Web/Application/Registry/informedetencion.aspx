﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="informedetencion.aspx.cs" Inherits="Web.Application.Registry.informedetencion" %>



<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Informe de uso de la fuerza</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>     
        td.strikeout {
            text-decoration: line-through;
        }

        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }       
        .scroll2 {
            height: 25vw;
            overflow: auto;
        }
       
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
        .checkboxtext{
            font-size: 100%;
            display: inline;       
            color: #004987;
        }
        .checkboxtext:hover{
            cursor:pointer;
        }

    </style>
    <!--
    <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-list-alt"></i>
                    Información personal
                </h1>
            </div>
        </div>-->
        <asp:Label ID="lblMessage" runat="server"></asp:Label>

        <section id="widget-grid" class="">
                       <div class="row ">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <a style="float: right; margin-left: 3px; display: block; font-size: smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa  fa-paw"></i> Biométricos</a>    <a style="float: right; margin-left: 3px; display: none; font-size: smaller" class="btn bg-color-red txt-color-white  btn-lg " id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
                    <a style="float: right; margin-left: 3px; display: block; font-size: smaller" class="btn bg-color-yellow txt-color-white  btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
                    <a style="float: right; margin-left: 3px; display: block; font-size: smaller" class="btn bg-color-purple txt-color-white  btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
                    <a style="float: right; margin-left: 3px; display: block; font-size: smaller" class="btn btn-warning  btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
                    <a style="float: right; margin-left:3px;font-size:smaller" class="btn btn-primary txt-color-white  btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
                    <a style="float: right; margin-left: 3px; display: block; font-size: smaller" class="btn btn-info  btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
                    <a style="float: right; margin-left: 3px; display: block; font-size: smaller" class="btn btn-success txt-color-white  btn-lg " id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a>
                    <a style="float: right; margin-left: 3px; display: block; font-size: smaller" class="btn btn-default  btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
                </article>
            </div>
        <br />
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
 
            <div class="scroll2">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-personal-2"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                    <h2>Informe uso de la fuerza </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                        <div class="widget-body">  
                            <div class="row">         
                                <div class="row smart-form">
                                    <section class="col col-3">
										<label class="label" >Autoridades lesionadas: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-sort-numeric-asc"></i>
											<input name="autoridadeslesionada"  type="text"  id="autoridadeslesionada" title="" pattern="^[0-9]*$" maxlength="9"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese las autoridades lesionadas.</b>
										</label>
									</section>
                                    <section class="col col-3">
										<label class="label" >Autoridades fallecidas: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-sort-numeric-asc"></i>
											<input name="autoridadesfallecidas"  type="text"  id="autoridadesfallecidas" title="" pattern="^[0-9]*$" maxlength="9"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese las autoridades fallecidas.</b>
										</label>
									</section>
                                    <section class="col col-3">
										<label class="label" >Personas lesionadas: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-sort-numeric-asc"></i>
											<input name="personaslesionadas"  type="text"  id="personaslesionadas" title="" pattern="^[0-9]*$" maxlength="9"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese las personas lesionadas.</b>
										</label>
									</section>
                                    <section class="col col-3">
										<label class="label" >Personas fallecidas: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-sort-numeric-asc"></i>
											<input name="personasfallecidas"  type="text"  id="personasfallecidas" title="" pattern="^[0-9]*$" maxlength="9"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese las personas fallecidas.</b>
										</label>
									</section>
                                    <section class="col col-3">
									    <%--<label for="reduccionusofuerza">Reducción fisica de movimientos </label> <input type="checkbox" name="reduccionusofuerza" value="reduccionusofuerza" id="reduccionusofuerza"/>--%> 
                                        <span class="checkboxtext" id="spanDos">                                              
                                            <b>Reducción fisica de movimientos &nbsp;&nbsp;</b>
                                        </span>
                                        <input type="checkbox" id="reduccionusofuerza" name="reduccionusofuerza" />    
                                    </section>
                                    <section class="col col-3">
										<%--<label for="usofuerza">Utilización de armas incapacitantes menos letales </label> <input type="checkbox" name="usofuerza" value="usofuerza" id="usofuerza"/>--%> 
                                        <span class="checkboxtext" id="spanTres">                                              
                                            <b>Utilización de armas incapacitantes menos letales&nbsp;&nbsp;</b>
                                        </span>
                                        <input type="checkbox" id="usofuerza" name="usofuerza" /> 
                                    </section>
                                    <section class="col col-3">
									    <%--<label for="usofuerzaletal">Utilización de armas de fuego o fuerza letal  </label> <input type="checkbox" name="usofuerzaletal" value="usofuerzaletal" id="usofuerzaletal"/>--%> 
                                        <span class="checkboxtext" >                                              
                                            <b>Utilización de armas de fuego o fuerza letal&nbsp;&nbsp;</b>
                                        </span>
                                        <input type="checkbox" id="usofuerzaletal" name="usofuerzaletal" /> 
                                    </section>
                                    <section class="col col-3">
                                        <span class="checkboxtext">                                              
                                            <b>¿Brindó o solicitó asistencia médica?  &nbsp;&nbsp;</b>
                                        </span>
                                        <input type="checkbox" id="asistenciamedica" name="asistenciamedica" /> 
                                    </section>
                                                            <b class="tooltip tooltip-bottom-right" >Ingrese el especifique.</b>
                                    <section class="col col-6">
                                        <label class="input">Descripción conducta<a style="color:red">*</a></label>
                                        <label class="textarea">
                                            <textarea id="descripcionconducta" name="descripcionconducta" class="form-control alptext" rows="5"  placeholder="Descripción conducta" maxlength="450"></textarea> 
                                            <b class="tooltip tooltip-bottom-right" >Ingrese la descripción de conducta.</b>
                                        </label>                                                        
                                    </section>
                                    <section class="col col-6">
                                        <label class="input">Especifique</label>
                                        <label class="textarea">
                                            <textarea id="especifique" name="especifique" class="form-control alptext" rows="5" maxlength="450" placeholder="Especifique"></textarea>  
                                            <b class="tooltip tooltip-bottom-right" >Ingrese la expecificación.</b>
                                        </label>
                                    </section>
                                    <section class="col col-3">
										<label class="label" >Primer apellido: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="primerapellido"  type="text"  id="primerapellido" class="alptext" maxlength="145"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese el primer apellido.</b>
										</label>
									</section>
									<section class="col col-3">
										<label class="label" >Segundo apellido: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="segundoapellido" type="text" id="segundoapellido" class="alptext" maxlength="145"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese el segundo apellido.</b>
										</label>
									</section>
                                    <section class="col col-3">
										<label class="label" >Nombre: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="nombre" type="text" id="nombre" maxlength="145" class="alptext"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese el nombre.</b>
										</label>
									</section>
									<section class="col col-3">
										<label class="label" >Adscripción: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="adscripcion" type="text" id="adscripcion" maxlength="145" class="alptext"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese la adscripción.</b>
										</label>
									</section>
                                    <section class="col col-3">
										<label class="label" >Cargo/grado: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="cargogrado" type="text" id="cargogrado" maxlength="145" class="alptext"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese el cargo/grado.</b>
										</label>
									</section>
                                    <section class="col col-3">
										<label class="label" >Firma: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="firma" type="text" id="firma" maxlength="145" class="alptext"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese la firma.</b>
										</label>
									</section>
                                    <section class="col col-3">
										<label class="label" >Firma: </label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="firma2" type="text" id="firma2" maxlength="145" class="alptext"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese la firma.</b>
										</label>
									</section>
                                </div>
                                <div class="row smart-form">
                                    <footer>
                                        <div class="row" style="display: inline-block; float: right; margin-right: 15px;">
                                            <!--<a class="btn btn-sm btn-default clear4"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                            <%--<a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default" id="ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>--%>
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-default saveInfoDetencion" id="savebtn" title="Guardar"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                            <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                        </div>
                                    </footer>
                                    <input type="hidden" id="trackingIdDetencion" runat="server" />
                                    <input type="hidden" id="idDetencion" runat="server" />      
                                    <input type="hidden" id="tracking" runat="server" />
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>                
                                  
        </article>
        </div>
        </section>
    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" width="240" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" /> <br /><br />
                    </div><%--src="~/Content/img/avatars/male.png"--%>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="Ingreso" runat="server" value="" />
    <input type="hidden" id="Antropometria" runat="server" value="" />
    <input type="hidden" id="Señas_Particulares" runat="server" value="" />
    <input type="hidden" id="Adicciones" runat="server" value="" />
    <input type="hidden" id="Estudios_Criminologicos" runat="server" value="" />
    <input type="hidden" id="Expediente_Medico" runat="server" value="" />
    <input type="hidden" id="Familiares" runat="server" value="" />
    <input type="hidden" id="Id" value="" />
    <input type="hidden" id="DetenidoTrackingId" value="" />
    <input type="hidden" id="TrackingId" value="" />
    <input type="hidden" id="editable" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>    
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        //window.addEventListener("keydown", function (e) {
        //    var sectionNumber = $("#lstTabs .active").children().attr("id").substring(7);
        //    if (e.ctrlKey && e.keyCode === 39) {
        //        e.preventDefault();
        //        document.getElementById("linkantropometria").click();
        //    }
        //    else if (e.ctrlKey && e.keyCode === 37) {
        //        e.preventDefault();
        //        document.getElementById("linkinformacion").click();
        //        switch (sectionNumber) {
        //            case "linkTab1": document.getElementById("linkTab1").click(); break;
        //            case "linkTab2": document.getElementById("linkTab2").click(); break;
        //        }
        //    }
        //    else if (e.ctrlKey && e.keyCode === 71) {
        //        e.preventDefault();
        //        document.getElementById("savebtn").click();
        //        switch (sectionNumber) {
        //            case "linkTab1": document.getElementById("savebtn").click(); break;
        //            case "linkTab2": document.getElementById("linkinformacion").click(); break;
        //        }
        //    }
        //});

        $(document).ready(function () {
            pageSetUp();
            var urlIzquierda = '';
            var urlDerecha = '';
            init();

            $('body').on("keydown", function (e) {

                if (e.ctrlKey && e.which === 37) {
                    window.location.href = urlIzquierda;
                }

                if (e.ctrlKey && e.which === 39) {
                    window.location.href = urlDerecha;
                }

                if (e.ctrlKey && e.which === 71) {
                    if (validardatos()) {
                        Guardar();
                    }
                }
            });

            $("#ligarNuevoEvento").hide();
            if ($("#<%= editable.ClientID%>").val() == "0") {
                $('#autoridadeslesionada').attr("disabled", "disabled");
                $('#autoridadesfallecidas').attr("disabled", "disabled");
                $('#personaslesionadas').attr("disabled", "disabled");
                $('#personasfallecidas').attr("disabled", "disabled");
                $('#reduccionusofuerza').attr("disabled", "disabled");
                $('#usofuerza').attr("disabled", "disabled");
                $('#usofuerzaletal').attr("disabled", "disabled");
                $('#descripcionconducta').attr("disabled", "disabled");
                $('#asistenciamedica').attr("disabled", "disabled");
                $('#especifique').attr("disabled", "disabled");
                $('#primerapellido').attr("disabled", "disabled");
                $('#segundoapellido').attr("disabled", "disabled");
                $('#nombre').attr("disabled", "disabled");
                $('#adscripcion').attr("disabled", "disabled");
                $('#cargogrado').attr("disabled", "disabled");
                $("#firma").attr("disabled", "disabled");
                $("#firma2").attr("disabled", "disabled");

                $("#ligarNuevoEvento").show();
            }
            

             $(document).on('keydown', 'input[pattern]', function(e){
              var input = $(this);
              var oldVal = input.val();
              var regex = new RegExp(input.attr('pattern'), 'g');

              setTimeout(function(){
                var newVal = input.val();
                if(!regex.test(newVal)){
                  input.val(oldVal); 
                }
              }, 0);
            });


            $("body").on("click", "#savebtn", function () {
                if (validardatos()) {

                    Guardar();
                }
            });
            

            $("#primerapellido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#segundoapellido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            $("#nombre").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("body").on("click", "#showInfoDetenido", function () {
                $("#showInfoDetenido").hide();
                $("#hideInfoDetenido").show();
                $("#SectionInfoDetenido").show();
            });
            $("body").on("click", "#hideInfoDetenido", function () {
                $("#showInfoDetenido").show();
                $("#hideInfoDetenido").hide();
                $("#SectionInfoDetenido").hide();
            });
           
            function validardatos() {
                var esvalido = true;

                  if ($("#autoridadeslesionada").val() == null || $("#autoridadeslesionada").val().split(" ").join("") == "") {
                    ShowError("Autoridades lesionadas", "El campo autoridades lesionadas es obligatorio.");
                    $('#autoridadeslesionada').parent().removeClass('state-success').addClass("state-error");
                    $('#autoridadeslesionada').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#autoridadeslesionada').parent().removeClass("state-error").addClass('state-success');
                    $('#autoridadeslesionada').addClass('valid');
                }
                if ($("#autoridadesfallecidas").val() == null || $("#autoridadesfallecidas").val().split(" ").join("") == "") {
                    ShowError("Autoridades fallecidas", "El campo autoridades fallecidas es obligatorio.");
                    $('#autoridadesfallecidas').parent().removeClass('state-success').addClass("state-error");
                    $('#autoridadesfallecidas').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#autoridadesfallecidas').parent().removeClass("state-error").addClass('state-success');
                    $('#autoridadesfallecidas').addClass('valid');
                }

                    if ($("#personaslesionadas").val() == null || $("#personaslesionadas").val().split(" ").join("") == "") {
                    ShowError("Personas lesionadas", "El campo pesonas lesionadas es obligatorio.");
                    $('#personaslesionadas').parent().removeClass('state-success').addClass("state-error");
                    $('#personaslesionadas').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#personaslesionadas').parent().removeClass("state-error").addClass('state-success');
                    $('#personaslesionadas').addClass('valid');
                }

                     if ($("#personasfallecidas").val() == null || $("#personasfallecidas").val().split(" ").join("") == "") {
                    ShowError("Personas fallecidas", "El campo pesonas fallecidas es obligatorio.");
                    $('#personasfallecidas').parent().removeClass('state-success').addClass("state-error");
                    $('#personasfallecidas').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#personasfallecidas').parent().removeClass("state-error").addClass('state-success');
                    $('#personasfallecidas').addClass('valid');
                }

                  if ($("#descripcionconducta").val() == null || $("#descripcionconducta").val().split(" ").join("") == "") {
                            ShowError("Descripción de conducta", "La descripción de conducta es obligatoria.");
                            $('#descripcionconducta').parent().removeClass('state-success').addClass("state-error");
                            $('#descripcionconducta').removeClass('valid');
                            esvalido = false;
                        }
                        else {
                            $('#descripcionconducta').parent().removeClass("state-error").addClass('state-success');
                            $('#descripcionconducta').addClass('valid');
                }

                if ($("#primerapellido").val() == null || $("#primerapellido").val().split(" ").join("") == "") {
                    ShowError("Primer apellido", "El primer apellido es obligatorio.");
                    $('#primerapellido').parent().removeClass('state-success').addClass("state-error");
                    $('#primerapellido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#primerapellido').parent().removeClass("state-error").addClass('state-success');
                    $('#primerapellido').addClass('valid');
                }
                if ($("#segundoapellido").val() == null || $("#segundoapellido").val().split(" ").join("") == "") {
                    ShowError("Segundo apellido", "El segundo apellido es obligatorio.");
                    $('#segundoapellido').parent().removeClass('state-success').addClass("state-error");
                    $('#segundoapellido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#segundoapellido').parent().removeClass("state-error").addClass('state-success');
                    $('#segundoapellido').addClass('valid');
                }
                if ($("#nombre").val() == null || $("#nombre").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El  nombre es obligatorio.");
                    $('#nombre').parent().removeClass('state-success').addClass("state-error");
                    $('#nombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#nombre').parent().removeClass("state-error").addClass('state-success');
                    $('#nombre').addClass('valid');
                }

                if ($("#adscripcion").val() == null || $("#adscripcion").val().split(" ").join("") == "") {
                    ShowError("Adscripcion", "La adscripcion es obligatoria.");
                    $('#adscripcion').parent().removeClass('state-success').addClass("state-error");
                    $('#adscripcion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#adscripcion').parent().removeClass("state-error").addClass('state-success');
                    $('#adscripcion').addClass('valid');
                }

                if ($("#cargogrado").val() == null || $("#cargogrado").val().split(" ").join("") == "") {
                    ShowError("Cargo/grado", "El cargo/grado es obligatorio.");
                    $('#cargogrado').parent().removeClass('state-success').addClass("state-error");
                    $('#cargogrado').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#cargogrado').parent().removeClass("state-error").addClass('state-success');
                    $('#cargogrado').addClass('valid');
                }

                if ($("#firma").val() == null || $("#firma").val().split(" ").join("") == "") {
                    ShowError("Firma", "La firma es obligatoria.");
                    $('#firma').parent().removeClass('state-success').addClass("state-error");
                    $('#firma').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#firma').parent().removeClass("state-error").addClass('state-success');
                    $('#firma').addClass('valid');
                }
           


                return esvalido;
            }

             function Guardar() {
                 startLoading();
                  var param = RequestQueryString("tracking");
                 var informe = {
                     Id: $('#Id').val(),
                     TrackingId: $('#TrackingId').val(),
                     Primerapellido: $("#primerapellido").val(),
                     Segundoapellido: $("#segundoapellido").val(),
                     Nombre : $("#nombre").val(),
                     Adscripcion: $("#adscripcion").val(),
                     Cargo: $("#cargogrado").val(),
                     Autoridadeslesionadas :$("#autoridadeslesionada").val(),
                     Autoridadesfallecidas :$("#autoridadesfallecidas").val(),
                     Personaslesionadas :$("#personaslesionadas").val(),
                     Personasfallecidas :$("#personasfallecidas").val(),
                     Reduccionfisicademovimientos :$("#reduccionusofuerza").is(":checked"),
                     Utilizaciondearmasincapacitantemenosletales :$("#usofuerza").is(":checked"),
                     Utilizaciondearmasdefuego :$("#usofuerzaletal").is(":checked"),
                     DescripcionConducta :$("#descripcionconducta").val(),
                     Brindosolicitoasistenciamedica :$("#asistenciamedica").is(":checked"),
                     Especifique: $("#especifique").val(),
                     Firma: $("#firma").val(),
                     Firma2:$("#firma2").val(),
                     TrackingIdDetenido:param
                   
                };
                 
                $.ajax({
                    type: "POST",
                    url: "informedetencion.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'Informe': informe }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {                            
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "  " + resultado.mensaje + ".", "<br /></div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("Bien hecho!", "  " + resultado.mensaje+"." );                                                  
                            $('#main').waitMe('hide');
                    
                            removeclassInputs();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);

                            if (resultado.mensaje == "El traslado ya esta registrado") {
                                ShowError("Traslado.", resultado.mensaje);
                            }
                            else
                            {
                                ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                            }
                        }
                    }
                });
            }

            
            
            ///


            function removeclassInputs()
            {
                $('#primerapellido').parent().removeClass('state-success');
                $('#segundoapellido').parent().removeClass('state-success');
                $('#nombre').parent().removeClass('state-success');
                $('#adscripcion').parent().removeClass('state-success');
                $('#cargogrado').parent().removeClass('state-success');
                $("#autoridadeslesionada").parent().removeClass('state-success');
                $("#autoridadesfallecidas").parent().removeClass('state-success');
                $("#personaslesionadas").parent().removeClass('state-success');
                $("#autoridadesfallecidas").parent().removeClass('state-success');
                $("#descripcionconducta").parent().removeClass('state-success');
                $("#firma").parent().removeClass('state-success');
            }

            function CargaInfoInicio(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "informedetencion.aspx/getinfo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        TrackingId: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            
                            $('#Id').val(resultado.Obj.Id);
                            $('#TrackingId').val(resultado.Obj.TrackingId);
                            //alert(resultado.Obj.Primerapellido);
                            
                            $("#primerapellido").val(resultado.Obj.Primerapellido);
                            $("#segundoapellido").val(resultado.Obj.Segundoapellido);
                            $("#nombre").val(resultado.Obj.Nombre);
                            $("#adscripcion").val(resultado.Obj.Adscripcion);
                            $("#cargogrado").val(resultado.Obj.Cargo);
                            $("#autoridadeslesionada").val(resultado.Obj.Autoridadeslesionadas);
                            $("#autoridadesfallecidas").val(resultado.Obj.Autoridadesfallecidas);
                            $("#personaslesionadas").val(resultado.Obj.Personaslesionadas);
                            $("#personasfallecidas").val(resultado.Obj.Personasfallecidas);
                            if (resultado.Obj.Reduccionfisicademovimientos == "true") {
                                $('#reduccionusofuerza').prop('checked', true);
                            }
                            else
                            {
                                 $('#reduccionusofuerza').prop('checked', false);
                            }

                            if (resultado.Obj.Utilizaciondearmasincapacitantemenosletales == "true") {
                                $('#usofuerza').prop('checked',true);
                            }
                            else
                            {
                                $('#usofuerza').prop('checked',false);
                            }
                            if (resultado.Obj.Utilizaciondearmasdefuego == "true")
                            {
                                $('#usofuerzaletal').prop('checked', true);
                            }
                            else
                            {
                                $('#usofuerzaletal').prop('checked', false);
                            }
                            $("#descripcionconducta").val(resultado.Obj.DescripcionConducta);

                            if (resultado.Obj.Brindosolicitoasistenciamedica == "true")
                            {
                                $('#asistenciamedica').prop('checked', true);
                            }
                            else
                            {
                                $('#asistenciamedica').prop('checked', false);
                            }
                            $("#especifique").val(resultado.Obj.Especifique);
                            $("#firma").val( resultado.Obj.Firma);
                            $("#firma2").val( resultado.Obj.Firma2);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

                

            $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
                var param = RequestQueryString("tracking");
                CargarDatosModal(param);
            });

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);
                            }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }


         


            
            

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
              if (url.indexOf("~/") == 0) {
                  url = baseUrl + url.substring(2);
              }
              return url;
            }

        function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);

                            $("#ctl00_contenido_idDetencion").val(resultado.obj.IdInfo);
                            $("#ctl00_contenido_trackingIdDetencion").val(resultado.obj.TrackingInfo);

                          
                                $('#edadInterno').text(resultado.obj.Edad);
                           

                           
                                $('#sexoInterno').text(resultado.obj.Sexo);

                            if(resultado.obj.municipioNombre != "")
                                $('#municipioInterno').text(resultado.obj.municipioNombre);
                            else
                                $('#municipioInterno').text("Municipio no registrada");

                            if (resultado.obj.domiclio != "")
                                $('#domicilioInterno').text(resultado.obj.domiclio);
                            else
                                $('#domicilioInterno').text("Domicilio no registrado");

                            if (resultado.obj.coloniaNombre != "")
                                $('#coloniaInterno').text(resultado.obj.coloniaNombre);
                            else
                                $('#coloniaInterno').text("Colonia no registrada");

                            $("#motivo").val(resultado.obj.Motivo);
                            $("#descripcion").val(resultado.obj.Descripcion);
                            $("#folio").val(resultado.obj.Folio);
                            $("#fecha").val(resultado.obj.FechaDetencion);
                            $("#codigoPostal").val(resultado.obj.CodigoPostal);
                            $("#lugar").val(resultado.obj.Lugar);
                            
                            $("#llamada").val(resultado.obj.LlamadaId);


                            $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkvistas").attr("href", "huella.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkestudios").attr("href", "record.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkseñas").attr("href", "signal.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            urlDerecha = "anthropometry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val();
                            $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            urlIzquierda = "personalinformation.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val();
                            $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkingreso").attr("href", "entry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {                    
                                                           
                    CargarDatos(param);
                    CargaInfoInicio(param);
                    $("#ctl00_contenido_tracking").val(param);
                }

                if ($("#ctl00_contenido_KAQWPK").val() == "false")
                    $("#savebtn").hide();
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#add_").show();
                    if ($("#<%= editable.ClientID %>").val() == "0") {
                        $("#savebtn").hide();
                    }
                    else {
                        $("#savebtn").show();
                    }
                }
                else {
                    $("textarea,input, select").each(function(index, element){ $(this).attr('disabled', true); });
                }


                  if ($("#ctl00_contenido_Ingreso").val() == "true") {
                      $("#linkingreso").show();
                  }
                  if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                      $("#linkinformacion").show();
                  }
                  if ($("#ctl00_contenido_Antropometria").val() == "true") {
                      $("#linkantropometria").show();
                  }
                  if ($("#ctl00_contenido_Señas_Particulares").val() == "true") {
                      $("#linkseñas").show();
                  }
                  if ($("#ctl00_contenido_Familiares").val() == "true") {
                      $("#linkfamiliares").show();
                  }
                  if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                      $("#linkexpediente").show();
                  }
                  if ($("#ctl00_contenido_Adicciones").val() == "true") {
                      $("#linkadicciones").show();
                  }

                $("#linkinformacion").show();
                  $("#linkestudios").show();
                  $("#linkInfoDetencion").show();

            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
        });
    </script>
</asp:Content>
