﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Security;
using System.Web.Services;

using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Web;

namespace Web.Application.Admin
{
    public class MailAux
    {
        public string TrackingId { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
        public string CC { get; set; }
        public string CCO { get; set; }
        public bool SendMail { get; set; }
    }

    public partial class correo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Configuración y seguridad" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static object getmail()
        {
            try
            {
                var mail = ControlSmtp.ObtenerSmtp();
                object objmail = null;

                if (mail != null)
                {
                    objmail = new
                    {
                        Id = mail.Id.ToString(),
                        TrackingId = mail.TrackingId != null ? mail.TrackingId.ToString() : "",
                        Host = mail.Host != null ? mail.Host : "",
                        Port = mail.Port != null ? mail.Port.ToString() : "",
                        Username = mail.Username != null ? mail.Username : "",
                        Password = mail.Password != null ? mail.Password : "",
                        From = mail.From != null ? mail.From : "",
                        CC = mail.CC != null ? mail.CC : "",
                        CCO = mail.CCO != null ? mail.CCO : "",
                        SendMail = mail.SendMail,
                    };
                }
                return objmail;
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static object save(MailAux mail)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Modificar)
                {
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;
                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    var mode = string.Empty;


                    var smtp = new Entity.Smtp();
                    if (!string.IsNullOrEmpty(mail.TrackingId))
                    {
                        smtp = ControlSmtp.ObtenerByTrackingId(new Guid(mail.TrackingId));
                    }

                    if (string.IsNullOrEmpty((mail.Host.Trim()))) smtp.Host = null;
                    else smtp.Host = mail.Host.Trim();

                    if (string.IsNullOrEmpty((mail.Port.Trim()))) smtp.Port = 0;
                    else smtp.Port = Convert.ToInt32(mail.Port.Trim());

                    if (string.IsNullOrEmpty((mail.Username.Trim()))) smtp.Username = null;
                    else smtp.Username = mail.Username.Trim();

                    if (string.IsNullOrEmpty((mail.Password.Trim()))) smtp.Password = null;
                    else smtp.Password = mail.Password.Trim();

                    if (string.IsNullOrEmpty((mail.From.Trim()))) smtp.From = null;
                    else smtp.From = mail.From.Trim();

                    if (string.IsNullOrEmpty((mail.CC.Trim()))) smtp.CC = null;
                    else smtp.CC = mail.CC.Trim();

                    if (string.IsNullOrEmpty((mail.CCO.Trim()))) smtp.CCO = null;
                    else smtp.CCO = mail.CCO.Trim();

                    smtp.SendMail = mail.SendMail;
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    if (string.IsNullOrEmpty(mail.TrackingId))
                    {

                        mode = @"registrada";
                        smtp.TrackingId = Guid.NewGuid();
                        smtp.CreadoPor = usuario;
                        int id = ControlSmtp.Guardar(smtp);
                        if (id < 0) throw new Exception(@"No fue posible guardar la información, intentelo nuevamente.");
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de datos de correo";
                        historial.CreadoPor = usuario;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {

                        mode = @"actualizada";
                        smtp.ModificadoPor = usuario;
                        ControlSmtp.Actualizar(smtp);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de datos de correo";
                        historial.CreadoPor = usuario;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    return JsonConvert.SerializeObject(new { success = true, message = mode, trackingId = mail.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message, trackingId = "" });
            }
        }
    }
}