﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace Web.Application.Admin
{
    public class UsuarioAux
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string Perfil { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string User { get; set; }
        public string Oficina { get; set; }
        public string Movil { get; set; }
        public string Password { get; set; }
        public string Rol { get; set; }
        public string Habilitado { get; set; }
        public string CUIP { get; set; }
        //public int CentroId { get; set; }
        //public int Contrato { get; set; }
    }

    public class UsuarioEditAux
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Perfil { get; set; }
        public string User { get; set; }
        public bool Habilitado { get; set; }
        public List<Entity.UsuarioPermiso> Permisos { get; set; }
        public List<Entity.UsuarioReportes> PermisosReportes { get; set; }
        //public string ContratoId { get; set; }
        //public string ClienteId { get; set; }
        public string Movil { get; set; }
        public string habilitarcomborol { get; set; }
        public string Password { get; set; }
        public string CUIP { get; set; }
    }

    public class ContratoAuxNuevo
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string VigenciaInicial { get; set; }
        public string VigenciaFinal { get; set; }
        public string Tipo { get; set; }
    }

    public partial class user_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //  validatelogin();
            getPermisos();
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Modificar && !permisos.Registrar)
                        //No cuenta con permisos de modificar/editar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                       
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static List<Combo> getroles(string setperfil)
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario = Convert.ToInt32(membershipUser.ProviderUserKey);

            var usuariodetalle = ControlUsuario.Obtener(usuario);

            var combo = new List<Combo>();
            var perfiles = Roles.GetAllRoles();

            if (perfiles.Length > 0)
            {
                if (usuariodetalle.RolId == 1 )
                {
                    foreach (var rol in perfiles.Where(x => x != "Supervisor" ))
                        combo.Add(new Combo { Desc = rol, Id = rol });
                }
                if (usuariodetalle.RolId == 2)
                {
                    foreach (var rol in perfiles.Where(x => x !=  "Administrador global" && x != "Supervisor"))
                        combo.Add(new Combo { Desc = rol, Id = rol });
                }
                else if (usuariodetalle.RolId == 13)
                {
                    foreach (var rol in perfiles)
                        combo.Add(new Combo { Desc = rol, Id = rol });
                }
                else
                {
                    foreach (var rol in perfiles)
                        if (rol != "Supervisor")
                        {
                            combo.Add(new Combo { Desc = rol, Id = rol });
                        }
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getInstituciones()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Institucion> instituciones = ControlInstitucion.ObteneTodos();


            if (instituciones.Count > 0)
            {
                foreach (var rol in instituciones)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static object getuser(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    var user = ControlUsuario.Obtener(new Guid(trackingid));
                    var usuarioms = Membership.GetUser(user.User);
                    var contratoCliente = ControlContratoCliente.ObtenerById(user.Id); 
                    
                    UsuarioEditAux obj = new UsuarioEditAux();
                    var habilitarcombo = false;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuariodetalle = ControlUsuario.Obtener(usId);
                    if (usuariodetalle.RolId == 1 || usuariodetalle.RolId == 13)
                    {
                        habilitarcombo = true;
                    }

                    if (user != null)
                    {
                        var permisos = ControlUsuarioPermiso.ObtenerPorUsuarioId(user.Id);
                        List<Entity.UsuarioReportes> permisosReportes = ControlUsuarioReportes.ObtenerPorUsuarioId(user.Id);
                        obj.Id = user.Id.ToString();
                        obj.TrackingId = user.UserId.ToString();
                        obj.Email =user.Email==""?null:user.Email ;
                        obj.Nombre = user.Nombre;
                        obj.ApellidoPaterno = user.ApellidoPaterno;
                        obj.ApellidoMaterno = user.ApellidoMaterno;
                        obj.User = user.User;
                        obj.Habilitado = user.Habilitado;
                        //obj.ContratoId = (contratoCliente != null) ? contratoCliente.ContratoId.ToString() : "0";
                        //obj.ClienteId = (contratoCliente != null) ? contratoCliente.ClienteId.ToString() : "0";
                        obj.Movil = user.Movil;           
                        obj.Perfil = Roles.GetRolesForUser(usuarioms.UserName)[0];
                        obj.Permisos = permisos;
                        obj.PermisosReportes = permisosReportes;
                        obj.habilitarcomborol = habilitarcombo.ToString();
                        obj.Password = usuarioms.GetPassword();
                        obj.CUIP = user.CUIP;
                    }
                    return obj;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static object save(UsuarioAux usuario, string[] registrar, string[] consultar, string[] modificar, string[] eliminar, string[] reportes, List<ContratoAuxNuevo> contratos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
                {
                    bool redireccionar = false;
                    var user = new Entity.Usuario();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuarioActual = ControlUsuario.Obtener(usId);

                    var tipoDeMensaje = Entity.TipoDeMensaje.NuevoUsuario;

                    if (string.IsNullOrWhiteSpace(usuario.User)) throw new Exception("El nombre de usuario se encuentra vacío.");
                    if (usuario.User.Length > 50) throw new Exception("El nombre de usuario es demasiado largo. No debe exceder de 50 caracteres");

                    if (string.IsNullOrWhiteSpace(usuario.Nombre)) throw new Exception("El nombre se encuentra vacío.");
                    if (usuario.Nombre.Length > 50) throw new Exception("El nombre es demasiado largo. No debe exceder de 50 caracteres");

                    if (string.IsNullOrWhiteSpace(usuario.ApellidoPaterno)) throw new Exception("El apellido paterno se encuentra vacío.");
                    if (usuario.ApellidoPaterno.Length > 50) throw new Exception("El apellido paterno es demasiado largo. No debe exceder de 50 caracteres");

                    if (!string.IsNullOrEmpty(usuario.Email))
                    {
                        if (string.IsNullOrWhiteSpace(usuario.Email)) throw new Exception("El correo electrónico se encuentra vacío.");
                        if (usuario.Email.Length > 50) throw new Exception("El correo electrónico es demasiado largo. No debe exceder de 50 caracteres");
                    }
                    if (!string.IsNullOrWhiteSpace(usuario.ApellidoMaterno))
                        if(usuario.ApellidoMaterno.Length > 50) throw new Exception("El apellido materno es demasiado largo. No debe exceder de 50 caracteres");

                    if (!string.IsNullOrWhiteSpace(usuario.Movil))
                        if (usuario.Movil.Length > 50) throw new Exception("El móvil es demasiado largo. No debe exceder de 50 caracteres");

                    Regex nombreUsuarioRegex = new Regex(@"^[a-zA-Z0-9@._\-]+$");
                    string nombresinespacios = usuario.User.Replace(" ","");
                    if (!nombreUsuarioRegex.IsMatch(nombresinespacios)) throw new Exception("El nombre de usuario contiene caracteres invalidos.");

                    var validarNombre = ValidarCaracteresEspeciales("nombre", usuario.Nombre);
                    if (validarNombre != "") throw new Exception(validarNombre);

                    var validarApPaterno = ValidarCaracteresEspeciales("apellido paterno", usuario.ApellidoPaterno);
                    if (validarApPaterno != "") throw new Exception(validarApPaterno);

                    var validarApMaterno = ValidarCaracteresEspeciales("apellido materno", usuario.ApellidoMaterno);
                    if (validarApMaterno != "") throw new Exception(validarApMaterno);

                    if (!string.IsNullOrWhiteSpace(usuario.Movil))
                    {
                        Regex celularRegex = new Regex(@"(\+?\d{2}\s?)?(\d{3}|\(\d{3}\))[-\s]?\d{3}[-\s]?\d{4}");
                        if (!celularRegex.IsMatch(usuario.Movil)) throw new Exception("El móvil no tiene un formato valido");
                    }
                    if (!string.IsNullOrEmpty(usuario.Email))
                    {
                        //Regex emailRegEx = new Regex(@"^[a-zA-Z]+([._a-zA-Z0-9]+)?@[a-zA-Z0-9]+.[a-zA-Z]{2,6}$");
                        Regex emailRegEx = new Regex(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$");
                        if (!emailRegEx.IsMatch(usuario.Email)) throw new Exception("El correo electrónico no tiene un formato valido.");
                    }
                    

                    user.Nombre = usuario.Nombre.ToUpper();
                    user.ApellidoPaterno = usuario.ApellidoPaterno.ToUpper();
                    user.ApellidoMaterno = usuario.ApellidoMaterno.ToUpper();
                    user.UltimaActualizacion = DateTime.Now;
                    user.Email = usuario.Email;
                    user.User = usuario.User;
                    user.Activo = true;
                    user.Movil = usuario.Movil;
                    user.Habilitado = Convert.ToBoolean(usuario.Habilitado);
                    usuario.Password = (!string.IsNullOrEmpty(usuario.Password)) ? usuario.Password : CrearPassword(4);
                    user.CreadoPor = usId;
                    user.CUIP = usuario.CUIP;
                    //user.CentroId = usuario.CentroId;
                    //Removerlo en el futuro
                    //user.CentroId = 0;
                    
                    string id = usuario.Id;
                    if (id == "") id = "0";

                    var user_duplicado = ControlUsuario.ObtenerPorUsuario(usuario.User);

                    if (user_duplicado != null && user_duplicado.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("El nombre de usuario: ", user_duplicado.User, " ya está registrado favor de cambiarlo por otro."));

                    if (!string.IsNullOrEmpty(usuario.Email))
                    {
                        var email_duplicado = ControlUsuario.ObtenerPorEmail(usuario.Email);

                        if (email_duplicado != null && email_duplicado.Id != Convert.ToInt32(id))
                            throw new Exception(string.Concat("El correo: ", email_duplicado.Email, " ya está registrado favor de cambiarlo por otro."));
                    }
                    if (!string.IsNullOrWhiteSpace(usuario.Movil))
                    {
                        var movil_duplicado = ControlUsuario.ObtenerPorMovil(usuario.Movil);

                        if (movil_duplicado != null && movil_duplicado.Id != Convert.ToInt32(id))
                            throw new Exception(string.Concat("El móvil: ", movil_duplicado.Movil, " ya está registrado favor de cambiarlo por otro."));
                    }

                    var contratoUsuario1 = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    if (string.IsNullOrEmpty(usuario.Id))
                    {
                        mode = @"registró";
                        var tmpmail = String.Concat(usuario.User, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());
                        if (!string.IsNullOrEmpty(usuario.Email))
                        {
                            var createdUser = Membership.CreateUser(usuario.User, usuario.Password, usuario.Email);
                            var userKey = createdUser.ProviderUserKey;
                            user.Id = Convert.ToInt32(userKey.ToString());
                            user.UserId = Guid.NewGuid();

                            var historial = new Entity.Historial();
                            historial.InternoId = 0;
                            historial.Habilitado = true;
                            historial.Fecha = DateTime.Now;
                            historial.Activo = true;
                            historial.TrackingId = Guid.NewGuid();
                            historial.Movimiento = "Registro de usuario";
                            historial.CreadoPor = usId;

                            historial.ContratoId = contratoUsuario1.IdContrato;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                        else
                        {
                            var createdUser = Membership.CreateUser(usuario.User, usuario.Password);
                            var userKey = createdUser.ProviderUserKey;
                            user.Id = Convert.ToInt32(userKey.ToString());
                            user.UserId = Guid.NewGuid();

                        }
                        AgregarModificarRol(usuario, false);
                        var rolid = ControlRol.ObtenerPorNombre(usuario.Rol);
                        user.RolId = rolid.id;

                        user.Id = ControlUsuario.Guardar(user);
                        if (user.Id <= 0)
                        {
                            Membership.DeleteUser(usuario.User);
                            throw new Exception("Hubo un problema al registrar al usuario.");
                        }

                        
                        foreach (var item in contratos)
                        {
                            //Registro de los contratos
                            Entity.ContratoUsuario contratoUsuario = new Entity.ContratoUsuario();
                            contratoUsuario.CreadoPor = usId;
                            contratoUsuario.IdContrato = Convert.ToInt32(item.Id);
                            contratoUsuario.IdUsuario = user.Id;
                            contratoUsuario.TrackingId = Guid.NewGuid();
                            contratoUsuario.Tipo = item.Tipo;
                            int idNuevo = ControlContratoUsuario.Guardar(contratoUsuario);
                        }

                        
                    }
                    else
                    {
                        mode = @"actualizó";
                        tipoDeMensaje = Entity.TipoDeMensaje.ModificacionDeUsuario;
                        user.Id = Convert.ToInt32(usuario.Id);
                        var us2 = ControlUsuario.Obtener(user.Id);
                        user.BanderaPassword = us2.BanderaPassword;
                        user.UserId = new Guid(usuario.TrackingId);
                        AgregarModificarRol(usuario, true);
                        if(usuarioActual.RolId == 1) //Solo si es admin global puede modificar el rol del usuario
                        {
                            var rolid = ControlRol.ObtenerPorNombre(usuario.Rol);
                            user.RolId = rolid.id;
                        }
                        else
                        {
                            var rolid = ControlRol.ObtenerPorNombre(usuario.Rol);
                            user.RolId = rolid.id;
                        }
                        var recuperardatos = ControlUsuario.Obtener(user.Id);
                        user.Oficina = recuperardatos.Oficina;
                        user.Avatar = recuperardatos.Avatar;
                        user.Email = usuario.Email;
                        if (string.IsNullOrEmpty(usuario.Email))
                        {
                           var uss= Membership.GetUser(usuario.User);
                            uss.Email = null;
                            Membership.UpdateUser(uss);
                        }
                        ControlUsuario.Actualizar(user);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de usuario";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario1.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);

                        var meberuser = Membership.GetUser(user.User);
                        meberuser.Email = usuario.Email;
                        meberuser.ChangePassword(meberuser.GetPassword(), usuario.Password);
                    
                        Membership.UpdateUser(meberuser);
                        ControlUsuarioPermiso.EliminarByUsuarioId(user.Id);

                        //Actualizar de los contratos

                        var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(user.Id);                        

                        foreach(var item in dataContratos)
                        {
                            bool isInList = false;
                            foreach(var item2 in contratos)
                            {
                                if(item.IdContrato.ToString() == item2.Id && item.Tipo == item2.Tipo)
                                {
                                    isInList = true;
                                    break;
                                }
                            }

                            if (!isInList)
                            {
                                item.Activo = false;
                                ControlContratoUsuario.Actualizar(item);
                            }
                        }
                        
                        Entity.ContratoUsuario contratoUsuario = new Entity.ContratoUsuario();
                        foreach (var item in contratos)
                        {
                            contratoUsuario.CreadoPor = usId;
                            contratoUsuario.IdContrato = Convert.ToInt32(item.Id);
                            contratoUsuario.IdUsuario = user.Id;
                            contratoUsuario.TrackingId = Guid.NewGuid();
                            contratoUsuario.Tipo = item.Tipo;

                            var contratoAux = ControlContratoUsuario.ObtenerPorContratoYUsuario(new object[] { user.Id, Convert.ToInt32(item.Id), item.Tipo });
                            
                            if(!(contratoAux != null))
                            {
                                int idNuevo = ControlContratoUsuario.Guardar(contratoUsuario);
                            }
                            else
                            {
                                if (!contratoAux.Activo)
                                {
                                    contratoAux.Activo = true;
                                    ControlContratoUsuario.Actualizar(contratoAux);
                                }
                            }
                        }

                        if(user.Id == usId)
                        {
                            var contratosData = ControlContratoUsuario.ObtenerPorUsuarioId(user.Id);

                            bool hasActives = true;

                            foreach(var item in contratosData)
                            {
                                if (item.Activo)
                                {
                                    hasActives = true;
                                    break;
                                }
                                else
                                {
                                    hasActives = false;
                                }
                            }

                            if (!hasActives)
                            {
                                HttpContext.Current.Session["numeroContrato"] = 0;
                            }

                            if (contratosData.Count > 1)
                            {
                                int idContratoUsuario = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                                var entity = ControlContratoUsuario.ObtenerPorId(idContratoUsuario);


                                if(entity != null)
                                {
                                    if (entity.Activo == false)
                                    {
                                        redireccionar = true;
                                        HttpContext.Current.Session["numeroContrato"] = 0;
                                    }                                    
                                }
                                else
                                {
                                    redireccionar = true;
                                    HttpContext.Current.Session["numeroContrato"] = 0;
                                }
                            }
                        }
                    }

                    ActualizarPermisos(consultar, 1, user.Id);
                    ActualizarPermisos(registrar, 2, user.Id);
                    ActualizarPermisos(modificar, 3, user.Id);
                    ActualizarPermisos(eliminar, 4, user.Id);
                    ActualizarPermisosReportes(reportes, user.Id);

                    //TODO: Notificar cambios de usuario por correo electrónico
                    if (!string.IsNullOrEmpty(user.Email))
                    {
                        ControlSmtp.NotificarPorCorreo(user.Nombre, user.User, usuario.Password, usuario.Rol, user.Email, string.Empty, string.Empty, tipoDeMensaje, string.Empty, string.Empty);
                    }
                    //Bloquear usuario
                    var usuarioNuevo = Membership.GetUser(user.User);

                    if (!user.Habilitado && mode == @"registró")
                    {
                        for (var i = 0; i < Membership.MaxInvalidPasswordAttempts; i++)
                        {
                            Membership.ValidateUser(usuarioNuevo.UserName, "thisisandummypasswordonlytolocktheuser");
                        }
                        Membership.UpdateUser(usuarioNuevo);
                    }
                    return new { exitoso = true, mensaje = mode, Id = user.Id.ToString(), TrackingId = user.UserId, Redireccionar = redireccionar };
                }
                else
                {                   
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(usuario.Id))
                {
                    
                    var user_duplicado = ControlUsuario.ObtenerPorUsuario(usuario.User);
                    if(user_duplicado ==null)
                    {
                        Membership.DeleteUser(usuario.User);
                    }
                    
                }
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        public static void AgregarModificarRol(UsuarioAux unserializedobj, bool modificar)
        {
            MembershipUser usuarioms;
            usuarioms = Membership.GetUser(unserializedobj.User);
            usuarioms.UnlockUser();
            usuarioms.Comment = unserializedobj.Nombre.Trim() + " " + unserializedobj.ApellidoPaterno.Trim() + " " + unserializedobj.ApellidoMaterno.Trim();

            if (!string.IsNullOrEmpty(unserializedobj.Email))
            {
                if (string.IsNullOrEmpty(usuarioms.Email))
                {
                    usuarioms.Email = unserializedobj.Email;
                    Membership.UpdateUser(usuarioms);
                }
                else {
                    if (!usuarioms.Email.Equals(unserializedobj.Email)) usuarioms.Email = unserializedobj.Email.Trim();
                    Membership.UpdateUser(usuarioms); }
            }
            if (modificar)
            {
                if (Roles.GetRolesForUser(unserializedobj.User).Length > 0)
                    Roles.RemoveUserFromRole(unserializedobj.User, Roles.GetRolesForUser(unserializedobj.User)[0]);
            }

            Roles.AddUserToRole(unserializedobj.User, unserializedobj.Rol);
        }


        public static void ActualizarPermisos(string[] pantalla, int idpermiso, int userid)
        {
            Entity.UsuarioPermiso usuariopermiso = new Entity.UsuarioPermiso();

            usuariopermiso.UsuarioId = userid;
            usuariopermiso.PermisoId = idpermiso;
            foreach (var item in pantalla)
            {

                usuariopermiso.PantallaId = Convert.ToInt32(item);
                var actualizarpermisos = ControlUsuarioPermiso.ObtenerPorParametros(usuariopermiso);
                if (actualizarpermisos is Entity.UsuarioPermiso)
                {
                    actualizarpermisos.Habilitado = true;
                    ControlUsuarioPermiso.Actualizar(actualizarpermisos);
                }
                else
                {
                    usuariopermiso.Activo = true;
                    usuariopermiso.Habilitado = true;
                    usuariopermiso.TrackingId = Guid.NewGuid();
                    ControlUsuarioPermiso.Guardar(usuariopermiso);
                }
            }
        }

        public static void ActualizarPermisosReportes(string[] reportes, int userid)
        {
            Entity.UsuarioReportes obj = new Entity.UsuarioReportes();
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

            obj.UsuarioId = userid;

            var usuarioReportes = ControlUsuarioReportes.ObtenerPorUsuarioId(userid);

            foreach (var item in reportes)
            {
                var split = item.Split('_');
                obj.ReporteId = Convert.ToInt32(split[0]);

                var actualizar = usuarioReportes.FirstOrDefault(x => x.ReporteId == obj.ReporteId);
                if (actualizar != null)
                {
                    actualizar.Habilitado = Convert.ToBoolean(split[1]);
                    actualizar.Activo= Convert.ToBoolean(split[1]);
                    ControlUsuarioReportes.Actualizar(actualizar);
                }
                else
                {
                    obj.CreadoPor = usId;
                    obj.Activo = true;
                    obj.Habilitado = Convert.ToBoolean(split[1]);
                    obj.TrackingId = Guid.NewGuid();
                    ControlUsuarioReportes.Guardar(obj);
                }
            }
        }

        public static string CrearPassword(int length)
        {
            //const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            const string valid = "1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        [WebMethod]
        public static Object restorepassword(string username)
        {

            try
            {

                var usuarioms = Membership.GetUser(username);
                var user = ControlUsuario.ObtenerPorUsuario(username);

                ///string pass = usuarioms.ResetPassword();

                var Perfil = Roles.GetRolesForUser(username)[0];

                var tipoDeMensaje = Entity.TipoDeMensaje.ReestablecerContrasena;
                ControlSmtp.NotificarPorCorreo(user.Nombre, usuarioms.UserName, usuarioms.GetPassword(), Perfil, usuarioms.Email, string.Empty, string.Empty, tipoDeMensaje, string.Empty, string.Empty);

                return new { exitoso = true };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static DataTable getpermisos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (!emptytable)
                {
                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                    List<Where> where = new List<Where>();
                    where.Add(new Where("P.Activo", "1"));

                    Query query = new Query
                    {
                        select = new List<string> {
                            "P.Nombre Nombre",
                            "P.Id Registrar",
                            "P.Id Consultar",
                            "P.Id Modificar",
                            "P.Id Eliminar",
                            "group_concat(R.Id,'-',R.Nombre) Reportes"
                        },
                        from = new Table("Pantalla", "P"),
                        wheres = where,
                        joins = new List<Join>()
                        {
                            new Join(new Table("reportes", "R"), "P.Id = R.PantallaId")
                        },
                        groupBy = "P.Id"
                    };
                    
                    DataTables dt = new DataTables(mysqlConnection);
                    return dt.Generar(query, draw, start, length, search, order, columns);
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static object getpermisionbydefault(string rol)
        {
            object obj = null;
            int rolId = ControlRol.ObtenerPorNombre(rol).id;
            var list = ControlPermiso.ObtenerPermisosPorDefault(rolId);
            if (list != null) obj = new { exitoso = true, Permisos = list };
            else obj = new { exitoso = false, mensaje = "No se pueden obtener los permisos por default para el rol seleccionado." };
            return obj;
        }

        [WebMethod]
        public static object getpermisionReportsbydefault(string rol)
        {
            try
            {
                object obj = null;
                int rolId = ControlRol.ObtenerPorNombre(rol).id;

                List<Entity.UsuarioReportes> permisosDefecto = new List<Entity.UsuarioReportes>();
                var reportesId = ControlReportes.ObtenerTodos().Select(x => x.Id);
                foreach (var id in reportesId)
                {
                    if (rolId == 1 || rolId == 2 || rolId == 13)
                    {
                        permisosDefecto.Add(new Entity.UsuarioReportes()
                        {
                            ReporteId = id,
                            Activo = true,
                            Habilitado = true
                        });
                    }
                    else
                    {
                        permisosDefecto.Add(new Entity.UsuarioReportes()
                        {
                            ReporteId = id,
                            Activo = false,
                            Habilitado = false
                        });
                    }
                }

                obj = new { exitoso = true, Permisos = permisosDefecto };
                return obj;
            }
            catch (Exception)
            {
                return new { exitoso = false, mensaje = "No se pueden obtener los permisos por default para el rol seleccionado." };
            }
        }

        [WebMethod]
        public static List<Combo> getCustomers()
        {
            var combo = new List<Combo>();
            var c = ControlCliente.ObtenerTodos();

            foreach (var item in c)
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getContracts(string[] data)
        {
            var combo = new List<Combo>();
            var c = ControlContrato.ObtenerPorCliente(Convert.ToInt32(data[0]));

            foreach (var item in c)
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getContratosOnTable(string tracking)
        {
            try
            {
                Entity.Usuario user = new Entity.Usuario();
                if(tracking != "")
                {
                    user = ControlUsuario.Obtener(new Guid(tracking));
                }
                else
                {
                    user.Id = 0;
                }                

                var contratos = ControlContratoAsignacion.ObtenerPorIdUsuario(user.Id);                
                var contratosFiltro = contratos.Where(x => x.Tipo == "subcontrato");
                List<object> list = new List<object>();

                foreach(var item in contratosFiltro)
                {
                 
                    object obj = new
                    {
                        Id = item.Id,
                        Contrato=ControlContrato.Obtenerporsubcontrato(item.Id).Nombre,
                        Nombre = item.Contrato,
                        VigenciaInicial = item.VigenciaInicial.ToShortDateString(),
                        VigenciaFinal = item.VigenciaFinal.ToShortDateString(),
                        IdUsuario = item.IdUsuario,
                        Activo = item.Activo,
                        Cliente = ControlCliente.ObtenerPorId(ControlContrato.ObtenerPorId(ControlSubcontrato.ObtenerPorId(item.Id).ContratoId).ClienteId).Nombre,
                        //Cliente = item.Tipo == "contrato" ? ControlCliente.ObtenerPorId(ControlContrato.ObtenerPorId(item.Id).ClienteId).Nombre : ControlCliente.ObtenerPorId(ControlContrato.ObtenerPorId(ControlSubcontrato.ObtenerPorId(item.Id).ContratoId).ClienteId).Nombre,
                        Tipo = item.Tipo
                    };

                    list.Add(obj);
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        public static string ValidarCaracteresEspeciales(string campo, string valor)
        {
            string msj = "";
            Regex caracteresEspeciales2 = new Regex(@"([^a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+)");
            var matchesCaracteresEspeciales = caracteresEspeciales2.Matches(valor);
            if (matchesCaracteresEspeciales.Count != 0)
            {
                msj = $"El {campo} contiene caracteres invalidos: ";
                List<char> charsEsp = new List<char>();
                foreach (var item in matchesCaracteresEspeciales)
                {
                    foreach (var character in item.ToString())
                    {
                        charsEsp.Add(character);
                    }
                }
                var charsEspDistinct = charsEsp.Distinct();
                if (charsEspDistinct.Count() > 1)
                {
                    for (int i = 0; i < charsEspDistinct.Count(); i++)
                    {
                        if (i == charsEspDistinct.Count() - 1) msj += charsEspDistinct.ToList()[i];
                        else msj += charsEspDistinct.ToList()[i] + ", ";
                    }
                }
                else msj += charsEsp.Distinct().First();

                msj += ".";
            }
            return msj;
        }

        [WebMethod]
        public static string GetReportesByPantallaId(string pantallaId)
        {
            try
            {
                if(!string.IsNullOrWhiteSpace(pantallaId) && Convert.ToInt32(pantallaId) != 0)
                {
                    var reportes = ControlReportes.ObtenerPorPantallaId(Convert.ToInt32(pantallaId)).Where(x => x.Activo);
                    var lista = reportes.Select(x => new { x.Id, x.Nombre }).ToList();

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", lista = lista });
                }
                else return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro la pantalla en el sistema." });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}