﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Admin
{
    public partial class contrato_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static string GetParametrosTamaño()
        {
            try
            {

                var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));

                var parametro = ControlParametroContrato.TraerTodos();

                decimal maximo = 0;
                decimal minimo = 0;

                foreach (var item in parametro)
                {

                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 13)
                    {
                        minimo = Convert.ToDecimal(item.Valor);
                    }

                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 14)
                    {
                        maximo = Convert.ToDecimal(item.Valor);
                    }

                }

                return JsonConvert.SerializeObject(new { exitoso = true, TMax = maximo, Tmin = minimo });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/logo.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        [WebMethod]
        public static List<Combo> getWSAInstitutions()
        {
            var combo = new List<Combo>();
            var c = ControlWSAInstitucion.ObtenerTodos();            

            foreach (var item in c)
            {                
                combo.Add(new Combo { Desc = item.NombreInstitucion, Id = item.IdInstitucion.ToString() });                
            }

            return combo;
        }

        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Configuración y seguridad" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static Object save(ContratoAux contrato)
        {
            try
            {
                if (Convert.ToDateTime(contrato.VigenciaInicial) > Convert.ToDateTime(contrato.VigenciaFinal))
                {
                    return  JsonConvert.SerializeObject(new  { exitoso = false, mensaje = "La vigencia inicial no puede ser mayor a la vigencia final.", Id = "", TrackingId = "" });

                }

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Modificar)
                {
                    var con = new Entity.Contrato();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = contrato.Id;
                    if (id == "") id = "0";

                    object[] data = new object[] { contrato.ClienteId, contrato.Nombre };
                    var contrato_duplicado = ControlContrato.ObtenerPorClienteYContrato(data);

                    if (contrato_duplicado != null && contrato_duplicado.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("El nombre del contrato: ", contrato_duplicado.Nombre, " ya está registrado favor de cambiarlo por otro."));
                    /*
                    var email_duplicado = ControlUsuario.ObtenerPorEmail(usuario.Email);

                    if (email_duplicado != null && email_duplicado.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("El correo: ", email_duplicado.Email, " ya está registrado favor de cambiarlo por otro."));
                        */
                    DateTime fechaInicial;
                    DateTime fechaFinal;

                    try
                    {
                        fechaInicial = Convert.ToDateTime(contrato.VigenciaInicial);                                                
                    }
                    catch (Exception)
                    {
                        throw new Exception(string.Concat("El valor de la vigencia inicial tiene un formato incorrecto."));
                    }

                    try
                    {
                        fechaFinal = Convert.ToDateTime(contrato.VigenciaFinal);
                    }
                    catch (Exception)
                    {
                        throw new Exception(string.Concat("El valor de las vigencia final tiene un formato incorrecto."));
                    }

                    if (fechaInicial > fechaFinal)
                    {
                        throw new Exception(string.Concat("La vigencia inicial no puede ser mayor a la vigencia final."));
                    }
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    if (string.IsNullOrEmpty(contrato.Id))
                    {
                        mode = @"registró";
                        var tmpmail = String.Concat(contrato.Nombre, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

                        con.ClienteId = Convert.ToInt32(contrato.ClienteId);
                        con.CreadoPor = usId;
                        con.Nombre = contrato.Nombre;
                        con.TrackingId = Guid.NewGuid();
                        con.VigenciaInicial = Convert.ToDateTime(contrato.VigenciaInicial);
                        con.InstitucionId = Convert.ToInt32(contrato.InstitucionId);
                        con.VigenciaFinal = Convert.ToDateTime(contrato.VigenciaFinal);
                        con.Id = ControlContrato.Guardar(con);
                        Entity.Accion accion = new Entity.Accion();
                        accion.CreadoPor = usId;
                        accion.Descripcion = "Nuevo contrato registrado";
                        accion.Fecha = DateTime.Now;
                        accion.Nombre = "Crear";
                        accion.TrackingId = Guid.NewGuid();
                        int idAccion = ControlAccion.Guardar(accion);

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de contrato";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {
                        mode = @"actualizó";

                        con.Activo = true;
                        con.Habilitado = true;
                        con.ClienteId = Convert.ToInt32(contrato.ClienteId);
                        con.Id = Convert.ToInt32(contrato.Id);
                        con.Nombre = contrato.Nombre;
                        con.InstitucionId = Convert.ToInt32(contrato.InstitucionId);
                        con.VigenciaInicial = Convert.ToDateTime(contrato.VigenciaInicial);
                        con.VigenciaFinal = Convert.ToDateTime(contrato.VigenciaFinal);
                        ControlContrato.Actualizar(con);
                        Entity.Accion accion = new Entity.Accion();
                        accion.CreadoPor = usId;
                        accion.Descripcion = "Contrato actualizado";
                        accion.Fecha = DateTime.Now;
                        accion.Nombre = "Actualizar";
                        accion.TrackingId = Guid.NewGuid();
                        int idAccion = ControlAccion.Guardar(accion);

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de contrato";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = con.Id.ToString(), TrackingId = con.TrackingId });
                    //return new { exitoso = true, mensaje = mode, Id = con.Id.ToString(), TrackingId = con.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static List<Combo> getCustomers()
        {
            var combo = new List<Combo>();
            var c = ControlCliente.ObtenerTodos();

            foreach (var item in c.Where(x=>x.Habilitado))
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> GetDivisas()
        {
            var combo = new List<Combo>();
            var c = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32( Entity.TipoDeCatalogo.divisas));

            foreach (var item in c)
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getInstitutions()
        {
            var combo = new List<Combo>();
            var institucion = ControlInstitucion.ObteneTodos();
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorUsuarioId(usId).LastOrDefault(x=>x.Activo);

            if (contratoUsuario != null)
            {
                var usuario = ControlUsuario.Obtener(contratoUsuario.IdUsuario);
                foreach (var item in institucion)
                {
                    if (usuario.RolId != 1 && usuario.RolId != 13)
                    {
                        if (item.Activo && item.Habilitado && item.ContratoId == contratoUsuario.IdContrato && item.Tipo == contratoUsuario.Tipo)
                        {
                            combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
                        }
                    }
                    else
                    {
                        if (item.Activo && item.Habilitado)
                        {
                            combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
                        }

                    }
                }
            }
            else
            {
                foreach (var item in institucion)
                {
                    if (item.Activo && item.Habilitado)
                    {
                        combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
                    }
                }
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getContractsForSubcontract(string tracking)
        {
            var combo = new List<Combo>();
            var contrato = ControlContrato.ObtenerPorTrackingId(new Guid(tracking));            

            if(contrato != null)
            {
                combo.Add(new Combo { Desc = contrato.Nombre, Id = contrato.Id.ToString() });
            }            

            return combo;
        }

        [WebMethod]
        public static List<Combo> getInstitutionsForSubcontract(string tracking)
        {
            var combo = new List<Combo>();
            var contrato = ControlContrato.ObtenerPorTrackingId(new Guid(tracking));
            var institucion = contrato != null ? ControlInstitucion.ObteneTodos() : null;

            if (institucion != null)
            {
                foreach (var k in institucion)
                {
                    if (k.ContratoId == contrato.Id)
                    {
                        combo.Add(new Combo { Desc = k.Nombre, Id = k.Id.ToString() });
                    }

                }
            }

            

            return combo;
        }

        [WebMethod]
        public static Object getContract(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Consultar)
                {
                    var contract = ControlContrato.ObtenerPorTrackingId(new Guid(trackingid));
                    ContratoEditAux obj = new ContratoEditAux();

                    if (contract != null)
                    {
                        obj.Id = contract.Id.ToString();
                        obj.ClienteId = contract.ClienteId.ToString();
                        obj.Nombre = contract.Nombre;
                        obj.TrackingId = contract.TrackingId.ToString();
                        obj.VigenciaFinal = contract.VigenciaFinal.ToString("dd/MM/yyyy");
                        obj.VigenciaInicial = contract.VigenciaInicial.ToString("dd/MM/yyyy");
                        obj.InstitucionId = contract.InstitucionId.ToString();
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", obj });

                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static DataTable getSubContracts(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string contratoTrackingId)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        var contrato = ControlContrato.ObtenerPorTrackingId(new Guid(contratoTrackingId));

                        List<Where> where = new List<Where>();
                        where.Add(new Where("S.Activo", "1"));
                        where.Add(new Where("S.ContratoId", contrato.Id.ToString()));
                        where.Add(new Where("S.Id", Where.GREATER, "0"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "S.Id",
                                "S.TrackingId",
                                "C.ClienteId",
                                "CL.Cliente",
                                "C.Contrato",
                                "CR.Nombre",
                                "S.Subcontrato",
                                "date_format(S.VigenciaInicial, '%d/%m/%Y') VigenciaInicial",
                                "date_format(S.VigenciaFinal, '%d/%m/%Y') VigenciaFinal",
                                "S.Activo",
                                "S.Habilitado",
                                "S.Logotipo",
                                "S.SalarioMinimo",
                                "S.InstitucionId",
                                "SI.WSAInstitucionId WSAInstitucionId",
                                "S.DivisaId",
                                "ifnull(S.Latitud,'') Latitud",
                                "ifnull(S.Longitud,'') Longitud",
                                "S.Banner Banner"
                            },
                            from = new DT.Table("subcontrato", "S"),
                            joins = new List<Join> {
                                new Join(new DT.Table("Contrato", "C"), "C.Id = S.ContratoId", "INNER"),
                                new Join(new DT.Table("Cliente", "CL"), "CL.Id = C.ClienteId", "INNER"),
                                new Join(new DT.Table("Institucion", "CR"), "CR.Id = S.InstitucionId", "INNER"),
                                new Join(new DT.Table("vSubContratoInstitucionWeb", "SI"), "SI.SubcontratoId = S.Id", "LEFT")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable dts = new DataTable();
                        dts = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static string blockSubcontract(string trackingid)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Eliminar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var subcontratoAux = ControlSubcontrato.ObtenerPorTrackingId(new Guid(trackingid));
                    subcontratoAux.Activo = true;
                    subcontratoAux.Habilitado = (subcontratoAux.Habilitado) ? false : true;
                    ControlSubcontrato.Actualizar(subcontratoAux);

                    string message = "";
                    message = subcontratoAux.Habilitado ? "habilitado" : "deshabilitado";

                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, message });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static Object saveSubcontract(SubcontratoAux subcontrato)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Modificar)
                {
                    var subcontratoNuevo = new Entity.Subcontrato();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = subcontrato.Id;
                    if (id == "") id = "";                    

                    DateTime fechaInicial;
                    DateTime fechaFinal;

                    try
                    {
                        fechaInicial = Convert.ToDateTime(subcontrato.VigenciaInicial);
                    }
                    catch (Exception)
                    {
                        throw new Exception(string.Concat("El valor de la vigencia inicial tiene un formato incorrecto."));
                    }

                    try
                    {
                        fechaFinal = Convert.ToDateTime(subcontrato.VigenciaFinal);
                    }
                    catch (Exception)
                    {
                        throw new Exception(string.Concat("El valor de las vigencia final tiene un formato incorrecto."));
                    }

                    if (fechaInicial > fechaFinal)
                    {
                        throw new Exception(string.Concat("La vigencia inicial no puede ser mayor a la vigencia final."));
                    }

                    subcontratoNuevo.ContratoId = Convert.ToInt32(subcontrato.ContratoId);
                    subcontratoNuevo.InstitucionId = Convert.ToInt32(subcontrato.InstitucionId);
                    subcontratoNuevo.Nombre = subcontrato.Subcontrato;
                    subcontratoNuevo.VigenciaFinal = Convert.ToDateTime(subcontrato.VigenciaFinal);
                    subcontratoNuevo.VigenciaInicial = Convert.ToDateTime(subcontrato.VigenciaInicial);
                    subcontratoNuevo.DivisaId = Convert.ToInt32(subcontrato.DivisaId);
                    subcontratoNuevo.Latitud = subcontrato.Latitud;
                    subcontratoNuevo.Longitud = subcontrato.Longitud;
                    subcontratoNuevo.Banner = subcontrato.Banner;

                    if (subcontrato.Logotipo == "undefined")
                        subcontratoNuevo.Logotipo = "~/Content/img/logo.png";
                    else
                        subcontratoNuevo.Logotipo = subcontrato.Logotipo;

                    subcontratoNuevo.SalarioMinimo = Convert.ToDecimal(subcontrato.SalarioMinimo);
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    if (string.IsNullOrEmpty(id))
                    {
                        mode = @"registró";
                        
                        subcontratoNuevo.Activo = true;
                        subcontratoNuevo.CreadoPor = usId;
                        subcontratoNuevo.Habilitado = true;
                        subcontratoNuevo.TrackingId = Guid.NewGuid();
                        subcontratoNuevo.Id = ControlSubcontrato.Guardar(subcontratoNuevo);
                        if(subcontrato.WSAInstitucionId !=null)
                        {
                            if (subcontrato.WSAInstitucionId.Length > 0 )
                            {
                                foreach (var item in subcontrato.WSAInstitucionId)
                                {
                                    Entity.SubcontratoWSAInstitucion subcontratoWSAInstitucion = new Entity.SubcontratoWSAInstitucion();
                                    subcontratoWSAInstitucion.TrackingId = new Guid();
                                    subcontratoWSAInstitucion.IdSubcontrato = subcontratoNuevo.Id;
                                    subcontratoWSAInstitucion.IdWSAInstitucion = Convert.ToInt32(item);
                                    subcontratoWSAInstitucion.Activo = true;
                                    subcontratoWSAInstitucion.Habilitado = true;

                                    subcontratoWSAInstitucion.Id = ControlSubcontratoWSAInstitucion.Guardar(subcontratoWSAInstitucion);
                                }
                            }
                        }

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de subcontrato";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);


                    }
                    else
                    {
                        mode = @"actualizó";
                        subcontratoNuevo.Id = Convert.ToInt32(subcontrato.Id);
                        subcontratoNuevo.Activo = true;
                        subcontratoNuevo.Habilitado = true;
                        subcontratoNuevo.TrackingId = new Guid(subcontrato.TrackingId);
                        
                        
                        ControlSubcontrato.Actualizar(subcontratoNuevo);

                        var subcontratoWSAInstituciones = ControlSubcontratoWSAInstitucion.ObtenerPorSubcontratoId(subcontratoNuevo.Id);
                        if (subcontratoWSAInstituciones.Count > 0)
                        {
                            if (subcontrato.WSAInstitucionId != null && subcontrato.WSAInstitucionId.Length > 0)
                            {
                                List<int> listaWSAInstitucionesOld = subcontratoWSAInstituciones.Select(x => x.IdWSAInstitucion).Distinct().ToList();
                                List<int> listaWSAInstitucionesNew = new List<int>();
                                foreach (var item in subcontrato.WSAInstitucionId)
                                {
                                    listaWSAInstitucionesNew.Add(Convert.ToInt32(item));
                                }

                                foreach (var item in listaWSAInstitucionesNew.Where(x => !listaWSAInstitucionesOld.Contains(x)))
                                {
                                    Entity.SubcontratoWSAInstitucion obj = new Entity.SubcontratoWSAInstitucion();
                                    obj.TrackingId = Guid.NewGuid();
                                    obj.IdSubcontrato = subcontratoNuevo.Id;
                                    obj.IdWSAInstitucion = item;
                                    obj.Activo = true;
                                    obj.Habilitado = true;

                                    obj.Id = ControlSubcontratoWSAInstitucion.Guardar(obj);
                                }

                                foreach (var item in listaWSAInstitucionesOld.Where(x => !listaWSAInstitucionesNew.Contains(x)))
                                {
                                    ControlSubcontratoWSAInstitucion.Eliminar(subcontratoWSAInstituciones.FirstOrDefault(x => x.IdWSAInstitucion == item).Id);
                                }
                            }
                            else
                            {
                                foreach (var item in subcontratoWSAInstituciones)
                                {
                                    ControlSubcontratoWSAInstitucion.Eliminar(item.Id);
                                }
                            }
                        }
                        else
                        {
                            if (subcontrato.WSAInstitucionId != null)
                            {
                                if (subcontrato.WSAInstitucionId.Length > 0)
                                {
                                    List<int> listaWSAInstitucionesNew = new List<int>();
                                    foreach (var item in subcontrato.WSAInstitucionId)
                                    {
                                        listaWSAInstitucionesNew.Add(Convert.ToInt32(item));
                                    }

                                    foreach (var item in listaWSAInstitucionesNew)
                                    {
                                        Entity.SubcontratoWSAInstitucion obj = new Entity.SubcontratoWSAInstitucion();
                                        obj.TrackingId = Guid.NewGuid();
                                        obj.IdSubcontrato = subcontratoNuevo.Id;
                                        obj.IdWSAInstitucion = item;
                                        obj.Activo = true;
                                        obj.Habilitado = true;

                                        obj.Id = ControlSubcontratoWSAInstitucion.Guardar(obj);
                                    }
                                }
                            }
                            
                        }
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de subcontrato";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    return new { exitoso = true, mensaje = mode, Id = subcontratoNuevo.Id.ToString(), TrackingId = subcontrato.TrackingId.ToString() };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
    }
    public class ContratoAux
    {
        public string Id { get; set; }
        public string ClienteId { get; set; }
        public string Nombre { get; set; }
        public string VigenciaInicial { get; set; }
        public string VigenciaFinal { get; set; }
        public string InstitucionId { get; set; }
    }

    public class ContratoEditAux
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string ClienteId { get; set; }
        public string Nombre { get; set; }
        public string VigenciaInicial { get; set; }
        public string VigenciaFinal { get; set; }
        public string InstitucionId { get; set; }
    }

    public class SubcontratoAux
    {
        public string Id { get; set; }
        public string ContratoId { get; set; }
        public string Subcontrato { get; set; }
        public string TrackingId { get; set; }
        public string VigenciaInicial { get; set; }
        public string VigenciaFinal { get; set; }
        public string InstitucionId { get; set; }
        public string Logotipo { get; set; }
        public string SalarioMinimo { get; set; }
        public string[] WSAInstitucionId { get; set; }
        public string DivisaId { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Banner { get; set; }
    }
}