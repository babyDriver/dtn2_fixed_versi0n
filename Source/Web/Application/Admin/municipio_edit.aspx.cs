﻿using Business;
using Entity.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Web.Application.Admin
{
    public partial class municipio_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //  validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static Object save(MunicipioAux municipio)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
                {
                    var munucipionuevo = new Entity.Municipio();

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = municipio.Id;
                    if (id == "") id = "0";
                    
                    var municipio_duplicado = ControlMunicipio.ObtenerTodo();

                    foreach(var k in municipio_duplicado)
                    {
                        if (k.EstadoId.ToString() == municipio.EstadoId && k.Nombre == municipio.Municipio && k.Id.ToString() != municipio.Id)
                        {
                            throw new Exception(string.Concat("El municipio ya está registrado, favor de registrar otro."));

                        }

                        if (k.Clave.ToString() == municipio.Clave && k.Id.ToString() != municipio.Id)
                        {
                            throw new Exception(string.Concat("La clave ya está siendo utilizada por otro municipio"));
                        }
                    }
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    if (string.IsNullOrEmpty(municipio.Id))
                    {
                        mode = @"registró";
                        //var tmpmail = String.Concat(cliente.Nombre, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

                        munucipionuevo.Activo = true;

                        munucipionuevo.Clave = municipio.Clave;
                        munucipionuevo.Nombre = municipio.Municipio;
                        munucipionuevo.Descripcion = municipio.Descripcion;
                        munucipionuevo.EstadoId = Convert.ToInt32(municipio.EstadoId);
                        munucipionuevo.Salario = Convert.ToDouble(municipio.Salario);
                        munucipionuevo.Habilitado = true;
                        munucipionuevo.Activo = true;
                        munucipionuevo.TrackingId = Guid.NewGuid();
                        munucipionuevo.Creadopor = usId;
                        munucipionuevo.Id = ControlMunicipio.Guardar(munucipionuevo);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de municipio";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {
                        mode = @"actualizó";
                        munucipionuevo.Id= Convert.ToInt32(municipio.Id);
                        munucipionuevo.Clave = municipio.Clave;
                        munucipionuevo.Nombre = municipio.Municipio;
                        munucipionuevo.Descripcion = municipio.Descripcion;
                        munucipionuevo.EstadoId = Convert.ToInt32(municipio.EstadoId);
                        munucipionuevo.Salario =Convert.ToDouble(municipio.Salario);
                        munucipionuevo.Habilitado = true;
                        munucipionuevo.Activo = true;
                        munucipionuevo.TrackingId = Guid.NewGuid();
                        munucipionuevo.Creadopor = usId;
                        ControlMunicipio.Actualizar(munucipionuevo);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de municipio";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    return new { exitoso = true, mensaje = mode, Id = munucipionuevo.Id.ToString(), TrackingId = munucipionuevo.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static Object GetMunicipio(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)

                {

                    var _municipio = ControlMunicipio.Obtener(new Guid(trackingid));
                    var _estado = ControlEstado.Obtener(_municipio.EstadoId);
                    MunicipioEditAux municipio = new MunicipioEditAux();
                    if (_municipio != null)
                    {

                        municipio.Id = _municipio.Id.ToString();
                        municipio.Municipio = _municipio.Nombre;
                        municipio.Descripcion = _municipio.Descripcion;
                        municipio.Clave = _municipio.Clave;
                        municipio.EstadoId = _municipio.EstadoId.ToString();
                        municipio.PaisId = _estado.IdPais.ToString();
                        municipio.TrackingId = _municipio.TrackingId.ToString();
                        municipio.Salario = _municipio.Salario.ToString();

                    }

                    return municipio;

                }
                else
                {
                    return new { exitoso = false, mensaje = "No se encontro informacion referente al estado seleccionado." };
                }
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message };
            }

        }

        public class MunicipioAux
        {
            public string Id { get; set; }
            public string Municipio { get; set; }
            public string Descripcion { get; set; }
            public string Clave { get; set; }
            public string EstadoId { get; set; }
            public string TrackingId { get; set; }
            public string PaisId { get; set; }
            public string Salario { get; set; }
        }

        public class MunicipioEditAux
        {
            public string Id { get; set; }
            public string Municipio { get; set; }
            public string Descripcion { get; set; }
            public string Clave { get; set; }
            public string EstadoId { get; set; }
            public string TrackingId { get; set; }
            public string PaisId { get; set; }
            public string Salario { get; set; }
        }
    }
}