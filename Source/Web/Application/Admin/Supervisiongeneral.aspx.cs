﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using Entity.Util;
using System.Linq;
using System.Collections;

namespace Web.Application.Admin
{
    public partial class Supervisiongeneral : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validatelogin();
            if (!IsPostBack)
            {
                tbFechaInicial.Text = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                tbFechaFinal.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string opcion, string fechaInicio, string fechaFinal)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
            {
                try
                {
                    if (opcion == null)
                    {
                        opcion = "0";
                    }
                    if (!emptytable)
                    {
                        //order.FirstOrDefault().column = 3;

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        List<Where> where2 = new List<Where>();
                        List<Join> join = new List<Join>();


                        join.Add(new Join(new Table("general", "g"), "I.id  = g.DetenidoId"));
                        join.Add(new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"));
                        join.Add(new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"));
                        join.Add(new Join(new Table("Motivo_detencion_Interno", "T"), "T.InternoId  = E.Id", "LEFT"));
                        join.Add(new Join(new Table("calificacion", "ca"), "ca.InternoId  = E.Id"));
                        join.Add(new Join(new Table("situacion_detenido", "sd"), "sd.Id  = ca.SituacionId"));
                        join.Add(new Join(new Table("calificacion_ingreso", "ci"), "ci.CalificacionId = ca.Id", "LEFT"));
                        join.Add(new Join(new Table("vDetenidohorasaresto", "V"), "v.DetalleDetencionId = E.Id", "LEFT"));
                        where.Add(new Where("cast(E.Fecha as DATE)", ">=", fechaInicio));
                        where.Add(new Where("cast(E.Fecha as DATE)", "<=", fechaFinal));

                        /// FILTROS  genericos
                        if (opcion != "9" && opcion != "10" && opcion != "7")
                        {
                            where.Add(new Where("E.Activo", "1"));
                            where.Add(new Where("E.Estatus", "<>", "2"));
                        }

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        int IdContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(IdContrato);

                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("E.Tipo", contratoUsuario.Tipo));


                        //where.Add(new Where("cl.Accion", "Registrado"));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                        }


                        /// 









                        if (opcion == "0" || opcion == null) //Todos
                        {


                        }

                        if (opcion == "1") //Menores de edad
                        {
                            where.Add(new Where("TIMESTAMPDIFF(YEAR, g.FechaNacimineto, CURDATE())", "<", "18"));


                            //where.Add(new Where("cl.Accion", "Registrado"));

                            if ((!string.Equals(perfil, "Administrador")))
                            {
                                var user = ControlUsuario.Obtener(usuario);
                            }
                        }

                        if (opcion == "2") //Detenidos sin calificar
                        {

                            where.Add(new Where("ifnull (sd.Nombre, 'Pendiente')", "Pendiente"));

                            //where.Add(new Where("cl.Accion", "Registrado"));

                            if ((!string.Equals(perfil, "Administrador")))
                            {
                                var user = ControlUsuario.Obtener(usuario);
                            }
                        }

                        if (opcion == "3") //Próximos a cumplir
                        {
                            order.FirstOrDefault().column = 9;
                            where.Add(new Where("ifnull (V.Horas, '0')", ">=", "0"));



                            if ((!string.Equals(perfil, "Administrador")))
                            {
                                var user = ControlUsuario.Obtener(usuario);
                            }
                        }

                        if (opcion == "4") //Detenidos en examén medico : DEtenidos que tienen registrados al menos un examen medico.
                        {
                            join.Add(new Join(new Table("examen_medico", "EM"), "EM.DetalleDetencionId = E.Id", "INNER"));

                        }


                        if (opcion == "5") //Detenidos calificados :Esta opción debe mostrar todos los detenidos  que ya fueron calificados.
                        {

                            where.Add(new Where("sd.Nombre", "<>", "null"));
                            // where.Add(new Where("ifnull (sd.Nombre, 'Pendiente')", "Pendiente"));

                            //where.Add(new Where("cl.Accion", "Registrado"));

                            if ((!string.Equals(perfil, "Administrador")))
                            {
                                var user = ControlUsuario.Obtener(usuario);
                            }
                        }

                        if (opcion == "6") //Detenidos en ingreso: los detenidos que ya pasaron por barandilla, es decir, que ya se registro su ingreso, más aún no se les ha realizado su examen médico y no han sido calificados.
                        {
                            join.Add(new Join(new Table("examen_medico", "EM"), "EM.DetalleDetencionId = E.Id"));
                            where.Add(new Where("ifnull(EM.Id,0)", "0"));
                            where.Add(new Where("ifnull(ca.Id,0)", "0"));
                        }

                        if (opcion == "7")//Detenidos en egreso
                        {
                            where.Add(new Where("E.Estatus", "2"));
                        }


                        if (opcion == "8") //Detenidos por fecha y hora de salida  pendiente por definir
                        {

                        }

                        if (opcion == "9")//Detenidos salida autorizada: detenidos a los que se le ha autorizado la salida, pero que aún no se ha liberado. Son detenidos sin horas de arresto, sin multa
                        {
                            join.Add(new Join(new Table("historial", "H"), "H.InternoId = E.Id", "LEFT"));
                            where.Add(new Where("H.Movimiento", "Salida efectuada del detenido"));
                            where.Add(new Where("ifnull (V.Horas, '0')", "<=", "0"));
                            where.Add(new Where("ca.TotalAPagar", "0"));

                        }

                        if (opcion == "10")//Salida autorizada por Pago de multa, pero que aún no han pagado. - En este filtro se muestran los detenidos que están pendientes de pago en caja.
                        {
                            join.Add(new Join(new Table("historial", "H"), "H.InternoId = E.Id", "LEFT"));
                            where.Add(new Where("H.Movimiento", "Salida efectuada del detenido"));
                            where.Add(new Where("ca.TotalAPagar", ">", "0"));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                                "distinct (I.Id) Id",
                                "I.TrackingId",
                                "trim(I.Nombre) Nombre",
                                "trim(I.Paterno) Paterno",
                                "trim(I.Materno) Materno",
                                "I.RutaImagen",
                                "cast(E.Expediente as unsigned) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus" ,
                                "ES.Nombre EstatusNombre",
                                "ifnull(T.internoId,0) DetalledetencionId",
                                "ifnull (sd.Nombre, 'Pendiente') Situacion",
                                "ifnull(ca.Id,0) CalificacionId",
                                "ci.IngresoId",
                                //"timediff(date_add(E.Fecha,interval ifnull(ca.totalHoras,0) hour),CURRENT_TIMESTAMP() ) Horas ",
                                "ca.TotalAPagar",
                                "ifnull(V.horas,0) Horas",
                                "g.FechaNacimineto",
                                "concat(I.Nombre,' ',TRIM(Paterno),' ',TRIM(Materno)) as NombreCompleto",
                            },
                            from = new Table("detenido", "I"),
                            joins = join,
                            /* joins = new List<Join>
                             {
                                 new Join(new Table("general", "g"), "I.id  = g.DetenidoId"),
                                 new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                                 new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"),
                                 new Join(new Table("Motivo_detencion_Interno", "T"), "T.InternoId  = E.Id","LEFT"),
                                 new Join(new Table("calificacion", "ca"), "ca.InternoId  = E.Id"),
                                 new Join(new Table("situacion_detenido", "sd"), "sd.Id  = ca.SituacionId"),
                                 //new Join(new Table("calificacionlog", "cl"), "cl.InternoId = E.Id"),
                                 new Join(new Table("calificacion_ingreso", "ci"), "ci.CalificacionId = ca.Id", "LEFT"),
                                 new Join(new Table("vDetenidohorasaresto", "V"), "v.DetalleDetencionId = E.Id", "LEFT"),
                                 new Join(new Table("examen_medico", "EX"), "EX.DetalleDetencionId  = E.Id")
                             },*/
                            wheres = where,
                            orderBy = "V.horas"
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var result1 = dt.Generar(query, draw, start, length, search, order, columns);
                        return result1;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static DataTable getDataHistorial(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string TrackingId)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Defensor público" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(TrackingId));
                        string[] data = new string[]
                        {
                            interno.Id.ToString(),
                            "true"
                        };
                        var detalleDetencion = interno != null ? ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(data) : null;

                        if (detalleDetencion == null)
                        {
                            detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();

                            

                        }


                        where.Add(new Where("O.Activo", "1"));

                        if (detalleDetencion != null)
                        {
                            where.Add(new Where("O.InternoId", detalleDetencion.DetenidoId.ToString()));
                        }

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        //Prueba de IN

                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);

                        }
                        Query query = new Query
                        {
                            select = new List<string> {
                                "O.Id",
                                "O.TrackingId",
                                "O.InternoId",
                                "O.Movimiento",
                                "date_format(O.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "O.Activo",
                                "O.Habilitado",
                                "U.Usuario"
                            },
                            from = new DT.Table("historial", "O"),
                            joins = new List<Join>
                            {
                                new Join(new DT.Table("usuario", "U"), "U.id  = ifnull(O.ModificadoPor,O.CreadoPor)")
                            },
                            wheres = where,
                            orderBy = "Fecha"
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var result1 = dt.Generar(query, draw, start, length, search, order, columns);
                        return result1;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        [WebMethod]
        public static List<Combo> GetFiltrosJuezCalificador()
        {
            List<Combo> combo = new List<Combo>();

            //List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.estado_civil));
            int IdContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(IdContrato);

            bool MostrarExamenmedico = false;

            try
            {
                var parametroContrato = ControlParametroContrato.TraerTodos().LastOrDefault(x => x.ContratoId == contratoUsuario.IdContrato && x.Parametroid == Convert.ToInt32(Entity.ParametroEnum.TRASLADO_O_SALIDA_SIN_EXAMEN_MÉDICO));

                if (parametroContrato.Valor == "1")
                {
                    MostrarExamenmedico = true;
                }

            }
            catch (Exception)
            {


            }

            //combo.Add(new Combo { Desc = "Todos los detenidos", Id ="0" });
            combo.Add(new Combo { Desc = "Detenidos en ingreso", Id = "6" });
            combo.Add(new Combo { Desc = "Detenidos en ergreso", Id = "7" });
            combo.Add(new Combo { Desc = "Detenidos en examén médico", Id = "4" });
            
            combo.Add(new Combo { Desc = "Detenidos calificados", Id = "5" });
            combo.Add(new Combo { Desc = "Detenidos por salir", Id = "3" });
            combo.Add(new Combo { Desc = "Detenidos salida autorizada", Id = "9" });
            combo.Add(new Combo { Desc = "Salida autorizada por pago de multa", Id = "10" });
            combo.Add(new Combo { Desc = "Detenidos sin calificar", Id = "2" });
            combo.Add(new Combo { Desc = "Menores de edad", Id = "1" });

            /*
             <!--  <option value="7">Detenidos en custodia externa</option>-->
                    <!--  <option value="8">Detenidos por fecha y hora de salida</option>-->
                    <!--<option value="9">Detenidos salida autorizada</option>-->
                    <!--<option value="10">Salida autorizada por pago de multa</option>-->
                    <option value="2">Detenidos sin calificar</option> 
                    <option value="1">Menores de edad</option>
                   
             */

            return combo;

        }
        [WebMethod]
        public static string ValidateFecha(historialaux historico)
        {
            try
            {
                var detalle = new Entity.DetalleDetencion();
                var fecha = Convert.ToDateTime(historico.Fecha);
                Entity.Historial historial = new Entity.Historial();
                historial.Id = Convert.ToInt32(historico.Id);
                historial.Fecha = Convert.ToDateTime(historico.Fecha);
                
                var histaux = ControlHistorial.ObtenerPorId(historial.Id);
                 if (historico.Movimiento == "Modificación de datos generales en certificado químico" || historico.Movimiento == "Modificación de datos generales en certificado quimico")
                {
                    var historyServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdHistorial(Convert.ToInt32(historico.Id));
                    if (historyServicioMedico != null)
                    {
                        var servicioMedico = ControlServicoMedico.ObtenerPorId(historyServicioMedico.IdServicioMedico);
                        detalle = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                    }
                }
                
                Boolean validate = true;
                if(detalle!=null)
                {
                    int result = DateTime.Compare(fecha, Convert.ToDateTime(detalle.Fecha));
                    if (result < 0)
                    {
                        validate = false;
                    }
                }
                
                var fechar = Convert.ToDateTime(detalle.Fecha);
                var h = fechar.Hour.ToString();
                if (fechar.Hour < 10) h = "0" + h;
                var m = fechar.Minute.ToString();
                if (fechar.Minute < 10) m = "0" + m;
                var s = fechar.Second.ToString();
                if (fechar.Second < 10) s = "0" + s;
                var registro = fechar.ToShortDateString() + " " + h + ":" + m + ":" + s;
                return JsonConvert.SerializeObject(new { exitoso = true, Validar = validate, fechaRegistro = registro });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }
        [WebMethod]
        public static object Save(historialaux historico)
        {

            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(usId);
                if (usuario != null)
                {
                    Entity.Historial historial = new Entity.Historial();
                    historial.Id = Convert.ToInt32(historico.Id);
                    historial.Fecha = Convert.ToDateTime(historico.Fecha);
                    historial.ModificadoPor = usuario.Id;
                    var histaux = ControlHistorial.ObtenerPorId(historial.Id);
                    if (historico.Movimiento == "Modificación de datos generales en certificado psicofisiológico")
                    {
                        var historyServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdHistorial(Convert.ToInt32(historico.Id));
                        if (historyServicioMedico != null)
                        {
                            var servicioMedico = ControlServicoMedico.ObtenerPorId(historyServicioMedico.IdServicioMedico);

                            var cert = ControlCertificadoPsicoFisiologico.ObtenerPorId(servicioMedico.CertificadoMedicoPsicofisiologicoId);
                            cert.FechaValoracion = historial.Fecha;
                            ControlCertificadoPsicoFisiologico.Actualizar(cert);
                        }
                    }
                    else if (historico.Movimiento == "Modificación de datos generales en certificado de lesiones")
                    {
                        var historyServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdHistorial(Convert.ToInt32(historico.Id));
                        if (historyServicioMedico != null)
                        {
                            var servicioMedico = ControlServicoMedico.ObtenerPorId(historyServicioMedico.IdServicioMedico);

                            var cert = ControlCertificadoLesiones.ObtenerPorId(servicioMedico.CertificadoLesionId);
                            cert.Modificadopor = 8;
                            cert.Fechaultimamodificacion = historial.Fecha;
                            cert.Fechavaloracion = historial.Fecha;
                            ControlCertificadoLesiones.Actualizar(cert);
                        }
                    }
                    else if (historico.Movimiento == "Modificación de datos generales en certificado químico" || historico.Movimiento == "Modificación de datos generales en certificado quimico")
                    {
                        var historyServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdHistorial(Convert.ToInt32(historico.Id));
                        if (historyServicioMedico != null)
                        {
                            var servicioMedico = ControlServicoMedico.ObtenerPorId(historyServicioMedico.IdServicioMedico);

                            var cert = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);
                            cert.Fecha_proceso = historial.Fecha;
                            cert.Fecha_toma = historial.Fecha;
                            ControlCertificadoQuimico.Actualizar(cert);
                        }
                    }
                    else if (historico.Movimiento == "Modificación de datos generales en certificado químico fecha de proceso")
                    {
                        var historyServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdHistorial(Convert.ToInt32(historico.Id));
                        if (historyServicioMedico != null)
                        {
                            var servicioMedico = ControlServicoMedico.ObtenerPorId(historyServicioMedico.IdServicioMedico);

                            var cert = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);
                            cert.Fecha_proceso = historial.Fecha;
                            //cert.Fecha_toma = historial.Fecha;
                            ControlCertificadoQuimico.Actualizar(cert);
                        }
                    }
                    else if (historico.Movimiento == "Modificación de datos generales en certificado químico fecha de toma")
                    {
                        var historyServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdHistorial(Convert.ToInt32(historico.Id));
                        if (historyServicioMedico != null)
                        {
                            var servicioMedico = ControlServicoMedico.ObtenerPorId(historyServicioMedico.IdServicioMedico);

                            var cert = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);
                            //cert.Fecha_proceso = historial.Fecha;
                            cert.Fecha_toma = historial.Fecha;
                            ControlCertificadoQuimico.Actualizar(cert);
                        }
                    }
                    else if (historico.Movimiento == "Modificación de datos generales en certificado químico")
                    {
                        var historyServicioMedico = ControlHistorialServicioMedico.ObtenerPorIdHistorial(Convert.ToInt32(historico.Id));
                        if (historyServicioMedico != null)
                        {
                            var servicioMedico = ControlServicoMedico.ObtenerPorId(historyServicioMedico.IdServicioMedico);

                            var cert = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);
                            //cert.Fecha_proceso = historial.Fecha;
                            cert.Fecha_toma = historial.Fecha;
                            cert.Fecha_proceso = historial.Fecha;
                            ControlCertificadoQuimico.Actualizar(cert);
                            var hist = new Entity.Historial();
                            hist.TrackingId = Guid.NewGuid();
                            hist.Activo = true;
                            hist.Habilitado = true;
                            hist.InternoId = cert.DetenidoId;
                            hist.Fecha = cert.Fecha_toma;
                            hist.Movimiento = "Modificación de datos generales en certificado químico fecha de toma";
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            hist.ContratoId = subcontrato.Id;
                            hist.Id = ControlHistorial.Guardar(hist);

                            hist = new Entity.Historial();
                            hist.TrackingId = Guid.NewGuid();
                            hist.Activo = true;
                            hist.Habilitado = true;
                            hist.InternoId = cert.DetenidoId;
                            hist.Fecha = cert.Fecha_proceso;
                            hist.Movimiento = "Modificación de datos generales en certificado químico fecha de proceso";
                            
                            hist.ContratoId = subcontrato.Id;
                            hist.Id = ControlHistorial.Guardar(hist);
                        }
                    }
                    //else if (historico.Movimiento == "Registro de movimiento de celda")
                    //{
                    //    var history = ControlHistorial.ObtenerPorId(Convert.ToInt32(historico.Id));
                    //    if (history != null)
                    //    {
                    //        var detalledetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(historico.InternoId)).LastOrDefault(x => x.Activo);
                    //        var movimientos = ControlMovimiento.ObtenerPorDetalleDetencionId(detalledetencion.Id);
                    //        var movimientoEdit = movimientos.FirstOrDefault(x => x.FechaYHora == history.Fecha);
                    //        if(movimientoEdit != null)
                    //        {
                    //            movimientoEdit.FechaYHora = historial.Fecha;
                    //            ControlMovimiento.Actualizar(movimientoEdit);
                    //        }

                    //    }
                    //}
                    else if (historico.Movimiento == "Registro de la calificación")
                    {
                        var histCalificacion = ControlHistorialCalificacion.ObtenerTodosPorInternoId(histaux.InternoId);

                        if(histCalificacion.Count>0)
                        {
                            Entity.HistorialCalificacion histCal = new Entity.HistorialCalificacion();

                            histCal = histCalificacion.LastOrDefault(x =>x.Fecha==histaux.Fecha);
                            if (histCal != null)
                            {
                                histCal.Fecha = historial.Fecha;
                                ControlHistorialCalificacion.Actulizar(histCal);


                            }

                        }
                        
                    }

                    else if (historico.Movimiento == "Modificación de la calificación")
                    {
                        var histCalificacion = ControlHistorialCalificacion.ObtenerTodosPorInternoId(histaux.InternoId);

                        if (histCalificacion.Count > 0)
                        {
                            Entity.HistorialCalificacion histCal = new Entity.HistorialCalificacion();

                            histCal = histCalificacion.LastOrDefault(x => x.Fecha == histaux.Fecha);
                            if (histCal != null)
                            {
                                histCal.Fecha = historial.Fecha;
                                ControlHistorialCalificacion.Actulizar(histCal);


                            }

                        }

                    }

                    else if (historico.Movimiento == "Modificación de datos generales en pago de multa")
                    {
                        var calificacion = ControlCalificacion.ObtenerPorInternoId(Convert.ToInt32(historico.InternoId));
                        var calificacioningreso = ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacion.Id);
                        calificacioningreso.Fecha = historial.Fecha;
                        ControlCalificacionIngreso.Actualizar(calificacioningreso);
                    }
                    else if (historico.Movimiento == "Registro de movimiento de celda")
                    {
                        Entity.MovimientoLog filtro = new Entity.MovimientoLog();
                        filtro.DetalledetencionId = histaux.InternoId;
                        filtro.Fecha = histaux.Fecha;
                        var movlog = ControlMovimientoLog.ObtenerMovimientoLog(filtro);
                        movlog.Fecha = historial.Fecha;
                        movlog.FechaAnt = histaux.Fecha;
                        ControlMovimientoLog.Actualizar(movlog);
                    }


                    else if (historico.Movimiento == "Modificación de movimiento de celda")
                    {
                        Entity.MovimientoLog filtro = new Entity.MovimientoLog();
                        filtro.DetalledetencionId = histaux.InternoId;
                        filtro.Fecha = histaux.Fecha;
                        var movlog = ControlMovimientoLog.ObtenerMovimientoLog(filtro);
                        movlog.Fecha = historial.Fecha;
                        movlog.FechaAnt = histaux.Fecha;
                        ControlMovimientoLog.Actualizar(movlog);
                    }

                    else if (historico.Movimiento == "Registro de alias en barandilla")
                    {
                        Entity.AliasLog filtro = new Entity.AliasLog();
                        filtro.DetenidoId = histaux.InternoId;
                        filtro.Fecha = histaux.Fecha;
                        var movlog = ControlAliasLog.ObtenerMovimientoLog(filtro);
                        movlog.Fecha = historial.Fecha;
                        movlog.FechaAnt = histaux.Fecha;
                        ControlAliasLog.Actualizar(movlog);
                    }


                    else if (historico.Movimiento == "Modificación de alias en barandilla")
                    {
                        Entity.AliasLog filtro = new Entity.AliasLog();
                        filtro.DetenidoId = histaux.InternoId;
                        filtro.Fecha = histaux.Fecha;
                        var movlog = ControlAliasLog.ObtenerMovimientoLog(filtro);
                        movlog.Fecha = historial.Fecha;
                        movlog.FechaAnt = histaux.Fecha;
                        ControlAliasLog.Actualizar(movlog);
                    }

                    ControlHistorial.Actualizar(historial);

                    if (historico.Movimiento == "Registro del detenido en barandilla")
                    {
                        var detalledetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(historico.InternoId)).LastOrDefault(x => x.Activo);

                        detalledetencion.Fecha = Convert.ToDateTime(historico.Fecha);
                        ControlDetalleDetencion.Actualizar(detalledetencion);

                        var infodetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detalledetencion.DetenidoId);
                        infodetencion.HoraYFecha = Convert.ToDateTime(historico.Fecha);
                        ControlInformacionDeDetencion.Actualizar(infodetencion);
                    }

                    

                    return new { exitoso = true, mensaje = "Se actualizó de forma correcta" };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No se pudo actualizar la información" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }

        }

        public class historialaux
        {
           public string Id { get; set; }
           public string Fecha { get; set; }
           public string InternoId { get; set; }
           public string Movimiento { get; set; }

        }
    }
}