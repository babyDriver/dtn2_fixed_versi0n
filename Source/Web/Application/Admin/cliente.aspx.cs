﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Admin
{
    public partial class cliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Configuración y seguridad" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static DataTable getCustomers(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        where.Add(new Where("C.Activo", "1"));
                        Query query = new Query
                        {
                            select = new List<string>{
                        "C.Id",
                        "C.TenantId",
                        "C.Cliente",
                        "C.Rfc",                        
                        "C.Activo",
                        "C.Habilitado"                        
                        },
                            from = new DT.Table("cliente", "C"),                          
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static string blockuser(string trackingid)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Eliminar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var clienteAux = ControlCliente.ObtenerPorTrackingId(new Guid(trackingid));
                    clienteAux.Habilitado = (clienteAux.Habilitado) ? false : true;
                    clienteAux.Activo = true;
                    ControlCliente.Actualizar(clienteAux);
                    Entity.Accion accion = new Entity.Accion();
                    accion.CreadoPor = usId;
                    accion.Descripcion = "Cliente eliminado";
                    accion.Fecha = DateTime.Now;
                    accion.Nombre = "Eliminar";
                    accion.TrackingId = Guid.NewGuid();
                    int idAccion = ControlAccion.Guardar(accion);
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    string message = "";
                    message = clienteAux.Habilitado ? "habilitado" : "deshabilitado";
                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    var movimiento = clienteAux.Habilitado ? "Cliente desbloqueado" : "Cliente bloqueado";
                    historial.Movimiento = movimiento;
                    historial.CreadoPor = usId;

                    historial.ContratoId = contratoUsuario.IdContrato;
                    historial.Id = ControlHistorial.Guardar(historial);
                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, mensaje = message });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}