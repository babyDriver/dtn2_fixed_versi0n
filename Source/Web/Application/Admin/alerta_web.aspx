﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="alerta_web.aspx.cs" Inherits="Web.Application.Admin.alerta_web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Configuración</li>
    <li id="alerta2"></li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-useredit-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-edit"></i></span>
                        <h2 id="alerta1"> Configuración de alerta web </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <fieldset>
                                    <legend class="hide"></legend>
                                    <div class="row">
                                         <section class="col col-3">
                                             <label>Ruta raiz del web service <a style="color:red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-hdd-o"></i>
                                                <input type="text" name="url" id="url" placeholder="Ruta raiz del web service" maxlength="250" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingrese la ruta raiz del web service.</b>
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label>Usuario ID <a style="color:red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-tasks"></i>
                                                <input type="text" name="usernmae" id="username" placeholder="Usuario" maxlength="50" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingrese el nombre de usuario.</b>
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                             <label>Contraseña <a style="color:red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-lock"></i>
                                                <input type="password" name="password" id="password" placeholder="Contraseña del correo" maxlength="256" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingrese la contraseña.</b>
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label>Denominación <a style="color:red">*</a></label>
                                            <label class="input">
                                                <input type="text" name="Denominacion" id="Denominacion" placeholder="Denominación" maxlength="50"class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingrese la denominación.</b>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                    <a href="javascript:void(0);" class="btn btn-sm btn-default save" id="save" style="display: none;"><i class="fa fa-save"></i>&nbsp;Guardar </a>                                
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="hideid" name="hideid" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                document.getElementsByClassName("save")[0].click();
            }
        });
        $(document).ready(function () {
            pageSetUp();

            function LoadData() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "alerta_web.aspx/getData",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.d != null) {
                            data = data.d;
                            $("#url").val(data.Url);
                            $("#username").val(data.Username);
                            $("#password").val(data.Password);
                            $("#hideid").val(data.TrackingId);
                            $("#Denominacin").val(data.Denominacion);
                           
                            $("#alerta1").html("Configuración de Ecatepec" );
                            $("#alerta2").html("Configuración de Ecatepec" );
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de alerta web. Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
            }

            $("body").on("click", ".clear", function () {
                $("#url").val("");
                $("#username").val("");
                $("#password").val("");
            });


            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (IsValid()) {
                    Save();
                }
            });

            function IsValid() {
                var esvalido = true;

                if ($("#url").val() == "") {
                    ShowError("Ruta raiz", "La ruta raiz del servicio web es obligatorio.");
                    $('#url').parent().removeClass('state-success').addClass("state-error");
                    $('#url').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#url').parent().removeClass("state-error").addClass('state-success');
                    $('#url').addClass('valid');
                }

                if ($("#username").val() == "") {
                    ShowError("Usuario", "El usuario es obligatorio.");
                    $('#username').parent().removeClass('state-success').addClass("state-error");
                    $('#username').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#username').parent().removeClass("state-error").addClass('state-success');
                    $('#username').addClass('valid');
                }

                if ($("#password").val() == "") {
                    ShowError("Contraseña", "La contraseña es obligatoria.");
                    $('#password').parent().removeClass('state-success').addClass("state-error");
                    $('#password').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#password').parent().removeClass("state-error").addClass('state-success');
                    $('#password').addClass('valid');
                }

                if ($("#Denominacin").val() == "") {
                    ShowError("Denominación", "La denominación es obligatoria.");
                    $('#Denominacin').parent().removeClass('state-success').addClass("state-error");
                    $('#Denominacin').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#Denominacin').parent().removeClass("state-error").addClass('state-success');
                    $('#Denominacin').addClass('valid');
                }
                return esvalido;
            }

            function Save() {
                startLoading();
                var data = GetValues();
                $.ajax({
                    type: "POST",
                    url: "alerta_web.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        'data': data,
                    }),
                    success: function (data) {
                        var result = JSON.parse(data.d);
                        if (result.success) {
                            $('#hideid').val(result.trackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Información de alerta web " + result.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            $('#main').waitMe('hide');
                            location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + result.message + " Si el problema persiste, contacte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                        }
                    }
                });
            }

            function GetValues() {
                var data = {
                    TrackingId: $('#hideid').val(),
                    Url: $('#url').val(),
                    Username: $('#username').val(),
                    Password: $('#password').val(),
                    Denominacion:$('#Denominacion').val()
                };

                return data;
            }

            function init() {
                LoadData();
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#save").show();
                }
            }

            init();

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
        });
    </script>
</asp:Content>
