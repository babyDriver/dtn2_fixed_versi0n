﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;


namespace Web.Application.Admin
{
    public partial class motivo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
            

            

        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        var divisas = ControlCatalogo.Obtener(subcontrato.DivisaId, Convert.ToInt32(Entity.TipoDeCatalogo.divisas));
                        
                        this.tipodivisacontrato.Value = divisas != null ? "Multa en " + divisas.Nombre : "";

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }


        [WebMethod]
        public static string blockitem(string id)
        {
            try
            {
                //  if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                // {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                Entity.MotivoDetencion motivo = ControlMotivoDetencion.ObtenerPorTrackingId(new Guid(id));
                motivo.Habilitado = motivo.Habilitado == 1 ? (short)0 : (short)1;
                string mensaje = motivo.Habilitado == 1 ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                ControlMotivoDetencion.Actualizar(motivo);
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
                // }
                // else
                // {
                //      return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                // }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }



        //[WebMethod]
        //public static string delete(int id)
        //{
        //    try
        //    {
        //        //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
        //        //{
        //        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

        //        int usuario;

        //        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
        //        else throw new Exception("No es posible obtener información del usuario en el sistema");

        //        Entity.Motivo item = ControlMotivo.ObtenerPorId(id); ;
        //        item.Activo = false;
        //        item.CreadoPor = usuario;
        //        ControlMotivo.Actualizar(item);
        //        return JsonConvert.SerializeObject(new { exitoso = true });
        //        //}
        //        //else
        //        //{
        //        //    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
        //    }
        //}

        [WebMethod]
        public static List<Combo> getListadoCombo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.motivo));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }
        [WebMethod]
        public static List<Combo> GetTipoCombo(String item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.TipoMotivoDetencion> Listado = ControlTipoMotivoDetencion.ObtenerTipos();
            if (Listado.Count>0)
            {
                foreach (var rol in Listado)
                    combo.Add(new Combo { Desc = rol.Descripcion, Id = rol.TipoId.ToString() });
            }


            return combo;
        }

        [WebMethod]
        public static DataTable getmotivo(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string motivo, bool emptytable)
        {
            try
            {
                // if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                //{
                if (!emptytable)
                {
                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    List<Where> where = new List<Where>();
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    if (!string.IsNullOrEmpty(motivo))
                    {
                        where.Add(new Where("S.Nombre", string.Concat(" like '%", motivo.Trim(), "%'")));
                    }
                    where.Add(new Where("S.Activo", "1"));                    
                    where.Add(new Where("S.ContratoId", contratoUsuario.IdContrato.ToString()));
                    where.Add(new Where("S.Tipo", contratoUsuario.Tipo));

                    Query query = new Query
                    {
                        select = new List<string>{
                                "S.Id",
                                "S.Motivo",
                                "S.Descripcion",
                                "S.TrackingId",
                                "S.Habilitado",
                                "S.Articulo",
                                "S.Multa",
                                "S.HorasdeArresto",
                                "S.Activo"
                            },
                        from = new Table("motivoDetencion", "S"),                        
                        wheres = where
                    };

                    DataTables dt = new DataTables(mysqlConnection);
                    return dt.Generar(query, draw, start, length, search, order, columns);
                    //}
                    //else
                    //{
                    //    return DataTables.ObtenerDataTableVacia(null, draw);
                    //}
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }


        [WebMethod]
        public static DataTable getmotivolog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string motivoid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (!emptytable)
                {
                    //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                    //{
                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);



                    List<Where> where = new List<Where>();
                    List<Order> orderby = new List<Order>();

                    if (Convert.ToInt32(motivoid) != 0)
                    {
                        where.Add(new Where("C.Id", motivoid.ToString()));
                    }


                    if (Convert.ToBoolean(todoscancelados))
                    {
                        where.Add(new Where("CC.Activo", "0"));

                    }

                    Query query = new Query
                    {
                        select = new List<string>{
                        "C.Id",
                        "C.TrackingId",
                        "C.Motivo",
                        "C.Habilitado",
                        "C.Descripcion",
                        "UR.Usuario CreadoPor",
                        "C.Accion",
                        "C.AccionId",
                        "C.Fec_Movto"
                        },
                        from = new Table("MotivoDetencion", "C"),
                        joins = new List<Join>
                        {
                            new Join(new Table("usuario", "UR"), "UR.Id = C.CreadoPor", "LEFT OUTER"),
                            new Join(new Table("motivo", "CC"), "CC.Id = C.Id")
                        },
                        wheres = where
                    };
                    DataTables dt = new DataTables(mysqlConnection);
                    return dt.Generar(query, draw, start, length, search, order, columns);
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
                //}
                //else
                //{
                //    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                //}

            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        //[WebMethod]
        //public static DataTable getContracts(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string clienteId)
        //{
        //    try
        //    {
        //        if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
        //        {
        //            if (!emptytable)
        //            {
        //                MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
        //                List<Where> where = new List<Where>();
        //                where.Add(new Where("C.Activo", "1"));
        //                if (!string.IsNullOrEmpty(clienteId) && !(clienteId == "0"))
        //                {
        //                    where.Add(new Where("C.ClienteId", clienteId));
        //                }
        //                Query query = new Query
        //                {
        //                    select = new List<string>{
        //                "C.Id",
        //                "C.TrackingId",
        //                "C.Motivo",
        //                "C.Descripcion",

        //                "C.Activo",
        //                "C.Habilitado"
        //                },
        //                    from = new DT.Table("MotivoDetencion", "C"),

        //                    wheres = where
        //                };

        //                DataTables dt = new DataTables(mysqlConnection);

        //                return dt.Generar(query, draw, start, length, search, order, columns);
        //            }
        //            else
        //            {
        //                return DataTables.ObtenerDataTableVacia(null, draw);
        //            }
        //        }
        //        else
        //        {
        //            return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return DataTables.ObtenerDataTableVacia(null, draw);
        //    }
        //}

        [WebMethod]
        public static DataTable Getmotivoslog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        where.Add(new Where("B.RolId", "<>", "13"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Motivo",
                                "A.Descripcion",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("motivodetencionlog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("motivodetencion","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);

                        ////
                        ///
                        where = new List<Where>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        where.Add(new Where("B.RolId", "=", "13"));
                        where.Add(new Where("A.Accion", "=", "Registrado"));
                        query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Motivo",
                                "A.Descripcion",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("motivodetencionlog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("motivodetencion","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt2 = new DataTables(mysqlconection);
                        DataTable _dt2 = dt2.Generar(query, draw, start, length, search, order, columns);

                        if (_dt2.data.Count > 0)
                        {
                            where = new List<Where>();
                          
                            if (Convert.ToBoolean(todoscancelados))
                            {
                                where.Add(new Where("C.Activo", "0"));
                            }
                            where.Add(new Where("B.RolId", "<>", "13"));
                          
                            query = new Query
                            {
                                select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Motivo",
                                "A.Descripcion",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                                from = new Table("motivodetencionlog", "A"),
                                joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("motivodetencion","C"),"C.Id=A.Id"),
                            },
                                wheres = where

                            };
                            DataTables dt3 = new DataTables(mysqlconection);
                            DataTable _dt3 = dt3.Generar(query, draw, start, length, search, order, columns);
                            List<object> listaoriginal = new List<object>();
                            List<object> listaNueva = new List<object>();
                            Dictionary<string, object> valuePairs = new Dictionary<string, object>();
                            Dictionary<string, object> valuePairsnew = new Dictionary<string, object>();
                            listaoriginal = _dt2.data;
                            listaNueva = _dt3.data;

                            foreach (var item in listaoriginal)
                            {
                                valuePairs = (Dictionary<string, object>)item;
                            }

                            foreach (var item in listaNueva)
                            {
                                valuePairsnew = (Dictionary<string, object>)item;
                            }
                            valuePairs["Creadopor"] = valuePairsnew["Creadopor"];

                            _dt2.data = listaoriginal;
                            _dt.data.Add(_dt2.data[0]);
                        }
                        ///

                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }

            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static DataTable getMotivos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    order.LastOrDefault().column = 0;
                    order.LastOrDefault().dir = "desc";
                    if (!emptytable)
                    {
                        
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        List<Where> where = new List<Where>();
                        where.Add(new Where("C.Activo", "1"));                        
                        where.Add(new Where("C.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("C.Tipo", contratoUsuario.Tipo));                        
                        
                        Query query = new Query
                        {
                            select = new List<string>{
                        "C.Id",
                        "C.TrackingId",
                        "C.Motivo",
                        "C.Descripcion",
                        "C.Articulo",
                        "D.Descripcion Tipo_Motivo" ,
                        "C.MultaMinimo",
                        "ifnull(C.MultaMaximo,0) MultaMaximo",
                        "C.Activo",
                        "C.Habilitado",
                        "C.HorasdeArresto",
                        "D.Id Tipo_MotivoId"
                        },
                            from = new DT.Table("MotivoDetencion", "C"),
                            joins = new List<Join>
                            {
                                new Join(new Table("usuario", "U"), "U.Id = C.CreadoPor", "inner"),
                                new Join(new Table("Tipo_Motivo_Detencion", "D"), "C.Tipo_Motivo = D.Id", "left")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable dt_ = new DataTable();
                        dt_= dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        public class ContratoAux
        {
            public string Id { get; set; }
            public string Motivo { get; set; }
            public string Descripcion { get; set; }
            public string Articulo { get; set; }
            public string MultaMinimo { get; set; }
            public string MultaMaximo { get; set; }
            public string horaArresto { get; set; }
            public string TrackingId { get; set; }
            public int Tipo_Motivo { get; set; }

        }

        public class ContratoEditAux
        {
            public string Id { get; set; }
            public string Motivo { get; set; }
            public string Descripcion { get; set; }
            public string Articulo { get; set; }
            public string MultaMinimo { get; set; }
            public string MultaMaximo { get; set; }
            public string horaArresto { get; set; }
            public string TrackingId { get; set; }
            public int Tipo_Motivo { get; set; }
        }

        [WebMethod]
        public static Object save(ContratoAux contrato)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
                {
                    var con = new Entity.MotivoDetencion();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = contrato.Id;
                    if (id == "") id = "0";

                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    var duplicado = ControlMotivoDetencion.ObtenerTodos();
                    
                    foreach(var k in duplicado)
                    {
                        if(k.Motivo.ToUpper() == contrato.Motivo.ToUpper() && id != k.Id.ToString() && k.ContratoId == contratoUsuario.IdContrato && k.Tipo == contratoUsuario.Tipo)
                            throw new Exception(string.Concat("Al parecer ya existe un registro nombrado ", k.Motivo, ". Verifique la información y vuelva a intentarlo."));
                    }

                    con.ContratoId = contratoUsuario.IdContrato;
                    con.Tipo = contratoUsuario.Tipo;

                    if (string.IsNullOrEmpty(contrato.Id))
                    {
                        mode = @"registró";


                        // con.Id = Convert.ToInt32(contrato.Id);
                        con.CreadoPor = usId;
                        con.Motivo = contrato.Motivo;
                        con.TrackingId = Guid.NewGuid();
                        con.Articulo = contrato.Articulo;
                        con.MultaMinimo = Convert.ToDecimal(contrato.MultaMinimo);
                        con.MultaMaximo = Convert.ToDecimal(contrato.MultaMaximo);
                        con.HorasdeArresto = Convert.ToInt32(contrato.horaArresto);
                        con.Descripcion = contrato.Descripcion;
                        con.Activo = 1;
                        con.Habilitado = 1;
                        con.Tipo_Motivo = contrato.Tipo_Motivo;
                        
                      


                        int idContrato = ControlMotivoDetencion.Guardar(con);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de motivo de detención";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);

                    }
                    else
                    {
                        mode = @"actualizó";
                        con.Id = Convert.ToInt32(contrato.Id);
                        con.CreadoPor = usId;
                        con.Motivo = contrato.Motivo;
                        con.TrackingId = new Guid(contrato.TrackingId);
                        con.Articulo = contrato.Articulo;
                        con.MultaMinimo = Convert.ToDecimal(contrato.MultaMinimo);
                        con.MultaMaximo = Convert.ToDecimal(contrato.MultaMaximo);
                        con.HorasdeArresto = Convert.ToInt32(contrato.horaArresto);
                        con.Descripcion = contrato.Descripcion;
                        con.Activo = 1;
                        con.Habilitado = 1;
                        con.Tipo_Motivo = contrato.Tipo_Motivo;
                        ControlMotivoDetencion.Actualizar(con);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de motivo de detención";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    return new { exitoso = true, mensaje = mode, Id = con.Id.ToString(), TrackingId = con.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static Object getContract(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    var contract = ControlMotivoDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                    ContratoEditAux obj = new ContratoEditAux();

                    if (contract != null)
                    {
                        obj.Id = contract.Id.ToString();
                        obj.Motivo = contract.Motivo.ToString();
                        obj.Descripcion = contract.Descripcion;
                        obj.Articulo = contract.Articulo.ToString();
                        obj.MultaMinimo = contract.MultaMinimo.ToString();
                        obj.MultaMaximo = contract.MultaMaximo.ToString();
                        obj.horaArresto = contract.HorasdeArresto.ToString();
                        obj.TrackingId = contract.TrackingId.ToString();
                    }
                    return obj;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }
    }
}