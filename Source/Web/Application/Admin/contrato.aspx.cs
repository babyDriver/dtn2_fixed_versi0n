﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Admin
{
    public partial class contrato : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Configuración y seguridad" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static DataTable getContracts(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string clienteId)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        where.Add(new Where("C.Activo", "1"));
                        if(!string.IsNullOrEmpty(clienteId) && !(clienteId == "0"))
                        {
                            where.Add(new Where("C.ClienteId", clienteId));
                        }                            
                        Query query = new Query
                        {
                            select = new List<string>{
                        "C.Id",
                        "C.TrackingId",
                        "C.ClienteId",
                        "CL.Cliente",
                        "C.Contrato",
                        "CR.Nombre",
                        "C.VigenciaInicial",
                        "C.VigenciaFinal",
                        "C.Activo",
                        "C.Habilitado"
                        },
                            from = new DT.Table("contrato", "C"),
                            joins = new List<Join> {
                                new Join(new DT.Table("Cliente", "CL"), "CL.Id = C.ClienteId", "INNER"),
                                new Join(new DT.Table("institucion", "CR"), "CR.Id = C.InstitucionId", "INNER")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static DataTable getSubContracts(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string clienteId)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        where.Add(new Where("S.Activo", "1"));
                        if (!string.IsNullOrEmpty(clienteId) && !(clienteId == "0"))
                        {
                            where.Add(new Where("C.ClienteId", clienteId));
                        }
                        Query query = new Query
                        {
                            select = new List<string>{
                            "S.Id",
                            "S.TrackingId",
                            "C.ClienteId",
                            "CL.Cliente",
                            "C.Contrato",
                            "CR.Nombre",
                            "S.Subcontrato",
                            "S.VigenciaInicial",
                            "S.VigenciaFinal",
                            "S.Activo",
                            "S.Habilitado"
                        },
                            from = new DT.Table("subcontrato", "S"),
                            joins = new List<Join> {
                                new Join(new DT.Table("Contrato", "C"), "C.Id = S.ContratoId", "INNER"),
                                new Join(new DT.Table("Cliente", "CL"), "CL.Id = C.ClienteId", "INNER"),
                                new Join(new DT.Table("institucion", "CR"), "CR.Id = C.InstitucionId", "INNER")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static List<Combo> getCustomers()
        {
            var combo = new List<Combo>();
            var c = ControlCliente.ObtenerTodos();

            foreach (var item in c.Where(x=>x.Habilitado))
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string blockContract(string trackingid)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Eliminar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var contratoAux = ControlContrato.ObtenerPorTrackingId(new Guid(trackingid));
                    contratoAux.Habilitado = (contratoAux.Habilitado) ? false : true;
                    contratoAux.Activo = true;
                    ControlContrato.Actualizar(contratoAux);
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    string message = "";
                    message = contratoAux.Habilitado ? "habilitado" : "deshabilitado";
                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    var movimiento = contratoAux.Habilitado ? "Contrato desbloqueado" : "Contrato bloqueado";
                    historial.Movimiento = movimiento;
                    historial.CreadoPor = usId;

                    historial.ContratoId = contratoUsuario.IdContrato;
                    historial.Id = ControlHistorial.Guardar(historial);
                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, message });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockSubcontract(string trackingid)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Eliminar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());                    
                    var subcontratoAux = ControlSubcontrato.ObtenerPorTrackingId(new Guid(trackingid));
                    subcontratoAux.Activo = true;
                    subcontratoAux.Habilitado = (subcontratoAux.Habilitado) ? false : true;
                    ControlSubcontrato.Actualizar(subcontratoAux);
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    string message = "";
                    message = subcontratoAux.Habilitado ? "desbloqueó" : "bloqueó";
                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    var movimiento = subcontratoAux.Habilitado ? "Subcontrato desbloqueado" : "Subcontrato bloqueado";
                    historial.Movimiento = movimiento;
                    historial.CreadoPor = usId;

                    historial.ContratoId = contratoUsuario.IdContrato;
                    historial.Id = ControlHistorial.Guardar(historial);
                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, message });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}