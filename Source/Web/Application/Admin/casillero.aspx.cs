﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Admin
{
    public partial class casillero : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }

        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                var mode = string.Empty;
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var item = new Entity.Casillero();
                item.Nombre = datos[2].ToString();
                item.Capacidad = Convert.ToInt32(datos[3]);
                item.Activo = true;
                item.Descripcion = datos[4].ToString(); ;

               
                string id = datos[1].ToString();
                Guid trackingid;
                if (!string.IsNullOrEmpty(id))
                {
                    if (!Guid.TryParse(id, out trackingid))
                    {
                        throw new Exception("No se logró obtener el identificador del registro");
                    }
                }
                else
                {
                    trackingid = Guid.Empty;
                }
                
                Entity.Casillero duplicado = ControlCasillero.ObtenerPorNombre(item.Nombre);
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                if (id == "") trackingid = Guid.Empty;
                if (duplicado != null && duplicado.TrackingId != trackingid && duplicado.ContratoId == contratoUsuario.IdContrato && duplicado.Tipo == contratoUsuario.Tipo)
                    throw new Exception(string.Concat("Al parecer ya existe un registro nombrado ", duplicado.Nombre, ". Verifique la información y vuelva a intentarlo"));

                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");
                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                item.ContratoId = contratoUsuario.IdContrato;
                item.Tipo = contratoUsuario.Tipo;

                if (!string.IsNullOrEmpty(datos[1].ToString()))
                {
                    mode = "actualizó";
                    item.Id = Convert.ToInt32(datos[0]);
                    Entity.Casillero casillero1 = ControlCasillero.ObtenerPorId(item.Id);

                    item.TrackingId = trackingid;
                    Entity.Casillero _celda = ControlCasillero.ObtenerPorId(Convert.ToInt32(datos[0]));
                    item.Disponible = _celda.Disponible;
                    item.Activo = _celda.Activo;
                    item.Habilitado = casillero1.Habilitado;
                    ControlCasillero.Actualizar(item);

                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    historial.Movimiento = "Modificación de casillero";
                    historial.CreadoPor = usuario;

                    historial.ContratoId = subcontrato.Id;
                    historial.Id = ControlHistorial.Guardar(historial);
                }
                else
                {
                    item.Activo = true;
                    mode = "registró";
                    item.TrackingId = Guid.NewGuid();
                    item.Creadopor = usId;
                    item.Id = ControlCasillero.Guardar(item);
                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    historial.Movimiento = "Registro de casillero";
                    historial.CreadoPor = usuario;

                    historial.ContratoId = subcontrato.Id;
                    historial.Id = ControlHistorial.Guardar(historial);
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = item.Id, TrackingId = item.TrackingId });
                
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitem(int id)
        {
            try
            {
                //  if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                // {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                Entity.Casillero celda = ControlCasillero.ObtenerPorId(id);
                celda.Activo = celda.Activo ? false : true;
                if (celda.Activo)
                {
                    celda.Habilitado = 1;
                }
                else
                {
                    celda.Habilitado = 0;
                }
                string mensaje = celda.Activo ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                ControlCasillero.Actualizar(celda);
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        
        [WebMethod]
        public static DataTable GetCasilleroLog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
            using (mysqlconection)
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                    {
                        if (!emptytable)
                        {
                            //MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);


                            List<Where> where = new List<Where>();
                            List<Order> orderby = new List<Order>();
                            if (Convert.ToInt32(centroid) != 0)
                            {
                                where.Add(new Where("A.Id", centroid.ToString()));
                            }
                            if (Convert.ToBoolean(todoscancelados))
                            {
                                where.Add(new Where("C.Activo", "0"));
                            }
                            where.Add(new Where("B.RolId", "<>", "13"));
                            Query query = new Query
                            {
                                select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Nombre",
                                "A.Capacidad",
                                "A.Disponible",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                                from = new Table("casillerolog", "A"),
                                joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("casillero","C"),"C.Id=A.Id"),

                            },
                                wheres = where

                            };
                            DataTables dt = new DataTables(mysqlconection);
                            DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                            where = new List<Where>();
                            if (Convert.ToInt32(centroid) != 0)
                            {
                                where.Add(new Where("A.Id", centroid.ToString()));
                            }
                            if (Convert.ToBoolean(todoscancelados))
                            {
                                where.Add(new Where("C.Activo", "0"));
                            }
                            where.Add(new Where("B.RolId", "=", "13"));
                            where.Add(new Where("A.Accion", "=", "Registrado"));
                            query = new Query
                            {
                                select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Nombre",
                                "A.Capacidad",
                                "A.Disponible",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                                from = new Table("casillerolog", "A"),
                                joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("casillero","C"),"C.Id=A.Id"),

                            },
                                wheres = where

                            };
                            DataTables dt2 = new DataTables(mysqlconection);
                            DataTable _dt2 = dt2.Generar(query, draw, start, length, search, order, columns);

                            if (_dt2.data.Count > 0)
                            {
                                where = new List<Where>();

                                if (Convert.ToBoolean(todoscancelados))
                                {
                                    where.Add(new Where("C.Activo", "0"));
                                }
                                where.Add(new Where("B.RolId", "<>", "13"));

                                query = new Query
                                {
                                    select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Nombre",
                                "A.Capacidad",
                                "A.Disponible",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                                    from = new Table("casillerolog", "A"),
                                    joins = new List<Join>
                            {
                                new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("casillero","C"),"C.Id=A.Id"),

                            },
                                    wheres = where

                                };
                                DataTables dt3 = new DataTables(mysqlconection);
                                DataTable _dt3 = dt3.Generar(query, draw, start, length, search, order, columns);

                                List<object> listaoriginal = new List<object>();
                                List<object> listaNueva = new List<object>();
                                Dictionary<string, object> valuePairs = new Dictionary<string, object>();
                                Dictionary<string, object> valuePairsnew = new Dictionary<string, object>();
                                listaoriginal = _dt2.data;
                                listaNueva = _dt3.data;

                                foreach (var item in listaoriginal)
                                {
                                    valuePairs = (Dictionary<string, object>)item;
                                }

                                foreach (var item in listaNueva)
                                {
                                    valuePairsnew = (Dictionary<string, object>)item;
                                }
                                valuePairs["Creadopor"] = valuePairsnew["Creadopor"];

                                _dt2.data = listaoriginal;
                                _dt.data.Add(_dt2.data[0]);

                            }

                            return _dt;




                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                    }

                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }

                finally
                {
                    if (mysqlconection.State == System.Data.ConnectionState.Open)
                        mysqlconection.Close();
                }
            }
           

        }
        [WebMethod]
        public static DataTable getcasillero(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    // if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                    //{
                    if (!emptytable)
                    {

                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        List<Where> where = new List<Where>();

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        where.Add(new Where("S.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("S.Tipo", contratoUsuario.Tipo));
                        Query query = new Query
                        {
                            select = new List<string>{
                                "S.Id",
                                "S.Nombre",
                                "S.Capacidad",
                                "S.Descripcion",
                                "S.Activo",
                                "S.TrackingId"
                            },
                            from = new Table("casillero", "S"),
                            wheres = where,
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        //return dt.Generar(query, draw, start, length, search, order, columns);
                        var table = dt.Generar(query, draw, start, length, search, order, columns);
                        foreach (var item in table.data)
                        {
                            var dictionary = (Dictionary<string, object>)item;
                            var nombre = dictionary["Nombre"].ToString();
                            if (nombre.Contains("<script>"))
                                dictionary["Nombre"] = nombre.Replace("<script>", "");

                            var descripcion = dictionary["Descripcion"].ToString();
                            if (descripcion.Contains("<script>"))
                                dictionary["Descripcion"] = descripcion.Replace("<script>", "");
                        }
                        return table;
                        //}
                        //else
                        //{
                        //    return DataTables.ObtenerDataTableVacia(null, draw);
                        //}
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
                finally
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                        mysqlConnection.Close();
                }

            }
        }
        
    }
}