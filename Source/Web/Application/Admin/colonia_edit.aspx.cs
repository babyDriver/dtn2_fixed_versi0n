﻿using Business;
using Entity.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Admin
{
    public partial class colonia_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static List<Combo> getCountries()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.pais));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getStates(string idPais)
        {
            List<Combo> combo = new List<Combo>();
            var data = ControlEstado.ObtenerPorPais(Convert.ToInt32(idPais));            

            if (data.Count > 0)
            {
                foreach (var rol in data)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCities(string idEstado)
        {
            List<Combo> combo = new List<Combo>();
            var data = ControlMunicipio.ObtenerPorEstado(Convert.ToInt32(idEstado));

            if (data.Count > 0)
            {
                foreach (var rol in data)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static Object getNeighborhood(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {                    
                    var colonia = ControlColonia.ObtenerPorTrackingId(new Guid(trackingid));
                    ColoniaEditAux coloniaAux = new ColoniaEditAux();

                    var municipio = ControlMunicipio.Obtener(colonia.IdMunicipio);
                    var estado = ControlEstado.Obtener(municipio.EstadoId);
                    var pais = ControlCatalogo.Obtener(estado.IdPais, 45);

                    if (colonia != null)
                    {
                        coloniaAux.ClaveOficina = colonia.ClaveDeOficina;
                        coloniaAux.CodigoPostal = colonia.CodigoPostal;
                        coloniaAux.Colonia = colonia.Asentamiento;
                        coloniaAux.Estado = colonia.Estado;
                        coloniaAux.Id = colonia.Id.ToString();
                        coloniaAux.IdMunicipio = colonia.IdMunicipio.ToString();
                        coloniaAux.Municipio = colonia.Municipio;
                        coloniaAux.TipoAsentamiento = colonia.TipoDeAsentamiento;
                        coloniaAux.TrackingId = colonia.TrackingId.ToString();
                        coloniaAux.IdEstado = estado.Id.ToString();
                        coloniaAux.IdPais = pais.Id.ToString();
                    }
                    return coloniaAux;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static Object save(ColoniaAux colonia)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
                {                    
                    var coloniaNueva = new Entity.Colonia();
                    
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = colonia.Id;
                    if (id == "") id = "0";

                    //Generar SP para checar duplicidad
                    //object[] data = new object[] { cliente.Nombre, cliente.Rfc };

                    //var cliente_duplicado = ControlCliente.ObtenerPorNombreYRfc(data);
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    object[] data = new object[] { Convert.ToInt32(colonia.IdMunicipio), colonia.CodigoPostal, colonia.Colonia };
                    //f1x3d
                    //var colonia_duplicada = ControlColonia.ObtenerPorMunicipioCodigoColonia(data);

                    //if(colonia_duplicada != null && colonia_duplicada.Id != Convert.ToInt32(id))
                    //{
                    //    throw new Exception(string.Concat("La colonia: ", colonia_duplicada.Asentamiento, " ya está registrada favor de registrar otra."));
                    //}

                    //if (cliente_duplicado != null && cliente_duplicado.Id != Convert.ToInt32(id))
                    //    throw new Exception(string.Concat("El nombre de cliente: ", cliente_duplicado.Nombre, " ya está registrado favor de cambiarlo por otro."));
                    /*
                    var email_duplicado = ControlUsuario.ObtenerPorEmail(usuario.Email);

                    if (email_duplicado != null && email_duplicado.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("El correo: ", email_duplicado.Email, " ya está registrado favor de cambiarlo por otro."));
                        */

                    if (string.IsNullOrEmpty(colonia.Id))
                    {
                        mode = @"registró";
                        //var tmpmail = String.Concat(cliente.Nombre, "@test.com");
                        //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

                        coloniaNueva.Activo = true;
                        coloniaNueva.Asentamiento = colonia.Colonia;
                        coloniaNueva.ClaveDeOficina = colonia.ClaveOficina;
                        coloniaNueva.CodigoPostal = colonia.CodigoPostal;
                        coloniaNueva.CreadoPor = usId;
                        coloniaNueva.Estado = colonia.Estado;
                        coloniaNueva.Habilitado = true;
                        coloniaNueva.IdMunicipio = Convert.ToInt32(colonia.IdMunicipio);
                        coloniaNueva.Municipio = colonia.Municipio;
                        coloniaNueva.TipoDeAsentamiento = colonia.TipoAsentamiento;
                        coloniaNueva.TrackingId = Guid.NewGuid();

                        coloniaNueva.Id = ControlColonia.Guardar(coloniaNueva);

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de colonia";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {
                        mode = @"actualizó";

                        coloniaNueva.Activo = true;
                        coloniaNueva.Asentamiento = colonia.Colonia;
                        coloniaNueva.ClaveDeOficina = colonia.ClaveOficina;
                        coloniaNueva.CodigoPostal = colonia.CodigoPostal;
                        coloniaNueva.CreadoPor = usId;
                        coloniaNueva.Estado = colonia.Estado;
                        coloniaNueva.Habilitado = true;
                        coloniaNueva.IdMunicipio = Convert.ToInt32(colonia.IdMunicipio);
                        coloniaNueva.Municipio = colonia.Municipio;
                        coloniaNueva.TipoDeAsentamiento = colonia.TipoAsentamiento;
                        coloniaNueva.TrackingId = new Guid(colonia.TrackingId);
                        coloniaNueva.Id = Convert.ToInt32(colonia.Id);

                        ControlColonia.Actualizar(coloniaNueva);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de colonia";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    return new { exitoso = true, mensaje = mode, Id = coloniaNueva.Id.ToString(), TrackingId = coloniaNueva.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
    }

    public class ColoniaAux
    {
        public string Id { get; set; }
        public string IdMunicipio { get; set; }
        public string CodigoPostal { get; set; }
        public string Colonia { get; set; }
        public string TipoAsentamiento { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string ClaveOficina { get; set; }    
        public string TrackingId { get; set; }
    }    

    public class ColoniaEditAux
    {
        public string Id { get; set; }
        public string IdMunicipio { get; set; }
        public string CodigoPostal { get; set; }
        public string Colonia { get; set; }
        public string TipoAsentamiento { get; set; }
        public string Municipio { get; set; }
        public string Estado { get; set; }
        public string ClaveOficina { get; set; }
        public string TrackingId { get; set; }
        public string IdEstado { get; set; }
        public string IdPais { get; set; }
    }
}