﻿using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application
{
    public partial class MotivoEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }


        public class ContratoAux
        {
            public string Id { get; set; }
            public string Motivo { get; set; }
            public string Descripcion { get; set; }
            public string Articulo { get; set; }
            public string Multa { get; set; }
            public string horaArresto { get; set; }
            public string TrackingId { get; set; }

        }


        public class ContratoEditAux
        {
            public string Id { get; set; }
            public string Motivo { get; set; }
            public string Descripcion { get; set; }
            public string Articulo { get; set; }
            public string Multa { get; set; }
            public string horaArresto { get; set; }
            public string TrackingId { get; set; }
        }

        [WebMethod]
        public static Object save(ContratoAux contrato)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
                {
                    var con = new Entity.MotivoDetencion();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = contrato.Id;
                    if (id == "") id = "0";

                    //object[] data = new object[] { contrato.Id, contrato.Motivo };
                    //var contrato_duplicado = ControlContrato.ObtenerPorClienteYContrato(data);


                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    if (string.IsNullOrEmpty(contrato.Id))
                    {
                        mode = @"registró";
                       

                       // con.Id = Convert.ToInt32(contrato.Id);
                        con.CreadoPor = usId;
                        con.Motivo = contrato.Motivo;
                        con.TrackingId = Guid.NewGuid();
                        con.Articulo = contrato.Articulo;
                        //con.Multa = Convert.ToDecimal(contrato.Multa);
                        con.HorasdeArresto = Convert.ToInt32(contrato.horaArresto);
                        con.Descripcion = contrato.Descripcion;
                        con.Activo = 1;
                        con.Habilitado = 1;

                       
                        int idContrato = ControlMotivoDetencion.Guardar(con);

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de motivo de detención";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);

                    }
                    else
                    {
                        mode = @"actualizó";
                        con.Id = Convert.ToInt32(contrato.Id);
                        con.CreadoPor = usId;
                        con.Motivo = contrato.Motivo;
                        con.TrackingId = new Guid(contrato.TrackingId);
                        con.Articulo = contrato.Articulo;
                        //con.Multa = Convert.ToDecimal(contrato.Multa);
                        con.HorasdeArresto = Convert.ToInt32(contrato.horaArresto);
                        con.Descripcion = contrato.Descripcion;
                        con.Activo = 1;
                        con.Habilitado = 1;
                        ControlMotivoDetencion.Actualizar(con);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de motivo de detención";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                        //con.Activo = true;
                        //con.ClienteId = Convert.ToInt32(contrato.ClienteId);
                        //con.Id = Convert.ToInt32(contrato.Id);
                        //con.Nombre = contrato.Nombre;
                        //con.VigenciaInicial = Convert.ToDateTime(contrato.VigenciaInicial);
                        //con.VigenciaFinal = Convert.ToDateTime(contrato.VigenciaFinal);
                        //ControlContrato.Actualizar(con);
                        //Entity.Accion accion = new Entity.Accion();
                        //accion.CreadoPor = usId;
                        //accion.Descripcion = "Contrato actualizado";
                        //accion.Fecha = DateTime.Now;
                        //accion.Nombre = "Actualizar";
                        //accion.TrackingId = Guid.NewGuid();
                        //int idAccion = ControlAccion.Guardar(accion);
                    }

                    return new { exitoso = true, mensaje = mode, Id = con.Id.ToString(), TrackingId = con.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static Object getContract(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    var contract = ControlMotivoDetencion.ObtenerPorTrackingId(new Guid(trackingid));
                    ContratoEditAux obj = new ContratoEditAux();

                    if (contract != null)
                    {
                        obj.Id = contract.Id.ToString();
                        obj.Motivo = contract.Motivo.ToString();
                        obj.Descripcion = contract.Descripcion;
                        obj.Articulo = contract.Articulo.ToString();
                        //obj.Multa = contract.Multa.ToString();
                        obj.horaArresto = contract.HorasdeArresto.ToString();
                        obj.TrackingId = contract.TrackingId.ToString();
                    }
                    return obj;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

    }
}