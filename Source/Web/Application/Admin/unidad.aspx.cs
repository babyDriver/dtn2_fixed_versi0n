﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using Entity.Util;
using System.Linq;

namespace Web.Application.Admin
{
    public partial class unidad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }


        //Metodo cambio de filtro unidad  sin contrato
        [WebMethod]
        public static List<Combo> _getcorporacion()
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.corporacion));


            //if (catalogo.Count > 0)
            //{
            //    //foreach (var rol in catalogo.Where(ratoId == contratoUsuario.IdContrato))
            //    foreach (var rol in catalogo)
            //        combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            //}

            catalogo = ControlCatalogo.ObtenerTodo(Convert.ToInt32(Entity.TipoDeCatalogo.corporacion));
            foreach(var item in catalogo.Where(x=> x.Activo && x.Habilitado))
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }
            return combo;

        }

        [WebMethod]
        public static List<Combo> getcorporacion()
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.corporacion));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo.Where(x => x.ContratoId == contratoUsuario.IdContrato))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }
        [WebMethod]
        public static DataTable getcatalogo(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
          

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        List<Where> where = new List<Where>();
                        List<Where> where2 = new List<Where>();
                       
                        where.Add(new Where("C.Activo", "1"));
                        where2.Add(new Where("C.Activo", "1"));
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));


                        //Cambio por filtro se eliminan los where si se desea no filtrar por contrato
                        where.Add(new Where("C.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("C.Tipo", contratoUsuario.Tipo));
                        Query query = new Query
                        {
                            select = new List<string>{
                            "C.Id",
                            "C.TrackingId",
                            "C.Nombre",
                            "C.Descripcion",
                            "C.Habilitado",
                            "ifnull(C.Placa,'') Placa",
                            "ifnull(u.nombre,'') Corporacion",
                            "ifnull(u.Id,0) CorporacionId"

                        },
                            from = new Table("Unidad", "C"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Corporacion", "U"), "U.Id = C.CorporacionId", "LEFT"),
                               // new Join(new Table("contratousuario", "cu"), "cu.IdUsuario = U.Id", "inner")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt= dt.Generar(query, draw, start, length, search, order, columns);
                        var table = dt.Generar(query, draw, start, length, search, order, columns);
                        foreach (var item in table.data)
                        {
                            var dictionary = (Dictionary<string, object>)item;
                            var nombre = dictionary["Nombre"].ToString();
                            if (nombre.Contains("<script>"))
                                dictionary["Nombre"] = nombre.Replace("<script>", "");

                            var descripcion = dictionary["Descripcion"].ToString();
                            if (descripcion.Contains("<script>"))
                                dictionary["Descripcion"] = descripcion.Replace("<script>", "");
                        }
                        return table;
                        
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static Object save(UnidadAux _unidad)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
                {
                    var con = new Entity.Unidad();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = _unidad.Id;
                    if (id == "") id = "0";

                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    //var duplicado = ControlMotivoDetencion.ObtenerTodos();

                    //foreach (var k in duplicado)
                    //{
                    //    if (k.Motivo.ToUpper() == _unidad.Motivo.ToUpper() && id != k.Id.ToString() && k.ContratoId == contratoUsuario.IdContrato && k.Tipo == contratoUsuario.Tipo)
                    //        throw new Exception(string.Concat("Al parecer ya existe un registro nombrado ", k.Motivo, ". Verifique la información y vuelva a intentarlo."));
                    //}

                    con.ContratoId = contratoUsuario.IdContrato;
                    con.Tipo = contratoUsuario.Tipo;

                    if (string.IsNullOrEmpty(_unidad.Id))
                    {
                        mode = @"registró";


                        // con.Id = Convert.ToInt32(contrato.Id);
                        con.Creadopor = usId;
                        con.Nombre = _unidad.Nombre;
                        con.Descripcion = _unidad.Descripcion;
                        con.Placa = _unidad.Placa;
                        con.CorporacionId = Convert.ToInt32(_unidad.CorporacionId);
                       
                        con.TrackingId = Guid.NewGuid().ToString();
                       
                        con.Activo = true;
                        con.Habilitado = true;


                        int idContrato = ControlUnidad.Guardar(con);


                    }
                    else
                    {
                        mode = @"actualizó";
                        con.Id = Convert.ToInt32(Convert.ToInt32(_unidad.Id));
                        con.TrackingId = _unidad.TrackingId;
                        con.Creadopor = usId;
                        con.Activo = true;
                        con.Habilitado = true;
                        con.Nombre = _unidad.Nombre;
                        con.Descripcion = _unidad.Descripcion;
                        con.Placa = _unidad.Placa;
                        con.CorporacionId = Convert.ToInt32(_unidad.CorporacionId);
                        ControlUnidad.Actualizar(con);
                    }

                    return new { exitoso = true, mensaje = mode, Id = con.Id.ToString(), TrackingId = con.TrackingId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        public static Entity.TipoDeCatalogo getTipoCatalogo(string tipo)
        {
            Entity.TipoDeCatalogo catalogo = new Entity.TipoDeCatalogo();
            switch (tipo)
            {

                case "unidad":
                    catalogo = Entity.TipoDeCatalogo.unidad; break;

            }
            return catalogo;
        }

        [WebMethod]
        public static string blockitem(string trackingId)
        {
            try
            {
                string tipocatalogo = "unidad";
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {
                    Entity.Catalogo catalogo = ControlCatalogo.Obtener(new Guid(trackingId), Convert.ToInt32(getTipoCatalogo(tipocatalogo)));
                    catalogo.tipo = getTipoCatalogo(tipocatalogo);
                    catalogo.Habilitado = catalogo.Habilitado ? false : true;
                    string msg = catalogo.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");


                    catalogo.Creadopor = usuario;
                    ControlCatalogo.Actualizar(catalogo);
                    return JsonConvert.SerializeObject(new { exitoso = true, msg });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
    public class UnidadAux
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Placa { get; set; }
        public string CorporacionId { get; set; }

    }
}