﻿using Business;
using Entity.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Admin
{
    public partial class subcontrato_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static List<Combo> getContracts()
        {
            var combo = new List<Combo>();
            var c = ControlContrato.ObtenerTodos();

            foreach (var item in c)
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        //[WebMethod]
        //public static Object save(SubcontratoEditAux subcontrato)
        //{
        //    try
        //    {
        //        if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
        //        {
        //            var subcontratoNuevo = new Entity.Subcontrato();
        //            var mode = string.Empty;
        //            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

        //            string id = subcontrato.Id.ToString();
        //            if (id == "0") id = "";

        //            //object[] data = new object[] { contrato.ClienteId, contrato.Nombre };
        //            //var contrato_duplicado = ControlContrato.ObtenerPorClienteYContrato(data);

        //            //if (contrato_duplicado != null && contrato_duplicado.Id != Convert.ToInt32(id))
        //            //    throw new Exception(string.Concat("El nombre del contrato: ", contrato_duplicado.Nombre, " ya está registrado favor de cambiarlo por otro."));
        //            /*
        //            var email_duplicado = ControlUsuario.ObtenerPorEmail(usuario.Email);

        //            if (email_duplicado != null && email_duplicado.Id != Convert.ToInt32(id))
        //                throw new Exception(string.Concat("El correo: ", email_duplicado.Email, " ya está registrado favor de cambiarlo por otro."));
        //                */

        //            DateTime fechaInicial;
        //            DateTime fechaFinal;

        //            try
        //            {
        //                fechaInicial = Convert.ToDateTime(subcontrato.VigenciaInicial);
        //            }
        //            catch (Exception)
        //            {
        //                throw new Exception(string.Concat("El valor de la vigencia inicial tiene un formato incorrecto."));
        //            }

        //            try
        //            {
        //                fechaFinal = Convert.ToDateTime(subcontrato.VigenciaFinal);
        //            }
        //            catch (Exception)
        //            {
        //                throw new Exception(string.Concat("El valor de las vigencia final tiene un formato incorrecto."));
        //            }

        //            if (fechaInicial > fechaFinal)
        //            {
        //                throw new Exception(string.Concat("La vigencia inicial no puede ser mayor a la vigencia final."));
        //            }

        //            subcontratoNuevo.ContratoId = Convert.ToInt32(subcontrato.ContratoId);                                        
        //            subcontratoNuevo.InstitucionId = Convert.ToInt32(subcontrato.InstitucionId);
        //            subcontratoNuevo.Nombre = subcontrato.Nombre;                    
        //            subcontratoNuevo.VigenciaFinal = Convert.ToDateTime(subcontrato.VigenciaFinal);
        //            subcontratoNuevo.VigenciaInicial = Convert.ToDateTime(subcontrato.VigenciaInicial);

        //            if (string.IsNullOrEmpty(id))
        //            {
        //                mode = @"registró";
        //                ///var tmpmail = String.Concat(contrato.Nombre, "@test.com");
        //                //user.Id = Convert.ToInt32(Membership.CreateUser(usuario.User, usuario.Password, usuario.Email).ProviderUserKey.ToString());

        //                subcontratoNuevo.Activo = true;                        
        //                subcontratoNuevo.CreadoPor = usId;
        //                subcontratoNuevo.Habilitado = true;                                                
        //                subcontratoNuevo.TrackingId = Guid.NewGuid();                        
        //                subcontratoNuevo.Id = ControlSubcontrato.Guardar(subcontratoNuevo);
        //            }
        //            else
        //            {
        //                mode = @"actualizó";
        //                subcontratoNuevo.Id = Convert.ToInt32(subcontrato.Id);
        //                subcontratoNuevo.Activo = true;
        //                subcontratoNuevo.Habilitado = true;
        //                subcontratoNuevo.TrackingId = new Guid(subcontrato.TrackingId);
        //                ControlSubcontrato.Actualizar(subcontratoNuevo);                        
        //            }

        //            return new { exitoso = true, mensaje = mode, Id = subcontratoNuevo.Id.ToString(), TrackingId = subcontrato.TrackingId.ToString() };
        //        }
        //        else
        //        {
        //            return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
        //    }
        //}

        //[WebMethod]
        //public static Object getSubcontract(string trackingid)
        //{
        //    try
        //    {
        //        if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
        //        {
        //            //var contract = ControlContrato.ObtenerPorTrackingId(new Guid(trackingid));
        //            var subcontract = ControlSubcontrato.ObtenerPorTrackingId(new Guid(trackingid));
        //            SubcontratoAux obj = new SubcontratoAux();

        //            if (subcontract != null)
        //            {
        //                obj.Id = subcontract.Id;
        //                obj.ContratoId = subcontract.ContratoId;
        //                obj.Nombre = subcontract.Nombre;
        //                obj.TrackingId = subcontract.TrackingId.ToString();
        //                obj.VigenciaFinal = subcontract.VigenciaFinal.Value.ToString("yyyy-MM-dd");
        //                obj.VigenciaInicial = subcontract.VigenciaInicial.ToString("yyyy-MM-dd");
        //                obj.InstitucionId = subcontract.InstitucionId;
        //            }
        //            return obj;
        //        }
        //        else
        //        {
        //            return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new { exitoso = false, mensaje = ex.Message };
        //    }
        //}

        [WebMethod]
        public static List<Combo> getInstitutions(string id)
        {
            var combo = new List<Combo>();

            string val = "";
            if (id != "")
            {
                val = id;
            }
            else
            {
                val = "0";
            }

            var contrato = ControlContrato.ObtenerPorId(Convert.ToInt32(val));
            var institucion = contrato != null ? ControlInstitucion.ObtenerPorId(contrato.InstitucionId) : null;

            if (institucion != null)
            {
                combo.Add(new Combo { Desc = institucion.Nombre, Id = institucion.Id.ToString() });
            }

            //foreach (var item in c)
            //{
            //    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            //}

            return combo;
        }
    }

    //public class SubcontratoAux
    //{
    //    public int Id { get; set; }             
    //    public int ContratoId { get; set; }
    //    public string Nombre { get; set; }
    //    public string TrackingId { get; set; }
    //    public string VigenciaInicial { get; set; }
    //    public string VigenciaFinal { get; set; }
    //    public int InstitucionId { get; set; }
    //}

    //public class SubcontratoEditAux
    //{
    //    public string Id { get; set; }
    //    public string TrackingId { get; set; }
    //    public string ContratoId { get; set; }
    //    public string Nombre { get; set; }
    //    public string VigenciaInicial { get; set; }
    //    public string VigenciaFinal { get; set; }
    //    public string InstitucionId { get; set; }
    //}
}