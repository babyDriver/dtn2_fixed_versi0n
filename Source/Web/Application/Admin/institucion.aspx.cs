﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Admin
{
    public partial class institucion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
                {
                    var mode = string.Empty;

                    var item = new Entity.Institucion();
                    Entity.Domicilio domicilio = new Entity.Domicilio();

                    item.Nombre = datos[2].ToString().Trim();
                    item.Telefono = datos[3].ToString();
                    item.Encargado = datos[4].ToString();
                    item.Clave = datos[5].ToString();
                    item.CupoHombres = Convert.ToInt32(datos[7].ToString());
                    item.CupoMujeres = Convert.ToInt32(datos[6].ToString());

                    domicilio.PaisId = Convert.ToInt32(datos[8].ToString());
                    if (domicilio.PaisId == 73)
                    {
                        domicilio.EstadoId = Convert.ToInt32(datos[9].ToString());
                        domicilio.MunicipioId = Convert.ToInt32(datos[10] != null ? datos[10].ToString() : "0");
                        domicilio.ColoniaId = Convert.ToInt32(datos[11] != null ? datos[11].ToString() : "0");
                    }
                    else
                    {
                        domicilio.Localidad = datos[15].ToString();

                    }
                    domicilio.Calle = datos[12].ToString();
                    domicilio.Telefono = datos[3].ToString();
                    

                    item.Activo = true;
                    item.Estado = ControlEstado.Obtener(Convert.ToInt32(datos[9].ToString()));
                    
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema.");


                    item.Creadopor = usuario;

                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    item.ContratoId = contratoUsuario.IdContrato;
                    item.Tipo = contratoUsuario.Tipo;
                    Entity.Institucion duplicado = ControlInstitucion.ObtenerPorNombre(item.Nombre);
                    string id = datos[0].ToString();
                    if (id == "") id = "0";
                    //if (duplicado != null && duplicado.Id != Convert.ToInt32(id) && duplicado.ContratoId == contratoUsuario.IdContrato && duplicado.Tipo == contratoUsuario.Tipo)
                    if (duplicado != null && duplicado.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("Al parecer ya existe un registro nombrado ", duplicado.Nombre.ToUpper(), ". Verifique la información y vuelva a intentarlo."));


                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {

                        mode = "actualizó";

                        if (!string.IsNullOrEmpty(datos[14].ToString()))
                        {
                            domicilio.Id = Convert.ToInt32(datos[14]);
                            domicilio.TrackingId = new Guid(datos[13]);
                            ControlDomicilio.Actualizar(domicilio);
                        }
                        else
                        {
                            domicilio.TrackingId = Guid.NewGuid();
                            domicilio.Id = ControlDomicilio.Guardar(domicilio);
                        }

                        item.Id = Convert.ToInt32(datos[0]);
                        item.DomicilioId = domicilio.Id;
                        item.TrackingId = new Guid(datos[1]);
                        Entity.Institucion _new = ControlInstitucion.ObtenerPorId(item.Id);
                        item.Habilitado = _new.Habilitado;


                        ControlInstitucion.Actualizar(item);

                       
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de institución";
                        historial.CreadoPor = usuario;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {
                        item.Habilitado = true;
                        mode = "registró";

                        domicilio.TrackingId = Guid.NewGuid();
                        domicilio.Id = ControlDomicilio.Guardar(domicilio);

                        item.TrackingId = Guid.NewGuid();
                        item.DomicilioId = domicilio.Id;
                        item.Id = ControlInstitucion.Guardar(item);

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de institución";
                        historial.CreadoPor = usuario;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = item.Id, TrackingId = item.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitem(string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema.");



                    Entity.Institucion item = ControlInstitucion.ObtenerPorTrackingId(new Guid(tracking));
                    item.Habilitado = item.Habilitado ? false : true;
                    string mensaje = item.Habilitado ? "Registro habilitado con éxito." : "Registro deshabilitado con éxito.";
                    item.Creadopor = usuario;
                    ControlInstitucion.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string delete(int id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {
                    Entity.Institucion item = ControlInstitucion.ObtenerPorId(id); ;
                    item.Activo = false;
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema.");

                    item.Creadopor = usuario;
                    ControlInstitucion.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getListadoCombo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Estado> listado = ControlEstado.ObtenerTodo();

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getPais()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.pais));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getEstado(string paisId)
        {
            var combo = new List<Combo>();
            List<Entity.Estado> listado = ControlEstado.ObtenerPorPais(Convert.ToInt32(paisId));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getMunicipio(string estadoId)
        {
            var combo = new List<Combo>();
                
            List<Entity.Municipio> listado = ControlMunicipio.ObtenerPorEstado(Convert.ToInt32(estadoId));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getNeighborhoods(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();

            var colonias = ControlColonia.ObtenerPorMunicipioId(Convert.ToInt32(idMunicipio));

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias)
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static DataTable getlist(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centro, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        List<Where> where = new List<Where>();

                        if (!string.IsNullOrEmpty(centro))
                        {
                            where.Add(new Where("S.Nombre", string.Concat(" like '%", centro.Trim(), "%'")));
                        }
                        where.Add(new Where("S.Activo", "1"));
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        
                        var usuario = ControlUsuario.Obtener(usId);
                        var roles = ControlRol.ObtenerPorId(usuario.RolId);
                        if (roles.name != "Administrador global")
                        {
                            where.Add(new Where("S.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("S.Tipo", contratoUsuario.Tipo));
                        }
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "S.Id",
                                "S.TrackingId",
                                "S.Nombre",
                                "D.Calle Domicilio",
                                "S.Telefono",
                                "S.Encargado",
                                "S.CupoHombres",
                                "S.CupoMujeres",
                                "S.Habilitado",
                                "S.Clave",
                                "ifnull(E.Nombre,'') Entidad",
                                "D.PaisId",
                                "D.EstadoId",
                                "D.MunicipioId",
                                "D.ColoniaId",
                                "D.TrackingId TrackingIdDomicilio",
                                "D.Id DomicilioId",
                                "ifnull(P.Nombre,'') Pais",
                                "ifnull(M.Nombre,'') Municipio",
                                "ifnull(C.Asentamiento,Localidad) Colonia",
                                "ifnull(D.Localidad,'') Localidad"
                            },
                            from = new Table("institucion", "S"),
                            joins = new List<Join>
                            {                            
                                new Join(new Table("domicilio", "D"), "S.DomicilioId = D.Id"),
                                new Join(new Table("estado","E"), "D.EstadoId = E.Id"),
                                new Join(new Table("usuario", "U"), "U.Id = S.CreadoPor", "inner"),
                                //new Join(new Table("contratousuario", "cu"), "cu.IdUsuario = U.Id", "inner"),
                                new Join(new Table("pais", "P"), "D.PaisId = P.Id"),
                                new Join(new Table("municipio", "M"), "D.MunicipioId = M.Id"),
                                new Join(new Table("colonia", "C"), "D.ColoniaId = C.Id")
                            },
                            wheres = where                            
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var table = dt.Generar(query, draw, start, length, search, order, columns);
                        foreach (var item in table.data)
                        {
                            var dictionary = (Dictionary<string, object>)item;
                            var nombre = dictionary["Nombre"].ToString();
                            if (nombre.Contains("<script>"))
                                dictionary["Nombre"] = nombre.Replace("<script>", "");
                        }
                        return table;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static DataTable getlistlog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);


                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();

                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("C.Id", centroid.ToString()));
                        }


                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("CC.Activo", "0"));

                        }
                        where.Add(new Where("UR.RolId", "<>", "13"));
                        Query query = new Query
                        {
                            select = new List<string>{
                        "C.Id",
                        "C.TrackingId",
                        "C.Nombre",
                        "C.Habilitado",
                        "UR.Usuario CreadoPor",
                        "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                        "C.AccionId",
                        "date_format(C.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"
                        },
                            from = new Table("institucionlog", "C"),
                            joins = new List<Join>
                        {
                           new Join(new Table("Vusuarios", "V"), "V.Id = C.CreadoPor", "LEFT OUTER"),
                            new Join(new Table("usuario", "UR"), "UR.Id = V.UsuarioId", "LEFT OUTER"),
                            new Join(new Table("institucion", "CC"), "CC.Id = C.Id"),
                        },
                            wheres = where
                        };
                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);

                        ////
                        ///
                        where = new List<Where>();

                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("C.Id", centroid.ToString()));
                        }


                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("CC.Activo", "0"));

                        }
                        where.Add(new Where("UR.RolId", "=", "13"));
                        where.Add(new Where("C.Accion", "=", "Registrado"));
                        query = new Query
                        {
                            select = new List<string>{
                        "C.Id",
                        "C.TrackingId",
                        "C.Nombre",
                        "C.Habilitado",
                        "UR.Usuario CreadoPor",
                        "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                        "C.AccionId",
                        "date_format(C.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"
                        },
                            from = new Table("institucionlog", "C"),
                            joins = new List<Join>
                        {
                            new Join(new Table("usuario", "UR"), "UR.Id = C.CreadoPor", "LEFT OUTER"),
                            new Join(new Table("institucion", "CC"), "CC.Id = C.Id"),
                        },
                            wheres = where
                        };
                        DataTables dt2 = new DataTables(mysqlConnection);
                        DataTable _dt2 = dt2.Generar(query, draw, start, length, search, order, columns);

                        if (_dt2.data.Count > 0)
                        {
                            where = new List<Where>();
                            if (Convert.ToBoolean(todoscancelados))
                            {
                                where.Add(new Where("CC.Activo", "0"));

                            }
                            where.Add(new Where("UR.RolId", "<>", "13"));
                            query = new Query
                            {
                                select = new List<string>{
                                "C.Id",
                                "C.TrackingId",
                                "C.Nombre",
                                "C.Habilitado",
                                "UR.Usuario CreadoPor",
                                "Case Accion when Accion<>'Activado' then 'Habilitado' when Accion <>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                                "C.AccionId",
                                "date_format(C.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"
                        },
                                from = new Table("institucionlog", "C"),
                                joins = new List<Join>
                        {
                            new Join(new Table("usuario", "UR"), "UR.Id = C.CreadoPor", "LEFT OUTER"),
                            new Join(new Table("institucion", "CC"), "CC.Id = C.Id"),
                        },
                                wheres = where
                            };
                            DataTables dt3 = new DataTables(mysqlConnection);
                            DataTable _dt3 = dt3.Generar(query, draw, start, 1, search, order, columns);

                            List<object> listaoriginal = new List<object>();
                            List<object> listaNueva = new List<object>();
                            Dictionary<string, object> valuePairs = new Dictionary<string, object>();
                            Dictionary<string, object> valuePairsnew = new Dictionary<string, object>();
                            listaoriginal = _dt2.data;
                            listaNueva = _dt3.data;

                            foreach (var item in listaoriginal)
                            {
                                valuePairs=(Dictionary<string, object>)item;
                            }

                            foreach (var item in listaNueva)
                            {
                                valuePairsnew = (Dictionary<string, object>)item;
                            }
                            valuePairs["CreadoPor"] = valuePairsnew["CreadoPor"];

                            _dt2.data = listaoriginal;
                            _dt.data.Add(_dt2.data[0]);

                        }

                        ///





                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

    }
}