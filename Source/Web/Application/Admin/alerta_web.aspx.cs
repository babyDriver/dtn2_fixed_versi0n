﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Security;
using System.Web.Services;

using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Web;
using System.Linq;

namespace Web.Application.Admin
{
    public partial class alerta_web : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Configuración y seguridad" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        
        [WebMethod]
        public static object getData()
        {
            try
            {
                var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
                var denoma = "";
                var alerta = "alerta web";
                if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
                {
                    denoma = datosAlertaWeb.Denominacion;
                    alerta = denoma;
                }
                

                object data = new
                {
                    Id = datosAlertaWeb.Id.ToString() ?? "",
                    TrackingId = datosAlertaWeb.TrackingId.ToString() ?? "",
                    Url = datosAlertaWeb.UrlWebService ?? "",
                    Username = datosAlertaWeb.Username ?? "",
                    Password = datosAlertaWeb.Password ?? "",
                    Denominacion=denoma,
                    AlertaWerb=alerta
                };

                return data;
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static object save(AlertaWebAux data)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Modificar)
                {
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario1 = ControlUsuario.Obtener(usId);
                    var mode = string.Empty;
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    var alertaWeb = new Entity.AlertaWeb();

                    if (!string.IsNullOrEmpty(data.TrackingId))
                    {
                        alertaWeb = ControlAlertaWeb.ObtenerPorTrackingId(new Guid(data.TrackingId));
                    }

                    if (string.IsNullOrEmpty((data.Url.Trim()))) alertaWeb.UrlWebService = null;
                    else alertaWeb.UrlWebService = data.Url.Trim();

                    if (string.IsNullOrEmpty((data.Username.Trim()))) alertaWeb.Username = null;
                    else alertaWeb.Username = data.Username.Trim();

                    if (string.IsNullOrEmpty((data.Password.Trim()))) alertaWeb.Password = null;
                    else alertaWeb.Password = data.Password.Trim();
                    alertaWeb.Denominacion = data.Denominacion;
                    if (string.IsNullOrEmpty(data.TrackingId))
                    {
                        mode = @"registrada";
                        alertaWeb.TrackingId = Guid.NewGuid();
                        alertaWeb.CreadoPor = usuario;
                        int id = ControlAlertaWeb.Guardar(alertaWeb);
                        if (id < 0) throw new Exception(@"No fue posible guardar la información, intentelo nuevamente.");

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de alerta web";
                        historial.CreadoPor = usuario1.Id;

                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {
                        mode = @"actualizada";
                        alertaWeb.ModificadoPor = usuario;
                        ControlAlertaWeb.Actualizar(alertaWeb);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de alerta web";
                        historial.CreadoPor = usuario1.Id;

                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                    return JsonConvert.SerializeObject(new { success = true, message = mode, trackingId = data.TrackingId });
                }
                else return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message, trackingId = "" });
            }
        }
    }

    public class AlertaWebAux
    {
        public string TrackingId { get; set; }
        public string Url { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Denominacion { get; set; }
    }
}