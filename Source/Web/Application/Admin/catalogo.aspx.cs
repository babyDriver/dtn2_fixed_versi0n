﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;


namespace Web.Application.Admin
{
    public partial class catalogo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string valor = Convert.ToString(Request.QueryString["name"]);          
            this.tipocatalogo.Value = valor;
           // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
              

                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar) 
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    

                    Entity.Catalogo item = new Entity.Catalogo();
                    item.Nombre = datos[2].ToString();
                    item.Descripcion = datos[3].ToString();
                    item.tipo = getTipoCatalogo(datos[4].ToString());
                    item.Activo = true;
                    item.Habilitado = true;
                    item.ContratoId = contratoUsuario.IdContrato;
                    item.Tipo = contratoUsuario.Tipo;
                    Entity.Catalogo duplicado = ControlCatalogo.Obtener(item.Nombre, Convert.ToInt32(item.tipo));
                    string id = datos[0].ToString();
                    if (id == "") id = "0";

                    if (!(item.tipo.ToString() != "pais" && item.tipo.ToString() != "corporacion" && item.tipo.ToString() != "delegacion" && item.tipo.ToString() != "unidad" && item.tipo.ToString() != "responsable_unidad" && item.tipo.ToString() != "situacion_detenido"))
                    {
                        if (duplicado != null && duplicado.Id != Convert.ToInt32(id) && duplicado.ContratoId == contratoUsuario.IdContrato && duplicado.Tipo == contratoUsuario.Tipo)
                            throw new Exception(string.Concat("Al parecer ya existe un registro nombrado ", duplicado.Nombre, ". Verifique la información y vuelva a intentarlo."));
                    }
                    else
                    {
                        if (duplicado != null && duplicado.Id != Convert.ToInt32(id))
                            throw new Exception(string.Concat("Al parecer ya existe un registro nombrado ", duplicado.Nombre, ". Verifique la información y vuelva a intentarlo."));
                    }

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");


                    item.Creadopor = usuario;

                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        mode = "actualizó";
                        item.Id = Convert.ToInt32(datos[0]);
                        item.TrackingId = new Guid(datos[1]);

                        item.Habilitado = Convert.ToBoolean(datos[5].ToString());

                        ControlCatalogo.Actualizar(item);

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación de catalogo "+item.tipo;
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);

                    }
                    else
                    {
                        mode = "registró";
                        item.TrackingId = Guid.NewGuid();
                        item.Id = ControlCatalogo.Guardar(item);
                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro de catalogo "+item.tipo;
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);

                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = item.Id, TrackingId = item.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        public static Entity.TipoDeCatalogo getTipoCatalogo(string tipo)
        {
            Entity.TipoDeCatalogo catalogo = new Entity.TipoDeCatalogo();
            switch (tipo)
            {
                case "tipo_lesion": catalogo = Entity.TipoDeCatalogo.tipo_lesion; break;
                case "sexo": catalogo = Entity.TipoDeCatalogo.sexo; break;
                case "pantalla": catalogo = Entity.TipoDeCatalogo.pantalla; break;
                case "permiso":
                    catalogo = Entity.TipoDeCatalogo.permiso; break;
                case "tipo_media_filiacion":
                    catalogo = Entity.TipoDeCatalogo.tipo_media_filiacion; break;
                case "etnia":
                    catalogo = Entity.TipoDeCatalogo.etnia; break;
                case "escolaridad":
                    catalogo = Entity.TipoDeCatalogo.escolaridad; break;
                case "peligrosidad_criminologica":
                    catalogo = Entity.TipoDeCatalogo.peligrosidad_criminologica; break;
                case "religion":
                    catalogo = Entity.TipoDeCatalogo.religion; break;
                case "estado_civil":
                    catalogo = Entity.TipoDeCatalogo.estado_civil; break;
                case "nacionalidad":
                    catalogo = Entity.TipoDeCatalogo.nacionalidad; break;
                case "parentesco":
                    catalogo = Entity.TipoDeCatalogo.parentesco; break;
                case "tipo_estudio":
                    catalogo = Entity.TipoDeCatalogo.tipo_estudio; break;
                case "tipo_tatuaje":
                    catalogo = Entity.TipoDeCatalogo.tipo_tatuaje; break;
                case "lunar":
                    catalogo = Entity.TipoDeCatalogo.lunar; break;
                case "media_filiacion":
                    catalogo = Entity.TipoDeCatalogo.media_filiacion; break;
                case "mes":
                    catalogo = Entity.TipoDeCatalogo.mes; break;
                case "tamaño":
                    catalogo = Entity.TipoDeCatalogo.tamaño; break;
                case "adherencia":
                    catalogo = Entity.TipoDeCatalogo.adherencia; break;
                case "tipo_senal":
                    catalogo = Entity.TipoDeCatalogo.tipo_senal; break;
                case "lado_senal":
                    catalogo = Entity.TipoDeCatalogo.lado_senal; break;
                case "vista_senal":
                    catalogo = Entity.TipoDeCatalogo.vista_senal; break;
                case "evento_reciente":
                    catalogo = Entity.TipoDeCatalogo.evento_reciente; break;
                case "turno":
                    catalogo = Entity.TipoDeCatalogo.turno; break;
                case "unidad":
                    catalogo = Entity.TipoDeCatalogo.unidad; break;
                case "responsable_unidad":
                    catalogo = Entity.TipoDeCatalogo.responsable_unidad; break;
                case "tipo_intoxicacion":
                    catalogo = Entity.TipoDeCatalogo.tipo_intoxicacion; break;
                case "evidencia_clasificacion":
                    catalogo = Entity.TipoDeCatalogo.clasificacion_evidencias; break;
                case "delegacion":
                    catalogo = Entity.TipoDeCatalogo.delegacion; break;
                case "corporacion":
                    catalogo = Entity.TipoDeCatalogo.corporacion; break;
                case "situacion_detenido":
                    catalogo = Entity.TipoDeCatalogo.situacion_detenido; break;
                case "institucion":
                    catalogo = Entity.TipoDeCatalogo.institucion; break;
                case "pertenencia":
                    catalogo = Entity.TipoDeCatalogo.pertenencia; break;
                case "estatus_pertenencia":
                    catalogo = Entity.TipoDeCatalogo.estatus_pertenencia; break;
                case "salida_tipo":
                    catalogo = Entity.TipoDeCatalogo.salida_tipo; break;
                case "prueba":
                    catalogo = Entity.TipoDeCatalogo.prueba; break;
                case "calificacion":
                    catalogo = Entity.TipoDeCatalogo.calificacion; break;
                case "tipo_examen":
                    catalogo = Entity.TipoDeCatalogo.tipo_examen; break;
                case "salida_autorizada":
                    catalogo = Entity.TipoDeCatalogo.salida_autorizada; break;
                case "institucion_disposicion":
                    catalogo = Entity.TipoDeCatalogo.institucion_disposicion; break;
                case "sector":
                    catalogo = Entity.TipoDeCatalogo.sector; break;
                case "entrega_pertenencia":
                    catalogo = Entity.TipoDeCatalogo.entrega_pertenencia; break;
                case "mucosas":
                    catalogo = Entity.TipoDeCatalogo.mucosas; break;
                case "aliento":
                    catalogo = Entity.TipoDeCatalogo.aliento; break;
                case "examen_neurologico":
                    catalogo = Entity.TipoDeCatalogo.examen_neurologico; break;
                case "conjuntivas":
                    catalogo = Entity.TipoDeCatalogo.conjuntivas; break;
                case "marcha":
                    catalogo = Entity.TipoDeCatalogo.marcha; break;
                case "reflejos_pupilares":
                    catalogo = Entity.TipoDeCatalogo.reflejos_pupilares; break;
                case "reflejos_osteo_tendinosos":
                    catalogo = Entity.TipoDeCatalogo.reflejos_osteo_tendinosos; break;
                case "conducta":
                    catalogo = Entity.TipoDeCatalogo.conducta; break;
                case "romberq":
                    catalogo = Entity.TipoDeCatalogo.romberq; break;
                case "lenguaje":
                    catalogo = Entity.TipoDeCatalogo.lenguaje; break;
                case "atencion":
                    catalogo = Entity.TipoDeCatalogo.atencion; break;
                case "orientacion":
                    catalogo = Entity.TipoDeCatalogo.orientacion; break;
                case "diadococinencia":
                    catalogo = Entity.TipoDeCatalogo.diadococinencia; break;
                case "ocupacion":
                    catalogo = Entity.TipoDeCatalogo.ocupacion; break;
                case "dedo_nariz":
                    catalogo = Entity.TipoDeCatalogo.dedo_nariz; break;
                case "talon_rodilla":
                    catalogo = Entity.TipoDeCatalogo.talon_rodilla; break;
                case "pupilas":
                    catalogo = Entity.TipoDeCatalogo.pupilas; break;
                case "coordinacion":
                    catalogo = Entity.TipoDeCatalogo.coordinacion; break;
                case "tipomovimiento_celda":
                    catalogo = Entity.TipoDeCatalogo.tipomovimiento_celda; break;
                case "motivo_rehabilitacion":
                    catalogo = Entity.TipoDeCatalogo.motivo_rehabilitacion; break;
                case "tipodediagnostico":
                    catalogo = Entity.TipoDeCatalogo.tipodediagnostico; break;
                case "Catalgo_Adiccion":
                    catalogo = Entity.TipoDeCatalogo.Catalgo_Adiccion; break;
                case "pais":
                    catalogo = Entity.TipoDeCatalogo.pais; break;
                case "cicatriz":
                    catalogo = Entity.TipoDeCatalogo.cicatriz; break;
                case "pertenenciacomun":
                    catalogo = Entity.TipoDeCatalogo.pertenenciacomun; break;
                case "lenguanativa":
                    catalogo = Entity.TipoDeCatalogo.lenguanativa; break;
                case "divisas":
                    catalogo = Entity.TipoDeCatalogo.divisas; break;
                case "actitud":
                    catalogo = Entity.TipoDeCatalogo.actitud; break;
                case "cavidad_oral":
                    catalogo = Entity.TipoDeCatalogo.cavidad_oral; break;
                case "ojos_abiertos_dedo_dedo":
                    catalogo = Entity.TipoDeCatalogo.ojos_abiertos_dedo_dedo; break;
                case "ojos_cerrados_dedo_dedo":
                    catalogo = Entity.TipoDeCatalogo.ojos_cerrados_dedo_dedo; break;
                case "ojos_abiertos_dedo_nariz":
                    catalogo = Entity.TipoDeCatalogo.ojos_abiertos_dedo_nariz; break;
                case "ojos_cerrados_dedo_nariz":
                    catalogo = Entity.TipoDeCatalogo.ojos_cerrados_dedo_nariz; break;
                case "tipo_alergia":
                    catalogo = Entity.TipoDeCatalogo.tipo_alergia; break;
                case "opcion_comun":
                    catalogo = Entity.TipoDeCatalogo.opcion_comun; break;
                case "orientación":
                    catalogo = Entity.TipoDeCatalogo.orientacion; break;
                case "Antecedentes":
                    catalogo = Entity.TipoDeCatalogo.Antecedentes; break;
                case "Conclusion":
                    catalogo = Entity.TipoDeCatalogo.Conclusion; break;
                case "equipo_a_utilizar":
                    catalogo = Entity.TipoDeCatalogo.equipo_a_utilizar; break;
                case "lugar":
                    catalogo = Entity.TipoDeCatalogo.lugar; break;
            }
            return catalogo;
        }

        [WebMethod]
        public static string delete(int id, string tipocatalogo)
        {
            try
            {

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {
                    Entity.Catalogo catalogo = ControlCatalogo.Obtener(id, Convert.ToInt32(getTipoCatalogo(tipocatalogo)));
                    catalogo.tipo = getTipoCatalogo(tipocatalogo);
                    catalogo.Activo = false;
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");


                    catalogo.Creadopor = usuario;
                    ControlCatalogo.Actualizar(catalogo);
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitem(string trackingId, string tipocatalogo)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {
                    Entity.Catalogo catalogo = ControlCatalogo.Obtener(new Guid(trackingId), Convert.ToInt32(getTipoCatalogo(tipocatalogo)));
                    catalogo.tipo = getTipoCatalogo(tipocatalogo);
                    catalogo.Tipo = catalogo.Tipo != null ? catalogo.Tipo.ToString() : catalogo.tipo.ToString();
                    catalogo.Habilitado = catalogo.Habilitado ? false : true;
                    string msg = catalogo.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");


                    catalogo.Creadopor = usuario;
                    ControlCatalogo.Actualizar(catalogo);
                    return JsonConvert.SerializeObject(new { exitoso = true, msg });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getcatalogo(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string catalogo, bool emptytable, string table)
        {
            if (table == "salida_tipo")
            {
                table = "tipo_salida";
            }
            else if (table == "evidencia_clasificacion")
            {
                table = "clasificacion_evidencias";
            }


            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        List<Where> where = new List<Where>();
                        List<Where> where2 = new List<Where>();
                        if (!string.IsNullOrEmpty(catalogo))
                        {
                            where.Add(new Where("C.Nombre", catalogo.Trim()));
                            where2.Add(new Where("C.Nombre", catalogo.Trim()));
                        }
                        where.Add(new Where("C.Activo", "1"));
                        where2.Add(new Where("C.Activo", "1"));
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if (!(table.ToString() != "corporacion" && table.ToString() != "delegacion" && table.ToString() != "unidad" && table.ToString() != "responsable_unidad" && table.ToString() != "situacion_detenido"))
                        {
                            where.Add(new Where("C.ContratoId", contratoUsuario.IdContrato.ToString()));
                            //where.Add(new Where("C.Tipo", contratoUsuario.Tipo));
                            Query query = new Query
                            {
                                select = new List<string>
                                {
                                    "C.Id",
                                    "C.TrackingId",
                                    "C.Nombre",
                                    "C.Descripcion",
                                    "C.Habilitado"
                                },
                                from = new Table(table, "C"),
                                //    joins = new List<Join>
                                //{
                                //    new Join(new Table("usuario", "U"), "U.Id = C.CreadoPor", "inner"),
                                //    new Join(new Table("contratousuario", "cu"), "cu.IdUsuario = U.Id", "inner")
                                //},
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                            var tables = dt.Generar(query, draw, start, length, search, order, columns);
                            foreach (var item in tables.data)
                            {
                                var dictionary = (Dictionary<string, object>)item;
                                var nombre = dictionary["Nombre"].ToString();
                                if (nombre.ToLower().Contains("<script>"))
                                    dictionary["Nombre"] = nombre.ToLower().Replace("<script>", "");

                                var descripcion = dictionary["Descripcion"].ToString();
                                if (descripcion.ToLower().Contains("<script>"))
                                    dictionary["Descripcion"] = descripcion.ToLower().Replace("<script>", "");
                            }
                            return tables;
                        }
                        else
                        {
                            Query query = new Query
                            {
                                select = new List<string>
                                {
                                    "C.Id",
                                    "C.TrackingId",
                                    "C.Nombre",
                                    "C.Descripcion",
                                    "C.Habilitado"
                                },
                                from = new Table(table, "C"),
                                joins = new List<Join>
                                {
                                //    new Join(new Table("usuario", "U"), "U.Id = C.CreadoPor", "inner")
                                },
                                wheres = where2
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            //   DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);

                            var tables = dt.Generar(query, draw, start, length, search, order, columns);
                            foreach (var item in tables.data)
                            {
                                var dictionary = (Dictionary<string, object>)item;
                                var nombre = dictionary["Nombre"].ToString();
                                if (nombre.ToLower().Contains("<script>"))
                                    dictionary["Nombre"] = nombre.ToLower().Replace("<script>", "");

                                var descripcion = dictionary["Descripcion"].ToString();
                                if (descripcion.ToLower().Contains("<script>"))
                                    dictionary["Descripcion"] = descripcion.ToLower().Replace("<script>", "");
                            }
                            return tables;
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static DataTable getcatalogolog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string catalogoid, string todoscancelados, bool emptytable, string table)
        {
            try
            {
                order[0].column = 3;

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        if (table == "salida_tipo")
                        {
                            table = "tipo_salida";
                        }
                        else if (table == "evidencia_clasificacion")
                        {
                            table = "clasificacion_evidencias";
                        }
                      

                        string tablelog = table + "log";

                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();

                        if (Convert.ToInt32(catalogoid) != 0)
                        {
                            where.Add(new Where("C.Id", catalogoid.ToString()));
                        }

                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("CC.Activo", "0"));
                        }

                         

                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "C.Id",
                                "C.TrackingId",
                                "C.Nombre",
                                "C.Habilitado",
                                "UR.Usuario CreadoPor",
                                "case Accion when Accion<>'Activado' then 'Habilitado' when Accion<>'Desactivado' then 'Deshabilitado' else Accion end as Accion",
                                "C.AccionId",
                                "date_format(C.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"
                            },
                            from = new Table(tablelog, "C"),
                            joins = new List<Join>
                            {
                                new Join(new Table("vusuarios", "VR"), "VR.Id = C.CreadoPor", "LEFT OUTER"),
                                new Join(new Table("usuario", "UR"), "UR.Id = VR.UsuarioId", "LEFT OUTER"),
                                new Join(new Table(table, "CC"), "CC.Id = C.Id"),
                            },
                            wheres = where
                        };
                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);

                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios  para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
    }
}