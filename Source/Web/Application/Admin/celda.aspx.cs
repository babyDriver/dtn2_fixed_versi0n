﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Admin
{
    public partial class celda : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //   getPermisos();
           // validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        //public void getPermisos()
        //{
        //    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
        //    int usuario;
        //    if (membershipUser != null)
        //    {
        //        usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
        //        string[] parametros = { usuario.ToString(), "Administración" };
        //        var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


        //        if (permisos == null)
        //        {
        //            //No cuenta con permisos 
        //            Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
        //        }
        //        else
        //        {
        //            if (!permisos.Consultar)
        //                //No cuenta con permisos de consultar el  apartado
        //                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
        //            else
        //            {
        //                //setear hidden registrar
        //                this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
        //                //setear hidden modificar
        //                this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
        //                //setear hidden habilitar
        //                this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

        //            }
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
        //    }
        //}
        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar) 
                //{
                var mode = string.Empty;
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var item = new Entity.Celda();
                item.Nombre = datos[2].ToString();
                item.Capacidad = Convert.ToInt32(datos[3]);
                item.Activo = true;
                item.Nivel_Peligrosidad = ControlCatalogo.Obtener(Convert.ToInt32(datos[4]), Convert.ToInt32(Entity.TipoDeCatalogo.peligrosidad_criminologica));

                                
                string id = datos[1].ToString();
                Guid trackingid;
                if (!string.IsNullOrEmpty(id))
                {
                    if (!Guid.TryParse(id, out trackingid))
                    {
                        throw new Exception("No se logró obtener el identificador del registro");
                    }
                }
                else
                {
                    trackingid = Guid.Empty;
                } 

                Entity.Celda duplicado = ControlCelda.ObtenerPorNombre(item.Nombre);
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                //if (id == "") trackingid = Guid.Empty;
                if (duplicado != null && duplicado.TrackingId != trackingid && duplicado.ContratoId == contratoUsuario.IdContrato && duplicado.Tipo == contratoUsuario.Tipo)
                    throw new Exception(string.Concat("Al parecer ya existe un registro nombrado ", duplicado.Nombre, ". Verifique la información y vuelva a intentarlo."));

                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                item.ContratoId = contratoUsuario.IdContrato;
                item.Tipo = contratoUsuario.Tipo;

                if (!string.IsNullOrEmpty(datos[1].ToString()))
                {
                    mode = "actualizó";
                    //item.Id = Convert.ToInt32(datos[0]);
                    item.TrackingId = trackingid;
                    Entity.Celda _celda = ControlCelda.ObtenerPorTrackingId(trackingid);
                    item.Habilitado = _celda.Habilitado;
                    ControlCelda.Actualizar(item);

                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    historial.Movimiento = "Modificación de celda";
                    historial.CreadoPor = usuario;

                    historial.ContratoId = contratoUsuario.IdContrato;
                    historial.Id = ControlHistorial.Guardar(historial);
                }
                else
                {
                    item.Creadopor = usuario;
                    item.Habilitado = true;
                    mode = "registró";
                    item.TrackingId = Guid.NewGuid();
                    item.Id = ControlCelda.Guardar(item);

                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    historial.Movimiento = "Registro de celda";
                    historial.CreadoPor = usuario;

                    historial.ContratoId = contratoUsuario.IdContrato;
                    historial.Id = ControlHistorial.Guardar(historial);
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = item.Id, TrackingId = item.TrackingId });
                
                //else
               // {
               //     return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
               // }


            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitem(string id)
        {
            try
            {
                //  if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                // {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                Entity.Celda celda = ControlCelda.ObtenerPorTrackingId(new Guid(id));
                celda.Habilitado = celda.Habilitado ? false : true;
                string mensaje = celda.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                ControlCelda.Actualizar(celda);
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
                // }
                // else
                // {
                //      return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                // }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }



        [WebMethod]
        public static string delete(int id)
        {
            try
            {
                //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                //{
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                Entity.Celda item = ControlCelda.ObtenerPorId(id); ;
                item.Activo = false;
                item.Creadopor = usuario;
                ControlCelda.Actualizar(item);
                return JsonConvert.SerializeObject(new { exitoso = true });
                //}
                //else
                //{
                //    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                //}
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getListadoCombo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.peligrosidad_criminologica));

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static DataTable getcelda(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string celda, bool emptytable)
        {
            try
            {
                // if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                //{
                if (!emptytable)
                {
                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    List<Where> where = new List<Where>();

                    if (!string.IsNullOrEmpty(celda))
                    {
                        where.Add(new Where("S.Nombre", string.Concat(" like '%", celda.Trim(), "%'")));
                    }
                    where.Add(new Where("S.Activo", "1"));
                    int contratoid = Convert.ToInt32(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));                    
                    where.Add(new Where("S.ContratoId", contratoUsuario.IdContrato.ToString()));
                    where.Add(new Where("S.Tipo", contratoUsuario.Tipo));
                    Query query = new Query
                    {
                        select = new List<string>{
                                "S.Id",
                                "S.Nombre",
                                "S.Capacidad",
                                "S.TrackingId",
                                "S.Habilitado",
                                "S.Nivel_Peligrosidad",
                                "M.Nombre Peligrosidad"
                            },
                        from = new Table("celda", "S"),
                        joins = new List<Join>
                            {
                                new Join(new Table("peligrosidad_criminologica", "M"), "M.Id = S.Nivel_Peligrosidad", "inner"),
                                new Join(new Table("usuario", "U"), "U.Id = S.CreadoPor", "inner")                                
                            },
                        wheres = where,                        
                    };

                    DataTables dt = new DataTables(mysqlConnection);
                    //return dt.Generar(query, draw, start, length, search, order, columns);
                    var table = dt.Generar(query, draw, start, length, search, order, columns);
                    foreach (var item in table.data)
                    {
                        var dictionary = (Dictionary<string, object>)item;
                        var nombre = dictionary["Nombre"].ToString();
                        if (nombre.Contains("<script>"))
                            dictionary["Nombre"] = nombre.Replace("<script>", "");
                    }
                    return table;
                    //}
                    //else
                    //{
                    //    return DataTables.ObtenerDataTableVacia(null, draw);
                    //}
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }


        [WebMethod]
        public static DataTable getceldalog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string celdaid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (!emptytable)
                {
                    //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                    //{
                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);



                    List<Where> where = new List<Where>();
                    List<Order> orderby = new List<Order>();

                    Guid trackingId;
                    if (Guid.TryParse(celdaid, out trackingId))
                    {
                        where.Add(new Where("C.TrackingId", trackingId.ToString()));
                    }


                    if (Convert.ToBoolean(todoscancelados))
                    {
                        where.Add(new Where("CC.Activo", "0"));

                    }
                    where.Add(new Where("UR.RolId", "<>", "13"));

                    Query query = new Query
                    {
                        select = new List<string>{
                         "C.Id",
                        "C.TrackingId",
                        "C.Nombre",
                        "C.Habilitado",
                        "C.Nivel_Peligrosidad",
                        "UR.Usuario CreadoPor",
                        "C.Accion",
                        "C.AccionId",
                        "date_format(C.Fec_Motvo, '%Y-%m-%d %H:%i:%S') Fec_Motvo"
                        },
                        from = new Table("celdalog", "C"),
                        joins = new List<Join>
                        {
                            new Join(new Table("usuario", "UR"), "UR.Id = C.CreadoPor", "LEFT OUTER"),
                            new Join(new Table("celda", "CC"), "CC.Id = C.Id"),
                            new Join(new Table("peligrosidad_criminologica", "A"), "C.Nivel_Peligrosidad = A.Id"),
                        },
                        wheres = where
                    };
                    DataTables dt = new DataTables(mysqlConnection);
                    var a = dt.Generar(query, draw, start, length, search, order, columns);

                    ////
                    where = new List<Where>();
                    if (Guid.TryParse(celdaid, out trackingId))
                    {
                        where.Add(new Where("C.TrackingId", trackingId.ToString()));
                    }


                    if (Convert.ToBoolean(todoscancelados))
                    {
                        where.Add(new Where("CC.Activo", "0"));

                    }
                    where.Add(new Where("UR.RolId", "=", "13"));
                    where.Add(new Where("C.Accion", "=", "Registrado"));
                      query = new Query
                    {
                        select = new List<string>{
                         "C.Id",
                        "C.TrackingId",
                        "C.Nombre",
                        "C.Habilitado",
                        "C.Nivel_Peligrosidad",
                        "UR.Usuario CreadoPor",
                        "C.Accion",
                        "C.AccionId",
                        "date_format(C.Fec_Motvo, '%Y-%m-%d %H:%i:%S') Fec_Motvo"
                        },
                        from = new Table("celdalog", "C"),
                        joins = new List<Join>
                        {
                            new Join(new Table("usuario", "UR"), "UR.Id = C.CreadoPor", "LEFT OUTER"),
                            new Join(new Table("celda", "CC"), "CC.Id = C.Id"),
                            new Join(new Table("peligrosidad_criminologica", "A"), "C.Nivel_Peligrosidad = A.Id"),
                        },
                        wheres = where
                    };
                    DataTables dt2 = new DataTables(mysqlConnection);
                    var a2 = dt2.Generar(query, draw, start, length, search, order, columns);

                    ///
                    if (a2.data.Count > 0)
                    {
                        ////
                        ///
                        where = new List<Where>();
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("CC.Activo", "0"));

                        }
                        where.Add(new Where("UR.RolId", "<>", "13"));

                        query = new Query
                        {
                            select = new List<string>{
                         "C.Id",
                        "C.TrackingId",
                        "C.Nombre",
                        "C.Habilitado",
                        "C.Nivel_Peligrosidad",
                        "UR.Usuario CreadoPor",
                        "C.Accion",
                        "C.AccionId",
                        "date_format(C.Fec_Motvo, '%Y-%m-%d %H:%i:%S') Fec_Motvo"
                        },
                            from = new Table("celdalog", "C"),
                            joins = new List<Join>
                        {
                            new Join(new Table("usuario", "UR"), "UR.Id = C.CreadoPor", "LEFT OUTER"),
                            new Join(new Table("celda", "CC"), "CC.Id = C.Id"),
                            new Join(new Table("peligrosidad_criminologica", "A"), "C.Nivel_Peligrosidad = A.Id"),
                        },
                            wheres = where
                        };
                        DataTables dt3 = new DataTables(mysqlConnection);
                        var a3 = dt3.Generar(query, draw, start, length, search, order, columns);

                        List<object> listaorignal = new List<object>();
                        List<object> listanew = new List<object>();
                        Dictionary<string, object> valuePairs = new Dictionary<string, object>();
                        Dictionary<string, object> valuePairsnew = new Dictionary<string, object>();
                        listaorignal = a2.data;
                        listanew = a3.data;

                        foreach (var item in listaorignal)
                        {
                            valuePairs=(Dictionary<string,object>)item;

                        }
                        foreach (var item in listanew)
                        {
                            valuePairsnew = (Dictionary<string, object>)item;

                        }
                        valuePairs["CreadoPor"] = listanew!=null? valuePairsnew["CreadoPor"]:"administrador global";

                        a2.data = listaorignal;

                        a.data.Add(a2.data[0]);



                    }





                    return a;
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
                //}
                //else
                //{
                //    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                //}

            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }


    }
}