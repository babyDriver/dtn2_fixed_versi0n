using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI.WebControls;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Admin
{
    public partial class users_ : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                // ojo con los acentos
                string[] parametros = { usuario.ToString(), "Configuración y seguridad" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        protected void CargarPerfiles()
        {
            var roles = Roles.GetAllRoles();

            foreach (var rol in roles)
                perfil.Items.Add(new ListItem(rol, rol));
        }



        [WebMethod]
        public static DataTable getusers(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                //de nuevo ojo con los acentos...
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Consultar)
                {
                    if (!emptytable)
                    {
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var usuario = ControlUsuario.Obtener(usId);
                        var roles = ControlRol.ObtenerPorId(usuario.RolId);


                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        if (roles.name == "Administrador global"|| roles.name=="Supervisor")
                        {
                            where.Add(new Where("UU.Activo", "1"));
                            if (usuario.RolId != 13)
                            {
                                where.Add(new Where("UU.RolId","<>", "13"));
                            }
                            Query query = new Query
                            {
                                select = new List<string>{
                           "U.applicationId",
                            "U.name",
                            "UU.Nombre",
                            "UU.ApellidoPaterno",
                            "UU.ApellidoMaterno",
                            "UU.Activo",
                            "UU.UserId",
                            "UU.Usuario",
                            "UU.Email",
                            "R.name Rol",
                            "M.IsLockedOut",
                            "(SELECT GROUP_CONCAT(ZZ.Cliente SEPARATOR ', ') FROM (SELECT Cliente, IdUsuario FROM contratousuario CUU INNER JOIN subcontrato S ON CUU.IdContrato = S.Id AND CUU.Tipo = 'subcontrato' INNER JOIN contrato CC ON CC.Id = S.ContratoId INNER JOIN cliente CL ON CL.Id = CC.ClienteId WHERE CUU.Activo = 1 UNION ALL SELECT Cliente, IdUsuario FROM contratoUsuario CUU INNER JOIN contrato C ON CUU.IdContrato = C.Id AND CUU.Tipo = 'contrato' INNER JOIN cliente CL ON CL.Id = C.ClienteId WHERE CUU.Activo = 1) ZZ WHERE ZZ.IdUsuario = UU.Id) Clientes",
                            "(SELECT GROUP_CONCAT(Subcontrato SEPARATOR ', ') FROM ContratoUsuario CU INNER JOIN Subcontrato C ON CU.IdContrato = C.Id WHERE CU.IdUsuario = UU.Id AND CU.Tipo = 'subcontrato' AND CU.Activo = 1) Subcontratos"
                            },
                                    from = new DT.Table("my_aspnet_membership", "M"),
                                    joins = new List<Join>
                            {
                                new Join(new DT.Table("my_aspnet_users", "U"), "M.UserId = U.id"),
                                new Join(new DT.Table("my_aspnet_usersinroles", "UR"), "U.id  = UR.userId "),
                                new Join(new DT.Table("my_aspnet_roles", "R"),"UR.roleId = R.id"),
                                new Join(new DT.Table("usuario", "UU"), "UU.Id = U.id"),
                            },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            if (usuario.RolId != 13)
                            {
                                where.Add(new Where("UU.RolId", "<>", "13"));
                            }
                            where.Add(new Where("UU.Activo", "1"));
                            where.Add(new Where("CU.IdContrato", contratoUsuario.IdContrato.ToString()));
                            Query query = new Query
                            {
                                select = new List<string>{
                           "U.applicationId",
                            "U.name",
                            "UU.Nombre",
                            "UU.ApellidoPaterno",
                            "UU.ApellidoMaterno",
                            "UU.Activo",
                            "UU.UserId",
                            "UU.Usuario",
                            "M.Email",
                            "R.name Rol",
                            "M.IsLockedOut",
                            "(SELECT GROUP_CONCAT(ZZ.Cliente SEPARATOR ', ') FROM (SELECT Cliente, IdUsuario FROM contratousuario CUU INNER JOIN subcontrato S ON CUU.IdContrato = S.Id AND CUU.Tipo = 'subcontrato' INNER JOIN contrato CC ON CC.Id = S.ContratoId INNER JOIN cliente CL ON CL.Id = CC.ClienteId WHERE CUU.Activo = 1 UNION ALL SELECT Cliente, IdUsuario FROM contratoUsuario CUU INNER JOIN contrato C ON CUU.IdContrato = C.Id AND CUU.Tipo = 'contrato' INNER JOIN cliente CL ON CL.Id = C.ClienteId WHERE CUU.Activo = 1) ZZ WHERE ZZ.IdUsuario = UU.Id) Clientes",
                            "(SELECT GROUP_CONCAT(Subcontrato SEPARATOR ', ') FROM ContratoUsuario CU INNER JOIN Subcontrato C ON CU.IdContrato = C.Id WHERE CU.IdUsuario = UU.Id AND CU.Tipo = 'subcontrato' AND CU.Activo = 1) Subcontratos"
                            },
                                from = new DT.Table("my_aspnet_membership", "M"),
                                joins = new List<Join>
                            {
                                new Join(new DT.Table("my_aspnet_users", "U"), "M.UserId = U.id"),
                                new Join(new DT.Table("my_aspnet_usersinroles", "UR"), "U.id  = UR.userId "),
                                new Join(new DT.Table("my_aspnet_roles", "R"),"UR.roleId = R.id"),
                                new Join(new DT.Table("usuario", "UU"), "UU.Id = U.id"),
                                new Join(new DT.Table("ContratoUsuario", "CU"), "UU.Id = CU.IdUsuario"),
                            },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            return dt.Generar(query, draw, start, length, search, order, columns);


                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la informaci�n.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }


        [WebMethod]
        public static string blockuser(string username)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Eliminar)
                {

                    var usuario = Membership.GetUser(username);
                    if (usuario.IsLockedOut)
                        usuario.UnlockUser();
                    else
                        for (var i = 0; i < Membership.MaxInvalidPasswordAttempts; i++)
                            Membership.ValidateUser(usuario.UserName, "thisisandummypasswordonlytolocktheuser");

                    Membership.UpdateUser(usuario);

                    var user = ControlUsuario.ObtenerPorUsuario(username);
                    user.Habilitado = !usuario.IsLockedOut;
                    ControlUsuario.Actualizar(user);

                    string mensaje = "";
                    mensaje = usuario.IsLockedOut ? "deshabilitado" : "habilitado";

                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, mensaje = mensaje });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acci�n." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getContratosByUsuario(string[] datos)
        {
            try
            {
                var usuario = ControlUsuario.Obtener(new Guid(datos[0].ToString()));
                List<object> list = ControlContratoAsignacion.GetContratos(usuario.Id);

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveContratos(List<ContratoNuevoAux> contratos, string tracking)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(new Guid(tracking));

                Entity.ContratoUsuario contratoUsuario = new Entity.ContratoUsuario();

                //La lista que recibo solo tiene los que el usuario puso en true
                //Buscar en la BD todos los contratos que tiene asignado el usuario
                //Los contratos que no esten en la lista recibida y que esten en la BD se pondran 
                //como Activo = 0

                var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                
                foreach(var item in dataContratos)
                {
                    foreach(var item2 in contratos)
                    {
                        if(!(item.IdContrato.ToString() == item2.Id))
                        {
                            //Actualizar en Activo = 0 el item
                        }
                    }
                }
                

                foreach(var item in contratos)
                {                    
                    object[] data =
                    {
                        usuario.Id,
                        Convert.ToInt32(item.Id)
                    };

                    var contratoEnBD = ControlContratoUsuario.ObtenerPorContratoYUsuario(data);

                    if(!(contratoEnBD != null))
                    {
                        contratoUsuario.CreadoPor = usId;
                        contratoUsuario.IdContrato = Convert.ToInt32(item.Id);
                        contratoUsuario.IdUsuario = usuario.Id;
                        contratoUsuario.TrackingId = Guid.NewGuid();
                        ControlContratoUsuario.Guardar(contratoUsuario);
                    }                    
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "guardo"  });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }

    public class ContratoNuevoAux
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string VigenciaInicial { get; set; }
        public string VigenciaFinal { get; set; }
    }
}
