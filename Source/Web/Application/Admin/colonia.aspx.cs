﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Table = DT.Table;

namespace Web.Application.Admin
{
    public partial class colonia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static DataTable getColonialog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Asentamiento Colonia",
                                "A.TipoDeAsentamiento",
                                "A.IdMunicipio",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("colonialog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("Vusuarios", "V"), "V.Id = A.CreadoPor", "LEFT OUTER"),
                                new Join(new Table("usuario", "B"), "B.Id = V.UsuarioId", "LEFT OUTER"),

                                
                                new Join(new Table("colonia","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
            


                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }

                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }

            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
        [WebMethod]
        public static DataTable getNeighborhoods(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        where.Add(new Where("C.Activo", "1"));
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        
                        Query query = new Query
                        {
                            select = new List<string>{
                        "C.Id",
                        "C.Habilitado",
                        "C.TrackingId",
                        "C.IdMunicipio",
                        "C.CodigoPostal",
                        "C.Asentamiento",
                        "C.TipoDeAsentamiento",
                        "C.Municipio",
                        "C.Estado",
                        "C.ClaveDeOficina"                        
                        },
                            from = new DT.Table("colonia", "C"),
                            joins = new List<Join>
                            {
                                new Join(new DT.Table("usuario", "U"), "U.Id = C.CreadoPor", "inner")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static string blockuser(string trackingid)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {                                        
                    var coloniaAux = ControlColonia.ObtenerPorTrackingId(new Guid(trackingid));
                    coloniaAux.Habilitado = (coloniaAux.Habilitado) ? false : true;
                    string mensaje = coloniaAux.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                    ControlColonia.Actualizar(coloniaAux);

                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    var movimiento = coloniaAux.Habilitado ? "Colonia habilitada" : "Colonia deshabilitada";
                    historial.Movimiento = movimiento;
                    historial.CreadoPor = usId;

                    historial.ContratoId = contratoUsuario.IdContrato;
                    historial.Id = ControlHistorial.Guardar(historial);

                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, mensaje });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}