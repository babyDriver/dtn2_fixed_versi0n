﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;

using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Admin
{
    public class ParametroAux
    {
        //public string ContratoId { get; set; }
       // public string FechaHora { get; set; }
        public string ParametroId { get; set; }
        public string Valor { get; set; }
        public string Id { get; set; }
        public string ContratoId { get; set; }


    }
    public partial class Parametros : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static DataTable GetParametros(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var usuario = ControlUsuario.Obtener(usId);



                        var roles = ControlRol.ObtenerPorId(usuario.RolId);

                        List<Where> where = new List<Where>();
                        if (roles.name != "Administrador global")
                        {
                            where.Add(new Where("CU.IdUsuario", usuario.Id.ToString()));

                            Query query = new Query
                            {
                                select = new List<string>
                                {
                                    "P.Id",
                                    "P.NombreParametro",
                                    "P.Descripcion",
                                    "T.TipoParametro",
                                    "ifnull(C.Id,0) ParametroContratoId",
                                    "P.TipoParametroId",
                                    "ifnull(C.Valor,'') Valor",
                                    "C.TrackingId",
                                    "S.Subcontrato",
                                    "S.Id ContratoId"
                                },
                                from = new DT.Table("parametrosAdministradorglobalview", "V"),
                                joins = new List<Join>
                                {
                                    new Join(new Table("Parametro", "P"), "V.ParametroId = P.Id", "inner"),
                                    new Join(new Table("tipoparametro", "T"), "T.Id = P.TipoParametroId", "inner"),
                                    new Join(new Table("parametro_contrato", "C"), "V.ParametroId = C.ParametroId and V.ContratoId = C.ContratoId"),
                                    new Join(new Table("subcontrato", "S"), "S.Id = V.ContratoId"),
                                    new Join(new Table("contratousuario", "CU"), "CU.IdContrato = S.Id")
                                },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            DataTable dt_ = new DataTable();
                            dt_ = dt.Generar(query, draw, start, length, search, order, columns);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            Query query = new Query
                            {
                                select = new List<string>
                                {
                                    "P.Id",
                                    "P.NombreParametro",
                                    "P.Descripcion",
                                    "T.TipoParametro",
                                    "ifnull(C.Id,0) ParametroContratoId",
                                    "P.TipoParametroId",
                                    "ifnull(C.Valor,'') Valor",
                                    "C.TrackingId",
                                    "S.Subcontrato",
                                    "S.Id ContratoId"
                                },
                                from = new DT.Table("parametrosAdministradorglobalview", "V"),
                                joins = new List<Join>
                                {
                                    new Join(new Table("Parametro", "P"), "V.ParametroId=P.Id", "inner"),
                                    new Join(new Table("tipoparametro", "T"), "T.Id = P.TipoParametroId", "inner"),
                                    new Join(new Table("parametro_contrato", "C"), "V.ParametroId=C.ParametroId and V.ContratoId=C.ContratoId" ),
                                    new Join(new Table("subcontrato", "S"), "S.Id=V.ContratoId" )
                                },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            DataTable dt_ = new DataTable();
                            dt_ = dt.Generar(query, draw, start, length, search, order, columns);

                            return dt.Generar(query, draw, start, length, search, order, columns);


                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static Object Save(ParametroAux parametroAux)
        {
            try
            {
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                Entity.ParametroContrato parametroContrato = new Entity.ParametroContrato();
                if (parametroAux.ContratoId !="null")
                {
                    parametroContrato.ContratoId = Convert.ToInt32(parametroAux.ContratoId);
                }
                else
                {
                    parametroContrato.ContratoId = contratoUsuario.IdContrato;

                }

                
                parametroContrato.FechaHora = DateTime.Now;
                parametroContrato.Parametroid = Convert.ToInt32(parametroAux.ParametroId);
                parametroContrato.TrackingId = Guid.NewGuid().ToString();
                parametroContrato.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                parametroContrato.Valor = parametroAux.Valor;
                var usId= Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());



                if (string.IsNullOrEmpty(parametroAux.Id) || parametroAux.Id == "0")
                {

                    int idmovimiento = ControlParametroContrato.Guardar(parametroContrato);
                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    historial.Movimiento = "Registro de parámetro";
                    historial.CreadoPor = usId;

                    historial.ContratoId = contratoUsuario.IdContrato;
                    historial.Id = ControlHistorial.Guardar(historial);
                }
                else
                {
                    parametroContrato.Id = Convert.ToInt32(parametroAux.Id);
                    ControlParametroContrato.Actualizar(parametroContrato);
                    var historial = new Entity.Historial();
                    historial.InternoId = 0;
                    historial.Habilitado = true;
                    historial.Fecha = DateTime.Now;
                    historial.Activo = true;
                    historial.TrackingId = Guid.NewGuid();
                    historial.Movimiento = "Modificación de parámetro";
                    historial.CreadoPor = usId;

                    historial.ContratoId = contratoUsuario.IdContrato;
                    historial.Id = ControlHistorial.Guardar(historial);
                }



                return new { exitoso = true, mensaje = "La información fue registrada satisfactoriamente", Id = "", TrackingId = "" };
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
    }
}