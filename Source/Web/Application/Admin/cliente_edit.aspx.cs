﻿using Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Admin
{
    public partial class cliente_edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Configuración y seguridad" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Modificar && !permisos.Registrar)
                        //No cuenta con permisos de modificar/editar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static Object getCustomer(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Consultar)
                {
                    var customer = ControlCliente.ObtenerPorTrackingId(new Guid(trackingid));
                    ClienteEditAux obj = new ClienteEditAux();
                    
                    if (customer != null)
                    {
                        obj.Id = customer.Id.ToString();                        
                        obj.Nombre = customer.Nombre;
                        obj.Rfc = customer.Rfc;
                        obj.TenantId = customer.TenantId.ToString();                        
                    }
                    return obj;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static Object save(ClienteAux cliente)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Configuración y seguridad" }).Modificar)
                {
                    var cli = new Entity.Cliente();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());                    
                                       
                    int id = 0;
                    if (cliente.Id != "")
                    {
                        id = Convert.ToInt32(cliente.Id);
                    }

                    object[] data = new object[]{ cliente.Nombre, cliente.Rfc };
                    
                    var cliente_duplicado = ControlCliente.ObtenerPorNombreYRfc(data);

                    if (cliente_duplicado != null && cliente_duplicado.Id != id)
                        throw new Exception(string.Concat("El nombre de cliente: ", cliente_duplicado.Nombre, " ya está registrado favor de cambiarlo por otro."));
                    /*
                    var email_duplicado = ControlUsuario.ObtenerPorEmail(usuario.Email);

                    if (email_duplicado != null && email_duplicado.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("El correo: ", email_duplicado.Email, " ya está registrado favor de cambiarlo por otro."));
                        */
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    if (string.IsNullOrEmpty(cliente.Id))
                    {
                        mode = @"registró";
                        var tmpmail = String.Concat(cliente.Nombre, "@test.com");
                      
                        cli.CreadoPor = usId;                                                
                        cli.Nombre = cliente.Nombre;
                        cli.Rfc = cliente.Rfc;
                        cli.TenantId = Guid.NewGuid();                        
                        cli.Id = ControlCliente.Guardar(cli);
                        Entity.Accion accion = new Entity.Accion();
                        accion.CreadoPor = usId;
                        accion.Descripcion = "Nuevo cliente registrado";
                        accion.Fecha = DateTime.Now;
                        accion.Nombre = "Crear";
                        accion.TrackingId = Guid.NewGuid();
                        int idAccion = ControlAccion.Guardar(accion);

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Registro del cliente";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                    else
                    {
                        mode = @"actualizó";

                        cli.Id = Convert.ToInt32(cliente.Id);                        
                        cli.Nombre = cliente.Nombre;
                        cli.Rfc = cliente.Rfc;
                        cli.Activo = true;
                        cli.Habilitado = true;
                        ControlCliente.Actualizar(cli);
                        Entity.Accion accion = new Entity.Accion();
                        accion.CreadoPor = usId;
                        accion.Descripcion = "Cliente actualizado";
                        accion.Fecha = DateTime.Now;
                        accion.Nombre = "Actualizar";
                        accion.TrackingId = Guid.NewGuid();
                        int idAccion = ControlAccion.Guardar(accion);

                        var historial = new Entity.Historial();
                        historial.InternoId = 0;
                        historial.Habilitado = true;
                        historial.Fecha = DateTime.Now;
                        historial.Activo = true;
                        historial.TrackingId = Guid.NewGuid();
                        historial.Movimiento = "Modificación del cliente";
                        historial.CreadoPor = usId;

                        historial.ContratoId = contratoUsuario.IdContrato;
                        historial.Id = ControlHistorial.Guardar(historial);
                    }                                        

                    return new { exitoso = true, mensaje = mode, Id = cli.Id.ToString(), TrackingId = cli.TenantId };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
    }

    public class ClienteAux
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Rfc { get; set; }        
    }

    public class ClienteEditAux
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string Nombre { get; set; }
        public string Rfc { get; set; }        
    }
}