﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Admin
{
    public partial class evento_reciente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static DataTable getEventoReciente(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string evento, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        List<Where> where = new List<Where>();

                        if (!string.IsNullOrEmpty(evento))
                        {
                            where.Add(new Where("E.Nombre", string.Concat(" like '%", evento.Trim(), "%'")));
                        }
                        where.Add(new Where("E.Activo", "1"));

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("E.Tipo", contratoUsuario.Tipo));

                        Query query = new Query
                        {
                            select = new List<string>{
                                "E.Id",
                                "E.TrackingId",
                                "E.Hora",
                                "E.Descripcion",
                                "E.Habilitado"
                            },
                            from = new Table("evento_reciente", "E"),
                            joins = new List<Join>
                            {

                            },
                            wheres = where,
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        //return dt.Generar(query, draw, start, length, search, order, columns);
                        var table = dt.Generar(query, draw, start, length, search, order, columns);
                        foreach (var item in table.data)
                        {
                            var dictionary = (Dictionary<string, object>)item;
                            var descripcion = dictionary["Descripcion"].ToString();
                            if (descripcion.Contains("<script>"))
                                dictionary["Descripcion"] = descripcion.Replace("<script>", "");
                        }
                        return table;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static string save(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));


                    Entity.EventoReciente item = new Entity.EventoReciente();
                    item.Hora = Convert.ToInt32(datos[2].ToString());
                    item.Descripcion = datos[3].ToString();
                    item.Activo = true;
                    item.Habilitado = true;
                    item.ContratoId = contratoUsuario.IdContrato;
                    item.Tipo = contratoUsuario.Tipo;
                    Entity.EventoReciente duplicado = ControlEventoReciente.ObtenerPorHora(item.Hora);
                    string id = datos[0].ToString();
                    if (id == "") id = "0";

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    if (duplicado != null && duplicado.Id != Convert.ToInt32(id) && duplicado.ContratoId == contratoUsuario.IdContrato)
                    {
                        throw new Exception(string.Concat("Al parecer ya existe un registro con la misma cantidad de ", duplicado.Hora, " hrs. Verifique la información y vuelva a intentarlo."));
                     
                    }
                    else
                    {
                        item.Creadopor = usuario;

                        if (!string.IsNullOrEmpty(datos[0].ToString()))
                        {
                            mode = "actualizó";
                            item.Id = Convert.ToInt32(datos[0]);
                            item.TrackingId = new Guid(datos[1]);

                            ControlEventoReciente.Actualizar(item);
                            
                            var historial = new Entity.Historial();
                            historial.InternoId = 0;
                            historial.Habilitado = true;
                            historial.Fecha = DateTime.Now;
                            historial.Activo = true;
                            historial.TrackingId = Guid.NewGuid();
                            historial.Movimiento = "Modificación de evento reciente";
                            historial.CreadoPor = usId;

                            historial.ContratoId = contratoUsuario.IdContrato;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                        else
                        {
                            mode = "registró";
                            item.TrackingId = Guid.NewGuid();
                            item.Id = ControlEventoReciente.Guardar(item);
                           
                            var historial = new Entity.Historial();
                            historial.InternoId = 0;
                            historial.Habilitado = true;
                            historial.Fecha = DateTime.Now;
                            historial.Activo = true;
                            historial.TrackingId = Guid.NewGuid();
                            historial.Movimiento = "Registro de evento reciente";
                            historial.CreadoPor = usId;

                            historial.ContratoId = contratoUsuario.IdContrato;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = item.Id, TrackingId = item.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitem(string id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {
                    Entity.EventoReciente eventoReciente = ControlEventoReciente.ObtenerPorId(Convert.ToInt32(id));
                    eventoReciente.Habilitado = eventoReciente.Habilitado ? false : true;
                    string msg = eventoReciente.Habilitado ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");


                    eventoReciente.Creadopor = usuario;
                    ControlEventoReciente.Actualizar(eventoReciente);
                    return JsonConvert.SerializeObject(new { exitoso = true, msg });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable geteventorecientelog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string id, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();

                        if (Convert.ToInt32(id) != 0)
                        {
                            where.Add(new Where("E.Id", id.ToString()));
                        }


                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("E.Activo", "0"));

                        }
                         
                        Query query = new Query
                        {
                            select = new List<string>{
                         "E.Id",
                        "E.TrackingId",
                        "E.Hora",
                        "E.Habilitado",
                        "E.Accion",
                        "E.AccionId",
                        "UR.Usuario CreadoPor",
                        "date_format(E.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"
                        },
                            from = new Table("evento_recientelog", "E"),
                            joins = new List<Join>
                        {
                            new Join(new Table("Vusuarios", "V"), "V.Id = E.CreadoPor", "LEFT OUTER"),
                            new Join(new Table("usuario", "UR"), "UR.Id = V.UsuarioId", "LEFT OUTER"),
                        },
                            wheres = where
                        };
                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable  _dt= dt.Generar(query, draw, start, length, search, order, columns);

                        ///
                        

                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios  para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
    }
}