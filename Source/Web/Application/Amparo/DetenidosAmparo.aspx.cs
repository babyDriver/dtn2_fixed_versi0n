﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using Entity.Util;
using System.Linq;
using System.Collections;
using System.Text.RegularExpressions;

namespace Web.Application.Amparo
{
    public partial class DetenidosAmparo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //  validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        public static string ValidaReglaDenegocioExamenmedico(int DetalleDetencionId, int ContratoId)
        {

            var examenMedico = ControlServicoMedico.ObtenerTodo();
            List<Entity.ServicioMedico> serviciomedico = new List<Entity.ServicioMedico>();
            foreach (var item in examenMedico.Where(x => x.DetalledetencionId == DetalleDetencionId))
            {
                serviciomedico.Add(item);
            }

            if (serviciomedico.Count > 0)
            {
                return "OK";

            }
            var parametros = ControlParametroContrato.TraerTodos();
            if (parametros.Count > 0)
            {
                Entity.ParametroContrato parametroContrato = new Entity.ParametroContrato();

                foreach (var item in parametros.Where(x => x.Parametroid == Convert.ToInt32(Entity.ParametroEnum.TRASLADO_O_SALIDA_SIN_EXAMEN_MÉDICO) && x.ContratoId == ContratoId))
                {
                    parametroContrato = item;
                }

                if (parametroContrato.Valor == "1")
                {
                    return "no tiene un examen médico asignado";

                }
                else
                {
                    return "OK";

                }
            }
            else { return "no tiene un examen médico asignado"; }

        }

        public static string ValidaReglaDeNegocioSalidaEfectuadaBiometricos(int DetenidoId, int ContratoId)
        {

            var biometricos = ControlBiometricos.GetByDetenidoId(DetenidoId);

            if (biometricos.Count > 0)
            {
                return "OK";
            }
            var parametros = ControlParametroContrato.TraerTodos();
            Entity.ParametroContrato parametro = new Entity.ParametroContrato();

            foreach (var item in parametros.Where(x => x.ContratoId == ContratoId && x.Parametroid == Convert.ToInt32(Entity.ParametroEnum.SALIDA_DEL_DETENIDO_SIN_BIOMETRICO)))
            {
                parametro = item;
            }

            if (parametro == null)
            {
                return "OK";
            }
            if ((parametro.Valor != null ? parametro.Valor : "0") == "1")
            {
                return "no tiene biométricos registrados";
            }
            else
            {

                return "OK";
            }
        }

        [WebMethod]
        public static string RechazarAmparo(string[] datos)
        {
            try
            {
                var mode = string.Empty;
                var interno = new Entity.Detenido();
                var detalle = new Entity.DetalleDetencion();
                var calificacion = new Entity.Calificacion();
                var amparo = new Entity.DetenidoAmparo();
                var salida = new Entity.SalidaEfectuadaJuez();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(usId);
                for (int i = 0; i < datos.Length; i++)
                {
                    interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[i].ToString()));
                    string idInterno = interno != null ? interno.Id.ToString() : "";
                    string[] internod = new string[2] {
                        idInterno, "true"
                    };
                    detalle = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                    calificacion = ControlCalificacion.ObtenerPorInternoId(detalle.Id);
                    salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalle.Id);
                    amparo = ControlDetenidoAmparo.ObtenerPorDetalleDetencionId(detalle.Id);
                    var horas = 0;
                    if(amparo !=null)
                    {
                        DateTime fechaSalida = amparo.Fecha;
                        TimeSpan horasRestantes;
                        horasRestantes = DateTime.Now.Subtract(fechaSalida);
                        horas = Convert.ToInt32(horasRestantes.TotalHours);

                        amparo.Activo = false;
                        ControlDetenidoAmparo.Actualizar(amparo);
                    }
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    Entity.Historial historial1 = new Entity.Historial();
                    historial1.Activo = true;
                    historial1.CreadoPor = usId;
                    historial1.Fecha = DateTime.Now;
                    historial1.Habilitado = true;
                    historial1.InternoId = detalle.Id;
                    historial1.Movimiento = "Solicitud de amparo rechazada";
                    historial1.TrackingId = detalle.TrackingId;
                    historial1.ContratoId = subcontrato.Id;
                    historial1.Id = ControlHistorial.Guardar(historial1);
                    
                    calificacion.TotalHoras = calificacion.TotalHoras + horas;
                    ControlCalificacion.Actualizar(calificacion);
                    salida.Activo = 0;
                    salida.Habilitado = 0;
                    ControlSalidaEfectuadaJuez.Actualizar(salida);

                }
                return JsonConvert.SerializeObject(new { exitoso = true,  mensaje = "Se regresó del amparo exitosamente" });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string guardaSalidaEfectuadaJuez(string[] datos, string fundamento, string tiposalidaid)
        {
            try
            {
                ArrayList arrayList = new ArrayList();
                string errores = "";
                ArrayList listaErrores = new ArrayList();
                Entity.Detenido interno = new Entity.Detenido();
                Entity.DetalleDetencion estatusInterno = new Entity.DetalleDetencion();
                Entity.SalidaEfectuadaJuez salida_efectuada = new Entity.SalidaEfectuadaJuez();
                Entity.Calificacion calificacion = new Entity.Calificacion();
                Entity.Catalogo situacion = new Entity.Catalogo();
                Entity.TrabajoSocial trabajoSocial = new Entity.TrabajoSocial();
                Entity.Institucion centroReclusion = new Entity.Institucion();
                Entity.Usuario usuario = new Entity.Usuario();
                Entity.General general = new Entity.General();
                Entity.Catalogo sexo = new Entity.Catalogo();
                Entity.Catalogo estadoCivil = new Entity.Catalogo();
                Entity.Catalogo escolaridad = new Entity.Catalogo();
                Entity.Catalogo nacionalidad = new Entity.Catalogo();
                Entity.Domicilio domicilio = new Entity.Domicilio();
                Entity.Colonia colonia = new Entity.Colonia();
                Entity.Municipio municipio = new Entity.Municipio();
                Entity.Estado estado = new Entity.Estado();
                Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                Entity.Catalogo unidad = new Entity.Catalogo();
                Entity.Catalogo responsableUnidad = new Entity.Catalogo();
                Entity.Colonia coloniaDetencion = new Entity.Colonia();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                usuario = ControlUsuario.Obtener(usId);
                int idmovimiento = 0;

                for (int i = 0; i < datos.Length; i++)
                {
                    interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[i].ToString()));
                    general = interno != null ? ControlGeneral.ObtenerPorDetenidoId(interno.Id) : null;
                    sexo = general != null ? ControlCatalogo.Obtener(general.SexoId, 4) : null;
                    estadoCivil = general != null ? ControlCatalogo.Obtener(general.EstadoCivilId, 26) : null;
                    escolaridad = general != null ? ControlCatalogo.Obtener(general.EscolaridadId, 15) : null;
                    nacionalidad = general != null ? ControlCatalogo.Obtener(general.NacionalidadId, 28) : null;
                    domicilio = interno != null ? ControlDomicilio.ObtenerPorId(interno.DomicilioId) : null;
                    colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null : null;
                    municipio = colonia != null ? ControlMunicipio.Obtener(colonia.IdMunicipio) : null;
                    estado = municipio != null ? ControlEstado.Obtener(municipio.EstadoId) : null;
                    infoDetencion = interno != null ? ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id) : null;
                    unidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.UnidadId, 82) : null;
                    responsableUnidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.ResponsableId, 83) : null;
                    coloniaDetencion = (infoDetencion != null) ? ControlColonia.ObtenerPorId(infoDetencion.ColoniaId) : null;

                    var motivoE = "";
                    var evento = ControlEvento.ObtenerById(infoDetencion.IdEvento);
                    var detenidoevento = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);
                    if(detenidoevento!=null)
                    {
                        var motivo = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                        if (motivo != null) motivoE = motivo.NombreMotivoLlamada;
                    }
                    

                    string idInterno = interno != null ? interno.Id.ToString() : "";
                    string[] internod = new string[2] {
                        idInterno, "true"
                    };

                    estatusInterno = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                    calificacion = estatusInterno != null ? ControlCalificacion.ObtenerPorInternoId(estatusInterno.Id) : null;
                    situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;
                    centroReclusion = estatusInterno != null ? ControlInstitucion.ObtenerPorId(estatusInterno.CentroId) : null;

                    if (ValidaReglaDeNegocioSalidaEfectuadaBiometricos(estatusInterno.DetenidoId, estatusInterno.ContratoId) != "OK")
                    {
                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("El detenido " + detenidoNombre + " no tiene biométricos registrados,  ");
                        continue;
                    }

                    if (ValidaReglaDenegocioExamenmedico(estatusInterno.Id, estatusInterno.ContratoId) != "OK")
                    {

                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("El detenido " + detenidoNombre + " no se le ha realizado un examen médico,  ");
                        continue;

                    }

                    if (calificacion != null)
                    {
                        

                        if (!calificacion.SoloArresto)
                        {
                            if (calificacion.TotalAPagar > 0)
                            {
                                //Obtener la calificacion_ingreso por id de la calificacion
                                Entity.CalificacionIngreso calificacionIngreso = ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacion.Id);
                                if (calificacionIngreso == null)
                                {
                                    string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                                    listaErrores.Add("El detenido " + detenidoNombre + " tiene el pago de multa pendiente,  ");
                                    continue;
                                }
                            }
                        }
                        var amparo = ControlDetenidoAmparo.ObtenerPorDetalleDetencionId(calificacion.InternoId);
                        if(amparo !=null)
                        {
                            amparo.Activo = false;
                            ControlDetenidoAmparo.Actualizar(amparo);
                        }
                        
                    }

                    salida_efectuada.Activo = 1;
                    salida_efectuada.Habilitado = 1;
                    salida_efectuada.Creadopor = usId;
                    salida_efectuada.DetalledetencionId = estatusInterno != null ? estatusInterno.Id : 0;
                    salida_efectuada.Fundamento = fundamento;
                    salida_efectuada.TrackingId = Guid.NewGuid().ToString();
                    salida_efectuada.TiposalidaId = Convert.ToInt32(tiposalidaid);

                    if (calificacion != null)
                    {
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        idmovimiento = ControlSalidaEfectuadaJuez.Guardar(salida_efectuada);
                        if (idmovimiento > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = estatusInterno.Id;
                            historial.Movimiento = "Salida efectuada del detenido";
                            historial.ContratoId = subcontrato.Id;
                            historial.TrackingId = estatusInterno.TrackingId;
                            historial.Id = ControlHistorial.Guardar(historial);

                            Entity.Historial historial1 = new Entity.Historial();
                            historial1.Activo = true;
                            historial1.CreadoPor = usId;
                            historial1.Fecha = DateTime.Now;
                            historial1.Habilitado = true;
                            historial1.ContratoId = subcontrato.Id;
                            historial1.InternoId = estatusInterno.Id;
                            historial1.Movimiento = "Solicitud de amparo aceptada";
                            historial1.TrackingId = estatusInterno.TrackingId;
                            historial1.Id = ControlHistorial.Guardar(historial1);
                            
                        }

                    }
                    else
                    {
                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("El detenido " + detenidoNombre + " no se le ha realizado una calificación,  ");
                        continue;
                    }
                    if (tiposalidaid == "7")
                    {
                        var calificacioningreso_ = ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacion.Id);
                        if (calificacioningreso_ == null)
                        {
                            string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                            listaErrores.Add("El detenido " + detenidoNombre + " no tiene registrado el pago de una multa,  ");
                            continue;
                        }
                    }

                    if (calificacion != null)
                    {
                        if (calificacion.TrabajoSocial)
                        {
                            //Obtener el trabajo social del interno
                            trabajoSocial = estatusInterno != null ? ControlTrabajoSpcial.ObtenerByDetenidoId(estatusInterno.Id) : null;
                            //Luego desactivarlo
                            trabajoSocial.Activo = 0;

                            ControlTrabajoSpcial.SalidaEfectuada(trabajoSocial);

                        }
                    }

                    //Generar autorizacion de salida

                    DateTime fecha = general != null ? general.FechaNacimineto : new DateTime();
                    TimeSpan time = DateTime.Today.Subtract(fecha);
                    var edad = "";
                    int auxEdad;
                    if (time.Days > 700000)
                    {
                        edad = "Sin Dato";
                        edad = general.Edaddetenido.ToString();
                    }

                    else
                    {
                        auxEdad = Convert.ToInt32(time.TotalDays) / 365;
                        edad = auxEdad.ToString();
                    }
                    List<Entity.Unidad> unidades = new List<Entity.Unidad>();

                    unidades = ControlUnidad.ObtenerPorEventoId(infoDetencion.IdEvento);

                    var responsableunidad = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { unidades.Count > 0 ? unidades.LastOrDefault().Id : 0, infoDetencion.IdEvento });
                    var registro = "";
                    if (estatusInterno != null)
                    {
                        var f = Convert.ToDateTime(estatusInterno.Fecha);
                        var h = f.Hour.ToString();
                        if (f.Hour < 10) h = "0" + h;
                        var m = f.Minute.ToString();
                        if (f.Minute < 10) m = "0" + m;
                        var s = f.Second.ToString();
                        if (f.Second < 10) s = "0" + s;
                        registro = f.ToShortDateString() + " " + h + ":" + m + ":" + s;
                    }
                    var fecha1 = "";
                    var f1 = DateTime.Now;
                    var h1 = f1.Hour.ToString();
                    if (f1.Hour < 10) h1 = "0" + h1;
                    var m1 = f1.Minute.ToString();
                    if (f1.Minute < 10) m1 = "0" + m1;
                    var s1 = f1.Second.ToString();
                    if (f1.Second < 10) s1 = "0" + s1;
                    fecha1 = f1.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;
                    object[] data = new object[]
                    {
                        (centroReclusion != null) ? centroReclusion.Nombre : "",
                        (usuario != null) ? usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno : "",
                        (estatusInterno != null) ? estatusInterno.Expediente : "",
                        registro,
                        (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                        edad,
                        sexo != null ? sexo.Nombre : "",
                        estadoCivil != null ? estadoCivil.Nombre : "",
                        escolaridad != null ? escolaridad.Nombre : "",
                        nacionalidad != null ? nacionalidad.Nombre : "",
                        "",
                        domicilio != null ? domicilio.Calle + " " + domicilio.Numero : "",
                        colonia != null ? colonia.Asentamiento : "",
                        municipio != null ? municipio.Nombre : "",
                        estado != null ? estado.Nombre : "",
                        domicilio != null ? domicilio.Telefono : "",
                         unidades.Count>0?unidades.LastOrDefault().Placa  : "",
                        (responsableunidad != null) ? responsableunidad.Descripcion.ToString() : "",
                        (responsableunidad != null) ? responsableunidad.Nombre : "",
                        (infoDetencion != null) ? infoDetencion.Id.ToString() : "",
                        motivoE,
                        (infoDetencion != null) ? infoDetencion.LugarDetencion : "",
                        (coloniaDetencion != null) ? coloniaDetencion.Asentamiento : "",
                        fecha1,
                        usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno,
                        situacion != null ? situacion.Nombre : "",
                        calificacion != null ? calificacion.SoloArresto ? "Si" : "No" : "",
                        calificacion != null ? calificacion.TotalHoras : 0,
                        calificacion != null ? calificacion.TotalAPagar : 0,
                        calificacion != null ? calificacion.Fundamento : "",
                        calificacion != null ? calificacion.Razon : "",
                        i.ToString()
                    };

                    object[] dataArchivo = ControlPDF.generarAutorizacionSalida(data);

                    Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                    reportesLog.ProcesoId = interno.Id;
                    reportesLog.ProcesoTrackingId = interno.TrackingId.ToString();
                    reportesLog.Modulo = "Detenidos en amparo";
                    reportesLog.Reporte = "Autorización de salida";
                    reportesLog.Fechayhora = DateTime.Now;
                    reportesLog.EstatusId = 1;
                    reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    ControlReportesLog.Guardar(reportesLog);
                    //Guardar los archivos generados
                    if (dataArchivo != null)
                    {
                        arrayList.Add(dataArchivo[1]);
                    }

                    if (idmovimiento > 0)
                    {
                       
                            estatusInterno.Estatus = 2;
                            estatusInterno.Activo = false;
                            ControlDetalleDetencion.Actualizar(estatusInterno);
                        

                    }
                }

                foreach (var item in listaErrores)
                {
                    errores += item;
                }

                return JsonConvert.SerializeObject(new { exitoso = true, archivos = arrayList, mensaje = "La salida efectuada se generó exitosamente", errores = errores });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
            {
                try
                {
                    
                    if (!emptytable)
                    {
                        if (order.Count > 1)
                        {
                            order.FirstOrDefault().column = 3;
                        }
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        List<Where> where2 = new List<Where>();
                        List<Join> join = new List<Join>();


                        join.Add(new Join(new Table("general", "g"), "I.id  = g.DetenidoId"));
                        join.Add(new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"));
                        join.Add(new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"));
                        join.Add(new Join(new Table("Motivo_detencion_Interno", "T"), "T.InternoId  = E.Id", "LEFT"));
                        join.Add(new Join(new Table("calificacion", "ca"), "ca.InternoId  = E.Id"));
                        join.Add(new Join(new Table("situacion_detenido", "sd"), "sd.Id  = ca.SituacionId"));
                        join.Add(new Join(new Table("calificacion_ingreso", "ci"), "ci.CalificacionId = ca.Id", "LEFT"));
                        join.Add(new Join(new Table("vdetenidohorasarestoAmparo", "V"), "v.DetalleDetencionId = E.Id", "LEFT"));
                        join.Add(new Join(new Table("salidaefectuadajuez", "Saj"), "E.id=Saj.DetalleDetencionId and Saj.Activo=1"));


                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        int IdContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(IdContrato);

                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("E.Tipo", contratoUsuario.Tipo));
                        var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                        where.Add(new Where("Saj.TiposalidaId", tipo.Id.ToString()));
                        where.Add(new Where("E.Estatus","<>", "2"));
                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                                "distinct (I.Id) Id",
                                "I.TrackingId",
                                "ltrim(I.Nombre) Nombre",
                                "ltrim(I.Paterno) Paterno",
                                "ltrim(I.Materno) Materno",
                                "I.RutaImagen",
                                "CAST( E.Expediente AS unsigned) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus" ,
                                "ES.Nombre EstatusNombre",
                                "ifnull(T.internoId,0) DetalledetencionId",
                                "ifnull (sd.Nombre, 'Pendiente') Situacion",
                                "ifnull(ca.Id,0) CalificacionId",
                                "ci.IngresoId",
                                //"timediff(date_add(E.Fecha,interval ifnull(ca.totalHoras,0) hour),CURRENT_TIMESTAMP() ) Horas ",
                                "ca.TotalAPagar",
                                "ifnull(V.horas,0) Horas",
                                "g.FechaNacimineto",
                                "E.Fecha",
                                "Saj.TrackingId TrackingSalida",
                                "concat(I.nombre,' ',TRIM(Paterno),' ',TRIM(Materno)) NombreCompleto"
                            },
                            from = new Table("detenido", "I"),
                            joins = join,
                            
                            wheres = where,
                            orderBy = "E.Fecha"
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var result1 = dt.Generar(query, draw, start, length, search, order, columns);
                        return result1;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static List<Combo> GetTipoSalida_Combo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(c => c.Habilitado))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                if (detalleDetencion == null)
                {

                    detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                }

                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                Entity.Colonia colonia = domicilio != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;
                Entity.InformacionDeDetencion infoDet = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                Entity.Evento evento2 = ControlEvento.ObtenerById(infoDet.IdEvento);
                Entity.Antropometria antro = ControlAntropometria.ObtenerPorDetenidoId(interno.Id);
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(infoDet.IdDetenido_Evento);
                var motivo = "";
                if (detenidoevento !=null)
                {
                    var mot = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                    if (mot != null) motivo = mot.NombreMotivoLlamada;
                }
                var edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);
                    else
                    {
                        var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                        var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                        var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                        Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                        foreach (var item in detenidosEvento)
                        {
                            if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                detenidoEvento = item;
                        }

                        if (detenidoEvento != null) edad = detenidoEvento.Edad;
                    }
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = "Calle: " + domicilio.Calle + " #" + domicilio.Numero + "";

                }

                string strColonia = "";
                if (colonia != null)
                {
                    strColonia = ",Colonia: " + colonia.Asentamiento + " CP:" + colonia.CodigoPostal + "";

                }

                string domCompleto = strDomicilio + "" + strColonia;

                object obj = null;

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = evento2.HoraYFecha;
                TimeSpan horasRestantes;
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                var situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;

                if (calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }
                else
                {
                    
                    fechaSalidaAdd = DateTime.MinValue;
                    horasRestantes = TimeSpan.MinValue;
                }
                var histo = ControlHistorial.ObtenerPorDetenido(detalleDetencion.Id).Where(x => x.Movimiento == "Solicitud de amparo registrada" || x.Movimiento == "Salida efectuada del detenido");
                foreach(var itm in histo)
                {
                    fechaSalidaAdd = itm.Fecha;
                }
                if (interno != null)
                {
                    if (string.IsNullOrEmpty(interno.RutaImagen))
                    {
                        interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                    }
                }
                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = situacion != null ? situacion.Nombre : "",
                    Domicilio = domCompleto,
                    RutaImagen = interno.RutaImagen,
                    Registro = detalleDetencion.Fecha.Value.ToString("dd-MM-yyyy HH:mm:ss"),
                    Salida = fechaSalidaAdd != DateTime.MinValue ? fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                    restantes = horasRestantes != TimeSpan.MinValue ? horasRestantes.TotalHours : 0,
                    Motivo = motivo,
                    Institucion = institucion != null ? institucion.Nombre : "",
                    Estatura = antro != null ? antro.Estatura.ToString() : 0.ToString(),
                    Peso = antro != null ? antro.Peso.ToString() : "0.00"
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}