﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using Entity.Util;
using System.Linq;

namespace Web.Application.Trabajo_social
{
    public partial class EstudioDiganostico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();

        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Trabajo social" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //Consultar
                        this.WERQEQ.Value = permisos.Consultar.ToString().ToLower();

                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static List<Combo> llena_Combo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(Convert.ToInt32(item));

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(F => F.Habilitado))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static string ValidateFecha(string[] info)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(info[0]));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalle = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                //**v*/ar detalle = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(info[0]));*/
                var fecha = Convert.ToDateTime(info[1]);
                int result = DateTime.Compare(fecha, Convert.ToDateTime(detalle.Fecha));
                Boolean validate = true;
                if (result < 0)
                {
                    validate = false;
                }
                var fechar = Convert.ToDateTime(detalle.Fecha);
                var h = fechar.Hour.ToString();
                if (fechar.Hour < 10) h = "0" + h;
                var m = fechar.Minute.ToString();
                if (fechar.Minute < 10) m = "0" + m;
                var s = fechar.Second.ToString();
                if (fechar.Second < 10) s = "0" + s;
                var registro = fechar.ToShortDateString() + " " + h + ":" + m + ":" + s;
                return JsonConvert.SerializeObject(new { exitoso = true, Validar = validate, fechaRegistro = registro }); 
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }

        [WebMethod]
        public static Object Save(EstudioDiagnositcoAux estudioDiagnositcoAux)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(estudioDiagnositcoAux.trackingId));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                estudioDiagnositcoAux.internoId = detalleDetencion.Id.ToString();

                Entity.EstudioDiagnostico estudioDiagnostico = new Entity.EstudioDiagnostico();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                estudioDiagnostico.Activo = 1;
                estudioDiagnostico.Habilitado = 1;
                estudioDiagnostico.Creadopor = 13;
                estudioDiagnostico.DetalledetencionId = Convert.ToInt32(estudioDiagnositcoAux.internoId);
                estudioDiagnostico.Fechayhora = Convert.ToDateTime(estudioDiagnositcoAux.fechayhora);
                estudioDiagnostico.TipodediagnosticoId = Convert.ToInt32(estudioDiagnositcoAux.tipodediagnosticoId);
                estudioDiagnostico.Observaciones = Convert.ToString(estudioDiagnositcoAux.observaciones);
                estudioDiagnostico.TrackingId = Guid.NewGuid().ToString();
                estudioDiagnostico.Detallediagnostico = estudioDiagnositcoAux.detallediganostico;

                int idmovimiento = ControlEstudioDiagnostico.Guardar(estudioDiagnostico);

                Entity.Historial historial = new Entity.Historial();
                historial.Activo = true;
                historial.CreadoPor = usId;
                historial.Fecha = DateTime.Now;
                historial.Habilitado = true;
                historial.InternoId = detalleDetencion.Id;
                historial.Movimiento = "Modificación de datos generales en diagnóstico del trabajo social";
                historial.TrackingId = Guid.NewGuid();
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                historial.ContratoId = subcontrato.Id;
                historial.Id = ControlHistorial.Guardar(historial);



                return new { exitoso = true, mensaje = "La información fue registrada satisfactoriamente", Id = "", TrackingId = "" };
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable,string TrackingProcesoId)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Trabajo social" }).Consultar)
            {
                try
                {
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(TrackingProcesoId));
                    string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };

                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Id", detalleDetencion.Id.ToString()));
                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Estatus", "<>", "2"));
         
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
             
                        }
                        Query query = new Query
                        {
                            select = new List<string> {


                        "D.Id Id",
                        "D.TrackingId",
                        "I.Nombre",
                        "I.Paterno",
                        "I.Materno",
                        "I.RutaImagen",
                        "E.Expediente",
                        "E.NCP",
                        "E.Estatus",
                        "ifnull(SD.Nombre,'') situacion",
                        "E.Activo",
                        
                 
                    },
                        
                            from = new Table("Diagnostico", "D"),
                            joins = new List<Join>
                    {
                        new Join(new Table("detalle_detencion", "E"), "E.id  = D.DetalleDetencionId"),
                        new Join(new Table("detenido", "I"), "I.id  = E.DetenidoId"),
                         new Join(new Table("Calificacion", "C"), "C.InternoId  = E.Id"),
                         new Join(new Table("situacion_detenido", "SD"), "SD.Id = C.SituacionId"),
                        new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus")
                       
                    },
                            wheres = where
                        };

                        if (contratoUsuario.Tipo == "contrato")
                        {
                            query.joins.Add(new Join(new DT.Table("contrato", "co"), "co.id  = E.ContratoId"));
                            where.Add(new Where("E.Tipo", "=", "contrato"));
                            where.Add(new Where("E.ContratoId", "=", contratoUsuario.IdContrato.ToString()));
                        }
                        else if (contratoUsuario.Tipo == "subcontrato")
                        {
                            query.joins.Add(new Join(new DT.Table("subcontrato", "co"), "co.id  = E.ContratoId"));
                            where.Add(new Where("E.Tipo", "=", "subcontrato"));
                            where.Add(new Where("E.Tipo", "=", "subcontrato"));
                            where.Add(new Where("E.ContratoId", "=", contratoUsuario.IdContrato.ToString()));
                        }

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable dt_ = new DataTable();
                        dt_= dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static Object GetDiagnosticoByTrackingId(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Trabajo social" }).Consultar)

                {

                    var estudio = ControlEstudioDiagnostico.ObtenerByTrackingId(trackingid);
                    EstudioDiagnositcoAux estudioDiagnositco = new EstudioDiagnositcoAux();
                    if (estudio != null)
                    {

                        estudioDiagnositco.internoId = estudio.DetalledetencionId.ToString();
                        estudioDiagnositco.tipodediagnosticoId = estudio.TipodediagnosticoId.ToString();
                        estudioDiagnositco.detallediganostico = estudio.Detallediagnostico;
                        estudioDiagnositco.fechayhora = estudio.Fechayhora.ToString("dd/MM/yyyy HH:mm");
                        estudioDiagnositco.observaciones = estudio.Observaciones.ToString();
                        estudioDiagnositco.trackingId = estudio.TrackingId.ToString();

                    }

                    return estudioDiagnositco;

                }
                else
                {
                    return new { exitoso = false, mensaje = "No se encontro informacion referente al estado seleccionado." };
                }
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message };
            }

        }
        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
        [WebMethod]
        public static string getdatos2(string trackingid)
        {
            try
            {

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Trabajo social" }).Consultar)
                {
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);

                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                    Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                             edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                            var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                            Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                            foreach (var item in detenidosEvento)
                            {
                                if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                    detenidoEvento = item;
                            }

                            if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                            else
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }

                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }
                    edad = detalleDetencion.DetalledetencionEdad;
                    var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                    Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                    var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                    Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                    object obj = null;


                    obj = new
                    {
                        Id = interno.Id.ToString(),
                        TrackingId = interno.TrackingId.ToString(),
                        Expediente = detalleDetencion.Expediente,
                        Centro = institucion != null ? institucion.Nombre : "Institución no registrada",
                        Fecha = detalleDetencion.Fecha != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                        RutaImagen = interno.RutaImagen != null ? interno.RutaImagen : "",
                        Edad = edad != 0 ? edad.ToString() : "Fecha de nacimiento no registrada",

                        Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Historial historial = ControlHistorial.ObtenerPorDetenido(detalleDetencion.DetenidoId).FirstOrDefault();
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                Entity.Colonia colonia = domicilio != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;

                DateTime nacimiento;
                int edad = 0;
                if (general != null)
                {
                    nacimiento = general.FechaNacimineto;
                    edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = "Calle: " + domicilio.Calle + " #" + domicilio.Numero + "";

                }

                string strColonia = "";
                if (colonia != null)
                {
                    strColonia = ",Colonia: " + colonia.Asentamiento + " CP:" + colonia.CodigoPostal + "";

                }

                string domCompleto = strDomicilio + "" + strColonia;

                object obj = null;

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = detalleDetencion.Fecha.Value;
                TimeSpan horasRestantes;
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                var situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;

                if (calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }
                else
                {
                    fechaSalidaAdd = fechaSalida.AddHours(36);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = situacion != null ? situacion.Nombre : "",
                    Domicilio = domCompleto,
                    RutaImagen = interno.RutaImagen,
                    //Registro = detalleDetencion.Fecha.Value.ToString("dd-MM-yyyy HH:mm:ss"),
                    Registro = historial.Fecha.ToString("dd-MM-yyyy HH:mm:ss"),
                    Salida = fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss"),
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                    horasRestantes.TotalHours
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
    public class EstudioDiagnositcoAux
    {
        public string fechayhora { get; set; }
        public string tipodediagnosticoId { get; set; }
        public string observaciones { get; set; }
        public string internoId { get; set; }
        public string trackingId { get; set; }
        public string detallediganostico { get; set; }


    }

}