﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="EstudioDiagnostico.aspx.cs" Inherits="Web.Application.Trabajo_social.EstudioDiganostico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Trabajo_social/Trabajosocial.aspx">Trabajo social</a></li>
    <li>Diagnóstico</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #dropdown {
            width: 439px;
        }
        .select2-dropdown span:first-child{
            display:none;
        }
          #ScrollableContent {
            height: calc(100vh - 400px);
        }
        .same-height-thumbnail{
            height: 200px;
            margin-top:0;
        }
         .margin-sides-10 label {
            margin-right: 10px;
            margin-left: 10px;
        }
    </style>
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
                Trabajo social
            </h1>
        </div>
    </div>
    <div class="row" style="display: none;">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <section id="widget-grid" class="">
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" id="btninfo" class="btn btn-md btn-primary detencion"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
     
     

        <div class="scroll" id="ScrollableContent">
            
            <div class="row" id="addentry" style="display: none; margin-left:4px">

                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                        <a class="btn btn-md btn-default  regresar" id="regresar" href="Trabajosocial.aspx"><i class="fa fa-mail-reply"></i> Regresar </a>
                </article>
                <br />
                 <br />
                <br />
            </div>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-users-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar detenido </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="select">
                                                <select name="peril" id="registro" runat="server">
                                                    <option value="0">[Perfil]</option>
                                                </select>
                                                <i></i>
                                            </label>
                                        </section>

                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="nombre" id="nombre" runat="server" placeholder="Nombre" maxlength="256" />
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            

            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" " data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Estudios o diagnósticos </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th >No. remisión</th>
                                        <th >Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <th data-hide="phone,tablet">Situación del detenido</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
 
        <%--<div id="add-datosPersonales" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">--%>
       
     
<div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--  --%>
     

     <div id="printpdf-modal" class="modal fade" tabindex="-1" data-keyboard="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Reporte de extravío
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="printpdf-modal-body" class="smart-form">

                        <div class="modal-body">
                            <div id="printpdf-modal-body-form" class="smart-form">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="row">
                                                <section class="col col-10">
                                                    <img id="avatar2" class="img-thumbnail" alt="" runat="server" src="~/Content/img/avatars/male.png" width="120" height="120" />
                                                </section>
                                            </div>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Nombre <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" readonly="true" name="nombrereportepdf" class="alptext" id="nombrereportepdf" placeholder="Nombre" />
                                                        <b class="tooltip tooltip-bottom-right">Nombre</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Edad <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="edadreportepdf" id="edadreportepdf" class="alptext" placeholder="Edad" class="integer" maxlength="2" />
                                                        <b class="tooltip tooltip-bottom-right">Edad</b>
                                                    </label>
                                                </section>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioBuscandopdf" value="BUSCADO(A)" /><i></i>Buscado
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioExtraviadopdf" value ="EXTRAVIADO(A)" /><i></i>Extraviado
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioInformacionpdf" value="INFORMACION" /><i></i>Información
                                            </label>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Alias <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="aliasreportepdf" id="aliasreportepdf" class="alptext" placeholder="Alias" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Alias</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Violento</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="violentopdf" id="violentopdf" class="alptext" placeholder="Violento" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Violento</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Enfermo mental</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="enfermopdf" id="enfermopdf" class="alptext" placeholder="Enfermo mental" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Enfermo mental</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Señas particulares</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="textarea">
                                                        <textarea rows="3" name="senaspdf" class="alptext" id="senaspdf" placeholder="Señas particulares" maxlength="500"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Señas particulares</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Comentarios</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="textarea">
                                                        <textarea rows="3" name="comentariospdf" class="alptext" id="comentariospdf" placeholder="Comentarios" maxlength="500"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Comentarios</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-9">
                                                    <label>Información de contacto:</label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Institución</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="institucionpdf" class="alptext" id="institucionpdf" placeholder="Institución" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Institución para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Dirección</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="direccionpdf" class="alptext" id="direccionpdf" placeholder="Dirección" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Dirección para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Teléfono</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="telefonopdf" class="alptext" id="telefonopdf" placeholder="Telefono" maxlength="50" />
                                                        <b class="tooltip tooltip-bottom-right">Teléfono para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                        </div>

                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <input type="hidden" id="trackingidpdf"/>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal" id="cancelarpdfbtn"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default printpdfbtn" id="printpdfbtn"><i class="fa fa-file-pdf-o"></i>&nbsp;Generar reporte </a>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>
     
    <div id="RegistroExpediente-modal" class="modal fade"  tabindex="-1" data-backdrop="static" data-keyboard="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class="fa fa-pencil" +=""></i> Estudio o diagnóstico</h4>
                </div>
                <div class="modal-body">
                    <div id="registromovimiento-form" class="smart-form">
                        <div class="modal-body">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <label style="color: dodgerblue" class="label">Fecha y hora <a style="color: red">*</a></label>
                                        <label class="input">
                                            <label class='input-group date' id='FehaHoradatetimepicker'>
                                                <input type="text" name="fechahora" id="fechahora" class='input-group date alptext' placeholder="Fecha y hora de ingreso" data-requerido="true"/>
                                                <b class="tooltip tooltip-bottom-right">Ingrese la fecha y hora.</b>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            </label>
                                        </label>
                                        <i></i>
                                    </section>
                                    <section class="col col-6">
                                        <label style="color: dodgerblue" class="label">Tipo de estudio o diagnóstico <a style="color: red">*</a></label>
                                        <select name="diagnostico" id="diagnostico" class="select2" style="width:100%"></select>
                                        <i></i>
                                    </section>
                                    <section class="col col-6">
                                        <label style="color: dodgerblue" class="label">Detalle del estudio o diagnóstico <a style="color: red">*</a></label>
                                        <label class="textarea">
                                            <textarea id="detallediagnostico" name="detallediagnostico" style="width:100%" maxlength="255" rows="5" class="alptext" placeholder="Detalle del estudio o diagnóstico"></textarea>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el detalle del estudio o diagnóstico.</b>
                                        </label>                                                        
                                    </section>
                                    <section class="col col-6">
                                        <label style="color: dodgerblue" class="label">Observación</label>
                                        <label class="textarea">
                                            <textarea id="observaciones" name="observaciones"  maxlength="255" class="alptext" style="width:100%" rows="5"></textarea>
                                            <b class="tooltip tooltip-bottom-right">Ingrese la observación.</b>
                                        </label>                                                        
                                    </section>
                                </div>
                            </fieldset>
                        </div>                        
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveregistro" id="guardarexpediente"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <%--  --%>
    <div id="RegistroExpediente2-modal" class="modal fade"  tabindex="-1" data-backdrop="static" data-keyboard="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class="fa fa-pencil" +=""></i> Estudio o diagnóstico
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="registromovimiento2-form" class="smart-form">
                        <div class="modal-body">
                            <fieldset>
                                <div  class="Row">
                                    <section class="col col-6">
                                        <label style="color: dodgerblue" class="label">Fecha y hora </label>
                                        <label class="input">
                                            <label class='input-group date' id='FehaHoradatetimepicker2'>
                                                <input type="text" disabled="disabled" name="fechahora" id="fechahora2" class='input-group date alptext' placeholder="Fecha y hora de ingreso" data-requerido="true"/>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    <b class="tooltip tooltip-bottom-right">Ingrese la fecha y hora.</b>
                                                </span>
                                            </label>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label style="color: dodgerblue" class="label">Tipo de estudio o diagnóstico</label>
                                        <label class="select"> 
                                            <select name="diagnostico2" id="diagnostico2" disabled="disabled"></select>
                                        </label>     
                                        <i></i>
                                    </section>
                                    <section  class="col col-6">
                                        <label style="color: dodgerblue" class="label">Detalle del estudio o diagnóstico</label>
                                        <label class="textarea">
                                            <textarea id="detallediagnostico2" disabled="disabled" name="detallediagnostico2" class="alptext" cols="30" rows="5" placeholder="Detalle del estudio o diagnóstico"></textarea>                                                            
                                            <b class="tooltip tooltip-bottom-right">Ingrese el detalle del estudio o diagnóstico.</b>
                                        </label>                                                        
                                    </section>
                                    <section class="col col-6">
                                        <label style="color: dodgerblue" class="label">Observación</label>
                                        <label class="textarea">
                                            <textarea id="observaciones2" disabled="disabled" name="observaciones"  class="alptext" cols="30" rows="5"></textarea>  
                                            <b class="tooltip tooltip-bottom-right">Ingrese la observación.</b>
                                        </label>                                                        
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                             <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela2"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="photo-arrested" class="modal fade" tabindex="-1" data-keyboard="true" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="#" alt="fotografía del detenido" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--  --%>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="WERQEQ" runat="server" value=""/>
    <input type="hidden" id="CeldaTrackingId"  value="" />
    <input type="hidden" id="hide" />
    <input type="hidden" id="hideid" runat="server" />
    <input type="hidden" id="Hidden1" runat="server" value="" />
    <input type="hidden" id="Hidden2" runat="server" value="" />
     <input type="hidden" id="Hidden3" runat="server" value=""/>
    <input type="hidden" id="FechaRegistro" />
    <input type="hidden" id="validarfecha" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            window.addEventListener("keydown", function (e) {
                if (e.ctrlKey && e.keyCode === 71) {
                    e.preventDefault();
                    if ($("#RegistroExpediente-modal").is(":visible")) {
                        document.getElementById("guardarexpediente").click();
                    }
                }
                
                
            });


            pageSetUp();
            CargarCombodiagnostico(121);
            CargarCombodiagnostico2(121);
            limpiarModal();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
          
            
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $("#diagnostico").select2()

            
                var param = RequestQueryString("tracking");
            if (param != undefined) {
              
                CargarDatosInterno(param);
                
            }
              function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
              
              
                if (url.indexOf("~/") == 0) {
                     
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "scrollY": "350px",
                "scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "EstudioDiagnostico.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                            var param = RequestQueryString("tracking");
                        
                        parametrosServerSide.emptytable = false;
                        if (param != undefined) {
                            parametrosServerSide.TrackingProcesoId = param;
                        }
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    
                    null,
                    null,
                      {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
               
                  
                    null,
                    null
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },

                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != "" && row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");

                                var imgAvatar = resolveUrl(photo);

                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatarx" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" />' +
                                    '</a>' +
                                    '<div>';
                            }
                             else {
                                pathfoto = resolveUrl("~/Content/img/avatars/male.png");
                                return '<div class="text-center">'+
                                    '<img id="avatarx" class="img-thumbnail text-center" alt = "" src = "' + pathfoto + '" height = "10" width = "50"/>' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var estatus = row.situacion;                               

                            return estatus;
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var edit = "edit";
                            var editar = "";
                            var color = "";
                            var txtestatus = "";
                            var icon = "";
                            var habilitar = "";

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                            }


                         //   if ($("#ctl00_contenido_KAQWPK").val() == "true")
                              //  editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                           // else if($("#ctl00_contenido_WERQEQ").val() == "true")
                               // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Proceso + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';

                             var action2 = "";
                            var motivo = "";
                            action2='&nbsp;<a class="btn btn-primary  ' + motivo + '"data-tracking="' + row.TrackingId + '"  id="idconsultar" title="Consultar"><i class="fa fa-eye"></i>&nbsp;Consultar</a>&nbsp;';

                            
                            
                            //var action5 = "";
                            //action5 = '&nbsp;<a class="btn datos btn-primary '+vacio+  '"  href="#"  id="datos" data-id="' + row.TrackingId + '" title="Datos personales">Datos personales</a>&nbsp;';
                            return action2;

                        }
                    }
                ]

            });

            $(document).on("click", "#idconsultar", function () {
                var tracking = $(this).attr("data-tracking");
                
                CargarEstudio(tracking);
                $("#RegistroExpediente2-modal").modal("show");
            })
            
            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);
                $("#photo-arrested").modal("show");
            });
            $("body").on("click", "#idexpediente", function () {
                 var CeldaTrackingId = $(this).attr("data-tracking");
                $("#hide").val(CeldaTrackingId);
              
                $("#RegistroExpediente2-modal").modal("show");

            });
            $(document).on("click", "#guardarexpediente", function () {
                if (validarregistroExpediente())
                {
                    guardarExpedienteTrabajoSocial();

                }
            });


            $("body").on("click", "#datospersonales", function () {
                //var id = $(this).attr("data-id");
                    var param = RequestQueryString("tracking");
                if (param != undefined) {
                    //MovimientoCelda.TrackingId = param;
                   
                    CargarDatosModal(param);
                }
               
               
                
            });
            //function CargarDatosPersonales(trackingid) {
            //    //jc
                
            //    startLoading();
            //      $.ajax({
            //          type: "POST",
            //          url: "EstudioDiagnostico.aspx/getdatosPersonales",
            //          contentType: "application/json; charset=utf-8",
            //          dataType: "json",
            //          data: JSON.stringify({
            //              trackingid: trackingid,
            //          }),
            //          cache: false,
            //          success: function (data) {
            //              var resultado = JSON.parse(data.d);
            //              if (resultado.exitoso) {
                              
            //                  $('#idnombreInterno').val(resultado.obj.Nombre);
            //                  $('#edad').val(resultado.obj.Edad);
            //                  $('#situacion').val(resultado.obj.Situacion);
            //                  var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
            //                 // alert(imagenAvatar);
            //                  $('#ctl00_contenido_avatar3').attr("src", imagenAvatar)
            //                   //$('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);

                              
            //                  $('#horaRegistro').val(resultado.obj.Registro);
            //                  $('#salida').val(resultado.obj.Salida);
            //                  $('#domicilio').val(resultado.obj.Domicilio);
            //                      $("#add-datosPersonales").modal("show");
            //              } else {
            //                  ShowError("Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

            //              }

            //              $('#main').waitMe('hide');

            //          },
            //          error: function () {
            //              $('#main').waitMe('hide');
            //              ShowError("Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
            //          }
            //      });
            //}
           




         

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");

                window.emptytable = true;
                window.table.api().ajax.reload();
            });


            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "sentence_entrylist.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Bien hecho!</strong>" +
                                "El registro  se actualizó correctamente.</div>");
                            ShowSuccess("¡Bien hecho!", "El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });
            //JGB 22052019
            function CargarCombodiagnostico2(id) {

                $.ajax({

                    type: "POST",
                    url: "Trabajosocial.aspx/llena_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#diagnostico2');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Tipo de estudio o diagnóstico]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(0);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de adicciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            
            function CargarCombodiagnostico(id) {
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/llena_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#diagnostico');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Tipo de estudio o diagnóstico]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(0);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de adicciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarListado(id) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getListadoCombo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_dropdown');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Delegación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("body").on("click", ".edit", function () {
                $('#FehaHoradatetimepicker').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm:ss'
                });
                var nombre = $(this).attr("data-value");
                $("#IdNombreInterno").text( nombre);
               
                $("#trasladoInterno-modal").modal("show")
                $("#guardatraslado").attr("TrackingId", $(this).attr("data-tracking"));
                
                CargarListado(0);
                window.emptytableadd = false;
                window.tableadd.api().ajax.reload();
            });

            $("body").on("click", ".save", function () {
                var tracking = $(this).attr("TrackingId")
                if (validarTraslado()) {
                    guardar(tracking);
                    $("#trasladoInterno-modal").modal("hide");
                }
            });

            $("body").on("click", "#btninfo", function () {
                 var param = RequestQueryString("tracking");
                if (param != undefined) {
                    $("#datospersonales-modal").modal("show");
                     CargarDatosModal(param);
                }
            });


              //$("body").on("click", ".guardatraslado", function () {
              //  $("#ctl00_contenido_lblMessage").html("");
                

              //      guardar();

         

            function estudioDiagnositco() {
                var estudioDiagnositcoAux = {
                    fechayhora: $("#fechahora").val(),
                    tipodediagnosticoId: $("#diagnostico").val(),
                    observaciones: $("#observaciones").val(),
                    detallediganostico: $("#detallediagnostico").val(),

                    TrackingId: $("#hide").val()

                };
                return estudioDiagnositcoAux;
            }

            $("#btncancela").click(function () {
                limpiarModal();
            });

            function limpiarModal()
            {
                $("#fechahora").val("");
                $("#diagnostico").val("0");
                $("#observaciones").val("");
                $("#detallediagnostico").val("");
                $('#detallediagnostico').parent().removeClass('state-success').removeClass('state-error');
                $('#detallediagnostico').removeClass('valid')
                $('#fechahora').parent().removeClass('state-success').removeClass('state-error');
                $('#fechahora').removeClass('valid')
                $('#diagnostico').parent().removeClass('state-success');
                $('#diagnostico').removeClass('valid');
                $("#diagnostico").select2({ containerCss: { "border-color": "#bdbdbd" } });

            }
            function CargarDatosInterno(trackingid) {
                
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "EstudioDiagnostico.aspx/getdatos2",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        
                        if (resultado.exitoso) {
                            $("#IdInstitucion").text(resultado.obj.Centro);
                            $("#Idremision").text(resultado.obj.Expediente);
                            $("#idnimbredelInterno").text(resultado.obj.Nombre + ' ' + resultado.obj.Paterno + ' ' + resultado.obj.Materno);
                          
                           //alert(resultado.obj.RutaImagen);

                            $('#ctl00_contenido_hideid').val(resultado.obj.Id);
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                                   if (resultado.obj.RutaImagen == "")
                            {
                               imagenAvatar = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png"; 
                            }
                            $('#avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);

                            
                             $('#edadInterno').text(resultado.obj.Edad);
                            
                                    $('#sexoInterno').text(resultado.obj.Sexo);
                                

                            $('#municipioInterno').text(resultado.obj.municipioNombre);
                            $('#domicilioInterno').text(resultado.obj.domiclio);
                            $('#coloniaInterno').text(resultado.obj.coloniaNombre);
                            
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la asignación. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                    }
                });
            }
             function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
              if (url.indexOf("~/") == 0) {
                  url = baseUrl + url.substring(2);
              }
              return url;
            }



            function validarregistroExpediente() {
                var esvalido = true;

                if ($("#fechahora").val() == "" || $('#fechahora').val() == null) {
                    ShowError("Fecha y hora", "El campo fecha y hora es obligatorio.");
                    $('#fechahora').parent().removeClass('state-success').addClass("state-error");
                    $('#fechahora').removeClass('valid');
                    esvalido = false;
                }
                else {
                    var tracking = "";
                    var param = RequestQueryString("tracking");
                    if (param != undefined) {
                        tracking = param;
                    }
                    info = [
                        tracking = tracking,
                        fecha = $("#fechahora").val()
                    ];
                    var valido = validatefecha(info);
                    valido = $("#validarfecha").val();
                    
                    if (valido == "false") {
                        var f = $("#FechaRegistro").val();
                        ShowError("Fecha y hora", "La fecha y hora no puede ser menor a la fecha " + f + ".");
                        $('#fechahora').parent().removeClass('state-success').addClass("state-error");
                        $('#fechahora').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#fechahora').parent().removeClass("state-error").addClass('state-success');
                        $('#fechahora').addClass('valid');
                    }
                    $('#fechahora').parent().removeClass("state-error").addClass('state-success');
                    $('#fechahora').addClass('valid');
                }

                if ($("#diagnostico").val() == null || $("#diagnostico").val() == "" || $("#diagnostico").val() == "0") {
                    ShowError("Diagnóstico", "El tipo de estudio o diagnóstico es obligatorio.");
                    $("#diagnostico").select2({ containerCss: { "border-color": "#a90329" } });

                    esvalido = false;
                }
                else {
                    $("#diagnostico").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($("#detallediagnostico").val() == "") {
                    ShowError("Detalle del estudio o diagnóstico", "El campo detalle del estudio o diagnóstico es obligatorio.");
                    $('#detallediagnostico').parent().removeClass('state-success').addClass("state-error");
                    $('#detallediagnostico').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#detallediagnostico').parent().removeClass("state-error").addClass('state-success');
                    $('#detallediagnostico').addClass('valid');
                }



                return esvalido;
            }

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (resultado.obj.RutaImagen == "")
                            {
                               imagenAvatar = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png"; 
                            }
                            $('#imgInfo').attr("src", imagenAvatar);

                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
        

             function CargarDatosPersonales(trackingid) {
                startLoading();
                  $.ajax({
                      type: "POST",
                      url: "EstudioDiagnostico.aspx/getdatosPersonales",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                             
                              $('#idnombreInterno').val(resultado.obj.Nombre);
                              $('#edad').val(resultado.obj.Edad);
                              $('#situacion').val(resultado.obj.Situacion);
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                              $('#Img1').attr("src", imagenAvatar);
                              $('#horaRegistro').val(resultado.obj.Registro);
                              $('#salida').val(resultado.obj.Salida);
                              $('#domicilio').val(resultado.obj.Domicilio);
                                  $("#datospersonales-modal").modal("show");
                          } else {
                              ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }

            function CargarEstudio(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "EstudioDiagnostico.aspx/GetDiagnosticoByTrackingId",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;                         
                         $("#fechahora2").val(data.fechayhora);
                         $("#diagnostico2").val(data.tipodediagnosticoId);
                         $("#observaciones2").val(data.observaciones);
                         $("#detallediagnostico2").val(data.detallediganostico);
                                                                        
                                                
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
              





            $("body").on("click", ".add", function () {
                limpiarModal();
                   $('#FehaHoradatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss' });
                $("#RegistroExpediente-modal").modal("show");

                

            });


            $("body").on("click", ".calculate", function () {
                $("#ctl00_contenido_fecha").val("");
                $("#ctl00_contenido_fechac").val("");
                $("#ctl00_contenido_anoss").val("0");
                $("#ctl00_contenido_mesess").val("0");
                $("#ctl00_contenido_diass").val("0");
                $("#ctl00_contenido_anosa").val("0");
                $("#ctl00_contenido_mesesa").val("0");
                $("#ctl00_contenido_diasa").val("0");
                $("#ctl00_contenido_diasc").val("");
                $("#ctl00_contenido_fechac").val("");
                $("#ctl00_contenido_porcentaje").val("");
                limpiar();
                
                
                $("#calculate-modal").modal("show");

            });


            $("body").on("click", ".calculatebtn", function () {

                if (validar()) {


                    limpiar();
                    $("#ctl00_contenido_fechac").val("");
                    $("#ctl00_contenido_diasc").val("");
                    $("#ctl00_contenido_porcentaje").val("");
                    var str = $("#ctl00_contenido_fecha").val();

                    if (/^\d{2}\/\d{2}\/\d{4}$/i.test(str)) {

                        var parts = str.split("/");

                        var day = parts[0] && parseInt(parts[0], 10);
                        var month = parts[1] && parseInt(parts[1], 10);
                        var year = parts[2] && parseInt(parts[2], 10);

                        if (day < 10) {
                            day = '0' + day
                        }
                        if (month < 10) {
                            month = '0' + month
                        }
                        var startdate = year + "/" + month + "/" + day;
                        var durationyear = parseInt($("#ctl00_contenido_anoss").val(), 10) - parseInt($("#ctl00_contenido_anosa").val(), 10);
                        var durationmonth = parseInt($("#ctl00_contenido_mesess").val(), 10) - parseInt($("#ctl00_contenido_mesesa").val(), 10);
                        var durationday = parseInt($("#ctl00_contenido_diass").val(), 10) - parseInt($("#ctl00_contenido_diasa").val(), 10);

                        if (day <= 31 && day >= 1 && month <= 12 && month >= 1) {

                            var expiryDate = new Date(year, month - 1, day);

                            expiryDate.setDate(expiryDate.getDate() + durationday);
                            expiryDate.setMonth(expiryDate.getMonth() + durationmonth);
                            expiryDate.setFullYear(expiryDate.getFullYear() + durationyear);
                            //expiryDate.setMonth(expiryDate.getMonth(), durationmonth);


                            var day = ('0' + expiryDate.getDate()).slice(-2);
                            var month = ('0' + (expiryDate.getMonth() + 1)).slice(-2);

                            var year = expiryDate.getFullYear();

                            var finaldate = year + "/" + month + "/" + day;
                            


                            // $('#ctl00_contenido_diasc').val(expiryDate.getDay());
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();

                            if (dd < 10) {
                                dd = '0' + dd
                            }

                            if (mm < 10) {
                                mm = '0' + mm
                            }

                            today = yyyy + '/' + mm + '/' + dd;

                            var compurgados = moment(today).diff(moment(startdate), 'days');

                            var total = moment(finaldate).diff(moment(startdate), 'days');

                            $("#ctl00_contenido_fechac").val(day + "/" + month + "/" + year);
                            if (compurgados >= 0) {
                                var porcentaje = parseFloat(Math.round(100 * compurgados) / total).toFixed(2);


                                if (total > 0) {
                                    $('#ctl00_contenido_diasc').val(compurgados);
                                    $('#ctl00_contenido_porcentaje').val(porcentaje);
                                }
                                else {
                                    ShowError("¡Error!", "La fecha a partir debe ser mayor a la fecha cumplimiento de sentencia.");
                                }
                            }
                            else {
                                ShowError("¡Error!", "La fecha a partir debe ser mayor a la fecha cumplimiento de sentencia.");
                            }

                        } else {
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    }
                }


            });

            function validar() {

                var esvalido = true;

                if ($("#ctl00_contenido_fecha").val() == null || $("#ctl00_contenido_fecha").val().split(" ").join("") == "") {
                    ShowError("Fecha a partir", "La fecha a partir es obligatoria.");
                    $('#ctl00_contenido_fecha').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_fecha').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_fecha').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_fecha').addClass('valid');
                }

                if ($("#ctl00_contenido_anoss").val() == null || $("#ctl00_contenido_anoss").val().split(" ").join("") == "") {
                    ShowError("Sentenca años", "Los años son obligatorios.");
                    $('#ctl00_contenido_anoss').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_anoss').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_anoss').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_anoss').addClass('valid');
                }

                if ($("#ctl00_contenido_mesess").val() == null || $("#ctl00_contenido_mesess").val().split(" ").join("") == "") {
                    ShowError("Sentenca meses", "Los meses son obligatorios.");;
                    $('#ctl00_contenido_mesess').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_mesess').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_mesess').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_mesess').addClass('valid');
                }

                if ($("#ctl00_contenido_diass").val() == null || $("#ctl00_contenido_diass").val().split(" ").join("") == "") {
                    ShowError("Sentenca días", "Los días son obligatorios.");
                    $('#ctl00_contenido_diass').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_diass').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_diass').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_diass').addClass('valid');
                }

                if ($("#ctl00_contenido_anosa").val() == null || $("#ctl00_contenido_anosa").val().split(" ").join("") == "") {
                    ShowError("Abono años", "Los años son obligatorios.");
                    $('#ctl00_contenido_anosa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_anosa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_anosa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_anosa').addClass('valid');
                }

                if ($("#ctl00_contenido_mesesa").val() == null || $("#ctl00_contenido_mesesa").val().split(" ").join("") == "") {
                    ShowError("Abono meses", "Los meses son obligatorios.");;
                    $('#ctl00_contenido_mesesa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_mesesa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_mesesa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_mesesa').addClass('valid');
                }

                if ($("#ctl00_contenido_diasa").val() == null || $("#ctl00_contenido_diasa").val().split(" ").join("") == "") {
                    ShowError("Abono días", "Los días son obligatorios.");
                    $('#ctl00_contenido_diasa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_diasa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_diasa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_diasa').addClass('valid');
                }

                return esvalido;

            }

            function limpiar() {
                $('#ctl00_contenido_fecha').parent().removeClass('state-success');
                $('#ctl00_contenido_fecha').parent().removeClass("state-error");
                $('#ctl00_contenido_anoss').parent().removeClass('state-success');
                $('#ctl00_contenido_anoss').parent().removeClass("state-error");
                $('#ctl00_contenido_mesess').parent().removeClass('state-success');
                $('#ctl00_contenido_mesess').parent().removeClass("state-error");
                $('#ctl00_contenido_diass').parent().removeClass('state-success');
                $('#ctl00_contenido_diass').parent().removeClass("state-error");
                $('#ctl00_contenido_anosa').parent().removeClass('state-success');
                $('#ctl00_contenido_anosa').parent().removeClass("state-error");
                $('#ctl00_contenido_mesesa').parent().removeClass('state-success');
                $('#ctl00_contenido_mesesa').parent().removeClass("state-error");
                $('#ctl00_contenido_diasa').parent().removeClass('state-success');
                $('#ctl00_contenido_diasa').parent().removeClass("state-error");
            }



         
            var breakpointAddDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
           
            if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                $("#addentry").show();
            }


            $("body").on("click", ".printpdf", function () {
                LimpiarModalReporte();

                var nombre = $(this).attr("data-nombre");
                $("#nombrereportepdf").val(nombre);
                var rutaavatar = $(this).attr("data-avatar");
                var imagenAvatar = ResolveUrl(rutaavatar.trim());
                $('#avatar').attr("src", imagenAvatar);
                var tracking = $(this).attr("data-tracking");
                $("#trackingidpdf").val(tracking);

                var alias = $(this).attr("data-alias");
                $("#aliasreportepdf").val(alias);

                var fechanacimiento = $(this).attr("data-fn");
                var today = new Date();
                var edad = moment(today).diff(moment(fechanacimiento), 'years');
                if (edad > 0) {
                    $("#edadreportepdf").val(edad);
                }

              

                $("#printpdf-modal").modal("show");
            });

            function LimpiarModalReporte() {
                $('#edadreportepdf').parent().removeClass('state-success');
                $('#edadreportepdf').parent().removeClass("state-error");
                $('#aliasreportepdf').parent().removeClass('state-success');
                $('#aliasreportepdf').parent().removeClass("state-error");
                $('#edadreportepdf').val("");
                $('#aliasreportepdf').val("");
                $('#nombrereportepdf').val("");
                $('#violentopdf').val("");
                $('#enfermopdf').val("");
                $('#comentariospdf').val("");
                $('#trackingidpdf').val("");
                $("#radioBuscandopdf").prop("checked", false);
                $("#radioExtraviadopdf").prop("checked", false);
                $("#radioInformacionpdf").prop("checked", false);
                $('#institucionpdf').val("");
                $('#direccionpdf').val("");
                $('#telefonopdf').val("");
                $('#senaspdf').val("");

            }
                function validarTraslado() {
                var esvalido = true;

                if ($("#ctl00_contenido_dropdown").val() == "0") {
                    ShowError("Delegación", "Capture un valor para el campo delegación para poder continuar");
                    $('#ctl00_contenido_dropdown').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_dropdown').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_dropdown').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_dropdown').addClass('valid');
                }
                return esvalido;
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".printpdfbtn", function () {
                if (validareporteextravio()) {
                    generareporteextravio();
                }
            });

            function validareporteextravio() {
                var esvalido = true;

                if ($("#edadreportepdf").val() == "") {
                    ShowError("Edad", "Capture un valor para el campo Edad para poder continuar");
                    $('#edadreportepdf').parent().removeClass('state-success').addClass("state-error");
                    $('#edadreportepdf').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#edadreportepdf').parent().removeClass("state-error").addClass('state-success');
                    $('#edadreportepdf').addClass('valid');
                }

                if ($("#aliasreportepdf").val() == "") {
                    ShowError("Alias", "Capture un valor para el campo Alias para poder continuar");
                    $('#aliasreportepdf').parent().removeClass('state-success').addClass("state-error");
                    $('#aliasreportepdf').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#aliasreportepdf').parent().removeClass("state-error").addClass('state-success');
                    $('#aliasreportepdf').addClass('valid');
                }

                return esvalido;
            }

            function obtenerdatosreporteextravio() {
                var titulo = "";

                if (document.getElementById('radioBuscandopdf').checked) {
                    titulo = document.getElementById('radioBuscandopdf').value;
                }

                if (document.getElementById('radioExtraviadopdf').checked) {
                    titulo = document.getElementById('radioExtraviadopdf').value;
                }

                if (document.getElementById('radioInformacionpdf').checked) {
                    titulo = document.getElementById('radioInformacionpdf').value;
                }

                var datos = [
                    tracking = $('#trackingidpdf').val(),
                    nombre = $("#nombrereportepdf").val(),
                    edad = $("#edadreportepdf").val(),
                    titulo = titulo,
                    alias = $("#aliasreportepdf").val(),
                    violento = $("#violentopdf").val(),
                    enfermo = $("#enfermopdf").val(),
                    comentarios = $("#comentariospdf").val(),
                    institucion = $('#institucionpdf').val(),
                    direccion = $('#direccionpdf').val(),
                    telefono = $('#telefonopdf').val(),
                    senas = $('#senaspdf').val()
                ]

                return datos;
            }

            function generareporteextravio() {

                var datos = obtenerdatosreporteextravio();

                $.ajax({

                    type: "POST",
                    url: "sentence_entrylist.aspx/reporteextravio",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            open(resultado.ubicacionarchivo.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            ShowError("¡Error!", resultado.mensaje);
                        }

                        $("#printpdf-modal").modal("hide");
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible generar el reporte de extravío. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        $("#printpdf-modal").modal("hide");
                    }
                });

            }
            //function ObtenerValoresTraslado() {                                
            //    var traslado = {                    
            //        MotivoDetencionId: $("#ctl00_contenido_dropdown").val(),
            //        Tracking
            //        InternoId: $('#ctl00_contenido_hideid').val()
                    
            //    };
            //    return contrato;
            //}
            function validatefecha(info) {
                var valido = true;
                
                $.ajax({
                    type: "POST",
                    url: "EstudioDiagnostico.aspx/ValidateFecha",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    data: JSON.stringify({
                        info: info,
                    }),
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {

                            if (resultado.Validar == false) {
                                valido = false;
                            }
                           var fecha = resultado.fechaRegistro;
                            $("#FechaRegistro").val(fecha);
                            $("#validarfecha").val(valido);                                                                         
                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
                return valido;
            }
          

            function guardarExpedienteTrabajoSocial() {
                startLoading();
                var estudioDiagnositcoAux = estudioDiagnositco();

                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    estudioDiagnositcoAux.TrackingId = param;
                }
               
                $.ajax({
                    type: "POST",
                    url: "EstudioDiagnostico.aspx/Save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'estudioDiagnositcoAux': estudioDiagnositcoAux }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje , "<br /></div>");
                            ShowSuccess("¡Bien hecho!",   "La información se registro correctamente" );                            
                           window.emptytable = false;
                        window.table.api().ajax.reload();
                           // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                          $('#main').waitMe('hide');
                           $("#RegistroExpediente-modal").modal("hide");
                            limpiarModal();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 400px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');

                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 180px)");
            });

            
        });
    </script>
</asp:Content>
