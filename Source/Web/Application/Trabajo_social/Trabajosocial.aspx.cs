﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using Entity.Util;
using System.Linq;
namespace Web.Application.Trabajo_social
{
    public partial class Trabajosocial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Trabajo social" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //Consultar
                        this.WERQEQ.Value = permisos.Consultar.ToString().ToLower();

                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }



        [WebMethod]
        public static DataTable getantecedente(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Trabajo social" }).Consultar)
            {
                try
                {

                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("I.TrackingId", tracking));
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                        string IdsContratos = "(";

                        int i = 0;

                        foreach (var item in dataContratos)
                        {
                            if (i == dataContratos.Count - 1)
                            {
                                IdsContratos += item.IdContrato + ")";
                            }
                            else
                            {
                                IdsContratos += item.IdContrato + ",";
                            }

                            i++;
                        }

                        where.Add(new Where("E.ContratoId", Where.IN, IdsContratos));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);

                        }
                        Query query = new Query
                        {
                            select = new List<string> {
                        "I.Id",
                        "I.TrackingId",
                        "I.Nombre",
                        "I.Paterno",
                        "I.Materno",
                        "I.RutaImagen",
                        "ifnull(ExpedienteA,Expediente) Expediente",
                        "DATE_FORMAT(E.Fecha,'%d/%m/%Y') Fecha",
                        "E.NCP",
                        "E.Estatus",
                        "E.Activo",
                        "E.TrackingId TrackingIdEstatus" ,
                        "ES.Nombre EstatusNombre",
                        "CR.Nombre Centro",
                        "DATE_FORMAT(G.FechaNacimineto,'%d/%m/%Y') FechaNacimineto"
                    },
                            from = new Table("detenido", "I"),
                            joins = new List<Join>
                    {
                         new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                         new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"),
                         new Join(new Table("institucion", "CR"), "CR.Id  = E.CentroId"),
                         new Join(new Table("general", "G"), "I.Id  = G.DetenidoId"),

                        new Join(new Table("(Select DetalleDetencionId, Expediente ExpedienteA from historico_agrupado_detenido )", "J"), "J.DetalleDetencionId=E.Id")
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Trabajo social" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Estatus", "<>", "2"));
                        where.Add(new Where("TS.Activo", "1"));
                        where.Add(new Where("C.TrabajoSocial", "1"));
                        var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                        where.Add(new Where("ifnull(Saj.TiposalidaId,0)", "<>", tipo.Id.ToString()));
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        //where.Add(new Where("I.ContratoId", Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]).ToString()));

                        //Prueba de IN

                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        Query query = new Query
                        {
                            select = new List<string> {
                                "distinct (I.Id) Id",
                                "I.TrackingId",
                                "I.Nombre",
                                "I.Paterno",
                                "I.Materno",
                                "I.RutaImagen",
                                "CAST(E.Expediente as unsigned ) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                
                                "E.TrackingId TrackingIdEstatus" ,
                                "ES.Nombre EstatusNombre",
                                "ifnull(T.internoId,0) DetalledetencionId",
                                "ifnull(EX.Id,0) ExpedienteId",
                                "ifnull(EX.TrackingId,0) ExpedientetrackingId",
                                "ifnull(EX.MotivoreahabilitacionId,0) Motivoreahabilitacion",
                                "ifnull(EX.AdiccionId,0) AdiccionId",
                                "ifnull(EX.Pandilla,'') Pandilla",
                                "ifnull(EX.ReligionId,0) ReligionId",
                                "ifnull(EX.Cuadropatalogico,'') Cuadropatalogico",
                                "ifnull(EX.Observacion,'') Observacion",
                                "TS.Id TrabajoSocialId" ,
                                "E.DetalledetencionEdad AS Edad",
                                "ifnull(Ex.Nombremadre,'') Nombremadre",
                                "ifnull(Ex.Celularmadre,'') Celularmadre",
                                "ifnull(Ex.Nombrepadre,'') Nombrepadre" ,
                                "ifnull(Ex.Celularpadre,'') Celularpadre",
                                "ifnull(Ex.Tutor,'') Tutor",
                                "ifnull(Ex.Celulartutor,'') celulartutor",
                                "ifnull(Ex.EscolaridadId,0) EscolaridadId",
                                "ifnull(Ex.Notificado,'') Notificado",
                                "ifnull(Ex.Celularnotificado,'') Celularnotificado",
                                "ifnull(Ex.Domiciliomadre,'') Domiciliomadre",
                                "ifnull(Ex.Domiciliopadre,'') Domiciliopadre",
                                "DATE_FORMAT(G.FechaNacimineto,'%d/%m/%Y') FechaNacimineto",
                                "ifnull(A.Alias,'') Alias",
                               // "(select  group_concat( alias ) from alias  xd where xd.DetenidoId=I.Id) Alias ",
                                "ifnull(S.Nombre,'') Escolaridad",
                                "ifnull(N.Personanotifica,'') PersonaNotifica",
                                "ifnull(N.Celular,'') CelularNotifica",
                                "IFNULL(CAST(DE.Edad as unsigned), 'Sin dato') Edad2",
                                "ifnull(SD.Nombre,'') situacion",
                                "concat(I.Nombre,' ',Paterno,' ',Materno) as NombreCompleto"

                                //"ifnull((select  internoId from motivo_detencion_interno T  where T.InternoId=E.Id limit 1) ,0) DetalleDetencion"
                            },
                            from = new Table("detenido", "I"),
                            joins = new List<Join>
                            {
                                new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                                new Join(new Table("Calificacion", "C"), "C.InternoId  = E.Id"),
                                new Join(new Table("Trabajosocial", "TS"), "TS.DetalleDetencionId  = E.Id"),
                                new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"),
                                new Join(new Table("Motivo_detencion_Interno", "T"), "T.InternoId  = E.Id","LEFT"),
                                new Join(new Table("expediente_trabajo_social", "EX"), "Ex.DetalledetencionId=E.Id","LEFT"),
                                new Join(new Table("general", "G"), "G.DetenidoId=I.Id","LEFT"),
                                new Join(new Table("DetenidosAlias", "A"), "A.DetenidoId=I.Id","LEFT"),
                                new Join(new Table("Escolaridad", "S"), "S.Id=EX.EscolaridadId","LEFT"),
                                new Join(new Table("informaciondedetencion", "N"), "I.id = N.IdInterno"),
                                new Join(new Table("eventos", "V"), "N.IdEvento = V.Id"),
                                new Join(new Table("situacion_detenido", "SD"), "SD.Id = C.SituacionId"),
                                new Join(new Table("(select Edad,Nombre, Paterno APaterno,Materno AMaterno, EventoId from detenido_evento)", "DE"), "V.Id = DE.EventoId and I.Nombre = DE.Nombre and I.Paterno = DE.APaterno and I.Materno = DE.AMaterno"),
                                new Join(new Table("salidaefectuadajuez", "Saj"), "E.id=Saj.DetalleDetencionId and Saj.Activo=1", "LEFT")
                    },
                            wheres = where
                        };

                        if (contratoUsuario.Tipo == "contrato")
                        {
                            query.joins.Add(new Join(new DT.Table("contrato", "co"), "co.id  = E.ContratoId"));
                            where.Add(new Where("E.Tipo", "=", "contrato"));
                            where.Add(new Where("E.ContratoId", "=", contratoUsuario.IdContrato.ToString()));
                        }
                        else if (contratoUsuario.Tipo == "subcontrato")
                        {
                            query.joins.Add(new Join(new DT.Table("subcontrato", "co"), "co.id  = E.ContratoId"));
                            where.Add(new Where("E.Tipo", "=", "subcontrato"));
                            where.Add(new Where("E.Tipo", "=", "subcontrato"));
                            where.Add(new Where("E.ContratoId", "=", contratoUsuario.IdContrato.ToString()));
                        }

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = new DataTable();
                        _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static List<Combo>llena_Combo(string item)
        {
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(Convert.ToInt32(item));

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(r=> r.Habilitado ))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }
        /*
        [WebMethod]
        public static List<Combo> llena_ComboAdiccion(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Adiccion> listado = ControlAdiccion.ObteneTodos();

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.TipoAdiccion, Id = rol.Id.ToString() });
            }
            return combo;
        }*/
        [WebMethod]
        public static List<Combo> getEscolaridad()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.escolaridad));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }
        [WebMethod]
        public static Object ExpedienteTrabajoSocial(ExpedienteTrabajoSocial expedienteTrabajoSocial)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(expedienteTrabajoSocial.trackingId));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                expedienteTrabajoSocial.internoId = detalleDetencion.Id.ToString();

                Entity.ExpedienteTrabajoSocial expediente = new Entity.ExpedienteTrabajoSocial();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                expediente.Activo = 1;
                expediente.Habilitado = 1;
                expediente.Creadopor = usId;
                expediente.DetalledetencionId = Convert.ToInt32(expedienteTrabajoSocial.internoId);
                expediente.MotivorehabilitacionId = Convert.ToInt32(expedienteTrabajoSocial.motivorehabilitacionId);
                expediente.AdiccionId = Convert.ToInt32(expedienteTrabajoSocial.adiccionId);
                expediente.Pandilla = Convert.ToString(expedienteTrabajoSocial.pandilla);
                expediente.ReligionId = Convert.ToInt32(expedienteTrabajoSocial.religionId);
                expediente.Cuadropatologico = Convert.ToString(expedienteTrabajoSocial.cuadropatologico);
                expediente.Observacion = Convert.ToString(expedienteTrabajoSocial.observaciones);
                expediente.TrackingId = Guid.NewGuid().ToString();
                expediente.Nombremadre = expedienteTrabajoSocial.nombremadre;
                expediente.Celularmadre = expedienteTrabajoSocial.celularmadre;
                expediente.Nombrepadre = expedienteTrabajoSocial.nombrepadre;
                expediente.Celularpadre = expedienteTrabajoSocial.celularpadre;
                expediente.Tutor = expedienteTrabajoSocial.tutor;
                expediente.Celulartutor = expedienteTrabajoSocial.celulartutor;
                expediente.EscolaridadId = Convert.ToInt32(expedienteTrabajoSocial.escolaridadid);
                expediente.Notificado = expedienteTrabajoSocial.notificado;
                expediente.Celularnotificado = expedienteTrabajoSocial.celularnotificado;
                expediente.Domiciliomadre = expedienteTrabajoSocial.Domiciliomadre;
                expediente.Domiciliopadre = expedienteTrabajoSocial.Domiciliopadre;
                if (expedienteTrabajoSocial.ExpedienteId == "0")
                {
                    int idmovimiento = ControlExpedienteTrabajosocial.Guardar(expediente);

                    Entity.Historial historial = new Entity.Historial();
                    historial.Activo = true;
                    historial.CreadoPor = usId;
                    historial.Fecha = DateTime.Now;
                    historial.Habilitado = true;
                    historial.InternoId = detalleDetencion.Id;
                    historial.Movimiento = "Modificación de datos generales en expendiente de trabajo social";
                    historial.TrackingId = Guid.NewGuid();
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    historial.ContratoId = subcontrato.Id;
                    historial.Id = ControlHistorial.Guardar(historial);

                    return new { exitoso = true, mensaje = "La información fue registrada satisfactoriamente", Id = "", TrackingId = "" };
                }
                else
                {
                    expediente.Id = Convert.ToInt32(expedienteTrabajoSocial.ExpedienteId);
                    expediente.TrackingId = expedienteTrabajoSocial.ExpedienteTrackingId;
                    ControlExpedienteTrabajosocial.Actualizar(expediente);

                    Entity.Historial historial = new Entity.Historial();
                    historial.Activo = true;
                    historial.CreadoPor = usId;
                    historial.Fecha = DateTime.Now;
                    historial.Habilitado = true;
                    historial.InternoId = detalleDetencion.Id;
                    historial.Movimiento = "Modificación de datos generales en expendiente de trabajo social";
                    historial.TrackingId = Guid.NewGuid();
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    historial.ContratoId = subcontrato.Id;
                    historial.Id = ControlHistorial.Guardar(historial);

                    return new { exitoso = true, mensaje = "La información fue actualizada satisfactoriamente", Id = "", TrackingId = "" };
                }



                
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
        [WebMethod]
        public static Object editpersona(personaeditAux datospersona)
        {
            try
            {
                var detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(datospersona.trackingid));
                string[] datadetenido = new string[]
                    {
                        detenido.Id.ToString(),
                        true.ToString()
                    };
                var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datadetenido);
                var general = ControlGeneral.ObtenerPorDetenidoId(detenido.Id);
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                general.FechaNacimineto = Convert.ToDateTime(datospersona.fechanacimiento);
                if(datospersona.fechanacimiento!=null)
                {
                    var edad = CalcularEdad(Convert.ToDateTime(datospersona.fechanacimiento));
                    general.Edaddetenido = edad;
                    detalleDetencion.DetalledetencionEdad = edad;
                    ControlDetalleDetencion.Actualizar(detalleDetencion);
                }
                ControlGeneral.Actualizar(general);


                detenido.Paterno = datospersona.paterno;
                detenido.Materno = datospersona.materno;
                detenido.Nombre = datospersona.nombre;
                ControlDetenido.Actualizar(detenido);

                Entity.Historial historial = new Entity.Historial();
                historial.Activo = true;
                historial.CreadoPor = usId;
                historial.Fecha = DateTime.Now;
                historial.Habilitado = true;
                historial.InternoId =detalleDetencion.Id;
                historial.Movimiento = "Modificación de datos generales en trabajo social";
                historial.TrackingId = Guid.NewGuid();
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                historial.ContratoId = subcontrato.Id;
                historial.Id = ControlHistorial.Guardar(historial);

                return new { exitoso = true, mensaje = "La información  de la persona fue actualizada correctamente", Id = "", TrackingId = "" };
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }



        [WebMethod]
        public static Object guardaSalidaEfectuada(SalidaEfectuadaAux salidaefectuada)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(salidaefectuada.trackingId));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                salidaefectuada.internoId = detalleDetencion.Id.ToString();

                Entity.SalidaEfectuada salida_efectuada = new Entity.SalidaEfectuada();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                salida_efectuada.Activo = 1;
                salida_efectuada.Habilitado = 1;
                salida_efectuada.Creadopor = usId;
                salida_efectuada.DetalledetencionId = Convert.ToInt32(salidaefectuada.internoId);
                salida_efectuada.Observacion = Convert.ToString(salidaefectuada.observacion);
                salida_efectuada.Responsabledeldetenido = Convert.ToString(salidaefectuada.responsable);
                salida_efectuada.TrackingId = Guid.NewGuid().ToString();

                int idmovimiento =ControlSalidaEfectuada.Guardar(salida_efectuada);

                Entity.TrabajoSocial trabajoSocial = new Entity.TrabajoSocial();
                trabajoSocial.Id = Convert.ToInt32(salidaefectuada.TrabajoSocialId);
                trabajoSocial.Activo = 0;
                ControlTrabajoSpcial.SalidaEfectuada(trabajoSocial);

                if(idmovimiento > 0)
                {
                    Entity.Historial historial = new Entity.Historial();
                    historial.Activo = true;
                    historial.CreadoPor = usId;
                    historial.Fecha = DateTime.Now;
                    historial.Habilitado = true;
                    historial.InternoId = detalleDetencion.Id;
                    historial.Movimiento = "Salida efectuada del detenido";
                    historial.TrackingId = detalleDetencion.TrackingId;
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    historial.ContratoId = subcontrato.Id;
                    historial.Id = ControlHistorial.Guardar(historial);

                    detalleDetencion.Estatus = 2;
                    detalleDetencion.Activo = false;                    
                    ControlDetalleDetencion.Actualizar(detalleDetencion);
                }

                return new { exitoso = true, mensaje = "La información fue registrada satisfactoriamente", Id = "", TrackingId = "" };
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
        [WebMethod]
        public static Object guardaSalidaEfectuadaJuez(SalidaEfectuadajuezAux salidaefectuadajuez)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(salidaefectuadajuez.trackingId));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                salidaefectuadajuez.internoId = detalleDetencion.Id.ToString();

                Entity.SalidaEfectuadaJuez salida_efectuada = new Entity.SalidaEfectuadaJuez();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                salida_efectuada.Activo = 1;
                salida_efectuada.Habilitado = 1;
                salida_efectuada.Creadopor = usId;
                salida_efectuada.DetalledetencionId = Convert.ToInt32(salidaefectuadajuez.internoId);
                salida_efectuada.Fundamento = Convert.ToString(salidaefectuadajuez.fundamento);
                
                salida_efectuada.TrackingId = Guid.NewGuid().ToString();

                int idmovimiento = ControlSalidaEfectuadaJuez.Guardar(salida_efectuada);

                if (idmovimiento > 0)
                {
                    Entity.Historial historial = new Entity.Historial();
                    historial.Activo = true;
                    historial.CreadoPor = usId;
                    historial.Fecha = DateTime.Now;
                    historial.Habilitado = true;
                    historial.InternoId = detalleDetencion.Id;
                    historial.Movimiento = "Salida efectuada del detenido";
                    historial.TrackingId = detalleDetencion.TrackingId;
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    historial.ContratoId = subcontrato.Id;
                    historial.Id = ControlHistorial.Guardar(historial);
                }

                Entity.TrabajoSocial trabajoSocial = new Entity.TrabajoSocial();
                trabajoSocial.Id = Convert.ToInt32(salidaefectuadajuez.TrabajoSocialId);
                trabajoSocial.Activo = 0;
                ControlTrabajoSpcial.SalidaEfectuada(trabajoSocial);




                return new { exitoso = true, mensaje = "La información fue registrada satisfactoriamente", Id = "", TrackingId = "" };
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
        [WebMethod]
        public static Object imprimereporte(SalidaEfectuadajuezAux datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Trabajo social" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Trabajo social" }).Modificar)
                {

                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos.trackingId));
                    string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                    object[] dataArchivo = null;

                    
                    dataArchivo = ControlPDFTrabajoSocial.generarReporteDiagnosticobyExpediente(Convert.ToInt32(datos.expedienteId),detalleDetencion.Id);

                    Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                    reportesLog.ProcesoId = interno.Id;
                    reportesLog.ProcesoTrackingId = interno.TrackingId.ToString();
                    reportesLog.Modulo = "Trabajo social";
                    reportesLog.Reporte = "Expediente";
                    reportesLog.Fechayhora = DateTime.Now;
                    reportesLog.EstatusId = 2;
                    reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    ControlReportesLog.Guardar(reportesLog);


                    return new { exitoso = true, mensaje = "Impresión", Id = detalleDetencion.Id.ToString(), TrackingId = detalleDetencion.TrackingId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "" };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }

            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }


        }
    }

    public class personaeditAux
    {
        public string paterno { get; set; }
        public string materno { get; set; }
        public string nombre { get; set; }
        public string fechanacimiento { get; set; }
        public string trackingid { get; set; }

    }

    public class SalidaEfectuadaAux
    {

        public string observacion { get; set; }
        public string responsable { get; set; }
        public string internoId { get; set; }
        public string trackingId { get; set; }
        public string TrabajoSocialId { get; set; }
    }

    public class SalidaEfectuadajuezAux
    {

        
        public string fundamento { get; set; }
        public string internoId { get; set; }
        public string trackingId { get; set; }
        public string TrabajoSocialId { get; set; }
        public string expedienteId { get; set; }
    }
    public class ExpedienteTrabajoSocial
    {
        public string motivorehabilitacionId { get; set; }
        public string adiccionId { get; set; }
        public string pandilla   { get; set; }
        public string religionId { get; set; }
        public string cuadropatologico { get; set; }
        public string observaciones { get; set; }
        public string internoId { get; set; }
        public string trackingId { get; set; }
        public string nombremadre { get; set; }
        public string celularmadre { get; set; }
        public string nombrepadre { get; set; }
        public string celularpadre { get; set; }
        public string tutor { get; set; }
        public string celulartutor { get; set; }
        public string escolaridadid { get; set; }
        public string notificado { get; set; }
        public string celularnotificado { get; set; }
        public string ExpedienteId { get; set; }
        public string ExpedienteTrackingId { get; set; }
        public string Domiciliomadre { get; set; }
        public string Domiciliopadre { get; set; }

    }
}