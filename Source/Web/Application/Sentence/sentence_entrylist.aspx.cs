﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using Entity.Util;
using System.Linq;
using System.Collections;
using System.Text.RegularExpressions;
using Entity;

namespace Web.Application.Sentence
{
    public partial class sentence_entrylist : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
          //  validatelogin();
            getPermisos();
            if (!IsPostBack)
            {
                tbFechaInicial.Text = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                tbFechaFinal.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }

        [WebMethod]
        public static List<Combo> getYearsModal()
        {
            List<Combo> combo = new List<Combo>();
            var listaanios = ControlDashBoardAnioTrabajo.GetAniosTrabajo();

            List<int> Anios = new List<int>();
            foreach (var item in listaanios)
            {
                Anios.Add(item.Aniotrabajo);
            }

            if (Anios.Count == 0 || DateTime.Now.Year > Anios.Max())
            {
                Entity.DasboardAnioTrabajo dasboardAnioTrabajo = new Entity.DasboardAnioTrabajo();
                dasboardAnioTrabajo.Aniotrabajo = DateTime.Now.Year;
                listaanios.Add(dasboardAnioTrabajo);
            }

            foreach (var item in listaanios.OrderByDescending(x => x.Aniotrabajo))
            {
                combo.Add(new Combo { Desc = item.Aniotrabajo.ToString(), Id = item.Aniotrabajo.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getlenguanativa()
        {
            List<Combo> combo = new List<Combo>();


            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.lenguanativa));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Juez calificador" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //Consultar
                        this.WERQEQ.Value = permisos.Consultar.ToString().ToLower();

                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static List<Combo> getInternos(string id)
        {
            var combo = new List<Combo>();
            int idDet = string.IsNullOrEmpty(id) ? 0 : Convert.ToInt32(id);
            var c = ControlDetenido.ObtenerPorId(Convert.ToInt32(idDet));

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            if (c != null)
            {
                var listaDetalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(c.Id);
                var listaEstatusActivos = listaDetalleDetencion.Where(x => x.Activo && x.ContratoId == contratoUsuario.IdContrato && x.Tipo == contratoUsuario.Tipo);

                if (listaEstatusActivos.Count() > 0)
                {
                    var tt = listaDetalleDetencion.ElementAt(0);
                    combo.Add(new Combo { Desc = tt.Expediente + " - " + c.Nombre + " " + c.Paterno + " " + c.Materno, Id = c.Id.ToString() });
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getClasificacion()
        {
            var combo = new List<Combo>();
            var c = ControlClasificacionEvidencia.ObtenerTodos();

            foreach (var item in c)
            {
                if (item.Habilitado)
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCasilleros()
        {
            var combo = new List<Combo>();
            var c = ControlCasillero.ObtenerTodos();

            var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

            foreach (var item in c)
            {
                if (item.Activo && item.ContratoId == contratoUsuarioK.IdContrato && item.Disponible > 0)
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string MostrarBotonesPertenencias()
        {
            Entity.ContratoUsuario contratoUsuarioK = new Entity.ContratoUsuario();
             contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var parametro = ControlParametroContrato.TraerTodos().FirstOrDefault(x=> x.ContratoId==contratoUsuarioK.IdContrato && x.Parametroid==Convert.ToInt32( Entity.ParametroEnum.BOTONES_PERTENENCIA));

            if(!string.IsNullOrEmpty(parametro!=null?parametro.Valor:""))
            {
                if (parametro.Valor=="1")
                {
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "true"});
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "false" });
                }

            }
            return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "false" });

        }

        [WebMethod]
        public static string savepertenencias(List<PertenenciaData> pertenencias, string interno)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    if (usuario.RolId == 13)
                    {
                        usuario = ControlUsuario.Obtener(8);
                    }

                    string[] internoestatus = { interno, true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    List<string> errores = new List<string>();
                    List<Business.PertenenciaAux> dataReporte = new List<Business.PertenenciaAux>();
                    var detenido = ControlDetenido.ObtenerPorId(Convert.ToInt32(interno));
                    foreach (var item in pertenencias)
                    {
                        Entity.Pertenencia pertenencia = new Entity.Pertenencia();
                        pertenencia.Bolsa = Convert.ToInt32(item.Bolsa);
                        pertenencia.Cantidad = Convert.ToInt32(item.Cantidad);

                        if (pertenencia.Bolsa < 0)
                        {
                            throw new Exception(string.Concat("No se permite un valor negativo para bolsa."));
                        }

                        if (pertenencia.Cantidad < 0)
                        {
                            throw new Exception(string.Concat("No se permite un valor negativo para cantidad."));
                        }

                        pertenencia.Clasificacion = Convert.ToInt32(item.Clasificacion);
                        pertenencia.CreadoPor = usuario.Id;
                        pertenencia.Estatus = 4;
                        pertenencia.Fotografia = item.Fotografia;
                        if (string.IsNullOrEmpty(pertenencia.Fotografia))
                        {
                            pertenencia.Fotografia = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/object.png");

                        }
                        pertenencia.InternoId = detalleDetencion.Id;
                        pertenencia.Observacion = item.Observacion;
                        pertenencia.PertenenciaNombre = item.Pertenencia;
                        pertenencia.CasilleroId = Convert.ToInt32(item.Casillero);

                        var casilleroNuevo = ControlCasillero.ObtenerPorId(Convert.ToInt32(item.Casillero));

                        if (!string.IsNullOrEmpty(item.Id))
                        {
                            var p = ControlPertenencia.ObtenerPorId(Convert.ToInt32(item.Id));
                            Entity.Casillero c = null;
                            if (p.CasilleroId != 0)
                            {
                                c = ControlCasillero.ObtenerPorId(p.CasilleroId);
                                c.Disponible = c.Disponible + p.Bolsa;
                            }

                            if (Convert.ToInt32(item.Casillero) != p.CasilleroId)
                            {
                                if (pertenencia.Bolsa > casilleroNuevo.Disponible)
                                {
                                    throw new Exception(string.Concat("No hay espacio suficiente en el casillero para la cantidad de bolsas."));
                                }
                            }
                            else
                            {
                                if (pertenencia.Bolsa > c.Disponible)
                                {
                                    throw new Exception(string.Concat("No hay espacio suficiente en el casillero para la cantidad de bolsas."));
                                }
                            }

                            mode = "actualizó";
                            pertenencia.Habilitado = true;
                            pertenencia.Activo = true;
                            pertenencia.Id = Convert.ToInt32(item.Id);
                            pertenencia.TrackingId = new Guid(item.TrackingId);
                            ControlPertenencia.Actualizar(pertenencia);
                            
                            pertenencia = ControlPertenencia.ObtenerPorId(pertenencia.Id);
                            dataReporte.Add(new Business.PertenenciaAux()
                            {
                                Id = pertenencia.Id.ToString(),
                                Nombre = pertenencia.PertenenciaNombre,
                                FechaEntrada = DateTime.Now.ToString(),
                                Bolsa = pertenencia.Bolsa.ToString(),
                                Cantidad = pertenencia.Cantidad.ToString(),
                                Casillero = ControlCasillero.ObtenerPorId(pertenencia.CasilleroId).Nombre,
                                Clasificacion = ControlCatalogo.Obtener(pertenencia.Clasificacion, 85).Nombre,
                                Estatus = pertenencia.Estatus.ToString(),
                                Observacion = pertenencia.Observacion
                            });

                            if (c != null)
                                ControlCasillero.Actualizar(c);

                            var casilleroNuevo2 = ControlCasillero.ObtenerPorId(Convert.ToInt32(item.Casillero));
                            casilleroNuevo2.Disponible = casilleroNuevo2.Disponible - pertenencia.Bolsa;
                            ControlCasillero.Actualizar(casilleroNuevo2);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usuario.Id;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Modificación de datos generales en control de pertenencias en juez calificador";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                        else
                        {
                            mode = "registró";
                            if (pertenencia.Bolsa > casilleroNuevo.Disponible)
                            {
                                throw new Exception(string.Concat("No hay espacio suficiente en el casillero para la cantidad de bolsas."));
                            }

                            pertenencia.Habilitado = true;
                            pertenencia.Activo = true;
                            pertenencia.TrackingId = Guid.NewGuid();
                            pertenencia.Id = ControlPertenencia.Guardar(pertenencia);

                            pertenencia = ControlPertenencia.ObtenerPorId(pertenencia.Id);
                            dataReporte.Add(new Business.PertenenciaAux()
                            {
                                Id = pertenencia.Id.ToString(),
                                Nombre = pertenencia.PertenenciaNombre,
                                FechaEntrada = DateTime.Now.ToString(),
                                Bolsa = pertenencia.Bolsa.ToString(),
                                Cantidad = pertenencia.Cantidad.ToString(),
                                Casillero = ControlCasillero.ObtenerPorId(pertenencia.CasilleroId).Nombre,
                                Clasificacion = ControlCatalogo.Obtener(pertenencia.Clasificacion, 85).Nombre,
                                Estatus = pertenencia.Estatus.ToString(),
                                Observacion = pertenencia.Observacion
                            });

                            if (pertenencia.Id <= 0)
                            {
                                errores.Add("Hubo un error al intentar registrar la pertenencia con el nombre de " + pertenencia.PertenenciaNombre + ", ");
                            }

                            casilleroNuevo.Disponible = casilleroNuevo.Disponible - pertenencia.Bolsa;
                            ControlCasillero.Actualizar(casilleroNuevo);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usuario.Id;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Modificación de datos generales en control de pertenencias en juez calificador";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    var domicilio = ControlDomicilio.ObtenerPorId(detenido.DomicilioId);

                    string nombreInterno = detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno;
                    string colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).Asentamiento : "" : "";
                    string cp = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "" : "";
                    string calle = (domicilio != null) ? (domicilio.Calle != null) ? domicilio.Calle : "" : "";
                    string numero = (domicilio != null) ? (domicilio.Numero != null) ? domicilio.Numero : "" : "";
                    string domicilioCompleto = "Calle: " + calle + " #" + numero + " Colonia:" + colonia + " C.P." + cp;
                    string bolsa = pertenencias[0].Bolsa;

                    string recibio = usuario.User;

                    //Generar PDF
                    object[] obj = ControlPDF.GenerarReciboDevolucionesPDF(dataReporte, nombreInterno, detalleDetencion.Expediente, domicilioCompleto, recibio, "", "", bolsa, nombreInterno, detenido.Id);

                    Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                    reportesLog.ProcesoId =detenido.Id;
                    reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                    reportesLog.Modulo = "Juez calificador";
                    reportesLog.Reporte = "Recibo de pertenencias";
                    reportesLog.Fechayhora = DateTime.Now;
                    reportesLog.EstatusId = 1;
                    reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    ControlReportesLog.Guardar(reportesLog);

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, errores = errores,tracking=detenido.TrackingId, ubicacionArchivo = obj[1].ToString() });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getPertenenciasComunes(string interno)
        {
            try
            {
                List<object> list = new List<object>();
                var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                List<Entity.Catalogo> pertenenciasComunes = ControlCatalogo.ObtenerTodo(123);
                List<Entity.Pertenencia> pertenenciasRegistradas = ControlPertenencia.ObtenerPorInterno(Convert.ToInt32(interno));
                string fecha = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                foreach (var item in pertenenciasComunes.Where(tx=>tx.Habilitado))
                {
                    object obj = new
                    {
                        Id = "",
                        TrackingId = "",
                        item.Nombre,
                        Fecha = fecha,
                        Observacion = "",
                        Cantidad = "",
                        Bolsa = "",
                        ClasificacionId = "",
                        CasilleroId = "",
                        Fotografia = "",
                        Casillero = "",
                        Clasificacion = ""
                    };

                    list.Add(obj);
                }

                foreach (var item in pertenenciasRegistradas)
                {
                    if (item.Habilitado && item.Activo)
                    {
                        object obj = new
                        {
                            Id = item.Id,
                            TrackingId = item.TrackingId.ToString(),
                            Nombre = item.PertenenciaNombre,
                            Fecha = fecha,
                            Observacion = item.Observacion,
                            Cantidad = item.Cantidad,
                            Bolsa = item.Bolsa,
                            ClasificacionId = item.Clasificacion,
                            CasilleroId = item.CasilleroId,
                            Fotografia = item.Fotografia,
                            Casillero = ControlCasillero.ObtenerPorId(item.CasilleroId).Nombre,
                            Clasificacion = ControlClasificacionEvidencia.ObtenerPorId(item.Clasificacion).Nombre
                        };

                        list.Add(obj);
                    }
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveAlias(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Modificar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    string mode = string.Empty;
                    Entity.Alias alias = new Entity.Alias();
                    alias.Nombre = datos[2].ToString();
                    alias.Activo = true;

                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[3]));
                    alias.DetenidoId = interno.Id;
                    string[] datadetenido = new string[]
                    {
                        interno.Id.ToString(),
                        true.ToString()
                    };
                    var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datadetenido);
                    var aliasrepetido = ControlAlias.ObtenerPorAlias(alias);

                    var id = datos[0].ToString();

                    if (id == "")
                        id = "0";

                    if (aliasrepetido != null && aliasrepetido.Id != Convert.ToInt32(id))
                        throw new Exception(string.Concat("Al parecer ya existe un alias ", alias.Nombre, ". Verifique la información y vuelva a intentarlo."));



                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        mode = "actualizó";
                        alias.Id = Convert.ToInt32(datos[0]);
                        alias.TrackingId = new Guid(datos[1]);
                        ControlAlias.Actualizar(alias);

                        if (alias.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Modificación de alias en juez calificador";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        mode = "registró";
                        alias.TrackingId = Guid.NewGuid();
                        alias.Creadopor = usId;
                        alias.Id = ControlAlias.Guardar(alias);

                        if (alias.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Registro de alias en juez calificador";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = alias.Id, TrackingId = alias.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitemalias(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Eliminar)
                {
                    Entity.Alias item = ControlAlias.ObtenerPorTrackingId(new Guid(trackingid));
                    item.Activo = item.Activo ? false : true;
                    ControlAlias.Actualizar(item);
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = item.Activo ? "Registro habilitado con éxito." : "Registro deshabilitado con éxito." });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getAlias(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string tracking)
        {

            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                    {
                        if (!emptytable)
                        {
                            ;

                            List<Where> where = new List<Where>();
                            Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(tracking));
                            //  where.Add(new Where("A.Activo", "1"));
                            where.Add(new Where("A.DetenidoId", interno.Id.ToString()));

                            Query query = new Query
                            {
                                select = new List<string> {
                        "A.Id",
                        "A.TrackingId",
                        "A.Alias",
                        "A.Activo"
                       },
                                from = new Table("alias", "A"),
                                joins = new List<Join>
                                {

                                },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para realizar la acción.", draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
                finally
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static List<Combo> getNacionalidad()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.nacionalidad));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getEscolaridad()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.escolaridad));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getReligion()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.religion));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getOcupacion()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.ocupacion));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getEstadoCivil()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.estado_civil));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> GetFiltrosJuezCalificador()
        {
            List<Combo> combo = new List<Combo>();

            //List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.estado_civil));
            int IdContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(IdContrato);

            Boolean MostrarExamenmedico = false;

            try
            {
                var parametroContrato = ControlParametroContrato.TraerTodos().LastOrDefault(x=> x.ContratoId==contratoUsuario.IdContrato && x.Parametroid==Convert.ToInt32(Entity.ParametroEnum.TRASLADO_O_SALIDA_SIN_EXAMEN_MÉDICO));

                if (parametroContrato.Valor != null)
                {
                    MostrarExamenmedico = true;
                }

            }
            catch (Exception)
            {

                
            }

            //combo.Add(new Combo { Desc = "Todos los detenidos", Id ="0" });
                    combo.Add(new Combo { Desc = "Detenidos en ingreso", Id = "6" });
            if (MostrarExamenmedico)
            {
                combo.Add(new Combo { Desc = "Detenidos en examén médico", Id = "4" });
            }
                    combo.Add(new Combo { Desc = "Detenidos calificados", Id = "5" });
                    combo.Add(new Combo { Desc = "Detenidos por salir", Id = "3" });
                    //combo.Add(new Combo { Desc = "Detenidos en custodia externa", Id = "7" });
                    //combo.Add(new Combo { Desc = "Detenidos por fecha y hora de salida", Id = "8" });
                    combo.Add(new Combo { Desc = "Detenidos salida autorizada", Id = "9" });
                    combo.Add(new Combo { Desc = "Salida autorizada por pago de multa", Id = "10" });
                    combo.Add(new Combo { Desc = "Detenidos sin calificar", Id = "2" });
                    combo.Add(new Combo { Desc = "Menores de edad", Id = "1" });

            /*
             <!--  <option value="7">Detenidos en custodia externa</option>-->
                    <!--  <option value="8">Detenidos por fecha y hora de salida</option>-->
                    <!--<option value="9">Detenidos salida autorizada</option>-->
                    <!--<option value="10">Salida autorizada por pago de multa</option>-->
                    <option value="2">Detenidos sin calificar</option> 
                    <option value="1">Menores de edad</option>
                   
             */

            return combo;

        }

        [WebMethod]
        public static List<Combo> getEtnia()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.etnia));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getSexo()
        {
            List<Combo> combo = new List<Combo>();


            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string saveDN(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Modificar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    string mode = string.Empty;
                    //guardar domicilio

                    Entity.Domicilio domicilio = new Entity.Domicilio();
                    domicilio.Calle = datos[2].ToString();
                    domicilio.Numero = datos[3].ToString();
                    //domicilio.Colonia = datos[4].ToString();
                    //domicilio.CP = Convert.ToInt32(datos[5].ToString());
                    domicilio.Telefono = datos[4].ToString();
                    domicilio.PaisId = Convert.ToInt32(datos[5].ToString());
                    if (domicilio.PaisId == 73)
                    {
                        domicilio.EstadoId = Convert.ToInt32(datos[6].ToString());
                        domicilio.MunicipioId = Convert.ToInt32(datos[7].ToString());
                        domicilio.ColoniaId = Convert.ToInt32(datos[20].ToString());
                    }
                    else
                    {
                        domicilio.Localidad = datos[18].ToString();
                    }
                    domicilio.Latitud = datos[22].ToString();
                    domicilio.Longitud = datos[23].ToString();

                    //guardar lugar nacimiento
                    Entity.Domicilio nacimiento = new Entity.Domicilio();
                    nacimiento.Calle = datos[11].ToString();
                    nacimiento.Numero = datos[12].ToString();
                    //nacimiento.Colonia = datos[15].ToString();
                    //nacimiento.CP = Convert.ToInt32(datos[16].ToString());
                    nacimiento.Telefono = datos[13].ToString();
                    nacimiento.PaisId = Convert.ToInt32(datos[14].ToString());
                    if (nacimiento.PaisId == 73)
                    {
                        nacimiento.EstadoId = Convert.ToInt32(datos[15].ToString());
                        nacimiento.MunicipioId = Convert.ToInt32(datos[16].ToString());
                        nacimiento.ColoniaId = Convert.ToInt32(datos[21].ToString());
                    }
                    else
                    {
                        nacimiento.Localidad = datos[19].ToString();
                    }

                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[17]));
                    string[] datadetenido = new string[]
                    {
                        interno.Id.ToString(),
                        true.ToString()
                    };
                    var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datadetenido);
                    if (!string.IsNullOrEmpty(datos[0].ToString()) && !string.IsNullOrEmpty(datos[9].ToString()))
                    {
                        mode = "actualizó";
                        domicilio.Id = Convert.ToInt32(datos[0]);
                        domicilio.TrackingId = new Guid(datos[1]);
                        ControlDomicilio.Actualizar(domicilio);
                        nacimiento.Id = Convert.ToInt32(datos[9]);
                        nacimiento.TrackingId = new Guid(datos[10]);
                        ControlDomicilio.Actualizar(nacimiento);
                        interno.DomicilioId = domicilio.Id;
                        interno.DomicilioNId = nacimiento.Id;
                        ControlDetenido.Actualizar(interno);

                        if (domicilio.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Modificación de domicilio actual en barandilla";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }

                        if (nacimiento.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Modificación de domicilio de nacimiento en barandilla";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(datos[0].ToString()) || !string.IsNullOrEmpty(datos[9].ToString()))
                            mode = "actualizó";
                        else
                            mode = "registró";

                        domicilio.TrackingId = Guid.NewGuid();
                        domicilio.Id = ControlDomicilio.Guardar(domicilio);
                        nacimiento.TrackingId = Guid.NewGuid();
                        nacimiento.Id = ControlDomicilio.Guardar(nacimiento);
                        interno.DomicilioId = domicilio.Id;
                        interno.DomicilioNId = nacimiento.Id;
                        ControlDetenido.Actualizar(interno);

                        if (domicilio.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Registro de domicilio actual en control de detenidos";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }

                        if (nacimiento.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Registro de domicilio de nacimiento en control de detenidos";
                            historial.TrackingId = nacimiento.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Iddomicilio = domicilio.Id, TrackingIddomicilio = domicilio.TrackingId, Idnacimiento = nacimiento.Id, TrackingIdnacimiento = nacimiento.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveGeneral(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Modificar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    string mode = string.Empty;
                    var fechaNacimiento = Convert.ToDateTime(datos[2].ToString());
                    if (fechaNacimiento > DateTime.Now.AddYears(-1))
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha de nacimiento debe ser de un año anterior a la fecha de hoy." });
                    if (datos[3].ToString().Length > 13)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El RFC debe contar con un máximo de 13 caracteres." });
                    Regex validarRFC_CURP = new Regex("[^A-Z0-9]+");
                    if (validarRFC_CURP.IsMatch(datos[3].ToString().ToUpper()))
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El RFC no puede contener caracteres especiales." });
                    if (validarRFC_CURP.IsMatch(datos[14].ToString().ToUpper()))
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La CURP no puede contener caracteres especiales." });

                    Entity.General general = new Entity.General();
                    general.FechaNacimineto = Convert.ToDateTime(datos[2].ToString());
                    general.RFC = datos[3].ToString();
                    general.NacionalidadId = Convert.ToInt32(datos[4].ToString());
                    general.EscolaridadId = Convert.ToInt32(datos[5].ToString());
                    general.ReligionId = Convert.ToInt32(datos[6].ToString());
                    general.OcupacionId = Convert.ToInt32(datos[7].ToString());
                    general.EstadoCivilId = Convert.ToInt32(datos[8].ToString());
                    general.EtniaId = Convert.ToInt32(datos[9].ToString());
                    general.SexoId = Convert.ToInt32(datos[10].ToString());
                    general.EstadoMental = Convert.ToBoolean(datos[11].ToString());
                    general.Inimputable = Convert.ToBoolean(datos[12].ToString());
                    general.CURP = datos[14].ToString();
                    general.LenguanativaId = Convert.ToInt32(datos[15]);
                    general.Edaddetenido = CalcularEdad(general.FechaNacimineto);
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[13]));
                    general.DetenidoId = interno.Id;
                    var infodet = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);

                    string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion2 = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    if (detalleDetencion2 == null)
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No puede modificar la información del detenido." });

                    infodet.Personanotifica = datos[16];
                    infodet.Celular = datos[17].Replace("(","").Replace(")","").Replace("-","").Replace(" ","");
                    if (!string.IsNullOrEmpty(datos[0].ToString()))
                    {
                        mode = "actualizaron";
                        general.Id = Convert.ToInt32(datos[0]);
                        general.TrackingId = new Guid(datos[1]);
                        ControlGeneral.Actualizar(general);
                        ControlInformacionDeDetencion.Actualizar(infodet);
                        detalleDetencion2.DetalledetencionSexoId = general.SexoId;
                        detalleDetencion2.DetalledetencionEdad = general.Edaddetenido;
                        ControlDetalleDetencion.Actualizar(detalleDetencion2);

                        if (general.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion2.Id;
                            historial.Movimiento = "Modificación de datos generales en control de detenidos";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        mode = "registró";
                        general.TrackingId = Guid.NewGuid();
                        general.Id = ControlGeneral.Guardar(general);
                        detalleDetencion2.DetalledetencionSexoId = general.SexoId;
                        detalleDetencion2.DetalledetencionEdad = general.Edaddetenido;
                        ControlDetalleDetencion.Actualizar(detalleDetencion2);
                        if (general.Id > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId =detalleDetencion2.Id;
                            historial.Movimiento = "Registro de datos generales en control de detenidos";
                            historial.TrackingId = Guid.NewGuid();
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }

                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, Id = general.Id, TrackingId = general.TrackingId });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getPais()
        {
            List<Combo> combo = new List<Combo>();

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.pais));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> GetTipoSalida_Combo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(c=>c.Habilitado))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetTipoSalida_Combo2(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x=> x.TrackingId.ToString()== "334eb77d-654d-4814-8e36-69787becec84"))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getMunicipio(string estadoId)
        {
            var combo = new List<Combo>();
            List<Entity.Municipio> listado = ControlMunicipio.ObtenerPorEstado(Convert.ToInt32(estadoId));

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x=>x.Habilitado))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getEstado(string paisId)
        {
            var combo = new List<Combo>();
            List<Entity.Estado> listado = ControlEstado.ObtenerPorPais(Convert.ToInt32(paisId));

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x=>x.Habilitado))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getNeighborhoods(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();

            var colonias = ControlColonia.ObtenerPorMunicipioId(Convert.ToInt32(idMunicipio));

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias.Where(x=>x.Habilitado))
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getZipCode(string idColonia)
        {
            try
            {
                var colonia = ControlColonia.ObtenerPorId(Convert.ToInt32(idColonia));

                string codigoPostal = "";

                if (colonia != null)
                {
                    codigoPostal = colonia.CodigoPostal;
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", cp = codigoPostal });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, });
            }
        }

        [WebMethod]
        public static string saveDetenido(string[] datos)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                Entity.Detenido detenido = new Entity.Detenido();
                detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[1].ToString()));
                string[] datadetenido = new string[]
                    {
                        detenido.Id.ToString(),
                        true.ToString()
                    };
                var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datadetenido);
                detenido.Materno = datos[4].ToString();
                detenido.Paterno = datos[3].ToString();
                detenido.Nombre = datos[2].ToString();
                detenido.RutaImagen = datos[5].ToString();
                ControlDetenido.Actualizar(detenido);
                Entity.Historial historial = new Entity.Historial();
                historial.Activo = true;
                historial.CreadoPor = usId;
                historial.Fecha = DateTime.Now;
                historial.Habilitado = true;
                historial.InternoId = detalleDetencion.Id;
                historial.Movimiento = "Modificación de datos generales en Juez calificador";
                historial.TrackingId = Guid.NewGuid();
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                historial.ContratoId = subcontrato.Id;
                historial.Id = ControlHistorial.Guardar(historial);

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "actualizó" });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string cargarDatosInterno(string tracking)
        {
            try
            {
                var detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(tracking));
                var detalle = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(new string[] { detenido.Id.ToString(), "true" });
                if (detalle == null)
                {

                    detalle = ControlDetalleDetencion.ObtenerPorDetenidoId(detenido.Id).LastOrDefault();
                }

                var general = detenido != null ? ControlGeneral.ObtenerPorDetenidoId(detenido.Id) : null;
                DateTime fechaNacimiento = general != null ? general.FechaNacimineto : new DateTime();
                string fecha = "";
                if (fechaNacimiento.Year != 1)
                {
                    fecha = fechaNacimiento.ToString("dd/MM/yyyy");
                }
                var informaciondetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detalle.DetenidoId);
                //Consultar domicilio actual
                var domicilioActual = detenido != null ? ControlDomicilio.ObtenerPorId(detenido.DomicilioId) : null;
                var domicilioNacimiento = detenido != null ? ControlDomicilio.ObtenerPorId(detenido.DomicilioNId) : null;
                Entity.DomicilioPorSubcontrato domicilioSC = ControlDomicilioPorSubcontrato.ObtenerporId(detalle.DetenidoId);
                var nacionalidad = ControlCatalogo.Obtener("Mexicana", Convert.ToInt32(Entity.TipoDeCatalogo.nacionalidad));
                object obj = new
                {
                    Id = detenido != null ? detenido.Id.ToString() : "",
                    TrackingId = detenido != null ? detenido.TrackingId.ToString() : "",
                    Nombre = detenido != null ? detenido.Nombre : "",
                    Paterno = detenido != null ? detenido.Paterno : "",
                    Materno = detenido != null ? detenido.Materno : "",
                    Foto = detenido != null ? detenido.RutaImagen : "",
                    Fecha = detalle != null ? detalle.Fecha.Value.ToString("dd/MM/yyyy HH:mm:ss") : "",
                    Expediente = detalle != null ? detalle.Expediente : "",
                    FechaNacimiento = fecha,
                    RFC = general != null ? string.IsNullOrEmpty(general.RFC) ? string.Empty : general.RFC : "",
                    CURP = general != null ? string.IsNullOrEmpty(general.CURP) ? string.Empty : general.CURP : "",
                    NacionalidadId = general != null ? general.NacionalidadId.ToString() : nacionalidad.Id.ToString(),
                    EscolaridadId = general != null ? general.EscolaridadId.ToString() : "",
                    ReligionId = general != null ? general.ReligionId.ToString() : "",
                    OcupacionId = general != null ? general.OcupacionId.ToString() : "",
                    EstadoCivilId = general != null ? general.EstadoCivilId.ToString() : "",
                    EtniaId = general != null ? general.EtniaId.ToString() : "",
                    SexoId = general != null ? general.SexoId.ToString() : "",
                    IdGeneral = general != null ? general.Id.ToString() : "",
                    TrackingIdGeneral = general != null ? general.TrackingId.ToString() : "",
                    IdNacimiento = domicilioNacimiento != null ? domicilioNacimiento.Id : 0,
                    TrackingIdNacimiento = domicilioNacimiento != null ? domicilioNacimiento.TrackingId.ToString() : "",
                    CalleNacimiento = domicilioNacimiento != null ? domicilioNacimiento.Calle : "",
                    NumeroNacimiento = domicilioNacimiento != null ? domicilioNacimiento.Numero : "",
                    TelefonoNacimiento = domicilioNacimiento != null ? domicilioNacimiento.Telefono : "",
                    PaisNacimiento = domicilioNacimiento != null ? domicilioNacimiento.PaisId : 0,
                    EstadoNacimiento = domicilioNacimiento != null ? domicilioNacimiento.EstadoId : 0,
                    MunicipioNacimiento = domicilioNacimiento != null ? domicilioNacimiento.MunicipioId : 0,
                    LocalidadNacimiento = domicilioNacimiento != null ? domicilioNacimiento.Localidad : "",
                    ColoniaIdNacimiento = domicilioNacimiento != null ? (domicilioNacimiento.ColoniaId) != null ? domicilioNacimiento.ColoniaId : 0 : 0,
                    CodigoPostalNacimiento = domicilioNacimiento != null ? (domicilioNacimiento.ColoniaId) != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilioNacimiento.ColoniaId)).CodigoPostal : "" : "",
                    IdDomicilio = domicilioActual != null ? domicilioActual.Id : 0,
                    TrackingIdDomicilio = domicilioActual != null ? domicilioActual.TrackingId.ToString() : "",
                    CalleDomicilio = domicilioActual != null ? domicilioActual.Calle : "",
                    NumeroDomicilio = domicilioActual != null ? domicilioActual.Numero : "",
                    TelefonoDomicilio = domicilioActual != null ? domicilioActual.Telefono : "",
                    PaisDomicilio = domicilioActual != null ? domicilioActual.PaisId : domicilioSC != null ? Convert.ToInt32(domicilioSC.PaisId) : 0,
                    EstadoDomicilio = domicilioActual != null ? domicilioActual.EstadoId : domicilioSC != null ? Convert.ToInt32(domicilioSC.EstadoId) : 0,
                    MunicipioDomicilio = domicilioActual != null ? domicilioActual.MunicipioId : domicilioSC != null ? Convert.ToInt32(domicilioSC.MunicipioId) : 0,
                    LocalidadDomicilio = domicilioActual != null ? domicilioActual.Localidad != null ? domicilioActual.Localidad : "" : "",
                    ColoniaIdDomicilio = domicilioActual != null ? (domicilioActual.ColoniaId) != null ? domicilioActual.ColoniaId : 0 : 0,
                    CodigoPostalDomicilio = domicilioActual != null ? (domicilioActual.ColoniaId) != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilioActual.ColoniaId)).CodigoPostal : "" : "",
                    LatitudDomicilio = domicilioActual != null ? string.IsNullOrEmpty(domicilioActual.Latitud) ? string.Empty : domicilioActual.Latitud : "",
                    LongitudDomicilio = domicilioActual != null ? string.IsNullOrEmpty(domicilioActual.Longitud) ? string.Empty : domicilioActual.Longitud : "",
                    TieneDomicilio = domicilioActual != null ? true : false,
                    TieneNacimiento = domicilioNacimiento != null ? true : false,                    
                    Personanotifica= informaciondetencion != null ? informaciondetencion.Personanotifica!=null? informaciondetencion .Personanotifica: "" : string.Empty,
                    Celularnotifica= informaciondetencion != null ? informaciondetencion.Celular!=null? informaciondetencion.Celular:"" : string.Empty,
                    Lenguanativa= general!=null?general.LenguanativaId.ToString():"0",
                    Lesion_visible= detalle.Lesion_visible.ToString()
                };

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", obj });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static Object getpositiobycontract(string LlamadaId)
        {
            try
            {
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                Entity.Subcontrato subcontrato = new Entity.Subcontrato();



                subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);


                object obj = new { Latitud = subcontrato.Latitud, Longitud = subcontrato.Longitud };



                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "Acción realizada correctamente", obj });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message });
            }
        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getdataPrincipal(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string opcion, string anio, string pages)
        {
            string[] dataPermiso = new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" };
            var permisoUno = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(dataPermiso);
            bool consultar = permisoUno != null ? permisoUno.Consultar : false;

            try
            {
                if (consultar)
                {
                    List<JuezCalificadorList> data = new List<JuezCalificadorList>();
                    int IdContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                    Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(IdContrato);
                    var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));

                    object[] parameters = new object[]
                    {
                    string.IsNullOrEmpty(anio) ? DateTime.Now.Year : Convert.ToInt32(anio),
                    string.IsNullOrEmpty(opcion) ? "0" : opcion,
                    contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                    contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                    tipo != null ? tipo.Id : 0
                    };

                    List<JuezCalificadorList> listTotals = ControlJuezCalificadorList.ObtenerTodos(parameters);
                    if (opcion == "2") listTotals = listTotals.Where(x => x.CalificacionId == 0).ToList();
                    if (listTotals.Count == 0)
                    {
                        object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json2;
                    }

                    if (string.IsNullOrEmpty(search.value))
                    {
                        data = listTotals;
                    }
                    else
                    {
                        var listAux = listTotals;
                        data = compareSearch(listAux, search.value.ToLower());
                    }

                    if(order.Count > 0)
                    {
                        Order order1 = order[0];
                        
                        switch (order1.column)
                        {
                            case 3: 
                                if(order1.dir == "asc") data = data.OrderBy(x => x.Nombre).ToList();
                                else if(order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre).ToList();
                                break;
                            case 4:
                                if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno).ToList();
                                else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno).ToList();                                
                                break;
                            case 5:
                                if (order1.dir == "asc") data = data.OrderBy(x => x.Materno).ToList();
                                else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno).ToList();                                
                                break;
                            case 6:
                                if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();                                
                                break;
                            case 11:
                                if (order1.dir == "asc") data = data.OrderBy(x => x.Fecha).ToList();
                                else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Fecha).ToList();                                
                                break;
                        }
                    }

                    List<JuezCalificadorList> listFinal = new List<JuezCalificadorList>();
                    int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                    int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                    if (numPages == pageActual)
                    {
                        if ((data.Count / length < 1) || (data.Count % length > 0))
                        {
                            int i = data.Count % length;
                            listFinal = data.GetRange(start, i);
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }
                    }
                    else
                    {
                        listFinal = data.GetRange(start, length);
                    }

                    object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                    return json;
                }
                else
                {
                    object json2 = new { data = new List<JuezCalificadorList>(), recordsTotal = new List<JuezCalificadorList>().Count, recordsFiltered = new List<JuezCalificadorList>().Count, };
                    return json2;
                }
            }
            catch (Exception ex)
            {
                object json2 = new { data = new List<JuezCalificadorList>(), recordsTotal = new List<JuezCalificadorList>().Count, recordsFiltered = new List<JuezCalificadorList>().Count, };
                return json2;
            }            
        }

        //console

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string opcion, string anio, string fechaInicial, string fechaFinal)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
            {
                using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
                {
                    try
                    {
                        if (opcion == null)
                        {
                            opcion = "0";
                        }
                        if (!emptytable)
                        {
                            if (string.IsNullOrEmpty(anio))
                            {
                                anio = DateTime.Now.Year.ToString();
                            }
                            var listaanios = ControlDashBoardAnioTrabajo.GetAniosTrabajo();

                            List<int> Anios = new List<int>();
                            foreach (var item in listaanios)
                            {
                                Anios.Add(item.Aniotrabajo);
                            }

                            if (Convert.ToInt32(anio) > Anios.Max())
                            {
                                anio = Anios.Max().ToString();
                            }

                            if (order.Count > 1)
                            {
                                order.FirstOrDefault().column = 3;
                            }


                            List<Where> where = new List<Where>();
                            List<Where> where2 = new List<Where>();
                            List<Join> join = new List<Join>();


                            join.Add(new Join(new Table("general", "g"), "I.id  = g.DetenidoId"));
                            //join.Add(new Join(new Table($"(select Id, TrackingId, DetenidoId, Fecha, Expediente, NCP, Activo, Estatus, ContratoId, Tipo from detalle_detencion where year(Fecha) = {anio})", "E"), "I.id  = E.DetenidoId"));
                            join.Add(new Join(new Table("detalle_detencion", "E"), "I.id = E.DetenidoId" ));
                            join.Add(new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"));
                            //join.Add(new Join(new Table("Motivo_detencion_Interno", "T"), "T.InternoId  = E.Id", "LEFT"));
                            join.Add(new Join(new Table("calificacion", "ca"), "ca.InternoId  = E.Id"));
                            join.Add(new Join(new Table("situacion_detenido", "sd"), "sd.Id  = ca.SituacionId"));
                            join.Add(new Join(new Table("calificacion_ingreso", "ci"), "ci.CalificacionId = ca.Id", "LEFT"));
                            //join.Add(new Join(new Table("vDetenidohorasaresto", "V"), "v.DetalleDetencionId = E.Id", "LEFT"));
                            join.Add(new Join(new Table("salidaefectuadajuez", "Saj"), "E.id=Saj.DetalleDetencionId and Saj.Activo=1", "LEFT"));

                            //Agrego los filtros de fecha inicial y final 
                            where.Add(new Where("CAST(E.fecha as date)", ">=",fechaInicial));
                            where.Add(new Where("CAST(E.fecha as date)", "<=", fechaFinal));

                            /// FILTROS  genericos
                            if (opcion != "9" && opcion != "10")
                            {
                                where.Add(new Where("E.Activo", "1"));
                                where.Add(new Where("E.Estatus", "<>", "2"));
                            }

                            var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                            int usuario;

                            if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                            else throw new Exception("No es posible obtener información del usuario en el sistema");

                            int IdContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(IdContrato);

                            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                            var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                            where.Add(new Where("ifnull(Saj.TiposalidaId,0)", "<>", tipo.Id.ToString()));
                            where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("E.Tipo", contratoUsuario.Tipo));


                            //where.Add(new Where("cl.Accion", "Registrado"));

                            if ((!string.Equals(perfil, "Administrador")))
                            {
                                var user = ControlUsuario.Obtener(usuario);
                            }

                            /// 

                            if (opcion == "0" || opcion == null) //Todos
                            {

                            }
                            else if (opcion == "1") //Menores de edad
                            {
                                where.Add(new Where("TIMESTAMPDIFF(YEAR, g.FechaNacimineto, CURDATE())", "<", "18"));

                                //where.Add(new Where("cl.Accion", "Registrado"));

                                if ((!string.Equals(perfil, "Administrador")))
                                {
                                    var user = ControlUsuario.Obtener(usuario);
                                }
                            }
                            else if (opcion == "2") //Detenidos sin calificar
                            {
                                where.Add(new Where("ifnull (sd.Nombre, 'Pendiente')", "Pendiente"));

                                //where.Add(new Where("cl.Accion", "Registrado"));

                                if ((!string.Equals(perfil, "Administrador")))
                                {
                                    var user = ControlUsuario.Obtener(usuario);
                                }
                            }
                            else if (opcion == "3") //Próximos a cumplir
                            {
                                order.FirstOrDefault().column = 9;

                                //where.Add(new Where("V.Horas", "<>", "null"));
                                where.Add(new Where("ca.TotalHoras", "<>", "null"));

                                if ((!string.Equals(perfil, "Administrador")))
                                {
                                    var user = ControlUsuario.Obtener(usuario);
                                }
                            }
                            else if (opcion == "4") //Detenidos en examén medico : DEtenidos que tienen registrados al menos un examen medico.
                            {
                                join.Add(new Join(new Table("servicio_medico", "EM"), "EM.DetalledetencionId = E.Id", "INNER"));
                            }
                            else if (opcion == "5") //Detenidos calificados :Esta opción debe mostrar todos los detenidos  que ya fueron calificados.
                            {
                                where.Add(new Where("sd.Nombre", "<>", "null"));
                                // where.Add(new Where("ifnull (sd.Nombre, 'Pendiente')", "Pendiente"));

                                //where.Add(new Where("cl.Accion", "Registrado"));

                                if ((!string.Equals(perfil, "Administrador")))
                                {
                                    var user = ControlUsuario.Obtener(usuario);
                                }
                            }
                            else if (opcion == "6") //Detenidos en ingreso: los detenidos que ya pasaron por barandilla, es decir, que ya se registro su ingreso, más aún no se les ha realizado su examen médico y no han sido calificados.
                            {
                                join.Add(new Join(new Table("examen_medico", "EM"), "EM.DetalleDetencionId = E.Id"));
                                where.Add(new Where("ifnull(EM.Id,0)", "0"));
                                where.Add(new Where("ifnull(ca.Id,0)", "0"));
                            }
                            else if (opcion == "7")//Detenidos en custodia externa pendiente por definir
                            {

                            }
                            else if (opcion == "8") //Detenidos por fecha y hora de salida  pendiente por definir
                            {

                            }
                            else if (opcion == "9")//Detenidos salida autorizada: detenidos a los que se le ha autorizado la salida, pero que aún no se ha liberado. Son detenidos sin horas de arresto, sin multa
                            {
                                join.Add(new Join(new Table("historial", "H"), "H.InternoId = E.Id", "LEFT"));
                                where.Add(new Where("H.Movimiento", "Salida efectuada del detenido"));
                                where.Add(new Where("ifnull (V.Horas, '0')", "<=", "0"));
                                where.Add(new Where("ca.TotalAPagar", "0"));
                            }
                            else if (opcion == "10")//Salida autorizada por Pago de multa, pero que aún no han pagado. - En este filtro se muestran los detenidos que están pendientes de pago en caja.
                            {
                                join.Add(new Join(new Table("historial", "H"), "H.InternoId = E.Id", "LEFT"));
                                where.Add(new Where("H.Movimiento", "Salida efectuada del detenido"));
                                where.Add(new Where("ca.TotalAPagar", ">", "0"));
                            }

                            Query query = new Query
                            {
                                select = new List<string> {
                                "distinct (I.Id) Id",
                                "I.TrackingId",
                                "E.DetenidoId",
                                "E.Fecha",
                                "ltrim(I.Nombre) Nombre",
                                "ltrim(I.Paterno) Paterno",
                                "ltrim(I.Materno) Materno",
                                "I.RutaImagen",
                                "CAST( E.Expediente AS unsigned) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.ContratoId",
                                "E.Tipo",
                                "E.TrackingId TrackingIdEstatus" ,
                                "ES.Nombre EstatusNombre",
                                //join.Add(new Join(new Table($"(Expediente, NCP, Activo, Estatus, ContratoId, Tipo from detalle_detencion where year(Fecha) = {anio})", "E"), "I.id  = E.DetenidoId"));
                                //"ifnull(T.internoId,0) DetalledetencionId",
                                "ifnull(E.id,0) DetalledetencionId",
                                "ifnull (sd.Nombre, 'Sin calificar') Situacion",
                                "ifnull(ca.Id,0) CalificacionId",
                                "ifnull(ci.IngresoId,0) IngresoId",
                                //"timediff(date_add(E.Fecha,interval ifnull(ca.totalHoras,0) hour),CURRENT_TIMESTAMP() ) Horas ",
                                "ifnull(ca.TotalAPagar,0) TotalAPagar",
                                //"ifnull(V.horas,0) Horas",
                                "ifnull(ca.TotalHoras,0) Horas",
                                //"g.FechaNacimineto",
                                "concat(I.Nombre,' ',Paterno,' ',Materno) as NombreCompleto"
                            },
                                from = new Table("detenido", "I"),
                                joins = join,
                                /* joins = new List<Join>
                                 {
                                     new Join(new Table("general", "g"), "I.id  = g.DetenidoId"),
                                     new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                                     new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"),
                                     new Join(new Table("Motivo_detencion_Interno", "T"), "T.InternoId  = E.Id","LEFT"),
                                     new Join(new Table("calificacion", "ca"), "ca.InternoId  = E.Id"),
                                     new Join(new Table("situacion_detenido", "sd"), "sd.Id  = ca.SituacionId"),
                                     //new Join(new Table("calificacionlog", "cl"), "cl.InternoId = E.Id"),
                                     new Join(new Table("calificacion_ingreso", "ci"), "ci.CalificacionId = ca.Id", "LEFT"),
                                     new Join(new Table("vDetenidohorasaresto", "V"), "v.DetalleDetencionId = E.Id", "LEFT"),
                                     new Join(new Table("examen_medico", "EX"), "EX.DetalleDetencionId  = E.Id")
                                 },*/
                                wheres = where,
                                orderBy = "E.Fecha"
                            };

                            DataTablesAux dt = new DataTablesAux(mysqlConnection);
                            var result1 = dt.Generar(query, draw, start, length, search, order, columns);
                            return result1;
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    catch (Exception ex)
                    {
                        return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                    }
                    finally
                    {
                        if (mysqlConnection.State == System.Data.ConnectionState.Open)
                            mysqlConnection.Close();
                    }
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string blockitem(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Eliminar)
                {
                   
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string ValidaRegladeNegocio(string trackingid)
        {
            try
            {

                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                string nombrecompleto = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                //var examenmedico = ControlServicoMedico.ObtenerTodo().Where(x=>x.DetalledetencionId==detalleDetencion.Id);
               var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);



                if (examenmedico.Count()>0)
                {

                    return JsonConvert.SerializeObject(new { exitoso = true });

                }
                else
                {
                    var parametros = ControlParametroContrato.TraerTodos();
                    //if(parametros==null || parametros.Count==0)
                    //{
                    //    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "Al detenido "+nombrecompleto+" no se le ha realizado un examen médico." });
                    //}
                    Entity.ParametroContrato param = new Entity.ParametroContrato();

                    foreach (var item in parametros.Where(x=> x.ContratoId== detalleDetencion.ContratoId && x.Parametroid==1))
                    {
                        param = item;
                    }
                    //        if (param.Valor == "0")
                    //        {
                    //            return JsonConvert.SerializeObject(new { exitoso = true });
                    //        }
                    //        else {
                    //            Entity.ParametroContrato param2 = new Entity.ParametroContrato();

                    //            param2 = parametros.LastOrDefault(x => x.Parametroid == Convert.ToInt32(Entity.ParametroEnum.ADMINISTRAR_EXAMEN_MEDICO) && x.ContratoId == detalleDetencion.ContratoId);

                    //            if (param2 == null)
                    //            {
                    //                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "Al detenido " + nombrecompleto + " no se le ha realizado un examen médico." });

                    //            }
                    //            if (param2.Valor == "1")
                    //            {
                    //                var administrarexamenmedico = ControlAdministracionExamenMedico.obtenerPorDetalledetencion(detalleDetencion.Id);

                    //                if (!string.IsNullOrEmpty(administrarexamenmedico != null ? administrarexamenmedico.Justificacion : ""))
                    //                {
                    //                    return JsonConvert.SerializeObject(new { exitoso = true });
                    //                }
                    //            }

                    //            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "Al detenido " + nombrecompleto + " no se le ha realizado un examen médico." });
                    //        }
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
             }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static object getdetenidosexistenciaPrincipal(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string year, string pages)
        {
            try
            {
                string[] dataPermisos = new string[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" };
                var permiso = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(dataPermisos);
                bool permisoConsultar = permiso != null ? permiso.Consultar : false;

                if (permisoConsultar)
                {
                    if (!emptytableadd)
                    {

                        List<ReporteExtravioList> data = new List<ReporteExtravioList>();
                        int IdContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(IdContrato);
                        var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));

                        object[] parameters = new object[]
                        {
                            string.IsNullOrEmpty(year) ? DateTime.Now.Year : Convert.ToInt32(year),
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            
                        };

                        List<ReporteExtravioList> listTotals = ControlReporteExtravioList.ObtenerTodos(parameters);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        List<ReporteExtravioList> listFinal = new List<ReporteExtravioList>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;
                    }
                    else
                    {
                        object json2 = new { data = new List<JuezCalificadorList>(), recordsTotal = new List<JuezCalificadorList>().Count, recordsFiltered = new List<JuezCalificadorList>().Count, };
                        return json2;
                    }
                }
                else
                {
                    object json2 = new { data = new List<JuezCalificadorList>(), recordsTotal = new List<JuezCalificadorList>().Count, recordsFiltered = new List<JuezCalificadorList>().Count, };
                    return json2;
                }
            }
            catch (Exception ex)
            {
                object json2 = new { data = new List<JuezCalificadorList>(), recordsTotal = new List<JuezCalificadorList>().Count, recordsFiltered = new List<JuezCalificadorList>().Count, };
                return json2;
            }
        }

        [WebMethod]
        public static DataTable getdetenidosexistencia(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);


                        where.Add(new Where("E.Estatus", "<>", "2"));
                        where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("E.Tipo", contratoUsuario.Tipo));
              

                        Query query = new Query
                        {
                            select = new List<string> {
                                "I.Id",
                                "I.TrackingId",
                                "R.Nombre",
                                "I.Paterno",
                                "I.Materno",
                                "E.Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus" ,
                                "I.RutaImagen",
                                "IFNULL(a.alias, '') AliasInterno",
                                "IFNULL(G.fechanacimineto, NOW()) FechaNacimiento",
                                "E.DetalledetencionEdad Edad"
                            },
                            from = new Table("detenido", "I"),
                            joins = new List<Join>
                            {
                                new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                                new Join(new Table("(select DetenidoId, group_concat(Alias) alias from Alias  where Alias.Activo=1 group by DetenidoId)", "A"), "I.id  = A.DetenidoId", "LEFT OUTER"),
                                new Join(new Table("general", "G"), "I.id  = G.DetenidoId", "LEFT OUTER"),
                                new Join(new Table("(Select Concat(Nombre,' ',Paterno,' ',Materno)  Nombre,Id from detenido)", "R"), "I.id  = R.Id", "LEFT OUTER"),
                                new Join(new Table("informaciondedetencion", "N"), "I.id = N.IdInterno"),
                                new Join(new Table("eventos", "V"), "N.IdEvento = V.Id"),
                                new Join(new Table("detenido_evento", "T"), "V.Id = T.EventoId and I.Nombre = T.Nombre and I.Paterno = T.Paterno and I.Materno = T.Materno"),
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static string reporteextravio(string[] datos)
        {
            var serializedObject = string.Empty;
            try
            {
                var obj = ControlPDF.GenerarPDFReporteExtravio(datos);
                serializedObject = JsonConvert.SerializeObject(obj);

                Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                reportesLog.ProcesoId = Convert.ToInt32(datos[1]);
                reportesLog.ProcesoTrackingId = datos[0].ToString();
                reportesLog.Modulo = "Juez calificador";
                reportesLog.Reporte = "Reporte de extravío";
                reportesLog.Fechayhora = DateTime.Now;
                reportesLog.EstatusId = 1;
                reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                ControlReportesLog.Guardar(reportesLog);

                return serializedObject;
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        
        [WebMethod]
        public static List<Combo> getListadoCombo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(86);
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(c=>c.ContratoId==contratoUsuario.IdContrato && c.Tipo==contratoUsuario.Tipo &&c.Habilitado))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static Object trasladoSave(Traslado traslado)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId( new Guid(traslado.TrackingId));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                traslado.InternoId = detalleDetencion.Id.ToString();

                var EntidadFiltro = new Entity.Traslado();
                EntidadFiltro.DelegacionId = Convert.ToInt32(traslado.DelegacionId);
                EntidadFiltro.DetalleDetencionId = Convert.ToInt32(traslado.InternoId);

                Entity.Traslado TrasladoEntity = ControlTraslado.GetTrasladoNyKeys(EntidadFiltro);

                if (TrasladoEntity!=null)
                {
                    return new { exitoso = false, mensaje = "Ya se traslado el detenido a la delegación seleccionada", Id = "", TrackingId = "" };

                }

                if (traslado.InternoId=="0" && traslado.DelegacionId == "0")
                {
                    return new { exitoso = false, mensaje = "no se registro el traslado", Id = "", TrackingId = "" };
                }
                var califcacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);

                if (califcacion != null)
                {
                    if (califcacion.TotalAPagar > 0)


                    {
                        var calificacioningreso = ControlCalificacionIngreso.ObtenerPorCalificacionId(califcacion.Id);
                        if (calificacioningreso == null)
                        {
                            return new {alerta=true, exitoso = false, mensaje = "El detenido "+ interno.Nombre+" "+interno.Paterno+" "+interno.Materno + " tiene una multa pendiente por pagar ", Id = "", TrackingId = "" };
                        }
                    }
                }


                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                Entity.Traslado entTraslado = new Entity.Traslado();
                entTraslado.DelegacionId =Convert.ToInt32( traslado.DelegacionId);
                entTraslado.DetalleDetencionId = Convert.ToInt32(traslado.InternoId);
                entTraslado.Activo = 1;
                entTraslado.Habilitado = 1;
                entTraslado.Fechahora = DateTime.Now;
                entTraslado.TrackingId= Guid.NewGuid().ToString();
                entTraslado.CreadoPor = usId;
                entTraslado.TiposalidaId = Convert.ToInt32(traslado.TiposalidaId);
                entTraslado.Fundamento = traslado.Fundamento;
                entTraslado.Horas = traslado.Horas;
                int valor = ControlTraslado.Guardar(entTraslado);

                if (valor > 0)
                {
                    Entity.Historial historial = new Entity.Historial();
                    historial.Activo = true;
                    historial.CreadoPor = usId;
                    historial.Fecha = DateTime.Now;
                    historial.Habilitado = true;
                    historial.InternoId = detalleDetencion.Id;
                    historial.Movimiento = "Traslado del detenido";
                    historial.TrackingId = new Guid(entTraslado.TrackingId);
                    var contratoUsuario1 = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato1 = ControlSubcontrato.ObtenerPorId(contratoUsuario1.IdContrato);
                    historial.ContratoId = subcontrato1.Id;
                    historial.Id = ControlHistorial.Guardar(historial);
                }

                object[] data = new object[]
                    {
                        entTraslado.DetalleDetencionId,
                        entTraslado.DelegacionId
                    };
                object[] dataArchivo = ControlPdfReporteSalidaTrasladocs.generarAutorizacionSalida(data);
                Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                reportesLog.ProcesoId = Convert.ToInt32(interno.Id);
                reportesLog.ProcesoTrackingId = interno.TrackingId.ToString();
                reportesLog.Modulo = "Juez calificador";
                reportesLog.Reporte = "Autorización de salida a disposición de otra autoridad";
                reportesLog.Fechayhora = DateTime.Now;
                reportesLog.EstatusId = 1;
                reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                ControlReportesLog.Guardar(reportesLog);


                Entity.SalidaEfectuadaJuez salidaefectuada = new Entity.SalidaEfectuadaJuez();
                salidaefectuada.Activo = 1;
                salidaefectuada.Habilitado = 1;
                salidaefectuada.Creadopor = usId;
                salidaefectuada.DetalledetencionId = entTraslado.DetalleDetencionId;
                
                salidaefectuada.Fundamento = traslado.Fundamento;
                salidaefectuada.TiposalidaId = entTraslado.TiposalidaId;
                salidaefectuada.TrackingId = Guid.NewGuid().ToString();
                ControlSalidaEfectuadaJuez.Guardar(salidaefectuada);
                Entity.Historial historial2 = new Entity.Historial();
                historial2.Activo = true;
                historial2.CreadoPor = usId;
                historial2.Fecha = DateTime.Now;
                historial2.Habilitado = true;
                historial2.InternoId = entTraslado.DetalleDetencionId;
                historial2.Movimiento = "Salida efectuada del detenido";
                historial2.TrackingId = new Guid(salidaefectuada.TrackingId);
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                historial2.ContratoId = subcontrato.Id;
                historial2.Id = ControlHistorial.Guardar(historial2);

                detalleDetencion.Estatus = 2;
                detalleDetencion.Activo = false;
                ControlDetalleDetencion.Actualizar(detalleDetencion);

                if (!(valor > 0))
                {
                    throw new Exception("No se pudo registrar el traslado");
                }

                return new { exitoso = true, mensaje = "El traslado del detenido se realizó correctamente", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", Id = "", TrackingId = "" };
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Historial historial = ControlHistorial.ObtenerPorDetenido(detalleDetencion.DetenidoId).FirstOrDefault();
                if (detalleDetencion == null)
                {

                    detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                }

                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                Entity.Colonia colonia = domicilio != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;
                Entity.InformacionDeDetencion infoDet = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                Entity.Antropometria antro = ControlAntropometria.ObtenerPorDetenidoId(interno.Id);

                var edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);
                    else
                    {
                        var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                        var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                        var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                        Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                        foreach (var item in detenidosEvento)
                        {
                            if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                detenidoEvento = item;
                        }

                        if (detenidoEvento != null) edad = detenidoEvento.Edad;
                    }
                }
                var evento2 = ControlEvento.ObtenerById(infoDet.IdEvento);
                var motivo = "";
                edad = detalleDetencion.DetalledetencionEdad;
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(infoDet.IdDetenido_Evento);
                if(detenidoevento!=null)
                {
                    var m = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);

                    if (m != null)
                    {
                        motivo = m.NombreMotivoLlamada;
                    }
                }
                
                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = "Calle: " + domicilio.Calle + " #" + domicilio.Numero + "";

                }

                string strColonia = "";
                if (colonia != null)
                {
                    strColonia = ",Colonia: " + colonia.Asentamiento + " CP:" + colonia.CodigoPostal + "";

                }

                string domCompleto = strDomicilio + "" + strColonia;

                object obj = null;

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = evento2.HoraYFecha;
                TimeSpan horasRestantes;
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                var situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;

                if (calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }
                else
                {
                    //fechaSalidaAdd = fechaSalida.AddHours(36);
                    //horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                    fechaSalidaAdd = DateTime.MinValue;
                    horasRestantes = TimeSpan.MinValue;
                }
                if (interno != null)
                {
                    if (string.IsNullOrEmpty(interno.RutaImagen))
                    {
                        interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                    }
                }
                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = situacion != null ? situacion.Nombre : "",
                    Domicilio = domCompleto,
                    RutaImagen = interno.RutaImagen,
                    //Registro = detalleDetencion.Fecha.Value.ToString("dd-MM-yyyy HH:mm:ss"),
                    Registro = historial.Fecha.ToString("dd-MM-yyyy HH:mm:ss"),
                    Salida = fechaSalidaAdd != DateTime.MinValue ? fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                    restantes = horasRestantes != TimeSpan.MinValue ? horasRestantes.TotalHours : 0,
                    Motivo = motivo,
                    Institucion = institucion != null ? institucion.Nombre : "",
                    Estatura = antro != null ? antro.Estatura.ToString() : 0.ToString(),
                    Peso = antro != null ? antro.Peso.ToString() : "0.00",
                    Motivo_Evento = evento2 != null ? evento2.Descripcion :""
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getfecha(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);

                object obj = null;
                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = detalleDetencion.Fecha.Value;
                TimeSpan horasRestantes;
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                var situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;

                if (calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }
                else
                {
                    fechaSalidaAdd = fechaSalida.AddHours(36);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }

                obj = new
                {
                    Salida = fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss")
                };
                
                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string guardaSalidaEfectuada(string[] datos, string observacion, string responsable)
        {
            try
            {
                ArrayList arrayList = new ArrayList();
                string errores = "";
                ArrayList listaErrores = new ArrayList();
                Entity.Detenido interno = new Entity.Detenido();
                Entity.DetalleDetencion detalleDetencion = new Entity.DetalleDetencion();
                Entity.SalidaEfectuada salida_efectuada = new Entity.SalidaEfectuada();
                Entity.Calificacion calificacion = new Entity.Calificacion();
                Entity.Catalogo situacion = new Entity.Catalogo();
                Entity.TrabajoSocial trabajoSocial = new Entity.TrabajoSocial();
                Entity.Institucion institucion = new Entity.Institucion();
                Entity.Usuario usuario = new Entity.Usuario();
                Entity.General general = new Entity.General();
                Entity.Catalogo sexo = new Entity.Catalogo();
                Entity.Catalogo estadoCivil = new Entity.Catalogo();
                Entity.Catalogo escolaridad = new Entity.Catalogo();
                Entity.Catalogo nacionalidad = new Entity.Catalogo();
                Entity.Domicilio domicilio = new Entity.Domicilio();
                Entity.Colonia colonia = new Entity.Colonia();
                Entity.Municipio municipio = new Entity.Municipio();
                Entity.Estado estado = new Entity.Estado();
                Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                Entity.Catalogo unidad = new Entity.Catalogo();
                Entity.Catalogo responsableUnidad = new Entity.Catalogo();
                Entity.Colonia coloniaDetencion = new Entity.Colonia();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                usuario = ControlUsuario.Obtener(usId);
                int idmovimiento = 0;

                for (int i = 0; i < datos.Length; i++)
                {
                    interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[i].ToString()));
                    general = interno != null ? ControlGeneral.ObtenerPorDetenidoId(interno.Id) : null;
                    sexo = general != null ? ControlCatalogo.Obtener(general.SexoId, 4) : null; 
                    estadoCivil = general != null ? ControlCatalogo.Obtener(general.EstadoCivilId, 26) : null;
                    escolaridad = general != null ? ControlCatalogo.Obtener(general.EscolaridadId, 15) : null;
                    nacionalidad = general != null ? ControlCatalogo.Obtener(general.NacionalidadId, 28) : null;
                    domicilio = interno != null ? ControlDomicilio.ObtenerPorId(interno.DomicilioId) : null;
                    colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null : null;
                    municipio = colonia != null ? ControlMunicipio.Obtener(colonia.IdMunicipio) : null;
                    estado = municipio != null ? ControlEstado.Obtener(municipio.EstadoId) : null;                    
                    infoDetencion = interno != null ? ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id) : null;
                    unidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.UnidadId, 82) : null;
                    responsableUnidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.ResponsableId, 83) : null;
                    coloniaDetencion = (infoDetencion != null) ? ControlColonia.ObtenerPorId(infoDetencion.ColoniaId) : null;
                    string idInterno = interno != null ? interno.Id.ToString() : "";
                    string[] internod = new string[2] {
                        idInterno, "true"
                    };

                    detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                    calificacion = detalleDetencion != null ? ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id) : null;
                    situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;
                    institucion = detalleDetencion != null ? ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId) : null;


                    if (ValidaReglaDeNegocioSalidaEfectuadaBiometricos(detalleDetencion.DetenidoId, detalleDetencion.ContratoId) != "OK")
                    {
                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("Al detenido " + detenidoNombre + " no se le han registrado sus biométricos,  ");
                        continue;
                    }
                    if (ValidaReglaDenegocioExamenmedico(detalleDetencion.Id, detalleDetencion.ContratoId) != "OK")
                    {

                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("Al detenido " + detenidoNombre + " no se le ha realizado un examen médico,  ");
                        continue;

                    }
                    if (calificacion != null)
                    {
                        if (calificacion.TotalHoras > 0)
                        {
                            DateTime fechaSalidaAdd = new DateTime();
                            DateTime fechaSalida = detalleDetencion.Fecha.Value;
                            TimeSpan horasRestantes;
                            fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                            horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);

                            if (horasRestantes.TotalHours <= -0.001)
                            {
                                string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                                listaErrores.Add("El detenido " + detenidoNombre + " tiene horas de arresto por cumplir,  ");
                                continue;
                            }
                        }

                        if (calificacion.TotalAPagar > 0)
                        {                            
                            //Obtener la calificacion_ingreso por id de la calificacion
                            Entity.CalificacionIngreso calificacionIngreso = ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacion.Id);
                            if(calificacionIngreso == null)
                            {
                                string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                                listaErrores.Add("El detenido " + detenidoNombre + " tiene el pago de multa pendiente,  ");
                                continue;
                            }                                                                       
                        }
                    }

                    salida_efectuada.Activo = 1;
                    salida_efectuada.Habilitado = 1;
                    salida_efectuada.Creadopor = usId;
                    salida_efectuada.DetalledetencionId = detalleDetencion != null ? detalleDetencion.Id : 0;
                    salida_efectuada.Observacion = observacion;
                    if(situacion != null)
                    {
                        if(situacion.Nombre.ToUpper() == "POR CONSIGNAR" || situacion.Nombre.ToUpper() == "A DISPOSICIÓN DE OTRA AUTORIDAD" || situacion.Nombre.ToUpper() == "POR ORDEN DE APREHENSIÓN" || situacion.Nombre.ToUpper() == "ENTREGADO A FAMILIAR")
                        {
                            salida_efectuada.Responsabledeldetenido = responsable;
                        }
                        else
                        {
                            salida_efectuada.Responsabledeldetenido = "";
                        }
                    }
                    
                    salida_efectuada.TrackingId = Guid.NewGuid().ToString();

                    if(calificacion != null)
                    {
                        idmovimiento = ControlSalidaEfectuada.Guardar(salida_efectuada);
                        if(idmovimiento > 0)
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = detalleDetencion.Id;
                            historial.Movimiento = "Salida efectuada del detenido";
                            historial.TrackingId = detalleDetencion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }
                    }
                    else
                    {
                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("El detenido " + detenidoNombre + " no se le ha realizado una calificación,  ");
                        continue;
                    }
                    

                    if(calificacion != null)
                    {
                        if (calificacion.TrabajoSocial)
                        {
                            //Obtener el trabajo social del interno
                            trabajoSocial = detalleDetencion != null ? ControlTrabajoSpcial.ObtenerByDetenidoId(detalleDetencion.Id) : null;
                            //Luego desactivarlo
                            trabajoSocial.Activo = 0;                           
                            ControlTrabajoSpcial.SalidaEfectuada(trabajoSocial);
                        }
                    }
                    
                    //Generar autorizacion de salida

                    DateTime fecha = general != null ? general.FechaNacimineto : new DateTime();
                    TimeSpan time = DateTime.Today.Subtract(fecha);
                    var edad = "";
                    int auxEdad;
                    if (time.Days > 700000)
                        edad = "Sin Dato";
                    else
                    {
                        auxEdad = Convert.ToInt32(time.TotalDays) / 365;
                        edad = auxEdad.ToString();
                    }                    

                    object[] data = new object[]
                    {
                        (institucion != null) ? institucion.Nombre : "",
                        (usuario != null) ? usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno : "",
                        (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                        (detalleDetencion != null) ? detalleDetencion.Fecha.ToString() : "",
                        (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                        edad,
                        sexo != null ? sexo.Nombre : "",
                        estadoCivil != null ? estadoCivil.Nombre : "",
                        escolaridad != null ? escolaridad.Nombre : "",
                        nacionalidad != null ? nacionalidad.Nombre : "",
                        "",
                        domicilio != null ? domicilio.Calle + " " + domicilio.Numero : "",
                        colonia != null ? colonia.Asentamiento : "",
                        municipio != null ? municipio.Nombre : "",
                        estado != null ? estado.Nombre : "",
                        domicilio != null ? domicilio.Telefono : "",
                        (unidad != null) ? unidad.Id.ToString() : "",
                        (responsableUnidad != null) ? responsableUnidad.Id.ToString() : "",
                        (responsableUnidad != null) ? responsableUnidad.Nombre : "",
                        (infoDetencion != null) ? infoDetencion.Id.ToString() : "",
                        (infoDetencion != null) ? infoDetencion.Descripcion : "",
                        (infoDetencion != null) ? infoDetencion.LugarDetencion : "",
                        (coloniaDetencion != null) ? coloniaDetencion.Asentamiento : "",
                        DateTime.Today,
                        usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno,
                        situacion != null ? situacion.Nombre : "",
                        calificacion != null ? calificacion.SoloArresto ? "Si" : "No" : "",
                        calificacion != null ? calificacion.TotalHoras : 0,
                        calificacion != null ? calificacion.TotalAPagar : 0,
                        calificacion != null ? calificacion.Fundamento : "",
                        calificacion != null ? calificacion.Razon : ""
                    };
                    
                    object[] dataArchivo = ControlPDF.generarAutorizacionSalida(data);
                    //Guardar los archivos generados
                    if (dataArchivo != null)
                    {
                        arrayList.Add(dataArchivo[1]);
                    }

                    if (idmovimiento > 0)
                    {
                        detalleDetencion.Estatus = 2;
                        detalleDetencion.Activo = false;
                        ControlDetalleDetencion.Actualizar(detalleDetencion);
                    }
                }

                foreach(var item in listaErrores)
                {
                    errores += item;
                }

                return JsonConvert.SerializeObject(new { exitoso = true, archivos = arrayList, mensaje = "La salida autorizada se realizó satisfactoriamente", errores = errores });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getDataHistorial(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string TrackingId)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(TrackingId));
                        string[] data = new string[]
                        {
                            interno.Id.ToString(),
                            "true"
                        };
                        var detalle_detencion = interno != null ? ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(data) : null;

                        where.Add(new Where("O.Activo", "1"));

                        if (detalle_detencion != null)
                        {
                            where.Add(new Where("O.InternoId", detalle_detencion.Id.ToString()));
                        }

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        //where.Add(new Where("I.ContratoId", Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]).ToString()));

                        //Prueba de IN

                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        Query query = new Query
                        {
                            select = new List<string> {
                                "O.Id",
                                "O.TrackingId",
                                "O.InternoId",
                                "O.Descripcion",
                                "O.Movimiento",
                                "date_format(O.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "O.Activo",
                                "O.Habilitado",
                                "U.Usuario Usuario"
                            },
                            from = new DT.Table("historialcalificacion", "O"),
                            joins = new List<Join>
                            {
                               new Join(new Table("vusuarios","V"),"O.Creadopor=V.Id ","LEFT OUTER"),
                                new Join(new DT.Table("usuario", "U"), "U.id  = V.UsuarioId")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var result1 = dt.Generar(query, draw, start, length, search, order, columns);

                        return result1;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string GetParametrosTamaño()
        {
            try
            {
                var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                var parametro = ControlParametroContrato.TraerTodos();
                decimal maximo = 0;
                decimal minimo = 0;

                foreach (var item in parametro)
                {
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 13)
                    {
                        minimo = Convert.ToDecimal(item.Valor);
                    }
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 14)
                    {
                        maximo = Convert.ToDecimal(item.Valor);
                    }
                }
                return JsonConvert.SerializeObject(new { exitoso = true, TMax = maximo, Tmin = minimo });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static object GetAlertaWeb()
        {
            try
            {
                var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
                var denoma = "Alerta web";
                var alerta = "alerta web";
                if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
                {
                    denoma = datosAlertaWeb.Denominacion;
                    alerta = denoma;
                }
                object data = new
                {

                    Denominacion = denoma,
                    AlertaWerb = alerta
                };

                return data;
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }
        [WebMethod]
        public static object ReportesSalidas(string tracking)
        {
            try
            {
                ArrayList arrayList = new ArrayList();
               
                ArrayList listaErrores = new ArrayList();
                Entity.Detenido interno = new Entity.Detenido();
                
                Entity.DetalleDetencion estatusInterno = new Entity.DetalleDetencion();
                Entity.SalidaEfectuadaJuez salida_efectuada = new Entity.SalidaEfectuadaJuez();
                Entity.Calificacion calificacion = new Entity.Calificacion();
                Entity.Catalogo situacion = new Entity.Catalogo();
                Entity.TrabajoSocial trabajoSocial = new Entity.TrabajoSocial();
                Entity.Institucion centroReclusion = new Entity.Institucion();
                Entity.Usuario usuario = new Entity.Usuario();
                Entity.General general = new Entity.General();
                Entity.Catalogo sexo = new Entity.Catalogo();
                Entity.Catalogo estadoCivil = new Entity.Catalogo();
                Entity.Catalogo escolaridad = new Entity.Catalogo();
                Entity.Catalogo nacionalidad = new Entity.Catalogo();
                Entity.Domicilio domicilio = new Entity.Domicilio();
                Entity.Colonia colonia = new Entity.Colonia();
                Entity.Municipio municipio = new Entity.Municipio();
                Entity.Estado estado = new Entity.Estado();
                Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                Entity.Catalogo unidad = new Entity.Catalogo();
                Entity.Catalogo responsableUnidad = new Entity.Catalogo();
                Entity.Colonia coloniaDetencion = new Entity.Colonia();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                usuario = ControlUsuario.Obtener(usId);
                int idmovimiento = 0;
                estatusInterno = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(tracking));
                interno = ControlDetenido.ObtenerPorId(estatusInterno.DetenidoId);
                general = interno != null ? ControlGeneral.ObtenerPorDetenidoId(interno.Id) : null;
                sexo = general != null ? ControlCatalogo.Obtener(general.SexoId, 4) : null;
                estadoCivil = general != null ? ControlCatalogo.Obtener(general.EstadoCivilId, 26) : null;
                escolaridad = general != null ? ControlCatalogo.Obtener(general.EscolaridadId, 15) : null;
                nacionalidad = general != null ? ControlCatalogo.Obtener(general.NacionalidadId, 28) : null;
                domicilio = interno != null ? ControlDomicilio.ObtenerPorId(interno.DomicilioId) : null;
                colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null : null;
                municipio = colonia != null ? ControlMunicipio.Obtener(colonia.IdMunicipio) : null;
                estado = municipio != null ? ControlEstado.Obtener(municipio.EstadoId) : null;
                infoDetencion = interno != null ? ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id) : null;
                unidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.UnidadId, 82) : null;
                responsableUnidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.ResponsableId, 83) : null;
                coloniaDetencion = (infoDetencion != null) ? ControlColonia.ObtenerPorId(infoDetencion.ColoniaId) : null;
                var motivoE = "";
                var evento = ControlEvento.ObtenerById(infoDetencion.IdEvento);
                var eventodetenido = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);
                if (eventodetenido != null)
                {
                    var motivo = ControlWSAMotivo.ObtenerPorId(eventodetenido.MotivoId);
                    if (motivo != null) motivoE = motivo.NombreMotivoLlamada;
                }
                
                calificacion = estatusInterno != null ? ControlCalificacion.ObtenerPorInternoId(estatusInterno.Id) : null;
                situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;
                centroReclusion = estatusInterno != null ? ControlInstitucion.ObtenerPorId(estatusInterno.CentroId) : null;
                var historialsalida = ControlHistorial.ObtenerPorDetenido(estatusInterno.Id).Where(x=> x.Movimiento== "Salida efectuada del detenido");
                var historialtraslado = ControlHistorial.ObtenerPorDetenido(estatusInterno.Id).Where(x => x.Movimiento== "Traslado del detenido");
                var v = 0;
                v = historialtraslado.Count();
                var fecha1 = "";
                var f1 = DateTime.Now;
                var c = 0;
                object[] dataArchivo = null;
                foreach (var item in historialsalida)
                {
                    c++;
                    f1 = item.Fecha;
                    usuario = ControlUsuario.Obtener(item.CreadoPor);
                }
                var h1 = f1.Hour.ToString();
                if (f1.Hour < 10) h1 = "0" + h1;
                var m1 = f1.Minute.ToString();
                if (f1.Minute < 10) m1 = "0" + m1;
                var s1 = f1.Second.ToString();
                if (f1.Second < 10) s1 = "0" + s1;
                fecha1 = f1.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;
                var registro = "";
                var edad = "";
                List<Entity.Unidad> unidades = new List<Entity.Unidad>();
                unidades = ControlUnidad.ObtenerPorEventoId(infoDetencion.IdEvento);
                string unidadesPlaca = "";
                string responsableunidaddescripcion = "";
                string responsableunidadnombre = "";

                if (unidades.Count > 0)
                {
                    unidadesPlaca = string.Join(", ", unidades.Select(x => x.Nombre));
                    List<Entity.Catalogo> responsables = new List<Entity.Catalogo>();
                    foreach (var item in unidades)
                    {
                        var responsableunidad = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { item.Id, infoDetencion.IdEvento });
                        responsables.Add(responsableunidad);
                        responsableunidadnombre += (responsableunidad.Nombre ?? "") + ", ";
                    }
                    responsableunidaddescripcion = string.Join(", ", responsables.Select(x => x.Descripcion));
                    responsableunidadnombre = string.Join(", ", responsables.Select(x => x.Nombre));
                }

                if (usuario.RolId == 13)
                {
                    usuario = ControlUsuario.Obtener(8);
                }
                if (estatusInterno != null)
                {
                    var f = Convert.ToDateTime(estatusInterno.Fecha);
                    var h = f.Hour.ToString();
                    if (f.Hour < 10) h = "0" + h;
                    var m = f.Minute.ToString();
                    if (f.Minute < 10) m = "0" + m;
                    var s = f.Second.ToString();
                    if (f.Second < 10) s = "0" + s;
                    registro = f.ToShortDateString() + " " + h + ":" + m + ":" + s;
                    edad = estatusInterno.DetalledetencionEdad.ToString();
                }
                var historialcalif = ControlHistorial.ObtenerPorDetenido(estatusInterno.Id).Where(x => x.Movimiento.Contains("calif")).FirstOrDefault();
                var usuario2 = ControlUsuario.Obtener(historialcalif.CreadoPor);
                object[] data = new object[]
                   {
                        (centroReclusion != null) ? centroReclusion.Nombre : "",
                        (usuario != null) ? usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno : "",
                        (estatusInterno != null) ? estatusInterno.Expediente : "",
                        registro,
                        (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                        edad,
                        sexo != null ? sexo.Nombre : "",
                        estadoCivil != null ? estadoCivil.Nombre : "",
                        escolaridad != null ? escolaridad.Nombre : "",
                        nacionalidad != null ? nacionalidad.Nombre : "",
                        "",
                        domicilio != null ? domicilio.Calle + " " + domicilio.Numero : "",
                        colonia != null ? colonia.Asentamiento : "",
                        municipio != null ? municipio.Nombre : "",
                        estado != null ? estado.Nombre : "",
                        domicilio != null ? domicilio.Telefono : "",
                        unidadesPlaca,
                        responsableunidaddescripcion,
                        responsableunidadnombre,
                        (infoDetencion != null) ? infoDetencion.Id.ToString() : "",
                        motivoE,
                        (infoDetencion != null) ? infoDetencion.LugarDetencion : "",
                        (coloniaDetencion != null) ? coloniaDetencion.Asentamiento : "",
                        fecha1,
                        usuario2.Nombre + " " + usuario2.ApellidoPaterno + " " + usuario2.ApellidoMaterno,
                        situacion != null ? situacion.Nombre : "",
                        calificacion != null ? calificacion.SoloArresto ? "Si" : "No" : "",
                        calificacion != null ? calificacion.TotalHoras : 0,
                        calificacion != null ? calificacion.TotalAPagar : 0,
                        calificacion != null ? calificacion.Fundamento : "",
                        calificacion != null ? calificacion.Razon : "",
                        1.ToString()
                   };
                
                if(v>0)
                {
                    var t = new Entity.Traslado();
                    t.DetalleDetencionId = estatusInterno.Id;
                    var traslado = ControlTraslado.GetTrasladoByDetalleDetencion(t);
                    if (traslado != null)
                    {
                        object[] data1 = new object[]
                                            {
                        traslado.DetalleDetencionId,
                        traslado.DelegacionId
                                            };
                        dataArchivo = ControlPdfReporteSalidaTrasladocs.generarAutorizacionSalida(data1);
                    }
                    
                }
                else
                {
                    dataArchivo = ControlPDF.generarAutorizacionSalida(data);
                }
                return new { exitoso = true, mensaje = "El reporte de salida del detenido se generó correctamente", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", Id = "", TrackingId = "" };

            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        //console
        [WebMethod]
        public static string guardaSalidaEfectuadaJuez(string[] datos, string fundamento, string tiposalidaid)
        {
            try
            {
                ArrayList arrayList = new ArrayList();
                string errores = "";
                ArrayList listaErrores = new ArrayList();
                Entity.Detenido interno = new Entity.Detenido();
                Entity.DetalleDetencion estatusInterno = new Entity.DetalleDetencion();
                Entity.SalidaEfectuadaJuez salida_efectuada = new Entity.SalidaEfectuadaJuez();
                Entity.Calificacion calificacion = new Entity.Calificacion();
                Entity.Catalogo situacion = new Entity.Catalogo();
                Entity.TrabajoSocial trabajoSocial = new Entity.TrabajoSocial();
                Entity.Institucion centroReclusion = new Entity.Institucion();
                Entity.Usuario usuario = new Entity.Usuario();
                Entity.General general = new Entity.General();
                Entity.Catalogo sexo = new Entity.Catalogo();
                Entity.Catalogo estadoCivil = new Entity.Catalogo();
                Entity.Catalogo escolaridad = new Entity.Catalogo();
                Entity.Catalogo nacionalidad = new Entity.Catalogo();
                Entity.Domicilio domicilio = new Entity.Domicilio();
                Entity.Colonia colonia = new Entity.Colonia();
                Entity.Municipio municipio = new Entity.Municipio();
                Entity.Estado estado = new Entity.Estado();
                Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                Entity.Catalogo unidad = new Entity.Catalogo();
                Entity.Catalogo responsableUnidad = new Entity.Catalogo();
                Entity.Colonia coloniaDetencion = new Entity.Colonia();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                usuario = ControlUsuario.Obtener(usId);
                int idmovimiento = 0;
                
                for (int i = 0; i < datos.Length; i++)
                {
                    interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[i].ToString()));
                    general = interno != null ? ControlGeneral.ObtenerPorDetenidoId(interno.Id) : null;
                    sexo = general != null ? ControlCatalogo.Obtener(general.SexoId, 4) : null;
                    estadoCivil = general != null ? ControlCatalogo.Obtener(general.EstadoCivilId, 26) : null;
                    escolaridad = general != null ? ControlCatalogo.Obtener(general.EscolaridadId, 15) : null;
                    nacionalidad = general != null ? ControlCatalogo.Obtener(general.NacionalidadId, 28) : null;
                    domicilio = interno != null ? ControlDomicilio.ObtenerPorId(interno.DomicilioId) : null;
                    colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null : null;
                    municipio = colonia != null ? ControlMunicipio.Obtener(colonia.IdMunicipio) : null;
                    estado = municipio != null ? ControlEstado.Obtener(municipio.EstadoId) : null;
                    infoDetencion = interno != null ? ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id) : null;
                    unidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.UnidadId, 82) : null;
                    responsableUnidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.ResponsableId, 83) : null;
                    coloniaDetencion = (infoDetencion != null) ? ControlColonia.ObtenerPorId(infoDetencion.ColoniaId) : null;
                    var motivoE = "";
                    var evento = ControlEvento.ObtenerById(infoDetencion.IdEvento);
                    var eventodetenido = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);
                    if(eventodetenido!=null)
                    {
                        var motivo = ControlWSAMotivo.ObtenerPorId(eventodetenido.MotivoId);
                        if (motivo != null) motivoE = motivo.NombreMotivoLlamada;
                    }
                    
                       
                    string idInterno = interno != null ? interno.Id.ToString() : "";
                    string[] internod = new string[2] {
                        idInterno, "true"
                    };

                    estatusInterno = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                    calificacion = estatusInterno != null ? ControlCalificacion.ObtenerPorInternoId(estatusInterno.Id) : null;
                    situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;
                    centroReclusion = estatusInterno != null ? ControlInstitucion.ObtenerPorId(estatusInterno.CentroId) : null;

                    if (ValidaReglaDeNegocioSalidaEfectuadaBiometricos(estatusInterno.DetenidoId, estatusInterno.ContratoId) != "OK")
                    {
                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("Al detenido " + detenidoNombre + " no se le han registrado sus biométricos,  ");
                        continue;
                    }

                    if (ValidaReglaDenegocioExamenmedico(estatusInterno.Id, estatusInterno.ContratoId) != "OK")
                    {

                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("Al detenido " + detenidoNombre + " no se le ha realizado un examen médico");
                        continue;

                    }

                    if (calificacion != null)
                    {
                        if (calificacion.TotalHoras > 0)
                        {
                            DateTime fechaSalidaAdd = new DateTime();
                            DateTime fechaSalida = evento.HoraYFecha;
                            TimeSpan horasRestantes;
                            fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                            horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                            var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                            if (Convert.ToInt32(tiposalidaid) != tipo.Id)
                            {
                                if (horasRestantes.TotalHours <= -0.001)
                                {
                                    string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                                    listaErrores.Add("El detenido " + detenidoNombre + " tiene horas de arresto por cumplir,  ");
                                    continue;
                                }
                            }

                        }

                        if (!calificacion.SoloArresto)
                        {
                            if (calificacion.TotalAPagar < 0)
                            {
                                //Obtener la calificacion_ingreso por id de la calificacion
                                Entity.CalificacionIngreso calificacionIngreso = ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacion.Id);
                                if (calificacionIngreso == null)
                                {
                                    string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                                    listaErrores.Add("El detenido " + detenidoNombre + " tiene el pago de multa pendiente,  ");
                                    continue;
                                }
                            }
                        }
                    }

                    salida_efectuada.Activo = 1;
                    salida_efectuada.Habilitado = 1;
                    salida_efectuada.Creadopor = usId;
                    salida_efectuada.DetalledetencionId = estatusInterno != null ? estatusInterno.Id : 0;
                    salida_efectuada.Fundamento = fundamento;
                    salida_efectuada.TrackingId = Guid.NewGuid().ToString();
                    salida_efectuada.TiposalidaId = Convert.ToInt32(tiposalidaid);
                    string tipoDeSalida = string.Empty;
                    List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                    if (listado.Count > 0)
                    {
                        foreach (var rol in listado.Where(c => c.Habilitado))
                        {
                            //if ( rol.TrackingId.ToString() == salida_efectuada.TrackingId)
                            if (rol.Id == salida_efectuada.TiposalidaId)
                            {
                                tipoDeSalida = rol.Nombre;
                            }
                        }
                    }

                    Entity.InformacionDeDetencion infoDet = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                    string motivoDescripcion = infoDet.Descripcion;

                    if (calificacion != null)
                    {

                        idmovimiento = ControlSalidaEfectuadaJuez.Guardar(salida_efectuada);
                        if (idmovimiento > 0)
                        {
                            var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                            var movimiento = "";
                            if (Convert.ToInt32(tiposalidaid) != tipo.Id)
                            {
                                movimiento= "Salida efectuada del detenido";
                            }
                            else
                            {
                                Entity.Historial historial1 = new Entity.Historial();
                                historial1.Activo = true;
                                historial1.CreadoPor = usId;
                                historial1.Fecha = DateTime.Now;
                                historial1.Habilitado = true;
                                historial1.InternoId = estatusInterno.Id;
                                historial1.Movimiento = "Solicitud de amparo registrada";
                                historial1.TrackingId = estatusInterno.TrackingId;
                                var contratoUsuario1 = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                                var subcontrato1 = ControlSubcontrato.ObtenerPorId(contratoUsuario1.IdContrato);
                                historial1.ContratoId = subcontrato1.Id;
                                historial1.Id = ControlHistorial.Guardar(historial1);
                                movimiento = "Solicitúd de amparoregistrada";
                            }
                                Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = estatusInterno.Id;
                            historial.Movimiento = "Salida efectuada del detenido";
                            historial.TrackingId = estatusInterno.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);
                        }

                    }
                    else
                    {
                        string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                        listaErrores.Add("Al detenido " + detenidoNombre + " no se le ha realizado una calificación");
                        continue;
                    }
                    if (tiposalidaid == "7")
                    {
                        var calificacioningreso_ = ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacion.Id);
                        if (calificacioningreso_ == null)
                        {
                            string detenidoNombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                            listaErrores.Add("El detenido " + detenidoNombre + " no tiene registrado el pago de una multa,  ");
                            continue;
                        }
                    }

                    if (calificacion != null)
                    {
                        if (calificacion.TrabajoSocial)
                        {
                            //Obtener el trabajo social del interno
                            trabajoSocial = estatusInterno != null ? ControlTrabajoSpcial.ObtenerByDetenidoId(estatusInterno.Id) : null;
                            //Luego desactivarlo
                            trabajoSocial.Activo = 0;

                            ControlTrabajoSpcial.SalidaEfectuada(trabajoSocial);

                        }
                    }

                    //Generar autorizacion de salida

                    DateTime fecha = general != null ? general.FechaNacimineto : new DateTime();
                    TimeSpan time = DateTime.Today.Subtract(fecha);
                    var edad = "";
                    int auxEdad;
                    if (time.Days > 700000)
                    {
                        edad = "Sin Dato";
                        edad = general.Edaddetenido.ToString();
                    }

                    else
                    {
                        auxEdad = Convert.ToInt32(time.TotalDays) / 365;
                        edad = auxEdad.ToString();
                    }
                    edad = estatusInterno.DetalledetencionEdad.ToString();
                    List<Entity.Unidad> unidades = new List<Entity.Unidad>();

                    unidades = ControlUnidad.ObtenerPorEventoId(infoDetencion.IdEvento);
                    string unidadesPlaca = "";
                    string responsableunidaddescripcion = "";
                    string responsableunidadnombre = "";

                    if (unidades.Count > 0)
                    {
                        unidadesPlaca = string.Join(", ", unidades.Select(x => x.Nombre));
                        List<Entity.Catalogo> responsables = new List<Entity.Catalogo>();
                        foreach (var item in unidades)
                        {
                            var responsableunidad = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { item.Id, infoDetencion.IdEvento });
                            responsables.Add(responsableunidad);
                            responsableunidadnombre += (responsableunidad.Nombre ?? "") + ", ";
                        }
                        responsableunidaddescripcion = string.Join(", ", responsables.Select(x => x.Descripcion));
                        responsableunidadnombre = string.Join(", ", responsables.Select(x => x.Nombre));
                    }

                    if (usuario.RolId == 13)
                    {
                        usuario = ControlUsuario.Obtener(8);
                    }

                    var registro = "";
                    if(estatusInterno !=null)
                    {
                        var f = Convert.ToDateTime(estatusInterno.Fecha);
                        var h = f.Hour.ToString();
                        if (f.Hour < 10) h = "0" + h;
                        var m = f.Minute.ToString();
                        if (f.Minute < 10) m = "0" + m;
                        var s = f.Second.ToString();
                        if (f.Second < 10) s = "0" + s;
                        registro = f.ToShortDateString() + " " + h + ":" + m + ":" + s;
                    }
                    var fecha1 = "";
                    var f1 = DateTime.Now;
                    var h1 = f1.Hour.ToString();
                    if (f1.Hour < 10) h1 = "0" + h1;
                    var m1 = f1.Minute.ToString();
                    if (f1.Minute < 10) m1 = "0" + m1;
                    var s1 = f1.Second.ToString();
                    if (f1.Second < 10) s1 = "0" + s1;
                    fecha1 = f1.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;
                    var historialcalif = ControlHistorial.ObtenerPorDetenido(estatusInterno.Id).Where(x => x.Movimiento.Contains("calif")).FirstOrDefault();
                    var usuario2 = ControlUsuario.Obtener(historialcalif.CreadoPor);
                    object[] data = new object[]
                    {
                        (centroReclusion != null) ? centroReclusion.Nombre : "",
                        (usuario != null) ? usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno : "",
                        (estatusInterno != null) ? estatusInterno.Expediente : "",
                        registro,
                        (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                        edad,
                        sexo != null ? sexo.Nombre : "",
                        estadoCivil != null ? estadoCivil.Nombre : "",
                        escolaridad != null ? escolaridad.Nombre : "",
                        nacionalidad != null ? nacionalidad.Nombre : "",
                        "",
                        domicilio != null ? domicilio.Calle + " " + domicilio.Numero : "",
                        colonia != null ? colonia.Asentamiento : "",
                        municipio != null ? municipio.Nombre : "",
                        estado != null ? estado.Nombre : "",
                        domicilio != null ? domicilio.Telefono : "",
                        unidadesPlaca,
                        responsableunidaddescripcion,
                        responsableunidadnombre,
                        (infoDetencion != null) ? infoDetencion.Id.ToString() : "",
                        motivoE,
                        (infoDetencion != null) ? infoDetencion.LugarDetencion : "",
                        (coloniaDetencion != null) ? coloniaDetencion.Asentamiento : "",
                        fecha1,
                        usuario2.Nombre + " " + usuario2.ApellidoPaterno + " " + usuario2.ApellidoMaterno,
                        situacion != null ? situacion.Nombre : "",
                        calificacion != null ? calificacion.SoloArresto ? "Si" : "No" : "",
                        calificacion != null ? calificacion.TotalHoras : 0,
                        calificacion != null ? calificacion.TotalAPagar : 0,
                        calificacion != null ? calificacion.Fundamento : "",
                        calificacion != null ? calificacion.Razon : "",
                        i.ToString(),
                        fundamento,
                        tipoDeSalida,
                        motivoDescripcion
                    };

                    object[] dataArchivo = ControlPDF.generarAutorizacionSalida(data);




                    //Guardar los archivos generados
                    if (dataArchivo != null)
                    {
                        arrayList.Add(dataArchivo[1]);
                    }
                    Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                    reportesLog.ProcesoId = Convert.ToInt32(interno.Id);
                    reportesLog.ProcesoTrackingId = interno.TrackingId.ToString();
                    reportesLog.Modulo = "Juez calificador";
                    reportesLog.Reporte = "Autorización de salida";
                    reportesLog.Fechayhora = DateTime.Now;
                    reportesLog.EstatusId = 1;
                    reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    ControlReportesLog.Guardar(reportesLog);
                    if (idmovimiento > 0)
                    {
                        var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                        if (Convert.ToInt32(tiposalidaid) != tipo.Id)
                        {
                            estatusInterno.Estatus = 2;
                            estatusInterno.Activo = false;
                            ControlDetalleDetencion.Actualizar(estatusInterno);
                        }
                        else
                        {
                            var Amparo = new Entity.DetenidoAmparo();
                            Amparo.TrackingId = Guid.NewGuid();
                            Amparo.Fecha = DateTime.Now;
                            Amparo.Activo = true;
                            Amparo.DetalleDetencionId = calificacion.InternoId;
                            Amparo.Id = ControlDetenidoAmparo.Guardar(Amparo);
                        }

                    }
                }

                foreach (var item in listaErrores)
                {
                    errores += item;
                }
                if (!string.IsNullOrEmpty(errores))
                {
                    
                }
                return JsonConvert.SerializeObject(new { exitoso = true, archivos = arrayList, mensaje = "La salida efectuada se generó exitosamente", errores = errores });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string filtrarTabla(object[] dataArreglo)
        {
            try
            {
                var opcion = dataArreglo[0].ToString();
                if (opcion == "0")
                {
                    
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        public static string ValidaReglaDeNegocioSalidaEfectuadaBiometricos(int DetenidoId, int ContratoId)
        {

            var biometricos = ControlBiometricos.GetByDetenidoId(DetenidoId);

            if (biometricos.Count > 0)
            {
                return "OK";
            }
            var parametros = ControlParametroContrato.TraerTodos();
            Entity.ParametroContrato parametro = new Entity.ParametroContrato();

            foreach (var item in parametros.Where(x=> x.ContratoId==ContratoId && x.Parametroid==Convert.ToInt32(Entity.ParametroEnum.SALIDA_DEL_DETENIDO_SIN_BIOMETRICO)))
            {
                parametro = item;
            }

            if (parametro == null)
            {
                return "OK";
            }
            if ((parametro.Valor!=null ? parametro.Valor:"0") == "1")
            {
                return "no tiene biométricos registrados";
            }
            else
            {

                return "OK";
            }
        }

        public static string ValidaReglaDenegocioExamenmedico(int DetalleDetencionId,int ContratoId)
        {

            var examenMedico = ControlServicoMedico.ObtenerTodo();
            List<Entity.ServicioMedico> serviciomedico = new List<Entity.ServicioMedico>();
            foreach (var item in examenMedico.Where(x=>x.DetalledetencionId==DetalleDetencionId))
            {
                serviciomedico.Add(item);
            }
            var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(DetalleDetencionId);
            if (serviciomedico .Count>0)
            {
                return "OK";

            }
            if (examenmedico.Count > 0)
            {
                return "OK";

            }
            var parametros = ControlParametroContrato.TraerTodos();
            if (parametros.Count > 0)
            {
                Entity.ParametroContrato parametroContrato = new Entity.ParametroContrato();

                foreach (var item in parametros.Where(x => x.Parametroid == Convert.ToInt32(Entity.ParametroEnum.TRASLADO_O_SALIDA_SIN_EXAMEN_MÉDICO) && x.ContratoId == ContratoId))
                {
                    parametroContrato = item;
                }

                if (parametroContrato.Valor == "1")
                {
                    Entity.ParametroContrato param = new Entity.ParametroContrato();

                    param = parametros.LastOrDefault(x => x.Parametroid == Convert.ToInt32(Entity.ParametroEnum.ADMINISTRAR_EXAMEN_MEDICO) && x.ContratoId == ContratoId);

                    if (param == null)
                    {
                        return "no tiene un examen médico asignado";

                    }
                    if (param.Valor == "1")
                    {
                        var administrarexamenmedico = ControlAdministracionExamenMedico.obtenerPorDetalledetencion(DetalleDetencionId);

                        if (!string.IsNullOrEmpty(administrarexamenmedico != null ? administrarexamenmedico.Justificacion : ""))
                        {
                            return "OK";
                        }
                    }

                    return "no tiene un examen médico asignado";

                }
                else
                {
                    return "OK";

                }
            }
            else { return "no tiene un examen médico asignado"; }

        }
        [WebMethod]
        public static string validarExamenmedico2(string trackingid)
        {
            try
            {
                string mensaje = string.Empty;
                int accion = 0;
                int detenidoId = 0;
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);

                if (detalleDetencion == null)
                {
                    detalleDetencion = ControlDetalleDetencion.ObtenerPorTrackingId(Guid.Parse(trackingid));
                }
                //var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);
                var examenmedico = ControlServicoMedico.ObtenerPorDetalleDetencionId(detalleDetencion.Id);

                //rep4ir
                if (examenmedico != null && examenmedico.Count >= 1)
                {
                    if (examenmedico.Count > 1)
                    {
                        accion = 2;
                        detenidoId = detalleDetencion.DetenidoId;
                    }
                    else
                    {
                        accion = 1;
                        detenidoId = detalleDetencion.DetenidoId;
                    }
                }
                else
                {
                    accion = 0;
                    mensaje = "No se ha registrado un examen médico para el detenido";
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mensaje, accion = accion, detenidoId = detenidoId });

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static string validarExamenmedico(string trackingid)
        {
            try
            {
                string mensaje = string.Empty;
                int accion = 0;
                int detenidoId = 0;
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);

                var examenmedico = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);

                if (examenmedico != null && examenmedico.Count > 0)
                {
                    if (examenmedico.Count > 1)
                    {
                        accion = 2;
                        detenidoId = interno.Id;
                    }
                    else
                    {
                        accion = 1;
                        detenidoId = interno.Id;
                    }
                }
                else
                {
                    accion = 0;
                    mensaje = "No se ha registrado un examen médico para el detenido";
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mensaje, accion = accion, detenidoId = detenidoId });

            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static string pdf2(int detenidoId, int examenId)
        {
            var serializedObject = string.Empty;
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    var detenido = ControlDetalleDetencion.ObtenerPorId(detenidoId);
                    var interno = ControlDetenido.ObtenerPorId(detenidoId);
                    if (interno==null)
                    {
                        interno = ControlDetenido.ObtenerPorId(detenido.DetenidoId);
                    }
                    var obj = ControlPDFEM.ReporteEM(interno.Id, interno.TrackingId, examenId);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static Object pdf(int detenidoId, int examenId)
        {
            var serializedObject = string.Empty;
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");
                    Entity.ServicioMedico servicioMedico = new Entity.ServicioMedico();
                    if (detenidoId == 1)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoPsicofisiologico(servicioMedico);
                        var det_detecnion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var detenido_ = ControlDetenido.ObtenerPorId(det_detecnion.DetenidoId);

                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = Convert.ToInt32(detenido_.Id);
                        reportesLog.ProcesoTrackingId = detenido_.TrackingId.ToString();
                        reportesLog.Modulo = "Juez calificador";
                        reportesLog.Reporte = "Certificado psicofisiológico";
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.EstatusId = 2;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);

                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId,  Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }
                    if (detenidoId == 2)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoLesion (servicioMedico);

                        var det_detecnion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var detenido_ = ControlDetenido.ObtenerPorId(det_detecnion.DetenidoId);

                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = Convert.ToInt32(detenido_.Id);
                        reportesLog.ProcesoTrackingId = detenido_.TrackingId.ToString();
                        reportesLog.Modulo = "Juez calificador";
                        reportesLog.Reporte = "Certificado de lesiones";
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.EstatusId = 2;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);

                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }
                    if (detenidoId == 3)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        Entity.ImprimeResultadoCertificadoQuimico resultadoCertificadoQuimico = new Entity.ImprimeResultadoCertificadoQuimico();
                        resultadoCertificadoQuimico.PrintEtanol = true;
                        resultadoCertificadoQuimico.PrintBenzodiazepina = true;
                        resultadoCertificadoQuimico.PrintAnfetamina = true;
                        resultadoCertificadoQuimico.PrintCannabis = true;
                        resultadoCertificadoQuimico.PrintCocaina = true;
                        resultadoCertificadoQuimico.PrintExtasis = true;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoQuimico(servicioMedico, resultadoCertificadoQuimico);

                        var det_detecnion = ControlDetalleDetencion.ObtenerPorId(servicioMedico.DetalledetencionId);
                        var detenido_ = ControlDetenido.ObtenerPorId(det_detecnion.DetenidoId);

                        Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                        reportesLog.ProcesoId = Convert.ToInt32(detenido_.Id);
                        reportesLog.ProcesoTrackingId = detenido_.TrackingId.ToString();
                        reportesLog.Modulo = "Juez calificador";
                        reportesLog.Reporte = "Certificado químico";
                        reportesLog.Fechayhora = DateTime.Now;
                        reportesLog.EstatusId = 2;
                        reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                        ControlReportesLog.Guardar(reportesLog);

                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }

                    var detenido = ControlDetenido.ObtenerPorId(detenidoId);

                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                    //var obj = ControlPDFEM.ReporteEM(detenido.Id, detenido.TrackingId, examenId);
                    //serializedObject = JsonConvert.SerializeObject(obj);
                    //return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
          
        }

        [WebMethod]
        public static DataTable getExamenMedicos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string Id)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(Id));
                        string[] data = new string[]
                        {
                            interno.Id.ToString(),
                            "true"
                        };
                        var detalle_detencion = interno != null ? ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(data) : null;

                        if (detalle_detencion != null)
                        {
                            where.Add(new Where("E.DetalleDetencionId", detalle_detencion.Id.ToString()));
                            where.Add(new Where("E.Activo", "1"));
                        }

                        Query query = new Query
                        {
                            select = new List<string> {
                                "E.DetalledetencionId Id",
                                "E.Id ExamenMedicoId",
                                "'' Tipo",
                                "date_format(E.FechaRegistro, '%Y-%m-%d %H:%i:%S') Fecha",
                                "U.Usuario ElaboradoPor",
                                "ifnull(E.CertificadoQuimicoId,0) CertificadoQuimicoId",
                                "ifnull(E.CertificadoLesionId,0) CertificadoLesionId",
                                "ifnull(E.CertificadoMedicoPsicofisiologicoId,0) CertificadoMedicoPsicofisiologicoId"
                            },
                            from = new Table("servicio_medico", "E"),
                            joins = new List<Join>
                            {
                                new Join(new Table("vusuarios","V"),"E.RegistradoPor=V.Id ","LEFT OUTER"),
                                new Join(new Table("usuario", "U"), "V.usuarioId = U.Id","LEFT")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var result1 = dt.Generar(query, draw, start, length, search, order, columns);

                        return result1;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }
        [WebMethod]
        public static DataTable getExamenMedicos2(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string Id)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        var interno = ControlDetenido.ObtenerPorId(Convert.ToInt32(Id));
                        string[] data = new string[]
                        {
                            interno.Id.ToString(),
                            "true"
                        };
                        var detalle_detencion = interno != null ? ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(data) : null;

                        if (detalle_detencion != null)
                        {
                            where.Add(new Where("E.DetalleDetencionId", detalle_detencion.Id.ToString()));
                            where.Add(new Where("E.Activo", "1"));
                        }

                        Query query = new Query
                        {
                            select = new List<string> {
                                "E.DetalleDetencionId Id",
                                "E.Id ExamenMedicoId",
                                "T.Nombre Tipo",
                                "date_format(E.HoraYFecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "U.Usuario ElaboradoPor"
                            },
                            from = new Table("examen_medico", "E"),
                            joins = new List<Join>
                            {
                                new Join(new Table("tipo_examen", "T"), "E.TipoExamenId = T.Id","LEFT"),
                                new Join(new Table("usuario", "U"), "E.CreadoPor = U.Id","LEFT")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var result1 = dt.Generar(query, draw, start, length, search, order, columns);

                        return result1;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }
        [WebMethod]
        public static string getEvento(string detenidoId)
        {
            List<object> lista = new List<object>();
            Entity.Evento evento = new Entity.Evento();
            Entity.Detenido detenido = new Entity.Detenido();
            Entity.InformacionDeDetencion informacionDetencion = new Entity.InformacionDeDetencion();
            Entity.Colonia colonia = new Entity.Colonia();

            detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(detenidoId));

            if (detenido != null)
            {
                informacionDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);

            }

            if (informacionDetencion != null)
            {
                evento = ControlEvento.ObtenerById(informacionDetencion.IdEvento);
            }

            if (evento != null)
            {
                string localizacion = string.Empty;

                colonia = ControlColonia.ObtenerPorId(evento.ColoniaId);

                localizacion = evento.Lugar;

                if (colonia != null)
                {
                    localizacion += " " + colonia.Municipio + " " + colonia.Estado + " C.P. " + colonia.CodigoPostal;
                }

                object obj = new
                {
                    Descripcion = evento.Descripcion != "" ? evento.Descripcion : "Sin descripción",
                    FolioEvento = evento.Folio,
                    FechaHora = evento.HoraYFecha,
                    Localizacion = localizacion != "" ? localizacion : "Sin lugar de detención"
                };

                lista.Add(obj);
            }

            return JsonConvert.SerializeObject(new { lista = lista });
        }

        [WebMethod]
        public static string getLugarEvento(string detenidoId)
        {
            List<object> lista = new List<object>();
            Entity.Evento evento = new Entity.Evento();
            Entity.Detenido detenido = new Entity.Detenido();
            Entity.InformacionDeDetencion informacionDetencion = new Entity.InformacionDeDetencion();
            Entity.Colonia colonia = new Entity.Colonia();

            detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(detenidoId));

            if (detenido != null)
            {
                informacionDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);

            }

            if (informacionDetencion != null)
            {
                evento = ControlEvento.ObtenerById(informacionDetencion.IdEvento);
            }

            if (evento != null)
            {
                object obj = new
                {
                    NumeroDetenidos = evento.NumeroDetenidos != 0 ? evento.NumeroDetenidos.ToString() : "Sin número de detenidos",
                    Latitud = evento.Latitud,
                    Longitud = evento.Longitud,
                };

                lista.Add(obj);
            }

            return JsonConvert.SerializeObject(new { lista = lista });
        }

        [WebMethod]
        public static string getEventoAW(string detenidoId)
        {
            List<object> lista = new List<object>();
            Entity.Evento evento = new Entity.Evento();
            Entity.Detenido detenido = new Entity.Detenido();
            Entity.InformacionDeDetencion informacionDetencion = new Entity.InformacionDeDetencion();
            Entity.WSAEvento eventoAW = new Entity.WSAEvento();
            Entity.WSALugar lugarEventoAW = new Entity.WSALugar();
            Entity.WSAColonia coloniaAW = new Entity.WSAColonia();
            string localizacion = string.Empty;

            detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(detenidoId));

            if (detenido != null)
            {
                informacionDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);

            }

            if (informacionDetencion != null)
            {
                evento = ControlEvento.ObtenerById(informacionDetencion.IdEvento);
            }

            if (evento != null)
            {
                eventoAW = ControlWSAEvento.ObtenerPorId(evento.IdEventoWS);

                if (eventoAW != null)
                {
                    lugarEventoAW = ControlWSALugar.ObtenerPorId(eventoAW.IdLugar);

                    if (lugarEventoAW != null)
                    {
                        coloniaAW = ControlWSAColonia.ObtenerPorId(lugarEventoAW.IdColonia);

                        if (coloniaAW != null)
                        {
                            Entity.WSAMunicipio municipioWSA = new Entity.WSAMunicipio();
                            Entity.WSAEstado estadoWSA = new Entity.WSAEstado();

                            municipioWSA = ControlWSAMunicipio.ObtenerPorId(coloniaAW.IdMunicipio);
                            estadoWSA = ControlWSAEstado.ObtenerPorId(lugarEventoAW.IdEstado);

                            localizacion = coloniaAW.NombreColonia + " " + lugarEventoAW.Numero + " " + municipioWSA.NombreMunicipio + " " + estadoWSA.NombreEstado;
                        }
                    }


                }

                object obj = new
                {
                    FolioEvento = eventoAW != null ? eventoAW.Folio : "",
                    Descripcion = eventoAW != null ? eventoAW.Descripcion : "",
                    Fecha = eventoAW != null ? eventoAW.FechaEvento.ToShortDateString() : "",
                    Lugar = localizacion != "" ? localizacion : "",
                    NombreResponsable = eventoAW != null ? eventoAW.NombreResponsable : "",
                    NumeroDetenidos = eventoAW != null ? eventoAW.NumeroDetenidos.ToString() : ""
                };

                lista.Add(obj);
            }

            return JsonConvert.SerializeObject(new { lista = lista });
        }

        [WebMethod]
        public static string getLugarEventoAW(string detenidoId)
        {
            List<object> lista = new List<object>();
            Entity.Evento evento = new Entity.Evento();
            Entity.Detenido detenido = new Entity.Detenido();
            Entity.InformacionDeDetencion informacionDetencion = new Entity.InformacionDeDetencion();
            Entity.WSAEvento eventoAW = new Entity.WSAEvento();
            Entity.WSALugar lugarEventoAW = new Entity.WSALugar();
            Entity.WSAColonia coloniaAW = new Entity.WSAColonia();
            string entreCalle = string.Empty;

            detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(detenidoId));

            if (detenido != null)
            {
                informacionDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detenido.Id);

            }

            if (informacionDetencion != null)
            {
                evento = ControlEvento.ObtenerById(informacionDetencion.IdEvento);
            }

            if (evento != null)
            {
                eventoAW = ControlWSAEvento.ObtenerPorId(evento.IdEventoWS);

                if (eventoAW != null)
                {
                    lugarEventoAW = ControlWSALugar.ObtenerPorId(eventoAW.IdLugar);

                    if (lugarEventoAW != null)
                    {
                        entreCalle = lugarEventoAW.EntreCalle != null ? lugarEventoAW.EntreCalle + lugarEventoAW.Ycalle != null ? " y " + lugarEventoAW.Ycalle : "" : "";
                    }
                }

                object obj = new
                {
                    Sector = lugarEventoAW.Sector != null ? lugarEventoAW.Sector : "",
                    EntreCalle = entreCalle,
                    Latitud = lugarEventoAW.Latitud != null ? lugarEventoAW.Latitud : "",
                    Longitud = lugarEventoAW.Longitud != null ? lugarEventoAW.Longitud : ""
                };
                lista.Add(obj);
            }

            return JsonConvert.SerializeObject(new { lista = lista });
        }

        [WebMethod]
        public static List<Combo> LoadDetenidosAniosTrabajo()
        {
            List<Combo> combo = new List<Combo>();
            var listaanios = ControlDashBoardAnioTrabajo.GetAniosTrabajo();

            List<int> Anios = new List<int>();
            foreach (var item in listaanios)
            {
                Anios.Add(item.Aniotrabajo);
            }

            if (Anios.Count == 0 || DateTime.Now.Year > Anios.Max())
            {
                Entity.DasboardAnioTrabajo dasboardAnioTrabajo = new Entity.DasboardAnioTrabajo();
                dasboardAnioTrabajo.Aniotrabajo = DateTime.Now.Year;
                listaanios.Add(dasboardAnioTrabajo);
            }
            foreach (var item in listaanios.OrderByDescending(x => x.Aniotrabajo))
            {
                combo.Add(new Combo { Desc = item.Aniotrabajo.ToString(), Id = item.Aniotrabajo.ToString() });
            }

            return combo;
        }

        public class Traslado
        {
            public string InternoId { get; set; }
            public string TrackingId { get; set; }
            public string DelegacionId { get; set; }
            public string TiposalidaId { get; set; }
            public string Fundamento { get; set; }
            public int Horas { get; set; }

        }

        public class PertenenciaData
        {
            public string Pertenencia { get; set; }
            public string Observacion { get; set; }
            public string Bolsa { get; set; }
            public string Cantidad { get; set; }
            public string Clasificacion { get; set; }
            public string Casillero { get; set; }
            public string Id { get; set; }
            public string TrackingId { get; set; }
            public string Fotografia { get; set; }
        }
    }
}