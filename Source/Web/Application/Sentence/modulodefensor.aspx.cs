﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Table = DT.Table;

namespace Web.Application.Sentence
{
    public partial class modulodefensor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }

        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {

                //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                //{
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencionDetenido = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                try
                {
                    infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);


                }
                catch (Exception ex)
                {

                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
                }
                var colonia = (infoDetencion != null) ? ControlColonia.ObtenerPorId(infoDetencion.ColoniaId) : null;
                var municipio = (infoDetencion != null) ? ControlMunicipio.Obtener(colonia != null ? colonia.IdMunicipio : 0) : null;
                var estado = (infoDetencion != null) ? ControlEstado.Obtener(municipio != null ? municipio.EstadoId : 0) : null;
                var pais = (infoDetencion != null) ? ControlCatalogo.Obtener(estado != null ? estado.IdPais : 0, 45) : null;
                var evento = (infoDetencion != null) ? ControlEvento.ObtenerById(infoDetencion.IdEvento) : null;
                var antropometria = (infoDetencion != null) ? ControlAntropometria.ObtenerPorDetenidoId(interno.Id) : null;

                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);

                Entity.Domicilio nacimiento = ControlDomicilio.ObtenerPorId(interno.DomicilioNId);
                Entity.Catalogo sexo = new Entity.Catalogo();
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                int edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);

                    sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                }

                var llamada = ControlLlamada.ObtenerById(evento.LlamadaId);
                var unidad = ControlCatalogo.Obtener(infoDetencion.UnidadId, 82);
                var responsable = ControlCatalogo.Obtener(infoDetencion.ResponsableId, 83);

                var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                Entity.Municipio municipio2 = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                Entity.Colonia colonia2 = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;
                object obj = null;

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = evento.HoraYFecha;
                var motivo = "";
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);
                if(detenidoevento!=null)
                {
                    var m = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                    if (m != null)
                    {
                        motivo = m.NombreMotivoLlamada;
                    }
                }
                
                if (calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                }
                else
                {
                    //fechaSalidaAdd = fechaSalida.AddHours(36);
                    fechaSalidaAdd = DateTime.MinValue;
                }
                if (interno != null)
                {
                    if (string.IsNullOrEmpty(interno.RutaImagen))
                    {
                        interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                    }
                }

                obj = new
                {
                    Id = interno != null ? interno.Id.ToString() : "",
                    TrackingId = interno != null ? interno.TrackingId.ToString() : "",
                    Expediente = (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                    Centro = institucion != null ? institucion.Nombre : "Centro no registrado",
                    Fecha = detalleDetencion != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd-MM-yyyy HH:mm:ss") : "",
                    Nombre = (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                    RutaImagen = (interno != null) ? interno.RutaImagen != null ? interno.RutaImagen : "" : "",
                    UnidadId = infoDetencion != null ? infoDetencion.UnidadId : 0,
                    ResponsableId = infoDetencion != null ? infoDetencion.ResponsableId : 0,
                    Motivo = motivo,
                    Descripcion = (infoDetencion != null) ? infoDetencion.Descripcion : "",
                    Folio = (infoDetencion != null) ? infoDetencion.Folio : "",
                    Lugar = (infoDetencion != null) ? infoDetencion.LugarDetencion : "",
                    FechaDetencion = (detalleDetencion != null) ? ((DateTime)detalleDetencion.Fecha).ToString("dd-MM-yyyy HH:mm:ss") : "",
                    PaisId = (pais != null) ? pais.Id : 0,
                    EstadoId = (estado != null) ? estado.Id : 0,
                    MunicipioId = (municipio != null) ? municipio.Id : 0,
                    ColoniaId = (colonia != null) ? colonia.Id : 0,
                    CodigoPostal = (colonia != null) ? colonia.CodigoPostal : "",
                    IdInfo = (infoDetencion != null) ? infoDetencion.Id.ToString() : "",
                    TrackingInfo = (infoDetencion != null) ? infoDetencion.TrackingId.ToString() : "",
                    LlamadaId = (evento != null) ? evento.LlamadaId : 0,
                    EventoId = (evento != null) ? evento.Id : 0,
                    Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                    Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                    municipioNombre = municipio2 != null ? municipio2.Nombre : "Municipio no registrado",
                    domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                    coloniaNombre = colonia2 != null ? colonia2.Asentamiento : "Colonia no registrada",
                    LlamadaNombre = llamada != null ? llamada.Descripcion : "",
                    EventoNombre = (evento != null) ? evento.Descripcion : "",
                    UnidadNombre = (unidad != null) ? unidad.Nombre : "",
                    ResponsableNombre = (responsable != null) ? responsable.Nombre : "",
                    PaisNombre = (pais != null) ? pais.Nombre : "",
                    EstadoNombre = (estado != null) ? estado.Nombre : "",
                    MunicipioNombre = (municipio != null) ? municipio.Nombre : "",
                    ColoniaNombre = (colonia != null) ? colonia.Asentamiento : "",
                    Estatura = (antropometria != null) ? antropometria.Estatura.ToString() : "",
                    Peso = (antropometria != null) ? antropometria.Peso.ToString() : "",
                    FechaSalida = fechaSalidaAdd != DateTime.MinValue ? fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    Personanotifica = infoDetencion.Personanotifica != null ? infoDetencion.Personanotifica : "",
                    Celular = infoDetencion.Celular != null ? infoDetencion.Celular : "",

                };
                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                //}
                //else
                //{
                //    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                //}
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Defensor público" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static DataTable GetObservacionesLog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            using (MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Defensor público" }).Consultar)
                    {
                        if (!emptytable)
                        {

                            List<Where> where = new List<Where>();
                            List<Order> orderby = new List<Order>();
                            if (Convert.ToInt32(centroid) != 0)
                            {
                                where.Add(new Where("A.Id", centroid.ToString()));
                            }
                            if (Convert.ToBoolean(todoscancelados))
                            {
                                where.Add(new Where("C.Activo", "0"));
                            }

                            Query query = new Query
                            {
                                select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.InternoId",
                                "replace(A.Observaciones,'\"','') Observaciones",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                                from = new Table("observacionLog", "A"),
                                joins = new List<Join>
                            {
                                new Join(new Table("vusuarios","V"),"A.Creadopor=V.Id ","LEFT OUTER"),
                                new Join(new Table("usuario","B"),"V.UsuarioId=B.Id ","LEFT OUTER"),
                                //new Join(new Table("Usuario","B"),"B.id=A.Creadopor","LEFT"),
                                new Join(new Table("observacion","C"),"C.Id=A.Id"),
                            },
                                wheres = where

                            };
                            DataTables dt = new DataTables(mysqlconection);
                            DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
                finally
                {
                    if (mysqlconection.State == System.Data.ConnectionState.Open)
                        mysqlconection.Close();
                }
            }
        }
        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
          
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Defensor público" }).Consultar)
            {
                using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
                {
                    try
                    {
                        if (!emptytable)
                        {

                            List<Where> where = new List<Where>();
                            List<Where> where2 = new List<Where>();
                            where.Add(new Where("E.Activo", "1"));
                            where.Add(new Where("E.Estatus", "<>", "2"));
                            var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                            int usuario;

                            if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                            else throw new Exception("No es posible obtener información del usuario en el sistema");

                            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                            where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("E.Tipo", contratoUsuario.Tipo));
                            var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                            where.Add(new Where("ifnull(Saj.TiposalidaId,0)", "<>", tipo.Id.ToString()));

                            Query query = new Query
                            {
                                select = new List<string> {
                                "distinct (I.Id) Id",
                                "I.TrackingId",
                                "I.Nombre",
                                "TRIM(I.Paterno) Paterno",
                                "TRIM(I.Materno) Materno",
                                "I.RutaImagen",
                                "CAST(ifnull(ExpedienteoOriginal,Expediente) AS unsigned) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus" ,
                                "ES.Nombre EstatusNombre",
                                "ifnull(T.internoId,0) ObservacionId",
                                "concat(I.Nombre,' ',Paterno,' ',Materno) as NombreCompleto"
                            },
                                from = new DT.Table("detenido", "I"),
                                joins = new List<Join>
                                {
                                    new Join(new DT.Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                                    new Join(new DT.Table("estatus", "ES"), "ES.id  = E.Estatus"),
                                    new Join(new DT.Table("observacion", "T"), "T.InternoId  = E.Id","LEFT"),
                                    new Join(new Table("(Select Id,ifnull(Expediente,0) as ExpedienteoOriginal,DetalleDetencionId from historico_agrupado_detenido)", "H"), "H.DetalleDetencionId=E.id"),
                                    new Join(new Table("salidaefectuadajuez", "Saj"), "E.id=Saj.DetalleDetencionId and Saj.Activo=1", "LEFT")
                                },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            var result1 = dt.Generar(query, draw, start, length, search, order, columns);
                            return result1;
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    catch (Exception ex)
                    {
                        return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                    }

                    finally
                    {
                        if (mysqlConnection.State == System.Data.ConnectionState.Open)
                            mysqlConnection.Close();
                    }
                
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

     

        [WebMethod]
        public static DataTable getDataHistorial(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string TrackingId)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Defensor público" }).Consultar)
            {
                using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
                {
                    try
                    {
                        if (!emptytable)
                        {


                            List<Where> where = new List<Where>();

                            var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(TrackingId));
                            string[] data = new string[]
                            {
                            interno.Id.ToString(),
                            "true"
                            };
                            var detalleDetencion = interno != null ? ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(data) : null;

                            where.Add(new Where("O.Activo", "1"));

                            if (detalleDetencion != null)
                            {
                                where.Add(new Where("O.InternoId", detalleDetencion.Id.ToString()));
                            }

                            var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                            int usuario;

                            if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                            else throw new Exception("No es posible obtener información del usuario en el sistema");


                            //Prueba de IN

                            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                            if ((!string.Equals(perfil, "Administrador")))
                            {
                                var user = ControlUsuario.Obtener(usuario);

                            }

                            Query query = new Query
                            {
                                select = new List<string> {
                                "O.Id",
                                "O.TrackingId",
                                "O.InternoId",
                                "O.Movimiento",
                                "date_format(O.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "O.Activo",
                                "O.Habilitado",
                                "U.Usuario"
                            },
                                from = new DT.Table("historial", "O"),
                                joins = new List<Join>
                            {
                                new Join(new Table("vusuarios","V"),"O.Creadopor=V.Id ","LEFT OUTER"),
                                new Join(new Table("usuario","U"),"V.UsuarioId=U.Id ","LEFT OUTER"),
                                //new Join(new DT.Table("usuario", "U"), "U.id  = O.CreadoPor")
                            },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            var result1 = dt.Generar(query, draw, start, length, search, order, columns);

                            return result1;
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    catch (Exception ex)
                    {
                        return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                    }

                    finally
                    {
                        if (mysqlConnection.State == System.Data.ConnectionState.Open)
                            mysqlConnection.Close();
                    }
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static DataTable getDataObservaciones(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string TrackingId)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Defensor público" }).Consultar)
            {
                using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
                {
                    try
                    {
                        if (!emptytable)
                        {

                            List<Where> where = new List<Where>();

                            var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(TrackingId));
                            string[] data = new string[]
                            {
                            interno.Id.ToString(),
                            "true"
                            };
                            var detalleDetencion = interno != null ? ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(data) : null;

                            where.Add(new Where("O.Activo", "1"));

                            if (detalleDetencion != null)
                            {
                                where.Add(new Where("O.InternoId", detalleDetencion.Id.ToString()));
                            }

                            var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                            int usuario;

                            if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                            else throw new Exception("No es posible obtener información del usuario en el sistema");



                            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                            if ((!string.Equals(perfil, "Administrador")))
                            {
                                var user = ControlUsuario.Obtener(usuario);

                            }
                            Query query = new Query
                            {
                                select = new List<string> {
                                "O.Id",
                                "O.TrackingId",
                                "O.InternoId",
                                "replace(O.Observaciones,'\"','') Observaciones",
                                "date_format(O.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "O.Activo",
                                "O.Habilitado",
                                "Usuario Usuario"
                            },
                                from = new DT.Table("observacion", "O"),
                                joins = new List<Join>
                            {
                                new Join(new Table("vusuarios","V"),"O.Creadopor=V.Id ","LEFT OUTER"),
                                new Join(new Table("usuario","U"),"V.UsuarioId=U.Id ","LEFT OUTER"),

                            },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            var result1 = dt.Generar(query, draw, start, length, search, order, columns);

                            return result1;
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    catch (Exception ex)
                    {
                        return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                    }

                    finally
                    {
                        if (mysqlConnection.State == System.Data.ConnectionState.Open)
                            mysqlConnection.Close();
                    }
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string blockObservacion(string trackingid)
        {
            var serializedObject = string.Empty;

            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Defensor público" }).Eliminar)
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var observacion = ControlObservacion.ObtenerPorTrackingId(new Guid(trackingid));
                    observacion.Activo = true;
                    observacion.Habilitado = (observacion.Habilitado) ? false : true;
                    ControlObservacion.Actualizar(observacion);

                    string message = "";
                    message = observacion.Habilitado ? "habilitado" : "deshabilitado";

                    return serializedObject = JsonConvert.SerializeObject(new { exitoso = true, message });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string saveObservacion(string[] datos)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                string mode = "";
                string id = "";
                id = datos[0].ToString();

                var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos[3].ToString()));
                string[] data = new string[]
                {
                    interno.Id.ToString(),
                    "true"
                };
                var detalleDetencion = interno != null ? ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(data) : null;
                Entity.Observacion observacion = new Entity.Observacion();
                observacion.Observaciones = datos[2].ToString();
                observacion.InternoId = detalleDetencion != null ? detalleDetencion.Id : 0;

                if (string.IsNullOrEmpty(id))
                {
                    mode = "registro";

                    observacion.Activo = true;
                    observacion.CreadoPor = usId;
                    observacion.Fecha = DateTime.Now;
                    observacion.Habilitado = true;
                    observacion.TrackingId = Guid.NewGuid();
                    observacion.Id = ControlObservacion.Guardar(observacion);
                    if(detalleDetencion !=null)
                    {
                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = detalleDetencion.Id;
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Movimiento = "Modificación de datos generales en observaciones";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);
                    }
                }
                else
                {
                    mode = "actualizó";
                    observacion.Activo = true;                                        
                    observacion.Habilitado = true;
                    observacion.TrackingId = new Guid(datos[1].ToString());
                    observacion.Id = Convert.ToInt32(datos[0]);
                    ControlObservacion.Actualizar(observacion);

                    if (detalleDetencion != null)
                    {
                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = detalleDetencion.Id;
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Movimiento = "Modificación de datos generales en observaciones";
                        historial.TrackingId = Guid.NewGuid();
                        historial.Id = ControlHistorial.Guardar(historial);
                    }

                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = ex.Message });
            }
        }
    }
}