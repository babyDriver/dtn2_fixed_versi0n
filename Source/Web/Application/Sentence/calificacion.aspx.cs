﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Sentence
{
    public partial class Calificacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Juez calificador" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static DataTable getCharges(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string TrackingId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                    {
                        if (!emptytable)
                        {
                            var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(TrackingId));
                            string[] dataInterno = new string[]
                            {
                            interno.Id.ToString(),
                            true.ToString()
                            };
                            var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(dataInterno);

                            List<Where> where = new List<Where>();
                            where.Add(new Where("E.internoId", detalleDetencion.Id.ToString()));
                            //where.Add(new Where("E.Habilitado", "1"));
                            Query query = new Query
                            {
                                select = new List<string>{
                            "C.Id",
                            "C.TrackingId",
                            "C.Motivo",
                            "C.Descripcion",
                            "C.Articulo",
                            "E.MultaSugerida",
                            "C.HorasdeArresto",
                            "C.Activo",
                            "E.Habilitado",
                            "E.Id IdMotInterno",
                            "E.TrackingId TrackingIdMotInterno",
                            "D.Descripcion Tipo_Motivo" ,
                            "E.MotivoDetencionId"
                        },
                                from = new DT.Table(" motivo_detencion_interno", "E"),
                                joins = new List<Join>
                            {
                                new Join(new DT.Table("MotivoDetencion", "C"), "C.Id  = E.MotivoDetencionId"),
                                new Join(new DT.Table("Tipo_Motivo_Detencion", "D"), "C.Tipo_Motivo = D.Id", "left")
                            },
                                wheres = where
                            };

                            DataTables dt = new DataTables(mysqlConnection);
                            return dt.Generar(query, draw, start, length, search, order, columns);
                        }
                        else
                        {
                            return DataTables.ObtenerDataTableVacia(null, draw);
                        }
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }

                finally
                {
                    if (mysqlConnection.State == System.Data.ConnectionState.Open)
                        mysqlConnection.Close();
                }

            }
        }

        [WebMethod]
        public static List<Combo> getSituations()
        {
            List<Combo> combo = new List<Combo>();
            
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            var situaciones = ControlCatalogo.ObtenerTodo(89);            

            if (situaciones.Count > 0)
            {
                foreach (var rol in situaciones)
                {
                    if(rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
                    }                    
                }                    
            }

            return combo;
        }

        [WebMethod]
        public static string blockitem(string id)
        {
            try
            {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                Entity.MotivoDetencionInterno motivo = ControlMotivoDetencionInterno.ObtenerByTrackingId(new Guid(id));
                motivo.Habilitado = motivo.Habilitado ? false : true;
                string mensaje = motivo.Habilitado ? "habilitó" : "deshabilitó";
                ControlMotivoDetencionInterno.Actualizar(motivo);

                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(motivo.InternoId);
                if (calificacion != null)
                {
                    Entity.MotivoDetencion motivoDet = ControlMotivoDetencion.ObtenerPorId(motivo.MotivoDetencionId);

                    if (!motivo.Habilitado)
                    {
                        calificacion.TotalHoras -= motivoDet.HorasdeArresto;

                        if (calificacion.TotalHoras < 0) calificacion.TotalHoras = 0;
                    }
                    else if (motivo.Habilitado)
                    {
                        calificacion.TotalHoras += motivoDet.HorasdeArresto;

                        if (motivoDet.Tipo_Motivo == 1 && calificacion.TotalHoras > 72) calificacion.TotalHoras = 72;
                        else if (motivoDet.Tipo_Motivo == 2 && calificacion.TotalHoras > 36) calificacion.TotalHoras = 36;
                        else if (motivoDet.Tipo_Motivo == 3 && calificacion.TotalHoras != 0) calificacion.TotalHoras = 0;
                    }
                    ControlCalificacion.Actualizar(calificacion);
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getInstitutions()
        {
            List<Combo> combo = new List<Combo>();

            var val = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
            string tipo = "";
            tipo = val != null ? val.Tipo : "";
            var contrato = new Entity.Contrato();
            var subcontrato = new Entity.Subcontrato();
            var institucion = new Entity.Institucion();

            if (tipo == "contrato")
            {
                contrato = ControlContrato.ObtenerPorId(val.IdContrato);
            }
            else if(tipo == "subcontrato")
            {
                subcontrato = ControlSubcontrato.ObtenerPorId(val.IdContrato);
            }

            if(tipo == "contrato")
            {
                institucion = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
            }
            else if(tipo == "subcontrato")
            {
                institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
            }

            
            if(institucion != null)
            {
                combo.Add(new Combo { Desc = institucion.Nombre, Id = institucion.Id.ToString() });
            }                                

            return combo;
        }

        [WebMethod]
        public static Object save(CalificacionAux datos, string descripcion)
        {
            try
            {
                datos.TotalAPagar = (Convert.ToDouble(datos.TotalDeMultas) + Convert.ToDouble(datos.Agravante)).ToString();
                double totalapagar = 0;
                totalapagar = Convert.ToDouble(datos.TotalAPagar);
                if (!string.IsNullOrEmpty(datos.Ajuste))
                {
                    datos.TotalAPagar = ((totalapagar) - (totalapagar * Convert.ToDouble(datos.Ajuste) / 100)).ToString();
                }
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Modificar)
                {
                    var calificacion = new Entity.Calificacion();
                    var internoAux = ControlDetenido.ObtenerPorTrackingId(new Guid(datos.TrackingIdInterno));
                    string[] dataInterno = new string[]
                    {
                        internoAux.Id.ToString(),
                        true.ToString()
                    };
                    DateTime FechaHist = new DateTime();
                    FechaHist = DateTime.Now;
                    var detalleDetencionAux = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(dataInterno);
                    
                    object[] dataArchivo = null;

                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = datos.Id;
                    if (id == "") id = "0";
                    var situacionAux = ControlCatalogo.Obtener(Convert.ToInt32(datos.SituacionId), 89);
                    calificacion.Activo = true;
                    calificacion.Agravante = datos.Agravante != "" ? Convert.ToDecimal(datos.Agravante) : 0;
                    calificacion.Ajuste = datos.Ajuste != "" ? Convert.ToDecimal(datos.Ajuste) : 0;
                    calificacion.CreadoPor = usId;
                    calificacion.Fundamento = datos.Fundamento;
                    calificacion.Habilitado = true;
                    calificacion.InstitucionId = Convert.ToInt32(datos.InstitucionId);
                    calificacion.InternoId = detalleDetencionAux.Id;
                    calificacion.Razon = datos.Razon;
                    calificacion.SituacionId = Convert.ToInt32(datos.SituacionId);
                    calificacion.SoloArresto = Convert.ToBoolean(datos.SoloArresto);
                    calificacion.TotalAPagar = datos.TotalAPagar != "" ? Convert.ToDecimal(datos.TotalAPagar) : 0;
                    if(calificacion.SoloArresto)
                    {
                        calificacion.TotalAPagar = 0;
                    }
                    calificacion.TotalDeMultas = datos.TotalDeMultas != "" ? Convert.ToDecimal(datos.TotalDeMultas) : 0;
                    //calificacion.MaxHoras = datos.MaxHoras;
                    calificacion.TotalHoras = datos.TotalHoras != "" ? Convert.ToInt32(datos.TotalHoras) : 0;
                    calificacion.TrabajoSocial = Convert.ToBoolean(datos.TrabajoSocial);

                    if (string.IsNullOrEmpty(datos.Id))
                    {
                        mode = @"registro";

                        calificacion.TrackingId = Guid.NewGuid();
                        calificacion.Id = ControlCalificacion.Guardar(calificacion);

                        var histDet = ControlHistorial.ObtenerPorDetenido(detalleDetencionAux.Id);

                        if (histDet.Count > 0)

                        {
                            var _hist = histDet.LastOrDefault(x => x.Movimiento == "Registro de la calificación");
                            if (_hist != null) FechaHist = _hist.Fecha;
                        }

                        if(!(calificacion.Id > 0)) throw new Exception("No se pudo registrar la calificación");
                        else
                        {
                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = calificacion.InternoId;
                            historial.Movimiento = "Registro de calificación";
                            historial.TrackingId = calificacion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);

                            Entity.HistorialCalificacion historialcalif0 = new Entity.HistorialCalificacion();
                            historialcalif0.Activo = true;
                            historialcalif0.CreadoPor = usId;
                            historialcalif0.Fecha = FechaHist;
                            historialcalif0.Habilitado = true;
                            historialcalif0.Descripcion = descripcion;
                            historialcalif0.InternoId = calificacion.InternoId;
                            historialcalif0.Movimiento = "Registro de calificación";
                            historialcalif0.TrackingId = calificacion.TrackingId;
                            historialcalif0.Id = ControlHistorialCalificacion.Guardar(historialcalif0);

                            if (!(historialcalif0.Id > 0))
                            {
                                throw new Exception("No se pudo registrar el historial");
                            }
                        }


                        if (calificacion.Id > 0)
                        {
                            if (calificacion.TrabajoSocial)
                            {
                                Entity.TrabajoSocial trabajoSocial = new Entity.TrabajoSocial();
                                trabajoSocial.Activo = 1;
                                trabajoSocial.Creadopor = usId;
                                trabajoSocial.Habilitado = 1;
                                trabajoSocial.DetalledetencionId = calificacion.InternoId;
                                //trabajoSocial.Programa = datos.Programa;
                                //trabajoSocial.PorcentajeHoras = datos.PorcentajeHoras;
                                trabajoSocial.FechaHora = DateTime.Now;
                                trabajoSocial.TrackingId = Guid.NewGuid().ToString();
                                int idTrabajo = ControlTrabajoSpcial.Guardar(trabajoSocial);
                                

                                if (!(idTrabajo > 0))
                                {
                                    throw new Exception("No se pudo registrar el trabajo social");
                                }
                                Entity.Historial historial = new Entity.Historial();
                                historial.Activo = true;
                                historial.CreadoPor = usId;
                                historial.Fecha = DateTime.Now;
                                historial.Habilitado = true;
                                historial.InternoId = calificacion.InternoId;
                                historial.Movimiento = "Registro de trabajo social en calificación";
                                historial.TrackingId = calificacion.TrackingId;
                                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                                historial.ContratoId = subcontrato.Id;
                                historial.Id = ControlHistorial.Guardar(historial);

                                if (!(historial.Id > 0))
                                {
                                    throw new Exception("No se pudo registrar el historial");
                                }
                            }

                            //Entity.HistorialCalificacion historialcalif = new Entity.HistorialCalificacion();
                            //historialcalif.Activo = true;
                            //historialcalif.CreadoPor = usId;
                            //historialcalif.Fecha = DateTime.Now;
                            //historialcalif.Habilitado = true;
                            //historialcalif.Descripcion = datos.Descripcion;
                            //historialcalif.InternoId = calificacion.InternoId;
                            //historialcalif.Movimiento = "Registro de trabajo social en calificación";
                            //historialcalif.TrackingId = calificacion.TrackingId;
                            //historialcalif.Id = ControlHistorialCalificacion.Guardar(historialcalif);

                            //if (!(historialcalif.Id > 0))
                            //{
                            //    throw new Exception("No se pudo registrar el historial");
                            //}
                        }
                    }
                    else
                    {
                        mode = @"actualizó";

                        calificacion.TrackingId = new Guid(datos.TrackingId);
                        calificacion.Id = Convert.ToInt32(datos.Id);                        

                        ControlCalificacion.Actualizar(calificacion);
                        var tsocial = ControlTrabajoSpcial.ObtenerByDetenidoId(calificacion.InternoId);

                        var histDet = ControlHistorial.ObtenerPorDetenido(detalleDetencionAux.Id);
                        Entity.Historial hist_ = new Entity.Historial();
                        var ff = DateTime.Now;
                        if (histDet.Count > 0)
                        {
                            hist_ = histDet.LastOrDefault(x=>x.Movimiento== "Modificación de la calificación");
                            if (hist_ != null) FechaHist = hist_.Fecha;
                        }
                        Entity.Historial historial = new Entity.Historial();
                        historial.Activo = true;
                        historial.CreadoPor = usId;
                        historial.Fecha = DateTime.Now;
                        historial.Habilitado = true;
                        historial.InternoId = calificacion.InternoId;
                        historial.Movimiento = "Actualización de calificación";
                        historial.TrackingId = calificacion.TrackingId;
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        historial.ContratoId = subcontrato.Id;
                        historial.Id = ControlHistorial.Guardar(historial);

                        Entity.HistorialCalificacion historialcalif0 = new Entity.HistorialCalificacion();
                        historialcalif0.Activo = true;
                        historialcalif0.CreadoPor = usId;
                        historialcalif0.Fecha = FechaHist;
                        historialcalif0.Habilitado = true;
                        historialcalif0.Descripcion = descripcion;
                        historialcalif0.InternoId = calificacion.InternoId;
                        historialcalif0.Movimiento = "Actualización de calificación";
                        historialcalif0.TrackingId = calificacion.TrackingId;
                        historialcalif0.Id = ControlHistorialCalificacion.Guardar(historialcalif0);

                        if (!(historialcalif0.Id > 0))
                        {
                            throw new Exception("No se pudo registrar el historial");
                        }


                        if (calificacion.Id > 0)
                        {
                            if (tsocial == null)
                            {
                                if (calificacion.TrabajoSocial)
                                {
                                    Entity.TrabajoSocial trabajoSocial = new Entity.TrabajoSocial();
                                    trabajoSocial.Activo = 1;
                                    trabajoSocial.Creadopor = usId;
                                    trabajoSocial.Habilitado = 1;
                                    trabajoSocial.DetalledetencionId = calificacion.InternoId;
                                    // sc0r# data
                                    //trabajoSocial.Programa = datos.Programa;
                                    //trabajoSocial.PorcentajeHoras = datos.PorcentajeHoras;
                                    trabajoSocial.FechaHora = DateTime.Now;
                                    trabajoSocial.TrackingId = Guid.NewGuid().ToString();
                                    int idTrabajo = ControlTrabajoSpcial.Guardar(trabajoSocial);

                                    if (!(idTrabajo > 0))
                                    {
                                        throw new Exception("No se pudo registrar el trabajo social");
                                    }
                                }
                            }

                            historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = calificacion.InternoId;
                            historial.Movimiento = "Modificación de trabajo social en calificación";
                            historial.TrackingId = calificacion.TrackingId;
                            contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);

                            if (!(historial.Id > 0))
                            {
                                throw new Exception("No se pudo registrar el historial");
                            }

                            //Entity.HistorialCalificacion historialcalif2 = new Entity.HistorialCalificacion();
                            //historialcalif2.Activo = true;
                            //historialcalif2.CreadoPor = usId;
                            //historialcalif2.Fecha = DateTime.Now;
                            //historialcalif2.Habilitado = true;
                            //historialcalif2.Descripcion = datos.Descripcion;
                            //historialcalif2.InternoId = calificacion.InternoId;
                            //historialcalif2.Movimiento = "Modificación de trabajo social en calificación";
                            //historialcalif2.TrackingId = calificacion.TrackingId;
                            //historialcalif2.Id = ControlHistorialCalificacion.Guardar(historialcalif2);

                            //if (!(historialcalif2.Id > 0))
                            //{
                            //    throw new Exception("No se pudo registrar el historial");
                            //}
                        }
                    }

                    if(situacionAux != null)
                    {
                        if (situacionAux.Nombre == "Libre por falta de elementos")
                        {
                            var detalleDetencion = ControlDetalleDetencion.ObtenerPorId(calificacion.InternoId);
                            var institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                            var usuario = ControlUsuario.Obtener(usId);
                            var interno = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
                            var generalInterno = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                            var sexo = ControlCatalogo.Obtener(generalInterno.SexoId, 4);
                            var estadoCivil = ControlCatalogo.Obtener(generalInterno.EstadoCivilId, 26);
                            var escolaridad = ControlCatalogo.Obtener(generalInterno.EscolaridadId, 15);
                            var nacionalidad = ControlCatalogo.Obtener(generalInterno.NacionalidadId, 28);
                            var domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                            var colonia = (domicilio != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;
                            var municipio = colonia != null ? ControlMunicipio.Obtener(colonia.IdMunicipio) : null;
                            var estado = municipio != null ? ControlEstado.Obtener(municipio.EstadoId) : null;
                            var situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;
                            var infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var unidad = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.UnidadId, 82) : null;
                            var responsable = (infoDetencion != null) ? ControlCatalogo.Obtener(infoDetencion.ResponsableId, 83) : null;
                            var coloniaDetencion = (infoDetencion != null) ? ControlColonia.ObtenerPorId(infoDetencion.ColoniaId) : null;                           

                            DateTime fecha = generalInterno != null ? generalInterno.FechaNacimineto : new DateTime();
                            TimeSpan time = DateTime.Today.Subtract(fecha);
                            var edad = "";
                            int auxEdad;
                            if (time.Days > 700000)
                                edad = "Sin Dato";
                            else
                            {
                                auxEdad = Convert.ToInt32(time.TotalDays) / 365;
                                edad = auxEdad.ToString();
                            }

                            object[] data = new object[]
                            {
                            (institucion != null) ? institucion.Nombre : "",
                            (usuario != null) ? usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno : "",
                            (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                            (detalleDetencion != null) ? detalleDetencion.Fecha.ToString() : "",
                            (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                            edad,
                            sexo != null ? sexo.Nombre : "",
                            estadoCivil != null ? estadoCivil.Nombre : "",
                            escolaridad != null ? escolaridad.Nombre : "",
                            nacionalidad != null ? nacionalidad.Nombre : "",
                            "",
                            domicilio != null ? domicilio.Calle + " " + domicilio.Numero : "",
                            colonia != null ? colonia.Asentamiento : "",
                            municipio != null ? municipio.Nombre : "",
                            estado != null ? estado.Nombre : "",
                            domicilio != null ? domicilio.Telefono : "",
                            (unidad != null) ? unidad.Id.ToString() : "",
                            (responsable != null) ? responsable.Id.ToString() : "",
                            (responsable != null) ? responsable.Nombre : "",
                            (infoDetencion != null) ? infoDetencion.Id.ToString() : "",
                            (infoDetencion != null) ? infoDetencion.Descripcion : "",
                            (infoDetencion != null) ? infoDetencion.LugarDetencion : "",
                            (coloniaDetencion != null) ? coloniaDetencion.Asentamiento : "",
                            DateTime.Today,
                            usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno,
                            situacion.Nombre,
                            calificacion.SoloArresto ? "Si" : "No",
                            calificacion.TotalHoras,
                            //calificacion.MaxHoras,
                            calificacion.TotalAPagar,
                            calificacion.Fundamento,
                            calificacion.Razon,
                            "0"
                            };

                            detalleDetencion.Estatus = 2;
                            Entity.SalidaEfectuadaJuez salidaEfectuada = new Entity.SalidaEfectuadaJuez();
                            salidaEfectuada.Creadopor = usId;
                            salidaEfectuada.DetalledetencionId = detalleDetencion.Id;
                            salidaEfectuada.Activo = 1;
                            salidaEfectuada.Habilitado = 1;
                            salidaEfectuada.Fundamento = "Libre por falta de elementos";
                            salidaEfectuada.TiposalidaId = 3;
                            salidaEfectuada.TrackingId = Guid.NewGuid().ToString();
                            ControlSalidaEfectuadaJuez.Guardar(salidaEfectuada);

                            Entity.Historial historial = new Entity.Historial();
                            historial.Activo = true;
                            historial.CreadoPor = usId;
                            historial.Fecha = DateTime.Now;
                            historial.Habilitado = true;
                            historial.InternoId = calificacion.InternoId;
                            historial.Movimiento = "Salida efectuada del detenido";
                            historial.TrackingId = calificacion.TrackingId;
                            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                            historial.ContratoId = subcontrato.Id;
                            historial.Id = ControlHistorial.Guardar(historial);

                           // ControlDetalleDetencion.Actualizar(detalleDetencion);

                            dataArchivo = ControlPDF.generarAutorizacionSalida(data);
                        }
                    }

                    int[] filtro = { detalleDetencionAux.ContratoId, detalleDetencionAux.DetenidoId };
                    var hit = ControlCrossreference.GetCrossReferences(filtro);
                    string msg = "";
                    var detenido = ControlDetenido.ObtenerPorId(detalleDetencionAux.DetenidoId);
                    bool fhit = false;
                    if (hit.Count > 0)
                    {
                        fhit = true;
                        msg = "La información se registro corectamente.   El detenido " + detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno + " tiene un hit";
                    }

                    return new {ishit=fhit,msghit=msg, exitoso = true,totalapagar=calificacion.TotalAPagar, mensaje = mode, Id = calificacion.Id.ToString(), TrackingId = calificacion.TrackingId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "" };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            return edad;
        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);                    
                    Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);

                    Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var evento2 = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                            var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento2.Id);
                            Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                            foreach (var item in detenidosEvento)
                            {
                                if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                    detenidoEvento = item;
                            }

                            if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                            else
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }

                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }
                    edad = detalleDetencion.DetalledetencionEdad;
                    var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                    Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                    var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                    Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                    object obj = null;

                    if (interno != null)
                    {
                        if (string.IsNullOrEmpty(interno.RutaImagen))
                        {
                            interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                        }
                    }

                    obj = new
                    {
                        Id = interno != null ? interno.Id.ToString() : "",
                        TrackingId = interno != null ? interno.TrackingId.ToString() : "",
                        Expediente = (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                        Centro = (institucion != null) ? institucion.Nombre : "",
                        Fecha = detalleDetencion != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Nombre = (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                        RutaImagen = (interno != null) ? interno.RutaImagen : "",
                        SituacionId = calificacion != null ? calificacion.SituacionId : 0,
                        InstitucionId = calificacion != null ? calificacion.InstitucionId : 0,
                        Fundamento = calificacion != null ? calificacion.Fundamento : "",
                        TrabajoSocial = calificacion != null ? calificacion.TrabajoSocial : false,
                        TotalHoras = calificacion != null ? calificacion.TotalHoras : 0,
                        //MaxHoras = calificacion != null ? calificacion.MaxHoras : "",
                        SoloArresto = calificacion != null ? calificacion.SoloArresto : false,
                        TotalDeMultas = calificacion != null ? calificacion.TotalDeMultas : 0,
                        Agravante = calificacion != null ? calificacion.Agravante : 0,
                        Ajuste = calificacion != null ? calificacion.Ajuste : 0,
                        TotalAPagar = calificacion != null ? calificacion.TotalAPagar : 0,
                        Razon = calificacion != null ? calificacion.Razon : "",
                        IdCalificacion = calificacion != null ? calificacion.Id.ToString() : "",
                        TrackingCalificacion = calificacion != null ? calificacion.TrackingId.ToString() : "",
                        Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                        Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                        Activo = calificacion != null ? calificacion.Activo : false,
                        NuevaCalificacion = calificacion == null ? false : true
                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string Gethoras(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                    Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);

                    object obj = null;
                    //Entity.MotivoDetencionInterno motdet = new Entity.MotivoDetencionInterno();
                    //motdet.InternoId = calificacion.InternoId;
                    //var listamotivos = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(motdet);
                    //foreach (var item in listamotivos)
                    //{
                    //    var motivo = ControlMotivoDetencion.ObtenerPorId(item.MotivoDetencionId);
                    //    int horasMotivo = motivo.HorasdeArresto;
                        
                    //}

                    //if (calificacion.TotalHoras > 72)
                    //{
                    //    calificacion.TotalHoras = 72;
                    //}
                    //else if(calificacion.TotalHoras < 0)
                    //{
                    //    calificacion.TotalHoras = 0;
                    //}

                    obj = new
                    {
                        NuevaCalificacion = calificacion == null ? false : true,
                        TotalHoras = calificacion != null ? calificacion.TotalHoras : 0
                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }


        [WebMethod]
        public static Object SaveMotivoInterno(MotivoDetencionAux motivoInterno)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Modificar)
                {
                    var con = new Entity.MotivoDetencionInterno();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = motivoInterno.Id.ToString();
                    if (id == "") id = "";

                    //object[] data = new object[] { contrato.Id, contrato.Motivo };
                    //var contrato_duplicado = ControlContrato.ObtenerPorClienteYContrato(data);
                    var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(motivoInterno.TrackingInternoId));
                    string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };

                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                    var multaSugerida = Convert.ToDecimal(motivoInterno.MultaSugerida != "" ? motivoInterno.MultaSugerida : "0");

                    Entity.MotivoDetencionInterno filtro = new Entity.MotivoDetencionInterno();
                    filtro.MotivoDetencionId = Convert.ToInt32(motivoInterno.MotivoDetencionId);
                    filtro.InternoId = Convert.ToInt32(detalleDetencion.Id);
                    filtro.MultaSugerida = multaSugerida;
                    Entity.MotivoDetencionInterno motvivodetinterno = ControlMotivoDetencionInterno.GetMotivoDetencionInterno(filtro);

                    var motivo = ControlMotivoDetencion.ObtenerPorId(Convert.ToInt32(motivoInterno.MotivoDetencionId));
                    

                    if (string.IsNullOrEmpty(id))
                    {
                        if (motvivodetinterno != null)
                        {
                            if (motvivodetinterno.InternoId != 0 && motvivodetinterno.MotivoDetencionId != 0)
                            {
                                return new { exitoso = false, mensaje = "El interno ya tiene asignado ese motivo de detención." };
                            }
                        }

                        mode = @"registró";

                        con.MotivoDetencionId = Convert.ToInt32(motivoInterno.MotivoDetencionId);
                        con.InternoId = detalleDetencion.Id;
                        con.MultaSugerida = multaSugerida;
                        con.TrackingId = Guid.NewGuid();
                        con.CreadoPor = usId;
                        con.Activo = true;
                        con.Habilitado = true;
                        con.Id = ControlMotivoDetencionInterno.Guardar(con);

                        if(!(con.Id > 0))
                        {                            
                            throw new Exception("No se pudo registrar el motivo");                            
                        }
                    }
                    else
                    {
                        mode = @"actualizó";

                        if (motvivodetinterno != null)
                        {
                            //if (motvivodetinterno.InternoId != 0 && motvivodetinterno.MotivoDetencionId != 0)
                            //{
                            //    return new { exitoso = false, mensaje = "El interno ya tiene asignado ese motivo de detención" };
                            //}
                        }

                        con.MotivoDetencionId = Convert.ToInt32(motivoInterno.MotivoDetencionId);
                        con.InternoId = detalleDetencion.Id;

                        con.MultaSugerida = multaSugerida;
                        
                        con.Id = Convert.ToInt32(motivoInterno.Id);
                        con.TrackingId = new Guid(motivoInterno.TrackingId);
                        con.Activo = true;
                        con.Habilitado = true;
                        ControlMotivoDetencionInterno.Actualizar(con);
                    }

                    return new { exitoso = true, mensaje = mode };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static Object GetMotivoDetencionById(string _id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    var contract = ControlMotivoDetencion.ObtenerPorId(Convert.ToInt32(_id));
                    ContratoEditAux obj = new ContratoEditAux();
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                    //List<Entity.ContratoUsuario> contratousuario = new List<Entity.ContratoUsuario>();
                    //Entity.ContratoUsuario contratousuario = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                    Entity.ContratoUsuario contratousuario = ControlContratoUsuario.ObtenerPorId(idContrato);
                    Entity.Subcontrato subcontrato = contratousuario != null ? ControlSubcontrato.ObtenerPorId(contratousuario.IdContrato): null;
                    //List<Entity.ContratoUsuario> ListaContratosusuarios = new List<Entity.ContratoUsuario>();
                    var divisa = ControlCatalogo.Obtener(subcontrato.DivisaId, Convert.ToInt32(Entity.TipoDeCatalogo.divisas));

                    //foreach (Entity.ContratoUsuario item in contratousuario)
                    //{
                    //    if (item.Activo)
                    //    {
                    //        ListaContratosusuarios.Add(item);
                    //    }

                    //}

                    //Entity.Subcontrato subcontrato = new Entity.Subcontrato();
                    //foreach (Entity.ContratoUsuario item in ListaContratosusuarios)
                    //{
                    //    subcontrato = ControlSubcontrato.ObtenerByContratoId(item.IdContrato)[0];

                    //}

                    if (contract != null)
                    {
                        var tipo = ControlTipoMotivoDetencion.ObtenerTipos().LastOrDefault(x => x.TipoId == contract.Tipo_Motivo);
                        obj.Id = contract.Id.ToString();
                        obj.Motivo = contract.Motivo.ToString();
                        obj.Descripcion = contract.Descripcion;
                        obj.Articulo = contract.Articulo.ToString();
                        obj.MultaMinimo = contract.MultaMinimo.ToString();
                        obj.MultaMaximo = contract.MultaMaximo.ToString();
                        obj.horaArresto = contract.HorasdeArresto.ToString();
                        
                        obj.SalarioMinimo = divisa.Nombre + ": $" + subcontrato.SalarioMinimo.ToString();
                        obj.ImporteMultaMinimo = Math.Round((contract.MultaMinimo * subcontrato.SalarioMinimo), 2).ToString();
                        obj.ImporteMultaMaximo = Math.Round((contract.MultaMaximo * subcontrato.SalarioMinimo), 2).ToString();
                        if (tipo != null)
                        {
                            obj.Tipo = tipo.Descripcion != null ? tipo.Descripcion : "";
                        }
                        else
                        {
                            obj.Tipo = "";
                        }
                    }
                    return obj;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static List<Combo> getListadoMotivos(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            List<Entity.MotivoDetencion> listado = ControlMotivoDetencion.ObtenerTodos();
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);


            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x => x.ContratoId == contratoUsuario.IdContrato && x.Tipo == contratoUsuario.Tipo && x.Activo == 1 && x.Habilitado == 1))
                    combo.Add(new Combo { Desc = rol.Motivo, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                Entity.Colonia colonia = domicilio != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;
                
                DateTime nacimiento;
                int edad = 0;
                if (general != null)
                {
                    nacimiento = general.FechaNacimineto;
                    edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = "Calle: " + domicilio.Calle + " #" + domicilio.Numero + "";

                }

                string strColonia = "";
                if (colonia != null)
                {
                    strColonia = ",Colonia: " + colonia.Asentamiento + " CP:" + colonia.CodigoPostal + "";

                }

                string domCompleto = strDomicilio + "" + strColonia;

                object obj = null;
                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = detalleDetencion.Fecha.Value;
                TimeSpan horasRestantes;
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                var situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;

                if(calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }
                else
                {
                    fechaSalidaAdd = fechaSalida.AddHours(36);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }
                var informacion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                var evento = ControlEvento.ObtenerById(informacion.IdEvento);
                var eventounidadresponsable = ControlEventoUnidadResponsable.ObtenerByEventoId(evento.Id);
                var Eur = ControlEventoUnidadResponsable.ObtenerTodosByEventoId(evento.Id);
                var unidades = "";
                var l = 1;
                var responsables = "";
                var m = 1;
                foreach(var item in Eur)
                {
                    var uni = ControlUnidad.ObtenerById(item.UnidadId);
                    if(l==1)
                    {
                        unidades =  uni.Nombre;
                    }
                    else
                    {
                        unidades = unidades +", "+ uni.Nombre;
                    }
                    l++;
                    var responsable = ControlCatalogo.Obtener(item.ResponsableId, Convert.ToInt32(Entity.TipoDeCatalogo.responsable_unidad));
                    if(m==1)
                    {
                        responsables= responsable.Nombre;
                    }
                    else
                    {
                        responsables = responsables + ", " + responsable.Nombre;
                    }
                    m++;
                }
                var unidadprueba = "";
                var responsableprueba = "";
                var unidad = new Entity.Unidad();
                if(eventounidadresponsable !=null)
                {
                     unidad = ControlUnidad.ObtenerById(eventounidadresponsable.UnidadId);
                    var responsable = ControlResponsable.ObtenerPorUnidadIdEventoId(new object[] { unidad.Id, evento.Id });
                    unidadprueba = unidad.Nombre;
                    responsableprueba = responsable.Nombre;
                }
               
                
                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = situacion != null ? situacion.Nombre : "",
                    Domicilio = domCompleto,
                    RutaImagen = interno.RutaImagen,
                    Registro = detalleDetencion.Fecha.Value.ToString("dd-MM-yyyy HH:mm:ss"),
                    Salida = fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss"),
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                    horasRestantes.TotalHours,
                    Lugar=informacion.LugarDetencion,
                    Unidad1=unidades,
                    Responsable1=responsables
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }

    public class CalificacionAux
    {
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string TrackingIdInterno { get; set; }
        public string SituacionId { get; set; }
        public string InstitucionId { get; set; }
        public string Fundamento { get; set; }
        public string TrabajoSocial { get; set; }
        public string MaxHoras { get; set; }
        public string TotalHoras { get; set; }

        //public string Programa { get; set; }
        //public decimal PorcentajeHoras { get; set; }
        public string SoloArresto { get; set; }
        public string TotalDeMultas { get; set; }
        public string Agravante { get; set; }
        public string Ajuste { get; set; }
        public string TotalAPagar { get; set; }
        public string Razon { get; set; }
    }

    public class MotivoDetencionAux
    {
        public string MotivoDetencionId { get; set; }
        public string TrackingInternoId { get; set; }
        public string MultaSugerida { get; set; }
        public string Id { get; set; }
        public string TrackingId { get; set; }
        public string SalarioMinimo { get; set; }
        public string ImporteMultaMinimo { get; set; }
        public string ImporteMultaMaximo { get; set; }
    }

    public class ContratoEditAux
    {
        public string Id { get; set; }
        public string Motivo { get; set; }
        public string Descripcion { get; set; }
        public string Articulo { get; set; }
        public string MultaMinimo { get; set; }
        public string MultaMaximo { get; set; }
        public string horaArresto { get; set; }
        public string SalarioMinimo { get; set; }
        public string ImporteMultaMinimo { get; set; }
        public string ImporteMultaMaximo { get; set; }
        public string Tipo { get; set; }
    }
}