﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;

namespace Web.Application.Sentence
{
    public partial class MotivoDetencionInterno : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Juez calificador" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                        this.WERQEQ.Value = permisos.Consultar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }


        [WebMethod]
        public static string blockitem(string id)
        {
            try
            {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                int usuario;
                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");
                Entity.MotivoDetencionInterno motivo = ControlMotivoDetencionInterno.ObtenerByTrackingId(new Guid(id));
                motivo.Habilitado = motivo.Habilitado ? false : true;
                string mensaje = motivo.Habilitado ? "habilitado" : "dehabilitado";
                ControlMotivoDetencionInterno.Actualizar(motivo);
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }



        [WebMethod]
        public static string delete(int id)
        {
            try
            {
                //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Eliminar)
                //{
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                Entity.Motivo item = ControlMotivo.ObtenerPorId(id); ;
                item.Activo = false;
                item.CreadoPor = usuario;
                ControlMotivo.Actualizar(item);
                return JsonConvert.SerializeObject(new { exitoso = true });
                //}
                //else
                //{
                //    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                //}
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }


        [WebMethod]
        public static DataTable getCustomers(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string TrackingProcesoId)
        {
            try
            {

                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(TrackingProcesoId));
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        //where.Add(new Where("C.Activo", "1"));

                        string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };

                        Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                        where.Add(new Where("E.internoId", detalleDetencion.Id.ToString()));
                        Query query = new Query
                        {
                            select = new List<string>{
                        "C.Id",
                        "C.TrackingId",
                        "C.Motivo",
                        "C.Descripcion",
                        "C.Articulo",
                        "E.MultaSugerida",
                        "C.HorasdeArresto",
                        "C.Activo",
                        "E.Habilitado",
                        "E.Id IdMotInterno",
                        "E.TrackingId TrackingIdMotInterno",
                        "E.MotivoDetencionId"
                        },
                            from = new DT.Table(" motivo_detencion_interno", "E"),
                            joins = new List<Join>
                            {
                                new Join(new Table("MotivoDetencion", "C"), "C.Id  = E.MotivoDetencionId"),
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }


        [WebMethod]
        public static List<Combo> getListadoCombo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            List<Entity.MotivoDetencion> listado = ControlMotivoDetencion.ObtenerTodos();
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);


            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x => x.ContratoId == contratoUsuario.IdContrato && x.Tipo == contratoUsuario.Tipo && x.Activo == 1 && x.Habilitado == 1))
                    combo.Add(new Combo { Desc = rol.Motivo, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static DataTable getinterno(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        where.Add(new Where("E.Estatus", "<>", "2"));

                        Query query = new Query
                        {
                            select = new List<string> {
                        "I.Id",
                        "I.TrackingId",
                        "I.Nombre",
                        "I.Paterno",
                        "I.Materno",
                        "E.Expediente",
                        "E.NCP",
                        "E.Estatus",
                        "E.Activo",
                        "E.TrackingId TrackingIdEstatus" ,
                       "I.RutaImagen",
                        "IFNULL(a.alias, '') AliasInterno",
                        "IFNULL(G.fechanacimineto, NOW()) FechaNacimiento"
                    },
                            from = new Table("detalle_detencion", "E"),
                            joins = new List<Join>
                    {
                        new Join(new Table("detenido", "I"), "I.id  = E.DetenidoId"),

                        new Join(new Table("(select DetenidoId, group_concat(Alias) alias from Alias group by DetenidoId)", "A"), "I.id  = A.DetenidoId", "LEFT OUTER"),
                        new Join(new Table("general", "G"), "I.id  = G.DetenidoId", "LEFT OUTER"),
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static Object GetMotivoDetencionById(string _id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {
                    var contract = ControlMotivoDetencion.ObtenerPorId(Convert.ToInt32(_id));
                    ContratoEditAux obj = new ContratoEditAux();

                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    //List<Entity.ContratoUsuario> contratousuario = new List<Entity.ContratoUsuario>();
                    //contratousuario = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                    //List<Entity.ContratoUsuario> ListaContratosusuarios = new List<Entity.ContratoUsuario>();


                    //foreach (Entity.ContratoUsuario item in contratousuario)
                    //{
                    //    if(item.Activo)
                    //    {
                    //        ListaContratosusuarios.Add(item);
                    //    }

                    //}
                    int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                    Entity.ContratoUsuario contrato_usuario = ControlContratoUsuario.ObtenerPorId(idContrato);
                    Entity.Subcontrato subcontrato = contrato_usuario != null ? ControlSubcontrato.ObtenerPorId(contrato_usuario.IdContrato) : null;                    
                    //if (contrato_usuario != null)
                    //{

                    //    if (contrato_usuario.Tipo == "subcontrato")
                    //    {
                    //        subcontrato = ControlSubcontrato.ObtenerPorId(contrato_usuario.IdContrato);

                    //    }
                    //    else
                    //    {
                    //        subcontrato = ControlSubcontrato.ObtenerByContratoId(idContrato)[0];
                    //    }

                    //}
                    
                    //foreach (Entity.ContratoUsuario item in ListaContratosusuarios)
                    //{
                       
                        //subcontrato = ControlSubcontrato.ObtenerByContratoId(item.IdContrato)[0];
                    //}

                    if (contract != null)
                    {
                        obj.Id = contract.Id.ToString();
                        obj.Motivo = contract.Motivo.ToString();
                        obj.Descripcion = contract.Descripcion;
                        obj.Articulo = contract.Articulo.ToString();
                        obj.MultaMinimo =Convert.ToInt32( contract.MultaMinimo).ToString();
                        obj.MultaMaximo = Convert.ToInt32(contract.MultaMaximo).ToString();
                        obj.horaArresto = contract.HorasdeArresto.ToString();
                        obj.SalarioMinimo = subcontrato.SalarioMinimo.ToString();
                        obj.ImporteMultaMinimo = Math.Round((contract.MultaMinimo * subcontrato.SalarioMinimo),2).ToString();
                        obj.ImporteMultaMaximo = Math.Round((contract.MultaMaximo * subcontrato.SalarioMinimo),2).ToString();
                    }
                    return obj;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }

        }
        [WebMethod]
        public static string TieneMotivos(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {

                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] internoestatus = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
               // Entity.Residente residente = ControlResidente.ObtenerPorEstatusInternoId(estatus.Id);
                    Entity.MotivoDetencionInterno motivo = new Entity.MotivoDetencionInterno();
                    motivo.InternoId = detalleDetencion.Id;
                    List<Entity.MotivoDetencionInterno> listaMotivo = new List<Entity.MotivoDetencionInterno>();

                    listaMotivo=ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(motivo);

                    object obj = null;

                    obj = new
                    { registros = listaMotivo.Count.ToString()
                        
                    };


                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });


                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }



        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Consultar)
                {


                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                    Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);


                    Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                            var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                            Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                            foreach (var item in detenidosEvento)
                            {
                                if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                    detenidoEvento = item;
                            }

                            if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                            else
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }
                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }
                    var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                    Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                    var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                    Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                    object obj = null;
                    if (interno != null)
                    {
                        if (string.IsNullOrEmpty(interno.RutaImagen))
                        {
                            interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                        }
                    }

                    obj = new
                    {
                        Id = interno != null ? interno.Id.ToString() : "",
                        TrackingId = interno != null ? interno.TrackingId.ToString() : "",
                        Expediente = (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                        Centro = (institucion != null) ? institucion.Nombre : "",
                        Fecha = detalleDetencion != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Nombre = (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                        RutaImagen = (interno != null) ? interno.RutaImagen : "",
                        SituacionId = calificacion != null ? calificacion.SituacionId : 0,
                        InstitucionId = calificacion != null ? calificacion.InstitucionId : 0,
                        Fundamento = calificacion != null ? calificacion.Fundamento : "",
                        TrabajoSocial = calificacion != null ? calificacion.TrabajoSocial : false,
                        TotalHoras = calificacion != null ? calificacion.TotalHoras : 0,
                        SoloArresto = calificacion != null ? calificacion.SoloArresto : false,
                        TotalDeMultas = calificacion != null ? calificacion.TotalDeMultas : 0,
                        Agravante = calificacion != null ? calificacion.Agravante : 0,
                        Ajuste = calificacion != null ? calificacion.Ajuste : 0,
                        TotalAPagar = calificacion != null ? calificacion.TotalAPagar : 0,
                        Razon = calificacion != null ? calificacion.Razon : "",
                        IdCalificacion = calificacion != null ? calificacion.Id.ToString() : "",
                        TrackingCalificacion = calificacion != null ? calificacion.TrackingId.ToString() : "",
                        Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                        Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        municipioNombre = municipio != null ? municipio.Nombre : "Municipio no registrado",
                        domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        //[WebMethod]
        //public static Object ValidaExistenciaMotivDentencionInterno(MotivoDetencionAux contrato)
        //{
        //    Entity.MotivoDetencionInterno filtro = new Entity.MotivoDetencionInterno();
        //    filtro.MotivoDetencionId = Convert.ToInt32(contrato.MotivoDetencionId);
        //    filtro.InternoId = Convert.ToInt32(contrato.InternoId);
        //    Entity.MotivoDetencionInterno motvivodetinterno = ControlMotivoDetencionInterno.GetMotivoDetencionInterno(filtro);
        //    if (motvivodetinterno.InternoId != 0 && motvivodetinterno.MotivoDetencionId != 0)
        //    {
        //        return new { }
        //    }


        //}
        [WebMethod]
        public static Object save(MotivoDetencionAux contrato)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Modificar)
                {
                    var con = new Entity.MotivoDetencionInterno();
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                    string id = contrato.Id.ToString();
                    if (id == "") id = "";

                    //object[] data = new object[] { contrato.Id, contrato.Motivo };
                    //var contrato_duplicado = ControlContrato.ObtenerPorClienteYContrato(data);
                    string[] internod = new string[2] {
                            contrato.InternoId.ToString(), "true"
                        };

                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);

                    Entity.MotivoDetencionInterno filtro = new Entity.MotivoDetencionInterno();
                    filtro.MotivoDetencionId = Convert.ToInt32(contrato.MotivoDetencionId);
                    filtro.InternoId = Convert.ToInt32(detalleDetencion.Id);
                    filtro.MultaSugerida = Convert.ToDecimal(contrato.MultaSugerida);
                    Entity.MotivoDetencionInterno motvivodetinterno = ControlMotivoDetencionInterno.GetMotivoDetencionInterno(filtro);                    

                    if (string.IsNullOrEmpty(id))
                    {
                        if (motvivodetinterno != null)
                        {
                            if (motvivodetinterno.InternoId != 0 && motvivodetinterno.MotivoDetencionId != 0)
                            {
                                return new { exitoso = false, mensaje = "El interno ya tiene asignado ese motivo de detención" };
                            }
                        }

                        mode = @"registró";

                        con.MotivoDetencionId = Convert.ToInt32(contrato.MotivoDetencionId);                      
                        con.InternoId = detalleDetencion.Id;
                        con.MultaSugerida = Convert.ToDecimal(contrato.MultaSugerida);
                        con.TrackingId = Guid.NewGuid();
                        con.CreadoPor = usId;
                        con.Activo = true;
                        con.Habilitado = true;
                        con.Id = ControlMotivoDetencionInterno.Guardar(con);                                               
                    }
                    else
                    {
                        mode = @"actualizó";

                        con.MotivoDetencionId = Convert.ToInt32(contrato.MotivoDetencionId);
                        con.InternoId = detalleDetencion.Id;
                        con.MultaSugerida = Convert.ToDecimal(contrato.MultaSugerida);
                        con.Id = Convert.ToInt32(contrato.Id);
                        con.TrackingId = new Guid(contrato.TrackingId);
                        con.Activo = true;
                        con.Habilitado = true;
                        ControlMotivoDetencionInterno.Actualizar(con);
                    }

                    return new { exitoso = true, mensaje = mode };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                DateTime nacimiento;
                int edad = 0;
                if (general != null)
                {
                    nacimiento = general.FechaNacimineto;
                    edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = domicilio.Localidad + ", " + domicilio.Calle + " " + domicilio.Numero;

                }

                object obj = null;

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = "PENDIENTE",
                    Domicilio = strDomicilio,

                    RutaImagen = interno.RutaImagen,
                    Registro = detalleDetencion.Fecha,
                    Salida = "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        public class MotivoDetencionAux
        {
            public string MotivoDetencionId { get; set; }
            public string InternoId { get; set; }
            public string MultaSugerida { get; set; }
            public string Id { get; set; }
            public string TrackingId { get; set; }
        }


        public class ContratoAux
        {
            public string Id { get; set; }
            public string Motivo { get; set; }
            public string Descripcion { get; set; }
            public string Articulo { get; set; }
            public string MultaMinimo { get; set; }
            public string MultaMaximo { get; set; }
            public string horaArresto { get; set; }
            public string SalarioMinimo { get; set; }
            public string ImporteMultaMinimo { get; set; }
            public string ImporteMultaMaximo { get; set; }
        }

        public class ContratoEditAux
        {
            public string Id { get; set; }
            public string Motivo { get; set; }
            public string Descripcion { get; set; }
            public string Articulo { get; set; }
            public string MultaMinimo { get; set; }
            public string MultaMaximo { get; set; }
            public string horaArresto { get; set; }
            public string SalarioMinimo { get; set; }
            public string ImporteMultaMinimo { get; set; }
            public string ImporteMultaMaximo { get; set; }
        }

    }
}