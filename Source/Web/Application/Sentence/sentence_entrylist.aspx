﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="sentence_entrylist.aspx.cs" Inherits="Web.Application.Sentence.sentence_entrylist" %>




<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Juez calificador</li>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' />
    <style type="text/css">
        .dropdown-menu{
            position: relative;
        }
         td.strikeout {
            text-decoration: line-through;
        }
      
        #dropdown {
            width: 459px;
        }
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }
        #mapid {
            height: 20vw;       
            width: 100%;
        }
        .coordinates {
            background: rgba(0,0,0,0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding:5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }
        .modal {
          overflow-y:auto;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 13px;
            line-height: 1.42857143;
        }
        input[type=checkbox]{
            -ms-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
            padding: 10px;
            cursor: pointer;
        }
        .checkboxtext{
            font-size: 100%;
            display: inline;       
            color: #004987;
        }
        .checkboxtext:hover{
            cursor:pointer;
        }

        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <div class="scroll">
        
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
        <%--<div class="row">
            <section class="col col-12">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
            
                <label style="float: right"> Mostrar: &nbsp</label>
            </div>
                </section>
        </div>--%>
    <div class="row" id="addentry" style="display: none;">
        <section class="col col-12">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
                <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Generar reporte de extravío </a>
                <%--<a class="btn btn-md btn-default" id="salidaefectuada"><i class="fa fa-sign-out"> </i>&nbsp;Salida efectuada</a>--%>
                <a class="btn bg-color-purple txt-color-white  btn-lg verEvento" id="linkEvento" title="Alerta web / evento" href="javascript:void(0);" ></a>
                <a class="btn bg-color-yellow txt-color-white  btn-lg examen" id="examen" title="Examen médico" href="javascript:void(0);"><i class="fa fa-user-md"></i>&nbsp; Examen médico</a>
                <a class="btn btn-info  btn-lg historial" title="Historial" id="historial" href="javascript:void(0);"><i class="fa fa-history fa-lg"></i>&nbsp; Historial</a>
                <a class="btn btn-success txt-color-white  btn-lg " id="salidaefectuadajuez"><i class="fa fa-sign-out"> </i>&nbsp;Autorizar salida</a>
                <a class="btn btn-primary txt-color-white btn-lg calificar" id="calificar"><i class="fa fa-sign-out"> </i>&nbsp;Calificar</a>
                <a class="btn btn-warning  btn-lg edit " id="traslado"><i class="glyphicon glyphicon-transfer"> </i>&nbsp;Traslado</a>
                <a style=" display: none;" class="btn btn-danger  btn-lg recibirpertenencia " id="recibirpertenencia"><i class="glyphicon glyphicon-briefcase"> </i>&nbsp;Recibir pertenencia</a>
                <a style=" display: none;" class="btn bg-color-green txt-color-white btn-lg  entregarpertenencia " id="entregarpertenencia"><i class="glyphicon glyphicon-briefcase"> </i>&nbsp;Entregar pertenencia</a>

                  
                
                    <%--<option value="0" selected="selected">Todos los detenidos</option>
                    <!--  <option value="6">Detenidos en ingreso</option> -->
                       <option value="4">Detenidos en examén médico</option> 
                    <option value="5">Detenidos calificados</option>
                    <option value="3">Detenidos por salir</option>
                    <!--  <option value="7">Detenidos en custodia externa</option>-->
                    <!--  <option value="8">Detenidos por fecha y hora de salida</option>-->
                    <!--<option value="9">Detenidos salida autorizada</option>-->
                    <!--<option value="10">Salida autorizada por pago de multa</option>-->
                    <option value="2">Detenidos sin calificar</option> 
                    <option value="1">Menores de edad</option>--%>
           </div>
        </section>
        <section class="col col-6">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">

                <%--<a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Sentence/celdas.aspx"" class="btn btn-md btn-default" id="celdas"><i class="fa"></i>Celdas </a>--%>
              
            </div>
        </section>
    </div>

    <p></p>
    <section id="widget-grid" class="">
       
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-detenidos-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Detenidos en existencia </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">                            
                                    <div class="row">                                        
                                        <div class="col-sm-3"> 
                                            <label for="tbFechaInicial">Fecha Inicial:</label>
                                            <asp:TextBox ID="tbFechaInicial" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                        
                                        <div class="col-sm-3"> 
                                            <label for="tbFechaFinal">Fecha Final:</label>
                                            <asp:TextBox ID="tbFechaFinal" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-3">
                                              <a class="btn btn-success btn-md" id="btnBuscarInfo"><i class="fa fa-search"></i> Consultar</a>
                                        </div>
                                    </div>                                   
                                    <%--<div class="row">
                                        <section class="col-md-2">
                                                 <a class="btn btn-success btn-md ShowModal" style="margin-top:22px;padding:5px;height:35px; margin-right:15px;"><i class="fa fa-search"></i> Buscar detenido</a>
                                            </section>
                                    </div>--%>
                                     <br />
                            <%--<div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                                    <div class="form-group">
                                        <label class="col-md-1 control-label" style="color: #3276b1; text-align: right;font-weight:bold; margin-right:8px"">Año:</label>
                                        <div class="col-md-8">
                                            <select id="idAnioDetenidos" class="select2"></select>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">

                                </div>
                                <div class="col-xs-5 col-sm-4 col-md-3 col-lg-3"></div>
                                <div class="col-xs-4 col-lg-3">
                                    <div class="form-group">
                                    <label class="col-md-2 control-label" style="color: #3276b1; text-align: center;font-weight:bold; margin-right:10px"">Mostrar:    </label>
                                    <div class="col-md-9">
                                        <select class="select2 mostrar" id="filtrojuezcalificador" >
                                        </select>
                                        </div>
                                </div>
                                    </div>
                            </div>--%>
                            <div class="row" style="width:100%;margin:0 auto;">
                                <table id="dt_basic" width="100%" class="table table-responsive table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th ></th>                                        
                                            <th <%--style="text-align:center"--%> data-class="expand">#</th>
                                            <th <%--style="text-align:center"--%>>Fotografía</th>
                                            <th <%--style="text-align:center"--%>>Nombre</th>
                                            <th <%--style="text-align:center"--%>>Apellido paterno</th>
                                            <th <%--style="text-align:center"--%> data-hide="phone,tablet">Apellido materno</th>
                                            <th <%--style="text-align:center" --%>data-hide="phone,tablet">No. remisión</th>                                     
                                            <th <%--style="text-align:center" --%>data-hide="phone,tablet">Situación del detenido</th>
                                            <th <%--style="text-align:center"--%>>Pago multa</th>
                                            <th <%--style="text-align:center"--%>>Horas</th>
                                            <%--<th>Fecha de salida</th>--%>
                                            <th <%--style="text-align:center"--%> data-hide="phone,tablet">Acciones</th>
                                            <th ></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de 
                                       <strong>  <span id="verb"></span></strong>
                                       

                                       &nbsp;el proceso  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-file-pdf-o" +=""></i> Generar reporte de extravío</h4>
                </div>
                <div class="modal-body">
                    <h5>Listado de  detenidos en existencia</h5>
                    <div id="addTable" class="row table-responsive">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label style="color: #3276b1; text-align: right;font-weight:bold; margin-right:8px">Año:</label>
                            <select id="filterYear" class="select2"></select>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <br />                            
                        </div>                        
                        <div class="col-sm-12">
                            <table id="dt_basicadd" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">Nombre</th>
                                       
                                        <th >No. Remisión</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="trasladoInterno-modal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class="glyphicon glyphicon-transfer"> </i> Traslado
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="registro-form" class="smart-form">

                        <div class="modal-body">
                            <div id="calculo_2" class="smart-form">
                                <fieldset>
                               
                                   <section><label class="input" id="IdNombreInterno"></label></section>

                                    
                                    <section>
                                       
                                           
                                                <label style="color: dodgerblue">Institución a dispocición: <a style="color: red">*</a>        </label>
                                        <label class="select" >
                                        <select name="rol" class="target" id="dropdown" runat="server" style="width:100%">
                                        </select>
                                        <i></i>
                                         </label>
                                            
                                      
                                           
                                    </section>
                                    <section>
                                                                                  
                                        <label style="color: dodgerblue">Horas a dispocición: <a style="color: red">*</a>        </label>
                                        <label class="textarea" >
                                            <textarea name="horas" maxleght="4" id="hours" runat="server" style="width:100%"></textarea>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el máximo de horas a disposición.</b>
                                        </label>
                                                                                                                            
                                    </section>
                                    <section >
                                                                    <label style="color: dodgerblue"  class="label">Tipo salida <a style="color: red">*</a></label>
                                                                    <label class="select">
                                                                        <select disabled="disabled" name="centro" id="combotiposalida2" >
                                                                        </select>
                                                                        <i></i>
                                                                    </label>
                                                                </section>
                                    <section>
                                    <label style="color: dodgerblue" class="input">Fundamento <a style="color: red">*</a></label>
                                    <label class="textarea">
                                     <textarea id="fundamentosalida" maxlength="512" name="observaciones" style="width:525px"  rows="5"></textarea>
                                        <b class="tooltip tooltip-bottom-right">Ingresa el fundamento.</b>
                                   </label>    
                                </section>  
                                    </fieldset>
                                    
                            </div>
                        </div>
                        
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="guardatraslado"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <%--  --%>
    <div id="ingresocelda-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Movimiento en celda
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="register-form" class="smart-form">

                        <div class="modal-body">
                            <div id="calculo_1" class="smart-form">
                                <fieldset>
                                    <section>
                                        <header>Datos del movimiento</header>
                                    </section> 

                                          <section class="col col-4">
                                                    <label class="label">Descripción</label>
                                                    <label class="select">
                                                <select name="descripcioncelda" id="descripcioncelda" runat="server">
                                                    <option value="0">[Entrada o salida]</option>
                                                    <option value="1">Entrada</option>
                                                    <option value="2">Salida</option>
                                                </select>
                                                        </label>
                                                <i></i>
                                        
                                                </section>

                                                    <section>
                                                            <label class="label">Fecha y hora </label>
                                                            <label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker'>
                                                                <input type="text" name="Fecha detencion" id="fechahora" class='input-group date alptext' placeholder="Fecha y hora" data-requerido="true"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                        </label>
                                                        <i></i>
                                        
                                                </section>

                                                    <section >
                                                    <label class="label">Celda </label>
                                                  <label class="input">
                                                            <label class='input' id='celda'>
                                                                
                                                                <input type="text" name="celda" id="idcelda" class='input alptext' placeholder="Celda" data-requerido="true"/>
                                                                
                                                            </label>
                                                       
                                                        </label>
                                                       
                                                <i></i>
                                        </section>
                                                    <section>
                                                   
                                                        <a  class="btn btn-default buscaCelda" id="buscaCelda"  title="Buscar celda"><i class="fa fa-search"></i>&nbsp; Buscar&nbsp;  </a>
                                                    </section>
                                                    <section >
                                                        <label class="label">Observaciones <a style="color: red">*</a></label>
                                                        <label class="input">
                                                            <textarea id="observaciones" name="observaciones" style="width:500px"  rows="5"></textarea>                                                            
                                                        </label>                                                        
                                                    </section>
                                                     <section class="col col-12">
                                                    <label class="label">Responsable </label>
                                                    
                                                          <label class="input">
                                                                    <label class='input' id='responsable'>
                                                                
                                                                        <input size="200" type="text" name="responsable" id="idresponsable" class='input alptext' placeholder="Responsable" data-requerido="true"/>
                                                                
                                                                    </label>
                                                       
                                                           </label>
                                                       
                                                <i></i>
                                        </section>

                                 </fieldset>
                            </div>
                        </div>

                        
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default calculatebtn" id="calculatebtn"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <%--  --%>
     <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>
     <input type="hidden" id="tiposalidaid"  value=""/>
     <div id="printpdf-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="printpdf-modal-title"></h4>
                </div>

                <div class="modal-body">
                    <div id="printpdf-modal-body" class="smart-form">

                        <div class="modal-body">
                            <div id="printpdf-modal-body-form" class="smart-form">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="row">
                                                <section class="col col-10">
<%--                                                    <img  class="img-thumbnail" alt=""  src="~/Content/img/avatars/male.png" width="120" height="120" />--%>

                    <img id="avatar" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" width="120" height="120" />                                    

                                                </section>
                                            </div>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Nombre <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" readonly="true" name="nombrereportepdf" id="nombrereportepdf" class="alptext" placeholder="Nombre" />
                                                        <b class="tooltip tooltip-bottom-right">Nombre</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue"> Edad <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="edadreportepdf" id="edadreportepdf" placeholder="Edad" class="integer alptext" maxlength="2" />
                                                        <b class="tooltip tooltip-bottom-right">Edad</b>
                                                    </label>
                                                </section>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioBuscandopdf" value="BUSCADO(A)" /><i></i>Buscado
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioExtraviadopdf" value ="EXTRAVIADO(A)" /><i></i>Extraviado
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioInformacionpdf" value="INFORMACIÓN " /><i></i>Información
                                            </label>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Alias <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="aliasreportepdf" id="aliasreportepdf" placeholder="Alias" maxlength="246" />
                                                        <b class="tooltip tooltip-bottom-right">Alias</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Violento</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="violentopdf" id="violentopdf" placeholder="Violento" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Violento</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Enfermo mental</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="enfermopdf" id="enfermopdf" class="alptext" placeholder="Enfermo mental" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Enfermo mental</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Señas particulares</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="textarea">
                                                        <textarea rows="3" name="senaspdf" id="senaspdf" placeholder="Señas particulares" class="alptext" maxlength="500"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Señas particulares</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Comentarios</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="textarea">
                                                        <textarea rows="3" name="comentariospdf" id="comentariospdf" class="alptext" placeholder="Comentarios" maxlength="500"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Comentarios</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section style="color: dodgerblue"class="col col-9">
                                                    <label>Información de contacto:</label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Institución</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="institucionpdf" id="institucionpdf" class="alptext" placeholder="Institución" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Institución para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Dirección</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" class="alptext" name="direccionpdf" id="direccionpdf" placeholder="Dirección" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Dirección para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label style="color: dodgerblue">Teléfono</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="tel" data-mask="(999) 999-9999" class="alptext" name="telefonopdf" id="telefonopdf" placeholder="Telefono" maxlength="50" />
                                                        <b class="tooltip tooltip-bottom-right">Teléfono para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                        </div>

                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <input type="hidden" id="trackingidpdf"/>
                        <input type="hidden" id="idpdf"/>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal" id="cancelarpdfbtn"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default printpdfbtn" id="printpdfbtn"><i class="fa fa-file-pdf-o"></i>&nbsp;Generar reporte </a>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modalEditarDetenido"   role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="titleModalEditarDetenido"></h4>
                </div>
                <div class="modal-body">                    
                    <div id="formEditarDetenido" class="smart-form ">
                        <div class="row">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="nav-item active" id="registroDetenido">
                                    <a class="nav-link" href="#tab1" data-toggle="tab">Registro del detenido</a>
                                </li>
                                <li class="nav-item" id="infoGeneral">
                                    <a class="nav-link" href="#tab2" data-toggle="tab">Información general del detenido</a>
                                </li>      
                                <li class="nav-item" id="domicilioDetenido">
                                    <a class="nav-link" href="#tab3" data-toggle="tab">Domicilio del detenido</a>
                                </li>   
                                <li class="nav-item" id="aliasDetenido">
                                    <a class="nav-link" href="#tab4" data-toggle="tab" onclick="reload()">Alias del detenido</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="row">
                                    <fieldset>             
                                        <div class="row">
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Fecha y hora de ingreso <a style="color: red">*</a></label>
                                            <label class="input">
                                                <label class='input-group date' id='autorizaciondatetimepicker' style="width:100%">
                                                    <input type="text" name="fecha" id="fecha" class='input-group date alptext' placeholder="Fecha y hora de ingreso" disabled="disabled" />
                                                    <i class="icon-append fa fa-calendar"></i>                                            
                                                </label>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">No. remisión</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-hashtag"></i>
                                                <input type="text" name="expediente" id="expediente" placeholder="No. de remisión" class="integer alptext" maxlength="13" disabled="disabled"/>
                                            </label>
                                        </section>  
                                        </div>
                                        <div class="row">
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Nombre(s) <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-pencil"></i>
                                                <input type="text" name="nombreDetenido" id="nombreDetenido" placeholder="Nombre" maxlength="50" class="alphanumeric alptext"/>
                                                <b class="tooltip tooltip-bottom-right">Ingrese el nombre.</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Apellido paterno <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-pencil"></i>
                                                <input type="text" name="paterno" id="paterno" placeholder="Apellido paterno" maxlength="50" class="alphanumeric alptext"/>
                                                <b class="tooltip tooltip-bottom-right">Ingrese el apellido paterno.</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Apellido materno <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-pencil"></i>
                                                <input type="text" name="materno" id="materno" placeholder="Apellido materno" maxlength="50" class="alphanumeric alptext"/>
                                                <b class="tooltip tooltip-bottom-right">Ingrese el apellido materno.</b>
                                            </label>
                                        </section>
                                        <section  class="col col-6">
                                            <label style="color:dodgerblue" class="label">Fotografía detenido <a style="color: red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-file-image-o"></i>
                                                <asp:FileUpload id="imagenDetenido" runat="server" />
                                                <input type="text" id="imagenDetenidoOriginal" hidden="hidden" style="display:none;" />
                                                <input type="text" id="idDetenido" hidden="hidden" style="display:none;" />
                                                <input type="text" id="trackingDetenido" hidden="hidden" style="display:none;" />
                                            </label>
                                        </section>
                                        <section class="col col-6">
										<label for="Lesion_Visible">Lesiones visibles a simple vista </label> <input name="Lesion_visible" id="Lesion_visible" type="checkbox" value="false" disabled="disabled"> 
                                    
                                    </section>
                                        <section class="col col-4"></section>
                                        <section class="col col-4" align="center">

                                            <img id="detenidoEditar" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>  source" width="70" height="70" />

                                        </section>
                                        <section class="col col-4"></section>
                                            </div>
                                    </fieldset>
                                    <footer style="padding: 0px;">
                                        <div class="row" style="display: inline-block; float: right; margin: 0px 15px;">
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btnSaveDetenido"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelDetenido"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                                        </div>
                                    </footer>
                                </div>
                                </div>     
                            <div class="tab-pane" id="tab2">
                                <div class="row">
                                    <fieldset>                                                                        
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Fecha de nacimiento <a style="color: red">*</a></label>
                                            <label class="input">
                                                <label class='input-group date' >
                                                <input type="text" name="fecha" id="Text1" placeholder="Fecha de nacimiento" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la fecha de nacimiento.</b>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                     </label>       
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Nacionalidad <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="nacionalidad" runat="server" class="select2" style="width:100%;">
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">RFC</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="rfc" id="rfc" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" runat="server" placeholder="RFC" maxlength="13" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa el RFC.</b>
                                            </label>
                                            <input type="hidden" id="trackingidInterno" runat="server" />
                                            <input type="hidden" id="trackingGeneral" runat="server" />
                                            <input type="hidden" id="idGeneral" runat="server" />
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">CURP</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="curp" id="CURP" runat="server" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="CURP" maxlength="18" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la CURP.</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Persona a quien se notifica</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-edit"></i>
                                                <input type="text" name="personanotifica" id="personanotifica"  placeholder="Persona a quien se notifica" maxlength="150" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la persona a quien se notifica.</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Celular </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-phone"></i>
                                                <input  name="celular" id="celular" maxlength="10" type="tel" placeholder="Celular" data-mask="(999) 999-9999"/>
                                                <b class="tooltip tooltip-bottom-right">Ingresa el celular.</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Escolaridad <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="escolaridad" runat="server" class="select2" style="width:100%;">
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Religión</label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="religion" runat="server" class="select2" style="width:100%;">
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Ocupación <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="ocupacion" runat="server" class="select2" style="width:100%;">
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Estado civil <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="civil" runat="server" class="select2" style="width:100%;">
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-3">
                                            <label style="color:dodgerblue" class="label">Etnia </label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="etinia" runat="server" class="select2" style="width:100%;">
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-3">
                                            <label style="color:dodgerblue" class="label">Sexo <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="sexo" runat="server" class="select2" style="width:100%;">
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section> 
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Lengua nativa </label>
                                            <%--<label class="select">--%>
                                                <select name="lenguanativa" id="lenguanativa" class="select2" style="width:100%;" >
                                                </select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>     
                                    </fieldset>
                                    <footer style="padding: 0px;">
                                        <div class="row" style="display: inline-block; float: right; margin: 0px 15px;">
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btnSaveGeneral"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelGeneral"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                                        </div>
                                    </footer>
                                </div>
                            </div>                            
                                <div class="tab-pane" id="tab3">
                                    <div class="row">
                                        <div class="row"></div>
                                        <br />
                                        <legend>Domicilio actual</legend>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <br />
                                                    <div class="smart-form">
                                                        <fieldset>
                                                            <legend class="hide">&nbsp;</legend>
                                                            <div class="row">
                                                                <section class="col col-6">
                                                                    <label style="color:dodgerblue" class="label">País <a style="color: red">*</a></label>
                                                                    <%--<label class="select">--%>
                                                                        <select name="centro" id="paisdomicilio" runat="server" class="select2" style="width:100%;">
                                                                        </select>
                                                                        <i></i>
                                                                    <%--</label>--%>
                                                                </section>
                                                                <section class="col col-6" id="sectionestado">
                                                                    <label style="color:dodgerblue" class="label">Estado <a style="color: red">*</a></label>
                                                                    <%--<label class="select">--%>
                                                                        <select name="centro" id="estadodomicilio" runat="server" class="select2" style="width:100%;">
                                                                        </select>
                                                                        <i></i>
                                                                    <%--</label>--%>
                                                                </section>
                                                                <section class="col col-6" id="sectionmunicipio">
                                                                    <label style="color:dodgerblue" class="label">Municipio <a style="color: red">*</a></label>
                                                                    <%--<label class="select">--%>
                                                                        <select name="centro" id="municipiodomicilio" runat="server" class="select2" style="width:100%;">
                                                                        </select>
                                                                        <i></i>
                                                                    <%--</label>--%>
                                                                </section>
                                                                <section class="col col-6" id="sectioncoloniaDomicilio">
                                                                    <label style="color:dodgerblue" class="label">Colonia <a style="color: red">*</a></label>
                                                                    <%--<label class="select">--%>
                                                                        <select name="centro" id="coloniaSelect" runat="server" class="select2" style="width:100%;">
                                                                        </select>
                                                                        <i></i>
                                                                    <%--</label>--%>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label style="color:dodgerblue" class="label">Calle <a style="color: red">*</a></label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-street-view"></i>
                                                                        <input type="text" name="calle" id="calledomicilio" runat="server" placeholder="Calle" maxlength="250" class="alphanumeric alptext" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa la calle.</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2">
                                                                    <label style="color:dodgerblue" class="label">Número <a style="color: red">*</a></label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                        <input type="text" name="numero" id="numerodomicilio" runat="server" placeholder="Número" class="alphanumeric alptext" maxlength="15" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa el número.</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2" id="sectioncpdomicilio">
                                                                    <label style="color:dodgerblue" class="label">C.P. <a style="color: red">*</a></label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                        <input type="text" name="cp" id="cpdomicilio" runat="server" placeholder="C.P" maxlength="5" class="integer alptext" disabled="disabled" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa el C.P.</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label style="color:dodgerblue" class="label">Teléfono <a style="color: red">*</a></label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-phone"></i>
                                                                        <input type="tel" name="telefono" id="telefonodomicilio" runat="server" placeholder="Teléfono" data-mask="(999) 999-9999" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa el teléfono .</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-6" id="sectionlocalidad">
                                                                    <label style="color:dodgerblue" class="label">Localidad <a style="color: red">*</a></label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-street-view"></i>
                                                                        <input type="text" name="colonialugar" id="localidad" hidden="hidden" runat="server" placeholder="Localidad" maxlength="250" class="alphanumeric alptext" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa localidad.</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label style="color:dodgerblue" class="label">Latitud</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-map-marker"></i>
                                                                        <input type="number" step="any" name="latitud" id="latitud" class="alptext"/>
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa la latitud.</b>
                                                                    </label>                                                                    
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label style="color:dodgerblue" class="label">Longitud</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-map-marker"></i>
                                                                        <input type="number" step="any" name="longitud" id="longitud" class="alptext"/>
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa la longitud.</b>
                                                                    </label>                                                                    
                                                                </section>                                                                
                                                            </div>
                                                            <footer>
                                                                <a id="showMap" class="btn btn-default" title="Volver al listado"><i class="fa fa-map-marker"></i>&nbsp;Ver Mapa </a>                                                                   
                                                            </footer>
                                                        </fieldset>
                                                    </div>
                                                    <br />
                                                    <br />                                                            
                                                    <br />
                                                    <br />
                                                    <legend>Domicilio de nacimiento</legend>
                                                    <br />
                                                    <div id="Div3" class="smart-form">
                                                        <fieldset>
                                                            <legend class="hide">&nbsp;</legend>
                                                            <div class="row">
                                                                <section class="col col-6">
                                                                    <label style="color:dodgerblue" class="label">País  </label>
                                                                   <%-- <label class="select">--%>
                                                                        <select name="centro" id="paisnacimineto" runat="server" class="select2" style="width:100%;">
                                                                        </select>
                                                                        <i></i>
                                                                    <%--</label>--%>
                                                                </section>
                                                                <section class="col col-6" id="sectionestadon">
                                                                    <label style="color:dodgerblue"  class="label">Estado  </label>
                                                                    
                                                                        <select name="centro" id="estadonacimiento" runat="server" class="select2" style="width:100%;">
                                                                        </select>
                                                                        <i></i>
                                                                     
                                                                </section>
                                                                <section class="col col-6" id="sectionmunicipion">
                                                                    <label style="color:dodgerblue" class="label">Municipio  </label>
                                                                     
                                                                        <select name="centro" id="municipionacimiento" runat="server" class="select2" style="width:100%;">
                                                                        </select>
                                                                        <i></i>
                                                                     
                                                                </section>
                                                                <section class="col col-6" id="sectioncoloniaNacimiento">
                                                                    <label style="color:dodgerblue" class="label">Colonia  </label>
                                                                     
                                                                        <select name="centro" id="coloniaSelectNacimiento" runat="server" class="select2" style="width:100%;">
                                                                        </select>
                                                                        <i></i>
                                                                     
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label  style="color:dodgerblue" class="label">Calle  </label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-street-view"></i>
                                                                        <input type="text" name="callelugar" id="callelugar" runat="server" placeholder="Calle" maxlength="250" class="alphanumeric alptext" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa la calle.</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2">
                                                                    <label style="color:dodgerblue" class="label">Número  </label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                        <input type="text" name="numerolugar" id="numerolugar" runat="server" placeholder="Número" maxlength="15" class="alphanumeric alptext" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa el número.</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2" id="sectioncpnacimiento">
                                                                    <label style="color:dodgerblue" class="label">C.P.  </label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                        <input type="text" name="cplugar" id="cplugar" runat="server" placeholder="C.P" maxlength="5" class="integer alptext" disabled="disabled" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa el C.P.</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label style="color:dodgerblue" class="label">Teléfono  </label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-phone"></i>
                                                                        <input type="tel" name="telefonolugar" id="telefonolugar" runat="server" placeholder="Teléfono" data-mask="(999) 999-9999" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa el teléfono .</b>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-6" id="sectionlocalidadn">
                                                                    <label style="color:dodgerblue" class="label">Localidad  </label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-street-view"></i>
                                                                        <input type="text" name="colonialugar" id="localidadn" hidden="hidden" runat="server" placeholder="Localidad" maxlength="250" class="alphanumeric" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa localidad.</b>
                                                                    </label>
                                                                </section>                                                                
                                                            </div>
                                                        </fieldset>
                                                        <footer style="padding: 0px;">
                                                            <div class="row" style="display: inline-block; float: right; margin: 0px 15px;">
                                                                <a style="float: none;" href="javascript:void(0);" class="btn btn-default" id="btnSaveDomicilio" title="Guardar"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelDomicilio"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                                                            </div>
                                                        </footer>
                                                        <input type="hidden" id="trackingDomicilio" runat="server" />
                                                        <input type="hidden" id="idDomicilio" runat="server" />
                                                        <input type="hidden" id="trackingNacimiento" runat="server" />
                                                        <input type="hidden" id="idNacimiento" runat="server" />
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                            <div class="tab-pane" id="tab4">                                
                                <div class="row">                                    
                                </div>
                                <br />
                                <legend>Alias / Sobrenombre</legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <fieldset>                                            
                                            <a class="btn btn-md btn-default" id="addAlias"><i class="fa fa-plus"></i>&nbsp;Agregar </a>                                                                                                    
                                            <br />
                                            <br />
                                            <table id="dt_basicAlias" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th data-class="expand">#</th>
                                                        <th>Alias</th>
                                                        <th data-hide="phone,tablet">Acciones</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addAlias-modal" tabindex="-1" role="dialog" data-keyboard="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="modal_title_alias"></h4>
                    </div>
                    <div class="modal-body">
                        <div id="addAlia-modal" class="smart-form">
                            <div class="modal-body">
                                <div id="Div5" class="smart-form">
                                    <fieldset>
                                        <section>
                                            <div class="row">
                                                <div>
                                                    <label style="color:dodgerblue" class="input">Alias/sobrenombre <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-user"></i>
                                                        <input type="text" name="alias" runat="server" id="alias" class="alphanumeric alptext" placeholder="Alias" maxlength="246" title="Alias" />
                                                        <b class="tooltip tooltip-bottom-right">Alias</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </section>
                                    </fieldset>
                                </div>
                            </div>
                            <footer>
                                <a class="btn btn-sm btn-default" data-dismiss="modal" id="btncancelAlias"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                                
                                <a class="btn btn-sm btn-default" id="btnsaveAlias"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div id="blockitem-modalalias" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div9" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbalias"></span></strong>
                                    &nbsp;el registro de  
                                    <strong>&nbsp<span id="itemnameblockalias"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>&nbsp;
                            <a class="btn btn-sm btn-default" id="btncontinuaralias"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                            </div>
                    </div>
                        </div>                        
                    </div>        
                </div>
            
    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Descripción del evento:</label>
                            <div class="col-md-8">
                                <textarea id="motivoInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">

                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="photo-arrested" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>  #" alt="fotografía del detenido" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="photo-arrested" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>  #" alt="fotografía del detenido" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="salidaefectuada-form" class="smart-form ">
                        <div class="row">
                            <fieldset>                                
                                <section>
                                    <label class="input">Observación <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-cubes"></i>
                                        <input type="text" name="observacionSalidaEfectuada" id="observacionSalidaEfectuada" class="alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la observación.</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input">Responsable <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="responsableSalidaEfectuada" id="responsableSalidaEfectuada" class="alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el responsable.</b>
                                    </label>
                                </section>                                                           
                            </fieldset>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btnSaveSalidaEfectuada"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btnCancelSalidaEfectuada"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal-juez" tabindex="-1" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="form-modal-title-juez"></h4>
                </div>
                <div class="modal-body">
                    <div id="salidaefectuada-form-juez" class="smart-form ">
                        <div class="row">
                            <fieldset>                                
                                <section>
                                    <label style="color: dodgerblue" class="input">Fundamento <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-cubes"></i>
                                        <input type="text" name="fundamentoSalidaEfectuada" id="fundamentoSalidaEfectuada" class="alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el fundamento.</b>
                                    </label>
                                </section>  
                                <section >
                                                                    <label style="color: dodgerblue" class="label">Tipo salida <a style="color: red">*</a></label>
                                                                    <label class="select">
                                                                        <select name="centro" id="combotiposalida" >
                                                                        </select>
                                                                        <i></i>
                                                                    </label>
                                                                </section>
                            </fieldset>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btnSaveSalidaEfectuadaJuez"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btnCancelSalidaEfectuadaJuez"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal-historial" tabindex="1" role="dialog" data-backdrop="static" data-keyboard="true" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeHistorial" tabindex="2">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-historial"></h4>
                </div>
                <div class="modal-body" style="padding:0 20px">
                    <div class="smart-form ">                        
                        <div class="row">
                            <fieldset tabindex="3">
                                <table id="dt_basic_historial" class="table table-striped table-bordered table-hover" width="100%" tabindex="">
                                    <thead>
                                        <tr>                                        
                                            <th data-class="expand">#</th>
                                            <th>Movimiento</th>
                                            <th>Descripción</th>
                                            <th>Fecha</th>
                                            <th>Usuario</th>                                                                                    
                                        </tr>
                                    </thead>
                                </table>                                
                            </fieldset>
                            <footer tabindex="6">
                                <a tabindex="7" data-dismiss="modal" class="btn btn-sm btn-default cancelHistorial" id="btnCancelHistorial"><i class="fa fa-close"></i>&nbsp;Cerrar </a>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                
                </div>
                <div class="modal-body">
                    <div id="mapa" class="smart-form ">
                        <legend>Ubicación</legend>
                        <br />
                            <div id="mapid"></div>
                        <pre id='coordinates' class='coordinates'></pre>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="closeMap"><i class="fa fa-close"></i>&nbsp;Cerrar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal-examen-medico2" tabindex="-1" role="dialog" data-keyboard="true" data-backdrop="static" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-examen2"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form ">                        
                        <div class="row">
                            <fieldset>
                                <table id="dt_basic_examen2" class="table table-striped table-bordered table-hover" style="width: 100%;">                         
                                    <thead>
                                        <tr>
                                            <th>Tipo</th>
                                            <th>Fecha</th>
                                            <th>Elaborado por</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </fieldset>
                            <footer style="padding: 0px;">
                                <a href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelExamen"><i class="fa fa-close"></i>&nbsp;Cerrar </a>                                                            
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="form-modal-examen-medico" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-examen"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form ">                        
                        <div class="row">
                            <fieldset>
                                <table id="dt_basic_examen" class="table table-striped table-bordered table-hover" width="100%">                         
                                    <thead>
                                        <tr>
                                            
                                            <th>Fecha</th>
                                            <th>Elaborado por</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </fieldset>
                            <footer>
                                <a class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelExamen"><i class="fa fa-close"></i>&nbsp;Cerrar </a>                                                            
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal-eventos" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
      <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 id ="alerta1"><i class="glyphicon glyphicon-folder-open"></i>  Evento /alerta web</h4>
                </div>
                <div class="modal-body">
                    <!-- Evento local -->
                   <!--  <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Datos del evento</label>-->
                    <div class="panel-group smart-accordion-default" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a data-toggle="collapse" style="color:#3276b1; " data-parent="#accordion" href="#collapseEvento"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Datos de evento </a></h4>
							</div>
							<div id="collapseEvento" class="panel-collapse collapse">
								<div class="panel-body no-padding">
                                    <fieldset>
									    <table id="dt_basic_evento" class="table table-striped table-bordered table-hover" width="100%">
										    <thead>
											    <tr>
                                                    <th>Folio</th>
												    <th>Descripción</th>
												    <th>Fecha y hora</th>
                                                    <th>Lugar de detención</th>
											    </tr>
										    </thead>
									    </table>
                                    </fieldset>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a data-toggle="collapse" style="color:#3276b1; " data-parent="#accordion" href="#collapseLugar" class="collapsed"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de evento</a></h4>
							</div>
							<div id="collapseLugar" class="panel-collapse collapse">
								<div class="panel-body">
									<table id="dt_basic_lugar" class="table table-striped table-bordered table-hover" width="100%">
										    <thead>
											    <tr>
												    <th>Número de detenidos</th>
                                                    <th>Latitud</th>
                                                    <th>Longitud</th>
											    </tr>
										    </thead>
									    </table>
								</div>
							</div>
						</div>
					</div>
                    <br />
                    <!-- Evento de Alerta web-->
               
                    <div class="panel-group smart-accordion-default" id="accordionAW">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionAW"  style="color:#3276b1; " href="#collapseEventoAW" id="alerta2"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Alerta web </a></h4>
							</div>
							<div id="collapseEventoAW" class="panel-collapse collapse">
								<div class="panel-body no-padding">
                                    <fieldset>
									    <table id="dt_basic_evento_AW" class="table table-striped table-bordered table-hover" width="100%">
										    <thead>
											    <tr>
												    <th>Folio</th>
												    <th>Descripción</th>
												    <th>Fecha</th>
                                                    <th>Lugar</th>
												    <th>Nombre del responsable</th>
                                                    <th>Número de detenidos</th>
											    </tr>
										    </thead>
									    </table>
                                    </fieldset>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionAW" style="color:#3276b1; " href="#collapseLugarAW" class="collapsed" id="alerta3"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de alerta web</a></h4>
							</div>
							<div id="collapseLugarAW" class="panel-collapse collapse">
								<div class="panel-body">
									<table id="dt_basic_lugar_AW" class="table table-striped table-bordered table-hover" width="100%">
										    <thead>
											    <tr>
												    <th>Sector</th>
												    <th>Entre Calle</th>
												    <th>Latitud</th>
												    <th>Longitud</th>
											    </tr>
										    </thead>
									    </table>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="form-modalpertenencias" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modalpertenencias-title"></h4>
                </div>
                <div class="modal-body">
                    <a href="javascript:void(0);" class="btn btn-md btn-default" id="addRow"><i class="fa fa-plus"></i>&nbsp;Agregar nueva fila </a>
                    <div id="pertenencias-form" class="smart-form ">
                        <div class="row">
                            <fieldset>
                                <%--<section>
                                    <label class="input">Contrato</label>
                                    <select name="contrato" id="contrato" style="width: 100%" class="select2">                                        
                                    </select>
                                    <i></i>
                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                </section>--%>
                                <section>
                                    <label style="color: dodgerblue" class="input">No. remisión - detenido <a style="color: red">*</a></label>
                                    <select name="interno" id="interno" style="width: 100%" class="select2"> 
                                    </select>
                                    <i></i>
                                </section>
                                                                                              
                                <div class="row" style="width:100%;margin:0 auto;">
                                    <table id="dt_nuevas_pertenencias" width="100%" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:5% !important;"></th>
                                                <th>Fecha de entrada</th>                                                
                                                <th>Pertenencia</th>                                                
                                                <th>Observaciones</th>
                                                <%--<th>Bolsa</th>--%>
                                                <th>Cantidad</th>
                                                <th>Clasificación</th>
                                                <th>Casillero</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <br />
                                <section>
                                    <label style="color: dodgerblue" class="input">Fotografía</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-file"></i>
                                        <asp:FileUpload  name="fotografia" id="fotografia" runat="server" />                                        
                                    </label>                                    
                                </section>                                                             
                            </fieldset>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default savepertenencia" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" ><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="form-modal-tabla" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-tabla"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-tabla" class="smart-form">                        
                            <div class="row">
                                <fieldset>
                                    <div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label class="input">Pertenencia de:</label>
                                                    <label class="input">
                                                        <input type="text" id="pertenenciaDeTabla" name="pertenenciaDeTabla" class="alptext" disabled="disabled" />                                            
                                                    </label>
                                                </section>                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label class="input">Usuario que registró:</label>
                                                    <label class="input">
                                                        <input id="usuarioQueRegistro" class="alptext" name="usuarioQueRegistro" />
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>           
                                        <br />
                                        <br />
                                        <div class="row">
                                            <table id="dt_basic_tabla" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                    <tr>    
                                                        <th data-class="expand"></th>
                                                        <th>Id</th>
                                                        <th>Nombre</th>
                                                        <th data-hide="phone,tablet">Cantidad</th>
                                                        <th data-hide="phone,tablet">Estatus</th>
                                                        <th data-hide="phone,tablet">Observacion</th>
                                                        <th data-hide="phone,tablet">Clasificacion</th>
                                                        <th data-hide="phone,tablet">Bolsa</th>                                                        
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>      
                                        <br />
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label class="input">Persona que recibe: <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <input id="personaQueRecibe" class="alptext" name="personaQueRecibe" />
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>             
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label class="input">Condición de entrega: <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <textarea id="condicionDeEntrega" class="alptext" name="condicionDeEntrega" ></textarea>
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>            
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label>Entrega: <a style="color: red">*</a></label>                                                                                                                                    
                                                    <select name="entrega" id="entrega" style="width: 100%" class="select2"></select>                                            
                                                </section> 
                                            </div>
                                        </div>            
                                    </div>
                                </fieldset>
                            </div>
                            <footer style="padding: 0px;">
                                <div class="row" style="display: inline-block; float: right; margin:0px;">
                                    <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" id="btnsaveTabla"><i class="fa fa-save"></i>&nbsp;Guardar </a>                                                         
                                    <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelTabla"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                             
                                </div>
                            </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="Max" value="0" />
        <input type="hidden" id="Min" value="0" />
        <input type="hidden" id="Validar1"  />
        <input type="hidden" id="Validar2"  />
    <input type="hidden" id="Validar3"  />
        <input type="hidden" id="Validar4"  />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script> 
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">

        window.addEventListener("keydown", function (e) {
            var sectionNumber = $("#myTab .active").children().attr("id");            

            if (e.keyCode === 27 && $("#modalEditarDetenido").is(":visible")) {
                $("#modalEditarDetenido").modal('hide');
            }

            if (e.ctrlKey && e.keyCode === 27) {
                e.preventDefault();
                if ($("#add-modal").is(":visible")) {
                    $("#add-modal").modal("hide");
                }
                else if ($("#printpdf-modal").is(":visible")) {
                    $("#printpdf-modal").modal("hide");                    
                }                                
            }
            if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                if ($("#modalEditarDetenido").is(":visible")) {
                    switch (sectionNumber) {
                        case "linkTab1": document.getElementById("linkTab4").click(); break;
                        case "linkTab2": document.getElementById("linkTab1").click(); break;
                        case "linkTab3": document.getElementById("linkTab2").click(); break;
                        case "linkTab4": document.getElementById("linkTab3").click(); break;
                    }
                }

                if ($("#form-modal-historial").is(":visible")) {
                    switch (sectionNumber) {
                        
                        case "linkTab5": document.getElementById("linkTab5").click(); break;
                    }
                    
                    document.getElementById("btnCancelHistorial").click();
                }
            }
            else if (e.ctrlKey && e.keyCode === 39) {
                e.preventDefault();
                if ($("#modalEditarDetenido").is(":visible")) {
                    switch (sectionNumber) {
                        case "linkTab1": document.getElementById("linkTab2").click(); break;
                        case "linkTab2": document.getElementById("linkTab3").click(); break;
                        case "linkTab3": document.getElementById("linkTab4").click(); break;
                        case "linkTab4": document.getElementById("linkTab1").click(); break;
                    }
                }
            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                if ($("#printpdf-modal").is(":visible")) {
                    document.getElementsByClassName("printpdfbtn")[0].click();
                }
                else if ($("#form-modal-juez").is(":visible")) {
                    document.getElementById("btnSaveSalidaEfectuadaJuez").click();
                }
                else if ($("#trasladoInterno-modal").is(":visible")) {
                    document.getElementsByClassName("save")[0].click();
                }
                else if ($("#form-modalpertenencias").is(":visible")) {
                    document.getElementsByClassName("savepertenencia")[0].click();
                }
                else if ($("#modalEditarDetenido").is(":visible")) {
                    switch (sectionNumber) {
                        case "linkTab1": document.getElementById("btnSaveDetenido").click(); break;
                        case "linkTab2": document.getElementById("btnSaveGeneral").click(); break;
                        case "linkTab3": document.getElementById("btnSaveDomicilio").click(); break;
                        case "linkTab4":
                            if ($("#addAlias-modal").is(":visible")) {
                                document.getElementById("btnsaveAlias").click();
                            }
                            break;
                    }
                }
                else if ($("#form-modal-historial").is(":visible")) {
                    switch (sectionNumber) {
                        case "linkTab5": document.getElementById("btnCancelHistorial").click(); break;
                        
                            break;
                    }
                }
            }
        });

        $(document).ready(function ()
        {            

            $('body').on("keydown", function (e) {
                
                if (e.ctrlKey && e.which === 71 && $("#modalEditarDetenido").is(":visible")) {
                    console.log('activo');

                    if ($('#myTab li')[0].className === "nav-item active") {
                        //Guardar detenido
                        if (validarDetenido()) {
                            guardarImagen();
                        }
                    }
                    else if ($('#myTab li')[1].className === "nav-item active") {
                        //General
                        var estado = false;
                        var inimputable = false;

                        datos = [
                            id = $('#ctl00_contenido_idGeneral').val(),
                            tracking = $('#ctl00_contenido_trackingGeneral').val(),
                            fecha = $("#ctl00_contenido_Text1").val(),
                            rfc = $("#ctl00_contenido_rfc").val(),
                            nacionalidad = $("#ctl00_contenido_nacionalidad").val(),
                            escolaridad = $("#ctl00_contenido_escolaridad").val(),
                            religion = $("#ctl00_contenido_religion").val(),
                            ocupacion = $("#ctl00_contenido_ocupacion").val(),
                            civil = $("#ctl00_contenido_civil").val(),
                            etnia = $("#ctl00_contenido_etinia").val(),
                            sexo = $("#ctl00_contenido_sexo").val(),
                            estado = estado,
                            inimputable = inimputable,
                            interno = $('#ctl00_contenido_trackingidInterno').val(),
                            CURP = $("#ctl00_contenido_CURP").val(),
                            LenguanativaId = $("#lenguanativa").val(),
                            Personanotifica = $("#personanotifica").val(),
                            Celulartifica = $("#celular").val(),
                        ];

                        if (validarGeneral()) {
                            SaveGeneral(datos);
                        }
                    }
                    else if ($('#myTab li')[2].className === "nav-item active") {
                        //Domicilio
                        datos = [
                            id = $('#ctl00_contenido_idDomicilio').val(),
                            trackingd = $('#ctl00_contenido_trackingDomicilio').val(),
                            called = $('#ctl00_contenido_calledomicilio').val(),
                            numerod = $('#ctl00_contenido_numerodomicilio').val(),
                            //coloniad = $("#ctl00_contenido_coloniadomicilio").val(),
                            //cpd = $("#ctl00_contenido_cpdomicilio").val(),
                            telefonod = $("#ctl00_contenido_telefonodomicilio").val(),
                            paisd = $("#ctl00_contenido_paisdomicilio").val(),
                            estadod = $("#ctl00_contenido_estadodomicilio").val(),
                            municipiod = $("#ctl00_contenido_municipiodomicilio").val(),
                            interno = $('#trackingDetenido').val(),
                            idn = $('#ctl00_contenido_idNacimiento').val(),
                            trackingn = $('#ctl00_contenido_trackingNacimiento').val(),
                            callen = $("#ctl00_contenido_callelugar").val(),
                            numeron = $("#ctl00_contenido_numerolugar").val(),
                            //colonian = $("#ctl00_contenido_colonialugar").val(),
                            //cpn = $("#ctl00_contenido_cplugar").val(),
                            telefonon = $("#ctl00_contenido_telefonolugar").val(),
                            paisn = $('#ctl00_contenido_paisnacimineto').val(),
                            estadon = $("#ctl00_contenido_estadonacimiento").val(),
                            municipion = getmunicipioid(),
                            interno = $('#trackingDetenido').val(),
                            localidad = $('#ctl00_contenido_localidad').val(),
                            localidan = $('#ctl00_contenido_localidadn').val(),
                            coloniaId = $("#ctl00_contenido_coloniaSelect").val(),
                            coloniaIdNacimiento = getmunicolnid(),
                            latituddomicilio = $("#latitud").val(),
                            longituddomicilio = $("#longitud").val()
                        ];

                        if (validarDN()) {
                            SaveDN(datos);
                        }
                    }
                    else if ($('#myTab li')[3].className === "nav-item active") {
                        if ($("#addAlias-modal").is(":visible")) {
                            var id = $("#btnsaveAlias").attr("data-id");
                            var tracking = $("#btnsaveAlias").attr("data-tracking");
                            if (validarAlias()) {
                                datos = [
                                    id = id,
                                    tracking = tracking,
                                    alias = $('#ctl00_contenido_alias').val(),
                                    interno = $('#trackingDetenido').val()
                                ];

                                SaveAlias(datos);
                            }
                        }                        
                    }
                }
            });

            $(document).keypress(function (eve) {                
                if (eve.keyCode === 13 && $("#form-modal-historial").is(':visible') && $("#btnCancelHistorial").is(':focus')) {
                    $("#form-modal-historial").modal('hide');
                }
            });

            $("#form-modal-historial").on("shown.bs.modal", function () {
                $("#form-modal-historial input[type='search']").attr('tabindex', '4');
                $("select[name=dt_basic_historial_length]").attr('tabindex', '5');
            });

            $("#form-modal-examen-medico2").on("shown.bs.modal", function () {
                $("#form-modal-examen-medico2 input[type='search']")[0].focus();
            });

            //$("#idAnioDetenidos").change(function () {
            //    startLoading();
            //    window.table.api().ajax.reload();
            //});

            function getYearsModal(val) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getYearsModal",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $("#filterYear");
                        Dropdown.append(new Option("", ""));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (val !== "") {
                            Dropdown.val(val).trigger('change.select2');
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de años. Si el problema persiste, consulta al personal de soporte técnico.");
                    }
                });
            }

            var fecha = new Date();
            var anio = fecha.getFullYear();
            loadDetenidosPorAnio(anio);
            //$("#idAnioDetenidos").val(anio);
            //rep4ir
            //getalerta();
            function getalerta() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/GetAlertaWeb",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.d != null) {
                            data = data.d;

                            var alerta1 = data.Denominacion;
                            var alerta2 = data.AlertaWerb;

                            //$("#Denominacin").val(data.Denominacion);
                            
                            $("#linkEvento").html('<i class="glyphicon glyphicon-folder-open"></i>&nbsp; '+alerta1+' / evento');
                            $("#alerta1").html('<i class="glyphicon glyphicon-folder-open"></i>  Evento / ' + alerta2);
                            $("#alerta2").html('<i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>' + alerta1);
                            $("#alerta3").html(' <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de '+alerta2);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error! ", "No fue posible cargar la información de alerta web. Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
            }

            $("#ctl00_contenido_fotografia").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar3").val(false);
                    }
                    else {
                        $("#Validar3").val(true);
                    }
                }
                else {
                    $("#Validar3").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar4").val(false);
                    }
                    else {
                        $("#Validar4").val(true);
                    }
                }
                else {
                    $("#Validar4").val(true);
                }



            });


            $("#ctl00_contenido_imagenDetenido").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar1").val(false);
                    }
                    else {
                        $("#Validar1").val(true);
                    }
                }
                else {
                    $("#Validar1").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar2").val(false);
                    }
                    else {
                        $("#Validar2").val(true);
                    }
                }
                else {
                    $("#Validar2").val(true);
                }



            });


            GetMinMax();

            function GetMinMax() {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/GetParametrosTamaño",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var max = resultado.TMax;
                            var min = resultado.Tmin;

                            $("#Max").val(max);
                            $("#Min").val(min);
                            // alert(resultado.rutaimagenfrontal);
                            //var imagenAvatar = ResolveUrl(resultado.rutaimagenfrontal);
                            //  imagenfrontalbiometricos = imagenAvatar;

                            //$('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            //$("#ctl00_contenido_fileUpload1").val(imagenAvatar).clone(true);
                            $('#main').waitMe('hide');
                            //location.reload(true);
                            //CargarDatos(datos.TrackingId);



                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! No fue posible cargar los parametros. Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }


            function loadDetenidosPorAnio(setvalor) {
                $.ajax({                    
                    type: "POST",
                    url: "sentence_entrylist.aspx/LoadDetenidosAniosTrabajo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idAnioDetenidos');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Seleccione año]", "0"));

                        if (response.d.length > 0) {
                            setvalue = response.d[0].Id;
                        }

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalor != "") {
                            Dropdown.val(setvalor);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error! ", "Ocurrió un error. No fue posible cargar la lista de estados. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
                
            $("#detenidoEditar").on("error", function () {
                $(this).attr("src", rutaDefaultServer);
            });

            $("#ctl00_contenido_rfc").focusout(function () {
            }).blur(function () {
                var curp10 = $("#ctl00_contenido_rfc").val();
                $("#ctl00_contenido_CURP").val(curp10.substring(0, 10));
            });

            var rutaDefaultServer = "";

            var responsiveHelper_dt_pertenencias_nuevas = undefined;

            getRutaDefaultServer();
            CarjarfiltroJuezCalificador("0");

            $("body").on("click", ".entregarpertenencia", function () {
                var dataArreglo = new Array();
                var tracking = "";
                var estatus = 0;
                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();
                        tracking = data.TrackingId;
                        dataArreglo.push(data.Id);
                        estatus = data.Estatus;
                    }
                });
                var detenidoId = dataArreglo[0];

                if (parseInt(dataArreglo.length) == 0) {
                    ShowAlert("¡Alerta!", "Seleccione un registro.");
                    return;
                }

                if (parseInt(dataArreglo.length) > 1) {
                    ShowAlert("¡Alerta!", "Seleccione solo un registro.");
                    return;
                }

                var redirect = "";
                redirect = " <%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Control_Pertenencias/control_pertenencias.aspx?tracking=" + tracking + "&valor=" + detenidoId + "&status=" + estatus;
                window.location.assign(redirect);
            });

            $('#interno').on('select2:opening', function (evt) {
                if (this.disabled) {
                    return false;
                }
            });

            $("body").on("click", ".recibirpertenencia", function () {
                var dataArreglo = new Array();

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();

                        dataArreglo.push(data.Id);
                    }
                });

                if (parseInt(dataArreglo.length) == 0) {
                    ShowAlert("¡Alerta!", "Seleccione un registro.");
                    return;
                }

                if (parseInt(dataArreglo.length) > 1) {
                    ShowAlert("¡Alerta!", "Seleccione solo un registro.");
                    return;
                }

                var detenidoId = dataArreglo[0];

                loadTablePertenenciasComunes(detenidoId);
                $("#interno").prop("disabled", true);

                $("#ctl00_contenido_fotografia").val(null);
                loadInternos(detenidoId, detenidoId);
                //loadClasificacion("0");
                //loadCasilleros("0");
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#form-modal-title").empty();
                $('#habilitado').prop('checked', false);
                $("#form-modalpertenencias-title").html('<i class="fa fa-pencil" +=""></i> Agregar registro');

                $("#form-modalpertenencias").modal("show");
            });

            function loadInternos(id, setvalue) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getInternos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    scrollY: "100%",
                    scrollX:"0%",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $('#interno');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Interno]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error! ", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadTablePertenenciasComunes(detenidoid) {
                $.ajax({                    
                    type: "POST",
                    url: "sentence_entrylist.aspx/getPertenenciasComunes",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        interno: detenidoid
                    }),
                    cache: false,
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        responsiveHelper_dt_pertenencias_nuevas = undefined;
                        $("#dt_nuevas_pertenencias").DataTable().destroy();
                        createTable(resultado.list);                        
                    },
                    error: function () {
                        ShowError("¡Error! ", "Ocurrió un error. No fue posible cargar la lista de pertenencias comunes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function createTable(data) {
                $('#dt_nuevas_pertenencias').dataTable({
                    "lengthMenu": [5, 10, 20, 50],
                    iDisplayLength: 5,
                    serverSide: false,
                    paging: true,
                    retrieve: true,


                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_pertenencias_nuevas) {
                            responsiveHelper_dt_pertenencias_nuevas = new ResponsiveDatatablesHelper($('#dt_nuevas_pertenencias'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_pertenencias_nuevas.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_pertenencias_nuevas.respond();
                        $('#dt_nuevas_pertenencias').waitMe('hide');
                    },
                    data: data,
                    columns: [
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        {
                            data: "Nombre",
                            visible: false
                        },
                        {
                            data: "Observacion",
                            visible: false
                        },
                        {
                            data: "Cantidad",
                            visible: false
                        },
                        {
                            data: "Casillero",
                            visible: false
                        },
                        {
                            data: "Clasificacion",
                            visible: false
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Id != null && row.TrackingId != null) {
                                    return "<input type='checkbox' data-tracking='" + row.TrackingId + "' data-id='" + row.Id + "' data-foto='" + row.Fotografia + "' />";
                                }
                                else {
                                    return "<input type='checkbox' data-tracking='' data-id='' data-foto='' />";
                                }
                            }
                        },
                        {
                            targets: 1,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.Fecha != null) {
                                    return "<input type='text' class='alptext' value='" + row.Fecha + "' style='width:100%' disabled='disabled' />";
                                }
                                else {
                                    return "<input type='text' class='alptext' value='' style='width:100%' disabled='disabled' />";
                                }
                            }
                        },
                        {
                            targets: 2,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Nombre != null) {
                                    return "<input type='text' class='alptext' value='" + row.Nombre + "' style='width:100%' data-required='true' />";
                                }
                                else {
                                    return "<input type='text' class='alptext' value='' style='width:100%' data-required='true' />";
                                }
                            }
                        },
                        {
                            targets: 3,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Observacion != null) {
                                    return "<textarea class='alptext' style='width:100%' data-required='true'>" + row.Observacion + "</textarea > ";
                                }
                                else {
                                    return "<textarea class='alptext' style='width:100%' data-required='true'></textarea > ";
                                }
                            }
                        },
                        {
                            targets: 4,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Cantidad != null) {
                                    return "<input type='number' class='alptext' name='bolsaTabla' min='1' style='width:100%' value='" + row.Cantidad + "' data-required='true' /> ";
                                }
                                else {
                                    return "<input type='number' class='alptext' name='bolsaTabla' min='1' style='width:100%' value='' data-required='true' /> ";
                                }
                            }
                        },

                        {
                            targets: 5,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                var datos = cargarClasificaciones();
                                var options = "";

                                options += "<option value='0'>[Clasificación]</option>";

                                for (var i = 0; i < datos.length; i++) {
                                    if (parseInt(datos[i].Id) === row.ClasificacionId) {
                                        options += "<option value='" + datos[i].Id + "' selected='selected' >" + datos[i].Desc + "</option>";
                                    }
                                    else {
                                        options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                    }
                                }
                                return "<select style='width:100%' data-required='true'>" + options + "</select>";
                            }
                        },
                        {
                            targets: 6,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                var datos = cargarCasilleros();
                                var options = "";

                                options += "<option value='0'>[Casillero]</option>";

                                for (var i = 0; i < datos.length; i++) {
                                    if (parseInt(datos[i].Id) === row.CasilleroId) {
                                        options += "<option value='" + datos[i].Id + "' selected='selected' >" + datos[i].Desc + "</option>";
                                    }
                                    else {
                                        if (row.CasilleroId != 0 && row.CasilleroId != undefined) {
                                            if (datos[i + 1] != undefined) {
                                                if (row.CasilleroId > datos[i].Id && row.CasilleroId < datos[i + 1].Id) {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                    options += "<option value='" + row.CasilleroId + "'selected='selected'>" + row.Casillero + "</option>";
                                                }
                                                else {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                }
                                            }
                                            else {
                                                if (row.CasilleroId > datos[i].Id) {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                    options += "<option value='" + row.CasilleroId + "'selected='selected'>" + row.Casillero + "</option>";
                                                }
                                                else {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                }
                                            }
                                        }
                                        else {
                                            options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                        }
                                    }
                                }

                                return "<select style='width:100%' data-required='true'>" + options + "</select>";
                            }
                        },
                        {
                            targets: 7,
                            render: function (data, type, row, meta) {
                                if (row.Nombre == null) {
                                    return "";
                                }
                                else {
                                    return row.Nombre;
                                }
                            }
                        },
                        {
                            targets: 8,
                            render: function (data, type, row, meta) {
                                if (row.Observacion == null) {
                                    return "";
                                }
                                else {
                                    return row.Observacion;
                                }
                            }
                        },
                        {
                            targets: 9,
                            render: function (data, type, row, meta) {
                                if (row.Cantidad == null) {
                                    return "";
                                }
                                else {
                                    return row.Cantidad;
                                }
                            }
                        },
                        {
                            targets: 10,
                            render: function (data, type, row, meta) {
                                if (row.Casillero == null) {
                                    return "";
                                }
                                else {
                                    return row.Casillero;
                                }
                            }
                        },
                        {
                            targets: 11,
                            render: function (data, type, row, meta) {
                                if (row.Clasificacion == null) {
                                    return "";
                                }
                                else {
                                    return row.Clasificacion;
                                }
                            }
                        }
                    ],
                    "fnRowCallback": function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                        if (data.TrackingId != "" && data.TrackingId != null) {
                            $('td', nRow).css('background-color', '#DDF4C2');
                        }
                    }
                });

                $("#dt_nuevas_pertenencias").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                ]).draw(false);
            }

            $("#addRow").click(function () {
                $("#dt_nuevas_pertenencias").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                ]).draw(false);
            });

            var dataClasificaciones = null;
            var dataCasilleros = null;
            var datosClasificaciones = [];
            var datosCasilleros = [];

            function cargarCasilleros() {
                if (dataCasilleros === null && datosCasilleros.length === 0) {

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "sentence_entrylist.aspx/getCasilleros",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        "scrollY": "100%",
                        "scrollX": "0%",
                        cache: false,
                        success: function (response) {
                            dataCasilleros += "<option></option>"
                            $.each(response.d, function (index, item) {
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;
                                datosCasilleros.push(tt);
                                dataCasilleros += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                            });
                        },
                        error: function () {
                            ShowError("¡Error! ", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                return datosCasilleros;
            }

            function cargarClasificaciones() { 
                if (dataClasificaciones == null && datosClasificaciones.length === 0) {                    
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "sentence_entrylist.aspx/getClasificacion",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", 
                        "scrollY": "100%",
                    "scrollX": "0%",
                        cache: false,
                        success: function (response) {
                            dataClasificaciones += "<option></option>";
                            
                            $.each(response.d, function (index, item) {
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;
                                datosClasificaciones.push(tt);
                                dataClasificaciones += "<option value=" + item.Id + ">" + item.Desc + "</option>";                                
                            });
                        },
                        error: function () {
                            
                            ShowError("¡Error! ", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");
                            
                        }
                    });
                }
                return datosClasificaciones;
            }

            function Cargatiposalida(set) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/GetTipoSalida_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({
                        item: set
                    }),
                    success: function (response) {
                        var Dropdown = $("#combotiposalida");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Tipo salida]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function Cargatiposalida2(set) {
               $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/GetTipoSalida_Combo2",
                    contentType: "application/json; charset=utf-8",
                   dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({
                        item: set
                    }),
                    success: function (response) {
                        var Dropdown = $("#combotiposalida2");
                        Dropdown.empty();
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            Cargatiposalida("0");
            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }
                    }
                });
            }

            $("#btnsaveAlias").click(function () {
                var id = $("#btnsaveAlias").attr("data-id");
                var tracking = $("#btnsaveAlias").attr("data-tracking");
                if (validarAlias()) {
                    datos = [
                        id = id,
                        tracking = tracking,
                        alias = $('#ctl00_contenido_alias').val(),
                        interno = $('#trackingDetenido').val()
                    ];

                    SaveAlias(datos);
                }
            });

            function SaveAlias(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/saveAlias",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            clearModalDetenido();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El alias se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            $("#addAlias-modal").modal("hide");
                            ShowSuccess("¡Bien hecho!", "El alias se " + resultado.mensaje + " correctamente.");
                            $('#dt_basicAlias').DataTable().ajax.reload();                            
                        }
                        else {
                            ShowError("¡Error! Algo salió mal. -Don´t save alias. ", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal. Don´t save alias. ", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("body").on("click", ".blockitemalias", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("data-verb");
                $("#itemnameblockalias").text(itemnameblock);
                $("#verbalias").text(verb);
                $("#btncontinuaralias").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modalalias").modal("show");
            });

            function validarAlias() {
                var esvalido = true;

                if ($("#ctl00_contenido_alias").val() == null || $("#ctl00_contenido_alias").val().split(" ").join("") == "") {
                    ShowError("Alias", "El alias es obligatorio.");
                    $('#ctl00_contenido_alias').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_alias').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_alias').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_alias').addClass('valid');
                }

                return esvalido;
            }

            $("#addAlias").click(function () {                
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_alias").val("");
                $("#btnsaveAlias").attr("data-id", "");
                $("#btnsaveAlias").attr("data-tracking", "");
                $("#modal_title_alias").empty();
                $("#modal_title_alias").html('<i class="fa fa-pencil" +=""></i> Agregar alias/sobrenombre');
                $("#addAlias-modal").modal("show");
            });

            $("body").on("click", ".editAlias", function () { 
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                $("#btnsaveAlias").attr("data-id", id);
                $("#btnsaveAlias").attr("data-tracking", tracking);

                $("#modal_title_alias").empty();
                $("#modal_title_alias").html("Editar alias/sobrenombre");
                $("#ctl00_contenido_alias").val($(this).attr("data-alias"));
                $("#addAlias-modal").modal("show");
            });

            $("#btncontinuaralias").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");                
                $.ajax({
                    url: "sentence_entrylist.aspx/blockitemalias",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {                        
                        if (JSON.parse(data.d).exitoso) {

                            $("#dt_basicAlias").DataTable().ajax.reload();
                            $("#blockitem-modalalias").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Bien hecho! </strong>" +
                                JSON.parse(data.d).mensaje + ".</div>");
                            ShowSuccess("¡Bien hecho! ", JSON.parse(data.d).mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }                   
                    }
                });                
            });

            var responsiveHelper_dt_basicAlias = undefined;
            var responsiveHelper_dt_basicExamen = undefined;

            function loadAlias(tracking) {
                $('#dt_basicAlias').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                    autoWidth: true,
                    destroy: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basicAlias) {
                        responsiveHelper_dt_basicAlias = new ResponsiveDatatablesHelper($('#dt_basicAlias'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basicAlias.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basicAlias.respond();
                    $('#dt_basicAlias').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');


                    }
                },
                ajax: {
                    type: "POST",
                    url: "sentence_entrylist.aspx/getAlias",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basicAlias').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });                        

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = tracking;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Alias",
                        data: "Alias"
                    },
                    null
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "editAlias";
                            var editar = "";
                            var habilitar = "";

                            if (row.Activo) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a style="padding-left: 8px;" class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-alias = "' + row.Alias + '" data-tracking="' + row.TrackingId + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitemalias" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-value = "' + row.Alias + '" data-verb = "' + txtestatus + '" title="' + txtestatus + '" style="padding-left: 8px; "><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return editar + habilitar;


                            ;
                        }
                    }
                ]
                });
            }

            $("#showMap").click(function () {
                if ($("#latitud").val() == "") {
                    $.ajax({
                        type: "POST",
                        url: "sentence_entrylist.aspx/getpositiobycontract",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processdata: true,
                        traditional: true,
                        data: JSON.stringify({ LlamadaId: "" }),
                        cache: false,
                        success: function (response) {
                            response = JSON.parse(response.d);

                            if (response.exitoso) {
                                CargarMapa(response.obj.Latitud, response.obj.Longitud);
                            }
                            else {

                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }
                else
                {
                    CargarMapa($("#latitud").val(), $("#longitud").val());
                }
                $("#form-modal-title-agregar").empty();
                $("#form-modal-title-agregar").html("Agregar");
                $("#mapModal").modal("show");
            });

            $("#btnSaveDomicilio").click(function () {
                datos = [
                    id = $('#ctl00_contenido_idDomicilio').val(),
                    trackingd = $('#ctl00_contenido_trackingDomicilio').val(),
                    called = $('#ctl00_contenido_calledomicilio').val(),
                    numerod = $('#ctl00_contenido_numerodomicilio').val(),
                    //coloniad = $("#ctl00_contenido_coloniadomicilio").val(),
                    //cpd = $("#ctl00_contenido_cpdomicilio").val(),
                    telefonod = $("#ctl00_contenido_telefonodomicilio").val(),
                    paisd = $("#ctl00_contenido_paisdomicilio").val(),
                    estadod = $("#ctl00_contenido_estadodomicilio").val(),
                    municipiod = $("#ctl00_contenido_municipiodomicilio").val(),
                    interno = $('#trackingDetenido').val(),
                    idn = $('#ctl00_contenido_idNacimiento').val(),
                    trackingn = $('#ctl00_contenido_trackingNacimiento').val(),
                    callen = $("#ctl00_contenido_callelugar").val(),
                    numeron = $("#ctl00_contenido_numerolugar").val(),
                    //colonian = $("#ctl00_contenido_colonialugar").val(),
                    //cpn = $("#ctl00_contenido_cplugar").val(),
                    telefonon = $("#ctl00_contenido_telefonolugar").val(),
                    paisn = $('#ctl00_contenido_paisnacimineto').val(),
                    estadon = $("#ctl00_contenido_estadonacimiento").val(),
                    municipion = getmunicipioid(),
                    interno = $('#trackingDetenido').val(),
                    localidad = $('#ctl00_contenido_localidad').val(),
                    localidan = $('#ctl00_contenido_localidadn').val(),
                    coloniaId = $("#ctl00_contenido_coloniaSelect").val(),
                    coloniaIdNacimiento = getmunicolnid(),
                    latituddomicilio = $("#latitud").val(),
                    longituddomicilio = $("#longitud").val()
                ];

                if (validarDN()) {
                    SaveDN(datos);
                }
            });

            function getmunicipioid() {
                if ($("#ctl00_contenido_municipionacimiento").val() == null) {
                    return 0
                }
                else {

                    return $("#ctl00_contenido_municipionacimiento").val();
                }
            }

            function getmunicolnid() {
                if ($("#ctl00_contenido_coloniaSelectNacimiento").val() == null) {
                    return 0
                }
                else {
                    return $("#ctl00_contenido_coloniaSelectNacimiento").val();
                }
            }

            function SaveDN(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/saveDN",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                            clearModalDetenido();
                            $('#<%= trackingDomicilio.ClientID %>').val(resultado.TrackingIddomicilio);
                            $('#<%= idDomicilio.ClientID %>').val(resultado.Iddomicilio);
                            $('#<%= trackingNacimiento.ClientID %>').val(resultado.TrackingIdnacimiento);
                            $('#<%= idNacimiento.ClientID %>').val(resultado.Idnacimiento);                            
                        }
                        else {
                            ShowError("¡Error! Algo salió mal. -Don´t save dn. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }                        
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal. Don´t save dn. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function CargarCodigoPostalDomicilio(idColonia) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getZipCode",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({
                        idColonia: idColonia
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        $("#ctl00_contenido_cpdomicilio").val(resultado.cp);
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarCodigoPostalNacimiento(idColonia) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getZipCode",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({
                        idColonia: idColonia
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        $("#ctl00_contenido_cplugar").val(resultado.cp);
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEstado(setestado, combo, paisid) {
                $(combo).empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getEstado",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    data: JSON.stringify({ paisId: 73 }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Estado]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setestado != "" && setestado != null) {
                            Dropdown.val(setestado);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de estados. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarMunicipio(set, combo, estadoid) {
                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "sentence_entrylist.aspx/getMunicipio",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ estadoId: estadoid }),
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Municipio]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de municipio. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            
            function CargarPais(set, combo) {
                $(combo).empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getPais",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[País]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarColonia(set, idMunicipio) {
                console.log(set + " " + idMunicipio);
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#ctl00_contenido_coloniaSelect");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarColoniaNacimiento(set, idMunicipio) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#ctl00_contenido_coloniaSelectNacimiento");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function validarDN() {
                var esvalido = true;

                if ($("#ctl00_contenido_paisdomicilio option:selected").val() == 73) {
                    if ($("#ctl00_contenido_calledomicilio").val() == null || $("#ctl00_contenido_calledomicilio").val().split(" ").join("") == "") {
                        ShowError("Calle domicilio", "La calle es obligatoria.");
                        $('#ctl00_contenido_calledomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_calledomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_calledomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_calledomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_estadodomicilio").val() == null || $("#ctl00_contenido_estadodomicilio").val().split(" ").join("") == "") {
                        ShowError("Estado domicilio", "El estado es obligatorio.");
                        $('#ctl00_contenido_estadodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_estadodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_estadodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_estadodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_municipiodomicilio").val() == null || $("#ctl00_contenido_municipiodomicilio").val().split(" ").join("") == "") {
                        ShowError("Municipio domicilio", "El municipio es obligatorio.");
                        $('#ctl00_contenido_municipiodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_municipiodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_municipiodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_municipiodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_numerodomicilio").val() == null || $("#ctl00_contenido_numerodomicilio").val().split(" ").join("") == "") {
                        ShowError("Número domicilio", "El número es obligatorio.");
                        $('#ctl00_contenido_numerodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_numerodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_numerodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_numerodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_coloniaSelect").val() == null || $("#ctl00_contenido_coloniaSelect").val().split(" ").join("") == "") {
                        ShowError("Colonia domicilio", "La colonia actual es obligatoria.");
                        $('#ctl00_contenido_coloniaSelect').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_coloniaSelect').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_coloniaSelect').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_coloniaSelect').addClass('valid');
                    }

                    if ($("#ctl00_contenido_cpdomicilio").val() == null || $("#ctl00_contenido_cpdomicilio").val().split(" ").join("") == "") {
                        ShowError("C.P. del domicilio", "El C.P. es obligatorio.");
                        $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_cpdomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        if (isNaN($("#ctl00_contenido_cpdomicilio").val())) {
                            ShowError("C.P. del domicilio", "El C.P. es numérico.");
                            $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success').addClass("state-error");
                            $('#ctl00_contenido_cpdomicilio').removeClass('valid');
                            esvalido = false;
                        }
                        else {
                            $('#ctl00_contenido_cpdomicilio').parent().removeClass("state-error").addClass('state-success');
                            $('#ctl00_contenido_cpdomicilio').addClass('valid');
                        }
                    }

                    if ($("#ctl00_contenido_telefonodomicilio").val() == null || $("#ctl00_contenido_telefonodomicilio").val().split(" ").join("") == "") {
                        ShowError("Teléfono domicilio", "El teléfono es obligatorio.");
                        $('#ctl00_contenido_telefonodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_telefonodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_telefonodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_telefonodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_paisdomicilio").val() == null || $("#ctl00_contenido_paisdomicilio").val() == 0) {
                        ShowError("País domicilio", "El país es obligatorio.");
                        $('#ctl00_contenido_paisdomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_paisdomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_paisdomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_paisdomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_estadodomicilio").val() == null || $("#ctl00_contenido_estadodomicilio").val() == 0) {
                        ShowError("Estado", "El estado es obligatorio.");
                        $('#ctl00_contenido_estadodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_estadodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_estadodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_estadodomicilio').addClass('valid');
                    }

                    //if ($("#ctl00_contenido_localidad").val() == null || $("#ctl00_contenido_localidad").val().split(" ").join("") == "") {
                    //      alert("jola");
                    //    ShowError("Teléfono domicilio", "El teléfono es obligatorio.");
                    //    $('#ctl00_contenido_localidad').parent().removeClass('state-success').addClass("state-error");
                    //    $('#ctl00_contenido_localidad').removeClass('valid');
                    //    esvalido = false;
                    //}
                    //else {
                    //    $('#ctl00_contenido_localidad').parent().removeClass("state-error").addClass('state-success');
                    //    $('#ctl00_contenido_localidad').addClass('valid');
                    //}
                }
                else {
                    if ($("#ctl00_contenido_calledomicilio").val() == null || $("#ctl00_contenido_calledomicilio").val().split(" ").join("") == "") {
                        ShowError("Calle domicilio", "La calle es obligatoria.");
                        $('#ctl00_contenido_calledomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_calledomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_calledomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_calledomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_numerodomicilio").val() == null || $("#ctl00_contenido_numerodomicilio").val().split(" ").join("") == "") {
                        ShowError("Número domicilio", "El número es obligatorio.");
                        $('#ctl00_contenido_numerodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_numerodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_numerodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_numerodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_cpdomicilio").val() == null || $("#ctl00_contenido_cpdomicilio").val().split(" ").join("") == "") {
                        ShowError("C.P. del domicilio", "El C.P. es obligatorio.");
                        $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_cpdomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        if (isNaN($("#ctl00_contenido_cpdomicilio").val())) {
                            ShowError("C.P. del domicilio", "El C.P. es numérico.");
                            $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success').addClass("state-error");
                            $('#ctl00_contenido_cpdomicilio').removeClass('valid');
                            esvalido = false;
                        }
                        else {
                            $('#ctl00_contenido_cpdomicilio').parent().removeClass("state-error").addClass('state-success');
                            $('#ctl00_contenido_cpdomicilio').addClass('valid');
                        }
                    }

                    if ($("#ctl00_contenido_telefonodomicilio").val() == null || $("#ctl00_contenido_telefonodomicilio").val().split(" ").join("") == "") {
                        ShowError("Teléfono domicilio", "El teléfono es obligatorio.");
                        $('#ctl00_contenido_telefonodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_telefonodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_telefonodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_telefonodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_paisdomicilio").val() == null || $("#ctl00_contenido_paisdomicilio").val() == 0) {
                        ShowError("País domicilio", "El país es obligatorio.");
                        $('#ctl00_contenido_paisdomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_paisdomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_paisdomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_paisdomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_localidad").val() == null || $("#ctl00_contenido_localidad").val().split(" ").join("") == "") {
                        ShowError("Localidad domicilio", "La localidad es obligatoria.");
                        $('#ctl00_contenido_localidad').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_localidad').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_localidad').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_localidad').addClass('valid');
                    }
                }


                //Si el pais seleccionado es mexico para nacimiento
                //if ($("#ctl00_contenido_paisnacimineto option:selected").val() == 73) {
                //    if ($("#ctl00_contenido_callelugar").val() == null || $("#ctl00_contenido_callelugar").val().split(" ").join("") == "") {
                //        ShowError("Calle nacimiento", "La calle es obligatoria.");
                //        $('#ctl00_contenido_callelugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_callelugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_callelugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_callelugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_numerolugar").val() == null || $("#ctl00_contenido_numerolugar").val().split(" ").join("") == "") {
                //        ShowError("Número nacimiento", "El número es obligatorio.");
                //        $('#ctl00_contenido_numerolugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_numerolugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_numerolugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_numerolugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_coloniaSelectNacimiento").val() == null || $("#ctl00_contenido_coloniaSelectNacimiento").val().split(" ").join("") == "") {
                //        ShowError("Colonia nacimiento", "La colonia de nacimiento es obligatoria.");
                //        $('#ctl00_contenido_coloniaSelectNacimiento').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_coloniaSelectNacimiento').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_coloniaSelectNacimiento').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_coloniaSelectNacimiento').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_cplugar").val() == null || $("#ctl00_contenido_cplugar").val().split(" ").join("") == "") {
                //        ShowError("C.P. nacimiento", "El C.P. es obligatorio.");
                //        $('#ctl00_contenido_cplugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_cplugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        if (isNaN($("#ctl00_contenido_cplugar").val())) {
                //            ShowError("C.P. nacimiento", "El C.P. es numérico.");
                //            $('#ctl00_contenido_cplugar').parent().removeClass('state-success').addClass("state-error");
                //            $('#ctl00_contenido_cplugar').removeClass('valid');
                //            esvalido = false;

                //        } else {
                //            $('#ctl00_contenido_cplugar').parent().removeClass("state-error").addClass('state-success');
                //            $('#ctl00_contenido_cplugar').addClass('valid');
                //        }
                //    }

                //    if ($("#ctl00_contenido_telefonolugar").val() == null || $("#ctl00_contenido_telefonolugar").val().split(" ").join("") == "") {
                //        ShowError("Teléfono nacimiento", "El teléfono es obligatorio.");
                //        $('#ctl00_contenido_telefonolugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_telefonolugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_telefonolugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_telefonolugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_paisnacimineto").val() == null || $("#ctl00_contenido_paisnacimineto").val() == 0) {
                //        ShowError("País nacimiento", "El país es obligatorio.");
                //        $('#ctl00_contenido_paisnacimineto').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_paisnacimineto').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_paisnacimineto').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_paisnacimineto').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_estadonacimiento").val() == null || $("#ctl00_contenido_estadonacimiento").val() == 0 || $("#ctl00_contenido_estadonacimiento").val() == "Seleccione") {
                //        ShowError("Estado nacimiento", "El estado es obligatorio.");
                //        $('#ctl00_contenido_estadonacimiento').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_estadonacimiento').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_estadonacimiento').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_estadonacimiento').addClass('valid');
                //    }


                //    if ($("#ctl00_contenido_municipionacimiento").val() == null || $("#ctl00_contenido_municipionacimiento").val() == 0) {
                //        ShowError("Municipio nacimiento", "El municipio es obligatorio.");
                //        $('#ctl00_contenido_municipionacimiento').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_municipionacimiento').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_municipionacimiento').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_municipionacimiento').addClass('valid');
                //    }

                //    //if ($("#ctl00_contenido_localidadn").val() == null || $("#ctl00_contenido_localidadn").val().split(" ").join("") == "") {
                //    //    ShowError("Teléfono domicilio", "El teléfono es obligatorio.");
                //    //    $('#ctl00_contenido_localidadn').parent().removeClass('state-success').addClass("state-error");
                //    //    $('#ctl00_contenido_localidadn').removeClass('valid');
                //    //    esvalido = false;
                //    //}
                //    //else {
                //    //    $('#ctl00_contenido_localidadn').parent().removeClass("state-error").addClass('state-success');
                //    //    $('#ctl00_contenido_localidadn').addClass('valid');
                //    //}

                //} //Si se ha seleccionado un pais diferente a mexico
                //else {
                //    if ($("#ctl00_contenido_callelugar").val() == null || $("#ctl00_contenido_callelugar").val().split(" ").join("") == "") {
                //        ShowError("Calle nacimiento", "La calle es obligatoria.");
                //        $('#ctl00_contenido_callelugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_callelugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_callelugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_callelugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_numerolugar").val() == null || $("#ctl00_contenido_numerolugar").val().split(" ").join("") == "") {
                //        ShowError("Número nacimiento", "El número es obligatorio.");
                //        $('#ctl00_contenido_numerolugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_numerolugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_numerolugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_numerolugar').addClass('valid');
                //    }

                //    //if ($("#ctl00_contenido_cplugar").val() == null || $("#ctl00_contenido_cplugar").val().split(" ").join("") == "") {
                //    //    ShowError("C.P. nacimiento", "El C.P. es obligatorio.");
                //    //    $('#ctl00_contenido_cplugar').parent().removeClass('state-success').addClass("state-error");
                //    //    $('#ctl00_contenido_cplugar').removeClass('valid');
                //    //    esvalido = false;
                //    //}
                //    //else {
                //    //    if (isNaN($("#ctl00_contenido_cplugar").val())) {
                //    //    ShowError("C.P. nacimiento", "El C.P. es numérico.");
                //    //    $('#ctl00_contenido_cplugar').parent().removeClass('state-success').addClass("state-error");
                //    //    $('#ctl00_contenido_cplugar').removeClass('valid');
                //    //    esvalido = false;

                //    //    } else {
                //    //        $('#ctl00_contenido_cplugar').parent().removeClass("state-error").addClass('state-success');
                //    //        $('#ctl00_contenido_cplugar').addClass('valid');
                //    //    }
                //    //}

                //    if ($("#ctl00_contenido_telefonolugar").val() == null || $("#ctl00_contenido_telefonolugar").val().split(" ").join("") == "") {
                //        ShowError("Teléfono nacimiento", "El teléfono es obligatorio.");
                //        $('#ctl00_contenido_telefonolugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_telefonolugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_telefonolugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_telefonolugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_localidadn").val() == null || $("#ctl00_contenido_localidadn").val().split(" ").join("") == "") {
                //        ShowError("Localidad nacimiento", "La localidad es obligatoria.");
                //        $('#ctl00_contenido_localidadn').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_localidadn').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_localidadn').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_localidadn').addClass('valid');
                //    }
                //}

                return esvalido;
            }

            function validarGeneral() {
                var esvalido = true;

                if ($("#ctl00_contenido_Text1").val() == null || $("#ctl00_contenido_Text1").val().split(" ").join("") == "") {
                    ShowError("Fecha nacimiento", "La fecha nacimiento es obligatoria.");
                    $('#ctl00_contenido_Text1').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_Text1').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_Text1').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_Text1').addClass('valid');
                }

                if ($("#ctl00_contenido_nacionalidad").val() == null || $("#ctl00_contenido_nacionalidad").val() == 0) {
                    ShowError("Nacionalidad", "La nacionalidad es obligatoria.");
                    $('#ctl00_contenido_nacionalidad').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_nacionalidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_nacionalidad').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_nacionalidad').addClass('valid');
                }

                if ($("#ctl00_contenido_escolaridad").val() == null || $("#ctl00_contenido_escolaridad").val() == 0) {
                    ShowError("Escolaridad", "La escolaridad es obligatoria.");
                    $('#ctl00_contenido_escolaridad').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_escolaridad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_escolaridad').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_escolaridad').addClass('valid');
                }

                
                if ($("#ctl00_contenido_ocupacion").val() == null || $("#ctl00_contenido_ocupacion").val() == 0) {
                    ShowError("Ocupación", "La ocupación es obligatoria.");
                    $('#ctl00_contenido_ocupacion').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_ocupacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_ocupacion').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_ocupacion').addClass('valid');
                }

                if ($("#ctl00_contenido_civil").val() == null || $("#ctl00_contenido_civil").val() == 0) {
                    ShowError("Estado civil", "El estado civil es obligatorio.");
                    $('#ctl00_contenido_civil').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_civil').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_civil').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_civil').addClass('valid');
                }

          

                if ($("#ctl00_contenido_sexo").val() == null || $("#ctl00_contenido_sexo").val() == 0) {
                    ShowError("Sexo", "El sexo es obligatorio.");
                    $('#ctl00_contenido_sexo').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_sexo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_sexo').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_sexo').addClass('valid');
                }


                return esvalido;
            }

            function SaveGeneral(datos) {                
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/saveGeneral",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {               
                            $('#<%= trackingGeneral.ClientID %>').val(resultado.TrackingId);
                            $('#<%= idGeneral.ClientID %>').val(resultado.Id);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Los datos generales se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "Los datos generales se " + resultado.mensaje + " correctamente.");                                                        
                        }
                        else {
                            ShowError("¡Error! Algo salió mal. -Don´t save broad. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }                        
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal. Don´t save broad. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);                        
                    }
                });
            }

            $("#btnSaveGeneral").click(function () {
                var estado = false;
                var inimputable = false; 

                datos = [
                    id = $('#ctl00_contenido_idGeneral').val(),
                    tracking = $('#ctl00_contenido_trackingGeneral').val(),
                    fecha = $("#ctl00_contenido_Text1").val(),
                    rfc = $("#ctl00_contenido_rfc").val(),
                    nacionalidad = $("#ctl00_contenido_nacionalidad").val(),
                    escolaridad = $("#ctl00_contenido_escolaridad").val(),
                    religion = $("#ctl00_contenido_religion").val(),
                    ocupacion = $("#ctl00_contenido_ocupacion").val(),
                    civil = $("#ctl00_contenido_civil").val(),
                    etnia = $("#ctl00_contenido_etinia").val(),
                    sexo = $("#ctl00_contenido_sexo").val(),
                    estado = estado,
                    inimputable = inimputable,
                    interno = $('#ctl00_contenido_trackingidInterno').val(),
                    CURP = $("#ctl00_contenido_CURP").val(),
                    LenguanativaId = $("#lenguanativa").val(),
                    Personanotifica = $("#personanotifica").val(),
                    Celulartifica=$("#celular").val(),
                ];

                if (validarGeneral()) {
                    SaveGeneral(datos);
                }
            });

            function CargarSexo(setsexo) {
                $('#ctl00_contenido_sexo').empty();
                $.ajax({

                    type: "POST",
                    url: "sentence_entrylist.aspx/getSexo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_sexo');

                        Dropdown.append(new Option("[Sexo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setsexo != "") {
                            Dropdown.val(setsexo);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de sexo. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CarjarfiltroJuezCalificador(setetnia) {
                $('#filtrojuezcalificador').empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/GetFiltrosJuezCalificador",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#filtrojuezcalificador');

                        Dropdown.append(new Option("Todos los detenidos", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setetnia != "") {
                            Dropdown.val(setetnia);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de etnias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEtnia(setetnia) {
                $('#ctl00_contenido_etinia').empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getEtnia",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_etinia');

                        Dropdown.append(new Option("[Etnia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setetnia != "") {
                            Dropdown.val(setetnia);
                            Dropdown.trigger("change");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de etnias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEstadoCivil(set) {
                $('#ctl00_contenido_civil').empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getEstadoCivil",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_civil');

                        Dropdown.append(new Option("[Estado Civil]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de estado civil. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarOcupacion(set) {
                $('#ctl00_contenido_ocupacion').empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getOcupacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_ocupacion');

                        Dropdown.append(new Option("[Ocupación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de ocupaciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarReligion(set) {
                $('#ctl00_contenido_religion').empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getReligion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_religion');

                        Dropdown.append(new Option("[Religión]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de religiones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEscolaridad(set) {
                $('#ctl00_contenido_escolaridad').empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getEscolaridad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_escolaridad');

                        Dropdown.append(new Option("[Escolaridad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de escolaridad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarNacionalidad(set) {
                $('#ctl00_contenido_nacionalidad').empty();
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getNacionalidad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_nacionalidad');

                        Dropdown.append(new Option("[Nacionalidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de nacionalidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function validarDetenido() {
                var esvalido = true;               

                if ($("#nombreDetenido").val() == null || $("#nombreDetenido").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#nombreDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#nombreDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#nombreDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#nombreDetenido').addClass('valid');
                }

                if ($("#paterno").val() == null || $("#paterno").val().split(" ").join("") == "") {
                    ShowError("Apellido paterno", "El apellido paterno es obligatorio.");
                    $('#paterno').parent().removeClass('state-success').addClass("state-error");
                    $('#paterno').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#paterno').parent().removeClass("state-error").addClass('state-success');
                    $('#paterno').addClass('valid');
                }

                if ($("#materno").val() == null || $("#materno").val().split(" ").join("") == "") {
                    ShowError("Apellido materno", "El apellido materno es obligatorio.");
                    $('#materno').parent().removeClass('state-success').addClass("state-error");
                    $('#materno').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#materno').parent().removeClass("state-error").addClass('state-success');
                    $('#materno').addClass('valid');
                }                
                 var file = document.getElementById('<% = imagenDetenido.ClientID %>').value;
                if (file != null && file != '') {

                    if (!validaImagen(file)) {
                        ShowError("Fotografía detenido", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        esvalido = false;
                    }
                    else {
                        var validar = $("#Validar1").val();
                        var validar2 = $("#Validar2").val();


                        if ($("#Max").val() != "0") {
                            if (validar == 'false') {
                                ShowError("Fotografía detenido", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                esvalido = false;
                            }

                        }
                        if ($("#Min").val() != "0") {
                            var s = $("#Min").val() * 1024;
                            if (validar2 == 'false') {
                                ShowError("Fotografía detenido", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                esvalido = false;
                            }
                        }
                    }
                }
                return esvalido;
            }

            function clearModalDetenido() {
                $(".input").removeClass('state-success');
                $(".input").removeClass('state-error');
                $(".input").removeClass('valid');
                $(".select").removeClass('state-success');
                $(".select").removeClass('state-error');
                $(".select").removeClass('valid');
                $("#ctl00_contenido_imagenDetenido").val("");
            }

            $("#btnSaveDetenido").click(function () {
                if (validarDetenido()) {
                    guardarImagen();
                }
            });

            function guardarImagen() {

                var files = $("#ctl00_contenido_imagenDetenido").get(0).files;

                var nombreAvatarAnterior = $("#imagenDetenidoOriginal").val();
       
                var nombreAvatar = "";
       
                if (files.length > 0) {

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "../Handlers/FileUploadHandler.ashx?action=2&before=" + nombreAvatarAnterior,
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (Results) {
                            if (Results.exitoso) {
                                nombreAvatar = Results.nombreArchivo;
                                GuardarDetenido(nombreAvatar);
                            }
                            else {
                                ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                            }
                        },
                        error: function (err) {
                            ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else {
                    GuardarDetenido(nombreAvatarAnterior);
                }
            }

            function GuardarDetenido(rutaAvatar) {                
                var datos = ObtenerValoresDetenido(rutaAvatar);             
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/saveDetenido",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            $("#dt_basic").DataTable().ajax.reload();
                            $("#modalEditarDetenido").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del detenido se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información del detenido se " + resultado.mensaje + " correctamente.");                            
                        }
                        else  {                        
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                 "Algo salió mal. " + resultado.mensaje + "</div>");
                            ShowError("¡Error! Algo salió mal. Don´t save arrested. ", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }                        
                    }
                });
            }

            function ObtenerValoresDetenido(rutaAvatar) {               
                var datos = [
                    Id = $('#idDetenido').val(),
                    TrackingId = $('#trackingDetenido').val(),                                                            
                    Nombre = $('#nombreDetenido').val(),
                    Paterno = $('#paterno').val(),
                    Materno = $('#materno').val(),                    
                    Avatar = rutaAvatar                    
                ];
                return datos;
            }

            $("body").on("click", ".editarDetenido", function () {
                clearModalDetenido();
                cargarDatosDetenido($(this).attr("data-tracking"));
                responsiveHelper_dt_basicAlias = undefined;
                $("dt_basicAlias").DataTable().destroy();
                loadAlias($(this).attr("data-tracking"));
            });

            CargarLenguaNativa("0");
            function CargarLenguaNativa(setlengua) {
                $('#lenguanativa').empty();
                $.ajax({

                    type: "POST",
                    url: "sentence_entrylist.aspx/getlenguanativa",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#lenguanativa');

                        Dropdown.append(new Option("[Lengua nativa]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setlengua != "") {
                            Dropdown.val(setlengua);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de lengua nativa. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function cargarDatosDetenido(tracking) {                                 
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/cargarDatosInterno",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({ tracking: tracking }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#nombreDetenido").val(resultado.obj.Nombre);
                            $("#paterno").val(resultado.obj.Paterno);
                            $("#materno").val(resultado.obj.Materno);
                            $("#fecha").val(resultado.obj.Fecha);
                            $("#expediente").val(resultado.obj.Expediente);
                            $("#idDetenido").val(resultado.obj.Id);
                            $("#trackingDetenido").val(resultado.obj.TrackingId);
                            $("#imagenDetenidoOriginal").val(resultado.obj.Foto);
                            $('#ctl00_contenido_Text1').datetimepicker({
                                ampm: true,
                                format: 'DD/MM/YYYY'
                            });

                            $("#modalEditarDetenido").modal("show");
                            $("#titleModalEditarDetenido").html('<i class="fa fa-pencil" +=""></i> Editar detenido');
                            
                            if (resultado.obj.Lesion_visible == "True") {
                               
                                $("#Lesion_visible").prop('checked', true);
                            }
                            else {
                                
                                $("#Lesion_visible").prop('checked', false);
                            }

                            if (resultado.obj.Foto != "" && resultado.obj.Foto != null) {
                                var ext = "." + resultado.obj.Foto.split('.').pop();
                                var photo = resultado.obj.Foto.replace(ext, ".thumb");                                
                                var imgAvatar = resolveUrl(photo);
                                $("#detenidoEditar").attr("src", imgAvatar);
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                $("#detenidoEditar").attr("src", pathfoto);
                            }
                            var lengua = resultado.obj.Lenguanativa;
                            
                            $("#ctl00_contenido_Text1").val(resultado.obj.FechaNacimiento);
                            $("#ctl00_contenido_rfc").val(resultado.obj.RFC);
                            $("#ctl00_contenido_CURP").val(resultado.obj.CURP);
                            $("#personanotifica").val(resultado.obj.Personanotifica);
                            $("#celular").val(resultado.obj.Celularnotifica);
                            $("#lenguanativa").val(resultado.obj.Lenguanativa);
                            $('#ctl00_contenido_trackingidInterno').val(resultado.obj.TrackingId);
                            $('#ctl00_contenido_idGeneral').val(resultado.obj.IdGeneral);
                            $('#ctl00_contenido_trackingGeneral').val(resultado.obj.TrackingIdGeneral),
                            $("#latitud").val(resultado.obj.LatitudDomicilio);
                            $("#longitud").val(resultado.obj.LongitudDomicilio);                            
                            CargarLenguaNativa(lengua);
                            CargarNacionalidad(resultado.obj.NacionalidadId);
                            CargarEscolaridad(resultado.obj.EscolaridadId);
                            CargarReligion(resultado.obj.ReligionId);
                            CargarSexo(resultado.obj.SexoId);
                            CargarEstadoCivil(resultado.obj.EstadoCivilId);
                            CargarOcupacion(resultado.obj.OcupacionId);
                            CargarEtnia(resultado.obj.EtniaId);
                            //Domicilio
                            console.log(resultado);
                            if (resultado.obj.TieneDomicilio) {
                                if (resultado.obj.PaisDomicilio == 73) {
                                    $('#sectionlocalidad').hide();
                                    $('#sectionmunicipio').show();
                                    $('#sectionestado').show();
                                    $('#sectioncoloniaDomicilio').show();
                                    $('#sectioncpdomicilio').show();
                                    CargarPais(resultado.obj.PaisDomicilio, "#ctl00_contenido_paisdomicilio");
                                    CargarEstado(resultado.obj.EstadoDomicilio, "#ctl00_contenido_estadodomicilio", resultado.obj.PaisDomicilio);
                                    CargarMunicipio(resultado.obj.MunicipioDomicilio, "#ctl00_contenido_municipiodomicilio", resultado.obj.EstadoDomicilio);
                                    CargarColonia(resultado.obj.ColoniaIdDomicilio, resultado.obj.MunicipioDomicilio);
                                    CargarCodigoPostalDomicilio(resultado.obj.ColoniaIdDomicilio);
                                    $("#ctl00_contenido_calledomicilio").val(resultado.obj.CalleDomicilio);
                                    $("#ctl00_contenido_numerodomicilio").val(resultado.obj.NumeroDomicilio);
                                    $("#ctl00_contenido_telefonodomicilio").val(resultado.obj.TelefonoDomicilio);
                                    $("#latitud").val(resultado.obj.LatitudDomicilio);
                                    $("#longitud").val(resultado.obj.LongitudDomicilio);
                                }
                                else {
                                    $('#sectionlocalidad').show();
                                    $('#sectionmunicipio').hide();
                                    $('#sectionestado').hide();
                                    $('#sectioncoloniaDomicilio').hide();
                                    $('#sectioncpdomicilio').hide();                                
                                    $('#ctl00_contenido_localidad').val(resultado.domicilio.LocalidadDomicilio);
                                }

                                $('#<%= trackingDomicilio.ClientID %>').val(resultado.obj.TrackingIdDomicilio);
                                $('#<%= idDomicilio.ClientID %>').val(resultado.obj.IdDomicilio);
                            }
                            else {
                                //CargarPais(73, '#ctl00_contenido_paisdomicilio');
                                //CargarEstado(0, '#ctl00_contenido_estadodomicilio', 73);
                                CargarPais(resultado.obj.PaisDomicilio, "#ctl00_contenido_paisdomicilio");
                                CargarEstado(resultado.obj.EstadoDomicilio, "#ctl00_contenido_estadodomicilio", resultado.obj.PaisDomicilio);
                                CargarMunicipio(resultado.obj.MunicipioDomicilio, "#ctl00_contenido_municipiodomicilio", resultado.obj.EstadoDomicilio);
                                CargarColonia(resultado.obj.ColoniaIdDomicilio, resultado.obj.MunicipioDomicilio);
                                $('#<%= trackingDomicilio.ClientID %>').val("");
                                $('#<%= idDomicilio.ClientID %>').val("");
                                $('#sectionlocalidadn').hide();
                            }                            

                            if (resultado.obj.TieneNacimiento) {
                                if (resultado.obj.PaisNacimiento == 73) {
                                    $('#sectionlocalidadn').hide();
                                    $('#sectionmunicipion').show();
                                    $('#sectionestadon').show();
                                    $('#sectioncoloniaNacimiento').show();
                                    $('#sectioncpnacimiento').show();
                                    CargarPais(resultado.obj.PaisNacimiento, "#ctl00_contenido_paisnacimineto");
                                    CargarEstado(resultado.obj.EstadoNacimiento, "#ctl00_contenido_estadonacimiento", resultado.obj.PaisNacimiento);
                                    CargarMunicipio(resultado.obj.MunicipioNacimiento, "#ctl00_contenido_municipionacimiento", resultado.obj.EstadoNacimiento);
                                    CargarColoniaNacimiento(resultado.obj.ColoniaIdNacimiento, resultado.obj.MunicipioNacimiento);
                                    CargarCodigoPostalNacimiento(resultado.obj.ColoniaIdNacimiento);
                                    $("#ctl00_contenido_callelugar").val(resultado.obj.CalleNacimiento);
                                    $("#ctl00_contenido_numerolugar").val(resultado.obj.NumeroNacimiento);
                                    $("#ctl00_contenido_telefonolugar").val(resultado.obj.TelefonoNacimiento);
                                }
                                else {
                                    $('#sectionlocalidadn').show();
                                    $('#sectionmunicipion').hide();
                                    $('#sectionestadon').hide();
                                    $('#sectioncoloniaNacimiento').hide();
                                    $('#sectioncpnacimiento').hide();                                    
                                    $('#ctl00_contenido_localidadn').val(resultado.nacimiento.LocalidadNacimiento);
                                }                               

                                $('#<%= trackingNacimiento.ClientID %>').val(resultado.obj.TrackingIdNacimiento);
                                $('#<%= idNacimiento.ClientID %>').val(resultado.obj.IdNacimiento);
                            }
                            else {
                                CargarPais(73, '#ctl00_contenido_paisnacimineto');
                                CargarEstado(0, '#ctl00_contenido_estadonacimiento', 73);
                                $('#domicilioInterno').text("Domicilio no registrado");                                
                                $('#<%= trackingNacimiento.ClientID %>').val("");
                                $('#<%= idNacimiento.ClientID %>').val("");
                                $('#sectionlocalidad').hide();
                            }    
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");                            
                            ShowError("¡Error! Algo salió mal. -Don´t save console.", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado);                            
                        }
                    },
                    error: function (error) {
                        $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + "err0o0or" + error + "</div>");                            
                        ShowError("¡Error! Algo salió mal. Don´t save console. ", "Si el problema persiste, contacte al personal de soporte técnico. " + error);                            
                    }
                });            
            }

            function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }

                }
                return false;
            }

            $("#ctl00_contenido_paisdomicilio").change(function () {
                if ($("#ctl00_contenido_paisdomicilio option:selected").val() != 73) {
                    $('#sectionlocalidad').show();
                    $('#sectionmunicipio').hide();
                    $('#sectionestado').hide();
                    $('#sectioncoloniaDomicilio').hide();
                    $('#sectioncpdomicilio').hide();
                    $("#ctl00_contenido_cpdomicilio").removeAttr("disabled");
                }
                else {
                    $('#sectionestado').show();
                    $('#sectionmunicipio').show();
                    $('#sectioncoloniaDomicilio').show();
                    $('#sectionlocalidad').hide();
                    $('#sectioncpdomicilio').show();
                    $("#ctl00_contenido_cpdomicilio").attr("disabled", "disabled");
                    CargarEstado(0, '#ctl00_contenido_estadodomicilio', $("#ctl00_contenido_paisdomicilio").val());
                }


            });

            $("#ctl00_contenido_estadodomicilio").change(function () {
                CargarMunicipio(0, '#ctl00_contenido_municipiodomicilio', $("#ctl00_contenido_estadodomicilio").val());
            });

            $("#ctl00_contenido_paisnacimineto").change(function () {
                if ($("#ctl00_contenido_paisnacimineto option:selected").val() != 73) {
                    $('#sectionlocalidadn').show();
                    $('#sectionmunicipion').hide();
                    $('#sectionestadon').hide();
                    $('#sectioncoloniaNacimiento').hide();
                    $('#sectioncpnacimiento').hide();
                    $("#ctl00_contenido_cplugar").removeAttr("disabled");
                }
                else {
                    $('#sectionestadon').show();
                    $('#sectionmunicipion').show();
                    $('#sectionlocalidadn').hide();
                    $('#sectioncoloniaNacimiento').show();
                    $('#sectioncpnacimiento').show();
                    $("#ctl00_contenido_cplugar").attr("disabled", "disabled");
                    CargarEstado(0, '#ctl00_contenido_estadonacimiento', $("#ctl00_contenido_paisnacimineto").val());
                }


            });

            $("#ctl00_contenido_estadonacimiento").change(function () {
                CargarMunicipio(0, '#ctl00_contenido_municipionacimiento', $("#ctl00_contenido_estadonacimiento").val());
            });

            $("#ctl00_contenido_coloniaSelect").change(function () {
                CargarCodigoPostalDomicilio($("#ctl00_contenido_coloniaSelect").val());
            });

            $("#ctl00_contenido_coloniaSelectNacimiento").change(function () {
                CargarCodigoPostalNacimiento($("#ctl00_contenido_coloniaSelectNacimiento").val());
            });

            $("#ctl00_contenido_municipiodomicilio").change(function () {
                CargarColonia("0", $("#ctl00_contenido_municipiodomicilio").val());
            });

            $("#ctl00_contenido_municipionacimiento").change(function () {
                CargarColoniaNacimiento("0", $("#ctl00_contenido_municipionacimiento").val());
            });

            //rep4ir
            //pageSetUp();
            var horasDatos = "";
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            var responsiveHelper_dt_basic_evento = undefined;
            var responsiveHelper_dt_basic_lugarevento = undefined;
            var responsiveHelper_dt_basic_eventoAW = undefined;
            var responsiveHelper_dt_basic_lugareventoAW = undefined;
            var exitoso = 0;
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

           

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY": "350px",
                "scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },                
                //"order": [[9, "asc"],[3, "asc"],[4, "asc"],[5, "asc"]],
                "order": [[11, "asc"]],
                ajax: {
                    url: "sentence_entrylist.aspx/getdata",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: function (d) {                        
                        startLoading();
                        d.emptytable = false;
                        d.opcion = $(".mostrar").val() || "";
                        d.anio = $("#idAnioDetenidos").val() || "";
                        d.pages = $('#dt_basic').DataTable().page.info().page || "";
                        d.fechaInicial = $("#tbFechaInicial").val();
                        d.fechaFinal = $("#tbFechaFinal").val();
                        return JSON.stringify(d);
                    },
                    dataSrc: "data",
                    dataFilter: function (data) {
                        var json = jQuery.parseJSON(data);
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        endLoading();
                        return JSON.stringify(json);
                    }
                },                
                columns: [
                    null,
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre",
                        orderable: true
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                     //null,
                  
                    null,
                   
                   
                    null,

                        {
                        name: "horas",
                        data: "horas"
                    },
                    null,
                    {
                        name: "Fecha",
                        data: "Fecha",
                        visible: false

                    },
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false

                    }
                ],
                columnDefs: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,

                        visible: true,
                        
                        render: function (data, type, row, meta) {
                            return '<input type="checkbox" class="checar" data-TrackingId="' + row.TrackingId + '" data-Id="' + row.Id +'"/>';
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != "" && row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");                                
                                var imgAvatar = resolveUrl(photo);                                
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.Situacion != null) {
                                return row.Situacion;
                            }
                            else {
                                return "Pendiente";
                            }
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            if (row.IngresoId === 0 && row.TotalAPagar > 0) {
                                return '<i class="fa fa-circle" style="color:#d9534f"></i>';
                            }
                            else {
                                return '<i class="fa fa-circle" style="color:#729e72"></i>';                                
                            }
                        }
                    },
                    {
                        targets: 9,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            
                            if (row.CalificacionId == 0) {
                                return "";
                            }                   

                            var time = row.horas != null ? row.horas : "";
                            
                            if (time === "") {
                                return "";
                            }
                            else {
                                var timeSplitted = time.split(' ');
                                var days = timeSplitted[0];
                                var hours = timeSplitted[2];
                                var minutes = timeSplitted[4];

                                if (days > 0 || hours > 0 || minutes > 0) {
                                    //Finalizo
                                    var timeFormatted = "" + Math.abs(days) + " días " + Math.abs(hours) + " horas " + Math.abs(minutes) + " minutos";
                                    return 'Finalizó hace ' + timeFormatted + ' <i class="fa fa-circle" style="color:#d9534f"></i>';
                                }
                                else if (days < 0 || hours < 0 || minutes < 0) {
                                    //Restan
                                    var timeFormatted = "" + Math.abs(days) + " días " + Math.abs(hours) + " horas " + Math.abs(minutes) + " minutos";
                                    return 'Restan: ' + timeFormatted + ' <i class="fa fa-circle" style="color:#729e72"></i>';
                                }
                                else if (days == 0 && hours == 0 && minutes == 0) {
                                    //Finalizo
                                    var timeFormatted = "" + Math.abs(days) + " días " + Math.abs(hours) + " horas " + Math.abs(minutes) + " minutos";
                                    return 'Finalizó hace ' + timeFormatted + ' <i class="fa fa-circle" style="color:#d9534f"></i>';
                                }
                            }
                        }
                    },
                    {
                        targets: 10,
                        orderable: false,
                        width: "200px",
                        render: function (data, type, row, meta) {
                            var edit = "edit";
                            var editar = "";
                            var color = "";
                            var txtestatus = "";
                            var icon = "";
                            var habilitar = "";

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                            }
                            //   if ($("#ctl00_contenido_KAQWPK").val() == "true")
                            //  editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            // else if($("#ctl00_contenido_WERQEQ").val() == "true")
                            // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Proceso + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';

                            var action2 = "";
                            var motivo = "Motivodetencion";
                            action2 = '<div class="btn-group"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Ver acciones <span class="caret"></span></button><ul class="dropdown-menu"><li><a href="MotivoDetencionInterno.aspx?tracking=' + row.TrackingId + '" title="Motivo de detención"><i class="glyphicon glyphicon-warning-sign"></i> Motivo de detención</a></li><li class="divider"></li>';

                            var action3 = "";
                            if (row.DetalledetencionId == 0) {
                                action3 = '<li><a  href="calificacion.aspx?tracking=' + row.TrackingId + '" title="Calificar detenido"><i class="fa fa-gavel"></i> Calificar detenido</a></li><li class="divider"></li>';
                            }
                            else {
                                if (row.CalificacionId == 0) {
                                    action3 = '<li><a  href="calificacion.aspx?tracking=' + row.TrackingId + '" title="Calificar detenido"><i class="fa fa-gavel"></i> Calificar detenido</a></li><li class="divider"></li>'
                                }
                                else {
                                    action3 = '<li><a  href="calificacion.aspx?tracking=' + row.TrackingId + '" title="Ver calificación"><i class="fa fa-gavel"></i> Ver Calificacón</a></li><li class="divider"></li>'
                                }
                            }
                            var hours = "";
                            var action4 = "";

                            //action4 = '<li><a  href="calificacion.aspx?tracking=' + row.TrackingId + '" title="Traslado"><i class="glyphicon glyphicon-transfer"></i> Traslado</a></li><li class="divider"></li>'
                            //var nombrecompleto = row.Nombre + " " + row.Paterno + " " + row.Materno
                            //if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                            //    editar = '<li><a class="' + edit + ' "href="javascript:void(0);" data-value="' + nombrecompleto + '"data-tracking="' + row.TrackingId + '"  title="Traslado"><i class="glyphicon glyphicon-transfer"></i> Traslado</a></li><li class="divider"></li>'
                            //}
                            var vacio = "";
                            var action7 = "";
                            var action5 = "";
                            action5 = '<div class="btn-group"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Ver / editar detenido <span class="caret"></span></button><ul class="dropdown-menu"><li><a class="btn-sm datos' + vacio + '"  href="#"  id="datos" data-id="' + row.TrackingId + '" data-hora="' + row.Horas + '" title="Información de detención"><i class="fa fa-bars"></i> Información de detención</a></li><li class="divider"></li>';

                           

                            var action8 = '<li><a class="btn-sm editarDetenido" href="javascript:void(0);" id="editarDetenido" data-tracking="' + row.TrackingId + '" title = "Editar detenido" > <i class="fa fa-gavel"></i> Editar detenido</a></li><li class="divider"></li>';

                            var action9 = '<li><a class="btn-sm ReporteSalidaDetenido" href="javascript:void(0);" id="ReporteSalida" data-tracking="' + row.TrackingIdEstatus + '" title = "Autorización de salida" ><i class="fa fa-sign-out"></i>  Autorización de salida</a></li><li class="divider"></li></ul></div>';
                            var valor = $("#filtrojuezcalificador").val();
                            if (valor != "9" && valor != "10") {
                                action9 = '</ul></div>';
                            }

                            //return action2 + action3 + editar + action5 + action8 + action9 + action10 + action7;
                            return  action5 + action8+action9 ;

                        }
                    }
                ],
                 select: {
                 style:    'os',
                 selector: 'td:first-child'
                },

            });

            dtable = $("#dt_basic").dataTable().api();

            $("#dt_basic_filter input[type='search']")
                .unbind()
                .bind("input", function (e) {

                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
                });

            var $textarea = $("#dt_basic_filter input[type='search']");

            $textarea.on('blur', function () {
                setTimeout(function () {
                    //$textarea.focus();
                    $('.btn-group').removeClass('open');
                }, 0);
            });

            $("#dt_basic_filter input[type='search']").keypress(function (e) {
                if (e.charCode === 13) {
                    dtable.search($("#dt_basic_filter input[type='search']").val()).draw();
                }
            });

            $("body").on("click", ".ReporteSalidaDetenido", function () {
                var tracking = $(this).attr("data-tracking");
                reportesalidadetenido(tracking);
            });

            function reportesalidadetenido(tracking) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/ReportesSalidas",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'tracking': tracking }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "  " + resultado.mensaje + ".", "<br /></div>");

                            ShowSuccess("¡Bien hecho!", "  " + resultado.mensaje + ".");
                            setTimeout(hideMessage, hideTime);
                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            
                        }
                        else {
                            $('#main').waitMe('hide');

                            

                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            
                            
                                ShowError("¡Error! Algo salió mal. Don´t save departure. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                            
                        }
                    }
                });
            }
            $("body").on("click", ".historial", function () {
                  var dataArreglo = new Array();                

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();                                                
                        dataArreglo.push(data.TrackingId);                        
                    }                        
                });    

               
                if (parseInt( dataArreglo.length) == 0)
                {
                    ShowAlert("¡Alerta!", "Seleccione un registro.");
                    return;
                }
               
                if (parseInt( dataArreglo.length) > 1)
                {
                    ShowAlert("¡Alerta!","Seleccione solo un registro.");
                    return;
                }
                
                var detenidoId = dataArreglo[0];

                var tracking =detenidoId;
                responsiveHelper_dt_basic_his = undefined;
                $('#dt_basic_historial').DataTable().destroy();
                loadTableHistorial(tracking);
                $("#form-modal-historial").modal('show');
                $("#form-modal-title-historial").empty();
                $("#form-modal-title-historial").html("Movimientos históricos del detenido");
            });

            $("body").on("click", ".verEvento", function () {

              var dataArreglo = new Array();                

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();                                                
                        dataArreglo.push(data.TrackingId);                        
                    }                        
                });    

               
                if (parseInt( dataArreglo.length) == 0)
                {
                    ShowAlert("¡Alerta!", "Seleccione un registro.");
                    return;
                }
               
                if (parseInt( dataArreglo.length) > 1)
                {
                    ShowAlert("¡Alerta!","Seleccione solo un registro.");
                    return;
                }
                
                var detenidoId = dataArreglo[0];
                //var detenidoId = $(this).attr("data-id");

                responsiveHelper_dt_basic_evento = undefined;
                $('#dt_basic_evento').DataTable().destroy();

                responsiveHelper_dt_basic_lugarevento = undefined;
                $('#dt_basic_lugar').DataTable().destroy();

                responsiveHelper_dt_basic_eventoAW = undefined;
                $('#dt_basic_evento_AW').DataTable().destroy();

                responsiveHelper_dt_basic_lugareventoAW = undefined;
                $('#dt_basic_lugar_AW').DataTable().destroy();

                loadTableEvento(detenidoId);
                loadTableLugarEvento(detenidoId);
                loadTableEventoAW(detenidoId);
                loadTableLugarEventoAW(detenidoId);


                $("#form-modal-eventos").modal('show');
                $("#form-modal-title-eventos").empty();
                $("#form-modal-title-eventos").html("Eventos");
                
            });

            $(".mostrar").change(function () {
                startLoading();
                var valor = $("#filtrojuezcalificador").val();
                if (valor == "9"||valor=="10") {
                    $("#salidaefectuadajuez").hide();
                }
                else {
                    $("#salidaefectuadajuez").show();
                }
                window.table.api().ajax.reload();
            });

            function loadTableEvento(detenidoId) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getEvento",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                     data: JSON.stringify({
                        detenidoId: detenidoId
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        llenarTablaEvento(r.lista);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
                
            }

            function llenarTablaEvento(lista) {
                $('#dt_basic_evento').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: false,
                    fixedColumns: true,
                    autoWidth: true,
                    
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_evento) {
                            responsiveHelper_dt_basic_evento = new ResponsiveDatatablesHelper($('#dt_basic_evento'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_evento.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_evento.respond();
                        $('#dt_basic_evento').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {

                    },
                    data: lista,
                    columns: [
                        {
                            name: "FolioEvento",
                            data: "FolioEvento"
                        },
                        {
                            name: "Descripcion",
                            data: "Descripcion"
                        },
                        {
                            name: "FechaHora",
                            data: "FechaHora"
                        },
                        {
                            name: "Localizacion",
                            data: "Localizacion"
                        },
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "FolioEvento",
                            render: function (data, type, row, meta) {
                                return row.FolioEvento;
                            }
                        },
                        {
                            targets: 1,
                            data: "Descripcion",
                            render: function (data, type, row, meta) {
                                return row.Descripcion;
                            }
                        },
                        {
                            targets: 2,
                            data: "FechaHora",
                            render: function (data, type, row, meta) {
                                return row.FechaHora;
                            }
                        },
                        {
                            targets: 3,
                            data: "Localizacion",
                            render: function (data, type, row, meta) {
                                return row.Localizacion;
                            }
                        }
                    ]
                });
            }

            function loadTableLugarEvento(detenidoId) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getLugarEvento",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                     data: JSON.stringify({
                        detenidoId: detenidoId
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        llenarTablaLugarEvento(r.lista);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
                
            }

            function llenarTablaLugarEvento(lista) {
                $('#dt_basic_lugar').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: false,
                    fixedColumns: true,
                    autoWidth: true,
                    //"scrollY": "100%",
                    //"scrollX": "0%",
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_lugarevento) {
                            responsiveHelper_dt_basic_lugarevento = new ResponsiveDatatablesHelper($('#dt_basic_lugar'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_lugarevento.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_lugarevento.respond();
                        $('#dt_basic_lugar').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        
                    },
                    data: lista,
                    columns: [
                        {
                            name: "NumeroDetenidos",
                            data: "NumeroDetenidos"
                        },
                        {
                            name: "Latitud",
                            data: "Latitud"
                        },
                        {
                            name: "Longitud",
                            data: "Longitud"
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "NumeroDetenidos",
                            render: function (data, type, row, meta) {
                                return row.NumeroDetenidos;
                            }
                        },
                        {
                            targets: 1,
                            data: "Latitud",
                            render: function (data, type, row, meta) {
                                return row.Latitud;
                            }
                        },
                        {
                            targets: 2,
                            data: "Longitud",
                            render: function (data, type, row, meta) {
                                return row.Longitud;
                            }
                        }
                    ]
                });
            }

            function loadTableEventoAW(detenidoId) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getEventoAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                     data: JSON.stringify({
                        detenidoId: detenidoId
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        llenarTablaEventoAW(r.lista);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
                
            }

            function llenarTablaEventoAW(lista) {
                $('#dt_basic_evento_AW').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: false,
                fixedColumns: true,
                    autoWidth: true,
                //"scrollY": "100%",
                //    "scrollX": "0%",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_eventoAW) {
                            responsiveHelper_dt_basic_eventoAW = new ResponsiveDatatablesHelper($('#dt_basic_evento_AW'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_eventoAW.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_eventoAW.respond();
                        $('#dt_basic_evento_AW').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        
                    },
                    data: lista,
                    columns: [
                        {
                            name: "FolioEvento",
                            data: "FolioEvento"
                        },
                        {
                            name: "Descripcion",
                            data: "Descripcion"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "Lugar",
                            data: "Lugar"
                        },
                        {
                            name: "NombreResponsable",
                            data: "NombreResponsable"
                        },
                        {
                            name: "NumeroDetenidos",
                            data: "NumeroDetenidos"
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "FolioEvento",
                            render: function (data, type, row, meta) {
                                return row.FolioEvento;
                            }
                        },
                        {
                            targets: 1,
                            data: "Descripcion",
                            render: function (data, type, row, meta) {
                                return row.Descripcion;
                            }
                        },
                        {
                            targets: 2,
                            data: "Fecha",
                            render: function (data, type, row, meta) {
                                return row.Fecha;
                            }
                        },
                        {
                            targets: 3,
                            data: "Lugar",
                            render: function (data, type, row, meta) {
                                return row.Lugar;
                            }
                        },
                        {
                            targets: 4,
                            data: "NombreResponsable",
                            render: function (data, type, row, meta) {
                                return row.NombreResponsable;
                            }
                        },
                        {
                            targets: 5,
                            data: "NumeroDetenidos",
                            render: function (data, type, row, meta) {
                                return row.NumeroDetenidos;
                            }
                        }
                    ]
                });
            }

            function loadTableLugarEventoAW(detenidoId) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/getLugarEventoAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                     data: JSON.stringify({
                        detenidoId: detenidoId
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        llenarTablaLugarEventoAW(r.lista);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function llenarTablaLugarEventoAW(lista) {
                $('#dt_basic_lugar_AW').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: false,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_lugareventoAW) {
                            responsiveHelper_dt_basic_lugareventoAW = new ResponsiveDatatablesHelper($('#dt_basic_lugar_AW'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_lugareventoAW.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_lugareventoAW.respond();
                        $('#dt_basic_lugar_AW').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        
                    },
                    data: lista,
                    columns: [
                        {
                            name: "Sector",
                            data: "Sector"
                        },
                        {
                            name: "EntreCalle",
                            data: "EntreCalle"
                        },
                        {
                            name: "Latitud",
                            data: "Latitud"
                        },
                        {
                            name: "Longitud",
                            data: "Longitud"
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "Sector",
                            render: function (data, type, row, meta) {
                                return row.Sector;
                            }
                        },
                        {
                            targets: 1,
                            data: "EntreCalle",
                            render: function (data, type, row, meta) {
                                return row.EntreCalle;
                            }
                        },
                        {
                            targets: 2,
                            data: "Latitud",
                            render: function (data, type, row, meta) {
                                return row.Latitud;
                            }
                        },
                        {
                            targets: 3,
                            data: "Longitud",
                            render: function (data, type, row, meta) {
                                return row.Longitud;
                            }
                        }
                    ]
                });
            }

            function loadTableHistorial(tracking) {
                $('#dt_basic_historial').dataTable({
                    "lengthMenu": [5, 10, 25, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    "scrollY": "350px",
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_his) {
                            responsiveHelper_dt_basic_his = new ResponsiveDatatablesHelper($('#dt_basic_historial'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_his.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_his.respond();
                        $('#dt_basic_historial').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                        }
                    },
                    "order": [[3, "asc"]],
                    ajax: {
                        type: "POST",
                        url: "sentence_entrylist.aspx/getDataHistorial",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_historial').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.TrackingId = tracking;
                            return JSON.stringify(parametrosServerSide);
                        }

                    },
                    columns: [                        
                        {
                            name: "TrackingId",
                            data: "TrackingId"
                        },
                        {
                            name: "Movimiento",
                            data: "Movimiento"
                        },
                        {
                            name: "Descripcion",
                            data: "Descripcion"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "Usuario",
                            data: "Usuario"
                        }                                                                             
                    ],
                    columnDefs: [                        
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        }                                                                      
                    ]
                });
                $('#dt_basic_historial').DataTable().ajax.reload();

                $("#dt_basic_historial").parent().css("height", "auto");
                $("#dt_basic_historial").parent().css("max-height", "350px");
            }

            function validarSalidaEfectuada() {
                var esvalido = true;

                if ($("#observacionSalidaEfectuada").val().split(" ").join("") == "") {
                    ShowError("Observación", "La observación es obligatoria.");
                    $('#observacionSalidaEfectuada').parent().removeClass('state-success').addClass("state-error");
                    $('#observacionSalidaEfectuada').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observacionSalidaEfectuada').parent().removeClass("state-error").addClass('state-success');
                    $('#observacionSalidaEfectuada').addClass('valid');
                }

                if ($("#responsableSalidaEfectuada").val().split(" ").join("") == "") {
                    ShowError("Responsable", "El responsable es obligatorio.");
                    $('#responsableSalidaEfectuada').parent().removeClass('state-success').addClass("state-error");
                    $('#responsableSalidaEfectuada').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#responsableSalidaEfectuada').parent().removeClass("state-error").addClass('state-success');
                    $('#responsableSalidaEfectuada').addClass('valid');
                }

                return esvalido;
            }

            function validarSalidaEfectuadaJuez() {
                var esvalido = true;

                if ($("#fundamentoSalidaEfectuada").val().split(" ").join("") == "") {
                    ShowError("Fundamento", "El fundamento es obligatorio.");
                    $('#fundamentoSalidaEfectuada').parent().removeClass('state-success').addClass("state-error");
                    $('#fundamentoSalidaEfectuada').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fundamentoSalidaEfectuada').parent().removeClass("state-error").addClass('state-success');
                    $('#fundamentoSalidaEfectuada').addClass('valid');
                }
                
                if ($("#combotiposalida").val() == "0") {
                    ShowError("Tipo de salida", "El tipo de salida es obligatorio.");
                    $('#combotiposalida').parent().removeClass('state-success').addClass("state-error");
                    $('#combotiposalida').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#combotiposalida').parent().removeClass("state-error").addClass('state-success');
                    $('#combotiposalida').addClass('valid');
                }
                return esvalido;
            }

            $("#salidaefectuada").click(function () {

                var dataArreglo = new Array();                

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();                                                
                        dataArreglo.push(data.TrackingId);                        
                    }                        
                });      

                if (dataArreglo.length > 0) {

                }
                else {
                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención!</strong>" +
                        " No ha seleccionado detenidos.</div>");
                    ShowAlert("¡Atención!", "No ha seleccionado detenidos.");
                    return;
                }

                $("#form-modal-title").empty();
                $("#form-modal-title").html("Agregar");

                /////
                 

                /////
                $("#form-modal").modal('show');                
            });

            function clearSalidaEfectuada() {
                $(".input").removeClass('valid');
                $(".input").removeClass('state-error');
                $(".input").removeClass('state-success');
                $("#observacionSalidaEfectuada").val("");                
                $("#responsableSalidaEfectuada").val("");                
            }

            function clearSalidaEfectuadaJuez() {
                $(".input").removeClass('valid');
                $(".input").removeClass('state-error');
                $(".input").removeClass('state-success');
                $("#fundamentoSalidaEfectuada").val("");
                $("#combotiposalida").val("0");
                $('#combotiposalida').parent().removeClass('state-success');
            }

            $("#btnSaveSalidaEfectuada").click(function () {
                if (validarSalidaEfectuada()) {                    
                    var dataArreglo = new Array();                


                    $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                        var checkit = this.node().childNodes[0].childNodes[0].checked;
                        var data;

                        if (checkit) {
                            data = this.data();                                                
                            dataArreglo.push(data.TrackingId);                        
                        }                        
                    });                          
                    saveSalidaEfectuada(dataArreglo, $("#observacionSalidaEfectuada").val(), $("#responsableSalidaEfectuada").val());
                }                
            });

            //rep4ir
            $("#btnSaveSalidaEfectuadaJuez").click(function () {
                if (validarSalidaEfectuadaJuez()) {                    
                    var dataArreglo = new Array();                

                    $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                        var checkit = this.node().childNodes[0].childNodes[0].checked;
                        var data;

                        if (checkit) {
                            data = this.data();                                                
                            dataArreglo.push(data.TrackingId);                        
                        }                        
                    });      
                    saveSalidaEfectuadaJuez(dataArreglo, $("#fundamentoSalidaEfectuada").val(), $("#combotiposalida").val());
                }                
            });

            $('#modalEditarDetenido').on('hidden.bs.modal', function () {
                $("#dt_basic").DataTable().ajax.reload();
            });


            function saveSalidaEfectuada(datos, observacion, responsable) {
                startLoading();                
                 
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/guardaSalidaEfectuada",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ datos: datos, observacion: observacion, responsable: responsable }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            
                            for (let i = 0; i < resultado.archivos.length; i++) {                                
                                var ruta = ResolveUrl(resultado.archivos[i]);
                                if (ruta != "") {
                                    window.setTimeout(window.open(ruta, '_blank'),3000);
                                }
                            }
                            
                            $("#form-modal").modal('hide'); 
                            $("#form-modal-title").empty();
                            clearSalidaEfectuada();
                            //rep4ir
                            $('#dt_basic').DataTable().ajax.reload();

                            if (resultado.errores != "" && resultado.errores != null) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención! </strong>" +
                                    "" + resultado.errores + "</div > ");
                                ShowAlert("¡Atención!", "  " + resultado.errores + ".");                                                  
                                $('#main').waitMe('hide');                            
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "  " + resultado.mensaje + ".", "<br /></div>");
                                ShowSuccess("¡Bien hecho!", "  " + resultado.mensaje+"." );                                                  
                                $('#main').waitMe('hide');                            
                            }                            
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");                            
                            ShowError("¡Error! Algo salió mal. -Don't save judgeDeparture. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado);                            
                        }
                    },
                    error: function (error) {
                        $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + error + "</div>");                            
                        ShowError("¡Error! Algo salió mal. Don't save judgeDeparture. ", "Si el problema persist3ee, contacte al personal de soporte técnico. " + error);                            
                    }
                });            
            }

            function saveSalidaEfectuadaJuez(datos, fundamento,tiposalidaid) {
                startLoading();                
                 
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/guardaSalidaEfectuadaJuez",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({ datos: datos, fundamento: fundamento,tiposalidaid:tiposalidaid }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            
                            for (let i = 0; i < resultado.archivos.length; i++) {                                
                                var ruta = ResolveUrl(resultado.archivos[i]);
                                if (ruta != "") {
                                    window.open(ruta, '_blank');
                                }
                            }
                            
                            $("#form-modal-juez").modal('hide'); 
                            $("#form-modal-title-juez").empty();
                            clearSalidaEfectuadaJuez();
                            $('#dt_basic').DataTable().ajax.reload();

                            if (resultado.errores != "" && resultado.errores != null) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención! </strong>" +
                                    "" + resultado.errores +"."+ "</div > ");
                                ShowAlert("¡Atención!", "  " + resultado.errores + ".");

                                setTimeout(hideMessage, hideTime);
                                $('#main').waitMe('hide');                            
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                    "  " + resultado.mensaje + ".", "<br /></div>");
                                
                                ShowSuccess("¡Bien hecho!", "  " + resultado.mensaje + "."); 

                                setTimeout(hideMessage, hideTime);
                               
                                $('#main').waitMe('hide');                            
                            }                            
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");                            
                            ShowError("¡Error! Algo salió mal. -Don´t save output. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado);                            
                        }
                    },
                    error: function (error) {
                        $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + error + "</div>");                            
                        ShowError("¡Error! Algio salió mal. Don´t save output. ", "Si el problema persiste, contacte al personal de soporte técnico. " + error);                            
                    }
                });            
            }

            $("#salidaefectuadajuez").click(function () {
                var dataArreglo = new Array();                

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();                                                
                        dataArreglo.push(data.TrackingId);                        
                    }                        
                });      

                if (dataArreglo.length > 0) {

                }
                else {
                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención! </strong>" +
                        "No ha seleccionado detenidos.</div>");
                    ShowAlert("¡Atención!", "No ha seleccionado detenidos.");
                    setTimeout(hideMessage, hideTime);
                    return;
                }
                $("#fundamentoSalidaEfectuada").val("");
                Cargatiposalida("0");
                $("#form-modal-title-juez").empty();
                $("#form-modal-title-juez").html('<i class="fa fa-plus" +=""></i>&nbsp;Autorizar salida');
                $("#form-modal-juez").modal('show');      
            });

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");

                window.emptytable = true;
                window.table.api().ajax.reload();
            });


            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "sentence_entrylist.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Bien hecho!</strong>" +
                                "El registro  se actualizó correctamente.</div>");
                            ShowSuccess("¡Bien hecho!", "El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });
            //JGB 22052019
            function CargarListado(id) {
                
                $.ajax({

                    type: "POST",
                    url: "sentence_entrylist.aspx/getListadoCombo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_dropdown');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Institución a disposición]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("body").on("click", ".edit", function () {

                clearTraslado();
                var xdexitoso = 0;
                //alert("hola");
                var dataArreglo = new Array();                

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();                                                
                        dataArreglo.push(data.TrackingId);                        
                    }                        
                });    

               
                if (parseInt( dataArreglo.length) == 0)
                {
                    ShowAlert("¡Alerta!", "Seleccione un registro.");
                    return;
                }
               
                if (parseInt( dataArreglo.length) > 1)
                {
                    ShowAlert("¡Alerta!","Seleccione solo un registro");
                    return;
                }
                
                var detenidoId = dataArreglo[0];


                $("#guardatraslado").attr('data-tracking',detenidoId);

                $.ajax({
                      type: "POST",
                      url: "sentence_entrylist.aspx/ValidaRegladeNegocio",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: detenidoId,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {  
                             
                             var nombre = $(this).attr("data-value");
                                $("#IdNombreInterno").text( nombre);
                              Cargatiposalida2("");
                                $("#trasladoInterno-modal").modal("show")
                               
                
                                CargarListado(0);
                                window.emptytableadd = false;
                                window.tableadd.api().ajax.reload();
                               
                          }
                          else
                          {
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención! </strong>" +
                                  "" + resultado.mensaje + "." + "</div > ");
                              ShowAlert("¡Atención!",resultado.mensaje );
                              xdexitoso = 0;
                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                });
              
          
                
            });
            $("body").on("click", ".calificar", function () {
                var dataArreglo = new Array();

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();
                        dataArreglo.push(data.TrackingId);
                    }
                });


                if (parseInt(dataArreglo.length) == 0) {
                    ShowAlert("¡Alerta!", "Seleccione un registro.");
                    return;
                }

                if (parseInt(dataArreglo.length) > 1) {
                    ShowAlert("¡Alerta!", "Seleccione solo un registro.");
                    return;
                }

                var detenidoId = dataArreglo[0];

                var redirect = "";
                redirect="calificacion.aspx?tracking=" + detenidoId  ;
                window.location.assign(redirect); 
            });
            $("body").on("click", ".verExamen", function () {
                var detenidoId = $(this).attr("data-id");
                var examenId = $(this).attr("data-examen-id");
                pdfExamenMedico2(detenidoId, examenId);
            });


            function pdfExamenMedico2(detenidoId, examenId) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/pdf2",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ detenidoId: detenidoId, examenId: examenId }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("body").on("click", ".examen", function ()
            {
                var dataArreglo = new Array();                

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();                                                
                        dataArreglo.push(data.TrackingId);                        
                    }                        
                });    

               
                if (parseInt( dataArreglo.length) == 0)
                {
                    ShowAlert("¡Alerta!", "Seleccione un registro.");
                    return;
                }
               
                if (parseInt( dataArreglo.length) > 1)
                {
                    ShowAlert("¡Alerta!","Seleccione solo un registro.");
                    return;
                }
                
                var detenidoId = dataArreglo[0];
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/validarExamenmedico2",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: detenidoId,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            if (resultado.accion == 0) {
                                ShowAlert("¡Alerta!", resultado.mensaje);
                            }
                            else if (resultado.accion == 1) {
                                pdfExamenMedico2(resultado.detenidoId, 0)
                            }
                            else if (resultado.accion == 2) {
                                var tracking = resultado.detenidoId;

                                $("#form-modal-examen-medico2").modal('show');
                                $("#form-modal-title-examen2").empty();
                                $("#form-modal-title-examen2").html("Exámenes médicos del detenido");

                                responsiveHelper_dt_basicExamen = undefined;
                                $('#dt_basic_examen2').DataTable().destroy();
                                loadTableExamenMedico2(tracking);
                            }
                        }
                        else {
                            ShowError("¡Error!", resultado.mensaje);
                        }

                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible imprimir el examen médico del interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
                //var tracking = detenidoId;

                //                  $("#form-modal-examen-medico").modal('show');
                //                  $("#form-modal-title-examen").empty();
                //                  $("#form-modal-title-examen").html("Examenes médicos del detenido");

                //                  responsiveHelper_dt_basicExamen = undefined;
                //                    $('#dt_basic_examen').DataTable().destroy();
                //                    loadTableExamenMedico2(tracking);
                //                  loadTableExamenMedico(tracking);
                      
            });

            $("body").on("click", "#printcertificadofisio", function () {
                var detenidoId = 1;
                var examenId = $(this).attr("data-examen-id");
                pdfExamenMedico(detenidoId,examenId);
            });
              $("body").on("click", "#printcertificadolesion", function () {
                  var detenidoId = 2;
                var examenId = $(this).attr("data-examen-id");
                pdfExamenMedico(detenidoId,examenId);
            });
              $("body").on("click", "#printcertificadoquimico", function () {
                var detenidoId = 3;
                var examenId = $(this).attr("data-examen-id");
                pdfExamenMedico(detenidoId,examenId);
            });

               function obtenerDatosTabla(imagen) {
                
                var dataArreglo = new Array();
                var pertenencia;

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    
                    var checkit = this.node().childNodes[0].childNodes[0].checked;

                    if (checkit) {
                        pertenencia = {};
                        pertenencia.Fecha = this.node().childNodes[1].childNodes[0].value;
                        pertenencia.Pertenencia = this.node().childNodes[2].childNodes[0].value;
                        pertenencia.Observacion = this.node().childNodes[3].childNodes[0].value;
                        //pertenencia.Bolsa = this.node().childNodes[4].childNodes[0].value;
                        pertenencia.Cantidad = this.node().childNodes[4].childNodes[0].value;
                        pertenencia.Clasificacion = this.node().childNodes[5].childNodes[0].value;
                        pertenencia.Casillero = this.node().childNodes[6].childNodes[0].value;
                        pertenencia.TrackingId = this.node().childNodes[0].childNodes[0].getAttribute("data-tracking");
                        pertenencia.Id = this.node().childNodes[0].childNodes[0].getAttribute("data-id");

                        if (imagen != null && imagen != undefined && imagen != "") {
                            pertenencia.Fotografia = imagen;                            
                        }
                        else {
                            pertenencia.Fotografia = this.node().childNodes[0].childNodes[0].getAttribute("data-foto");                            
                        }

                        dataArreglo.push(pertenencia);                        
                    }                        
                });                     

                return dataArreglo;
            }

            Mostrarbotones();
              function Mostrarbotones() {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/MostrarBotonesPertenencias",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {                            
                           

                             

                            $('#main').waitMe('hide');
                            if (resultado.mensaje == "true")
                            {
                                $("#recibirpertenencia").show();
                                $("#entregarpertenencia").show();

                            }
                            

                        } else {
                            ShowError("¡Error! Algo salió mal. -Don´t show buttons. ", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal. Don´t show buttons. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }





            function Save(datos, interno) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/savepertenencias",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'pertenencias': datos, interno: interno
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {                            
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modalpertenencias").modal("hide");
                            clearModal()

                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");

                            var ruta = resolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            var redirect = "";
                            redirect = " <%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Control_Pertenencias/control_pertenencias.aspx?tracking=" + resultado.tracking + "&valor=" + interno + "&status=" + estatus;
                                 window.location.assign(redirect); 

                        } else {
                            ShowError("¡Error! Algo salió mal. -Don´t save belongings", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal. Don´t save belongings", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }
              function clearModal() {
                $(".input").removeClass('state-success');
                $(".input").removeClass('state-error');
                $(".input").removeClass('valid');
            }

            function pdfExamenMedico(detenidoId,examenId) {
                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ detenidoId: detenidoId, examenId: examenId }),
                    cache: false,
                    success: function (data) {
                       var resultado = data.d;
                        if (resultado.exitoso) {
                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            function loadTableExamenMedico2(id) {
                $('#dt_basic_examen2').dataTable({
                    "lengthMenu": [5, 10, 25, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    destroy: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'

                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basicExamen) {
                            responsiveHelper_dt_basicExamen = new ResponsiveDatatablesHelper($('#dt_basic_examen2'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basicExamen.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basicExamen.respond();
                        $('#dt_basic_examen2').waitMe('hide');
                    },
                    ajax: {
                        type: "POST",
                        url: "sentence_entrylist.aspx/getExamenMedicos2",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_examen2').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.Id = id;
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            name: "Tipo",
                            data: "Tipo"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "ElaboradoPor",
                            data: "ElaboradoPor"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 3,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var verReporte = "";

                                return verReporte = '<a href="javascript:void(0);" style="padding-left: 8.5px;" class="btn btn-primary btn-circle verExamen" data-id = "' + row.Id + '" data-examen-id = "' + row.ExamenMedicoId + '"  title="Ver Examen"><i class="glyphicon glyphicon-eye-open"></i></a>';
                                // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';
                            }
                        }
                    ]
                });
            }

            function loadTableExamenMedico(id) {
                $('#dt_basic_examen').dataTable({
                    "lengthMenu": [5, 10, 25, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    destroy: true,
                    
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                       
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basicExamen) {
                            responsiveHelper_dt_basicExamen = new ResponsiveDatatablesHelper($('#dt_basic_examen'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basicExamen.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basicExamen.respond();
                        $('#dt_basic_examen').waitMe('hide');
                    },
                    ajax: {
                        type: "POST",
                        url: "sentence_entrylist.aspx/getExamenMedicos",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_examen').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.Id = id;
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                       
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "ElaboradoPor",
                            data: "ElaboradoPor"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 2,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var verReporte = "";

                                var action2 = '';
                                var action3 = '';
                                action2='<div class="btn-group" style="width:100px"><button  class="btn btn-primary dropdown-toggle"   data-toggle="dropdown"> <i class="fa fa-user-md"></i>  Examen  médico     &nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span></button><ul class="dropdown-menu">'
                                if (row.CertificadoMedicoPsicofisiologicoId !="0")
                                {
                                    action2 +='<li><a class="btn-sm printcertificadofisio" href="javascript:void(0);" id="printcertificadofisio" data-id="' + row.CertificadoMedicoPsicofisiologicoId + '" data-examen-id="' + row.ExamenMedicoId + '" title = "Psicofisiológico" > <i class="fa fa-print"></i> Psicofisiológico</a></li><li class="divider"></li>'


                                }
                                if (row.CertificadoLesionId !="0")
                                {
                                    action2 +='<li><a class="btn-sm printcertificadolesion" href="javascript:void(0);" id="printcertificadolesion" data-id="' + row.CertificadoLesionId + '" data-examen-id="' + row.ExamenMedicoId + '" title = "Lesiones" > <i class="fa fa-print"></i> Lesiones</a></li><li class="divider"></li>'


                                }
                                 if (row.CertificadoQuimicoId !="0")
                                {
                                    action2 +='<li><a class="btn-sm printcertificadoquimico" href="javascript:void(0);" id="printcertificadoquimico" data-id="' + row.CertificadoQuimicoId + '" data-examen-id="' + row.ExamenMedicoId + '" title = "Químico" > <i class="fa fa-print"></i> Químico</a></li><li class="divider"></li>'


                                }

                                action3 = '</ul></div>';
                                //return verReporte = '<a style="padding-left: 8.5px;" class="btn btn-primary btn-circle verExamen" data-id = "' + row.Id + '" data-examen-id = "' + row.ExamenMedicoId + '"  title="Ver Examen"><i class="glyphicon glyphicon-eye-open"></i></a>';
                                return action2 + action3;
                                // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';
                            }
                        }
                    ]
                });
            }

            $("body").on("click", ".save", function () {
                var tracking = $(this).attr("data-tracking");
                
                if (validarTraslado()) {
                    guardar(tracking);
                    $("#trasladoInterno-modal").modal("hide");
                    clearTraslado();
                }
            });

            $("body").on("click", ".savepertenencia", function () {                
                var hasSelected = false;

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    
                    var checkit = this.node().childNodes[0].childNodes[0].checked;

                    if (checkit) {
                        hasSelected = true;
                    }                        
                });     

                if (hasSelected) {
                    if (validarCamposEnTabla()) {
                        guardarImagenPertenencia();                
                    }                    
                }
                else {
                    ShowAlert("¡Atención!", " No ha seleccionado alguna pertenencia para registrar o actualizar.");
                }
            });



                function guardarImagenPertenencia() {

                var files = $("#ctl00_contenido_fotografia").get(0).files;
                var nombreAvatarAnterior = $("#btnsave").attr("data-fotografia");
                var nombreAvatar = "";

                if (files.length > 0) {

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "../Handlers/FileUploadHandler.ashx?action=4&before=" + nombreAvatarAnterior,
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (Results) {
                            if (Results.exitoso) {
                                nombreAvatar = Results.nombreArchivo;
                                //var id = $("#btnsave").attr("data-id");
                                //var tracking = $("#btnsave").attr("data-tracking");
                                //var habilitado = $("#btnsave").attr("data-habilitado");

         //                       datos = [
         //                           Id = id,
         //                           TrackingId = tracking,
         //                           InternoId = $("#interno").val(),
         //                           Pertenencia = $("#pertenencia").val(),
         //                           Observacion = $("#observacion").val(),
         //                           Clasificacion = $("#clasificacion").val(),
         //                           Bolsa = $("#bolsa").val(),
         //                           Cantidad = $("#cantidad").val(),
         //                           Fotografia = nombreAvatar,
									//Casillero = $("#casillero").val(),
         //                       ];

                                var datos = obtenerDatosTabla(nombreAvatar);

                                Save(datos, $("#interno").val());
                            }
                            else {
                                ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                            }
                        },
                        error: function (err) {
                            ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else {

                    //var id = $("#btnsave").attr("data-id");
                    //var tracking = $("#btnsave").attr("data-tracking");
                    //var habilitado = $("#btnsave").attr("data-habilitado");

                    //datos = [
                    //    Id = id,
                    //    TrackingId = tracking,
                    //    InternoId = $("#interno").val(),
                    //    Pertenencia = $("#pertenencia").val(),
                    //    Observacion = $("#observacion").val(),
                    //    Clasificacion = $("#clasificacion").val(),
                    //    Bolsa = $("#bolsa").val(),
                    //    Cantidad = $("#cantidad").val(),
                    //    Fotografia = (nombreAvatarAnterior == undefined) ? "" : nombreAvatarAnterior,
                    //];
                    var foto = (nombreAvatarAnterior == undefined) ? "" : nombreAvatarAnterior                    
                    var datos = obtenerDatosTabla(foto);
                    

                    Save(datos, $("#interno").val());
                }
            }


            function validarCamposEnTabla() {
                var isValid = true;

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    
                    var checkit = this.node().childNodes[0].childNodes[0].checked;

                    if (checkit) {
                        var nombre = this.node().childNodes[2].childNodes[0].getAttribute("data-required");                        
                        var observacion = this.node().childNodes[3].childNodes[0].getAttribute("data-required");
                      //  var bolsa = this.node().childNodes[4].childNodes[0].getAttribute("data-required");                        
                        var cantidad = this.node().childNodes[4].childNodes[0].getAttribute("data-required");                        
                        var clasificacion = this.node().childNodes[5].childNodes[0].getAttribute("data-required");                        
                        var casillero = this.node().childNodes[6].childNodes[0].getAttribute("data-required");    

                        if (nombre === "true") {                            
                            if (this.node().childNodes[2].childNodes[0].value === "" ||
                                this.node().childNodes[2].childNodes[0].value === undefined ||
                                this.node().childNodes[2].childNodes[0].value === null) {                                
                                this.node().childNodes[2].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Pertenencia", "El campo pertenencia es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[2].childNodes[0].removeAttribute('class');
                            }
                        }                        

                        if (observacion === "true") {                            
                            if (this.node().childNodes[3].childNodes[0].value === "" ||
                                this.node().childNodes[3].childNodes[0].value === undefined ||
                                this.node().childNodes[3].childNodes[0].value === null) {                                
                                this.node().childNodes[3].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Observación", "El campo observación es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[3].childNodes[0].removeAttribute('class');
                            }
                        }

                        //if (bolsa === "true") {
                        //    var val = parseInt(this.node().childNodes[4].childNodes[0].value);                                                        

                        //    if (isNaN(val)) {
                        //        this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                        //        ShowError("Bolsa", "El campo bolsa tiene un valor incorrecto.");
                        //        isValid = false;                                
                        //    }                            

                        //    if (val <= 0) {
                        //        this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                        //        ShowError("Bolsa", "El campo bolsa debe tener un valor mayor a cero.");
                        //        isValid = false;
                        //    }


                        //    if (!isNaN(val) && val > 0) {
                        //        this.node().childNodes[4].childNodes[0].removeAttribute('class');
                        //    }
                        //}

                        if (cantidad === "true") {                            
                            var val = parseInt(this.node().childNodes[4].childNodes[0].value);                                                        

                            if (isNaN(val)) {
                                this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Cantidad", "El campo cantidad es obligatorio.");
                                isValid = false;                                
                            }                            

                            if (val <= 0) {
                                this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Cantidad", "El campo cantidad debe tener un valor mayor a cero.");
                                isValid = false;
                            }


                            if (!isNaN(val) && val > 0) {
                                this.node().childNodes[4].childNodes[0].removeAttribute('class');
                            }
                        }

                        if (clasificacion === "true") {                            
                            if (this.node().childNodes[5].childNodes[0].value === "0") {                                
                                this.node().childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Clasificación", "El campo clasificación es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[5].childNodes[0].removeAttribute('class');
                            }
                        }              

                        if (casillero === "true") {                            
                            if (this.node().childNodes[6].childNodes[0].value === "0") {                                
                                this.node().childNodes[6].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Casillero", "El campo casillero es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[6].childNodes[0].removeAttribute('class');
                            }
                        }

                        var file = document.getElementById('<% = fotografia.ClientID %>').value;
                if (file != null && file != '') {

                    if (!validaImagen(file)) {
                        ShowError("Fotografía", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        isValid = false;

                    }
                    else {
                        var validar = $("#Validar3").val();
                        var validar2 = $("#Validar4").val();


                        if ($("#Max").val() != "0") {
                            if (validar == 'false') {
                                ShowError("Fotografía", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                isValid = false;
                            }

                        }
                        if ($("#Min").val() != "0") {
                            var s = $("#Min").val() * 1024;
                            if (validar2 == 'false') {
                                ShowError("Fotografía", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                isValid = false;
                            }
                        }
                    }
                }

                    }                        
                });

                return isValid;
            }

            $("#filterYear").change(function () {
                startLoading();
                responsiveHelper_dt_basicadd = undefined;
                $('#dt_basicadd').DataTable().destroy();
                loadReportStray();
            });  

            $("body").on("click", ".add", function () {
                $("#filterYear").empty();
                startLoading();
                var year = new Date().getFullYear();
                getYearsModal(year);
                $("#add-modal").modal("show");
                responsiveHelper_dt_basicadd = undefined;
                $('#dt_basicadd').DataTable().destroy();
                loadReportStray();
            });

            function validar() {

                var esvalido = true;

                if ($("#ctl00_contenido_fecha").val() == null || $("#ctl00_contenido_fecha").val().split(" ").join("") == "") {
                    ShowError("Fecha a partir", "La fecha a partir es obligatoria.");
                    $('#ctl00_contenido_fecha').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_fecha').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_fecha').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_fecha').addClass('valid');
                }

                if ($("#ctl00_contenido_anoss").val() == null || $("#ctl00_contenido_anoss").val().split(" ").join("") == "") {
                    ShowError("Sentenca años", "Los años son obligatorios.");
                    $('#ctl00_contenido_anoss').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_anoss').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_anoss').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_anoss').addClass('valid');
                }

                if ($("#ctl00_contenido_mesess").val() == null || $("#ctl00_contenido_mesess").val().split(" ").join("") == "") {
                    ShowError("Sentenca meses", "Los meses son obligatorios.");;
                    $('#ctl00_contenido_mesess').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_mesess').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_mesess').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_mesess').addClass('valid');
                }

                if ($("#ctl00_contenido_diass").val() == null || $("#ctl00_contenido_diass").val().split(" ").join("") == "") {
                    ShowError("Sentenca días", "Los días son obligatorios.");
                    $('#ctl00_contenido_diass').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_diass').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_diass').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_diass').addClass('valid');
                }

                if ($("#ctl00_contenido_anosa").val() == null || $("#ctl00_contenido_anosa").val().split(" ").join("") == "") {
                    ShowError("Abono años", "Los años son obligatorios.");
                    $('#ctl00_contenido_anosa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_anosa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_anosa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_anosa').addClass('valid');
                }

                if ($("#ctl00_contenido_mesesa").val() == null || $("#ctl00_contenido_mesesa").val().split(" ").join("") == "") {
                    ShowError("Abono meses", "Los meses son obligatorios.");;
                    $('#ctl00_contenido_mesesa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_mesesa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_mesesa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_mesesa').addClass('valid');
                }

                if ($("#ctl00_contenido_diasa").val() == null || $("#ctl00_contenido_diasa").val().split(" ").join("") == "") {
                    ShowError("Abono días", "Los días son obligatorios.");
                    $('#ctl00_contenido_diasa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_diasa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_diasa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_diasa').addClass('valid');
                }

                return esvalido;

            }

            function limpiar() {
                $('#ctl00_contenido_fecha').parent().removeClass('state-success');
                $('#ctl00_contenido_fecha').parent().removeClass("state-error");
                $('#ctl00_contenido_anoss').parent().removeClass('state-success');
                $('#ctl00_contenido_anoss').parent().removeClass("state-error");
                $('#ctl00_contenido_mesess').parent().removeClass('state-success');
                $('#ctl00_contenido_mesess').parent().removeClass("state-error");
                $('#ctl00_contenido_diass').parent().removeClass('state-success');
                $('#ctl00_contenido_diass').parent().removeClass("state-error");
                $('#ctl00_contenido_anosa').parent().removeClass('state-success');
                $('#ctl00_contenido_anosa').parent().removeClass("state-error");
                $('#ctl00_contenido_mesesa').parent().removeClass('state-success');
                $('#ctl00_contenido_mesesa').parent().removeClass("state-error");
                $('#ctl00_contenido_diasa').parent().removeClass('state-success');
                $('#ctl00_contenido_diasa').parent().removeClass("state-error");
            }



            var responsiveHelper_dt_basicadd = undefined;
            var breakpointAddDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };            

            function loadReportStray() {
                $('#dt_basicadd').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    destroy: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basicadd) {
                            responsiveHelper_dt_basicadd = new ResponsiveDatatablesHelper($('#dt_basicadd'), breakpointAddDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basicadd.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basicadd.respond();
                        $('#dt_basicadd').waitMe('hide');
                    },
                    ajax: {
                        url: "sentence_entrylist.aspx/getdetenidosexistenciaPrincipal",
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: function (d) {                            
                            d.emptytableadd = false;
                            d.year = $("#filterYear").val() || "";
                            d.pages = $('#dt_basic').DataTable().page.info().page || "";
                            return JSON.stringify(d);
                        },
                        dataSrc: "data",
                        dataFilter: function (data) {
                            var json = jQuery.parseJSON(data);
                            json.recordsTotal = json.d.recordsTotal;
                            json.recordsFiltered = json.d.recordsFiltered;
                            json.data = json.d.data;
                            endLoading();
                            return JSON.stringify(json);
                        }
                    },
                    columns: [
                        null,
                        null,
                        {
                            name: "Expediente",
                            data: "Expediente",
                            orderable: false
                        },
                        null,
                    ],
                    columnDefs: [{
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return row.Nombre + " " + row.Paterno + " " + row.Materno;
                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var edit = "edit";
                            var editar = "";
                            var nombre = row.Nombre + " " + row.Paterno + " " + row.Materno;
                            //if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-success btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=1' + '" title="Agregar sentencia/proceso"><i class="glyphicon glyphicon-plus"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a style="padding-left: 8.5px;" class="btn btn-primary btn-circle printpdf" href="javascript:void(0);" data-nombre="' + nombre + '" data-avatar= "' + row.RutaImagen
                                + '" data-fn= "' + row.FechaNacimiento + '" data-edad="' + row.Edad + '" data-alias= "' + row.AliasInterno + '" data-tracking= "' + row.TrackingId + '" data-id= "' + row.Id + '" title="Generar reporte de extravío"><i class="fa fa-file-pdf-o"></i></a>';

                            return editar;

                        }
                    }

                    ]
                });
            }

            if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                $("#addentry").show();
            }


            $("body").on("click", ".printpdf", function () {
                LimpiarModalReporte();

                var nombre = $(this).attr("data-nombre");
                $("#nombrereportepdf").val(nombre);
                var rutaavatar = $(this).attr("data-avatar");
            
                var imagenAvatar = ResolveUrl(rutaavatar.trim());
                $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                var tracking = $(this).attr("data-tracking");
                $("#trackingidpdf").val(tracking);
                var imagenavatar2 = "";
                imagenavatar2 = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png";
                if (rutaavatar != "") {
                   
                    $('#avatar').attr("src",imagenAvatar);

                }
                else {
                    $('#avatar').attr("src", imagenavatar2);
                }
                var alias = $(this).attr("data-alias");
                $("#aliasreportepdf").val(alias);

                var fechanacimiento = $(this).attr("data-fn");
                var today = new Date();
                var edad = moment(today).diff(moment(fechanacimiento), 'years');
                edad = $(this).attr("data-edad");
                if (edad > 0) {
                    $("#edadreportepdf").val(edad);
                }
                else {
                    var edadEvento = $(this).attr("data-edad");
                    var ed = "";
                    if (edadEvento != "0") {
                        ed = edadEvento;
                    }
                    $("#edadreportepdf").val(ed);
                }
                var Id = $(this).attr("data-id");
                $("#idpdf").val(Id);
                $("#radioExtraviadopdf").prop('checked', 'checked')
                $("#printpdf-modal-title").html('<i class="fa fa-file-pdf-o" +=""></i> Reporte de extravío');
                $("#printpdf-modal").modal("show");
            });

            function LimpiarModalReporte() {
                $('#edadreportepdf').parent().removeClass('state-success');
                $('#edadreportepdf').parent().removeClass("state-error");
                $('#aliasreportepdf').parent().removeClass('state-success');
                $('#aliasreportepdf').parent().removeClass("state-error");
                $('#edadreportepdf').val("");
                $('#aliasreportepdf').val("");
                $('#nombrereportepdf').val("");
                $('#violentopdf').val("");
                $('#enfermopdf').val("");
                $('#comentariospdf').val("");
                $('#trackingidpdf').val("");
                $('#idpdf').val("");
                $("#radioBuscandopdf").prop("checked", false);
                $("#radioExtraviadopdf").prop("checked", false);
                $("#radioInformacionpdf").prop("checked", false);
                $('#institucionpdf').val("");
                $('#direccionpdf').val("");
                $('#telefonopdf').val("");
                $('#senaspdf').val("");

            }
                function validarTraslado() {
                var esvalido = true;

                if ($("#ctl00_contenido_dropdown").val() == "0") {
                    ShowError("Institución a disposición", "La Institución a disposición es obligatoria");
                    $('#ctl00_contenido_dropdown').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_dropdown').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_dropdown').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_dropdown').addClass('valid');
                    }

                     if ($("#combotiposalida2").val() == "0") {
                    ShowError("Tipo salida", "El tipo salida es obligatorio");
                    $('#combotiposalida2').parent().removeClass('state-success').addClass("state-error");
                    $('#combotiposalida2').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#combotiposalida2').parent().removeClass("state-error").addClass('state-success');
                    $('#combotiposalida2').addClass('valid');
                    }

                     if ($("#fundamentosalida").val() == "") {
                    ShowError("Fundamento", "El fundamento es obligatorio");
                    $('#fundamentosalida').parent().removeClass('state-success').addClass("state-error");
                    $('#fundamentosalida').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fundamentosalida').parent().removeClass("state-error").addClass('state-success');
                    $('#fundamentosalida').addClass('valid');
                }
                return esvalido;
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".printpdfbtn", function () {
                if (validareporteextravio()) {
                    generareporteextravio();
                }
            });

            function validareporteextravio() {
                var esvalido = true;

                if ($("#edadreportepdf").val() == "") {
                    ShowError("Edad", "La edad es obligatoria.");
                    $('#edadreportepdf').parent().removeClass('state-success').addClass("state-error");
                    $('#edadreportepdf').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#edadreportepdf').parent().removeClass("state-error").addClass('state-success');
                    $('#edadreportepdf').addClass('valid');
                }

                if ($("#aliasreportepdf").val() == "") {
                    ShowError("Alias", "El alias es obligatorio.");
                    $('#aliasreportepdf').parent().removeClass('state-success').addClass("state-error");
                    $('#aliasreportepdf').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#aliasreportepdf').parent().removeClass("state-error").addClass('state-success');
                    $('#aliasreportepdf').addClass('valid');
                }

                return esvalido;
            }

            function obtenerdatosreporteextravio() {
                var titulo = "";

                if (document.getElementById('radioBuscandopdf').checked) {
                    titulo = document.getElementById('radioBuscandopdf').value;
                }

                if (document.getElementById('radioExtraviadopdf').checked) {
                    titulo = document.getElementById('radioExtraviadopdf').value;
                }

                if (document.getElementById('radioInformacionpdf').checked) {
                    titulo = document.getElementById('radioInformacionpdf').value;
                }

                var datos = [
                    tracking = $('#trackingidpdf').val(),
                    id = $('#idpdf').val(),
                    nombre = $("#nombrereportepdf").val(),
                    edad = $("#edadreportepdf").val(),
                    titulo = titulo,
                    alias = $("#aliasreportepdf").val(),
                    violento = $("#violentopdf").val(),
                    enfermo = $("#enfermopdf").val(),
                    comentarios = $("#comentariospdf").val(),
                    institucion = $('#institucionpdf').val(),
                    direccion = $('#direccionpdf').val(),
                    telefono = $('#telefonopdf').val(),
                    senas = $('#senaspdf').val()
                ]

                return datos;
            }

            function generareporteextravio() {

                var datos = obtenerdatosreporteextravio();

                $.ajax({

                    type: "POST",
                    url: "sentence_entrylist.aspx/reporteextravio",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            open(resultado.ubicacionarchivo.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            ShowError("¡Error!", resultado.mensaje);
                        }

                        $("#printpdf-modal").modal("hide");
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible generar el reporte de extravío. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        $("#printpdf-modal").modal("hide");
                    }
                });

            }
            //function ObtenerValoresTraslado() {                                
            //    var traslado = {                    
            //        MotivoDetencionId: $("#ctl00_contenido_dropdown").val(),
            //        Tracking
            //        InternoId: $('#ctl00_contenido_hideid').val()
                    
            //    };
            //    return contrato;
            //}
            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

            function clearTraslado() {
                $(".select").removeClass('state-success');
                $(".select").removeClass('state-error');
                $(".select").removeClass('valid');
                $('#fundamentosalida').parent().removeClass('state-success');
                $('#fundamentosalida').parent().removeClass('state-error');
                $('#fundamentosalida').val("");
            }

            function guardar(tracking) {
                startLoading();
                var traslado = {
                    DelegacionId: $("#ctl00_contenido_dropdown").val(),
                    InternoId: $('#ctl00_contenido_hideid').val(),
                    TiposalidaId: $("#combotiposalida2").val(),
                    Fundamento: $("#fundamentosalida").val(),
                    Horas: $("#hours").val(),
                
                    TrackingId:  tracking
                   
                };                

                $.ajax({
                    type: "POST",
                    url: "sentence_entrylist.aspx/trasladoSave",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'traslado': traslado }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {                            
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "  " + resultado.mensaje + ".", "<br /></div>");
                                                                          
                            ShowSuccess("¡Bien hecho!", "  " + resultado.mensaje+"." );                                                  
                            setTimeout(hideMessage, hideTime);
                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                    ;
                              var param = RequestQueryString("tracking");
                            if (param != undefined) {
                                var url = "MotivoDetencionInterno.aspx?tracking=" + param ;
                                $(location).attr('href', url);
                            }
                            $("#dt_basic").DataTable().ajax.reload();
                        }
                        else {
                            $('#main').waitMe('hide');

                            if (resultado.alerta) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención! </strong>" +
                                 resultado.mensaje + "</div>");
                                ShowAlert("Traslado..", resultado.mensaje);
                                setTimeout(hideMessage, hideTime);
                                return;
                            }

                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            if (resultado.mensaje == "El traslado ya esta registrado") {
                                ShowError("Traslado..", resultado.mensaje);
                                setTimeout(hideMessage, hideTime);
                            }
                            else
                            {
                                ShowError("¡Error! Algo salió mal. Don´t save transfer. ", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                            }
                        }
                    }
                });
            }

             $("body").on("click", ".datos", function () {
                 var id = $(this).attr("data-id");
                 var hora = $(this).attr("data-hora");
                CargarDatosPersonales(id, hora);
                
            });



            
            function RevisaRegladeNegocio(trackingid) {
                startLoading();
                  
              }
            function CargarDatosPersonales(trackingid, hora) {
                startLoading();
                  $.ajax({
                      type: "POST",
                      url: "sentence_entrylist.aspx/getdatos",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {                              
                            $("#fechaInfo").val(resultado.obj.Registro);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Institucion);
                            $("#fechaSalidaInfo").val(resultado.obj.Salida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            $("#motivoInfo").val(resultado.obj.Motivo_Evento);
                              if (resultado.obj.Estatura != 0)
                                  $("#estaturaInfo").val(resultado.obj.Estatura)
                              else
                                  $("#estaturaInfo").val("")
                              if (resultado.obj.Peso != 0)
                                  $("#pesoInfo").val(resultado.obj.Peso)
                              else
                                  $("#pesoInfo").val("")

                                if (resultado.obj.RutaImagen == "")
                              {
                                  resultado.obj.RutaImagen = rutaDefaultServer;
                              }
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            
                              
                            $('#imgInfo').attr("src", imagenAvatar);
                              
                              var num = parseInt(hora);
                              var x = "";
                              if (num > 0)                                 
                                  x = 'Restan: '+ hora
                            
                              else                                 
                                  x = 'Finalizó hace ' + hora
                              $('#salida').val(x);
                              
                              $('#domicilio').val(resultado.obj.Domicilio);
                                  $("#datospersonales-modal").modal("show");
                          } else {
                              ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
            }


            $("#btnCancelHistorial").parent().css("margin", "10px");
            
        });

        function CargarMapa(latitud, longitud) {
            //if ($('#latitud').val() == "" || $('#longitud').val() == "") {
            setTimeout(function () {

                mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
                var coordinates = document.getElementById('coordinates');
                function onDragEnd() {
                    var lngLat = marker.getLngLat();
                    coordinates.style.display = 'block';
                    coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                    let x = lngLat.lng;
                    x = x.toString();
                    x = x.substring(0, x.length - 3);
                    $('#longitud').val(x);

                    let y = lngLat.lat;
                    y = y.toString();
                    y = y.substring(0, y.length - 3);
                    $('#latitud').val(y);
                }

                var map = new mapboxgl.Map({
                    container: 'mapid',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [longitud, latitud],
                    zoom: 16
                });
                var marker = new mapboxgl.Marker({
                    draggable: true
                }).setLngLat([longitud, latitud]).addTo(map);
                map.addControl(new mapboxgl.NavigationControl());
                marker.on('dragend', onDragEnd);
            }, 500);
        }
        $("#btnBuscarInfo").click(
            function () {

                $("#dt_basic").DataTable().ajax.reload();

            }
        );

    </script>


    <script>
        <%--function loadMap() {
            if ($('#<%= latitudd.ClientID %>').val() == "" || $('#<%= longitudd.ClientID %>').val() == "") {
                setTimeout(function () {
                    mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
                    var coordinates = document.getElementById('coordinates');
                    function onDragEnd() {
                        var lngLat = marker.getLngLat();
                        coordinates.style.display = 'block';
                        coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                        let x = lngLat.lng;
                        x = x.toString();
                        x = x.substring(0, x.length - 3);
                        $('#<%= longitudd.ClientID %>').val(x);

                        x = lngLat.lat;
                        x = x.toString();
                        x = x.substring(0, x.length - 3);
                        $('#<%= latitudd.ClientID %>').val(x);
                    }

                    var map = new mapboxgl.Map({
                        container: 'mapid',
                        style: 'mapbox://styles/mapbox/streets-v11',
                        center: [-89.61086650942, 20.97689912377],
                        zoom: 16
                    });
                    var marker = new mapboxgl.Marker({
                        draggable: true
                    })
                        .setLngLat([-89.61086650942, 20.97689912377])
                        .addTo(map);
                    map.addControl(new mapboxgl.NavigationControl());
                    marker.on('dragend', onDragEnd);
                }, 1000);
            }

            else {

                setTimeout(function () {
                    var mymap = L.map('mapid').setView([$('#<%= latitudd.ClientID %>').val(), $('#<%= longitudd.ClientID %>').val()], 13);



                    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        maxZoom: 18,
                        id: 'mapbox.streets',
                        accessToken: 'pk.eyJ1IjoiYXNlc29ydXNpdGVjaCIsImEiOiJjanhtbzh6aW0wNXIwM2NvNjVweHlnd2JxIn0.YbBuq1IIm9cVDgg64NaxcQ'
                    }).addTo(mymap);


                    var marker = L.marker([$('#<%= latitudd.ClientID %>').val(), $('#<%= longitudd.ClientID %>').val()]).addTo(mymap);
                }, 1000);
            }
        }--%>

        function reload() {
            $("dt_basicAlias").DataTable().ajax.reload();
            $("dt_basic_evento").DataTable().ajax.reload();
            $("dt_basic_lugar").DataTable().ajax.reload();
            $("dt_basic_evento_AW").DataTable().ajax.reload();
            $("dt_basic_lugar_AW").DataTable().ajax.reload();            
        }
    </script>
</asp:Content>
