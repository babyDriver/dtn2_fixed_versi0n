﻿using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;
using System.Linq;

namespace Web.Application.UsersActivity
{
    public partial class UsersActivity : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (!emptytable)
                {

                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                    List<Where> where = new List<Where>();
                    //where.Add(new Where("ES.Activo", "1"));
                    //where.Add(new Where("C.Activo", "1"));

                    

                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    
                    //where.Add(new Where("DD.Activo", "1"));
                    // where.Add(new Where("ES.estatus", "1"));
                    //where.Add(new Where("ifnull(A.Id,0)", "0"));

                    Query query = new Query
                    {
                        select = new List<string> {
                        "A.Usuario",
                        "A.Nombre",
                        "A.ApellidoPaterno",
                        "A.ApellidoMaterno",
                        "A.NombreCompleto",
                        "A.Rol",
                        "A.SubContratos",
                        "date_format(A.Fechahora,'%Y-%m-%d %H:%i:%S') Fecha", 
                    },
                        from = new Table("VActividadUsuarios", "A"),
                        
                        wheres = where
                    };

                    DataTables dt = new DataTables(mysqlConnection);
                    var data = dt.Generar(query, draw, start, length, search, order, columns);
                    return data;
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }

        }

    }
}