using System;
using System.Collections.Generic;
using System.Linq;
using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using Entity.Util;


namespace Web.Application.Sentence
{
    public partial class celdas : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tbFechaInicio.Text = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                tbFechaFin.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            validatelogin();
            getPermisos();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Control de celdas" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //Consultar
                        this.WERQEQ.Value = permisos.Consultar.ToString().ToLower();

                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable,string CeldaId, string fechaInicio, string fechaFin)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Estatus", "<>", "2"));
                        where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("E.Tipo", contratoUsuario.Tipo));
                        where.Add(new Where("cast(E.Fecha as DATE)", ">=", fechaInicio));
                        where.Add(new Where("cast(E.Fecha as DATE)", "<=", fechaFin));
                        //where.Add(new Where("E.Fecha", ">=", DateTime.Now("dd/mm/yy")));
                        if (Convert.ToInt32(CeldaId)>0)
                        {
                            where.Add(new Where("M.CeldaId", CeldaId));
                            where.Add(new Where("M.Activo", "1"));
                            where.Add(new Where("M.TipomovimientoId", "1"));
                        }
                        if(Convert.ToInt32(CeldaId)==-1)
                        {
                            //   where.Add(new Where("ifnull(M.CeldaId,0)", "0"));
                            where.Add(new Where("ifnull(M.Activo,1)", "1"));
                            where.Add(new Where("ifnull(M.TipomovimientoId,2)", "2"));
                        }
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        //where.Add(new Where("I.ContratoId", Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]).ToString()));

                        //Prueba de IN

                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                        //string IdsContratos = "(";

                        int i = 0;

                        var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));
                        where.Add(new Where("ifnull(Saj.TiposalidaId,0)", "<>", tipo.Id.ToString()));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        Query query = new Query
                        {
                            select = new List<string> {
                        " distinct I.Id",
                        "I.TrackingId",
                        "I.Nombre",
                        "TRIM(I.Paterno) Paterno",
                        "TRIM(I.Materno) Materno",
                        "I.RutaImagen",
                        "CAST(ifnull(ExpedienteoOriginal,Expediente) AS unsigned) Expediente",
                        "E.NCP",
                        "E.Estatus",
                        "E.Activo",
                        "E.TrackingId TrackingIdEstatus" ,
                        "ES.Nombre EstatusNombre",
                        "ifnull(M.CeldaId,0) CeldaId",
                        "sd.Nombre Situacion",
                        "ifnull(ca.Id,0) CalificacionId",
                        "concat(trim(I.Nombre),' ',trim(Paterno),' ',trim(Materno)) NombreCompleto"
                    },
                            from = new Table("Detenido", "I"),
                            joins = new List<Join>
                    {
                        new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                        new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"),
                        new Join(new Table("contrato", "c"), "c.id  = E.ContratoId"),
                        new Join(new Table("Movimiento", "M"), "E.Id  = M.DetalleDetencionId"),
                          new Join(new Table("calificacion", "ca"), "ca.InternoId  = E.Id"),
                        new Join(new Table("situacion_detenido", "sd"), "sd.Id  = ca.SituacionId"),
                        new Join(new Table("(Select Id,ifnull(Expediente,0) as ExpedienteoOriginal,DetalleDetencionId from historico_agrupado_detenido)", "H"), "H.DetalleDetencionId=E.id"),
                        new Join(new Table("salidaefectuadajuez", "Saj"), "E.id=Saj.DetalleDetencionId and Saj.Activo=1", "LEFT")

                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable dt_ = new DataTable();
                            dt_=dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string blockitem(string trackingid)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Eliminar)
                {
                 
                    return JsonConvert.SerializeObject(new { exitoso = true });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                Entity.Colonia colonia = domicilio != null ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;

                DateTime nacimiento;
                int edad = 0;
                if (general != null)
                {
                    nacimiento = general.FechaNacimineto;
                    edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = "Calle: " + domicilio.Calle + " #" + domicilio.Numero + "";

                }

                string strColonia = "";
                if (colonia != null)
                {
                    strColonia = ",Colonia: " + colonia.Asentamiento + " CP:" + colonia.CodigoPostal + "";

                }

                string domCompleto = strDomicilio + "" + strColonia;

                object obj = null;

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = detalleDetencion.Fecha.Value;
                TimeSpan horasRestantes;
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                var situacion = calificacion != null ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;

                if (calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }
                else
                {
                    fechaSalidaAdd = fechaSalida.AddHours(36);
                    horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                }

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = situacion != null ? situacion.Nombre : "",
                    Domicilio = domCompleto,
                    RutaImagen = interno.RutaImagen!=null? interno.RutaImagen:"",
                    Registro = detalleDetencion.Fecha.Value.ToString("dd-MM-yyyy HH:mm:ss"),
                    Salida = fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss"),
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                    horasRestantes.TotalHours
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }


        [WebMethod]
        public static DataTable getinterno(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytableadd, string fechaInicio, string fechaFin)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {
                    if (!emptytableadd)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("cast(E.Fecha as DATE)", ">=", fechaInicio));
                        where.Add(new Where("cast(E.Fecha as DATE)", "<=", fechaFin));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        where.Add(new Where("E.Estatus", "<>", "2"));

                        Query query = new Query
                        {
                            select = new List<string> {
                        "I.Id",
                        "I.TrackingId",
                        "I.Nombre",
                        "I.Paterno",
                        "I.Materno",
                        "E.Expediente",
                        "E.NCP",
                        "E.Estatus",
                        "E.Activo",
                        "E.TrackingId TrackingIdEstatus" ,
                        "I.RutaImagen",
                        "IFNULL(a.alias, '') AliasInterno",
                        "IFNULL(G.fechanacimineto, NOW()) FechaNacimiento",
                        "sd.Nombre Situacion",
                        "ifnull(ca.Id,0) CalificacionId"
                    },
                            from = new Table("Detenido", "I"),
                            joins = new List<Join>
                    {
                        new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                        new Join(new Table("(select detenidoid, group_concat(Alias) alias from Alias group by detenidoid)", "A"), "I.id  = A.detenidoid", "LEFT OUTER"),
                        new Join(new Table("general", "G"), "I.id  = G.detenidoid", "LEFT OUTER"),
                      
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        [WebMethod]
        public static string reporteextravio(string[] datos)
        {
            var serializedObject = string.Empty;
            try
            {
                var obj = ControlPDF.GenerarPDFReporteExtravio(datos);
                serializedObject = JsonConvert.SerializeObject(obj);
                return serializedObject;
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getproceso(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();


                        where.Add(new Where("E.Activo", "1"));

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");


                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }


                        Query query = new Query
                        {
                            select = new List<string> {
                        "P.Id",
                        "P.TrackingId",
                        "P.Proceso",
                        "P.Habilitado",
                        "I.Id IdInterno",
                        "I.TrackingId TrackingIdInterno",
                        "I.Nombre",
                        "I.Paterno",
                        "I.Materno",
                        "E.Expediente",
                        "E.NCP",
                        "E.Estatus",
                        "E.TrackingId TrackingIdEstatus" ,
                    },
                            from = new Table("proceso", "P"),
                            joins = new List<Join>
                    {
                        new Join(new Table("detenido", "I"), "I.id  = P.internoid"),
                        new Join(new Table("detalle_detencion", "E"), "I.id  = E.internoid"),
                    },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var a = dt.Generar(query, draw, start, length, search, order, columns);
                        return a;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }
        [WebMethod]
        public static List<Combo> getListadoCombo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            List<Entity.Celda> listado = ControlCelda.ObtenerTodos();
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);


            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x => x.ContratoId == contratoUsuario.IdContrato && x.Tipo == contratoUsuario.Tipo && x.Activo  && x.Habilitado))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }
        [WebMethod]
        public static Object GetCeldaDetalleById(string _id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {
                    var listadetalles = ControlCeldaDetalle.GetDetall(Convert.ToInt32(_id));

                    int Capacidad = 0;
                    int porcentajeOcupacion = 0;
                    int cantidadDetenidos = 0;
                    if (listadetalles != null)
                    {
                        Capacidad = listadetalles.FirstOrDefault().Capacidad;
                        int cantidadEntrada = listadetalles.Where(t => t.TipoMovimientoId == 1 && t.Estatus==1 ).Count();
                        int cantidadSalida = listadetalles.Where(t => t.TipoMovimientoId == 2 && t.Estatus == 1).Count();
                        porcentajeOcupacion = ((cantidadEntrada - cantidadSalida) * 100) / Capacidad;
                        cantidadDetenidos = cantidadEntrada - cantidadSalida;
                    }



                    object obj = null;


                    obj = new
                    {
                        CapacidadOcupacion = Capacidad != null ? Capacidad.ToString() : "0",
                        PorcentajeOcupacion = porcentajeOcupacion != null ? porcentajeOcupacion.ToString() : "0",
                        CantidadDetenidos = cantidadDetenidos,

                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj }); ;
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para listar la información." };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }

        }

        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {

                //if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Registro en barandilla" }).Consultar)
                //{
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencionDetenido = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.InformacionDeDetencion infoDetencion = new Entity.InformacionDeDetencion();
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                try
                {
                    infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);


                }
                catch (Exception ex)
                {

                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
                }
                var colonia = (infoDetencion != null) ? ControlColonia.ObtenerPorId(infoDetencion.ColoniaId) : null;
                var municipio = (infoDetencion != null) ? ControlMunicipio.Obtener(colonia != null ? colonia.IdMunicipio : 0) : null;
                var estado = (infoDetencion != null) ? ControlEstado.Obtener(municipio != null ? municipio.EstadoId : 0) : null;
                var pais = (infoDetencion != null) ? ControlCatalogo.Obtener(estado != null ? estado.IdPais : 0, 45) : null;
                var evento = (infoDetencion != null) ? ControlEvento.ObtenerById(infoDetencion.IdEvento) : null;
                var antropometria = (infoDetencion != null) ? ControlAntropometria.ObtenerPorDetenidoId(interno.Id) : null;

                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);

                Entity.Domicilio nacimiento = ControlDomicilio.ObtenerPorId(interno.DomicilioNId);
                Entity.Catalogo sexo = new Entity.Catalogo();
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                int edad = 0;
                if (general != null)
                {
                    if (general.FechaNacimineto != DateTime.MinValue)
                        edad = CalcularEdad(general.FechaNacimineto);

                    sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                }

                var llamada = ControlLlamada.ObtenerById(evento.LlamadaId);
                var unidad = ControlCatalogo.Obtener(infoDetencion.UnidadId, 82);
                var responsable = ControlCatalogo.Obtener(infoDetencion.ResponsableId, 83);
                var motivo = "";
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);
                if(detenidoevento!=null)
                {
                    var m = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                    if (m != null)
                    {
                        motivo = m.NombreMotivoLlamada;
                    }
                }
                
                var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                Entity.Municipio municipio2 = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;

                var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                Entity.Colonia colonia2 = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;
                object obj = null;

                DateTime fechaSalidaAdd = new DateTime();
                DateTime fechaSalida = evento.HoraYFecha;

                if (calificacion != null)
                {
                    fechaSalidaAdd = fechaSalida.AddHours(calificacion.TotalHoras);
                }
                else
                {
                    //fechaSalidaAdd = fechaSalida.AddHours(36);
                    fechaSalidaAdd = DateTime.MinValue;
                }
                if (interno != null)
                {
                    if (string.IsNullOrEmpty(interno.RutaImagen))
                    {
                        interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                    }
                }

                obj = new
                {
                    Id = interno != null ? interno.Id.ToString() : "",
                    TrackingId = interno != null ? interno.TrackingId.ToString() : "",
                    Expediente = (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                    Centro = institucion != null ? institucion.Nombre : "Centro no registrado",
                    Fecha = detalleDetencion != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd-MM-yyyy HH:mm:ss") : "",
                    Nombre = (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                    RutaImagen = (interno != null) ? interno.RutaImagen != null ? interno.RutaImagen : "" : "",
                    UnidadId = infoDetencion != null ? infoDetencion.UnidadId : 0,
                    ResponsableId = infoDetencion != null ? infoDetencion.ResponsableId : 0,
                    Motivo = motivo,
                    Descripcion = (infoDetencion != null) ? infoDetencion.Descripcion : "",
                    Folio = (infoDetencion != null) ? infoDetencion.Folio : "",
                    Lugar = (infoDetencion != null) ? infoDetencion.LugarDetencion : "",
                    FechaDetencion = (detalleDetencion != null) ? ((DateTime)detalleDetencion.Fecha).ToString("dd-MM-yyyy HH:mm:ss") : "",
                    PaisId = (pais != null) ? pais.Id : 0,
                    EstadoId = (estado != null) ? estado.Id : 0,
                    MunicipioId = (municipio != null) ? municipio.Id : 0,
                    ColoniaId = (colonia != null) ? colonia.Id : 0,
                    CodigoPostal = (colonia != null) ? colonia.CodigoPostal : "",
                    IdInfo = (infoDetencion != null) ? infoDetencion.Id.ToString() : "",
                    TrackingInfo = (infoDetencion != null) ? infoDetencion.TrackingId.ToString() : "",
                    LlamadaId = (evento != null) ? evento.LlamadaId : 0,
                    EventoId = (evento != null) ? evento.Id : 0,
                    Edad = edad > 0 ? edad.ToString() : "Fecha de nacimiento no registrada",
                    Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                    municipioNombre = municipio2 != null ? municipio2.Nombre : "Municipio no registrado",
                    domiclio = domicilio != null ? domicilio.Calle + " #" + domicilio.Numero.ToString() : "Domicilio no registrado",
                    coloniaNombre = colonia2 != null ? colonia2.Asentamiento : "Colonia no registrada",
                    LlamadaNombre = llamada != null ? llamada.Descripcion : "",
                    EventoNombre = (evento != null) ? evento.Descripcion : "",
                    UnidadNombre = (unidad != null) ? unidad.Nombre : "",
                    ResponsableNombre = (responsable != null) ? responsable.Nombre : "",
                    PaisNombre = (pais != null) ? pais.Nombre : "",
                    EstadoNombre = (estado != null) ? estado.Nombre : "",
                    MunicipioNombre = (municipio != null) ? municipio.Nombre : "",
                    ColoniaNombre = (colonia != null) ? colonia.Asentamiento : "",
                    Estatura = (antropometria != null) ? antropometria.Estatura.ToString() : "",
                    Peso = (antropometria != null) ? antropometria.Peso.ToString() : "",
                    FechaSalida = fechaSalidaAdd != DateTime.MinValue ? fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    Personanotifica = infoDetencion.Personanotifica != null ? infoDetencion.Personanotifica : "",
                    Celular = infoDetencion.Celular != null ? infoDetencion.Celular : "",

                };
                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                //}
                //else
                //{
                //    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                //}
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
    }
}
