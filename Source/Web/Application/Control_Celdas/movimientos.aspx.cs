﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Table = DT.Table;

namespace Web.Application.Sentence
{
    public partial class movimientos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getPermisos();
        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Control de celdas" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //Consultar
                      //  this.WERQEQ.Value = permisos.Consultar.ToString().ToLower();

                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }
        [WebMethod]
        public static string ValidateFecha(string[] info)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(info[0]));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalle = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                //**v*/ar detalle = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(info[0]));*/
                var fecha = Convert.ToDateTime(info[1]);
                int result = DateTime.Compare(fecha, Convert.ToDateTime(detalle.Fecha));
                Boolean validate = true;
                if (result < 0)
                {
                    validate = false;
                }
                var fechar = Convert.ToDateTime(detalle.Fecha);
                var h = fechar.Hour.ToString();
                if (fechar.Hour < 10) h = "0" + h;
                var m = fechar.Minute.ToString();
                if (fechar.Minute < 10) m = "0" + m;
                var s = fechar.Second.ToString();
                if (fechar.Second < 10) s = "0" + s;
                var registro = fechar.ToShortDateString() + " " + h + ":" + m + ":" + s;
                return JsonConvert.SerializeObject(new { exitoso = true, Validar = validate, fechaRegistro = registro }); 
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }
        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }




        //    // var contrato=ControlContrato.ObtenerPorId(usuarioContrato.)
        //}
        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }
        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {


                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                    Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);

                    
                    Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                    Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                    Entity.Catalogo sexo = new Entity.Catalogo();
                    int edad = 0;
                    if (general != null)
                    {
                        if (general.FechaNacimineto != DateTime.MinValue)
                            edad = CalcularEdad(general.FechaNacimineto);
                        else
                        {
                            var detalleDetencion2 = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                            var evento = ControlEvento.ObtenerById(detalleDetencion2.IdEvento);
                            var detenidosEvento = ControlDetenidoEvento.ObtenerPorEventoId(evento.Id);
                            Entity.DetenidoEvento detenidoEvento = new Entity.DetenidoEvento();
                            foreach (var item in detenidosEvento)
                            {
                                if (item.Nombre == interno.Nombre && item.Paterno == interno.Paterno && item.Materno == interno.Materno)
                                    detenidoEvento = item;
                            }

                            if (detenidoEvento == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro al detenido en el evento." });
                            else
                            {
                                edad = detenidoEvento.Edad;
                            }
                        }
                        sexo = ControlCatalogo.Obtener(general.SexoId, 4);
                    }
                    var _mun = domicilio != null ? Convert.ToInt32(domicilio.MunicipioId) : 0;
                    Entity.Municipio municipio = _mun != 0 ? ControlMunicipio.Obtener(_mun) : null;
                    if (edad == 0) edad = detalleDetencion.DetalledetencionEdad;
                    var _col = domicilio != null ? Convert.ToInt32(domicilio.ColoniaId) : 0;
                    Entity.Colonia colonia = _col != 0 ? ControlColonia.ObtenerPorId(_col) : null;

                    object obj = null;

                    if (interno != null)
                    {
                        if (string.IsNullOrEmpty( interno.RutaImagen))
                        {
                            interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                        }
                    }
                    obj = new
                    {
                        Id = interno != null ? interno.Id.ToString() : "",
                        TrackingId = interno != null ? interno.TrackingId.ToString() : "",
                        Expediente = (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                        Centro = (institucion != null) ? institucion.Nombre : "",
                        Fecha = detalleDetencion != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Nombre = (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                        RutaImagen = (interno != null) ? interno.RutaImagen : "",
                        SituacionId = calificacion != null ? calificacion.SituacionId : 0,
                        InstitucionId = calificacion != null ? calificacion.InstitucionId : 0,
                        Fundamento = calificacion != null ? calificacion.Fundamento : "",
                        TrabajoSocial = calificacion != null ? calificacion.TrabajoSocial : false,
                        TotalHoras = calificacion != null ? calificacion.TotalHoras : 0,
                        SoloArresto = calificacion != null ? calificacion.SoloArresto : false,
                        TotalDeMultas = calificacion != null ? calificacion.TotalDeMultas : 0,
                        Agravante = calificacion != null ? calificacion.Agravante : 0,
                        Ajuste = calificacion != null ? calificacion.Ajuste : 0,
                        TotalAPagar = calificacion != null ? calificacion.TotalAPagar : 0,
                        Razon = calificacion != null ? calificacion.Razon : "",
                        IdCalificacion = calificacion != null ? calificacion.Id.ToString() : "",
                        TrackingCalificacion = calificacion != null ? calificacion.TrackingId.ToString() : "",
                        Edad = edad == 0 ? "Fecha de nacimiento no registrada" : edad.ToString(),
                        Sexo = sexo.Nombre != null ? sexo.Nombre : "Sexo no registrado",
                        municipioNombre = municipio !=null? municipio.Nombre:"Municipio no registrado",
                        domiclio = domicilio != null? domicilio.Calle + " #" + domicilio.Numero.ToString():"Domicilio no registrado",
                        coloniaNombre = colonia != null ? colonia.Asentamiento : "Colonia no registrada",
                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static DataTable Getmovimientoslog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable,string tracking)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        if(!string.IsNullOrEmpty(tracking))
                        {

                            var interno = ControlDetenido.ObtenerPorTrackingId(new Guid(tracking));
                            string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                            var detalle = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                            where.Add(new Where("A.DetalledetencionId", detalle.Id.ToString()));
                        }
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.DetalledetencionId",
                                "A.TipomovimientoId",
                                "A.CeldaId",
                                "A.Observaciones",
                                "A.Responsable",
                                "A.InstitucionId",
                                "A.habilitado",
                                "case when RolId = 13 then (select Usuario from usuario where Id = 8) else Usuario end Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("movimientolog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("vusuarios","V"),"V.id=A.Creadopor","LEFT"),
                                new Join(new Table("Usuario","B"),"B.id=V.UsuarioId","LEFT"),
                                new Join(new Table("movimiento","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        var data = dt.Generar(query, draw, start, length, search, order, columns);
                        return data;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }

            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
        [WebMethod]
        public static DataTable getMovimientos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string TrackingProcesoId)
        {
            try
            {

                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(TrackingProcesoId));

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {
                    if (!emptytable)
                    {

                        string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                        Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));

                        where.Add(new Where("E.DetalledetencionId", detalleDetencion.Id.ToString()));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "date_format(E.FechaHora, '%Y-%m-%d %H:%i:%S') FechaHora",
                                "C.Nombre Movimiento",
                                "replace(E.Observaciones,'\"','') Observaciones",
                                "E.Responsable",
                                "D.Nombre Institucion",
                                "E.Id",
                                "E.TrackingId",
                                "E.DetalledetencionId",
                                "E.TipomovimientoId",
                                "E.InstitucionId",
                                "E.CeldaId",
                                "F.Nombre Celda",
                                "F.TrackingId CeldaTrackingId",
                                "E.Habilitado",
                                "date_format(E.FechaHora, '%d/%m/%Y %H:%i:%S') FechaHoraFormateada"
                            },
                            from = new DT.Table(" movimiento", "E"),
                            joins = new List<Join>
                            {
                                new Join(new DT.Table("tipomovimiento_celda", "C"), "C.Id  = E.TipomovimientoId"),
                                new Join(new DT.Table("institucion", "D"), "D.Id  = E.InstitucionId"),
                                new Join(new DT.Table("Celda", "F"), "F.Id  = E.CeldaId"),
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _datatable = new DataTable();
                        _datatable= dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static List<Combo> getListadoMovimiento_Celda_Combo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(119);

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }
        [WebMethod]
        public static List<Combo> getListadoMovimiento_CentroReclusion_Combo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Institucion> listado = ControlInstitucion.ObteneTodos();
            int id = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(idContrato));
            var centro = new Entity.Institucion();

            if (contratoUsuario.Tipo == "contrato")
            {
                centro = contratoUsuario != null ? ControlInstitucion.ObtenerPorId(ControlContrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId) : null;
            }
            else if (contratoUsuario.Tipo == "subcontrato")
            {
                centro = contratoUsuario != null ? ControlInstitucion.ObtenerPorId(ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId) : null;
            }


            if (centro != null)
            {
                
                combo.Add(new Combo { Desc = centro.Nombre, Id = centro.Id.ToString() });
            }

            return combo;
        }
        [WebMethod]
        public static string GetCeldaByDetalleDetencion(string tracking)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(tracking));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                var mov = ControlMovimiento.ObtenerPorDetalleDetencionId(detalleDetencion.Id);
                var celdas = "";
                var celdatracking = "";
                var final = false;
                if(mov.Count>0)
                {
                    
                    var celda = ControlCelda.ObtenerPorId(mov[0].CeldaId);
                    celdas = celda.Nombre;
                    celdatracking = celda.TrackingId.ToString();
                    foreach(var item in mov)
                    {
                        if(item.TipomovimientoId==2)
                        {
                            final = true;
                        }
                    }

                }
                object obj = null;

                obj = new
                {
                    nombrecelda=celdas,
                    TrackingCeldas=celdatracking,
                    Finales=final
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static Object MovimientoCeldaSave(MovimientoCeldaAux MovimientoCelda)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(MovimientoCelda.TrackingId));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                MovimientoCelda.InternoId = detalleDetencion.Id.ToString();

                var tipoMovimiento = Convert.ToInt32(MovimientoCelda.TipoMovimientoId);
                var fecha = Convert.ToDateTime(MovimientoCelda.Fechahora);
                if(tipoMovimiento == 1 && fecha > DateTime.Now) return new { exitoso = false, mensaje = "La fecha y hora no debe ser mayor a la fecha actual para el movimiento de entrada a celda.", fallo = "fecha" };
                else if(tipoMovimiento == 2 && fecha < DateTime.Now) return new { exitoso = false, mensaje = "La fecha y hora no debe ser menor a la fecha actual para el movimiento de salida a celda.", fallo = "fecha" };

                Entity.Movimiento movimiento = new Entity.Movimiento();
                var celda = ControlCelda.ObtenerPorTrackingId(new Guid(MovimientoCelda.CeldaTrackingId));
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                movimiento.Activo = 1;
                movimiento.Habilitado = 1;
                movimiento.Creadopor = usId;
                movimiento.DetalledetencionId = Convert.ToInt32(MovimientoCelda.InternoId);
                movimiento.FechaYHora = Convert.ToDateTime(MovimientoCelda.Fechahora);
                movimiento.InstitucionId = Convert.ToInt32(MovimientoCelda.InstitucionId);
                movimiento.TipomovimientoId = Convert.ToInt32(MovimientoCelda.TipoMovimientoId);
                movimiento.Responsable = Convert.ToString(MovimientoCelda.ResponsablieId);
                movimiento.CeldaId = Convert.ToInt32(celda.Id);
                movimiento.Observacion = Convert.ToString(MovimientoCelda.Observaciones);

                Entity.Movimiento MovimientoAux = new Entity.Movimiento();
                var listadoMovimientos = ControlMovimiento.ObtenerTodos();
                if (listadoMovimientos.Count > 0)
                {
                    foreach (var item in listadoMovimientos.Where(x => x.DetalledetencionId == movimiento.DetalledetencionId  && x.Activo == 1))
                    {
                        MovimientoAux = item;
                    }


                    if (MovimientoAux.TipomovimientoId != movimiento.TipomovimientoId  )
                    {
                        if (MovimientoAux.Id != 0) {
                            MovimientoAux.Activo = 0;
                            ControlMovimiento.Actualizar(MovimientoAux);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(MovimientoCelda.Id))
                        {
                            return new { exitoso = false, mensaje = "Seleccione un tipo de movimiento diferente.", Id = "", TrackingId = "" };
                        }
                    }
                }
                movimiento.TrackingId = Guid.NewGuid().ToString();


                if (movimiento.TipomovimientoId == 1)
                {
                    var listadetalles = ControlCeldaDetalle.GetDetall(Convert.ToInt32(movimiento.CeldaId));

                    int Capacidad = 0;
                    int porcentajeOcupacion = 0;
                    int cantidadDetenidos = 0;
                    if (listadetalles != null)
                    {
                        Capacidad = listadetalles.FirstOrDefault().Capacidad;
                        int cantidadEntrada = listadetalles.Where(t => t.TipoMovimientoId == 1).Count();
                        int cantidadSalida = listadetalles.Where(t => t.TipoMovimientoId == 2).Count();
                        porcentajeOcupacion = ((cantidadEntrada - cantidadSalida) * 100) / Capacidad;
                        cantidadDetenidos = cantidadEntrada - cantidadSalida;
                    }
                    var item_mode = new Entity.Movimiento();

                    if (!string.IsNullOrEmpty(MovimientoCelda.Id))
                    {
                        item_mode = ControlMovimiento.ObtenerPorId(movimiento.Id);
                    }
                    if (Capacidad - cantidadDetenidos == 0 && item_mode != null)
                    {
                        return new { exitoso = false, mensaje = "La celda no tiene lugares disponibles", Id = "", TrackingId = "" };

                    }
                }
               else
                {
                    var mov = ControlMovimiento.ObtenerPorDetalleDetencionId(movimiento.DetalledetencionId);
                    if (mov == null || mov.Count == 0)
                    {
                        return new { exitoso = false, mensaje = "No se puede generar una salida sin una entrada .", fallo = "fecha" };
                        //return new { exitoso = false, mensaje = "El detenido no puede generar una salida si no hay una entrada.", Id = "", TrackingId = "" };
                    }
                    else
                    {
                        var valido = false;
                        foreach(var item in mov)
                        {
                            if(item.TipomovimientoId==1)
                            {
                                valido = true;
                            }
                        }
                        if(!valido)
                        {
                            return new { exitoso = false, mensaje = "No se puede generar una salida sin una entrada .", fallo = "fecha" };
                            //return new { exitoso = false, mensaje = "El detenido no puede generar una salida si no hay una entrada.", Id = "", TrackingId = "" };
                        }
                    }
                }
                if (string.IsNullOrEmpty(MovimientoCelda.Id))
                {
                    if(movimiento.TipomovimientoId==2)
                    {
                        if (listadoMovimientos.Count == 0)
                        {
                            return new { exitoso = false, mensaje = "El detenido no ha ingresado a la celda selccionada.", Id = "", TrackingId = "" };

                        }
                        
                        
                        var movimientocelda = listadoMovimientos.LastOrDefault(x => x.TipomovimientoId == 1 && x.DetalledetencionId == movimiento.DetalledetencionId && x.CeldaId == movimiento.CeldaId && x.Habilitado == 1);

                        if (movimientocelda == null)
                        {
                            return new { exitoso = false, mensaje = "El detenido no ha ingresado a la celda selccionada.", Id = "", TrackingId = "" };
                        }

                    }

                    int idmovimiento = ControlMovimiento.Guardar(movimiento);
                    return new { exitoso = true, mensaje = "La información se registró satisfactoriamente.", Id = "", TrackingId = "" };
                }
                else
                {
                    if(movimiento.TipomovimientoId==1)
                    {

                    }
                    movimiento.Id = Convert.ToInt32(MovimientoCelda.Id);
                    ControlMovimiento.Actualizar(movimiento);
                    return new { exitoso = true, mensaje = "La información se actualizó satisfactoriamente.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static DataTable getcelda(int draw, int start, int length, Search search, List<Order> order, List<Column> columns,  bool emptytable)
        {
            try
            {
                // if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Administración" }).Consultar)
                //{
                if (!emptytable)
                {
                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                    List<Where> where = new List<Where>();
                    int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                    Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);



                    where.Add(new Where("S.Activo", "1"));
                    where.Add(new Where("S.Habilitado", "1"));
                    where.Add(new Where("S.ContratoId", contratoUsuario.IdContrato.ToString()));
                    Query query = new Query
                    {
                        select = new List<string>{
                                "S.Id",
                                "S.Nombre",
                                "S.Capacidad",
                                "S.TrackingId",
                                "S.Habilitado",
                                "S.Nivel_Peligrosidad",
                                "M.Nombre Peligrosidad"
                            },
                        from = new DT.Table("celda", "S"),
                        joins = new List<Join>
                            {
                                new Join(new DT.Table("peligrosidad_criminologica", "M"), "M.Id = S.Nivel_Peligrosidad"),
                            },
                        wheres = where
                    };

                    DataTables dt = new DataTables(mysqlConnection);
                    //return dt.Generar(query, draw, start, length, search, order, columns);
                    var a = dt.Generar(query, draw, start, length, search, order, columns);
                    return a;
                    //}
                    //else
                    //{
                    //    return DataTables.ObtenerDataTableVacia(null, draw);
                    //}
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }


        [WebMethod]
        public static string getdatosPersonales(string trackingid)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                string[] datos = { interno.Id.ToString(), true.ToString() };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                Entity.General general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                DateTime nacimiento;
                int edad = 0;
                if (general != null)
                {
                    nacimiento = general.FechaNacimineto;
                    edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;
                }

                string strDomicilio = "";
                if (domicilio != null)
                {
                    strDomicilio = domicilio.Localidad + ", " + domicilio.Calle + " " + domicilio.Numero;

                }

                object obj = null;

                obj = new
                {
                    Id = interno.Id.ToString(),
                    TrackingId = interno.TrackingId.ToString(),
                    Edad = edad,
                    Situacion = "PENDIENTE",
                    Domicilio = strDomicilio,

                    RutaImagen = interno.RutaImagen,
                    Registro = detalleDetencion.Fecha,
                    Salida = "",
                    Nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno,
                };



                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string blockitem(string Id)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Juez calificador" }).Eliminar)
                {
                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                Entity.Movimiento movimiento = ControlMovimiento.ObtenerPorId(Convert.ToInt32(Id));
                movimiento.Habilitado = movimiento.Habilitado == 1 ? 0 : 1;
               
                string mensaje = movimiento.Habilitado == 1 ? "Registro habilitado con éxito" : "Registro deshabilitado con éxito";
                ControlMovimiento.Actualizar(movimiento);
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje });

                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }

    public class MovimientoCeldaAux
    {
        public string Id { get; set; }
        public string TipoMovimientoId { get; set; }
        public string Fechahora { get; set; }
        public string CeldaTrackingId { get; set; }
        public string CeldaTObservacionesrackingId { get; set; }
        public string ResponsablieId { get; set; }
        public string InstitucionId { get; set; }
        public string TrackingId { get; set; }
        public string InternoId { get; set; }
        public string Observaciones { get; set; }
    }
}