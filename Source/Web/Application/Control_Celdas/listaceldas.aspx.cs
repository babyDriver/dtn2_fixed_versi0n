﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Control_Celdas
{
    public partial class listaceldas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static DataTable getcelda(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string celda, bool emptytable)
        {
            try
            {
                 if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {
                if (!emptytable)
                {
                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                    List<Where> where = new List<Where>();

                    if (!string.IsNullOrEmpty(celda))
                    {
                        where.Add(new Where("S.Nombre", string.Concat(" like '%", celda.Trim(), "%'")));
                    }
                    where.Add(new Where("S.Activo", "1"));
                    Query query = new Query
                    {
                        select = new List<string>{
                                "S.Id",
                                "S.Nombre",
                                "S.Capacidad",
                                "S.TrackingId",
                                "S.Habilitado",
                                "S.Nivel_Peligrosidad",
                                "M.Nombre Peligrosidad"
                            },
                        from = new Table("celda", "S"),
                        joins = new List<Join>
                            {
                                new Join(new Table("peligrosidad_criminologica", "M"), "M.Id = S.Nivel_Peligrosidad"),
                            },
                        wheres = where
                    };

                    DataTables dt = new DataTables(mysqlConnection);
              
                    var a = dt.Generar(query, draw, start, length, search, order, columns);
                    return a;
                    }
                   else
                   {
                       return DataTables.ObtenerDataTableVacia(null, draw);
                   }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
    }
}