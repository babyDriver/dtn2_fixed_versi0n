﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using Entity.Util;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Linq;

namespace Web.Application.Control_Celdas
{
    public partial class registro_celda : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static DataTable getcelda(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string celda, bool emptytable)
        {
            try
            {
                 if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {
                if (!emptytable)
                {
                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                    List<Where> where = new List<Where>();

                    if (!string.IsNullOrEmpty(celda))
                    {
                        where.Add(new Where("S.Nombre", string.Concat(" like '%", celda.Trim(), "%'")));
                    }
                    where.Add(new Where("S.Activo", "1"));
                    Query query = new Query
                    {
                        select = new List<string>{
                                "S.Id",
                                "S.Nombre",
                                "S.Capacidad",
                                "S.TrackingId",
                                "S.Habilitado",
                                "S.Nivel_Peligrosidad",
                                "M.Nombre Peligrosidad"
                            },
                        from = new Table("celda", "S"),
                        joins = new List<Join>
                            {
                                new Join(new Table("peligrosidad_criminologica", "M"), "M.Id = S.Nivel_Peligrosidad"),
                            },
                        wheres = where
                    };

                    DataTables dt = new DataTables(mysqlConnection);
                    //return dt.Generar(query, draw, start, length, search, order, columns);
                    var a = dt.Generar(query, draw, start, length, search, order, columns);
                    return a;
                    }
                    else
                    { 
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuenta con privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
        [WebMethod]
        public static string getdatos(string trackingid)
        {
            try
            {

                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de celdas" }).Consultar)
                {


                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(trackingid));
                    string[] datos = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datos);
                    Entity.Institucion institucion = ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId);
                    Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorInternoId(detalleDetencion.Id);
                    Entity.Historial historial = ControlHistorial.ObtenerPorDetenido(detalleDetencion.DetenidoId).FirstOrDefault();
                    var motivoDetencionInterno = new Entity.MotivoDetencionInterno();
                    motivoDetencionInterno.InternoId = detalleDetencion.Id;
                    List<Entity.MotivoDetencionInterno> ListamotivoDetencionInterno = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(motivoDetencionInterno);
                    string strMotivosDetencion = "";
                    string strMotivoRemision = "";
                    Entity.InformacionDeDetencion informacionDeDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(detalleDetencion.Id);

                    if (informacionDeDetencion != null)
                    {
                        strMotivoRemision = informacionDeDetencion.Motivo;
                    }


                    if (ListamotivoDetencionInterno != null)
                    {
                        if (ListamotivoDetencionInterno.Count > 0)
                        {
                            foreach (Entity.MotivoDetencionInterno item in ListamotivoDetencionInterno)
                            {
                                Entity.MotivoDetencion objmotivoDetencion = ControlMotivoDetencion.ObtenerPorId(item.MotivoDetencionId);
                                strMotivosDetencion += objmotivoDetencion.Motivo + ",";
                            }
                            strMotivosDetencion = strMotivosDetencion.Substring(0, strMotivosDetencion.Length - 1);
                        }
                    }
                    object obj = null;


                    obj = new
                    {
                        Id = interno != null ? interno.Id.ToString() : "",
                        TrackingId = interno != null ? interno.TrackingId.ToString() : "",
                        Expediente = (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                        Centro = (institucion != null) ? institucion.Nombre : "",
                        //Fecha = detalleDetencion != null ? Convert.ToDateTime(detalleDetencion.Fecha).ToString("dd/MM/yyyy hh:mm:ss") : "",
                        Fecha = historial.Fecha.ToString("dd-MM-yyyy HH:mm:ss"),
                        Nombre = (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                        RutaImagen = (interno != null) ? interno.RutaImagen : "",
                        SituacionId = calificacion != null ? calificacion.SituacionId : 0,
                        InstitucionId = calificacion != null ? calificacion.InstitucionId : 0,
                        Fundamento = calificacion != null ? calificacion.Fundamento : "",
                        TrabajoSocial = calificacion != null ? calificacion.TrabajoSocial : false,
                        TotalHoras = calificacion != null ? calificacion.TotalHoras : 0,
                        SoloArresto = calificacion != null ? calificacion.SoloArresto : false,
                        TotalDeMultas = calificacion != null ? calificacion.TotalDeMultas : 0,
                        Agravante = calificacion != null ? calificacion.Agravante : 0,
                        Ajuste = calificacion != null ? calificacion.Ajuste : 0,
                        TotalAPagar = calificacion != null ? calificacion.TotalAPagar : 0,
                        Razon = calificacion != null ? calificacion.Razon : "",
                        IdCalificacion = calificacion != null ? calificacion.Id.ToString() : "",
                        TrackingCalificacion = calificacion != null ? calificacion.TrackingId.ToString() : "",
                        MotivosDetencion = strMotivosDetencion,
                        MotivoRemision = strMotivoRemision,
                        FechaRemision = informacionDeDetencion.HoraYFecha,
                        FechaDetencion = detalleDetencion.Fecha
                    };
                    return JsonConvert.SerializeObject(new { exitoso = true, obj = obj });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
        [WebMethod]
        public static List<Combo> getListadoMovimiento_Celda_Combo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(119);

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }
        [WebMethod]
        public static List<Combo> getListadoMovimiento_CentroReclusion_Combo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Institucion> listado = ControlInstitucion.ObteneTodos();

            if (listado.Count > 0)
            {
                foreach (var rol in listado)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static Object MovimientoCeldaSave(MovimientoCeldaAux MovimientoCelda)
        {
            try
            {
                Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(MovimientoCelda.TrackingId));
                string[] internod = new string[2] {
                            interno.Id.ToString(), "true"
                        };
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                MovimientoCelda.InternoId = detalleDetencion.Id.ToString();

                Entity.Movimiento movimiento = new Entity.Movimiento();
                var celda = ControlCelda.ObtenerPorTrackingId( new Guid(MovimientoCelda.CeldaTrackingId));
                movimiento.Activo = 1;
                movimiento.Habilitado = 1;
                movimiento.Creadopor = 13;
                movimiento.DetalledetencionId =Convert.ToInt32( MovimientoCelda.InternoId);
                movimiento.FechaYHora = Convert.ToDateTime(MovimientoCelda.Fechahora);
                movimiento.InstitucionId= Convert.ToInt32(MovimientoCelda.InstitucionId);
                movimiento.TipomovimientoId = Convert.ToInt32(MovimientoCelda.TipoMovimientoId);
                movimiento.Responsable = Convert.ToString(MovimientoCelda.ResponsablieId);
                movimiento.CeldaId= Convert.ToInt32(celda.Id);
                movimiento.Observacion= Convert.ToString(MovimientoCelda.Observaciones);
                movimiento.TrackingId = Guid.NewGuid().ToString();

                int idmovimiento = ControlMovimiento.Guardar(movimiento);




                return new { exitoso = true, mensaje = "La información fue registrada satisfactoriamente", Id = "", TrackingId = "" };
            }
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }


    }

    public class MovimientoCeldaAux
    {
        public string TipoMovimientoId { get; set; }
        public string Fechahora { get; set; }
        public string CeldaTrackingId { get; set; }
        public string CeldaTObservacionesrackingId { get; set; }
        public string ResponsablieId { get; set; }
        public string InstitucionId { get; set; }
        public string TrackingId { get; set; }
        public string InternoId { get; set; }
        public string Observaciones { get; set; }
    }

   
}
