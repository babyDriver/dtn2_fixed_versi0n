<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="celdas.aspx.cs" Inherits="Web.Application.Sentence.celdas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
 <li>Control de celdas</li>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
         td.strikeout {
            text-decoration: line-through;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <div class="scroll">
        


    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>


    <div class="row" id="addentry" >
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark" id="titlehead">
<%--                <i class="fa fa-lock" ></i>--%>
                <img style="width:40px; height:40px; margin-bottom:5px" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/Jail.png" />
                Control de celdas
            </h1>
        </div>
    </div>
        <!--agregar time picker-->
        <br />
            <div class="row">                                        
                <div class="col-sm-3"> 
                    <label for="tbFechaInicio">Fecha Inicial:</label>
                    <asp:TextBox ID="tbFechaInicio" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
                </div>
                                        
                <div class="col-sm-3"> 
                    <label for="tbFechaFin">Fecha Final:</label>
                    <asp:TextBox ID="tbFechaFin" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
                </div>
                <div class="col-sm-3">
                    <a class="btn btn-success btn-md" id="btnBuscarInfo"><i class="fa fa-search"></i> Consultar</a>
                </div>
            </div> 
         <br />
    <div class="row">        
        <section class="col col-md-5">
            <div class="form-group">
                <label class="col-md-1 control-label" style="color: #3276b1;text-align: center;padding:5px;font-weight:bold;">Celda: </label>&nbsp;&nbsp;&nbsp;                                
                <div class="col-md-8" style="margin-left:5px;">
                    <label class="select" style="width: 100%;">                
                <select name="rol" id="dropdown" runat="server" style="width: 100%; padding: 5px 0px;"></select>                                                                                           
                <i></i>
                
            </label>
                </div>
            </div>
           <%-- <label style="display: block;">Celda</label>--%>
            
        </section>
    </div>
    <div class="row"> 
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <section class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label class ="input" id="Capacidad" ></label>
            </section>
            <section class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label class ="input" id="PorcentajeOcupacion" ></label>
            </section>
            <section class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label class ="input" id="CantidadDetenidos" ></label>
            </section>
        </article>
    </div>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
<%--                        <span class="widget-icon"><i class="fa fa-group"></i></span>--%>

                             <img style="width:30px; height:30px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/JailWhite.png" />

                        <h2>Lista de detenidos en celdas</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th >Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                     
                                        <th data-hide="phone,tablet">Situación del detenido</th>
                                        <th data-hide="phone,tablet" style="width:40%">Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="icon-append fa fa-user"></i>&nbsp;Datos personales</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" width="240" height="240"/>
                    </div>
                </div>  
            </div>
        </div>
    </div>

     <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>
    </div>
    <div id="photo-arrested" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del interno</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" alt="fotografía del detenido" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
     <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            var rutaDefaultServer = "";

            getRutaDefaultServer();

            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "celdas.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }                                             
                    }
                });
            }
            
            CargarListado("");
            CargarGrid("0");
            function CargarGrid(Id) {
                responsiveHelper_dt_basic = undefined;
                window.table = $('#dt_basic').dataTable({
                    destroy: true,
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic').waitMe('hide');
                    },
                    ajax: {
                        type: "POST",
                        url: "celdas.aspx/getdata",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.CeldaId = Id;
                            parametrosServerSide.fechaInicio = $("#tbFechaInicio").val();
                            parametrosServerSide.fechaFin = $("#tbFechaFin").val();
                            return JSON.stringify(parametrosServerSide);
                        }

                    },
                    columns: [
                        {
                            data: "TrackingId",
                            targets: 0,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                return "";
                            }
                        },
                        null,
                        null,
                        {
                            name: "Nombre",
                            data: "Nombre"
                        },
                        {
                            name: "Paterno",
                            data: "Paterno"
                        },
                        {
                            name: "Materno",
                            data: "Materno"
                        },

                        {
                            name: "Expediente",
                            data: "Expediente"
                        },
                        null,
                        null,
                        {
                            name: "NombreCompleto",
                            data: "NombreCompleto",
                            visible: false

                        }
                    ],
                    columnDefs: [

                        {
                            data: "TrackingId",
                            targets: 0,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                return "";
                            }
                        },
                        {
                            targets: 1,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        }, {
                            targets: 2,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.RutaImagen != null) {
                                    var ext = "." + row.RutaImagen.split('.').pop();
                                    var photo = row.RutaImagen.replace(ext, ".thumb");
                                    var imgAvatar = resolveUrl(photo);
                                    return '<div class="text-center">' +
                                        '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                        '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                        '</a>' +
                                        '<div>';
                                } else {
                                    pathfoto = resolveUrl("~/Content/img/avatars/male.png");
                                    return '<div class="text-center">'+
                                        '<img id="avatar" class="img-thumbnail text-center" alt = "" src = "' + pathfoto + '" height = "10" width = "50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';"/>' +
                                        '<div>';
                                }
                            }
                        },
                        {
                            targets: 7,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.Situacion != null) {
                                return row.Situacion;
                            }
                            else {
                                return "Pendiente";
                            }
                            }
                        },
                        {
                            targets: 8,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var edit = "";

                                var action2 = "";
                                action2 = '&nbsp;<a class="btn datos btn-primary ' + edit + '"  href="#"  id="datos" data-id="' + row.TrackingId + '" title="Datos personales"><i class="fa fa-bars"></i> Datos personales</a>&nbsp;';

                                var action3 = "";


                                var action4 = "";
                                action4 = '&nbsp;<a class="btn bg-color-yellow txt-color-white ' + edit + '"  href="movimientos.aspx?tracking=' + row.TrackingId + '" title="Movimientos"><i class="glyphicon glyphicon-transfer"></i> Movimientos</a>&nbsp;';

                                return action2 + action3 + action4;

                            }
                        }
                    ]

                });
            }
            $("body").on("click", ".datos", function () {
                var id = $(this).attr("data-id");
                CargarDatos(id);
                
            });

            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "celdas.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);

                            var imagenAvatar = resolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);

                            $("#datospersonales-modal").modal("show");
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            ///change combo
            $("#ctl00_contenido_dropdown").change(function () {
               
                var id = $("#ctl00_contenido_dropdown").val();
   
                if (parseInt(id) > 0) {
                    $("#idArticulo").text($("#ctl00_contenido_dropdown").val())
                    $.ajax({

                        type: "POST",
                        url: "celdas.aspx/GetCeldaDetalleById",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ _id: id }),
                        cache: false,
                        success: function (data) {
                            var resultado = JSON.parse(data.d)

                            //loadCustomers(data.id); 
                            $("#Capacidad").text('Capacidad: ' + resultado.obj.CapacidadOcupacion);
                            $("#PorcentajeOcupacion").text('Porcentaje ocupación: ' + resultado.obj.PorcentajeOcupacion + '%');
                            $("#CantidadDetenidos").text('Cantidad de detenidos: ' + resultado.obj.CantidadDetenidos);
                        },
                        error: function () {
                            ShowError("¡Error!", "No fue posible cargar la lista de motivos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else
                {
                    $("#Capacidad").text('');
                            $("#PorcentajeOcupacion").text('');
                            $("#CantidadDetenidos").text('');

                }
                CargarGrid(id);
                
            });

            //// end change combo




            
            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            function CargarListado(id) {
                $('#ctl00_contenido_peligrosidad').empty();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "celdas.aspx/getListadoCombo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_dropdown');
                        Dropdown.empty();
                        Dropdown.append(new Option("Todos", "0"));
                        Dropdown.append(new Option("Sin asignar", "-1"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de motivos de detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var responsiveHelper_dt_basicadd = undefined;
            var breakpointAddDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytableadd = true;
            window.tableadd = $('#dt_basicadd').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basicadd) {
                        responsiveHelper_dt_basicadd = new ResponsiveDatatablesHelper($('#dt_basicadd'), breakpointAddDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basicadd.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basicadd.respond();
                    $('#dt_basicadd').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "sentence_entrylist.aspx/getinterno",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basicadd').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });


                        parametrosServerSide.emptytableadd = false;
                        // delimitacion de las fechas
                        parametrosServerSide.fechaInicio = $("#tbfechaInicio").val();
                        parametrosServerSide.fechaFin = $("#tbfechaFin").val();
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    null,
                    null,
                    {
                        name: "Expediente",
                        data: "Expediente",
                        orderable: false
                    },
                    null,
                ],
                columnDefs: [{
                    data: "TrackingId",
                    targets: 0,
                    orderable: false,
                    visible: false,
                    render: function (data, type, row, meta) {
                        return "";
                    }
                },

                {
                    targets: 1,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return row.Nombre + " " + row.Paterno + " " + row.Materno;
                    }
                },
                {
                    targets: 3,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var edit = "edit";
                        var editar = "";
                        
                        //if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-success btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=1' + '" title="Agregar sentencia/proceso"><i class="glyphicon glyphicon-plus"></i></a>&nbsp;';
                        if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-primary btn-circle printpdf" href="javascript:void(0);" data-nombre="' + row.Nombre + ' ' + row.Paterno + ' ' + row.Materno + '" data-avatar= "' + row.RutaImagen
                            + '" data-fn= "' + row.FechaNacimiento + '" data-alias= "' + row.AliasInterno + '" data-tracking= "' + row.TrackingId + '" title="Generar reporte de extravío"><i class="fa fa-file-pdf-o"></i></a>&nbsp;';

                        return editar;

                    }
                }

                ]
            });

            if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                $("#addentry").show();
            }



            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

        });
        // refrescar pagina
        $("#btnBuscarInfo").click(
            function () {

                $("#dt_basic").DataTable().ajax.reload();

            }
        );
    </script>
</asp:Content>
