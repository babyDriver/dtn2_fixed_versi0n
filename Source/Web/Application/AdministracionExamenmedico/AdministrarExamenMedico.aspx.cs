﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Table = DT.Table;

namespace Web.Application.AdministracionExamenmedico
{
    public partial class AdministrarExamenMedico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Defensor público" }).Consultar)
            {
                try
                {
                    if (!emptytable)
                    {

                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        List<Where> where2 = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Estatus", "<>", "2"));
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                        where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("E.Tipo", contratoUsuario.Tipo));


                        Query query = new Query
                        {
                            select = new List<string> {
                                "distinct (I.Id) Id",
                                "I.TrackingId",
                                "TRIM(I.Nombre) Nombre",
                                "TRIM(I.Paterno) Paterno",
                                "TRIM(I.Materno) Materno",
                                "concat(TRIM(I.Nombre),' ',TRIM(Paterno),' ',TRIM(Materno)) as NombreCompleto",
                                "I.RutaImagen",
                                "cast(E.Expediente as unsigned) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus" ,
                                "ES.Nombre EstatusNombre",
                                "ifnull(T.internoId,0) ObservacionId",
                                "Justificacion",
                            },
                            from = new DT.Table("detenido", "I"),
                            joins = new List<Join>
                                {
                                    new Join(new DT.Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                                    new Join(new DT.Table("estatus", "ES"), "ES.id  = E.Estatus"),
                                    new Join(new DT.Table("observacion", "T"), "T.InternoId  = E.Id","LEFT"),
                                    new Join(new DT.Table("administracionexamenmedico", "A"), "A.DetalledetencionId  = E.Id","INNER")
                                },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var result1 = dt.Generar(query, draw, start, length, search, order, columns);
                        return result1;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static List<Combo> getInternos()
        {
            var combo = new List<Combo>();
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
            object[] dataParam = new object[]
            {
                contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                contratoUsuario != null ? contratoUsuario.Tipo : string.Empty
            };

            var detenidos = ControlDetenidosActivosSelect.ObtenerTodos(dataParam);
            //var detalledetencion = ControlDetalleDetencion.ObtenertodossibExamenmedico();

            //int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            //Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            foreach (var item in detenidos)
            {               
                combo.Add(new Combo { Desc = item.Expediente + " - " + item.Nombre + " " + item.Paterno + " " + item.Materno, Id = item.IdDetalle.ToString() });                
            }

            return combo;
        }


        [WebMethod]
        public static Object Save(AdministrarExamenMedicoAuz administrarExamen)
        {
            try
            {
                Entity.AdministracionExamenMedico administracionExamenMedico = new Entity.AdministracionExamenMedico();
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                administracionExamenMedico.CreadoPor = usId;
                administracionExamenMedico.DetalledetencionId = Convert.ToInt32(administrarExamen.DetalledetencionId);
                administracionExamenMedico.Fecha = DateTime.Now;
                administracionExamenMedico.Justificacion = administrarExamen.Justificacion;
                administracionExamenMedico.TrackingId = Guid.NewGuid().ToString();
                ControlAdministracionExamenMedico.Guardar(administracionExamenMedico);



                    return new { exitoso = true, mensaje = "La información se registro satisfactoriamente", Id = "", TrackingId = "" };
                }


            
            catch (Exception ex)
            {

                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }



    }
    public class AdministrarExamenMedicoAuz
    {
        public string DetalledetencionId { get; set; }
        public string Justificacion { get; set; }
        public string Id { get; set; }
        public string TrackingId { get; set; }

    }
}
