﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business;

using System.Configuration;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Services;
using System.Web;


namespace Web.Application.Carta
{
    public partial class CartaAntecedentes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Carta antecedentes no penales" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //Consultar

                        //setear hidden registrar
                       

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static string GetParametrosTamaño()
        {
            try
            {
                var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                var parametro = ControlParametroContrato.TraerTodos();
                decimal maximo = 0;
                decimal minimo = 0;

                foreach (var item in parametro)
                {
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 13)
                    {
                        minimo = Convert.ToDecimal(item.Valor);
                    }
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 14)
                    {
                        maximo = Convert.ToDecimal(item.Valor);
                    }
                }
                return JsonConvert.SerializeObject(new { exitoso = true, TMax = maximo, Tmin = minimo });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string CargarDatos()
        {

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            //var cartaantecedentes = ControlCartaAntecedentesPoliciacosConsecutivo.obteberByContratoId(contratoUsuario.IdContrato);
            var centro = new Entity.Institucion ();

            if (contratoUsuario.Tipo == "contrato")
            {
                centro = contratoUsuario != null ? ControlInstitucion.ObtenerPorId(ControlContrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId) : null;
            }
            else if (contratoUsuario.Tipo == "subcontrato")
            {
                centro = contratoUsuario != null ? ControlInstitucion.ObtenerPorId(ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId) : null;
            }


            if (centro != null)
            {

            }


            object obj = null;
            string _elaboro="";
            try
            {
                _elaboro = ControlUsuario.ObtenerPorEmail(HttpContext.Current.User.Identity.Name).Nombre;
            }
            catch (Exception)
            {

                
            }

            obj = new
            {
                centro = centro.Nombre,
                //consecutivo = cartaantecedentes != null ? cartaantecedentes.Consecutivo.ToString() : "1",
                elaboro = _elaboro,
                fechahora = DateTime.Now.ToShortDateString()
            };



            return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });



        }

        [WebMethod]
        public static Object Save(CartaAntecedentesAux CartaAntecedentes)
        {
            try
            {
                Entity.CartaAntecedentePoliciacoConsecutivo CartaEntidad = new Entity.CartaAntecedentePoliciacoConsecutivo();
                var historial = new Entity.Historial();
                historial.InternoId = 0;
                historial.Habilitado = true;
                historial.Fecha = DateTime.Now;
                historial.Activo = true;
                historial.TrackingId = Guid.NewGuid();
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                historial.Movimiento = "Modificación al formato de referencia";
                historial.ContratoId = subcontrato.Id;
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(usId);
                historial.CreadoPor = usuario.Id;
                historial.Id = ControlHistorial.Guardar(historial);



                CartaEntidad.TrakingId = Guid.NewGuid().ToString();
                CartaEntidad.ContratoId = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                CartaEntidad.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                CartaEntidad.Fecha = Convert.ToDateTime(CartaAntecedentes.Fecha);
                CartaEntidad.Rutafoto = CartaAntecedentes.Rutafoto;
                CartaEntidad.Domicilio = CartaAntecedentes.Domicilio;
                CartaEntidad.Ciudad = CartaAntecedentes.Ciudad;
                CartaEntidad.Antecedentes = Convert.ToInt32(CartaAntecedentes.Antecedentes);
                CartaEntidad.Anio = Convert.ToInt32(CartaAntecedentes.Anio);
                CartaEntidad.FechaAntecedente = Convert.ToDateTime(CartaAntecedentes.Fechaantecedente);
                CartaEntidad.Elaboro = CartaAntecedentes.Elaboro;
                CartaEntidad.Reviso = CartaAntecedentes.Reviso;
                CartaEntidad.ReciboCaja = CartaAntecedentes.ReciboCaja;
                CartaEntidad.Ciudadano = CartaAntecedentes.Ciudadano;
                int idmovimiento = ControlCartaAntecedentesPoliciacosConsecutivo.Guardar(CartaEntidad);
                var cartapdf = ControlCartaAntecedentesPoliciacosConsecutivo.obteberById(idmovimiento);
                cartapdf.cuerpo = CartaAntecedentes.Cuerpo;


                object[] dataArchivo = null;
                dataArchivo = ControlPdfCartaAntecedentesPoliciacos.generarReporteDiagnostico(cartapdf);


                return new { exitoso = true, mensaje = "El formato referencia se generó correctamente", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "" ,mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString():""};
               


               
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
    }
    public class CartaAntecedentesAux
    {
        public string Ciudadano { get; set; }
        public string Fecha { get; set; }
        public string Rutafoto { get; set; }
        public string Domicilio { get; set; }
        public string Ciudad { get; set; }
        public string Antecedentes { get; set; }
        public string Anio { get; set; }
        public string Fechaantecedente { get; set; }
        public string Elaboro { get; set; }
        public string Reviso { get; set; }
        public string ReciboCaja { get; set; }
        public string Cuerpo { get; set; }

    }
}