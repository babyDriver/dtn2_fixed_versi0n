﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="CartaAntecedentes.aspx.cs" Inherits="Web.Application.Carta.CartaAntecedentes" %>



<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Formato referencia</li>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>
        select:disabled {
            background: #eee;
            cursor: not-allowed;
        }        
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }        
     
    </style>
    <div class="scroll">
        <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="glyphicon glyphicon-edit "></i>
                Carta antecedentes policiacos 
            </h1>
        </div>
    </div>-->


    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    
    
   <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken"  id="wid-entrya-1"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="glyphicon  glyphicon-edit"></i></span>
                    <h2>Generar formato referencia </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <div id="smart-form-register-entry" class="smart-form">
                            <header>
                              
                            </header>
                            <fieldset>
                               
                                <div class="row">
                                   <fieldset>
								<div class="row">
									<section class="col col-4">
										<label class="label" >Institución: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-institution"></i>
											<input name="name" disabled="disabled" type="text" class="alptext"  id="institucion"/>
										</label>
									</section>
									<section class="col col-4">
										<label class="label" >Ciudadano: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-edit"></i>
											<input name="email" type="text" maxlength="250" id="ciudadano"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese el ciudadano.</b>
										</label>
									</section>
									<section class="col col-4">
										<label class="label" >Fecha: <a style="color:red">*</a></label>
										<label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker'>
                                                                <input type="text" name="fechahora" id="fechahora" class='input-group date alptext' placeholder="Fecha" data-requerido="true"/>
                                                                <b class="tooltip tooltip-bottom-right" >Ingrese la fecha.</b>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                            
                                                        </label>
									</section>
								</div>
								<div class="row">
									<section class="col col-4">
										<label class="label" >Fotografía:</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-file-image-o"></i>
                                            <asp:FileUpload ID="rutafoto" runat="server" />
                                        </label>
                                       
									</section>
									
									<section class="col col-4">
										<label class="label" >Domicilio: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-globe"></i>
											<input name="text" type="text" maxlength="250" class="alptext" id="domicilio"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese el domicilio.</b>
										</label>
									</section>
									<section class="col col-4">
										<label class="label" >Ciudad: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-globe"></i>
											<input name="text" maxlength="250" type="text" id="ciudad" class="alptext" />
                                            <b class="tooltip tooltip-bottom-right" >Ingrese la ciudad.</b>
										</label>
									</section>
									 
								</div>
								<div class="row">
									<section class="col col-4">
                                     <label class="label" >Antecedentes <a style="color:red">*</a></label>
                                     <label class="select">
                                     <select name="antecedentes" id="antecedentes">
                                     	<option value="0">NO</option>
                                     	<option value="1">SI </option>
                                     	
                                     </select>
                                     <i></i>
                                                       
                                                        </label>
                                                    </section>
									<section class="col col-4">
										<label class="label" >Años: <a style="color:red">*</a></label>
										<label class="input"> <i class="icon-append fa fa-hashtag"></i>
											<input name="name"  type="text"  id="anios" title="" pattern="^[0-9]*$" maxlength="2"/>
                                          <b class="tooltip tooltip-bottom-right" >Ingrese los años.</b>
										</label>
									</section>
									<section class="col col-4">
										<label class="label" >Fecha Antecedente: <a style="color:red">*</a></label>
										<label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker2'>
                                                                <input type="text" name="fechahora" id="fechahora2" class='input-group date' placeholder="Fecha Antecedente" data-requerido="true"/>
                                                                <b class="tooltip tooltip-bottom-right" >Ingrese la fecha de antecedentes.</b>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                            
                                                        </label>
									</section>
									<section class="col col-4">
										<label class="label" >Elaboró:</label>
										<label class="input"> <i class="icon-append fa fa-envelope-o"></i>
											<input name="email" type="text" maxlength="150" class="alptext"  id="elaboro"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese quien lo elaboró.</b>
										</label>
										 
									</section>
									<section class="col col-4">
										<label class="label" >Revisó:</label>
										<label class="input"> <i class="icon-append fa fa-envelope-o"></i>
											<input name="email" class="alptext" maxlength="150" type="text" id="reviso"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese quien lo revisó.</b>
										</label>
										
									</section>
									<section class="col col-4">
										<label class="label" >Recibo de caja No.:</label>
										<label class="input"> <i class="icon-append fa fa-hashtag"></i>
											<input name="recibocaja" maxlength="50" class="integer alptext" id="recibocaja"/>
                                            <b class="tooltip tooltip-bottom-right" >Ingrese el No. de recibo de caja.</b>
										</label>
										
									</section>
								



									

									<section class="col col-8">
										<label class="label" >Cuerpo de Carta:</label>
										<label class="label"></label>
										<label class="textarea"> <i class="icon-append fa fa-comment"></i> 	
											<textarea name="comment" disabled="disabled" class="alptext" id="cuerpo" rows="10">                                                                                                                                              A:[día] de [mes] del:[año]
Se hace constar que el 
C. [Ciudadano] 

Quien manifiesta bajo protesta decir la verdad, ser vecino de esta ciudad desde hace [Años] años con domicilio en [Domicilio] de la ciudad de [Ciudad]
 
Dentro de los registros que se lleva en esta dependencia, se encontró que [Antecedentes] tiene antecedentes policíacos y encarcelarios registrados en los archivos de esta Secretaría desde el: [Fecha Antecedente].

										</textarea>
										<div class="note">
										Nota: Al modificar la redacción del "cuerpo de la carta" debe respetar la información que esta entre corchetes. Ej: [Domicilio]. Si lo omite o modifica no será impresa la información capturada.
									</div>
								</label></section>
								<section class="col col-4">
									    <label class="label"></label>
									    <br>

										<a title="Modificar" class="btn btn-sm btn-warning edit "id="edtitarcuerpo" href="javascript:void(0);"><i class="glyphicon glyphicon-edit"></i> Modificar</a>
											
									
								</section>
								
								</div>

								
							</fieldset>
                                </div>
                              
                          

                            </fieldset>
                            <footer>
                                <a title="Generar referencia" class="btn btn-sm btn-success edit " id="generarcarta" href="javascript:void(0);">
									<i class="glyphicon glyphicon-file"></i> Generar formato</a>                               
                            </footer>
                        </div>
                    </div>
                    
                    <input type="hidden" id="avatarOriginal" runat="server" />
                    <input type="hidden" id="trackingestatus" runat="server" />
                    <input type="hidden" id="idestatus" runat="server" />
                    <input type="hidden" id="HQLNBB" runat="server" value="" />
                    <input type="hidden" id="KAQWPK" runat="server" value="" />
                    <input type="hidden" id="LCADLW" runat="server" value=""/>
                    
                
                </div>
            </div>
        </article>
    </div>
    </div>


    <input type="hidden" id="Max" value="0" />
        <input type="hidden" id="Min" value="0" />
        <input type="hidden" id="Validar1"  />
        <input type="hidden" id="Validar2"  />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                document.querySelector("#generarcarta").click();
            }
        });
        $(document).ready(function () {
            pageSetUp();
            
              $('#FehaHoradatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
              $('#FehaHoradatetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            CargarDatos();
            $("#anios").on("keydown", function (e) {                
                if(!((e.keyCode > 95 && e.keyCode < 106)
                    || (e.keyCode > 47 && e.keyCode < 58)
                    || e.keyCode == 8)) {                    
                    return false;
                }
            });

            GetMinMax();

            function GetMinMax() {
                $.ajax({
                    type: "POST",
                    url: "CartaAntecedentes.aspx/GetParametrosTamaño",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var max = resultado.TMax;
                            var min = resultado.Tmin;

                            $("#Max").val(max);
                            $("#Min").val(min);
                            
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("#ctl00_contenido_rutafoto").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar1").val(false);
                    }
                    else {
                        $("#Validar1").val(true);
                    }
                }
                else {
                    $("#Validar1").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar2").val(false);
                    }
                    else {
                        $("#Validar2").val(true);
                    }
                }
                else {
                    $("#Validar2").val(true);
                }

                
            });

            function CargarDatos() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "CartaAntecedentes.aspx/CargarDatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {

                            $("#institucion").val(resultado.obj.centro);
                            $("#fechahora").val(resultado.obj.fechahora);
                            $("#fechahora2").val(resultado.obj.fechahora);
                            $("#antecedentes").val("0")
                        
                        }
                        else {
                            ShowError("Error!", "No fue posible cargar la información de la asignación. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                    }
                });
            }
                

            function validarCampos()
            {
                var  esvalido = true;   
                 if ($("#ciudadano").val() == null || $("#ciudadano").val().split(" ").join("") == "") {
                    ShowError("Ciudadano", "El campo ciudadano  es obligatorio.");
                    $('#ciudadano').parent().removeClass('state-success').addClass("state-error");
                    $('#ciudadano').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ciudadano').parent().removeClass("state-error").addClass('state-success');
                    $('#ciudadano').addClass('valid');
                }

                  if ($("#domicilio").val() == null || $("#domicilio").val().split(" ").join("") == "") {
                    ShowError("Domicilio", "El campo domicilio  es obligatorio.");
                    $('#domicilio').parent().removeClass('state-success').addClass("state-error");
                    $('#domicilio').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#domicilio').parent().removeClass("state-error").addClass('state-success');
                    $('#domicilio').addClass('valid');
                }

                  if ($("#ciudad").val() == null || $("#ciudad").val().split(" ").join("") == "") {
                    ShowError("Ciudad", "El campo ciudad  es obligatorio.");
                    $('#ciudad').parent().removeClass('state-success').addClass("state-error");
                    $('#ciudad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ciudad').parent().removeClass("state-error").addClass('state-success');
                    $('#ciudad').addClass('valid');
                }
                  if ($("#anios").val() == null || $("#anios").val().split(" ").join("") == "") {
                    ShowError("Años", "El campo años  es obligatorio.");
                    $('#anios').parent().removeClass('state-success').addClass("state-error");
                    $('#anios').removeClass('valid');
                    esvalido = false;
                }
                  else {
                      if ($("#anios").val() > 0) {
                         $('#anios').parent().removeClass("state-error").addClass('state-success');
                         $('#anios').addClass('valid');

                      } else {
                          
                        ShowError("Años", "El campo Años debe contener un valor mayor a 0.");
                        $('#anios').parent().removeClass('state-success').addClass("state-error");
                        $('#anios').removeClass('valid');
                        esvalido = false;
                      }

                 
                }

                if ($("#fechahora").val() == null || $("#fechahora").val().split(" ").join("") == "") {
                    ShowError("Fecha", "El campo fecha  es obligatorio.");
                    $('#fechahora').parent().removeClass('state-success').addClass("state-error");
                    $('#fechahora').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fechahora').parent().removeClass("state-error").addClass('state-success');
                    $('#fechahora').addClass('valid');
                }

                  if ($("#fechahora2").val() == null || $("#fechahora2").val().split(" ").join("") == "") {
                    ShowError("Fecha antecedente", "El campo fecha antecedente  es obligatorio.");
                    $('#fechahora2').parent().removeClass('state-success').addClass("state-error");
                    $('#fechahora2').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fechahora2').parent().removeClass("state-error").addClass('state-success');
                    $('#fechahora2').addClass('valid');
                }

                 var file = document.getElementById('<% = rutafoto.ClientID %>').value;
                if (file != null && file != '') {

                    if (!validaImagen(file)) {
                        ShowError("Fotografía", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        esvalido = false;

                    }
                    else {
                        var validar = $("#Validar1").val();
                        var validar2 = $("#Validar2").val();


                        if ($("#Max").val() != "0") {
                            if (validar == 'false') {
                                ShowError("Fotografía", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                esvalido = false;
                            }

                        }
                        if ($("#Min").val() != "0") {
                            var s = $("#Min").val() * 1024;
                            if (validar2 == 'false') {
                                ShowError("Fotografía", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                esvalido = false;
                            }
                        }
                    }
                }

                return esvalido;

            }


            function obtenerevalorscarta()

            {
                var CartaAntecedentes               = {                    
                    Ciudadano: $("#ciudadano").val(),
                    Fecha: $('#fechahora').val(),
                    Rutafoto:$('#ctl00_contenido_rutafoto').val(),
                    Domicilio: $('#domicilio').val(),
                    Ciudad: $("#ciudad").val(),
                    Antecedentes: $("#antecedentes").val(),
                    Anio: $("#anios").val(),
                    Fechaantecedente: $('#fechahora2').val(),
                    Elaboro: $('#elaboro').val(),
                    Reviso: $('#reviso').val(),
                    Recibocaja: $('#recibocaja').val(),
                    Cuerpo:$('#cuerpo').val(),
                    TrackingId: "",
                    Id:$('#ctl00_contenido_hideid').val()
                    
                };
                return CartaAntecedentes;



            }

            function limpiar() {

                $("#ciudadano").val("");
                $('#ciudadano').parent().removeClass('state-success');
                $('#ciudadano').removeClass('valid');

                $('#fechahora').val("");
                $('#fechahora').parent().removeClass('state-success');
                $('#ctl00_contenido_rutafoto').val("");
                $('#domicilio').val("");
                 $('#domicilio').parent().removeClass('state-success');
                $('#domicilio').removeClass('valid');
                $("#ciudad").val("");
                $('#ciudad').parent().removeClass('state-success');
                $('#ciudad').removeClass('valid');
                $("#antecedentes").val("");

                $("#anios").val("");
                 $('#anios').parent().removeClass('state-success');
                $('#anios').removeClass('valid');
                $('#fechahora2').val("");
                 $('#fechahora2').parent().removeClass('state-success');
                $('#elaboro').val("");
                $('#reviso').val("");
                $('#recibocaja').val("");
                

            }

               function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }

                }
                return false;
            }

            function guardarImagen() {
                var files = "";
                var nombreAvatarAnterior = "";
                var nombreAvatar = "";

                var files = $("#ctl00_contenido_rutafoto").get(0).files;
                var nombreAvatarAnterior = $("#generarcarta").attr("data-fotografia");
                var nombreAvatar = "";

                if (files.length > 0) {

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "../Handlers/FileUploadHandler.ashx?action=5&before=" + nombreAvatarAnterior,
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (Results) {
                            if (Results.exitoso) {
                                nombreAvatar = Results.nombreArchivo;
                                var id = $("#btnsave").attr("data-id");
                                var tracking = $("#btnsave").attr("data-tracking");
                                var habilitado = $("#btnsave").attr("data-habilitado");

                                datos = [

                                    Fotografia = nombreAvatar,

                                ];

                                GenerarCarta(nombreAvatar);
                            }
                            else {
                                ShowError("Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                            }
                        },
                        error: function (err) {
                            ShowError("Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else
                {
                    GenerarCarta("");
                }
              
            }


             function GenerarCarta(avatar) {
                startLoading();
                var CartaAntecedentes = obtenerevalorscarta();
                 CartaAntecedentes.Rutafoto = avatar;
                              
                $.ajax({
                    type: "POST",
                    url: "CartaAntecedentes.aspx/Save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'CartaAntecedentes': CartaAntecedentes }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!",   "El formato referencia se generó correctamente" );                            

                              var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                            limpiar();
                            CargarDatos();
                       
                          $('#main').waitMe('hide');
                           //location.reload();
                           
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

             function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }
       

            function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }

                }
                return false;
            }
            $("body").on("click", "#generarcarta", function () {
                if (validarCampos())
                {
                    guardarImagen();
                }
            });
           
            $("body").on("click", "#edtitarcuerpo", function () {
                $("#cuerpo").prop("disabled",false);

            });

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });


    </script>
</asp:Content>
