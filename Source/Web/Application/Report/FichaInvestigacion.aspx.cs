using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;

namespace Web.Application.Report
{
    public partial class FichaInvestigacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validatelogin();
            verifyPermission(22);
            if (!IsPostBack)
            {
                tbFechaInicial.Text = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                tbFechaFinal.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static string pdf(string tracking)
        {
            var serializedObject = string.Empty;
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 22 });

                if (permiso != null)
                {
                    var detalledetencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(tracking));
                    var ficha = new Entity.FichaInvestigacion();
                    ficha.TrackingId = Guid.NewGuid();
                    ficha.DetenidoId = detalledetencion.Id;
                    ficha.Fecha = DateTime.Now;
                    ficha.Creadopor = usId;
                    ficha.Id = ControlFichaInvestigacion.Guardar(ficha);
                    if (ficha.Id < 1) throw new Exception("Hubo un problema al realizar el reporte de la ficha de investigación");
                    var obj = ControlPDFBarandilla.generarFichaInvestigacion(detalledetencion, ficha.Id);
                    //var obj = ControlPDFBarandilla.GenerarInformeDetencion(InternoId);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> getCustomers()
        {
            var combo = new List<Combo>();
            var c = ControlCliente.ObtenerTodos().Where(x => x.Habilitado);

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

            var clientes = ControlContrato.ObetenerPorUsuarioId(usId).Select(x => x.ClienteId).Distinct();

            foreach (var i in clientes)
            {
                var cliente = c.FirstOrDefault(x => x.Id == i);
                if (cliente != null) combo.Add(new Combo { Desc = cliente.Nombre, Id = cliente.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetContracts(string Clienteid)
        {
            var combo = new List<Combo>();

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var contratos = ControlContrato.ObetenerPorUsuarioId(usId).Where(x => x.Habilitado && x.Activo).OrderBy(x => x.Id);
            var id = 0;
            foreach (var _contrato in contratos)
            {
                if (id != _contrato.Id)
                {
                    if (Clienteid != "0" && !string.IsNullOrEmpty(Clienteid))
                    {
                        if (_contrato.ClienteId == Convert.ToInt32(Clienteid))
                        {
                            combo.Add(new Combo { Desc = _contrato.Nombre, Id = _contrato.Id.ToString() });
                        }
                    }
                    else
                    {
                        combo.Add(new Combo { Desc = _contrato.Nombre, Id = _contrato.Id.ToString() });
                    }
                    id = _contrato.Id;
                }


            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetSubcontracts(string Contratoid)
        {
            var combo = new List<Combo>();

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var sub = ControlSubcontrato.ObtenerByUsuarioId(usId);

            if (!string.IsNullOrWhiteSpace(Contratoid) && Contratoid != "0")
            {
                foreach (var _subcontrato in sub.Where(x => x.ContratoId == Convert.ToInt32(Contratoid) && x.Habilitado))
                {
                    combo.Add(new Combo { Desc = _subcontrato.Nombre, Id = _subcontrato.Id.ToString() });
                }
            }
            else
            {
                foreach (var _subcontrato in sub.Where(x => x.Habilitado))
                {
                    combo.Add(new Combo { Desc = _subcontrato.Nombre, Id = _subcontrato.Id.ToString() });
                }
            }
            return combo;
        }

        [WebMethod]
        public static DataTable getDetenidos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string clienteid, string contratoid, string subcontratoid, string fechaInicio, string fechaFinal)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 22 });

                if (permiso != null)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = contratoUsuario.IdContrato.ToString();
                        //var pertenencias = ControlPertenencia.ObtenerPorInterno(usId);

                        if (!string.IsNullOrEmpty(subcontrato) && subcontrato != "0")
                        {
                            where.Add(new Where("A.ContratoId", "=", subcontrato));
                        }
                        //if (!string.IsNullOrEmpty(subcontratoid) && subcontratoid != "0")
                        //{
                        //    where.Add(new Where("A.ContratoId", "=", subcontratoid.ToString()));
                        //}

                        where.Add(new Where("cast(A.Fecha as DATE)", ">=", fechaInicio));
                        where.Add(new Where("cast(A.Fecha as DATE)", "<=", fechaFinal));

                        Query query = new Query
                        {
                            select = new List<string>{
                                "A.Id DetalleDetencionId",
                                "A.TrackingId",
                                "cast(A.Expediente as unsigned) Expediente",
                                "concat(A.NombreDetenido, ' ', APaternoDetenido, ' ', AMaternoDetenido) Nombre",
                                //"A.NombreDetenido Nombre",
                                "A.DetalledetencionEdad Edad",
                                "D.Descripcion Evento",
                                "W.nombreMotivoLlamada Motivo",
                                "Q.Nombre Unidad",
                                "H.Nombre Responsable",
                                "CL.Nombre Conclucion",
                                "date_format(D.HoraYFecha, '%Y-%m-%d %H:%i:%S') Detencion",
                                "P.Nombre Situacion",
                                "L.TotalHoras Horas",
                                "M.Subcontrato Barandilla"

                            },
                            from = new Table("detalle_detencion", "A"),
                            joins = new List<Join>
                            {                                
                                //traduce view                                             
                                //new Join(new Table("detenido_evento", "F"), "C.IdDetenido_Evento = F.Id "), 
                                new Join(new Table("informaciondedetencion", "C"), "A.DetenidoId  = C.IdInterno"),
                                new Join(new Table("eventos", "D"), "C.IdEvento  = D.Id"),
                                new Join(new Table("wsamotivo", "W"), "D.MotivoId = W.idMotivo"),
                                new Join(new Table("evento_unidad_responsable", "EU"), "D.Id = EU.EventoId"),
                                new Join(new Table("responsable_unidad", "H"), "EU.ResponsableId = H.Id"),
                                //add new inner's
                                new Join(new Table("servicio_medico", "MS"), "A.Id = MS.DetalledetencionId"),
                                new Join(new Table("certificado_medico_psicofisiologico", "B"), "MS.CertificadoMedicoPsicofisiologicoId = B.Id"),
                                new Join(new Table("detallecertificado_psicofisiologicoconclusion", "DC"), "B.ConclusionId = DC.certificado_psicofisiologicoConclusionId"),
                                new Join(new Table("conclusion", "CL"), "DC.ConclusionId = CL.Id"),
                                new Join(new Table("unidad", "Q"), "EU.UnidadId = Q.Id"),
                                new Join(new Table("calificacion", "L"), "A.Id = L.InternoId"),
                                new Join(new Table("situacion_detenido", "P"), "L.SituacionId = P.Id"),
                                new Join(new Table("subcontrato", "M"), "D.ContratoId  = M.Id"),
                                new Join(new Table("contrato", "N"), "M.ContratoId  = N.Id")
                                //new Join(new Table("cliente", "O"), "N.ClienteId  = O.Id"),
                            },
                            wheres = where,
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
    }

}
