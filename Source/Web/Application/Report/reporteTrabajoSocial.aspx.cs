﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using Entity.Util;
using Web.Application.Trabajo_social;
using Newtonsoft.Json;

namespace Web.Application.Report
{
    public class FiltroConsultaAux
    {
        public string nombre { get; set; }
        public string apellidopaterno { get; set; }
        public string apellidomaterno { get; set; }
        public string rfc { get; set; }
        public string edad { get; set; }
        public string sexo { get; set; }
        public string ocupacion { get; set; }
        public string nacionalidad { get; set; }
        public string calle { get; set; }
        public string numero { get; set; }
        public string sector { get; set; }
        public string colonia { get; set; }
        public string telefono { get; set; }
        public string alias { get; set; }
        public string estatura { get; set; }
        public string peso { get; set; }
        public string Idx { get; set; }
    }

    public partial class reporteTrabajoSocial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(17);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            string[] parametros = { usId.ToString(), "Trabajo social" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (!permitido)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }
        [WebMethod]
        public static string ValidarBoton()
        {
            try
            {
                var permitido = false;
                var p1 = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Trabajo social" });
                if (p1 != null)
                {
                    if (p1.Consultar)
                    {
                        permitido = true;
                    }
                }
                return JsonConvert.SerializeObject(new { success = true, Boton = permitido });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }
        [WebMethod]
        public static object imprimereporte(SalidaEfectuadajuezAux datos)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 17 });
                string[] parametros = { usId.ToString(), "Trabajo social" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    Entity.Detenido interno = ControlDetenido.ObtenerPorTrackingId(new Guid(datos.trackingId));
                    string[] internod = new string[2] {
                        interno.Id.ToString(), "true"
                    };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internod);
                    object[] dataArchivo = null;

                    dataArchivo = ControlPDFTrabajoSocial.generarReporteDiagnostico(detalleDetencion.Id);

                    return new { exitoso = true, mensaje = "Impresión", Id = detalleDetencion.Id.ToString(), TrackingId = detalleDetencion.TrackingId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "" };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static List<Combo> getNacionalidad()
        {
            List<Combo> combo = new List<Combo>();
            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.nacionalidad));

            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getOcupacion()
        {
            List<Combo> combo = new List<Combo>();
            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.ocupacion));

            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;

        }

        [WebMethod]
        public static List<Combo> getSexo()
        {
            List<Combo> combo = new List<Combo>();
            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.sexo));

            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getNeighborhoods(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();

            var colonias = ControlColonia.ObtenerTodas();

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias)
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = string.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }

        [WebMethod]
        public static List<Combo> getAlias(string idMunicipio)
        {
            List<Combo> combo = new List<Combo>();
            var alias = ControlAlias.ObteneTodos();

            if (alias.Count > 0)
            {
                foreach (var rol in alias)
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, FiltroConsultaAux filtroconsulta)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 17 });
            string[] parametros = { usId.ToString(), "Trabajo social" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (permitido)
            {
                try
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Estatus", "<>", "2"));
                        where.Add(new Where("TS.Activo", "1"));
                        //where.Add(new Where("C.TrabajoSocial", "=", "1"));
                        if (!string.IsNullOrEmpty(filtroconsulta.nombre))
                        {
                            where.Add(new Where("I.Nombre", filtroconsulta.nombre));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.apellidopaterno))
                        {
                            where.Add(new Where("I.Paterno", filtroconsulta.apellidopaterno));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.apellidomaterno))
                        {
                            where.Add(new Where("I.Materno", filtroconsulta.apellidomaterno));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.rfc))
                        {
                            where.Add(new Where("G.RFC", filtroconsulta.rfc));
                        }
                        //if (filtroconsulta.edad != "")
                        //{
                        //    where.Add(new Where("E.Activo", filtroconsulta.edad));
                        //}
                        if (!string.IsNullOrEmpty(filtroconsulta.sexo) && filtroconsulta.sexo != "0")
                        {
                            where.Add(new Where("G.SexoId", filtroconsulta.sexo));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.ocupacion) && filtroconsulta.ocupacion != "0")
                        {
                            where.Add(new Where("G.OcupacionId", filtroconsulta.ocupacion));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.nacionalidad) && filtroconsulta.nacionalidad != "0")
                        {
                            where.Add(new Where("G.NacionalidadId", filtroconsulta.nacionalidad));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.calle))
                        {
                            where.Add(new Where("D.Calle", filtroconsulta.calle));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.numero))
                        {
                            where.Add(new Where("D.Numero", filtroconsulta.numero));
                        }
                        //if (filtroconsulta.sector != "")
                        //{
                        //    where.Add(new Where("D.MunicipioId", filtroconsulta.));
                        //}
                        if (!string.IsNullOrEmpty(filtroconsulta.colonia) && filtroconsulta.colonia != "0")
                        {
                            where.Add(new Where("D.ColoniaId", filtroconsulta.colonia));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.telefono))
                        {
                            where.Add(new Where("D.Telefono", filtroconsulta.telefono));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.alias) && filtroconsulta.colonia != "0")
                        {
                            where.Add(new Where("A.Id", filtroconsulta.alias));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.telefono))
                        {
                            where.Add(new Where("AN.Estatura", filtroconsulta.estatura));
                        }
                        if (!string.IsNullOrEmpty(filtroconsulta.peso))
                        {
                            where.Add(new Where("AN.Peso", filtroconsulta.peso));
                        }
                        if (filtroconsulta.Idx == "-1")
                        {
                            where.Add(new Where("I.Id", filtroconsulta.Idx));
                        }

                        /////filtros criterio de busqueda
                        ///

                        ///////-----------------fin

                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "distinct (I.Id) Id",
                                "I.TrackingId",
                                "I.Nombre",
                                "I.Paterno",
                                "I.Materno",
                                "I.RutaImagen",
                                "CAST(E.Expediente AS unsigned) Expediente",
                                "E.NCP",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus",
                                "ES.Nombre EstatusNombre",
                                "ifnull(T.internoId,0) DetalledetencionId",
                                "ifnull(EX.Id,0) ExpedienteId",
                                "ifnull(EX.MotivoreahabilitacionId,0) Motivoreahabilitacion",
                                "ifnull(EX.AdiccionId,0) AdiccionId",
                                "ifnull(EX.Pandilla,'') Pandilla",
                                "ifnull(EX.ReligionId,0) ReligionId",
                                "ifnull(EX.Cuadropatalogico,'') Cuadropatalogico",
                                "ifnull(EX.Observacion,'') Observacion",
                                "TS.Id TrabajoSocialId",
                                "concat(E.NombreDetenido,' ',trim(APaternoDetenido),' ',trim(AMaternoDetenido)) NombreCompleto"
                            },
                            from = new Table("detenido", "I"),
                            joins = new List<Join>
                            {
                                new Join(new Table("detalle_detencion", "E"), "I.id  = E.DetenidoId"),
                                new Join(new Table("Calificacion", "C"), "C.InternoId  = E.Id"),
                                new Join(new Table("Trabajosocial", "TS"), "TS.DetalleDetencionId  = E.Id","INNER"),
                                new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"),
                                new Join(new Table("Motivo_detencion_Interno", "T"), "T.InternoId  = E.Id","LEFT"),
                                new Join(new Table("expediente_trabajo_social", "EX"), "Ex.DetalledetencionId=E.Id","LEFT"),
                                new Join(new Table("General","G"),"I.Id=G.DetenidoId","LEFT"),
                                new Join(new Table("domicilio","D"),"D.Id=I.DomicilioId","LEFT"),
                                new Join(new Table("Alias","A"),"A.DetenidoId=I.Id","LEFT"),
                                new Join(new Table("antropometria","AN"),"I.Id=AN.DetenidoId","LEFT")
                            },
                            wheres = where
                        };

                        if (contratoUsuario.Tipo == "contrato")
                        {
                            query.joins.Add(new Join(new DT.Table("contrato", "co"), "co.id  = E.ContratoId"));
                            where.Add(new Where("E.Tipo", "=", "contrato"));
                            where.Add(new Where("E.ContratoId", "=", contratoUsuario.IdContrato.ToString()));
                        }
                        else if (contratoUsuario.Tipo == "subcontrato")
                        {
                            query.joins.Add(new Join(new DT.Table("subcontrato", "co"), "co.id  = E.ContratoId"));
                            where.Add(new Where("E.Tipo", "=", "subcontrato"));
                            where.Add(new Where("E.Tipo", "=", "subcontrato"));
                            where.Add(new Where("E.ContratoId", "=", contratoUsuario.IdContrato.ToString()));
                        }

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = new DataTable();
                        _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }
    }
}