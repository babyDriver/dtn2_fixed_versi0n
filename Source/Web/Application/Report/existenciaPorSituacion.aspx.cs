﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.Services;

namespace Web.Application.Report
{
    public partial class existenciaPorSituacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(10);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static string pdf(string situacionId, string Id)
        {
            var serializedObject = string.Empty;
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var contrato = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato);
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 10 });

                if (permiso != null)
                {
                    var obj = ControlPDFJuez.ReporteSituaciones(institucion.Id, contratoUsuario.IdContrato, situacionId,Convert.ToInt32(Id), contratoUsuario.Tipo);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getDetenidos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string Situacion)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 10 });

                if (permiso != null)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var contrato = contratoUsuarioK.IdContrato;

                        var pertenencias = ControlPertenencia.ObtenerPorInterno(usId);
                        
                        where.Add(new Where("C.SituacionId", "=", Situacion.ToString()));
                        where.Add(new Where("EI.Activo", "1"));
                        where.Add(new Where("EI.ContratoId", contrato.ToString()));
                        where.Add(new Where("EI.Tipo", contratoUsuarioK.Tipo));
                        where.Add(new Where("S.ContratoId", contrato.ToString()));
                        where.Add(new Where("S.Tipo", contratoUsuarioK.Tipo));
                        //Tengo el id del contratousuario, su contratoid puede ser un contrato o un subcontrato
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        Query query = new Query
                        {
                            select = new List<string>{
                                "EI.TrackingId EstatusTracking",
                                "I.RutaImagen",
                                "I.Nombre",
                                "I.Paterno",
                                "I.Materno",
                                "CAST( EI.Expediente AS unsigned) Expediente",
                                "concat(I.Nombre,' ',TRIM(Paterno),' ',TRIM(Materno)  ) NombreCompleto",
                                "date_format(EI.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "group_concat(MD.Motivo) Motivo",
                                "IDD.LugarDetencion",
                                "date_format(IDD.HoraYFecha, '%Y-%m-%d %H:%i:%S') DateDetencion",
                                "U.Nombre Unidad",
                                "R.Nombre Responsable",
                                "EI.DetalledetencionEdad Edad",
                                "C.TotalHoras",
                                "P.Pertenencia",
                                "ifnull(G.Edaddetenido,0) Edaddetenido"
                            },
                            from = new Table("Detenido", "I"),
                            joins = new List<Join> {
                                new Join(new Table("detalle_detencion", "EI"), "EI.DetenidoId = I.Id"),
                                new Join(new Table("Informaciondedetencion", "IDD"), "IDD.IdInterno = I.Id"),
                                new Join(new Table("Unidad", "U"), "U.Id = IDD.IdUnidad"),
                                new Join(new Table("Responsable_unidad", "R"), "R.Id = IDD.IdResponsable"),
                                new Join(new Table("General", "G"), "G.DetenidoId = I.Id"),
                                new Join(new Table("Calificacion", "C"), "C.InternoId = EI.Id"),
                                new Join(new Table("Pertenencia", "P"), "P.InternoId = EI.Id and P.Id = (select max(Id) from pertenencia where InternoId = EI.Id)"),
                                new Join(new Table("Situacion_detenido", "S"), "S.Id = C.SituacionId"),
                                new Join(new Table("motivo_detencion_interno", "MDI"), "MDI.InternoId = EI.Id"),
                                new Join(new Table("motivodetencion", "MD"), "MD.Id = MDI.MotivoDetencionId")
                            },
                            wheres = where,
                            groupBy = "I.Id"
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var _dt= dt.Generar(query, draw, start, length, search, order, columns);
                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static List<Combo> getSituacion()
        {
            List<Combo> combo = new List<Combo>();

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            List<Entity.Catalogo> catalogo = ControlCatalogo.ObtenerHabilitados(Convert.ToInt32(Entity.TipoDeCatalogo.situacion_detenido));


            if (catalogo.Count > 0)
            {
                foreach (var rol in catalogo)
                {
                    if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
                    }
                }
            }

            return combo;
        }
    }
}