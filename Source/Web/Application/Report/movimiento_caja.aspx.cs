﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Report
{
    public partial class movimiento_caja : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            verifyPermission(19);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }
                
        [WebMethod]
        public static DataTable getCortes(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 19 });

                if (permiso != null)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        where.Add(new Where("C.Activo", "1"));

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if (contratoUsuario != null)
                        {
                            where.Add(new Where("C.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("C.Tipo", contratoUsuario.Tipo));
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                                "C.Id",
                                "C.TrackingId",
                                "C.SaldoInicial",
                                "date_format(C.FechaInicio, '%Y-%m-%d %H:%i:%S') FechaInicio",
                                "date_format(C.FechaFin, '%Y-%m-%d %H:%i:%S') FechaFin",
                                "C.Activo",
                                "C.Habilitado",
                                "C.CreadoPor",
                                "U.Usuario NombreCompleto"
                            },
                            from = new DT.Table("CorteDeCaja", "C"),
                            joins = new List<Join> {
                                 new Join(new DT.Table("vusuarios", "V"), "V.Id = C.CreadoPor", "INNER"),
                                new Join(new DT.Table("Usuario", "U"), "U.Id = V.UsuarioId", "INNER")
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static string generarReciboCorteDeCaja(string tracking)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 19 });

                if (permiso != null)
                {
                    Entity.CorteDeCaja corteDeCaja = ControlCorteDeCaja.ObtenerByTrackingId(new Guid(tracking));

                    object[] dataEgreso = new object[]
                    {
                    (corteDeCaja != null) ? corteDeCaja.Fecha.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    (corteDeCaja != null) ? corteDeCaja.FechaFin != null ? corteDeCaja.FechaFin.Value.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    true
                    };
                    List<Entity.EgresoIngreso> listEgresos = ControlEgresoIngreso.ObtenerTodoByFecha(dataEgreso);

                    object[] dataIngreso = new object[]
                    {
                    (corteDeCaja != null) ? corteDeCaja.Fecha.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    (corteDeCaja != null) ? corteDeCaja.FechaFin != null ? corteDeCaja.FechaFin.Value.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    false
                    };
                    List<Entity.EgresoIngreso> listIngresos = ControlEgresoIngreso.ObtenerTodoByFecha(dataIngreso);
                    object[] dataCancelacion = new object[]
                    {
                    (corteDeCaja != null) ? corteDeCaja.Fecha.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    (corteDeCaja != null) ? corteDeCaja.FechaFin != null ? corteDeCaja.FechaFin.Value.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") : ""
                    };
                    List<Entity.Cancelacion> listCancelacion = ControlCancelacion.ObtenerTodoByFecha(dataCancelacion);

                    int idContratoUsuario = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContratoUsuario);
                    int institucionId = 0;

                    if (contratoUsuario != null)
                    {
                        if (contratoUsuario.Tipo == "contrato")
                        {
                            institucionId = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId;
                        }
                        else if (contratoUsuario.Tipo == "subcontrato")
                        {
                            institucionId = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId;
                        }
                    }

                    var institucion = ControlInstitucion.ObtenerPorId(institucionId);
                    string centroNombre = (institucion != null) ? institucion.Nombre : "";
                    string logotipo = "~/Content/img/logo.png";
                    var x = ControlSubcontrato.ObtenerByContratoId(institucion.ContratoId);
                    foreach (var val in x)
                    {
                        if (institucion.Id == val.InstitucionId & val.Logotipo != "undefined")
                            logotipo = val.Logotipo;
                    }

                    object[] dataTitulos = new object[]
                    {
                    centroNombre
                    };

                    object[] dataArchivo = ControlPDF.generarReciboCorteDeCaja(dataTitulos, listIngresos, listEgresos, listCancelacion, logotipo);

                    return JsonConvert.SerializeObject(new { success = true, message = "guardó", ubicacionArchivo = (dataArchivo != null) ? dataArchivo[1].ToString() : "" });
                }
                else return JsonConvert.SerializeObject(new { success = false, message = "No cuenta con privilegios para realizar la acción." });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }
    }
}