﻿using Business;
using System.Collections.Generic;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using System.Linq;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Web.Application.Report
{
    public partial class Reporteagrupados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(13);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 13 });

            if (permiso != null)
            {
                try
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        //where.Add(new Where("ES.Activo", "1"));
                        //where.Add(new Where("C.Activo", "1"));
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var contrato = contratoUsuarioK.IdContrato;
                        where.Add(new Where("DD.ContratoId", contratoUsuarioK.IdContrato.ToString()));
                        // where.Add(new Where("ES.estatus", "1"));
                        where.Add(new Where("(select group_concat(HD.Expediente separator ',') from historico_agrupado_detenido HD where (HD.DetenidoOrignalId=A.Id))", "<>", "null"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.Id",
                                "A.TrackingId",
                                "concat(A.Nombre,' ',trim(Paterno),' ',trim(Materno)) DetenidoOriginal",
                                "cast(ifnull(Expedienteoriginal,Expediente) as unsigned) Expediente",
                                "(select group_concat(CAST(HD.Expediente as unsigned) separator ',</br> ')  from historico_agrupado_detenido HD inner join detenido D on HD.DetenidoOrignalId=D.id where (HD.DetenidoOrignalId=A.Id)) Expediente1",
                                "(select group_concat(Nombre,' ',trim(APP),' ',trim(APM) separator ',</br>') from historico_agrupado_detenido HAD inner join (select Id,Nombre,Paterno APP, Materno APM from detenido   ) F on HAD.DetenidoId=F.id where HAD.DetenidoOrignalId = A.Id) Detenido2",
                            },
                            from = new Table("detenido", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("detalle_detencion", "DD"), "DD.Id=(select B.Id from detalle_detencion B where B.DetenidoId=A.Id order by 1 desc limit 1)"),
                                new Join(new Table("vAgrupadoExpedienteoriginal","V"),"A.Id=V.DetenidoOrignalId"),
                            },
                            wheres = where,
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static object GeneraPdF(RptAgrupadoAux rangofechas)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 13 });

                if (permiso != null)
                {
                    if (Convert.ToDateTime(rangofechas.Fechainicio) > Convert.ToDateTime(rangofechas.Fechafin))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    object[] dataArchivo = null;
                    DateTime[] Fecha = new DateTime[2] {
                        Convert.ToDateTime( rangofechas.Fechainicio),
                        Convert.ToDateTime( rangofechas.Fechafin )};

                    var agrupados = ControlAgrupadoReporte.GetByRangoFechas(Fecha);

                    if (agrupados.Count == 0)
                    {
                        return new { exitoso = false, mensaje = "No se encontraron resultados", Id = "", TrackingId = "" };
                    }
                    List<Entity.AgrupadoReporte> listaFiltrada = new List<Entity.AgrupadoReporte>();
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    foreach (var item in agrupados.Where(x => x.ContratoId == contratoUsuario.IdContrato))
                    {
                        listaFiltrada.Add(item);
                    }
                    //dataArchivo = ControlPDFTrabajoSocial.generarReporteDiagnostico(detalleDetencion.Id);
                    if (listaFiltrada.Count == 0)
                    {
                        return new { exitoso = false, mensaje = "No se encontraron resultados", Id = "", TrackingId = "" };

                    }
                    dataArchivo = ControlPDFReporteAgrupadoDetenidos.generarReporteAgrupados(listaFiltrada, Fecha);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "" };
                }
                else
                {
                    return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción.", Id = "", TrackingId = "" };
                }
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
    }

    public class RptAgrupadoAux
    {
        public string Fechainicio { get; set; }
        public string Fechafin { get; set; }

    }
}