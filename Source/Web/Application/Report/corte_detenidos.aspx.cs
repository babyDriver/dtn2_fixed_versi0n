﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using System.Linq;
using Entity;

namespace Web.Application.Report
{
    public partial class corte_detenidos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // validatelogin();
            verifyPermission(5);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });
            string[] parametros = { usId.ToString(), "Registro en barandilla" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if(permisos!=null || permiso!=null)
            {
                permitido = true;
            }
            

            if (!permitido)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static object GetAlertaWeb()
        {
            try
            {
                var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
                var denoma = "Alerta web";
                var alerta = "alerta web";
                if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
                {
                    denoma = datosAlertaWeb.Denominacion;
                    alerta = denoma;
                }
                object data = new
                {

                    Denominacion = denoma,
                    AlertaWerb = alerta
                };

                return data;
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getcortedetenidos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string pages)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 5 });
            string[] parametros = { usId.ToString(), "Registro en barandilla" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (permitido)
            {
                try
                {
                    if (!emptytable)
                    {
                        List<Entity.ListadoReporteCorteDetenidos> data = new List<Entity.ListadoReporteCorteDetenidos>();
                        List<Entity.ListadoReporteCorteDetenidos> listTotals = new List<Entity.ListadoReporteCorteDetenidos>();

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        object[] dataParams = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0
                        };

                        listTotals = ControlListadoReporteCorteDetenidos.ObtenerTodos(dataParams);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value.ToLower());
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 0:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Remision)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Remision)).ToList();
                                    break;
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Evento)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Evento)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Edad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Edad).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Ingreso).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Ingreso).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Calificacion).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Calificacion).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Motivo).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Motivo).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Celda).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Celda).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToDateTime(x.Fecha)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToDateTime(x.Fecha)).ToList();
                                    break;
                                case 10:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Folio)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Folio)).ToList();
                                    break;
                                case 11:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Motivos).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Motivos).ToList();
                                    break;
                                case 12:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.HoraDetencion).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.HoraDetencion).ToList();
                                    break;
                                case 13:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Unidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Unidad).ToList();
                                    break;
                                case 14:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Responsable).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Responsable).ToList();
                                    break;
                                case 15:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Lugar).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Lugar).ToList();
                                    break;
                            }
                        }

                        List<Entity.ListadoReporteCorteDetenidos> listFinal = new List<Entity.ListadoReporteCorteDetenidos>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;

                        //MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        //List<Where> where = new List<Where>();
                        //var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        //var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        //int usuario;

                        //var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        //if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        //else throw new Exception("No es posible obtener información del usuario en el sistema");

                        //where.Add(new Where("EI.Activo", "1"));
                        //where.Add(new Where("EI.ContratoId", contratoUsuario.IdContrato.ToString()));
                        //where.Add(new Where("EI.Tipo", contratoUsuario.Tipo));

                        //Query query = new Query
                        //{
                        //    select = new List<string>
                        //    {
                        //        "CAST( EI.Expediente AS unsigned) Remision",
                        //        "CAST(E.Folio AS unsigned) Evento",
                        //        "concat(upper(D.Nombre),' ',upper(Paterno),' ',upper(Materno)) Nombre",
                        //        "EI.DetalledetencionEdad Edad",
                        //        "date_format(EI.Fecha, '%Y-%m-%d %H:%i:%S') Ingreso",
                        //        "S.Nombre Calificacion",
                        //        "CE.Nombre Celda",
                        //        //"date_format(E.HoraYFecha, '%Y-%m-%d %H:%i:%S') Cumple"
                        //        "date_format(date_add(E.HoraYFecha, INTERVAL ifnull(C.TotalHoras, 36) HOUR), '%Y-%m-%d %H:%i:%S') Cumple",
                        //        "ifnull(MDE.nombreMotivoLlamada,'') Motivo",
                        //        "ifnull(date_format(WE.FechaCapturaDetenidos,'%Y-%m-%d %H:%i:%S' ),'') Fecha",
                        //        "ifnull(WE.Folio,'') Folio",
                        //        "ifnull(Mo.Motivo,'') Motivos",
                        //        "date_format(IDD.HoraYFecha,'%Y-%m-%d %H:%i:%S') HoraDetencion",
                        //        "ifnull(U.Nombre,'') Unidad",
                        //        "R.Nombre Responsable",
                        //        "IDD.LugarDetencion Lugar",
                        //        "ifnull(CQ.Foliocertificado,'') FolioQuimico"
                        //    },
                        //    from = new Table("detenido", "D"),
                        //    joins = new List<Join>
                        //    {
                        //        new Join(new Table("informaciondedetencion", "ID"), "D.Id = ID.IdInterno"),
                        //        new Join(new Table("detalle_detencion", "EI"), "D.id  = EI.DetenidoId"),
                        //        new Join(new Table("eventos", "E"), "ID.IdEvento = E.Id"),
                        //        new Join(new Table("general", "G"), "D.Id = G.DetenidoId"),
                        //        new Join(new Table("calificacion", "C"), "EI.Id = C.InternoId"),
                        //        new Join(new Table("situacion_detenido", "S"), "C.SituacionId = S.Id"),
                        //        new Join(new Table("movimiento", "M"), "EI.Id = M.DetalledetencionId and M.TipomovimientoId = (select max(TipomovimientoId) from movimiento where DetalledetencionId = EI.Id)"),
                        //        new Join(new Table("celda", "CE"), "M.CeldaId = CE.Id"),
                        //        new Join(new Table("informaciondedetencion", "IDD"), "IDD.IdInterno = D.Id"),
                        //        new Join(new Table("wsaevento", "WE"), "E.IdEventoWS =We.Id"),
                        //        new Join(new Table("motivodetencion", "Mo"), "WE.IdMotivo=Mo.Id","LEFT OUTER"),
                        //        new Join(new Table("evento_unidad_responsable", "EUR"), "EUR.Id=(select EU.Id from evento_unidad_responsable EU where EU.EventoId=E.Id order by 1 desc limit 1)","LEFT OUTER"),
                        //        new Join(new Table("unidad", "U"), "EUR.UnidadId=U.Id","LEFT OUTER"),
                        //        new Join(new Table("responsable_unidad", "R"), "EUR.ResponsableId=R.Id","LEFT OUTER"),
                        //        new Join(new Table("servicio_medico", "SE"), "SE.Id=(select SM.Id from servicio_medico SM where SM.DetalledetencionId=EI.Id order by 1 desc limit 1)","LEFT OUTER"),
                        //        new Join(new Table("certificado_quimico", "CQ"), "SE.CertificadoQuimicoId=CQ.Id","LEFT OUTER"),
                        //        new Join(new Table("(select detenido_evento.Id,detenido_evento.MotivoId from detenido_evento)", "DE"), "IDD.IdDetenido_Evento=DE.Id","LEFT OUTER"),
                        //        new Join(new Table("wsamotivo", "MDE"), "DE.MotivoId=MDE.idMotivo","LEFT OUTER"),
                        //    },
                        //    wheres = where
                        //};

                        //DataTables dt = new DataTables(mysqlConnection);
                        //var _DT = dt.Generar(query, draw, start, length, search, order, columns);
                        //return _DT;
                    }
                    else
                    {
                        object json2 = new { data = new List<ListadoReporteCorteDetenidos>(), recordsTotal = new List<ListadoReporteCorteDetenidos>().Count, recordsFiltered = new List<ListadoReporteCorteDetenidos>().Count, };
                        return json2;
                    }
                }
                catch (Exception ex)
                {
                    object json2 = new { data = new List<ListadoReporteCorteDetenidos>(), recordsTotal = new List<ListadoReporteCorteDetenidos>().Count, recordsFiltered = new List<ListadoReporteCorteDetenidos>().Count, };
                    return json2;
                }
            }
            else
            {
                object json2 = new { data = new List<ListadoReporteCorteDetenidos>(), recordsTotal = new List<ListadoReporteCorteDetenidos>().Count, recordsFiltered = new List<ListadoReporteCorteDetenidos>().Count, };
                return json2;
            }
        }

        [WebMethod]
        public static string pdfCorteDetenidos()
        {
            var serializedObject = string.Empty;
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 5 });
                string[] parametros = { usId.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    var obj = ControlPDFBarandilla.generarReporteCorteDetenidos(contratoUsuario);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}