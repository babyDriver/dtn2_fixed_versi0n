﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using System.Linq;
using Entity;

namespace Web.Application.Report
{
    public partial class reporte_examenmedico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getinterno(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string pages)
        {
            var p1 = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Reportes" });
            var p2 = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" });
            var permitido = false;
            if (p1 != null)
            {
                if (p2 != null)
                {
                    if (p1.Consultar || p2.Consultar)
                    {
                        permitido = true;
                    }

                }
                else
                {
                    if (p1.Consultar) permitido = true;
                }
            }
            else
            {
                if (p2 != null)
                {
                    if (p2.Consultar) permitido = true;
                }
            }
            if (permitido)
            {
                try
                {
                    if (!emptytable)
                    {
                        List<Entity.ListadoReporteExamenMedico> data = new List<Entity.ListadoReporteExamenMedico>();
                        List<Entity.ListadoReporteExamenMedico> listTotals = new List<Entity.ListadoReporteExamenMedico>();

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        object[] dataParams = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            true
                        };

                        listTotals = ControlListadoReporteExamenMedico.ObtenerTodos(dataParams);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value.ToLower());
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Sexo).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Sexo).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToDateTime(x.FechaExamen)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToDateTime(x.FechaExamen)).ToList();
                                    break;
                            }
                        }

                        List<Entity.ListadoReporteExamenMedico> listFinal = new List<Entity.ListadoReporteExamenMedico>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;

                        //    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        //    List<Where> where = new List<Where>();
                        //    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        //    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        //    int usuario;

                        //    int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        //    Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                        //    where.Add(new Where("EI.ContratoId", contratoUsuario.IdContrato.ToString()));
                        //    where.Add(new Where("EI.Tipo", contratoUsuario.Tipo));

                        //    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        //    else throw new Exception("No es posible obtener información del usuario en el sistema");

                        //    object[] paramsAux = new object[]
                        //    {
                        //        contratoUsuario.IdContrato,
                        //        contratoUsuario.Tipo,
                        //        true
                        //    };

                        //    var data = ControlListadoReporteExamenMedico.ObtenerTodos(paramsAux);

                        //    where.Add(new Where("EM.Activo", "1"));

                        //    Query query = new Query
                        //    {
                        //        select = new List<string> {
                        //    "I.Id",
                        //    "I.Nombre",
                        //    "I.Paterno",
                        //    "I.Materno",
                        //    "I.TrackingId",
                        //    "S.Nombre Sexo",
                        //    "cast(EI.Expediente as unsigned) Expediente",
                        //    "CONCAT(I.Nombre,' ',Paterno,' ',Materno) NombreCompleto",
                        //    "EM.TrackingId ExamenTrackingId",
                        //    "date_format(EM.HoraYFecha, '%Y-%m-%d %H:%i:%S') Fecha"
                        //},
                        //        from = new Table("detenido", "I"),
                        //        joins = new List<Join>
                        //{
                        //    new Join(new Table("informaciondedetencion", "ID"), "I.Id = ID.IdInterno"),
                        //    new Join(new Table("detalle_detencion", "EI"), "I.id  = EI.DetenidoId"),
                        //    new Join(new Table("general", "G"), "I.Id = G.DetenidoId"),
                        //    new Join(new Table("examen_medico", "EM"), "EI.Id  = EM.DetalleDetencionId"),
                        //    new Join(new Table("sexo", "S"), "G.SexoId = S.Id"),
                        //},
                        //        wheres = where
                        //    };

                        //    DataTables dt = new DataTables(mysqlConnection);
                        //    return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        object json2 = new { data = new List<ListadoReporteExamenMedico>(), recordsTotal = new List<ListadoReporteExamenMedico>().Count, recordsFiltered = new List<ListadoReporteExamenMedico>().Count, };
                        return json2;
                    }
                }
                catch (Exception ex)
                {
                    object json2 = new { data = new List<ListadoReporteExamenMedico>(), recordsTotal = new List<ListadoReporteExamenMedico>().Count, recordsFiltered = new List<ListadoReporteExamenMedico>().Count, };
                    return json2;
                }
            }
            else
            {
                object json2 = new { data = new List<ListadoReporteExamenMedico>(), recordsTotal = new List<ListadoReporteExamenMedico>().Count, recordsFiltered = new List<ListadoReporteExamenMedico>().Count, };
                return json2;
            }
        }

        [WebMethod]
        public static string pdf(string datos)
        {
            var serializedObject = string.Empty;
            try
            {
                var p1 = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Reportes" });
                var p2 = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Examen médico" });
                var permitido = false;
                if (p1 != null)
                {
                    if (p2 != null)
                    {
                        if (p1.Consultar || p2.Consultar)
                        {
                            permitido = true;
                        }

                    }
                    else
                    {
                        if (p1.Consultar) permitido = true;
                    }
                }
                else
                {
                    if (p2 != null)
                    {
                        if (p2.Consultar) permitido = true;
                    }
                }
                if (permitido)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema.");

                    var data = JsonConvert.DeserializeObject<PrivateJsonClass>(datos);
                    var interno = ControlDetenido.ObtenerPorId(data.id);

                    string[] Id = { interno.Id.ToString(), true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorId(data.idDetalle);
                    if (detalleDetencion == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro la información en el sistema." });

                    var examenesMedicos = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);

                    if (examenesMedicos.Count > 0)
                    {
                        var examenMedico = examenesMedicos.FirstOrDefault(e => e.TrackingId == new Guid(data.trackingId));
                        var obj = ControlPDFEM.ReporteEM(interno.Id, interno.TrackingId, examenMedico != null ? examenMedico.Id : 0);
                        serializedObject = JsonConvert.SerializeObject(obj);
                    }
                    else
                    {
                        serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El detenido no cuenta con examen médico." });
                    }

                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        public class AdicionalAux
        {
            public string Id { get; set; }
        }
        class PrivateJsonClass
        {
            public int id { get; set; }
            public string trackingId { get; set; }
            public int idDetalle { get; set; }
        }
    }
}