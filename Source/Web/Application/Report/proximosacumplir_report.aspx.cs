﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.Services;

namespace Web.Application.Report
{
    public partial class proximosacumplir_report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            verifyPermission(8);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static string pdf()
        {
            var serializedObject = string.Empty;
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            //var contrato = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato);
            var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);

            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 8 });

                if (permiso != null)
                {
                    var obj = ControlPDFJuez.ReporteProximosaCumplir(institucion.Id, contratoUsuario.IdContrato);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static DataTable getDetenidos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 8 });

                if (permiso != null)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var contrato = contratoUsuarioK.IdContrato;

                        where.Add(new Where("Contrato", contrato.ToString()));
                        //Tengo el id del contratousuario, su contratoid puede ser un contrato o un subcontrato
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        Query query = new Query
                        {
                            select = new List<string>{
                                "EstatusTracking",
                                "RutaImagen",
                                "trim(Nombre) Nombre",
                                "trim(Paterno) Paterno",
                                "trim(Materno) Materno",
                                "cast(Expediente as unsigned) Expediente",
                                "Fecha",
                                "TotalHoras",
                                "TotalAPagar",
                                "Situacion",
                                "Contrato",
                                "Concat(Nombre,' ',trim(Paterno),' ',trim(Materno)) NombreCompleto"
                            },
                            from = new Table("vproximosacumplir", "I"),//,
                            wheres = where
                        };
                        
                        DataTables dt = new DataTables(mysqlConnection);
                        var data = dt.Generar(query, draw, start, length, search, order, columns);
                        return data;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
    }
}