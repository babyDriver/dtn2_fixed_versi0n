﻿using Business;
using Entity.Util;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;

namespace Web.Application.Report
{
    public partial class evidencias_report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(14);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });
            string[] parametros = { usId.ToString(), "Control de pertenencias/evidencias" };

            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (!permitido)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }
        [WebMethod]
        public static string ValidarBoton()
        {
            try
            {
                var permitido = false;
                var p1 = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de pertenencias/evidencias" });
                if (p1 != null)
                {
                    if (p1.Consultar)
                    {
                        permitido = true;
                    }
                }
                return JsonConvert.SerializeObject(new { success = true, Boton = permitido });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }
        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 14 });
            string[] parametros = { usId.ToString(), "Control de pertenencias/evidencias" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (permitido)
            {
                try
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Estatus", "<>", "2"));
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");
                        
                        var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var contrato = contratoUsuarioK.IdContrato;

                        where.Add(new Where("E.ContratoId", contratoUsuarioK.IdContrato.ToString()));
                        where.Add(new Where("E.Tipo", contratoUsuarioK.Tipo));
                        where.Add(new Where("P.Activo", "1"));
                        where.Add(new Where("P.Clasificacion", "1"));
                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("PL.Accion", "Registrado"));
                        where.Add(new Where("P.Estatus", "4"));
                        //where.Add(new Where("EPL.Nombre", "Entregado"));

                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }

                        Query query = new Query
                        {
                            select = new List<string> 
                            {
                                "I.Id",
                                "I.TrackingId",
                                "I.Nombre",
                                "I.Paterno",
                                "I.Materno",
                                "CAST(E.Expediente as unsigned) Expediente",
                                "E.Estatus",
                                "E.Activo",
                                "E.TrackingId TrackingIdEstatus" ,
                                "ES.Nombre EstatusNombre",
                                "P.Cantidad",
                                "P.Observacion",
                                "P.Pertenencia",
                                "P.CreadoPor",
                                "U.Usuario NombreUsuario",
                                "PL.Fec_Movto FRecibe",
                                "concat(I.Nombre,' ',trim(Paterno),' ',trim(Materno)) NombreCompleto"
                                //"US.Nombre Entregado",
                                //"EPL.Fec_Movto FEntrega"
                            },
                            from = new Table("pertenencia", "P"),
                            joins = new List<Join>
                            {
                                new Join(new Table("detalle_detencion", "E"), "E.id  = P.InternoId"),
                                new Join(new Table("Detenido", "I"), "E.DetenidoId = I.Id"),
                                new Join(new Table("estatus", "ES"), "ES.id  = E.Estatus"),
                                new Join(new Table("vusuarios", "V"), "P.CreadoPor = V.Id"),
                                new Join(new Table("usuario", "U"), "V.UsuarioId = U.Id"),
                                new Join(new Table("pertenencialog", "PL"), "P.Id = PL.Id"),
                            },
                            wheres = where,
                            groupBy = "P.Id"
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        var _dt= dt.Generar(query, draw, start, length, search, order, columns);
                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string pdf(string Inicio, string Fin)
        {
            var serializedObject = string.Empty;

            if (Convert.ToDateTime(Inicio) > Convert.ToDateTime(Fin))
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." });
            }

            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 14 });
                string[] parametros = { usId.ToString(), "Control de pertenencias/evidencias" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var contrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    var institucion = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                    var tipo = contratoUsuario.Tipo;

                    var obj = ControlPDFJuez.ReporteEvidencias(contratoUsuario.IdContrato, institucion.Nombre, Inicio, Fin, tipo);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}
