﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Report
{
    public partial class recibo_donacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(16);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            string[] parametros = { usId.ToString(), "Control de pertenencias/evidencias" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (!permitido)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }
        
        [WebMethod]
        public static DataTable getData(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 16 });
                string[] parametros = { usId.ToString(), "Control de pertenencias/evidencias" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        where.Add(new Where("C.Activo", "1"));

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if (contratoUsuario != null)
                        {
                            where.Add(new Where("EI.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("EI.Tipo", contratoUsuario.Tipo));
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                                "C.Id",
                                "C.TrackingId",
                                "C.PertenenciasDe",
                                "US.Usuario  UsuarioQueRegistro",
                                "date_format(C.FechaEntrega, '%Y-%m-%d %H:%i:%S') Fecha",
                                "EI.TrackingId TrackingInterno"
                            },
                            from = new DT.Table("donacion_pertenencias_evidencias", "C"),
                            joins = new List<Join> {
                                new Join(new DT.Table("donacion_detalle_pertenencias_evidencias", "CC"), "CC.DonacionId = C.Id", "INNER"),
                                new Join(new DT.Table("pertenencia", "P"), "P.Id = CC.PertenenciaId", "INNER"),
                                new Join(new DT.Table("detalle_detencion", "EI"), "EI.Id = P.InternoId", "INNER"),
                                new Join(new DT.Table("usuario", "U"), "U.Usuario = C.usuarioqueregistro" ),
                                new Join(new DT.Table("Vusuarios", "V"), "V.Id = U.Id" ),
                                new Join(new DT.Table("usuario", "US"), "US.Id = V.UsuarioId" )
                            },
                            wheres = where,
                            groupBy = "C.Id"
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static string generarReciboDonacion(string[] datos)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 16 });
                string[] parametros = { usId.ToString(), "Control de pertenencias/evidencias" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    var devolucion = ControlDonacionPertenencia.ObtenerByTrackingId(new Guid(datos[0].ToString()));
                    var detalle = devolucion != null ? ControlDonacionDetallePertenencia.ObtenerByDonacionId(devolucion.Id) : null;
                    List<PertenenciaAux> pertenencias = new List<PertenenciaAux>();
                    string bolsa = "";

                    if (detalle != null)
                    {
                        foreach (var item in detalle)
                        {
                            var pertenencia = ControlPertenencia.ObtenerPorId(item.PertenenciaId);

                            if (pertenencia != null)
                            {
                                if (pertenencia.Bolsa.ToString() != "0")
                                {
                                    bolsa = pertenencia.Bolsa.ToString();
                                }

                                PertenenciaAux valAux = new PertenenciaAux()
                                {
                                    Bolsa = pertenencia.Bolsa.ToString(),
                                    Cantidad = pertenencia.Cantidad.ToString(),
                                    Clasificacion = ControlClasificacionEvidencia.ObtenerPorId(pertenencia.Clasificacion).Nombre,
                                    Estatus = pertenencia.Estatus.ToString(),
                                    Id = pertenencia.Id.ToString(),
                                    Nombre = pertenencia.PertenenciaNombre,
                                    Observacion = pertenencia.Observacion
                                };

                                pertenencias.Add(valAux);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Hubo un error al recuperar la información");
                    }

                    var detalleDetencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[1].ToString()));
                    string remision = detalleDetencion != null ? detalleDetencion.Expediente : "";
                    var interno = detalleDetencion != null ? ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId) : null;
                    var domicilio = interno != null ? ControlDomicilio.ObtenerPorId(interno.DomicilioId) : null;
                    string nombreInterno = interno != null ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "";
                    string colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).Asentamiento : "" : "";
                    string cp = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "" : "";
                    string calle = (domicilio != null) ? (domicilio.Calle != null) ? domicilio.Calle : "" : "";
                    string numero = (domicilio != null) ? (domicilio.Numero != null) ? domicilio.Numero : "" : "";
                    string domicilioCompleto = "Calle: " + calle + " #" + numero + " Colonia:" + colonia + " C.P." + cp;
                    string recibio = devolucion.PersonaQueRecibe;
                    var usuario = ControlUsuario.Obtener(usId);
                    if (usuario.RolId == 13)
                    {
                        usuario = ControlUsuario.Obtener(8);
                    }
                    string nombreEntregaA = usuario.User;

                    object[] dataArchivo = ControlPDF.GenerarReciboDonacionesPDF(pertenencias, nombreInterno, remision, domicilioCompleto, recibio, bolsa, nombreEntregaA, interno.Id);

                    return JsonConvert.SerializeObject(new { success = true, message = "guardó", ubicacionArchivo = (dataArchivo != null) ? dataArchivo[1].ToString() : "" });
                }
                return JsonConvert.SerializeObject(new { success = false, message = "No cuenta con privilegios para realizar la acción." });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }
    }
}