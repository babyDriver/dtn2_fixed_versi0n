﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using System.Linq;


namespace Web.Application.Report
{
    public partial class reporte_examen_medico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //  validatelogin();
            verifyPermission(7);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static DataTable getinterno(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 7 });

            if (permiso != null)
            {
                try
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                        where.Add(new Where("EI.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("EI.Tipo", contratoUsuario.Tipo));

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        where.Add(new Where("EM.Activo", "1"));

                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "I.Id",
                                "I.Nombre",
                                "I.Paterno",
                                "I.Materno",
                                "I.TrackingId",
                                "S.Nombre Sexo",
                                "EM.TrackinngId ExamenTrackingId",
                                "EM.Id ExamenMedicoId",
                                "ifnull(EM.CertificadoQuimicoId,0) CertificadoQuimicoId",
                                "ifnull(EM.CertificadoLesionId,0) CertificadoLesionId",
                                "ifnull(EM.CertificadoMedicoPsicofisiologicoId,0) CertificadoMedicoPsicofisiologicoId",
                                "DATE_FORMAT(EM.FechaRegistro,'%Y-%m-%d %H:%i') Evaluacion",
                                "DATE_FORMAT(ID.HoraYFecha,'%Y-%m-%d %H:%i') Registro",
                                //"DATE_FORMAT(EI.Fecha,'%Y-%m-%d %H:%i') Registro",
                                "concat(EI.NombreDetenido,' ',trim(APaternoDetenido),' ',trim(AMaternoDetenido)) NombreCompleto"
                            },
                            from = new Table("detenido", "I"),
                            joins = new List<Join>
                            {
                                new Join(new Table("informaciondedetencion", "ID"), "I.Id = ID.IdInterno"),
                                new Join(new Table("detalle_detencion", "EI"), "I.id  = EI.DetenidoId"),
                                new Join(new Table("general", "G"), "I.Id = G.DetenidoId"),
                                new Join(new Table("servicio_medico", "EM"), "EI.Id  = EM.DetalleDetencionId"),
                                new Join(new Table("sexo", "S"), "G.SexoId = S.Id"),
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static object pdf(int detenidoId, int examenId)
        {
            var serializedObject = string.Empty;
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 7 });

                if (permiso != null)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");
                    Entity.ServicioMedico servicioMedico = new Entity.ServicioMedico();
                    if (detenidoId == 1)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoPsicofisiologico(servicioMedico);
                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }
                    if (detenidoId == 2)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoLesion(servicioMedico);
                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }
                    if (detenidoId == 3)
                    {
                        servicioMedico = ControlServicoMedico.ObtenerPorId(examenId);

                        object[] dataArchivo = null;
                        Entity.ImprimeResultadoCertificadoQuimico resultadoCertificadoQuimico = new Entity.ImprimeResultadoCertificadoQuimico();
                        resultadoCertificadoQuimico.PrintEtanol = true;
                        resultadoCertificadoQuimico.PrintBenzodiazepina = true;
                        resultadoCertificadoQuimico.PrintAnfetamina = true;
                        resultadoCertificadoQuimico.PrintCannabis = true;
                        resultadoCertificadoQuimico.PrintCocaina = true;
                        resultadoCertificadoQuimico.PrintExtasis = true;
                        dataArchivo = ControlPDFServicioMedico.GeneraCertificadoQuimico(servicioMedico, resultadoCertificadoQuimico);
                        return new { exitoso = true, mensaje = "", ServicioMedTracingId = servicioMedico.TrackinngId, Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };

                    }

                    var detenido = ControlDetenido.ObtenerPorId(detenidoId);

                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                    //var obj = ControlPDFEM.ReporteEM(detenido.Id, detenido.TrackingId, examenId);
                    //serializedObject = JsonConvert.SerializeObject(obj);
                    //return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }

            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }

        }

        //[WebMethod]
        //public static string pdf(string datos)
        //{
        //    var serializedObject = string.Empty;
        //    try
        //    {
        //        if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Reportes" }).Consultar)
        //        {
        //            var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

        //            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
        //            int usuario;

        //            if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
        //            else throw new Exception("No es posible obtener información del usuario en el sistema.");

        //            var data = JsonConvert.DeserializeObject<PrivateJsonClass>(datos);
        //            var interno = ControlDetenido.ObtenerPorId(data.id);

        //            string[] Id = { interno.Id.ToString(), true.ToString() };
        //            Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).FirstOrDefault();
        //            if(detalleDetencion == null) return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No se encontro la información en el sistema." });

        //            var examenesMedicos = ControlExamenMedico.ObtenerTodosPorDetalleDetencionId(detalleDetencion.Id);

        //            if (examenesMedicos.Count > 0)
        //            {
        //                var examenMedico = examenesMedicos.FirstOrDefault(e => e.TrackingId == new Guid(data.trackingId));
        //                var obj = ControlPDFEM.ReporteEM(interno.Id, interno.TrackingId, examenMedico != null ? examenMedico.Id : 0);
        //                serializedObject = JsonConvert.SerializeObject(obj);
        //            }
        //            else
        //            {
        //                serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = "El detenido no cuenta con examen médico." });
        //            }

        //            return serializedObject;
        //        }
        //        else
        //        {
        //            return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
        //    }
        //}

        public class AdicionalAux
        {
            public string Id { get; set; }
        }
        class PrivateJsonClass
        {
            public int id { get; set; }
            public string trackingId { get; set; }
        }
    }
}