﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Web.Security;
using System.Web.Services;
using System.Web;
using System.Linq;
using Entity;

namespace Web.Application.Report
{
    public partial class reportes_detencion_boleta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(2);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });
            string[] parametros = { usId.ToString(), "Registro en barandilla" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (!permitido)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getinterno(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string pages)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 2 });
            string[] parametros = { usId.ToString(), "Registro en barandilla" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (permitido)
            {
                try
                {
                    if (!emptytable)
                    {

                        List<Entity.ListadoReporteDetencionBoleta> data = new List<Entity.ListadoReporteDetencionBoleta>();
                        List<Entity.ListadoReporteDetencionBoleta> listTotals = new List<Entity.ListadoReporteDetencionBoleta>();

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        object[] dataParams = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0
                        };

                        listTotals = ControlListadoReporteDetencionBoleta.ObtenerTodos(dataParams);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value.ToLower());
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Sexo).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Sexo).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estado).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estado).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Municipio).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Municipio).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Colonia).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Colonia).ToList();
                                    break;                                
                            }
                        }

                        List<Entity.ListadoReporteDetencionBoleta> listFinal = new List<Entity.ListadoReporteDetencionBoleta>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;
                    }
                    else
                    {
                        object json2 = new { data = new List<ListadoReporteDetencionBoleta>(), recordsTotal = new List<ListadoReporteDetencionBoleta>().Count, recordsFiltered = new List<ListadoReporteDetencionBoleta>().Count, };
                        return json2;
                    }
                }
                catch (Exception ex)
                {
                    object json2 = new { data = new List<ListadoReporteDetencionBoleta>(), recordsTotal = new List<ListadoReporteDetencionBoleta>().Count, recordsFiltered = new List<ListadoReporteDetencionBoleta>().Count, };
                    return json2;
                }
            }
            else
            {
                object json2 = new { data = new List<ListadoReporteRemision>(), recordsTotal = new List<ListadoReporteRemision>().Count, recordsFiltered = new List<ListadoReporteRemision>().Count, };
                return json2;
            }
        }

        [WebMethod]
        public static string pdf(string[] datos)
        {
            var serializedObject = string.Empty;
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 2 });
                string[] parametros = { usId.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    int InternoId = Convert.ToInt32(datos[0]);

                    var obj = ControlPDFBarandilla.GenerarInformeDetencion(InternoId);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string pdfBoleta(string[] datos)
        {
            var serializedObject = string.Empty;
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 2 });
                string[] parametros = { usId.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    int InternoId = Convert.ToInt32(datos[0]);

                    var obj = ControlPDFBarandilla.GenerarReporteBoleta(InternoId);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}