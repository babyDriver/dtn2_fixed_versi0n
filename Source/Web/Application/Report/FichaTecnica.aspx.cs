using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;

namespace Web.Application.Report
{
    public partial class FichaTecnica : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tbFechaInicio.Text = DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                tbFechaFin.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            validatelogin();
            verifyPermission(21);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static List<Combo> getCustomers()
        {
            var combo = new List<Combo>();
            var c = ControlCliente.ObtenerTodos().Where(x => x.Habilitado);

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

            var clientes = ControlContrato.ObetenerPorUsuarioId(usId).Select(x => x.ClienteId).Distinct();

            foreach (var i in clientes)
            {
                var cliente = c.FirstOrDefault(x => x.Id == i);
                if (cliente != null) combo.Add(new Combo { Desc = cliente.Nombre, Id = cliente.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetContracts(string Clienteid)
        {
            var combo = new List<Combo>();

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var contratos = ControlContrato.ObetenerPorUsuarioId(usId).Where(x => x.Habilitado && x.Activo).OrderBy(x => x.Id);
            var id = 0;
            foreach (var _contrato in contratos)
            {
                if (id != _contrato.Id)
                {
                    if (Clienteid != "0" && !string.IsNullOrEmpty(Clienteid))
                    {
                        if (_contrato.ClienteId == Convert.ToInt32(Clienteid))
                        {
                            combo.Add(new Combo { Desc = _contrato.Nombre, Id = _contrato.Id.ToString() });
                        }
                    }
                    else
                    {
                        combo.Add(new Combo { Desc = _contrato.Nombre, Id = _contrato.Id.ToString() });
                    }
                    id = _contrato.Id;
                }


            }
            return combo;
        }

        [WebMethod]
        public static string pdf(string tracking)
        {
            var serializedObject = string.Empty;
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 21 });

                if (permiso != null)
                {
                    var detalledetencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(tracking));
                    var ficha = new Entity.FichaTecnica();
                    ficha.TrackingId = Guid.NewGuid();
                    ficha.DetenidoId = detalledetencion.Id;
                    ficha.Fecha = DateTime.Now;
                    ficha.Creadopor = usId;
                    ficha.Id = ControlFichaTecnica.Guardar(ficha);
                    if (ficha.Id < 1) throw new Exception("Hubo un problema al realizar el reporte de la ficha tecnica");
                    var obj = ControlPDFBarandilla.GenerarFichaTecnica(detalledetencion, ficha.Id);
                    //var obj = ControlPDFBarandilla.GenerarInformeDetencion(InternoId);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static List<Combo> GetSubcontracts(string Contratoid)
        {
            var combo = new List<Combo>();

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var sub = ControlSubcontrato.ObtenerByUsuarioId(usId);

            if (!string.IsNullOrWhiteSpace(Contratoid) && Contratoid != "0")
            {
                foreach (var _subcontrato in sub.Where(x => x.ContratoId == Convert.ToInt32(Contratoid) && x.Habilitado))
                {
                    combo.Add(new Combo { Desc = _subcontrato.Nombre, Id = _subcontrato.Id.ToString() });
                }
            }
            else
            {
                foreach (var _subcontrato in sub.Where(x => x.Habilitado))
                {
                    combo.Add(new Combo { Desc = _subcontrato.Nombre, Id = _subcontrato.Id.ToString() });
                }
            }
            return combo;
        }

        [WebMethod]
        public static DataTable getDetenidos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string clienteid, string contratoid, string subcontratoid, string fechaInicio, string fechaFin)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 21 });

                if (permiso != null)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var contrato = contratoUsuarioK.IdContrato;

                        var pertenencias = ControlPertenencia.ObtenerPorInterno(usId);

                        // delimitar un rango de fechas
                        where.Add(new Where("cast(D.HoraYFecha as DATE)", ">=", fechaInicio));
                        where.Add(new Where("cast(D.HoraYFecha as DATE)", "<=", fechaFin));

                        if (!string.IsNullOrEmpty(clienteid) && clienteid != "0")
                        {
                            where.Add(new Where("N.ClienteId", "=", clienteid.ToString()));
                        }
                        //if (!string.IsNullOrEmpty(contratoid) && contratoid != "0")
                        //{
                        //    where.Add(new Where("A.ContratoId", "=", contratoid.ToString()));
                        //}

                        if (!string.IsNullOrEmpty(subcontratoid) && subcontratoid != "0")
                        {
                            where.Add(new Where("M.Id", "=", subcontratoid.ToString()));
                        }

                        //Tengo el id del contratousuario, su contratoid puede ser un contrato o un subcontrato
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        //"cast(A.Expediente as unsigned) Expediente",

                        Query query = new Query
                        {
                            select = new List<string>{
                                "DD.Id DetalleDetencionId",
                                "DD.TrackingId",
                                "DD.Expediente",
                                "DD.NombreDetenido as Nombre",
                                "DD.DetalledetencionEdad as Edad",
                                "D.Descripcion Evento",
                                "W.nombreMotivoLlamada Motivo",
                                "ifnull(Q.Nombre, 'n/a') Unidad",
                                "ifnull(H.Nombre, 'n/a') Responsable",
                                //"'' Unidad",
                                //"'' Responsable",
                                "'' Conclusion",
                                "date_format(D.HoraYFecha, '%Y-%m-%d %H:%i:%S') Detencion",
                                //"P.Nombre Situacion",
                                //"L.TotalHoras Horas",
                                "'' Situacion",
                                "'' Horas",
                                "M.Subcontrato Barandilla",
                                "N.ClienteId",
                                "N.Id ContratoId",
                                "M.Id SubcontratoId"

                            },
                            from = new Table("informaciondedetencion", "C"),                            
                            joins = new List<Join>
                            {                                
                                //traduce view                                             
                                //new Join(new Table("detenido_evento", "F"), "C.IdDetenido_Evento = F.Id "), 
                                new Join(new Table("detalle_detencion", "DD"), "C.IdInterno  = DD.DetenidoId"), 
                                new Join(new Table("eventos", "D"), "C.IdEvento  = D.Id"),
                                new Join(new Table("wsamotivo", "W"), "D.MotivoId = W.idMotivo"),
                                new Join(new Table("responsable_unidad", "H"), "C.IdResponsable = H.Id"),
                                new Join(new Table("unidad", "Q"), "C.IdUnidad = Q.Id"),
                                //new Join(new Table("calificacion", "L"), "A.Id = L.InternoId"),
                                //new Join(new Table("situacion_detenido", "P"), "L.SituacionId = P.Id"),
                                new Join(new Table("subcontrato", "M"), "D.ContratoId  = M.Id"),
                                new Join(new Table("contrato", "N"), "M.ContratoId  = N.Id")
                                //new Join(new Table("cliente", "O"), "N.ClienteId  = O.Id"),
                            },
                            wheres = where,
                        };

                        DataTablesAux dt = new DataTablesAux(mysqlConnection);
                        var _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
    }
}
