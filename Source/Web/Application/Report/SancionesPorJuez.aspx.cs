﻿using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;

namespace Web.Application.Report
{
    public partial class SancionesPorJuez : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validatelogin();
            verifyPermission(12);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 12 });

            if (permiso != null)
            {
                try
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();
                        //where.Add(new Where("ES.Activo", "1"));
                        //where.Add(new Where("C.Activo", "1"));
                        var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                        var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                        int usuario;

                        if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                        else throw new Exception("No es posible obtener información del usuario en el sistema");

                        var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var contrato = contratoUsuarioK.IdContrato;
                        where.Add(new Where("ES.ContratoId", contratoUsuarioK.IdContrato.ToString()));
                       // where.Add(new Where("ES.estatus", "1"));
                        where.Add(new Where("ifnull(A.Id,0)", "0"));
                        if ((!string.Equals(perfil, "Administrador")))
                        {
                            var user = ControlUsuario.Obtener(usuario);
                            //where.Add(new Where("E.CentroId", user.CentroId.ToString()));
                        }
                        Query query = new Query
                        {
                            select = new List<string> 
                            {
                                "C.Id",
                                "C.TrackingId",
                                "U.Usuario",
                                "U.ApellidoPaterno",
                                "U.ApellidoMaterno",
                                "CAST( ES.Expediente AS unsigned) Expediente",
                                "date_format(ES.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "D.Nombre",
                                "D.Paterno",
                                "D.Materno",
                                "C.TotalDeMultas",
                                "C.TotalHoras",
                                "concat(D.Nombre,' ',trim(Paterno),' ',trim(Materno)) NombreCompleto"
                            },
                            from = new Table("calificacion", "C"),
                            joins = new List<Join>
                            {
                                new Join(new Table("detalle_detencion", "ES"), "ES.Id  = C.InternoId"), 
                                new Join(new Table("Detenido", "D"), "Es.DetenidoId = D.Id"),
                                new Join(new Table("vusuarios", "V"), "V.Id  = C.CreadoPor"),
                                new Join(new Table("Usuario", "U"), "U.Id  = V.UsuarioId"),
                                new Join(new Table("historico_agrupado_detenido", "A"), "ES.Id  = A.DetalleDetencionId")
                            },
                            wheres = where,
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt= dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                catch (Exception ex)
                {
                    return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
                }
            }
            else
            {
                return DataTables.ObtenerDataTableVacia("", draw);
            }
        }

        [WebMethod]
        public static string pdf(string Inicio, string Fin)
        {
            var serializedObject = string.Empty;

            if (Convert.ToDateTime(Inicio) > Convert.ToDateTime(Fin))
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." });
            }

            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 12 });

                if (permiso != null)
                {
                    var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                    var contratoId = 0;
                    var institucionId = 0;
                    var tipo = "";

                    foreach (var k in dataContratos)
                    {
                        if (k.IdUsuario == usId)
                        {
                            contratoId = k.IdContrato;
                            var contrato = ControlContrato.ObtenerPorId(k.IdContrato);
                            institucionId = contrato.InstitucionId;
                            tipo = k.Tipo;
                        }
                    }

                    var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    var institucion = ControlInstitucion.ObtenerPorId(institucionId);

                    string[] parametros = new string[3] { contratoUsuarioK.IdContrato.ToString(), Inicio, Fin };

                    var listasanciones = ControlReporteSancionesJuezCalificador.GetreporteSanciones(parametros);

                    if (listasanciones.Count == 0)
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados" });
                    }

                    var obj = ControlPDFJuez.ReporteSanciones(contratoUsuarioK.IdContrato, institucion.Nombre, Inicio, Fin, tipo);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }
}