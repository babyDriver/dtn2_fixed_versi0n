﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="SancionesPorJuez.aspx.cs" Inherits="Web.Application.Report.SancionesPorJuez" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
    <li>Juez calificador</li>
    <li>Sanciones por juez</li>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #content {
            height: 600px;
        }
        div.scroll {
            height: 590px;
            overflow: auto;
            overflow-x: hidden;
        }
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
              <i class="fa fa-file-text-o fa-fw "></i>
              Sanciones por juez
            </h1>
        </div>
    </div>
    <asp:Label ID="Label1" runat="server"></asp:Label>
    <section id="widget-grid-sanciones" class="">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-sanciones-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                       <%-- <span class="widget-icon"></span> --%>              <i style="float:left; margin-top:10px; margin-left:5px;" class="fa fa-legal fa-fw "></i>

                        <h2>Sanciones por juez</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div id="smart-form-register-entry" class="smart-form">
                            <fieldset>
                               
                                <div class="row">
                                    <section class="col col-3">
                                                    <label class="label">Fecha de inicio <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <label class='input-group date' >
                                                        <input type="text" name="inicio" id="inicio" placeholder="Fecha de inicio" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha de inicio.</b>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                    </label>
                                                </section>
                                    <section class="col col-3">
                                                    <label class="label">Fecha final <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <label class='input-group date' >
                                                        <input type="text" name="final" id="final" placeholder="Fecha final" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha final.</b>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                    </label>
                                                </section>
                                    <section class="col col-3">
                                        <a class="btn btn-success btn-md reportePdf" style="margin-top:22px;padding:5px"><i class="glyphicon glyphicon-file"></i> Generar PDF</a>
                                    </section>
                                    </div>
                                </fieldset>
                            </div>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Juez</th>
                                        <th >No. remisión</th>
                                        <th>Fecha</th>
                                        <th>Detenido</th>
                                        <th>Multa económica</th>
                                        <th>Horas de detención</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        </section>
    </div>
    <input type="hidden" id="Hidden1" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            
            $("body").on("click", ".reportePdf", function () {
                $("#ctl00_contenido_lblMessage").html("");

                if ($("#ctl00_contenido_inicio").val() == "" || $("#ctl00_contenido_final").val() == "") {
                    ShowAlert("¡Aviso!", "Seleccione un rango de fechas");
                }
                else {
                    pdf();
                }
            });
            

            $('#ctl00_contenido_inicio').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });
            $('#ctl00_contenido_final').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });


            function pdf() {
                
                $.ajax({

                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/SancionesPorJuez.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        Inicio: $("#ctl00_contenido_inicio").val(),
                        Fin: $("#ctl00_contenido_final").val()
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "order": [[6, "desc"], [7, "desc"]],
                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/SancionesPorJuez.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    null,
                    null,
                    {
                        name: "Juez",
                        data: "Usuario"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },
                    {
                        name: "Detenido",
                        data: "NombreCompleto"
                    },
                    {
                        name: "Multa",
                        data: "TotalDeMultas"
                    },
                    {
                        name: "Horas",
                        data: "TotalHoras"
                    }
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    }
                ]

            });
        });
    </script>
</asp:Content>