﻿using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;
using System.Linq;
using Entity;

namespace Web.Application.Report
{
    public partial class ReporteIngresos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(6);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });
            string[] parametros = { usId.ToString(), "Registro en barandilla" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (!permitido)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string pages)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 6 });
            string[] parametros = { usId.ToString(), "Registro en barandilla" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (permitido)
            {
                try
                {
                    if (!emptytable)
                    {
                        List<Entity.ListadoReporteIngresos> data = new List<Entity.ListadoReporteIngresos>();
                        List<Entity.ListadoReporteIngresos> listTotals = new List<Entity.ListadoReporteIngresos>();

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        object[] dataParams = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0
                        };

                        listTotals = ControlListadoReporteIngresos.ObtenerTodos(dataParams);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value.ToLower());
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Detenido).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Detenido).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Fecha).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Fecha).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Folio).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Folio).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Motivo).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Motivo).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.TotalHoras).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.TotalHoras).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Unidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Unidad).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Responsable).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Responsable).ToList();
                                    break;
                                case 10:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.LugarDetencion).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.LugarDetencion).ToList();
                                    break;
                            }
                        }

                        List<Entity.ListadoReporteIngresos> listFinal = new List<Entity.ListadoReporteIngresos>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;
                    }
                    else
                    {
                        object json2 = new { data = new List<ListadoReporteIngresos>(), recordsTotal = new List<ListadoReporteIngresos>().Count, recordsFiltered = new List<ListadoReporteIngresos>().Count, };
                        return json2;
                    }
                }
                catch (Exception ex)
                {
                    object json2 = new { data = new List<ListadoReporteIngresos>(), recordsTotal = new List<ListadoReporteIngresos>().Count, recordsFiltered = new List<ListadoReporteIngresos>().Count, };
                    return json2;
                }
            }
            else
            {
                object json2 = new { data = new List<ListadoReporteIngresos>(), recordsTotal = new List<ListadoReporteIngresos>().Count, recordsFiltered = new List<ListadoReporteIngresos>().Count, };
                return json2;
            }
        }

        [WebMethod]
        public static object pdf(string Inicio, string Fin)
        {
            var serializedObject = string.Empty;

            if (Convert.ToDateTime(Inicio) > Convert.ToDateTime(Fin))
            {
                return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
            }

            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 6 });
                string[] parametros1 = { usId.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros1);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    var dataContratos = ControlContratoUsuario.ObtenerPorUsuarioId(usId);
                    var contratoId = 0;
                    var institucionId = 0;
                    var tipo = "";

                    foreach (var k in dataContratos)
                    {
                        if (k.IdUsuario == usId)
                        {
                            contratoId = k.IdContrato;
                            var contrato = ControlContrato.ObtenerPorId(k.IdContrato);
                            institucionId = contrato.InstitucionId;
                            tipo = k.Tipo;
                        }
                    }

                    var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                    var institucion = ControlInstitucion.ObtenerPorId(institucionId);

                    Inicio = Inicio + " 00:00:00";
                    Fin = Fin + " 23:59:59";

                    object[] parametros = new object[4] { contratoUsuarioK.Tipo, contratoUsuarioK.IdContrato.ToString(), Convert.ToDateTime(Inicio), Convert.ToDateTime(Fin) };

                    var listadoreporteingresos = ControlListadoReporteIngresos.ObtenerPorFechas(parametros);

                    //var listasanciones = ControlReporteSancionesJuezCalificador.GetreporteSanciones(parametros);

                    if (listadoreporteingresos.Count == 0)
                    {
                        return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "no se encontraron resultados" });
                    }

                    //var obj = ControlPDFBarandilla.GeneraReporteIngresos(listadoreporteingresos.LastOrDefault().DetalledetencionId,listadoreporteingresos,parametros);
                    //serializedObject = JsonConvert.SerializeObject(obj[1]);
                    //return serializedObject;

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFBarandilla.GeneraReporteIngresos(listadoreporteingresos.LastOrDefault().TrackingId, listadoreporteingresos, parametros);
                    return new { exitoso = true, mensaje = "", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static object GetAlertaWeb()
        {
            try
            {
                var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
                var denoma = "Alerta web";
                var alerta = "alerta web";
                if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
                {
                    denoma = datosAlertaWeb.Denominacion;
                    alerta = denoma;
                }
                object data = new
                {

                    Denominacion = denoma,
                    AlertaWerb = alerta
                };

                return data;
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }
    }
}