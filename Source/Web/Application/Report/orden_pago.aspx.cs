﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.Services;

namespace Web.Application.Report
{
    public partial class orden_pago : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            verifyPermission(11);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });

            if (permiso == null)
            {
                Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static string imprimirRecibo(string tracking)
        {
            try
            {
                //Consultar usuario actual
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 11 });

                if (permiso != null)
                {                
                    //Consultar la calificacion_ingreso
                    Entity.CalificacionIngreso calificacionIngreso = ControlCalificacionIngreso.ObtenerPorTrackingId(new Guid(tracking));
                    //Consultar la calificacion
                    object[] datosBusqueda = new object[]
                    {
                    calificacionIngreso.CalificacionId,
                    false
                    };
                    Entity.Calificacion calificacion = calificacionIngreso != null ? ControlCalificacion.ObtenerPorId(datosBusqueda) : null;
                    //Consultar el ingreso
                    Entity.EgresoIngreso ingreso = calificacionIngreso != null ? ControlEgresoIngreso.ObtenerPorId(calificacionIngreso.IngresoId) : null;
                    //Generar el recibo de la caja                
                    Entity.DetalleDetencion detalleDetencion = calificacion != null ? ControlDetalleDetencion.ObtenerPorId(calificacion.InternoId) : null;
                    List<Entity.MotivoDetencion> motivos = ControlMotivoDetencion.ObtenerTodoPorInternoId(detalleDetencion.Id);
                    //Consulta de todos los objetos necesarios
                    var institucion = detalleDetencion != null ? ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId) : null;
                    var interno = (detalleDetencion != null) ? ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId) : null;
                    var domicilio = (interno != null) ? ControlDomicilio.ObtenerPorId(interno.DomicilioId) : null;
                    var colonia = (domicilio != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;
                    string calle = (domicilio != null) ? domicilio.Calle : "";
                    string numero = (domicilio != null) ? domicilio.Numero : "";
                    string coloniaAux = (colonia != null) ? colonia.Asentamiento : "";
                    string cp = (colonia != null) ? colonia.CodigoPostal : "";
                    string domicilioCompleto = "";
                    if (domicilio != null)
                    {
                        domicilioCompleto = calle + " " + numero + " " + coloniaAux + " " + cp;
                    }
                    else
                    {
                        domicilioCompleto = "Domicilio no registrado";
                    }

                    var situacion = (calificacion != null) ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;
                    //Construccion del objeto con los datos del PDF
                    object[] data = new object[]
                    {
                    (institucion != null) ? institucion.Nombre : "",
                    calificacionIngreso.Id,
                    (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                    (detalleDetencion != null) ? detalleDetencion.Fecha.Value.ToString("dd/MM/yyyy HH:MM:ss") : "",
                    (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                    domicilioCompleto,
                    (situacion != null) ? situacion.Nombre : "",
                    (calificacion != null) ? calificacion.TotalDeMultas.ToString() : "",
                    (calificacion != null) ? calificacion.Agravante.ToString() : "",
                    (calificacion != null) ? calificacion.Ajuste.ToString() : "",
                    (calificacion != null) ? calificacion.TotalAPagar.ToString() : "",
                    (calificacion != null) ? calificacion.TotalHoras.ToString() : "",
                    (calificacion != null) ? calificacion.Fundamento.ToString() : "",
                    (interno != null) ? interno.Id : 0,
                    };

                    List<Entity.PagoMulta> multas = ControlPagoMulta.ObtenerByInternoId(detalleDetencion.Id);

                    object[] datosArchivo = ControlPDFJuez.generarReciboOrdenDePago(data, multas);

                    return JsonConvert.SerializeObject(new { success = true, message = "generó", ubicacionArchivo = (datosArchivo != null) ? datosArchivo[1].ToString() : "" });
                }
                else return JsonConvert.SerializeObject(new { success = false, message = "No cuenta con privilegios para realizar la acción." });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }

        [WebMethod]
        public static DataTable getRecibos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 11 });

                if (permiso != null)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        where.Add(new Where("CI.Activo", "1"));
                        //where.Add(new Where("EI.ContratoId", HttpContext.Current.Session["numeroContrato"].ToString()));
                        //Tengo el id del contratousuario, su contratoid puede ser un contrato o un subcontrato
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        where.Add(new Where("EI.ContratoId", contratoUsuario.IdContrato.ToString()));
                        where.Add(new Where("EI.Tipo", contratoUsuario.Tipo));

                        Query query = new Query
                        {
                            select = new List<string>{
                                "C.Id",
                                "CI.Id ReciboId",
                                "CI.TrackingId ReciboTracking",
                                "C.TrackingId CalificacionTracking",
                                "EI.TrackingId EstatusTracking",
                                "I.RutaImagen",
                                "I.Nombre",
                                "I.Paterno",
                                "I.Materno",
                                "CAST( EI.Expediente AS unsigned) Expediente",
                                "C.TotalAPagar",
                                "date_format(CI.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "concat(EI.NombreDetenido,' ',trim(APaternoDetenido),' ',trim(AMaternoDetenido)) NombreCompleto"
                            },
                            from = new Table("Calificacion_Ingreso", "CI"),
                            joins = new List<Join> {
                                new Join(new DT.Table("Calificacion", "C"), "C.Id = CI.CalificacionId", "INNER"),
                                new Join(new DT.Table("detalle_detencion", "EI"), "EI.Id = C.InternoId", "INNER"),
                                new Join(new DT.Table("Detenido", "I"), "I.Id = EI.DetenidoId", "INNER"),

                            },
                            wheres = where
                        };

                        //if (contratoUsuario != null)
                        //{
                        //    if (contratoUsuario.Tipo == "contrato")
                        //    {
                        //        query.joins.Add(new Join(new DT.Table("Contrato", "CO"), "CO.Id = EI.ContratoId", "INNER"));
                        //        where.Add(new Where("CO.Id", contratoUsuario.IdContrato.ToString()));
                        //    }
                        //    else if (contratoUsuario.Tipo == "subcontrato")
                        //    {
                        //        query.joins.Add(new Join(new DT.Table("SubContrato", "SU"), "SU.Id = EI.ContratoId", "INNER"));
                        //        where.Add(new Where("SU.Id", contratoUsuario.IdContrato.ToString()));
                        //    }
                        //}

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
    }
}