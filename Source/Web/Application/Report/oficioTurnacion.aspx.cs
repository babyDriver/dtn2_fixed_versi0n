﻿using Business;
using DT;
using Entity;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;

namespace Web.Application.Report
{
    public partial class oficioTurnacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //validatelogin();
            verifyPermission(4);
        }

        public void verifyPermission(int reporteId)
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, reporteId });
            string[] parametros = { usId.ToString(), "Registro en barandilla" };
            var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permisos != null || permiso != null)
            {
                permitido = true;
            }
            if (!permitido)
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }
        }

        [WebMethod]
        public static string pdf(string tracking, string Autoridad)
        {
            var detenido = ControlDetenido.ObtenerPorTrackingId(new Guid(tracking));
            var detencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(new string[] { detenido.Id.ToString(), "true" });

            var serializedObject = string.Empty;
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var contrato = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato);
            var calificacion = ControlCalificacion.ObtenerPorInternoId(detencion.Id);
            var institucion = ControlInstitucion.ObtenerPorId(calificacion.InstitucionId);

            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 4 });
                string[] parametros = { usId.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    var obj = ControlPDFBarandilla.oficioTurnacion(institucion.Id, contratoUsuario.IdContrato, contratoUsuario.Tipo, detenido, Autoridad, calificacion.CreadoPor);
                    serializedObject = JsonConvert.SerializeObject(obj);
                    return serializedObject;
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                return serializedObject = JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getDetenidos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string pages)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 4 });
                string[] parametros = { usId.ToString(), "Registro en barandilla" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permisos != null || permiso != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!emptytable)
                    {
                        List<Entity.ListadoReporteOficio> data = new List<Entity.ListadoReporteOficio>();
                        List<Entity.ListadoReporteOficio> listTotals = new List<Entity.ListadoReporteOficio>();

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        object[] dataParams = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0
                        };

                        listTotals = ControlListadoReporteOficio.ObtenerTodos(dataParams);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value.ToLower());
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno).ToList();
                                    break;                                
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToDateTime(x.Fecha)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToDateTime(x.Fecha)).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.LugarDetencion).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.LugarDetencion).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Unidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Unidad).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Responsable).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Responsable).ToList();
                                    break;
                                case 10:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Motivo).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Motivo).ToList();
                                    break;
                            }
                        }

                        List<Entity.ListadoReporteOficio> listFinal = new List<Entity.ListadoReporteOficio>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;
                    }
                    else
                    {
                        object json2 = new { data = new List<ListadoReporteOficio>(), recordsTotal = new List<ListadoReporteOficio>().Count, recordsFiltered = new List<ListadoReporteOficio>().Count, };
                        return json2;
                    }
                }
                else
                {
                    object json2 = new { data = new List<ListadoReporteOficio>(), recordsTotal = new List<ListadoReporteOficio>().Count, recordsFiltered = new List<ListadoReporteOficio>().Count, };
                    return json2;
                }
            }
            catch (Exception ex)
            {
                object json2 = new { data = new List<ListadoReporteOficio>(), recordsTotal = new List<ListadoReporteOficio>().Count, recordsFiltered = new List<ListadoReporteOficio>().Count, };
                return json2;
            }
        }
    }
}