﻿using Business;
using DT;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Entity;

namespace Web.Application.Caja
{
    public partial class caja : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getPermisos();
            // validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Caja" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();

                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static DataTable GetEgresolog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        else

                            
                        where.Add(new Where("C.Egreso", "1"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Folio",
                                "A.Concepto",
                                "A.Total",
                                "A.PersonaQuePaga",
                                "A.Observacion",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("egresoingresolog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("vusuarios","V"),"A.Creadopor=V.Id ","LEFT OUTER"),
                                new Join(new Table("usuario","B"),"V.UsuarioId=B.Id ","LEFT OUTER"),
                                new Join(new Table("egresoingreso","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }

                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
        [WebMethod]
        public static DataTable getEgresos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);                        

                        List<Where> where = new List<Where>();

                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Egreso", "1"));

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if (contratoUsuario != null)
                        {
                            where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("E.Tipo", contratoUsuario.Tipo));
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                                "E.Id",
                                "E.TrackingId",
                                "date_format(E.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "E.Folio",
                                "E.Concepto",
                                "E.Total",
                                "REPLACE(E.PersonaQuePaga,'\"','') PersonaQuePaga",
                                "REPLACE(E.Observacion,'\"','') Observacion",                                
                                "E.Activo",
                                "E.Habilitado",
                                "E.CreadoPor",
                                "E.Egreso"
                            },
                            from = new Table("EgresoIngreso", "E"),                            
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
        [WebMethod]
        public static DataTable GetIngresoslog(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string centroid, string todoscancelados, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlconection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
                        List<Where> where = new List<Where>();
                        List<Order> orderby = new List<Order>();
                        if (Convert.ToInt32(centroid) != 0)
                        {
                            where.Add(new Where("A.Id", centroid.ToString()));
                        }
                        if (Convert.ToBoolean(todoscancelados))
                        {
                            where.Add(new Where("C.Activo", "0"));
                        }
                        else
                        
                        
                        where.Add(new Where("C.Egreso", "0"));
                        Query query = new Query
                        {
                            select = new List<string>
                            {
                                "A.id",
                                "A.TrackingId",
                                "A.Folio",
                                "A.Concepto",
                                "A.Total",
                                "A.PersonaQuePaga",
                                "A.Observacion",
                                "A.habilitado",
                                "B.Usuario Creadopor",
                                "A.Accion",
                                "A.AccionId",
                                "date_format(A.Fec_Movto, '%Y-%m-%d %H:%i:%S') Fec_Movto"

                            },
                            from = new Table("egresoingresolog", "A"),
                            joins = new List<Join>
                            {
                                new Join(new Table("vusuarios","V"),"A.Creadopor=V.Id ","LEFT OUTER"),
                                new Join(new Table("usuario","B"),"V.UsuarioId=B.Id ","LEFT OUTER"),
                                new Join(new Table("egresoingreso","C"),"C.Id=A.Id"),
                            },
                            wheres = where

                        };
                        DataTables dt = new DataTables(mysqlconection);
                        DataTable _dt = dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }

                }
                else
                {
                    return DataTables.ObtenerDataTableVacia("No cuentas con los privilegios para listar la información.", draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }
        [WebMethod]
        public static DataTable getIngresos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        where.Add(new Where("E.Activo", "1"));
                        where.Add(new Where("E.Egreso", "0"));

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if (contratoUsuario != null)
                        {
                            where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("E.Tipo", contratoUsuario.Tipo));
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                                "E.Id",
                                "E.TrackingId",
                                "date_format(E.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "E.Folio",
                                "E.Concepto",
                                "E.Total",
                                "REPLACE(E.PersonaQuePaga,'\"','') PersonaQuePaga",
                                "replace(E.Observacion,'\"','') Observacion",
                                "E.Activo",
                                "E.Habilitado",
                                "E.CreadoPor",
                                "E.Egreso"
                            },
                            from = new Table("EgresoIngreso", "E"),
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt= dt.Generar(query, draw, start, length, search, order, columns);
                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static DataTable getCancelaciones(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        where.Add(new Where("E.Activo", "1"));

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if (contratoUsuario != null)
                        {
                            where.Add(new Where("E.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("E.TipoContrato", contratoUsuario.Tipo));
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                                "E.Id",
                                "E.TrackingId",
                                "date_format(E.Fecha, '%Y-%m-%d %H:%i:%S') Fecha",
                                "E.Folio",
                                "E.Tipo",
                                "E.Total",
                                "E.PersonaQuePaga",
                                "E.Observacion",
                                "E.Activo",
                                "E.Habilitado",
                                "E.CreadoPor"                                
                            },
                            from = new Table("Cancelacion", "E"),
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getMultas(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string pages)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
                {
                    if (!emptytable)
                    {
                        List<Entity.ListadoMultas> data = new List<Entity.ListadoMultas>();
                        List<Entity.ListadoMultas> listTotals = new List<Entity.ListadoMultas>();

                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                        object[] dataParams = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0
                        };

                        listTotals = ControlListadoMultas.ObtenerTodos(dataParams);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value.ToLower());
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.NombreDetenido).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.NombreDetenido).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.APaternoDetenido).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.APaternoDetenido).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.AMaternoDetenido).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.AMaternoDetenido).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.TotalAPagar).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.TotalAPagar).ToList();
                                    break;
                            }
                        }

                        List<Entity.ListadoMultas> listFinal = new List<Entity.ListadoMultas>();
                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json;
                    }
                    else
                    {
                        object json2 = new { data = new List<ListadoMultas>(), recordsTotal = new List<ListadoMultas>().Count, recordsFiltered = new List<ListadoMultas>().Count, };
                        return json2;
                    }
                }
                else
                {
                    object json2 = new { data = new List<ListadoMultas>(), recordsTotal = new List<ListadoMultas>().Count, recordsFiltered = new List<ListadoMultas>().Count, };
                    return json2;
                }
            }
            catch (Exception ex)
            {
                object json2 = new { data = new List<ListadoMultas>(), recordsTotal = new List<ListadoMultas>().Count, recordsFiltered = new List<ListadoMultas>().Count, };
                return json2;
            }
        }

        //[WebMethod]
        //public static DataTable getMultas(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        //{
        //    try
        //    {
        //        if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
        //        {
        //            if (!emptytable)
        //            {
        //                MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

        //                List<Where> where = new List<Where>();

        //                where.Add(new Where("C.Activo", "1"));
        //                where.Add(new Where("C.SoloArresto", "0"));
        //                where.Add(new Where("C.TotalAPagar",">","0"));
        //                //where.Add(new Where("EI.ContratoId", HttpContext.Current.Session["numeroContrato"].ToString()));
        //                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

        //                if (contratoUsuario != null)
        //                {
        //                    where.Add(new Where("EI.ContratoId", contratoUsuario.IdContrato.ToString()));
        //                    where.Add(new Where("EI.Tipo", contratoUsuario.Tipo));
        //                }

        //                Query query = new Query
        //                {
        //                    select = new List<string>{
        //                        "C.Id",
        //                        "C.TrackingId CalificacionTracking",
        //                        "EI.TrackingId EstatusTracking",
        //                        "I.RutaImagen",
        //                        "I.Nombre",
        //                        "I.Paterno",
        //                        "I.Materno",
        //                        "EI.Expediente",
        //                        "C.TotalAPagar",
        //                        "concat(EI.NombreDetenido,' ',trim(APaternoDetenido),' ',trim(AMaternoDetenido)) NombreCompleto"
        //                    },
        //                    from = new Table("Calificacion", "C"),
        //                    joins = new List<Join> {
        //                        new Join(new DT.Table("detalle_detencion", "EI"), "EI.Id = C.InternoId", "INNER"),
        //                        new Join(new DT.Table("Detenido", "I"), "I.Id = EI.DetenidoId", "INNER")
        //                    },
        //                    wheres = where
        //                };
        //                DataTables dt = new DataTables(mysqlConnection);
        //                var table = dt.Generar(query, draw, start, length, search, order, columns);
        //                return table;
        //            }
        //            else
        //            {
        //                return DataTables.ObtenerDataTableVacia(null, draw);
        //            }
        //        }
        //        else
        //        {
        //            return DataTables.ObtenerDataTableVacia(null, draw);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return DataTables.ObtenerDataTableVacia(null, draw);
        //    }
        //}

        [WebMethod]
        public static DataTable getCortes(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        where.Add(new Where("C.Activo", "1"));

                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if (contratoUsuario != null)
                        {
                            where.Add(new Where("C.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("C.Tipo", contratoUsuario.Tipo));
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                                "C.Id",
                                "C.TrackingId",
                                "C.SaldoInicial",
                                "date_format(C.FechaInicio, '%Y-%m-%d %H:%i:%S') FechaInicio",
                                "date_format(C.FechaFin, '%Y-%m-%d %H:%i:%S') FechaFin",
                                "C.Activo",
                                "C.Habilitado",
                                "C.CreadoPor",
                                "Concat(Nombre, ' ' , ApellidoPaterno, ' ' ,ApellidoMaterno) NombreCompleto"
                            },
                            from = new Table("CorteDeCaja", "C"),
                            joins = new List<Join> {
                                new Join(new Table("vusuarios","V"),"C.Creadopor=V.Id ","LEFT OUTER"),
                                new Join(new Table("usuario","U"),"V.UsuarioId=U.Id ","LEFT OUTER"),
                                
                            },
                            wheres = where
                        };

                        DataTables dt = new DataTables(mysqlConnection);
                        DataTable _dt = new DataTable();
                      _dt= dt.Generar(query, draw, start, length, search, order, columns);
                        return _dt;
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static DataTable getRecibos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Caja" }).Consultar)
                {
                    if (!emptytable)
                    {
                        MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                        List<Where> where = new List<Where>();

                        where.Add(new Where("CI.Activo", "1"));
                        //where.Add(new Where("EI.ContratoId", HttpContext.Current.Session["numeroContrato"].ToString()));
                        //Tengo el id del contratousuario, su contratoid puede ser un contrato o un subcontrato
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                        if (contratoUsuario != null)
                        {
                            where.Add(new Where("EI.ContratoId", contratoUsuario.IdContrato.ToString()));
                            where.Add(new Where("EI.Tipo", contratoUsuario.Tipo));
                        }

                        Query query = new Query
                        {
                            select = new List<string>{
                                "C.Id",
                                "ifnull(CI.Folio,0) ReciboId",
                                "CI.Id IdRecibo",
                                "CI.TrackingId ReciboTracking",
                                "C.TrackingId CalificacionTracking",
                                "EI.TrackingId EstatusTracking",
                                "I.RutaImagen",
                                "I.Nombre",
                                "I.Paterno",
                                "I.Materno",
                                "EI.Expediente",
                                "C.TotalAPagar",
                                "date_format(CI.Fecha, '%Y-%m-%d %H:%i:%S') Fecha"
                            },
                            from = new Table("Calificacion_Ingreso", "CI"),
                            joins = new List<Join> {
                                new Join(new DT.Table("Calificacion", "C"), "C.Id = CI.CalificacionId", "INNER"),
                                new Join(new DT.Table("detalle_detencion", "EI"), "EI.Id = C.InternoId", "INNER"),
                                new Join(new DT.Table("Detenido", "I"), "I.Id = EI.DetenidoId", "INNER"),
                                
                            },
                            wheres = where
                        };                       

                        DataTables dt = new DataTables(mysqlConnection);
                        var _dt=dt.Generar(query, draw, start, length, search, order, columns);
                        return dt.Generar(query, draw, start, length, search, order, columns);
                    }
                    else
                    {
                        return DataTables.ObtenerDataTableVacia(null, draw);
                    }
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(null, draw);
            }
        }

        [WebMethod]
        public static string existeUnCorteActivo()
        {
            try
            {
                bool isActive = false;
                int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
                //Consultar ultimo corte
                object[] parametros = new object[]
                {
                    contratoUsuario.IdContrato,
                    contratoUsuario.Tipo
                };
                Entity.CorteDeCaja ultimoCorte = ControlCorteDeCaja.ObtenerUltimoCorte(parametros);
                if (ultimoCorte != null)
                {                    
                    if (ultimoCorte.FechaFin != null)
                    {
                        //Tiene fecha fin y debe de crearse un nuevo corte
                        isActive = false;
                    }
                    else
                    {
                        isActive = true;
                        //No tiene fecha de fin, entonces se puede reutilizar 
                    }                                                            
                }
                else
                {
                    isActive = false;
                    //No tiene datos, crear un nuevo corte
                }

                return JsonConvert.SerializeObject(new { success = true, message = "", isActive });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }

        [WebMethod]
        public static string saveEgreso(string[] datos)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());                                

                Entity.EgresoIngreso egreso = new Entity.EgresoIngreso();
                egreso.Activo = true;
                egreso.Concepto = datos[2].ToString();
                egreso.CreadoPor = usId;                       
                egreso.Habilitado = true;
                egreso.Observacion = datos[5].ToString();
                egreso.PersonaQuePaga = datos[4].ToString();
                egreso.Total = Convert.ToDecimal(datos[3].ToString());
                egreso.Egreso = true;

                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                if (contratoUsuario != null)
                {
                    egreso.ContratoId = contratoUsuario.IdContrato;
                    egreso.Tipo = contratoUsuario.Tipo;
                }

                string id = "";
                id = datos[0].ToString();
                string mode = "";

                if (string.IsNullOrEmpty(id))
                {
                    mode = "registró";

                    object[] parametros = new object[]
                    {
                        true,
                        contratoUsuario.IdContrato,
                        contratoUsuario.Tipo
                    };

                    var egresoAux = ControlEgresoIngreso.ObtenerFolio(parametros);                    
                    int folio = (egresoAux != null) ? egresoAux.Folio + 1 : 1;
                    egreso.Folio = folio;
                    egreso.Fecha = DateTime.Now;
                    egreso.TrackingId = Guid.NewGuid();
                    ControlParametroContrato.InsertaParametroContratiSiNoExsite(5, contratoUsuario.IdContrato, contratoUsuario.IdUsuario, egresoAux!=null? egresoAux.Folio:0);
                    egreso.Id = ControlEgresoIngreso.Guardar(egreso);

                    if (!(egreso.Id > 0))
                    {
                        throw new Exception("No se pudo registrar el egreso");
                    }
                }
                else
                {
                    mode = "actualizó";
                    egreso.Id = Convert.ToInt32(datos[0].ToString());
                    egreso.TrackingId = new Guid(datos[1].ToString());
                    var egresoUpd = ControlEgresoIngreso.ObtenerPorId(egreso.Id);
                    egreso.Folio = egresoUpd.Folio;
                    egreso.Fecha = egresoUpd.Fecha;
                    ControlEgresoIngreso.Actualizar(egreso);
                }

                return JsonConvert.SerializeObject(new { success = true, message = mode });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }

        [WebMethod]
        public static string saveIngreso(string[] datos)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                Entity.EgresoIngreso egreso = new Entity.EgresoIngreso();
                egreso.Activo = true;
                egreso.Concepto = datos[2].ToString();
                egreso.CreadoPor = usId;                
                egreso.Habilitado = true;
                egreso.Observacion = datos[5].ToString();
                egreso.PersonaQuePaga = datos[4].ToString();
                egreso.Total = Convert.ToDecimal(datos[3].ToString());
                egreso.Egreso = false;

                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                if (contratoUsuario != null)
                {
                    egreso.ContratoId = contratoUsuario.IdContrato;
                    egreso.Tipo = contratoUsuario.Tipo;
                }

                string id = "";
                id = datos[0].ToString();
                string mode = "";

                if (string.IsNullOrEmpty(id))
                {
                    mode = "registró";
                    object[] parametros = new object[]
                   {
                        false,
                        contratoUsuario.IdContrato,
                        contratoUsuario.Tipo
                   };
                    var egresoAux = ControlEgresoIngreso.ObtenerFolio(parametros);
                    int folio = (egresoAux != null) ? egresoAux.Folio + 1 : 1;
                    ControlParametroContrato.InsertaParametroContratiSiNoExsite(4, contratoUsuario.IdContrato,contratoUsuario.IdUsuario , egresoAux!=null? egresoAux.Folio:0);
                    egreso.Folio = folio;
                    egreso.Fecha = DateTime.Now;
                    egreso.TrackingId = Guid.NewGuid();
                    egreso.Id = ControlEgresoIngreso.Guardar(egreso);

                    if (!(egreso.Id > 0))
                    {
                        throw new Exception("No se pudo registrar el ingreso");
                    }
                }
                else
                {
                    mode = "actualizó";
                    egreso.Id = Convert.ToInt32(datos[0].ToString());
                    egreso.TrackingId = new Guid(datos[1].ToString());
                    var egresoUpd = ControlEgresoIngreso.ObtenerPorId(egreso.Id);
                    egreso.Folio = egresoUpd.Folio;
                    egreso.Fecha = egresoUpd.Fecha;
                    ControlEgresoIngreso.Actualizar(egreso);
                }

                return JsonConvert.SerializeObject(new { success = true, message = mode });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }

        [WebMethod]
        public static string eliminarEgreso(string tracking)
        {
            try
            {
                string mensaje = ControlEgresoIngreso.Desactivar(new Guid(tracking)); 
                return JsonConvert.SerializeObject(new { success = true, message = mensaje });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }

        [WebMethod]
        public static Object obtenerDatos(string[] datos)
        {
            try
            {
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                Regex noWords = new Regex("^[0-9]*$");
                if (!noWords.IsMatch(datos[0])) throw new Exception("El folio solo puede contener numeros");

                var folio = 0;
                if (!int.TryParse(datos[0], out folio)) throw new Exception("El folio es demasiado grande");

                object[] parametros = new object[]
                {
                    folio,
                    Convert.ToBoolean(datos[1].ToString()),
                    contratoUsuario.IdContrato,
                    contratoUsuario.Tipo
                };

                var data = ControlEgresoIngreso.ObtenerByFolioAndTipo(parametros);

                object obj = new
                {
                    Id = data != null ? data.Id : 0,
                    TrackingId = data != null ? data.TrackingId : new Guid(),
                    Activo = data != null ? data.Activo : false,
                    Concepto = data != null ? data.Concepto : "",
                    CreadoPor = data != null ? data.CreadoPor : 0,
                    Egreso = data != null ? data.Egreso : false,
                    Fecha = data != null ? data.Fecha.ToString("DD/MM/YYYY HH:MM:SS") : "",
                    Folio = data != null ? data.Folio.ToString() : "",
                    Habilitado = data != null ? data.Habilitado : false,
                    Observacion = data != null ? data.Observacion : "",
                    PersonaQuePaga = data != null ? data.PersonaQuePaga : "",
                    Total = data != null ? data.Total : 0
                };

                return JsonConvert.SerializeObject(new { success = true, message = "", obj = obj });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }

        [WebMethod]
        public static string saveCancelacion(string[] datos)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                if (string.IsNullOrEmpty(datos[2])) throw new Exception("Debe buscar un folio para realizar una cancelación");
                if (datos[2] == "00000000-0000-0000-0000-000000000000") throw new Exception("No se encontro el folio en el sistema");
                var item = ControlEgresoIngreso.ObtenerPorTrackingId(new Guid(datos[2].ToString()));                

                Entity.Cancelacion cancelacion = new Entity.Cancelacion();
                cancelacion.Activo = true;
                cancelacion.CreadoPor = usId;
                cancelacion.Habilitado = true;
                cancelacion.Observacion = item != null ? item.Observacion : "";
                cancelacion.PersonaQuePaga = item != null ? item.PersonaQuePaga : "";
                cancelacion.Tipo = item != null ? item.Egreso ? "Egreso" : "Ingreso" : "";
                cancelacion.Total = item != null ? item.Total : 0;
                cancelacion.FolioMovimiento = item.Folio;

                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                if (contratoUsuario != null)
                {
                    cancelacion.ContratoId = contratoUsuario.IdContrato;
                    cancelacion.TipoContrato = contratoUsuario.Tipo;
                }

                string id = "";
                id = datos[0].ToString();
                string mode = "";

                var repetido = new Entity.Cancelacion();
                object[] dataRepetido = new object[]
                {
                    cancelacion.Tipo,
                    item.Folio,
                    contratoUsuario.IdContrato,
                    contratoUsuario.Tipo
                };
                repetido = ControlCancelacion.ObtenerByTipoAndFolio(dataRepetido);

                if(repetido != null)
                {
                    throw new Exception(string.Concat("El concepto que desea cancelar ya tiene una cancelación asignada, no puede volver a cancelar el concepto"));
                }

                if (string.IsNullOrEmpty(id))
                {
                    mode = "registró";
                    object[] parametros = new object[]
                    {
                        contratoUsuario.IdContrato,
                        contratoUsuario.Tipo
                    };

                    var cancelacionAux = ControlCancelacion.ObtenerFolio(parametros);
                    int folio = (cancelacionAux != null) ? cancelacionAux.Folio + 1 : 1;
                    cancelacion.Folio = folio;
                    cancelacion.Fecha = DateTime.Now;
                    cancelacion.TrackingId = Guid.NewGuid();
                    ControlParametroContrato.InsertaParametroContratiSiNoExsite(7, contratoUsuario.IdContrato, contratoUsuario.IdUsuario, cancelacionAux!=null? cancelacionAux.Folio:0);
                    cancelacion.Id = ControlCancelacion.Guardar(cancelacion);

                    item.Activo = false;
                    ControlEgresoIngreso.Actualizar(item);

                    if (!(cancelacion.Id > 0))
                    {
                        throw new Exception("No se pudo registrar la cancelación");
                    }
                }

                return JsonConvert.SerializeObject(new { success = true, message = mode });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }

        [WebMethod]
        public static string eliminarCancelacion(string tracking)
        {
            try
            {
                string mensaje = ControlCancelacion.Desactivar(new Guid(tracking));
                return JsonConvert.SerializeObject(new { success = true, message = mensaje });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }
        [WebMethod]
        public static string getRutaServer()
        {
            string rutaDefault = "";

            try
            {
                rutaDefault = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", rutaDefault });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message, rutaDefault });
            }
        }
        [WebMethod]
        public static string pagarMulta(string[] datos)
        {
            try
            {
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var contratoUsuario2 = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                //Consultar usuario actual
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                //Consultar la calificacion
                object[] dataCal = new object[]
                {
                    datos[0].ToString(),
                    true
                };
                Entity.Calificacion calificacion = ControlCalificacion.ObtenerPorTrackingId(dataCal);
                //Consultar ultimo ingreso
                object[] parametros = new object[]
                {
                    false,
                    contratoUsuario.IdContrato,
                    contratoUsuario.Tipo
                };
                Entity.EgresoIngreso ingresoAux = ControlEgresoIngreso.ObtenerFolio(parametros);
                //Guardar el ingreso
                Entity.EgresoIngreso ingreso = new Entity.EgresoIngreso();
                ingreso.Activo = true;
                ingreso.Concepto = "Pago de multa";
                ingreso.CreadoPor = usId;
                ingreso.Egreso = false;
                ingreso.Fecha = DateTime.Now;
                ingreso.Folio = ingresoAux != null ? ingresoAux.Folio + 1 : 1;
                ingreso.Habilitado = true;
                ingreso.Observacion = datos[3].ToString();
                ingreso.PersonaQuePaga = datos[2].ToString();
                ingreso.Total = calificacion.TotalAPagar;
                ingreso.TrackingId = Guid.NewGuid();
                ingreso.ContratoId = contratoUsuario.IdContrato;
                ingreso.Tipo = contratoUsuario.Tipo;
                ControlParametroContrato.InsertaParametroContratiSiNoExsite(4, contratoUsuario.IdContrato, contratoUsuario.IdUsuario, ingresoAux!=null? ingresoAux.Folio:0 );
                ingreso.Id = ControlEgresoIngreso.Guardar(ingreso);

                if (!(ingreso.Id > 0))
                {
                    throw new Exception("No se pudo registrar el ingreso");
                }

                //Obtener el ultimo folio de la calificacioningreso
                object[] parametrosFolio = new object[]
                {
                    contratoUsuario.IdContrato,
                    contratoUsuario.Tipo
                };
                Entity.CalificacionIngreso calificacionAux = ControlCalificacionIngreso.ObtenerFolio(parametrosFolio);                
                //Guardar en la tabla calificacioningreso
                Entity.CalificacionIngreso calificacionIngreso = new Entity.CalificacionIngreso();
                calificacionIngreso.Activo = true;
                calificacionIngreso.CalificacionId = calificacion.Id;
                calificacionIngreso.CreadoPor = usId;
                calificacionIngreso.Fecha = DateTime.Now;
                calificacionIngreso.Habilitado = true;
                calificacionIngreso.IngresoId = ingreso.Id;                
                calificacionIngreso.TrackingId = Guid.NewGuid();
                calificacionIngreso.ContratoId = contratoUsuario.IdContrato;
                calificacionIngreso.Tipo = contratoUsuario.Tipo;

                if (calificacionAux != null)
                {
                    calificacionIngreso.Folio = calificacionAux.Folio + 1;
                }
                else
                {
                    calificacionIngreso.Folio = 1;
                }
                ControlParametroContrato.InsertaParametroContratiSiNoExsite(6, contratoUsuario.IdContrato, contratoUsuario.IdUsuario, calificacionAux!=null?calificacionAux.Folio:0 );
                calificacionIngreso.Id = ControlCalificacionIngreso.Guardar(calificacionIngreso);

                if (!(calificacionIngreso.Id > 0))
                {
                    throw new Exception("No se pudo registrar el pago de la multa");
                }

                //Cambiar el estatus de la calificacion a Activo = false
                calificacion.Activo = false;
                ControlCalificacion.Actualizar(calificacion);
                //Generar el recibo de la caja                
                Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(datos[1].ToString()));
                List<Entity.MotivoDetencion> motivos = ControlMotivoDetencion.ObtenerTodoPorInternoId(detalleDetencion.Id);                
                //Consulta de todos los objetos necesarios
                var institucion = detalleDetencion != null ? ControlInstitucion.ObtenerPorId(detalleDetencion.CentroId) : null;
                var interno = (detalleDetencion != null) ? ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId) : null;
                var domicilio = (interno != null) ? ControlDomicilio.ObtenerPorId(interno.DomicilioId) : null;
                var colonia = (domicilio != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;
                string calle = (domicilio != null) ? domicilio.Calle : "";
                string numero = (domicilio != null) ? domicilio.Numero : "";
                string coloniaAux = (colonia != null) ? colonia.Asentamiento : "";
                string cp = (colonia != null) ? colonia.CodigoPostal : "";
                string domicilioCompleto = calle + " " + numero + " " + coloniaAux + " " + cp;
                var situacion = (calificacion != null) ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;
                //Construccion del objeto con los datos del PDF                
                object[] data = new object[]
                {
                    (institucion != null) ? institucion.Nombre : "",
                    calificacionIngreso.Folio,
                    (detalleDetencion != null) ? detalleDetencion.Expediente : "",
                    (detalleDetencion != null) ? detalleDetencion.Fecha.Value.ToString("dd/MM/yyyy HH:MM:ss") : "",
                    (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                    domicilioCompleto,
                    (situacion != null) ? situacion.Nombre : "",
                    (calificacion != null) ? calificacion.TotalDeMultas.ToString() : "",
                    (calificacion != null) ? calificacion.Agravante.ToString() : "",
                    (calificacion != null) ? calificacion.Ajuste.ToString() : "",
                    (calificacion != null) ? calificacion.TotalAPagar.ToString() : "",
                    (calificacion != null) ? calificacion.TotalHoras.ToString() : "",
                    (calificacion != null) ? calificacion.Fundamento.ToString() : "",
                    (institucion != null) ? institucion.Id : 0
                };

                List<Entity.PagoMulta> multas =  ControlPagoMulta.ObtenerByInternoId(detalleDetencion.Id);
                
                object[] datosArchivo = ControlPDF.generarReciboPagoMulta(data, multas);

                Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                reportesLog.ProcesoId = interno.Id;
                reportesLog.ProcesoTrackingId = interno.TrackingId.ToString();
                reportesLog.Modulo = "Caja";
                reportesLog.Reporte = "Recibo de pago de multa";
                reportesLog.Fechayhora = DateTime.Now;
                reportesLog.EstatusId = 1;
                reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                ControlReportesLog.Guardar(reportesLog);

                var contratoUsuario1 = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario1.IdContrato);

                Entity.Historial historial = new Entity.Historial();
                historial.Activo = true;
                historial.CreadoPor = usId;
                historial.Fecha = DateTime.Now;
                historial.Habilitado = true;
                historial.InternoId = detalleDetencion.DetenidoId;
                historial.Movimiento = "Modificación de datos generales en pago de multa";
                historial.TrackingId = Guid.NewGuid();
                historial.ContratoId = subcontrato.Id;
                historial.Id = ControlHistorial.Guardar(historial);

                return JsonConvert.SerializeObject(new { success = true, message = "guardó", ubicacionArchivo = (datosArchivo != null) ? datosArchivo[1].ToString() : "" });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }

        [WebMethod]
        public static string imprimirRecibo(string tracking)
        {
            try
            {
                //Consultar usuario actual
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                //Consultar la calificacion_ingreso
                Entity.CalificacionIngreso calificacionIngreso = ControlCalificacionIngreso.ObtenerPorTrackingId(new Guid(tracking));
                //Consultar la calificacion
                object[] datosBusqueda = new object[]
                {
                    calificacionIngreso.CalificacionId,
                    false
                };
                Entity.Calificacion calificacion = calificacionIngreso != null ? ControlCalificacion.ObtenerPorId(datosBusqueda) : null;
                //Consultar el ingreso
                Entity.EgresoIngreso ingreso = calificacionIngreso != null ? ControlEgresoIngreso.ObtenerPorId(calificacionIngreso.IngresoId) : null;
                //Generar el recibo de la caja                
                Entity.DetalleDetencion estatusInterno = calificacion != null ? ControlDetalleDetencion.ObtenerPorId(calificacion.InternoId) : null;
                List<Entity.MotivoDetencion> motivos = ControlMotivoDetencion.ObtenerTodoPorInternoId(estatusInterno.Id);
                //Consulta de todos los objetos necesarios
                var institucion = estatusInterno != null ? ControlInstitucion.ObtenerPorId(estatusInterno.CentroId) : null;
                var interno = (estatusInterno != null) ? ControlDetenido.ObtenerPorId(estatusInterno.DetenidoId) : null;
                var domicilio = (interno != null) ? ControlDomicilio.ObtenerPorId(interno.DomicilioId) : null;
                var colonia = (domicilio != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)) : null;
                string calle = (domicilio != null) ? domicilio.Calle : "";
                string numero = (domicilio != null) ? domicilio.Numero : "";
                string coloniaAux = (colonia != null) ? colonia.Asentamiento : "";
                string cp = (colonia != null) ? colonia.CodigoPostal : "";
                string domicilioCompleto = calle + " " + numero + " " + coloniaAux + " " + cp;
                var situacion = (calificacion != null) ? ControlCatalogo.Obtener(calificacion.SituacionId, 89) : null;
                //Construccion del objeto con los datos del PDF
                object[] data = new object[]
                {
                    (institucion != null) ? institucion.Nombre : "",
                    calificacionIngreso.Folio,
                    (estatusInterno != null) ? estatusInterno.Expediente : "",
                    (estatusInterno != null) ? estatusInterno.Fecha.Value.ToString("dd/MM/yyyy HH:MM:ss") : "",
                    (interno != null) ? interno.Nombre + " " + interno.Paterno + " " + interno.Materno : "",
                    domicilioCompleto,
                    (situacion != null) ? situacion.Nombre : "",
                    (calificacion != null) ? calificacion.TotalDeMultas.ToString() : "",
                    (calificacion != null) ? calificacion.Agravante.ToString() : "",
                    (calificacion != null) ? calificacion.Ajuste.ToString() : "",
                    (calificacion != null) ? calificacion.TotalAPagar.ToString() : "",
                    (calificacion != null) ? calificacion.TotalHoras.ToString() : "",
                    (calificacion != null) ? calificacion.Fundamento.ToString() : "",
                    (institucion != null) ? institucion.Id : 0
                };

                List<Entity.PagoMulta> multas = ControlPagoMulta.ObtenerByInternoId(estatusInterno.Id);

                object[] datosArchivo = ControlPDF.generarReciboPagoMulta(data, multas);
                Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                reportesLog.ProcesoId = interno.Id;
                reportesLog.ProcesoTrackingId = interno.TrackingId.ToString();
                reportesLog.Modulo = "Caja";
                reportesLog.Reporte = "Recibos";
                reportesLog.Fechayhora = DateTime.Now;
                reportesLog.EstatusId = 1;
                reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                ControlReportesLog.Guardar(reportesLog);

                return JsonConvert.SerializeObject(new { success = true, message = "generó", ubicacionArchivo = (datosArchivo != null) ? datosArchivo[1].ToString() : "" });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }

        [WebMethod]
        public static string guardarCorte(string saldoInicial)
        {
            try
            {
                int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
                //Consultar usuario actual
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                //Consultar ultimo corte
                object[] parametros = new object[]
                {
                    contratoUsuario.IdContrato,
                    contratoUsuario.Tipo
                };
                Entity.CorteDeCaja ultimoCorte = ControlCorteDeCaja.ObtenerUltimoCorte(parametros);
                if(ultimoCorte != null)
                {                    
                    ultimoCorte.FechaFin = DateTime.Now;
                    ControlCorteDeCaja.Actualizar(ultimoCorte);                                        
                }
                //Guardar corte                
                Entity.CorteDeCaja corteDeCaja = new Entity.CorteDeCaja();
                corteDeCaja.Activo = true;
                corteDeCaja.CreadoPor = usId;
                corteDeCaja.Fecha = DateTime.Now;
                corteDeCaja.FechaFin = null;
                corteDeCaja.Habilitado = true;
                corteDeCaja.SaldoInicial = Convert.ToDecimal(saldoInicial);
                if (contratoUsuario != null)
                {
                    corteDeCaja.ContratoId = contratoUsuario.IdContrato;
                    corteDeCaja.Tipo = contratoUsuario.Tipo;
                }
                corteDeCaja.TrackignId = Guid.NewGuid();
                int idCorte = ControlCorteDeCaja.Guardar(corteDeCaja);

                if (!(idCorte > 0))
                {
                    throw new Exception("No se pudo registrar el corte de caja");
                }

                return JsonConvert.SerializeObject(new { success = true, message = "guardó" });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }

        [WebMethod]
        public static string cambiarTurno(string[] datos)
        {
            try
            {
                string usuario = datos[0].ToString();
                string password = datos[1].ToString();
                var usuarioAux = new Entity.Usuario();
                var estatusV = Membership.ValidateUser(usuario, password);

                if (estatusV)
                {
                    usuarioAux = ControlUsuario.ObtenerPorUsuario(usuario);
                    if(usuarioAux != null)
                    {
                        int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                        object[] parametros = new object[]
                        {
                            contratoUsuario.IdContrato,
                            contratoUsuario.Tipo
                        };
                        Entity.CorteDeCaja ultimoCorte = ControlCorteDeCaja.ObtenerUltimoCorte(parametros);
                        if (ultimoCorte != null)
                        {
                            ultimoCorte.FechaFin = DateTime.Now;
                            ControlCorteDeCaja.Actualizar(ultimoCorte);
                        }
                        //Crear el nuevo corte de caja
                        
                        Entity.CorteDeCaja corteDeCaja = new Entity.CorteDeCaja();
                        corteDeCaja.Activo = true;
                        corteDeCaja.CreadoPor = usuarioAux.Id;
                        corteDeCaja.Fecha = DateTime.Now;
                        corteDeCaja.FechaFin = null;
                        corteDeCaja.Habilitado = true;
                        if (contratoUsuario != null)
                        {
                            corteDeCaja.ContratoId = contratoUsuario.IdContrato;
                            corteDeCaja.Tipo = contratoUsuario.Tipo;
                        }
                        corteDeCaja.SaldoInicial = Convert.ToDecimal(datos[2]);
                        corteDeCaja.TrackignId = Guid.NewGuid();
                        int idCorte = ControlCorteDeCaja.Guardar(corteDeCaja);

                        if (!(idCorte > 0))
                        {
                            throw new Exception("No se pudo registrar el corte de caja");
                        }
                    }
                }
                else
                {
                    return JsonConvert.SerializeObject(new { success = false, message = "El usuario proporcionado no es valido" });
                }

                return JsonConvert.SerializeObject(new { success = true, message = "guardó" });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }

        [WebMethod]
        public static string generarReciboCorteDeCaja(string tracking)
        {
            try
            {                
                Entity.CorteDeCaja corteDeCaja = ControlCorteDeCaja.ObtenerByTrackingId(new Guid(tracking));
                var contratoUsuarioActual = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

                object[] dataEgreso = new object[]
                {
                    (corteDeCaja != null) ? corteDeCaja.Fecha.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    (corteDeCaja != null) ? corteDeCaja.FechaFin != null ? corteDeCaja.FechaFin.Value.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    true
                };
                List<Entity.EgresoIngreso> listEgresos = ControlEgresoIngreso.ObtenerTodoByFecha(dataEgreso);
                List<Entity.EgresoIngreso> listEgresosFiltro = new List<Entity.EgresoIngreso>();

                foreach(var item in listEgresos)
                {
                    if(item.ContratoId == contratoUsuarioActual.IdContrato && item.Tipo == contratoUsuarioActual.Tipo)
                    {
                        listEgresosFiltro.Add(item);
                    }
                }

                object[] dataIngreso = new object[]
                {
                    (corteDeCaja != null) ? corteDeCaja.Fecha.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    (corteDeCaja != null) ? corteDeCaja.FechaFin != null ? corteDeCaja.FechaFin.Value.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    false
                };
                List<Entity.EgresoIngreso> listIngresos = ControlEgresoIngreso.ObtenerTodoByFecha(dataIngreso);
                List<Entity.EgresoIngreso> listIngresosFiltro = new List<Entity.EgresoIngreso>();

                foreach (var item in listIngresos)
                {
                    if (item.ContratoId == contratoUsuarioActual.IdContrato && item.Tipo == contratoUsuarioActual.Tipo)
                    {
                        listIngresosFiltro.Add(item);
                    }
                }

                object[] dataCancelacion = new object[]
                {
                    (corteDeCaja != null) ? corteDeCaja.Fecha.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    (corteDeCaja != null) ? corteDeCaja.FechaFin != null ? corteDeCaja.FechaFin.Value.ToString("dd-MM-yyyy HH:mm:ss") : DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") : ""
                };
                List<Entity.Cancelacion> listCancelacion = ControlCancelacion.ObtenerTodoByFecha(dataCancelacion);
                List<Entity.Cancelacion> listCancelacionFiltro = new List<Entity.Cancelacion>();

                foreach (var item in listCancelacion)
                {
                    if (item.ContratoId == contratoUsuarioActual.IdContrato && item.TipoContrato == contratoUsuarioActual.Tipo)
                    {
                        listCancelacionFiltro.Add(item);
                    }
                }

                int idContratoUsuario = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContratoUsuario);
                int institucionId = 0;                

                if(contratoUsuario != null)
                {
                    if(contratoUsuario.Tipo == "contrato")
                    {
                        institucionId = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId;
                    }
                    else if(contratoUsuario.Tipo == "subcontrato")
                    {
                        institucionId = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato).InstitucionId;
                    }
                }

                var institucion = ControlInstitucion.ObtenerPorId(institucionId);
                string centroNombre = (institucion != null) ? institucion.Nombre : "";
                string logotipo = "~/Content/img/logo.png";
                var x = ControlSubcontrato.ObtenerByContratoId(institucion.ContratoId);
                foreach (var val in x)
                {
                    if (institucion.Id == val.InstitucionId & val.Logotipo != "undefined")
                        logotipo = val.Logotipo;
                }

                object[] dataTitulos = new object[]
                {
                    centroNombre
                };

                object[] dataArchivo = ControlPDF.generarReciboCorteDeCaja(dataTitulos, listIngresosFiltro, listEgresosFiltro, listCancelacionFiltro, logotipo);

                Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                reportesLog.ProcesoId = 0;
                reportesLog.ProcesoTrackingId = "";
                reportesLog.Modulo = "Caja";
                reportesLog.Reporte = "Cortes";
                reportesLog.Fechayhora = DateTime.Now;
                reportesLog.EstatusId = 1;
                reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                ControlReportesLog.Guardar(reportesLog);

                return JsonConvert.SerializeObject(new { success = true, message = "guardó", ubicacionArchivo = (dataArchivo != null) ? dataArchivo[1].ToString() : "" });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }
    }
}