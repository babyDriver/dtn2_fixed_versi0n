﻿using Business;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Configuration;
using DT;
using System.Web.Security;
using System.Linq;

namespace Web.Application.AdvancedSearch
{
    public partial class AdvancedSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            validatelogin();
        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        [WebMethod]
        public static string getDetallado(Guid trackingid)
        {
            try
            {
                int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);

                var interno = ControlDetenido.ObtenerPorTrackingId(trackingid);
                var detalle = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                var informacion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                var evento = ControlEvento.ObtenerById(informacion.IdEvento);
                var general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);

                var eventUnitResponsable = ControlEventoUnidadResponsable.ObtenerTodosByEventoId(evento.Id);
                
                var estatus = ControlCatalogo.Obtener(detalle.Estatus, Convert.ToInt32(Entity.TipoDeCatalogo.estatus));
                var alerta = ControlWSAEvento.ObtenerPorId(evento.IdEventoWS);
                var calificacion = ControlCalificacion.ObtenerPorInternoId(detalle.Id);
                var salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalle.Id);
                var subcontrato = ControlSubcontrato.ObtenerPorId(detalle.ContratoId);
                var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                var motivo1 = "";
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(informacion.IdDetenido_Evento);
                if(detenidoevento!=null)
                {
                    var m = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                    if (m != null) motivo1 = m.NombreMotivoLlamada;
                }
                
                var responsableString = "";
                var unidadString = "";
                var corporacionString = "";
                if(eventUnitResponsable.Count>0)
                {
                    foreach (var item in eventUnitResponsable)
                    {
                        var unidad = ControlUnidad.ObtenerById(item.UnidadId);
                        unidadString += (unidad.Nombre ?? "") + ", ";

                        var corporacion = ControlCatalogo.Obtener(unidad.CorporacionId, (int)Entity.TipoDeCatalogo.corporacion);
                        corporacionString += (corporacion.Nombre ?? "") + ", ";

                        var responsable = ControlCatalogo.Obtener(item.ResponsableId, (int)Entity.TipoDeCatalogo.responsable_unidad);
                        responsableString += responsable.Descripcion + "-" + responsable.Nombre + ", ";
                    }

                    unidadString = unidadString.Substring(0, unidadString.Length - 2);
                    corporacionString = corporacionString.Substring(0, corporacionString.Length - 2);
                    responsableString = responsableString.Substring(0, responsableString.Length - 2);
                }

                var nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                var remision = detalle.Expediente;
                
                var registro = "";
                if (detalle.Fecha!=null)
                {
                    var f1 = Convert.ToDateTime(detalle.Fecha);
                    var h1 = f1.Hour.ToString();
                    if (f1.Hour < 10) h1 = "0" + h1;
                    var m1 = f1.Minute.ToString();
                    if (f1.Minute < 10) m1 = "0" + m1;
                    var s1 = f1.Second.ToString();
                    if (f1.Second < 10) s1 = "0" + s1;
                    registro = f1.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;

                }
                
                var ES = estatus.Nombre;
                var llamada = "";
                if (alerta != null)
                {
                    llamada = alerta.Folio;
                }
                var Even = evento.Folio;
                var Descripcion = evento.Descripcion;
                var lugar = informacion.LugarDetencion;
                var coloniaEvento = ControlColonia.ObtenerPorId(evento.ColoniaId);
                var coloniaNumeroEvento = $"Colonia: {coloniaEvento.Asentamiento} C.P. {coloniaEvento.CodigoPostal}";

                var situacionCalificacion = "";
                var juezCalifico = "";
                DateTime fechaSalidaAdd = new DateTime();
                fechaSalidaAdd = DateTime.MinValue;
                DateTime fechaSalida = evento.HoraYFecha;
                int horasRestantes = 0;
                if (calificacion != null)
                {
                    var usuarioCalifico = ControlUsuario.Obtener(calificacion.CreadoPor);
                    if (usuarioCalifico != null)
                    {
                        if (usuarioCalifico.RolId == 13) usuarioCalifico = ControlUsuario.Obtener(8);
                        juezCalifico = usuarioCalifico.User;
                    }
                    var situacionObj = ControlCatalogo.Obtener(calificacion.SituacionId, (int)Entity.TipoDeCatalogo.situacion_detenido);
                    if (situacionObj != null) situacionCalificacion = situacionObj.Nombre;

                    var horas = calificacion.TotalHoras;
                    fechaSalidaAdd = fechaSalida.AddHours(horas);
                    horasRestantes = calificacion.TotalHoras;
                }
                
                var hist = ControlHistorial.ObtenerPorDetenido(detalle.Id).Where(x => x.Movimiento.ToLower().Contains("salida") || x.Movimiento.ToLower().Contains("traslado"));
                
                foreach (var item in hist)
                {
                    fechaSalidaAdd = item.Fecha;
                }
                var hora = Convert.ToDateTime(detalle.Fecha).Hour.ToString();
                if (Convert.ToDateTime(detalle.Fecha).Hour < 10)
                {
                    hora = "0" + hora;
                }

                var minutos = Convert.ToDateTime(detalle.Fecha).Minute.ToString();
                if (Convert.ToDateTime(detalle.Fecha).Minute < 10)
                {
                    minutos = "0" + minutos;
                }
                var tiempo = hora + ":" + minutos;

                var tipoSalida = "";
                if (salida != null)
                {
                    var salidaTipo = ControlCatalogo.Obtener(salida.TiposalidaId, (int)Entity.TipoDeCatalogo.salida_tipo);
                    if (salidaTipo != null)
                    {
                        tipoSalida = salidaTipo.Nombre;
                    }
                }

                var foto = "";
                if (string.IsNullOrEmpty(interno.RutaImagen))
                {
                    interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                }
                foto = interno.RutaImagen;

                var domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                string colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).Asentamiento : "" : "";
                string cp = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "" : "";
                string calle = (domicilio != null) ? (domicilio.Calle != null) ? domicilio.Calle : "" : "";
                string numero = (domicilio != null) ? (domicilio.Numero != null) ? domicilio.Numero : "" : "";
                string domicilioCompleto = $"{(!string.IsNullOrWhiteSpace(calle) ? "Calle: " + calle : "")} {(!string.IsNullOrWhiteSpace(numero) ? "#" + numero : "")} {(!string.IsNullOrWhiteSpace(colonia) ? "Colonia: " + colonia : "")} {(!string.IsNullOrWhiteSpace(cp) ? "C.P. " + cp : "")}";

                var aliases = ControlAlias.ObteneTodos();
                List<Entity.Alias> aliasesDetenido = aliases.Where(x => x.DetenidoId == interno.Id).ToList();
                string aliasDetenido = "";
                if (aliasesDetenido.Count > 0)
                {
                    foreach (var alias in aliasesDetenido)
                    {
                        aliasDetenido += alias.Nombre + ", ";
                    }
                    aliasDetenido = aliasDetenido.Substring(0, aliasDetenido.Length - 2);
                }

                string motivoString = "";
                Entity.MotivoDetencionInterno motAux = new Entity.MotivoDetencionInterno();
                motAux.InternoId = detalle.Id;
                var motivos = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(motAux);
                if (motivos.Count > 0)
                {
                    var totalHorasMotivos = 0;
                    foreach (var motivo in motivos)
                    {
                        var mot = ControlMotivoDetencion.ObtenerPorId(motivo.MotivoDetencionId);
                        if (mot != null)
                        {
                            if (motivo.Habilitado)
                            {
                                totalHorasMotivos += mot.HorasdeArresto;
                                motivoString += mot.Motivo + ", ";
                            }
                        }
                    }
                    if (motivoString != "")
                    {
                        motivoString = motivoString.Substring(0, motivoString.Length - 2);
                    }

                    if (totalHorasMotivos > 72) totalHorasMotivos = 72;
                    if (horasRestantes < totalHorasMotivos) horasRestantes = totalHorasMotivos;
                }

                string tieneHit = "";
                var filtroHit = new int[]
                {
                    idContrato,
                    interno.Id
                };
                var hit = ControlCrossreference.GetCrossReferences(filtroHit);
                if (hit.Count > 0) tieneHit = "Si";
                else tieneHit = "No";


                var certificadosString = "";
                var diagnosticoString = "";
                var servicioMedico = ControlServicoMedico.ObtenerPorDetalleDetencionId(detalle.Id).OrderBy(x=>x.FechaRegistro).LastOrDefault();
                if (servicioMedico != null)
                {
                    if(servicioMedico.CertificadoLesionId != 0)
                    {
                        var cerlesion = ControlCertificadoLesiones.ObtenerPorId(servicioMedico.CertificadoLesionId);
                        var h1 = cerlesion.FechaRegistro.Hour.ToString();
                        if (cerlesion.FechaRegistro.Hour < 10) h1 = "0" + h1;
                        var m1 = cerlesion.FechaRegistro.Minute.ToString();
                        if (cerlesion.FechaRegistro.Minute < 10) m1 = "0" + m1;
                        var s1 = cerlesion.FechaRegistro.Second.ToString();
                        if (cerlesion.FechaRegistro.Second < 10) s1 = "0" + s1;
                        var f = cerlesion.FechaRegistro.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;
                        certificadosString += $"Certificado de lesiones: {f}\n";
                    }
                    if (servicioMedico.CertificadoMedicoPsicofisiologicoId != 0)
                    {
                        var cerpsico = ControlCertificadoPsicoFisiologico.ObtenerPorId(servicioMedico.CertificadoMedicoPsicofisiologicoId);
                        var h1 = cerpsico.FechaValoracion.Hour.ToString();
                        if (cerpsico.FechaValoracion.Hour < 10) h1 = "0" + h1;
                        var m1 = cerpsico.FechaValoracion.Minute.ToString();
                        if (cerpsico.FechaValoracion.Minute < 10) m1 = "0" + m1;
                        var s1 = cerpsico.FechaValoracion.Second.ToString();
                        if (cerpsico.FechaValoracion.Second < 10) s1 = "0" + s1;
                        var f = cerpsico.FechaValoracion.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;
                        certificadosString += $"Certificado psicofisiológico: {f}\n";
                    }
                    if (servicioMedico.CertificadoQuimicoId != 0)
                    {
                        var cerquimico = ControlCertificadoQuimico.ObtenerPorId(servicioMedico.CertificadoQuimicoId);

                        diagnosticoString += "Etanol: " + ObtenerOpcionComun(cerquimico.EtanolId) + " " + (cerquimico.Grado != 0 ? "(" + cerquimico.Grado + " mg/dL)" : "") + "\n";
                        diagnosticoString += "Benzodiazepina: " + ObtenerOpcionComun(cerquimico.BenzodiapinaId) + "\n";
                        diagnosticoString += "Anfetaminas: " + ObtenerOpcionComun(cerquimico.AnfetaminaId) + "\n";
                        diagnosticoString += "Cannabis: " + ObtenerOpcionComun(cerquimico.CannabisId) + "\n";
                        diagnosticoString += "Cocaína: " + ObtenerOpcionComun(cerquimico.CocainaId) + "\n";
                        diagnosticoString += "Éxtasis: " + ObtenerOpcionComun(cerquimico.ExtasisId);

                        var h1 = cerquimico.FechaRegistro.Hour.ToString();
                        if (cerquimico.FechaRegistro.Hour < 10) h1 = "0" + h1;
                        var m1 = cerquimico.FechaRegistro.Minute.ToString();
                        if (cerquimico.FechaRegistro.Minute < 10) m1 = "0" + m1;
                        var s1 = cerquimico.FechaRegistro.Second.ToString();
                        if (cerquimico.FechaRegistro.Second < 10) s1 = "0" + s1;
                        var f = cerquimico.FechaRegistro.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;

                        certificadosString += $"Certificado químico: {f}";
                    }
                }

                var edad = 0;
                if (general.FechaNacimineto != DateTime.MinValue)
                    edad = CalcularEdad(general.FechaNacimineto);
                else if (general.Edaddetenido != 0)
                    edad = general.Edaddetenido;
                else
                {
                    var infDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                    var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infDetencion.IdDetenido_Evento);

                    if (detenidoEvento != null)
                    {
                        edad = detenidoEvento.Edad;
                    }
                }

                object obj = null;
                obj = new
                {
                    Detenido = nombre,
                    Numero = remision,
                    Reg = registro,
                    Esta = ES,
                    Call = llamada,
                    Eve = Even,
                    Hechos = Descripcion,
                    place = lugar,
                    Unit = unidadString,
                    Responsable = responsableString,
                    Corporacion = corporacionString,
                    Horas = tiempo,
                    Picture = foto,
                    domicilio = domicilioCompleto,
                    edad = edad,
                    alias = aliasDetenido,
                    motivos = motivoString,
                    MotivoEvento=motivo1,
                    tipoSalida = tipoSalida,
                    fechaSalida = fechaSalidaAdd != DateTime.MinValue ? fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss") : "",
                    institucion = institucion.Nombre,
                    horasSalida = horasRestantes == 0 ? "" : horasRestantes.ToString(),
                    situacion = situacionCalificacion,
                    juezCalifico = juezCalifico,
                    coloniaNumeroEvento = coloniaNumeroEvento,
                    tieneHit = tieneHit,
                    sexo = ControlCatalogo.Obtener(general.SexoId, (int)Entity.TipoDeCatalogo.sexo).Nombre,
                    certificadosMedicos = certificadosString,
                    diagnosticoMedico = diagnosticoString
                };
                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }

        private static string ObtenerOpcionComun(int Id)
        {
            return ControlCatalogo.Obtener(Id, Convert.ToInt32(Entity.TipoDeCatalogo.opcion_comun)).Nombre;
        }

        private static int CalcularEdad(DateTime fechaNac)
        {
            int edad = 0;
            edad = DateTime.Now.Year - fechaNac.Year;
            if (DateTime.IsLeapYear(fechaNac.Year) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear + 1 < fechaNac.DayOfYear)
                    edad = edad - 1;
            }
            else if (!DateTime.IsLeapYear(fechaNac.Year) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear + 1)
                    edad = edad - 1;
            }
            else
            {
                if (DateTime.Now.DayOfYear < fechaNac.DayOfYear)
                    edad = edad - 1;
            }

            return edad;
        }

        [WebMethod]
        public static string GetSencilla(Guid Tracking)
        {
            try
            {
                var interno = ControlDetenido.ObtenerPorTrackingId(Tracking);
                var detalle = ControlDetalleDetencion.ObtenerPorDetenidoId(interno.Id).LastOrDefault();
                var informacion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                var evento = ControlEvento.ObtenerById(informacion.IdEvento);
                var general = ControlGeneral.ObtenerPorDetenidoId(interno.Id);
                var unidad = ControlUnidad.ObtenerPorEventoId(evento.Id);
                var estatus = ControlCatalogo.Obtener(detalle.Estatus, Convert.ToInt32(Entity.TipoDeCatalogo.estatus));
                var alerta = ControlWSAEvento.ObtenerPorId(evento.IdEventoWS);
                var salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalle.Id);
                var calificacion = ControlCalificacion.ObtenerPorInternoId(detalle.Id);

                var nombre = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                var remision = detalle.Expediente;

                
                var registro = "";
                if (detalle.Fecha != null)
                {
                    var f1 = Convert.ToDateTime(detalle.Fecha);
                    var h1 = f1.Hour.ToString();
                    if (f1.Hour < 10) h1 = "0" + h1;
                    var m1 = f1.Minute.ToString();
                    if (f1.Minute < 10) m1 = "0" + m1;
                    var s1 = f1.Second.ToString();
                    if (f1.Second < 10) s1 = "0" + s1;
                    registro = f1.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;

                }
                var ES = estatus.Nombre;
                var llamada = "";
                if(alerta !=null)
                {
                    llamada = alerta.Folio;
                }
                var Even = evento.Folio;
                var Descripcion = evento.Descripcion;
                var hora = Convert.ToDateTime(detalle.Fecha).Hour.ToString();
                if(Convert.ToDateTime(detalle.Fecha).Hour<10)
                {
                    hora = "0" + hora;
                }
                var minutos= Convert.ToDateTime(detalle.Fecha).Minute.ToString();
                if(Convert.ToDateTime(detalle.Fecha).Minute < 10)
                    {
                    minutos = "0" + minutos;
                }
                var tiempo = hora + ":" + minutos;

                var tipoSalida = "";
                if(salida !=null)
                {
                    var salidaTipo = ControlCatalogo.Obtener(salida.TiposalidaId, (int)Entity.TipoDeCatalogo.salida_tipo);
                    if (salidaTipo != null)
                    {
                        tipoSalida = salidaTipo.Nombre;
                    }
                }

                var foto = "";
                if(string.IsNullOrEmpty(interno.RutaImagen))
                {
                    interno.RutaImagen = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                }
                 foto = interno.RutaImagen;

                var domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);
                string colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).Asentamiento : "" : "";
                string cp = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "" : "";
                string calle = (domicilio != null) ? (domicilio.Calle != null) ? domicilio.Calle : "" : "";
                string numero = (domicilio != null) ? (domicilio.Numero != null) ? domicilio.Numero : "" : "";
                string domicilioCompleto = $"{(!string.IsNullOrWhiteSpace(calle) ? "Calle: " + calle : "")} {(!string.IsNullOrWhiteSpace(numero) ? "#" + numero : "")} {(!string.IsNullOrWhiteSpace(colonia) ? "Colonia: " + colonia : "")} {(!string.IsNullOrWhiteSpace(cp) ? "C.P. " + cp : "")}";
                var motivoe = "";
                var detenidoevento = ControlDetenidoEvento.ObtenerPorId(informacion.IdDetenido_Evento);
                if(detenidoevento!=null)
                {
                    var m = ControlWSAMotivo.ObtenerPorId(detenidoevento.MotivoId);
                    if (m != null) motivoe = m.NombreMotivoLlamada;
                }
                
                var aliases = ControlAlias.ObteneTodos();
                List<Entity.Alias> aliasesDetenido = aliases.Where(x => x.DetenidoId == interno.Id).ToList();
                string aliasDetenido = "";
                if (aliasesDetenido.Count > 0)
                {
                    foreach (var alias in aliasesDetenido)
                    {
                        aliasDetenido += alias.Nombre + ", ";
                    }
                    aliasDetenido = aliasDetenido.Substring(0, aliasDetenido.Length - 2);
                }

                DateTime fechaSalidaAdd = new DateTime();
                fechaSalidaAdd = DateTime.MinValue;
                DateTime fechaSalida = evento.HoraYFecha;
                if (calificacion != null)
                {
                    var horas = calificacion.TotalHoras;
                    fechaSalidaAdd = fechaSalida.AddHours(horas);
                }
                
                var hist = ControlHistorial.ObtenerPorDetenido(detalle.Id).Where(x => x.Movimiento.ToLower().Contains("salida") || x.Movimiento.ToLower().Contains("traslado"));

                foreach (var item in hist)
                {
                    fechaSalidaAdd = item.Fecha;
                }
                string motivoString = "";
                Entity.MotivoDetencionInterno motAux = new Entity.MotivoDetencionInterno();
                motAux.InternoId = detalle.Id;
                var motivos = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(motAux);
                if(motivos.Count> 0)
                {
                    foreach (var motivo in motivos)
                    {
                        var mot = ControlMotivoDetencion.ObtenerPorId(motivo.MotivoDetencionId);
                        if (mot != null)
                        {
                            if (motivo.Habilitado)
                            {
                                motivoString += mot.Motivo + ", ";
                            }
                        }
                    }
                    if (motivoString != "")
                    {
                        motivoString = motivoString.Substring(0, motivoString.Length - 2);
                    }
                }

                var edad = 0;
                if (general.FechaNacimineto != DateTime.MinValue)
                    edad = CalcularEdad(general.FechaNacimineto);
                else if (general.Edaddetenido != 0)
                    edad = general.Edaddetenido;
                else
                {
                    var infDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(interno.Id);
                    var detenidoEvento = ControlDetenidoEvento.ObtenerPorId(infDetencion.IdDetenido_Evento);

                    if (detenidoEvento != null)
                    {
                        edad = detenidoEvento.Edad;
                    }
                }

                object obj = null;
                obj = new
                {
                    Detenido = nombre,
                    Numero = remision,
                    REg = registro,
                    Esta = ES,
                    Call = llamada,
                    Eve = Even,
                    Hechos = Descripcion,
                    Horas = tiempo,
                    Imagen = foto,
                    domicilio = domicilioCompleto,
                    edad = edad,
                    alias = aliasDetenido,
                    motivos = motivoString,
                    MotivoEvento=motivoe,
                    tipoSalida = tipoSalida,
                    fechaSalida = fechaSalidaAdd != DateTime.MinValue ? fechaSalidaAdd.ToString("dd-MM-yyyy HH:mm:ss") : ""
                };
                return JsonConvert.SerializeObject(new { exitoso = true, obj = obj, });
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(new { success = false, message = e.Message });
            }
        }

        [WebMethod]
        public static DataTable getdata(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, bool emptytable, string Nombre, string Apellido_paterno, string Apellido_materno, string Edad, string Alias,
            string No_remision, string Folio_llamada, string Unidad, string Corporacion, string Evento, string Descripcion_hechos, string Motivo_calificacion, string Horas, string inicial,string final)
        {
            try
            {
                if (!emptytable)
                {

                    MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);

                    List<Where> where = new List<Where>();
                    //where.Add(new Where("ES.Activo", "1"));
                    //where.Add(new Where("C.Activo", "1"));

                    if (!string.IsNullOrWhiteSpace(Nombre)) where.Add(new Where("D.Nombre", Where.LIKE, "%" + Nombre + "%"));
                    if (!string.IsNullOrWhiteSpace(Apellido_paterno)) where.Add(new Where("trim(D.Paterno)", Where.LIKE, "%" + Apellido_paterno + "%"));
                    if (!string.IsNullOrWhiteSpace(Apellido_materno)) where.Add(new Where("trim(D.Materno)", Where.LIKE, "%" + Apellido_materno + "%"));
                    if (!string.IsNullOrWhiteSpace(Edad)) where.Add(new Where("DD.DetalledetencionEdad", Where.LIKE, "%" + Edad + "%"));
                    if (!string.IsNullOrWhiteSpace(Alias)) where.Add(new Where("A.Alias", Where.LIKE, "%" + Alias + "%"));
                    if (!string.IsNullOrWhiteSpace(No_remision)) where.Add(new Where("CAST(DD.Expediente AS unsigned)", Where.LIKE, "%" + No_remision + "%"));
                    if (!string.IsNullOrWhiteSpace(Folio_llamada)) where.Add(new Where("ifnull(WE.Folio,'')", Where.LIKE, "%" + Folio_llamada + "%"));
                    if (!string.IsNullOrWhiteSpace(Unidad)) where.Add(new Where("ifnull(U.Nombre,'')", Where.LIKE, "%" + Unidad + "%"));
                    if (!string.IsNullOrWhiteSpace(Corporacion)) where.Add(new Where("ifnull(C.Nombre,'')", Where.LIKE, "%" + Corporacion + "%"));
                    if (!string.IsNullOrWhiteSpace(Evento)) where.Add(new Where("E.Descripcion", Where.LIKE, "%" + Evento + "%"));
                    if (!string.IsNullOrWhiteSpace(Descripcion_hechos)) where.Add(new Where("E.Descripcion", Where.LIKE, "%" + Descripcion_hechos + "%"));
                    if (!string.IsNullOrWhiteSpace(Motivo_calificacion)) where.Add(new Where("INF.Motivo", Where.LIKE, "%" + Motivo_calificacion + "%"));
                    if (!string.IsNullOrWhiteSpace(Horas)) where.Add(new Where("ifnull(CA.TotalHoras,0)", Where.LIKE, "%" + Horas + "%"));
                    if(!string.IsNullOrEmpty(inicial) && !string.IsNullOrEmpty(final))
                    {
                        var fechaFinal = Convert.ToDateTime(final);
                        var fechaInicial = Convert.ToDateTime(inicial);
                        fechaFinal = fechaFinal.AddDays(1);

                        where.Add(new Where("DD.Fecha", Where.GREATEREQUAL,fechaInicial.ToString("yyyy-MM-INFdd")));
                        where.Add(new Where("DD.Fecha", Where.LESSEQUAL, fechaFinal.ToString("yyyy-MM-dd")));
                    }

                    var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];

                    var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                    int usuario;

                    if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                    else throw new Exception("No es posible obtener información del usuario en el sistema");

                    var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var contrato = contratoUsuarioK.IdContrato;
                    where.Add(new Where("DD.ContratoId", contratoUsuarioK.IdContrato.ToString()));
                    // where.Add(new Where("DD.Activo", "1"));
                    // where.Add(new Where("ES.estatus", "1"));
                    // where.Add(new Where("ifnull(A.Id,0)", "0"));

                    Query query = new Query
                    {
                        select = new List<string> {
                        "D.trackingId",
                        "TRIM(D.Nombre) Nombre",
                        "TRIM(D.Paterno) Paterno",
                        "TRIM(D.Materno) Materno",
                        "concat(TRIM(D.Nombre),' ',TRIM(Paterno),' ',TRIM(Materno)) as NombreCompleto",
                        "DD.DetalledetencionEdad Edad",
                        "A.Alias",
                        "CAST(DD.Expediente AS unsigned) Expediente",
                        "ifnull(WE.Folio,'') Folio",
                        "ifnull(U.Nombre,'') Unidad",
                        "ifnull(C.Nombre,'') Corporacion",
                        "E.Folio Evento",
                        "E.Descripcion DescripcionHechos",
                        //"ifnull(MTV.nombreMotivoLlamada, '') Motivo",
                        "CA.Razon Motivo",
                        "ifnull(CA.TotalHoras, 0) Horas"
                    },
                        from = new Table("detalle_detencion", "DD"),
                        joins = new List<Join>
                    {
                        new Join(new Table("detenido", "D"), "DD.DetenidoId=D.Id","INNER"),
                        new Join(new Table("detenidosalias", "A"), "A.DetenidoId=D.Id"),
                        new Join(new Table("informaciondedetencion", "INF"), "INF.IdInterno=D.Id"),
                        new Join(new Table("eventos", "E"), "INF.IdEvento =E.Id"),
                        new Join(new Table("evento_unidad_responsable", "EUR"), " EUR.Id=(select EU.Id from evento_unidad_responsable EU where EU.EventoId=E.Id order by 1 desc limit 1)"),
                        new Join(new Table("unidad", "U"), " EUR.UnidadId=U.Id"),
                        new Join(new Table("corporacion", "C"), "U.CorporacionId=C.Id"),
                        new Join(new Table("calificacion", "CA"), "CA.InternoId=DD.Id"),                        
                        new Join(new Table("wsaevento", "WE"), "WE.Id=E.IdEventoWS")
                        //new Join(new Table("wsamotivo", "MTV"), "MTV.idMotivo=E.MotivoId", "LEFT"),
                        //new Join(new Table("general", "G"), " G.DetenidoId=D.Id","INNER"),
                        //new Join(new Table("vdetencion_totalhorascalificacion", "VDC"), "VDC.IdDetencion = DD.Id", "LEFT"),
                        //new Join(new Table("VMotivoDetencionCalificacion", "M"), "M.detenidoId=DD.Id"),
                    },
                        wheres = where
                    };

                    DataTablesAux dt = new DataTablesAux(mysqlConnection);
                    var data = dt.Generar(query, draw, start, length, search, order, columns);
                    return data;
                }
                else
                {
                    return DataTables.ObtenerDataTableVacia(null, draw);
                }
            }
            catch (Exception ex)
            {
                return DataTables.ObtenerDataTableVacia(ex.ToString(), draw);
            }
           
        }
    }
}