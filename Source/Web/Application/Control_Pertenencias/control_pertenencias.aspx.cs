﻿using Business;
using DT;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Web.Application.Registry;

namespace Web.Application.Control_Pertenencias
{
    public partial class control_pertenencias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // validatelogin();
            getPermisos();

            if(!IsPostBack)
            {
                tbFechaInicial.Text =  DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd");
                tbFechaFinal.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }

        }
        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;

            }

        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                string[] parametros = { usuario.ToString(), "Control de pertenencias/evidencias" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);


                if (permisos == null)
                {
                    //No cuenta con permisos 
                    Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                }
                else
                {
                    if (!permisos.Consultar)
                        //No cuenta con permisos de consultar el  apartado
                        Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
                    else
                    {
                        //setear hidden registrar
                        this.HQLNBB.Value = permisos.Registrar.ToString().ToLower();
                        //setear hidden modificar
                        this.KAQWPK.Value = permisos.Modificar.ToString().ToLower();
                        //setear hidden habilitar
                        this.LCADLW.Value = permisos.Eliminar.ToString().ToLower();
                        //setear hidden nombre usuario
                        var usr = ControlUsuario.Obtener(usuario);
                        if (usr.RolId == 13)
                        {
                            usuario = 8;
                        }
                        this.UENSRA.Value = ControlUsuario.Obtener(usuario).User;
                    }
                }
            }
            else
            {
                Response.Redirect(String.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        [WebMethod]
        public static List<Combo> getYears()
        {
            List<Combo> combo = new List<Combo>();
            var listaanios = ControlDashBoardAnioTrabajo.GetAniosTrabajo();

            List<int> Anios = new List<int>();
            foreach (var item in listaanios)
            {
                Anios.Add(item.Aniotrabajo);
            }

            if (Anios.Count == 0 || DateTime.Now.Year > Anios.Max())
            {
                Entity.DasboardAnioTrabajo dasboardAnioTrabajo = new Entity.DasboardAnioTrabajo();
                dasboardAnioTrabajo.Aniotrabajo = DateTime.Now.Year;
                listaanios.Add(dasboardAnioTrabajo);
            }

            foreach (var item in listaanios.OrderByDescending(x => x.Aniotrabajo))
            {
                combo.Add(new Combo { Desc = item.Aniotrabajo.ToString(), Id = item.Aniotrabajo.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string GetParametrosTamaño()
        {
            try
            {
                var contratoususario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]));
                var parametro = ControlParametroContrato.TraerTodos();
                decimal maximo = 0;
                decimal minimo = 0;

                foreach (var item in parametro)
                {
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 13)
                    {
                        minimo = Convert.ToDecimal(item.Valor);
                    }
                    if (item.ContratoId == contratoususario.IdContrato && item.Parametroid == 14)
                    {
                        maximo = Convert.ToDecimal(item.Valor);
                    }
                }
                return JsonConvert.SerializeObject(new { exitoso = true, TMax = maximo, Tmin = minimo });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }


        [WebMethod]
        public static string getPertenenciasComunes(string interno)
        {
            try
            {
                List<object> list = new List<object>();
                var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                List<Entity.Catalogo> pertenenciasComunes = ControlCatalogo.ObtenerHabilitados(123);
                string[] ids = new string[] { interno, "true" };
                var detalledetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(ids);
                List<Entity.Pertenencia> pertenenciasRegistradas = ControlPertenencia.ObtenerPorInterno(Convert.ToInt32(detalledetencion.Id));
                string fecha = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                foreach (var item in pertenenciasComunes)
                {
                    object obj = new
                    {
                        Id = "",
                        TrackingId = "",
                        item.Nombre,
                        Fecha = fecha,
                        Observacion = "",
                        Cantidad = "",
                        Bolsa = "",
                        ClasificacionId = "",
                        CasilleroId = "",
                        Fotografia = "",
                        Casillero = "",
                        Clasificacion = ""
                    };

                    list.Add(obj);
                }

                foreach (var item in pertenenciasRegistradas)
                {
                    if (item.Habilitado && item.Activo)
                    {
                        object obj = new
                        {
                            Id = item.Id,
                            TrackingId = item.TrackingId.ToString(),
                            Nombre = item.PertenenciaNombre,
                            Fecha = fecha,
                            Observacion = item.Observacion,
                            Cantidad = item.Cantidad,
                            Bolsa = item.Bolsa,
                            ClasificacionId = item.Clasificacion,
                            CasilleroId = item.CasilleroId,
                            Fotografia = item.Fotografia,
                            Casillero = ControlCasillero.ObtenerPorId(item.CasilleroId).Nombre,
                            Clasificacion = ControlClasificacionEvidencia.ObtenerPorId(item.Clasificacion).Nombre
                        };

                        list.Add(obj);
                    }
                }

                return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static string getNumInterno(string internoId)
        {
            string exp = "";
            if(internoId !=null)
            {
                var detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(internoId));
                exp = detalleDetencion[0].Expediente;
            }
            
            return exp;
        }

        public static List<T> compareSearch<T>(List<T> list, string search)
        {
            var type = list.GetType().GetGenericArguments()[0];
            var properties = type.GetProperties();
            var xx = list.Where(x => properties.Any(p =>
            {
                var value = p.GetValue(x) != null ? p.GetValue(x) : string.Empty;
                value = value.ToString().ToLower();
                return value.ToString().Contains(search);
            }));

            return xx.ToList();
        }

        [WebMethod]
        public static object getDataPrincipal(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, string[] datos, string pages, string year, string fechaInicial, string fechaFinal)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de pertenencias/evidencias" }).Consultar)
                {
                    int yearFilter = string.IsNullOrEmpty(year) ? DateTime.Now.Year : Convert.ToInt32(year);
                    List<object> list = new List<object>();
                    List<Entity.ListadoPertenencia> listTotals = new List<Entity.ListadoPertenencia>();
                    List<Entity.ListadoPertenencia> listFinal = new List<Entity.ListadoPertenencia>();
                    List<Entity.ListadoPertenencia> data = new List<Entity.ListadoPertenencia>();

                    string internoId = !string.IsNullOrEmpty(datos[3]) ? datos[3].ToString() : "0";

                    string[] datosInterno = new string[]
                    {
                                    internoId,
                                    true.ToString()
                    };

                    if(internoId != "0" && !string.IsNullOrEmpty(internoId))
                    {
                        var detAux = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(internoId));
                        if(detAux != null)
                        {
                            var detFin = detAux.Where(x => x.Activo).FirstOrDefault();
                            if(detFin == null)
                            {
                                datos[0] = "1";
                                datos[1] = "0";
                                datos[2] = "0";
                            }
                        }
                    }
                    
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datosInterno);
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    String Fotografia = string.Empty;
                    string estatusInternoId = detalleDetencion != null ? detalleDetencion.Id.ToString() : "0";
                    //var calificacion = ControlCalificacion.ObtenerPorInternoId(estatusInterno.Id);

                    //Todo sin filtros
                    if (datos[0].ToString() == "0" && datos[1].ToString() == "0" && datos[2].ToString() == "0")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId),
                            yearFilter,
                            fechaInicial,
                            fechaFinal
                        };
                        
                        listTotals = ControlListadoPertenencia.ObtenerTodos(parametros).ToList();

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre.Trim()).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno.Trim()).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno.Trim()).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Clasificacion.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Clasificacion.Trim()).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Pertenencia.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Pertenencia.Trim()).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Cantidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Cantidad).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Bolsa).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Bolsa).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estatus.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estatus.Trim()).ToList();
                                    break;
                            }
                        }

                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        foreach (var item in listFinal)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : false;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                var foto = System.Web.HttpContext.Current.Server.MapPath(item.Fotografia);
                                //var foto = item.Fotografia.Replace("//", "/");
                                if (File.Exists(foto))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,                                
                                item.Paterno,
                                item.Materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                esValido,
                                mensajeRN
                                // calificacion = calificacion != null ? calificacion.Id:0
                            };

                            list.Add(obj);
                        }
                    } //Todos los filtros
                    else if (datos[0].ToString() == "1" && datos[1].ToString() == "1" && datos[2].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId),
                            yearFilter,
                            fechaInicial,
                            fechaFinal
                        };

                        listTotals = ControlListadoPertenencia.ObtenerTodos(parametros);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre.Trim()).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno.Trim()).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno.Trim()).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Clasificacion.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Clasificacion.Trim()).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Pertenencia.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Pertenencia.Trim()).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Cantidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Cantidad).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Bolsa).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Bolsa).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estatus.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estatus.Trim()).ToList();
                                    break;
                            }
                        }

                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        foreach (var item in listFinal)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : false;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                item.Paterno,
                                item.Materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                esValido,
                                mensajeRN
                            };

                            list.Add(obj);
                        }
                    } //Libre con pertenencias y detenidos con pertenencias
                    else if (datos[0].ToString() == "1" && datos[1].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId),
                            yearFilter,
                            fechaInicial,
                            fechaFinal
                        };

                        listTotals = ControlListadoPertenencia.ObtenerTodosConPertenencias(parametros);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre.Trim()).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno.Trim()).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno.Trim()).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Clasificacion.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Clasificacion.Trim()).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Pertenencia.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Pertenencia.Trim()).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Cantidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Cantidad).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Bolsa).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Bolsa).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estatus.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estatus.Trim()).ToList();
                                    break;
                            }
                        }

                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        foreach (var item in listFinal)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : false;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";

                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                item.Paterno,
                                item.Materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido
                            };

                            list.Add(obj);
                        }
                    } //Libre con pertenencias y detenido sin pertenencias
                    else if (datos[0].ToString() == "1" && datos[2].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId),
                            yearFilter,
                            fechaInicial,
                            fechaFinal
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerDetenidosSinPertenencias(parametros);
                        List<Entity.ListadoPertenencia> listados2 = ControlListadoPertenencia.ObtenerLibresConPertenencias(parametros);

                        listTotals.AddRange(listados);
                        listTotals.AddRange(listados2);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre.Trim()).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno.Trim()).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno.Trim()).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Clasificacion.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Clasificacion.Trim()).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Pertenencia.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Pertenencia.Trim()).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Cantidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Cantidad).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Bolsa).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Bolsa).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estatus.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estatus.Trim()).ToList();
                                    break;
                            }
                        }

                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        foreach (var item in listFinal)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : false;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";

                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                item.Paterno,
                                item.Materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido

                            };

                            list.Add(obj);
                        }

                        //foreach (var item in listados2)
                        //{
                        //    var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                        //    bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                        //    int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                        //    Fotografia = string.Empty;
                        //    if (item.Fotografia != null && item.Fotografia != "")
                        //    {
                        //        if (File.Exists(item.Fotografia))
                        //        {
                        //            Fotografia = item.Fotografia;
                        //        }
                        //    }
                        //    var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                        //    string mensajeRN = "";
                        //    string esValido = "1";
                        //    if (reglanegocio != "")
                        //    {
                        //        mensajeRN = reglanegocio;
                        //        esValido = "0";

                        //    }
                        //    var paterno = item.Paterno.Trim();
                        //    var materno = item.Materno.Trim();
                        //    var NombreUsuario = item.UsuarioUsuario;
                        //    if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                        //    object obj = new
                        //    {
                        //        item.Id,
                        //        item.Expediente,
                        //        item.IdEstatusInterno,
                        //        item.PertenenciaId,
                        //        item.TrackingId,
                        //        item.Nombre,
                        //        paterno,
                        //        materno,
                        //        item.Clasificacion,
                        //        item.Pertenencia,
                        //        item.Cantidad,
                        //        item.Bolsa,
                        //        item.Estatus,
                        //        item.ClasificacionId,
                        //        NombreUsuario,
                        //        item.ApellidoPaterno,
                        //        item.ApellidoMaterno,
                        //        item.Observacion,
                        //        Fotografia,
                        //        habilitadoAux,
                        //        casillero,
                        //        mensajeRN,
                        //        esValido
                        //    };

                        //    list.Add(obj);
                        //}
                    } //Detenidos con pertenencias y detenidos sin pertenencias
                    else if (datos[1].ToString() == "1" && datos[2].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId),
                            yearFilter,
                            fechaInicial,
                            fechaFinal
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerDetenidosSinPertenencias(parametros);
                        List<Entity.ListadoPertenencia> listados2 = ControlListadoPertenencia.ObtenerDetenidosConPertenencias(parametros);

                        listTotals.AddRange(listados);
                        listTotals.AddRange(listados2);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre.Trim()).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno.Trim()).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno.Trim()).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Clasificacion.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Clasificacion.Trim()).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Pertenencia.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Pertenencia.Trim()).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Cantidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Cantidad).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Bolsa).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Bolsa).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estatus.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estatus.Trim()).ToList();
                                    break;
                            }
                        }

                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        foreach (var item in listFinal)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : false;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                item.Paterno,
                                item.Materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido

                            };

                            list.Add(obj);
                        }

                        //foreach (var item in listados2)
                        //{
                        //    var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                        //    bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                        //    int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                        //    Fotografia = string.Empty;
                        //    if (item.Fotografia != null && item.Fotografia != "")
                        //    {
                        //        if (File.Exists(item.Fotografia))
                        //        {
                        //            Fotografia = item.Fotografia;
                        //        }
                        //    }
                        //    var paterno = item.Paterno.Trim();
                        //    var materno = item.Materno.Trim();
                        //    var NombreUsuario = item.UsuarioUsuario;
                        //    if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                        //    object obj = new
                        //    {
                        //        item.Id,
                        //        item.Expediente,
                        //        item.IdEstatusInterno,
                        //        item.PertenenciaId,
                        //        item.TrackingId,
                        //        item.Nombre,
                        //        paterno,
                        //        materno,
                        //        item.Clasificacion,
                        //        item.Pertenencia,
                        //        item.Cantidad,
                        //        item.Bolsa,
                        //        item.Estatus,
                        //        item.ClasificacionId,
                        //        NombreUsuario,
                        //        item.ApellidoPaterno,
                        //        item.ApellidoMaterno,
                        //        item.Observacion,
                        //        Fotografia,
                        //        habilitadoAux,
                        //        casillero
                        //    };

                        //    list.Add(obj);
                        //}
                    } //Libre con pertenencias
                    else if (datos[0].ToString() == "1")
                    {
                        int estatusAux = 0;
                        if (internoId != "0" && !string.IsNullOrEmpty(internoId))
                        {
                            var detAux = ControlDetalleDetencion.ObtenerPorDetenidoId(Convert.ToInt32(internoId));
                            if (detAux != null)
                            {
                                var detFin = detAux.Where(x => x.Activo).FirstOrDefault();                                
                                if (detFin == null)
                                {
                                    estatusAux = detAux.Where(x => x.Activo == false).FirstOrDefault().Id;
                                    datos[0] = "1";
                                    datos[1] = "0";
                                    datos[2] = "0";
                                }
                            }
                        }

                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            estatusAux != 0 ? estatusAux : Convert.ToInt32(estatusInternoId),
                            yearFilter,
                            fechaInicial,
                            fechaFinal
                        };

                        listTotals = ControlListadoPertenencia.ObtenerLibresConPertenencias(parametros);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre.Trim()).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno.Trim()).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno.Trim()).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Clasificacion.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Clasificacion.Trim()).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Pertenencia.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Pertenencia.Trim()).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Cantidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Cantidad).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Bolsa).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Bolsa).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estatus.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estatus.Trim()).ToList();
                                    break;
                            }
                        }

                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        foreach (var item in listFinal)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : false;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                item.Paterno,
                                item.Materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido

                            };

                            list.Add(obj);
                        }
                    }//Detenidos con pertenencias
                    else if (datos[1].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId),
                            yearFilter,
                            fechaInicial,
                            fechaFinal
                        };

                        listTotals = ControlListadoPertenencia.ObtenerDetenidosConPertenencias(parametros);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre.Trim()).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno.Trim()).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno.Trim()).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Clasificacion.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Clasificacion.Trim()).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Pertenencia.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Pertenencia.Trim()).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Cantidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Cantidad).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Bolsa).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Bolsa).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estatus.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estatus.Trim()).ToList();
                                    break;
                            }
                        }

                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        foreach (var item in listFinal)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : false;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(HttpContext.Current.Server.MapPath(item.Fotografia)))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                item.Paterno,
                                item.Materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido
                            };

                            list.Add(obj);
                        }
                    }//Detenidos sin pertenencias
                    else if (datos[2].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId),
                            yearFilter,
                            fechaInicial,
                            fechaFinal

                        };

                        listTotals = ControlListadoPertenencia.ObtenerDetenidosSinPertenencias(parametros);

                        if (listTotals.Count == 0)
                        {
                            object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                            return json2;
                        }

                        if (string.IsNullOrEmpty(search.value))
                        {
                            data = listTotals;
                        }
                        else
                        {
                            var listAux = listTotals;
                            data = compareSearch(listAux, search.value);
                        }

                        if (order.Count > 0)
                        {
                            Order order1 = order[0];

                            switch (order1.column)
                            {
                                case 1:
                                    if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                    break;
                                case 2:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Nombre.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Nombre.Trim()).ToList();
                                    break;
                                case 3:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Paterno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Paterno.Trim()).ToList();
                                    break;
                                case 4:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Materno.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Materno.Trim()).ToList();
                                    break;
                                case 5:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Clasificacion.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Clasificacion.Trim()).ToList();
                                    break;
                                case 6:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Pertenencia.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Pertenencia.Trim()).ToList();
                                    break;
                                case 7:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Cantidad).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Cantidad).ToList();
                                    break;
                                case 8:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Bolsa).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Bolsa).ToList();
                                    break;
                                case 9:
                                    if (order1.dir == "asc") data = data.OrderBy(x => x.Estatus.Trim()).ToList();
                                    else if (order1.dir == "desc") data = data.OrderByDescending(x => x.Estatus.Trim()).ToList();
                                    break;
                            }
                        }

                        int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                        int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                        if (numPages == pageActual)
                        {
                            if ((data.Count / length < 1) || (data.Count % length > 0))
                            {
                                int i = data.Count % length;
                                listFinal = data.GetRange(start, i);
                            }
                            else
                            {
                                listFinal = data.GetRange(start, length);
                            }
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }

                        foreach (var item in listFinal)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : false;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                item.Paterno,
                                item.Materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido

                            };

                            list.Add(obj);
                        }
                    }
                    //string ms = JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
                    //return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
                    object json = new { data = list, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                    return json;
                }
                else
                {
                    object json2 = new { data = new List<Entity.ListadoPertenencia>(), recordsTotal = new List<Entity.ListadoPertenencia>().Count, recordsFiltered = new List<Entity.ListadoPertenencia>().Count, };
                    return json2;
                }
            }
            catch (Exception ex)
            {
                object json2 = new { data = new List<Entity.ListadoPertenencia>(), recordsTotal = new List<Entity.ListadoPertenencia>().Count, recordsFiltered = new List<Entity.ListadoPertenencia>().Count, };
                return json2;
            }
        }

        [WebMethod]
        public static string getData(string[] datos)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de pertenencias/evidencias" }).Consultar)
                {
                    List<object> list = new List<object>();

                    string internoId = datos[3] != null ? datos[3].ToString() : "0";                    

                    string[] datosInterno = new string[]
                    {
                                    internoId,
                                    true.ToString()
                    };

                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(datosInterno);
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    String Fotografia = string.Empty;
                    string estatusInternoId = detalleDetencion != null ? detalleDetencion.Id.ToString() : "0";
                    //var calificacion = ControlCalificacion.ObtenerPorInternoId(estatusInterno.Id);

                    //Todo sin filtros
                    if (datos[0].ToString() == "0" && datos[1].ToString() == "0" && datos[2].ToString() == "0")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId)
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerTodos(parametros);

                        foreach (var item in listados)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                var foto = System.Web.HttpContext.Current.Server.MapPath(item.Fotografia);
                                //var foto = item.Fotografia.Replace("//", "/");
                                if (File.Exists(foto))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                           
                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = ""; 
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                esValido,
                                mensajeRN
                                // calificacion = calificacion != null ? calificacion.Id:0
                            };

                            list.Add(obj);
                        }
                    } //Todos los filtros
                    else if (datos[0].ToString() == "1" && datos[1].ToString() == "1" && datos[2].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId)
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerTodos(parametros);

                        foreach (var item in listados)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                esValido,
                                mensajeRN
                            };

                            list.Add(obj);
                        }
                    } //Libre con pertenencias y detenidos con pertenencias
                    else if (datos[0].ToString() == "1" && datos[1].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId)
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerTodosConPertenencias(parametros);

                        foreach (var item in listados)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";

                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido
                            };

                            list.Add(obj);
                        }
                    } //Libre con pertenencias y detenido sin pertenencias
                    else if (datos[0].ToString() == "1" && datos[2].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId)
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerDetenidosSinPertenencias(parametros);
                        List<Entity.ListadoPertenencia> listados2 = ControlListadoPertenencia.ObtenerLibresConPertenencias(parametros);

                        foreach (var item in listados)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";

                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido
                                
                            };

                            list.Add(obj);
                        }

                        foreach (var item in listados2)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido
                            };

                            list.Add(obj);
                        }
                    } //Detenidos con pertenencias y detenidos sin pertenencias
                    else if (datos[1].ToString() == "1" && datos[2].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId)
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerDetenidosSinPertenencias(parametros);
                        List<Entity.ListadoPertenencia> listados2 = ControlListadoPertenencia.ObtenerDetenidosConPertenencias(parametros);

                        foreach (var item in listados)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }

                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido
                                
                            };

                            list.Add(obj);
                        }

                        foreach (var item in listados2)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero
                            };

                            list.Add(obj);
                        }
                    } //Libre con pertenencias
                    else if (datos[0].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId)
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerLibresConPertenencias(parametros);

                        foreach (var item in listados)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido

                            };

                            list.Add(obj);
                        }
                    }//Detenidos con pertenencias
                    else if (datos[1].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId)
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerDetenidosConPertenencias(parametros);

                        foreach (var item in listados)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido
                            };

                            list.Add(obj);
                        }
                    }//Detenidos sin pertenencias
                    else if (datos[2].ToString() == "1")
                    {
                        object[] parametros = new object[]
                        {
                            contratoUsuario != null ? contratoUsuario.Tipo : "",
                            contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                            Convert.ToInt32(estatusInternoId)
                        };

                        List<Entity.ListadoPertenencia> listados = ControlListadoPertenencia.ObtenerDetenidosSinPertenencias(parametros);

                        foreach (var item in listados)
                        {
                            var pertenenciaAux = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                            bool habilitadoAux = pertenenciaAux != null ? pertenenciaAux.Habilitado : true;
                            int casillero = pertenenciaAux != null ? pertenenciaAux.CasilleroId : 0;

                            Fotografia = string.Empty;
                            if (item.Fotografia != null && item.Fotografia != "")
                            {
                                if (File.Exists(item.Fotografia))
                                {
                                    Fotografia = item.Fotografia;
                                }
                            }
                            var reglanegocio = ControlPertenencia.ReglaNegocio(item.IdEstatusInterno);

                            string mensajeRN = "";
                            string esValido = "1";
                            if (reglanegocio != "")
                            {
                                mensajeRN = reglanegocio;
                                esValido = "0";

                            }
                            var paterno = item.Paterno.Trim();
                            var materno = item.Materno.Trim();
                            var NombreUsuario = item.UsuarioUsuario;
                            if (string.IsNullOrEmpty(NombreUsuario)) NombreUsuario = "";
                            object obj = new
                            {
                                item.Id,
                                item.Expediente,
                                item.IdEstatusInterno,
                                item.PertenenciaId,
                                item.TrackingId,
                                item.Nombre,
                                paterno,
                                materno,
                                item.Clasificacion,
                                item.Pertenencia,
                                item.Cantidad,
                                item.Bolsa,
                                item.Estatus,
                                item.ClasificacionId,
                                NombreUsuario,
                                item.ApellidoPaterno,
                                item.ApellidoMaterno,
                                item.Observacion,
                                Fotografia,
                                habilitadoAux,
                                casillero,
                                mensajeRN,
                                esValido

                            };

                            list.Add(obj);
                        }
                    }
                    string ms= JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", list = list });
                }
                else
                {
                    throw new Exception("No cuenta con privilegios para esta función");
                }                
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        public static List<Column> crearColumnas()
        {
            List<Column> columns = new List<Column>();
            string[] nombreColumnas = new string[]
            {
                "Id", "Expediente", "Nombre", "Paterno", "Materno", "Clasificacion", "Pertenencia", "Cantidad",
                "Bolsa", "Estatus"
            };

            for(int i = 0; i < nombreColumnas.Length; i++)
            {
                Column column = new Column()
                {
                    data = nombreColumnas[i].ToString(),
                    name = "",
                    orderable = true,
                    search = new Search()
                    {
                        regex = false,
                        value = ""
                    },
                    searchable = true
                };
                columns.Add(column);
            }

            return columns;
        }

        [WebMethod]
        public static List<Combo> getClasifications()
        {
            var combo = new List<Combo>();            
            var c = ControlCatalogo.ObtenerHabilitados(85);
            
            foreach (var item in c)
            {
                if(item.Activo && item.Habilitado)
                {
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
                }                
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getInternosAgregar(int id)
        {
            var combo = new List<Combo>();
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
            int idCon = contratoUsuario != null ? contratoUsuario.IdContrato : 0;
            string tipo = contratoUsuario != null ? contratoUsuario.Tipo : string.Empty;
            var c = ControlDetalleDetencion.ObtenerPorDetenidoId(id).Where(x => x.Activo && x.ContratoId == idCon && x.Tipo == tipo && x.Estatus == 1).OrderByDescending(x => x.Id).ToList();                        

            if(c.Count > 1)
            {
                var item = c[0];
                if (item.Activo && item.ContratoId == idCon && item.Tipo == tipo && item.Estatus == 1)
                {
                    string val = item.Expediente + " - " + item.NombreDetenido + " " + item.APaternoDetenido + " " + item.AMaternoDetenido;
                    combo.Add(new Combo { Desc = val, Id = item.DetenidoId.ToString() });
                }
            }
            
            if(c.Count == 1)
            {
                var item = c[0];
                if (item.Activo && item.ContratoId == idCon && item.Tipo == tipo && item.Estatus == 1)
                {
                    string val = item.Expediente + " - " + item.NombreDetenido + " " + item.APaternoDetenido + " " + item.AMaternoDetenido;
                    combo.Add(new Combo { Desc = val, Id = item.DetenidoId.ToString() });
                }
            }

            return combo;
        }

        [WebMethod]
        public static object getDataInternos(int draw, int start, int length, Search search, List<Order> order, List<Column> columns, int detId, string pages, string fechaInicial, string fechaFinal)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de pertenencias/evidencias" }).Consultar)
                {
                    List<Entity.DetenidosActivosSelect> listTotals = new List<Entity.DetenidosActivosSelect>();
                    List<Entity.DetenidosActivosSelect> data = new List<Entity.DetenidosActivosSelect>();
                    List<Entity.DetenidosActivosSelect> listFinal = new List<Entity.DetenidosActivosSelect>();

                    int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                    Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);                    


                    object[] dataParam = new object[]
                    {
                        contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                        contratoUsuario != null ? contratoUsuario.Tipo : string.Empty,
                        fechaInicial,
                        fechaFinal                                              
                    };

                    listTotals = ControlDetenidosActivosSelect.ObtenerTodos(dataParam).ToList();

                    if(detId > 0)
                    {
                        listTotals = listTotals.Where(x => x.Id == detId).ToList();
                    }

                    if (listTotals.Count == 0)
                    {
                        object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                        return json2;
                    }

                    if (string.IsNullOrEmpty(search.value))
                    {
                        data = listTotals;
                    }
                    else
                    {
                        var listAux = listTotals;
                        data = compareSearch(listAux, search.value);
                    }

                    if (order.Count > 0)
                    {
                        Order order1 = order[0];

                        switch (order1.column)
                        {
                            case 0:
                                if (order1.dir == "asc") data = data.OrderBy(x => Convert.ToInt32(x.Expediente)).ToList();
                                else if (order1.dir == "desc") data = data.OrderByDescending(x => Convert.ToInt32(x.Expediente)).ToList();
                                break;                            
                        }
                    }

                    int numPages = (data.Count % length > 0) ? ((data.Count / length) + 1) : (data.Count / length);
                    int pageActual = pages != string.Empty ? Convert.ToInt32(pages) + 1 : 1;

                    if (numPages == pageActual)
                    {
                        if ((data.Count / length < 1) || (data.Count % length > 0))
                        {
                            int i = data.Count % length;
                            listFinal = data.GetRange(start, i);
                        }
                        else
                        {
                            listFinal = data.GetRange(start, length);
                        }
                    }
                    else
                    {
                        listFinal = data.GetRange(start, length);
                    }

                    
                    object json = new { data = listFinal, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                    return json;
                }
                else
                {
                    object json2 = new { data = new List<Entity.DetenidosActivosSelect>(), recordsTotal = new List<Entity.DetenidosActivosSelect>().Count, recordsFiltered = new List<Entity.DetenidosActivosSelect>().Count, };
                    return json2;
                }
            }
            catch (Exception ex)
            {
                object json2 = new { data = new List<Entity.DetenidosActivosSelect>(), recordsTotal = new List<Entity.DetenidosActivosSelect>().Count, recordsFiltered = new List<Entity.DetenidosActivosSelect>().Count, };
                return json2;
            }
        }

        [WebMethod]
        public static object getInternos(string term, int page, int page_limit)
        {
            List<object> dataFin = new List<object>();

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
            List<Entity.DetenidosActivosSelect> listTotals = new List<Entity.DetenidosActivosSelect>();
            List<Entity.DetenidosActivosSelect> data = new List<Entity.DetenidosActivosSelect>();
            List<Entity.DetenidosActivosSelect> listFinal = new List<Entity.DetenidosActivosSelect>();

            object[] dataParam = new object[]
            {
                contratoUsuario != null ? contratoUsuario.IdContrato : 0,
                contratoUsuario != null ? contratoUsuario.Tipo : string.Empty
            };

            listTotals = ControlDetenidosActivosSelect.ObtenerTodos(dataParam);            

            if (listTotals.Count == 0)
            {
                object json2 = new { data = listTotals, recordsTotal = listTotals.Count, recordsFiltered = data.Count, };
                return json2;
            }

            if (string.IsNullOrEmpty(term))
            {
                data = listTotals;
            }
            else
            {
                var listAux = listTotals;
                data = compareSearch(listAux, term);
            }

            int numPages = (data.Count % page_limit > 0) ? ((data.Count / page_limit) + 1) : (data.Count / page_limit);
            int pageActual = page == 1 ? page : Convert.ToInt32(page) + 1;

            if (numPages == pageActual)
            {
                if ((data.Count / page_limit < 1) || (data.Count % page_limit > 0))
                {
                    int i = data.Count % page_limit;
                    listFinal = data.GetRange(page * page_limit, i);
                }
                else
                {
                    listFinal = data.GetRange(page * page_limit, page_limit);
                }
            }
            else
            {
                if(page == 1)
                {
                    listFinal = data.GetRange(0, page_limit);
                }
                else
                {
                    listFinal = data.GetRange(page * page_limit, page_limit);
                }
            }

            foreach (var item in listFinal)
            {
                string text = item.Expediente + " - " + item.Nombre + " " + item.Paterno + " " + item.Materno;
                string id = item.Id.ToString();

                object obj = new
                {
                    id,
                    text
                };

                dataFin.Add(obj);
            }

            //foreach (var item in c)
            //{
            //    var listaDetalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(item.Id);
            //    var listaEstatusActivos = listaDetalleDetencion.Where(x => x.Activo && x.ContratoId == contratoUsuario.IdContrato && x.Tipo == contratoUsuario.Tipo);
            //    var tipo = ControlCatalogo.Obtener("Salida por amparo", Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));


            //    if (listaEstatusActivos.Count() > 0)
            //    {
            //        var tt = listaDetalleDetencion.ElementAt(0);
            //        var salida = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(tt.Id);
            //        if(salida!=null)
            //        {
            //            if(salida.TiposalidaId !=tipo.Id )
            //            {
            //                if (salida.Activo == 1)
            //                combo.Add(new Combo { Desc = tt.Expediente + " - " + item.Nombre + " " + item.Paterno + " " + item.Materno, Id = item.Id.ToString() });
            //            }
            //            else
            //            {
            //                if(!tipo.Activo)
            //                    combo.Add(new Combo { Desc = tt.Expediente + " - " + item.Nombre + " " + item.Paterno + " " + item.Materno, Id = item.Id.ToString() });
            //            }

            //        }
            //        else
            //        {
            //            combo.Add(new Combo { Desc = tt.Expediente + " - " + item.Nombre + " " + item.Paterno + " " + item.Materno, Id = item.Id.ToString() });
            //        }

            //    }                
            //}

            object json = new { results = dataFin, count_filtered  = data.Count, more = true };
            return json;
        }

        [WebMethod]
        public static List<Combo> getEntrega()
        {
            var combo = new List<Combo>();
            var c = ControlEntregaPertenencia.ObtenerTodos();

            foreach (var item in c.Where(x=>x.Habilitado))
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static string getPertenencias(string[] datos, string[] idPertenencias)
        {
            try
            {                
                string ids = "";

                int j = 0;
                for(j = 0; j < idPertenencias.Length; j++)
                {
                    if(j == idPertenencias.Length - 1)
                    {
                        ids += idPertenencias[j].ToString();
                    }
                    else
                    {
                        ids += idPertenencias[j].ToString() + ",";
                    }                    
                }

                object[] parametros =
                {
                    ids,
                    Convert.ToInt32(datos[0])
                };

                List<object> list = ControlPertenencia.GetPertenencias(parametros);                

                var pertenencias = ControlPertenencia.ObtenerPorInterno(Convert.ToInt32(datos[0]));                
                var usuario = ControlUsuario.Obtener(pertenencias[0].CreadoPor);
                if (usuario.RolId == 13)
                {
                    usuario = ControlUsuario.Obtener(8);

                }
                var detalleDetencionData = ControlDetalleDetencion.ObtenerPorId(pertenencias[0].InternoId);
                var internoData = detalleDetencionData != null ? ControlDetenido.ObtenerPorId(detalleDetencionData.DetenidoId) : null;

                string nombreInterno = "";
                string nombreUsuario = "";

                if (internoData != null)
                {
                    nombreInterno = internoData.Nombre + " " + internoData.Paterno + " " + internoData.Materno;
                }

                if(usuario != null)
                {
                    nombreUsuario = usuario.User;
                }                                

                return JsonConvert.SerializeObject(new { success = true, list = list, usuarios = nombreUsuario, interno = nombreInterno });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }

        [WebMethod]
        public static string save(List<Business.PertenenciaAux> data, string[] dataGenerales)
        {
            List<string> errores = new List<string>();
            try
            {                
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                //Guardar la devolucion
                Entity.DevolucionPertenencia devolucion = new Entity.DevolucionPertenencia();
                devolucion.Condicion = dataGenerales[3].ToString();
                devolucion.CreadoPor = usId;
                devolucion.Entrega = Convert.ToInt32(dataGenerales[4]);
                devolucion.FechaEntrega = DateTime.Now;
                devolucion.PersonaQueRecibe = dataGenerales[2].ToString();
                devolucion.PertenenciasDe = dataGenerales[0].ToString();
                devolucion.UsuarioQueRegistro = dataGenerales[1].ToString();
                devolucion.TrackingId = Guid.NewGuid();
                
                var detalleDetencionAux = new Entity.DetalleDetencion();
                var calificacionAux = new Entity.Calificacion();
                var pagoMultaAux = new Entity.CalificacionIngreso();
                var salidaEfectuadaAux = new Entity.SalidaEfectuada();
                var salidaEfectuadaJuezAux = new Entity.SalidaEfectuadaJuez();
                var trabajoSocialAux = new Entity.TrabajoSocial();
                if (data.Count > 0)
                {
                    var item = data[0];
                    var pertenenciasAux = ControlPertenencia.ObtenerPorId(Convert.ToInt32(item.Id));
                    detalleDetencionAux = pertenenciasAux != null ? ControlDetalleDetencion.ObtenerPorId(pertenenciasAux.InternoId) : null;
                    calificacionAux = detalleDetencionAux != null ? ControlCalificacion.ObtenerPorInternoId(detalleDetencionAux.Id) : null;
                    DateTime fechaSalida = new DateTime();
                    DateTime fechaSalidaAdd = new DateTime();
                    TimeSpan horasRestantes;
                    if (calificacionAux != null)
                    {
                        if (!calificacionAux.Activo)
                        {
                            pagoMultaAux = calificacionAux != null ? ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacionAux.Id) : null;

                            if (pagoMultaAux != null)
                            {
                                fechaSalida = detalleDetencionAux.Fecha.Value;
                                fechaSalidaAdd = fechaSalida.AddHours(calificacionAux.TotalHoras);
                                horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                                double tiempoRestante = horasRestantes.TotalMinutes;

                                if (tiempoRestante <= 0)
                                {
                                    errores.Add("El detenido tiene horas por cumplir, le faltan " + Math.Abs(horasRestantes.Hours) + " hora(s), " + Math.Abs(horasRestantes.Minutes) + " minuto(s), " + Math.Abs(horasRestantes.Seconds) + " segundo(s) para salir");
                                    throw new Exception("El detenido tiene horas por cumplir, le faltan " + Math.Abs(horasRestantes.Hours) + " hora(s), " + Math.Abs(horasRestantes.Minutes) + " minuto(s), " + Math.Abs(horasRestantes.Seconds) + " segundo(s) para salir");
                                }
                                else
                                {
                                    if (calificacionAux.TrabajoSocial)
                                    {
                                        salidaEfectuadaAux = ControlSalidaEfectuada.ObtenerByDetalledetencionId(detalleDetencionAux.Id);
                                        salidaEfectuadaJuezAux = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalleDetencionAux.Id);

                                        if (!(salidaEfectuadaAux != null))
                                        {
                                            if (!(salidaEfectuadaJuezAux != null))
                                            {
                                                errores.Add("No se ha realizado la salida efectuada al detenido");
                                                throw new Exception("No se ha realizado la salida efectuada al detenido");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        salidaEfectuadaAux = ControlSalidaEfectuada.ObtenerByDetalledetencionId(detalleDetencionAux.Id);
                                        salidaEfectuadaJuezAux = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalleDetencionAux.Id);

                                        if (!(salidaEfectuadaAux != null))
                                        {
                                            if (!(salidaEfectuadaJuezAux != null))
                                            {
                                                errores.Add("No se ha realizado la salida efectuada al detenido");
                                                throw new Exception("No se ha realizado la salida efectuada al detenido");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errores.Add("No se ha pagado la multa del detenido");
                                throw new Exception("No se ha pagado la multa del detenido");
                            }
                        }
                        else
                        {
                            if (calificacionAux.TotalAPagar > 0)
                            {
                                pagoMultaAux = calificacionAux != null ? ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacionAux.Id) : null;

                                if (!(pagoMultaAux != null))
                                {
                                    errores.Add("No se ha pagado la multa del detenido");
                                    throw new Exception("No se ha pagado la multa del detenido");
                                }
                            }
                            else
                            {
                                if (calificacionAux.TrabajoSocial)
                                {
                                    salidaEfectuadaAux = ControlSalidaEfectuada.ObtenerByDetalledetencionId(detalleDetencionAux.Id);
                                    salidaEfectuadaJuezAux = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalleDetencionAux.Id);

                                    if (!(salidaEfectuadaAux != null))
                                    {
                                        if (!(salidaEfectuadaJuezAux != null))
                                        {
                                            errores.Add("No se ha realizado la salida efectuada al detenido");
                                            throw new Exception("No se ha realizado la salida efectuada al detenido");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Entity.Traslado traslado = new Entity.Traslado();
                        traslado.DetalleDetencionId = detalleDetencionAux.Id;
                        traslado = ControlTraslado.GetTrasladoByDetalleDetencion(traslado);

                        if (string.IsNullOrEmpty(traslado != null ? traslado.TrackingId : ""))
                        {
                            errores.Add("No se ha calificado al detenido");
                            throw new Exception("No se ha calificado al detenido");
                        }
                    }
                }

                //Obtener el Id de la devolucion
                int IdDevolucion = ControlDevolucionPertenencia.Guardar(devolucion);

                if (!(IdDevolucion > 0))
                {
                    throw new Exception("No se pudo registrar la devolucion");
                }

                Entity.DevolucionDetallePertenencia devolucionDetalle = new Entity.DevolucionDetallePertenencia();
                int idPertenencia = Convert.ToInt32(data[0].Id);

                foreach(var item in data)
                {
                    //Guardar los detalles de la devolucion                    
                    devolucionDetalle.CreadoPor = usId;
                    devolucionDetalle.DevolucionId = IdDevolucion;
                    devolucionDetalle.PertenenciaId = Convert.ToInt32(item.Id);
                    devolucionDetalle.TrackingId = Guid.NewGuid();
                    int idDetalle = ControlDevolucionDetallePertenencia.Guardar(devolucionDetalle);
                    if (!(idDetalle > 0))
                    {
                        throw new Exception("No se pudo registrar el detalle de la devolución");
                    }
                    //Obtener la pertenencia por Id                    
                    var pertenenciaUpd = ControlPertenencia.ObtenerPorId(Convert.ToInt32(item.Id));
                    //Cambiarle el estatus                    
                    pertenenciaUpd.Estatus = 2;
                    //Actualizar
                    ControlPertenencia.Actualizar(pertenenciaUpd);
                    //actualizar casillero
                    var casillero = ControlCasillero.ObtenerPorId(pertenenciaUpd.CasilleroId);
                   
                    ControlCasillero.Actualizar(casillero);
                }

                var pertenencia = ControlPertenencia.ObtenerPorId(idPertenencia);
                var detalleDetencion = ControlDetalleDetencion.ObtenerPorId(pertenencia.InternoId);
                var interno = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
                var domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);

                string nombreInterno = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                
                string remisionId = detalleDetencion != null ? detalleDetencion.Expediente : "";
                string colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).Asentamiento : "" : "";
                string cp = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "" : "";
                string calle = (domicilio != null) ? (domicilio.Calle != null) ? domicilio.Calle : "" : "";
                string numero = (domicilio != null) ? (domicilio.Numero != null) ? domicilio.Numero : "" : "";
                string domicilioCompleto = "";
                if (domicilio !=null)
                { domicilioCompleto = "Calle: " + calle + " #" + numero + " Colonia:" + colonia + " C.P." + cp; }
                else
                {
                    domicilioCompleto = "Sin dato";
                }
                 
                string recibio = dataGenerales[2].ToString();
                string bolsa = data[0].Bolsa;

                var usuario = ControlUsuario.Obtener(usId);
                if (usuario.RolId == 13)
                {
                    usuario = ControlUsuario.Obtener(8);
                }
                string nombreEntregaA = usuario.User;

                var tipoEntrega = ControlCatalogo.Obtener(devolucion.Entrega, 100).Nombre;

                var pertenenciasDevolucion = ControlDevolucionDetallePertenencia.ObtenerByDevolucionId(IdDevolucion);
                List<Business.PertenenciaAux> listaPertenencias = new List<Business.PertenenciaAux>();
                foreach (var item in pertenenciasDevolucion)
                {
                    var pertenenciaFinal = ControlPertenencia.ObtenerPorId(item.PertenenciaId);
                    listaPertenencias.Add(new Business.PertenenciaAux()
                    {
                        Bolsa = pertenenciaFinal.Bolsa.ToString(),
                        Cantidad = pertenenciaFinal.Cantidad.ToString(),
                        Clasificacion = ControlClasificacionEvidencia.ObtenerPorId(pertenenciaFinal.Clasificacion).Nombre,
                        Estatus = pertenenciaFinal.Estatus.ToString(),
                        Id = pertenenciaFinal.Id.ToString(),
                        Nombre = pertenenciaFinal.PertenenciaNombre,
                        Observacion = pertenenciaFinal.Observacion,
                        Casillero = ControlCasillero.ObtenerPorId(pertenenciaFinal.CasilleroId).Nombre,
                        FechaEntrada = pertenenciaFinal.FechaEntrada.ToString()
                    });
                }

                //Generar PDF
                object[] obj = ControlPDF.GenerarReciboDevolucionesPDF(listaPertenencias, nombreInterno, remisionId, domicilioCompleto, recibio, devolucion.Condicion, tipoEntrega, bolsa, nombreEntregaA,  interno.Id);

                Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                reportesLog.ProcesoId = interno.Id;
                reportesLog.ProcesoTrackingId = interno.TrackingId.ToString();
                reportesLog.Modulo = "Control de pertenencias y evidencias";
                reportesLog.Reporte = "Recibo de devolución";
                reportesLog.Fechayhora = DateTime.Now;
                reportesLog.EstatusId = 1;
                reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                ControlReportesLog.Guardar(reportesLog);
                return JsonConvert.SerializeObject(new { success = true, ubicacionArchivo = obj[1].ToString(), errores });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message, errores });
            }
        }

        [WebMethod]
        public static string saveDonacion(List<Business.PertenenciaAux> data, string[] dataGenerales)
        {
            List<string> errores = new List<string>();
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                //Guardar la donacion
                Entity.DonacionPertenencia donacion = new Entity.DonacionPertenencia();
                donacion.Accion = (Convert.ToBoolean(dataGenerales[6])) ? "Donado" : "Consignado";
                donacion.Condicion = dataGenerales[3].ToString();
                donacion.CreadoPor = usId;
                donacion.Entrega = Convert.ToInt32(dataGenerales[4].ToString());
                donacion.FechaEntrega = DateTime.Now;
                donacion.InstitucionId = Convert.ToInt32(dataGenerales[5].ToString());
                donacion.TrackingId = Guid.NewGuid();
                donacion.UsuarioQueRegistro = dataGenerales[1].ToString();
                donacion.PersonaQueRecibe = dataGenerales[2].ToString();
                donacion.PertenenciasDe = dataGenerales[0].ToString();

                var detalleDetencionAux = new Entity.DetalleDetencion();
                var calificacionAux = new Entity.Calificacion();
                var pagoMultaAux = new Entity.CalificacionIngreso();
                var salidaEfectuadaAux = new Entity.SalidaEfectuada();
                var salidaEfectuadaJuezAux = new Entity.SalidaEfectuadaJuez();
                var trabajoSocialAux = new Entity.TrabajoSocial();
                if (data.Count > 0)
                {
                    var item = data[0];
                    var pertenenciasAux = ControlPertenencia.ObtenerPorId(Convert.ToInt32(item.Id));
                    detalleDetencionAux = pertenenciasAux != null ? ControlDetalleDetencion.ObtenerPorId(pertenenciasAux.InternoId) : null;
                    calificacionAux = detalleDetencionAux != null ? ControlCalificacion.ObtenerPorInternoId(detalleDetencionAux.Id) : null;
                    DateTime fechaSalida = new DateTime();
                    DateTime fechaSalidaAdd = new DateTime();
                    TimeSpan horasRestantes;
                    if (calificacionAux != null)
                    {
                        if (!calificacionAux.Activo)
                        {
                            pagoMultaAux = calificacionAux != null ? ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacionAux.Id) : null;

                            if (pagoMultaAux != null)
                            {
                                fechaSalida = detalleDetencionAux.Fecha.Value;
                                fechaSalidaAdd = fechaSalida.AddHours(calificacionAux.TotalHoras);
                                horasRestantes = DateTime.Now.Subtract(fechaSalidaAdd);
                                double tiempoRestante = horasRestantes.TotalMinutes;

                                if (tiempoRestante <= 0)
                                {
                                    errores.Add("El detenido tiene horas por cumplir, le faltan " + Math.Abs(horasRestantes.Hours) + " hora(s), " + Math.Abs(horasRestantes.Minutes) + " minuto(s), " + Math.Abs(horasRestantes.Seconds) + " segundo(s) para salir");
                                    throw new Exception("El detenido tiene horas por cumplir, le faltan " + Math.Abs(horasRestantes.Hours) + " hora(s), " + Math.Abs(horasRestantes.Minutes) + " minuto(s), " + Math.Abs(horasRestantes.Seconds) + " segundo(s) para salir");
                                }
                                else
                                {
                                    if (calificacionAux.TrabajoSocial)
                                    {
                                        salidaEfectuadaAux = ControlSalidaEfectuada.ObtenerByDetalledetencionId(detalleDetencionAux.Id);
                                        salidaEfectuadaJuezAux = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalleDetencionAux.Id);

                                        if (!(salidaEfectuadaAux != null))
                                        {
                                            if (!(salidaEfectuadaJuezAux != null))
                                            {
                                                errores.Add("No se ha realizado la salida efectuada al detenido");
                                                throw new Exception("No se ha realizado la salida efectuada al detenido");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        salidaEfectuadaAux = ControlSalidaEfectuada.ObtenerByDetalledetencionId(detalleDetencionAux.Id);
                                        salidaEfectuadaJuezAux = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalleDetencionAux.Id);

                                        if (!(salidaEfectuadaAux != null))
                                        {
                                            if (!(salidaEfectuadaJuezAux != null))
                                            {
                                                errores.Add("No se ha realizado la salida efectuada al detenido");
                                                throw new Exception("No se ha realizado la salida efectuada al detenido");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                errores.Add("No se ha pagado la multa del detenido");
                                throw new Exception("No se ha pagado la multa del detenido");
                            }
                        }
                        else
                        {
                            if (calificacionAux.TotalAPagar > 0)
                            {
                                pagoMultaAux = calificacionAux != null ? ControlCalificacionIngreso.ObtenerPorCalificacionId(calificacionAux.Id) : null;

                                if (!(pagoMultaAux != null))
                                {
                                    errores.Add("No se ha pagado la multa del detenido");
                                    throw new Exception("No se ha pagado la multa del detenido");
                                }
                            }
                            else
                            {
                                if (calificacionAux.TrabajoSocial)
                                {
                                    salidaEfectuadaAux = ControlSalidaEfectuada.ObtenerByDetalledetencionId(detalleDetencionAux.Id);
                                    salidaEfectuadaJuezAux = ControlSalidaEfectuadaJuez.ObtenerByDetalleDetencionId(detalleDetencionAux.Id);

                                    if (!(salidaEfectuadaAux != null))
                                    {
                                        if (!(salidaEfectuadaJuezAux != null))
                                        {
                                            errores.Add("No se ha realizado la salida efectuada al detenido");
                                            throw new Exception("No se ha realizado la salida efectuada al detenido");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        errores.Add("No se ha calificado al detenido");
                        throw new Exception("No se ha calificado al detenido");
                    }
                }

                //Obtener el Id de la donacion
                int IdDonacion = ControlDonacionPertenencia.Guardar(donacion);

                if (!(IdDonacion > 0))
                {
                    throw new Exception("No se pudo registrar la donación");
                }

                Entity.DonacionDetallePertenencia donacionDetalle = new Entity.DonacionDetallePertenencia();
                int idPertenencia = Convert.ToInt32(data[0].Id);

                foreach (var item in data)
                {
                    //Guardar los detalles de la donacion                                        
                    donacionDetalle.CreadoPor = usId;
                    donacionDetalle.DonacionId = IdDonacion;
                    donacionDetalle.PertenenciaId = Convert.ToInt32(item.Id);
                    donacionDetalle.TrackingId = Guid.NewGuid();                    

                    int idDetalle = ControlDonacionDetallePertenencia.Guardar(donacionDetalle);

                    if (!(idDetalle > 0))
                    {
                        throw new Exception("No se pudo registrar el detalle de la donación");
                    }

                    //Obtener la pertenencia por Id                    
                    var pertenenciaUpd = ControlPertenencia.ObtenerPorId(Convert.ToInt32(item.Id));
                    //Cambiarle el estatus                    
                    pertenenciaUpd.Estatus = (Convert.ToBoolean(dataGenerales[6])) ? 1 : 3;
                    //Actualizar
                    ControlPertenencia.Actualizar(pertenenciaUpd);
                }

                var pertenencia = ControlPertenencia.ObtenerPorId(idPertenencia);
                var detalleDetencion = ControlDetalleDetencion.ObtenerPorId(pertenencia.InternoId);
                var interno = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
                var domicilio = ControlDomicilio.ObtenerPorId(interno.DomicilioId);

                string nombreInterno = interno.Nombre + " " + interno.Paterno + " " + interno.Materno;
                //Por lo pronto se toma el Id el interno
                string remisionId = detalleDetencion != null ? detalleDetencion.Expediente : "";
                string colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).Asentamiento : "" : "";
                string cp = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "" : "";
                string calle = (domicilio != null) ? (domicilio.Calle != null) ? domicilio.Calle : "" : "";
                string numero = (domicilio != null) ? (domicilio.Numero != null) ? domicilio.Numero : "" : "";
                string domicilioCompleto = "Calle: " + calle + " #" + numero + " Colonia:" + colonia + " C.P." + cp;
                string recibio = dataGenerales[2].ToString();
                string bolsa = data[0].Bolsa;

                var usuario = ControlUsuario.Obtener(usId);
                if (usuario.RolId == 13)
                {
                    usuario = ControlUsuario.Obtener(8);
                }
                string nombreEntregaA = usuario.User;

                //Generar PDF
                object[] obj = ControlPDF.GenerarReciboDonacionesPDF(data, nombreInterno, remisionId, domicilioCompleto, recibio, bolsa, nombreEntregaA, interno.Id);

                Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                reportesLog.ProcesoId = interno.Id;
                reportesLog.ProcesoTrackingId = interno.TrackingId.ToString();
                reportesLog.Modulo = "Control de pertenencias y evidencias";
                reportesLog.Reporte = "Recibo de donación";
                reportesLog.Fechayhora = DateTime.Now;
                reportesLog.EstatusId = 1;
                reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                ControlReportesLog.Guardar(reportesLog);
                
                return JsonConvert.SerializeObject(new { success = true, ubicacionArchivo = obj[1].ToString(), errores });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message, errores });
            }
        }

        [WebMethod]
        public static List<Combo> getInstituciones()
        {            
            var combo = new List<Combo>();
            var instituciones = ControlInstitucion.ObteneTodos();

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            foreach (var item in instituciones)
            {
                if (item.Activo && item.Habilitado && item.ContratoId == contratoUsuario.IdContrato && item.Tipo == contratoUsuario.Tipo)
                {
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getContracts()
        {
            var combo = new List<Combo>();
            var c = ControlContrato.ObtenerTodos();

            foreach (var item in c)
            {
                combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getClasificacion()
        {
            var combo = new List<Combo>();            
            var c = ControlCatalogo.ObtenerHabilitados(85);

            foreach (var item in c)
            {
                if(item.Activo && item.Habilitado)
                {
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
                }                
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCasilleros()
        {
            var combo = new List<Combo>();
            var c = ControlCasillero.ObtenerTodos();

            var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

            foreach (var item in c)
            {
                if (item.Activo && item.ContratoId == contratoUsuarioK.IdContrato && item.Disponible > 0)
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getCasillerosAll()
        {
            var combo = new List<Combo>();
            var c = ControlCasillero.ObtenerTodos();

            var contratoUsuarioK = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));

            foreach (var item in c)
            {
                if (item.Activo && item.ContratoId == contratoUsuarioK.IdContrato )
                    combo.Add(new Combo { Desc = item.Nombre, Id = item.Id.ToString() });
            }

            return combo;
        }

        //[WebMethod]
        //public static string savePertenencia(string[] datos)
        //{
        //    try
        //     {
        //        int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
        //        string[] dataInterno = new string[]
        //        {
        //            datos[0].ToString(),
        //            true.ToString()
        //        };
        //        Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(dataInterno);

        //        Entity.Pertenencia pertenencia = new Entity.Pertenencia();
        //        pertenencia.Bolsa = Convert.ToInt32(datos[4]);
        //        pertenencia.Cantidad = Convert.ToInt32(datos[5]);


        //        if (pertenencia.Bolsa < 0)
        //        {
        //            throw new Exception(string.Concat("No se permite un valor negativo para bolsa."));
        //        }

        //        if(pertenencia.Cantidad < 0)
        //        {
        //            throw new Exception(string.Concat("No se permite un valor negativo para cantidad."));
        //        }

        //        if(detalleDetencion != null)
        //        {
        //            var casilleroNuevo = ControlCasillero.ObtenerPorId(Convert.ToInt32(datos[7]));
        //            if (pertenencia.Bolsa > casilleroNuevo.Disponible)
        //            {
        //                throw new Exception(string.Concat("No hay espacio suficiente en el casillero para la cantidad de bolsas."));
        //            }

        //            pertenencia.Clasificacion = Convert.ToInt32(datos[3]);
        //            pertenencia.CreadoPor = usId;
        //            pertenencia.Estatus = 4;
        //            pertenencia.InternoId = detalleDetencion.Id;
        //            pertenencia.Observacion = datos[2].ToString();
        //            pertenencia.PertenenciaNombre = datos[1].ToString();
        //            pertenencia.TrackingId = Guid.NewGuid();
        //            pertenencia.Fotografia = datos[6].ToString();
        //            pertenencia.CasilleroId = Convert.ToInt32(datos[7]);
        //            int idPertenencia = ControlPertenencia.Guardar(pertenencia);

        //            casilleroNuevo.Disponible = casilleroNuevo.Disponible - pertenencia.Bolsa;
        //            ControlCasillero.Actualizar(casilleroNuevo);

        //            if (!(idPertenencia > 0))
        //            {
        //                throw new Exception("No se pudo registrar la pertenencia");
        //            }
        //        }
        //        else
        //        {
        //            throw new Exception(string.Concat("Hubo un error al cargar los datos del interno."));
        //        }


        //        return JsonConvert.SerializeObject(new { success = true, message = "guardo" });
        //    }
        //    catch (Exception ex)
        //    {
        //        return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
        //    }
        //}

        [WebMethod]
        public static string savePertenencia(List<PertenenciaData> pertenencias, string interno)
        {
            try
            {
                if (ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de pertenencias/evidencias" }).Registrar || ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(new String[] { Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString(), "Control de pertenencias/evidencias" }).Modificar)
                {
                    var mode = string.Empty;
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    if (usuario.RolId == 13)
                    {
                        usuario = ControlUsuario.Obtener(8);
                    }
                    string[] internoestatus = { interno, true.ToString() };
                    Entity.DetalleDetencion detalleDetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(internoestatus);
                    List<string> errores = new List<string>();
                    var d = 0;
                    var e = 0;

                    List<Business.PertenenciaAux> dataReporte = new List<Business.PertenenciaAux>();
                    foreach (var item in pertenencias)
                    {
                        Entity.Pertenencia pertenencia = new Entity.Pertenencia();
                        pertenencia.Bolsa = Convert.ToInt32(1);
                        pertenencia.Cantidad = Convert.ToInt32(item.Cantidad);

                        if (pertenencia.Bolsa < 0)
                        {
                            throw new Exception(string.Concat("No se permite un valor negativo para bolsa."));
                        }

                        if (pertenencia.Cantidad < 0)
                        {
                            throw new Exception(string.Concat("No se permite un valor negativo para cantidad."));
                        }

                        pertenencia.Clasificacion = Convert.ToInt32(item.Clasificacion);
                        pertenencia.CreadoPor = usuario.Id;
                        pertenencia.Estatus = 4;
                        pertenencia.Fotografia = item.Fotografia;
                        if (string.IsNullOrEmpty(pertenencia.Fotografia))
                        {
                            pertenencia.Fotografia = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/object.png");

                        }
                        pertenencia.InternoId = detalleDetencion.Id;
                        pertenencia.Observacion = item.Observacion;
                        pertenencia.PertenenciaNombre = item.Pertenencia;
                        pertenencia.CasilleroId = Convert.ToInt32(item.Casillero);

                        var casilleroNuevo = ControlCasillero.ObtenerPorId(Convert.ToInt32(item.Casillero));

                        if (!string.IsNullOrEmpty(item.Id))
                        {
                            var p = ControlPertenencia.ObtenerPorId(Convert.ToInt32(item.Id));
                            Entity.Casillero c = null;
                            if (p.CasilleroId != 0)
                            {
                                c = ControlCasillero.ObtenerPorId(p.CasilleroId);
                                c.Disponible = c.Disponible + p.Bolsa;
                            }

                            if (Convert.ToInt32(item.Casillero) != p.CasilleroId)
                            {
                                if (pertenencia.Bolsa > casilleroNuevo.Disponible)
                                {
                                    if (casilleroNuevo.Disponible < 1)
                                    {
                                        e = casilleroNuevo.Disponible + d;
                                    }
                                    throw new Exception(string.Concat("El casillero "+casilleroNuevo.Nombre+" solo tiene disponible "+e+" bolsas, por favor administre en otros casilleros el resto de las pertenencias"));
                                }
                            }
                            else
                            {
                                if (pertenencia.Bolsa > c.Disponible)
                                {
                                    if (c.Disponible < 1)
                                    {
                                        e = c.Disponible + d;
                                    }
                                    throw new Exception(string.Concat("El casillero "+c.Nombre+" solo tiene disponible "+d+"  bolsas, por favor administre en otros casilleros el resto de las pertenencias"));
                                }
                            }

                            mode = "actualizó";
                            pertenencia.Habilitado = true;
                            pertenencia.Activo = true;
                            pertenencia.Id = Convert.ToInt32(item.Id);
                            pertenencia.TrackingId = new Guid(item.TrackingId);
                            ControlPertenencia.Actualizar(pertenencia);

                            pertenencia = ControlPertenencia.ObtenerPorId(pertenencia.Id);
                            dataReporte.Add(new Business.PertenenciaAux()
                            {
                                Id = pertenencia.Id.ToString(),
                                Nombre = pertenencia.PertenenciaNombre,
                                FechaEntrada = DateTime.Now.ToString(),
                                Bolsa = pertenencia.Bolsa.ToString(),
                                Cantidad = pertenencia.Cantidad.ToString(),
                                Casillero = ControlCasillero.ObtenerPorId(pertenencia.CasilleroId).Nombre,
                                Clasificacion = ControlCatalogo.Obtener(pertenencia.Clasificacion, 85).Nombre,
                                Estatus = pertenencia.Estatus.ToString(),
                                Observacion = pertenencia.Observacion
                            });

                            if (c != null)
                                ControlCasillero.Actualizar(c);

                            var casilleroNuevo2 = ControlCasillero.ObtenerPorId(Convert.ToInt32(item.Casillero));
                            casilleroNuevo2.Disponible = casilleroNuevo2.Disponible - pertenencia.Bolsa;
                            ControlCasillero.Actualizar(casilleroNuevo2);
                        }
                        else
                        {
                            mode = "registró";
                            if (pertenencia.Bolsa > casilleroNuevo.Disponible)
                            {
                                if (casilleroNuevo.Disponible < 1)
                                {
                                    e = casilleroNuevo.Disponible + d;
                                }
                                throw new Exception(string.Concat("La pertenencia "+pertenencia.PertenenciaNombre +" no se guardo, el casillero "+casilleroNuevo.Nombre+" solo tiene disponible "+d+" bolsas, por favor administre en otros casilleros el resto de las pertenencias"));
                            }

                            pertenencia.Habilitado = true;
                            pertenencia.Activo = true;
                            pertenencia.TrackingId = Guid.NewGuid();
                            pertenencia.Id = ControlPertenencia.Guardar(pertenencia);

                            pertenencia = ControlPertenencia.ObtenerPorId(pertenencia.Id);
                            dataReporte.Add(new Business.PertenenciaAux()
                            {
                                Id = pertenencia.Id.ToString(),
                                Nombre = pertenencia.PertenenciaNombre,
                                FechaEntrada = DateTime.Now.ToString(),
                                Bolsa = pertenencia.Bolsa.ToString(),
                                Cantidad = pertenencia.Cantidad.ToString(),
                                Casillero = ControlCasillero.ObtenerPorId(pertenencia.CasilleroId).Nombre,
                                Clasificacion = ControlCatalogo.Obtener(pertenencia.Clasificacion, 85).Nombre,
                                Estatus = pertenencia.Estatus.ToString(),
                                Observacion = pertenencia.Observacion
                            });

                            if (pertenencia.Id <= 0)
                            {
                                errores.Add("Hubo un error al intentar registrar la pertenencia con el nombre de " + pertenencia.PertenenciaNombre + ", ");
                            }
                            d = d + 1;
                            casilleroNuevo.Disponible = casilleroNuevo.Disponible - 1;
                            ControlCasillero.Actualizar(casilleroNuevo);
                        }
                    }

                    Entity.Historial historial = new Entity.Historial();
                    historial.Activo = true;
                    historial.CreadoPor = usuario.Id;
                    historial.Fecha = DateTime.Now;
                    historial.Habilitado = true;
                    historial.InternoId = detalleDetencion.DetenidoId;
                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                    var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    historial.ContratoId = subcontrato.Id;
                    historial.Movimiento = "Modificación de datos generales en control de pertenencias";
                    historial.TrackingId = Guid.NewGuid();
                    historial.Id = ControlHistorial.Guardar(historial);


                    var detenido = ControlDetenido.ObtenerPorId(detalleDetencion.DetenidoId);
                    var domicilio = ControlDomicilio.ObtenerPorId(detenido.DomicilioId);

                    string nombreInterno = detenido.Nombre + " " + detenido.Paterno + " " + detenido.Materno;
                    string colonia = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).Asentamiento : "" : "";
                    string cp = (domicilio != null) ? (domicilio.ColoniaId != null) ? ControlColonia.ObtenerPorId(Convert.ToInt32(domicilio.ColoniaId)).CodigoPostal : "" : "";
                    string calle = (domicilio != null) ? (domicilio.Calle != null) ? domicilio.Calle : "" : "";
                    string numero = (domicilio != null) ? (domicilio.Numero != null) ? domicilio.Numero : "" : "";
                    string domicilioCompleto = "Calle: " + calle + " #" + numero + " Colonia:" + colonia + " C.P." + cp;
                    string bolsa = pertenencias[0].Bolsa;
                                        
                    string recibio = usuario.User;
                    
                    //Generar PDF
                    object[] obj = ControlPDF.GenerarReciboDevolucionesPDF(dataReporte, nombreInterno, detalleDetencion.Expediente, domicilioCompleto, recibio, "", "", bolsa, nombreInterno, detenido.Id);
                    Entity.ReportesLog reportesLog = new Entity.ReportesLog();
                    reportesLog.ProcesoId = detenido.Id;
                    reportesLog.ProcesoTrackingId = detenido.TrackingId.ToString();
                    reportesLog.Modulo = "Control de pertenencias y evidencias";
                    reportesLog.Reporte = "Recibo de pertenencias";
                    reportesLog.Fechayhora = DateTime.Now;
                    reportesLog.EstatusId = 1;
                    reportesLog.CreadoPor = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    ControlReportesLog.Guardar(reportesLog);
                    return JsonConvert.SerializeObject(new { exitoso = true, mensaje = mode, errores = errores, ubicacionArchivo = obj[1].ToString() });
                }
                else
                {
                    return JsonConvert.SerializeObject(new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." });
                }
            }
            catch (Exception ex)
            {
                
                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }
    }

    public class PertenenciaAux
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Bolsa { get; set; }
        public string Cantidad { get; set; }
        public string Clasificacion { get; set; }
        public string Estatus { get; set; }
        public string Observacion { get; set; }
    }
}