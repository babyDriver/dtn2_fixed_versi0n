﻿using System;
using System.Configuration;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.CodeDom;

namespace Web.Application.Handlers
{
    /// <summary>
    /// Summary description for FileUploadHandler
    /// </summary>
    public class FileUploadHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string id = context.Request.Form["id"];
                string accion = context.Request.QueryString["action"];
                string nombreAnterior = context.Request.QueryString["before"];

                if (context.Request.Files.Count > 0)
                {
                    string rutaAlmacenada = string.Empty;
                    string rutaDescarga = string.Empty;
                    string rutaDescargaAux = string.Empty;


                    if (accion == "2")
                    {
                        //rutaDescarga = String.Concat(ConfigurationManager.AppSettings["relativepath"],"Content/img/interno");
                        rutaDescarga = String.Concat(ConfigurationManager.AppSettings["relativepath"],"Content/biometrias");
                        rutaDescargaAux = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/biometrias");
                        id = string.Concat(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, "_", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    }
                    else if(accion == "3")
                    {
                        rutaDescarga = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/logotipoCliente");
                        rutaDescargaAux = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/logotipoCliente/");
                        id = string.Concat(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, "_", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    }
                    else if(accion == "4")
                    {
                        rutaDescarga = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/imagenPertenencia");
                        rutaDescargaAux = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/imagenPertenencia/");
                        id = string.Concat(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, "_", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    }
                    else if (accion == "5")
                    {
                        rutaDescarga = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/imgcarta");
                        rutaDescargaAux = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/imgcarta/");
                        id = string.Concat(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, "_", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    }
                    else if (accion == "6")
                    {
                        string certificadoLesionFotografia = context.Request.QueryString["certificadoFotografia"];
                        string trackingDetencion = context.Request.QueryString["detencion"];
                        var certificadoFotografia = Business.ControlCertificadoLesionFotografias.ObtenerPorId(int.Parse(certificadoLesionFotografia));
                        var detencion = Business.ControlDetalleDetencion.ObtenerPorTrackingId(new Guid(trackingDetencion));

                        rutaDescarga = string.Concat(ConfigurationManager.AppSettings["relativepath"], $"Content/img/detenido/{detencion.ContratoId}/{detencion.DetenidoId}/Lesiones/{certificadoFotografia.Id}");
                        rutaDescargaAux = string.Concat(ConfigurationManager.AppSettings["relativepath"], $"Content/img/detenido/{detencion.ContratoId}/{detencion.DetenidoId}/Lesiones/{certificadoFotografia.Id}/");
                        id = string.Concat(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, "_", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    }
                    else
                    {
                        rutaDescarga = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars");
                        rutaDescargaAux = String.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/");
                        id = string.Concat(DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, "_", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    }


                    HttpFileCollection files = context.Request.Files;

                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];
                        
                        var ruta = context.Server.MapPath(rutaDescarga);
                        var renamedFile = string.Empty;
                        var fileName = file.FileName;
                        var path = string.Concat(ruta, "\\");
                        var index = fileName.LastIndexOf(".");
                        var len = fileName.Length - index;
                        var extension = fileName.Substring(index, len);


                        renamedFile = string.Concat(id, extension);
                        renamedFile = renamedFile.Replace(" ", "");
                        
                        rutaAlmacenada = string.Concat(rutaDescargaAux, renamedFile);

                        string fname = string.Concat(path, renamedFile);

                        if(!Directory.Exists(ruta))
                        {
                            Directory.CreateDirectory(ruta);
                        }

                        file.SaveAs(fname);

                        CreaThumb(fname);

                        if(accion != "4")
                        {
                            EliminaArchivo(nombreAnterior, context.Server.MapPath(nombreAnterior));
                        }                        

                        context.Response.ContentType = "application/json; charset=utf-8";
                        context.Response.Write(JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", nombreArchivo = rutaAlmacenada }));
                    }
                }
                else
                {
                    context.Response.Write(JsonConvert.SerializeObject(new { exitoso = true, mensaje = "", nombreArchivo = nombreAnterior }));
                }
            }
            catch (Exception e)
            {
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write(JsonConvert.SerializeObject(new { exitoso = false, mensaje = e.Message, nombreArchivo = "" }));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private void EliminaArchivo(string nombre, string path)
        {
            try
            {
                if (string.IsNullOrEmpty(nombre))
                    return;

                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                string pathTumb = path.Replace(".jpg", ".thumb").Replace(".jpeg", ".thumb").Replace(".JPG", ".thumb").Replace(".JPEG", ".thumb");

                if (File.Exists(pathTumb))
                {
                    File.Delete(pathTumb);
                }
            }
            catch
            {
                // si no se puede eliminar, ignorar
                return;
            }
        }

        private bool CreaThumb(string filepath)
        {
            using (System.Drawing.Image image = System.Drawing.Image.FromFile(filepath))
            {
                //url file output
                string output = string.Empty;
                // Load image.
                // Compute thumbnail size.
                Size thumbnailSize = GetThumbnailSize(image);

                // Get thumbnail.
                using (System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero))
                {
                    try
                    {
                        string extension = string.Empty;
                        extension = Path.GetExtension(filepath);

                        // Save thumbnail.
                        if (extension == ".JPG" || extension == ".jpg" || extension == ".JPEG" || extension == ".jpeg")
                        {
                            output = filepath.Replace(".jpg", ".thumb").Replace(".jpeg", ".thumb").Replace(".JPG", ".thumb").Replace(".JPEG", ".thumb");
                            thumbnail.Save(output);
                        }
                        else if (extension == ".PNG" || extension == ".png")
                        {
                            output = filepath.Replace(".png", ".thumb").Replace(".PNG", ".thumb");
                            thumbnail.Save(output, ImageFormat.Png);
                        }
                        else if (extension == ".GIF" || extension == "-gif")
                        {
                            output = filepath.Replace(".GIF", ".thumb").Replace(".gif", ".thumb");
                            thumbnail.Save(output, ImageFormat.Gif);
                        }

                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                    finally
                    {
                        image.Dispose();
                        thumbnail.Dispose();
                    }
                }
               
            }
        }

        private Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 40;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }

    }
}