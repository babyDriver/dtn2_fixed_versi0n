﻿using Business;
using DT;
using Entity;
using Entity.Util;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Application.Reportes_estadisticos
{
    public partial class reporteestadistico : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            verifyPermissionReportesEstadisticos();
            //Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
        }

        public void verifyPermissionReportesEstadisticos()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            List<int> reportesEstadisticosId = new List<int>()
            {
                24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40
            };

            
            var permisos = Business.ControlUsuarioReportes.ObtenerPorUsuarioId(usId).Where(x=>x.Activo && x.Habilitado);
            var permitido = false;
            string[] parametros = { usId.ToString(), "Reportes estadísticos" };
            var permiso = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            foreach (var item in permisos)
            {
                if (reportesEstadisticosId.Contains(item.ReporteId) )
                {
                    permitido = true;
                }
            }
            if(permiso!=null)
            {
                permitido = true;
            }
            if (!permitido)
            {
                Response.Redirect(string.Concat(ConfigurationManager.AppSettings["relativepath"], "error-403.aspx"));
            }
        }

        public static void validatelogin()
        {
            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

            string ip = "";
            ip = HttpContext.Current.Session["ipusuario"].ToString();
            if (ip != userlogin.IPequipo)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
                System.Web.HttpContext.Current.Session.RemoveAll();
                return;
            }
        }

        [WebMethod]
        public static List<Combo> getProcesos()
        {
            var combo = new List<Combo>();
            var c = ControlProceso.GetAll().Where(x => x.Activo == 1);


            foreach (var i in c)
            {
                combo.Add(new Combo { Desc = i.proceso, Id = i.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetSubprocesos(string procesoId)
        {
            var combo = new List<Combo>();

            var subprocesos = ControlSubProceso.GetAll();
            if (Convert.ToInt32(procesoId) != 0)
            {
                subprocesos = subprocesos.Where(x => x.ProcesoId == Convert.ToInt32(procesoId)).ToList();
            }
            
            foreach (var subProceso in subprocesos)
            {
                combo.Add(new Combo { Desc = subProceso.Subproceso, Id = subProceso.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getCustomers()
        {
            var combo = new List<Combo>();
            var c = ControlCliente.ObtenerTodos().Where(x => x.Habilitado);

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

            var clientes = ControlContrato.ObetenerPorUsuarioId(usId).Select(x => x.ClienteId).Distinct();

            foreach (var i in clientes)
            {
                var cliente = c.FirstOrDefault(x => x.Id == i);
                if (cliente != null) combo.Add(new Combo { Desc = cliente.Nombre, Id = cliente.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetContracts(string Clienteid)
        {
            var combo = new List<Combo>();

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var contratos = ControlContrato.ObetenerPorUsuarioId(usId).Where(x => x.ClienteId == int.Parse(Clienteid) && x.Habilitado && x.Activo).OrderBy(x=>x.Id);
            var contratoid = 0;
            foreach (var _contrato in contratos)
            {
                if(_contrato.Id!=contratoid)
                {
                    combo.Add(new Combo { Desc = _contrato.Nombre, Id = _contrato.Id.ToString() });
                }
                contratoid = _contrato.Id;
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetTipoSalida_Combo(string item)
        {
            var combo = new List<Combo>();
            bool activo = true;
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(Convert.ToInt32(Entity.TipoDeCatalogo.salida_tipo));

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(c => c.Habilitado))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetSubcontracts(string Contratoid)
        {
            var combo = new List<Combo>();

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var sub = ControlSubcontrato.ObtenerByUsuarioId(usId);

            if (!string.IsNullOrWhiteSpace(Contratoid))
            {
                foreach (var _subcontrato in sub.Where(x => x.ContratoId == Convert.ToInt32(Contratoid) && x.Habilitado))
                {
                    combo.Add(new Combo { Desc = _subcontrato.Nombre, Id = _subcontrato.Id.ToString() });
                }
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getNeighborhoods(filtrosreportesestadisticos filtro)
        {
            List<Combo> combo = new List<Combo>();
            List<Entity.Colonia> colonias = new List<Entity.Colonia>();
            List<int> municipiosId = new List<int>();
            if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
            if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
            if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

            if (filtro.clienteid == "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(usId);
                if (usuario != null)
                {
                    var subcontratos = ControlSubcontrato.ObtenerByUsuarioId(usuario.Id);

                    var institucionesId = subcontratos.Select(x => x.InstitucionId);

                    foreach (var id in institucionesId)
                    {
                        var institucion = ControlInstitucion.ObtenerPorId(id);
                        var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
                        municipiosId.Add(Convert.ToInt32(domicilio.MunicipioId));
                    }
                    colonias = ControlColonia.ObtenerTodas().Where(x => municipiosId.Contains(x.IdMunicipio) && x.Habilitado).ToList();
                }
            }

            if (filtro.clienteid != "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
            {
                var contratosId = ControlContrato.ObtenerTodos().Where(x => x.ClienteId == Convert.ToInt32(filtro.clienteid) && x.Habilitado).Select(x => x.Id);
                var institucionesId = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId) && x.Habilitado).Select(x => x.InstitucionId);

                foreach (var id in institucionesId)
                {
                    var institucion = ControlInstitucion.ObtenerPorId(id);
                    var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
                    municipiosId.Add(Convert.ToInt32(domicilio.MunicipioId));
                }
                colonias = ControlColonia.ObtenerTodas().Where(x => municipiosId.Contains(x.IdMunicipio) && x.Habilitado).ToList();
            }

            if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid == "0")
            {
                var institucionesId = ControlSubcontrato.ObtenerTodos().Where(x => x.ContratoId == Convert.ToInt32(filtro.contratoid) && x.Habilitado).Select(x => x.InstitucionId);

                foreach (var id in institucionesId)
                {
                    var institucion = ControlInstitucion.ObtenerPorId(id);
                    var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
                    municipiosId.Add(Convert.ToInt32(domicilio.MunicipioId));
                }

                colonias = ControlColonia.ObtenerTodas().Where(x => municipiosId.Contains(x.IdMunicipio) && x.Habilitado).ToList();
            }

            if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid != "0")
            {
                var subcontrato = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtro.subcontratoid));
                var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

                colonias = ControlColonia.ObtenerTodas().Where(x => x.IdMunicipio == domicilio.MunicipioId && x.Habilitado).ToList();
            }

            if (colonias.Count > 0)
            {
                foreach (var rol in colonias)
                    combo.Add(new Combo { Desc = rol.Asentamiento, Id = rol.Id.ToString() });
            }

            return combo;
        }

        [WebMethod]
        public static object Detenidosportipodelesion(filtrosreportesestadisticos filtro)
        {
            try
            {
                if (!Validarangofechas(filtro))
                {
                    return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                }
                object[] data;

                if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                {
                    if (filtro.contratoid == null || filtro.contratoid == "0")
                    {
                        if (filtro.clienteid == null || filtro.clienteid == "0")
                        {
                            data = new object[]
                            {
                                "No especificado",
                                "No especificado"
                            };
                        }
                        else
                        {
                            var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                            foreach (var contrato in contratos)
                            {
                                var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                            }

                            data = new object[]
                            {
                                "No especificado",
                                "No especificado"
                            };
                        }
                    }
                    else
                    {
                        var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                            municipio,
                            institucionSubcontrato.Nombre
                        };
                    }
                }
                else
                {
                    var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                    var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                    var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                    var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                    var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                    var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                    var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                    var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                    var municipio = "No especificado";
                    if (municipioContrato != null)
                    {
                        municipio = municipioContrato.Nombre;
                    }
                    else if (muni != null)
                    {
                        municipio = muni.Nombre;
                    }
                    data = new object[]
                    {
                        municipio,
                        institucionSubcontrato.Nombre
                    };
                }
                var cliente = "0";
                if (!string.IsNullOrEmpty(filtro.clienteid)) cliente = filtro.clienteid;
                string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.motivoid ?? "0", filtro.subcontratoid ?? "0", filtro.LesionId ?? "0",cliente };

                var detenidos = ControlDetenidosPorTipoLesion.ObtenerDetenidosPorTipoLesion(filtros);

                object[] dataArchivo = null;

                if (filtro.clienteid == "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    if (usuario != null)
                    {
                        var clientes = ControlContrato.ObetenerPorUsuarioId(usuario.Id).Select(x => x.ClienteId).Distinct();
                        var contratosId = ControlContrato.ObtenerTodos().Where(x => clientes.Contains(x.ClienteId) && x.Habilitado).Select(x => x.Id);
                        var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                        var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                        detenidos = detenidos.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                    }
                }

                if (filtro.clienteid != "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    if (usuario != null)
                    {
                        var contratosId = ControlContrato.ObtenerTodos().Where(x => x.ClienteId == Convert.ToInt32(filtro.clienteid) && x.Habilitado).Select(x => x.Id);
                        var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                        var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                        detenidos = detenidos.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                    }
                }

                if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid == "0")
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    if (usuario != null)
                    {
                        var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => x.ContratoId == int.Parse(filtro.contratoid)).Select(x => x.Id);
                        var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                        detenidos = detenidos.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                    }
                }

                if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid != "0")
                {
                    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var usuario = ControlUsuario.Obtener(usId);
                    if (usuario != null)
                    {
                        var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => x.IdContrato == int.Parse(filtro.subcontratoid) && x.Activo).Select(x => x.IdContrato);
                        detenidos = detenidos.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                    }
                }


                DateTime fechaInicial = DateTime.Parse(filtro.Fechainical);
                DateTime fechaFinal = DateTime.Parse(filtro.Fechafinal);

                if (filtro.UtilizarHora) detenidos = detenidos.Where(x => x.Fecha >= fechaInicial && x.Fecha <= fechaFinal.AddMinutes(1).AddSeconds(-1)).ToList();
                else detenidos = detenidos.Where(x => x.Fecha.Date >= fechaInicial.Date && x.Fecha.Date <= fechaFinal.Date).ToList();


                dataArchivo = ControlPDFReportesEstadisticos.ReporteTituloDetenidosporLesion(detenidos,filtros,data);

                return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object DetenidosPorLesionCL(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 34 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var clienteid = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) clienteid = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), !string.IsNullOrWhiteSpace(filtro.LesionId) ? filtro.LesionId : "0", !string.IsNullOrWhiteSpace(filtro.subcontratoid) ? filtro.subcontratoid : "0", !string.IsNullOrWhiteSpace(filtro.CorporacionId) ? filtro.CorporacionId : "0", !string.IsNullOrWhiteSpace(filtro.UnidadId) ? filtro.UnidadId : "0", filtro.UtilizarHora.ToString(),clienteid };

                    //var comparativo = ControlComparativoDeLesionadosPorUnidad.ObtenerComparativoDeLesionadosPorUnidad(filtros);

                    var lesionesdetenido = ControlReporteEstadisticoPorLesion.GetPorLesions(filtros);

                    Boolean mayores = Convert.ToBoolean(filtro.MayoresEdad);
                    Boolean menores = Convert.ToBoolean(filtro.MenoresEdad);
                    if (mayores && !menores)
                    {
                        lesionesdetenido = lesionesdetenido.Where(x => x.Edad >= 18).ToList();
                    }
                    else if (!mayores && menores)
                    {
                        lesionesdetenido = lesionesdetenido.Where(x => x.Edad < 18).ToList();
                    }

                    filtro.LesionId = (!string.IsNullOrEmpty(filtro.LesionId)) ? filtro.LesionId : "0";

                    //if (filtro.LesionId != "0")
                    //{
                    //    comparativo = comparativo.Where(x => x.TipolesionId == Convert.ToInt32(filtro.LesionId)).ToList();

                    //}
                    var fechaInicial = new DateTime();
                    var fechaFinal = new DateTime();
                    fechaInicial = Convert.ToDateTime(filtro.Fechainical);
                    fechaFinal = Convert.ToDateTime(filtro.Fechafinal);

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDetenidosPorlesion(lesionesdetenido, data, fechaInicial, fechaFinal, filtro.UtilizarHora, filtros);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object ComparativodeLesionadosporunidadCL(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 32 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var cliente1 = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid))
                        cliente1 = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), !string.IsNullOrWhiteSpace(filtro.LesionId) ? filtro.LesionId : "0", !string.IsNullOrWhiteSpace(filtro.subcontratoid) ? filtro.subcontratoid : "0", !string.IsNullOrWhiteSpace(filtro.CorporacionId) ? filtro.CorporacionId : "0", !string.IsNullOrWhiteSpace(filtro.UnidadId) ? filtro.UnidadId : "0", filtro.UtilizarHora.ToString(),cliente1 };

                    //var comparativo = ControlComparativoDeLesionadosPorUnidad.ObtenerComparativoDeLesionadosPorUnidad(filtros);

                    var lesionadosporunidad = ControlReporteEstadisticoDetenidoPorTipoLesionCL.GetByFilter(filtros);

                    Boolean mayores = Convert.ToBoolean(filtro.MayoresEdad);
                    Boolean menores = Convert.ToBoolean(filtro.MenoresEdad);
                    if (mayores && !menores)
                    {
                        lesionadosporunidad = lesionadosporunidad.Where(x => x.Edad >= 18).ToList();
                    }
                    else if (!mayores && menores)
                    {
                        lesionadosporunidad = lesionadosporunidad.Where(x => x.Edad < 18).ToList();

                    }

                    filtro.LesionId = (!string.IsNullOrEmpty(filtro.LesionId)) ? filtro.LesionId : "0";

                    //if (filtro.LesionId != "0")
                    //{
                    //    comparativo = comparativo.Where(x => x.TipolesionId == Convert.ToInt32(filtro.LesionId)).ToList();

                    //}
                    var fechaInicial = new DateTime();
                    var fechaFinal = new DateTime();
                    fechaInicial = Convert.ToDateTime(filtro.Fechainical);
                    fechaFinal = Convert.ToDateTime(filtro.Fechafinal);

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteComparativoLesionadosCL(lesionadosporunidad, data, fechaInicial, fechaFinal, filtro.UtilizarHora, filtros);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object ComparativodeLesionadosporunidad(filtrosreportesestadisticos filtro)
        {
            try
            {
                if (!Validarangofechas(filtro))
                {
                    return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                }

                object[] data;

                if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                {
                    if (filtro.contratoid == null || filtro.contratoid == "0")
                    {
                        if (filtro.clienteid == null || filtro.clienteid == "0")
                        {
                            data = new object[]
                            {
                                "No especificado",
                                "No especificado"
                            };
                        }
                        else
                        {
                            var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                            foreach (var contrato in contratos)
                            {
                                var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                            }

                            data = new object[]
                            {
                                "No especificado",
                                "No especificado"
                            };
                        }
                    }
                    else
                    {
                        var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                        var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                        data = new object[]
                        {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                        };
                    }
                }
                else
                {
                    var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                    var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                    var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                    var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                    var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                    var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                    
                    var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                    var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                    var municipio = "No especificado";
                    if (municipioContrato != null)
                    {
                        municipio = municipioContrato.Nombre;
                    }
                    else if (muni != null)
                    {
                        municipio = muni.Nombre;
                    }
                    data = new object[]
                    {
                        municipio,
                        institucionSubcontrato.Nombre
                    };
                }

                string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), !string.IsNullOrWhiteSpace(filtro.motivoid) ? filtro.motivoid : "0", !string.IsNullOrWhiteSpace(filtro.subcontratoid) ? filtro.subcontratoid : "0", !string.IsNullOrWhiteSpace(filtro.CorporacionId) ? filtro.CorporacionId : "0", !string.IsNullOrWhiteSpace(filtro.UnidadId) ? filtro.UnidadId : "0",!string.IsNullOrEmpty(filtro.clienteid)? filtro.clienteid:"0" };

                var comparativo = ControlComparativoDeLesionadosPorUnidad.ObtenerComparativoDeLesionadosPorUnidad(filtros);

                filtro.LesionId = (!string.IsNullOrEmpty(filtro.LesionId)) ? filtro.LesionId : "0";


                if (filtro.LesionId != "0")
                {
                    comparativo = comparativo.Where(x => x.TipolesionId == Convert.ToInt32(filtro.LesionId)).ToList();

                }
                var fechaInicial = new DateTime();
                var fechaFinal = new DateTime();
                fechaInicial = Convert.ToDateTime(filtro.Fechainical);
                fechaFinal = Convert.ToDateTime(filtro.Fechafinal);

                object[] dataArchivo = null;
                dataArchivo = ControlPDFReportesEstadisticos.ReporteComparativoLesionados(comparativo, data, fechaInicial, fechaFinal, filtro.UtilizarHora,filtros);

                return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static List<Combo> GetResultadosQuimicos()
        {
            var combo = new List<Combo>();
            var tiporeporte = ControlResultadosQuimicos.ObtenerResultadosQuimicos();

            foreach (var _tiporeporte in tiporeporte.Where(x => x.Activo == 1))
            {
                combo.Add(new Combo { Desc = _tiporeporte.Resultado, Id = _tiporeporte.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetReporttype()
        {
            var combo = new List<Combo>();

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var accesoReportes = Business.ControlUsuarioReportes.ObtenerPorUsuarioId(usId).Where(x => x.Activo && x.Habilitado).Select(x => x.ReporteId).Distinct().ToList();
            List<int> pantallasId = new List<int>();
            foreach (var idReporte in accesoReportes)
            {
                var reporte = ControlReportes.ObtenerPorId(idReporte);
                if(reporte != null)
                {
                    if (!pantallasId.Contains(reporte.PantallaId)) pantallasId.Add(reporte.PantallaId);
                }
            }
            string[] parametros = { usId.ToString(), "Reportes estadísticos" };
            var permiso = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if(permiso!=null)
            {
                permitido = true;
            }
            var tiporeporte = ControlTipoReporte.ObtenerTodos().Where(x => x.Activo == 1 && pantallasId.Contains(x.PantallaId));
            var t = 0;
            foreach(var item in tiporeporte)
            {
                t++;
            }
            if (t>0)
            {
                
            }
             else
            {
                if(permitido)
                {
                    tiporeporte = ControlTipoReporte.ObtenerTodos().Where(x => x.Activo == 1 ).ToList();
                }
            }

            foreach (var _tiporeporte in tiporeporte)
            {
                combo.Add(new Combo { Desc = _tiporeporte.Reporte, Id = _tiporeporte.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> GetReporteEstadistico(string tiporeporteid)
        {
            var combo = new List<Combo>();

            int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var accesoReportes = Business.ControlUsuarioReportes.ObtenerPorUsuarioId(usId).Where(x => x.Activo && x.Habilitado).Select(x => x.ReporteId).Distinct().ToList();
            string[] parametros = { usId.ToString(), "Reportes estadísticos" };
            var permiso = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
            var permitido = false;
            if (permiso != null)
            {
                permitido = true;
            }
            var tiporeporte = ControlReporteEstadistico.ObtenerTodos().Where(x => accesoReportes.Contains(x.ReporteId));
            
            
            tiporeporte = ControlReporteEstadistico.ObtenerTodos().Where(x => x.Activo == 1);
            foreach (var _tiporeporte in tiporeporte.Where(x => x.Tipo_reporteId == Convert.ToInt32(tiporeporteid)))
            {
                if(_tiporeporte.Activo==1)
                {
                    combo.Add(new Combo { Desc = _tiporeporte.Descripcion, Id = _tiporeporte.Id.ToString() });
                }
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getListadoMotivos(filtrosreportesestadisticos filtro)
        {
            var combo = new List<Combo>();
            List<Entity.MotivoDetencion> listado = ControlMotivoDetencion.ObtenerTodos();
            if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
            if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
            if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

            if (filtro.clienteid == "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(usId);
                if (usuario != null)
                {
                    var subcontratos = ControlSubcontrato.ObtenerByUsuarioId(usuario.Id).Select(x => x.Id);
                    listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
                }
            }

            if (filtro.clienteid != "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
            {
                var contratosId = ControlContrato.ObtenerTodos().Where(x => x.ClienteId == Convert.ToInt32(filtro.clienteid)).Select(x => x.Id);
                var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
            }

            if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid == "0")
            {
                var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => x.ContratoId == Convert.ToInt32(filtro.contratoid)).Select(x => x.Id);
                listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
            }

            if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid != "0")
            {
                listado = listado.Where(x => x.ContratoId == Convert.ToInt32(filtro.subcontratoid)).ToList();
            }

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x => x.Activo == 1 && x.Habilitado == 1))
                    combo.Add(new Combo { Desc = rol.Motivo, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getListadoTipoIntoxicacion()
        {
            var combo = new List<Combo>();
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(84);

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x => x.Activo == true && x.Habilitado == true))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getListadoCorporacion(filtrosreportesestadisticos filtro)
        {
            var combo = new List<Combo>();
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(87);
            if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
            if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
            if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

            if (filtro.clienteid == "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(usId);
                if (usuario != null)
                {
                    var subcontratos = ControlSubcontrato.ObtenerByUsuarioId(usuario.Id).Select(x => x.Id);
                    listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
                }
            }

            if (filtro.clienteid != "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
            {
                var contratosId = ControlContrato.ObtenerTodos().Where(x => x.ClienteId == Convert.ToInt32(filtro.clienteid)).Select(x => x.Id);
                var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
            }

            if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid == "0")
            {
                var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => x.ContratoId == Convert.ToInt32(filtro.contratoid)).Select(x => x.Id);
                listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
            }

            if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid != "0")
            {
                listado = listado.Where(x => x.ContratoId == Convert.ToInt32(filtro.subcontratoid)).ToList();
            }
            listado = ControlCatalogo.ObtenerTodo(87);
            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x => x.Activo == true && x.Habilitado == true))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getListadoUnidad(filtrounidad filtro)
        {
            var combo = new List<Combo>();
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(82);
            if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
            if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
            if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

            if (filtro.clienteid == "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(usId);
                if (usuario != null)
                {
                    var subcontratos = ControlSubcontrato.ObtenerByUsuarioId(usuario.Id).Select(x => x.Id);
                    listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
                }
            }

            if (filtro.clienteid != "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
            {
                var contratosId = ControlContrato.ObtenerTodos().Where(x => x.ClienteId == Convert.ToInt32(filtro.clienteid)).Select(x => x.Id);
                var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
            }

            if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid == "0")
            {
                var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => x.ContratoId == Convert.ToInt32(filtro.contratoid)).Select(x => x.Id);
                listado = listado.Where(x => subcontratos.Contains(x.ContratoId)).ToList();
            }

            if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid != "0")
            {
                listado = listado.Where(x => x.ContratoId == Convert.ToInt32(filtro.subcontratoid)).ToList();
            }
            listado = ControlCatalogo.ObtenerTodo(82);
            if (listado.Count > 0)
            {
                foreach (var unidad in listado.Where(x => x.Activo == true && x.Habilitado == true))
                {
                    var unit = ControlUnidad.ObtenerById(unidad.Id);
                    if (filtro.corporacion == null || filtro.corporacion == "0" || unit.CorporacionId == int.Parse(filtro.corporacion))
                    {
                        combo.Add(new Combo { Desc = unit.Nombre, Id = unit.Id.ToString() });
                    }
                }
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getSituations()
        {
            List<Combo> combo = new List<Combo>();

            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            Entity.ContratoUsuario contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

            var situaciones = ControlCatalogo.ObtenerTodo(89);

            if (situaciones.Count > 0)
            {
                foreach (var rol in situaciones)
                {
                    if (rol.ContratoId == contratoUsuario.IdContrato && rol.Tipo == contratoUsuario.Tipo && rol.Activo && rol.Habilitado)
                    {
                        combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
                    }
                }
            }

            return combo;
        }

        [WebMethod]
        public static List<Combo> getListadoJuez()
        {
            var combo = new List<Combo>();
            List<Entity.Usuario> listado = ControlUsuario.ObteneTodos();

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x => x.Activo == true && x.Habilitado == true && x.RolId == 5))
                    combo.Add(new Combo { Desc = rol.User, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static List<Combo> getListadoLesion()
        {
            var combo = new List<Combo>();
            List<Entity.Catalogo> listado = ControlCatalogo.ObtenerTodo(1);

            if (listado.Count > 0)
            {
                foreach (var rol in listado.Where(x => x.Activo == true && x.Habilitado == true))
                    combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
            }
            return combo;
        }

        [WebMethod]
        public static object Comparativomotivos(filtrosreportesestadisticos filtro)
        {
            try
            {

                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 24 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if(permisos!=null)
                {
                    permitido = true;
                }
                
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if(municipioContrato!=null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if(muni!=null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.motivoid != null ? filtro.motivoid : "0", filtro.subcontratoid != null ? filtro.subcontratoid : "0", filtro.UtilizarHora.ToString() ,!string.IsNullOrEmpty(filtro.clienteid)? filtro.clienteid:"0"};

                    var comparativo = ControlComparativoMotivos.GetComparativomotivos(filtros);

                    object[] dataArchivo = null;
                    Boolean mayores = Convert.ToBoolean(filtro.MayoresEdad);
                    Boolean menores = Convert.ToBoolean(filtro.MenoresEdad);
                    if (mayores && !menores)
                    {
                        comparativo = comparativo.Where(x => x.Edad >= 18).ToList();
                    }
                    else if (!mayores && menores)
                    {
                        comparativo = comparativo.Where(x => x.Edad < 18).ToList();

                    }
                    Boolean usahora = new Boolean();
                    usahora = Convert.ToBoolean(filtro.UtilizarHora);

                    if (usahora)
                    {
                        DateTime fechainicial = new DateTime();
                        DateTime FechaFinal = new DateTime();
                        fechainicial = Convert.ToDateTime(filtro.Fechainical);
                        FechaFinal = Convert.ToDateTime(filtro.Fechafinal);
                        comparativo = comparativo.Where(x => x.Fecha >= fechainicial && x.Fecha < FechaFinal).ToList();

                    }

                    dataArchivo = ControlPDFReportesEstadisticos.ReporteComparativoMoivos(comparativo, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object Resultadosquimicospordiadelasemana(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 33 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var clienteid = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) clienteid = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.motivoid != null ? filtro.motivoid : "0", filtro.subcontratoid != null ? filtro.subcontratoid : "0", filtro.UtilizarHora.ToString(),clienteid };

                    var comparativo = ControlComparativoMotivos.GetComparativomotivos(filtros);
                    Boolean usarhora = Convert.ToBoolean(filtro.UtilizarHora);
                    if (usarhora)
                    {

                        comparativo = comparativo.Where(x => x.Fecha >= Convert.ToDateTime(filtro.Fechainical) && x.Fecha <= Convert.ToDateTime(filtro.Fechafinal)).ToList();

                    }
                    var resultados = ControlResultadosQuimicos.ObtenerResultadosQuimicos();

                    if (resultados.Count == 0)
                    {
                        return new { exitoso = false, mensaje = "No se encontraron resultados", Id = "", TrackingId = "" };
                    }

                    if (Convert.ToInt32(filtro.ResultadoId) != 0)
                    {
                        resultados = resultados.Where(x => x.Id == Convert.ToInt32(filtro.ResultadoId)).ToList();
                    }

                    var listadoquimico = ControlCertificadoQuimico.ObTenerTodo();

                    listadoquimico = listadoquimico.Where(x => x.Fecha_proceso >= Convert.ToDateTime(filtro.Fechainical) && x.Fecha_proceso <= Convert.ToDateTime(filtro.Fechafinal)).ToList();

                    Boolean mayores = Convert.ToBoolean(filtro.MayoresEdad);
                    Boolean menores = Convert.ToBoolean(filtro.MenoresEdad);
                    if (mayores && !menores)
                    {
                        listadoquimico = listadoquimico.Where(x => x.Edad >= 18).ToList();
                    }
                    else if (!mayores && menores)
                    {
                        listadoquimico = listadoquimico.Where(x => x.Edad < 18).ToList();

                    }

                    if (listadoquimico.Count() == 0)
                    {
                        return new { exitoso = false, mensaje = "No se encontraron resultados", Id = "", TrackingId = "" };

                    }

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteResultadosQuimicospordiadelasemana(comparativo, resultados, listadoquimico, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object Detenidospordiadelasemana(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 27 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var clienteid = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) clienteid = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.motivoid != null ? filtro.motivoid : "0", filtro.subcontratoid != null ? filtro.subcontratoid : "0", filtro.UtilizarHora.ToString(),clienteid };

                    var comparativo = ControlComparativoMotivos.GetComparativomotivos(filtros);
                    Boolean usarhora = Convert.ToBoolean(filtro.UtilizarHora);
                    if (usarhora)
                    {

                        comparativo = comparativo.Where(x => x.Fecha >= Convert.ToDateTime(filtro.Fechainical) && x.Fecha <= Convert.ToDateTime(filtro.Fechafinal)).ToList();

                    }
                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDetenidospordiadelasemana(comparativo, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object Detenidospormes(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 28 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                       municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var clienteid = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) clienteid = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.motivoid != null ? filtro.motivoid : "0", filtro.subcontratoid != null ? filtro.subcontratoid : "0", filtro.UtilizarHora.ToString(), clienteid };

                    var comparativo = ControlComparativoMotivos.GetComparativomotivos(filtros);

                    Boolean usarhora = Convert.ToBoolean(filtro.UtilizarHora);
                    if (usarhora)
                    {
                        comparativo = comparativo.Where(x => x.Fecha >= Convert.ToDateTime(filtro.Fechainical) && x.Fecha <= Convert.ToDateTime(filtro.Fechafinal)).ToList();
                    }

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDetenidospormes(comparativo, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object RelacionDetenidosporSalidaAutorizada(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 39 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var cliente = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) cliente = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.TipoSalidaId != null ? filtro.TipoSalidaId : "0", filtro.subcontratoid != null ? filtro.subcontratoid : "0", filtro.UtilizarHora.ToString(),cliente };

                    var relacionDetenidos = ControlRelacionDetenidosPorSalidaAutorizada.GetDet(filtros);

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteRelaciondetenidoporSalidaAutorizada(relacionDetenidos, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object RelacionDetenidosporCalificacion(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 38 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var cleinte = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) cleinte = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.SituacionId != null ? filtro.SituacionId : "0", filtro.subcontratoid != null ? filtro.subcontratoid : "0", filtro.UtilizarHora.ToString(),cleinte };

                    var relacionDetenidos = ControlRelacionDetenidosCalificacion.GetDet(filtros);

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteRelaciondetenidoporCalificacion(relacionDetenidos, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object DetalleDetenidospormotivos(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 35 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var clienteId = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) clienteId = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.motivoid != null ? filtro.motivoid : "0", filtro.subcontratoid != null ? filtro.subcontratoid : "0", filtro.UtilizarHora.ToString(),clienteId };

                    var detalledetenidopormotivos = ControlDetalleDetenidosPorMotivo.GetDetalledetenidopormotivos(filtros);

                    Boolean mayores = Convert.ToBoolean(filtro.MayoresEdad);
                    Boolean menores = Convert.ToBoolean(filtro.MenoresEdad);
                    if (mayores && !menores)
                    {

                        detalledetenidopormotivos = detalledetenidopormotivos.Where(x => x.Edad >= 18).ToList();

                    }
                    else if (!mayores && menores)
                    {
                        detalledetenidopormotivos = detalledetenidopormotivos.Where(x => x.Edad < 18).ToList();
                    }

                    filtro.SituacionId = (!string.IsNullOrEmpty(filtro.SituacionId)) ? filtro.SituacionId : "0";

                    if (filtro.SituacionId != "0")
                    {
                        detalledetenidopormotivos = detalledetenidopormotivos.Where(x => x.SituacionId == Convert.ToInt32(filtro.SituacionId)).ToList();
                    }

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDetalledetenidopormotivos(detalledetenidopormotivos, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object DetalleDetenidosporedad(filtrosreportesestadisticos filtro, filtroedades filtro2)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 25 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    filtro.LesionId = (!string.IsNullOrEmpty(filtro.LesionId)) ? filtro.LesionId : "0";
                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                    "No especificado",
                                    "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                    "No especificado",
                                    "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                                municipioContrato.Nombre,
                                institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                            municipio,
                            institucionSubcontrato.Nombre
                        };
                    }
                    var cleinte1 = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) cleinte1 = filtro.clienteid;

                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.LesionId, (!string.IsNullOrEmpty(filtro.contratoid) ? filtro.contratoid : "0"), filtro.UtilizarHora.ToString(),cleinte1 };
                    List<Entity.Detenidosporedad> detalledetenidoporedad = new List<Entity.Detenidosporedad>();
                    if (filtro.LesionId == "0")
                    {
                        detalledetenidoporedad = ControlDetenidosPorEdad.ObtenerDetenidosporedad(filtros);
                    }
                    else
                    {
                        detalledetenidoporedad = ControlDetenidosPorEdad.ObtenerDetenidosporedadLesion(filtros);
                    }
                    object[] dataArchivo = null;

                    if (filtro.clienteid == "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
                    {
                        var usuario = ControlUsuario.Obtener(usId);
                        if (usuario != null)
                        {
                            var clientes = ControlContrato.ObetenerPorUsuarioId(usuario.Id).Select(x => x.ClienteId).Distinct();
                            var contratosId = ControlContrato.ObtenerTodos().Where(x => clientes.Contains(x.ClienteId) && x.Habilitado).Select(x => x.Id);
                            var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                            var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                            detalledetenidoporedad = detalledetenidoporedad.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                        }
                    }

                    if (filtro.clienteid != "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
                    {
                        var usuario = ControlUsuario.Obtener(usId);
                        if (usuario != null)
                        {
                            var contratosId = ControlContrato.ObtenerTodos().Where(x => x.ClienteId == Convert.ToInt32(filtro.clienteid) && x.Habilitado).Select(x => x.Id);
                            var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                            var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                            detalledetenidoporedad = detalledetenidoporedad.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                        }
                    }

                    if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid == "0")
                    {
                        var usuario = ControlUsuario.Obtener(usId);
                        if (usuario != null)
                        {
                            var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => x.ContratoId == int.Parse(filtro.contratoid)).Select(x => x.Id);
                            var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                            detalledetenidoporedad = detalledetenidoporedad.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                        }
                    }

                    if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid != "0")
                    {
                        var usuario = ControlUsuario.Obtener(usId);
                        if (usuario != null)
                        {
                            var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => x.IdContrato == int.Parse(filtro.subcontratoid) && x.Activo).Select(x => x.IdContrato);
                            detalledetenidoporedad = detalledetenidoporedad.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                        }
                    }

                    if (filtro2.MayoresEdad && !filtro2.MenoresEdad) detalledetenidoporedad = detalledetenidoporedad.Where(x => x.Edad >= 18).ToList();
                    else if (!filtro2.MayoresEdad && filtro2.MenoresEdad) detalledetenidoporedad = detalledetenidoporedad.Where(x => x.Edad < 18).ToList();


                    DateTime fechaInicial = DateTime.Parse(filtro.Fechainical);
                    DateTime fechaFinal = DateTime.Parse(filtro.Fechafinal);


                    if (filtro.UtilizarHora) detalledetenidoporedad = detalledetenidoporedad.Where(x => x.Fecha >= fechaInicial && x.Fecha <= fechaFinal.AddMinutes(1).AddSeconds(-1)).ToList();
                    else detalledetenidoporedad = detalledetenidoporedad.Where(x => x.Fecha.Date >= fechaInicial.Date && x.Fecha.Date <= fechaFinal.Date).ToList();

                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDetalledetenidoporedad(detalledetenidoporedad, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object DetalleDetenidosporrangoedad(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 29 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    if (Convert.ToInt32(filtro.Edadinicial) > Convert.ToInt32(filtro.Edadfinal))
                    {
                        return new { exitoso = false, mensaje = "La edad inicial no puede ser mayor que la edad final." };
                    }


                    object[] dataArchivo = null;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";
                    var clienteId = "0";
                    if (!String.IsNullOrEmpty(filtro.clienteid)) clienteId = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.clienteid, filtro.contratoid, filtro.UtilizarHora.ToString(),clienteId };
                    var detalledetenidoporedad = ControlDetenidosPorEdad.ObtenerDetenidosporedad(filtros);

                    if (filtro.clienteid == "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
                    {
                        var usuario = ControlUsuario.Obtener(usId);
                        if (usuario != null)
                        {
                            var clientes = ControlContrato.ObetenerPorUsuarioId(usuario.Id).Select(x => x.ClienteId).Distinct();
                            var contratosId = ControlContrato.ObtenerTodos().Where(x => clientes.Contains(x.ClienteId) && x.Habilitado).Select(x => x.Id);
                            var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                            var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                            detalledetenidoporedad = detalledetenidoporedad.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                        }
                    }

                    if (filtro.clienteid != "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0")
                    {
                        var usuario = ControlUsuario.Obtener(usId);
                        if (usuario != null)
                        {
                            var contratosId = ControlContrato.ObtenerTodos().Where(x => x.ClienteId == Convert.ToInt32(filtro.clienteid) && x.Habilitado).Select(x => x.Id);
                            var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId)).Select(x => x.Id);
                            var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                            detalledetenidoporedad = detalledetenidoporedad.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                        }
                    }

                    if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid == "0")
                    {
                        var usuario = ControlUsuario.Obtener(usId);
                        if (usuario != null)
                        {
                            var subcontratos = ControlSubcontrato.ObtenerTodos().Where(x => x.ContratoId == int.Parse(filtro.contratoid)).Select(x => x.Id);
                            var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => subcontratos.Contains(x.IdContrato) && x.Activo).Select(x => x.IdContrato);
                            detalledetenidoporedad = detalledetenidoporedad.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                        }
                    }

                    if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid != "0")
                    {
                        var usuario = ControlUsuario.Obtener(usId);
                        if (usuario != null)
                        {
                            var subcontratosUsuarioActual = ControlContratoUsuario.ObtenerPorUsuarioId(usuario.Id).Where(x => x.IdContrato == int.Parse(filtro.subcontratoid) && x.Activo).Select(x => x.IdContrato);
                            detalledetenidoporedad = detalledetenidoporedad.Where(x => subcontratosUsuarioActual.Contains(x.Subcontrato)).ToList();
                        }
                    }

                    //if (filtro2.MayoresEdad && !filtro2.MenoresEdad) detalledetenidoporedad = detalledetenidoporedad.Where(x => x.Edad >= 18).ToList();
                    //else if (!filtro2.MayoresEdad && filtro2.MenoresEdad) detalledetenidoporedad = detalledetenidoporedad.Where(x => x.Edad < 18).ToList();

                    DateTime fechaInicial = DateTime.Parse(filtro.Fechainical);
                    DateTime fechaFinal = DateTime.Parse(filtro.Fechafinal);

                    if (Convert.ToBoolean(filtro.UtilizarHora)) detalledetenidoporedad = detalledetenidoporedad.Where(x => x.Fecha >= fechaInicial && x.Fecha <= fechaFinal.AddMinutes(1).AddSeconds(-1)).ToList();
                    else detalledetenidoporedad = detalledetenidoporedad.Where(x => x.Fecha.Date >= fechaInicial.Date && x.Fecha.Date <= fechaFinal.Date).ToList();

                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDetalledetenidoporrangoedad(detalledetenidoporedad.Where(x => x.Edad >= Convert.ToInt32(filtro.Edadinicial) && x.Edad <= Convert.ToInt32(filtro.Edadfinal)).ToList(), filtros);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object DetalleDetenidosporrangoremision(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 30 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    if (Convert.ToInt32(filtro.Remisioninicial) > Convert.ToInt32(filtro.Remisionfinal))
                    {
                        return new { exitoso = false, mensaje = "La remisión inicial no puede ser mayor que la remisión final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";
                    var subcontratoid = filtro.subcontratoid;
                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
                        var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        filtro.subcontratoid = subcontrato.Id.ToString();
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                var institucionContrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                                var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                                var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                                data = new object[]
                                {
                                municipioContrato.Nombre,
                                institucionContrato.Nombre
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }
                                var institucionContrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                                var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                                var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                                data = new object[]
                                {
                                municipioContrato.Nombre,
                                institucionContrato.Nombre
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var clienteid = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) clienteid = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), subcontratoid, filtro.Remisioninicial.ToString(), filtro.Remisionfinal.ToString(), filtro.UtilizarHora.ToString(),clienteid };

                    var detalledetenidoporangoremisioninfodet = ControlReporteEstadisticoInformeDetencion.ObtenerInformes(filtros);
                    object[] dataArchivo = null;
                    if (Convert.ToBoolean(filtro.UtilizarHora))
                    {
                        DateTime Fechainicial = new DateTime();
                        DateTime Fechafinal = new DateTime();
                        Fechainicial = Convert.ToDateTime(filtro.Fechainical);
                        Fechafinal = Convert.ToDateTime(filtro.Fechafinal);

                        detalledetenidoporangoremisioninfodet.Where(x => x.Fecharegistro >= Fechainicial && x.Fecharegistro < Fechafinal);

                    }

                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDetalledetenidoporrangoremision(detalledetenidoporangoremisioninfodet, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object Trazabilidad(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 40 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    if (Convert.ToInt32(filtro.Remisioninicial) > Convert.ToInt32(filtro.Remisionfinal))
                    {
                        return new { exitoso = false, mensaje = "La remisión inicial no puede ser mayor que la remisión final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var cliente = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) cliente = filtro.clienteid;

                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.subcontratoid != null ? filtro.subcontratoid.ToString() : "0", filtro.UtilizarHora.ToString(),cliente };

                    var reporteestadisticotraz = ControlReporteEstadisticoTrazabilidad.GetTrazabilidad(filtros);
                    if (filtro.UtilizarHora)
                    {
                        reporteestadisticotraz = reporteestadisticotraz.Where(x => x.Fechayhora >= Convert.ToDateTime(filtro.Fechainical) && x.Fechayhora <= Convert.ToDateTime(filtro.Fechafinal)).ToList();
                    }
                    //if (filtro.UtilizarHora) Detenidosporcolonia = Detenidosporcolonia.Where(x => x.Fecha >= fechaInicial && x.Fecha <= fechaFinal.AddMinutes(1).AddSeconds(-1)).ToList();

                    if (Convert.ToInt32(filtro.ProcesoId) != 0)
                    {
                        var procesos = ControlProceso.GetAll();
                        Entity.Proceso proceso = new Entity.Proceso();
                        proceso = procesos.LastOrDefault(x => x.Id == Convert.ToInt32(filtro.ProcesoId));
                        reporteestadisticotraz = reporteestadisticotraz.Where(x => x.Proceso == proceso.proceso).ToList();

                        if (Convert.ToInt32(filtro.SubprocesoId) != 0)
                        {
                            var suprocesos = ControlSubProceso.GetAll();
                            Entity.SubProceso subProceso = new Entity.SubProceso();
                            subProceso = suprocesos.LastOrDefault(x => x.Id == Convert.ToInt32(filtro.SubprocesoId));
                            reporteestadisticotraz = reporteestadisticotraz.Where(x => x.Subproceso == subProceso.Subproceso).ToList();
                        }
                    }

                    object[] dataArchivo = null;

                    dataArchivo = ControlPDFReportesEstadisticos.ReporteTrazabilidad(reporteestadisticotraz, filtros);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object DescriptivoDetenidosporrangoremision(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 31 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    if (Convert.ToInt32(filtro.Remisioninicial) > Convert.ToInt32(filtro.Remisionfinal))
                    {
                        return new { exitoso = false, mensaje = "La remisión inicial no puede ser mayor que la remisión final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var cliente1 = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) cliente1 = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.subcontratoid.ToString(), filtro.Remisioninicial.ToString(), filtro.Remisionfinal.ToString(), filtro.UtilizarHora.ToString(),cliente1 };

                    var detalledetenidoporangoedad = ControlDescriptivoDetenidos.ObtenerDescriptivo(filtros);
                    object[] dataArchivo = null;


                    dataArchivo = ControlPDFReportesEstadisticos.ReporteTituloDetalledeteniodosDescriptivoporrangoremision(detalledetenidoporangoedad, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object Detenidosporcolonia(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 26 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    if (string.IsNullOrEmpty(filtro.ColoniaId)) filtro.ColoniaId = "0";
                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    object[] data;
                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var cleinteid = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) cleinteid = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.ColoniaId != null ? filtro.ColoniaId : "0", filtro.subcontratoid, filtro.UtilizarHora.ToString(),cleinteid };
                    List<Entity.Detenidosporcolonia> Detenidosporcolonia = new List<Entity.Detenidosporcolonia>();
                    Boolean Mayores = Convert.ToBoolean(filtro.MayoresEdad);
                    Boolean Menores = Convert.ToBoolean(filtro.MenoresEdad);

                    if (Mayores && !Menores)
                    {
                        Detenidosporcolonia = ControlDetenidosPorColonia.GetDetenidosMayoresporcolonias(filtros);
                    }
                    else if (!Mayores && Menores)
                    {
                        Detenidosporcolonia = ControlDetenidosPorColonia.GetDetenidosMenoresporcolonias(filtros);
                    }
                    else
                    {
                        Detenidosporcolonia = ControlDetenidosPorColonia.GetDetenidosporcolonias(filtros);
                    }
                    object[] dataArchivo = null;

                    if (filtro.ColoniaId != "0")
                    {
                        Detenidosporcolonia = Detenidosporcolonia.Where(z => z.ColoniaId == Convert.ToInt32(filtro.ColoniaId)).ToList();
                    }

                    //if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid != "0" && filtro.ColoniaId == "0")
                    //{
                    //    var subcontrato = ControlSubcontrato.ObtenerPorId(Convert.ToInt32(filtro.subcontratoid));
                    //    var institucion = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                    //    var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

                    //    var colonias = ControlColonia.ObtenerTodas().Where(x => x.IdMunicipio == domicilio.MunicipioId && x.Habilitado).Select(x => x.Id);

                    //    Detenidosporcolonia = Detenidosporcolonia.Where(z => colonias.Contains(z.ColoniaId)).ToList();
                    //}

                    //if (filtro.clienteid != "0" && filtro.contratoid != "0" && filtro.subcontratoid == "0" && filtro.ColoniaId == "0")
                    //{
                    //    var institucionId = ControlSubcontrato.ObtenerTodos().Where(x => x.ContratoId == Convert.ToInt32(filtro.contratoid) && x.Habilitado).Select(x => x.InstitucionId);

                    //    List<int> municipiosId = new List<int>();
                    //    foreach (var id in institucionId)
                    //    {
                    //        var institucion = ControlInstitucion.ObtenerPorId(id);
                    //        var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
                    //        municipiosId.Add(Convert.ToInt32(domicilio.MunicipioId));
                    //    }

                    //    var colonias = ControlColonia.ObtenerTodas().Where(x => municipiosId.Contains(x.IdMunicipio) && x.Habilitado).Select(x => x.Id);

                    //    Detenidosporcolonia = Detenidosporcolonia.Where(z => colonias.Contains(z.ColoniaId)).ToList();
                    //}

                    //if (filtro.clienteid != "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0" && filtro.ColoniaId == "0")
                    //{
                    //    var contratosId = ControlContrato.ObtenerTodos().Where(x => x.ClienteId == Convert.ToInt32(filtro.clienteid) && x.Habilitado).Select(x => x.Id);
                    //    var institucionesId = ControlSubcontrato.ObtenerTodos().Where(x => contratosId.Contains(x.ContratoId) && x.Habilitado).Select(x => x.InstitucionId);

                    //    List<int> municipiosId = new List<int>();

                    //    foreach (var id in institucionesId)
                    //    {
                    //        var institucion = ControlInstitucion.ObtenerPorId(id);
                    //        var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
                    //        municipiosId.Add(Convert.ToInt32(domicilio.MunicipioId));
                    //    }
                    //    var colonias = ControlColonia.ObtenerTodas().Where(x => municipiosId.Contains(x.IdMunicipio) && x.Habilitado).Select(x => x.Id);

                    //    Detenidosporcolonia = Detenidosporcolonia.Where(z => colonias.Contains(z.ColoniaId)).ToList();
                    //}

                    //if (filtro.clienteid == "0" && filtro.contratoid == "0" && filtro.subcontratoid == "0" && filtro.ColoniaId == "0")
                    //{
                    //    int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    //    var usuario = ControlUsuario.Obtener(usId);
                    //    if (usuario != null)
                    //    {
                    //        var subcontratos = ControlSubcontrato.ObtenerByUsuarioId(usuario.Id);

                    //        var institucionesId = subcontratos.Select(x => x.InstitucionId);

                    //        List<int> municipiosId = new List<int>();

                    //        foreach (var id in institucionesId)
                    //        {
                    //            var institucion = ControlInstitucion.ObtenerPorId(id);
                    //            var domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
                    //            municipiosId.Add(Convert.ToInt32(domicilio.MunicipioId));
                    //        }
                    //        var colonias = ControlColonia.ObtenerTodas().Where(x => municipiosId.Contains(x.IdMunicipio) && x.Habilitado).Select(x => x.Id);

                    //        Detenidosporcolonia = Detenidosporcolonia.Where(z => colonias.Contains(z.ColoniaId)).ToList();
                    //    }
                    //}

                    DateTime fechaInicial = DateTime.Parse(filtro.Fechainical);
                    DateTime fechaFinal = DateTime.Parse(filtro.Fechafinal);

                    if (filtro.UtilizarHora) Detenidosporcolonia = Detenidosporcolonia.Where(x => x.Fecha >= fechaInicial && x.Fecha <= fechaFinal.AddMinutes(1).AddSeconds(-1)).ToList();
                    else Detenidosporcolonia = Detenidosporcolonia.Where(x => x.Fecha.Date >= fechaInicial.Date && x.Fecha.Date <= fechaFinal.Date).ToList();

                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDetenidosporcolonia(Detenidosporcolonia, filtros, data);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static bool Validarangofechas(filtrosreportesestadisticos filtro)
        {
            if (Convert.ToDateTime(filtro.Fechainical) > Convert.ToDateTime(filtro.Fechafinal))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        [WebMethod]
        public static object DescuentoAgravanetJC(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 36 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }
                    if (string.IsNullOrEmpty(filtro.JuezId))
                    { filtro.JuezId = "0"; }
                    var clienteid = "0";
                    if (!string.IsNullOrEmpty(filtro.clienteid)) clienteid = filtro.clienteid;
                    string[] filtros = { filtro.Fechainical.ToString(), filtro.Fechafinal.ToString(), filtro.motivoid != null ? filtro.motivoid : "0", filtro.subcontratoid != null ? filtro.subcontratoid : "0", filtro.JuezId, filtro.UtilizarHora.ToString(),clienteid };

                    var descuentoagravante = ControlDescuentoAgravanteJuezCalificador.GetDescuentosagravantesjuezcalificador(filtros);

                    object[] dataArchivo = null;
                    dataArchivo = ControlPDFReportesEstadisticos.ReporteDescuentosagravanteJC(descuentoagravante, filtros);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object DetenidosGradoIntoxicacion(filtrosreportesestadisticos filtro)
        {
            try
            {
                if (!Validarangofechas(filtro))
                {
                    return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                }

                object[] data;

                if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                var detenciones = new List<Entity.DetalleDetencion>();
                if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                {
                    if (filtro.contratoid == null || filtro.contratoid == "0")
                    {
                        if (filtro.clienteid == null || filtro.clienteid == "0")
                        {
                            detenciones = ControlDetalleDetencion.ObteneTodos();

                            data = new object[]
                            {
                                "No especificado",
                                "No especificado"
                            };
                        }
                        else
                        {
                            var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                            foreach (var contrato in contratos)
                            {
                                var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                detenciones = detenciones.Concat(ControlDetalleDetencion.ObteneTodos().Where(x => subcontratos.Contains(x.ContratoId))).ToList();
                            }

                            data = new object[]
                            {
                                "No especificado",
                                "No especificado"
                            };
                        }
                    }
                    else
                    {
                        var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);
                        detenciones = ControlDetalleDetencion.ObteneTodos().Where(x => subcontratos.Contains(x.ContratoId)).ToList();

                        var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                        data = new object[]
                        {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                        };
                    }
                }
                else
                {

                    detenciones = ControlDetalleDetencion.ObteneTodos().Where(x => x.ContratoId == int.Parse(filtro.subcontratoid)).ToList();
                    if(!string.IsNullOrEmpty(filtro.clienteid) && filtro.clienteid!="0")
                    {
                        var dete = new List<Entity.DetalleDetencion>();
                        foreach (var item in detenciones)
                        {
                            var sc = ControlSubcontrato.ObtenerPorId(item.ContratoId);
                            var c = ControlContrato.ObtenerPorId(sc.ContratoId);
                            if (c.ClienteId == Convert.ToInt32(filtro.clienteid))
                                dete.Add(item);
                    }
                        detenciones = dete;
                    }
                    
                    var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                    var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                    var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                    var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                    var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                    var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                    var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                    var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));


                    var municipio = "No especificado";
                    if (municipioContrato != null)
                    {
                        municipio = municipioContrato.Nombre;
                    }
                    else if (muni != null)
                    {
                        municipio = muni.Nombre;
                    }
                    data = new object[]
                    {
                        municipio,
                        institucionSubcontrato.Nombre
                    };
                }

                var fechaInicial = new DateTime();
                var fechaFinal = new DateTime();
                fechaInicial = Convert.ToDateTime(filtro.Fechainical);
                fechaFinal = Convert.ToDateTime(filtro.Fechafinal);
                var clienteid = "0";
                if (!string.IsNullOrEmpty(filtro.clienteid)) clienteid = filtro.clienteid;

                var dataArchivo = ControlPDFDetenidosGradoIntoxicacion.generarPDF(data, fechaInicial, fechaFinal, filtro.UtilizarHora, detenciones, int.Parse(filtro.TipoIntoxicacionId));

                return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }

        [WebMethod]
        public static object SalidasCustodiasOtrasDependencias(filtrosreportesestadisticos filtro)
        {
            try
            {
                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var permiso = Business.ControlUsuarioReportes.ObtenerPorUsuarioIdReporteId(new int[] { usId, 37 });
                string[] parametros = { usId.ToString(), "Reportes estadísticos" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                var permitido = false;
                if (permiso != null)
                {
                    permitido = true;
                }
                else if (permisos != null)
                {
                    permitido = true;
                }
                if (permitido)
                {
                    if (!Validarangofechas(filtro))
                    {
                        return new { exitoso = false, mensaje = "La fecha inicial no puede ser mayor que la final." };
                    }

                    object[] data;

                    if (string.IsNullOrEmpty(filtro.clienteid)) filtro.clienteid = "0";
                    if (string.IsNullOrEmpty(filtro.contratoid)) filtro.contratoid = "0";
                    if (string.IsNullOrEmpty(filtro.subcontratoid)) filtro.subcontratoid = "0";

                    var detenciones = new List<Entity.DetalleDetencion>();
                    if (filtro.subcontratoid == null || filtro.subcontratoid == "0")
                    {
                        if (filtro.contratoid == null || filtro.contratoid == "0")
                        {
                            if (filtro.clienteid == null || filtro.clienteid == "0")
                            {
                                detenciones = ControlDetalleDetencion.ObteneTodos();

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                            else
                            {
                                var contratos = ControlContrato.ObtenerPorCliente(int.Parse(filtro.clienteid));
                                foreach (var contrato in contratos)
                                {
                                    var subcontratos = ControlSubcontrato.ObtenerByContratoId(contrato.Id).Select(x => x.Id);
                                    detenciones = detenciones.Concat(ControlDetalleDetencion.ObteneTodos().Where(x => subcontratos.Contains(x.ContratoId))).ToList();
                                }

                                data = new object[]
                                {
                                "No especificado",
                                "No especificado"
                                };
                            }
                        }
                        else
                        {
                            var subcontratos = ControlSubcontrato.ObtenerByContratoId(int.Parse(filtro.contratoid)).Select(x => x.Id);
                            detenciones = ControlDetalleDetencion.ObteneTodos().Where(x => subcontratos.Contains(x.ContratoId)).ToList();

                            var contrato = ControlContrato.ObtenerPorId(int.Parse(filtro.contratoid));
                            var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                            var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                            var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);

                            data = new object[]
                            {
                            municipioContrato.Nombre,
                            institucionContrato.Nombre
                            };
                        }
                    }
                    else
                    {
                        detenciones = ControlDetalleDetencion.ObteneTodos().Where(x => x.ContratoId == int.Parse(filtro.subcontratoid)).ToList();

                        var subcontrato = ControlSubcontrato.ObtenerPorId(int.Parse(filtro.subcontratoid));
                        var institucionSubcontrato = ControlInstitucion.ObtenerPorId(subcontrato.InstitucionId);
                        var contrato = ControlContrato.ObtenerPorId(subcontrato.ContratoId);
                        var institucionContrato = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
                        var domicilioInstitucionContrato = ControlDomicilio.ObtenerPorId(institucionContrato.DomicilioId);
                        var municipioContrato = ControlMunicipio.Obtener(domicilioInstitucionContrato.MunicipioId ?? 0);
                        var domi = ControlDomicilio.ObtenerPorId(institucionSubcontrato.DomicilioId);
                        var muni = ControlMunicipio.Obtener(Convert.ToInt32(domi.MunicipioId));

                        var municipio = "No especificado";
                        if (municipioContrato != null)
                        {
                            municipio = municipioContrato.Nombre;
                        }
                        else if (muni != null)
                        {
                            municipio = muni.Nombre;
                        }
                        data = new object[]
                        {
                        municipio,
                        institucionSubcontrato.Nombre
                        };
                    }
                    var det = new List<Entity.DetalleDetencion>();
                    if((filtro.subcontratoid == null || filtro.subcontratoid == "0") && !string.IsNullOrEmpty(filtro.clienteid) && filtro.clienteid!="0" )
                    {
                        foreach (var item in detenciones)
                        {
                            var sc = ControlSubcontrato.ObtenerPorId(item.ContratoId);
                            var c = ControlContrato.ObtenerPorId(sc.ContratoId);
                            if (c.ClienteId == Convert.ToInt32(filtro.clienteid))
                            {
                                det.Add(item);
                            }
                            detenciones = det;
                        }
                    }
                    var fechaInicial = new DateTime();
                    var fechaFinal = new DateTime();
                    fechaInicial = Convert.ToDateTime(filtro.Fechainical);
                    fechaFinal = Convert.ToDateTime(filtro.Fechafinal);
                    
                    if (filtro.SituacionId != "0")
                    {
                        List<Entity.DetalleDetencion> ListadetalldetencionFiltradaporSituacion = new List<Entity.DetalleDetencion>();
                        foreach (var item in detenciones)
                        {
                            var calificacion = ControlCalificacion.ObtenerPorInternoId(item.Id);
                            if (calificacion != null)
                            {
                                if (calificacion.SituacionId == Convert.ToInt32(filtro.SituacionId))
                                {
                                    ListadetalldetencionFiltradaporSituacion.Add(item);

                                }
                            }

                        }
                        detenciones = ListadetalldetencionFiltradaporSituacion;
                    }
                    if (filtro.motivoid != "0")
                    {
                        List<Entity.DetalleDetencion> Listafiltradapormotivos = new List<Entity.DetalleDetencion>();

                        foreach (var item in detenciones)
                        {
                            Entity.MotivoDetencionInterno motivointerno = new Entity.MotivoDetencionInterno(); ;
                            motivointerno.InternoId = item.Id;
                            var motivodetencioninterno = ControlMotivoDetencionInterno.GetListaMotivosDetencionByInternoId(motivointerno);
                            if (motivodetencioninterno.Where(x => x.MotivoDetencionId == Convert.ToInt32(filtro.motivoid)).ToList().Count > 0)
                            {
                                Listafiltradapormotivos.Add(item);
                            }
                        }

                        detenciones = Listafiltradapormotivos;
                    }

                    var dataArchivo = ControlPDFSalidasCustodiasOtrasDependencias.generarPDF(data, fechaInicial, fechaFinal, filtro.UtilizarHora, detenciones);

                    return new { exitoso = true, mensaje = "Impresión", Ubicacion = (dataArchivo != null) ? dataArchivo[1].ToString() : "", mensajeerror = (dataArchivo != null) ? dataArchivo[2].ToString() : "" };
                }
                else return new { exitoso = false, mensaje = "No cuenta con privilegios para realizar la acción." };
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message, Id = "", TrackingId = "" };
            }
        }
    }

    public class filtrosreportesestadisticos
    {
        public string Fechainical { get; set; }
        public string Fechafinal { get; set; }
        public string TipoIntoxicacionId { get; set; }
        public string motivoid { get; set; }
        public string clienteid { get; set; }
        public string contratoid { get; set; }
        public string subcontratoid { get; set; }
        public bool UtilizarHora { get; set; }
        public string TipoReporte { get; set; }
        public string ColoniaId { get; set; }
        public string CorporacionId { get; set; }
        public string UnidadId { get; set; }
        public string LesionId { get; set; }
        public string Edadinicial { get; set; }
        public string Edadfinal { get; set; }
        public string Remisioninicial { get; set; }
        public string Remisionfinal { get; set; }
        public string SituacionId { get; set; }
        public string TipoSalidaId { get; set; }
        public string MayoresEdad { get; set; }
        public string MenoresEdad { get; set; }
        public string JuezId { get; set; }
        public string ProcesoId { get; set; }
        public string SubprocesoId { get; set; }
        public string ResultadoId { get; set; }
    }

    public class filtrounidad
    {
        public string id { get; set; }
        public string corporacion { get; set; }
        public string clienteid { get; set; }
        public string contratoid { get; set; }
        public string subcontratoid { get; set; }
    }

    public class filtroedades
    {
        public bool MayoresEdad { get; set; }
        public bool MenoresEdad { get; set; }
    }
}