﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="reporteestadistico.aspx.cs" Inherits="Web.Application.Reportes_estadisticos.reporteestadistico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes estadísticos</li> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
        <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 style=" width:500px;" class="page-title txt-color-blueDark">
                <i class="fa fa-file-pdf-o "></i>
             Reportes estadísticos
            </h1>
        </div>
    </div>

    <div class="panel-group smart-accordion-default" id="accordion">
        <div class="panel panel-default">
	        <div class="panel-heading">
			    <h4 class="panel-title"><a data-toggle="collapse"  " data-parent="#accordion" href="#collapseLugar" class="collapsed"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Datos generales</a></h4>
		    </div>
			<div id="collapseLugar" class="panel-collapse collapse">
				<div class="panel-body">
				    <div class="row smart-form">
                        <section class="col-md-4">
                            <label class="label">Cliente</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="cliente" id="cliente"></select>
                                <i></i>
                            </label>
                        </section>
                        <section class="col-md-4">
                            <label class="label">Contrato</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="contrato" id="idcontratoreporteestadistico"></select>
                                <i></i>
                            </label>
                        </section>
                        <section class="col-md-4">
                            <label class="label">Subcontrato</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="subcontrato" id="subcontrato"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
				</div>
			</div>
		</div>
        <div class="panel panel-default">
	        <div class="panel-heading">
		        <h4 class="panel-title"><a data-toggle="collapse"  data-parent="#accordion" href="#colapseacciones" class="collapsed"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Reportes</a></h4>
	        </div>
	        <div id="colapseacciones" class="panel-collapse collapse">
		        <div class="panel-body">
			        <div class="row smart-form">
                        <section class="col-md-4">
                            <label class="label">Tipo de reporte</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="cliente" id="tiporeporte"></select>
                                <i></i>
                            </label>
                        </section>
                        <section class="col-md-4">
                            <label class="label">Reportes estadísticos</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="reportesestadisticos" id="idreportesestadisticos"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
			    </div>
		    </div>
	    </div>
        <div class="panel panel-default">
	        <div class="panel-heading">
		        <h4 class="panel-title"><a data-toggle="collapse"  data-parent="#accordion" href="#filtros" class="collapsed"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Filtros</a></h4>
		    </div>
		    <div id="filtros" class="panel-collapse collapse">
			    <div class="panel-body">
                    <div id="ContenidoFiltros"></div>
                    <div class="row">
                        <section class="col-md-5">
                            <label>Fecha inicial <a style="color: red">*</a></label>
                            <label class="input">
                                <div class='input-group date' id='vigenciaInicialDTP'>
                                    <input type="text" name="vigenciaInicialSub" id="vigenciaInicialSub" autocomplete="off" class='form-control' placeholder="Fecha inicial de la vigencia."/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </label>
                        </section>
                        <section class="col-md-5">
                            <label>Fecha final <a style="color: red">*</a></label>
                            <label class="input">
                                <div class='input-group date' id='vigenciaFinalSubDTP'>
                                    <input type="text" name="vigenciaFinalSub" id="vigenciaFinalSub" autocomplete="off" class='form-control' placeholder="Fecha final de la vigencia."/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </label>
                        </section>
                    </div>
				    <div class="row">
                        <section class="col-md-2">
                            <label class="checkbox" style="font-weight:bold; margin-left:20px;">
                                <input type="checkbox" name="utilizarHora" id="utilizarHora" value="true"/>
                                Utilizar hora
                            </label>
                        </section>
                    </div>
      				<div class="row smart-form" id="detenidosGradoIntoxicacionDiv">
                        <section class="col-md-8">
                            <label class="label">Intoxicación</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idTipoIntoxicacion" id="idTipoIntoxicacion"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="row" id="corporacionDiv">
                        <section class="col-md-8">
                            <label class="label">Corporación</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idCorporacion" id="idCorporacion"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="row smart-form" id="unidadDiv">
                        <section class="col-md-8">
                            <label class="label">Unidad</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idUnidad" id="idUnidad"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="row smart-form" id="motivosDiv">
                        <section class="col-md-8">
                            <label class="label">Motivos</label>
                            <select name="idmotivos" id="idmotivos" class="select2" style="width:100%;"></select>
                        </section>
                    </div>
                    <div class="row smart-form" id="coloniasDiv">
                        <section class="col-md-8">
                            <label class="label">Colonia</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idcolonia" id="idcolonia"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="row smart-form" id="resultadosDiv">
                        <section class="col-md-8">
                            <label class="label">Resultados</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idresultados" id="idresultados"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="row" id="mayor_menor_edadDiv">
                        <section class="col-md-12">
                            <label class="checkbox" style="font-weight:bold; margin-left:20px;">
                                <input type="checkbox" name="mayoresEdad" id="mayoresEdad" value="true"/>
                                Mayores de edad
                            </label>
                        </section>
                        <section class="col-md-12">
                            <label class="checkbox" style="font-weight:bold; margin-left:20px;">
                                <input type="checkbox" name="menoresEdad" id="menoresEdad" value="true"/>
                                Menores de edad
                            </label>
                        </section>
                    </div>
                    <div class="row" id="rangoedad">
                        <section class="col-6">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;Edad inicial <a style="color: red">*</a></label>
                            <input type="text" title="" id="edadinicial" pattern="^[0-9]*$" maxlength="3" />
                            
                            <label>&nbsp;Edad final <a style="color: red">*</a></label>
                            <input type="text" title="" id="edadfinal" pattern="^[0-9]*$" maxlength="3" />
                        </section>
                    </div>
                    <div class="row" id="rangoremision">
                        <section class="col-6">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;Remisión inicial <a style="color: red">*</a></label>
                            <input type="text" title="" id="remisioninicial" pattern="^[0-9]*$" maxlength="8" />
                        
                            <label>&nbsp;Remisión final <a style="color: red">*</a></label>
                            <input type="text" title="" id="remisionfinal" pattern="^[0-9]*$" maxlength="8" />
                        </section>
                    </div>
                    <div class="row smart-form" id="lesionDiv">
                        <section class="col-md-8">
                            <label class="label">Lesión</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idLesion" id="idLesion"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                     <div class="row smart-form" id="situaciondiv">
                         <section class="col-md-8">
                            <label class="label">Situación</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idsituacion" id="idsituacion"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="row smart-form" id="tiposalidadiv">
                        <section class="col-md-8">
                            <label class="label">Tipo salida</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idtiposalida" id="idtiposalida"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="row smart-form" id="juezdiv">
                        <section class="col-md-8">
                            <label class="label">Juez</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idJuez" id="idJuez"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="row smart-form" id="procesodiv">
                        <section class="col-md-4">
                            <label class="label">Proceso</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idJuez" id="proceso"></select>
                                <i></i>
                            </label>
                        </section>
                        <section class="col-md-4">
                            <label class="label">Subproceso</label>
                            <label class="select" style="margin-right: 10px">
                                <select name="idJuez" id="subproceso"></select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <br />
                    <footer>
                        <a class="btn btn-sm btn-success" id="generapdf"><i class="glyphicon glyphicon-file"></i>&nbsp;Generar PDF</a>&nbsp;
                        <%--<a class="btn btn-sm btn-default" id="verpantalla"><i class="fa fa-desktop bigger-120"></i>&nbsp;Ver pantalla</a>--%>
                        <%--<a class="btn btn-sm btn-default" id="excel"><i class="fa fa-file-excel-o bigger-120"></i>&nbsp;Excel /xls</a>--%>
                    </footer>
				</div>
			</div>
		</div>
    </div>
    <input type="hidden" id="hidcliente" />
    <input type="hidden" id="hidcontrato" />
    <input type="hidden" id="hidsubcontrato" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>  
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#idmotivos").select2();
            $("#idmotivos").next().children().children().css("padding-left", "5px").css("margin-right", "10px");

            $("#detenidosGradoIntoxicacionDiv").hide();
            $("#coloniasDiv").hide();
            $("#resultadosDiv").hide();
            $("#corporacionDiv").hide();
            $("#unidadDiv").hide();
            $("#mayor_menor_edadDiv").hide();
            $("#lesionDiv").hide();
            $("#rangoedad").hide();
            $("#rangoremision").hide();
            $("#situaciondiv").hide();
            $("#tiposalidadiv").hide();
            $("#juezdiv").hide();
            $("#procesodiv").hide();
            $("#verpantalla").attr("disabled", "disabled");
            $("#excel").attr("disabled", "disabled");
            $(document).on('keydown', 'input[pattern]', function (e) {
                var input = $(this);
                var oldVal = input.val();
                var regex = new RegExp(input.attr('pattern'), 'g');

                setTimeout(function () {
                    var newVal = input.val();
                    if (!regex.test(newVal)) {
                        input.val(oldVal);
                    }
                }, 0);
            });

            $("body").on("click", "#generapdf", function () {
                if (validar()) {
                    
                    if ($("#idreportesestadisticos").val() == 1) {
                        Reportecomparativomotivos();
                    }
                    else if ($("#idreportesestadisticos").val() == 2) {
                        ReporteDetalledetenidopormotivos();
                    }
                    else if ($("#idreportesestadisticos").val() == 3) {
                        ReporteDetalledetenidoporedad();
                    }
                    else if ($("#idreportesestadisticos").val() == 4) {
                        ReporteDetalledetenidoporcolonia();
                    }
                    else if ($("#idreportesestadisticos").val() == 5) {
                        ReporteDescuentoAgravanetJC();
                    }
                    else if ($("#idreportesestadisticos").val() == 6) {
                        ComparativodeLesionadosporunidad();
                    }
                    else if ($("#idreportesestadisticos").val() == 7) {
                        Detenidosportipodelesion();
                    }
                    else if ($("#idreportesestadisticos").val() == 8) {
                        ReporteDetenidosPorGradoIntoxicacion();
                    }
                    else if ($("#idreportesestadisticos").val() == 9) {
                        ReporteSalidasCustodiasOtrasDependencias();
                    }
                    else if ($("#idreportesestadisticos").val() == 10) {
                       
                        ReporteDetenidospordiadelasemana();
                    }
                    else if ($("#idreportesestadisticos").val() == 11) {
                        ReporteDetenidospormes();
                    }
                    else if ($("#idreportesestadisticos").val() == 12) {
                        ReporteDetalledetenidoporrangoedad();
                    }
                    else if ($("#idreportesestadisticos").val() == 13) {
                        ReporteDetalledetenidoporranremision();
                    }
                    else if ($("#idreportesestadisticos").val() == 14) {
                        RelacionDetenidosporCalificacion();
                    }
                    else if ($("#idreportesestadisticos").val() == 15) {
                        RelacionDetenidosporSalidaAutorizada();
                    }
                    else if ($("#idreportesestadisticos").val() == 17) {
                        DescriptivoDetenidosporrangoremision();
                    }
                     else if ($("#idreportesestadisticos").val() == 18) {
                        Trazabilidad();
                    }
                    else if ($("#idreportesestadisticos").val() == 19) {
                        ComparativodeLesionadosporunidadCL();
                    }

                   else if ($("#idreportesestadisticos").val() == 20) {
                        Resultadosquimicospordiadelasemana();
                    }
                    else if ($("#idreportesestadisticos").val() == 21) {
                        Detenidoporlesion();
                    }
                }
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            function Detenidosportipodelesion() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    LesionId: $("#idLesion").val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/Detenidosportipodelesion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();

                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function combotipoSalida(set) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/GetTipoSalida_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({
                        item: set
                    }),
                    success: function (response) {
                        var Dropdown = $("#idtiposalida");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Tipo salida]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function Detenidoporlesion() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $('#cliente').val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    CorporacionId: $("#idCorporacion").val(),
                    UnidadId: $("#idUnidad").val(),
                    MayoresEdad: $("#mayoresEdad").prop("checked"),
                    MenoresEdad: $("#menoresEdad").prop("checked"),
                    LesionId: $("#idLesion").val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/DetenidosPorLesionCL",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }


             function ComparativodeLesionadosporunidadCL() {
                var filtro = {LesionId: $("#idLesion").val(),
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    CorporacionId: $("#idCorporacion").val(),
                    UnidadId: $("#idUnidad").val(),
                    MayoresEdad: $("#mayoresEdad").prop("checked"),
                    MenoresEdad: $("#menoresEdad").prop("checked"),
                    LesionId: $("#idLesion").val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/ComparativodeLesionadosporunidadCL",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }


            function ComparativodeLesionadosporunidad() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    CorporacionId: $("#idCorporacion").val(),
                    UnidadId: $("#idUnidad").val(),
                    LesionId: $("#idLesion").val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/ComparativodeLesionadosporunidad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteDescuentoAgravanetJC() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    subcontratoid: $('#subcontrato').val(),
                    JuezId: $('#idJuez').val(),
                    motivoid: $('#idmotivos').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteId: $("#hidcliente").val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/DescuentoAgravanetJC",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function Reportecomparativomotivos() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    subcontratoid: $('#subcontrato').val(),
                    motivoid: $('#idmotivos').val(),
                    MayoresEdad: $("#mayoresEdad").prop("checked"),
                    MenoresEdad: $("#menoresEdad").prop("checked"),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteid: $('#cliente').val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/Comparativomotivos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function Resultadosquimicospordiadelasemana() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    subcontratoid: $('#subcontrato').val(),
                    ResultadoId: $('#idresultados').val(),
                    MayoresEdad: $("#mayoresEdad").prop("checked"),
                    MenoresEdad: $("#menoresEdad").prop("checked"),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteId: $('#cliente').val(),
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/Resultadosquimicospordiadelasemana",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamentee");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }



            function ReporteDetenidospordiadelasemana() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    subcontratoid: $('#subcontrato').val(),
                    motivoid: $('#idmotivos').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteid: $('#cliente').val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/Detenidospordiadelasemana",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamentee");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteDetenidospormes() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    subcontratoid: $('#subcontrato').val(),
                    motivoid: $('#idmotivos').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteid: $('#cliente').val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/Detenidospormes",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteDetalledetenidoporcolonia() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    ColoniaId: $('#idcolonia').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    MayoresEdad: $("#mayoresEdad").prop("checked"),
                    MenoresEdad: $("#menoresEdad").prop("checked")
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/Detenidosporcolonia",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteDetalledetenidoporedad() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    ColoniaId: $('#idcolonia').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    LesionId: $('#idLesion').val()
                };

                var filtro2 = {
                    MayoresEdad: $("#mayoresEdad").prop("checked"),
                    MenoresEdad: $("#menoresEdad").prop("checked")
                }

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/DetalleDetenidosporedad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro, 'filtro2': filtro2 }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteDetalledetenidoporranremision() {
                
                var sub = $('#subcontrato').val();
                
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $('#cliente').val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: sub,
                    ColoniaId: $('#idcolonia').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    Remisioninicial: $('#remisioninicial').val(),
                    Remisionfinal: $('#remisionfinal').val()
                };

                var isvalid = true;
                if (filtro.Remisioninicial == "") {
                    ShowAlert("¡Atención!", "El campo remisión incial es obligatorio");
                    isvalid = false;
                }
                if (filtro.Remisionfinal == "") {
                    ShowAlert("¡Atención!", "El campo remisión final es obligatorio");
                    isvalid = false;
                }
                if (filtro.Remisioninicial == "0") {
                    ShowAlert("¡Atención!", "El campo remisión incial debe ser mayor a 0");
                    isvalid = false;
                }
                if (filtro.Remisionfinal == "0") {
                    ShowAlert("¡Atención!", "El campo remisión final debe ser mayor a 0");
                    isvalid = false;
                }
                if (!isvalid) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/DetalleDetenidosporrangoremision",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }


       function Trazabilidad() {
                 
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    ProcesoId: $("#proceso").val(),
                    SubprocesoId: $("#subproceso").val(),
                    contratoId: $("#idcontratoreporteestadistico").val(),
                    subcontratoid: $('#subcontrato').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteId: $('#cliente').val(),
           };

           if ($('#subcontrato').val() == "0")
           {
               ShowError("Subcontrato", "Seleccione un subcontrato");
               return;
           }

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/Trazabilidad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }




            function DescriptivoDetenidosporrangoremision() {
                 
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    ColoniaId: $('#idcolonia').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    Remisioninicial: $('#remisioninicial').val(),
                    Remisionfinal: $('#remisionfinal').val()
                };

                var isvalid = true;
                if (filtro.Remisioninicial == "") {
                    ShowAlert("¡Atención!", "El campo remisión incial es obligatorio");
                    isvalid = false;
                }
                if (filtro.Remisionfinal == "") {
                    ShowAlert("¡Atención!", "El campo remisión final es obligatorio");
                    isvalid = false;
                }
                if (filtro.Remisioninicial == "0") {
                    ShowAlert("¡Atención!", "El campo remisión incial debe ser mayor a 0");
                    isvalid = false;
                }
                if (filtro.Remisionfinal == "0") {
                    ShowAlert("¡Atención!", "El campo remisión final debe ser mayor a 0");
                    isvalid = false;
                }
                if (!isvalid) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/DescriptivoDetenidosporrangoremision",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteDetalledetenidoporrangoedad() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    ColoniaId: $('#idcolonia').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    Edadinicial: $('#edadinicial').val(),
                    Edadfinal: $('#edadfinal').val()
                };

                var isvalid = true;
                if (filtro.Edadinicial == "") {
                    ShowAlert("¡Atención!", "El campo edad incial es obligatorio");
                    isvalid = false;
                }
                if (filtro.Edadfinal == "") {
                    ShowAlert("¡Atención!", "El campo edad final es obligatorio");
                    isvalid = false;
                }
                if (filtro.Edadinicial == "0") {
                    ShowAlert("¡Atención!", "El campo edad incial debe ser mayor a 0");
                    isvalid = false;
                }
                if (filtro.Edadfinal == "0") {
                    ShowAlert("¡Atención!", "El campo edad final debe ser mayor a 0");
                    isvalid = false;
                }
                if (!isvalid) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/DetalleDetenidosporrangoedad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function RelacionDetenidosporSalidaAutorizada() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    subcontratoid: $('#subcontrato').val(),
                    TipoSalidaId: $('#idtiposalida').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteId: $('#cliente').val(),
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/RelacionDetenidosporSalidaAutorizada",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function RelacionDetenidosporCalificacion() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    subcontratoid: $('#subcontrato').val(),
                    SituacionId: $('#idsitucacion').val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteId: $('#cliente').val(),
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/RelacionDetenidosporCalificacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteDetalledetenidopormotivos() {
                var filtro = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val(),
                    subcontratoid: $('#subcontrato').val(),
                    motivoid: $('#idmotivos').val(),
                    SituacionId: $("#idsituacion").val(),
                    MayoresEdad: $("#mayoresEdad").prop("checked"),
                    MenoresEdad: $("#menoresEdad").prop("checked"),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    clienteId: $('#cliente').val(),
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/DetalleDetenidospormotivos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            //setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            // setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteDetenidosPorGradoIntoxicacion() {
                var filtro = {
                    fechaInical: $("#vigenciaInicialSub").val(),
                    fechaFinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    tipoIntoxicacionId: $('#idTipoIntoxicacion').val(),
                    utilizarHora: $("#utilizarHora").prop("checked")
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/DetenidosGradoIntoxicacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ReporteSalidasCustodiasOtrasDependencias() {
                var filtro = {
                    fechaInical: $("#vigenciaInicialSub").val(),
                    fechaFinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    utilizarHora: $("#utilizarHora").prop("checked"),
                    SituacionId: $("#idsituacion").val(),
                    motivoid: $("#idmotivos").val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/SalidasCustodiasOtrasDependencias",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            ShowSuccess("Bien hecho!", "El reporte se generó correctamente");

                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function validar() {
                var esvalido = true;

                if ($("#idreportesestadisticos").val() == "0") {
                    ShowError("Reporte estadistico", "Seleccione un reporte estadístio.");
                    esvalido = false;
                }

                if ($("#vigenciaInicialSub").val() == "") {
                    ShowError("Fecha incial", "La fecha incial es obligatoria.");
                    esvalido = false;
                }

                if ($("#vigenciaFinalSub").val() == "") {
                    ShowError("Fecha final", "La fecha final es obligatoria.");
                    esvalido = false;
                }

                return esvalido;
            }

            function validarangofechas() {
                var esvalido = true;
                var filtro2 = {
                    Fechainical: $("#vigenciaInicialSub").val(),
                    Fechafinal: $('#vigenciaFinalSub').val()
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/Validarangofechas",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro2 }),
                    success: function (data) {
                        var resultado = data.d;
                        alert(resultado.exitoso);
                        return resultado.exitoso;
                        if (resultado.exitoso) {
                            //setTimeout(hideMessage, hideTime);
                            $('#main').waitMe('hide');
                            //location.reload();
                        }
                        else {
                            esvalido = false;
                        }
                    }
                });

                return esvalido;
            }

            loadCustomers("0");
       
         
            $("#idcontratoreporteestadistico").change(function () {
                $("#hidcontrato").val($(this).val());
                GetsubContracts("0");
                
                if ($("#idreportesestadisticos").val() == 3) {

                }
                else if ($("#idreportesestadisticos").val() == 4) {
                    getlistadocolonias("0");
                }
                else if ($("#idreportesestadisticos").val() == 6) {
                    getListadoCorporacion("0");
                    getListadoUnidad("0");
                }
                else if ($("#idreportesestadisticos").val() == 7) {
                    getListadoLesion("0");
                }
                else if ($("#idreportesestadisticos").val() == 8) {
                    getListadoTipoIntoxicacion("0");
                }
                else if ($("#idreportesestadisticos").val() == 9) {

                }
                else if ($("#idreportesestadisticos").val() == 19) {
                    //getListadoCorporacion("0");
                    getListadoUnidad("0");
                }
            else if ($("#idreportesestadisticos").val() == 20) {
                    //getListadoCorporacion("0");
                    GetResultadosQuimicos("0");
                }


                 else if ($("#idreportesestadisticos").val() == 21) {
                    //getListadoCorporacion("0");
                    getListadoUnidad("0");
                }

                else {
                    getListadoMotivos("0");
                }
            });

            GetReporttype("0");
            getListadoMotivos("0");

            function loadSituations(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getSituations",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idsituacion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todas]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de países. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }


            function changeReporteEstdistico(Id) {
                if (Id == 1) {
                    $("#mayor_menor_edadDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").show();
                    $("#coloniasDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 2) {
                    loadSituations("0");
                    getListadoMotivos("0");
                    $("#mayor_menor_edadDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").show();
                    $("#coloniasDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").show();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 3) {
                    getListadoLesion("0");
                    $("#mayor_menor_edadDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#lesionDiv").show();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 4) {
                    getlistadocolonias("0");
                    $("#coloniasDiv").show();
                    $("#mayor_menor_edadDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();

                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 5) {
                    getListadoMotivos("0");
                    getListadoJuez("0");
                    $("#motivosDiv").show();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();

                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").show();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 6) {
                    getListadoCorporacion("0");
                    getListadoUnidad("0");
                    getListadoLesion("0");
                    $("#lesionDiv").show();
                    $("#corporacionDiv").show();
                    $("#unidadDiv").show();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();

                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 7) {
                    $("#lesionDiv").show();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 8) {
                    getListadoTipoIntoxicacion("0");
                    $("#detenidosGradoIntoxicacionDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 9) {
                    loadSituations("0");
                    getListadoMotivos("0");
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").show();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").show();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 12) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").show();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 13) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").show();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 14) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    loadSituations("0");
                    $("#resultadosDiv").hide();
                    $("#situaciondiv").show();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                }
                else if (Id == 15) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#resultadosDiv").hide();
                    $("#situaciondiv").hide();
                    combotipoSalida("0");
                    $("#tiposalidadiv").show();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                }
                else if (Id == 17) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").show();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 18) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    loadProcesos(0);
                    $("#procesodiv").show();
                    $("#resultadosDiv").hide();
                }
                else if (Id == 19) {
                    getListadoLesion("0");
                    $("#lesionDiv").show();
                    $("#corporacionDiv").show();
                    $("#unidadDiv").show();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").show();
                    $("#resultadosDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                }
                else if (Id == 20) {
                    GetResultadosQuimicos("0");
                    $("#lesionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#resultadosDiv").show();
                    $("#mayor_menor_edadDiv").show();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                }
                else if (Id == 21) {
                    getListadoLesion("0");
                    $("#lesionDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#resultadosDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").show();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                }
                else {
                    getListadoMotivos("0");
                    $("#motivosDiv").show();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#resultadosDiv").hide();
                }
            }

            ////
            $("#idreportesestadisticos").change(function () {
                if ($(this).val() == 1) {
                    $("#mayor_menor_edadDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").show();
                    $("#coloniasDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 2) {
                    loadSituations("0");
                    getListadoMotivos("0");
                    $("#mayor_menor_edadDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").show();
                    $("#coloniasDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").show();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 3) {
                    getListadoLesion("0");
                    $("#mayor_menor_edadDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#lesionDiv").show();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 4) {
                    getlistadocolonias("0");
                    $("#coloniasDiv").show();
                    $("#mayor_menor_edadDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();

                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 5) {
                    getListadoMotivos("0");
                    getListadoJuez("0");
                    $("#motivosDiv").show();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();

                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").show();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 6) {
                    getListadoCorporacion("0");
                    getListadoUnidad("0");
                    getListadoLesion("0");
                    $("#lesionDiv").show();
                    $("#corporacionDiv").show();
                    $("#unidadDiv").show();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();

                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 7) {
                    $("#lesionDiv").show();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 8) {
                    getListadoTipoIntoxicacion("0");
                    $("#detenidosGradoIntoxicacionDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 9) {
                    loadSituations("0");
                    getListadoMotivos("0");
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").show();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").show();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 12) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").show();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 13) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").show();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 14) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    loadSituations("0");

                    $("#situaciondiv").show();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 15) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#resultadosDiv").hide();
                    $("#situaciondiv").hide();
                    combotipoSalida("0");
                    $("#tiposalidadiv").show();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                }
                else if ($(this).val() == 17) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").show();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 18) {
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    loadProcesos(0);
                    $("#procesodiv").show();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 19) {

                    getListadoLesion("0");
                    $("#lesionDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").show();

                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else if ($(this).val() == 20) {

                    GetResultadosQuimicos("0");
                    $("#lesionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#resultadosDiv").show();
                    $("#mayor_menor_edadDiv").show();

                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                }
                else if ($(this).val() == 21) {
                    getListadoLesion("0");
                    $("#lesionDiv").show();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#motivosDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").show();

                    $("#rangoedad").hide();
                    $("#rangoremision").hide();
                    $("#situaciondiv").hide();
                    $("#tiposalidadiv").hide();
                    $("#juezdiv").hide();
                    $("#procesodiv").hide();
                    $("#resultadosDiv").hide();
                }
                else {
                    getListadoMotivos("0");
                    $("#motivosDiv").show();
                    $("#resultadosDiv").hide();
                    $("#detenidosGradoIntoxicacionDiv").hide();
                    $("#corporacionDiv").hide();
                    $("#unidadDiv").hide();
                    $("#coloniasDiv").hide();
                    $("#mayor_menor_edadDiv").hide();
                    $("#lesionDiv").hide();
                    $("#rangoedad").hide();
                }
            });

            $("#cliente").change(function () {
                $("#hidcliente").val($("#cliente").val());
                $("#idcontratoreporteestadistico").val("0");
                $("#subcontrato").val("0");
                GetContracts("0");
                GetsubContracts("0");

                if ($("#idreportesestadisticos").val() == 3) {

                }
                else if ($("#idreportesestadisticos").val() == 4) {
                    getlistadocolonias("0");
                }
                else if ($("#idreportesestadisticos").val() == 6) {
                    getListadoCorporacion("0");
                    getListadoUnidad("0");
                }
                else if ($("#idreportesestadisticos").val() == 7) {
                    getListadoLesion("0");
                }
                else if ($("#idreportesestadisticos").val() == 8) {
                    getListadoTipoIntoxicacion("0");
                }
                else if ($("#idreportesestadisticos").val() == 9) {

                }
                else {
                    getListadoMotivos("0");
                }
            });

            $("#subcontrato").change(function () {
                $("#hidsubcontrato").val($(this).val());
                getlistadocolonias("0");
                if ($("#idreportesestadisticos").val() == 3) {

                }
                else if ($("#idreportesestadisticos").val() == 4) {
                    getlistadocolonias("0");
                }
                else if ($("#idreportesestadisticos").val() == 6) {
                    getListadoCorporacion("0");
                    getListadoUnidad("0");
                }
                else if ($("#idreportesestadisticos").val() == 7) {
                    getListadoLesion("0");
                }
                else if ($("#idreportesestadisticos").val() == 8) {
                    getListadoTipoIntoxicacion("0");
                }
                else if ($("#idreportesestadisticos").val() == 9) {

                }
                else {
                    getListadoMotivos("0");
                }
            });

            $("#tiporeporte").change(function () {
                GetReporteEstadistico("0");
            });

            $("#idCorporacion").change(function () {
                getListadoUnidad("0");
            });

            function getListadoMotivos(setvalue) {
                var filtro = {
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                     
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getListadoMotivos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ 'filtro': filtro }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idmotivos');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de reportes estadisticos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function getListadoTipoIntoxicacion(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getListadoTipoIntoxicacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idTipoIntoxicacion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de tipos de intoxicación. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function getListadoCorporacion(setvalue) {
                var filtro = {
                    id: $("#subcontrato").val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getListadoCorporacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ 'filtro': filtro }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idCorporacion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de corporaciones. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function getListadoUnidad(setvalue) {
                var filtro = {
                    id: $("#subcontrato").val(),
                    corporacion: $("#idCorporacion").val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getListadoUnidad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ 'filtro': filtro }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idUnidad');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function getListadoLesion(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getListadoLesion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idLesion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de lesiones. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function getListadoJuez(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getListadoJuez",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idJuez');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de jueces. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            $('#vigenciaInicialSub').datetimepicker({
                ampm: true,
               format: 'DD/MM/YYYY HH:mm'
            });

            $('#vigenciaFinalSub').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY HH:mm'
            });

            function getlistadocolonias(setvalue) {
                var filtro = {
                    fechaInical: $("#vigenciaInicialSub").val(),
                    fechaFinal: $('#vigenciaFinalSub').val(),
                    clienteId: $("#hidcliente").val(),
                    contratoId: $("#hidcontrato").val(),
                    subcontratoId: $("#hidsubcontrato").val(),
                    tipoIntoxicacionId: $('#idTipoIntoxicacion').val(),
                    utilizarHora: $("#utilizarHora").prop("checked")
                };

                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    cache: false,
                    data: JSON.stringify({ 'filtro': filtro }),
                    success: function (response) {
                        var Dropdown = $('#idcolonia');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todas]", "0"));

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de reportes estadisticos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function GetReporteEstadistico(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/GetReporteEstadistico",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ tiporeporteid: $("#tiporeporte").val() }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idreportesestadisticos');
                        Dropdown.children().remove();
                        var i = 0;
                        var id=0
                        Dropdown.append(new Option("[Reportes estadisticos]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                            i++;
                            id = item.Id;
                        });
                        console.log("i = " + i);
                        console.log("id = " + id);
                        if (i == 1)
                        {
                            setvalue = id;
                            changeReporteEstdistico(id);
                        }
                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de reportes estadisticos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            GetResultadosQuimicos("0");

            function GetResultadosQuimicos(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/GetResultadosQuimicos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idresultados');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de tipo de reportes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function GetReporttype(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/GetReporttype",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#tiporeporte');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de tipo de reportes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function GetsubContracts(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/GetSubcontracts",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Contratoid: $("#hidcontrato").val() }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#subcontrato');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Subcontratos]", "0"));
                        var id = 0;
                        var i = 0;
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                            i++;
                            id = item.Id;
                        });

                        if (i < 2)
                        {
                            setvalue = id;
                            $("#hidsubcontrato").val(id);
                        }
                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                        getlistadocolonias("0");
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de subcontratos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function GetContracts(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/GetContracts",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Clienteid: $("#hidcliente").val() }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idcontratoreporteestadistico');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Contrato]", "0"));
                        var i = 0;
                        var id = 0;
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                            id = item.Id;
                            i++;
                        });

                        if (i < 2)
                        {
                            setvalue = id;
                            $("#hidcontrato").val(id)
                            GetsubContracts("0");
                        }
                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                        getlistadocolonias("0");
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de contratos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
            function loadProcesos(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getProcesos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#proceso');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("Todos", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                        getlistadocolonias("0");
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clientes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
            $("#proceso").change(function () {

                loadSubproceso("0");
            });
            function loadSubproceso(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/GetSubprocesos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ procesoId: $("#proceso").val() }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#subproceso');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("Todos", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                        getlistadocolonias("0");
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clientes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadCustomers(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "reporteestadistico.aspx/getCustomers",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#cliente');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Cliente]", "0"));
                        var i = 0;
                        var id = 0;
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                             id= item.Id;
                            i++;
                        });

                        if (i < 2)
                        {
                            setvalue = id;
                            $("#hidcliente").val(id);
                            GetContracts("0");
                        }
                        
                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select");
                        }
                      
                        getlistadocolonias("0");
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clientes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
        });
    </script>
</asp:Content>
