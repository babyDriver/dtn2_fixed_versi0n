﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Business;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Web.Application.Shared
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
                //Response.AddHeader("Cache-control", "no-store, must-revalidate, private, no-cache");
                //Response.AddHeader("Pragma", "no-cache");
                //Response.AddHeader("Expires", "0");
                

                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());

                
                var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

                string ip = "";
                ip = HttpContext.Current.Session["ipusuario"].ToString();
                if (ip != userlogin.IPequipo)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                    return;

                }



                var usuario = ControlUsuario.Obtener(usId);
                var roles = ControlRol.ObtenerPorId(usuario.RolId);
                var idcontra = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                if (idcontra < 1)
                {
                    var id = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var contratos = ControlContratoUsuario.ObtenerPorUsuarioId(id);
                    var dataAux = contratos.Where(x => x.Activo == true && x.Tipo == "subcontrato").ToList();

                    if (dataAux.Count == 1)
                    {
                        var contrato = dataAux.ElementAt(0);
                        HttpContext.Current.Session["numeroContrato"] = contrato.Id;
                        id = contrato.Id;
                    }
                    else if (dataAux.Count == 0)
                    {
                        HttpContext.Current.Session["numeroContrato"] = 0;
                    }
                    else
                    {
                        HttpContext.Current.Session["numeroContrato"] = 0;
                    }
                }
                if (roles.name != "Administrador global"&& roles.name!= "Supervisor")
                {
                    validarContrato();
                }
                var membershipUser = Membership.GetUser(Page.User.Identity.Name);
                if (!string.IsNullOrEmpty(membershipUser.Comment))
                    lblUsurname.Text = membershipUser.Comment.ToString();
                else
                    lblUsurname.Text = membershipUser.UserName;
                
                    //string avatar = ControlUsuario.Obtener(new Guid(membershipUser.ProviderUserKey.ToString())).Avatar;
                    string avatar = ControlUsuario.Obtener(Convert.ToInt16(membershipUser.ProviderUserKey)).Avatar;
                    if (!string.IsNullOrEmpty(avatar))
                    {
                        imgUsuario.Src = avatar.Replace(".jpg", ".thumb").Replace(".JPEG", ".thumb").Replace(".JPG", ".thumb");
                        imgUsuarioMobile.Src = avatar.Replace(".jpg", ".thumb").Replace(".JPEG", ".thumb").Replace(".JPG", ".thumb");
                    }
                    else
                    {
                        imgUsuario.Src = string.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                        imgUsuarioMobile.Src = string.Concat(ConfigurationManager.AppSettings["relativepath"], "Content/img/avatars/male.png");
                    }

                    //int usuarioId = ControlUsuario.Obtener(new Guid(membershipUser.ProviderUserKey.ToString())).Id;
                    int usuarioId = ControlUsuario.Obtener(Convert.ToInt16(membershipUser.ProviderUserKey)).Id;
                    getPermisos();
            //}
            
        }
        public void getPermisos()
        {
            var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            int usuario;
            if (membershipUser != null)
            {
                usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                //La forma en la que esta hecho ObtenerPermisos_GetByUsuarioIdPantalla_SP 
                string[] parametros = { usuario.ToString(), "Administración" };
                var permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    adminhidden.Value = "true";
                }

                parametros[1] = "Administrar examen médico";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    var parametroContratos = ControlParametroContrato.TraerTodos();
                    int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);

                    var contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);

                    Entity.ParametroContrato param2 = new Entity.ParametroContrato();

                    param2 = parametroContratos.LastOrDefault(x => x.Parametroid == Convert.ToInt32(Entity.ParametroEnum.ADMINISTRAR_EXAMEN_MEDICO) && x.ContratoId == contratoUsuario.IdContrato);

                    if (param2 != null)
                    {
                        if (param2.Valor == "1")
                        {
                            administrarexamenmedicohidden.Value = "true";
                        }
                    }

                }

                parametros[1] = "Registro en barandilla";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    registryhidden.Value = "true";
                }

                parametros[1] = "Información de Detenido";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    informaciondetenidohidden.Value = "true";
                }
                
                parametros[1] = "Juez calificador";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    controldetenidoshidden.Value = "true";
                }
                parametros[1] = "Detenidos Amparo";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    DetenidosAmparohiden.Value = "true";
                }

                parametros[1] = "Uso del sistema";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    UsoSistemahidden.Value = "true";
                }

                parametros[1] = "Configuración y seguridad";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    configurationhidden.Value = "true";
                }

                parametros[1] = "Control de pertenencias/evidencias";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    pertenenciashidden.Value = "true";
                }

                parametros[1] = "Examen médico";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    examenmedicohidden.Value = "true";
                }

                parametros[1] = "Caja";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    cajahidden.Value = "true";
                }

                parametros[1] = "Llamadas y eventos";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    eventoshidden.Value = "true";
                }

                parametros[1] = "Trabajo social";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    socialhidden.Value = "true";
                }

                //what if
                parametros[1] = "Control de celdas";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    controlceldashidden.Value = "true";
                }

                parametros[1] = "Defensor público";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    defensorpublicohidden.Value = "true";
                }

                parametros[1] = "Formato referencia";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    policiacohidden.Value = "true";
                }

                parametros[1] = "Buscador avanzado";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    BuscadorAvanzadohidden.Value = "true";
                    
                }
                parametros[1] = "Actividad de usuarios";
                permisos = ControlUsuarioPantalla.ObtenerPorUsuarioIdPantallaId(parametros);
                if (permisos != null && permisos.Consultar)
                {
                    ActividadUsuarioshidden.Value = "true";

                }

                var us = ControlUsuario.Obtener(usuario);
                var roles = ControlRol.ObtenerPorId(us.RolId);
                if (roles.name == "Administrador global" || roles.name == "Supervisor")
                {
                    globalhidden.Value = "true";
                }
                if (roles.name == "Supervisor")
                {
                    Supervisorhidden.Value = "true";
                }
                else
                {
                    Supervisorhidden.Value = "false";

                }
            }
        }
        [WebMethod]
        public static object GetAlertaWeb()
        {
            try
            {
                var datosAlertaWeb = ControlAlertaWeb.ObtenerTodos().FirstOrDefault();
                var denoma = "Alerta web";
                var alerta = "alerta web";
                if (!string.IsNullOrEmpty(datosAlertaWeb.Denominacion))
                {
                    denoma = datosAlertaWeb.Denominacion;
                    alerta = denoma;
                }
                object data = new
                {
                    
                    Denominacion = denoma,
                    AlertaWerb = alerta
                };

                return data;
            }
            catch (Exception ex)
            {
                return new { exitoso = false, mensaje = ex.Message };
            }
        }
        public void validarContrato()
        {
            int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
            
            if(idContrato == 0)
            {
                if(!HttpContext.Current.Request.Path.EndsWith("dashboard.aspx", StringComparison.InvariantCultureIgnoreCase))
                {
                    System.Web.HttpContext.Current.Session.RemoveAll();
                    System.Web.HttpContext.Current.Response.Redirect("../dashboard.aspx");
                }                
            }
        }
    }
}