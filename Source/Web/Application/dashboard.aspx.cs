﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Business;
using Entity;
using Entity.Util;
using System.Data;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Web.Security;
using System.Web.Services;
using System.Configuration;

namespace Web.Application
{
    public partial class dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int idContrato = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                var usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var userlogin = ControlUsuario_login.ObtenerPorusuarioId(usId);

                string ip = "";
                ip = HttpContext.Current.Session["ipusuario"].ToString();
                if (ip != userlogin.IPequipo)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();

                }

                var contratoAsignado = ControlContratoUsuario.ObtenerPorContratoYUsuario(new object[]
                {
                    Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString()),
                    idContrato,
                    "subcontrato"
                });

                if (contratoAsignado != null)
                {
                    if (!contratoAsignado.Activo)
                    {
                        idContrato = 0;
                    }
                }

                if (!(idContrato != 0))
                {
                    int id = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                    var contratos = ControlContratoUsuario.ObtenerPorUsuarioId(id);
                    var dataAux = contratos.Where(x => x.Activo == true && x.Tipo == "subcontrato").ToList();

                    if (dataAux.Count == 1)
                    {
                        var contrato = dataAux.ElementAt(0);
                        HttpContext.Current.Session["numeroContrato"] = contrato.Id;
                    }
                    else if (dataAux.Count == 0)
                    {
                        HttpContext.Current.Session["numeroContrato"] = 0;
                    }
                    else
                    {
                        HttpContext.Current.Session["numeroContrato"] = 0;
                    }
                }

                var usuario = ControlUsuario.Obtener(usId);
                if (usuario != null)
                {
                    List<Entity.UsuarioReportes> permisosReportes = ControlUsuarioReportes.ObtenerPorUsuarioId(usuario.Id);
                    if (permisosReportes.Count == 0)
                    {
                        var reportesId = ControlReportes.ObtenerTodos().Select(x => x.Id);
                        foreach (var id in reportesId)
                        {
                            var permisoReporte = new Entity.UsuarioReportes();
                            if (usuario.RolId == 1 || usuario.RolId == 2 || usuario.RolId == 13)
                            {
                                permisoReporte = new Entity.UsuarioReportes()
                                {
                                    UsuarioId = usuario.Id,
                                    ReporteId = id,
                                    Activo = true,
                                    Habilitado = true
                                };
                            }
                            else
                            {
                                permisoReporte = new Entity.UsuarioReportes()
                                {
                                    UsuarioId = usuario.Id,
                                    ReporteId = id,
                                    Activo = false,
                                    Habilitado = false
                                };
                            }

                            ControlUsuarioReportes.Guardar(permisoReporte);
                        }
                    }
                }
            }
        }

        [WebMethod]
        public static string ValidateBandera()
        {
            try
            {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                int usuario;

                if (membershipUser != null) usuario = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");
                var contra = false;
                var user = ControlUsuario.Obtener(usuario);
                if (!user.BanderaPassword)
                {
                    contra = true;
                }
                if (!string.IsNullOrEmpty(user.Email))
                {
                    contra = false;
                }
                return JsonConvert.SerializeObject(new { exitoso = true, valido = contra });
            }
            catch (MySqlException ex)
            {

                return JsonConvert.SerializeObject(new { exitoso = false, mensaje = ex.Message });
            }
        }

        [WebMethod]
        public static object savepassword(string password)
        {
            try
            {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                int iduser;

                if (membershipUser != null) iduser = Convert.ToInt32(membershipUser.ProviderUserKey);
                else throw new Exception("No es posible obtener información del usuario en el sistema");

                var user = ControlUsuario.Obtener(iduser);

                user.UltimaActualizacion = DateTime.Now;

                var usuarioms = Membership.GetUser(user.User);
                usuarioms.UnlockUser();
                usuarioms.Comment = user.Nombre.Trim() + " " + user.ApellidoPaterno.Trim() + " " + user.ApellidoMaterno.Trim();
                usuarioms.ChangePassword(usuarioms.GetPassword(), password);
                Membership.UpdateUser(usuarioms);

                user.BanderaPassword = true;
                ControlUsuario.Actualizar(user);

                return new { exitoso = true, mensaje = "actualizó" };

            }
            catch (MySqlException ex)
            {

                return new { exitoso = false, mensaje = ex.Message };
            }
        }

        [WebMethod]
        public static string gettotal(string centroId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;
                    int _centroId = 0;

                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }

                    if (_centroId > 0)
                    {
                        command.CommandText = "Ingresos_GetTotalCentro_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }

                    else
                        command.CommandText = "Ingresos_GetTotal_SP";
                    command.CommandType = CommandType.StoredProcedure;


                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    int hombres = 0;
                    int mujeres = 0;
                    int cupoHombres = 0;
                    int cupoMujeres = 0;
                    int totalInternos = 0;
                    int sindato = 0;
                    using (dr)
                    {
                        while (dr.Read())
                        {
                            if (dr["Nombre"].ToString() == "Masculino")
                            {
                                hombres += Convert.ToInt32(dr["total"].ToString());
                            }
                            if (dr["Nombre"].ToString() == "Femenino")
                            {
                                mujeres += Convert.ToInt32(dr["total"].ToString());
                            }
                            if (dr["Nombre"].ToString() == "Cupo")
                            {
                                cupoHombres += Convert.ToInt32(dr["cupoHombres"].ToString());
                                cupoMujeres += Convert.ToInt32(dr["cupoMujeres"].ToString());
                            }
                            if (dr["Nombre"].ToString() == "Total")
                            {
                                totalInternos += Convert.ToInt32(dr["total"].ToString());

                            }
                        }
                    }

                    if (mujeres + hombres < totalInternos)
                    {
                        sindato = totalInternos - (mujeres + hombres);
                    }

                    var porcentajeocupacion = 0;
                    if (cupoHombres > 0 || cupoMujeres > 0)
                        porcentajeocupacion = (totalInternos * 100) / (cupoHombres + cupoMujeres);

                    object datos = new
                    {
                        hombres = hombres,
                        mujeres = mujeres,
                        internos = totalInternos,
                        sindefinir = sindato,
                        porcentajeocupacion = porcentajeocupacion
                    };

                    return JsonConvert.SerializeObject(datos);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }


        [WebMethod]
        public static string gettotalIngresos(string centroId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;
                    int _centroId = 0;
                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }


                    if (_centroId > 0)
                    {
                        command.CommandText = "Ingresos_GetTotalCentroIngresos_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }
                    else
                        command.CommandText = "Ingresos_GetTotalIngresos_SP";
                    command.CommandType = CommandType.StoredProcedure;


                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    int hombres = 0;
                    int mujeres = 0;
                    int cupoHombres = 0;
                    int cupoMujeres = 0;
                    int totalInternos = 0;
                    int sindato = 0;
                    using (dr)
                    {
                        while (dr.Read())
                        {
                            if (dr["Nombre"].ToString() == "Masculino")
                            {
                                hombres += Convert.ToInt32(dr["total"].ToString());
                            }
                            if (dr["Nombre"].ToString() == "Femenino")
                            {
                                mujeres += Convert.ToInt32(dr["total"].ToString());
                            }
                            if (dr["Nombre"].ToString() == "Cupo")
                            {
                                cupoHombres += Convert.ToInt32(dr["cupoHombres"].ToString());
                                cupoMujeres += Convert.ToInt32(dr["cupoMujeres"].ToString());
                            }
                            if (dr["Nombre"].ToString() == "Total")
                            {
                                totalInternos += Convert.ToInt32(dr["total"].ToString());

                            }
                        }
                    }

                    if (mujeres + hombres < totalInternos)
                    {
                        sindato = totalInternos - (mujeres + hombres);
                    }

                    var porcentajeocupacion = 0;
                    if (cupoHombres > 0 || cupoMujeres > 0)
                        porcentajeocupacion = (totalInternos * 100) / (cupoHombres + cupoMujeres);

                    object datos = new
                    {
                        hombres = hombres,
                        mujeres = mujeres,
                        internos = totalInternos,
                        sindefinir = sindato,
                        porcentajeocupacion = porcentajeocupacion
                    };

                    return JsonConvert.SerializeObject(datos);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }


        [WebMethod]
        public static string getingresos(string centroId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;
                    int _centroId = 0;
                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }


                    if (_centroId > 0)
                    {
                        command.CommandText = "Ingresos_GetByYearCentro_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }
                    else
                        command.CommandText = "Ingresos_GetByYear_SP";
                    command.CommandType = CommandType.StoredProcedure;


                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    using (dr)
                    {
                        while (dr.Read())
                        {
                            object datos = new
                            {
                                anio = dr["anio"],
                                total = dr["total"]

                            };
                            data.Add(datos);
                        }
                    }
                    return JsonConvert.SerializeObject(data);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }


        [WebMethod]
        public static string getjuridica(string centroId)
        {
            //var usuario = ControlUsuario.Obtener(GetUser(HttpContext.Current.User.Identity.Name));
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;

                    int _centroId = 0;
                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }


                    if (_centroId > 0)
                    {
                        command.CommandText = "Interno_GetBySituacionJuridicaCentro_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }
                    else
                        command.CommandText = "Interno_GetBySituacionJuridica_SP";

                    command.CommandType = CommandType.StoredProcedure;


                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    using (dr)
                    {
                        while (dr.Read())
                        {
                            object datos = new
                            {
                                situacion = dr["situacion"],
                                total = dr["total"]

                            };
                            data.Add(datos);
                        }
                    }
                    return JsonConvert.SerializeObject(data);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }
        [WebMethod]
        public static string getfuero(string centroId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;

                    int _centroId = 0;
                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }


                    if (_centroId > 0)
                    {
                        command.CommandText = "Interno_GetByFueroCentro_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }
                    else
                        command.CommandText = "Interno_GetByFuero_SP";

                    command.CommandType = CommandType.StoredProcedure;


                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    using (dr)
                    {
                        while (dr.Read())
                        {
                            object datos = new
                            {
                                fuero = dr["fuero"],
                                total = dr["total"],

                            };
                            data.Add(datos);
                        }
                    }
                    return JsonConvert.SerializeObject(data);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static string getestadocivil(string centroId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;
                    int _centroId = 0;
                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }


                    if (_centroId > 0)
                    {
                        command.CommandText = "Interno_GetByEstadoCivilCentro_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }
                    else
                        command.CommandText = "Interno_GetByEstadoCivil_SP";

                    command.CommandType = CommandType.StoredProcedure;

                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    using (dr)
                    {
                        while (dr.Read())
                        {
                            object datos = new
                            {
                                estadocivil = dr["estadocivil"],
                                total = dr["total"]

                            };
                            data.Add(datos);
                        }
                    }
                    return JsonConvert.SerializeObject(data);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static string getetnia(string centroId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;
                    int _centroId = 0;
                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }


                    if (_centroId > 0)
                    {
                        command.CommandText = "Interno_GetByEtniaCentro_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }
                    else
                        command.CommandText = "Interno_GetByEtnia_SP";
                    command.CommandType = CommandType.StoredProcedure;


                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    using (dr)
                    {
                        while (dr.Read())
                        {
                            object datos = new
                            {
                                etnia = dr["etnia"],
                                total = dr["total"]

                            };
                            data.Add(datos);
                        }
                    }
                    return JsonConvert.SerializeObject(data);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static string getreligion(string centroId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;
                    int _centroId = 0;
                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }


                    if (_centroId > 0)
                    {
                        command.CommandText = "Interno_GetByReligionCentro_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }
                    else
                        command.CommandText = "Interno_GetByReligion_SP";

                    command.CommandType = CommandType.StoredProcedure;


                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    using (dr)
                    {
                        while (dr.Read())
                        {
                            object datos = new
                            {
                                religion = dr["religion"],
                                total = dr["total"]


                            };
                            data.Add(datos);
                        }
                    }
                    return JsonConvert.SerializeObject(data);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }


        [WebMethod]
        public static string getnacionalidad(string centroId)
        {
            using (MySqlConnection mysqlConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString))
            {
                try
                {
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = mysqlConnection;
                    int _centroId = 0;
                    if (centroId != null && centroId != "")
                    {
                        _centroId = Convert.ToInt32(centroId);
                    }


                    if (_centroId > 0)
                    {
                        command.CommandText = "Interno_GetByExtranjerosCentro_SP";
                        command.Parameters.AddWithValue("_CentroId", centroId);
                    }
                    else
                        command.CommandText = "Interno_GetByExtranjeros_SP";

                    command.CommandType = CommandType.StoredProcedure;


                    MySqlDataReader dr;
                    mysqlConnection.Open();
                    dr = command.ExecuteReader();
                    List<object> data = new List<object> { };
                    using (dr)
                    {
                        while (dr.Read())
                        {

                            object datos = new
                            {
                                etnia = dr["nacionalidad"],
                                total = dr["total"]


                            };
                            data.Add(datos);
                        }
                    }
                    return JsonConvert.SerializeObject(data);
                }
                catch (MySqlException ex)
                {

                    return "[\"error\":true,\"detalles\":\"" + ex.ToString() + "\"]";
                }

                finally
                {
                    if (mysqlConnection.State == ConnectionState.Open)
                        mysqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public static List<Combo> getInstituciones()
        {
            var combo = new List<Combo>();
            var perfil = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name)[0];
            if (!string.Equals(perfil, "Administrador"))
            {
                var membershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                //int usuario = Convert.ToInt32(membershipUser.ProviderUserKey);                
            }
            else
            {
                List<Entity.Institucion> listado = ControlInstitucion.ObteneTodos();
                combo.Add(new Combo { Desc = "Todos", Id = "0" });
                if (listado.Count > 0)
                {
                    foreach (var rol in listado)
                        combo.Add(new Combo { Desc = rol.Nombre, Id = rol.Id.ToString() });
                }
            }
            return combo;
        }

        [WebMethod]
        public static string getdelitos(string year)
        {
            int id = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);

            //var listado = ControlDashboardDetenidos.GetAll();
            //int detenidoshombres = 0;
            var contratousuario = ControlContratoUsuario.ObtenerPorId(id);

            var dashmotivos = ControlDashboardMotivosDetencion.Get_List_By_ContratoId(contratousuario.IdContrato);
            List<object> data = new List<object> { };

            foreach (var item in dashmotivos)
            {
                var moti = "";
                if (item.Motivo.Length > 20)
                {
                    moti = item.Motivo.Substring(0, 20);
                }
                else
                {
                    moti = item.Motivo;
                }
                object datos = new
                {
                    Cantidad = item.Cantidad,

                    Motivo = moti
                };
                data.Add(datos);
            }

            return JsonConvert.SerializeObject(data);

        }

        [WebMethod]
        public static List<Combo> getContracts()
        {
            int id = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
            var combo = new List<Combo>();
            var c = ControlContratoUsuario.ObtenerPorUsuarioId(id);

            var cAux = c.Where(x => x.Activo == true && x.Tipo == "subcontrato").ToList();

            foreach (var item in cAux)
            {
                //var contrato = new Entity.Contrato();
                var subcontrato = new Entity.Subcontrato();
                subcontrato = ControlSubcontrato.ObtenerPorId(item.IdContrato);
                combo.Add(new Combo { Desc = subcontrato.Nombre + " - " + item.Tipo, Id = item.Id.ToString() });

            }

            return combo;
        }
        public static string nombredelmes(int mes)
        {
            switch (mes)
            {
                case 1:
                    return "ENERO";

                case 2:
                    return "FEBRERO";
                case 3:
                    return "MARZO";
                case 4:
                    return "ABRIL";
                case 5:
                    return "MAYO";
                case 6:
                    return "JUNIO";
                case 7:
                    return "JULIO";
                case 8:
                    return "AGOSTO";
                case 9:
                    return "SEPTIEMBRE";
                case 10:
                    return "OCTUBRE";
                case 11:
                    return "NOVIEMBRE";
                case 12:
                    return "DICIEMBRE";
                default:
                    return "";

            }


        }


        [WebMethod]
        public static List<Combo> LoadDashboardAniosTrabajo()
        {
            List<Combo> combo = new List<Combo>();
            var listaanios = ControlDashBoardAnioTrabajo.GetAniosTrabajo();
            List<int> Anios = new List<int>();
            foreach (var item in listaanios)
            {
                Anios.Add(item.Aniotrabajo);
            }

            if (Anios.Count == 0 || DateTime.Now.Year > Anios.Max())
            {
                Entity.DasboardAnioTrabajo dasboardAnioTrabajo = new DasboardAnioTrabajo();
                dasboardAnioTrabajo.Aniotrabajo = DateTime.Now.Year;
                listaanios.Add(dasboardAnioTrabajo);
            }
            foreach (var item in listaanios.OrderByDescending(x => x.Aniotrabajo))
            {
                combo.Add(new Combo { Desc = item.Aniotrabajo.ToString(), Id = item.Aniotrabajo.ToString() });

            }

            return combo;
        }

        [WebMethod]
        public static string LoadDashboard(string anio)
        {
            string log = "";
            try
            {
                if (string.IsNullOrEmpty(anio))
                {
                    anio = DateTime.Now.Year.ToString();
                }

                int id = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);
                var sexos = ControlCatalogo.ObtenerTodo(4);
                var masculinoId = sexos.FirstOrDefault(x => x.Nombre.ToUpper() == "Masculino".ToUpper());
                var femeninoId = sexos.FirstOrDefault(x => x.Nombre.ToUpper() == "Femenino".ToUpper());
                if (masculinoId == null && id != 0) return JsonConvert.SerializeObject(new { success = false, message = "No se encontro el sexo 'Masculino' en el sistema. Favor de agregarlo en la sección Administración - Sexo." });
                if (femeninoId == null && id != 0) return JsonConvert.SerializeObject(new { success = false, message = "No se encontro el sexo 'Femenino' en el sistema. Favor de agregarlo en la sección Administración - Sexo." });


                var listado = ControlDashboardDetenidos.GetByAnio(Convert.ToInt32(anio));

                var listadoenTS = ControlDashboardDetenidos.ObtenerEnTrabajoSocial(Convert.ToInt32(anio));

                if (listadoenTS != null)
                {
                    if (listadoenTS.Count > 0)
                    {
                        listado.AddRange(listadoenTS);
                    }

                }
                var contratousuario = ControlContratoUsuario.ObtenerPorId(id);

                var listadomesactua = listado.Where(x => x.Fecha.Month == DateTime.Now.Month).ToList();
                var detenidoshombres = listado.Where(x => x.SexoId == masculinoId.Id && x.ContratoId == contratousuario.IdContrato && x.Activo == 1 && x.Estatus == 1).Count();
                var detenidosmujeres = listado.Where(x => x.SexoId == femeninoId.Id && x.ContratoId == contratousuario.IdContrato && x.Activo == 1 && x.Estatus == 1).Count();

                var detenidoshombresh = listado.Where(x => x.SexoId == masculinoId.Id && x.ContratoId == contratousuario.IdContrato).Count();
                var detenidosmujeresh = listado.Where(x => x.SexoId == femeninoId.Id && x.ContratoId == contratousuario.IdContrato).Count();

                var hts = listado.Where(x => x.SexoId == masculinoId.Id && x.ContratoId == contratousuario.IdContrato && x.Activo == 1 && x.TrabajosocialId != 0 && x.Estatus == 1).Count();
                var mts = listado.Where(x => x.SexoId == femeninoId.Id && x.ContratoId == contratousuario.IdContrato && x.Activo == 1 && x.TrabajosocialId != 0 && x.Estatus == 1).Count();
                var detenidoshombresmesactual = listadomesactua.Where(x => x.SexoId == masculinoId.Id && x.ContratoId == contratousuario.IdContrato && x.Activo == 1 && x.Estatus == 1).Count();
                var detenidosmujeresmesactual = listadomesactua.Where(x => x.SexoId == femeninoId.Id && x.ContratoId == contratousuario.IdContrato && x.Activo == 1 && x.Estatus == 1).Count();

                var detenidoshombresmesactualh = listadomesactua.Where(x => x.SexoId == masculinoId.Id && x.ContratoId == contratousuario.IdContrato).Count();
                var detenidosmujeresmesactualh = listadomesactua.Where(x => x.SexoId == femeninoId.Id && x.ContratoId == contratousuario.IdContrato).Count();

                log = "obtine datos de graficas de detenidos,historicos y mes actual\n";


                string nombremes = nombredelmes(Convert.ToInt32(DateTime.Now.Month)) + " " + DateTime.Now.Year.ToString();
                log += "obtine el nombre del mes\n";
                var fechainical = DateTime.Now;
                var fechafinal = DateTime.Now;
                var diadelasemana = DateTime.Now.DayOfWeek;
                log += "obtine el nombre de la semana \n";

                switch (diadelasemana)
                {
                    case DayOfWeek.Sunday:
                        fechainical = fechainical.AddDays(-6);
                        fechafinal = DateTime.Now;
                        break;
                    case DayOfWeek.Monday:
                        fechainical = DateTime.Now;
                        fechafinal = DateTime.Now.AddDays(6);
                        break;
                    case DayOfWeek.Tuesday:
                        fechainical = fechainical.AddDays(-1);
                        fechafinal = fechafinal.AddDays(5);
                        break;
                    case DayOfWeek.Wednesday:
                        fechainical = fechainical.AddDays(-2);
                        fechafinal = fechafinal.AddDays(4);
                        break;
                    case DayOfWeek.Thursday:
                        fechainical = fechainical.AddDays(-3);
                        fechafinal = fechafinal.AddDays(3);
                        break;
                    case DayOfWeek.Friday:
                        fechainical = fechainical.AddDays(-4);
                        fechafinal = fechafinal.AddDays(2);
                        break;
                    case DayOfWeek.Saturday:
                        fechainical = fechainical.AddDays(-5);
                        fechafinal = fechafinal.AddDays(1);
                        break;

                    default:
                        fechainical = DateTime.Now;
                        fechafinal = DateTime.Now;
                        break;
                }



                string[] fechas = new string[] { fechainical.ToString(), fechafinal.ToString() };
                List<Entity.DashboarDetenidos> detenidossemanaactuak = new List<DashboarDetenidos>();
                int detenidoshombressemanaactual = 0;
                int detenidosmujeressemanaactual = 0;
                int detenidoshombressemanaactualh = 0;
                int detenidosmujeressemanaactualh = 0;

                string nombresemana = "";

                log += "genera consulta de la semana\n ";
                detenidossemanaactuak = ControlDashboardDetenidos.GetbyFechas(fechas);
                log += " resultados de la consulta de la semana\n ";
                detenidoshombressemanaactual = detenidossemanaactuak.Count == 0 ? 0 : detenidossemanaactuak.Where(x => x.SexoId == masculinoId.Id && x.ContratoId == contratousuario.IdContrato && x.Activo == 1 && x.Estatus == 1).Count();
                detenidosmujeressemanaactual = detenidossemanaactuak.Count == 0 ? 0 : detenidossemanaactuak.Where(x => x.SexoId == femeninoId.Id && x.ContratoId == contratousuario.IdContrato && x.Activo == 1 && x.Estatus == 1).Count();

                detenidoshombressemanaactualh = detenidossemanaactuak.Count == 0 ? 0 : detenidossemanaactuak.Where(x => x.SexoId == masculinoId.Id && x.ContratoId == contratousuario.IdContrato).Count();
                detenidosmujeressemanaactualh = detenidossemanaactuak.Count == 0 ? 0 : detenidossemanaactuak.Where(x => x.SexoId == femeninoId.Id && x.ContratoId == contratousuario.IdContrato).Count();


                nombresemana = "DEL " + fechainical.ToShortDateString().ToString() + " AL " + fechafinal.ToShortDateString().ToString();

                log += " formato nombre de la semana semana\n ";




                //return JsonConvert.SerializeObject(new { success = true, message = "", contratoId = id, hombresdetenidos = detenidoshombres, Mujeresdetenidas = detenidosmujeres, Totaldetenidos = detenidoshombres + detenidosmujeres, TotalPersonas = hts + mts, HTS = hts, MTS = mts, hombresdetenidosh = detenidoshombresh, Mujeresdetenidash = detenidosmujeresh, Totaldetenidosh = detenidoshombresh + detenidosmujeresh, hombresdetenidosmesactual = detenidoshombresmesactual, Mujeresdetenidasmesactual = detenidosmujeresmesactual, Totaldetenidosmesactual = detenidoshombresmesactual + detenidosmujeresmesactual, nombredelmes = nombremes, hombresdetenidosmesactualh = detenidoshombresmesactualh, Mujeresdetenidasmesactualh = detenidosmujeresmesactualh });
                try
                {
                    return JsonConvert.SerializeObject(new { success = true, message = "", contratoId = id, hombresdetenidos = detenidoshombres, Mujeresdetenidas = detenidosmujeres, Totaldetenidos = detenidoshombres + detenidosmujeres, TotalPersonas = hts + mts, HTS = hts, MTS = mts, hombresdetenidosh = detenidoshombresh, Mujeresdetenidash = detenidosmujeresh, Totaldetenidosh = detenidoshombresh + detenidosmujeresh, hombresdetenidosmesactual = detenidoshombresmesactual, Mujeresdetenidasmesactual = detenidosmujeresmesactual, Totaldetenidosmesactual = detenidoshombresmesactual + detenidosmujeresmesactual, nombredelmes = nombremes, TotalSemana = nombresemana.ToString(), hombresdetenidosmesactualh = detenidoshombresmesactualh, Mujeresdetenidasmesactualh = detenidosmujeresmesactualh, Hombresdetenidossemana = detenidoshombressemanaactual, Mujeresdetenidassemana = detenidosmujeressemanaactual, Hombresdetenidossemanah = detenidoshombressemanaactualh, Mujeresdetenidassemanah = detenidosmujeressemanaactualh });


                }
                catch (Exception)
                {

                    return JsonConvert.SerializeObject(new { success = true, message = "", contratoId = id, hombresdetenidos = detenidoshombres, Mujeresdetenidas = detenidosmujeres, Totaldetenidos = detenidoshombres + detenidosmujeres, TotalPersonas = hts + mts, HTS = hts, MTS = mts, hombresdetenidosh = detenidoshombresh, Mujeresdetenidash = detenidosmujeresh, Totaldetenidosh = detenidoshombresh + detenidosmujeresh, hombresdetenidosmesactual = detenidoshombresmesactual, Mujeresdetenidasmesactual = detenidosmujeresmesactual, Totaldetenidosmesactual = detenidoshombresmesactual + detenidosmujeresmesactual, nombredelmes = nombremes, TotalSemana = "", hombresdetenidosmesactualh = detenidoshombresmesactualh, Mujeresdetenidasmesactualh = detenidosmujeresmesactualh, Hombresdetenidossemana = detenidoshombressemanaactual.ToString(), Mujeresdetenidassemana = detenidosmujeressemanaactual.ToString(), Hombresdetenidossemanah = detenidoshombresmesactualh.ToString(), Mujeresdetenidassemanah = detenidosmujeressemanaactualh.ToString() });
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.InnerException, messagelog = log });


            }

        }


        [WebMethod]
        public static string loadContratoId()
        {
            try
            {
                int id = Convert.ToInt32(HttpContext.Current.Session["numeroContrato"]);

                bool val = true;

                int idAux = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var contratos = ControlContratoUsuario.ObtenerPorUsuarioId(idAux);

                int usId = Convert.ToInt32(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
                var usuario = ControlUsuario.Obtener(usId);



                var roles = ControlRol.ObtenerPorId(usuario.RolId);

                if (contratos.Count == 0)
                {
                    val = false;
                }

                var contratoUsuario = new Entity.ContratoUsuario();
                var contrato = new Entity.Contrato();
                var subcontrato = new Entity.Subcontrato();
                string nombreContrato = "";

                if (id != 0)
                {
                    contratoUsuario = ControlContratoUsuario.ObtenerPorId(id);

                    if (contratoUsuario != null)
                    {
                        subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                        nombreContrato = subcontrato != null ? subcontrato.Nombre : "";

                    }
                }

                return JsonConvert.SerializeObject(new { success = true, message = "", contratoId = id, hasRegisters = val, nombreContrato = nombreContrato, Rol = roles.name });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }

        [WebMethod]
        public static string guardarContratoId(string dato)
        {
            try
            {
                HttpContext.Current.Session["numeroContrato"] = Convert.ToInt32(dato);

                int idContrato = Convert.ToInt32(dato);
                var contratoUsuario = new Entity.ContratoUsuario();
                var contrato = new Entity.Contrato();
                var subcontrato = new Entity.Subcontrato();
                string nombreSeleccionado = "";

                if (idContrato != 0)
                {
                    contratoUsuario = ControlContratoUsuario.ObtenerPorId(idContrato);
                }

                if (contratoUsuario != null)
                {
                    subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
                    nombreSeleccionado = subcontrato != null ? subcontrato.Nombre : "";

                }

                return JsonConvert.SerializeObject(new { success = true, message = "", contratoId = HttpContext.Current.Session["numeroContrato"], nombreContrato = nombreSeleccionado });
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(new { success = false, message = ex.Message });
            }
        }

        public class InstitucionAux
        {
            public string idInstitucion { get; set; }
            public string nombreInstitucion { get; set; }
            public string nombreCorto { get; set; }
            public string idMunicipio { get; set; }
        }
    }
}