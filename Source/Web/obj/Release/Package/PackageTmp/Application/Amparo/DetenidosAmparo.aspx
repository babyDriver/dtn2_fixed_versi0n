﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="DetenidosAmparo.aspx.cs" Inherits="Web.Application.Amparo.DetenidosAmparo" %>




<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Detenidos en amparo</li>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' />
    <style type="text/css">
        .dropdown-menu{
            position: relative;
        }
         td.strikeout {
            text-decoration: line-through;
        }
      
        #dropdown {
            width: 459px;
        }
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }
        #mapid {
            height: 20vw;       
            width: 100%;
        }
        .coordinates {
            background: rgba(0,0,0,0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding:5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }
        .modal {
          overflow-y:auto;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 13px;
            line-height: 1.42857143;
        }
        input[type=checkbox]{
            -ms-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
            padding: 10px;
            cursor: pointer;
        }
        .checkboxtext{
            font-size: 100%;
            display: inline;       
            color: #004987;
        }
        .checkboxtext:hover{
            cursor:pointer;
        }

        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <div class="scroll">
        <!--
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
                Control de detenidos existencia
            </h1>
        </div>
    </div>-->
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
        
    <div class="row" id="addentry" >
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark" id="titlehead">
<%--                <i class="fa fa-lock" ></i>--%>

                 <img style="width:40px; height:40px; margin-bottom:5px" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/Amparo.png" /><asp:Label ID="Label2" runat="server"></asp:Label>

                Detenidos en amparo
            </h1>
        </div>
        <section class="col col-12">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
               
                <a class="btn btn-success txt-color-white  btn-lg " id="salidaefectuadajuez"><i class="fa fa-sign-out"> </i>&nbsp; Salida</a>
                <a class="btn btn-primary txt-color-white btn-lg calificar" id="RegresoAmparo"><i class="fa fa-sign-in"> </i>&nbsp;Regreso de amparo</a>
                
           </div>
        </section>
        


    </div>


    <p></p>
    <section id="widget-grid" class="">
       
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-detenidos-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-balance-scale"></i></span>
                        <h2>Detenidos en amparo </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div class="row" style="width:100%;margin:0 auto;">
                            <table id="dt_basic" width="100%" class="table table-responsive table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th ></th>                                        
                                        <th <%--style="text-align:center"--%> data-class="expand">#</th>
                                        <th <%--style="text-align:center"--%>>Fotografía</th>
                                        <th <%--style="text-align:center"--%>>Nombre</th>
                                        <th <%--style="text-align:center"--%>>Apellido paterno</th>
                                        <th <%--style="text-align:center"--%> data-hide="phone,tablet">Apellido materno</th>
                                        <th <%--style="text-align:center" --%>data-hide="phone,tablet">No. remisión</th>                                     
                                        <th <%--style="text-align:center" --%>data-hide="phone,tablet">Situación del detenido</th>
                                        <th <%--style="text-align:center"--%>>Pago multa</th>
                                        <th <%--style="text-align:center"--%>>Horas</th>
                                        <%--<th>Fecha de salida</th>--%>
                                        <th <%--style="text-align:center"--%> data-hide="phone,tablet">Acciones</th>
                                        <th ></th>
                                        <th></th> 
                                    </tr>
                                </thead>
                            </table>
                                </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
     <div class="modal fade" id="form-modal-juez" tabindex="-1" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="form-modal-title-juez"></h4>
                </div>
                <div class="modal-body">
                    <div id="salidaefectuada-form-juez" class="smart-form ">
                        <div class="row">
                            <fieldset>                                
                                <section>
                                    <label style="color: dodgerblue" class="input">Fundamento <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-cubes"></i>
                                        <input type="text" name="fundamentoSalidaEfectuada" id="fundamentoSalidaEfectuada" class="alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el fundamento.</b>
                                    </label>
                                </section>  
                                <section >
                                                                    <label style="color: dodgerblue" class="label">Tipo salida <a style="color: red">*</a> </labe>
                                                                    <label class="select">
                                                                        <select name="centro" id="combotiposalida" >
                                                                        </select>
                                                                        <i></i>
                                                                    </label>
                                                                </section>
                            </fieldset>
                        </div>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btnCancelSalidaEfectuadaJuez"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                            <a class="btn btn-sm btn-default" id="btnSaveSalidaEfectuadaJuez"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    
    <div id="trasladoInterno-modal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class="glyphicon glyphicon-transfer"> </i> Traslado
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="registro-form" class="smart-form">

                        <div class="modal-body">
                            <div id="calculo_2" class="smart-form">
                                <fieldset>
                               
                                   <section><label class="input" id="IdNombreInterno"></label></section>

                                    
                                    <section>
                                       
                                           
                                                <label style="color: dodgerblue">Institución a dispocición: <a style="color: red">*</a>        </label>
                                        <label class="select" >
                                        <select name="rol" class="target" id="dropdown" runat="server" style="width:100%">
                                        </select>
                                        <i></i>
                                         </label>
                                            
                                      
                                           
                                    </section>
                                    <section >
                                                                    <label style="color: dodgerblue"  class="label">Tipo salida <a style="color: red">*</a></label>
                                                                    <label class="select">
                                                                        <select disabled="disabled" name="centro" id="combotiposalida2" >
                                                                        </select>
                                                                        <i></i>
                                                                    </label>
                                                                </section>
                                    <section>
                                    <label style="color: dodgerblue" class="input">Fundamento <a style="color: red">*</a></label>
                                    <label class="input">
                                     <textarea id="fundamentosalida" maxlength="512" name="observaciones" style="width:525px"  rows="5"></textarea>                                                            
                                   </label>    
                                </section>  
                                    </fieldset>
                                    
                            </div>
                        </div>

                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save" id="guardatraslado"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <%--  --%>
   
    <%--  --%>
     <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>
     <input type="hidden" id="tiposalidaid"  value=""/>
     
    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="motivoInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">

                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />

                    </div>
                </div>
            </div>
        </div>
    </div>
  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>  
    <script type="text/javascript">
        $(document).ready(function ()
        {
           
                
            $("#detenidoEditar").on("error", function () {
                $(this).attr("src", rutaDefaultServer);
            });
            Cargatiposalida("0");
            function Cargatiposalida(set) {
                $.ajax({

                    type: "POST",
                    url: "DetenidosAmparo.aspx/GetTipoSalida_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({
                        item: set
                    }),
                    success: function (response) {
                        var Dropdown = $("#combotiposalida");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Tipo salida]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

             $("#ctl00_contenido_rfc").focusout(function() {             
                }).blur(function() {
                    var curp10 = $("#ctl00_contenido_rfc").val();
                    $("#ctl00_contenido_CURP").val(curp10.substring(0,10));
                });

            var rutaDefaultServer = "";

          var responsiveHelper_dt_pertenencias_nuevas = undefined;

            getRutaDefaultServer();
           
            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "DetenidosAmparo.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }
                    }
                });
            }

            $("body").on("click", ".blockitemalias", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("data-verb");
                $("#itemnameblockalias").text(itemnameblock);
                $("#verbalias").text(verb);
                $("#btncontinuaralias").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modalalias").modal("show");
            });

            pageSetUp();
            var horasDatos = "";
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            var responsiveHelper_dt_basic_evento = undefined;
            var responsiveHelper_dt_basic_lugarevento = undefined;
            var responsiveHelper_dt_basic_eventoAW = undefined;
            var responsiveHelper_dt_basic_lugareventoAW = undefined;
            var exitoso = 0;
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                 if (url.indexOf("~/") == 0) {
                     url = baseUrl + url.substring(2);
                 }
                 return url;
            }

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY": "350px",
                "scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                //"order": [[9, "asc"],[3, "asc"],[4, "asc"],[5, "asc"]],
                "order": [[11, "asc"]],
                ajax: {
                    type: "POST",
                    url: "DetenidosAmparo.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    null,
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    //null,

                    null,


                    null,

                    {
                        name: "Horas",
                        data: "Horas"
                    },
                    null,
                    {
                        name: "Fecha",
                        data: "Fecha",
                        visible: false
                    },
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                   
                ],
                columnDefs: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,

                        visible: true,

                        render: function (data, type, row, meta) {
                            return '<input type="checkbox" class="checar" data-TrackingId="' + row.TrackingId + '" data-Id="' + row.Id + '"/>';
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != "" && row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.Situacion != null) {
                                return row.Situacion;
                            }
                            else {
                                return "Pendiente";
                            }
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.IngresoId != null || row.TotalAPagar == 0) {
                                return '<i class="fa fa-circle" style="color:#729e72"></i>'
                            }
                            else {
                                return '<i class="fa fa-circle" style="color:#d9534f"></i>'
                            }
                        }
                    },
                    {
                        targets: 9,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var num = parseInt(row.Horas);
                            var x = row.Horas;
                            var signo = x.substring(0, 1);

                            var horas = x.replace("-", " ");

                            var num2 = parseInt(horas.replace(":", ""));
                            if (signo == "-") {
                                num2 = num2 * -1;
                            }
                            if (num > 0)
                                return 'Restan: ' + horas + ' <i class="fa fa-circle" style="color:#729e72"></i>'

                            if (num2 > 0)
                                return 'Restan: ' + horas + ' <i class="fa fa-circle" style="color:#729e72"></i>'
                            if (num2 < 0)
                                return 'Finalizó hace ' + horas + ' <i class="fa fa-circle" style="color:#d9534f"></i>'

                            if (num == 0 && row.Situacion == "Pendiente")
                                return ''



                            if (num < 0)
                                return 'Finalizó hace ' + horas + ' <i class="fa fa-circle" style="color:#d9534f"></i>'
                        }
                    },
                    {
                        targets: 10,
                        orderable: false,
                        width: "200px",
                        render: function (data, type, row, meta) {
                            var edit = "edit";
                            var editar = "";
                            var color = "";
                            var txtestatus = "";
                            var icon = "";
                            var habilitar = "";
                            var edit = "";

                            var action2 = "";
                            action2 = '&nbsp;<a class="btn datos btn-primary datos"  href="#"  id="datos" data-id="' + row.TrackingId + '" title="Datos personales"><i class="fa fa-bars"></i> Datos personales</a>&nbsp;';
                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                            }
                           
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Proceso + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';

                          
                            var hours = "";
                            var action4 = "";

                            
                            var vacio = "";
                            var action7 = "";
                            var action5 = "";
                            action5 = '<div class="btn-group"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Ver / editar detenido <span class="caret"></span></button><ul class="dropdown-menu"><li><a class="btn-sm datos' + vacio + '"  href="#"  id="datos" data-id="' + row.TrackingId + '" data-hora="' + row.Horas + '" title="Información de detención"><i class="fa fa-bars"></i> Información de detención</a></li><li class="divider"></li>';



                            var action8 = '<li><a class="btn-sm editarDetenido" href="javascript:void(0);" id="editarDetenido" data-tracking="' + row.TrackingId + '" title = "Editar detenido" > <i class="fa fa-gavel"></i> Editar detenido</a></li><li class="divider"></li></ul></div>';

                            //return action2 + action3 + editar + action5 + action8 + action9 + action10 + action7;
                            return action2;

                        }
                    }
                ],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },

            });
           
            function validarSalidaEfectuadaJuez() {
                var esvalido = true;

                if ($("#fundamentoSalidaEfectuada").val().split(" ").join("") == "") {
                    ShowError("Fundamento", "El fundamento es obligatorio.");
                    $('#fundamentoSalidaEfectuada').parent().removeClass('state-success').addClass("state-error");
                    $('#fundamentoSalidaEfectuada').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fundamentoSalidaEfectuada').parent().removeClass("state-error").addClass('state-success');
                    $('#fundamentoSalidaEfectuada').addClass('valid');
                }
                
                if ($("#combotiposalida").val() == "0") {
                    ShowError("Tipo de salida", "El tipo de salida es obligatorio.");
                    $('#combotiposalida').parent().removeClass('state-success').addClass("state-error");
                    $('#combotiposalida').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#combotiposalida').parent().removeClass("state-error").addClass('state-success');
                    $('#combotiposalida').addClass('valid');
                }
                return esvalido;
            }

            function clearSalidaEfectuadaJuez() {
                $(".input").removeClass('valid');
                $(".input").removeClass('state-error');
                $(".input").removeClass('state-success');
                $("#fundamentoSalidaEfectuada").val("");  
                $("#combotiposalida").val("0");
                $('#combotiposalida').parent().removeClass('state-success');
            }

            $("#btnSaveSalidaEfectuadaJuez").click(function () {
                if (validarSalidaEfectuadaJuez()) {                    
                    var dataArreglo = new Array();                

                    $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                        var checkit = this.node().childNodes[0].childNodes[0].checked;
                        var data;

                        if (checkit) {
                            data = this.data();                                                
                            dataArreglo.push(data.TrackingId);                        
                        }                        
                    });      

                    saveSalidaEfectuadaJuez(dataArreglo, $("#fundamentoSalidaEfectuada").val(),$("#combotiposalida").val());
                }                
            });

                $('#modalEditarDetenido').on('hidden.bs.modal', function () {
                    location.reload(true);
})

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
            function rechazaramparo(datos) {
                startLoading(); 
                $.ajax({
                    type: "POST",
                    url: "DetenidosAmparo.aspx/RechazarAmparo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({ datos: datos }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {

                            
                            
                            $('#dt_basic').DataTable().ajax.reload();

                            
                            
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                    "  " + resultado.mensaje + ".", "<br /></div>");
                                ShowSuccess("¡Bien hecho!", "  " + resultado.mensaje + ".");
                                setTimeout(hideMessage, hideTime);

                                $('#main').waitMe('hide');
                            
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado);
                            setTimeout(hideMessage, hideTime);
                        }
                    },
                    error: function (error) {
                        $('#main').waitMe('hide');
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                            "Algo salió mal. " + error + "</div>");
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + error);
                    }
                });   
            }
            function saveSalidaEfectuadaJuez(datos, fundamento,tiposalidaid) {
                startLoading();                
                 
                $.ajax({
                    type: "POST",
                    url: "DetenidosAmparo.aspx/guardaSalidaEfectuadaJuez",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({ datos: datos, fundamento: fundamento,tiposalidaid:tiposalidaid }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            
                            for (let i = 0; i < resultado.archivos.length; i++) {                                
                                var ruta = ResolveUrl(resultado.archivos[i]);
                                if (ruta != "") {
                                    window.open(ruta, '_blank');
                                }
                            }
                            
                            $("#form-modal-juez").modal('hide'); 
                            $("#form-modal-title-juez").empty();
                            clearSalidaEfectuadaJuez();
                            $('#dt_basic').DataTable().ajax.reload();

                            if (resultado.errores != "" && resultado.errores != null) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención! </strong>" +
                                    "" + resultado.errores + "</div > ");
                                ShowAlert("¡Atención!", "  " + resultado.errores + ".");  
                                setTimeout(hideMessage, hideTime);  
                                
                                $('#main').waitMe('hide');                            
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "  " + resultado.mensaje + ".", "<br /></div>");
                                ShowSuccess("¡Bien hecho!", "  " + resultado.mensaje+"." );
                                setTimeout(hideMessage, hideTime);
                                
                                $('#main').waitMe('hide');                            
                            }                            
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");                            
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado);
                            setTimeout(hideMessage, hideTime);                            
                        }
                    },
                    error: function (error) {
                        $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + error + "</div>");                            
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + error);                            
                    }
                });            
            }
            $("#RegresoAmparo").click(function () {
                var dataArreglo = new Array();

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();
                        dataArreglo.push(data.TrackingSalida);
                    }
                });

                if (dataArreglo.length > 0) {
                    
                }
                else {
                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención! </strong>" +
                        "No ha seleccionado detenidos.</div>");
                    ShowAlert("¡Atención!", "No ha seleccionado detenidos.");
                    setTimeout(hideMessage, hideTime);
                    return;
                }
                var dataArreglo = new Array();

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();
                        dataArreglo.push(data.TrackingId);
                    }
                });      
                rechazaramparo(dataArreglo);
            });

            $("#salidaefectuadajuez").click(function () {
                var dataArreglo = new Array();                

                $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {                        
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    var data;

                    if (checkit) {
                        data = this.data();                                                
                        dataArreglo.push(data.TrackingId);                        
                    }                        
                });      

                if (dataArreglo.length > 0) {

                }
                else {
                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Atención! </strong>" +
                        "No ha seleccionado detenidos.</div>");
                    ShowAlert("¡Atención!", "No ha seleccionado detenidos.");
                    setTimeout(hideMessage, hideTime);
                    return;
                }
                $("#fundamentoSalidaEfectuada").val("");
                Cargatiposalida("0");
                $("#form-modal-title-juez").empty();
                $("#form-modal-title-juez").html('<i class="fa fa-plus" +=""></i> Autorizar salida');
                $("#form-modal-juez").modal('show');      
            });

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            var responsiveHelper_dt_basicadd = undefined;
            var breakpointAddDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytableadd = true;
           
            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

           

             $("body").on("click", ".datos", function () {
                 var id = $(this).attr("data-id");
                 var hora = $(this).attr("data-hora");
                CargarDatosPersonales(id, hora);
                
            });

            
            function CargarDatosPersonales(trackingid, hora) {
                startLoading();
                  $.ajax({
                      type: "POST",
                      url: "DetenidosAmparo.aspx/getdatos",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {                              
                            $("#fechaInfo").val(resultado.obj.Registro);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Institucion);
                            $("#fechaSalidaInfo").val(resultado.obj.Salida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                              $("#motivoInfo").val(resultado.obj.Motivo);
                              if (resultado.obj.Estatura != 0)
                                  $("#estaturaInfo").val(resultado.obj.Estatura)
                              else
                                  $("#estaturaInfo").val("")
                              if (resultado.obj.Peso != 0)
                                  $("#pesoInfo").val(resultado.obj.Peso)
                              else
                                  $("#pesoInfo").val("")

                                if (resultado.obj.RutaImagen == "")
                              {
                                  resultado.obj.RutaImagen = rutaDefaultServer;
                              }
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            
                              
                            $('#imgInfo').attr("src", imagenAvatar);
                              
                              var num = parseInt(hora);
                              var x = "";
                              if (num > 0)                                 
                                  x = 'Restan: '+ hora
                            
                              else                                 
                                  x = 'Finalizó hace ' + hora
                              $('#salida').val(x);
                              
                              $('#domicilio').val(resultado.obj.Domicilio);
                                  $("#datospersonales-modal").modal("show");
                          } else {
                              ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
            }

        });
    </script>


    <script>
        
        function reload() {
            $("dt_basicAlias").DataTable().ajax.reload();
            $("dt_basic_evento").DataTable().ajax.reload();
            $("dt_basic_lugar").DataTable().ajax.reload();
            $("dt_basic_evento_AW").DataTable().ajax.reload();
            $("dt_basic_lugar_AW").DataTable().ajax.reload();            
        }
    </script>
</asp:Content>
