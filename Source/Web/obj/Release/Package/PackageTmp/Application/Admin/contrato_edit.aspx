﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="contrato_edit.aspx.cs" Inherits="Web.Application.Admin.contrato_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Configuración</li>
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Admin/contrato.aspx">Contratos</a></li>    
    <li>Registro</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #content {
            height: 600px;
        }
        div.scroll {
            height: 590px;
            overflow: auto;
            overflow-x: hidden;
        }
        .select2-selection__choice {
            padding: 1px 28px 1px 8px;
            margin: 4px 0 3px 5px;
            position: relative;
            line-height: 18px;
            color: #fff;
            cursor: default;
            border: 1px solid #2a6395;
        }
    </style>
    <%--<link href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/css/bootstrap.css" rel="stylesheet"/>--%>
    <div class="scroll">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
    <div class="row">    
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-useredit-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-edit"></i></span>
                    <h2>Contrato </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">                        
                        <div id="smart-form-register" class="smart-form">
                            <header id="header_form">
                                Formulario de registro
                            </header>
                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="input">Cliente <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>                                                
                                            <select name="cliente" id="idcliente" style="width: 100%" class="select2"></select>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Contrato <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-book"></i>
                                            <input type="text" name="contrato" id="contratoId" placeholder="Contrato" maxlength="256" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el contrato.</b>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Vigencia inicial <a style="color:red">*</a></label>
                                       
                                        <label class="input">
                                                        <label class='input-group date' >
                                                        <input type="text" name="vigenciaInicial" id="vigenciaInicial"  class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingrese la vigencia inicial.</b>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                    </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-4"></section>
                                    <section class="col col-4">
                                        <label class="input">Institución <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>                                                
                                            <select name="institucion" id="institucion" style="width: 100%" class="select2"></select>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Vigencia final <a style="color:red">*</a></label>
                                        <label class="input">
                                            <label class='input-group date' >
                                            <input type="text" name="vigenciaFinal" id="vigenciaFinal"  class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                            <input type="hidden" id="hideid" name="hideid" runat="server" />
                                            <input type="hidden" id="hide" name="hide" runat="server" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la vigencia final.</b>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                </label>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                            <footer>
                                <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                    <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                    <a style="float: none;"  class="btn btn-sm btn-default" href="contrato.aspx"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                                
                                    
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
        
        <div class="row" id="listaSubcontratos">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="icon-append fa fa-archive"></i></span>
                        <h2>Subcontratos</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body"">                            
                            <div class="smart-form">         
                                <fieldset>
                                    <div class="row">                                        
                                        <a href="javascript:void(0);" class="btn btn-sm btn-default add" id="addSub"><i class="fa fa-plus"></i>&nbsp;Agregar </a>                                            
                                    </div>
                                    <br />
                                </fieldset>                                
                            </div>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Cliente</th>
                                        <th data-class="expand">Contrato</th>
                                        <th data-hide="phone,tablet">SubContrato</th>          
                                        <th data-hide="phone,tablet">Institución</th>
                                        <th data-hide="phone,tablet">Logotipo</th>
                                        <th data-hide="phone,tablet">Acciones</th>                                          
                                    </tr>
                                </thead>
                            </table>                                                      
                        </div>
                    </div>
                </div>
            </article>
        </div>
        </section>
        </div>

    <div class="modal fade" id="form-modal" tabindex="-1"  role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <div class="row">
                            <fieldset>                                
                                <section>
                                    <label class="col-md-4 control-label" style="color:#3276b1; text-align: left;">Contrato      <a style="color:red">*</a></label>
                                    <select name="contratoSub" id="contratoSub" style="width: 100%" class="select2" disabled="disabled"> 
                                    </select>
                                    <i></i>
                                </section>
                                <section>
                                    <label class="input" style="color:#3276b1;">Subcontrato <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-archive"></i>
                                        <input type="text" name="subcontrato" id="subcontrato" placeholder="Subcontrato" maxlength="512" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el subcontrato.</b>
                                    </label>
                                </section>
                                <section>
                                    <label style="color:#3276b1;">Vigencia inicial <a style="color:red">*</a></label>
                                    <label class="input">
                                        <label class='input-group date' >
                                        <input type="text" name="vigenciaInicialSub" id="vigenciaInicialSub" class="form-control datepicker" data-dateformat='dd/mm/yyyy' runat="server" />                                        
                                        <b class="tooltip tooltip-bottom-right">Ingrese la vigencia inicial.</b>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                            </label>
                                    </label>
                                </section>
                                <section>
                                    <label style="color:#3276b1;">Vigencia final <a style="color:red">*</a></label>
                                    <label class="input">
                                        <label class='input-group date' >
                                        <input type="text" name="vigenciaFinalSub" id="vigenciaFinalSub" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />                                        
                                        <b class="tooltip tooltip-bottom-right">Ingrese la vigencia final.</b>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                            </label>
                                    </label>
                                </section>     
                                <section>
                                <%--    <label class="input" style="color:#3276b1;">Institución <a style="color:red">*</a></label>
                                    <select name="institucionSub" id="institucionSub" style="width: 100%" class="select2"> 
                                    </select>
                                    <i></i>--%>

                                    <label style="color:#3276b1;" class="input">Institución <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>                                                
                                            <select name="institucionSub" id="institucionSub" style="width: 100%" class="select2"></select>
                                        </label>

                                </section>
                                 <section>
                                    <label class="input" style="color:#3276b1;">Divisa <a style="color:red">*</a></label>
                                    <select name="divisa" id="divisa" style="width: 100%" class="select2"> 
                                    </select>
                                    <i></i>
                                </section>
                                 <section>
                                    <label class="label" style="color:#3276b1;">Salario mínimo <a style="color:red">*</a></label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input  title="" type ="text" name="salario" id="salario" placeholder="Salario mínimo" pattern="^\d*(\.\d{0,2})?$" maxlength="13" class="form-control alphanumeric"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese el salario mínimo.</b>
                                    </div>
                                </section>
                                <section>
                                    <label class="input" style="color:#3276b1;">Institución alerta web </label>
                                    <select multiple class="select2" style="width:100%" id="institucionAlerta">
                                        <option value="0">Seleccione</option>
                                    </select>
                                    <i></i>
                                    <b class="tooltip tooltip-bottom-right">Ingresa orientación.</b>
                                </section>
                                 <section>
                                    <label class="input" style="color:#3276b1;">Latitud </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-map-marker"></i>
                                        <input type="number" step="any" name="latitud" id="latitud" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la latitud.</b>
                                    </label>
                                </section>
                                 <section>
                                    <label class="input" style="color:#3276b1;">Longitud </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-map-marker"></i>
                                        <input  type="number" step="any" name="longitud" id="longitud"  class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la longitud.</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input" style="color:#3276b1;">Logotipo</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-file"></i>
                                        <asp:FileUpload name="logotipo" id="logotipo" runat="server" />                                        
                                    </label>                                    
                                </section>
                                <section>
                                    <label class="input" style="color:#3276b1;">Banner</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-file"></i>
                                        <asp:FileUpload name="Banner" id="Banner" runat="server" />                                        
                                    </label>                                    
                                </section>   
                            </fieldset>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right; margin-right: 10px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btnSaveSub"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelSub"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                            </div>                         
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blocksub-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbSub"></span></strong>&nbsp;el registro <strong>&nbsp<span id="subblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right; margin-right: 10px;">      
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuarSub"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;                           
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                           </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="photo-arrested" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Logotipo</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="#" alt="fotografía del subcontrato" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="TrackingContratoId" runat="server" value="" />    
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="Max" value="0" />
    <input type="hidden" id="Min" value="0" />
    <input type="hidden" id="Validar1"  />
    <input type="hidden" id="Validar2"  />
    <input type="hidden" id="Validar3"  />
    <input type="hidden" id="Validar4"  />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/moment/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    
    <script type="text/javascript">

        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                document.getElementsByClassName("save")[0].click();
            }
        });
        $(document).ready(function () {

            var rutaDefaultServer = "";

            getRutaDefaultServer();
            LoadDivisas("0");


            function getRutaDefaultServer() {
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;
                        }
                    }
                });
            }

            $('#institucionSub').on('select2:opening select2:closing', function( event ) {
    var $searchfield = $( '#'+event.target.id ).parent().find('.select2-search__field');
    $searchfield.prop('disabled', false);
            });

            $("body").on("click", ".blocksub", function () {
                var subblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#subblock").text(subblock);
                $("#verbSub").text(verb);
                $("#btncontinuarSub").attr("data-id", $(this).attr("data-id"));
                $("#blocksub-modal").modal("show");
            });

            $("#btncontinuarSub").unbind("click").on("click", function () {
                var trackingid = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "contrato_edit.aspx/blockSubcontract",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            $("#dt_basic").DataTable().ajax.reload();
                            $("#blocksub-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                " Registro " + JSON.parse(data.d).message + " con éxito.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Registro " + JSON.parse(data.d).message + " con éxito.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del contrato. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del contrato. . Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });

            $("#salario").on({
                "focus": function (event) {
                    $(event.target).select();
                },
                "keyup": function (event) {
                    $(event.target).val(function (index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }
            });

           

            $("body").on("click", ".editSub", function () {
                $('#ctl00_contenido_vigenciaInicialSub').datetimepicker({
                    ampm: false,
                    format: 'dd/MM/yyyy'
                });
                $('#ctl00_contenido_vigenciaFinalSub').datetimepicker({
                    ampm: false,
                    format: 'dd/MM/yyyy'
                });
                $("#form-modal").modal('show');
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar registro");
                
                $("#btnSaveSub").attr("data-tracking", $(this).attr("data-trackingid"));
                $("#btnSaveSub").attr("data-id", $(this).attr("data-id"));
                $("#btnSaveSub").attr("data-logo", $(this).attr("data-logo"));
                $("#btnSaveSub").attr("data-Banner", $(this).attr("data-Banne"));
                $("#subcontrato").val($(this).attr("data-subcontrato"));
                $("#ctl00_contenido_vigenciaInicialSub").val($(this).attr("data-vigInicial"));
                $("#ctl00_contenido_vigenciaFinalSub").val($(this).attr("data-vigFinal"));
                $("#salario").val($(this).attr("data-salario"));
                $("#divisa").val($(this).attr("data-divisaid"));
                
                $("#divisa").change();
                $("#latitud").val($(this).attr("data-latitud"));
                $("#longitud").val($(this).attr("data-longitud"));
                var param = RequestQueryString("tracking");

                  if (param != null && param != "") {
                    loadContractsForSubcontract("0", param);
                }
                else
                {
                    
                      loadContractsForSubcontract("0", $("#ctl00_contenido_TrackingContratoId").val());
                }
                
                
                loadInstitutionsForSubcontract($(this).attr("data-institucion"), $("#ctl00_contenido_TrackingContratoId").val());
                loadWSAInstitutions($(this).attr("data-wsainstitucion"));
            });

            $("#addSub").click(function () {
                clearSubModal();
                $('#ctl00_contenido_vigenciaInicialSub').datetimepicker({
                    ampm: true,
                    format: 'DD/MM/YYYY'
                });
                $('#ctl00_contenido_vigenciaFinalSub').datetimepicker({
                    ampm: true,
                    format: 'DD/MM/YYYY'
                });
                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar registro");
                $("#form-modal").modal('show');
                $("#btnSaveSub").attr("data-tracking", "");
                $("#btnSaveSub").attr("data-id", "");
                $("#btnSaveSub").attr("data-logo", "");
                $("#btnSaveSub").attr("data-Banner", "");
                var param = RequestQueryString("tracking");
                if (param != null && param != "") {
                    loadContractsForSubcontract("0", param);
                }
                else
                {
                    
                      loadContractsForSubcontract("0", $("#ctl00_contenido_TrackingContratoId").val());
                }
                loadInstitutionsForSubcontract("0", $("#ctl00_contenido_TrackingContratoId").val());
                loadWSAInstitutions("0");
            });
            
            $("#ctl00_contenido_logotipo").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar1").val(false);
                    }
                    else {
                        $("#Validar1").val(true);
                    }
                }
                else {
                    $("#Validar1").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar2").val(false);
                    }
                    else {
                        $("#Validar2").val(true);
                    }
                }
                else {
                    $("#Validar2").val(true);
                }



            });
            $("#ctl00_contenido_Banner").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar3").val(false);
                    }
                    else {
                        $("#Validar3").val(true);
                    }
                }
                else {
                    $("#Validar3").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar4").val(false);
                    }
                    else {
                        $("#Validar4").val(true);
                    }
                }
                else {
                    $("#Validar4").val(true);
                }



            });
            GetMinMax();

            function GetMinMax() {
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/GetParametrosTamaño",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var max = resultado.TMax;
                            var min = resultado.Tmin;

                            $("#Max").val(max);
                            $("#Min").val(min);
                            // alert(resultado.rutaimagenfrontal);
                            //var imagenAvatar = ResolveUrl(resultado.rutaimagenfrontal);
                            //  imagenfrontalbiometricos = imagenAvatar;

                            //$('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            //$("#ctl00_contenido_fileUpload1").val(imagenAvatar).clone(true);
                            $('#main').waitMe('hide');
                            //location.reload(true);
                            //CargarDatos(datos.TrackingId);



                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }
            
            function loadContractsForSubcontract(setvalue, tracking) {

                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/getContractsForSubcontract",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        tracking: tracking
                    }),
                    success: function (response) {
                        var Dropdown = $('#contratoSub');
                        // Dropdown.children().remove();
                        Dropdown.append(new Option("[Contrato]", "0"));
                        if (response.d.length > 0) {
                            setvalue = response.d[0].Id;
                        }
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de contratos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadWSAInstitutions(setvalue) {
                var valores = setvalue.split(",")

                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/getWSAInstitutions",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $('#institucionAlerta');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Institución alerta web]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (valores != "") {
                            Dropdown.val(valores);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de instituciones de la alerta web. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadInstitutionsForSubcontract(setvalue, tracking) {
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/getInstitutions",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $('#institucionSub');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Institución]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de instituciones. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function validarSubcontrato() {
                var esvalido = true;

                if ($('#institucionSub').val() == "0" || $('#institucionSub').val() == null) {
                    ShowError("Institución", "La institución es obligatoria.");
                    $("#institucionSub").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#institucionSub").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#contratoSub').val() == "0" || $('#contratoSub').val() == null) {
                    ShowError("Contrato", "El contrato es obligatorio.");
                    $("#contratoSub").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#contratoSub").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($("#subcontrato").val().split(" ").join("") == "") {
                    ShowError("Subcontrato", "El subcontrato es obligatorio.");
                    $('#subcontrato').parent().removeClass('state-success').addClass("state-error");
                    $('#subcontrato').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#subcontrato').parent().removeClass("state-error").addClass('state-success');
                    $('#subcontrato').addClass('valid');
                }

                if ($("#ctl00_contenido_vigenciaInicialSub").val().split(" ").join("") == "") {
                    ShowError("Vigencia inicial", "La vigencia inicial es obligatoria.");
                    $('#ctl00_contenido_vigenciaInicialSub').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_vigenciaInicialSub').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_vigenciaInicialSub').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_vigenciaInicialSub').addClass('valid');
                }

                if ($("#ctl00_contenido_vigenciaFinalSub").val().split(" ").join("") == "") {
                    ShowError("Vigencia final", "La vigencia final es obligatoria.");
                    $('#ctl00_contenido_vigenciaFinalSub').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_vigenciaFinalSub').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_vigenciaFinalSub').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_vigenciaFinalSub').addClass('valid');
                }

                 if ($('#divisa').val() == "0" || $('#divisa').val() == null) {
                    ShowError("Divisa", "La divisa es obligatoria.");
                    $("#divisa").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#divisa").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#salario').val() == "0" || $('#salario').val() == null || $('#salario').val() == "") {
                    ShowError($("#salario").parent().prev().text().replace('*',''), $("#salario").parent().prev().text().replace('*','') + " es obligatorio");
                    $('#salario').parent().removeClass('state-success').addClass("state-error");
                    $('#salario').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#salario').parent().removeClass("state-error").addClass('state-success');
                    $('#salario').addClass('valid');
                }

                var file = document.getElementById('<% = logotipo.ClientID %>').value;
                if (file != null && file != '') {

                    if (!validaImagen(file)) {
                        ShowError("Logotipo", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        esvalido = false;

                    }
                    else {
                        var validar = $("#Validar1").val();
                        var validar2 = $("#Validar2").val();


                        if ($("#Max").val() != "0") {
                            if (validar == 'false') {
                                ShowError("Logotipo", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                esvalido = false;
                            }

                        }
                        if ($("#Min").val() != "0") {
                            var s = $("#Min").val() * 1024;
                            if (validar2 == 'false') {
                                ShowError("Logotipo", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                esvalido = false;
                            }
                        }
                    }
                }
                var file2 = document.getElementById('<% = Banner.ClientID %>').value;
                if (file2 != null && file2 != '') {
                    if (!validaImagen(file2)) {
                        ShowError("Banner", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        esvalido = false;

                    }
                    else {
                        var validar = $("#Validar3").val();
                        var validar2 = $("#Validar4").val();
                        
                        if ($("#Max").val() != "0") {
                            if (validar == 'false') {
                                ShowError("Banner", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                esvalido = false;
                            }

                        }
                        if ($("#Min").val() != "0") {
                            var s = $("#Min").val() * 1024;
                            if (validar2 == 'false') {
                                ShowError("Banner", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                esvalido = false;
                            }
                        }
                    }
                }
                return esvalido;
            }

            function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }

                }
                return false;
            }

            $("#btnSaveSub").click(function () {
                if (validarSubcontrato()) {
                    GuardarLogotipo($("#btnSaveSub").attr("data-logo"));
                }
            });

            function GuardarLogotipo(logoAnterior) {
                var files = $("#ctl00_contenido_logotipo").get(0).files;
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append(files[i].name, files[i]);
                }

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Handlers/FileUploadHandler.ashx?action=3&before=" + logoAnterior,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        var resultado = data;
                        if (resultado.exitoso) {
                            var s = resultado.nombreArchivo;
                            
                            GuardarBanner($("#btnSaveSub").attr("data-Banner"),s);
                            //guardarSub(resultado.nombreArchivo);
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                        }
                    }
                });
            }

            function GuardarBanner(BannerAnterior, nombrearchivo) {
                
                var files = $("#ctl00_contenido_Banner").get(0).files;
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append(files[i].name, files[i]);
                }

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Handlers/FileUploadHandler.ashx?action=3&before=" + BannerAnterior,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        var resultado = data;
                        if (resultado.exitoso) {
                            
                            guardarSub(nombrearchivo,resultado.nombreArchivo);
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                        }
                    }
                });
            }
            function ObtenerValoresSub() {

                var cliente = {
                    ContratoId: $('#contratoSub').val(),
                    InstitucionId: $('#institucionSub').val(),
                    Id: $('#btnSaveSub').attr("data-id"),
                    TrackingId: $('#btnSaveSub').attr("data-tracking"),
                    Subcontrato: $("#subcontrato").val(),
                    VigenciaInicial: $("#ctl00_contenido_vigenciaInicialSub").val(),
                    VigenciaFinal: $("#ctl00_contenido_vigenciaFinalSub").val(),
                    Logotipo: $("#ctl00_contenido_logotipo").val(),
                    SalarioMinimo: $("#salario").val(),
                    WSAInstitucionId: $("#institucionAlerta").val(),
                    DivisaId: $("#divisa").val(),
                    Latitud: $("#latitud").val(),
                    Longitud: $("#longitud").val(),
                    Banner: $("#ctl00_contenido_Banner").val()
                };

                return cliente;
            }

            function guardarSub(nombreArchivo,nombreBanner) {
                startLoading();
                var cliente = ObtenerValoresSub();
                cliente.Logotipo = nombreArchivo;
                
                cliente.Banner = nombreBanner;
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/saveSubcontract",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'subcontrato': cliente }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $("#form-modal").modal('hide');
                            clearSubModal();
                            cargagrid();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del subcontrato se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del subcontrato se  " + resultado.mensaje + " correctamente.");
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            pageSetUp();

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                $("#ctl00_contenido_usuario").val("");
                $("#ctl00_contenido_nombre").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#ctl00_contenido_email").val("");
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    guardarcontrato();
                }
            });
            $('#ctl00_contenido_vigenciaInicial').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });
            $('#ctl00_contenido_vigenciaFinal').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });

            function getdatosContrato(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/getContract",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);

                        loadCustomers(resultado.obj.ClienteId);
                        loadInstitutions(resultado.obj.InstitucionId);
                        $("#contratoId").val(resultado.obj.Nombre);
                        $("#ctl00_contenido_vigenciaInicial").val(resultado.obj.VigenciaInicial);
                        $("#ctl00_contenido_vigenciaFinal").val(resultado.obj.VigenciaFinal);
                        $('#ctl00_contenido_hideid').val(resultado.obj.Id);
                        $('#ctl00_contenido_hide').val(resultado.obj.TrackingId);
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });


            }
            
            function validar() {
                var esvalido = true;

                if ($('#institucion').val() == "0" || $('#institucion').val() == null) {
                    ShowError("Institución", "La institución es obligatoria.");
                    $("#institucion").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#institucion").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#idcliente').val() == "0" || $('#idcliente').val() == null) {
                    ShowError("Cliente", "El cliente es obligatorio.");
                    $("#idcliente").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#idcliente").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($("#contratoId").val().split(" ").join("") == "") {
                    ShowError("Contrato", "El contrato es obligatorio.");
                    $('#contratoId').parent().removeClass('state-success').addClass("state-error");
                    $('#contratoId').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#contratoId').parent().removeClass("state-error").addClass('state-success');
                    $('#contratoId').addClass('valid');
                }

                if ($("#ctl00_contenido_vigenciaInicial").val().split(" ").join("") == "") {
                    ShowError("Vigencia inicial", "La vigencia inicial es obligatoria.");
                    $('#ctl00_contenido_vigenciaInicial').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_vigenciaInicial').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_vigenciaInicial').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_vigenciaInicial').addClass('valid');
                }

                if ($("#ctl00_contenido_vigenciaFinal").val().split(" ").join("") == "") {
                    ShowError("Vigencia final", "La vigencia final es obligatoria.");
                    $('#ctl00_contenido_vigenciaFinal').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_vigenciaFinal').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_vigenciaFinal').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_vigenciaFinal').addClass('valid');
                }

                return esvalido;
            }

            function guardarcontrato() {
                startLoading();
                var contrato = ObtenerValores();
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/save",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    cache: true,
                    data: JSON.stringify({ 'contrato': contrato }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var param = RequestQueryString("tracking");
                            if (resultado.TrackingId != "00000000-0000-0000-0000-000000000000") {
                                $('#ctl00_contenido_hide').val(resultado.TrackingId);
                                $('#ctl00_contenido_TrackingContratoId').val(resultado.TrackingId);
                            }
                            else {
                                $('#ctl00_contenido_hide').val(param);
                                $('#ctl00_contenido_TrackingContratoId').val(param);
                            }
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            if ($('#ctl00_contenido_TrackingContratoId').val() != null && $('#ctl00_contenido_TrackingContratoId').val != "") {
                                $("#listaSubcontratos").show();

                                 
                            }
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del contrato se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del contrato se  " + resultado.mensaje + " correctamente.");
                           
                            
                                cargagrid();
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "Ocurrió un error. No fue posible guardar el contrato. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function ObtenerValores() {
                var contrato = {
                    Nombre: $('#contratoId').val(),
                    VigenciaInicial: $('#ctl00_contenido_vigenciaInicial').val(),
                    VigenciaFinal: $('#ctl00_contenido_vigenciaFinal').val(),
                    Id: $('#ctl00_contenido_hideid').val(),
                    ClienteId: $('#idcliente').val(),
                    InstitucionId: $("#institucion").val()
                };

                return contrato;
            }

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {

                    getdatosContrato(param);
                        cargagrid();
                    //CargarContrato(param);
                    //$("#listaSubcontratos").show();
                    //$("#ctl00_contenido_TrackingContratoId").val(param);
                }
                else {
                    loadCustomers("0");
                    loadInstitutions("0");
                    $("#listaSubcontratos").hide();
                    $("#ctl00_contenido_TrackingContratoId").val("");
                }
            }

            function LoadDivisas(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/GetDivisas",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#divisa');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Divisa]", "0"));
                        $.each(response.d, function (index, item) {

                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de divisas. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadCustomers(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/getCustomers",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idcliente');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Cliente]", "0"));
                        $.each(response.d, function (index, item) {

                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clientes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadInstitutions(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/getInstitutions",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#institucion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Institución]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de instituciones. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function clearSubModal() {
                $(".input").removeClass('state-success');
                $(".input").removeClass('state-error');
                $(".input").removeClass('valid');
                $(".input-group").removeClass('valid');
                $(".input-group").removeClass('state-success');
                $(".input-group").removeClass('state-error');
                $("#subcontrato").val("");
                $("#ctl00_contenido_vigenciaInicialSub").val("");
                $("#ctl00_contenido_vigenciaFinalSub").val("");
                $("#ctl00_contenido_logotipo").val("");
                $("#salario").val("");
                $("#divisa").removeClass('state-success');
                $("#divisa").removeClass('state-error');
                $("#institucionSub").removeClass('state-success');
                $("#institucionSub").removeClass('state-error');
                $("#divisa").parent().removeClass('state-success');
                $("#divisa").parent().removeClass('state-error');
                $("#institucionSub").parent().removeClass('state-success');
                $("#institucionSub").parent().removeClass('state-error');
                $("#divisa").val("0");
                $("#divisa").change();
                $("#latitud").val("");
                $("#longitud").val("");
                $("#ctl00_contenido_Banner").val("");
            }

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            init();

            //Variables tabla de subcontratos
            var responsiveHelper_dt_basic_sub = undefined;
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_col_reorder_sub = undefined;
            var responsiveHelper_datatable_tabletools_sub = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $("body").on("click", ".photoview", function (e) {
              var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });
         
            function cargagrid() {
                responsiveHelper_dt_basic = undefined;
                $('#dt_basic').dataTable({
                    destroy: true,
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                        }
                    },
                    ajax: {
                        type: "POST",
                        url: "contrato_edit.aspx/getSubcontracts",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            parametrosServerSide.emptytable = false;
                            var param = RequestQueryString("tracking");
                            if (param != null && param != "") {
                                parametrosServerSide.contratoTrackingId = param;

                            }
                            else {

                                parametrosServerSide.contratoTrackingId = $("#ctl00_contenido_TrackingContratoId").val()
                            }
                            return JSON.stringify(parametrosServerSide);
                        }

                    },
                    columns: [
                        {
                            name: "Cliente",
                            data: "Cliente"
                        },
                        {
                            name: "Contrato",
                            data: "Contrato"
                        },
                        {
                            name: "Subcontrato",
                            data: "Subcontrato"
                        },
                        {
                            name: "Nombre",
                            data: "Nombre"
                        },
                        {
                            name: "Logotipo",
                            data: "Logotipo"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 4,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.Logotipo != "" && row.Logotipo != null) {
                                    var ext = "." + row.Logotipo.split('.').pop();
                                    var photo = row.Logotipo.replace(ext, ".thumb");
                                    var imgAvatar = resolveUrl(photo);
                                    return '<div class="text-center">' +
                                        '<a href="#" class="photoview" data-foto="' + imgAvatar + '" >' +
                                        '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + resolveUrl(rutaDefaultServer) + '\';" />' +
                                        '</a>' +
                                        '<div>';
                                } else {
                                    pathfoto = resolveUrl("/Content/img/logo.png");
                                    return '<div class="text-center">' +
                                        '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                        '<div>';
                                }

                            }
                        },
                        {
                            targets: 5,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "editSub";
                                var editar = '<a class="btn btn-primary btn-circle ' + edit + '" data-trackingid="' + row.TrackingId + '" data-salario="' + row.SalarioMinimo + '" data-id="' + row.Id + '" data-logo="' + row.Logotipo + '" data-Banner="' + row.Banner +'" data-subcontrato="' + row.Subcontrato + '" data-vigInicial="' + row.VigenciaInicial + '" data-vigFinal="' + row.VigenciaFinal + '" data-institucion="' + row.InstitucionId + '" data-divisaId="' + row.DivisaId + '" data-wsainstitucion="' + row.WSAInstitucionId + '" title = "Editar" > <i class="glyphicon glyphicon-pencil"></i></a>& nbsp; ';
                                var habilitar = "";
                                if (!row.Habilitado) {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";

                                }
                                else {

                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                }

                                if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" data-trackingid="' + row.TrackingId + '" data-salario="' + row.SalarioMinimo + '" data-id="' + row.Id + '" data-logo="' + row.Logotipo + '" data-subcontrato="' + row.Subcontrato + '" data-Banner="' + row.Banner + '" data-vigInicial="' + row.VigenciaInicial + '" data-vigFinal="' + row.VigenciaFinal + '" data-institucion="' + row.InstitucionId + '" data-divisaId="' + row.DivisaId + '" data-latitud="' + row.Latitud + '" data-longitud="' + row.Longitud + '" data-wsainstitucion="' + row.WSAInstitucionId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blocksub" href="javascript:void(0);" data-value="' + row.Subcontrato + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                                return editar + habilitar;

                            }
                        }
                    ]

                });
            }

            var hideTime = 5000;

            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

            $("#divisa").change(function () {
                if ($("#divisa :selected").text().includes("UMA")) {
                    $("#salario").parent().prev().text("UMA (Unidad de Medida y Actualización)");
                    $("#salario").attr("placeholder", "UMA (Unidad de Medida y Actualización)");
                }
                else {
                    $("#salario").parent().prev().html("Salario mínimo <a style='color:red'>*</a>");
                    $("#salario").attr("placeholder", "Salario mínimo");
                }
            });

        });
    </script>
</asp:Content>
