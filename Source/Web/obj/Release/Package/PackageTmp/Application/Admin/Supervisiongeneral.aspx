﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="Supervisiongeneral.aspx.cs" Inherits="Web.Application.Admin.Supervisiongeneral" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    
    <li>Supervisión general</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">

       <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #dropdown {
            width: 439px;
        }
        
     
        /*input[type=text]:disabled{
            background-color:lightgrey;
            cursor:not-allowed;
        }*/
        /*input[type=datetime]:disabled{
            background-color:lightgrey;
            cursor:not-allowed;
        }*/
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    
<style>
        table {

          overflow-x:auto;
        }
        table td {
          word-wrap: break-word;
          max-width: 400px;
        }
        #dt_basic_observaciones td {
          white-space:inherit;
        }
</style>
     <div class="row">
            <section class="col col-12">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
            <select class="mostrar" id="filtrojuezcalificador" style="float: right">
                </select>
                <label style="float: right"> Mostrar: &nbsp</label>
            </div>
                </section>
        </div>
<div class="scroll">
    <section id="widget-grid" class="">
       
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-detenidos-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Supervisión general </h2>
                    </header>
                    <div>
                        <br />
                            <div class="row">                                        
                                <div class="col-sm-3"> 
                                    <label for="tbFechaInicial">Fecha Inicial:</label>
                                    <asp:TextBox ID="tbFechaInicial" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
                                </div>
                                        
                                <div class="col-sm-3"> 
                                    <label for="tbFechaFinal">Fecha Final:</label>
                                    <asp:TextBox ID="tbFechaFinal" runat="server" TextMode="Date" ClientIDMode="Static"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                        <a class="btn btn-success btn-md" id="btnBuscarInfo"><i class="fa fa-search"></i> Consultar</a>
                                </div>
                            </div>                                   
                            <div class="row">
                                <section class="col-md-2">
                                            <a class="btn btn-success btn-md ShowModal" style="margin-top:22px;padding:5px;height:35px; margin-right:15px;"><i class="fa fa-search"></i> Buscar detenido</a>
                                    </section>
                            </div>
                        <br />
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div class="row" style="width:100%;margin:0 auto;">
                            <table id="dt_basic" width="100%" class="table table-responsive table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                                                              
                                        <th <%--style="text-align:center"--%> data-class="expand">#</th>
                                        <th <%--style="text-align:center"--%>>Fotografía</th>
                                        <th <%--style="text-align:center"--%>>Nombre</th>
                                        <th <%--style="text-align:center"--%>>Apellido paterno</th>
                                        <th <%--style="text-align:center"--%> data-hide="phone,tablet">Apellido materno</th>
                                        <th <%--style="text-align:center" --%>data-hide="phone,tablet">No. remisión</th>                                     
                                        <th <%--style="text-align:center" --%>data-hide="phone,tablet">Estatus</th>
                                        
                                        <%--<th>Fecha de salida</th>--%>
                                        <th <%--style="text-align:center"--%> data-hide="phone,tablet">Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                                </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
     <div class="modal fade" id="form-modal-historial" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-historial"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form ">
                        <div class="row">
                            <fieldset>
                                <table id="dt_basic_historial" class="table table-striped table-bordered table-hover" width="100%">                         
                                    <thead>
                                        <tr>                                        
                                            <th>#</th>
                                            <th>Movimiento</th>
                                            <th>Fecha</th>
                                            <th>Usuario</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </fieldset>
                            <footer>
                                <a class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelHistorial"><i class="fa fa-close"></i>&nbsp;Cerrar </a>                                                            
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="FechaRegistro" />
    <input type="hidden" id="validarfecha" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>  
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_dt_basic_his = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            var rutaDefaultServer = "";
            getRutaDefaultServer();

            $('.date').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            CargarfiltroJuezCalificador("0");

            function getRutaDefaultServer() {
                $.ajax({
                    type: "POST",
                    url: "Supervisiongeneral.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;
                        }
                    }
                });
            }
            function validatefecha(Id, Fecha, InternoId, Movimiento) {
                var historico = { 'Id': Id, 'Fecha': Fecha, 'InternoId': InternoId, 'Movimiento': Movimiento };
                var valido = true;
                $.ajax({
                    type: "POST",
                    url: "Supervisiongeneral.aspx/ValidateFecha",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    data: JSON.stringify({
                        'historico': historico,
                    }),
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {

                            if (resultado.Validar == false) {
                                valido = false;
                            }
                            var fecha = resultado.fechaRegistro;
                            $("#FechaRegistro").val(fecha);
                            $("#validarfecha").val(valido);

                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
                return valido;
            }
            function Save(Id, Fecha, InternoId, Movimiento) {
                var historico = { 'Id': Id, 'Fecha': Fecha, 'InternoId': InternoId, 'Movimiento': Movimiento };

                $.ajax({
                    type: "POST",
                    url: "Supervisiongeneral.aspx/Save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'historico': historico }),
                    success: function (data) {
                        var resultado = data.d;

                        if (resultado.exitoso) {
                            ShowSuccess("¡Bien hecho!", "La fecha se actualizó con éxito")
                            $("#dt_basic_historial").DataTable().ajax.reload();
                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function loadTableHistorial(tracking) {
                $('#dt_basic_historial').dataTable({
                    "lengthMenu": [5, 10, 25, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    //"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    //"t" +
                    //"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "autoWidth": true,
                    //"oLanguage": {
                    //    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    //},
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_his) {
                            responsiveHelper_dt_basic_his = new ResponsiveDatatablesHelper($('#dt_basic_historial'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_his.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_his.respond();
                        $('#dt_basic_historial').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                        }
                    },
                    "order": [[2, "asc"]],
                    ajax: {
                        type: "POST",
                        url: "Supervisiongeneral.aspx/getDataHistorial",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_historial').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.TrackingId = tracking;
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            name: "TrackingId",
                            data: "TrackingId"
                        },
                        {
                            name: "Movimiento",
                            data: "Movimiento"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "Usuario",
                            data: "Usuario"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            targets: 2,
                            orderable: true,
                            render: function (data, type, row, meta) {
                                // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                                return '<div><label class="input"><label class="input-group date" id="FehaHoradatetimepicker" ><input type="text" name="fechahora"  id="fechaid_' + row.Id + '" value="' + row.Fecha + '" class="input-group fechahora alptext" placeholder="Fecha" data-requerido="true"/> <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span></label></label></div>';
                            }
                        },
                        {
                            targets: 4,
                            orderable: false,

                            render: function (data, type, row, meta) {
                                var action = '<a class="btn save btn-primary txt-color-white href="#" data-id="' + row.Id + '" data-internoid="' + row.InternoId + '" data-movimiento="' + row.Movimiento + '" title="Guardar"><i class="fa fa-save"></i>  Guardar</a>&nbsp;';

                                return action;
                            }
                        }
                    ]
                });
            }

            $("body").on("click", ".save", function () {
                var id = $(this).attr("data-id");
                var internoid = $(this).attr("data-internoid");
                var movimiento = $(this).attr("data-movimiento");
                var idfecha = "#fechaid_" + id;
                if ($(idfecha).val() == "") {
                    ShowError("Fecha", "El campo fecha es obligatorio");
                    return;
                }
                else {
                    var valido = validatefecha(id, $(idfecha).val(), internoid, movimiento);
                    valido = $("#validarfecha").val();
                    if (valido == "false") {

                        var f = $("#FechaRegistro").val();
                         if (movimiento == "Modificación de datos generales en certificado químico" || movimiento == "Modificación de datos generales en certificado quimico") {
                            ShowError("Fecha y hora de toma", "La fecha y hora de toma no puede ser menor a la fecha " + f + ".");
                            ShowError("Fecha y hora de proceso", "La fecha y hora de proceso no puede ser menor a la fecha " + f + ".");
                        }
                        
                        //$('#fechahora3').parent().removeClass('state-success').addClass("state-error");
                        //$('#fechahora3').removeClass('valid');
                        return;
                    }
                }
                Save(id, $(idfecha).val(), internoid, movimiento);
            });

            $("body").on("click", ".historial", function () {
                var tracking = $(this).attr("data-id");
                responsiveHelper_dt_basic_his = undefined;

                $('#dt_basic_historial').DataTable().destroy();
                loadTableHistorial(tracking);
                $('.date').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $("#form-modal-historial").modal('show');
                $("#form-modal-title-historial").empty();
                $("#form-modal-title-historial").html("Movimientos históricos del detenido");
            });

            window.table = $('#dt_basic').dataTable({
                "order": [[5, "asc"]],
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY": "350px",
                "scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "Supervisiongeneral.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.opcion = $(".mostrar").val();
                        parametrosServerSide.fechaInicio = $("#tbFechaInicial").val();
                        parametrosServerSide.fechaFinal = $("#tbFechaFinal").val();
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "Estatus",
                        data: "EstatusNombre"
                    },
                    //null,
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != "" && row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        width: "200px",
                        render: function (data, type, row, meta) {
                            var action = '<a class="btn historial bg-color-yellow txt-color-white href="#" data-id="' + row.TrackingId + '" title="Historial de movimientos"><i class="fa fa-history fa-lg"></i> Movimientos detenidos</a>&nbsp;';

                            return action;
                        }
                    }
                ],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
            });

            $(".mostrar").change(function () {
                window.table.api().ajax.reload();
            });

            function CargarfiltroJuezCalificador(setetnia) {
                $('#filtrojuezcalificador').empty();
                $.ajax({

                    type: "POST",
                    url: "Supervisiongeneral.aspx/GetFiltrosJuezCalificador",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#filtrojuezcalificador');

                        Dropdown.append(new Option("Todos los detenidos", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setetnia != "") {
                            Dropdown.val(setetnia);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de etnias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }
        });
        $("#btnBuscarInfo").click(
            function () {

                $("#dt_basic").DataTable().ajax.reload();

            }
        );
    </script>
</asp:Content>
