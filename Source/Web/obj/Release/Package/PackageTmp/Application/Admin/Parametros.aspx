﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="Parametros.aspx.cs" Inherits="Web.Application.Admin.Parametros" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Configuración</li>
    <li>Parámetros</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #dropdown {
            width: 439px;
        }
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-cogfa fa-cog"></i>
                Parámetros
            </h1>
        </div>
    </div>
    <div class="row" style="display: none;">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>

      
        <div class="row ">
        </div>
    <div class="row" id="addentry" style="display: none;">
        <section class="col col-4">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">

                <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
              <!--  <a href="javascript:void(0);" class="btn btn-md btn-default calculate" id="calculate"><i class="fa fa-calculator"></i>&nbsp;Cálculo de porcentaje compurgado </a>-->

            </div>
        </section>
        <section class="col col-6">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">

                <%--<a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Sentence/celdas.aspx"" class="btn btn-md btn-default" id="celdas"><i class="fa"></i>Celdas </a>--%>
              
            </div>
        </section>


    </div>


    <p></p>
    
    <section id="widget-grid" class="">
        
        <div class="row">
           
            

            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" " data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-cogfa fa-cog"></i></span>
                        <h2>Parámetros  </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Parámetro</th>
                                        <th >Descripción</th>
                                        <th >Tipo de parámetro</th>
                                        <th>Subcontrato</th>
                                        
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
           
    </section>
     </div>
    

    
        <%--<div id="add-datosPersonales" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">--%>
       
     
   


 


    <%--  --%>
     <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>

  
     
    <div id="ConfiguracionParametro-modal" class="modal fade"  tabindex="-1" data-backdrop="static" data-keyboard="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title"><i class="fa fa-cogfa fa-cog"></i> Configurar Parámetro
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="configurar-form" class="smart-form">

                        <div class="modal-body">
                            
                        
                            <fieldset>
                                <div class ="Row">
                                    <section class="col col-12">
                                    <label style="color: dodgerblue"> Parámetro:</label> <strong><span id="idParametro"></span></strong>
                                        </section>
                                </div>
                                <div  class="Row">
                                                       
                                               
                                               
                                            

                                                <section class="col col-6" id="scmbvalor">

                                                    <label style="color: dodgerblue" class="label">Seleccione valor <a style="color:red">*</a></label>
                                                   <label class="select col-6"> 
                                                <select name="diagnostico" id="cmbvalor" >
                                                    <option value="-1">[Seleccione valor]</option>
                                                    <option value="1">SI</option>
                                                    <option  value="0">NO</option>
                                                </select>
                                                   </label>     
                                                <i></i>
                                        
                                                </section>
                             
                                               
                                                    <section class="col col-6" id="sidvalor">
                                                        <label style="color: dodgerblue" class="label">Valor <a style="color:red">*</a></label>
                                                        <label class="input">
                                                            <input id="idvalor" name="valor"  placeholder="Valor"  maxlength="45" class="alptext" />                                                           
                                                        </label>                                                        
                                                    </section>
                                                </div>
                            </fieldset>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="guardaparametro"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>


 
    <input type="hidden" id="hide" />
    <input type="hidden" id="ParametroId"" />
    <input type="hidden" id="ContratoId"" />
    <input type="hidden" id="hideid" runat="server" />
    <input type="hidden" id="ParametroContratoId"  value="" />
    <input type="hidden" id="TipoParametroId" value="" />
     <input type="hidden" id="Valor" value=""/>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">

        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#ConfiguracionParametro-modal").is(":visible")) {
                    document.getElementById("guardaparametro").click();
                }
            }
        });
        $(document).ready(function () {

            pageSetUp();
           
          
            var responsiveHelper_dt_basic = undefined;
            
            
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };



            
                var param = RequestQueryString("tracking");
            if (param != undefined) {
              
                CargarDatosInterno(param);
                
                }

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "scrollY":        "350px",
                "scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "Parametros.aspx/GetParametros",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                            var param = RequestQueryString("tracking");
                        
                        parametrosServerSide.emptytable = false;
                        if (param != undefined) {
                            parametrosServerSide.TrackingProcesoId = param;
                        }
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    
                    null,
                      {
                        name: "NombreParametro",
                        data: "NombreParametro"
                    },
                    {
                        name: "Descripcion",
                        data: "Descripcion"
                    },
                    {
                        name: "TipoParametro",
                        data: "TipoParametro"
                    },
                    {
                        name: "Subcontrato",
                        data: "Subcontrato"
                    },
               
                  
                    null,
                    
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    
                   {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },

                 
                    
                    {
                        targets: 6,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var edit = "edit";
                            var editar = "";
                            var color = "";
                            var txtestatus = "";
                            var icon = "";
                            var habilitar = "";

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                            }


                         //   if ($("#ctl00_contenido_KAQWPK").val() == "true")
                              //  editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                           // else if($("#ctl00_contenido_WERQEQ").val() == "true")
                               // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                            
                             var action2 = "";
                            var motivo = "";
                            action2='&nbsp;<a class="btn btn-primary  ' + motivo + '"data-Id="' + row.Id + '"data-Parametro="' + row.NombreParametro + '"data-TipoParametroId="' + row.TipoParametroId + '"data-Valor="' + row.Valor + '"data-ParametroContratoId="' + row.ParametroContratoId + '"data-contratoid="' + row.ContratoId + '"  id="idconsultar" title="Configurar"><i class="fa fa-cog"></i>&nbsp;Configurar</a>&nbsp;';

                            
                            
                            //var action5 = "";
                            //action5 = '&nbsp;<a class="btn datos btn-primary '+vacio+  '"  href="#"  id="datos" data-id="' + row.TrackingId + '" title="Datos personales">Datos personales</a>&nbsp;';
                            return action2;

                        }
                    }
                ]

            });

            $(document).on("click", "#guardaparametro", function () {

                GuardarParametro();

            });

            $(document).on("click", "#idconsultar", function () {
                $("#idvalor").val("");
                 limpiarModal();
                var parametro = $(this).attr("data-Parametro");
                var TipoParametroId = $(this).attr("data-TipoParametroId");
                var ParametroContratoId = $(this).attr("data-ParametroContratoId");
                var Valor = $(this).attr("data-Valor");
                var ContratoId=$(this).attr("data-contratoid");
                $("#ContratoId").val(ContratoId);
                var ParametroId= $(this).attr("data-Id")
                $("#idParametro").text(parametro);
                $("#ParametroContratoId").val(ParametroContratoId);
                $("#ParametroId").val(ParametroId);
                  $("#TipoParametroId").val(TipoParametroId);
                if (TipoParametroId == "1") {
                    $("#sidvalor").hide();
                    $("#scmbvalor").show();
                }
                else if (TipoParametroId == "5") {
                    $("#scmbvalor").hide();
                    $("#sidvalor").show();
                    
                    $("#idvalor").attr("type", "password");
                }
                else {
                    $("#scmbvalor").hide();
                    $("#idvalor").attr("type", "text");
                    $("#sidvalor").show();
                }
                if (ParametroContratoId != "0") {
                    if (TipoParametroId == "1") {
                        $("#cmbvalor").val(Valor);
                        $("#Valor").val(Valor);
                    }
                    else if (TipoParametroId == "5") {
                    $("#scmbvalor").val(Valor);
                        $("#sidvalor").val(Valor);
                        $("#idvalor").val(Valor);
                    $("#idvalor").attr("type", "password");
                    }

                    else
                    {
                        $("#idvalor").val(Valor);
                        $("#Valor").val(Valor);
                        $("#idvalor").attr("type", "text");
                    }


                }
                else

                {
                    if (TipoParametroId == "1") {
                        
                        $("#Valor").val($("#cmbvalor").val());
                    }
                    else {
                        
                        $("#Valor").val($("#idvalor").val());
                    }



                }
                $('#idvalor').parent().removeClass("state-error");
                $("#ConfiguracionParametro-modal").modal("show");

             
            })
            
          

            function parametros() {    
                if ($("#TipoParametroId").val() == "1") {
                    $("#Valor").val($("#cmbvalor").val());
                }
                else
                {
                    $("#Valor").val($("#idvalor").val());
                }
                var parametroAux = {
                    ParametroId: $("#ParametroId").val(),
                    
                    Valor: $("#Valor").val(),
                    Id: $("#ParametroContratoId").val(),
                    TipoParametroId: $("#TipoParametroId").val(),
                    ContratoId: $("#ContratoId").val(),
                   
                    TrackingId: $("#hide").val()
                    
                };
                return parametroAux;
            }
          

            function GuardarParametro() {
                if (validar()) {
                startLoading();
                var parametroAux = parametros();

                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    parametroAux.TrackingId = param;
                }

                
                    $.ajax({
                        type: "POST",
                        url: "Parametros.aspx/Save",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        data: JSON.stringify({ 'parametroAux': parametroAux }),
                        success: function (data) {
                            var resultado = data.d;
                            if (resultado.exitoso) {
                                $('#ctl00_contenido_hideid').val(resultado.Id);
                                $('#ctl00_contenido_hide').val(resultado.TrackingId);
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                    " " + resultado.mensaje, "<br /></div>");
                                setTimeout(hideMessage, hideTime);
                                ShowSuccess("¡Bien hecho!", "La información se registró/actualizó correctamente");
                                window.emptytable = false;
                                window.table.api().ajax.reload();

                                $('#main').waitMe('hide');
                                $("#ConfiguracionParametro-modal").modal("hide");

                                limpiarModal();
                            }
                            else {
                                $('#main').waitMe('hide');
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                    "Algo salió mal. " + resultado.mensaje + "</div>");
                                setTimeout(hideMessage, hideTime);
                                ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                            }
                        }
                    });
                }
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
            
        });

        function validar() {
            var esvalido = true;
            if ($("#TipoParametroId").val() != "1") {
                if ($("#idvalor").val() == null || $("#idvalor").val().split(" ").join("") == "") {
                    ShowError("Campo obligatorio", "Favor de ingresar un valor.");
                    $('#idvalor').parent().removeClass('state-success').addClass("state-error");
                    $('#idvalor').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#idvalor').parent().removeClass("state-error").addClass('state-success');
                    $('#idvalor').addClass('valid');
                }
            }
            else
            {
                
              
                if ($("#cmbvalor").val() == "" || $("#cmbvalor").val() == "-1")
                {

                ShowError("Campo obligatorio", "Favor de seleccionar un valor.");
                    $('#cmbvalor').parent().removeClass('state-success').addClass("state-error");
                    $('#idvalor').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#cmbvalor').parent().removeClass("state-error").addClass('state-success');
                    $('#cmbvalor').addClass('valid');
                }
                
            }
            
            return esvalido;
        }

        function limpiarModal() {
            $("#idvalor").empty();            
            $('#idvalor').parent().removeClass('state-success');
            $('#idvalor').parent().removeClass("state-error");
            $('#cmbvalor').parent().removeClass('state-success');
            $('#cmbvalor').parent().removeClass("state-error");
            $('#cmbvalor').val("-1");
        }


       
           
        $("#idvalor").keyup(function () {
            if ($("#TipoParametroId").val() == "3") {
                 console.log("numerico");
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            }
        });
    </script>
</asp:Content>


