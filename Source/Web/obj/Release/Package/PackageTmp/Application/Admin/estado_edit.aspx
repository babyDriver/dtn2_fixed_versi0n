﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="estado_edit.aspx.cs" Inherits="Web.Application.Admin.estado_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li style="color:silver">Administración</li>
    <li style="color:silver">Localización</li>
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Admin/Estado.aspx">Estado</a></li>
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Admin/Estado_edit.aspx">Registro</a></li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-estado-edit-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-edit"></i></span>
                    <h2>Estado </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">                        
                        <div id="smart-form-register" class="smart-form">
                            <header id="header_form">
                                Formulario de registro
                            </header>
                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="input">País <a style="color:red">*</a></label>                                        
                                        <select name="pais" id="pais" style="width: 100%" class="select2">
                                        </select>                                                                                    
                                        <div class="note note-error" style="color: red">&nbsp</div>
                                    </section>
                                    <section class="col col-4">
                                        <label >Estado <a style="color:red">*</a></label>                                        
                                        <label class="input">
                                            <i class="icon-append fa fa-book">&nbsp; </i>
                                            <input type="text" name="Estado" id="estado" placeholder="Estado" maxlength="256" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el estado.</b>
                                        </label>                                                                             
                                        <div class="note note-error" style="color: red">&nbsp</div>
                                    </section>
                                     <section class="col col-4">
                                        <label>Descripción</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-building"></i>
                                            <input type="text" name="descripcion" id="descripcion" placeholder="Descripción" class="alptext" maxlength="255" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la descripción.</b>
                                        </label>
                                      
                                    </section>
                                    <div>
                                    <section class="col col-4">
                                        <label>Clave <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-hashtag"></i>
                                            <input type="text" name="clave" id="clave" placeholder="Clave del estado" maxlength="5" class="input-number alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la clave.</b>
                                        </label>
                                        <div class="note note-error" style="color: red">&nbsp</div>
                                    </section>
                                        </div>
                                </div>
                            </fieldset>
                                <footer>
                                    <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                        <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                        <a style="float: none;" href="estado.aspx" class="btn btn-sm btn-default"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                                
                                    </div>
                                </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
        </section>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="hideid" runat="server" value="" />
    <input type="hidden" id="hide" runat="server" value="" />
 </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                console.log("ctl+g");
                e.preventDefault();

                document.getElementsByClassName("save")[0].click();
            }
        });
        $(document).ready(function () {

            pageSetUp();              

            $('.input-number').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            });

            $("#pais").change(function () {
               // loadStates("0", $("#pais").val());
            });

          

            $("#pais").select2();
          
            //loadCountries("0");                        

            function loadCountries(setvalue) {

                $.ajax({                    
                    type: "POST",
                    url: "colonia_edit.aspx/getCountries",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false
                    //success: function (response) {
                    //    var Dropdown = $('#pais');
                    //    Dropdown.children().remove();
                    //    Dropdown.append(new Option("[País]", "0"));
                    //    $.each(response.d, function (index, item) {
                    //        Dropdown.append(new Option(item.Desc, item.Id));
                    //    });

                    //    if (setvalue != "") {
                    //        Dropdown.val(setvalue);
                    //        Dropdown.trigger("change.select2");
                    //    }
                    //},
                    //error: function () {
                    //    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    //}
                }).done(function (response) {
                    var Dropdown = $('#pais');
                    Dropdown.children().remove();
                    Dropdown.append(new Option("[País]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setvalue != "") {
                        Dropdown.val(setvalue);
                        Dropdown.trigger("change.select2");
                    }                    
                });
            }

            function loadCities(setvalue, idEstado) {

                $.ajax({                    
                    type: "POST",
                    url: "colonia_edit.aspx/getCities",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idEstado: idEstado
                    }),
                    cache: false
                    //success: function (response) {
                    //    var Dropdown = $('#municipio');
                    //    Dropdown.children().remove();
                    //    Dropdown.append(new Option("[Municipio]", "0"));
                    //    $.each(response.d, function (index, item) {
                    //        Dropdown.append(new Option(item.Desc, item.Id));
                    //    });

                    //    if (setvalue != "") {
                    //        Dropdown.val(setvalue);
                    //        Dropdown.trigger("change.select2");
                    //    }
                    //},
                    //error: function () {
                    //    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    //}
                }).done(function (response) {
                    var Dropdown = $('#municipio');
                    Dropdown.children().remove();
                    Dropdown.append(new Option("[Municipio]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setvalue != "") {
                        Dropdown.val(setvalue);
                        Dropdown.trigger("change.select2");
                    }
                });
            }

            function loadStates(setvalue, idPais) {

                $.ajax({                    
                    type: "POST",
                    url: "colonia_edit.aspx/getStates",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idPais: idPais
                    }),
                    cache: false
                    //success: function (response) {
                    //    var Dropdown = $('#estado');
                    //    Dropdown.children().remove();
                    //    Dropdown.append(new Option("[Estado]", "0"));
                    //    $.each(response.d, function (index, item) {
                    //        Dropdown.append(new Option(item.Desc, item.Id));
                    //    });

                    //    if (setvalue != "") {
                    //        Dropdown.val(setvalue);
                    //        Dropdown.trigger("change.select2");
                    //    }
                    //},
                    //error: function () {
                    //    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    //}
                }).done(function (response) {
                    var Dropdown = $('#estado');
                    Dropdown.children().remove();
                    //Dropdown.append(new Option("[Estado]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setvalue != "") {
                        Dropdown.val(setvalue);
                        Dropdown.trigger("change.select2");
                    }
                });
            }

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                $("#ctl00_contenido_usuario").val("");
                $("#ctl00_contenido_nombre").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#ctl00_contenido_email").val("");                                                                
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    guardar();
                }
            });            

             function CargarColonia(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "estado_edit.aspx/GetEstado",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;                         
                         loadCountries(data.PaisId);
                         //loadStates(data.IdEstado, data.IdPais);
                         //loadCities(data.IdMunicipio, data.IdEstado);
                         $("#estado").val(data.Estado);
                         $("#descripcion").val(data.Descripcion);
                         
                         
                         $("#clave").val(data.Clave)                                                  
                         $('#ctl00_contenido_hideid').val(data.Id);
                         $('#ctl00_contenido_hide').val(data.TrackingId);                         
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function validar() {                
                var esvalido = true;                

                if ($('#pais').val() == "0" || $('#pais').val() == null) {
                    ShowError("País", "El país es obligatorio.");
                    $("#pais").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#pais").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#estado').val() == "" || $('#estado').val() == null) {
                    ShowError("Estado", "El estado es obligatorio.");
                    $('#estado').parent().removeClass('state-success').addClass("state-error");
                    $('#estado').removeClass('valid');
                    esvalido = false;
                }
                else {
                     $('#estado').addClass('valid');
                }

               

                      

                if ($("#clave").val().split(" ").join("") == "") {
                    ShowError("Clave", "La clave del estado es obligatoria.");
                    $('#clave').parent().removeClass('state-success').addClass("state-error");
                    $('#clave').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#clave').parent().removeClass("state-error").addClass('state-success');
                    $('#clave').addClass('valid');
                } 

                return esvalido;
            }

            function guardar() {
                startLoading();
                var estado = ObtenerValores();
                
                $.ajax({
                    type: "POST",
                    url: "estado_edit.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'estado': estado }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "La información del estado se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "La información del estado se  " + resultado.mensaje + " correctamente.");                            
                            $('#main').waitMe('hide');
                           // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            //$(location).attr('href', url);
                            setTimeout ("redireccionar()", 5000);
                             var url = "estado.aspx"; 
                            
                           
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }
            var pagina="estado.aspx"
        function redireccionar()
        {
        location.href=pagina
        }
            function ObtenerValores() {                                
                var estado = {
                    PaisId:$('#pais').val(),
                    Estado: $("#estado").val(),
                    Descripcion: $("#descripcion").val(),
                    Clave: $("#clave").val(),
                    Id: $('#ctl00_contenido_hideid').val(),
                    TrackingId: $('#ctl00_contenido_hide').val()
                };
                return estado;
            }

            function init() {
                var param = RequestQueryString("tracking");                
                if (param != undefined) {                    
                    CargarColonia(param);
                }
                else {
                    loadCountries("0");                    
                }                                
            }

            init();

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>

