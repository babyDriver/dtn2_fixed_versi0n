﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="Epi.aspx.cs" Inherits="Web.Application.Epi.Epi" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>EPI</li>
    

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #content {
            height: 600px;
        }
        div.scroll {
            height: 590px;
            overflow: auto;
            overflow-x: hidden;
        }
    </style>
    <div class="row" id="addentry" >
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <h1 class="txt-color-blueDark" id="titlehead">


 <img style="width:35px; height:35px; margin-bottom:5px" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/EPI.png" />  <asp:Label ID="Label2" runat="server"></asp:Label>
  EPI
            </h1>
        </div>
    </div>
    <div class="scroll">
   

    <asp:Label ID="Label1" runat="server"></asp:Label>
    <section id="widget-grid-sanciones" class="">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-sanciones-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-file-excel-o"></i></span>

                        <h2>EPI</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div id="smart-form-register-entry" class="smart-form">
                            <fieldset>
                               
                                <div class="row">
                                    <section class="col col-2">
                                                    <label class="label">Fecha de inicio <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" name="inicio" id="inicio" placeholder="Fecha de inicio" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha de inicio.</b>
                                                    </label>
                                                </section>
                                    <section class="col col-2">
                                                    <label class="label">Fecha final <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-calendar"></i>
                                                        <input type="text" name="final" id="final" placeholder="Fecha final" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                        <b class="tooltip tooltip-bottom-right">Ingresa la fecha final.</b>
                                                    </label>
                                                </section>
                                   
                                    </div>
                                <div class="row">
                                     <section class="col col-0">
                                        <a class="btn btn-success btn-md reportePdf" style="padding:5px"><i class="glyphicon glyphicon-file"></i> Generar PDF</a>
                                    </section>
                                     <section class="col col-1">
                                        <a class="btn btn-primary btn-md reporteexcel" style="padding:5px"><i class="glyphicon glyphicon-file"></i> Generar Excel/ .xls</a>
                                    </section>
                                </div>
                                </fieldset>
                            </div>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        
                                        <th data-class="expand">#</th>
                                        <th >No. remisión</th>
                                        <th>Nombre</th>
                                        <th>Fecha y hora de ingreso</th>
                                        <th>Edad</th>
                                        <th>Sexo</th>
                                        <th>Conclusión médica</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        </section>
    </div>
    <input type="hidden" id="Hidden1" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $('#ctl00_contenido_inicio').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY HH:mm'
            });

            $('#ctl00_contenido_final').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY HH:mm'
            });
            getFechasIniciales();

            function getFechasIniciales() {
                $.ajax({
                    type: "POST",
                    url: "Epi.aspx/GetFecha",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var fechainicial = resultado.FechaI;
                            var fechafinal = resultado.FechaF;
                            $('#ctl00_contenido_inicio').val(fechainicial);
                            $('#ctl00_contenido_final').val(fechafinal);

                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("body").on("click", ".reportePdf", function () {
                $("#ctl00_contenido_lblMessage").html("");
                var valido = validarFechas();
                if (valido) {
                    pdf();
                }
                //if ($("#ctl00_contenido_inicio").val() == "" || $("#ctl00_contenido_final").val() == "") {
                //    ShowAlert("¡Aviso!", "Seleccione un rango de fechas");
                //}
                //else {
                //    pdf();
                //}
            });


            $("body").on("click", ".reporteexcel", function () {
                $("#ctl00_contenido_lblMessage").html("");
                var valido = validarFechas();
                if (valido) {
                    excel();
                }
                //if ($("#ctl00_contenido_inicio").val() == "" || $("#ctl00_contenido_final").val() == "") {
                //    ShowAlert("¡Aviso!", "Seleccione un rango de fechas");
                //}
                //else {
                //    pdf();
                //}
            });


            function validarFechas() {
                var esvalido = true;
                if ($("#ctl00_contenido_inicio").val() == "" || $("#ctl00_contenido_inicio").val() == null) {
                    ShowError("Fecha inicial", "La fecha inicial es obligatoria.");
                    $('#ctl00_contenido_inicio').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_inicio').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_inicio').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_inicio').addClass('valid');
                }
                if ($("#ctl00_contenido_final").val() == "" || $("#ctl00_contenido_final").val() == null) {
                    ShowError("Fecha final", "La fecha final es obligatoria.");
                    $('#ctl00_contenido_final').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_final').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_final').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_final').addClass('valid');
                }
                var inicio = $("#ctl00_contenido_inicio").val() == null ? "" : $("#ctl00_contenido_inicio").val();
                var fin = $("#ctl00_contenido_final").val() == null ? "" : $("#ctl00_contenido_final").val();
                if (inicio != "") {
                    inicio = inicio.split("/");
                    inicio = inicio[2] + "/" + inicio[1] + "/" + inicio[0];

                }
                if (fin != "") {
                    fin = fin.split("/");
                    fin = fin[2] + "/" + fin[1] + "/" + fin[0];
                }
                if (inicio != "" & fin != "") {
                    if (fin < inicio) {
                        ShowError("Fecha final", "La fecha final deberá ser mayor que la fecha inicial.");
                        $('#ctl00_contenido_final').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_final').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_final').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_final').addClass('valid');
                    }
                }
                return esvalido;
            }

             function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "order": [[2, "desc"]],
                ajax: {
                    type: "POST",
                    url: "Epi.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    null,
                    
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },
                    {
                        name: "Edad",
                        data: "Edad"
                    },
                    {
                        name: "Sexo",
                        data: "Sexo"
                    },
                    {
                        name: "conclucion",
                        data: "conclucion"
                    }


                ],
                columnDefs: [

                    
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    }
                ]

            });

            function excel() {
                
                $.ajax({

                    type: "POST",
                    url: "Epi.aspx/Excel",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         inicio: $("#ctl00_contenido_inicio").val(),
                         fin: $("#ctl00_contenido_final").val()
                     }),
                     cache: false,
                     success: function (data) {
                         var resultado = data.d;

                         if (resultado.exitoso) {
                             var ruta = resolveUrl(data.d.Ubicacion);
                             if (ruta != "") {
                                 window.open(ruta, '_blank');
                             }
                         }
                         else {


                             $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                 "Algo salió mal: " + data.d.mensaje + "</div>");
                             ShowAlert("¡Aviso!", data.d.mensaje);
                         }
                     },
                     error: function () {
                         ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                     }
                 });
            }

            function pdf() {
                
                $.ajax({

                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Epi/Epi.aspx/PDF",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        inicio: $("#ctl00_contenido_inicio").val(),
                        fin: $("#ctl00_contenido_final").val()
                    }),
                    cache: false,
                    success: function (data) {
                         var resultado = data.d;

                        if (resultado.exitoso)
                        {
                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                        }
                        else {
                             

                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            
                            ShowAlert("¡Aviso!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            
        });
    </script>
</asp:Content>
