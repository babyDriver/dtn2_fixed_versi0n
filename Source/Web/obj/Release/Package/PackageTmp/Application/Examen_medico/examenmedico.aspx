﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="examenmedico.aspx.cs" Inherits="Web.Application.Examen_medico.examenmedico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Examen_medico/examenmedico_list.aspx">Examen médico</a></li>
    <li>Examen médico</li>  
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        .dataTables_filter .input-group-addon {
            height: auto;
        }
        .same-height-thumbnail {
            height: 150px;
            margin-top: 0;
        }
        .smart-form header {
            padding: 0;
        }
        .dt-toolbar {
            padding: 4px 0px;
        }
        #ScrollableContent {
            height: calc(100vh - 400px);
        }
        .margin-sides-10 label {
            margin-right: 10px;
            margin-left: 10px;
        }
    </style>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a> 
                                    <a href="javascript:void(0);" class="btn btn-md btn-warning" id="sexoEdad" style="margin-left: 20px;"><i class="fa fa-pencil"></i>&nbsp;Cambiar edad y sexo</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="scroll" id="ScrollableContent">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-examenmedico-2" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-user-md"></i></span>
                        <h2>Examen médico </h2>
                    </header>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body" style="padding-bottom: 0px;">
                        <!-- widgetbody -->
                        <ul id="tabs" class="nav nav-tabs ">
                            <li id="tabexamen" class="active">
                                <a id="refexamen" href="#s1" data-toggle="tab" aria-expanded="true">Examen médico</a>
                            </li>
                            <li id="tabpruebas" style="display:none;" class="">
                                <a id="refpruebas" href="#s2" data-toggle="tab" aria-expanded="false">Pruebas</a>
                            </li>
                        </ul>
                        <div id="content" class="tab-content padding-10" style="padding-bottom: 0px !important;">
                            <div class="tab-pane fade active in " id="s1">
                                <div class="row smart-form">
                                    <div class="col-md-5 col-lg-5 col-xs-5">
                                        <section>
                                            <label class="label">Tipo de examen <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="tipo_examen">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa tipo de examen.</b>
                                            </label>
                                        </section>
                                        <section>
                                            <label class="label">Inspección ocular / interrogatorio <a style="color: red">*</a></label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="4" id="ocular" placeholder="Inspección ocular / interrogatorio" class="alphanumeric alptext" maxlength="4000"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Ingresa inpección ocular/interrogatorio.</b>
                                            </label>
                                        </section>
                                        <section>
                                            <label class="label">Indicaciones médicas <a style="color: red">*</a></label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="2" id="indicaciones" placeholder="Indicaciones médicas" class="alphanumeric alptext" maxlength="255"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Ingresa indicaciones médicas.</b>
                                            </label>
                                        </section>
                                        <section>
                                            <label class="label">Observación <a style="color: red">*</a> </label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                <textarea rows="2" id="observacion" placeholder="Observación" class="alphanumeric alptext"></textarea>
                                                <b class="tooltip tooltip-bottom-right">Ingresa observación.</b>
                                            </label>                                        
                                        </section>
                                        <section>
                                            <label class="checkbox" style="color:#3276B1; font-weight:bold;">
                                                <input type="checkbox" name="Lesiones_visibles" id="Lesion_visible" value="true"/>
                                                <i></i>Lesiones visibles a simple vista</label>
                                        </section>
                                        <br />
                                        <br />
                                    </div>
                                    <div class="col-md-1 col-lg-1 col-xs-1"></div>
                                    <div class="col-md-5 col-lg-5 col-xs-5">
                                        <section>
                                            <label class="label">Fecha y hora</label>
                                            <label class="input">
                                                <label class='input-group date' id='autorizaciondatetimepicker' style="width: 100%">
                                                    <input type="text" name="fecha" id="fecha" class='input-group date alptext' placeholder="Fecha y hora de registro" disabled="disabled" />
                                                    <i class="icon-append fa fa-calendar"></i>
                                                </label>
                                            </label>
                                        </section>
                                        <header>Lesiones</header>
                                        <br />
                                        <div>
                                            <a href="javascript:void(0);" class="btn btn-md btn-default disabled" title="Agregar" style="padding: 6px 12px;" id="addlesion"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                        </div>
                                        <p></p>
                                        <br />
                                        <table id="dt_basiclesiones" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th data-class="expand">#</th>
                                                    <th>Tipo</th>
                                                    <th data-hide="phone,tablet">Lugar</th>
                                                    <th data-hide="phone,tablet">Observación</th>
                                                    <th data-hide="phone,tablet">Acciones</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <section>
                                            <div class="inline-group" style="font-size: smaller">
                                                <label class="checkbox"style="color:#3276B1; font-weight:bold;">
                                                    <input type="checkbox" name="peligro_" id="peligro" value="true"/>
                                                    <i></i>Ponen en peligro la vida</label>
                                                <label class="checkbox" style="color:#3276B1; font-weight:bold;">
                                                    <input type="checkbox" name="consecuencias_" id="consecuencias" value="false"/>
                                                    <i></i>Dejan consecuencias médico legales</label>
                                                <label class="checkbox" style="color:#3276B1; font-weight:bold;">
                                                    <input type="checkbox" name="fallecimiento" id="fallecimiento" value="false"/>
                                                    <i></i>Fallecimiento</label>
                                            </div>
                                        </section>
                                        <section>
                                            <label class="label">Número de días que tarda en sanar</label>
                                            <label class="input">
                                                <input type="text" name="dias" id="dias" placeholder="Días en sanar" maxlength="4" class="number alptext" data-requerido="true" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa los días en sanar.</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12"></div>
                                    <div class="col-md-5 col-lg-5 col-xs-5">
                                        <header>Intoxicaciones</header>
                                        <br />
                                        <div>
                                            <a href="javascript:void(0);" class="btn btn-md btn-default #consecuencias disabled" title="Agregar" style="padding: 6px 12px;" id="addintoxicacion"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                        </div>
                                        <p></p>
                                        <br />
                                        <table id="dt_basicintoxicaciones" class="table table-striped table-bordered table-hover" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th data-class="expand">#</th>
                                                    <th>Tipo</th>
                                                    <th data-hide="phone,tablet">Observación</th>
                                                    <th data-hide="phone,tablet">Acciones</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col-md-1 col-lg-1 col-xs-1"></div>
                                    <div class="col-md-5 col-lg-5 col-xs-5">
                                        <header>Tatuajes</header>
                                        <br />
                                        <div>
                                            <a href="javascript:void(0);" class="btn btn-md btn-default disabled" title="Agregar" style="padding: 6px 12px;" id="addtatuaje"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                        </div>
                                        <p></p>
                                        <br />
                                        <table id="dt_basictatuajes" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th data-class="expand">#</th>
                                                    <th>Tipo</th>
                                                    <th data-hide="phone,tablet">Lugar</th>
                                                    <th data-hide="phone,tablet">Observación</th>
                                                    <th data-hide="phone,tablet">Acciones</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12">
                                        <br />
                                        <div class="row smart-form">
                                            <footer>
                                                <div class="row" style="display: inline-block; float: right">
                                                    <a style="float: none;" href="javascript:void(0);" class="btn btn-default btn-sm saveexamen" title="Guardar" id="saveexamen"><i class="fa fa-save"></i>&nbsp;Guardar &nbsp;</a>
                                                    <a style="float: none;" href="examenmedico_list.aspx" class="btn btn-default btn-sm" title="Volver al listado de examen médico"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar&nbsp; </a>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="s2">
                                <div class="row smart-form">
                                    <div class="col-md-12 col-lg-12 col-xs-12 margin-sides-10">
                                        <header style="margin-bottom: 10px;" class="col-md-12">Exámenes</header>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">1.-Mucosas <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="mucosas">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa el tipo.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">2.-Aliento <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="aliento">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa aliento.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">3.-Alcoholímetro <a style="color: red">*</a></label>
                                            <label class="input">
                                                <input type="text" name="alcoholimetro" maxlength="10" id="alcoholimetro" placeholder="Alcoholímetro" class="alphanumeric alptext"/>
                                                <b class="tooltip tooltip-bottom-right">Ingrese el nivel de alcohol.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">4.-Examen neurológico <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="neurologico">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa examen neurológico.</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12 margin-sides-10">
                                        <header style="margin-bottom: 10px;" class="col-md-12">Signos vitales</header>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">T/A <a style="color: red">*</a></label>
                                            <label class="input">
                                                <input type="text" name="ta" maxlength="10" id="ta" placeholder="" class="alphanumeric alptext"/>
                                                <b class="tooltip tooltip-bottom-right">Ingresa T/A.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">FC <a style="color: red">*</a></label>
                                            <label class="input">
                                                <input type="text" name="fc" maxlength="10" id="fc" placeholder="" class="alphanumeric alptext"/>
                                                <b class="tooltip tooltip-bottom-right">Ingresa la FC.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">FR <a style="color: red">*</a></label>
                                            <label class="input">
                                                <input type="text" name="fr" maxlength="10" id="fr" placeholder="" class="alphanumeric alptext"/>
                                                <b class="tooltip tooltip-bottom-right">Ingresa la FR.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">Pulso <a style="color: red">*</a></label>
                                            <label class="input">
                                                <input type="text" name="pulso" maxlength="10" id="pulso"  placeholder="" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa el pulso.</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12 margin-sides-10">
                                        <header style="margin-bottom: 10px;" class="col-md-12">Especifique</header>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">1.- Disartía <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="disartia">
                                                    <option value="-1">[Seleccione]</option>
                                                    <option value="true">Si</option>
                                                    <option value="false">No</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa disartía.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">2.- Conjuntivas <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="conjuntivas">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa conjuntivas.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">3.- Marcha <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="marcha">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa marcha.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">4.-Pupilas <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="pupilas">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa pupilas.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">5.- Coordinación <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="coordinacion">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa coordinación.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">6.- Reflejos pupilares <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="pupilares">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa reflejos pupilares.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label"> 7.- Reflejos osteo tendinosos <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="osteo">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa reflejos osteo tendinosos.</b>
                                            </label>
                                        </section>    
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">8.- Romberg <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="romberq">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa romberg.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">9.-Conducta <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="conducta">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa conducta.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">10.- Lenguaje <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="lenguaje">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa lenguaje.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">11.- Atención <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="atencion">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa atención.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">12.-Orientación <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="orientacion">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa orientación.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">13.- Diadococinesia <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="diadococinencia">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa diadococinesia.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">14.-Dedo-nariz <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="dedo">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa dedo-nariz.</b>
                                            </label>
                                        </section>
                                        <section class="col-md-3 col-lg-3 col-xs-6">
                                            <label class="label">15.- Talón-rodilla <a style="color: red">*</a></label>
                                            <label class="select">
                                                <select runat="server" id="talon">
                                                    <option value="0">Seleccione</option>
                                                </select>
                                                <i></i>
                                                <b class="tooltip tooltip-bottom-right">Ingresa talón-rodilla.</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12">
                                        <div class="row smart-form">
                                            <footer>                
                                                <div class="row" style="display: inline-block; float: right">
                                                    <a style="float: none;" href="javascript:void(0);" class="btn btn-default btn-sm saveprueba" title="Guardar" id="savepruebas"><i class="fa fa-save"></i>&nbsp;Guardar&nbsp; </a>
                                                    <a style="float: none;" href="examenmedico_list.aspx" class="btn btn-default btn-sm" title="Volver al listado de examen médico"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar &nbsp;</a>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end widgetbody -->
            </article>
        </div>
    </section>

    <div class="modal fade" id="form-modal" tabindex="-1" data-keyboard="true" data-backdrop="static" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label style="color: dodgerblue"> Tipo <a style="color: red">*</a></label>
                                    <label class="select">
                                        <select name="tipoadicional" id="tipoadicional" runat="server"></select>
                                        <i></i>
                                    </label>
                                </section>
                                <section id="sectionLugar">
                                    <label class="input" style="color: dodgerblue" >Lugar <a style="color: red">*</a></label>
                                    <label class="select">
                                        <select name="lugar" id="lugaradicional" runat="server"></select>
                                        <i></i>
                                    </label>

                                </section>
                                <section>
                                    <label class="input" style="color: dodgerblue" >Observación</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="observacion"  id="observacionadicional" runat="server"  placeholder="Observación" maxlength="256" class="alptext"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese observación</b>
                                    </label>
                                </section>
                            </div>
                            <input type="hidden" id="trackingadicional" runat="server" value="" />
                        </fieldset>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveadicional" id="btnsaveadicional"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="historyTatuajes-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial tatuajes</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dt_basichistorytatuajes" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-hiden="tablet,fablet,phone">Lugar</th>
                                        <th>Movimiento</th>
                                        <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="historyIntoxicacion-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial intoxicaciones</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dt_basichistoryintoxicacion" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-hiden="tablet,fablet,phone">Observacion</th>
                                        <th>Movimiento</th>
                                        <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>

    <div id="historyLesion-modal" class="modal fade" data-backdrop="static" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial lesiones</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dt_basichistoryLesion" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-hiden="tablet,fablet,phone">Lugar</th>
                                        <th>Movimiento</th>
                                        <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="sexoedad-modal" tabindex="-1" data-backdrop="static" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class='fa fa-pencil'></i> Cambiar edad y sexo</h4>
                </div>
                <div class="modal-body" style="padding:0 20px;">
                    <div id="sexoedad-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="label" style="color:#3276b1;">Edad<a style="color: red">*</a></label>
                                    <input type="text" title="" class="form-control" maxlength="3" pattern='^[0-9]*$' id="edad"/>
                                </section>
                                <%--<section> 
                                    <label class="label" style="color:#3276b1;">Fecha de nacimiento <a style="color: red">*</a></label>
                                    <div class='input-group date' id='fechadatetimepicker'>
                                        <input type="text" class="form-control" name="Fecha" id="fechaNacimiento" placeholder="Fecha de nacimiento" data-requerido="true"/>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </section>--%>
                                <section>
                                    <label style="color:#3276b1;">Sexo <a style="color: red">*</a></label>
                                    <label class="select">
                                        <select name="rol" id="sexo" runat="server">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btncontinuarsexo"><i class="fa fa-save"></i>&nbsp;Continuar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelsexomodal"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-modal" tabindex="-1" data-backdrop="static" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmar cambiar edad y sexo</h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <p class="center">¿Está seguro de cambiar la información del detenido?</p>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnconfirm"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelconfirm"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockuser-modal" class="modal fade" data-backdrop="static" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de<strong>&nbsp<span id="usernameblock"></span></strong>?</p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog" data-keyboard="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control alptext" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control alptext" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img  width="240" height="240" align="center" class="img-thumbnail same-height-thumbnail" id="imgInfo" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="tracking" runat="server" />
    <asp:HiddenField ID="ExamenTracking" runat="server" />
    <asp:HiddenField ID="PruebaTracking" runat="server" />
    <asp:HiddenField ID="trackingUser" runat="server" />
    <input type="hidden"id="idHistoryT"  />
    <input type="hidden" id="idHistoryL" />
    <input type="hidden" id="idhistoryLesion" />
    <input type="hidden" id="sexoId" runat="server" value="" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js"></script>

    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            var sectionNumber = $("#tabs .active").children().attr("id");

            if (e.ctrlKey && e.keyCode === 39) {
                e.preventDefault();
                switch (sectionNumber) {
                    case "refexamen": document.getElementById("refpruebas").click(); break;
                    case "refpruebas": document.getElementById("refexamen").click(); break;
                }
            }
            else if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                switch (sectionNumber) {
                    case "refexamen": document.getElementById("refpruebas").click(); break;
                    case "refpruebas": document.getElementById("refexamen").click(); break;
                }
            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                switch (sectionNumber) {
                    case "refexamen":
                        if ($("#form-modal").is(":visible")) {
                            document.getElementsByClassName("saveadicional")[0].click();
                        }
                        else if ($("#sexoedad-modal").is(":visible")) {
                            document.getElementsByClassName("save")[0].click();
                        }
                        else {
                            document.getElementsByClassName("saveexamen")[0].click();
                        }

                        break;
                    case "refpruebas": document.getElementsByClassName("saveprueba")[0].click(); break;
                }
            }
        });

        $("#sexoedad-modal").on("shown.bs.modal", function () {
            $("#edad").focus();
        });

        $("#form-modal").on("shown.bs.modal", function () {
            $("#ctl00_contenido_tipoadicional").focus();
        });

        pageSetUp();
        init();
        function init() {
            if ($("#ctl00_contenido_tracking").val() != "") {
                CargarDatos($("#ctl00_contenido_tracking").val());
                validarsiexisteexamen($("#ctl00_contenido_tracking").val());
            }
            var param = RequestQueryString("action");

            if (param == "update") {
                $('#addtatuaje').removeClass('disabled');
                $('#addlesion').removeClass('disabled');
                $('#addintoxicacion').removeClass('disabled');
                CargarDatosExamen();
            }
            else {

                CargarListado(0, "mucosas");
                CargarListado(0, "aliento");
                CargarListado(0, "neurologico");
                CargarListado(0, "tipo_examen");
                CargarListado(0, "conjuntivas");
                CargarListado(0, "marcha");
                CargarListado(0, "pupilas");
                CargarListado(0, "coordinacion");
                CargarListado(0, "pupilares");
                CargarListado(0, "osteo");
                CargarListado(0, "romberq");
                CargarListado(0, "conducta");
                CargarListado(0, "lenguaje");
                CargarListado(0, "atencion");
                CargarListado(0, "orientacion");
                CargarListado(0, "diadococinencia");
                CargarListado(0, "dedo");
                CargarListado(0, "talon");
                loadDate();
            }

            //Cargar opciones select

            $("#btncancelsexomodal").parent().css("margin", "10px");
        }

        $(document).on('keydown', 'input[pattern]', function (e) {
            var input = $(this);
            var oldVal = input.val();
            var regex = new RegExp(input.attr('pattern'), 'g');

            setTimeout(function () {
                var newVal = input.val();
                if (!regex.test(newVal)) {
                    input.val(oldVal);
                }
            }, 0);
        });

        /* Listado tatuajes*/
        var responsiveHelper_dt_basictatuajes = undefined;
        var breakpointAddDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        window.emptytabletatuajes = true;
        window.tabletatuajes = $('#dt_basictatuajes').dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span style="height: 16px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basictatuajes) {
                    responsiveHelper_dt_basictatuajes = new ResponsiveDatatablesHelper($('#dt_basictatuajes'), breakpointAddDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basictatuajes.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basictatuajes.respond();
                $('#dt_basictatuajes').waitMe('hide');
            },
            "createdRow": function (row, data, index) {
                if (!data["Habilitado"]) {
                    $('td', row).eq(0).addClass('strikeout');
                    $('td', row).eq(1).addClass('strikeout');
                    $('td', row).eq(2).addClass('strikeout');
                    $('td', row).eq(3).addClass('strikeout');
                }
            },
            ajax: {
                type: "POST",
                url: "examenmedico.aspx/getDataTatuaje",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosServerSide) {
                    $('#dt_basictatuajes').waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });

                    parametrosServerSide.emptytableadd = false;
                    parametrosServerSide.tracking = $("#ctl00_contenido_ExamenTracking").val();
                    parametrosServerSide.tipo = "tatuaje";
                    return JSON.stringify(parametrosServerSide);
                }
            },
            columns: [
                null,
                null,
                {
                    name: "Tipo",
                    data: "Tipo",
                    orderable: false
                },
                {
                    name: "Lugar",
                    data: "Lugar",
                    orderable: false
                },
                {
                    name: "Observacion",
                    data: "Observacion",
                    orderable: false
                },
                null,
            ],
            columnDefs: [
                {
                    data: "TrackingId",
                    targets: 0,
                    orderable: false,
                    visible: false,
                    render: function (data, type, row, meta) {
                        return "";
                    }
                },
                {
                    targets: 1,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var edit = "edit";
                        var editar = "";
                        var habilitar = "";
                        var icon = "";
                        var color = "";
                        var txtestatus = "";
                        if (row.Habilitado) {

                            txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                        }
                        else {
                            txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                        }

                        if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' edittatuaje" + href="javascript:void(0);" ' + '" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipoTatuajeId + '"data-lugar= "' + row.LugarId + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"data-clasificacion = tatuaje title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;';
                        if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a data-clasificacion = tatuaje class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-tracking= "' + row.TrackingId + '"data-desc="' + row.Lugar+'" data-tipo= "' + row.TipoTatuajeId + '"data-lugar= "' + row.LugarId + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                        return editar + habilitar
                            + '<a name="history" class="btn btn-default btn-circle historialT" id="Historialtatuaje" href="javascript:void(0);" data-value="' + row.Lugar + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                    }
                }
            ]
        });

        /* Fin listado tatuajes*/

        /* Listado lesiones*/
        var responsiveHelper_dt_basiclesiones = undefined;
        var breakpointAddDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        window.emptytablelesiones = true;
        window.tablelesiones = $('#dt_basiclesiones').dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span style="height: 16px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basiclesiones) {
                    responsiveHelper_dt_basiclesiones = new ResponsiveDatatablesHelper($('#dt_basiclesiones'), breakpointAddDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basiclesiones.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basiclesiones.respond();
                $('#dt_basiclesiones').waitMe('hide');
            },
            "createdRow": function (row, data, index) {
                if (!data["Habilitado"]) {
                    $('td', row).eq(0).addClass('strikeout');
                    $('td', row).eq(1).addClass('strikeout');
                    $('td', row).eq(2).addClass('strikeout');
                    $('td', row).eq(3).addClass('strikeout');
                }
            },
            ajax: {
                type: "POST",
                url: "examenmedico.aspx/getDataLesion",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosServerSide) {
                    $('#dt_basiclesiones').waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });

                    parametrosServerSide.emptytableadd = false;
                    parametrosServerSide.tracking = $("#ctl00_contenido_ExamenTracking").val();
                    parametrosServerSide.tipo = "lesion";
                    return JSON.stringify(parametrosServerSide);
                }
            },
            columns: [
                null,
                null,
                {
                    name: "Tipo",
                    data: "Tipo",
                    orderable: false
                },
                {
                    name: "Lugar",
                    data: "Lugar",
                    orderable: false
                },
                {
                    name: "Observacion",
                    data: "Observacion",
                    orderable: false
                },
                null,
            ],
            columnDefs: [
                {
                    data: "TrackingId",
                    targets: 0,
                    orderable: false,
                    visible: false,
                    render: function (data, type, row, meta) {
                        return "";
                    }
                },
                {
                    targets: 1,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var edit = "edit";
                        var editar = "";
                        var habilitar = "";
                        var icon = "";
                        var color = "";
                        var txtestatus = "";
                        if (row.Habilitado) {
                            txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                        }
                        else {
                            txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                        }

                        if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' editlesion" + href="javascript:void(0);" ' + '" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipoLesionId + '"data-lugar= "' + row.LugarId + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"data-clasificacion = lesion title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;';
                        if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a data-clasificacion = lesion class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-tracking= "' + row.TrackingId + '"data-desc="' + row.Lugar +'" data-tipo= "' + row.TipoLesionId + '"data-lugar= "' + row.LugarId + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                        return editar + habilitar + '<a name="history" class="btn btn-default btn-circle historialLes" href="javascript:void(0);" data-value="' + row.Lugar + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                    }
                }
            ]
        });

        /* Fin listado lesiones*/
        var responsiveHelper_dt_basichistoryLesion = undefined;
        var breackpointHistoryLesDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        }
        window.emptytablehistoryles = true;

        window.tablehistoryLes = $("#dt_basichistoryLesion").dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            order: [[3, 'asc']],
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span style="height: 32px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basichistoryLesion) {
                    responsiveHelper_dt_basichistoryLesion = new ResponsiveDatatablesHelper($("#dt_basichistoryLesion"), breackpointHistoryLesDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basichistoryLesion.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basichistoryLesion.respond();
                $("#dt_basichistoryLesion").waitMe('hide');
            },
            ajax: {
                type: "POST",
                url: "examenmedico.aspx/GetLesionLog",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosserverside) {
                    $("#dt_basichistoryLesion").waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });
                    var centroid = $('#idhistoryLesion').val() || "0";
                    parametrosserverside.centroid = centroid;
                    parametrosserverside.todoscancelados = false;
                    parametrosserverside.emptytable = emptytablehistoryles;
                    return JSON.stringify(parametrosserverside);
                }
            },
            columns: [
                {
                    name: "Lugar",
                    data: "Lugar",
                    ordertable: false
                },
                {
                    name: "Accion",
                    data: "Accion",
                    ordertable: false
                },

                {
                    name: "Creadopor",
                    data: "Creadopor",
                    ordertable: false
                }
                ,
                {
                    name: "Fec_Movto",
                    data: "Fec_Movto",
                    ordertable: false
                }
            ],
        });

        var responsiveHelper_dt_basichistoryIntoxicaciones = undefined;
        var breackpontHistoryIntoxDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        }

        /* Listado intoxicaciones*/
        var responsiveHelper_dt_basicintoxicaciones = undefined;
        var breakpointAddDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        $("body").on("click", ".historialLes", function () {
            var id = $(this).attr("data-id");
            $("#idhistoryLesion").val(id);
            //var descripcion = $(this).attr("data-value");
            //$("#descripcionhistorialLesion").text(descripcion);
            $("#historyLesion-modal").modal("show");
            window.emptytablehistoryles = false;
            window.tablehistoryLes.api().ajax.reload();
        }
            )
        $("body").on("click", ".historialIntox", function () {
            var id = $(this).attr("data-id");
            $("#idHistoryL").val(id);
            //var descripcion = $(this).attr("data-value");
            //$("#descripcionhistorialintoxicacion").text(descripcion);
            $("#historyIntoxicacion-modal").modal("show");
            window.emptytablehistoryintx = false;
            window.tablehistoryintox.api().ajax.reload();
        });
       
        window.emptytableintoxicaciones = true;
        window.tableintoxicaciones = $('#dt_basicintoxicaciones').dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span style="height: 16px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basicintoxicaciones) {
                    responsiveHelper_dt_basicintoxicaciones = new ResponsiveDatatablesHelper($('#dt_basicintoxicaciones'), breakpointAddDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basicintoxicaciones.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basicintoxicaciones.respond();
                $('#dt_basicintoxicaciones').waitMe('hide');
            },
            "createdRow": function (row, data, index) {
                if (!data["Habilitado"]) {
                    $('td', row).eq(0).addClass('strikeout');
                    $('td', row).eq(1).addClass('strikeout');
                    $('td', row).eq(2).addClass('strikeout');
                    $('td', row).eq(3).addClass('strikeout');
                }
            },
            ajax: {
                type: "POST",
                url: "examenmedico.aspx/getDataIntoxicacion",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosServerSide) {
                    $('#dt_basicintoxicaciones').waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });

                    parametrosServerSide.emptytableadd = false;
                    parametrosServerSide.tracking = $("#ctl00_contenido_ExamenTracking").val();

                    return JSON.stringify(parametrosServerSide);
                }
            },
            columns: [
                null,
                null,
                {
                    name: "Tipo",
                    data: "Tipo",
                    orderable: false
                },
                {
                    name: "Observacion",
                    data: "Observacion",
                    orderable: false
                },
                null,
            ],
            columnDefs: [
                {
                    data: "TrackingId",
                    targets: 0,
                    orderable: false,
                    visible: false,
                    render: function (data, type, row, meta) {
                        return "";
                    }
                },
                {
                    targets: 1,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    targets: 4,
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var edit = "edit";
                        var editar = "";
                        var habilitar = "";
                        var icon = "";
                        var color = "";
                        var txtestatus = "";
                        if (row.Habilitado) {

                            txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                        }
                        else {

                            txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                        }

                        if ($("#ctl00_contenido_HQLNBB").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' editintoxicacion" + href="javascript:void(0);" ' + '" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipoIntoxicacionId + '"data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"data-clasificacion = intoxicacion title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;';
                        if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a data-clasificacion = intoxicacion class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-tracking= "' + row.TrackingId + '"data-tipo= "' + row.TipointoxicacionId + '"data-desc="' + row.Tipo +'" data-observacion= "' + row.Observacion + '"data-id= "' + row.Id + '"title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                        return editar + habilitar + '<a name="history" class="btn btn-default btn-circle historialIntox" href="javascript:void(0);" data-value="' + row.Observacion + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                    }
                }
            ]
        });

        /* Fin listado intoxicaciones*/
        window.emptytablehistoryintx = true;
        window.tablehistoryintox = $("#dt_basichistoryintoxicacion").dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span style="height: 32px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basichistoryIntoxicaciones) {
                    responsiveHelper_dt_basichistoryIntoxicaciones = new ResponsiveDatatablesHelper($("#dt_basichistoryintoxicacion"), breackpontHistoryIntoxDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basichistoryIntoxicaciones.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basichistoryIntoxicaciones.respond();
                $("#dt_basichistoryintoxicacion").waitMe("hide");
            },
            ajax: {
                type: "POST",
                url: "examenmedico.aspx/GetIntoxicacionLog",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosserverside) {
                    $("#dt_basichistoryintoxicacion").waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });
                    var centroid = $('#idHistoryL').val() || "0";
                    parametrosserverside.centroid = centroid;
                    parametrosserverside.todoscancelados = false;
                    parametrosserverside.emptytable = emptytablehistoryintx;
                    return JSON.stringify(parametrosserverside);
                }
            },
            columns: [
                {
                    name: "Observacion",
                    data: "Observacion",
                    ordertable: false
                },
                {
                    name: "Accion",
                    data: "Accion",
                    ordertable: false
                },
                {
                    name: "Creadopor",
                    data: "Creadopor",
                    ordertable: false
                },
                {
                    name: "Fec_Movto",
                    data: "Fec_Movto",
                    ordertable: false
                }
            ],
        });

        var responsiveHelper_dt_basichistorytatuajes=undefined;
        var breakpontHistoryTatuajesDefinition = {
            desktop: Infinity,
            tablet: 1024,
            fablet: 768,
            phone: 480
        };

        window.emptytablehistory = true;
        window.tablehistory = $("#dt_basichistorytatuajes").dataTable({
            "lengthMenu": [10, 20, 50, 100],
            iDisplayLength: 10,
            serverSide: true,
            fixedColumns: true,
            order: [[3, 'asc']],
            autoWidth: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "oLanguage": {
                "sSearch": '<span style="height: 32px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            },
            "preDrawCallback": function () {
                if (!responsiveHelper_dt_basichistorytatuajes) {
                    responsiveHelper_dt_basichistorytatuajes = new ResponsiveDatatablesHelper($("#dt_basichistorytatuajes"), breakpontHistoryTatuajesDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basichistorytatuajes.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basichistorytatuajes.respond();
                $("#dt_basichistorytatuajes").waitMe("hide");
            },
            ajax: {
                type: "POST",
                url: "examenmedico.aspx/GetTatuajeLog",
                contentType: "application/json; charset=utf-8",
                data: function (parametrosserverside) {
                    $("#dt_basichistorytatuajes").waitMe({
                        effect: 'bounce',
                        text: 'Cargando...',
                        bg: 'rgba(255,255,255,0.7)',
                        color: '#000',
                        sizeW: '',
                        sizeH: '',
                        source: ''
                    });
                    var centroid = $('#idHistoryT').val() || "0";
                    parametrosserverside.centroid = centroid;
                    parametrosserverside.todoscancelados = false;
                    parametrosserverside.emptytable = emptytablehistory;
                    return JSON.stringify(parametrosserverside);
                }
            },
            columns: [
                {
                    name: "Lugar",
                    data: "Lugar",
                    ordertable: false
                },
                {
                    name: "Accion",
                    data: "Accion",
                    ordertable: false
                },
                {
                    name: "Creadopor",
                    data: "Creadopor",
                    ordertable: false
                },
                {
                    name: "Fec_Movto",
                    data: "Fec_Movto",
                    ordertable: false
                }
            ],

        });

        $("body").on("click", ".historialT", function () {
            var id = $(this).attr("data-id");
            $('#idHistoryT').val(id);
            //var descripcion = $(this).attr("data-value");
            //$("#descripcionhistorialtatuajes").text(descripcion);
            $("#historyTatuajes-modal").modal("show");
            window.emptytablehistory = false;
            window.tablehistory.api().ajax.reload();
        });

        $("body").on("click", ".blockuser", function () {
            var usernameblock = $(this).attr("data-desc");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
            
            
            $("#btncontinuar").attr("data-tracking", $(this).attr("data-tracking"));
            $("#btncontinuar").attr("data-clasificacion", $(this).attr("data-clasificacion"));
                $("#blockuser-modal").modal("show");
        });  

        $("#btncontinuar").unbind("click").on("click", function () {
            var datos = {       
                    TrackingId : $("#btncontinuar").attr("data-tracking"),                         
                    Clasificacion: $("#btncontinuar").attr("data-clasificacion"),                                      
                };                
                    bloquear(datos);                
            });

        $("#tabexamen").unbind("click").on("click", function () {
            $('#tabexamen').parent().addClass('active');
            $("#refexamen").attr("aria-expanded", "true");
            $("#refpruebas").attr("aria-expanded", "false");
            $('#tabpruebas').parent().removeClass('active');
        });

        $("#tabpruebas").unbind("click").on("click", function () {
            $('#tabpruebas').parent().addClass('active');
            $("#refexamen").attr("aria-expanded", "false");
            $("#refpruebas").attr("aria-expanded", "true");
            $('#tabexamen').parent().removeClass('active');
        });

        $('#fechahora').datepicker({
            format: 'YYYY-MM-DD'
        });



          $("body").on("click", ".saveexamen", function () {
                
                if (validarExamen()) {
                    guardarExamen();
              }
              $("#ctl00_contenido_lblMessage").html("");
        }); 

          function validarExamen() {                
                var esvalido = true;            

                if ($('#ctl00_contenido_tipo_examen').val() == "0" || $('#ctl00_contenido_tipo_examen').val() == null) {
                    ShowError("Tipo de examen", "El tipo de examen es obligatorio.");
                   // $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                  //  $("#ctl00_contenido_tipo_examen").select({ containerCss: { "border-color": "#bdbdbd" } });
                }
               
                if ($("#ocular").val().split(" ").join("") == "") {
                    ShowError("Inspección ocular", "La inspección ocular es obligatoria.");
                    $('#ocular').parent().removeClass('state-success').addClass("state-error");
                    $('#ocular').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ocular').parent().removeClass("state-error").addClass('state-success');
                    $('#ocular').addClass('valid');
                } 

                if ($("#indicaciones").val().split(" ").join("") == "") {
                    ShowError("Indicaciones médicas", "Las indicaciones médicas son obligatorias.");
                    $('#indicaciones').parent().removeClass('state-success').addClass("state-error");
                    $('#indicaciones').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#indicaciones').parent().removeClass("state-error").addClass('state-success');
                    $('#indicaciones').addClass('valid');
              } 
                
               if ($("#observacion").val().split(" ").join("") == "") {
                    ShowError("Observación", "La observación es obligatoria.");
                    $('#observacion').parent().removeClass('state-success').addClass("state-error");
                    $('#observacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observacion').parent().removeClass("state-error").addClass('state-success');
                    $('#observacion').addClass('valid');
               }
                 
                return esvalido;
            }


        function CargarLugar(set) {
            $('#tipo').empty();
            $.ajax({

                type: "POST",
                url: "examenmedico.aspx/GetLugar",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#Lugar');

                    Dropdown.append(new Option("[Lugar]", 0));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));

                    });



                    Dropdown.val(set);
                    Dropdown.trigger("change.select2");


                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de tipos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }
        function pdf2(trackingid) {

            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/pdf2",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    TrackingId: trackingid,
                }),
                cache: false,
                success: function (data) {
                    var resultado = JSON.parse(data.d);
                    if (resultado.success) {
                        open(resultado.file.replace("~", ""));
                    }
                    else {
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                            "Algo salió mal: " + resultado.mensaje + "</div>");
                        setTimeout(hideMessage, hideTime);
                        ShowError("¡Error!", resultado.mensaje);
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

            function guardarExamen() {
                startLoading();
                var examen = ObtenerValoresExamen();
               
                $.ajax({
                    type: "POST",
                    url: "examenmedico.aspx/saveexamen",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'examen': examen }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $("#observacion").val(resultado.Observacion);
                            $('#ctl00_contenido_ExamenTracking').val(resultado.TrackingId);
                            validarsiexisteexamen(resultado.TrackingId);
                            if (data.d.ishit) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Atención! </strong>" +
                                    data.d.msghit, "<br /></div>");
                                ShowAlert("¡Atencion!", data.d.msghit)
                                setTimeout(hideMessage, hideTime);
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                    "La información del examen médico se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                                setTimeout(hideMessage, hideTime);
                                ShowSuccess("¡Bien hecho!", "La información del examen médico se  " + resultado.mensaje + " correctamente.");
                                pdf2(resultado.TrackingId);
                            }
                            $('#addtatuaje').removeClass('disabled');
                            $('#addlesion').removeClass('disabled');
                            $('#addintoxicacion').removeClass('disabled');
                            $('#ocular').parent().removeClass('state-success');
                            $('#indicaciones').parent().removeClass('state-success');
                            $('#observacion').parent().removeClass('state-success');
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }
            function ObtenerValoresExamen() {                                
                var examen = {
                    Edad: 0,
                    SexoId: $('#ctl00_contenido_sexoId').val(),
                    DetalleDetencionTracking: $('#ctl00_contenido_tracking').val(),
                    TipoExamen: $('#ctl00_contenido_tipo_examen').val(),
                    InspeccionOcular: $('#ocular').val(),
                    IndicacionesMedicas: $('#indicaciones').val(),
                    Observacion: $("#observacion").val(),
                    TrackingId: $('#ctl00_contenido_ExamenTracking').val(),
                    DiasSanarLesiones: $("#dias").val(),
                    RiesgoVida:  $('#peligro').is(':checked'),
                    ConsecuenciasLesiones: $('#consecuencias').is(':checked'),      
                    Fecha: $('#fecha').val(),
                    Lesion_visible: $('#Lesion_visible').is(':checked'),
                    Fallecimiento:$('#fallecimiento').is(':checked')
                   
                };
                return examen;
        }

         function pdf(datos) {
                $.ajax({
                    type: "POST",
                    url: "examenmedico.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });

            }

         $("body").on("click", ".saveprueba", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validarPruebaExamen()) {
                    guardarPrueba();
                }
        }); 

                function guardarPrueba() {
                startLoading();
                var prueba = ObtenerValoresPrueba();
                
                $.ajax({
                    type: "POST",
                    url: "examenmedico.aspx/saveprueba",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'prueba': prueba }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                           
                            $('#ctl00_contenido_PruebaTracking').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información de pruebas de examen médico se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información de pruebas de examen médico se  " + resultado.mensaje + " correctamente.");                            
                            $('#main').waitMe('hide');
                            $('#ta').parent().removeClass('state-success');
                            $('#alcoholimetro').parent().removeClass('state-success');
                            $('#fc').parent().removeClass('state-success');
                            $('#pulso').parent().removeClass('state-success');
                            $('.input').removeClass('state-success');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            if(resultado.mensaje == "No existe un exámen médico para esta prueba") ShowError("¡Error! Algo salió mal", resultado.mensaje);
                            else ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }
            function ObtenerValoresPrueba() {                                
                var prueba = {

                   
                    ExamenMedicoTracking : $("#ctl00_contenido_ExamenTracking").val(),
                    TrackingId : $("#ctl00_contenido_PruebaTracking").val(),                
                    MucosasId : $("#ctl00_contenido_mucosas").val(),
                    AlientoId : $("#ctl00_contenido_aliento").val(),
                    Examen_neurologicoId : $("#ctl00_contenido_neurologico").val(),
                    Disartia : $("#ctl00_contenido_disartia").val(),
                    ConjuntivasId : $("#ctl00_contenido_conjuntivas").val(),
                    MarchaId : $("#ctl00_contenido_marcha").val(),
                    PupilasId : $("#ctl00_contenido_pupilas").val(),
                    CoordinacionId : $("#ctl00_contenido_coordinacion").val(),
                    Reflejos_pupilaresId : $("#ctl00_contenido_pupilares").val(),
                    TendinososId : $("#ctl00_contenido_osteo").val(),
                    RomberqId : $("#ctl00_contenido_romberq").val(),
                    ConductaId : $("#ctl00_contenido_conducta").val(),
                    LenguajeId : $("#ctl00_contenido_lenguaje").val(),
                    AtencionId : $("#ctl00_contenido_atencion").val(),
                    OrientacionId : $("#ctl00_contenido_orientacion").val(),
                    DiadococinenciaId : $("#ctl00_contenido_diadococinencia").val(),
                    DedoId : $("#ctl00_contenido_dedo").val(),
                    TalonId : $("#ctl00_contenido_talon").val(),
                    Alcoholimetro : $("#alcoholimetro").val(),
                    TA : $("#ta").val(),
                    FC : $("#fc").val(),
                    FR : $("#fr").val(),
                    Pulso : $("#pulso").val()                   
                };
                return prueba;
        }

        function validarPruebaExamen() {
            var esvalido = true;
            
                if ($("#alcoholimetro").val().split(" ").join("") == "") {
                    ShowError("Alcoholímetro", "El campo alcoholímetro es obligatorio.");
                    $('#alcoholimetro').parent().removeClass('state-success').addClass("state-error");
                    $('#alcoholimetro').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#alcoholimetro').parent().removeClass("state-error").addClass('state-success');
                    $('#alcoholimetro').addClass('valid');
                } 

               if ($("#ta").val().split(" ").join("") == "") {
                    ShowError("TA", "El campo TA es obligatorio.");
                    $('#ta').parent().removeClass('state-success').addClass("state-error");
                    $('#ta').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ta').parent().removeClass("state-error").addClass('state-success');
                    $('#ta').addClass('valid');
                } 
            
               if ($("#fc").val().split(" ").join("") == "") {
                    ShowError("FC", "El campo FC es obligatorio.");
                    $('#fc').parent().removeClass('state-success').addClass("state-error");
                    $('#fc').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fc').parent().removeClass("state-error").addClass('state-success');
                    $('#fc').addClass('valid');
            } 
            if ($("#fr").val().split(" ").join("") == "") {
                ShowError("FR", "El campo FR es obligatorio.");
                $('#fr').parent().removeClass('state-success').addClass("state-error");
                $('#fr').removeClass('valid');
                esvalido = false;
            }
            else {
                $('#fr').parent().removeClass("state-error").addClass('state-success');
                $('#fr').addClass('valid');
            }

            
            if ($("#pulso").val().split(" ").join("") == "") {
                    ShowError("Pulso", "El campo pulso es obligatorio.");
                    $('#pulso').parent().removeClass('state-success').addClass("state-error");
                    $('#pulso').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#pulso').parent().removeClass("state-error").addClass('state-success');
                    $('#pulso').addClass('valid');
                }

            if ($('#ctl00_contenido_mucosas').val() == "0" || $('#ctl00_contenido_mucosas').val() == null) {
                    ShowError("Mucosas", "El campo mucosas es obligatorio.");
                    $("#ctl00_contenido_mucosas").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_mucosas").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

              if ($('#ctl00_contenido_aliento').val() == "0" || $('#ctl00_contenido_aliento').val() == null) {
                    ShowError("Aliento", "El campo aliento es obligatorio.");
                    $("#ctl00_contenido_aliento").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_aliento").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

               if ($('#ctl00_contenido_neurologico').val() == "0" || $('#ctl00_contenido_neurologico').val() == null) {
                    ShowError("Examen neurológico", "El campo examen neurológico es obligatorio.");
                    $("#ctl00_contenido_neurologico").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_neurologico").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_conjuntivas').val() == "0" || $('#ctl00_contenido_conjuntivas').val() == null) {
                    ShowError("Conjuntivas", "El campo conjuntivas es obligatorio.");
                    $("#ctl00_contenido_conjuntivas").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_conjuntivas").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_marcha').val() == "0" || $('#ctl00_contenido_marcha').val() == null) {
                    ShowError("Marcha", "El campo marcha es obligatorio.");
                    $("#ctl00_contenido_marcha").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_marcha").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
          

            if ($('#ctl00_contenido_pupilas').val() == "0" || $('#ctl00_contenido_pupilas').val() == null) {
                    ShowError("Pupilas", "El campo pupilas es obligatorio.");
                    $("#ctl00_contenido_pupilas").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_pupilas").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_coordinacion').val() == "0" || $('#ctl00_contenido_coordinacion').val() == null) {
                    ShowError("Coordinación", "El campo coordinación es obligatorio.");
                    $("#ctl00_contenido_coordinacion").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_coordinacion").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_pupilares').val() == "0" || $('#ctl00_contenido_pupilares').val() == null) {
                    ShowError("Reflejos pupilares", "El campo reflejos pupilares es obligatorio.");
                    $("#ctl00_contenido_pupilares").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_pupilares").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_osteo').val() == "0" || $('#ctl00_contenido_osteo').val() == null) {
                    ShowError("Reflejos osteo tendinosos", "El campo reflejos osteo tendinosos es obligatorio.");
                    $("#ctl00_contenido_osteo").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_osteo").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
           

            if ($('#ctl00_contenido_romberq').val() == "0" || $('#ctl00_contenido_romberq').val() == null) {
                    ShowError("Romberg", "El campo romberg es obligatorio.");
                    $("#ctl00_contenido_romberq").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_romberq").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            
            if ($('#ctl00_contenido_disartia').val() == "0" || $('#ctl00_contenido_disartia').val() == "-1" || $('#ctl00_contenido_disartia').val() == null) {
                    ShowError("Disartía", "El campo disartía es obligatorio.");
                    $("#ctl00_contenido_orientacion").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_disartia").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_conducta').val() == "0" || $('#ctl00_contenido_conducta').val() == null) {
                    ShowError("Conducta", "El campo conducta es obligatorio.");
                    $("#ctl00_contenido_conducta").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_conducta").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
            if ($('#ctl00_contenido_lenguaje').val() == "0" || $('#ctl00_contenido_lenguaje').val() == null) {
                    ShowError("Lenguaje", "El campo lenguaje es obligatorio.");
                    $("#ctl00_contenido_lenguaje").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_lenguaje").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
            
            if ($('#ctl00_contenido_atencion').val() == "0" || $('#ctl00_contenido_atencion').val() == null) {
                    ShowError("Atención", "El campo atención es obligatorio.");
                    $("#ctl00_contenido_atencion").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_atencion").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
        
            
            if ($('#ctl00_contenido_diadococinencia').val() == "0" || $('#ctl00_contenido_diadococinencia').val() == null) {
                    ShowError("Diadococinencia", "El campo diadococinencia es obligatorio.");
                    $("#ctl00_contenido_diadococinencia").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_diadococinencia").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
         
            if ($('#ctl00_contenido_dedo').val() == "0" || $('#ctl00_contenido_dedo').val() == null) {
                    ShowError("Dedo-nariz", "El campo dedo-nariz es obligatorio.");
                    $("#ctl00_contenido_dedo").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_dedo").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
         
            if ($('#ctl00_contenido_talon').val() == "0" || $('#ctl00_contenido_talon').val() == null) {
                    ShowError("Talón-rodilla", "El campo talón-rodilla es obligatorio.");
                    $("#ctl00_contenido_talon").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_talon").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if ($('#ctl00_contenido_orientacion').val() == "0" || $('#ctl00_contenido_orientacion').val() == null) {
                ShowError("Orientación", "El campo orientación es obligatorio.");
                $("#ctl00_contenido_orientacion").select({ containerCss: { "border-color": "#a90329" } });
                esvalido = false;
            }
            else {
                $("#ctl00_contenido_orientacion").select({ containerCss: { "border-color": "#bdbdbd" } });
            }
          
            return esvalido;
        }
        function validarsiexisteexamen(trackingid) {
            
            
            startLoading();
            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/Validateexamenexistente",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingid: trackingid,
                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {

                        
                        var existe = resultado.prueba;
                        if (existe=="Fallido") {
                            $("#tabpruebas").hide();
                            
                        }
                        else {
                            $("#tabpruebas").show();
                        }
                        
                        

                       

                    } else {
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }
        function CargarDatos(trackingid) {
                var param = RequestQueryString("tracking");
                
                if (param != undefined) {
                    trackingid=param
                   
                }
            startLoading();
            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/getdatos",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingid: trackingid,
                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {

                         $('#nombreInterno').text(resultado.obj.Nombre);

                        $('#expedienteInterno').text(resultado.obj.Expediente);
                        $('#centroInterno').text(resultado.obj.Centro);
                        $('#edadInterno').text(resultado.obj.Edad);
                        $('#sexoInterno').text(resultado.obj.Sexo);
                        $('#ctl00_contenido_sexoId').val(resultado.obj.SexoId);
                        $('#domicilioInterno').text(resultado.obj.CalleDomicilio);
                        $('#coloniaInterno').text(resultado.obj.coloniaNombre);
                        $('#municipioInterno').text(resultado.obj.municipioNombre);
                        if (resultado.obj.Lesion_Visible== "True") {

                            $("#Lesion_visible").prop('checked', true);
                        }
                        else {
                            $("#Lesion_visible").prop('checked', false);
                        }

                        var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);                        
                        if (imagenAvatar != null && imagenAvatar != "") {
                           
                            $('#avatar').attr("src", imagenAvatar);
                        }
                        else {
                            var fotoNull = ResolveUrl("~/Content/img/avatars/male.png");
                            $('#avatar').attr("src", fotoNull);
                        }                            
                        $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);
                        $('#ctl00_contenido_trackingUser').val(resultado.obj.TrackingId);
                     
                    } else {
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarDatosExamen() {
            startLoading();

            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/getDatosExamen",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingExamenId: $("#ctl00_contenido_ExamenTracking").val()

                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {

                        //Datos exmane medico               
                   
                        $('#ocular').val(resultado.objexamen.InspeccionOcular);
                        $('#indicaciones').val(resultado.objexamen.IndicacionesMedicas);
                        $("#observacion").val(resultado.objexamen.Observacion);
                        $("#dias").val(resultado.objexamen.DiasSanarLesiones);
                        CargarListado(resultado.objexamen.Tipo, "tipo_examen");

                     
                        $('#fecha').val(resultado.objexamen.Fecha);

                        if (resultado.objexamen.Lesion_Visible == "True") {
                            $("#Lesion_visible").prop('checked', true);
                        }
                        else {
                            $("#Lesion_visible").prop('checked', false);
                        }
                        if (resultado.objexamen.Fallecimiento == "True") {
                            $("#fallecimiento").prop('checked', true);
                        }
                        else {
                            $("#fallecimiento").prop('checked', false);
                        }
                        if (resultado.objexamen.RiesgoVida == "True") {
                            $("#peligro").prop('checked', true);
                        }
                        else {
                            $("#peligro").prop('checked', false);
                        }
                        if (resultado.objexamen.ConsecuenciasLesiones == "True") {
                            $("#consecuencias").prop('checked', true);
                        }
                        else {
                            $("#consecuencias").prop('checked', false);
                        }

                        if (resultado.objprueba != null) {
                            $("#alcoholimetro").val(resultado.objprueba.Alcoholimetro);
                            $("#ta").val(resultado.objprueba.TA);
                            $("#fc").val(resultado.objprueba.FC);
                            $("#fr").val(resultado.objprueba.FR);
                            $("#pulso").val(resultado.objprueba.Pulso);
                            CargarListado(resultado.objprueba.MucosasId, "mucosas");
                            CargarListado(resultado.objprueba.AlientoId, "aliento");
                            CargarListado(resultado.objprueba.Examen_neurologicoId, "neurologico");

                            CargarListado(resultado.objprueba.ConjuntivasId, "conjuntivas");
                            CargarListado(resultado.objprueba.MarchaId, "marcha");
                            CargarListado(resultado.objprueba.PupilasId, "pupilas");
                            CargarListado(resultado.objprueba.CoordinacionId, "coordinacion");
                            CargarListado(resultado.objprueba.Reflejos_pupilaresId, "pupilares");
                            CargarListado(resultado.objprueba.TendinososId, "osteo");
                            CargarListado(resultado.objprueba.RomberqId, "romberq");
                            CargarListado(resultado.objprueba.ConductaId, "conducta");
                            CargarListado(resultado.objprueba.LenguajeId, "lenguaje");
                            CargarListado(resultado.objprueba.AtencionId, "atencion");
                            CargarListado(resultado.objprueba.OrientacionId, "orientacion");
                            CargarListado(resultado.objprueba.DiadococinenciaId, "diadococinencia");
                            CargarListado(resultado.objprueba.DedoId, "dedo");
                            CargarListado(resultado.objprueba.TalonId, "talon");

                            if (resultado.objprueba.Disartia == "True")
                                $("#ctl00_contenido_disartia option[value=true]").attr('selected', 'selected');

                            if (resultado.objprueba.Disartia == "False")
                                $("#ctl00_contenido_disartia option[value=false]").attr('selected', 'selected');

                        }
                        else {
                            CargarListado(0, "mucosas");
                            CargarListado(0, "aliento");
                            CargarListado(0, "neurologico");
                            CargarListado(0, "conjuntivas");
                            CargarListado(0, "marcha");
                            CargarListado(0, "pupilas");
                            CargarListado(0, "coordinacion");
                            CargarListado(0, "pupilares");
                            CargarListado(0, "osteo");
                            CargarListado(0, "romberq");
                            CargarListado(0, "conducta");
                            CargarListado(0, "lenguaje");
                            CargarListado(0, "atencion");
                            CargarListado(0, "orientacion");
                            CargarListado(0, "diadococinencia");
                            CargarListado(0, "dedo");
                            CargarListado(0, "talon");
                        }
                    } else {
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarDatosPruebas() {
             startLoading();

            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/getDatosPruebas",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingExamenId: $("#ctl00_contenido_PruebaTracking").val()
                    
                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {

                        //Datos Pruebas

                        CargarListado(resultado.objprueba.Mucosas, "mucosas");

                        //Datos exmane medico               
                       
                        $('#ocular').val(resultado.objexamen.InspeccionOcular);
                        $('#indicaciones').val(resultado.objexamen.IndicacionesMedicas);
                        $("#observacion").val(resultado.objexamen.Observacion);
                        $("#dias").val(resultado.objexamen.DiasSanarLesiones);
                        CargarListado(resultado.objexamen.Tipo, "tipo_examen");
                        $('#fecha').val(resultado.objexamen.Fecha);

                        if (resultado.objprueba != null) {
                            $("#alcoholimetro").val(resultado.objprueba.Alcoholimetro);
                            $("#ta").val(resultado.objprueba.TA);
                            $("#fc").val(resultado.objprueba.FC);
                            $("#fr").val(resultado.objprueba.FR);
                            $("#pulso").val(resultado.objprueba.Pulso);
                            CargarListado(resultado.objprueba.MucosasId, "mucosas");
                            CargarListado(resultado.objprueba.AlientoId, "aliento");
                            CargarListado(resultado.objprueba.Examen_neurologicoId, "neurologico");

                            CargarListado(resultado.objprueba.ConjuntivasId, "conjuntivas");
                            CargarListado(resultado.objprueba.MarchaId, "marcha");
                            CargarListado(resultado.objprueba.PupilasId, "pupilas");
                            CargarListado(resultado.objprueba.CoordinacionId, "coordinacion");
                            CargarListado(resultado.objprueba.Reflejos_pupilaresId, "pupilares");
                            CargarListado(resultado.objprueba.TendinososId, "osteo");
                            CargarListado(resultado.objprueba.RomberqId, "romberq");
                            CargarListado(resultado.objprueba.ConductaId, "conducta");
                            CargarListado(resultado.objprueba.LenguajeId, "lenguaje");
                            CargarListado(resultado.objprueba.AtencionId, "atencion");
                            CargarListado(resultado.objprueba.OrientacionId, "orientacion");
                            CargarListado(resultado.objprueba.DiadococinenciaId, "diadococinencia");
                            CargarListado(resultado.objprueba.DedoId, "dedo");
                            CargarListado(resultado.objprueba.TalonId, "talon");
                        }
                      
                      
                     
                    } else {
                        ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

       $("body").on("click", "#addtatuaje", function () {
           $("#ctl00_contenido_lblMessage").html("");
           limpiarAdicional();
           CargarListado(0, "tipo_tatuaje");    
           CargarListado(0, "lugar");
                $("#ctl00_contenido_observacionadicional").val("");       
                $("#ctl00_contenido_lugaradicional").val(""); 
                $("#btnsaveadicional").attr("data-id", "");
                $("#btnsaveadicional").attr("data-tracking", "");
                $("#btnsaveadicional").attr("data-clasificacion", "tatuaje");
                $("#form-modal-title").empty();
                $("#sectionLugar").show();
                $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar tatuaje");
                $("#form-modal").modal("show");
            });
       
        
        $("body").on("click", "#addlesion", function () {
            $("#ctl00_contenido_lblMessage").html("");
            limpiarAdicional();
            CargarListado(0, "tipo_lesion");
            CargarListado(0, "lugar");
            $("#ctl00_contenido_observacionadicional").val("");
            $("#ctl00_contenido_lugaradicional").val("");
            $("#btnsaveadicional").attr("data-id", "");
            $("#btnsaveadicional").attr("data-tracking", "");
            $("#btnsaveadicional").attr("data-clasificacion", "lesion");
            $("#form-modal-title").empty();
            $("#sectionLugar").show();
            $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar lesión");
            $("#form-modal").modal("show");
        });
                
        $("body").on("click", "#addintoxicacion", function () {
            $("#ctl00_contenido_lblMessage").html("");
            limpiarAdicional();
            CargarListado(0, "tipo_intoxicacion");

            $("#ctl00_contenido_observacionadicional").val("");
            $("#ctl00_contenido_lugaradicional").val("");
            $("#btnsaveadicional").attr("data-id", "");
            $("#btnsaveadicional").attr("data-tracking", "");
            $("#btnsaveadicional").attr("data-clasificacion", "intoxicacion");
            $("#form-modal-title").empty();
            $("#sectionLugar").hide();
            $("#form-modal-title").html("<i class='fa fa-pencil'></i> Agregar intoxicación");
            $("#form-modal").modal("show");
        });

        $("body").on("click", ".edittatuaje", function () {
            limpiarAdicional();
            $("#ctl00_contenido_lblMessage").html("");

            $("#btnsaveadicional").attr("data-id", $(this).attr("data-id"));
            $("#btnsaveadicional").attr("data-tracking", $(this).attr("data-tracking"));
            $("#btnsaveadicional").attr("data-clasificacion", $(this).attr("data-clasificacion"));
            $("#ctl00_contenido_lugaradicional").val($(this).attr("data-lugar"));
            $("#ctl00_contenido_observacionadicional").val($(this).attr("data-observacion"));
            $("#ctl00_contenido_tipoadicional").val($(this).attr("data-tipo"));
            CargarListado($(this).attr("data-tipo"), "tipo_tatuaje");
            CargarListado($(this).attr("data-lugar"), "lugar");
            $("#sectionLugar").show();
            $("#form-modal-title").empty();
            $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar tatuaje");
            $("#form-modal").modal("show");
        });

        $("body").on("click", ".editintoxicacion", function () {
            limpiarAdicional();
            $("#ctl00_contenido_lblMessage").html("");

            $("#btnsaveadicional").attr("data-id", $(this).attr("data-id"));
            $("#btnsaveadicional").attr("data-tracking", $(this).attr("data-tracking"));
            $("#btnsaveadicional").attr("data-clasificacion", $(this).attr("data-clasificacion"));
            $("#ctl00_contenido_observacionadicional").val($(this).attr("data-observacion"));
            $("#ctl00_contenido_tipoadicional").val($(this).attr("data-tipo"));
            CargarListado($(this).attr("data-tipo"), "tipo_intoxicacion");
            $("#sectionLugar").hide();
            $("#form-modal-title").empty();
            $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar intoxicación");
            $("#form-modal").modal("show");
        });

        $("body").on("click", ".editlesion", function () {
            limpiarAdicional();
            $("#ctl00_contenido_lblMessage").html("");

            $("#btnsaveadicional").attr("data-id", $(this).attr("data-id"));
            $("#btnsaveadicional").attr("data-tracking", $(this).attr("data-tracking"));
            $("#btnsaveadicional").attr("data-clasificacion", $(this).attr("data-clasificacion"));
            $("#ctl00_contenido_lugaradicional").val($(this).attr("data-lugar"));
            $("#ctl00_contenido_observacionadicional").val($(this).attr("data-observacion"));
            $("#ctl00_contenido_tipoadicional").val($(this).attr("data-tipo"));
            CargarListado($(this).attr("data-tipo"), "tipo_lesion");
            CargarListado($(this).attr("data-lugar"), "lugar");
            $("#sectionLugar").show();
            $("#form-modal-title").empty();
            $("#form-modal-title").html("<i class='fa fa-pencil'></i> Editar lesión");
            $("#form-modal").modal("show");
        });

        $("body").on("click", ".saveadicional", function () {

            var datos = {
                Id: $("#btnsaveadicional").attr("data-id"),
                ExamenTrackingId: $("#ctl00_contenido_ExamenTracking").val(),
                TrackingId: $("#btnsaveadicional").attr("data-tracking"),
                Lugar: $("#ctl00_contenido_lugaradicional").val(),
                Observacion: $("#ctl00_contenido_observacionadicional").val(),
                Tipo: $("#ctl00_contenido_tipoadicional").val(),
                Clasificacion: $("#btnsaveadicional").attr("data-clasificacion"),


            };
            if (validarAdicional($(this).attr("data-clasificacion"))) {
                saveAdicional(datos);
            }

        });

        function validarAdicional(clasificacion) {
            var esvalido = true;

            if ($('#ctl00_contenido_tipoadicional').val() == "0" || $('#ctl00_contenido_tipoadicional').val() == null) {
                ShowError("Tipo", "El campo tipo es obligatorio.");
                $("#ctl00_contenido_tipoadicional").select({ containerCss: { "border-color": "#a90329" } });
                esvalido = false;
            }
            else {
                $("#ctl00_contenido_tipoadicional").select({ containerCss: { "border-color": "#bdbdbd" } });
            }

            if (clasificacion != "intoxicacion") {


                if ($('#ctl00_contenido_lugaradicional').val() == "0" || $('#ctl00_contenido_lugaradicional').val() == null) {
                    ShowError("Lugar", "El campo lugar es obligatorio.");
                    $("#ctl00_contenido_lugaradicional").select({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#ctl00_contenido_lugaradicional").select({ containerCss: { "border-color": "#bdbdbd" } });
                }

            }
            return esvalido;
        }

        function limpiarAdicional() {
            $('#ctl00_contenido_tipoadicional').parent().removeClass('state-success');

            $('#ctl00_contenido_tipoadicional').parent().removeClass("state-error");
            $('#ctl00_contenido_lugaradicional').parent().removeClass('state-success');
            $('#ctl00_contenido_lugaradicional').parent().removeClass("state-error");
            $('#ctl00_contenido_observacionadicional').parent().removeClass('state-success');
            $('#ctl00_contenido_observacionadicional').parent().removeClass("state-error");
        }

        function saveAdicional(datos) {
            startLoading();
            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/saveAdicional",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    item: datos
                }),
                cache: false,
                success: function (data) {
                    var resultado = data.d;
                    if (resultado.exitoso) {
                        window.emptytablelesiones = false;
                        window.tablelesiones.api().ajax.reload();
                        window.emptytableintoxicaciones = false;
                        window.tableintoxicaciones.api().ajax.reload();
                        window.emptytabletatuajes = false;
                        window.tabletatuajes.api().ajax.reload();


                        $("#form-modal").modal("hide");
                        $('#main').waitMe('hide');
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                            "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                        setTimeout(hideMessage, hideTime);
                        ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                    } else {
                        ShowError("¡Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                    $('#main').waitMe('hide');

                    $('#tabexamen').parent().addClass('active');
                    $("#refexamen").attr("aria-expanded", "true");
                    $("#refpruebas").attr("aria-expanded", "false");
                    $('#tabpruebas').parent().removeClass('active');
                },
                error: function () {
                    ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                    $('#main').waitMe('hide');
                }
            });
        }

        function bloquear(datos) {
            startLoading();
            $.ajax({
                url: "examenmedico.aspx/blockuser",
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    item: datos,
                }),
                success: function (data) {
                    var data = JSON.parse(data.d);
                    if (data.exitoso) {
                        window.emptytablelesiones = false;
                        window.tablelesiones.api().ajax.reload();
                        window.emptytableintoxicaciones = false;
                        window.tableintoxicaciones.api().ajax.reload();
                        window.emptytabletatuajes = false;
                        window.tabletatuajes.api().ajax.reload();
                        $("#blockuser-modal").modal("hide");
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                            data.mensaje + "</div>");
                        setTimeout(hideMessage, hideTime);
                        ShowSuccess("¡Bien hecho!", data.mensaje);
                    }
                    else {
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                            "Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                        setTimeout(hideMessage, hideTime);
                        ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del usuario. . Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                    $('#main').waitMe('hide');
                }
            });
        }

        function CargarListado(id, select) {
            $('#ctl00_contenido_' + select).empty();
            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/getListado",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({

                    select: select
                }),
                cache: false,
                success: function (response) {
                    if (select == "tipo_tatuaje" || select == "tipo_intoxicacion" || select == "tipo_lesion" )
                        select = "tipoadicional";
                    if (select == "lugar" )
                        select = "lugaradicional";
                    var Dropdown = $('#ctl00_contenido_' + select);


                    Dropdown.empty();
                    Dropdown.append(new Option("[Seleccione]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));

                    });
                    if (id != "") {
                        Dropdown.val(id);
                        Dropdown.trigger("change.select");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de" + select + ". Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }
        
        function ResolveUrl(url) {
            var baseUrl = "<%= ResolveUrl("~/") %>";
            if (url.indexOf("~/") == 0) {
                url = baseUrl + url.substring(2);
            }
            return url;
        }

        function loadDate() {
            
            var date = new Date();
           
            var dia = date.getDate();
            var mes = date.getMonth() + 1;
            var anio = date.getFullYear();
            var hora = date.getHours();
            var minutos = date.getMinutes();
            var segundos = date.getSeconds();


            dia = dia.toString().length == 1 ? "0" + dia : dia;
            mes = mes.toString().length == 1 ? "0" + mes : mes;
            hora = hora.toString().length == 1 ? "0" + hora : hora;
            minutos = minutos.toString().length == 1 ? "0" + minutos : minutos;
            segundos = segundos.toString().length == 1 ? "0" + segundos : segundos;
            var fechaCompleta = dia + "/" + mes + "/" + anio + " " + hora + ":" + minutos + ":" + segundos;

            $('#fecha').val(fechaCompleta);
        }

        $('#dias').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        $("body").on("click", ".detencion", function () {
            $("#datospersonales-modal").modal("show");
            var param = $("#ctl00_contenido_trackingUser").val();
            CargarDatosModal(param);
        });

        $("body").on("click", "#sexoEdad", function () {
            $("#sexoedad-modal").modal("show");
            $('#ctl00_contenido_sexo').parent().removeClass('state-success');
            $('#ctl00_contenido_sexo').parent().removeClass("state-error");
            $('#edad').parent().removeClass('state-success');
            $('#edad').parent().removeClass("state-error");

            var param = $("#ctl00_contenido_trackingUser").val();
            CargarSexoEdad(param);
        });

        $("body").on("click", "#btncontinuarsexo", function () {
            if (validarSexoEdad()) {
                $("#confirm-modal").modal("show");
            }
        });

        $('#fechadatetimepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        //$('#fechadatetimepicker').data("DateTimePicker").hide();

        function CargarSexoEdad(trackingid) {
            startLoading();
            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/getSexoEdad",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingid: trackingid,
                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {
                        $("#edad").val(resultado.obj.Edad);
                        $("#fechaNacimiento").val(resultado.obj.FechaNacimiento);
                        CargarSexo(resultado.obj.Sexo);
                    }
                    else {
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarDatosModal(trackingid) {
            startLoading();
            $.ajax({
                type: "POST",
                url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    trackingid: trackingid,
                }),
                cache: false,
                success: function (data) {

                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {
                        $("#fechaInfo").val(resultado.obj.FechaDetencion);
                        $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                        $("#eventoInfo").val(resultado.obj.EventoNombre);
                        $("#folioInfo").val(resultado.obj.Folio);
                        $("#unidadInfo").val(resultado.obj.UnidadNombre);
                        $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                        $("#descripcionInfo").val(resultado.obj.Motivo);
                        $("#detalleInfo").val(resultado.obj.Descripcion);
                        $("#lugarInfo").val(resultado.obj.Lugar);
                        $("#paisInfo").val(resultado.obj.PaisNombre);
                        $("#estadoInfo").val(resultado.obj.EstadoNombre);
                        $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                        $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                        $("#cpInfo").val(resultado.obj.CodigoPostal);
                        $("#estaturaInfo").val(resultado.obj.Estatura);
                        $("#pesoInfo").val(resultado.obj.Peso);
                        $("#institucionInfo").val(resultado.obj.Centro);
                        $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                        $("#nombreInfo").val(resultado.obj.Nombre);
                        var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                        if (imagenAvatar != null && imagenAvatar != "") {
                            $('#imgInfo').attr("src", imagenAvatar);
                        }
                        else {
                            var fotoNull = ResolveUrl("~/Content/img/avatars/male.png");
                            $('#imgInfo').attr("src", fotoNull);
                        }
                    }
                    else {
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                    }

                    $('#main').waitMe('hide');

                },
                error: function () {
                    $('#main').waitMe('hide');
                    ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        $("body").on("click", "#btnconfirm", function () {
            saveSexoEdad();
            $("#confirm-modal").modal("hide");
        });

        function validarSexoEdad() {                
            var esvalido = true;                

            if ($('#ctl00_contenido_sexo').val() == "0" || $('#ctl00_contenido_sexo').val() == null) {
                ShowError("Sexo", "El campo de sexo es obligatorio.");
                $("#ctl00_contenido_sexo").select({ containerCss: { "border-color": "#a90329" } });
                esvalido = false;
            }
            else {
                $('#ctl00_contenido_sexo').parent().removeClass("state-error").addClass('state-success');
                $('#ctl00_contenido_sexo').addClass('valid');
            }

            if ($("#edad").val().split(" ").join("") == "") {
                ShowError("Edad", "El campo edad es obligatorio.");
                $('#edad').parent().removeClass('state-success').addClass("state-error");
                $('#edad').removeClass('valid');
                esvalido = false;
            }
            else {
                $('#edad').parent().removeClass("state-error").addClass('state-success');
                $('#edad').addClass('valid');
            }

            return esvalido;
        }

        function saveSexoEdad() {
            startLoading();

            datos = [
                Edad = $('#edad').val(),
                Sexo = $('#ctl00_contenido_sexo').val(),
                DetalleDetencionTracking = $('#ctl00_contenido_tracking').val()
            ];

            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/saveSexoEdad",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    datos: datos
                }),
                cache: false,
                success: function (data) {
                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {
                        ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                        $("#sexoedad-modal").modal("hide");
                        $("#confirm-modal").modal("hide");

                        $('#sexoInterno').text(resultado.sexo);
                        $('#edadInterno').text(resultado.edad);

                        $('#main').waitMe('hide');
                    }
                    else {
                        ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                    $('#main').waitMe('hide');
                },
                error: function () {
                    ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                    $('#main').waitMe('hide');
                }
            });
        }

        function CargarSexo(setsexo) {
            $('#ctl00_contenido_sexo').empty();
            $.ajax({
                type: "POST",
                url: "examenmedico.aspx/getSexo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_sexo');

                    Dropdown.append(new Option("[Sexo]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));

                    });

                    if (setsexo != "") {

                        Dropdown.val(setsexo);
                        Dropdown.trigger("change.select2");

                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de sexo. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function addText() {
            $("#observacion").value = $("#observacion").val()
        }

        var hideTime = 5000;
        function hideMessage() {
            $("#ctl00_contenido_lblMessage").html("");
        }

        $("#showInfoDetenido").click(function () {
            $("#hideInfoDetenido").css('display', 'block');
            $("#showInfoDetenido").css('display', 'none');

            $("#SectionInfoDetenido").show();
            $("#ScrollableContent").css("height", "calc(100vh - 400px)");
        })
        $("#hideInfoDetenido").click(function () {
            $("#hideInfoDetenido").css('display', 'none');
            $("#showInfoDetenido").css('display', 'block');

            $("#SectionInfoDetenido").hide();
            $("#ScrollableContent").css("height", "calc(100vh - 180px)");
        });

    </script>
</asp:Content>

