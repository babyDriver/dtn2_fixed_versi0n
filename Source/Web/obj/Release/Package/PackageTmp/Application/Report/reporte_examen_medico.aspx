﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="reporte_examen_medico.aspx.cs" Inherits="Web.Application.Report.reporte_examen_medico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
        <li>Examen médico</li>
    <li>Examen médico</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">    
    <link href="../../Content/css/bootstrap.css" rel="stylesheet"/>
     <style type="text/css">
        .dropdown-menu{
            position: relative;
        }
         td.strikeout {
            text-decoration: line-through;
        }
      
        #dropdown {
            width: 459px;
        }
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }
        #mapid {
            height: 20vw;       
            width: 100%;
        }
        .coordinates {
            background: rgba(0,0,0,0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding:5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }
        .modal {
          overflow-y:auto;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 13px;
            line-height: 1.42857143;
        }
        input[type=checkbox]{
            -ms-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
            padding: 10px;
            cursor: pointer;
        }
        .checkboxtext{
            font-size: 100%;
            display: inline;       
            color: #004987;
        }
        .checkboxtext:hover{
            cursor:pointer;
        }

        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-file-text-o"></i>
                Reporte de examen médico
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-users-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false" data-widget-togglebutton="false">
                    <header>
<%--                        <span class="widget-icon"><i class="fa fa-file-text-o"></i></span>--%>

              <img style="width:33px; height:33px; margin-bottom:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/MedicalTestLineWhite.png" />

                        <h2>Examen médico </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th>Apellido materno</th>
                                        <th data-hide="phone,tablet">Sexo</th>                                        
                                        <th>Fecha y hora de evaluación</th>
                                        <th>Fecha y hora de registro</th>                                        
                                        <th data-hide="phone,tablet">Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="VYXMBM" runat="server" value="" />
    <input type="hidden" id="RAWMOV" runat="server" value="" />
    <input type="hidden" id="WERQEQ" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,

                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {

                },
                ajax: {
                    type: "POST",
                    url: "reporte_examen_medico.aspx/getinterno",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "Id",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Sexo",
                        data: "Sexo"
                    },
                    {
                        name: "Evaluacion",
                        data: "Evaluacion"
                    },
                    {
                        name: "Registro",
                        data: "Registro"
                    },
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false

                    }
                ],
                columnDefs: [
                    {
                        data: "Id",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var verReporte = "";

                            var action2 = '';
                            var action3 = '';
                            action2 = '<div class="btn-group" style="width:100%"><button  class="btn btn-primary dropdown-toggle"   data-toggle="dropdown"> <i class="fa fa-user-md"></i>  Examen  médico     &nbsp;&nbsp;&nbsp;&nbsp;<span class="caret"></span></button><ul class="dropdown-menu">'
                            if (row.CertificadoMedicoPsicofisiologicoId != "0") {
                                action2 += '<li><a class="btn-sm printcertificadofisio" href="javascript:void(0);" id="printcertificadofisio" data-id="' + row.CertificadoMedicoPsicofisiologicoId + '" data-examen-id="' + row.ExamenMedicoId + '" title = "Psicofisiológico" > <i class="fa fa-print"></i> Psicofisiológico</a></li><li class="divider"></li>'


                            }
                            if (row.CertificadoLesionId != "0") {
                                action2 += '<li><a class="btn-sm printcertificadolesion" href="javascript:void(0);" id="printcertificadolesion" data-id="' + row.CertificadoLesionId + '" data-examen-id="' + row.ExamenMedicoId + '" title = "Lesiones" > <i class="fa fa-print"></i> Lesiones</a></li><li class="divider"></li>'


                            }
                            if (row.CertificadoQuimicoId != "0") {
                                action2 += '<li><a class="btn-sm printcertificadoquimico" href="javascript:void(0);" id="printcertificadoquimico" data-id="' + row.CertificadoQuimicoId + '" data-examen-id="' + row.ExamenMedicoId + '" title = "Químico" > <i class="fa fa-print"></i> Químico</a></li><li class="divider"></li>'
                            }

                            action3 = '</ul></div>';

                            if (row.Evaluacion == null) {
                                var sinExamen = '<li><span class="label label-warning" style="display:block; margin:0px 25px 5px 25px;">Sin examen médico</span></li>';
                                return action2 + sinExamen + action1;
                            }
                            else {
                                //return verReporte = '<a style="padding-left: 8.5px;" class="btn btn-primary btn-circle verExamen" data-id = "' + row.Id + '" data-examen-id = "' + row.ExamenMedicoId + '"  title="Ver Examen"><i class="glyphicon glyphicon-eye-open"></i></a>';
                                return action2 + action3;
                                // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';
                            }
                        }
                    }
                ]
            });

            $("body").on("click", "#printcertificadofisio", function () {
                var detenidoId = 1;
                var examenId = $(this).attr("data-examen-id");

                pdfExamenMedico(detenidoId, examenId);
            });

            $("body").on("click", "#printcertificadolesion", function () {
                var detenidoId = 2;
                var examenId = $(this).attr("data-examen-id");
                pdfExamenMedico(detenidoId, examenId);
            });

            $("body").on("click", "#printcertificadoquimico", function () {
                var detenidoId = 3;
                var examenId = $(this).attr("data-examen-id");
                pdfExamenMedico(detenidoId, examenId);
            });

            function pdfExamenMedico(detenidoId,examenId) {
                $.ajax({
                    type: "POST",
                    url: "reporte_examen_medico.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ detenidoId: detenidoId, examenId: examenId }),
                    cache: false,
                    success: function (data) {
                       var resultado = data.d;
                        if (resultado.exitoso) {
                            var ruta = resolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }




            $("body").on("click", "#reportePDF", function () {
                
                var datos = {
                      "id": $(this).attr("data-id"),
                      "trackingId": $(this).attr("data-trackingId")
                  };
                pdf(JSON.stringify(datos));
            });


              function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            function pdf(datos) {
                $.ajax({
                    type: "POST",
                    url: "reporte_examen_medico.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });

            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
           
        });
    </script>
</asp:Content>
