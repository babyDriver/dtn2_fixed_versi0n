﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="autorizacionSalida.aspx.cs" Inherits="Web.Application.Report.autorizacionSalida" %>

<asp:Content  ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>    
        <li>Juez calificador</li>
    <li>Autorizacion de salida por pago de multa</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">   
    <style>
        
        

        td.strikeout {
            text-decoration: line-through;
        }
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }
        input[type="number"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }               
    </style>
    <div class="scroll">
       <div class="row">

        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 style=" width:500px;" class="page-title txt-color-blueDark">
                <i class="fa fa-check fa-fw "></i>
             Autorizaciones de salida por pago de multa
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <section id="widget-grid-multa" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-check fa-fw "></i></span>
                        <h2>Autorizaciones de salida por pago de multa </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic_recibos" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>                                                        
                                                            <th data-class="expand">#</th>
                                                            <th>Folio</th>
                                                            <th>Fecha</th>
                                                            <th>Nombre</th>
                                                            <th>Apellido paterno</th>
                                                            <th data-hide="phone,tablet">Apellido materno</th>
                                                            <th data-hide="phone,tablet">No. remisión</th>
                                                            <th data-hide="phone,tablet">Monto</th>
                                                            <th data-hide="phone,tablet">Acciones</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                   
                                                </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>    

    <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="VYXMBM" runat="server" value=""/>
     <input type="hidden" id="RAWMOV" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>
     </div>
      <div id="photo-arrested" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del interno</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src=" <%= ConfigurationManager.AppSettings["relativepath"]  %> #" alt="fotografía del detenido" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">         
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {            
            

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);
                $("#photo-arrested").modal("show");
            });

             $('#dt_basic_recibos').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                 autoWidth: true,
                
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_recibos) {
                        responsiveHelper_dt_basic_recibos = new ResponsiveDatatablesHelper($('#dt_basic_recibos'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_recibos.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_recibos.respond();
                    $('#dt_basic_recibos').waitMe('hide');
                },

                //"createdRow": function (row, data, index) {
                //    if (!data["Habilitado"]) {
                //        $('td', row).eq(1).addClass('strikeout');
                //        $('td', row).eq(2).addClass('strikeout');
                //        $('td', row).eq(3).addClass('strikeout');

                //    }
                //},
                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/autorizacionSalida.aspx/getRecibos",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id"
                    },
                    {
                        name: "ReciboId",
                        data: "ReciboId"
                    },
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },                    
                    {
                        name: "TotalAPagar",
                        data: "TotalAPagar"
                    },                    
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },                    
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "imprimir";
                            var registro = '<a class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" data-reciboid="' + row.ReciboTracking + '" title = "Editar" ><i class="glyphicon glyphicon-file"></i> Generar PDF</a>';

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                registro = '<a class="btn btn-success btn-block ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" data-reciboid="' + row.ReciboTracking + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;'
                                block = "";
                            }                            

                            return registro;
                        }
                    }
                ]
            });

            $("body").on("click", ".imprimir", function () {
                var tracking = $(this).attr("data-reciboid");
                generarReciboMulta(tracking)
            });
            

            function generarReciboMulta(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/autorizacionSalida.aspx/imprimirRecibo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            
                            var ruta = ResolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }                            
                            $('#main').waitMe('hide');
                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong> ¡Bien hecho! </strong>" +
                                "El recibo se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho!", "El recibo se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! No fue posible guardar el pago de la multa", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }


            //$("body").on("click", ".imprimir", function () {
            //    var tracking = $(this).attr("data-reciboid");
            //    generarReciboMulta(tracking)
            //});
            
             //Variables tabla de recibos
            var responsiveHelper_dt_basic_recibos = undefined;
            var responsiveHelper_datatable_fixed_column_recibos = undefined;
            var responsiveHelper_datatable_col_reorder_recibos = undefined;
            var responsiveHelper_datatable_tabletools_recibos = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = true;
            
            
            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
        });
    </script>
</asp:Content>



