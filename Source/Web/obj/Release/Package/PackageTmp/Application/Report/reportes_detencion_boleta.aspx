﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="reportes_detencion_boleta.aspx.cs" Inherits="Web.Application.Report.reportes_detencion_boleta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reporte</li>
    <li>Barandilla</li>
    <li>Informe de detención y boleta</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-file-text-o"></i>
                Informe detención y boleta
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-users-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-file-text-o"></i></span>
                        <h2>Informe detención y boleta</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div class="row">
                                    <section class="col col-md-12" id="BotonBarandilla">
                                        <a class="btn btn-md btn-default" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Registry/entrylist.aspx" style="padding:5px">
                                            <i class="fa fa-mail-reply"></i> Regresar a barandilla</a>
                                    </section>
                                </div>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">#</th>
                                        <th>No. remisión</th>
                                        <th>Nombre</th>
                                        <th data-hide="phone,tablet">Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <th data-hide="phone,tablet">Sexo</th>
                                        <th data-hide="phone,tablet">Estado</th>
                                        <th data-hide="phone,tablet">Municipio</th>
                                        <th data-hide="phone,tablet">Sector</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
</div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="VYXMBM" runat="server" value=""/>
     <input type="hidden" id="RAWMOV" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();

            var responsiveHelper_dt_basic = undefined;
            ValidarBoton();

            function ValidarBoton() {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/autorizacionSalida.aspx/ValidarBoton",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            if (resultado.Boton) {

                                $("#BotonBarandilla").show();
                            }
                            else {
                                $("#BotonBarandilla").hide();
                            }
                        }
                        else {
                            ShowError("¡Error! No fue posible validar si mostrar el boton", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }
            var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    
                },
                //ajax: {
                //    type: "POST",
                //    url: "reportes_detencion_boleta.aspx/getinterno",
                //    contentType: "application/json; charset=utf-8",
                //    data: function (parametrosServerSide) {
                //        $('#dt_basic').waitMe({
                //            effect: 'bounce',
                //            text: 'Cargando...',
                //            bg: 'rgba(255,255,255,0.7)',
                //            color: '#000',
                //            sizeW: '',
                //            sizeH: '',
                //            source: ''
                //        });
                //        parametrosServerSide.emptytable = false;
                //        return JSON.stringify(parametrosServerSide);
                //    }
                //},
                ajax: {
                    url: "reportes_detencion_boleta.aspx/getinterno",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: function (d) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        d.emptytable = false;
                        d.pages = $('#dt_basic').DataTable().page.info().page || "";
                        return JSON.stringify(d);
                    },
                    dataSrc: "data",
                    dataFilter: function (data) {
                        var json = jQuery.parseJSON(data);
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        return JSON.stringify(json);
                    }
                },
                columns: [
                    null,
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Sexo",
                        data: "Sexo"
                    },
                    {
                        name: "Estado",
                        data: "Estado"
                    },
                    {
                        name: "Municipio",
                        data: "Municipio"
                    },
                    {
                        name: "Colonia",
                        data: "Colonia"
                    },
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                ],
                columnDefs: [
                    {
                        targets: 0,
                        data: "Id",
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 9,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var printPDF = "edit";
                            var imprimirPDF = '<a class="btn btn-primary" id="imprimir" data-id="' + row.Id + '" title="Informe PDF"><i class="glyphicon glyphicon-file"></i> Informe PDF</a>&nbsp;';
                            var imprimirPDFBoleta = '<a class="btn bg-color-green txt-color-white" id="imprimirBoleta" data-id="' + row.Id + '" title="Boleta PDF"><i class="glyphicon glyphicon-file"></i> Boleta PDF</a>';
                            var imprimirExcelBoleta = '<a class="btn bg-color-green txt-color-white" id="imprimirBoletaExcel" data-id="' + row.Id + '" title="Boleta Excel"><i class="glyphicon glyphicon-file"></i> Boleta Excel</a>';

                            return imprimirPDF + imprimirPDFBoleta;
                        }
                    }
                ]              
            });

            dtable = $("#dt_basic").dataTable().api();

            $("#dt_basic_filter input[type='search']")
                .unbind()
                .bind("input", function (e) {

                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
                });

            $("#dt_basic_filter input[type='search']").keypress(function (e) {
                if (e.charCode === 13) {
                    dtable.search($("#dt_basic_filter input[type='search']").val()).draw();
                    e.preventDefault();
                }
            });

            $("body").on("click", "#imprimir", function () {
                var id = $(this).attr("data-id");
                datos = id;

                pdf(id);
            });

            $("body").on("click", "#imprimirBoleta", function () {
                var id = $(this).attr("data-id");
                datos = id;

                pdfBoleta(id);
            });

            $("body").on("click", "#imprimirBoletaExcel", function () {
                var id = $(this).attr("data-id");
                datos = id;

                excelBoleta(id);
            });

            function pdf(InternoId) {
                datos = [id = InternoId];

                $.ajax({

                    type: "POST",
                    url: "reportes_detencion_boleta.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            open(resultado.ubicacionarchivo.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el informe de detenidos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });

            }

            function pdfBoleta(InternoId) {
                datos = [id = InternoId];

                $.ajax({

                    type: "POST",
                    url: "reportes_detencion_boleta.aspx/pdfBoleta",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            open(resultado.ubicacionarchivo.replace("~", ""));

                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", resultado.mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el informe de detenidos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function excelBoleta(InternoId) {
                datos = [id = InternoId];

                $.ajax({

                    type: "POST",
                    url: "reportes_detencion_boleta.aspx/exportaExcelBoleta",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", resultado.mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    }
                    //error: function () {
                    //    ShowError("¡Error!", "No fue posible imprimr el reporte de boleta de control. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    //}
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>