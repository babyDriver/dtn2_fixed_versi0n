﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="movimientos.aspx.cs" Inherits="Web.Application.Sentence.movimientos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
  
  <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Control_Celdas/celdas.aspx?name=escolaridad">Control de celdas</a></li>

  <li>  Movimientos</li>
       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
         td.strikeout {
            text-decoration: line-through;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
        .wrapping {
            /* These are technically the same, but use both */
            overflow-wrap: break-word;
            word-wrap: break-word;

            -ms-word-break: break-all;
            /* This is the dangerous one in WebKit, as it breaks things wherever */
            word-break: break-all;
            /* Instead use this non-standard one: */
            word-break: break-word;
    
            /* Adds a hyphen where the word breaks, if supported (No Blink) */
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
        #ScrollableContent {
            height: calc(100vh - 510px);
        }
    </style>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
            <div>
                <header>
                    <span class="widget-icon"><i class="fa fa-search"></i></span>
                    <h2>Buscar clientes </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <div id="smart-form-register" class="smart-form">
                            <header>
                                Criterios de búsqueda
                            </header>
                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="select">
                                            <select name="peril" id="perfil" runat="server">
                                                <option value="0">[Perfil]</option>
                                            </select>
                                            <i></i>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="nombre" id="nombre" runat="server" placeholder="Nombre" maxlength="256" class="alptext"/>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                            <footer>
                                <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-list-alt"></i>
                Movimientos
            </h1>
        </div>
    </div>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>

    <section id="widget-grid" class="">
		<div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="row padding-bottom-10" id="addentry" style="padding-left: 13px;">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <a href="javascript:void(0);" class="btn btn-md btn-default" id="idmovimiento"><i class="fa fa-plus"></i> Agregar movimiento </a>&nbsp;
                <a href="celdas.aspx" class="btn btn-md btn-default regresar" id="regresar"><i class="fa fa-mail-reply"></i> Regresar </a>
            </div>
        </div>
        <div class="scroll" id="ScrollableContent">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Lista de movimientos del detenido</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Fecha y hora</th>
                                        <th>Movimiento</th>
                                        <th>Observación</th>
                                        <th>Responsable</th>
                                        <th>Institución</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <div id="BuscaCelda-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Búsqueda de celdas</h4>
                </div>
                <div class="modal-body">
                    <div id="registro-form" class="smart-form">
                        <div class="modal-body">
                            <div>
                                <div class="jarviswidget-editbox"></div>
                                <div class="widget-body">
                                    <table id="dt_basicCeldas2" class="table table-striped table-bordered table-hover" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th data-class="expand">Nombre</th>
                                                <th data-hide="phone,tablet">Capacidad</th>
                                                <th data-hide="phone,tablet">Nivel de peligrosidad</th>
                                                <%--<th data-hide="phone,tablet">Estatus</th>--%>
                                                <th data-hide="phone,tablet">Acciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveregistro" id="saveregistro"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela2"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="historymovimiento-modal" class="modal fade" data-backdrop="static" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial movimientos</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dt_basichistoryMov" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th data-hiden="tablet,fablet,phone">Observación</th>
									    <th>Responsable</th>
                                        <th>Movimiento</th>
                                        <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="RegistroCelda-modal" class="modal fade" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal-title"><i class='fa fa-pencil'></i> Registro de movimientos</h4>
                </div>
                <div class="modal-body">
                    <div id="registromovimiento-form" class="smart-form">
                        <div class="modal-body">
                            <div class="row">
                                <section class="col col-10">
                                    <label style="color:dodgerblue" class="label">Descripción <a style="color: red">*</a></label>
                                    <select name="descripcioncelda" id="descripcioncelda" style="width: 100%" class="select2"></select>
                                    <%--<label class="select">
                                            <select name="descripcioncelda" id="descripcioncelda" >
                                    </select>--%>
                                    <i></i>
                                </section>
                                <section  class="col col-5">
                                    <label class="label" style="color:dodgerblue" >Fecha y hora <a style="color: red">*</a></label>
                                    <label class="input">
                                        <label class='input-group date' id='FehaHoradatetimepicker'>
                                            <input type="text" name="fechahora" id="fechahora" placeholder="Fecha y hora de ingreso" data-requerido="true"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa la fecha y hora.</b>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <div class="jarviswidget-editbox"></div>
                                <section class="col col-3">
                                    <label class="label" style="color:dodgerblue" >Celda <a style="color: red">*</a></label>
                                    <label class="input">
                                        <label class='input' id='celda'>
                                            <input type="text" name="txtcelda" id="txtcelda" class='input alptext' placeholder="Celda" disabled="disabled" data-requerido="true" />
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <section class="col col-2">
                                    <a href="javascript:void(0);" style="padding: 6px 12px; margin-top: 25px;" class="btn btn-default buscaCelda" id="buscaCelda" title="Buscar celda"><i class="fa fa-search"></i>&nbsp; Buscar&nbsp;  </a>
                                </section>
                                <div class="widget-body" hidden="hidden" id="divCeldas" >
                                    <table id="dt_basicCeldas" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th data-class="expand">Nombre</th>
                                                <th data-hide="phone,tablet">Capacidad</th>
                                                <th data-hide="phone,tablet">Nivel de peligrosidad</th>
                                                <%--<th data-hide="phone,tablet">Estatus</th>--%>
                                                <th data-hide="phone,tablet">Acciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <section style="padding-top:10px; width:463px;" class="col col-10">
                                    <label class="label" style="color:dodgerblue" >Observaciones <a style="color: red">*</a></label>
                                    <label class="textarea">
                                        <textarea style="width:445px;" id="observaciones" name="observaciones" cols="20" rows="5" maxlength="2000" class="alptext"></textarea>
                                        <b class="tooltip tooltip-bottom-right">Ingresa las observaciones.</b>
                                    </label>                                                        
                                </section>
                                <section style="" class="col col-10">
                                    <label class="label" style="color:dodgerblue" >Responsable <a style="color: red">*</a></label>
                                    <label class="input">
                                        <label class='input' id='responsable'>
                                            <input  type="text" name="idresponsable" maxlength="150" id="idresponsable" class='input alptext' placeholder="Responsable" data-requerido="true"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el responsable.</b>
                                        </label>
                                    </label>
                                    <i></i>
                                </section>
                                <section class="col col-6">
                                    <label class="label" style="color:dodgerblue" >Institución</label>
                                    <label class='input' id='indst'>
                                        <input <%--size="250"--%> type="text" name="idresponsable" id="Institucion" class='input alptext' placeholder="Institucion" data-requerido="true" disabled="disabled"/>
                                    </label>
                                    <i></i>
                                </section>
                            </div>
                        </div>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveregistro" id="guardatraslado"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog" data-keyboard="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control alptext" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control alptext" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockuser-modal" class="modal fade" data-backdrop="static" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de <strong>&nbsp<span id="usernameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer style="padding: 0px;">
                            <div class="row" style="display: inline-block; float: right; margin:0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="CentroId"  value="" />
    <input type="hidden" id="CeldaTrackingId"  value="" />
    <input type="hidden" id="Final" />
    <input type="hidden" id="hide" runat="server" />
    <input type="hidden" id="hideid" runat="server" />
    <input type="hidden" id="HistorialM" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="validarfecha" />
    <input type="hidden" id="FechaRegistro" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                if ($("#RegistroCelda-modal").is(":visible")) {
                    document.getElementById("guardatraslado").click();
                }
            }
        });

        $(document).ready(function () {
            
            $("#RegistroCelda-modal").on("shown.bs.modal", function () {
                $("#descripcioncelda").next().children().children().focus();
            });

            pageSetUp();
            var rutaDefaultServer = "";
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_dt_basicCeldas = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            CargarListadoMovimientos(0);
            CargarListadoCentrosReclusion(0);
            var param = RequestQueryString("tracking");

            if (param != undefined) {

                CargarDatos(param);
                $("#ctl00_contenido_trackingInterno").val(param);
            }

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            getRutaDefaultServer();
            function getRutaDefaultServer() {
                $.ajax({
                    type: "POST",
                    url: "movimientos.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;
                        }
                    }
                });
            }

            function validatefecha(info) {
                var valido = true;
                $.ajax({
                    type: "POST",
                    url: "movimientos.aspx/ValidateFecha",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    data: JSON.stringify({
                        info: info,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            if (resultado.Validar == false) {
                                valido = false;
                            }

                            var fecha = resultado.fechaRegistro;
                            $("#FechaRegistro").val(fecha);
                            $("#validarfecha").val(valido);
                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
                return valido;
            }

            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "movimientos.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            $('#ctl00_contenido_hideid').val(resultado.obj.Id);
                            $('#ctl00_contenido_hide').val(resultado.obj.TrackingId);
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);

                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            $('#avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);

                            $('#edadInterno').text(resultado.obj.Edad);

                            $('#sexoInterno').text(resultado.obj.Sexo);

                            $('#municipioInterno').text(resultado.obj.municipioNombre);
                            $('#domicilioInterno').text(resultado.obj.domiclio);
                            $('#coloniaInterno').text(resultado.obj.coloniaNombre);
                            CargaTabla();
                            // var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            //$('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            //$('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }

                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".datos", function () {
                var id = $(this).attr("data-id");
                CargarDatos(id);
            });

            function CargarCeldaByDetalleDetencion(trackingid) {
                //jc
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "movimientos.aspx/GetCeldaByDetalleDetencion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var celda = resultado.obj.nombrecelda;
                            var TrackingCelda = resultado.obj.TrackingCeldas;
                            var final = resultado.obj.Finales;

                            $('#CeldaTrackingId').val(TrackingCelda);
                            $('#txtcelda').val(celda);
                            if (final) {
                                $('#CeldaTrackingId').val("");
                                $('#txtcelda').val("");
                            }
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la celda del movimiento. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }

                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarDatosPersonales(trackingid) {
                //jc
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "movimientos.aspx/getdatosPersonales",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno2').val(resultado.obj.Nombre);
                            $('#edad').val(resultado.obj.Edad);
                            $('#situacion').val(resultado.obj.Situacion);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            // alert(imagenAvatar);
                            $('#ctl00_contenido_avatar2').attr("src", imagenAvatar)
                            //$('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);

                            $('#horaRegistro').val(resultado.obj.Registro);
                            $('#salida').val(resultado.obj.Salida);
                            $('#domicilio').val(resultado.obj.Domicilio);
                            $("#datospersonales-modal").modal("show");
                        } else {
                            ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }

                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);

                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            $('#imgInfo').attr("src", imagenAvatar);
                            $("#datospersonales-modal").modal("show");
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }

                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            window.table = $('#dt_basicCeldas').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basicCeldas) {
                        responsiveHelper_dt_basicCeldas = new ResponsiveDatatablesHelper($('#dt_basicCeldas'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basicCeldas.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basicCeldas.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "movimientos.aspx/getcelda",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;        //window.emptytable;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Capacidad",
                        data: "Capacidad"
                    },
                    {
                        name: "M.Nombre",
                        data: "Peligrosidad"
                    },
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        width: 100,
                        render: function (data, type, row, meta) {
                            return "<div class='wrapping'>" + row.Nombre + "</div>";
                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "Editarmovimiento";
                            var editar = "";
                            var habilitar = "";
                            var seleccion = "seleccionacelda";

                            // deshabilitar estas dos columnas para activar permisos y desactivar las 2 que siguen
                            //if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' "href="javascript:void(0);" data-value = "' + row.Nombre + '" data-capacidad="' + row.Capacidad + '" data-tracking="' + row.TrackingId + '" data-Nivel_Peligrosidad="' + row.Nivel_Peligrosidad + '" data-habilitado="' + row.Habilitado + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            //if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Nombre + '" title="' + txtestatus + '" data-tracking="' + row.TrackingId + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';

                            editar = '&nbsp;<a href="javascript:void(0);" style="padding: 6px 12px;" class="btn btn-primary  ' + seleccion + '" data-tracking="' + row.TrackingId + '" data-value="' + row.Nombre + '" data-id="' + row.Id + '" title="Seleccionar">Seleccionar</a>&nbsp;';

                            return editar
                                //habilitar +
                                //  '<a class="btn btn-default btn-circle historial" href="javascript:void(0);" data-value="' + row.Nombre + '" data-tracking="' + row.TrackingId + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>'
                                ;
                        }
                    }
                ]
            });

            function CargaTabla() {
                window.table = $('#dt_basic').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                            $('td', row).eq(4).addClass('strikeout');
                        }
                    },
                    ajax: {
                        type: "POST",
                        url: "movimientos.aspx/getMovimientos",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            var param2 = RequestQueryString("tracking");
                            if (param2 != undefined) {
                                parametrosServerSide.TrackingProcesoId = param2;
                            }

                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            name: "FechaHora",
                            data: "FechaHora"
                        },
                        {
                            name: "Movimiento",
                            data: "Movimiento"
                        },
                        {
                            name: "Observaciones",
                            data: "Observaciones",
                        },
                        {
                            name: "Responsable",
                            data: "Responsable"
                        },
                        {
                            name: "Institucion",
                            data: "Institucion"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 5,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "Editarmovimiento";
                                var editar = "";
                                var habilitar = "";
                                var seleccion = "seleccionacelda";
                                if (row.Habilitado == 1) {
                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                    edit = "edit";
                                }
                                else {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                    edit = "";
                                }

                                // deshabilitar estas dos columnas para activar permisos y desactivar las 2 que siguen
                                //if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' "href="javascript:void(0);" data-value = "' + row.Nombre + '" data-capacidad="' + row.Capacidad + '" data-tracking="' + row.TrackingId + '" data-Nivel_Peligrosidad="' + row.Nivel_Peligrosidad + '" data-habilitado="' + row.Habilitado + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                //if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Nombre + '" title="' + txtestatus + '" data-tracking="' + row.TrackingId + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                                if ($("#ctl00_contenido_KAQWPK").val() == "true") {
                                    editar = '<a class="btn btn-primary btn-circle  ' + edit + ' "href="javascript:void(0);" id="Editarmovimiento" data-value = "' + row.Id + '" data-DetalledetencionId="' + row.DetalledetencionId + '" data-tracking="' + row.TrackingId + '" data-TipomovimientoId="' + row.TipomovimientoId + '" data-habilitado="' + row.Habilitado + '" data-Celda="' + row.Celda + '" data-InstitucionId="' + row.InstitucionId + '" data-Observaciones="' + row.Observaciones + '" data-Responsable="' + row.Responsable + '" data-Fechahora="' + row.FechaHora + '" data-CeldaTrackingId="' + row.CeldaTrackingId + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                }
                                else {
                                    editar = '<a class="btn btn-primary btn-circle  ' + edit + ' "href="javascript:void(0);" id="Editarmovimiento" data-value = "' + row.Id + '" data-DetalledetencionId="' + row.DetalledetencionId + '" data-tracking="' + row.TrackingId + '" data-TipomovimientoId="' + row.TipomovimientoId + '" data-habilitado="' + row.Habilitado + '" data-Celda="' + row.Celda + '" data-InstitucionId="' + row.InstitucionId + '" data-Observaciones="' + row.Observaciones + '" data-Responsable="' + row.Responsable + '" data-Fechahora="' + row.FechaHora + '" data-CeldaTrackingId="' + row.CeldaTrackingId + '"  title="Ver"><i class="glyphicon glyphicon-search"></i></a>&nbsp;';
                                }

                                if ($("#ctl00_contenido_LCADLW").val() == "true") {
                                    habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Movimiento + '" data-id="' + row.Id + '" title="' + txtestatus + '" data-tracking="' + row.TrackingId + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                                }
                                return editar + habilitar + '<a name="history" class="btn btn-default btn-circle historialMov" href="javascript:void(0);" data-value="' + row.Observaciones + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                            }
                        }
                    ]
                });
            }

            var responsiveHelper_dt_basichistory = undefined;
            var breakpointHistoryMovDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };

            window.emtytablehistoryMov = true;
            window.tablestoryMov = $("#dt_basichistoryMov").dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[4, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basichistory) {
                        responsiveHelper_dt_basichistory = new ResponsiveDatatablesHelper($("#dt_basichistoryMov"), breakpointHistoryMovDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basichistory.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basichistory.respond();
                    $("#dt_basichistoryMov").waitMe("hide");
                },
                ajax: {
                    type: "POST",
                    url: "movimientos.aspx/Getmovimientoslog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosserverside) {
                        $("#dt_basichistoryMov").waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var centroid = $('#HistorialM').val() || "0";
                        var param2 = RequestQueryString("tracking") || "";
                        parametrosserverside.tracking = param2;
                        parametrosserverside.centroid = centroid;
                        parametrosserverside.todoscancelados = false;
                        parametrosserverside.emptytable = emtytablehistoryMov;
                        return JSON.stringify(parametrosserverside);
                    }
                },
                columns: [
                    {
                        name: "Observaciones",
                        data: "Observaciones",
                        ordertable: false
                    },
                    {
                        name: "Responsable",
                        data: "Responsable",
                        ordertable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        ordertable: false
                    },
                    {
                        name: "Creadopor",
                        data: "Creadopor",
                        ordertable: false
                    },
                    {
                        name: "Fec_Movto",
                        data: "Fec_Movto",
                        ordertable: false
                    }
                ],
            });

            $("body").on("click", ".historialMov", function () {
                var id = $(this).attr("data-id");
                $("#HistorialM").val(id);
                //var descripcion = $(this).attr("data-value");
                //$("#descripcionhistorialmovimiento").text(descripcion);
                $("#historymovimiento-modal").modal("show");
                window.emtytablehistoryMov = false;
                window.tablestoryMov.api().ajax.reload();
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingid = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "movimientos.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        Id: trackingid,
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockuser-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                data.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", data.mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del usuario. . Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".buscaCelda", function () {
                $('#divCeldas').show("swing");

                //ShowSelected();
                window.emptytableadd = false;
                window.tableadd.api().ajax.reload();
            });

            $('#idmovimiento').on('click', function () {
                $('#FehaHoradatetimepicker').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm:ss'
                });
                limpiarCampos();
                var tkg = "";
                var param = RequestQueryString("tracking") || "";
                if (param != "") {
                    tkg = param;
                }
                CargarCeldaByDetalleDetencion(tkg);
                $("#modal-title").html("Registro de movimientos");
                $("#RegistroCelda-modal").modal("show");
                //if (param != undefined) {
                //    $('#idmovimiento').attr("href", "registro_celda.aspx?tracking=" + param);
                //}
            });

            $("body").on("click", ".blockitem", function () {
                var usernameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockuser-modal").modal("show");
            });

            //$("body").on("click", ".selecciSonacelda", function () {
            //    var nombreCelda = $(this).attr("data-value");
            //    var CeldaTrackingId = $(this).attr("data-tracking");
            //    $("#txtcelda").val(nombreCelda);
            //    $("#CeldaTrackingId").val(CeldaTrackingId);
            //    $('#divCeldas').hide("linear")

            //});

            $("body").on("click", "#Editarmovimiento", function () {
                console.log($(this).attr("data-Fechahora"));
                var CeldaTrackingId = $(this).attr("data-CeldaTrackingId");
                var id = $(this).attr("data-value");
                var TipoMovimientoId = $(this).attr("data-TipoMovimientoId");

                var Celda = $(this).attr("data-Celda");
                var InstitucionId = $(this).attr("data-InstitucionId");
                var Observaciones = $(this).attr("data-Observaciones");
                var Responsable = $(this).attr("data-responsable");

                var Fechahora = $(this).attr("data-Fechahora");
                $('#FehaHoradatetimepicker').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm:ss'
                });
                limpiarCampos();
                CargarListadoMovimientos(TipoMovimientoId);
                $('#ctl00_contenido_hideid').val(id);
                $('#CeldaTrackingId').val(CeldaTrackingId);
                $('#txtcelda').val(Celda);
                $("#ctl00_contenido_Institucion").val(InstitucionId)
                $('#observaciones').val(Observaciones);
                $('#idresponsable').val(Responsable);
                $('#fechahora').val(Fechahora.replace('-', '/'));
                $("#modal-title").html("Edición de movimientos");
                $("#RegistroCelda-modal").modal("show");
            });

            $("body").on("click", ".seleccionacelda", function () {
                var id = $(this).attr("data-value");
                var nombreCelda = $(this).attr("data-value");
                var CeldaTrackingId = $(this).attr("data-tracking");
                $("#txtcelda").val(nombreCelda);
                $("#CeldaTrackingId").val(CeldaTrackingId);
                $('#divCeldas').hide("linear")
            });

            function ObtenerValoresMovimientoCelda() {
                var MovimientoCelda = {
                    TipoMovimientoId: $("#descripcioncelda").val(),
                    Fechahora: $('#fechahora').val(),
                    CeldaTrackingId: $('#CeldaTrackingId').val(),
                    Observaciones: $("#observaciones").val(),
                    ResponsablieId: $("#idresponsable").val(),
                    InstitucionId: $("#CentroId").val(),
                    TrackingId: "",
                    Id: $('#ctl00_contenido_hideid').val()
                };

                return MovimientoCelda;
            }

            $("body").on("click", ".detencion", function () {
                //var id = $(this).attr("data-id");
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    //MovimientoCelda.TrackingId = param;

                    CargarDatosModal(param);
                }
            });

            function CargarListadoMovimientos(id) {
                $.ajax({
                    type: "POST",
                    url: "movimientos.aspx/getListadoMovimiento_Celda_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#descripcioncelda');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Seleccione movimiento]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function limpiarCampos() {
                // $("#descripcioncelda").val("0");
                CargarListadoMovimientos("0");
                $('#descripcioncelda').removeClass('valid');
                $('#descripcioncelda').removeClass('state-success');
                $('#descripcioncelda').parent().removeClass('state-success').removeClass("state-error");
                $("#fechahora").val("");
                $('#fechahora').removeClass('valid');
                $('#fechahora').removeClass('state-success').removeClass("state-error");
                $('#fechahora').parent().removeClass('state-success').removeClass("state-error");
                $("#txtcelda").val("");
                $('#celda').removeClass('valid');
                $('#celda').removeClass('state-success').removeClass("state-error");
                $('#celda').parent().removeClass('state-success').removeClass("state-error");
                $("#observaciones").val("");
                $('#observaciones').removeClass('valid');
                $('#observaciones').removeClass('state-success').removeClass("state-error");
                $('#observaciones').parent().removeClass('state-success').removeClass("state-error");
                $("#idresponsable").val("");
                $('#idresponsable').removeClass('valid');
                $('#idresponsable').removeClass('state-success').removeClass("state-error");
                $('#idresponsable').parent().removeClass('state-success').removeClass("state-error");
                $("#ctl00_contenido_Institucion").val("0");
                $('#ctl00_contenido_Institucion').removeClass('valid');
                $('#ctl00_contenido_Institucion').removeClass('state-success').removeClass("state-error");
                $('#ctl00_contenido_Institucion').parent().removeClass('state-success').removeClass("state-error");
                $('#ctl00_contenido_hideid').val("");
            }

            function validarCeldas() {
                var esvalido = true;

                if ($("#descripcioncelda").val() == "0") {
                    ShowError("Descripción", "El campo descripción es obligatorio");
                    $('#descripcioncelda').parent().removeClass('state-success').addClass("state-error");
                    $('#descripcioncelda').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#descripcioncelda').parent().removeClass("state-error").addClass('state-success');
                    $('#descripcioncelda').addClass('valid');
                }

                if ($("#fechahora").val() == "" || $('#fechahora').val() == null) {
                    ShowError("Fecha y hora", "El campo fecha y hora es obligatorio");
                    $('#fechahora').parent().removeClass('state-success').addClass("state-error");
                    $('#fechahora').removeClass('valid');
                    esvalido = false;
                }
                else {
                    var tracking = "";
                    var param = RequestQueryString("tracking");
                    if (param != undefined) {
                        tracking = param;
                    }
                    info = [
                        tracking = tracking,
                        fecha = $("#fechahora").val()
                    ];
                    var valido = validatefecha(info);
                    valido = $("#validarfecha").val();
                    if (valido == "false") {
                        var f = $("#FechaRegistro").val();
                        ShowError("Fecha y hora", "La fecha y hora no puede ser menor a la fecha " + f + ".");

                        $('#fechahora').parent().removeClass('state-success').addClass("state-error");
                        $('#fechahora').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#fechahora').parent().removeClass("state-error").addClass('state-success');
                        $('#fechahora').addClass('valid');
                    }
                }

                if ($("#txtcelda").val() == "" || $('#txtcelda').val() == null) {
                    ShowError("Celda", "El campo celda es obligatorio");
                    $('#celda').parent().removeClass('state-success').addClass("state-error");
                    $('#celda').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#celda').parent().removeClass("state-error").addClass('state-success');
                    $('#celda').addClass('valid');
                }

                if ($("#observaciones").val() == "" || $('#observaciones').val() == null) {
                    ShowError("Observaciones", "El campo observaciones es obligatorio");
                    $('#observaciones').parent().removeClass('state-success').addClass("state-error");
                    $('#observaciones').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observaciones').parent().removeClass("state-error").addClass('state-success');
                    $('#observaciones').addClass('valid');
                }

                if ($("#idresponsable").val() == "") {
                    ShowError("Responsable", "El campo responsable es obligatorio");
                    $('#idresponsable').parent().removeClass('state-success').addClass("state-error");
                    $('#idresponsable').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#idresponsable').parent().removeClass("state-error").addClass('state-success');
                    $('#idresponsable').addClass('valid');
                }

                if ($("#ctl00_contenido_Institucion").val() == "0") {
                    ShowError("Institución", "El campo institución es obligatorio");
                    $('#ctl00_contenido_Institucion').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_Institucion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_Institucion').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_Institucion').addClass('valid');
                }

                return esvalido;
            }

            function guardarMovimientoCelda() {
                startLoading();
                var MovimientoCelda = ObtenerValoresMovimientoCelda();

                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    MovimientoCelda.TrackingId = param;
                }

                $.ajax({
                    type: "POST",
                    url: "movimientos.aspx/MovimientoCeldaSave",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'MovimientoCelda': MovimientoCelda }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", resultado.mensaje);
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            $('#main').waitMe('hide');
                            $("#RegistroCelda-modal").modal("hide");
                            limpiarCampos();
                        }
                        else {
                            $('#main').waitMe('hide');
                            if (resultado.mensaje == "La celda no tiene lugares disponibles." || resultado.mensaje == "Seleccione un tipo de movimiento diferente." || resultado.fallo == "fecha") {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención! </strong>" +
                                    " " + resultado.mensaje + "</div>");
                                setTimeout(hideMessage, hideTime);
                                ShowAlert("Atencion!", resultado.mensaje);
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                    "Algo salió mal. " + resultado.mensaje + "</div>");
                                setTimeout(hideMessage, hideTime);
                                ShowError("¡Error! Algo salió mal", resultado.mensaje);
                            }
                        }
                    }
                });
            }

            function CargarListadoCentrosReclusion(id) {
                $.ajax({
                    type: "POST",
                    url: "movimientos.aspx/getListadoMovimiento_CentroReclusion_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        $.each(response.d, function (index, item) {
                            $("#Institucion").val(item.Desc);
                            $("#CentroId").val(item.Id);
                        });
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de Instituciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            init();

            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#idmovimiento").show();
                }
                else {
                    $("#idmovimiento").hide();

                    $("#guardatraslado").hide();

                    $("#id_input").prop("disabled", true);

                    $("#ctl00_contenido_dropdown").prop("disabled", true);
                }
            }

            $("body").on("click", "#guardatraslado", function () {
                if (validarCeldas()) {
                    guardarMovimientoCelda();
                }
            });

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 510px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');

                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 290px)");
            });

        });
    </script>
</asp:Content>
