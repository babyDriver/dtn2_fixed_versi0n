﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="eventsform.aspx.cs" Inherits="Web.Application.Registry.eventsform" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Registry/events.aspx">Llamadas y eventos</a></li>
    <li>Eventos</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' />
    <style type="text/css">
        #mapid {
            height: 30vw;
            width: 100%;
        }

        .coordinates {
            background: rgba(0,0,0,0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding: 5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }

        .wrapping {
            /* These are technically the same, but use both */
            overflow-wrap: break-word;
            word-wrap: break-word;
            -ms-word-break: break-all;
            /* This is the dangerous one in WebKit, as it breaks things wherever */
            word-break: break-all;
            /* Instead use this non-standard one: */
            word-break: break-word;
            /* Adds a hyphen where the word breaks, if supported (No Blink) */
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
        td.strikeout {
            text-decoration: line-through;
        }                   
        textarea {            
            margin-top:10px;
            width:100%;
            border: 1px solid #BDBDBD;
        }
        textarea:focus {
            box-shadow: 0 0 5px #5D98CC;            
            border: 1px solid #5D98CC;            
        }
        textarea:hover {
            box-shadow: 0 0 0px #5D98CC;            
            border: 1px solid #5D98CC;            
        }        
     
        input[type=checkbox]{
            -ms-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
            padding: 10px;
            cursor: pointer;
        }
        .checkboxtext{
            font-size: 100%;
            display: inline;       
            color: #004987;
        }
        .checkboxtext:hover{
            cursor:pointer;
        }
        .errorInputTabla{
            background-color:#fff0f0;
            border-color:#A90329;
        }
    </style>
    <br />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:HiddenField ID="tracking" runat="server" />
    <div id="Lmapa"></div>
    <div class="scroll">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-entrya-1" data-widget-editbutton="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="glyphicon  glyphicon-edit"></i></span>
                            <h2>Evento</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div id="smart-form-register-entry" class="smart-form">
                                    <fieldset style="padding-top: 0px">
                                       
                                        <div class="row">
                                            <a style="float: right; margin-left: 3px; margin-right: 30px; display: none;" id="add" class="btn btn-primary btn-lg" href="javascript:void(0);"><i class="fa fa-plus"></i>Registro en barandilla</a>
                                        </div>
                                        <div class="row">
                                             <!--leyend-->
                                            <header class="col col-1" style="border-bottom: none;" id="alerta1">Alerta web</header>
                                            <section class="col col-4">
                                                <label class="label">Institución</label>
                                                <select name="Unidad" id="alertaWebInstitucion" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Unidad</label>
                                                <select name="Unidad" id="alertaWebUnidad" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <header id="localizacionHeader"></header>
                                        </div>
                                        <div class="row">
                                            <section class="col col-3">
                                                <label class="label">Llamada</label>
                                                <select name="Llamada" id="llamada" class="select2" style="width: 100%;"></select>
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">Fecha y hora <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <div class='input-group date' id='autorizaciondatetimepicker'>
                                                        <input type="text" name="Fecha y hora" id="fecha" class='form-control' placeholder="Fecha y hora de ingreso" data-requerido="true" />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Folio <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Folio" id="folio" placeholder="Folio" maxlength="50" class="alphanumeric alptext" data-requerido="true" disabled="disabled" />
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Motivo <a style="color: red">*</a></label>
                                                <select name="Motivo" id="motivoevento" class="select2" style="width: 100%;" data-requerido="true">
                                               
                                                    </select>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">Descripción<a style="color: red">*</a></label>
                                                <label class="input">
                                                    <textarea id="descripcion" name="Descripción" placeholder="Descripción" maxlength="1000" class="form-control alptext" rows="6" data-requerido="true"></textarea>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la descripción.</b>
                                                </label>
                                            </section>
                                            <%--<div class="row">
                                                    <header>Victimas</header>
                                                </div>
                                                <br />--%>
                                                <div class="row">
                                                    <section class="col col-6">
                                                        <%-- display: none; --%>
                                                        <div class="row col" style="height: 50px;">
                                                            <a style="float: left; font-size: smaller" class="btn btn-default btn-lg" title="Agregar victima" id="linkVictima"><i class="fa fa-plus"></i> Agregar victima</a>                                                    
                                                        </div> 
                                                        <table id="dt_basic_tabla_victimas" class="table table-striped table-bordered table-hover" width="97%">
                                                        <thead>
                                                            <tr>
                                                                <th data-class="expand">Nombre</th>
                                                                <th data-hide="expand">Tipo de victima</th>
                                                                <%--<th data-hide="expand">Reparacion</th>--%>
                                                                <th data-hide="expand">Daño o perjuicio</th>
                                                            </tr>
                                                        </thead>
                                                        </table>
                                                    </section>
                                            </div>

                                            <section class="col col-4">
                                                <label class="label">Lugar detención <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Lugar detención" id="lugar" placeholder="Lugar detención" maxlength="1000" class="alphanumeric alptext" data-requerido="true" />
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el lugar de detención.</b>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Municipio <a style="color: red">*</a></label>
                                                <select name="Municipio" id="municipio" class="select2" style="width:100%;"></select>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Colonia <a style="color: red">*</a></label>
                                                <select name="Colonia" id="colonia" data-requerido="true" class="select2" style="width:100%;"></select>
                                            </section>
                                            </div>
                                        <div class="row">
                                            <section class="col col-2">
                                                <label class="label">Código postal <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Código postal" id="codigoPostal" placeholder="Código Postal" maxlength="6" class="numeric" data-requerido="true" disabled="disabled" />
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Número detenidos <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Número detenidos" id="numeroDetenidos" placeholder="Número detenidos" maxlength="6" class="number" data-requerido="true" />
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el número de detenidos.</b>
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Latitud</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Latitud" id="latitud" placeholder="Latitud" maxlength="25" class="number" data-requerido="false" />
                                                    <b class="tooltip tooltip-bottom-right">Ingrese la latitud.</b>
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Longitud</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Longitud" id="longitud" placeholder="Longitud" maxlength="25" class="number" data-requerido="false" />
                                                    <b class="tooltip tooltip-bottom-right">Ingrese la longitud.</b>
                                                </label>
                                            </section>

                                        </div>
                                        <footer>
                                            <a href="events.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                            <a class="btn btn-default save" id="save_" title="Guardar registro actual"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                        </footer>
                                        <div class="row">
                                            <div class="col col-6">
                                                <div class="row">
                                                    <header>Detenidos</header>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col col-12">
                                                        <a style="float: left; display: none; font-size: smaller" class="btn btn-default  btn-lg" title="Agregar detenidos" id="linkDetenidos"><i class="fa fa-plus"></i> Agregar detenido</a>
                                                    </div>
                                                </div>
                                                <br />
                                                <table id="dt_basic_tabla_detenidos" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th data-class="expand">Nombre</th>
                                                            <th data-hide="phone,tablet">Apellido paterno</th>
                                                            <th data-hide="phone,tablet">Apellido materno</th>
                                                            <th data-hide="phone,tablet">Sexo</th>
                                                            <th data-hide="phone,tablet">Edad</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col col-6">
                                                <div class="row">
                                                    <header>Unidades involucradas</header>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col col-12">
                                                        <a style="float: left; display: none; font-size: smaller" class="btn btn-default  btn-lg" title="Agregar unidad" id="linkUnidad"><i class="fa fa-plus"></i> Agregar unidad</a>
                                                    </div>
                                                </div>
                                                <br />
                                                <table id="dt_basic_tabla_unidades" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th data-class="expand">Unidad</th>
                                                            <th data-hide="phone,tablet">Clave-responsable</th>
                                                            <th data-hide="phone,tablet">Corporación</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <header>Ubicación del evento en el mapa</header>
                                            <br />
                                        </div>
                                        <div id="mapid"></div>
                                        <pre id='coordinates' class='coordinates'></pre>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modalVictima"  role="dialog"  data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-victima-title"></h4>
                </div>
                <div class="modal-body1">
                    <br />
                    
                    
                    <div id="register-victima-form" class="smart-form">
                        
                     
                        <fieldset>
                           
                            <div class="row" style="width:100%;margin:0 auto;">
                                    <table id="dt_nueva_victima" class="table table-striped table-bordered table-hover" width="100%" >
                                        <thead>
                                            <tr>
                                                <th style="width:8%  !important; "></th>
                                                <th style="text-align:center;" >Nombre</th>                                                
                                                <th style="text-align:center">Tipo de victima</th>                                                
                                                <th style="text-align:center">Daño o perjuicio</th>
                                                
                                                <%--<th style="text-align:center">Acciones</th>--%>
                                                
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                           
                        </fieldset>
                        <footer>
                            
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelVictima"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save" id="btnGuardarVictima"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            <a href="javascript:void(0);" class="btn btn-md btn-primary " id="addRowV"  style="margin-left:10px"><i class="fa fa-plus"></i>&nbsp;Agregar nueva fila </a>
                        </footer>
                    </div>
                      
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalUnidad" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-unidad-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="label" style="color: dodgerblue">Unidad <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>                                                
                                        <select name="unidad" id="unidad" style="width: 100%" class="select2" data-requerido-unidad="true"></select>
                                    </label>
                                </section>
                                <section>
                                    <label class="label" style="color: dodgerblue">Clave-responsable <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>                                                
                                        <select name="responsable" id="responsable" style="width: 100%" class="select2" data-requerido-unidad="true"></select>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default save" id="btnGuardarUnidad"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalDetenido"  role="dialog"  data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-detenido-title"></h4>
                </div>
                <div class="modal-body1">
                    <br />
                    
                    
                    <div id="register-detendio-form" class="smart-form">
                        
                     
                        <fieldset>
                           
                            <div class="row" style="width:100%;margin:0 auto;">
                                    <table id="dt_nuevo_detenido" class="table table-striped table-bordered table-hover" width="100%" >
                                        <thead>
                                            <tr>
                                                <th style="width:8%  !important; "></th>
                                                <th style="text-align:center;" >Nombre</th>                                                
                                                <th style="text-align:center">Apellido paterno</th>                                                
                                                <th style="text-align:center">Apellido materno</th>
                                                
                                                <th style="text-align:center">Sexo</th>
                                                <th style="text-align:center">Edad</th>
                                                <th style="text-align:center">Motivo</th>
                                                <%--<th style="text-align:center">Acciones</th>--%>

                                                
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                           
                        </fieldset>
                        <footer>
                            
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelDetenido"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save" id="btnGuardarDetenido"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            <a href="javascript:void(0);" class="btn btn-md btn-primary " id="addRow"  style="margin-left:10px"><i class="fa fa-plus"></i>&nbsp;Agregar nueva fila </a>
                        </footer>
                    </div>
                      
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="evento-reciente-modal" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa  fa-folder"></i> Eventos recientes</h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-detenido" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label style="color: dodgerblue">Evento <a style="color: red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="evento" disabled="disabled"  class="select2" style="width:100%; background-color:#eee;">
                                        </select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section>
                                    <label style="color: dodgerblue">Detenido <a style="color: red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="detenido" class="select2" style="width:100%;"></select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section>
                                    <label class="input" id="idsexo"></label>
                                </section>
                                <section>
                                    <label class="input" id="idedad"></label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelevento"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default saveDetenido" id="saveDetenido"><i class="fa fa-save"></i>&nbsp;Continuar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="map"></div>
    <input type="hidden" id="estado" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <input type="hidden" id="L1" />
    <input type="hidden" id="L2" />
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
       
        var maps, infowindow;
        var responsiveHelper_dt_nuevo_detenido = undefined;
        var responsiveHelper_dt_nueva_victima = undefined;

        var breakpointDefinition = {
                tablet: 1024,
                phone: 480
        };

        //var dataC = null;
        //var datosC = [];

        function initMap() {

            maps = new google.maps.Map(document.getElementById('map'), {
                center: { lat: 25.4653403, lng: 0 },
                zoom: 15
            });
            infoWindow = new google.maps.InfoWindow;
            var lat = 25.4653403;
            var lon = -101.0320401;
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getpositiobycontract",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                processdata: true,
                traditional: true,
                data: JSON.stringify({ LlamadaId: "" }),
                cache: false,
                success: function (response) {
                    response = JSON.parse(response.d);

                    if (response.exitoso) {
                        $("#L1").val(response.obj.Latitud);
                        $("#L2").val(response.obj.Longitud);

                        CargarMapa()
                    } else {

                    }
                },
                error: function () {
                    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                }
            });
        }

        $("#addRowV").click(function () {
            $("#dt_nueva_victima").DataTable().row.add([
                "",
                "",
                "",
                ""
            ]).draw(false);
            //$(".tipoeventogrid").select2();
        });

        $("#addRow").click(function () {
                $("#dt_nuevo_detenido").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "" 
                   
            ]).draw(false);
              $(".motivoeventogrid").select2();
        });

        function LoadNuevaVictima() {

            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getDataNuevo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    bandera: "1"
                }),
                cache: false,
                success: function (response) {
                    var resultado = JSON.parse(response.d);
                    $("#dt_nueva_victima").DataTable().destroy();
                    Createtablenuevavictima(resultado.list);
                    //unknow
                    //$(".tipoeventogrid").select2();
                },
                error: function () {
                    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la tabla. Si el problema persiste contacte al soporte técnico del sistema.");
                }
            });

        }


        function LoadNuevoDetenido() {

            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getDataNuevo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    bandera: "0"
                }),
                cache: false,
                success: function (response) {
                    var resultado = JSON.parse(response.d);
                    responsiveHelper_dt_pertenencias_nuevas = undefined;
                    $("#dt_nuevo_detenido").DataTable().destroy();
                    Createtablenuevodetenido(resultado.list);
                    $(".motivoeventogrid").select2();

                },
                error: function () {
                    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la tabla. Si el problema persiste contacte al soporte técnico del sistema.");
                }
            });

        }

        function cargarGrid(url) {
            var data = null;
            var datos = [];
            if (data == null && datos.length == 0) {                
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "eventsform.aspx/" + url,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (response) {
                        data += "<option></option>";

                        // why tween$
                        $.each(response.d, function (index, item) {
                            var tt = {};
                            tt.Id = item.Id;
                            tt.Desc = item.Desc;

                            datos.push(tt);
                            data += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                        });
                    },
                    error: function () {

                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar el grid. Si el problema persiste contacte al soporte técnico del sistema.");

                    }
                });

            }
            console.log(datos);
            return datos;
        }

        $(document).ready(function () {

            $('#modalDetenido').on('hidden.bs.modal', function (e) {
                location.reload(true);
            });

        });

        $("#modalDetenido").on('shown.bs.modal', function () {
            var table = $('#dt_nuevo_detenido').DataTable();
            //console.log("cargadatos2");
            table.columns.adjust().draw();
        });
      
        function validarCamposEnTabla() {
            var isValid = true;

            $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                var nombre = this.node().childNodes[1].childNodes[0].getAttribute("data-required");
                var Apellidopaterno = this.node().childNodes[2].childNodes[0].getAttribute("data-required");
                var Apellidomaterno = this.node().childNodes[3].childNodes[0].getAttribute("data-required");
                //var bolsa = this.node().childNodes[4].childNodes[0].getAttribute("data-required");                        
                var Sexo = this.node().childNodes[4].childNodes[0].getAttribute("data-required");
                var Edad = this.node().childNodes[5].childNodes[0].getAttribute("data-required");
                var Motivo = this.node().childNodes[6].childNodes[0].getAttribute("data-required");

                if (nombre === "true") {
                    if (this.node().childNodes[1].childNodes[0].value === "" ||
                        this.node().childNodes[1].childNodes[0].value === undefined ||
                        this.node().childNodes[1].childNodes[0].value === null) {
                        this.node().childNodes[1].childNodes[0].setAttribute('class', 'errorInputTabla');
                        ShowError("Nombre", "El campo nombre es obligatorio.");
                        isValid = false;
                    }
                    else {
                        this.node().childNodes[1].childNodes[0].removeAttribute('class');
                    }
                }

                if (Apellidopaterno === "true") {
                    if (this.node().childNodes[2].childNodes[0].value === "" ||
                        this.node().childNodes[2].childNodes[0].value === undefined ||
                        this.node().childNodes[2].childNodes[0].value === null) {
                        this.node().childNodes[2].childNodes[0].setAttribute('class', 'errorInputTabla');
                        ShowError("Apellido paterno", "El campo apellido paterno es obligatorio.");
                        isValid = false;
                    }
                    else {
                        this.node().childNodes[2].childNodes[0].removeAttribute('class');
                    }
                }

                if (Apellidomaterno === "true") {
                    if (this.node().childNodes[3].childNodes[0].value === "" ||
                        this.node().childNodes[3].childNodes[0].value === undefined ||
                        this.node().childNodes[3].childNodes[0].value === null) {
                        this.node().childNodes[3].childNodes[0].setAttribute('class', 'errorInputTabla');
                        ShowError("Apellido materno", "El campo apellido materno es obligatorio.");
                        isValid = false;
                    }
                    else {
                        this.node().childNodes[3].childNodes[0].removeAttribute('class');
                    }
                }

                if (Sexo === "true") {
                    if (this.node().childNodes[4].childNodes[0].value === "0" ||
                        this.node().childNodes[4].childNodes[0].value === undefined ||
                        this.node().childNodes[4].childNodes[0].value === null) {
                        this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                        ShowError("Sexo", "El sexo es obligatorio.");
                        isValid = false;
                    }
                    else {
                        this.node().childNodes[4].childNodes[0].removeAttribute('class');
                    }
                }

                if (Edad == "true") {
                    if (this.node().childNodes[5].childNodes[0].value === "") {
                        this.node().childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                        ShowError("Edad", "El campo edad es obligatorio.");
                        isValid = false;
                    }
                    else {

                        if (this.node().childNodes[5].childNodes[0].value === "0") {
                            this.node().childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Edad", "El campo edad debe ser mayor a 0.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[5].childNodes[0].removeAttribute('class');
                        }
                    }
                }

                if (Motivo === "true") {
                    if (this.node().childNodes[6].childNodes[0].value === "0") {
                        //this.node().childNodes[6].childNodes[0].setAttribute('class', 'errorInputTabla');
                        ShowError("Motivo", "El campo motivo es obligatorio.");
                        isValid = false;
                    }
                    else {

                    }
                }
            });

            return isValid;
        }

        function Createtablenuevavictima(data) {
            $('#dt_nueva_victima').dataTable({
                "lengthMenu": [5, 10, 20, 50],
                iDisplayLength: 4,
                serverSide: false,
                paging: true,
                retrieve: true,
                //"scrollY": "350px",
                //"scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_nueva_victima) {
                        responsiveHelper_dt_nueva_victima = new ResponsiveDatatablesHelper($('#dt_nueva_victima'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_nueva_victima.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_nueva_victima.respond();
                    $('#dt_nueva_victima').waitMe('hide');
                },
                data: data,
                columns: [
                    null,
                    null,
                    null,
                    null
                    //null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<a class="btn btn-xs btn-danger btn-circle remiRow" title="Eliminar"><i class="glyphicon glyphicon-remove"></i></a>';
                        }
                    },
                    {
                        width: "300px",
                        targets: 1,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            return "<input type='text' value='' style='width:100%' data-required='true' />";
                        }
                    },       
                    {
                        width: "440px",
                        targets: 2,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            ////
                            var datos = cargarGrid("getTipo");
                            var options = "";
                            options += "<option value='0'>[Tipo de victima]</option>";

                            for (var i = 0; i < datos.length; i++) {
                                // break tim3
                                options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                            }
                            return "<select style='width:100%' data-required='true'>" + options + "</select>";
                            //return "<select name='tipoVictima' id='tipovictimagrid' class='select2 tipovictimagrid'  style='width:100%' data-required='true'>" + options + "</select>";
                        }
                    },
                    {
                        width: "300px",
                        targets: 3,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            return "<input type='text' value='' style='width:100%' data-required='true' />";
                        }
                    },                    
                ]
            });
        }


        function Createtablenuevodetenido(data) {
            $('#dt_nuevo_detenido').dataTable({
                "lengthMenu": [5, 10, 20, 50],
                iDisplayLength: 7,
                serverSide: false,
                paging: true,
                retrieve: true,
                //"scrollY": "350px",
                //"scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_nuevo_detenido) {
                        responsiveHelper_dt_nuevo_detenido = new ResponsiveDatatablesHelper($('#dt_nuevo_detenido'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_nuevo_detenido.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_nuevo_detenido.respond();
                    $('#dt_nuevo_detenido').waitMe('hide');
                },
                data: data,
                columns: [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                    //null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<a class="btn btn-xs btn-danger btn-circle removeRow" title="Eliminar"><i class="glyphicon glyphicon-remove"></i></a>';
                        }
                    },
                    {
                        width: "200px",
                        targets: 1,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            return "<input type='text' value='' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        width: "200px",
                        targets: 2,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                            return "<input type='text' value='' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        width: "200px",
                        targets: 3,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                            return "<input type='text' value='' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        width: "145px",
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var datos = cargarGrid("getSexogrid");
                            var options = "";
                            options += "<option value='0'>[Sexo]</option>";

                            for (var i = 0; i < datos.length; i++) {
                                options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                            }

                            return "<select style='width:100%' data-required='true'>" + options + "</select>";
                        }
                    },
                    {
                        width: "40px",
                        targets: 5,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                            return "<input type='text' maxlength='3' pattern='^[0-9]*$' value='' style=' width:100%' data-required='true'  />";
                        }
                    },
                    {
                        width: "440px",
                        targets: 6,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                            var datos = cargarGrid("getMotivogrid");
                            var options = "";
                            options += "<option value='0'>[Motivo]</option>";

                            for (var i = 0; i < datos.length; i++) {

                                if (parseInt(datos[i].Id) == parseInt($("#motivoevento").val())) {
                                    options += "<option value='" + datos[i].Id + "'selected='selected'>" + datos[i].Desc + "</option>";
                                }
                                else {
                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                }
                            }

                            return "<select name='Motivo' id='motivoeventogrid' class='select2 motivoeventogrid'  style='width:100%' data-required='true'>" + options + "</select>";
                        }
                    }
                    //,
                    //{
                    //     targets: 7,
                    //    orderable: false,
                    //    render: function (data, type, row, meta) {
                    //        return '<a class="btn btn-primary btn-circle  deleteitem "  title="Eliminar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                    //    }
                    //}
                ]
            });
        }

        $("body").on("click", ".removeRow", function () {
            var table = $('#dt_nuevo_detenido').DataTable();
            $(this).parent().parent().addClass("selectedRow");
            table.row('.selectedRow').remove().draw(false);
        });

        $("body").on("click", ".remiRow", function () {
            var table = $('#dt_nueva_victima').DataTable();
            $(this).parent().parent().addClass("selectedRow");
            table.row('.selectedRow').remove().draw(false);
        });

        $(document).on('keydown', 'input[pattern]', function (e) {
            var input = $(this);
            var oldVal = input.val();
            var regex = new RegExp(input.attr('pattern'), 'g');

            setTimeout(function () {
                var newVal = input.val();
                if (!regex.test(newVal)) {
                    input.val(oldVal);
                }
            }, 0);
        });

        $(document).ready(function () {
            $("#motivoevento").select2();
            $("#motivoeventogrid").select2();
            $("#llamada").select2();
            $("#unidad").select2();
            $("#responsable").select2();
            $("#municipio").select2();
            $("#colonia").select2();
            $("#alertaWebInstitucion").select2();
            $("#alertaWebUnidad").select2();
            $("#sexoDetenido").select2();
            //added
            $("#tipoVictima").select2();
            $("#evento").select2();
            $("#detenido").select2();
            $('#autorizaciondatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss',
                defaultDate: new Date(),
                autoclose: true
            }).on('change', function (e) {
                $(".bootstrap-datetimepicker-widget").toggle();
            });
            //$('#FehaNacimientodatetimepicker').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});

            var param = RequestQueryString("tracking");
            var responsiveHelper_dt_basic_tabla_unidades = undefined;
            var responsiveHelper_dt_basic_tabla_detenidos = undefined;
            //d3l3t3 th1$ $tuff
            //var responsiveHelper_dt_basic_tabla_eventos = undefined;
            var responsiveHelper_dt_basic_tabla_victimas = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            getalerta();
            function getalerta() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/GetAlertaWeb",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.d != null) {
                            data = data.d;
                            /*
                             * Yucatan
                            var alerta1 = "Pitoria";
                            */
                            var alerta1 = data.Denominacion;
                            var alerta2 = data.AlertaWerb;

                            //$("#Denominacin").val(data.Denominacion);

                            //$("#linkEvento").html('<i class="glyphicon glyphicon-folder-open"></i>&nbsp; ' + alerta1 + ' / evento');
                            $("#alerta1").html(alerta1);
                            //$("#alerta2").html('<i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>' + alerta1);
                            //$("#alerta3").html(' <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de ' + alerta2);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de alerta web. Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
            }

            window.table = $('#dt_basic_tabla_victimas').dataTable({
                "lengthMenu": [5, 20, 50, 100],
                iDisplayLength: 4,
                serverSide: true,
                fixedColumns: true,
                bPaginate: false,
                bLengthChange: false,
                autoWidth: true,
                bFilter: false,
                bInfo: false,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_tabla_victimas) {
                        responsiveHelper_dt_basic_tabla_victimas = new ResponsiveDatatablesHelper($('#dt_basic_tabla_victimas'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_tabla_victimas.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_tabla_victimas.respond();
                    $('#dt_basic_tabla_victimas').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
            
                },
                ajax: {
                    type: "POST",
                    url: "eventsform.aspx/getEventoVictimas",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic_tabla_victimas').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
            
                        var trackingid;
            
                        if (param !== undefined) {
                            trackingid = param;
                        }
                        else {
                            trackingid = "";
                        }
            
                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = trackingid;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Tipo de victima",
                        data: "TipoVictima"
                    },
                    //{
                    //    name: "Reparacion",
                    //    data: "Reparacion",                        
                    //    visible: false
                    //},
                    {
                        name: "Daños o perjuicios",
                        data: "DanioPerjuicio"
                    }
                ],
                columnDefs: [
                    {
            
                    }
                ]
            
            });

            window.table = $('#dt_basic_tabla_detenidos').dataTable({
                "lengthMenu": [5, 20, 50, 100],
                iDisplayLength: 5,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_tabla_detenidos) {
                        responsiveHelper_dt_basic_tabla_detenidos = new ResponsiveDatatablesHelper($('#dt_basic_tabla_detenidos'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_tabla_detenidos.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_tabla_detenidos.respond();
                    $('#dt_basic_tabla_detenidos').waitMe('hide');
                },
                "createdRow": function (row, data, index) {

                },
                ajax: {
                    type: "POST",
                    url: "eventsform.aspx/getDetenidosTabla",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic_tabla_detenidos').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        var trackingid;

                        if (param !== undefined) {
                            trackingid = param;
                        }
                        else {
                            trackingid = "";
                        }

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = trackingid;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Sexo",
                        data: "Sexo"
                    },
                    {
                        name: "Edad",
                        data: "Edad"
                    },
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                ],
                columnDefs: [
                    {

                    }
                ]

            });

            $('#dt_basic_tabla_unidades').dataTable({
                "lengthMenu": [5, 20, 50, 100],
                iDisplayLength: 5,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_tabla_unidades) {
                        responsiveHelper_dt_basic_tabla_unidades = new ResponsiveDatatablesHelper($('#dt_basic_tabla_unidades'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_tabla_unidades.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_tabla_unidades.respond();
                    $('#dt_basic_tabla_unidades').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "eventsform.aspx/getUnidadadesTabla",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic_tabla_unidades').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        var trackingid;

                        if (param !== undefined) {
                            trackingid = param;
                        }
                        else {
                            trackingid = "";
                        }

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = trackingid;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        name: "Unidad",
                        data: "Unidad"
                    },
                    {
                        name: "ClaveResponsable",
                        data: "ClaveResponsable"
                    },
                    {
                        name: "Corporacion",
                        data: "Corporacion"
                    },
                ],
                columnDefs: [

                ]
            });

            $("#municipio").change(function () {
                loadNeighborhood("0", $("#municipio").val());
            });

            $("#colonia").change(function () {
                loadZipCode($("#colonia").val());
            });

            $("#detenido").change(function () {
                var id = $("#detenido").val();

                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getbyid",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ _id: id }),
                    cache: false,
                    success: function (data) {
                        data = data.d;
                        if (data.Sexo != undefined) {
                            $("#idsexo").text('Sexo: ' + data.Sexo);
                        }
                        else {
                            $("#idsexo").text('');
                        }
                        if (data.Edad != undefined) {
                            $("#idedad").text('Edad: ' + data.Edad);
                        }
                        else {
                            $("#idedad").text('');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de motivos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            });

           

            $("#llamada").change(function () {
                if ($(this).val() == "0") {
                    loadCities("0", $("#estado").val());
                    loadNeighborhood("0", "0");
                    $("#codigoPostal").val("");
                    $("#descripcion").val("");
                    $("#lugar").val("");
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "eventsform.aspx/getCall",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processdata: true,
                        traditional: true,
                        data: JSON.stringify({ LlamadaId: $("#llamada").val() }),
                        cache: false,
                        success: function (response) {
                            response = JSON.parse(response.d);

                            if (response.exitoso) {
                                loadCities(response.obj.IdMunicipio, response.obj.IdEstado);
                                loadNeighborhood(response.obj.ColoniaId, response.obj.IdMunicipio);
                                $("#codigoPostal").val(response.obj.CodigoPostal);
                                $("#descripcion").val(response.obj.Descripcion);
                                $("#lugar").val(response.obj.Lugar);
                            } else {
                                ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }
            });

            $("#alertaWebInstitucion").change(function () {
                loadUnidadesAW("0", $("#alertaWebInstitucion").val());
                loadEventosAW("0", 0);
            });

            $("#alertaWebUnidad").change(function () {
                loadEventosAW($("#alertaWebUnidad").val(), 0);
            });
            
            $("body").on("click", "#add", function () {
                var tracking = $(this).attr("data-tracking");

                cargarEvento(tracking);
                cargarDetenido(tracking);
         
                $("#evento-reciente-modal").modal("show");
            });

            $("#dt_nuevo_detenido").on("click", "a.deleteitem", function () {
            //  e.preventDefault();
 
            //editor.remove( $(this).closest('tr'), {
            //title: 'Delete record',
            //message: 'Are you sure you wish to remove this record?',
            //buttons: 'Delete'
            });


    //           $('#example').on('click', 'a.editor_remove', function (e) {
    //    e.preventDefault();
 
    //    editor.remove( $(this).closest('tr'), {
    //        title: 'Delete record',
    //        message: 'Are you sure you wish to remove this record?',
    //        buttons: 'Delete'
    //    } );
    //} );


            function loadFolio() {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getFolio",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        response = JSON.parse(response.d);

                        if (response.exitoso) {
                            $("#folio").val(response.folio);
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadCities(setvalue, idEstado) {
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getCities",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idEstado: idEstado
                    }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#municipio');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Municipio]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de municipios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
          
            function loadMotivos(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getMotivogrid",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        
                    }),
                    success: function (response) {
                        var Dropdown = $("#motivoevento");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Motivo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de motivos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadNeighborhood(set, idMunicipio) {
                $.ajax({
                    type: "POST",
                    url: "callform.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#colonia");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de colonias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadUnidad(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getUnidades",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $("#unidad");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Unidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de unidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadTipo(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getTipo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $("#tipoVictima");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Tipo de victima]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                            console.log(item.Desc);
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de victimas. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadSexo(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getSexo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $("#sexoDetenido");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Sexo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de sexos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadResponsable(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getResponsables",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $("#responsable");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Clave-responsable]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadZipCode(idColonia) {
                $.ajax({
                    type: "POST",
                    url: "callform.aspx/getZipCode",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idColonia: idColonia
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        $("#codigoPostal").val(resultado.cp);
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function generarObjeto($items) {
                var obj = {};
                $items.each(function () {
                    var id = this.id;
                    obj[id] = $(this).val();
                });
                return obj;
            }

            function llenarSelect(idSelect, datos) {
                var select = document.getElementById('' + idSelect);

                for (var i = 0; i < datos.length; i++) {
                    var opt = document.createElement('option');
                    opt.innerHTML = datos[i].Nombre;
                    opt.value = datos[i].Id;
                    select.appendChild(opt);
                }
            }

            function camposVacios(_items) {
                var _tmpItems = [].slice.call(_items);
                var esValido = true;
                _tmpItems.map(function (item) {
                    var parent = item.parentNode;
                    var attName = item.getAttribute('name');
                    if (item.nodeName.toLowerCase() === 'select' && item.value === '0') {
                        ShowError('' + attName, 'El campo ' + attName.toLowerCase() + ' es obligatorio.');
                        parent.classList.remove('state-success');
                        parent.classList.add('state-error');
                        item.classList.remove('valid');
                        esValido = false;
                    } else if (item.value === '' || item.value === undefined || item.value.length === 0) {
                        ShowError('' + attName, 'El campo ' + attName.toLowerCase() + ' es obligatorio.');
                        parent.classList.remove('state-success');
                        parent.classList.add('state-error');
                        item.classList.remove('valid');
                        esValido = false;
                    } else {
                        parent.classList.add('state-success');
                        parent.classList.remove('state-error');
                        item.classList.add('valid');
                    }
                });
                return esValido;
            }

            function validarUnidad() {
                var esvalido = true;
                if ($("#unidad").val() == null || $("#unidad").val() == "0") {
                    ShowError("Unidad", "El campo unidad es obligatorio.");
                    $('#unidad').parent().removeClass('state-success').addClass("state-error");
                    $('#unidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#unidad').parent().removeClass("state-error").addClass('state-success');
                    $('#unidad').addClass('valid');
                }

                if ($("#responsable").val() == null || $("#responsable").val() == "0") {
                    ShowError("Clave-responsable", "El campo clave-responsable es obligatorio.");
                    $('#responsable').parent().removeClass('state-success').addClass("state-error");
                    $('#responsable').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#responsable').parent().removeClass("state-error").addClass('state-success');
                    $('#responsable').addClass('valid');
                }

                return esvalido;
            }
            $("#nombreDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("#paternoDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("#maternoDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            function validarResponsable() {
                var esvalido = true;
                if ($("#nombreDetenido").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El campo nombre es obligatorio.");
                    $('#nombreDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#nombreDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#nombreDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#nombreDetenido').addClass('valid');
                }

                if ($("#paternoDetenido").val().split(" ").join("") == "") {
                    ShowError("Apellido paterno", "El campo apellido paterno es obligatorio.");
                    $('#paternoDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#paternoDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#paternoDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#paternoDetenido').addClass('valid');
                }

                if ($("#maternoDetenido").val().split(" ").join("") == "") {
                    ShowError("Apellido materno", "El campo apellido materno es obligatorio.");
                    $('#maternoDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#maternoDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#maternoDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#maternoDetenido').addClass('valid');
                }

                if ($("#sexoDetenido").val() == null || $("#sexoDetenido").val() == "0") {
                    ShowError("Sexo", "El campo sexo es obligatorio.");
                    $('#sexoDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#sexoDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#sexoDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#sexoDetenido').addClass('valid');
                }

                

                if ($("#edad").val() == null || $("#edad").val() == "" || $("#edad").val() <= 0 || $("#edad").val() > 105) {
                    if ($("#edad").val() == null || $("#edad").val() == "") ShowError("Edad", "El campo edad es obligatorio.");
                    else if ($("#edad").val() <= 0) ShowError("Edad", "La edad mínima es de 1 año.");
                    else if ($("#edad").val() > 105) ShowError("Edad", "La edad máxima es de 105 años.");
                    $('#edad').parent().removeClass('state-success').addClass("state-error");
                    $('#edad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#edad').parent().removeClass("state-error").addClass('state-success');
                    $('#edad').addClass('valid');
                }

                return esvalido;
            }

            function obtenerEvento(tkg) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/getEventoByTrackingId",
                    data: JSON.stringify({ tracking: tkg }),
                    success: function (response) {
                        response = JSON.parse(response.d);
                        endLoading();

                        if (response.exitoso) {
                            loadCalls(response.obj.IdLlamada);
                            loadCities(response.obj.IdMunicipio, response.obj.IdEstado);
                            loadNeighborhood(response.obj.ColoniaId, response.obj.IdMunicipio);
                            loadMotivos(response.obj.MotivoId);
                            $("#codigoPostal").val(response.obj.CodigoPostal);
                            $("#descripcion").val(response.obj.Descripcion);
                            $("#lugar").val(response.obj.Lugar);
                            $("#fecha").val(response.obj.HoraYFecha);
                            $("#folio").val(response.obj.Folio);
                            $("#numeroDetenidos").val(response.obj.NumeroDetenidos);
                          
                            $("#latitud").val(response.obj.Latitud);
                            $("#longitud").val(response.obj.Longitud);
                            if ($('#latitud').val() == "" || $('#longitud').val() == "") {
                                  
                            }
                            else {
                                $("#L1").val(response.obj.Latitud);
                                $("#L2").val(response.obj.Longitud);
                            }
                            CargarMapa();
                            $('#linkUnidad').show();
                            $('#linkDetenidos').show();

                            window.emptytable = false;
                            responsiveHelper_dt_basic_tabla_eventos = undefined;
                            $("#dt_basic_tabla_eventos").DataTable().destroy();
                            // loadEventosAW(response.obj.UnidadIdAW,response.obj.relacionado);

                            $("#btnGuardarUnidad").attr("data-EventoId", response.obj.Id);
                            $("#btnGuardarDetenido").attr("data-EventoId", response.obj.Id);
                            $("#btnGuardarVictima").attr("data-EventoId", response.obj.Id);
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                        }
                    },
                    error: function (error) {
                       
                        endLoading();
                    }
                });
            }

            function loadCalls(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getCalls",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#llamada');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Sin llamada]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de llamadas. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadUnidadesAW(setvalue, institucionId) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getUnidadesAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "institucionId": institucionId }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#alertaWebUnidad');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Unidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadInstitucionesAW(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getInstitucionesAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#alertaWebInstitucion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Institucion]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de instituciones. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            //loadNeighborhood("0", "0");
            function loadEventosAW(idUnidadInstitucion, relacionado) {
                responsiveHelper_dt_basic_tabla_eventos = undefined;
                $("#dt_basic_tabla_eventos").DataTable().destroy();

                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getEventosAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idUnidadInstitucion: idUnidadInstitucion
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);
                      
                        if (r.latitud != '')
                            $("#latitud").val(r.latitud);

                        if (r.longitud != '')
                            $("#longitud").val(r.longitud);

                        llenarTabla(r.lista, relacionado);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de eventos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });

            }

            function llenarTabla(lista, relacionado) {
                $('#dt_basic_tabla_eventos').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: false,
                    fixedColumns: true,
                    fixedColumns: true,
                    autoWidth: true,
                    "scrollY": "100%",
                    "scrollX": "0%", // // // // // // // // // // // // // // // //// // // // // // // //// // // // // // // //// // // // // // // //// // // // // // // //
                    "scrollCollapse": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_tabla_eventos) {
                            responsiveHelper_dt_basic_tabla_eventos = new ResponsiveDatatablesHelper($('#dt_basic_tabla_eventos'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_tabla_eventos.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_tabla_eventos.respond();
                        $('#dt_basic_tabla_eventos').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                    },
                    data: lista,
                    columnDefs: [
                        {
                            targets: 0,
                            name: "UnidadId",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (param != undefined) {
                                    if (relacionado == 0)
                                        return '<a class="btn btn-info btn-xs relacionar" href="#" data-unidadId="' + row.UnidadId + '">Relacionar</a>';
                                    else
                                        return "";
                                }
                                else {
                                    return "";
                                }
                            }
                        },
                        {
                            targets: 1,
                            data: "Folio",
                            render: function (data, type, row, meta) {
                                return row.Folio;
                            }
                        },
                        {
                            targets: 2,
                            data: "Fecha",
                            render: function (data, type, row, meta) {
                                return row.Fecha;
                            }
                        },
                        {
                            targets: 3,
                            data: "Motivo",
                            render: function (data, type, row, meta) {
                                return row.Motivo;
                            }
                        },
                        {
                            targets: 4,
                            data: "NumeroDetenidos",
                            render: function (data, type, row, meta) {
                                return row.NumeroDetenidos;
                            }
                        },
                        {
                            targets: 5,
                            data: "Detenidos",
                            render: function (data, type, row, meta) {
                                return row.Detenidos;
                            }
                        },
                        {
                            targets: 6,
                            data: "Estado",
                            render: function (data, type, row, meta) {
                                return "<div class='wrapping'>" + row.Estado + "</div>";
                            }
                        },
                        {
                            targets: 7,
                            data: "Municipio",
                            render: function (data, type, row, meta) {
                                return row.Municipio;
                            }
                        },
                        {
                            targets: 8,
                            data: "Colonia",
                            render: function (data, type, row, meta) {
                                return row.Colonia;
                            }
                        },
                        {
                            targets: 9,
                            data: "Numero",
                            render: function (data, type, row, meta) {
                                return row.Numero;
                            }
                        },
                        {
                            targets: 10,
                            data: "EntreCalle",
                            render: function (data, type, row, meta) {
                                return row.EntreCalle;
                            }
                        },
                        {
                            targets: 11,
                            data: "Responsable",
                            render: function (data, type, row, meta) {
                                return "<div class='wrapping'>" + row.Responsable + "</div>";
                            }
                        },
                        {
                            targets: 12,
                            data: "Descripcion",
                            render: function (data, type, row, meta) {
                                return "<div class='wrapping'>" + row.Descripcion + "</div>";
                            }
                        }
                    ]
                });
            }

            function init() {
                if (param !== undefined) {
                    obtenerEvento(param);
                    $('#add').show();
                    $('#add').attr("data-tracking", param);
                    cargarPaisEstadoMunicipio(false);
                }
                else {
                    //loadCountries("73");
                    //loadStates("0", 73);
                    loadMotivos("0");
                    cargarPaisEstadoMunicipio(true);
                    loadFolio();
                }
                loadCalls("0");
                loadInstitucionesAW("0");
                loadUnidadesAW("0", "");

                if (param !== undefined) {
                    obtenerEvento(param);
                    $('#add').show();
                    $('#add').attr("data-tracking", param);
                    cargarPaisEstadoMunicipio(false);
                }
                else {
                    //loadCountries("73");
                    //loadStates("0", 73);
                    cargarPaisEstadoMunicipio(true);
                    loadFolio();
                }
            }

            function cargarPaisEstadoMunicipio(cargarCombo) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getPaisEstadoMunicipio",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        if (r.exitoso) {
                            $("#estado").val(r.Estado);
                            $("#municipio").val(r.Municipio);
                            if (cargarCombo) {
                                loadCities("1918", r.Estado);
                                loadNeighborhood("0", "1918");
                            }

                            $("#localizacionHeader").text(r.Localizacion);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });

            }

            function cargarDetenido(id) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getDetenidos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $('#detenido');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Detenido]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        Dropdown.val("0");
                        Dropdown.trigger('change');
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function cargarEvento(tracking) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getEvento",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        tracking: tracking
                    }),
                    success: function (response) {
                        var Dropdown = $('#evento');
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        Dropdown.trigger('change');
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar el evento. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function ObtenerValores() {
                var datos = [
                    eventoId = $('#evento').val(),
                    detenidoId = $('#detenido').val()
                ];

                return datos;
            }

            function validar() {
                var esvalido = true;

                if ($("#detenido").val() == null || $("#detenido").val() == 0) {
                    ShowError("Detenido", "El campo de detenido es obligatorio.");
                    $('#detenido').parent().removeClass('state-success').addClass("state-error");
                    $('#detenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#detenido').parent().removeClass("state-error").addClass('state-success');
                    $('#detenido').addClass('valid');
                }

                return esvalido;
            }
            function obtenerDatosTabla(eventoId) {
                var dataArreglo = new Array();
                var DetenidoEvento;

                $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    DetenidoEvento = {};
                    DetenidoEvento.EventoId = eventoId;
                    DetenidoEvento.Nombre = this.node().childNodes[1].childNodes[0].value;
                    DetenidoEvento.Paterno = this.node().childNodes[2].childNodes[0].value;
                    DetenidoEvento.Materno = this.node().childNodes[3].childNodes[0].value;
                    DetenidoEvento.SexoId = this.node().childNodes[4].childNodes[0].value;
                    DetenidoEvento.Edad = this.node().childNodes[5].childNodes[0].value;
                    DetenidoEvento.Motivo = this.node().childNodes[6].childNodes[0].value;

                    dataArreglo.push(DetenidoEvento);
                });               

                return dataArreglo;
            }

            function GuardarDetenido() {
                startLoading();

                var datos = ObtenerValores();
                

                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/saveDetenidoBarandilla",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso && resultado.alerta == false) {
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del detenido se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del detenido se " + resultado.mensaje + " correctamente.");

                            $("#evento-reciente-modal").modal("hide");
                            location.href = "entry.aspx?tracking=" + resultado.id;

                        }
                        else if (resultado.exitoso && resultado.alerta == true) {
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-times'></i><strong>Atención! </strong>" +
                                resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowAlert("Atención!", resultado.mensaje);
                            $('#main').waitMe('hide');

                            $("#evento-reciente-modal").modal("hide");

                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("body").on("click", "#saveDetenido", function () {
                $("#ctl00_contenido_lblMessage").html("");

                if (validar()) {
                    GuardarDetenido();
                }
            });


            $("#save_").on("click", function () {
                startLoading();
                var $items = $('[data-requerido]');

                var items_validar = $('[data-requerido="true"]');

                if (!camposVacios(items_validar)) {
                    endLoading();
                    return false;
                }

                var latitud ;
                var longitud ;

                var obj = generarObjeto($items);
                obj.tracking = param !== undefined ? param : "";
                obj.llamada = $("#llamada").val();
                obj.unidadAW = $("#alertaWebUnidad").val();
           
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/save",
                    data: JSON.stringify({ csObj: obj }),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso) {
                            $("#folio").val(response.folio);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del evento se " + response.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del evento se " + response.mensaje + " correctamente.");
                            $('#main').waitMe('hide');
                           
                            if (obj.tracking == "")
                            {
                                var redirect = "";
                                 redirect=" <%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/eventsform.aspx?tracking=" + response.tracking  ;
                                setTimeout( window.location.assign(redirect),1000); 
                            }
                            $('#linkUnidad').show();
                            $('#linkDetenidos').show();
                            $('#add').show();

                            $("#btnGuardarUnidad").attr("data-EventoId", response.EventoId);
                            $("#btnGuardarUnidad").attr("data-tracking", response.tracking);

                            $("#btnGuardarDetenido").attr("data-EventoId", response.EventoId);
                            $("#btnGuardarDetenido").attr("data-tracking", response.tracking);

                            $("#btnGuardarVictima").attr("data-EventoId", response.EventoId);
                            $("#btnGuardarVictima").attr("data-tracking", response.tracking);

                            $('#add').attr("data-tracking", response.tracking);
                            //alert(response.tracking);
                            param = response.tracking;

                            window.emptytable = false;
                            responsiveHelper_dt_basic_tabla_eventos = undefined;
                            $("#dt_basic_tabla_eventos").DataTable().destroy();

                            latitud = $("#latitud").val();
                            longitud = $("#longitud").val();
                            
                            loadEventosAW($("#alertaWebUnidad").val());
                        
                            $("#latitud").val(latitud);
                            $("#longitud").val(longitud);
                        }
                        else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico. " + response.mensaje);

                            if (response.fallo == 'fecha') {
                                var fecha = document.getElementById("fecha");
                                fecha.parentNode.classList.remove('state-success');
                                fecha.parentNode.classList.add('state-error');
                            }
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
       

               
            });

            $("#btnGuardarUnidad").on("click", function () {
                startLoading();
                var $items = $('[data-requerido-unidad]');

                if (!validarUnidad()) {
                    endLoading();
                    return false;
                }

                var obj = generarObjeto($items);
                obj.EventoId = $(this).attr("data-EventoId");

                if (param == undefined) {
                    param = $(this).attr("data-tracking");
                }

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/saveUnidad",
                    data: JSON.stringify({ csObj: obj }),
                    success: function (response) {
                        var response = JSON.parse(response.d);
                        if (response.exitoso) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información de la unidad se " + response.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información de la unidad se " + response.mensaje + " correctamente.");
                            $('#main').waitMe('hide');

                            $("#modalUnidad").modal("hide");

                            $("#dt_basic_tabla_unidades").emptytable = false;
                            $("#dt_basic_tabla_unidades").DataTable().table().ajax.reload();
                        } else {
                            if (response.alerta) {
                                $('#main').waitMe('hide');
                                ShowAlert("¡Atención!",  response.mensaje);
                            }

                            else {
                                ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                                $('#main').waitMe('hide');
                            }
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
            });

            $("#btnGuardarDetenido").on("click", function () {
                startLoading();
                var $items = $('[data-requerido-detenido]');

                var filas = 0;
                $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    filas++;
                });

                if (filas > 0) {
                    if (!validarCamposEnTabla()) {
                        endLoading();
                        return;
                    }
                }
                else {
                    endLoading();
                    return;
                }

                // sh02
                var list2 = obtenerDatosTabla( $(this).attr("data-EventoId"));
                var obj = generarObjeto($items);
                obj.EventoId = $(this).attr("data-EventoId");
                obj.NumeroDetenidos = $("#numeroDetenidos").val();
                //obj.FechaNacimiento = $("#fechanacimiento").val();
                obj.Edad = $("#edad").val();
                if (param == undefined) {
                    param = $(this).attr("data-tracking");
                }

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/saveDetenido",
                    data: JSON.stringify({ csObj: obj ,list:list2}),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso && response.Alertadetenido == false) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se registro satisfactoriamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se registró satisfactoriamente.");
                            setTimeout(hideMessage, hideTime);
                            
                            $('#main').waitMe('hide');
                           setTimeout( $("#modalDetenido").modal("hide"),95000);
                          
                        }
                        else if (response.exitoso && response.Alertadetenido == true) {
                            limpiar();
                            ShowAlert("¡Atención!", response.Mensajealerta);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se registró satisfactoriamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se registró satisfactoriamente.");
                              
                            setTimeout(hideMessage, hideTime);
                          
                            $('#main').waitMe('hide');

                           setTimeout( $("#modalDetenido").modal("hide"),95000);

                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }

                        else {
                            if (response.mensaje == " \n La fecha de nacimiento debe de ser mayor a un año") {
                                ShowError("Fecha de nacimiento", response.mensaje);
                            }
                            else {
                                ShowError("¡Error!", response.mensaje + " Si el problema persiste contacte al personal de soporte técnico.");
                            }
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
            });

            $("#btnGuardarVictima").on("click", function () {
                startLoading();
                var $items = $('[data-requerido-victima]');

                var filas = 0;
                $("#dt_nueva_victima").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    filas++;
                });

                if (filas > 0) {
                    if (!validarCamposEnTabla()) {
                        endLoading();
                        return;
                    }
                }
                else {
                    endLoading();
                    return;
                }

                // sh02
                var list2 = obtenerDatosTabla($(this).attr("data-EventoId"));
                var obj = generarObjeto($items);
                obj.EventoId = $(this).attr("data-EventoId");
                obj.NumeroDetenidos = $("#numeroDetenidos").val();
                //obj.FechaNacimiento = $("#fechanacimiento").val();
                obj.Edad = $("#edad").val();
                if (param == undefined) {
                    param = $(this).attr("data-tracking");
                }

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/saveVictima",
                    data: JSON.stringify({ csObj: obj, list: list2 }),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso && response.Alertadetenido == false) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se registro satisfactoriamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se registró satisfactoriamente.");
                            setTimeout(hideMessage, hideTime);

                            $('#main').waitMe('hide');
                            setTimeout($("#modalDetenido").modal("hide"), 95000);

                        }
                        else if (response.exitoso && response.Alertadetenido == true) {
                            limpiar();
                            ShowAlert("¡Atención!", response.Mensajealerta);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se registró satisfactoriamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se registró satisfactoriamente.");

                            setTimeout(hideMessage, hideTime);

                            $('#main').waitMe('hide');

                            setTimeout($("#modalDetenido").modal("hide"), 95000);

                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }

                        else {
                            if (response.mensaje == " \n La fecha de nacimiento debe de ser mayor a un año") {
                                ShowError("Fecha de nacimiento", response.mensaje);
                            }
                            else {
                                ShowError("¡Error!", response.mensaje + " Si el problema persiste contacte al personal de soporte técnico.");
                            }
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
            });


            //function closemodaldetenidoevento()
            //{
            //    $("#modalDetenido").modal("hide");
            //}

            $("body").on("click", "#linkVictima", function () {
                $("#nombre").val("");
                LoadNuevaVictima();

                $('#nombre').parent().removeClass('state-success');
                $('#nombre').parent().removeClass("state-error");
                $('#tipoVictima').parent().removeClass('state-success');
                $('#tipoVictima').parent().removeClass("state-error");
                $('#danioPerjuicio').parent().removeClass('state-success');
                $('#danioPerjuicio').parent().removeClass("state-error");
                //cargar catalogue
                loadTipo("0");

                $("#modal-detenido-title").html("<i class='fa fa-pencil'></i>Agregar victima");
                $("#modalVictima").modal("show");
            });


            $("body").on("click", "#linkUnidad", function () {
                limpiar();
                loadUnidad("0");
                loadResponsable("0");
                $("#modal-unidad-title").html("<i class='fa fa-pencil'></i>Agregar unidad");
                $("#modalUnidad").modal("show");
            });

            $("body").on("click", "#linkDetenidos", function () {
                $("#nombreDetenido").val("");
                $("#paternoDetenido").val("");
                $("#maternoDetenido").val("");
                LoadNuevoDetenido();

                $('#nombreDetenido').parent().removeClass('state-success');
                $('#nombreDetenido').parent().removeClass("state-error");
                $('#paternoDetenido').parent().removeClass('state-success');
                $('#paternoDetenido').parent().removeClass("state-error");
                $('#maternoDetenido').parent().removeClass('state-success');
                $('#maternoDetenido').parent().removeClass("state-error");
                $('#sexoDetenido').parent().removeClass('state-success');
                $('#sexoDetenido').parent().removeClass("state-error");
                //$('#fechanacimiento').parent().removeClass('state-success');
                //$("#fechanacimiento").val("");
                $('#edad').parent().removeClass('state-success');
                $("#edad").val("");

                loadSexo("0");
             
                $("#modal-detenido-title").html("<i class='fa fa-pencil'></i>Agregar detenido");
                $("#modalDetenido").modal("show");
            });

            $('#numeroDetenidos').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            });

            $("body").on("click", ".relacionar", function () {
                var obj = {};

                obj.unidadId = $(this).attr("data-unidadId");
                obj.institucionId = $("#alertaWebInstitucion").val();
                obj.tracking = param !== undefined ? param : "";

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/relacionarLlamadaAW",
                    data: JSON.stringify({ csObj: obj }),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información de alerta web se relacionó correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información de alerta web se relacionó correctamente.");
                            $('#main').waitMe('hide');
                            $("#modalDetenido").modal("hide");
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
            });

            init();

            function limpiar() {
                $('#unidad').parent().removeClass('state-success');
                $('#unidad').parent().removeClass("state-error");
                $('#responsable').parent().removeClass('state-success');
                $('#responsable').parent().removeClass("state-error");
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
        });
        
       
        function CargarMapa()
        {

          //  if ($('#latitud').val() == "" || $('#longitud').val() == "") {

                mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
                var coordinates = document.getElementById('coordinates');
                function onDragEnd() {
                    var lngLat = marker.getLngLat();
                    coordinates.style.display = 'block';
                    coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                    let x = lngLat.lng;
                    let y = x;
                    x = x.toString();
                    x = x.substring(0, x.length - 3);
                    $('#longitud').val(x);

                    x = lngLat.lat;
                    let z = x;
                    x = x.toString();
                    x = x.substring(0, x.length - 3);
                    $('#latitud').val(x);
                }
                var Latitud = -89.61086650942;
                var longitud = 20.97689912377;
                Latitud = $("#L1").val();
                longitud = $("#L2").val();

                var map = new mapboxgl.Map({
                    container: 'mapid',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [longitud, Latitud],
                    zoom: 16
                });
                var marker = new mapboxgl.Marker({
                    draggable: true
                })

                    .setLngLat([longitud, Latitud])
                    .addTo(map);
                map.addControl(new mapboxgl.NavigationControl());
                marker.on('dragend', onDragEnd);
            
            //else {

            //    var mymap = L.map('mapid').setView([$('#latitud').val(), $('#longitud').val()], zoom = 16, 13, 16);



            //    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            //        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            //        maxZoom: 18,
            //        id: 'mapbox.streets',
            //        accessToken: 'pk.eyJ1IjoiYXNlc29ydXNpdGVjaCIsImEiOiJjanhtbzh6aW0wNXIwM2NvNjVweHlnd2JxIn0.YbBuq1IIm9cVDgg64NaxcQ'
            //    }).addTo(mymap);


            //    var marker = L.marker([$('#latitud').val(), $('#longitud').val()]).addTo(mymap);

            //}
        }
    </script>
    <script>
        setTimeout(function () {
            if ($('#latitud').val() == "" || $('#longitud').val() == "") {
                mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
                var coordinates = document.getElementById('coordinates');
                function onDragEnd() {
                    var lngLat = marker.getLngLat();
                    coordinates.style.display = 'block';
                    coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                    let x = lngLat.lng;
                    let y = x;
                    x = x.toString();
                    x = x.substring(0, x.length - 3);
                    $('#longitud').val(x);

                    x = lngLat.lat;
                    let z = x;
                    x = x.toString();
                    x = x.substring(0, x.length - 3);
                    $('#latitud').val(x);
                }
                var Latitud = -89.61086650942;
                var longitud = 20.97689912377;
                Latitud = $("#L1").val();
                longitud = $("#L2").val();

                $('#latitud').val(Latitud);
                $('#longitud').val(longitud);

                //alert($("#L1").val() + "  " + $("#L2").val());
                var map = new mapboxgl.Map({
                    container: 'mapid',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [longitud, Latitud],
                    zoom: 16
                });
                var marker = new mapboxgl.Marker({
                    draggable: true
                })

                    .setLngLat([longitud, Latitud])
                    .addTo(map);
                map.addControl(new mapboxgl.NavigationControl());
                marker.on('dragend', onDragEnd);
            }
            else {

                //var mymap = L.map('mapid').setView([$('#latitud').val(), $('#longitud').val(), zoom = 16], 13);



                //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                //    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                //    maxZoom: 18,
                //    id: 'mapbox.streets',
                //    accessToken: 'pk.eyJ1IjoiYXNlc29ydXNpdGVjaCIsImEiOiJjanhtbzh6aW0wNXIwM2NvNjVweHlnd2JxIn0.YbBuq1IIm9cVDgg64NaxcQ'
                //}).addTo(mymap);


                //var marker = L.marker([$('#latitud').val(), $('#longitud').val()]).addTo(mymap);

            }

        }, 2500)

    </script>
    <script  src ="https://maps.googleapis.com/maps/api/js?key=<%= ConfigurationManager.AppSettings["ApiKeyGoogle"]  %>&callback=initMap">
    </script>

</asp:Content>
