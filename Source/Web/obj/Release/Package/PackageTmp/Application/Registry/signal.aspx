﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="signal.aspx.cs" Inherits="Web.Application.Registry.signal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Señales</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        *{
            margin: 0;
            padding: 0;
        }

        .magnify {
            width: 300px;
            margin: 50px auto;
            position: relative;
        }

        /*Lets create the magnifying glass*/
        .large {
            width: 175px;
            height: 175px;
            position: absolute;
            border-radius: 100%;
            /*Multiple box shadows to achieve the glass effect*/
            box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85), 0 0 7px 7px rgba(0, 0, 0, 0.25), inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
            /*Lets load up the large image first*/
            background: url('../../Content/img/senalesdescripcion.jpg') no-repeat;
            /*hide the glass by default*/
            display: none;
        }
        td.strikeout {
            text-decoration: line-through;
        }
        /*To solve overlap bug at the edges during magnification*/
        .small {
            display: block;
        }

        .scroll2 {
            height: 20vw;
            overflow: auto;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="glyphicon glyphicon-bookmark"></i>
                Señas particulares
            </h1>
        </div>
    </div>-->
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
   
    <section id="widget-grid" class="">
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="row padding-bottom-10">
            <div class="menu-container" style="display: inline-block; float: right">
                <a style="display: none;font-size:smaller" class="btn btn-default btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
                <a style="display: none;font-size:smaller" class="btn btn-success txt-color-white btn-lg" id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a> 
                <a style="display: none;font-size:smaller" class="btn btn-info btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
                <a style="font-size:smaller" class="btn btn-primary txt-color-white btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
                <a style="display: none;font-size:smaller" class="btn btn-warning btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-purple txt-color-white btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-yellow txt-color-white btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-red txt-color-white btn-lg" id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
                <a style="margin-right:30px; font-size:smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa fa-paw"></i> Biométricos</a>
            </div>
        </div>
        <div class="scroll2" id="ScrollableContent">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-signal-2"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="glyphicon glyphicon-bookmark"></i></span>
                        <h2>Señas particulares</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body">
                            <p></p>
                            <div class="row" id="addsignal" style="display: none;">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                    <a href="javascript:void(0);" class="btn btn-md btn-default add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                </div>
                            </div>
                            <p></p>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th >Tipo</th>
                                        <th data-hide="phone,tablet">Descripción</th>
                                        <th data-hide="phone,tablet">Cantidad</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="row smart-form">
                            <footer>
                                <div class="row" style="display: inline-block; float: right">
                                    <%--<a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default" id="ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>--%>
                                    <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>                                                                         
                                </div>
                            </footer>
                        </div>  
                        <br />
                    </div>                
                </div>
            </article>
        </div>
    </section>

    <div class="modal fade" id="form-modal" data-backdrop="static" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-serie" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label style="color: dodgerblue" class="text">Tipo <a style="color: red">*</a></label>
                                    <label class="input">
                                        <select name="tipo" id="tipo" class="select2" style="width:100%;">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                                <section>
                                    <label class="text" style="color: dodgerblue">Lado <a style="color: red">*</a></label>
                                    <label class="input">
                                        <select name="lado" id="lado" class="select2" style="width:100%;">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                                <section>
                                    <label class="text" style="color: dodgerblue">Lugar <a style="color: red">*</a></label>
                                    <label class="input" style="display:none">
                                        <i class="icon-append fa fa-street-view"></i>
                                        <input type="text" name="region" id="region" runat="server" placeholder="Lugar" class="input-region alptext"  maxlength="256" onpaste="return false" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el lugar.</b>
                                    </label>
                                        <select name="Lugar" id="Lugar" class="select2" style="width:100%;">
                                            </select>
                                        <i></i>
                                </section>
                                <section>
                                    <label style="color: dodgerblue" class="text" >Vista <a style="color: red">*</a></label>
                                    <label class="input">
                                        <select name="vista" id="vista" class="select2" style="width:100%;">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                                <section>
                                    <label style="color: dodgerblue" class="text">Cantidad <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-sort-numeric-desc"></i>
                                        <input type="text" name="cantidad" id="cantidad" runat="server" placeholder="Cantidad" class="integer alptext" maxlength="9" onpaste="return false" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la cantidad.</b>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section>
                                    <section>
                                        <label style="color: dodgerblue" class="text">Descripción <a style="color: red">*</a></label>
                                        <label class="textarea">
                                            <i class="icon-append fa fa-comment"></i>
                                            <textarea rows="4" name="descripcion_" id="descripcion_" placeholder="Descripción" class="alphanumeric alptext" maxlength="256" runat="server"></textarea>
                                            <b class="tooltip tooltip-bottom-right">Ingrese descripción</b>
                                        </label>
                                    </section>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                            <input type="hidden" id="trackingid" runat="server" />
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar señal
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div4" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro de  seña <strong><span id="senaleliminar"></span></strong> será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>&nbsp;
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="historysen-modal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorialsen"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dt_basichistorysen" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
									    <th data-hiden="tablet,fablet,phone">Señal</th>
									    <th>Movimiento</th>
                                        <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="Idsenal" />
    <input type="hidden" id="Ingreso" runat="server" value=""/>
    <input type="hidden" id="Informacion_Personal" runat="server" value=""/>
    <input type="hidden" id="Antropometria" runat="server" value=""/>
    <input type="hidden" id="Adicciones" runat="server" value=""/>
    <input type="hidden" id="Estudios_Criminologicos" runat="server" value=""/>
    <input type="hidden" id="Expediente_Medico" runat="server" value=""/>
    <input type="hidden" id="Familiares" runat="server" value=""/>
    <input type="hidden" id="editable" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
      <script type="text/javascript">
          window.addEventListener("keydown", function (e) {
              if (e.ctrlKey && e.keyCode === 39) {
                  e.preventDefault();
                  document.getElementById("linkestudios").click();
              }
              else if (e.ctrlKey && e.keyCode === 37) {
                  e.preventDefault();
                  document.getElementById("linkantropometria").click();
                  if ($("#form-modal").is(":visible")) {
                      document.getElementById("btnsave").click();
                  }
              }
              else if (e.keyCode === 27) {
                  if ($("#form-modal").is(":visible")) {
                      $("#form-modal").modal("hide");
                  }
              }
              else if (e.ctrlKey && e.keyCode === 71) {
                  e.preventDefault();
                  if ($("#form-modal").is(":visible")) {
                      document.getElementsByClassName("btnsave")[0].click();
                  }
              }
          });

          $(document).ready(function () {

              $('body').on("keydown", function (e) {
                  if (e.ctrlKey && e.which === 71 && $("#form-modal").is(":visible")) {
                      var id = $("#btnsave").attr("data-id");
                      var tracking = $("#btnsave").attr("data-tracking");

                      if (validar()) {

                          datos = [
                              id = id,
                              tracking = tracking,
                              tipo = $("#tipo").val(),
                              lado = $("#lado").val(),
                              region = $("#ctl00_contenido_region").val(),
                              vista = $("#vista").val(),
                              cantidad = $("#ctl00_contenido_cantidad").val(),
                              descripcion = $("#ctl00_contenido_descripcion_").val(),
                              interno = $('#ctl00_contenido_trackingid').val(),
                              Lugar = $("#Lugar").val()
                          ];

                          Save(datos);

                      }
                  }
              });

              $("#ScrollableContent").css("height", "calc(100vh - 443px)");

              $("#datospersonales-modal").on("hidden.bs.modal", function () {
                  document.getElementsByClassName("detencion")[0].focus();
              });

              $("#form-modal").on("shown.bs.modal", function () {
                  $("#tipo").next().children().children().focus();
              });

              $("#form-modal").on("hidden.bs.modal", function () {
                  document.querySelector(".add").focus();
              });

              pageSetUp();

              const IMAGES = document.querySelectorAll(".thumb");
              let imageActive = false;
              let normalImage;
              let bigImage;

              //IMAGES.forEach(image => image.addEventListener("click", () => {
              //    handleImage(image);
              //}));

              //const handleImage = image => {
              //    if (!imageActive) openImage(image);
              //    else if (imageActive) closeImage(image);
              //};

              //const openImage = image => {
              //    imageActive = true;
              //    setImageSrc(image);
              //    image.parentNode.classList.add("is-active");
              //};

              //const closeImage = image => {
              //    imageActive = false;
              //    setImageSrc(image);
              //    image.parentNode.classList.remove("is-active");
              //};

              //const setImageSrc = image => image.setAttribute("src", getImageSrc(image));

              //const getImageSrc = image => {
              //    let normalImage = image.getAttribute("data-thumb");
              //    let bigImage = image.getAttribute("data-big");

              //    if (imageActive) return bigImage;
              //    else if (!imageActive) return normalImage;
              //};

              init();

              $("#ligarNuevoEvento").hide();
              if ($("#<%= editable.ClientID %>").val() == "0") {

                  $(".add").hide();
                  $("#ligarNuevoEvento").show();
              }

              var responsiveHelper_dt_basic_Sen = undefined;
              var responsiveHelper_dt_basic = undefined;
              var responsiveHelper_dt_basicAlias = undefined;
              var responsiveHelper_dt_basicNombre = undefined;
              var responsiveHelper_datatable_fixed_column = undefined;
              var responsiveHelper_datatable_col_reorder = undefined;
              var responsiveHelper_datatable_tabletools = undefined;
              var breackpoinghistsenDefinition = {
                  tablet: 1024,
                  phone: 480
              };
              var breakpointDefinition = {
                  tablet: 1024,
                  phone: 480
              };

              window.table = $('#dt_basic').dataTable({
                  "lengthMenu": [10, 20, 50, 100],
                  iDisplayLength: 10,
                  serverSide: true,
                  fixedColumns: true,
                  autoWidth: true,
                  "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                      "t" +
                      "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                  "autoWidth": true,
                  "oLanguage": {
                      "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                  },
                  "preDrawCallback": function () {
                      if (!responsiveHelper_dt_basic) {
                          responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                      }
                  },
                  "rowCallback": function (nRow) {
                      responsiveHelper_dt_basic.createExpandIcon(nRow);
                  },
                  "drawCallback": function (oSettings) {
                      responsiveHelper_dt_basic.respond();
                      $('#dt_basic').waitMe('hide');
                  },

                  "createdRow": function (row, data, index) {
                      if (!data["Activo"]) {
                          $('td', row).eq(1).addClass('strikeout');
                          $('td', row).eq(2).addClass('strikeout');
                          $('td', row).eq(3).addClass('strikeout');

                      }
                  },
                  ajax: {
                      type: "POST",
                      url: "signal.aspx/getSenal",
                      contentType: "application/json; charset=utf-8",
                      data: function (parametrosServerSide) {
                          $('#dt_basic').waitMe({
                              effect: 'bounce',
                              text: 'Cargando...',
                              bg: 'rgba(255,255,255,0.7)',
                              color: '#000',
                              sizeW: '',
                              sizeH: '',
                              source: ''
                          });

                          var trackingid = ($('#<%= trackingid.ClientID %>').val());

                          parametrosServerSide.emptytable = false;
                          parametrosServerSide.tracking = trackingid;
                          return JSON.stringify(parametrosServerSide);
                      }

                  },
                  columns: [

                      {
                          data: "TrackingId",
                          targets: 0,
                          orderable: false,
                          visible: false,
                          render: function (data, type, row, meta) {
                              return "";
                          }
                      },
                      null,
                      {
                          name: "Nombre",
                          data: "Nombre"
                      },
                      {
                          name: "Descripcion",
                          data: "Descripcion"
                      },
                      {
                          name: "Cantidad",
                          data: "Cantidad"
                      },
                      null
                  ],
                  columnDefs: [

                      {
                          data: "TrackingId",
                          targets: 0,
                          orderable: false,
                          visible: false,
                          render: function (data, type, row, meta) {
                              return "";
                          }
                      },
                      {
                          targets: 1,
                          orderable: false,
                          render: function (data, type, row, meta) {
                              return meta.row + meta.settings._iDisplayStart + 1;
                          }
                      },
                      {
                          targets: 5,
                          orderable: false,
                          render: function (data, type, row, meta) {
                              var txtestatus = "";
                              var icon = "";
                              var color = "";
                              var edit = "edit";
                              var editar = "";
                              var habilitar = "";

                              if (row.Activo) {
                                  txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                              }
                              else {
                                  txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                              }

                              var disabled = "";
                              if ($("#<%= editable.ClientID %>").val() == "0") disabled = "disabled";

                              if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' ' + disabled + '" href="javascript:void(0);" data-id="' + row.Id + '" data-tipo = "' + row.Tipo + '" data-lado = "' + row.Lado + '" data-region = "' + row.Region + '" data-cantidad = "' + row.Cantidad + '" data-vista = "' + row.Vista + '" data-descripcion = "' + row.Descripcion + '" data-tracking="' + row.TrackingId + '" data-Lugar="' + row.LugarId+'"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                              if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem ' + disabled + '" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-value = "' + row.Descripcion + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                              return editar + habilitar + '<a name="historysen" class="btn btn-default btn-circle historialsen" href="javascript:void(0);" data-value="' + row.Descripcion + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';


                          }
                      }
                  ]
              });

              window.emptytableHistory = true;
              window.tablehistory = $("#dt_basichistorysen").dataTable({
                  "lengthMenu": [10, 20, 50, 100],
                  iDisplayLength: 10,
                  serverSide: true,
                  fixedColumns: true,
                  order: [[3, 'asc']],
                  autoWidth: true,
                  "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                      "t" +
                      "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                  "autoWidth": true,
                  "oLanguage": {
                      "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                  },
                  "preDrawCallback": function () {
                      if (!responsiveHelper_dt_basic_Sen) {
                          responsiveHelper_dt_basic_Sen = new ResponsiveDatatablesHelper($("#dt_basichistorysen"), breackpoinghistsenDefinition);
                      }
                  },
                  "rowCallback": function (nRow) {
                      responsiveHelper_dt_basic_Sen.createExpandIcon(nRow);
                  },
                  "drawCallback": function (oSettings) {
                      responsiveHelper_dt_basic_Sen.respond();
                      $("#dt_basichistorysen").waitMe("hide");
                  },
                  ajax: {
                      type: "POST",
                      url: "signal.aspx/getsenallog",
                      contentType: "application/json; charset=utf-8",
                      data: function (parametrosserverside) {
                          $("#dt_basichistorysen").waitMe({
                              effect: 'bounce',
                              text: 'Cargando...',
                              bg: 'rgba(255,255,255,0.7)',
                              color: '#000',
                              sizeW: '',
                              sizeH: '',
                              source: ''
                          });
                          var centroid = $('#Idsenal').val();
                          parametrosserverside.centroid = centroid;
                          parametrosserverside.todoscancelados = false;
                          parametrosserverside.emptytable = emptytableHistory;
                          return JSON.stringify(parametrosserverside);
                      }
                  },
                  columns: [
                      {
                          name: "Descripcion",
                          data: "Descripcion",
                          ordertable: false

                      },
                      {
                          name: "Accion",
                          data: "Accion",
                          ordertable: false
                      },

                      {
                          name: "Creadopor",
                          data: "Creadopor",
                          ordertable: false
                      }
                      ,
                      {
                          name: "Fec_Movto",
                          data: "Fec_Movto",
                          ordertable: false
                      }
                  ],
              });


              $("body").on("click", ".historialsen", function () {
                  var id = $(this).attr("data-id");
                  $("#Idsenal").val(id);
                  var descripcion = $(this).attr("data-value");
                  $("#descripcionhistorialsen").text(" de la seña " + descripcion);

                  $("#historysen-modal").modal("show");
                  window.emptytableHistory = false;
                  window.tablehistory.api().ajax.reload();
              });
              function CargarLugar(set) {
                  $('#tipo').empty();
                  $.ajax({

                      type: "POST",
                      url: "signal.aspx/GetLugar",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      cache: false,
                      success: function (response) {
                          var Dropdown = $('#Lugar');

                          Dropdown.append(new Option("[Lugar]", 0));
                          $.each(response.d, function (index, item) {
                              Dropdown.append(new Option(item.Desc, item.Id));

                          });



                          Dropdown.val(set);
                          Dropdown.trigger("change.select2");


                      },
                      error: function () {
                          ShowError("¡Error!", "No fue posible cargar la lista de tipos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }
              function CargarTipo(set) {
                  $('#tipo').empty();
                  $.ajax({

                      type: "POST",
                      url: "signal.aspx/getTipo",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      cache: false,
                      success: function (response) {
                          var Dropdown = $('#tipo');

                          Dropdown.append(new Option("[Tipo]", 0));
                          $.each(response.d, function (index, item) {
                              Dropdown.append(new Option(item.Desc, item.Id));

                          });



                          Dropdown.val(set);
                          Dropdown.trigger("change.select2");


                      },
                      error: function () {
                          ShowError("¡Error!", "No fue posible cargar la lista de tipos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }

              function CargarLado(set) {
                  $('#lado').empty();
                  $.ajax({

                      type: "POST",
                      url: "signal.aspx/getLado",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      cache: false,
                      success: function (response) {
                          var Dropdown = $('#lado');

                          Dropdown.append(new Option("[Lado]", 0));
                          $.each(response.d, function (index, item) {
                              Dropdown.append(new Option(item.Desc, item.Id));

                          });
                          Dropdown.val(set);
                          Dropdown.trigger("change.select2");

                      },
                      error: function () {
                          ShowError("¡Error!", "No fue posible cargar la lista de lados. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }

              function CargarVista(set) {
                  $('#vista').empty();
                  $.ajax({

                      type: "POST",
                      url: "signal.aspx/getVista",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      cache: false,
                      success: function (response) {
                          var Dropdown = $('#vista');

                          Dropdown.append(new Option("[Vista]", 0));
                          $.each(response.d, function (index, item) {
                              Dropdown.append(new Option(item.Desc, item.Id));

                          });



                          Dropdown.val(set);
                          Dropdown.trigger("change.select2");


                      },
                      error: function () {
                          ShowError("¡Error!", "No fue posible cargar la lista de vista. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }
              $("body").on("click", ".blockitem", function () {
                  var itemnameblock = $(this).attr("data-value");
                  var verb = $(this).attr("style");
                  $("#itemnameblock").text(itemnameblock);
                  $("#verb").text(verb);
                  $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                  $("#blockitem-modal").modal("show");
              });

              $("#btncontinuar").unbind("click").on("click", function () {
                  var id = $(this).attr("data-id");
                  startLoading();
                  $.ajax({
                      url: "signal.aspx/blockitem",
                      type: 'POST',
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: id
                      }),
                      success: function (data) {
                          var data = JSON.parse(data.d);
                          if (data.exitoso) {
                              window.emtytable = false;
                              window.table.api().ajax.reload();
                              $("#blockitem-modal").modal("hide");
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                  data.mensaje + "</div>");
                              setTimeout(hideMessage, hideTime);

                              ShowSuccess("¡Bien hecho!", data.mensaje);
                          }
                          else {
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                  "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                              setTimeout(hideMessage, hideTime);

                              ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }
                          $('#main').waitMe('hide');
                      }

                  });
                  window.emptytable = true;
                  window.table.api().ajax.reload();
              });
              $("body").on("click", ".search", function () {
                  window.emptytable = false;
                  window.table.api().ajax.reload();
              });

              $("body").on("click", ".clear", function () {
                  $('#tipo').val("0");
                  $('#lado').val("0");
                  $('#ctl00_contenido_region').val("");
                  $('#vista').val("0");
                  $('#ctl00_contenido_cantidad').val("");
                  $('#ctl00_contenido_descripcion_').val("");

              });



              $("body").on("click", ".add", function () {
                  $("#ctl00_contenido_lblMessage").html("");
                  $("#ctl00_contenido_region").val("");
                  $("#ctl00_contenido_cantidad").val("");
                  $("#ctl00_contenido_descripcion_").val("");
                  CargarLado(0);
                  CargarTipo(0);
                  CargarVista(0);
                  CargarLugar(0);
                  limpiar();
                  $("#btnsave").attr("data-id", "");
                  $("#btnsave").attr("data-tracking", "");
                  $("#modal-title").empty();
                  $("#form-modal-title").html("<i class='fa fa-pencil'+></i> Agregar Registro");
                  $("#form-modal").modal("show");



              });

              $("body").on("click", ".edit", function () {
                  limpiar();
                  $("#ctl00_contenido_lblMessage").html("");
                  var id = $(this).attr("data-id");
                  var tracking = $(this).attr("data-tracking");
                  var tipo = $(this).attr("data-tipo");
                  var lado = $(this).attr("data-lado");
                  var vista = $(this).attr("data-vista");
                  var lugar = $(this).attr("data-Lugar");
                  $("#btnsave").attr("data-id", id);
                  $("#btnsave").attr("data-tracking", tracking);
                  $("#ctl00_contenido_region").val($(this).attr("data-region"));
                  $("#ctl00_contenido_cantidad").val($(this).attr("data-cantidad"));
                  $("#ctl00_contenido_descripcion_").val($(this).attr("data-descripcion"));
                  $("#modal-title").empty();
                  $("#form-modal-title").html("<i class='fa fa-pencil'+></i> Editar Registro");
                  CargarVista(vista);
                  CargarLado(lado);
                  CargarTipo(tipo);
                  CargarLugar(lugar);

                  $("#form-modal").modal("show");
              });

              $("body").on("click", ".delete", function () {
                  var id = $(this).attr("data-id");
                  var nombre = $(this).attr("data-value");
                  $("#senaleliminar").text(nombre);
                  $("#btndelete").attr("data-id", id);
                  $("#delete-modal").modal("show");
              });

              $("body").on("click", ".save", function () {
                  var id = $("#btnsave").attr("data-id");
                  var tracking = $("#btnsave").attr("data-tracking");

                  if (validar()) {

                      datos = [
                          id = id,
                          tracking = tracking,
                          tipo = $("#tipo").val(),
                          lado = $("#lado").val(),
                          region = $("#ctl00_contenido_region").val(),
                          vista = $("#vista").val(),
                          cantidad = $("#ctl00_contenido_cantidad").val(),
                          descripcion = $("#ctl00_contenido_descripcion_").val(),
                          interno = $('#ctl00_contenido_trackingid').val(),
                          Lugar = $("#Lugar").val()
                      ];


                      Save(datos);

                  }


              });

              function limpiar() {

                  $('#tipo').parent().removeClass('state-success');
                  $('#tipo').parent().removeClass("state-error");
                  $('#Lugar').parent().removeClass('state-success');
                  $('#Lugar').parent().removeClass("state-error");
                  $('#lado').parent().removeClass('state-success');
                  $('#lado').parent().removeClass("state-error");
                  $('#ctl00_contenido_region').parent().removeClass('state-success');
                  $('#ctl00_contenido_region').parent().removeClass("state-error");
                  $('#vista').parent().removeClass('state-success');
                  $('#vista').parent().removeClass("state-error");
                  $('#ctl00_contenido_cantidad').parent().removeClass('state-success');
                  $('#ctl00_contenido_cantidad').parent().removeClass("state-error");
                  $('#ctl00_contenido_descripcion_').parent().removeClass('state-success');
                  $('#ctl00_contenido_descripcion_').parent().removeClass("state-error");

              }

              function validar() {
                  var esvalido = true;

                  //if ($("#ctl00_contenido_region").val().split(" ").join("") == "") {
                  //    ShowError("Lugar", "El lugar es obligatorio.");
                  //    $('#ctl00_contenido_region').parent().removeClass('state-success').addClass("state-error");
                  //    $('#ctl00_contenido_region').removeClass('valid');
                  //    esvalido = false;
                  //}
                  //else {
                  //    $('#ctl00_contenido_region').parent().removeClass("state-error").addClass('state-success');
                  //    $('#ctl00_contenido_region').addClass('valid');
                  //}


                  if ($("#ctl00_contenido_cantidad").val().split(" ").join("") == "" || $("#ctl00_contenido_cantidad").val() == "0") {
                      ShowError("Cantidad", "La cantidad es obligatoria.");
                      $('#ctl00_contenido_cantidad').parent().removeClass('state-success').addClass("state-error");
                      $('#ctl00_contenido_cantidad').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      if (isNaN($("#ctl00_contenido_cantidad").val())) {
                          ShowError("Cantidad", "La cantidad es numérica.");
                          $('#ctl00_contenido_cantidad').parent().removeClass('state-success').addClass("state-error");
                          $('#ctl00_contenido_cantidad').removeClass('valid');
                          esvalido = false;
                      }
                      else {
                          $('#ctl00_contenido_cantidad').parent().removeClass("state-error").addClass('state-success');
                          $('#ctl00_contenido_cantidad').addClass('valid');
                      }
                  }

                  if ($("#ctl00_contenido_descripcion_").val().split(" ").join("") == "") {
                      ShowError("Descripción", "La descripción es obligatoria.");
                      $('#ctl00_contenido_descripcion_').parent().removeClass('state-success').addClass("state-error");
                      $('#ctl00_contenido_descripcion_').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#ctl00_contenido_descripcion_').parent().removeClass("state-error").addClass('state-success');
                      $('#ctl00_contenido_descripcion_').addClass('valid');
                  }

                  if ($("#Lugar").val() == null || $("#Lugar").val() == "0") {
                      ShowError("Lugar", "El Lugar es obligatorio.");
                      $('#Lugar').parent().removeClass('state-success').addClass("state-error");
                      $('#Lugar').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#Lugar').parent().removeClass("state-error").addClass('state-success');
                      $('#Lugar').addClass('valid');
                  }

                  if ($("#tipo").val() == null || $("#tipo").val() == "0") {
                      ShowError("Tipo", "El tipo es obligatorio.");
                      $('#tipo').parent().removeClass('state-success').addClass("state-error");
                      $('#tipo').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#tipo').parent().removeClass("state-error").addClass('state-success');
                      $('#tipo').addClass('valid');
                  }

                  if ($("#lado").val() == null || $("#lado").val() == "0") {
                      ShowError("Lado", "El lado es obligatorio.");
                      $('#lado').parent().removeClass('state-success').addClass("state-error");
                      $('#lado').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#lado').parent().removeClass("state-error").addClass('state-success');
                      $('#lado').addClass('valid');
                  }

                  if ($("#vista").val() == null || $("#vista").val() == "0") {
                      ShowError("Vista", "La vista es obligatoria.");
                      $('#vista').parent().removeClass('state-success').addClass("state-error");
                      $('#vista').removeClass('valid');
                      esvalido = false;
                  }
                  else {
                      $('#vista').parent().removeClass("state-error").addClass('state-success');
                      $('#vista').addClass('valid');
                  }


                  return esvalido;
              }




              function Save(datos) {
                  startLoading();
                  $.ajax({
                      type: "POST",
                      url: "signal.aspx/save",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          datos: datos
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              window.emptytable = false;
                              window.table.api().ajax.reload();
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                  "La seña particular  se " + resultado.mensaje + " correctamente.", "<br /></div>");
                              setTimeout(hideMessage, hideTime);

                              $("#form-modal").modal("hide");
                              ShowSuccess("¡Bien hecho!", "La seña particular se " + resultado.mensaje + " correctamente.");
                          }
                          else {
                              ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                          }
                          $('#main').waitMe('hide');
                      },
                      error: function () {
                          ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                          $('#main').waitMe('hide');
                      }
                  });
              }

              function CargarDatos(trackingid) {
                  startLoading();
                  $.ajax({
                      type: "POST",
                      url: "signal.aspx/getdatos",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              $('#nombreInterno').text(resultado.obj.Nombre);
                              $('#expedienteInterno').text(resultado.obj.Expediente);
                              $('#centroInterno').text(resultado.obj.Centro);
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                              if (imagenAvatar != "")
                                  $('#avatar').attr("src", imagenAvatar);

                              $('#edadInterno').text(resultado.obj.Edad);
                              $('#sexoInterno').text(resultado.obj.Sexo);

                              $('#municipioInterno').text(resultado.obj.municipioNombre);
                              $('#domicilioInterno').text(resultado.obj.domiclio);
                              $('#coloniaInterno').text(resultado.obj.coloniaNombre);

                              $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                              $("#linkvistas").attr("href", "huella.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                              $("#linkestudios").attr("href", "record.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());

                              $("#linkseñas").attr("href", "signal.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                              $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                              $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                              $("#linkingreso").attr("href", "entry.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                              $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                              $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                              $('#<%= trackingid.ClientID %>').val(resultado.obj.TrackingId);
                          } else {
                              ShowError("¡Error!", "No fue posible cargar la información de  detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("¡Error!", "No fue posible cargar la información de  detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }

              function ResolveUrl(url) {
                  var baseUrl = "<%= ResolveUrl("~/") %>";
                  if (url.indexOf("~/") == 0) {
                      url = baseUrl + url.substring(2);
                  }
                  return url;
              }


              $("#btndelete").unbind("click").on("click", function () {
                  var id = $(this).attr("data-id");
                  startLoading();
                  $.ajax({
                      url: "signal.aspx/delete",
                      type: 'POST',
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          id: id
                      }),
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              window.emptytable = false;
                              window.table.api().ajax.reload();
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                  "La señal fue eliminada del catálogo.", "</div>");
                              setTimeout(hideMessage, hideTime);

                              ShowSuccess("¡Bien hecho!", "La señal fue eliminada correctamente.");
                          }
                          else {
                              $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                  "Algo salió mal: " + resultado.mensaje + "</div>");
                              setTimeout(hideMessage, hideTime);

                          }
                          $("#delete-modal").modal("hide");
                          $('#main').waitMe('hide');
                      }
                  });
              });


              function init() {

                  var param = RequestQueryString("tracking");
                  $('#<%= trackingid.ClientID %>').val(param);
                  if (param != undefined) {
                      CargarDatos(param);
                  }

                  if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                      $("#addsignal").show();
                  }
                  else {
                      $("textarea,input, select").each(function (index, element) { $(this).attr('disabled', true); });
                  }

                  if ($("#ctl00_contenido_Ingreso").val() == "true") {
                      $("#linkingreso").show();
                  }
                  if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                      $("#linkinformacion").show();
                  }
                  if ($("#ctl00_contenido_Antropometria").val() == "true") {
                      $("#linkantropometria").show();
                  }
                  if ($("#ctl00_contenido_Familiares").val() == "true") {
                      $("#linkfamiliares").show();
                  }
                  if ($("#ctl00_contenido_Estudios_Criminologicos").val() == "true") {
                      $("#linkestudios").show();
                  }
                  if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                      $("#linkexpediente").show();
                  }
                  if ($("#ctl00_contenido_Adicciones").val() == "true") {
                      $("#linkadicciones").show();
                  }
                  $("#linkseñas").show();
                  $("#linkInfoDetencion").show();
              }

              var native_width = 0;
              var native_height = 0;

              //Now the mousemove function
              $(".magnify").mousemove(function (e) {
                  //When the user hovers on the image, the script will first calculate
                  //the native dimensions if they don't exist. Only after the native dimensions
                  //are available, the script will show the zoomed version.
                  if (!native_width && !native_height) {
                      //This will create a new image object with the same image as that in .small
                      //We cannot directly get the dimensions from .small because of the 
                      //width specified to 200px in the html. To get the actual dimensions we have
                      //created this image object.
                      var image_object = new Image();
                      image_object.src = $(".small").attr("src");

                      //This code is wrapped in the .load function which is important.
                      //width and height of the object would return 0 if accessed before 
                      //the image gets loaded.
                      native_width = image_object.width;

                      native_height = image_object.height;
                  }
                  else {
                      //x/y coordinates of the mouse
                      //This is the position of .magnify with respect to the document.
                      var magnify_offset = $(this).offset();
                      //We will deduct the positions of .magnify from the mouse positions with
                      //respect to the document to get the mouse positions with respect to the 
                      //container(.magnify)
                      var mx = e.pageX - magnify_offset.left;
                      var my = e.pageY - magnify_offset.top;

                      //Finally the code to fade out the glass if the mouse is outside the container
                      if (mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0) {
                          $(".large").fadeIn(100);
                      }
                      else {
                          $(".large").fadeOut(100);
                      }
                      if ($(".large").is(":visible")) {
                          //The background position of .large will be changed according to the position
                          //of the mouse over the .small image. So we will get the ratio of the pixel
                          //under the mouse pointer with respect to the image and use that to position the 
                          //large image inside the magnifying glass
                          var rx = Math.round(mx / $(".small").width() * native_width - $(".large").width()) * -1.1;
                          var ry = Math.round(my / $(".small").height() * native_height - $(".large").height()) * -1.1;
                          var bgp = rx + "px " + ry + "px";

                          //Time to move the magnifying glass with the mouse
                          var px = mx - $(".large").width() / 2;
                          var py = my - $(".large").height() / 2;
                          //Now the glass moves with the mouse
                          //The logic is to deduct half of the glass's width and height from the 
                          //mouse coordinates to place it with its center at the mouse coordinates

                          //If you hover on the image now, you should see the magnifying glass in action
                          $(".large").css({ left: px, top: py, backgroundPosition: bgp });
                      }
                  }
              });

              $("#ctl00_contenido_cantidad").keyup(function () {
                  this.value = (this.value + '').replace(/[^0-9]/g, '');
              });

              $("body").on("click", ".detencion", function () {
                  $("#datospersonales-modal").modal("show");
                  var param = RequestQueryString("tracking");
                  CargarDatosModal(param);
              });

              function CargarDatosModal(trackingid) {
                  startLoading();
                  $.ajax({
                      type: "POST",
                      url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {

                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              $("#fechaInfo").val(resultado.obj.FechaDetencion);
                              $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                              $("#eventoInfo").val(resultado.obj.EventoNombre);
                              $("#folioInfo").val(resultado.obj.Folio);
                              $("#unidadInfo").val(resultado.obj.UnidadNombre);
                              $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                              $("#descripcionInfo").val(resultado.obj.Motivo);
                              $("#detalleInfo").val(resultado.obj.Descripcion);
                              $("#lugarInfo").val(resultado.obj.Lugar);
                              $("#paisInfo").val(resultado.obj.PaisNombre);
                              $("#estadoInfo").val(resultado.obj.EstadoNombre);
                              $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                              $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                              $("#cpInfo").val(resultado.obj.CodigoPostal);
                              $("#estaturaInfo").val(resultado.obj.Estatura);
                              $("#pesoInfo").val(resultado.obj.Peso);
                              $("#institucionInfo").val(resultado.obj.Centro);
                              $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                              $("#nombreInfo").val(resultado.obj.Nombre);
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                              if (imagenAvatar != "")
                                  $('#imgInfo').attr("src", imagenAvatar);
                          }
                          else {
                              ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
              }

              var hideTime = 5000;
              function hideMessage() {
                  $("#ctl00_contenido_lblMessage").html("");
              }

              $("#showInfoDetenido").click(function () {
                  $("#hideInfoDetenido").css('display', 'block');
                  $("#showInfoDetenido").css('display', 'none');

                  $("#SectionInfoDetenido").show();
                  $("#ScrollableContent").css("height", "calc(100vh - 443px)");
              })
              $("#hideInfoDetenido").click(function () {
                  $("#hideInfoDetenido").css('display', 'none');
                  $("#showInfoDetenido").css('display', 'block');

                  $("#SectionInfoDetenido").hide();
                  $("#ScrollableContent").css("height", "calc(100vh - 216px)");
              });

          });

      </script>
</asp:Content>
