﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="personalinformation.aspx.cs" Inherits="Web.Application.Registry.personalinformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Información personal</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' />
    <style>        

        td.strikeout {
            text-decoration: line-through;
        }               

        #mapid {
            height: 20vw;       
            width: 100%;
        }
        #content {
            height: 600px;
        }
        div.scroll {
            height: 590px;
            overflow: auto;
            overflow-x: hidden;
        }
        .coordinates {
            background: rgba(0,0,0,0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding:5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }
        .scroll2 {
            height: 25vw;
            overflow: auto;
        }
        @media only screen and (max-width: 1245px)
        {
            .scroll2{
                height: 15vw;
            }
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
        .bootstrapWizard1 {
            display: block;
            list-style: none;
            padding: 0;
            position: relative;
            width: 100%;
        }

        .bootstrapWizard1 a:hover, .bootstrapWizard a:active, .bootstrapWizard a:focus {
            text-decoration: none;
        }

        .bootstrapWizard1 li {
            display: block;
            float: left;
            width: 9%;
            text-align: center;
            padding-left: 0;
        }

        .bootstrapWizard1 li:before {
            border-top: 3px solid #55606E;
            content: "";
            display: block;
            font-size: 0;
            overflow: hidden;
            position: relative;
            top: 11px;
            right: 1px;
            width: 100%;
            z-index: 1;
        }

        .bootstrapWizard1 li:first-child:before {
            left: 50%;
            max-width: 50%;
        }

        .bootstrapWizard1 li:last-child:before {
            max-width: 50%;
            width: 50%;
        }

        .bootstrapWizard1 li.complete .step {
            background: #0aa66e;
            padding: 1px 6px;
            border: 3px solid #55606E;
        }

        .bootstrapWizard1 li .step i {
            font-size: 10px;
            font-weight: 400;
            position: relative;
            top: -1.5px;
        }

        .bootstrapWizard1 li .step {
            background: #B2B5B9;
            color: #fff;
            display: inline;
            font-size: 15px;
            font-weight: 700;
            line-height: 12px;
            padding: 7px 13px;
            border: 3px solid transparent;
            border-radius: 50%;
            line-height: normal;
            position: relative;
            text-align: center;
            z-index: 2;
            transition: all .1s linear 0s;
        }

        .bootstrapWizard1 li.active .step, .bootstrapWizard li.active.complete .step {
            background: #0091d9;
            color: #fff;
            font-weight: 700;
            padding: 7px 13px;
            font-size: 15px;
            border-radius: 50%;
            border: 3px solid #55606E;
        }

        .bootstrapWizard1 li.complete .title, .bootstrapWizard li.active .title {
            color: #2B3D53;
        }

        .bootstrapWizard1 li .title {
            color: #bfbfbf;
            display: block;
            font-size: 13px;
            line-height: 15px;
            max-width: 100%;
            position: relative;
            table-layout: fixed;
            text-align: center;
            top: 20px;
            word-wrap: break-word;
            z-index: 104;
        }

        .wizard-actions {
            display: block;
            list-style: none;
            padding: 0;
            position: relative;
            width: 100%;
        }

        .wizard-actions li {
            display: inline;
        }

        .tab-content.transparent {
            background-color: transparent;
        }

        .img-thumbnail {
            height: 300px;
        }
        td.strikeout {
            text-decoration: line-through;
        }
        .scroll2 {
            height: 20vw;
            overflow: auto;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }

        .btnMap {
            float: right;
            height: 31px;
            margin: 10px 15px 0 5px;
            padding: 0 22px;
            font: 300 15px/29px 'Open Sans',Helvetica,Arial,sans-serif;
            cursor: pointer;
        }
    </style>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <!--
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-list-alt"></i>
                    Información personal
                </h1>
            </div>
        </div>-->
        <section id="widget-grid" class="">
            <div class="row" style="margin:0;">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                            <h2>Información del detenido - Datos generales</h2>
                            <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                            <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body" id="SectionInfoDetenido">

                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <br />
                                                    <img width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td colspan="2">
                                                    <br />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <table class="table-responsive">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        <table class="table-responsive">
                                                            <tbody>
                                                                <tr>
                                                                    <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                    <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>
                                                                        <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                    <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                    <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                        <br />
                                                                    </span></th>
                                                                    <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                        <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                        <table class="table-responsive">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        <table class="table-responsive">
                                                            <tbody>
                                                                <tr>
                                                                    <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                    <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                    <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                    <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                    <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                        <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 20px;">
                                        <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="row padding-bottom-10">
                <div class="menu-container" style="display: inline-block; float: right">
                    <a style="display: none;font-size:smaller" class="btn btn-default btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
                    <a style="display: none;font-size:smaller" class="btn btn-success txt-color-white btn-lg" id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a> 
                    <a style="display: none;font-size:smaller" class="btn btn-info btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
                    <a style="font-size:smaller" class="btn btn-primary txt-color-white btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
                    <a style="display: none;font-size:smaller" class="btn btn-warning btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
                    <a style="display: none;font-size:smaller" class="btn bg-color-purple txt-color-white btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
                    <a style="display: none;font-size:smaller" class="btn bg-color-yellow txt-color-white btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
                    <a style="display: none;font-size:smaller" class="btn bg-color-red txt-color-white btn-lg" id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
                    <a style="margin-right:30px; font-size:smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa fa-paw"></i> Biométricos</a>
                </div>
            </div>
            <div class="scroll2" id="ScrollableContent">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-personal-2" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                            <h2>Información personal </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div class="row">
                                <p></p>
                                <p></p>
                                <div id="data-1" class="col-sm-12">
                                    <div class="form-bootstrapWizard">
                                        <ul id="lstTabs" class="bootstrapWizard form-wizard" style="z-index:0">
                                            <li class="active" data-target="#step1">
                                                <a id="linkTab1" href="#tab1" data-toggle="tab">
                                                    <span class="step">1</span>
                                                    <span class="title">
                                                        <h5>General</h5>
                                                    </span>
                                                </a>
                                            </li>
                                            <li data-target="#step4">
                                                <a id="linkTab2" href="#tab4" data-toggle="tab">
                                                    <span class="step">2</span>
                                                    <span class="title">
                                                        <h5>Domicilio</h5>
                                                    </span>
                                                </a>
                                            </li>
                                            <li data-target="#step2">
                                                <a id="linkTab3" href="#tab2" data-toggle="tab">
                                                    <span class="step">3</span>
                                                    <span class="title">
                                                        <h5>Alias</h5>
                                                    </span>
                                                </a>
                                            </li>
                                            <%--
                                    <li data-target="#step3">
                                        <a href="#tab3" data-toggle="tab">
                                            <span class="step">3</span>
                                            <span class="title">Otros nombres </span>
                                        </a>
                                    </li>--%>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab1">
                                            <div class="row">
                                                <legend style="display: none;">Datos de procedencia</legend>
                                                <div class="col-sm-12" style="display: none;">
                                                    <div class="form-group">
                                                        <br />
                                                        <div class="row" id="addprocedencia" style="display: none;">
                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                                                <a class="btn btn-md btn-default add" id="a1"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th data-class="expand">#</th>
                                                                    <th>Remesa</th>
                                                                    <th>Expediente</th>
                                                                    <th>Estado de procedencia</th>
                                                                    <th data-hide="phone,tablet">Centro de procendencia(s)</th>
                                                                    <th data-hide="phone,tablet">Acciones</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <input id="other" type="hidden" name="other" />
                                                </div>
                                            </div>
                                            <legend>Datos generales</legend>
                                            <div id="smart-form-register" class="smart-form">
                                                <fieldset style="padding-top: 0px;">
                                                    <legend class="hide"></legend>
                                                    <div class="row">
                                                        <section class="col col-3">
                                                            <label class="label">Fecha de nacimiento <a style="color: red">*</a></label>
                                                            <label class="input">
                                                                <label class='input-group date' >
                                                                    <input type="text" name="fecha" id="fecha" placeholder="Fecha de nacimiento"  runat="server" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingrese la fecha de nacimiento.</b>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                                </label>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Nacionalidad <a style="color: red">*</a></label>
                                                            <select name="centro" id="nacionalidad" runat="server" class="select2" style="width:100%;">
                                                            </select>
                                                            <i></i>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Escolaridad <a style="color: red">*</a></label>
                                                            <select name="centro" id="escolaridad" runat="server" class="select2" style="width:100%;"></select>
                                                            <i></i>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Ocupación <a style="color: red">*</a></label>
                                                            <select name="centro" id="ocupacion" runat="server" class="select2" style="width:100%;"></select>
                                                            <i></i>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">RFC</label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-user"></i>
                                                                <input type="text" name="rfc" id="rfc" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" runat="server" placeholder="RFC" maxlength="13" class="alphanumeric alptext" />
                                                                <b class="tooltip tooltip-bottom-right">Ingresa el RFC.</b>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">CURP</label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-user"></i>
                                                                <input type="text" name="curp" id="curp" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" runat="server" placeholder="CURP" maxlength="18" class="alphanumeric alptext" />
                                                                <b class="tooltip tooltip-bottom-right">Ingresa la CURP.</b>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Religión</label>
                                                            <select name="centro" id="religion" runat="server" class="select2" style="width:100%;"></select>
                                                            <i></i>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Estado civil <a style="color: red">*</a></label>
                                                            <select name="centro" id="civil" runat="server" class="select2" style="width:100%;"></select>
                                                            <i></i>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Sexo <a style="color: red">*</a></label>
                                                            <select name="centro" id="sexo" runat="server" class="select2" style="width:100%;"></select>
                                                            <i></i>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Salario semanal</label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-hashtag"></i>
                                                                <input title="" type ="text" name="salarioSemanal" pattern="\d*(\.\d*)?$" maxlength="13" id="salarioSemanal" runat="server"/>
                                                                <b class="tooltip tooltip-bottom-right" >Ingrese el salario semanal.</b>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Etnia</label>
                                                            <select name="centro" id="etinia" runat="server" class="select2" style="width:100%;"></select>
                                                            <i></i>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">Lengua nativa </label>
                                                            <select name="centro" id="lenguanativa" runat="server" class="select2" style="width:100%;"></select>
                                                            <i></i>
                                                        </section>
                                                        <section class="col col-2"></section>
                                                        <section class="col col-2" style="display: none">
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="mental" id="mental" runat="server" />
                                                                <i></i>
                                                                Estado Mental
                                                            </label>
                                                        </section>
                                                        <section class="col col-2"></section>
                                                        <section class="col col-3" style="display: none">
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="inimputable" id="inimputable" runat="server" />
                                                                <i></i>
                                                                Inimputable
                                                            </label>
                                                        </section>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="row smart-form">
                                                <footer>
                                                    <div class="row" style="display: inline-block; float: right">
                                                        <!--<a class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                                        <%--<a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>--%>
                                                        <a style="float: none; display: none;" href="javascript:void(0);" class="btn btn-default saveGeneral" id="SaveGnral" title="Guardar"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                                        <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                                        <input type="hidden" id="trackingid" runat="server" />
                                                        <input type="hidden" id="trackingGeneral" runat="server" />
                                                        <input type="hidden" id="idGeneral" runat="server" />
                                                    </div>
                                                </footer>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab2">
                                            <div class="row"></div>
                                            <legend>Alias / Sobrenombre</legend>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="row" id="addalias" style="display: none;">
                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                                                <a href="javascript:void(0);" class="btn btn-md btn-default addAlias" id="addAlias"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <table id="dt_basicAlias" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th data-class="expand">#</th>
                                                                    <th>Alias</th>
                                                                    <th data-hide="phone,tablet">Acciones</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%--
                                <div class="tab-pane" id="tab3">
                                    <br />
                                    <br />
                                    <legend>Otros nombres</legend>
                                    <div class="row">
                                        <div class="col-sm-12">

                                            <div class="form-group">

                                                <br />
                                                <div class="row" id="addnombre" style="display: none;">
                                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                                        <a class="btn btn-md btn-default addNombre" id="a2"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                                    </div>
                                                </div>
                                                <br />
                                                <table id="dt_basicNombre" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th data-class="expand">#</th>
                                                            <th >Nombre(s)</th>
                                                            <th>Apellido Paterno</th>
                                                            <th>Apellido Materno</th>
                                                            <th data-hide="phone,tablet">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                        <div class="tab-pane" id="tab4">
                                                <div class="row"></div>
                                                <legend>Domicilio actual</legend>
                                                <div class="col-sm-12" style="padding: 0px;">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div id="Div1" class="smart-form">
                                                                <fieldset style="padding-top: 0px;">
                                                                    <div class="row" id="domicilio_actual_form">
                                                                        <section class="col col-3">
                                                                            <label class="label">País <a style="color: red">*</a></label>
                                                                            <select name="centro" id="paisdomicilio" runat="server" class="select2" style="width: 100%;" tabindex="-1"></select>
                                                                        </section>
                                                                        <section class="col col-3" id="sectionestado">
                                                                            <label class="label">Estado <a style="color: red">*</a></label>
                                                                            <select name="centro" id="estadodomicilio" runat="server" class="select2" style="width: 100%;"></select>
                                                                        </section>
                                                                        <section class="col col-3" id="sectionmunicipio">
                                                                            <label class="label">Municipio <a style="color: red">*</a></label>
                                                                            <select name="centro" id="municipiodomicilio" runat="server" class="select2" style="width: 100%;"></select>
                                                                        </section>
                                                                        <section class="col col-3" id="sectioncoloniaDomicilio">
                                                                            <label class="label">Colonia <a style="color: red">*</a></label>
                                                                            <select name="centro" id="coloniaSelect" runat="server" class="select2" style="width: 100%;"></select>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">Calle <a style="color: red">*</a></label>
                                                                            <label class="input">
                                                                                <i class="icon-append fa fa-street-view"></i>
                                                                                <input type="text" name="calle" id="calledomicilio" runat="server" placeholder="Calle" maxlength="250" class="alphanumeric alptext" />
                                                                                <b class="tooltip tooltip-bottom-right">Ingresa la calle.</b>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <section class="col col-6" style="padding-left: 0;">
                                                                                <label class="label">Número <a style="color: red">*</a></label>
                                                                                <label class="input">
                                                                                    <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                                    <input type="text" name="numero" id="numerodomicilio" runat="server" placeholder="Número" class="alphanumeric alptext" maxlength="15" />
                                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el número.</b>
                                                                                </label>
                                                                            </section>
                                                                            <section class="col col-6" style="padding-right: 0;" id="sectioncpdomicilio">
                                                                                <label class="label">C.P. <a style="color: red">*</a></label>
                                                                                <label class="input">
                                                                                    <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                                    <input type="text" name="cp" id="cpdomicilio" runat="server" placeholder="C.P" maxlength="5" class="integer" disabled="disabled" />
                                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el C.P.</b>
                                                                                </label>
                                                                            </section>
                                                                        </section>
                                                                        <section class="col col-2">
                                                                            <label class="label">Teléfono <!-- Note="Se hace opcional la captura del telefono del domicilio " <a style="color: red">**</a> --></label>
                                                                            <label class="input">
                                                                                <i class="icon-append fa fa-phone"></i>
                                                                                <input type="tel" name="telefono" id="telefonodomicilio" runat="server" placeholder="Teléfono" data-mask="(999) 999-9999" />
                                                                                <b class="tooltip tooltip-bottom-right">Ingresa el teléfono .</b>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-4" id="sectionlocalidad">
                                                                            <label class="label">Localidad <a style="color: red">*</a></label>
                                                                            <label class="input">
                                                                                <i class="icon-append fa fa-street-view"></i>
                                                                                <input type="text" name="colonialugar" id="localidad" hidden="hidden" runat="server" placeholder="Localidad" maxlength="250" class="alphanumeric alptext" />
                                                                                <b class="tooltip tooltip-bottom-right">Ingresa localidad.</b>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-2">
                                                                            <label class="label">Latitud</label>
                                                                            <label class="input">
                                                                                <i class="icon-append fa fa-map-marker"></i>
                                                                                <%--<input title="" type ="text"  pattern="^\-{0,1}\d*(\.\d*)?$" maxlength="18" name="latitudd" id="latitudd" runat="server"/>--%>
                                                                                <input title="" type ="text"  pattern="^\-{0,1}\d*(\.\d*)?$" maxlength="18" name="latitudd" id="latitudd" />
                                                                                <b class="tooltip tooltip-bottom-right">Ingresa la latitud.</b>
                                                                            </label>
                                                                            <%--<div class="note note-error" style="color: red">Obligatorio</div>--%>
                                                                        </section>
                                                                        <section class="col col-2">
                                                                            <label class="label">Longitud</label>
                                                                            <label class="input">
                                                                                <i class="icon-append fa fa-map-marker"></i>
                                                                                <input title="" type ="text"  pattern="^\-{0,1}\d*(\.\d*)?$" maxlength="18" name="longitudd" id="longitudd"  />
                                                                                <b class="tooltip tooltip-bottom-right">Ingresa la longitud.</b>
                                                                            </label>
                                                                            <%--<div class="note note-error" style="color: red">Obligatorio</div>--%>
                                                                        </section>
                                                                        <%--<section class="col col-5">
                                                                    <label class="label">Colonia</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-street-view"></i>
                                                                        <input type="text" name="colonia" id="coloniadomicilio" runat="server" placeholder="Colonia" maxlength="250" class="alphanumeric" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa la colonia.</b>
                                                                    </label>
                                                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                                                </section>--%>

                                                                        <a href="javascript:void(0);" id="showMap" class="btn btn-default btnMap" title="Volver al listado"><i class="fa fa-map-marker"></i>&nbsp;Ver Mapa </a>                                                                   
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <legend>Domicilio de nacimiento</legend>
                                                            <div id="Div3" class="smart-form">
                                                                <fieldset style="padding-top: 0px;">
                                                                    <div class="row">
                                                                        <section class="col col-3">
                                                                            <label class="label">País</label>
                                                                            <select name="centro" id="paisnacimineto" runat="server" class="select2" style="width: 100%;"></select>
                                                                        </section>
                                                                        <section class="col col-3" id="sectionestadon">
                                                                            <label class="label">Estado</label>
                                                                            <select name="centro" id="estadonacimiento" runat="server" class="select2" style="width: 100%;"></select>
                                                                        </section>
                                                                        <section class="col col-3" id="sectionmunicipion">
                                                                            <label class="label">Municipio</label>
                                                                            <select name="centro" id="municipionacimiento" runat="server" class="select2" style="width: 100%;"></select>
                                                                        </section>
                                                                        <section class="col col-3" id="sectioncoloniaNacimiento">
                                                                            <label class="label">Colonia</label>
                                                                            <select name="centro" id="coloniaSelectNacimiento" runat="server" class="select2" style="width: 100%;"></select>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">Calle</label>
                                                                            <label class="input">
                                                                                <i class="icon-append fa fa-street-view"></i>
                                                                                <input type="text" name="callelugar" id="callelugar" runat="server" placeholder="Calle" maxlength="250" class="alphanumeric alptext" />
                                                                                <b class="tooltip tooltip-bottom-right">Ingresa la calle.</b>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <section class="col col-6" style="padding-left: 0px;">
                                                                                <label class="label">Número</label>
                                                                                <label class="input">
                                                                                    <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                                    <input type="text" name="numerolugar" id="numerolugar" runat="server" placeholder="Número" maxlength="15" class="alphanumeric alptext" />
                                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el número.</b>
                                                                                </label>
                                                                            </section>
                                                                            <section class="col col-6" style="padding-right: 0px;" id="sectioncpnacimiento">
                                                                                <label class="label">C.P.</label>
                                                                                <label class="input">
                                                                                    <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                                    <input type="text" name="cplugar" id="cplugar" runat="server" placeholder="C.P" maxlength="5" class="integer" disabled="disabled" />
                                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el C.P.</b>
                                                                                </label>
                                                                            </section>
                                                                        </section>
                                                                        <section class="col col-2">
                                                                            <label class="label">Teléfono</label>
                                                                            <label class="input">
                                                                                <i class="icon-append fa fa-phone"></i>
                                                                                <input type="tel" name="telefonolugar" id="telefonolugar" runat="server" placeholder="Teléfono" data-mask="(999) 999-9999" />
                                                                                <b class="tooltip tooltip-bottom-right">Ingresa el teléfono .</b>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-4" id="sectionlocalidadn">
                                                                            <label class="label">Localidad</label>
                                                                            <label class="input">
                                                                                <i class="icon-append fa fa-street-view"></i>
                                                                                <input type="text" name="colonialugar" id="localidadn" hidden="hidden" runat="server" placeholder="Localidad" maxlength="250" class="alphanumeric alptext" />
                                                                                <b class="tooltip tooltip-bottom-right">Ingresa localidad.</b>
                                                                            </label>
                                                                        </section>
                                                                        <%--<section class="col col-5">
                                                                    <label class="label">Colonia</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-street-view"></i>
                                                                        <input type="text" name="colonialugar" id="colonialugar" runat="server" placeholder="Colonia" maxlength="250" class="alphanumeric" />
                                                                        <b class="tooltip tooltip-bottom-right">Ingresa la colonia.</b>
                                                                    </label>
                                                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                                                </section>--%>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <div class="row smart-form">
                                                                <footer>
                                                                    <div class="row" style="display: inline-block; float: right">
                                                                        <!--<a class="btn btn-sm btn-default clear4"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                                                        <a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>
                                                                        <a style="float: none; display: none;" href="javascript:void(0);" class="btn btn-default saveDomicilio" id="SaveDom" title="Guardar"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                                                        <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                                                    </div>
                                                                </footer>
                                                                <input type="hidden" id="trackingDomicilio" runat="server" />
                                                                <input type="hidden" id="idDomicilio" runat="server" />
                                                                <input type="hidden" id="trackingNacimiento" runat="server" />
                                                                <input type="hidden" id="idNacimiento" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>

    <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
         
                <div class="modal-body">
                    <div id="mapa" class="smart-form ">
                        <legend>Ubicación</legend>
                        <br />
                        <div id="mapid"></div>
                        <pre id='coordinates' class='coordinates'></pre>
                        <footer>
                            <a href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="closeMap"><i class="fa fa-close"></i>&nbsp;Cerrar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Eliminar Datos de procedencia</h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    Los datos de procedencia <strong><span id="eliminarprocedencia"></span></strong>será eliminados. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addProcedencia-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal_title_procedencia"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <div class="modal-body">
                            <div id="adiction_1" class="smart-form">
                                <fieldset>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input">Remesa <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-newspaper-o"></i>
                                                    <input type="text" name="remisa" runat="server" id="remisa" class="alphanumeric alptext" placeholder="Remesa" maxlength="250" title="Remisa" />
                                                    <b class="tooltip tooltip-bottom-right">Remesa</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input">Expediente <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-newspaper-o"></i>
                                                    <input type="text" name="expedir" runat="server" id="expediente" class="integer alptext" maxlength="10" placeholder="Expediente" title="Expediente" />
                                                    <b class="tooltip tooltip-bottom-right">Expediente</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="label">Estado de procedencia <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select name="centro" id="estaodprocedencia" runat="server"></select>
                                                    <i></i>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="label">Centro de procedencia <a style="color: red">*</a></label>
                                                <label class="select">
                                                    <select name="centro" id="centroprocedencia" runat="server">
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <!--<a class="btn btn-sm btn-default clear1"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteAlias-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Eliminar Alias</h4>
                </div>
                <div class="modal-body">
                    <div id="Div2" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El alias <strong><span id="eliminaralias"></span></strong>será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btndeleteAlias"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addAlias-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal_title_alias"></h4>
                </div>
                <div class="modal-body">
                    <div id="addAlia-modal" class="smart-form">
                        <div class="modal-body">
                            <div id="Div5" class="smart-form">
                                <fieldset>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input" style="color: dodgerblue">Alias/sobrenombre <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-user"></i>
                                                    <input type="text" name="alias" runat="server" id="alias" class="alphanumeric alptext" placeholder="Alias" maxlength="240" title="Alias" />
                                                    <b class="tooltip tooltip-bottom-right">Alias</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <!--<a class="btn btn-sm btn-default clear2"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveAlias" id="btnsaveAlias"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancelAlais" data-dismiss="modal" id="btncancelAlias"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="deleteNombre-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Eliminar</h4>
                </div>
                <div class="modal-body">
                    <div id="nombres-modal" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El  nombre <strong><span id="eliminarnombre"></span></strong>será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btndeleteNombre"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addNombre-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-title-otro-nombre"></h4>
                </div>
                <div class="modal-body">
                    <div id="Div6" class="smart-form">
                        <div class="modal-body">
                            <div id="Div7" class="smart-form">
                                <fieldset>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input">Nombre(s) <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-user"></i>
                                                    <input type="text" name="otro" runat="server" id="otro" class="alphanumeric alptext" placeholder="Nombre(s)" maxlength="250" title="Nombre(s)" />
                                                    <b class="tooltip tooltip-bottom-right">Nombre(s)</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input">Apellido paterno <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-user"></i>
                                                    <input type="text" name="paterno" runat="server" id="paterno" class="alphanumeric alptext" placeholder="Apellido Paterno" maxlength="250" title="Apellido Paterno" />
                                                    <b class="tooltip tooltip-bottom-right">Apellido paterno</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label class="input">Apellido materno <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-user"></i>
                                                    <input type="text" name="materno" runat="server" id="materno" class="alphanumeric alptext" placeholder="Apellido Materno" maxlength="250" title="Apellido Materno" />
                                                    <b class="tooltip tooltip-bottom-right">Apellido materno</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <!--<a class="btn btn-sm btn-default clear3"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveNombre" id="btnsaveNombre"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancelNombre" data-dismiss="modal" id="A4"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modalprocedencia" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div id="Div4" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbprocedencia"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblockprocedencia"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuarprocedencia"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modalalias" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div id="Div9" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbalias"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblockalias"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuaralias"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>&nbsp;
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modalnombre" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div id="Div10" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbnombre"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblocknombre"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuarnombre"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color: #3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color: #3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color: #3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color: #3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color: #3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color: #3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color: #3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br />
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="historyalias-modal" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Historial <span id="descripcionalias"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dt_basichistoryalias" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
									    <th data-hiden="tablet,fablet,phone">Alias</th>
									    <th>Movimiento</th>
                                        <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="evento-reciente-modal" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="btncanceleventoX" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Eventos recientes<i class='fa fa-pencil' style="float:left; margin-top:4px; margin-right:3px;"></i></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label style="color:dodgerblue">Eventos de las ultimas <a style="color:red">*</a></label>
                                    <select name="rol" id="eventoRecienteLigar" class="select2" style="width: 100%;"></select>
                                </section>
                                <section>
                                    <label  style="color:dodgerblue">Evento <a style="color:red">*</a></label>
                                    <select name="rol" id="eventoLigar" class="select2" style="width: 100%;"></select>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveDetenidoEventoReciente"><i class="fa fa-save"></i>&nbsp;Continuar </a>                            
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" id="btncancelevento"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="IdAlias" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="Ingreso" runat="server" value="" />
    <input type="hidden" id="Antropometria" runat="server" value="" />
    <input type="hidden" id="Señas_Particulares" runat="server" value="" />
    <input type="hidden" id="Adicciones" runat="server" value="" />
    <input type="hidden" id="Estudios_Criminologicos" runat="server" value="" />
    <input type="hidden" id="Expediente_Medico" runat="server" value="" />
    <input type="hidden" id="Familiares" runat="server" value="" />
    <input type="hidden" id="editable" runat="server" value="" />
    <input type="hidden" id="colId" />
    <input type="hidden" id="colNacId" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">        
        window.addEventListener("keydown", function (e) {
            var sectionNumber = $("#lstTabs .active").children().attr("id").substring(7);

            if (e.ctrlKey && e.keyCode === 39) {
                e.preventDefault();
                document.getElementById("linkinforme").click();
            }
            else if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                document.getElementById("linkInfoDetencion").click();
                switch (sectionNumber) {
                    case "1": document.getElementById("SaveGnral").click(); break;
                    case "2": document.getElementById("SaveDom").click(); break;
                    case "3":
                        if ($("#addAlias-modal").is(":visible")) {
                            document.getElementsByClassName("btnsaveAlias")[0].click();
                        }
                        break;
                }

            }
            else if (e.ctrlKey && e.keyCode === 9) {
                e.preventDefault();

            }
            else if (e.ctrlKey && e.keyCode === 38) {
                e.preventDefault();
                switch (sectionNumber) {
                    case "1": document.getElementById("linkTab3").click(); break;
                    case "2": document.getElementById("linkTab1").click(); break;
                    case "3": document.getElementById("linkTab2").click(); break;
                }
            }
            else if (e.ctrlKey && e.keyCode === 40) {
                e.preventDefault();
                switch (sectionNumber) {
                    case "1": document.getElementById("linkTab2").click(); break;
                    case "2": document.getElementById("linkTab3").click(); break;
                    case "3": document.getElementById("linkTab1").click(); break;
                }
            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                switch (sectionNumber) {
                    case "1": document.getElementsByClassName("SaveGnral")[0].click(); break;
                    case "2": document.getElementsByClassName("SaveDom")[0].click(); break;
                    case "3":
                        if ($("#addAlias-modal").is(":visible")) {
                            document.getElementsByClassName("btnsaveAlias")[0].click();
                        }
                        break;
                }
            }
         
        });

        $(document).on('keydown', 'input[pattern]', function (e) {
            var input = $(this);
            var oldVal = input.val();
            var regex = new RegExp(input.attr('pattern'), 'g');

            setTimeout(function () {
                var newVal = input.val();
                if (!regex.test(newVal)) {
                    input.val(oldVal);
                }
            }, 0);
        });

        $(document).ready(function () {            
            setTimeout(function () {                
                $('body').hide();
            }, 2000);
            setTimeout(function () {                
                $('body').show(0);
            }, 2100);            

            $('#ctl00_contenido_fecha').datetimepicker({
                ampm: true,
                format: 'DD/MM/YYYY'
            });

            $('body').on("keydown", function (e) {
                if (e.ctrlKey && e.which === 71 && $("#addAlias-modal").is(":visible")) {
                    var id = $("#btnsaveAlias").attr("data-id");
                    var tracking = $("#btnsaveAlias").attr("data-tracking");
                    if (validarAlias()) {
                        datos = [
                            id = id,
                            tracking = tracking,
                            alias = $('#ctl00_contenido_alias').val(),
                            interno = $('#ctl00_contenido_trackingid').val()
                        ];

                        SaveAlias(datos);
                    }
                }
                else if (e.ctrlKey && e.which === 71 && $('#lstTabs li')[1].className === "active") {
                    datos = [
                        id = $('#ctl00_contenido_idDomicilio').val(),
                        trackingd = $('#ctl00_contenido_trackingDomicilio').val(),
                        called = $('#ctl00_contenido_calledomicilio').val(),
                        numerod = $('#ctl00_contenido_numerodomicilio').val(),
                        //coloniad = $("#ctl00_contenido_coloniadomicilio").val(),
                        //cpd = $("#ctl00_contenido_cpdomicilio").val(),
                        telefonod = $("#ctl00_contenido_telefonodomicilio").val(),
                        paisd = $("#ctl00_contenido_paisdomicilio").val(),
                        estadod = $("#ctl00_contenido_estadodomicilio").val(),
                        municipiod = $("#ctl00_contenido_municipiodomicilio").val(),
                        interno = $('#ctl00_contenido_trackingid').val(),
                        idn = $('#ctl00_contenido_idNacimiento').val(),
                        trackingn = $('#ctl00_contenido_trackingNacimiento').val(),
                        callen = $("#ctl00_contenido_callelugar").val(),
                        numeron = $("#ctl00_contenido_numerolugar").val(),
                        //colonian = $("#ctl00_contenido_colonialugar").val(),
                        //cpn = $("#ctl00_contenido_cplugar").val(),
                        telefonon = $("#ctl00_contenido_telefonolugar").val(),
                        paisn = $('#ctl00_contenido_paisnacimineto').val(),
                        estadon = $("#ctl00_contenido_estadonacimiento").val(),
                        municipion = $("#ctl00_contenido_municipionacimiento").val(),
                        interno = $('#ctl00_contenido_trackingid').val(),
                        localidad = $('#ctl00_contenido_localidad').val(),
                        localidan = $('#ctl00_contenido_localidadn').val(),
                        coloniaId = $("#ctl00_contenido_coloniaSelect").val(),
                        coloniaIdNacimiento = $("#ctl00_contenido_coloniaSelectNacimiento").val(),
                        latituddomicilio = $("#latitudd").val(),
                        longituddomicilio = $("#longitudd").val()
                    ];

                    if (validarDN()) {
                        SaveDN(datos);
                    }
                }
                else if (e.ctrlKey && e.which === 71 && $('#lstTabs li')[0].className === "active") {
                    var estado = false;
                    var inimputable = false;
                    /*
                    if ($('#ctl00_contenido_mental').is(":checked")) {
                        estado = true;
    
                    }
    
                    if ($('#ctl00_contenido_inimputable').is(":checked")) {
                        inimputable = true;
                    }*/

                    datos = [
                        id = $('#ctl00_contenido_idGeneral').val(),
                        tracking = $('#ctl00_contenido_trackingGeneral').val(),
                        fecha = $("#ctl00_contenido_fecha").val(),
                        rfc = $("#ctl00_contenido_rfc").val(),
                        nacionalidad = $("#ctl00_contenido_nacionalidad").val(),
                        escolaridad = $("#ctl00_contenido_escolaridad").val(),
                        religion = $("#ctl00_contenido_religion").val(),
                        ocupacion = $("#ctl00_contenido_ocupacion").val(),
                        civil = $("#ctl00_contenido_civil").val(),
                        etnia = $("#ctl00_contenido_etinia").val(),
                        sexo = $("#ctl00_contenido_sexo").val(),
                        estado = estado,
                        inimputable = inimputable,
                        interno = $('#ctl00_contenido_trackingid').val(),
                        LenguaNativa = $("#ctl00_contenido_lenguanativa").val(),
                        CURP = $("#ctl00_contenido_curp").val(),
                        SalarioSemanal = $("#<%= salarioSemanal.ClientID %>").val().replace(/([\$\,\.])/, ''),
                        Edad = $("#edadInterno").text()
                    ];

                    if (validarGeneral()) {
                        SaveGeneral(datos);
                    }
                }
            });

            $("#ctl00_contenido_fecha").data("DateTimePicker").hide();

            $("#datospersonales-modal").on("hidden.bs.modal", function () {
                document.getElementsByClassName("detencion")[0].focus();
            });

            $("#mapModal").on("shown.bs.modal", function () {
                $("#closeMap").focus();
            });

            $("#addAlias-modal").on("shown.bs.modal", function () {
                $("#ctl00_contenido_alias").focus();
            });

            $("#mapModal").on("hidden.bs.modal", function () {
                $("#showMap").focus();
            });

            $("#addAlias-modal").on("hidden.bs.modal", function () {
                document.querySelector("#addAlias").focus();
            });

            $("#ScrollableContent").css("height", "calc(100vh - 443px)");

            $("#ctl00_contenido_civil").select2();
            $("#ctl00_contenido_ocupacion").select2();
            $("#ctl00_contenido_nacionalidad").select2();
            $("#ctl00_contenido_escolaridad").select2();
            $("#ctl00_contenido_etnia").select2();
            $("#ctl00_contenido_sexo").select2();

            $("#ctl00_contenido_paisdomicilio").select2();
            $("#ctl00_contenido_estadodomicilio").select2();
            $("#ctl00_contenido_municipiodomicilio").select2();
            $("#ctl00_contenido_coloniaSelect").select2();
            $("#ctl00_contenido_paisnacimineto").select2();
            $("#ctl00_contenido_estadonacimiento").select2();
            $("#ctl00_contenido_municipionacimiento").select2();
            $("#ctl00_contenido_coloniaSelectNacimiento").select2();

            $("#eventoRecienteLigar").select2({
                dropdownParent: $("#evento-reciente-modal")
            });
            $("#eventoLigar").select2({
                dropdownParent: $("#evento-reciente-modal")
            });

            $(".ligarNuevoEvento").hide();
            if ($("#<%= editable.ClientID%>").val() == "0") {
                $("#ctl00_contenido_paisdomicilio").attr("disabled", "disabled");
                $("#ctl00_contenido_estadodomicilio").attr("disabled", "disabled");
                $("#ctl00_contenido_municipiodomicilio").attr("disabled", "disabled");
                $("#ctl00_contenido_coloniaSelect").attr("disabled", "disabled");
                $("#ctl00_contenido_calledomicilio").attr("disabled", "disabled");
                $("#ctl00_contenido_numerodomicilio").attr("disabled", "disabled");
                $("#ctl00_contenido_cpdomicilio").attr("disabled", "disabled");
                $("#ctl00_contenido_telefonodomicilio").attr("disabled", "disabled");
                $("#ctl00_contenido_paisnacimineto").attr("disabled", "disabled");
                $("#ctl00_contenido_estadonacimiento").attr("disabled", "disabled");
                $("#ctl00_contenido_municipionacimiento").attr("disabled", "disabled");
                $("#ctl00_contenido_coloniaSelectNacimiento").attr("disabled", "disabled");
                $("#ctl00_contenido_callelugar").attr("disabled", "disabled");
                $("#ctl00_contenido_numerolugar").attr("disabled", "disabled");
                $("#ctl00_contenido_cplugar").attr("disabled", "disabled");
                $("#ctl00_contenido_telefonolugar").attr("disabled", "disabled");
                $("#latitudd").attr("disabled", "disabled");
                $("#longitudd").attr("disabled", "disabled");
                $("#ctl00_contenido_localidad").attr("disabled", "disabled");
                $("#ctl00_contenido_localidadn").attr("disabled", "disabled");

                $('#ctl00_contenido_fecha').attr("disabled", "disabled");
                $('#ctl00_contenido_nacionalidad').attr("disabled", "disabled");
                $('#ctl00_contenido_rfc').attr("disabled", "disabled");
                $('#ctl00_contenido_curp').attr("disabled", "disabled");
                $('#ctl00_contenido_escolaridad').attr("disabled", "disabled");
                $('#ctl00_contenido_religion').attr("disabled", "disabled");
                $('#ctl00_contenido_ocupacion').attr("disabled", "disabled");
                $('#ctl00_contenido_civil').attr("disabled", "disabled");
                $('#ctl00_contenido_etinia').attr("disabled", "disabled");
                $('#ctl00_contenido_sexo').attr("disabled", "disabled");
                $('#ctl00_contenido_lenguanativa').attr("disabled", "disabled");
                $('#ctl00_contenido_salarioSemanal').attr("disabled", "disabled");
               

                $("#ctl00_contenido_paisdomicilio").css("background", "#eee");
                $("#ctl00_contenido_estadodomicilio").css("background", "#eee");
                $("#ctl00_contenido_municipiodomicilio").css("background", "#eee");
                $("#ctl00_contenido_coloniaSelect").css("background", "#eee");
                $("#ctl00_contenido_calledomicilio").css("background", "#eee");
                $("#ctl00_contenido_numerodomicilio").css("background", "#eee");
                $("#ctl00_contenido_cpdomicilio").css("background", "#eee");
                $("#ctl00_contenido_telefonodomicilio").css("background", "#eee");
                $("#ctl00_contenido_paisnacimineto").css("background", "#eee");
                $("#ctl00_contenido_estadonacimiento").css("background", "#eee");
                $("#ctl00_contenido_municipionacimiento").css("background", "#eee");
                $("#ctl00_contenido_coloniaSelectNacimiento").css("background", "#eee");
                $("#ctl00_contenido_callelugar").css("background", "#eee");
                $("#ctl00_contenido_numerolugar").css("background", "#eee");
                $("#ctl00_contenido_cplugar").css("background", "#eee");
                $("#ctl00_contenido_telefonolugar").css("background", "#eee");
                $("#latitudd").css("background", "#eee");
                $("#longitudd").css("background", "#eee");
                $("#ctl00_contenido_localidad").css("background", "#eee");
                $("#ctl00_contenido_localidadn").css("background", "#eee");

                $('#ctl00_contenido_fecha').css("background", "#eee");
                $('#ctl00_contenido_rfc').css("background", "#eee");
                $('#ctl00_contenido_curp').css("background", "#eee");
                $('#ctl00_contenido_salarioSemanal').css("background", "#eee");
                setTimeout(function () {
                    $('#ctl00_contenido_nacionalidad').next().children().children().css("background", "#eee");
                    $('#ctl00_contenido_escolaridad').next().children().children().css("background", "#eee");
                    $('#ctl00_contenido_religion').next().children().children().css("background", "#eee");
                    $('#ctl00_contenido_ocupacion').next().children().children().css("background", "#eee");
                    $('#ctl00_contenido_civil').next().children().children().css("background", "#eee");
                    $('#ctl00_contenido_etinia').next().children().children().css("background", "#eee");
                    $('#ctl00_contenido_sexo').next().children().children().css("background", "#eee");
                    $('#ctl00_contenido_lenguanativa').next().children().children().css("background", "#eee");
                }, 500);

                $("#showMap").hide();

                $(".ligarNuevoEvento").show();
            }




            $("#ctl00_contenido_numerodomicilio").bind('keypress', function (event) {
                var regex = new RegExp("^[0-9a-zA-ZáéíóúÁÉÍÓÚ -]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("#ctl00_contenido_numerolugar").bind('keypress', function (event) {
                var regex = new RegExp("^[0-9a-zA-ZáéíóúÁÉÍÓÚ -]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("body").on("click", ".detencion", function () {
                $("#form-modal-title").empty();
                $("#form-modal-title").html("Información de la detención");
                $("#form-modal").modal("show");
            });

            $("#showMap").click(function () {
                loadMap();
                $("#form-modal-title-agregar").empty();
                $("#form-modal-title-agregar").html("Agregar");
                $("#mapModal").modal("show");
            });
            pageSetUp();

            $("#ctl00_contenido_coloniaSelect").change(function () {
                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                CargarCodigoPostalDomicilio($("#ctl00_contenido_coloniaSelect").val());
            });

            $("#ctl00_contenido_rfc").focusout(function () {
            }).blur(function () {
                var curp10 = $("#ctl00_contenido_rfc").val();
                $("#ctl00_contenido_curp").val(curp10.substring(0, 10));
            });

            $("#ctl00_contenido_coloniaSelectNacimiento").change(function () {
                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                CargarCodigoPostalNacimiento($("#ctl00_contenido_coloniaSelectNacimiento").val());
            });

            $("#ctl00_contenido_municipiodomicilio").change(function () {
                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                CargarColonia($("#colId").val(), $("#ctl00_contenido_municipiodomicilio").val());
            });

            $("#ctl00_contenido_municipionacimiento").change(function () {
                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                CargarColoniaNacimiento($("#colNacId").val(), $("#ctl00_contenido_municipionacimiento").val());
            });

            function CargarCodigoPostalDomicilio(idColonia) {
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/getZipCode",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idColonia: idColonia
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        $("#ctl00_contenido_cpdomicilio").val(resultado.cp);
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarCodigoPostalNacimiento(idColonia) {
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/getZipCode",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idColonia: idColonia
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        $("#ctl00_contenido_cplugar").val(resultado.cp);
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            init();
            responsiveHelper_datatable_Alias = undefined;
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_dt_basicAlias = undefined;
            var responsiveHelper_dt_basicNombre = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            var breakpointAliasDefinition = {
                tablet: 1024,
                phone: 480
            };
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },

                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');

                    }
                },
                ajax: {
                    type: "POST",
                    url: "personalinformation.aspx/getProcedencia",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        var trackingid = ($('#<%= trackingid.ClientID %>').val());

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = trackingid;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Remisa",
                        data: "Remisa"
                    },
                    {
                        name: "Expedir",
                        data: "Expedir"
                    },
                    {
                        name: "Estado",
                        data: "Estado"
                    },
                    {
                        name: "Centro",
                        data: "Centro"
                    },
                    null
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 6,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "edit";
                            var editar = "";
                            var habilitar = "";

                            if (row.Activo) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-remisa = "' + row.Remisa + '" data-centro = "' + row.CentroId + '" data-expediente = "' + row.Expedir + '" data-estado = "' + row.EstadoId + '" data-tracking="' + row.TrackingId + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitemprocedencia" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-value = "' + row.Remisa + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return editar + habilitar;
                        }
                    }
                ]
            });

            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        value: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            if (resultado.tienegeneral) {
                                $('#edadInterno').text(resultado.general.Edad);
                            }

                            $('#fechaInterno').text(resultado.obj.Fecha);
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#avatar').attr("src", imagenAvatar);
                            $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            //$("#linkvistas").attr("href", "huella.aspx?tracking=" + $("#ctl00_contenido_tracking").val());
                            $("#linkestudios").attr("href", "record.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            //    $("#linkadicciones").attr("href", "addiction.aspx?tracking=" + resultado.obj.TrackingId);
                            //    $("#linkfamiliares").attr("href", "family.aspx?tracking=" + resultado.obj.TrackingId);
                            $("#linkseñas").attr("href", "signal.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkingreso").attr("href", "entry.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkvistas").attr("href", "huella.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());

                            $('#<%= trackingid.ClientID %>').val(resultado.obj.TrackingId);

                            if (resultado.tienenacimiento) {
                                $('#ctl00_contenido_callelugar').val(resultado.nacimiento.CalleNacimiento);
                                $('#ctl00_contenido_numerolugar').val(resultado.nacimiento.NumeroNacimiento);
                                //$('#ctl00_contenido_colonialugar').val(resultado.nacimiento.ColoniaNacimiento);
                                //Cambio de CP
                                $('#ctl00_contenido_cplugar').val(resultado.nacimiento.CodigoPostalNacimiento);
                                $('#ctl00_contenido_telefonolugar').val(resultado.nacimiento.TelefonoNacimiento);
                                CargarPais(resultado.nacimiento.PaisNacimiento, '#ctl00_contenido_paisnacimineto');

                                if (resultado.nacimiento.PaisNacimiento == 73) {
                                    $('#sectionlocalidadn').hide();
                                    $('#sectionmunicipion').show();
                                    $('#sectionestadon').show();
                                    $('#sectioncoloniaNacimiento').show();
                                    $('#sectioncpnacimiento').show();
                                    CargarEstado(resultado.nacimiento.EstadoNacimiento, '#ctl00_contenido_estadonacimiento', resultado.nacimiento.PaisNacimiento);
                                    CargarMunicipio(resultado.nacimiento.MunicipioNacimiento, '#ctl00_contenido_municipionacimiento', resultado.nacimiento.EstadoNacimiento);
                                    CargarColoniaNacimiento(resultado.nacimiento.ColoniaIdNacimiento, resultado.nacimiento.MunicipioNacimiento);
                                    $("#colNacId").val(resultado.nacimiento.ColoniaIdNacimiento);
                                }
                                else {

                                    $('#sectionlocalidadn').show();
                                    $('#sectionmunicipion').hide();
                                    $('#sectionestadon').hide();
                                    $('#sectioncoloniaNacimiento').hide();
                                    $('#sectioncpnacimiento').hide();
                                    //CargarEstado(0, '#ctl00_contenido_estadonacimiento');
                                    //CargarMunicipio(0, '#ctl00_contenido_municipionacimiento');
                                    $('#ctl00_contenido_localidadn').val(resultado.nacimiento.LocalidadNacimiento);
                                }

                                $('#<%= trackingNacimiento.ClientID %>').val(resultado.nacimiento.TrackingId);
                                $('#<%= idNacimiento.ClientID %>').val(resultado.nacimiento.Id);
                            }
                            else {
                                CargarPais(73, '#ctl00_contenido_paisnacimineto');
                                /* Yucatan 
                                CargarEstado(0, '#ctl00_contenido_estadonacimiento', 73);*/
                                CargarEstado(26, '#ctl00_contenido_estadonacimiento', 73);
                                CargarMunicipio(0, '#ctl00_contenido_municipionacimiento', 26);
                                $('#<%= trackingNacimiento.ClientID %>').val("");
                                $('#<%= idNacimiento.ClientID %>').val("");
                                $('#sectionlocalidadn').hide();
                            }

                            if (resultado.tienedomicilio) {
                                $('#coloniaInterno').text(resultado.domicilio.coloniaNombre);
                                $('#municipioInterno').text(resultado.domicilio.municipioNombre);
                                $('#domicilioInterno').text(resultado.domicilio.CalleDomicilio + " #" + resultado.domicilio.NumeroDomicilio);
                                $('#ctl00_contenido_calledomicilio').val(resultado.domicilio.CalleDomicilio);
                                $('#ctl00_contenido_numerodomicilio').val(resultado.domicilio.NumeroDomicilio);
                                //$('#ctl00_contenido_coloniadomicilio').val(resultado.domicilio.ColoniaDomicilio);
                                //Cambio de CP
                                $('#ctl00_contenido_cpdomicilio').val(resultado.domicilio.CodigoPostalDomicilio);
                                $('#ctl00_contenido_telefonodomicilio').val(resultado.domicilio.TelefonoDomicilio);
                                CargarPais(resultado.domicilio.PaisDomicilio, '#ctl00_contenido_paisdomicilio');
                                $("#latitudd").val(resultado.domicilio.LatitudDomicilio),
                                    $("#longitudd").val(resultado.domicilio.LongitudDomicilio)

                                if (resultado.domicilio.PaisDomicilio == 73) {
                                    $('#sectionlocalidad').hide();
                                    $('#sectionmunicipio').show();
                                    $('#sectionestado').show();
                                    $('#sectioncoloniaDomicilio').show();
                                    $('#sectioncpdomicilio').show();
                                    CargarEstado(resultado.domicilio.EstadoDomicilio, '#ctl00_contenido_estadodomicilio', resultado.domicilio.PaisDomicilio);
                                    CargarMunicipio(resultado.domicilio.MunicipioDomicilio, '#ctl00_contenido_municipiodomicilio', resultado.domicilio.EstadoDomicilio);
                                    CargarColonia(resultado.domicilio.ColoniaIdDomicilio, resultado.domicilio.MunicipioDomicilio);
                                    $("#colId").val(resultado.domicilio.ColoniaIdDomicilio);
                                }
                                else {

                                    $('#sectionlocalidad').show();
                                    $('#sectionmunicipio').hide();
                                    $('#sectionestado').hide();
                                    $('#sectioncoloniaDomicilio').hide();
                                    $('#sectioncpdomicilio').hide();
                                    //CargarEstado(0, '#ctl00_contenido_estadodomicilio');
                                    //CargarMunicipio(0, '#ctl00_contenido_municipiodomicilio');
                                    $('#ctl00_contenido_localidad').val(resultado.domicilio.LocalidadDomicilio);
                                }

                                $('#<%= trackingDomicilio.ClientID %>').val(resultado.domicilio.TrackingId);
                                $('#<%= idDomicilio.ClientID %>').val(resultado.domicilio.Id);
                            }
                            else {
                                if (resultado.tienedomsc) {
                                    CargarPais(resultado.objdomsc.PaisId, '#ctl00_contenido_paisdomicilio');
                                    CargarEstado(resultado.objdomsc.EstadoId, '#ctl00_contenido_estadodomicilio', resultado.objdomsc.paisId);
                                    CargarMunicipio(resultado.objdomsc.MunicipioId, '#ctl00_contenido_municipiodomicilio', resultado.objdomsc.EstadoId);
                                    CargarColonia("0", resultado.objdomsc.MunicipioId);
                                    $("#colId").val("0");
                                    $('#domicilioInterno').text("Domicilio no registrado");
                                    $('#coloniaInterno').text("Colonia no registrada");
                                    $('#municipioInterno').text("Municipio no registrado");
                                    //CargarMunicipio(0, '#ctl00_contenido_municipiodomicilio');
                                    $('#<%= trackingDomicilio.ClientID %>').val("");
                                    $('#<%= idDomicilio.ClientID %>').val("");
                                    $('#sectionlocalidad').hide();
                                }
                                else {
                                    //CargarEstado(0, '#ctl00_contenido_estadodomicilio');
                                    CargarPais(73, '#ctl00_contenido_paisdomicilio');
                                    CargarEstado(26, '#ctl00_contenido_estadodomicilio', 73);
                                    CargarMunicipio(0, '#ctl00_contenido_municipiodomicilio', 26);
                                    $('#domicilioInterno').text("Domicilio no registrado");
                                    $('#coloniaInterno').text("Colonia no registrada");
                                    $('#municipioInterno').text("Municipio no registrado");
                                    //CargarMunicipio(0, '#ctl00_contenido_municipiodomicilio');
                                    $('#<%= trackingDomicilio.ClientID %>').val("");
                                    $('#<%= idDomicilio.ClientID %>').val("");
                                    $('#sectionlocalidad').hide();
                                }
                            }

                            if (resultado.tienegeneral) {
                                var fechaNacimiento = resultado.general.FechaNacimineto;
                                if (fechaNacimiento != "") {
                                    $('#ctl00_contenido_fecha').val(fechaNacimiento).trigger("change");
                                }
                                else {
                                    $('#ctl00_contenido_fecha').val(fechaNacimiento);
                                }

                                $('#sexoInterno').text(resultado.general.Sexo);

                                CargarNacionalidad(resultado.general.Nacionalidad);
                                $('#ctl00_contenido_rfc').val(resultado.general.RFC);
                                $('#ctl00_contenido_curp').val(resultado.general.CURP);
                                CargarEscolaridad(resultado.general.Escolaridad);
                                CargarReligion(resultado.general.Religion);
                                CargarOcupacion(resultado.general.Ocupacion);

                                CargarEstadoCivil(resultado.general.Estado);
                                CargarEtnia(resultado.general.Etnia);
                                CargarSexo(resultado.general.SexoId);
                                CargarLenguaNativa(resultado.general.LenguaNativaId);
                                $('#<%= trackingGeneral.ClientID %>').val(resultado.general.TrackingId);
                                $('#<%= idGeneral.ClientID %>').val(resultado.general.Id);
                                $("#<%= salarioSemanal.ClientID %>").val(resultado.general.SalarioSemanal);
                            }
                            else {
                                $('#sexoInterno').text("Sexo no registrado");
                                CargarNacionalidad(38);
                                CargarEscolaridad(0);
                                CargarReligion("0");
                                CargarOcupacion(0);
                                CargarEstadoCivil(0);
                                CargarEtnia("0");
                                CargarSexo(0);
                                CargarLenguaNativa(0);
                                $('#<%= trackingGeneral.ClientID %>').val("");
                                $('#<%= idGeneral.ClientID %>').val("");
                            }


                        } else {
                            ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            function CargarEstado(setestado, combo, paisid) {

                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getEstado",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ paisId: 73 }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Estado]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (setestado != "" && setestado != null) {
                            Dropdown.val(setestado);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {

                        ShowError("¡Error!", "No fue posible cargar la lista de estados. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarMunicipio(set, combo, estadoid) {
                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getMunicipio",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ estadoId: estadoid }),
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Municipio]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de municipio. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarPais(set, combo) {
                $(combo).empty();
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/getPais",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[País]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarColonia(set, idMunicipio) {
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#ctl00_contenido_coloniaSelect");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarColoniaNacimiento(set, idMunicipio) {
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#ctl00_contenido_coloniaSelectNacimiento");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        else {
                            Dropdown.val(0);
                            Dropdown.trigger('change');
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarCentro(estadoid, setcentro) {
                $('#ctl00_contenido_centroprocedencia').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getInstitucion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ estadoid: estadoid }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_centroprocedencia');
                        Dropdown.append(new Option("[Centro]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (setcentro != "") {

                            Dropdown.val(setcentro);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de centros. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("#ctl00_contenido_estaodprocedencia").change(function () {
                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                if ($("#ctl00_contenido_estaodprocedencia option:selected").val() > 0) {
                    $('#sectioncentroprocedencia').show();
                    CargarCentro($("#ctl00_contenido_estaodprocedencia").val(), 0);
                }
                else {
                    $('#sectioncentroprocedencia').hide();
                }
            });

            function CargarLenguaNativa(setlengua) {
                $('#ctl00_contenido_lenguanativa').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getlenguanativa",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_lenguanativa');

                        Dropdown.append(new Option("[Lengua nativa]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (setlengua != "") {

                            Dropdown.val(setlengua);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de lengua nativa. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarSexo(setsexo) {
                $('#ctl00_contenido_sexo').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getSexo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_sexo');

                        Dropdown.append(new Option("[Sexo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (setsexo != "") {

                            Dropdown.val(setsexo);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de sexo. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEtnia(setetnia) {
                $('#ctl00_contenido_etinia').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getEtnia",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_etinia');

                        Dropdown.append(new Option("[Etnia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (setetnia != "") {

                            Dropdown.val(setetnia);
                        }
                        Dropdown.trigger("change.select2");
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de etnias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEstadoCivil(set) {
                $('#ctl00_contenido_civil').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getEstadoCivil",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_civil');

                        Dropdown.append(new Option("[Estado Civil]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);


                        }
                        Dropdown.trigger("change.select2");
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de estado civil. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarOcupacion(set) {
                $('#ctl00_contenido_ocupacion').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getOcupacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_ocupacion');

                        Dropdown.append(new Option("[Ocupación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de ocupaciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarReligion(set) {
                $('#ctl00_contenido_religion').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getReligion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_religion');

                        Dropdown.append(new Option("[Religión]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);

                        }
                        Dropdown.trigger("change.select2");
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de religiones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEscolaridad(set) {
                $('#ctl00_contenido_escolaridad').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getEscolaridad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_escolaridad');

                        Dropdown.append(new Option("[Escolaridad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de escolaridad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarNacionalidad(set) {
                $('#ctl00_contenido_nacionalidad').empty();
                $.ajax({

                    type: "POST",
                    url: "personalinformation.aspx/getNacionalidad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_nacionalidad');

                        Dropdown.append(new Option("[Nacionalidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de nacionalidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("body").on("click", ".clear", function () {
                //  $("#ctl00_contenido_mental:checkbox").attr('checked', false);
                //   $("#ctl00_contenido_inimputable:checkbox").attr('checked', false);
                $("#ctl00_contenido_fecha").val("");
                $("#ctl00_contenido_rfc").val("");
                $("#ctl00_contenido_nacionalidad").val("0");
                $("#ctl00_contenido_escolaridad").val("0");
                $("#ctl00_contenido_religion").val("0");
                $("#ctl00_contenido_ocupacion").val("0");
                $("#ctl00_contenido_civil").val("0");
                $("#ctl00_contenido_etinia").val("0");
                $("#ctl00_contenido_sexo").val("0");
            });

            $("body").on("click", ".clear1", function () {
                $("#ctl00_contenido_remisa").val("");
                $("#ctl00_contenido_expediente").val("");
                $("#ctl00_contenido_estaodprocedencia").val("0");
                $("#ctl00_contenido_centroprocedencia").val("0");
            });

            $("body").on("click", ".clear3", function () {
                $("#ctl00_contenido_otro").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
            });

            $("body").on("click", ".clear2", function () {
                $("#ctl00_contenido_alias").val("");
            });
            $("#ctl00_contenido_alias").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("body").on("click", ".clear4", function () {
                $("#ctl00_contenido_calledomicilio").val("");
                $("#ctl00_contenido_numerodomicilio").val("");
                $("#ctl00_contenido_coloniadomicilio").val("");
                $("#ctl00_contenido_cpdomicilio").val("");
                $("#ctl00_contenido_telefonodomicilio").val("");
                $("#ctl00_contenido_paisdomicilio").val("0");
                $("#ctl00_contenido_estadodomicilio").val("0");
                $("#ctl00_contenido_municipiodomicilio").val("0");
                $("#ctl00_contenido_callelugar").val("");
                $("#ctl00_contenido_numerolugar").val("");
                $("#ctl00_contenido_colonialugar").val("");
                $("#ctl00_contenido_cpdomicilio").val("");
                $("#ctl00_contenido_cplugar").val("");
                $("#ctl00_contenido_telefonolugar").val("");
                $("#ctl00_contenido_paisnacimineto").val("0");
                $("#ctl00_contenido_estadonacimiento").val("0");
                $("#ctl00_contenido_municipionacimiento").val("0");
                $("#ctl00_contenido_localidad").val("");
                $("#ctl00_contenido_localidadn").val("");
                $("#latitudd").val("");
                $("#longitudd").val("");

            });

            $("body").on("click", ".saveGeneral", function () {
                var estado = false;
                var inimputable = false;
                /*
                if ($('#ctl00_contenido_mental').is(":checked")) {
                    estado = true;

                }

                if ($('#ctl00_contenido_inimputable').is(":checked")) {
                    inimputable = true;
                }*/

                datos = [
                    id = $('#ctl00_contenido_idGeneral').val(),
                    tracking = $('#ctl00_contenido_trackingGeneral').val(),
                    fecha = $("#ctl00_contenido_fecha").val(),
                    rfc = $("#ctl00_contenido_rfc").val(),
                    nacionalidad = $("#ctl00_contenido_nacionalidad").val(),
                    escolaridad = $("#ctl00_contenido_escolaridad").val(),
                    religion = $("#ctl00_contenido_religion").val(),
                    ocupacion = $("#ctl00_contenido_ocupacion").val(),
                    civil = $("#ctl00_contenido_civil").val(),
                    etnia = $("#ctl00_contenido_etinia").val(),
                    sexo = $("#ctl00_contenido_sexo").val(),
                    estado = estado,
                    inimputable = inimputable,
                    interno = $('#ctl00_contenido_trackingid').val(),
                    LenguaNativa = $("#ctl00_contenido_lenguanativa").val(),
                    CURP = $("#ctl00_contenido_curp").val(),
                    SalarioSemanal = $("#<%= salarioSemanal.ClientID %>").val().replace(/([\$\,\.])/, ''),
                    Edad = $("#edadInterno").text()
                ];

                if (validarGeneral()) {
                    SaveGeneral(datos);
                }
            });

            function validarGeneral() {
                var esvalido = true;

                if ($("#ctl00_contenido_fecha").val() == null || $("#ctl00_contenido_fecha").val().split(" ").join("") == "") {
                    ShowError("Fecha nacimiento", "La fecha nacimiento es obligatoria.");
                    $('#ctl00_contenido_fecha').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_fecha').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_fecha').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_fecha').addClass('valid');
                }

                if ($("#ctl00_contenido_nacionalidad").val() == null || $("#ctl00_contenido_nacionalidad").val() == 0) {
                    ShowError("Nacionalidad", "La nacionalidad es obligatoria.");
                    $('#ctl00_contenido_nacionalidad').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_nacionalidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_nacionalidad').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_nacionalidad').addClass('valid');
                }

                if ($("#ctl00_contenido_escolaridad").val() == null || $("#ctl00_contenido_escolaridad").val() == 0) {
                    ShowError("Escolaridad", "La escolaridad es obligatoria.");
                    $('#ctl00_contenido_escolaridad').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_escolaridad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_escolaridad').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_escolaridad').addClass('valid');
                }

                //if ($("#ctl00_contenido_religion").val() == null || $("#ctl00_contenido_religion").val() == 0) {
                //    ShowError("Religión", "La religión es obligatoria.");
                //    $('#ctl00_contenido_religion').parent().removeClass('state-success').addClass("state-error");
                //    $('#ctl00_contenido_religion').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#ctl00_contenido_religion').parent().removeClass("state-error").addClass('state-success');
                //    $('#ctl00_contenido_religion').addClass('valid');
                //}

                if ($("#ctl00_contenido_ocupacion").val() == null || $("#ctl00_contenido_ocupacion").val() == 0) {
                    ShowError("Ocupación", "La ocupación es obligatoria.");
                    $('#ctl00_contenido_ocupacion').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_ocupacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_ocupacion').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_ocupacion').addClass('valid');
                }

                if ($("#ctl00_contenido_civil").val() == null || $("#ctl00_contenido_civil").val() == 0) {
                    ShowError("Estado civil", "El estado civil es obligatorio.");
                    $('#ctl00_contenido_civil').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_civil').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_civil').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_civil').addClass('valid');
                }

                //if ($("#ctl00_contenido_etinia").val() == null || $("#ctl00_contenido_etinia").val() == 0) {
                //    ShowError("Etnia", "La etnia es obligatoria.");
                //    $('#ctl00_contenido_etinia').parent().removeClass('state-success').addClass("state-error");
                //    $('#ctl00_contenido_etinia').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#ctl00_contenido_etinia').parent().removeClass("state-error").addClass('state-success');
                //    $('#ctl00_contenido_etinia').addClass('valid');
                //}

                if ($("#ctl00_contenido_sexo").val() == null || $("#ctl00_contenido_sexo").val() == 0) {
                    ShowError("Sexo", "El sexo es obligatorio.");
                    $('#ctl00_contenido_sexo').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_sexo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_sexo').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_sexo').addClass('valid');
                }

                return esvalido;
            }

            function limpiarGeneral() {
                $('#ctl00_contenido_fecha').parent().removeClass('state-success');
                $('#ctl00_contenido_fecha').parent().removeClass("state-error");
                $('#ctl00_contenido_rfc').parent().removeClass('state-success');
                $('#ctl00_contenido_rfc').parent().removeClass("state-error");
                $('#ctl00_contenido_nacionalidad').parent().removeClass('state-success');
                $('#ctl00_contenido_nacionalidad').parent().removeClass("state-error");
                $('#ctl00_contenido_escolaridad').parent().removeClass('state-success');
                $('#ctl00_contenido_escolaridad').parent().removeClass("state-error");
                $('#ctl00_contenido_religion').parent().removeClass('state-success');
                $('#ctl00_contenido_religion').parent().removeClass("state-error");
                $('#ctl00_contenido_ocupacion').parent().removeClass('state-success');
                $('#ctl00_contenido_ocupacion').parent().removeClass("state-error");
                $('#ctl00_contenido_civil').parent().removeClass('state-success');
                $('#ctl00_contenido_civil').parent().removeClass("state-error");
                $('#ctl00_contenido_etinia').parent().removeClass('state-success');
                $('#ctl00_contenido_etinia').parent().removeClass("state-error");
                $('#ctl00_contenido_sexo').parent().removeClass('state-success');
                $('#ctl00_contenido_sexo').parent().removeClass("state-error");
            }

            function SaveGeneral(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/saveGeneral",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in alert-dismissable'><button class='close' >x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Los datos generales se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Los datos generales se " + resultado.mensaje + " correctamente.");
                           // if (resultado.mensaje2 == "La fecha de nacimiento no coincide con la edad registrada en el evento.") ShowAlert("¡Atención!", resultado.mensaje2);
                            limpiarGeneral();
                            var param = RequestQueryString("tracking");

                            if (param != undefined) {

                                CargarDatos(param);
                            }
                            $('#<%= trackingGeneral.ClientID %>').val(resultado.TrackingId);
                            $('#<%= idGeneral.ClientID %>').val(resultado.Id);
                        }
                        else {

                            ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico. ");


                        }
                        limpiarGeneral();
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("body").on("click", ".add", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_remisa").val("");
                $("#ctl00_contenido_expediente").val("");
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#modal_title_procedencia").empty();
                $("#modal_title_procedencia").html("Agregar datos de procedencia");
                $("#addProcedencia-modal").modal("show");

                limpiar();
                CargarCentro(0, 0);
                CargarEstado(0, '#ctl00_contenido_estaodprocedencia');
            });

            $("body").on("click", ".edit", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                var centro = $(this).attr("data-centro");
                var estado = $(this).attr("data-estado");
                $("#btnsave").attr("data-id", id);
                $("#btnsave").attr("data-tracking", tracking);
                $("#ctl00_contenido_remisa").val($(this).attr("data-remisa"));
                $("#ctl00_contenido_expediente").val($(this).attr("data-expediente"));
                CargarCentro(estado, centro);
                CargarEstado(estado, '#ctl00_contenido_estaodprocedencia');
                $("#modal_title_procedencia").empty();
                $("#modal_title_procedencia").html("Editar datos de procedencia");
                $("#addProcedencia-modal").modal("show");
            });

            $("body").on("click", ".delete", function () {
                var id = $(this).attr("data-id");
                var nombre = $(this).attr("data-value");
                $("#eliminarprocedencia").text(nombre);
                $("#btndelete").attr("data-id", id);
                $("#delete-modal").modal("show");
            });

            $("body").on("click", ".save", function () {
                var id = $("#btnsave").attr("data-id");
                var tracking = $("#btnsave").attr("data-tracking");
                if (validarProcedencia()) {
                    datos = [
                        id = id,
                        tracking = tracking,
                        remisa = $('#ctl00_contenido_remisa').val(),
                        expediente = $("#ctl00_contenido_expediente").val(),
                        centro = $("#ctl00_contenido_centroprocedencia").val(),
                        estado = $("#ctl00_contenido_estaodprocedencia").val(),
                        interno = $('#ctl00_contenido_trackingid').val()
                    ];

                    Save(datos);
                }
            });

            function validarProcedencia() {
                var esvalido = true;

                if ($("#ctl00_contenido_remisa").val() == null || $("#ctl00_contenido_remisa").val().split(" ").join("") == "") {
                    ShowError("Remisa", "La remisa es obligatoria.");
                    $('#ctl00_contenido_remisa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_remisa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_remisa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_remisa').addClass('valid');
                }

                if ($("#ctl00_contenido_expediente").val() == null || $("#ctl00_contenido_expediente").val().split(" ").join("") == "") {
                    ShowError("Expediente", "El expediente es obligatorio.");
                    $('#ctl00_contenido_expediente').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_expediente').removeClass('valid');
                    esvalido = false;
                }
                else {
                    if (isNaN($("#ctl00_contenido_expediente").val())) {

                        ShowError("Expediente", "El expediente es numérico.");
                        $('#ctl00_contenido_expediente').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_expediente').removeClass('valid');
                        esvalido = false;

                    } else {
                        $('#ctl00_contenido_expediente').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_expediente').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_estaodprocedencia").val() == null || $("#ctl00_contenido_estaodprocedencia").val() == 0) {
                    ShowError("Estado", "El estado es obligatorio.");
                    $('#ctl00_contenido_estaodprocedencia').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_estaodprocedencia').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_estaodprocedencia').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_estaodprocedencia').addClass('valid');
                }

                if ($("#ctl00_contenido_centroprocedencia").val() == null || $("#ctl00_contenido_centroprocedencia").val() == 0) {
                    ShowError("Centro", "El centro es obligatorio.");
                    $('#ctl00_contenido_centroprocedencia').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_centroprocedencia').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_centroprocedencia').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_centroprocedencia').addClass('valid');
                }



                return esvalido;
            }

            function limpiar() {


                $('#ctl00_contenido_remisa').parent().removeClass('state-success');
                $('#ctl00_contenido_remisa').parent().removeClass("state-error");
                $('#ctl00_contenido_expediente').parent().removeClass('state-success');
                $('#ctl00_contenido_expediente').parent().removeClass("state-error");
                $('#ctl00_contenido_estaodprocedencia').parent().removeClass('state-success');
                $('#ctl00_contenido_estaodprocedencia').parent().removeClass("state-error");
                $('#ctl00_contenido_centroprocedencia').parent().removeClass('state-success');
                $('#ctl00_contenido_centroprocedencia').parent().removeClass("state-error");

            }

            function Save(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var tablealias = $('#dt_basic').dataTable();
                            tablealias._fnAjaxUpdate();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El dato de procedencia se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);

                            $("#addProcedencia-modal").modal("hide");
                            ShowSuccess("¡Bien hecho!", "El dato de procedencia se " + resultado.mensaje + " correctamente.");
                        }
                        else {
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("#btndelete").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "personalinformation.aspx/delete",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var tablealias = $('#dt_basic').dataTable();
                            tablealias._fnAjaxUpdate();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El dato de procedencia fue eliminado correctamente.", "</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho!", "El dato de procedencia fue eliminado correctamente");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                        }
                        $("#delete-modal").modal("hide");
                        $('#main').waitMe('hide');
                    }
                });
            });

            $("body").on("click", ".blockitemprocedencia", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblockprocedencia").text(itemnameblock);
                $("#verbprocedencia").text(verb);
                $("#btncontinuarprocedencia").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modalprocedencia").modal("show");
            });

            $("#btncontinuarprocedencia").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "personalinformation.aspx/blockitemprocedencia",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modalprocedencia").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Bien hecho!</strong>" +
                                " El registro  se actualizó correctamente.</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho!", "El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }
                        $('#main').waitMe('hide');
                    }

                });
                var tablealias = $('#dt_basic').dataTable();
                tablealias._fnAjaxUpdate();
            });

            window.table = $('#dt_basicAlias').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basicAlias) {
                        responsiveHelper_dt_basicAlias = new ResponsiveDatatablesHelper($('#dt_basicAlias'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basicAlias.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basicAlias.respond();
                    $('#dt_basicAlias').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "personalinformation.aspx/getAlias",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basicAlias').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        var trackingid = ($('#<%= trackingid.ClientID %>').val());

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = trackingid;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Alias",
                        data: "Alias"
                    },
                    null
                ],
                columnDefs: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "editAlias";
                            var editar = "";
                            var habilitar = "";

                            if (row.Activo) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }

                            var disabled = "";
                            if ($("#<%= editable.ClientID %>").val() == "0") disabled = "disabled";

                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' ' + disabled + '" href="javascript:void(0);" data-id="' + row.Id + '" data-alias = "' + row.Alias + '" data-tracking="' + row.TrackingId + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitemalias ' + disabled + '" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-value = "' + row.Alias + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return editar + habilitar + '<a name="history" class="btn btn-default btn-circle historialalias" href="javascript:void(0);" data-value="' + row.Alias + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                        }
                    }
                ]
            });

            window.emptytableAlias = true;
            window.tableAlias = $("#dt_basichistoryalias").dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                order: [[3, 'asc']],
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_datatable_Alias) {
                        responsiveHelper_datatable_Alias = new ResponsiveDatatablesHelper($("#dt_basichistoryalias"), breakpointAliasDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_datatable_Alias.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_datatable_Alias.respond();
                    $("#dt_basichistoryalias").waitMe("hide");
                },
                ajax: {
                    type: "POST",
                    url: "personalinformation.aspx/getaliaslog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosserverside) {
                        $("#dt_basichistoryalias").waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var centroid = $('#IdAlias').val();
                        parametrosserverside.centroid = centroid;
                        parametrosserverside.todoscancelados = false;
                        parametrosserverside.emptytable = emptytableAlias;
                        return JSON.stringify(parametrosserverside);
                    }
                },
                columns: [
                    {
                        name: "Alias",
                        data: "Alias",
                        ordertable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        ordertable: false
                    },
                    {
                        name: "Creadopor",
                        data: "Creadopor",
                        ordertable: false
                    },
                    {
                        name: "Fec_Movto",
                        data: "Fec_Movto",
                        ordertable: false
                    }
                ],
            });

            $("body").on("click", ".historialalias", function () {
                var id = $(this).attr("data-id");
                $("#IdAlias").val(id);
                var descripcion = $(this).attr("data-value");
                $("#descripcionalias").text(" del alias " + descripcion);

                $("#historyalias-modal").modal("show");
                window.emptytableAlias = false;
                window.tableAlias.api().ajax.reload();
            });
            $("body").on("click", ".addAlias", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_alias").val("");
                $("#btnsaveAlias").attr("data-id", "");//Seguir buscando trackings y cambiarlos por los de detencion
                $("#btnsaveAlias").attr("data-tracking", "");

                $("#modal_title_alias").empty();
                $("#modal_title_alias").html('<i class="fa fa-pencil" +=""></i> Agregar alias/sobrenombre');

                $("#addAlias-modal").modal("show");
                limpiarAlias();
            });

            $("body").on("click", ".editAlias", function () {
                limpiarAlias();
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                $("#btnsaveAlias").attr("data-id", id);
                $("#btnsaveAlias").attr("data-tracking", tracking);

                $("#modal_title_alias").empty();
                $("#modal_title_alias").html("Editar alias/sobrenombre");
                $("#ctl00_contenido_alias").val($(this).attr("data-alias"));
                $("#addAlias-modal").modal("show");
            });

            $("body").on("click", ".deleteAlias", function () {
                var id = $(this).attr("data-id");
                var nombre = $(this).attr("data-value");
                $("#eliminaralias").text(nombre);
                $("#btndeleteAlias").attr("data-id", id);
                $("#deleteAlias-modal").modal("show");
            });


            function validarAlias() {
                var esvalido = true;

                if ($("#ctl00_contenido_alias").val() == null || $("#ctl00_contenido_alias").val().split(" ").join("") == "") {
                    ShowError("Alias", "El alias es obligatorio.");
                    $('#ctl00_contenido_alias').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_alias').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_alias').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_alias').addClass('valid');
                }

                return esvalido;
            }

            function limpiarAlias() {
                $('#ctl00_contenido_alias').parent().removeClass('state-success');
                $('#ctl00_contenido_alias').parent().removeClass("state-error");
            }

            $("body").on("click", ".saveAlias", function () {
                var id = $("#btnsaveAlias").attr("data-id");
                var tracking = $("#btnsaveAlias").attr("data-tracking");
                if (validarAlias()) {
                    datos = [
                        id = id,
                        tracking = tracking,
                        alias = $('#ctl00_contenido_alias').val(),
                        interno = $('#ctl00_contenido_trackingid').val()
                    ];

                    SaveAlias(datos);
                }

            });

            function SaveAlias(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/saveAlias",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El alias se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);

                            $("#addAlias-modal").modal("hide");
                            ShowSuccess("¡Bien hecho!", "El alias se " + resultado.mensaje + " correctamente.");
                            var tablealias = $('#dt_basicAlias').dataTable();
                            tablealias._fnAjaxUpdate();
                        }
                        else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("#btndeleteAlias").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "personalinformation.aspx/deleteAlias",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            var tablealias = $('#dt_basicAlias').dataTable();
                            tablealias._fnAjaxUpdate();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El alias fue eliminado correctamente.", "</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho!", "El alias fue eliminado correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);

                        }
                        $("#deleteAlias-modal").modal("hide");
                        $('#main').waitMe('hide');
                    }
                });
            });

            $("body").on("click", ".blockitemalias", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblockalias").text(itemnameblock);
                $("#verbalias").text(verb);
                $("#btncontinuaralias").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modalalias").modal("show");
            });

            $("#btncontinuaralias").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "personalinformation.aspx/blockitemalias",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {

                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modalalias").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                JSON.parse(data.d).mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho! ", JSON.parse(data.d).mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }
                        $('#main').waitMe('hide');
                    }

                });
                var tablealias = $('#dt_basicAlias').dataTable();
                tablealias._fnAjaxUpdate();
            });

            window.table = $('#dt_basicNombre').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basicNombre) {
                        responsiveHelper_dt_basicNombre = new ResponsiveDatatablesHelper($('#dt_basicNombre'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basicNombre.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basicNombre.respond();
                    $('#dt_basicNombre').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "personalinformation.aspx/getNombre",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basicNombre').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        var trackingid = ($('#<%= trackingid.ClientID %>').val());

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = trackingid;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    null
                ],
                columnDefs: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 5,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "editNombre";
                            var editar = "";
                            var habilitar = "";
                            if (row.Activo) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-nombre = "' + row.Nombre + '" data-paterno = "' + row.Paterno + '" data-materno = "' + row.Materno + '" data-tracking="' + row.TrackingId + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitemnombre" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-value = "' + row.Nombre + ' ' + row.Paterno + ' ' + row.Materno + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return editar + habilitar
                                ;
                        }
                    }
                ]
            });

            $("body").on("click", ".deleteNombre", function () {
                var id = $(this).attr("data-id");
                var nombre = $(this).attr("data-value");
                $("#eliminarnombre").text(nombre);
                $("#btndeleteNombre").attr("data-id", id);
                $("#deleteNombre-modal").modal("show");
            });

            $("body").on("click", ".blockitemnombre", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblocknombre").text(itemnameblock);
                $("#verbnombre").text(verb);
                $("#btncontinuarnombre").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modalnombre").modal("show");
            });

            $("#btncontinuarnombre").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "personalinformation.aspx/blockitemnombre",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modalnombre").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Bien hecho!</strong>" +
                                " El registro  se actualizó correctamente.</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho!", "El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }
                        $('#main').waitMe('hide');
                    }

                });
                var table = $('#dt_basicNombre').dataTable();
                table._fnAjaxUpdate();
            });

            $("body").on("click", ".addNombre", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_otro").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#btnsaveNombre").attr("data-id", "");
                $("#btnsaveNombre").attr("data-tracking", "");

                $("#modal-title-otro-nombre").empty();
                $("#modal-title-otro-nombre").html("Agregar nombre");
                limpiarNombre();
                $("#addNombre-modal").modal("show");
            });

            $("body").on("click", ".editNombre", function () {
                limpiarNombre();
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                $("#btnsaveNombre").attr("data-id", id);
                $("#btnsaveNombre").attr("data-tracking", tracking);
                $("#ctl00_contenido_otro").val($(this).attr("data-nombre"));
                $("#ctl00_contenido_paterno").val($(this).attr("data-paterno"));
                $("#ctl00_contenido_materno").val($(this).attr("data-materno"));

                $("#modal-title-otro-nombre").empty();
                $("#modal-title-otro-nombre").html("Editar nombre");
                $("#addNombre-modal").modal("show");
            });

            function validarNombre() {
                var esvalido = true;

                if ($("#ctl00_contenido_otro").val() == null || $("#ctl00_contenido_otro").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#ctl00_contenido_otro').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_otro').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_otro').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_otro').addClass('valid');
                }

                if ($("#ctl00_contenido_paterno").val() == null || $("#ctl00_contenido_paterno").val().split(" ").join("") == "") {
                    ShowError("Apellido paterno", "El apellido paterno es obligatorio.");
                    $('#ctl00_contenido_paterno').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_paterno').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_paterno').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_paterno').addClass('valid');
                }

                if ($("#ctl00_contenido_materno").val() == null || $("#ctl00_contenido_materno").val().split(" ").join("") == "") {
                    ShowError("Apellido materno", "El apellido materno es obligatorio.");
                    $('#ctl00_contenido_materno').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_materno').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_materno').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_materno').addClass('valid');
                }

                return esvalido;
            }

            function limpiarNombre() {
                $('#ctl00_contenido_otro').parent().removeClass('state-success');
                $('#ctl00_contenido_otro').parent().removeClass("state-error");
                $('#ctl00_contenido_paterno').parent().removeClass('state-success');
                $('#ctl00_contenido_paterno').parent().removeClass("state-error");
                $('#ctl00_contenido_materno').parent().removeClass('state-success');
                $('#ctl00_contenido_materno').parent().removeClass("state-error");
            }

            $("body").on("click", ".saveNombre", function () {
                var id = $("#btnsaveNombre").attr("data-id");
                var tracking = $("#btnsaveNombre").attr("data-tracking");
                if (validarNombre()) {
                    datos = [
                        id = id,
                        tracking = tracking,
                        nombre = $('#ctl00_contenido_otro').val(),
                        paterno = $('#ctl00_contenido_paterno').val(),
                        materno = $('#ctl00_contenido_materno').val(),
                        interno = $('#ctl00_contenido_trackingid').val()
                    ];

                    SaveNombre(datos);
                }
            });

            function SaveNombre(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/saveNombre",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El nombre se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);

                            $("#addNombre-modal").modal("hide");
                            ShowSuccess("¡Bien hecho!", "El nombre se " + resultado.mensaje + " correctamente.");
                        }
                        else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", resultado.mensaje + " Si el problema persiste, contacte al personal de soporte técnico.");
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("#btndeleteNombre").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "personalinformation.aspx/deleteNombre",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El nombre fue eliminado correctamente.", "</div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho!", "El nombre fue eliminado correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);

                        }
                        $("#deleteNombre-modal").modal("hide");
                        $('#main').waitMe('hide');
                    }
                });
            });

            $("body").on("click", ".saveDomicilio", function () {

                datos = [
                    id = $('#ctl00_contenido_idDomicilio').val(),
                    trackingd = $('#ctl00_contenido_trackingDomicilio').val(),
                    called = $('#ctl00_contenido_calledomicilio').val(),
                    numerod = $('#ctl00_contenido_numerodomicilio').val(),
                    //coloniad = $("#ctl00_contenido_coloniadomicilio").val(),
                    //cpd = $("#ctl00_contenido_cpdomicilio").val(),
                    telefonod = $("#ctl00_contenido_telefonodomicilio").val(),
                    paisd = $("#ctl00_contenido_paisdomicilio").val(),
                    estadod = $("#ctl00_contenido_estadodomicilio").val(),
                    municipiod = $("#ctl00_contenido_municipiodomicilio").val(),
                    interno = $('#ctl00_contenido_trackingid').val(),
                    idn = $('#ctl00_contenido_idNacimiento').val(),
                    trackingn = $('#ctl00_contenido_trackingNacimiento').val(),
                    callen = $("#ctl00_contenido_callelugar").val(),
                    numeron = $("#ctl00_contenido_numerolugar").val(),
                    //colonian = $("#ctl00_contenido_colonialugar").val(),
                    //cpn = $("#ctl00_contenido_cplugar").val(),
                    telefonon = $("#ctl00_contenido_telefonolugar").val(),
                    paisn = $('#ctl00_contenido_paisnacimineto').val(),
                    estadon = $("#ctl00_contenido_estadonacimiento").val(),
                    municipion = $("#ctl00_contenido_municipionacimiento").val(),
                    interno = $('#ctl00_contenido_trackingid').val(),
                    localidad = $('#ctl00_contenido_localidad').val(),
                    localidan = $('#ctl00_contenido_localidadn').val(),
                    coloniaId = $("#ctl00_contenido_coloniaSelect").val(),
                    coloniaIdNacimiento = $("#ctl00_contenido_coloniaSelectNacimiento").val(),
                    latituddomicilio = $("#latitudd").val(),
                    longituddomicilio = $("#longitudd").val()
                ];

                if (validarDN()) {
                    SaveDN(datos);
                }

            });

            function validarDN() {
                var esvalido = true;

                if ($("#ctl00_contenido_paisdomicilio option:selected").val() == 73) {
                    if ($("#ctl00_contenido_calledomicilio").val() == null || $("#ctl00_contenido_calledomicilio").val().split(" ").join("") == "") {
                        ShowError("Calle domicilio", "La calle es obligatoria.");
                        $('#ctl00_contenido_calledomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_calledomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_calledomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_calledomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_estadodomicilio").val() == null || $("#ctl00_contenido_estadodomicilio").val().split(" ").join("") == "" || $("#ctl00_contenido_estadodomicilio").val() == "0") {
                        ShowError("Estado domicilio", "El estado es obligatorio.");
                        $('#ctl00_contenido_estadodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_estadodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_estadodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_estadodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_municipiodomicilio").val() == null || $("#ctl00_contenido_municipiodomicilio").val().split(" ").join("") == "" || $("#ctl00_contenido_municipiodomicilio").val() == "0") {
                        ShowError("Municipio domicilio", "El municipio es obligatorio.");
                        $('#ctl00_contenido_municipiodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_municipiodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_municipiodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_municipiodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_numerodomicilio").val() == null || $("#ctl00_contenido_numerodomicilio").val().split(" ").join("") == "") {
                        ShowError("Número domicilio", "El número es obligatorio.");
                        $('#ctl00_contenido_numerodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_numerodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        var regex = new RegExp('\\d{1,}', 'g');
                        if (!regex.test($("#ctl00_contenido_numerodomicilio").val())) {
                            ShowError("Número domicilio", "El número del domicilio no contiene ningún número.");
                            $('#ctl00_contenido_numerodomicilio').parent().removeClass('state-success').addClass("state-error");
                            $('#ctl00_contenido_numerodomicilio').removeClass('valid');
                            esvalido = false;
                        }
                        else {
                            $('#ctl00_contenido_numerodomicilio').parent().removeClass("state-error").addClass('state-success');
                            $('#ctl00_contenido_numerodomicilio').addClass('valid');
                        }
                    }
                    
                    if ($("#ctl00_contenido_coloniaSelect").val() == null || $("#ctl00_contenido_coloniaSelect").val().split(" ").join("") == "" || $("#ctl00_contenido_coloniaSelect").val()=="0") {
                        ShowError("Colonia domicilio", "La colonia actual es obligatoria.");
                        $('#ctl00_contenido_coloniaSelect').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_coloniaSelect').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_coloniaSelect').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_coloniaSelect').addClass('valid');
                    }

                    if ($("#ctl00_contenido_cpdomicilio").val() == null || $("#ctl00_contenido_cpdomicilio").val().split(" ").join("") == "") {
                        ShowError("C.P. del domicilio", "El C.P. es obligatorio.");
                        $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_cpdomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        if (isNaN($("#ctl00_contenido_cpdomicilio").val())) {
                            ShowError("C.P. del domicilio", "El C.P. es numérico.");
                            $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success').addClass("state-error");
                            $('#ctl00_contenido_cpdomicilio').removeClass('valid');
                            esvalido = false;
                        }
                        else {
                            $('#ctl00_contenido_cpdomicilio').parent().removeClass("state-error").addClass('state-success');
                            $('#ctl00_contenido_cpdomicilio').addClass('valid');
                        }
                    }

                    /* SE HACE OPCIONAL LA CAPTURA DE UN TELEFONO PARA EL DOMICILIO */
                    /*
                    if ($("#ctl00_contenido_telefonodomicilio").val() == null || $("#ctl00_contenido_telefonodomicilio").val().split(" ").join("") == "") {
                        ShowError("Teléfono domicilio", "El teléfono es obligatorio.");
                        $('#ctl00_contenido_telefonodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_telefonodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_telefonodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_telefonodomicilio').addClass('valid');
                    }
                    */
                    $('#ctl00_contenido_telefonodomicilio').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_telefonodomicilio').addClass('valid');
                    /* TERMINA HACER OPCIONAL LA CAPTURA DEL TELEFONO DEL DOMICILIO */

                    if ($("#ctl00_contenido_paisdomicilio").val() == null || $("#ctl00_contenido_paisdomicilio").val() == 0) {
                        ShowError("País domicilio", "El país es obligatorio.");
                        $('#ctl00_contenido_paisdomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_paisdomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_paisdomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_paisdomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_estadodomicilio").val() == null || $("#ctl00_contenido_estadodomicilio").val() == 0) {
                        ShowError("Estado", "El estado es obligatorio.");
                        $('#ctl00_contenido_estadodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_estadodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_estadodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_estadodomicilio').addClass('valid');
                    }

                    //if ($("#ctl00_contenido_localidad").val() == null || $("#ctl00_contenido_localidad").val().split(" ").join("") == "") {
                    //      alert("jola");
                    //    ShowError("Teléfono domicilio", "El teléfono es obligatorio.");
                    //    $('#ctl00_contenido_localidad').parent().removeClass('state-success').addClass("state-error");
                    //    $('#ctl00_contenido_localidad').removeClass('valid');
                    //    esvalido = false;
                    //}
                    //else {
                    //    $('#ctl00_contenido_localidad').parent().removeClass("state-error").addClass('state-success');
                    //    $('#ctl00_contenido_localidad').addClass('valid');
                    //}
                }
                else {
                    if ($("#ctl00_contenido_calledomicilio").val() == null || $("#ctl00_contenido_calledomicilio").val().split(" ").join("") == "") {
                        ShowError("Calle domicilio", "La calle es obligatoria.");
                        $('#ctl00_contenido_calledomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_calledomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_calledomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_calledomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_numerodomicilio").val() == null || $("#ctl00_contenido_numerodomicilio").val().split(" ").join("") == "") {
                        ShowError("Número domicilio", "El número es obligatorio.");
                        $('#ctl00_contenido_numerodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_numerodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_numerodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_numerodomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_cpdomicilio").val() == null || $("#ctl00_contenido_cpdomicilio").val().split(" ").join("") == "") {
                        ShowError("C.P. del domicilio", "El C.P. es obligatorio.");
                        $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_cpdomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        if (isNaN($("#ctl00_contenido_cpdomicilio").val())) {
                            ShowError("C.P. del domicilio", "El C.P. es numérico.");
                            $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success').addClass("state-error");
                            $('#ctl00_contenido_cpdomicilio').removeClass('valid');
                            esvalido = false;
                        }
                        else {
                            $('#ctl00_contenido_cpdomicilio').parent().removeClass("state-error").addClass('state-success');
                            $('#ctl00_contenido_cpdomicilio').addClass('valid');
                        }
                    }

                    /* SE HACE OPCIONAL LA CAPTURA DEL TELEFONO DEL DOMICILIO */
                    /*
                    if ($("#ctl00_contenido_telefonodomicilio").val() == null || $("#ctl00_contenido_telefonodomicilio").val().split(" ").join("") == "") {
                        ShowError("Teléfono domicilio", "El teléfono es obligatorio.");
                        $('#ctl00_contenido_telefonodomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_telefonodomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_telefonodomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_telefonodomicilio').addClass('valid');
                    }
                    */
                    $('#ctl00_contenido_telefonodomicilio').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_telefonodomicilio').addClass('valid');
                    /* TERMINA HACER OPCIONAL LA CAPTURA DEL TELEFONO DEL DOMICILIO */

                    if ($("#ctl00_contenido_paisdomicilio").val() == null || $("#ctl00_contenido_paisdomicilio").val() == 0) {
                        ShowError("País domicilio", "El país es obligatorio.");
                        $('#ctl00_contenido_paisdomicilio').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_paisdomicilio').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_paisdomicilio').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_paisdomicilio').addClass('valid');
                    }

                    if ($("#ctl00_contenido_localidad").val() == null || $("#ctl00_contenido_localidad").val().split(" ").join("") == "") {
                        ShowError("Localidad domicilio", "La localidad es obligatoria.");
                        $('#ctl00_contenido_localidad').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_localidad').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_localidad').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_localidad').addClass('valid');
                    }
                }



                if ($("#ctl00_contenido_numerolugar").val() != null && $("#ctl00_contenido_numerolugar").val().trim() != "") {
                    var regex = new RegExp('\\d{1,}', 'g');
                    if (!regex.test($("#ctl00_contenido_numerolugar").val())) {
                        ShowError("Número nacimiento", "El número del domicilio de nacimiento no contiene ningún número.");
                        $('#ctl00_contenido_numerolugar').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_numerolugar').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_numerolugar').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_numerolugar').addClass('valid');
                    }
                }

                //Si el pais seleccionado es mexico para nacimiento
                //if ($("#ctl00_contenido_paisnacimineto option:selected").val() == 73) {
                //    if ($("#ctl00_contenido_callelugar").val() == null || $("#ctl00_contenido_callelugar").val().split(" ").join("") == "") {
                //        ShowError("Calle nacimiento", "La calle es obligatoria.");
                //        $('#ctl00_contenido_callelugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_callelugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_callelugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_callelugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_numerolugar").val() == null || $("#ctl00_contenido_numerolugar").val().split(" ").join("") == "") {
                //        ShowError("Número nacimiento", "El número es obligatorio.");
                //        $('#ctl00_contenido_numerolugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_numerolugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_numerolugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_numerolugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_coloniaSelectNacimiento").val() == null || $("#ctl00_contenido_coloniaSelectNacimiento").val().split(" ").join("") == "") {
                //        ShowError("Colonia nacimiento", "La colonia de nacimiento es obligatoria.");
                //        $('#ctl00_contenido_coloniaSelectNacimiento').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_coloniaSelectNacimiento').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_coloniaSelectNacimiento').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_coloniaSelectNacimiento').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_cplugar").val() == null || $("#ctl00_contenido_cplugar").val().split(" ").join("") == "") {
                //        ShowError("C.P. nacimiento", "El C.P. es obligatorio.");
                //        $('#ctl00_contenido_cplugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_cplugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        if (isNaN($("#ctl00_contenido_cplugar").val())) {
                //            ShowError("C.P. nacimiento", "El C.P. es numérico.");
                //            $('#ctl00_contenido_cplugar').parent().removeClass('state-success').addClass("state-error");
                //            $('#ctl00_contenido_cplugar').removeClass('valid');
                //            esvalido = false;
                //        }
                //        else {
                //            $('#ctl00_contenido_cplugar').parent().removeClass("state-error").addClass('state-success');
                //            $('#ctl00_contenido_cplugar').addClass('valid');
                //        }
                //    }

                //    if ($("#ctl00_contenido_telefonolugar").val() == null || $("#ctl00_contenido_telefonolugar").val().split(" ").join("") == "") {
                //        ShowError("Teléfono nacimiento", "El teléfono es obligatorio.");
                //        $('#ctl00_contenido_telefonolugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_telefonolugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_telefonolugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_telefonolugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_paisnacimineto").val() == null || $("#ctl00_contenido_paisnacimineto").val() == 0) {
                //        ShowError("País nacimiento", "El país es obligatorio.");
                //        $('#ctl00_contenido_paisnacimineto').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_paisnacimineto').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_paisnacimineto').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_paisnacimineto').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_estadonacimiento").val() == null || $("#ctl00_contenido_estadonacimiento").val() == 0 || $("#ctl00_contenido_estadonacimiento").val() == "Seleccione") {
                //        ShowError("Estado nacimiento", "El estado es obligatorio.");
                //        $('#ctl00_contenido_estadonacimiento').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_estadonacimiento').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_estadonacimiento').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_estadonacimiento').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_municipionacimiento").val() == null || $("#ctl00_contenido_municipionacimiento").val() == 0) {
                //        ShowError("Municipio nacimiento", "El municipio es obligatorio.");
                //        $('#ctl00_contenido_municipionacimiento').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_municipionacimiento').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_municipionacimiento').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_municipionacimiento').addClass('valid');
                //    }

                //if ($("#ctl00_contenido_localidadn").val() == null || $("#ctl00_contenido_localidadn").val().split(" ").join("") == "") {
                //    ShowError("Teléfono domicilio", "El teléfono es obligatorio.");
                //    $('#ctl00_contenido_localidadn').parent().removeClass('state-success').addClass("state-error");
                //    $('#ctl00_contenido_localidadn').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#ctl00_contenido_localidadn').parent().removeClass("state-error").addClass('state-success');
                //    $('#ctl00_contenido_localidadn').addClass('valid');
                //}

                //} //Si se ha seleccionado un pais diferente a mexico
                //else {
                //    if ($("#ctl00_contenido_callelugar").val() == null || $("#ctl00_contenido_callelugar").val().split(" ").join("") == "") {
                //        ShowError("Calle nacimiento", "La calle es obligatoria.");
                //        $('#ctl00_contenido_callelugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_callelugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_callelugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_callelugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_numerolugar").val() == null || $("#ctl00_contenido_numerolugar").val().split(" ").join("") == "") {
                //        ShowError("Número nacimiento", "El número es obligatorio.");
                //        $('#ctl00_contenido_numerolugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_numerolugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_numerolugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_numerolugar').addClass('valid');
                //    }

                //if ($("#ctl00_contenido_cplugar").val() == null || $("#ctl00_contenido_cplugar").val().split(" ").join("") == "") {
                //    ShowError("C.P. nacimiento", "El C.P. es obligatorio.");
                //    $('#ctl00_contenido_cplugar').parent().removeClass('state-success').addClass("state-error");
                //    $('#ctl00_contenido_cplugar').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    if (isNaN($("#ctl00_contenido_cplugar").val())) {
                //    ShowError("C.P. nacimiento", "El C.P. es numérico.");
                //    $('#ctl00_contenido_cplugar').parent().removeClass('state-success').addClass("state-error");
                //    $('#ctl00_contenido_cplugar').removeClass('valid');
                //    esvalido = false;

                //    } else {
                //        $('#ctl00_contenido_cplugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_cplugar').addClass('valid');
                //    }
                //}

                //    if ($("#ctl00_contenido_telefonolugar").val() == null || $("#ctl00_contenido_telefonolugar").val().split(" ").join("") == "") {
                //        ShowError("Teléfono nacimiento", "El teléfono es obligatorio.");
                //        $('#ctl00_contenido_telefonolugar').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_telefonolugar').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_telefonolugar').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_telefonolugar').addClass('valid');
                //    }

                //    if ($("#ctl00_contenido_localidadn").val() == null || $("#ctl00_contenido_localidadn").val().split(" ").join("") == "") {
                //        ShowError("Localidad nacimiento", "La localidad es obligatoria.");
                //        $('#ctl00_contenido_localidadn').parent().removeClass('state-success').addClass("state-error");
                //        $('#ctl00_contenido_localidadn').removeClass('valid');
                //        esvalido = false;
                //    }
                //    else {
                //        $('#ctl00_contenido_localidadn').parent().removeClass("state-error").addClass('state-success');
                //        $('#ctl00_contenido_localidadn').addClass('valid');
                //    }
                //}

                return esvalido;
            }

            function limpiarDN() {
                $('#ctl00_contenido_calledomicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_calledomicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_numerodomicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_numerodomicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_coloniadomicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_coloniadomicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_cpdomicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_cpdomicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_telefonodomicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_telefonodomicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_paisdomicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_paisdomicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_estadodomicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_estadodomicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_municipiodomicilio').parent().removeClass('state-success');
                $('#ctl00_contenido_municipiodomicilio').parent().removeClass("state-error");
                $('#ctl00_contenido_callelugar').parent().removeClass('state-success');
                $('#ctl00_contenido_callelugar').parent().removeClass("state-error");
                $('#ctl00_contenido_numerolugar').parent().removeClass('state-success');
                $('#ctl00_contenido_numerolugar').parent().removeClass("state-error");
                $('#ctl00_contenido_colonialugar').parent().removeClass('state-success');
                $('#ctl00_contenido_colonialugar').parent().removeClass("state-error");
                $('#ctl00_contenido_cplugar').parent().removeClass('state-success');
                $('#ctl00_contenido_cplugar').parent().removeClass("state-error");
                $('#ctl00_contenido_telefonolugar').parent().removeClass('state-success');
                $('#ctl00_contenido_telefonolugar').parent().removeClass("state-error");
                $('#ctl00_contenido_paisnacimineto').parent().removeClass('state-success');
                $('#ctl00_contenido_paisnacimineto').parent().removeClass("state-error");
                $('#ctl00_contenido_estadonacimiento').parent().removeClass('state-success');
                $('#ctl00_contenido_estadonacimiento').parent().removeClass("state-error");
                $('#ctl00_contenido_municipionacimiento').parent().removeClass('state-success');
                $('#ctl00_contenido_municipionacimiento').parent().removeClass("state-error");
                $('#ctl00_contenido_localidad').parent().removeClass('state-success');
                $('#ctl00_contenido_localidad').parent().removeClass("state-error");
                $('#ctl00_contenido_localidadn').parent().removeClass('state-success');
                $('#ctl00_contenido_localidadn').parent().removeClass("state-error");
            }

            $("#ctl00_contenido_paisdomicilio").change(function () {

                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                  
                
                if ($("#ctl00_contenido_paisdomicilio option:selected").val() != 73) {
                    $('#sectionlocalidad').show();
                    $('#sectionmunicipio').hide();
                    $('#sectionestado').hide();
                    $('#sectioncoloniaDomicilio').hide();
                    $('#sectioncpdomicilio').hide();
                    $("#ctl00_contenido_cpdomicilio").removeAttr("disabled");
                }
                else {
                    
                    $('#sectionestado').show();
                    $('#sectionmunicipio').show();
                    $('#sectioncoloniaDomicilio').show();
                    $('#sectionlocalidad').hide();
                    $('#sectioncpdomicilio').show();
                    $("#ctl00_contenido_cpdomicilio").attr("disabled", "disabled");
                    CargarEstado(0, '#ctl00_contenido_estadodomicilio', $("#ctl00_contenido_paisdomicilio").val());
                }
            });

            $("#ctl00_contenido_estadodomicilio").change(function () {
                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                CargarMunicipio(0, '#ctl00_contenido_municipiodomicilio', $("#ctl00_contenido_estadodomicilio").val());
            });

            $("#ctl00_contenido_paisnacimineto").change(function () {
                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                if ($("#ctl00_contenido_paisnacimineto option:selected").val() != 73) {
                    $('#sectionlocalidadn').show();
                    $('#sectionmunicipion').hide();
                    $('#sectionestadon').hide();
                    $('#sectioncoloniaNacimiento').hide();
                    $('#sectioncpnacimiento').hide();
                    $("#ctl00_contenido_cplugar").removeAttr("disabled");
                }
                else {
                    $('#sectionestadon').show();
                    $('#sectionmunicipion').show();
                    $('#sectionlocalidadn').hide();
                    $('#sectioncoloniaNacimiento').show();
                    $('#sectioncpnacimiento').show();
                    $("#ctl00_contenido_cplugar").attr("disabled", "disabled");
                    CargarEstado(0, '#ctl00_contenido_estadonacimiento', $("#ctl00_contenido_paisnacimineto").val());
                }
            });

            $("#ctl00_contenido_estadonacimiento").change(function () {
                if ($("#<%= editable.ClientID%>").val() == "0")
                    return;
                CargarMunicipio(0, '#ctl00_contenido_municipionacimiento', $("#ctl00_contenido_estadonacimiento").val());
            });

            function SaveDN(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/saveDN",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " con éxito.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " con éxito.");
                            limpiarDN();

                            $('#<%= trackingDomicilio.ClientID %>').val(resultado.TrackingIddomicilio);
                            $('#<%= idDomicilio.ClientID %>').val(resultado.Iddomicilio);
                            $('#<%= trackingNacimiento.ClientID %>').val(resultado.TrackingIdnacimiento);
                            $('#<%= idNacimiento.ClientID %>').val(resultado.Idnacimiento);

                            var latituddomicilio = $("#latitudd").val();
                            var longituddomicilio = $("#longitudd").val();
                        }
                        else {
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                        limpiarDN();
                        $('#main').waitMe('hide');
                        var param = RequestQueryString("tracking");
                        $('#<%= trackingid.ClientID %>').val(param);
                        if (param != undefined) {

                            CargarDatos(param);
                        }
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function init() {
                var param = RequestQueryString("tracking");
                $('#<%= trackingid.ClientID %>').val(param);
                if (param != undefined) {
                    CargarDatos(param);
                }

                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addprocedencia").show();
                    $("#addnombre").show();

                    if ($("#<%= editable.ClientID %>").val() == "0") {
                        $("#addalias").hide();
                        $(".saveGeneral").hide();
                        $(".saveDomicilio").hide();
                    }
                    else {
                        $("#addalias").show();
                        $(".saveGeneral").show();
                        $(".saveDomicilio").show();
                    }
                }
                else {
                    $("textarea,input, select").each(function (index, element) { $(this).attr('disabled', true); });
                }

                if ($("#ctl00_contenido_Ingreso").val() == "true") {
                    $("#linkingreso").show();
                }
                if ($("#ctl00_contenido_Antropometria").val() == "true") {
                    $("#linkantropometria").show();
                }
                if ($("#ctl00_contenido_Señas_Particulares").val() == "true") {
                    $("#linkseñas").show();
                }
                if ($("#ctl00_contenido_Familiares").val() == "true") {
                    $("#linkfamiliares").show();
                }
                if ($("#ctl00_contenido_Estudios_Criminologicos").val() == "true") {
                    $("#linkestudios").show();
                }
                if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                    $("#linkexpediente").show();
                }
                if ($("#ctl00_contenido_Adicciones").val() == "true") {
                    $("#linkadicciones").show();
                }

                $("#linkInfoDetencion").show();
                $("#linkinformacion").show();
            }
            
            //$('#ctl00_contenido_fecha').mask('99/99/9999', { placeholder: "dd/mm/yyyy" });

            $("#ctl00_contenido_expediente").keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });


            $("#ctl00_contenido_expediente").keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });

            $("#ctl00_contenido_cpdomicilio").keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });

            $("#ctl00_contenido_cplugar").keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });

            $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
                var param = RequestQueryString("tracking");
                CargarDatosModal(param);
            });

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }

                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

            $("#<%= salarioSemanal.ClientID %>").on({
                "focus": function (event) {
                    $(event.target).select();
                },
                "keyup": function (event) {
                    $(event.target).val(function (index, value) {
                        return '$' + value.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }
            });

            $("body").on("click", ".ligarNuevoEvento", function () {
                limpiar_evento_reciente_modal_colores();
                $("#eventoRecienteLigar").val("");
                $("#eventoLigar").val("");
                $("#ctl00_contenido_fileUpload").val('').clone(true);
                cargarEventoReciente(0);
                $("#evento-reciente-modal").modal("show");
            });

            $("#eventoRecienteLigar").change(function () {
                cargarEvento($("#eventoRecienteLigar").val());
            });

            $("#btncancelevento").click(function () {
                limpiar_evento_reciente_modal();
                $("#evento-reciente-modal").modal("hide");
            });

            $("#btncanceleventoX").click(function () {
                limpiar_evento_reciente_modal();
                $("#evento-reciente-modal").modal("hide");
            });

            function limpiar_evento_reciente_modal() {
                $("#idsexo").text("");
                $("#idedad").text("");

                $("#eventoLigar").val("0");
                $("#eventoLigar").empty();
                $("#eventoRecienteLigar").val("0");
                $("#eventoRecienteLigar").empty();
            }

            $("body").on("click", ".saveDetenidoEventoReciente", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar_evento_reciente()) {
                    GuardarDetenidoEventoReciente();
                }
            });

            function validar_evento_reciente() {
                var esvalido = true;

                if ($("#eventoRecienteLigar").val() == null || $("#eventoRecienteLigar").val() == 0) {
                    ShowError("Evento reciente", "El evento reciente es obligatorio.");
                    $('#eventoRecienteLigar').parent().removeClass('state-success').addClass("state-error");
                    $('#eventoRecienteLigar').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#eventoRecienteLigar').parent().removeClass("state-error").addClass('state-success');
                    $('#eventoRecienteLigar').addClass('valid');
                }

                if ($("#eventoLigar").val() == null || $("#eventoLigar").val() == 0) {
                    ShowError("Evento", "El evento es obligatorio.");
                    $('#eventoLigar').parent().removeClass('state-success').addClass("state-error");
                    $('#eventoLigar').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#eventoLigar').parent().removeClass("state-error").addClass('state-success');
                    $('#eventoLigar').addClass('valid');
                }

                return esvalido;
            }

            function GuardarDetenidoEventoReciente() {
                startLoading();
                var datos = ObtenerValoresDetenidoNuevoEvento();

                $.ajax({
                    type: "POST",
                    url: "entry.aspx/saveDetenidoNuevoEvento",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso && resultado.alerta == false) {
                            limpiar_evento_reciente_modal_colores();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del detenido se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del detenido se " + resultado.mensaje + " correctamente.");
                            $('#main').waitMe('hide');

                            limpiar_evento_reciente_modal();
                            $("#evento-reciente-modal").modal("hide");

                            //location.href = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Registry/entry.aspx?tracking="+resultado.id;
                            location.href = "entry.aspx?tracking=" + resultado.id + "&editable=1";
                        }
                        else if (resultado.exitoso && resultado.alerta == true) {
                            limpiar_evento_reciente_modal_colores();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-times'></i><strong>Atención! </strong>" +
                                resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowAlert("Atención!", resultado.mensaje);
                            $('#main').waitMe('hide');

                            limpiar_evento_reciente_modal();
                            $("#evento-reciente-modal").modal("hide");
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            function ObtenerValoresDetenidoNuevoEvento() {
                var param = RequestQueryString("tracking");

                var datos = [
                    eventoId = $('#eventoLigar').val(),
                    detenido = param
                ];

                return datos;
            }

            function limpiar_evento_reciente_modal_colores() {
                $('#eventoRecienteLigar').parent().removeClass('state-success');
                $('#eventoRecienteLigar').parent().removeClass("state-error");
                $('#eventoLigar').parent().removeClass("state-success");
                $('#eventoLigar').parent().removeClass("state-error");
            }

            function cargarEventoReciente(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/getEventoReciente",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#eventoRecienteLigar');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Evento Reciente]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de eventos recientes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function cargarEvento(id) {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/getEventos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $('#eventoLigar');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Evento]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc.substring(0, 85) + "...", item.Id));
                        });

                        $(Dropdown).trigger('change');
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de eventos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }            

            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 443px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');                                
                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 216px)");
            });

        });
    </script>
    <script>
        function loadMap() {
            if ($('#latitudd').val() == "" || $('#longitudd').val() == "") {



                var lat;
                var lon;
                $.ajax({
                    type: "POST",
                    url: "personalinformation.aspx/getpositiobycontract",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    data: JSON.stringify({ LlamadaId: "" }),
                    cache: false,
                    success: function (response) {
                        response = JSON.parse(response.d);

                        if (response.exitoso) {

                            lat = response.obj.Latitud;

                            lon = response.obj.Longitud;



                        } else {

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });

                setTimeout(function () {
                    mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
                    var coordinates = document.getElementById('coordinates');
                    function onDragEnd() {
                        var lngLat = marker.getLngLat();
                        coordinates.style.display = 'block';
                        coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                        let x = lngLat.lng;
                        x = x.toString();
                        x = x.substring(0, x.length - 3);
                        $('#longitudd').val(x);

                        x = lngLat.lat;
                        x = x.toString();
                        x = x.substring(0, x.length - 3);
                        $('#latitudd').val(x);
                    }

                    var map = new mapboxgl.Map({
                        container: 'mapid',
                        style: 'mapbox://styles/mapbox/streets-v11',
                        center: [lon, lat],
                        zoom: 16
                    });
                    var marker = new mapboxgl.Marker({
                        draggable: true
                    })
                        .setLngLat([lon, lat])
                        .addTo(map);
                    map.addControl(new mapboxgl.NavigationControl());
                    marker.on('dragend', onDragEnd);
                }, 1000);
            }

            else {


                setTimeout(function () {
                    var mymap = L.map('mapid').setView([$('#latitudd').val(), $('#longitudd').val()], 13);



                    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        maxZoom: 18,
                        id: 'mapbox.streets',
                        accessToken: 'pk.eyJ1IjoiYXNlc29ydXNpdGVjaCIsImEiOiJjanhtbzh6aW0wNXIwM2NvNjVweHlnd2JxIn0.YbBuq1IIm9cVDgg64NaxcQ'
                    }).addTo(mymap);


                    var marker = L.marker([$('#latitudd').val(), $('#longitudd').val()]).addTo(mymap);
                }, 1000);
            }
        }

    </script>
</asp:Content>
