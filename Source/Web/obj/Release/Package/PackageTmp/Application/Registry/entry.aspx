﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="entry.aspx.cs" Inherits="Web.Application.Registry.entry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
   <li><a href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Registro</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>
        select:disabled {
            background: #eee;
            cursor: not-allowed;
        }        
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }
        td.strikeout {
            text-decoration: line-through;
        }
    </style>
    <div class="scroll">
        <!--<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="glyphicon glyphicon-edit "></i>
                    Registro 
                </h1>
            </div>
        </div>-->
        <div style="display:none;">
            <input type="text" />
            <input type="password" />
        </div>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <div class="row padding-bottom-10" style="display: inline-block; float: right">
            <a style="display: none;font-size:smaller" class="btn btn-default btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
            <a style="display: none;font-size:smaller" class="btn btn-success txt-color-white btn-lg" id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a> 
            <a style="display: none;font-size:smaller" class="btn btn-info btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
            <a style="font-size:smaller" class="btn btn-primary txt-color-white btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
            <a style="display: none;font-size:smaller" class="btn btn-warning btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
            <a style="display: none;font-size:smaller" class="btn bg-color-purple txt-color-white btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
            <a style="display: none;font-size:smaller" class="btn bg-color-yellow txt-color-white btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
            <a style="display: none;font-size:smaller" class="btn bg-color-red txt-color-white btn-lg" id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
            <a style="margin-right:30px; font-size:smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa fa-paw"></i> Biométricos</a>
            <div style="clear:both"></div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken"  id="wid-entrya-1"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="glyphicon  glyphicon-edit"></i></span>
                            <h2>Registro </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div id="smart-form-register-f" class="smart-form">
                                    <header>
                                        <div class="row">
                                            <div class="col col-9">Alta de detenido</div>
                                            <div class="col col-3">
                                                <label id="idcuip" aling="right" runat="server"><strong></strong></label>
                                            </div>
                                        </div>
                                    </header>
                                    <fieldset>
                                        <div class="row" id="Camposxl">
                                            <section class="col col-3">
                                                <label class="label">Fecha y hora de ingreso <a style="color:red">*</a></label>
                                                <label class="input">
                                                    <label class='input-group date' id='autorizaciondatetimepicker' style="width:100%">
                                                        <input type="text" name="autorizacion" id="fecha" class='input-group date alptext' placeholder="Fecha y hora de ingreso" runat="server" disabled="disabled" />
                                                        <i class="icon-append fa fa-calendar"></i>
                                                        <%--<span class="input-group-addon" id="seleccionarFecha"><span class="glyphicon glyphicon-calendar"></span>
                                                        </span>--%>
                                                    </label>
                                                </label>
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">No. remisión</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-hashtag"></i>
                                                    <input type="text" name="expediente" runat="server" id="expediente" placeholder="No. de remisión" class="integer alptext" maxlength="13" disabled="disabled"/>
                                                </label>
                                            </section>
                                            <section class="col col-6" style="display:none;">
                                                <label class="label">Institución a disposición <a style="color:red">*</a></label>
                                                <label class="select">
                                                    <select name="centro" id="centro" runat="server" disabled="disabled"></select>
                                                    <i></i>
                                                </label>
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">Nombre(s) <a style="color:red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="nombre" runat="server" id="nombre" placeholder="Nombre" maxlength="50" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa nombre(s).</b>
                                                </label>
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">Apellido paterno <a style="color:red">*</a> </label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="apellido_paterno" runat="server" id="paterno" placeholder="Apellido paterno" maxlength="50" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el apellido paterno.</b>
                                                </label>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-3" style="display:none">
                                                <label class="label">Num. código de barras (NCP) <a style="color:red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-barcode"></i>
                                                    <input type="text" name="barras" runat="server" id="ncp" placeholder="(NCP)" maxlength="13" class="alphanumeric alptext"/>
                                                </label>
                                            </section>
                                            <section class="col col-3" style="display:none;">
                                            <label class="label">Contrato <a style="color:red">*</a></label>
                                                <select name="contrato2" id="contrato2" style="width: 100%" class="select2"></select>
                                                <i></i>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-3">
                                                <label class="label">Apellido materno <a style="color:red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="apellido_materno" runat="server" id="maternos" placeholder="Apellido materno" maxlength="50" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa el apellido materno.</b>
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">Fotografía del detenido </label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-file-image-o"></i>
                                                    <asp:FileUpload ID="fileUpload1" runat="server" />
                                                </label>
                                            </section>
                                            <section class="col col-9">
                                                <table id="dt_basic" class="table table-striped table-bordered table-hover"  width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th data-class="expand">#</th>
                                                            <th>Fotografía</th>
                                                            <th data-hide="phone,tablet">Nombre</th>
                                                            <th data-hide="phone,tablet">Apellido Paterno</th>
                                                            <th data-hide="phone,tablet">Apellido Materno</th>
                                                            <th data-hide="phone,tablet">Acciones</th>                                       
                                                        </tr>
                                                    </thead>                               
                                                </table>
                                            </section>
                                            <section class="col col-3">
                                                <img id="avatar" width="200" height="100" class="img-thumbnail" alt="" runat="server" src="/Content/img/avatars/male.png" onerror="this.src='/Content/img/avatars/male.png'"/>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-8">
                                                <label class="checkbox" style="color:#3276B1; font-weight:bold;">
                                                    <input type="checkbox" name="Lesiones_visibles" id="Lesion_visible" value="true"/>
                                                    <i></i>Lesiones visibles a simple vista</label>                                    
                                            </section>
                                        </div>
                                    </fieldset>
                                    <footer>
                                        <div class="row" style="display: inline-block; float: right">
                                            <%--<a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default" id="ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>--%>
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default biometrico" id="buscarmaterial"><i class="fa fa-search"></i>&nbsp;Obtener biométricos </a>
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default biometrico" id="biometrico"><i class="fa fa-plus"></i>&nbsp;KIB </a>
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-default save" id="save_" style="display: none;" title="Guardar registro actual"><i class="fa fa-save"></i>&nbsp;Guardar </a>                                
                                            <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                        </div>
                                    </footer>
                                </div>
                            </div>

                            <input type="hidden" id="tracking" runat="server" />
                            <input type="hidden" id="id" runat="server" />
                            <input type="hidden" id="trackingresidente" runat="server" />
                            <input type="hidden" id="idresidente" runat="server" />
                            <input type="hidden" id="avatarOriginal" runat="server" />
                            <input type="hidden" id="trackingestatus" runat="server" />
                            <input type="hidden" id="idestatus" runat="server" />
                            <input type="hidden" id="HQLNBB" runat="server" value="" />
                            <input type="hidden" id="KAQWPK" runat="server" value="" />
                            <input type="hidden" id="LCADLW" runat="server" value=""/>
                            <input type="hidden" id="PCBRIG" runat="server" value=""/>
                            <input type="hidden" id="idsubcontrato" value=""/>
                            <input type="hidden" id="idexpediente"  value=""/>
                            <input type="hidden" id="Informacion_Personal" runat="server" value=""/>
                            <input type="hidden" id="Antropometria" runat="server" value=""/>
                            <input type="hidden" id="Señas_Particulares" runat="server" value=""/>
                            <input type="hidden" id="Adicciones" runat="server" value=""/>
                            <input type="hidden" id="Estudios_Criminologicos" runat="server" value=""/>
                            <input type="hidden" id="Expediente_Medico" runat="server" value=""/>
                            <input type="hidden" id="Familiares" runat="server" value=""/>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

    <div id="supervisor-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Autorización</h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <div class ="Row">
                                <section class="col col-8" id="sidvalor">
                                    <label class="label" style="color: dodgerblue">Contraseña <a style="color:red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-pencil"></i>
                                        <input id="idvalor" name="valor" type="password" placeholder="Contraseña"/>                                                            
                                        <b class="tooltip tooltip-bottom-right">Ingrese la contraseña del supervisor.</b>
                                    </label>                                                        
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="editable" runat="server"/>
    <input type="hidden" id="Max" value="0" />
    <input type="hidden" id="Min" value="0" />
    <input type="hidden" id="Validar1" />
    <input type="hidden" id="Validar2" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/protocolcheck.js"></script>
    <script type="text/javascript">

        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 39) {
                e.preventDefault();

                document.getElementById("linkInfoDetencion").click();
            }
            else if (e.ctrlKey && e.keyCode === 37)
            {
                e.preventDefault();
                document.getElementById("save_").click();

            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                //criteri4
                //if ($("#supervisor-modal").is(":visible")) {
                //    document.querySelector("#btncontinuar").click();
                //}
                document.querySelector(".save").click();
                
            }
        });

        $(document).ready(function () {
            $("#supervisor-modal").on("shown.bs.modal", function () {
                $("#idvalor").focus();
            });

            $("#ligarNuevoEvento").hide();
            if ($("#<%= editable.ClientID%>").val() == "0") {
                $("#<%= nombre.ClientID %>").attr("disabled", "disabled");
                $("#<%= paterno.ClientID %>").attr("disabled", "disabled");
                $("#<%= maternos.ClientID %>").attr("disabled", "disabled");
                $("#<%= fileUpload1.ClientID %>").attr("disabled", "disabled");
                $("#Lesion_visible").attr("disabled", "disabled");
                $("#biometrico").hide();
                $("#buscarmaterial").hide();

                $("#ligarNuevoEvento").show();
            }

            pageSetUp();
            loadActualContractId();
            $('#autorizaciondatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss'
            });

            var imagenfrontalbiometricos = "";

            function habilitarbotones() {
                $('#linkvistas').removeClass("btn bg-color-green txt-color-white btn-lg disabled").addClass('btn bg-color-green txt-color-white btn-lg');
                $('#linkexpediente').removeClass("btn bg-color-red txt-color-white  btn-lg disabled").addClass('btn bg-color-red txt-color-white  btn-lg');
                $('#linkestudios').removeClass("btn bg-color-yellow txt-color-white  btn-lg disabled").addClass('btn bg-color-yellow txt-color-white  btn-lg');
                $('#linkseñas').removeClass("btn bg-color-purple txt-color-white  btn-lg disabled").addClass('btn bg-color-purple txt-color-white  btn-lg');
                $('#linkantropometria').removeClass("btn btn-warning  btn-lg disabled").addClass('btn btn-warning  btn-lg');

                $('#linkinformacion').removeClass("btn btn-info  btn-lg disabled").addClass('btn btn-info  btn-lg');

                $('#linkInfoDetencion').removeClass("btn btn-success txt-color-white  btn-lg disabled").addClass('btn btn-success txt-color-white  btn-lg');
                $('#linkingreso').removeClass("btn btn-default  btn-lg disabled").addClass('btn btn-default  btn-lg');
            }

            //$('#autorizaciondatetimepicker').data("DateTimePicker");

            $('#autorizaciondatetimepicker').data("DateTimePicker").hide();

            $("#contrato2").change(function () {
                CargarCentro("1", $("#contrato2").val());
            });

            $("body").on("click", "#biometrico", function () {
                OpenKib($("#idsubcontrato").val(), $("#idexpediente").val());
                //GetPerfiles(param);
            });

            $(document).on("click", "#btncontinuar", function () {
                if ($("#idvalor").val() == "") {
                    ShowError("Contraseña", "La contraseña del supervisor es obligatoria.");
                    $('#idvalor').parent().removeClass('state-success').addClass("state-error");
                    $('#idvalor').removeClass('valid');
                    $('#idvalor').val("");
                    return;
                }
                else {
                    $('#idvalor').parent().removeClass("state-error").addClass('state-success');
                    $('#idvalor').addClass('valid');
                }



                if ($("#idvalor").val() != $("#ctl00_contenido_PCBRIG").val()) {
                    ShowError("Contraseña", "La contraseña del supervisor es invalida.");
                    $('#idvalor').parent().removeClass('state-success').addClass("state-error");
                    $('#idvalor').removeClass('valid');
                    $('#idvalor').val("");
                    return;
                }
                else {
                    $('#idvalor').parent().removeClass("state-error").addClass('state-success');
                    $('#idvalor').addClass('valid');
                }
                guardar();

                $("#supervisor-modal").modal("hide");


            });

            function OpenKib(DetenidoId, subcontratoId) {

                window.openExternal("KIBSTATION:" + DetenidoId + "," + subcontratoId, function () {
                    alert("Ocurrió un error al abrir el recurso");
                });
            }
            function loadActualContractId() {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/getActualContractId",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var resultado = JSON.parse(response.d);

                        if (resultado.exitoso) {
                            loadContracts(resultado.contratoId);
                            CargarCentro("1", resultado.contratoId);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
            GetMinMax();

            function GetMinMax() {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/GetParametrosTamaño",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var max = resultado.TMax;
                            var min = resultado.Tmin;

                            $("#Max").val(max);
                            $("#Min").val(min);
                            // alert(resultado.rutaimagenfrontal);
                            //var imagenAvatar = ResolveUrl(resultado.rutaimagenfrontal);
                            //  imagenfrontalbiometricos = imagenAvatar;

                            //$('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            //$("#ctl00_contenido_fileUpload1").val(imagenAvatar).clone(true);
                            $('#main').waitMe('hide');
                            //location.reload(true);
                            //CargarDatos(datos.TrackingId);

                          

                        }
                        else {
                            $('#main').waitMe('hide');
                            
                                ShowError("¡Error! Algo s4li0 mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }
            $("#ctl00_contenido_fileUpload1").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar1").val(false);
                    }
                    else {
                        $("#Validar1").val(true);
                    }
                }
                else {
                    $("#Validar2").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar2").val(false);
                    }
                    else {
                        $("#Validar2").val(true);
                    }
                }
                else {
                    $("#Validar2").val(true);
                }
               
                
            });
            function loadContracts(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "entry.aspx/getContracts",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {

                        var Dropdown = $('#contrato2');
                        if (response.d.length == 1) {
                            $("#contrato2").attr("disabled", "disabled");
                        }
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Contrato]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    CargarDatos(param);
                    GetPerfiles(param);
                    $("#pertenenciaLink").attr("href", "belongings.aspx?tracking=" + param);
                    $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + param + "&editable=" + $("#<%= editable.ClientID %>").val());
                }
                else {
                    //cargar fecha automatica                    

                    var date = new Date();
                    var dia = date.getDate();
                    var mes = date.getMonth() + 1;
                    var anio = date.getFullYear();
                    var hora = date.getHours();
                    var minutos = date.getMinutes();
                    var segundos = date.getSeconds();


                    dia = dia.toString().length == 1 ? "0" + dia : dia;
                    mes = mes.toString().length == 1 ? "0" + mes : mes;
                    hora = hora.toString().length == 1 ? "0" + hora : hora;
                    minutos = minutos.toString().length == 1 ? "0" + minutos : minutos;
                    segundos = segundos.toString().length == 1 ? "0" + segundos : segundos;
                    var fechaCompleta = dia + "/" + mes + "/" + anio + " " + hora + ":" + minutos + ":" + segundos;

                    $('#<%= fecha.ClientID %>').val(fechaCompleta);
                }
                if ($("#ctl00_contenido_HQLNBB").val() == "true" || $("#ctl00_contenido_KAQWPK").val() == "true") {
                    if ($("#<%= editable.ClientID%>").val() == "0") {
                        $("#save_").hide();
                    }
                    else {
                        $("#save_").show();
                    }
                }

                if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                    $("#linkinformacion").show();
                }
                if ($("#ctl00_contenido_Antropometria").val() == "true") {
                    $("#linkantropometria").show();
                }
                if ($("#ctl00_contenido_Señas_Particulares").val() == "true") {
                    $("#linkseñas").show();
                }
                if ($("#ctl00_contenido_Familiares").val() == "true") {
                    $("#linkfamiliares").show();
                }
                if ($("#ctl00_contenido_Estudios_Criminologicos").val() == "true") {
                    $("#linkestudios").show();
                }
                if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                    $("#linkexpediente").show();
                }
                if ($("#ctl00_contenido_Adicciones").val() == "true") {
                    $("#linkadicciones").show();
                }
                $("#linkingreso").show();
                $("#pertenenciaLink").show();
                $("#linkInfoDetencion").show();
            }


            init();

            var responsiveHelper_dt_basic = undefined;

            function GetPerfiles(TrackingId) {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/getPerfiles",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        trackingId: TrackingId
                    }),
                    success: function (response) {
                        var r = JSON.parse(response.d);
                        loadTablePrincipal(r.list);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificación de pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            function loadTablePrincipal(info) {
                $('#dt_basic').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    //iDisplayLength: 20,                
                    serverSide: false,
                    fixedColumns: true,
                    retrieve: true,
                    paging: true,
                    autoWidth: true,
                    "scrollY": "350px",
                    "scrollCollapse": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {

                    },
                    data: info,
                    columns: [
                        null,
                        null,

                        {
                            name: "Nombre",
                            data: "Nombre"
                        },
                        {
                            name: "Paterno",
                            data: "Paterno"
                        },
                        {
                            name: "Materno",
                            data: "Materno"
                        }

                        , null
                    ],

                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {
                            targets: 1,
                            orderable: false,
                            render: function (data, type, row, meta) {

                                if (row.RutaImagen != "" && row.RutaImagen != null) {

                                    var ext = "." + row.RutaImagen.split('.').pop();
                                    var photo = row.RutaImagen.replace(ext, ".thumb");
                                    var imgAvatar = resolveUrl(photo);

                                    return '<div class="text-center">' +
                                        '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                        '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                        '</a>' +
                                        '<div>';
                                } else {
                                    pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                    return '<div class="text-center">' +
                                        '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                        '<div>';
                                }
                            }
                        },
                        {
                            targets: 5,
                            orderable: false,
                            render: function (data, type, row, meta) {

                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "edit";



                                //var editar = '<a class="btn btn-primary  ' + edit + '" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-clasificacion="' + row.ClasificacionId + '" title = "Ver" > <i class="glyphicon glyphicon-eye-open">&nbsp;Ver</i></a>&nbsp;';
                                var editar = '';
                                //if ($("#ctl00_contenido_KAQWPK").val() == "true") 
                                editar = '&nbsp;&nbsp;&nbsp;<a class="btn btn-success  ' + edit + '" id="relacionaperfil" href="javascript:void(0);" data-id=" " data-nombre="' + row.Nombre + ' " data-paterno="' + row.Paterno + '" data-materno="' + row.Materno + ' " data-fechaNacimiento="' + row.FechaNacimiento + ' " data-recibio=" " data-bolsa=" " data-cantidad=""  title = "Ver" > <i class="glyphicon glyphicon-eye-open"></i>&nbsp;Relacionar perfil</a >&nbsp;';

                                return editar;
                            }
                        }
                    ]
                });
            }

            $("body").on("click", "#relacionaperfil", function () {
                // alert($(this).attr("data-nombre"));
                var datos = [

                    TrackingId = $('#ctl00_contenido_tracking').val(),
                    Nombre = $(this).attr("data-nombre"),
                    Paterno = $(this).attr("data-paterno"),
                    Materno = $(this).attr("data-materno"),
                    FechaNacimiento = $(this).attr("data-fechaNacimiento"),

                ];

                RelcionaPerfil(datos);

            });

            function RelcionaPerfil(datos) {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/RelacionarPerfil",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        Datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            if (resultado.mensaje == "") {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                    "Se relaciono el perfil exitosamente", "<br /></div>");
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                    "Se registraron los biométricos correctamente. " + resultado.mensaje, "<br /></div>");

                            }
                            setTimeout(hideMessage, hideTime);
                            if (resultado.mensaje == "") {
                                ShowSuccess("Bien hecho!", "Se relaciono el perfil exitosamente. " + resultado.mensaje + " ");
                            }
                            else {
                                ShowAlert("¡Atención !", "Se relaciono el perfil exitosamente. " + resultado.mensaje + " ");
                            }
                            // alert(resultado.rutaimagenfrontal);
                            //var imagenAvatar = ResolveUrl(resultado.rutaimagenfrontal);
                            //  imagenfrontalbiometricos = imagenAvatar;

                            //$('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            //$("#ctl00_contenido_fileUpload1").val(imagenAvatar).clone(true);
                            $('#main').waitMe('hide');
                            //location.reload(true);
                            //CargarDatos(datos.TrackingId);

                            $('#<%= nombre.ClientID %>').val(datos[1]);
                            $('#<%= paterno.ClientID %>').val(datos[2]);
                            $('#<%= maternos.ClientID %>').val(datos[3]);

                        }
                        else {
                            $('#main').waitMe('hide');
                            if (resultado.mensaje == "No hay biométricos registrados para este detenido")
                                ShowAlert("¡Atención!", resultado.mensaje);
                            else
                                ShowError("¡Error! Algo salir mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        value: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {

                            loadContracts(resultado.obj.ContratoId);

                            CargarCentro(resultado.obj.CentroId, resultado.obj.ContratoId);

                            $('#<%= expediente.ClientID %>').val(resultado.obj.Expediente);
                            $('#<%= fecha.ClientID %>').val(resultado.obj.Fecha);
                            $('#<%= nombre.ClientID %>').val(resultado.obj.Nombre);
                            $('#<%= paterno.ClientID %>').val(resultado.obj.Paterno);
                            $('#<%= maternos.ClientID %>').val(resultado.obj.Materno);
                            $('#<%= tracking.ClientID %>').val(resultado.obj.TrackingId);
                            $('#<%= id.ClientID %>').val(resultado.obj.Id);
                            $('#<%= trackingresidente.ClientID %>').val(resultado.obj.TrackingIdResidente);
                            $('#<%= idresidente.ClientID %>').val(resultado.obj.IdResidente);
                            $('#<%= trackingestatus.ClientID %>').val(resultado.obj.TrackingIdEstatus);
                            $('#<%= idestatus.ClientID %>').val(resultado.obj.IdEstatus);

                            if (resultado.obj.Lesion_visible == "True") {

                                $("#Lesion_visible").prop('checked', true);
                            }
                            else {

                                $("#Lesion_visible").prop('checked', false);
                            }
                            $("#idsubcontrato").val(resultado.obj.ContratoId);
                            $("#idexpediente").val(resultado.obj.Id);
                            var imagenAvatar = ""
                            if (resultado.obj.RutaImagen != "" && resultado.obj.RutaImagen != null) {
                                imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            }
                            else {
                                imagenAvatar = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png";

                            }

                            $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(resultado.obj.RutaImagen);
                            $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkvistas").attr("href", "huella.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkestudios").attr("href", "record.aspx?tracking=" + resultado.obj.Id + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkseñas").attr("href", "signal.aspx?tracking=" + resultado.obj.Id + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + resultado.obj.Id + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + resultado.obj.Id + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + resultado.obj.Id   + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkingreso").attr("href", "entry.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la asignación. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                    }
                });
            }

            function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }

                }
                return false;
            }

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_expediente").val("");
                $("#ctl00_contenido_centro").val("0");
                $("#ctl00_contenido_fecha").val("");
                $("#ctl00_contenido_nombre").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_maternos").val("");

                $("#ctl00_contenido_fileUpload1").val("");
                $('#ctl00_contenido_id').val("");
                $('#ctl00_contenido_tracking').val("");
                $('#ctl00_contenido_avatar').attr("src", "/Content/img/avatars/male.png");
                $("#ctl00_contenido_lblMessage").html("");
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) { //guardar();
                    var param = RequestQueryString("tracking");
                    if (param != undefined) {
                        validarBiometricos(param);
                      //  guardar();
                    }
                }
            });

            function validar() {
                var esvalido = true;

                //if ($("#ctl00_contenido_centro").val() == null || $("#ctl00_contenido_centro").val() == 0) {
                //    ShowError("Centro", "El centro es obligatorio.");
                //    $('#ctl00_contenido_centro').parent().removeClass('state-success').addClass("state-error");
                //    $('#ctl00_contenido_centro').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#ctl00_contenido_centro').parent().removeClass("state-error").addClass('state-success');
                //    $('#ctl00_contenido_centro').addClass('valid');
                //}



                if ($("#ctl00_contenido_fecha").val() == null || $("#ctl00_contenido_fecha").val().split(" ").join("") == "") {
                    ShowError("Fecha", "La fecha es obligatoria.");
                    $('#ctl00_contenido_fecha').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_fecha').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_fecha').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_fecha').addClass('valid');
                }

                if ($("#ctl00_contenido_nombre").val() == null || $("#ctl00_contenido_nombre").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#ctl00_contenido_nombre').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_nombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_nombre').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_nombre').addClass('valid');
                }

                if ($("#ctl00_contenido_paterno").val() == null || $("#ctl00_contenido_paterno").val().split(" ").join("") == "") {
                    ShowError("Apellido paterno", "El apellido paterno es obligatorio.");
                    $('#ctl00_contenido_paterno').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_paterno').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_paterno').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_paterno').addClass('valid');
                }

                if ($("#ctl00_contenido_maternos").val() == null || $("#ctl00_contenido_maternos").val().split(" ").join("") == "") {
                    ShowError("Apellido materno", "El apellido materno es obligatorio.");
                    $('#ctl00_contenido_maternos').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_maternos').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_maternos').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_maternos').addClass('valid');
                }

                if ($('#ctl00_contenido_tracking').val() == null || $('#ctl00_contenido_tracking').val() == "") {
                    if ($("#ctl00_contenido_fileUpload1").val() == null || $("#ctl00_contenido_fileUpload1").val() == "") {
                        ShowError("Imagen", "La imagen es obligatoria.");
                        $('#ctl00_contenido_fileUpload1').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_fileUpload1').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_fileUpload1').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_fileUpload1').addClass('valid');
                    }
                }

                var file = document.getElementById('<% = fileUpload1.ClientID %>').value;
                if (file != null && file != '') {

                    if (!validaImagen(file)) {
                        ShowError("Fotografía del detenido", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        esvalido = false;
                    }
                    else {
                        var validar = $("#Validar1").val();
                        var validar2 = $("#Validar2").val();
                        
                       
                        if ($("#Max").val() != "0") {
                            if (validar == 'false') {
                                ShowError("Fotografía del detenido", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                esvalido = false;
                            }
                             
                        }
                        if ($("#Min").val() != "0") {
                            var s = $("#Min").val() * 1024;
                            if (validar2 == 'false') {
                                ShowError("Fotografía del detenido", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                esvalido = false;
                            }
                        }
                    }
                    
                }

                return esvalido;
            }

            $("#ctl00_contenido_nombre").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("#ctl00_contenido_paterno").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("#ctl00_contenido_maternos").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
           
            function limpiar() {
                $('#ctl00_contenido_expediente').parent().removeClass('state-success');
                $('#ctl00_contenido_expediente').parent().removeClass("state-error");
                $('#ctl00_contenido_centro').parent().removeClass('state-success');
                $('#ctl00_contenido_centro').parent().removeClass("state-error");
                $('#ctl00_contenido_fecha').parent().removeClass('state-success');
                $('#ctl00_contenido_fecha').parent().removeClass("state-error");
                $('#ctl00_contenido_nombre').parent().removeClass('state-success');
                $('#ctl00_contenido_nombre').parent().removeClass("state-error");
                $('#ctl00_contenido_paterno').parent().removeClass('state-success');
                $('#ctl00_contenido_paterno').parent().removeClass("state-error");
                $('#ctl00_contenido_maternos').parent().removeClass('state-success');
                $('#ctl00_contenido_maternos').parent().removeClass("state-error");

                $('#ctl00_contenido_fileUpload1').parent().removeClass('state-success');
                $('#ctl00_contenido_fileUpload1').parent().removeClass("state-error");
            }

            function CargarCentro(setcentro, contratoId) {
                $('#ctl00_contenido_centro').empty();
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/getInstitucion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        data: contratoId
                    }),
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_centro');
                        if (response.d.length > 0) {
                            setcentro = response.d[0].Id;
                        }

                        Dropdown.append(new Option("[Institución]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setcentro != "") {
                            Dropdown.val(setcentro);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de instituciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function guardar() {

                var files = $("#ctl00_contenido_fileUpload1").get(0).files;

                var nombreAvatarAnterior = $("#ctl00_contenido_avatarOriginal").val();

                var nombreAvatar = "";

                if (files.length > 0) {

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "../Handlers/FileUploadHandler.ashx?action=2&before=" + nombreAvatarAnterior,
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (Results) {
                            if (Results.exitoso) {
                                nombreAvatar = Results.nombreArchivo;
                                GuardarDetenido(nombreAvatar);
                            }
                            else {
                                ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                            }
                        },
                        error: function (err) {
                            ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else {
                    GuardarDetenido(nombreAvatar);
                }
            }

            $("body").on("click", "#buscarmaterial", function () {
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    //flux
                    GuardaRutasGeneradasporBiometricos(param);
                }

            });

            function GuardaRutasGeneradasporBiometricos(TrackingId) {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/saveRutaBiometrico",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        trackingId: TrackingId,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {

                            if (resultado.mensaje == "") {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                    "La información del detenido se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                    "Se registraron los biométricos correctamente. " + resultado.mensaje, "<br /></div>");

                            }
                            setTimeout(hideMessage, hideTime);
                            if (resultado.mensaje == "") {
                                ShowSuccess("Bien hecho!", "Se registraron los biométricos correctamente. " + resultado.mensaje + " ");
                            }
                            else {
                                ShowAlert("¡Atención !", "Se registraron los biométricos correctamente. " + resultado.mensaje + " ");
                            }
                            // alert(resultado.rutaimagenfrontal);
                            var imagenAvatar = ResolveUrl(resultado.rutaimagenfrontal);
                            imagenfrontalbiometricos = imagenAvatar;

                            $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            $("#ctl00_contenido_fileUpload1").val(imagenAvatar).clone(true);
                            $('#main').waitMe('hide');

                        }
                        else {
                            $('#main').waitMe('hide');
                            if (resultado.mensaje == "No hay biométricos registrados para este detenido")
                                ShowAlert("¡Atención!", resultado.mensaje);
                            else
                                ShowError("¡Error! Algo sali000 mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }
            
            function validarBiometricos(TrackingId) {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/validaBiometricosalGuardar",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        value: TrackingId,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            if (resultado.mensaje == "") {

                                guardar();
                            }
                            else {
                                ShowAlert("Atención!", "No se encontraron biométricos");
                                $('#idvalor').parent().removeClass('state-success');
                                $('#idvalor').parent().removeClass("state-error");
                                $('#idvalor').val("");
                                //cr1teri4
                                //$("#supervisor-modal").modal("show");
                            }
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salio super mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            function GuardarDetenido(rutaAvatar) {
                startLoading();
                if (imagenfrontalbiometricos != "") {
                    rutaAvatar = imagenfrontalbiometricos;
                }
                var datos = ObtenerValores(rutaAvatar);

                $.ajax({
                    type: "POST",
                    url: "entry.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            $('#ctl00_contenido_id').val(resultado.Id);
                            $('#ctl00_contenido_tracking').val(resultado.TrackingId);
                            $('#ctl00_contenido_idresidente').val(resultado.IdResidente);
                            $('#ctl00_contenido_trackingresidente').val(resultado.TrackingIdResidente);
                            $('#ctl00_contenido_idestatus').val(resultado.IdEstatus);
                            $('#ctl00_contenido_trackingestatus').val(resultado.TrackingIdEstatus);
                            $('#ctl00_contenido_avatarOriginal').val(resultado.RutaImagen);
                            var esvalido = true;
                            var imagenAvatar = "";

                            if (imagenfrontalbiometricos != "") {
                                imagenAvatar = ResolveUrl(imagenfrontalbiometricos);
                            }
                            else {
                                imagenAvatar = ResolveUrl(resultado.RutaImagen);
                            }

                            $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            $("#ctl00_contenido_fileUpload1").val('').clone(true);
                            limpiar();
                            habilitarbotones();
                            //f1x3d
                            //CargarDatos(resultado.TrackingId);
                            CargarDatos(resultado.Id);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "La información del detenido se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "La información del detenido se " + resultado.mensaje + " correctamente.");
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            if (resultado.mensaje == "No puede modificar la información de un detenido egresado.") {
                                ShowError("¡Error! Algo salió mal", resultado.mensaje);
                            }
                            else {
                                ShowError("¡Error! Algo saliø mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                            }
                        }
                        $('#main').waitMe('hide');

                    }
                });
            }

            function ObtenerValores(rutaAvatar) {
                if (rutaAvatar == "") rutaAvatar = $("#ctl00_contenido_avatarOriginal").val();

                var datos = [
                    Id = $('#ctl00_contenido_id').val(),
                    TrackingId = $('#ctl00_contenido_tracking').val(),
                    Expediente = $('#ctl00_contenido_expediente').val(),
                    Centro = $('#ctl00_contenido_centro').val(),
                    Fecha = $('#ctl00_contenido_fecha').val(),
                    Nombre = $('#ctl00_contenido_nombre').val(),
                    Paterno = $('#ctl00_contenido_paterno').val(),
                    Materno = $('#ctl00_contenido_maternos').val(),
                    NCP = "",
                    Avatar = rutaAvatar,
                    IdResidente = $('#ctl00_contenido_idresidente').val(),
                    TrackingIdResidente = $('#ctl00_contenido_trackingresidente').val(),

                    IdEstatus = $('#ctl00_contenido_idestatus').val(),
                    TrackingIdEstatus = $('#ctl00_contenido_trackingestatus').val(),
                    ContratoId = $("#contrato2").val(),
                    Lesion_visible = $('#Lesion_visible').is(":checked")
                ];
                return datos;
            }
            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            var rutaDefaultServer = "";

            getRutaDefaultServer();

            function getRutaDefaultServer() {
                $.ajax({
                    type: "POST",
                    url: "entry.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;
                        }
                    }
                });
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("#ctl00_contenido_expediente").keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
        });
    </script>
</asp:Content>
