﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="informaciondetencion.aspx.cs" Inherits="Web.Application.Registry.InformacionDetencion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Información de detención</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>     
        td.strikeout {
            text-decoration: line-through;
        }
        input[type="text"]:disabled {
            background: #eee;
            cursor: not-allowed;
        }      
        .scroll2 {
            height: 25vw;
            overflow: auto;
        }
        @media only screen and (max-width: 1245px)
        {
            .scroll2{
                height: 15vw;
            }
        }
        .info-container {
            display:table-cell;
        }
       .info-text {
            display:table-cell;
            padding-left: 10px;
       }
        @media only screen and (max-width: 1280px) {
            .menu-container a:nth-child(5){ width:auto; }
            .menu-container a:nth-child(6){ width:auto; }
            .menu-container a:nth-child(7){ width:262px; }
            .menu-container a:nth-child(8){ width:172px; }
        }
        @media only screen and (max-width: 1068px) {
            .menu-container a:nth-child(5){ width:auto; }
            .menu-container a:nth-child(6){ width:240px; }
            .menu-container a:nth-child(7){ width:172px; }
            .menu-container a:nth-child(8){ width:auto; }
        }
        @media only screen and (max-width: 915px) {
            .menu-container a:nth-child(5){ width:auto; }
            .menu-container a:nth-child(6){ width:307px; }
            .menu-container a:nth-child(7){ width:auto; }
            .menu-container a:nth-child(8){ width:auto; }
        }
        @media only screen and (max-width: 800px) {
            .menu-container a:nth-child(5){ width:284px; }
            .menu-container a:nth-child(6){ width:auto; }
            .menu-container a:nth-child(7){ width:auto; }
            .menu-container a:nth-child(8){ width:auto; }
        }
        #dt_basic_tabla_unidades_filter .form-control{
            width: auto;
            margin-bottom: 5px;
        }
        #dt_basic_tabla_unidades_length .form-control{
            float: none;
            width: auto;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <!--
    <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-list-alt"></i>
                    Información personal
                </h1>
            </div>
        </div>-->
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <section id="widget-grid" class="">
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
            <div class="row padding-bottom-10">
                <article class="col-sm-12 col-md-12 col-lg-12 menu-container" >
                    <a style="float: right; margin-left: 3px; display: block; font-size: smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa  fa-paw"></i> Biométricos</a>
                    <a style="float: right; margin-left: 3px; display: none; font-size: smaller" class="btn bg-color-red txt-color-white  btn-lg " id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
                    <a style="float: right; margin-left: 3px; display: none; font-size: smaller" class="btn bg-color-yellow txt-color-white  btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
                    <a style="float: right; margin-left: 3px; display: none; font-size: smaller" class="btn bg-color-purple txt-color-white  btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
                    <a style="float: right; margin-left: 3px; display: none; font-size: smaller" class="btn btn-warning  btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
                    <a style="float: right; margin-left: 3px; font-size:smaller" class="btn btn-primary txt-color-white  btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
                    <a style="float: right; margin-left: 3px; display: none; font-size: smaller" class="btn btn-info  btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
                    <a style="float: right; margin-left: 3px; display: none; font-size: smaller" class="btn btn-success txt-color-white  btn-lg " id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a>
                    <a style="float: right; margin-left: 3px; display: none; font-size: smaller" class="btn btn-default  btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
                </article>
            </div>

            <div class="scroll2" id="ScrollableContent">
        <article class="col-sm-12 col-md-12 col-lg-12" tabindex="0">
            <div class="jarviswidget jarviswidget-color-darken" tabindex="1" id="wid-personal-2"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                    <h2>Información de la detención </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body">                                                                         
                        <div class="row">                                                   
                            <div class="col-sm-12">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <div class="row">
                                        <div id="Div1" class="smart-form">
                                            <fieldset>
                                                <legend>Datos de la detención</legend>
                                                    <div class="row" style="margin-bottom:10px;">
                                                        <section class="col col-4">
                                                            <label class="label">Fecha y hora</label>
                                                            <label class="input" >
                                                                <label class="input-group date" id='autorizaciondatetimepicker' style="width:100%">
                                                                    <input type="text" name="Fecha y hora" id="fecha" class='input-group date alptext' placeholder="Fecha y hora de ingreso" disabled="disabled"/>
                                                                </label>
                                                            </label>
                                                        </section>
                                                    
                                                        <section class="col col-4">
                                                            <label class="label">Evento</label>
                                                            <label class="select">
                                                                <select name="evento" id="evento" disabled="disabled"></select>
                                                                <i></i>
                                                            </label>                                                        
                                                        </section>
                                                        <section class="col col-4">
                                                            <label class="label">Folio</label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                <input type="text" name="folio" id="folio" placeholder="Folio" maxlength="50" class="alphanumeric" disabled="disabled" />
                                                                <b class="tooltip tooltip-bottom-right">Ingresa el folio.</b>
                                                            </label>
                                                        </section>
                                                    </div>
                                                    
                                                        <table tabindex="2" id="dt_basic_tabla_unidades" class="table table-striped table-bordered table-hover" style="width:100%">
                                                            <thead>
                                                                <tr>
                                                                    <th data-class="expand">Unidad</th>                                                        
                                                                    <th data-hide="phone,tablet">Responsable</th>
                                                                    <th data-hide="phone,tablet">Corporación</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    

                                                    <div class="row" style="margin-top:15px">
                                                        
                                                        <section class="col col-6">
                                                        <label class="input">Descripción del evento <a style="color:red">*</a></label>
                                                        <label class="textarea">
                                                            <textarea tabindex="5" id="motivo" name="motivo" class="form-control alptext" rows="5"  placeholder="Descripción del evento" maxlength="1000"></textarea> 
                                                            <b class="tooltip tooltip-bottom-right" >Ingresa la descripción del evento.</b>
                                                        </label>                                                        
                                                       
                                                        </section>
                                                        <section class="col col-6" style="display:none">
                                                            <label class="label">Detalle / Trayectoria de la Unidad <a style="color: red">*</a></label>
                                                            <label class="textarea">
                                                                <%--<i class="icon-append fa fa-sort-numeric-desc"></i>--%>
                                                                <%--<input type="text" name="descripcion" id="descripcion" placeholder="Descripción" class="alphanumeric" maxlength="1000" />--%>
                                                                <textarea id="descripcion" name="descripcion" placeholder="Descripción" maxlength="1000" class="form-control alptext" rows="5"></textarea>
                                                                <b class="tooltip tooltip-bottom-right">Ingresa la descripción.</b>
                                                            </label>
                                                        </section>                                                                
                                                        <section class="col col-6">
                                                            <label class="label">Lugar de detención <a style="color: red">*</a></label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                <input type="text" name="lugar" id="lugar" placeholder="Lugar de detención" maxlength="1000" class="alphanumeric alptext" disabled="disabled" />
                                                                <b class="tooltip tooltip-bottom-right">Ingresa el lugar de detención.</b>
                                                            </label>
                                                        </section>                                                                                                        
                                                        <section class="col col-6">
                                                            <label class="label">País <a style="color: red">*</a></label>
                                                            <label class="select">
                                                                <select name="pais" id="pais" disabled="disabled">                                                
                                                                </select>
                                                                <i></i>
                                                            </label>
                                                        </section>


                                                        <section class="col col-6">
                                                            <label class="label">Estado <a style="color: red">*</a></label>
                                                            <label class="select">
                                                                <select name="estado" id="estado" disabled="disabled">                                                
                                                                </select>
                                                                <i></i>
                                                            </label>
                                                        </section>        
                                                        <section class="col col-6">
                                                            <label class="label">Municipio <a style="color: red">*</a></label>
                                                            <label class="select">
                                                                <select name="municipio" id="municipio" disabled="disabled">                                                
                                                                </select>
                                                                <i></i>
                                                            </label>
                                                        </section>
                                                        <section class="col col-4">
                                                            <label class="label">Colonia <a style="color: red">*</a></label>
                                                            <label class="select">
                                                                <select name="colonia" id="colonia" disabled="disabled">                                                
                                                                </select>
                                                                <i></i>
                                                            </label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label">Código postal <a style="color: red">*</a></label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-pencil"></i>
                                                                <input type="text" name="codigoPostal" id="codigoPostal" class="alptext"  maxlength="5" disabled="disabled"/>
                                                            </label>
                                                        </section> 
                                                            <section class="col col-4">
                                                            <label class="label">Persona a quien se notifica </label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-pencil"></i>
                                                                <input tabindex="6" type="text" name="personanotifica" id="personanotifica" class="alptext" maxlength="150" />
                                                                <b class="tooltip tooltip-bottom-right">Ingresa la persona a la cual se notifica.</b>
                                                            </label>
                                                        </section>      
                                                            <section class="col col-2">
                                                            <label class="label">Celular </label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-phone"></i>
                                                                <input tabindex="7" name="celular" id="celular" maxlength="10" type="tel" placeholder="Celular" data-mask="(999) 999-9999"/>
                                                                <b class="tooltip tooltip-bottom-right">Ingresa el celular.</b>
                                                            </label>
                                                        </section>      
                                                    </div>
                                                
                                                </div>                        
                                        </div>
                                    </div>
                                    <div class="row smart-form" >
                                        <footer >
                                            <a  class="btn btn-default" title="Llamada Evento" id="linkAlertaWeb" tabindex="12" ><i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;  Ver evento / alerta web</a>
                                            <a href="entrylist.aspx" class="btn btn-default" title="Volver al listado" tabindex="11"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                            <!--<a class="btn btn-sm btn-default clear4"><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                            <a class="btn btn-default saveInfoDetencion" id="savebtn" style="display: none;" title="Guardar" tabindex="10"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                           <%-- <a href="javascript:void(0);" class="btn btn-md btn-default" id="ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>--%>
                                        </footer>
                                        <input type="hidden" id="trackingIdDetencion" runat="server" />
                                        <input type="hidden" id="idDetencion" runat="server" />      
                                        <input type="hidden" id="tracking" runat="server" />
                                    </div>
                                </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>                    
        </article>
        </div>
        </section>
    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control alptext" id="nombreInfo" disabled="disabled"  type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control alptext" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
   <div class="modal fade" id="form-modal-eventos" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
      <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="glyphicon glyphicon-folder-open"></i>  Evento /alerta web</h4>
                </div>
                <div class="modal-body">
                    <!-- Evento local -->
                   <!--  <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Datos del evento</label>-->
                    <div class="panel-group smart-accordion-default" id="accordion">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a data-toggle="collapse" style="color:#3276b1; " data-parent="#accordion" href="#collapseEvento"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Datos de evento </a></h4>
							</div>
							<div id="collapseEvento" class="panel-collapse collapse">
								<div class="panel-body no-padding">
                                    <fieldset>
									    <table id="dt_basic_evento" class="table table-striped table-bordered table-hover" width="100%">
										    <thead>
											    <tr>
                                                    <th>Folio</th>
												    <th>Descripción</th>
												    <th>Fecha y hora</th>
                                                    <th>Lugar de detención</th>
											    </tr>
										    </thead>
									    </table>
                                    </fieldset>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a data-toggle="collapse" style="color:#3276b1; " data-parent="#accordion" href="#collapseLugar" class="collapsed"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de evento</a></h4>
							</div>
							<div id="collapseLugar" class="panel-collapse collapse">
								<div class="panel-body">
									<table id="dt_basic_lugar" class="table table-striped table-bordered table-hover" style="width: 100%;">
										    <thead>
											    <tr>
												    <th>Número de detenidos</th>
                                                    <th>Latitud</th>
                                                    <th>Longitud</th>
											    </tr>
										    </thead>
									    </table>
								</div>
							</div>
						</div>
					</div>
                    <br />
                    <!-- Evento de Alerta web-->
               
                    <div class="panel-group smart-accordion-default" id="accordionAW">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionAW"  style="color:#3276b1; " href="#collapseEventoAW"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Alerta web </a></h4>
							</div>
							<div id="collapseEventoAW" class="panel-collapse collapse">
								<div class="panel-body no-padding">
                                    <fieldset>
									    <table id="dt_basic_evento_AW" class="table table-striped table-bordered table-hover" width="100%">
										    <thead>
											    <tr>
												    <th>Folio</th>
												    <th>Descripción</th>
												    <th>Fecha</th>
                                                    <th>Lugar</th>
												    <th>Nombre del responsable</th>
                                                    <th>Número de detenidos</th>
										    </tr>
									    </thead>
								    </table>
                                </fieldset>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionAW" style="color:#3276b1; " href="#collapseLugarAW" class="collapsed"> <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de alerta web</a></h4>
						</div>
						<div id="collapseLugarAW" class="panel-collapse collapse">
				            <div class="panel-body">
								<table id="dt_basic_lugar_AW" class="table table-striped table-bordered table-hover" width="100%">
										<thead>
											<tr>
												<th>Sector</th>
												<th>Entre Calle</th>
												<th>Latitud</th>
												<th>Longitud</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>



    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="Ingreso" runat="server" value="" />
    <input type="hidden" id="Antropometria" runat="server" value="" />
    <input type="hidden" id="Señas_Particulares" runat="server" value="" />
    <input type="hidden" id="Adicciones" runat="server" value="" />
    <input type="hidden" id="Estudios_Criminologicos" runat="server" value="" />
    <input type="hidden" id="Expediente_Medico" runat="server" value="" />
    <input type="hidden" id="Familiares" runat="server" value="" />
    <input type="hidden" id="editable" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>    
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>


    <script type="text/javascript">
        
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 39) {
                e.preventDefault();
                document.getElementById("linkinformacion").click();
            }
            else if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                document.getElementById("linkingreso").click();
            }
            else if (e.keyCode === 27) {
                if ($("#form-modal-eventos").is(":visible")) {
                    $("#form-modal-eventos").modal("hide");
                }
                else if ($("#datospersonales-modal").is(":visible")) {
                    $("#datospersonales-modal").modal("hide");
                }

            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                document.getElementsByClassName("saveInfoDetencion")[0].click();
            }
       
        });

        $(document).ready(function () {
            $("#ligarNuevoEvento").hide();
            if ($("#<%= editable.ClientID%>").val() == "0") {
                $("#evento").attr("disabled", "disabled");
                $("#evento").css("background", "#eee");
                $("#motivo").attr("disabled", "disabled");
                $("#descripcion").attr("disabled", "disabled");
                $("#pais").attr("disabled", "disabled");
                $("#pais").css("background", "#eee");
                $("#estado").attr("disabled", "disabled");
                $("#estado").css("background", "#eee");
                $("#municipio").attr("disabled", "disabled");
                $("#municipio").css("background", "#eee");
                $("#colonia").attr("disabled", "disabled");
                $("#colonia").css("background", "#eee");
                $("#personanotifica").attr("disabled", "disabled");
                $("#celular").attr("disabled", "disabled");
                $("#celular").css("background", "#eee");

                $("#ligarNuevoEvento").show();
            }
            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 443px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');

                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 216px)");
            });

            $("#ScrollableContent").css("height", "calc(100vh - 443px)");

            $("#datospersonales-modal").on("hidden.bs.modal", function () {
                document.getElementsByClassName("detencion")[0].focus();
            });


            getalerta();
            function getalerta() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "InformacionDetencion.aspx/GetAlertaWeb",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.d != null) {
                            data = data.d;

                            var alerta1 = data.Denominacion;
                            var alerta2 = data.AlertaWerb;

                            //$("#Denominacin").val(data.Denominacion);

                            //$("#linkEvento").html('<i class="glyphicon glyphicon-folder-open"></i>&nbsp; ' + alerta1 + ' / evento');
                            $("#linkAlertaWeb").html('<i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;  Ver evento / '+alerta2);
                            //$("#alerta2").html('<i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>' + alerta1);
                            //$("#alerta3").html(' <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de ' + alerta2);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de alerta web. Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
            }

            var responsiveHelper_dt_basic_evento = undefined;
            var responsiveHelper_dt_basic_lugarevento = undefined;
            var responsiveHelper_dt_basic_eventoAW = undefined;
            var responsiveHelper_dt_basic_lugareventoAW = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            pageSetUp();
            
            init();            

            $("body").on("click", "#linkAlertaWeb", function () {
                var tracking = $("#ctl00_contenido_tracking").val();

               responsiveHelper_dt_basic_evento = undefined;
                $('#dt_basic_evento').DataTable().destroy();

                responsiveHelper_dt_basic_lugarevento = undefined;
                $('#dt_basic_lugar').DataTable().destroy();

                responsiveHelper_dt_basic_eventoAW = undefined;
                $('#dt_basic_evento_AW').DataTable().destroy();

                responsiveHelper_dt_basic_lugareventoAW = undefined;
                $('#dt_basic_lugar_AW').DataTable().destroy();

                loadTableEvento(tracking);
                loadTableLugarEvento(tracking);
                loadTableEventoAW(tracking);
                loadTableLugarEventoAW(tracking);

                $("#form-modal-eventos").modal('show');
                $("#form-modal-title-eventos").empty();
                $("#form-modal-title-eventos").html("Evento alerta web");
            });

            function loadTableEvento(detenidoId) {
                $.ajax({
                    type: "POST",
                    url: "InformacionDetencion.aspx/getEvento",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                     data: JSON.stringify({
                        detenidoId: detenidoId
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        llenarTablaEvento(r.lista);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
                
            }

            function llenarTablaEvento(lista) {
                $('#dt_basic_evento').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: false,
                    fixedColumns: true,
                    autoWidth: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_evento) {
                            responsiveHelper_dt_basic_evento = new ResponsiveDatatablesHelper($('#dt_basic_evento'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_evento.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_evento.respond();
                        $('#dt_basic_evento').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {

                    },
                    data: lista,
                    columns: [
                        {
                            name: "FolioEvento",
                            data: "FolioEvento"
                        },
                        {
                            name: "Descripcion",
                            data: "Descripcion"
                        },
                        {
                            name: "FechaHora",
                            data: "FechaHora"
                        },
                        {
                            name: "Localizacion",
                            data: "Localizacion"
                        },
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "FolioEvento",
                            render: function (data, type, row, meta) {
                                return row.FolioEvento;
                            }
                        },
                        {
                            targets: 1,
                            data: "Descripcion",
                            render: function (data, type, row, meta) {
                                return row.Descripcion;
                            }
                        },
                        {
                            targets: 2,
                            data: "FechaHora",
                            render: function (data, type, row, meta) {
                                return row.FechaHora;
                            }
                        },
                        {
                            targets: 3,
                            data: "Localizacion",
                            render: function (data, type, row, meta) {
                                return row.Localizacion;
                            }
                        }
                    ]
                });
            }

            function loadTableLugarEvento(detenidoId) {
                $.ajax({
                    type: "POST",
                    url: "InformacionDetencion.aspx/getLugarEvento",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                     data: JSON.stringify({
                        detenidoId: detenidoId
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        llenarTablaLugarEvento(r.lista);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
                
            }

            function llenarTablaLugarEvento(lista) {
                $('#dt_basic_lugar').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: false,
                    fixedColumns: true,
                    autoWidth: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_lugarevento) {
                            responsiveHelper_dt_basic_lugarevento = new ResponsiveDatatablesHelper($('#dt_basic_lugar'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_lugarevento.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_lugarevento.respond();
                        $('#dt_basic_lugar').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        
                    },
                    data: lista,
                    columns: [
                        {
                            name: "NumeroDetenidos",
                            data: "NumeroDetenidos"
                        },
                        {
                            name: "Latitud",
                            data: "Latitud"
                        },
                        {
                            name: "Longitud",
                            data: "Longitud"
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "NumeroDetenidos",
                            render: function (data, type, row, meta) {
                                return row.NumeroDetenidos;
                            }
                        },
                        {
                            targets: 1,
                            data: "Latitud",
                            render: function (data, type, row, meta) {
                                return row.Latitud;
                            }
                        },
                        {
                            targets: 2,
                            data: "Longitud",
                            render: function (data, type, row, meta) {
                                return row.Longitud;
                            }
                        }
                    ]
                });
            }

            function loadTableEventoAW(detenidoId) {
                $.ajax({
                    type: "POST",
                    url: "InformacionDetencion.aspx/getEventoAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                     data: JSON.stringify({
                        detenidoId: detenidoId
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        llenarTablaEventoAW(r.lista);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
                
            }

            function llenarTablaEventoAW(lista) {
                $('#dt_basic_evento_AW').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: false,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_eventoAW) {
                            responsiveHelper_dt_basic_eventoAW = new ResponsiveDatatablesHelper($('#dt_basic_evento_AW'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_eventoAW.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_eventoAW.respond();
                        $('#dt_basic_evento_AW').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        
                    },
                    data: lista,
                    columns: [
                        {
                            name: "FolioEvento",
                            data: "FolioEvento"
                        },
                        {
                            name: "Descripcion",
                            data: "Descripcion"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "Lugar",
                            data: "Lugar"
                        },
                        {
                            name: "NombreResponsable",
                            data: "NombreResponsable"
                        },
                        {
                            name: "NumeroDetenidos",
                            data: "NumeroDetenidos"
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "FolioEvento",
                            render: function (data, type, row, meta) {
                                return row.FolioEvento;
                            }
                        },
                        {
                            targets: 1,
                            data: "Descripcion",
                            render: function (data, type, row, meta) {
                                return row.Descripcion;
                            }
                        },
                        {
                            targets: 2,
                            data: "Fecha",
                            render: function (data, type, row, meta) {
                                return row.Fecha;
                            }
                        },
                        {
                            targets: 3,
                            data: "Lugar",
                            render: function (data, type, row, meta) {
                                return row.Lugar;
                            }
                        },
                        {
                            targets: 4,
                            data: "NombreResponsable",
                            render: function (data, type, row, meta) {
                                return row.NombreResponsable;
                            }
                        },
                        {
                            targets: 5,
                            data: "NumeroDetenidos",
                            render: function (data, type, row, meta) {
                                return row.NumeroDetenidos;
                            }
                        }
                    ]
                });
            }

            function loadTableLugarEventoAW(detenidoId) {
                $.ajax({
                    type: "POST",
                    url: "InformacionDetencion.aspx/getLugarEventoAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                     data: JSON.stringify({
                        detenidoId: detenidoId
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        llenarTablaLugarEventoAW(r.lista);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function llenarTablaLugarEventoAW(lista) {
                $('#dt_basic_lugar_AW').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: false,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_lugareventoAW) {
                            responsiveHelper_dt_basic_lugareventoAW = new ResponsiveDatatablesHelper($('#dt_basic_lugar_AW'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_lugareventoAW.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_lugareventoAW.respond();
                        $('#dt_basic_lugar_AW').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        
                    },
                    data: lista,
                    columns: [
                        {
                            name: "Sector",
                            data: "Sector"
                        },
                        {
                            name: "EntreCalle",
                            data: "EntreCalle"
                        },
                        {
                            name: "Latitud",
                            data: "Latitud"
                        },
                        {
                            name: "Longitud",
                            data: "Longitud"
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "Sector",
                            render: function (data, type, row, meta) {
                                return row.Sector;
                            }
                        },
                        {
                            targets: 1,
                            data: "EntreCalle",
                            render: function (data, type, row, meta) {
                                return row.EntreCalle;
                            }
                        },
                        {
                            targets: 2,
                            data: "Latitud",
                            render: function (data, type, row, meta) {
                                return row.Latitud;
                            }
                        },
                        {
                            targets: 3,
                            data: "Longitud",
                            render: function (data, type, row, meta) {
                                return row.Longitud;
                            }
                        }
                    ]
                });
            }

            $('#autorizaciondatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss'
            });

            $('#autorizaciondatetimepicker').data("DateTimePicker").hide();

            //Deteccion de cambios en los combos de ubicaciones y llamadas / eventos
            $("#pais").change(function () {
                loadStates("0", $("#pais").val());
            });

            $("#estado").change(function () {
                loadCities("0", $("#estado").val());
            });

            $("#municipio").change(function () {
                loadNeighborhood("0", $("#municipio").val());
            });

            $("#colonia").change(function () {
                loadZipCode($("#colonia").val());
            });

            $("#llamada").change(function () {
                loadEvents("0", $("#llamada").val());
            });


            var responsiveHelper_dt_basic_tabla_unidades = undefined;

            function loadTablaUnidades(trackingId) {
                $('#dt_basic_tabla_unidades').dataTable({
                    destroy: true,
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px; margin-left:10px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_tabla_unidades) {
                            responsiveHelper_dt_basic_tabla_unidades = new ResponsiveDatatablesHelper($('#dt_basic_tabla_unidades'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_tabla_unidades.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_tabla_unidades.respond();
                        $('#dt_basic_tabla_unidades').waitMe('hide');
                        responsiveHelper_dt_basic_tabla_unidades = undefined;
                    },
                    ajax: {
                        type: "POST",
                        url: "informacionDetencion.aspx/getUnidadadesTabla",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_tabla_unidades').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.tracking = trackingId;
                            return JSON.stringify(parametrosServerSide);
                        }
                    },
                    columns: [
                        {
                            name: "Unidad",
                            data: "Unidad"
                        },
                        {
                            name: "Responsable",
                            data: "Responsable"
                        },
                         {
                            name: "Corporacion",
                            data: "Corporacion"
                        },
                        {
                            name: "UnidadId",
                            data: "UnidadId"
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 3,                            
                            name: "UnidadId",
                            visible: false,
                        }
                    ]
                });
            }

            $("#evento").change(function () {
                //cargar los datos del evento y setearlo
                $('#dt_basic_tabla_unidades').DataTable().destroy();
                loadEventById($("#evento").val());
                //loadUnits(0,$("#evento").val());
            });


            //function CargarCorporacion(set) {
            //    $('#corporacion').empty();
            //    $.ajax({

            //        type: "POST",
            //        url: "informacionDetencion.aspx/getcorporacion",
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //         data: JSON.stringify({
            //            unidad: set
            //        }),
            //        cache: false,
            //        success: function (response) {
            //            var Dropdown = $('#corporacion');

                     
            //            $.each(response.d, function (index, item) {
            //                Dropdown.append(new Option(item.Desc, item.Id));

            //            });

            //            //if (set != "") {

            //            //    Dropdown.val(set);
            //            //    Dropdown.trigger("change.select2");

            //            //}
            //        },
            //        error: function () {
            //            ShowError("¡Error!", "No fue posible cargar la lista de corporaciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
            //        }
            //    });
            //}

            //$("#unidad").change(function () {
            //    //cargar los datos del responsable y setearlo
            //    datos = Obtenerdatos();
            //    if ($("#unidad").val() != 0) {
            //        CargarCorporacion($("#unidad").val());
            //    }
            //    loadResponsablesByUnitIdEventId(0,datos);
            //});

            //function Obtenerdatos() {
            //    datos = [
            //        unidadId = $("#unidad").val(),
            //        eventoId = $("#evento").val(),
            //    ]

            //    return datos;
            //}

            //Cargar paises
            function loadCountries(setvalue) {
                $.ajax({                    
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getCountries",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#pais');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[País]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de países. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            //Cargar estados
            function loadStates(setvalue, idPais) {

                $.ajax({                    
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getStates",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idPais: idPais
                    }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#estado');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Estado]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de estados. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            //Cargar municipios
            function loadCities(setvalue, idEstado) {

                $.ajax({                    
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getCities",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idEstado: idEstado
                    }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#municipio');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Municipio]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de municipios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            //Cargar colonias
            function loadNeighborhood(set, idMunicipio) {                
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#colonia");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de colonias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            //Cargar codigo postal
            function loadZipCode(idColonia) {                
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getZipCode",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idColonia: idColonia
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        $("#codigoPostal").val(resultado.cp);                                                
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el código postal. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            //Cargar llamadas
            function loadCalls(set) {                
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getCalls",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (response) {
                        var Dropdown = $("#llamada");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Llamada]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de llamadas. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            //Cargar eventos
            function loadEvents(set, idLlamada) {       
              
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getEvents",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idLlamada: idLlamada,
                        idEvento: set
                    }),
                    success: function (response) {
                        var Dropdown = $("#evento");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Evento]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de eventos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            //Cargar evento por id
            function loadEventById(id) {                
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getEventById",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        if (resultado.exitoso) {
                            $("#descripcion").val(resultado.obj.Descripcion);
                            $("#lugar").val(resultado.obj.Lugar);
                            $("#folio").val(resultado.obj.Folio);
                            $("#fecha").val(resultado.obj.HoraYFecha);
                            loadCountries(resultado.obj.PaisId);
                            loadStates(resultado.obj.EstadoId, resultado.obj.PaisId);
                            loadCities(resultado.obj.MunicipioId, resultado.obj.EstadoId);
                            loadNeighborhood(resultado.obj.ColoniaId, resultado.obj.MunicipioId);
                            loadZipCode(resultado.obj.ColoniaId);
                            loadTablaUnidades(resultado.obj.TrackingId);

                            if (resultado.obj.EventoIdAW > 0) {
                                $('#linkAlertaWeb').show();
                            }
                        }                                                
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar los datos del evento. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }                          

            <%--//Cargar unidades
            function loadUnits(set,id) {                
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getUnits",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $("#unidad");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Unidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de unidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }--%>

            //Cargar Responsables
            <%--function loadResponsablesByUnitIdEventId(set,datos) {                
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getResponsable",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos
                    }),
                    success: function (response) {
                         var Dropdown = $("#responsable");
                        Dropdown.empty();
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }                        
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de responsables. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }--%>

            var eventId;
            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);

                            $("#ctl00_contenido_idDetencion").val(resultado.obj.IdInfo);
                            $("#ctl00_contenido_trackingIdDetencion").val(resultado.obj.TrackingInfo);

                          
                                $('#edadInterno').text(resultado.obj.Edad);
                           

                           
                                $('#sexoInterno').text(resultado.obj.Sexo);

                            $('#municipioInterno').text(resultado.obj.municipioNombre);
                            $('#domicilioInterno').text(resultado.obj.domiclio);
                            $('#coloniaInterno').text(resultado.obj.coloniaNombre);

                            $("#motivo").val(resultado.obj.Descripcion);
                            $("#descripcion").val(resultado.obj.Motivo);
                            $("#folio").val(resultado.obj.Folio);
                            $("#fecha").val(resultado.obj.FechaDetencion);
                            $("#codigoPostal").val(resultado.obj.CodigoPostal);
                            $("#lugar").val(resultado.obj.Lugar);
                            loadCountries(resultado.obj.PaisId);
                            loadStates(resultado.obj.EstadoId, resultado.obj.PaisId);
                            loadCities(resultado.obj.MunicipioId, resultado.obj.EstadoId);
                            loadNeighborhood(resultado.obj.ColoniaId, resultado.obj.MunicipioId);
                            loadZipCode(resultado.obj.ColoniaId);
                            $("#llamada").val(resultado.obj.LlamadaId);
                            $("#personanotifica").val(resultado.obj.Personanotifica);
                            $("#celular").val(resultado.obj.Celular);

                            loadEvents(resultado.obj.EventoId, resultado.obj.LlamadaId);
                         
                            eventId = resultado.obj.EventoId;
                            loadEventById(eventId);
                            //loadUnits(resultado.obj.UnidadId, resultado.obj.EventoId);

                            datos = [
                                unidadId = resultado.obj.UnidadId,
                                eventoId = resultado.obj.EventoId
                            ]

                            //loadResponsablesByUnitIdEventId(resultado.obj.ResponsableId, datos);

                            $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkvistas").attr("href", "huella.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkestudios").attr("href", "record.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkseñas").attr("href", "signal.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkingreso").attr("href", "entry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + resultado.obj.TrackingId + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());

                         
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            //$('#ctl00_contenido_fecha').mask('99/99/9999', { placeholder: "mm/dd/yyyy" });    

            $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
                var param = RequestQueryString("tracking");
                CargarDatosModal(param);
            });

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);
                            }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            

            $("#personanotifica").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            $("body").on("click", ".saveInfoDetencion", function () {                
                if (validar()) {
                    var unidadID;
                    var responsableID;
                    $("#dt_basic_tabla_unidades").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                        var data;

                            data = this.data();
                            unidadID = data.UnidadId;
                            responsableID = data.ResponsableId;
                    });  
                    var datos = {
                        TrackingIdDetenido: $("#ctl00_contenido_tracking").val(),
                        UnidadId: unidadID,
                        ResponsableId: responsableID,
                        Motivo: $("#motivo").val(),
                        Descripcion: $("#descripcion").val(),
                        Lugar: $("#lugar").val(),
                        Folio: $("#folio").val(),
                        HoraYFecha: $("#fecha").val(),
                        ColoniaId: $("#colonia").val(),
                        Id: $("#ctl00_contenido_idDetencion").val(),
                        TrackingInfo: $("#ctl00_contenido_trackingIdDetencion").val(),
                        eventoId: $("#evento").val(),
                        Personanotifica: $("#personanotifica").val(),
                        Celular:$("#celular").val()
                    };
                    
                    Save(datos);
                }
            });                                

            function validar() {
                var esvalido = true;

                //$("#dt_basic_tabla_unidades").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                //    var data;
                //    var checkit = this.node().childNodes[0].childNodes[0].checked;

                //    if (!checkit) {
                //        ShowError("Unidad", "La unidad es obligatoria.");
                //        esvalido = false;

                //    }                        
                //}); 

                //if ($('#responsable').val() == "0" || $('#responsable').val() == null) {
                //    ShowError("Responsable", "El responsable de la unidad es obligatorio.");
                //    $('#responsable').parent().removeClass('state-success').addClass("state-error");
                //    $('#responsable').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#responsable').parent().removeClass("state-error").addClass('state-success');
                //    $('#responsable').addClass('valid');
                //}

                if ($("#motivo").val().split(" ").join("") == "") {
                    ShowError("Descripción del evento", "La descripción del evento es obligatoria.");
                    $('#motivo').parent().removeClass('state-success').addClass("state-error");
                    $('#motivo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#motivo').parent().removeClass("state-error").addClass('state-success');
                    $('#motivo').addClass('valid');
                }

                if ($("#descripcion").val().split(" ").join("") == "") {
                    ShowError("Descripción", "La descripción es obligatoria.");
                    $('#descripcion').parent().removeClass('state-success').addClass("state-error");
                    $('#descripcion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#descripcion').parent().removeClass("state-error").addClass('state-success');
                    $('#descripcion').addClass('valid');
                }

                if ($("#lugar").val().split(" ").join("") == "") {
                    ShowError("Lugar", "El lugar es obligatorio.");
                    $('#lugar').parent().removeClass('state-success').addClass("state-error");
                    $('#lugar').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#lugar').parent().removeClass("state-error").addClass('state-success');
                    $('#lugar').addClass('valid');
                }

                //if ($("#folio").val().split(" ").join("") == "") {
                //    ShowError("Folio", "El folio es obligatorio.");
                //    $('#folio').parent().removeClass('state-success').addClass("state-error");
                //    $('#folio').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#folio').parent().removeClass("state-error").addClass('state-success');
                //    $('#folio').addClass('valid');
                //}                

                if ($('#pais').val() == "0" || $('#pais').val() == null) {
                    ShowError("País", "El país es obligatorio.");
                    $('#pais').parent().removeClass('state-success').addClass("state-error");
                    $('#pais').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#pais').parent().removeClass("state-error").addClass('state-success');
                    $('#pais').addClass('valid');
                }

                if ($('#estado').val() == "0" || $('#estado').val() == null) {
                    ShowError("Estado", "El estado es obligatorio.");
                    $('#estado').parent().removeClass('state-success').addClass("state-error");
                    $('#estado').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#estado').parent().removeClass("state-error").addClass('state-success');
                    $('#estado').addClass('valid');
                }

                if ($('#municipio').val() == "0" || $('#municipio').val() == null) {
                    ShowError("Municipio", "El municipio es obligatorio.");
                    $('#municipio').parent().removeClass('state-success').addClass("state-error");
                    $('#municipio').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#municipio').parent().removeClass("state-error").addClass('state-success');
                    $('#municipio').addClass('valid');
                }

                if ($('#colonia').val() == "0" || $('#colonia').val() == null) {
                    ShowError("Colonia", "La colonia es obligatoria.");
                    $('#colonia').parent().removeClass('state-success').addClass("state-error");
                    $('#colonia').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#colonia').parent().removeClass("state-error").addClass('state-success');
                    $('#colonia').addClass('valid');
                }

                if ($("#codigoPostal").val().split(" ").join("") == "") {
                    ShowError("Código Postal", "El código postal es obligatorio.");
                    $('#codigoPostal').parent().removeClass('state-success').addClass("state-error");
                    $('#codigoPostal').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#codigoPostal').parent().removeClass("state-error").addClass('state-success');
                    $('#codigoPostal').addClass('valid');
                }

                return esvalido;
            }

            function limpiarEstilosCampos() {                
                $(".select").removeClass('valid');
                $(".select").removeClass('state-error');
                $(".select").removeClass('state-success');
                $(".input").removeClass('valid');
                $(".input").removeClass('state-error');
                $(".input").removeClass('state-success');        
                $('#fecha').parent().removeClass('state-success');
                $('#fecha').parent().removeClass('state-error');
                $('#fecha').parent().removeClass('valid');
            }

            function Save(datos) {
                startLoading();                
                $.ajax({                    
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'datos': datos
                    }),
                    cache: false,
                    success: function (data) {                                                
                        if (data.d.exitoso) {                                                     

                            $("#motivo").val(data.d.Motivo);
                            $("#ctl00_contenido_idDetencion").val(data.d.Id);
                            $("#ctl00_contenido_trackingIdDetencion").val(data.d.TrackingId);
                            limpiarEstilosCampos();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + data.d.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);

                            ShowSuccess("¡Bien hecho!", "La información se " + data.d.mensaje + " correctamente.");

                        } else {
                            ShowError("¡Error! Algo salió mal", data.d.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + data.d.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
            

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
              if (url.indexOf("~/") == 0) {
                  url = baseUrl + url.substring(2);
              }
              return url;
            }

        

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {                    
                    loadCalls("0");                                        
                    CargarDatos(param);
                    $("#ctl00_contenido_tracking").val(param);
                }

                if ($("#ctl00_contenido_KAQWPK").val() == "false")
                    $("#savebtn").hide();
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#add_").show();

                    if ($("#<%= editable.ClientID%>").val() == "0") {
                        $("#savebtn").hide();
                    }
                    else {
                        $("#savebtn").show();
                    }
                }
                else {
                    $("textarea,input, select").each(function(index, element){ $(this).attr('disabled', true); });
                }


                if ($("#ctl00_contenido_Ingreso").val() == "true") {
                    $("#linkingreso").show();
                }
                if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                    $("#linkinformacion").show();
                }
                if ($("#ctl00_contenido_Antropometria").val() == "true") {
                    $("#linkantropometria").show();
                }
                if ($("#ctl00_contenido_Señas_Particulares").val() == "true") {
                    $("#linkseñas").show();
                }
                if ($("#ctl00_contenido_Familiares").val() == "true") {
                    $("#linkfamiliares").show();
                }
                if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                    $("#linkexpediente").show();
                }
                if ($("#ctl00_contenido_Adicciones").val() == "true") {
                    $("#linkadicciones").show();
                }

                $("#linkinformacion").show();
                $("#linkestudios").show();
                $("#linkInfoDetencion").show();

                setTimeout(function () {
                    $("#dt_basic_tabla_unidades_filter input[type='search']").attr('tabindex', '3');
                    $("select[name=dt_basic_tabla_unidades_length]").attr('tabindex', '4');
                }, 1000);
            }

        });
    </script>
</asp:Content>
