﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="calls.aspx.cs" Inherits="Web.Application.Registry.calls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/calls.aspx">Llamadas y eventos</a></li>
    <li>Llamadas</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-phone "></i>
                Llamadas
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry" >
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="callform.aspx" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
        </div>
    </div>
    
    <p></p>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-calls-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar interno </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="select">
                                                <select name="peril" id="registro" runat="server">
                                                    <option value="0">[Perfil]</option>
                                                </select>
                                                <i></i>
                                            </label>
                                        </section>
                                    
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="nombre" id="nombre" runat="server" placeholder="Nombre" maxlength="256"/>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-calls-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-phone "></i></span>
                        <h2>Lista de llamadas </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                <thead>
                                    <tr>                                        
                                        <th data-class="expand">#</th>
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Involucrado</th>
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Descripción</th>
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Lugar</th>
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Colonia</th>                                      
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Código Postal</th>
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Municipio</th>                                      
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Estado</th>                                      
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Folio</th>
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Hora y Fecha</th>                                      
                                        <th data-hide="phone,tablet" <%--style="text-align:center"--%>>Acciones</th>                                       
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>   
    </div>
    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;la llamada  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="VYXMBM" runat="server" value=""/>
     <input type="hidden" id="RAWMOV" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

         
            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY": "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
               "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                        $('td', row).eq(7).addClass('strikeout');
                        $('td', row).eq(8).addClass('strikeout');
                        $('td', row).eq(9).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "calls.aspx/getllamadas",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [                    
                    {
                        name: "Id",
                        data: "Id"
                    },                    
                    {
                        name: "Involucrado",
                        data: "Involucrado"
                    },
                    {
                        name: "Descripcion",
                        data: "Descripcion"
                    },
                    {
                        name: "LugarDetencion",
                        data: "LugarDetencion"
                    },
                    {
                        name: "Asentamiento",
                        data: "Asentamiento"
                    },
                    {
                        name: "CodigoPostal",
                        data: "CodigoPostal"
                    },
                    {
                        name: "Municipio",
                        data: "Municipio"
                    },
                    {
                        name: "Estado",
                        data: "Estado"
                    },
                    {
                        name: "Folio",
                        data: "Folio"
                    },
                    {
                        name: "HoraYFecha",
                        data: "HoraYFecha"
                    },
                    null
                ],
                columnDefs: [                                     
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;                            
                        }
                    },
                    //{
                    //    targets: 9,
                    //    orderable: false,
                    //    render: function (data, type, row, meta) {
                    //        return new Date(row.HoraYFecha);                            
                    //    }
                    //},
                    {
                        targets: 10,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "edit";
                            var editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="callform.aspx?tracking=' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            var habilitar = "";
                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                edit = "edit";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                edit= "disabled";
                            }

                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="callform.aspx?tracking=' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Descripcion + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return  editar+habilitar;
                        }
                    }
                ]

            });           

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");

                window.emptytable = true;
                window.table.api().ajax.reload();
            });


            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "calls.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (data) {
                        var data = JSON.parse(data.d);
                        if (data.exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                data.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", data.mensaje);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });

            
            init();
            function init() {               
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }

            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
        </script>
</asp:Content>
