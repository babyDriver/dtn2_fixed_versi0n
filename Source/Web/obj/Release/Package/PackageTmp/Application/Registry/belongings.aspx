﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="belongings.aspx.cs" Inherits="Web.Application.Registry.belongings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Registry/entrylist.aspx">Registro en barandilla</a></li>
    <li>Pertenencias</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }        
        .errorInputTabla{
            background-color:#fff0f0;
            border-color:#A90329;
        }
        .scroll2 {
            height: 20vw;
            overflow: auto;
        }
        /*Se cambia debido al issue #1058, afectara solo al primer modal-lg*/
        /*.modal-lg{
            width: max-content;
        }*/
        .modal-lg-pertenencias{
            width: max-content;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>  
    <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-list-alt"></i>
                Información personal
            </h1>
        </div>
    </div>-->
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">

                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="row padding-bottom-10">
            <div class="menu-container" style="display: inline-block; float: right">
                <a style="display: none;font-size:smaller" class="btn btn-default btn-lg" title="Registro" id="linkingreso" href="javascript:void(0);"><i class="fa fa-edit"></i> Registro</a>
                <a style="display: none;font-size:smaller" class="btn btn-success txt-color-white btn-lg" id="linkInfoDetencion" title="Información de detención" href="javascript:void(0);"><i class="glyphicon glyphicon-eye-open"></i> Informe de detención</a> 
                <a style="display: none;font-size:smaller" class="btn btn-info btn-lg" title="Información personal" id="linkinformacion" href="javascript:void(0);"><i class="fa fa-list-alt"></i> Información personal</a>
                <a style="font-size:smaller" class="btn btn-primary txt-color-white btn-lg" id="linkinforme" title="Informe  del uso de la fuerza" href="javascript:void(0);"><i class="fa fa-hand-rock-o"></i> Informe  del uso de la fuerza</a>
                <a style="display: none;font-size:smaller" class="btn btn-warning btn-lg" title="Filiación" id="linkantropometria" href="javascript:void(0);"><i class="fa fa-language"></i> Filiación</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-purple txt-color-white btn-lg" id="linkseñas" title="Señas particulares" href="javascript:void(0);"><i class="glyphicon glyphicon-bookmark"></i> Señas particulares</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-yellow txt-color-white btn-lg" id="linkestudios" title="Antecedentes" href="javascript:void(0);"><i class=" fa fa-inbox"></i> Antecedentes</a>
                <a style="display: none;font-size:smaller" class="btn bg-color-red txt-color-white btn-lg" id="linkexpediente" title="Pertenencias" href="javascript:void(0);"><i class="glyphicon glyphicon-briefcase"></i> Pertenencias</a>
                <a style="margin-right:30px; font-size:smaller" class="btn bg-color-green txt-color-white btn-lg" id="linkvistas" title="Biométricos" href="javascript:void(0);"><i class="fa fa-paw"></i> Biométricos</a>
            </div>
        </div>
        <div class="scroll2" id="ScrollableContent">
    <%--            <div class="jarviswidget jarviswidget-color-darken" id="wid-study-3"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    </div>--%>
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-study-2"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-certificate"></i></span>
                        <h2>Pertenencias </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <p></p>
                            <div class="row" id="add_" style="display: none;">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                    <a href="javascript:void(0);" class="btn btn-md btn-default add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
                                </div>
                            </div>
                            <p></p>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover"  style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th ></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th>Pertenencia</th>
                                        <th data-hide="phone,tablet">Observacion</th>
                                        <th>Bolsa</th>
                                        <th>Cantidad</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="row smart-form">
                            <footer>
                                <div class="row" style="display: inline-block; float: right">
                                    <%--<a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-default" id="ligarNuevoEvento"><i class="fa fa-random"></i>&nbsp;Ligar a nuevo evento </a>--%>
                                    <a style="float: none;" href="entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg modal-lg-pertenencias" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <a href="javascript:void(0);" class="btn btn-md btn-default" id="addRow"><i class="fa fa-plus"></i>&nbsp;Agregar nueva fila </a>
                    <div id="register-form" class="smart-form ">
                        <div class="row">
                            <fieldset>
                                <%--<section>
                                    <label class="input">Contrato</label>
                                    <select name="contrato" id="contrato" style="width: 100%" class="select2">                                        
                                    </select>
                                    <i></i>
                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                </section>--%>
                                <section  class="col col-4">
                                     <label style="color: dodgerblue" class="input">Detenido <a style="color: red">*</a></label>
                                    <select name="interno" id="interno" style="width: 100%" class="select2"> 
                                    </select>
                                    <i></i>
                                </section>
                                <%--<section>
                                    <label class="input">Pertenencia</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-archive"></i>
                                        <input type="text" name="pertenencia" id="pertenencia" placeholder="Pertenencia" maxlength="512" class="alphanumeric" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la pertenencia.</b>
                                    </label>
                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                </section>
                                <section>
                                    <label class="input">Observación</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-commenting-o"></i>
                                        <input type="text" name="observacion" id="observacion" placeholder="Observación" maxlength="512" class="alphanumeric" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la observación.</b>
                                    </label>
                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                </section>
                                <section>
                                    <label class="input">Clasificación</label>
                                    <select name="clasificacion" id="clasificacion" style="width: 100%" class="select2">                                        
                                    </select>
                                    <i></i>
                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                </section>
                                <section>
								<label class="input">Casillero</label>
                                    <select name="casillero" id="casillero" style="width: 100%" class="select2">                                        
                                    </select>
                                    <i></i>
                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                </section>
                                <section>
                                    <label class="input">Bolsa</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-database"></i>
                                        <input type="number" name="bolsa" id="bolsa" min="0" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa el número de bolsa.</b>
                                    </label>
                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                </section>
                                <section>
                                    <label class="input">Cantidad</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-cubes"></i>
                                        <input type="number" name="cantidad" id="cantidad" min="0" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la cantidad.</b>
                                    </label>
                                    <div class="note note-error" style="color: red">Obligatorio</div>
                                </section>--%>                                                                
                                <div class="row" style="width:100%;margin:0 auto;">
                                    <table id="dt_nuevas_pertenencias" width="100%" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:5% !important;"></th>
                                                <th>Fecha de entrada</th>                                                
                                                <th>Pertenencia</th>                                                
                                                <th>Observaciones</th>
                                                <%--<th>Bolsa</th>--%>
                                                <th>Cantidad</th>
                                                <th>Clasificación</th>
                                                <th>Casillero</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <br />
                                <section  >
                                    <label style="color: dodgerblue" class="input">Fotografía</label>
                                    <label style="width:370px" class="input">
                                        <i  class="icon-append fa fa-file"></i>
                                        <asp:FileUpload  name="fotografia" id="fotografia" runat="server" />                                        
                                    </label>                                    
                                </section>                                                             
                            </fieldset>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar familiar
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro <strong><span id="itemeliminar"></span></strong>&nbsp;será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float:none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                                <a style="float:none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="photo-arrested" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía de la pertenencia / evidencia</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="#" alt="fotografía de la pertenencia / evidencia" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre Completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="~/Content/img/avatars/male.png" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="tracking" runat="server" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="Ingreso" runat="server" value="" />
    <input type="hidden" id="Informacion_Personal" runat="server" value="" />
    <input type="hidden" id="Antropometria" runat="server" value="" />
    <input type="hidden" id="Señas_Particulares" runat="server" value="" />
    <input type="hidden" id="Adicciones" runat="server" value="" />
    <input type="hidden" id="Expediente_Medico" runat="server" value="" />
    <input type="hidden" id="Familiares" runat="server" value="" />
    <input type="hidden" id="idInterno" value="" />
    <input type="hidden" id="editable" runat="server" value="" />
    <input type="hidden" id="Max" value="0" />
    <input type="hidden" id="Min" value="0" />
    <input type="hidden" id="Validar1"  />
    <input type="hidden" id="Validar2"  />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 39) {
                e.preventDefault();
                document.getElementById("linkvistas").click();
            }
            else if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                document.getElementById("linkestudios").click();
                if ($("#form-modal").is(":visible")) {
                    document.getElementById("btnsave").click();
                }
            }
            else if (e.keyCode === 27) {
                if ($("#form-modal").is(":visible")) {
                    $("#form-form-modal").modal("hide");
                }
            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                if ($("#form-modal").is(":visible")) {
                    document.getElementsByClassName("btnsave")[0].click();
                }
            }
        });

        $(document).ready(function () {

            $('body').on("keydown", function (e) {
                if (e.ctrlKey && e.which === 71 && $("#form-modal").is(":visible")) {
                    var hasSelected = false;

                    $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {

                        var checkit = this.node().childNodes[0].childNodes[0].checked;

                        if (checkit) {
                            hasSelected = true;
                        }
                    });

                    if (hasSelected) {
                        if (validarCamposEnTabla()) {
                            guardarImagen();
                        }
                    }
                    else {
                        ShowAlert("Atención!", " No ha seleccionado alguna pertenencia para registrar o actualizar.");
                    }
                }
            });

            $("#ScrollableContent").css("height", "calc(100vh - 443px)");

            $("#datospersonales-modal").on("hidden.bs.modal", function () {
                document.getElementsByClassName("detencion")[0].focus();
            });

            $("#form-modal").on("shown.bs.modal", function () {
                $("#addRow").focus();
            });

            $("#form-modal").on("hidden.bs.modal", function () {
                document.querySelector(".add").focus();
            });

            $("#ligarNuevoEvento").hide();
            if ($("#<%= editable.ClientID %>").val() == "0") {
                $("#ligarNuevoEvento").show();
            }

            $("#bolsa").on("keydown", function (e) {
                if (!((e.keyCode > 95 && e.keyCode < 106)
                    || (e.keyCode > 47 && e.keyCode < 58)
                    || e.keyCode == 8)) {
                    return false;
                }
            });

            $(document).on('keydown', 'input[pattern]', function (e) {
                var input = $(this);
                var oldVal = input.val();
                var regex = new RegExp(input.attr('pattern'), 'g');

                setTimeout(function () {
                    var newVal = input.val();
                    if (!regex.test(newVal)) {
                        input.val(oldVal);
                    }
                }, 0);
            });
            $("#cantidad").on("keydown", function (e) {
                if (!((e.keyCode > 95 && e.keyCode < 106)
                    || (e.keyCode > 47 && e.keyCode < 58)
                    || e.keyCode == 8)) {
                    return false;
                }
            });

            $("#ctl00_contenido_fotografia").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar1").val(false);
                    }
                    else {
                        $("#Validar1").val(true);
                    }
                }
                else {
                    $("#Validar1").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar2").val(false);
                    }
                    else {
                        $("#Validar2").val(true);
                    }
                }
                else {
                    $("#Validar2").val(true);
                }



            });


            GetMinMax();

            function GetMinMax() {
                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/GetParametrosTamaño",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var max = resultado.TMax;
                            var min = resultado.Tmin;

                            $("#Max").val(max);
                            $("#Min").val(min);
                            // alert(resultado.rutaimagenfrontal);
                            //var imagenAvatar = ResolveUrl(resultado.rutaimagenfrontal);
                            //  imagenfrontalbiometricos = imagenAvatar;

                            //$('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            //$("#ctl00_contenido_fileUpload1").val(imagenAvatar).clone(true);
                            $('#main').waitMe('hide');
                            //location.reload(true);
                            //CargarDatos(datos.TrackingId);



                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }


            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            //Variables tabla modal
            var responsiveHelper_dt_pertenencias_nuevas = undefined;
            var responsiveHelper_datatable_fixed_column_pertenencias_nuevas = undefined;
            var responsiveHelper_datatable_col_reorder_pertenencias_nuevas = undefined;
            var responsiveHelper_datatable_tabletools_pertenencias_nuevas = undefined;

            function loadTablePertenenciasByPertenencia(PertenenciaId) {
                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/getPertenenciasByPertenenciaId",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        interno: $("#idInterno").val(),
                        pertenenciaId: PertenenciaId
                    }),
                    cache: false,
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        responsiveHelper_dt_pertenencias_nuevas = undefined;
                        $("#dt_nuevas_pertenencias").DataTable().destroy();
                        createTable(resultado.list);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de pertenencias comunes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadTablePertenenciasComunes() {
                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/getPertenenciasComunes",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        interno: $("#idInterno").val()
                    }),
                    cache: false,
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        responsiveHelper_dt_pertenencias_nuevas = undefined;
                        $("#dt_nuevas_pertenencias").DataTable().destroy();
                        createTable(resultado.list);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de pertenencias comunes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }



            function createTable(data) {
                $('#dt_nuevas_pertenencias').dataTable({
                    "lengthMenu": [5, 10, 20, 50],
                    iDisplayLength: 5,
                    serverSide: false,
                    paging: true,
                    retrieve: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_pertenencias_nuevas) {
                            responsiveHelper_dt_pertenencias_nuevas = new ResponsiveDatatablesHelper($('#dt_nuevas_pertenencias'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_pertenencias_nuevas.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_pertenencias_nuevas.respond();
                        $('#dt_nuevas_pertenencias').waitMe('hide');
                    },
                    data: data,
                    columns: [
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        {
                            data: "Nombre",
                            visible: false
                        },
                        {
                            data: "Observacion",
                            visible: false
                        },
                        {
                            data: "Cantidad",
                            visible: false
                        },
                        {
                            data: "Casillero",
                            visible: false
                        },
                        {
                            data: "Clasificacion",
                            visible: false
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Id != null && row.TrackingId != null) {
                                    return "<input type='checkbox' data-tracking='" + row.TrackingId + "' data-id='" + row.Id + "' data-foto='" + row.Fotografia + "' />";
                                }
                                else {
                                    return "<input type='checkbox' data-tracking='' data-id='' data-foto='' />";
                                }
                            }
                        },
                        {
                            targets: 1,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.Fecha != null) {
                                    return "<input type='text' class='alptext' value='" + row.Fecha + "' style='width:100%' disabled='disabled' />";
                                }
                                else {
                                    return "<input type='text' class='alptext' value='' style='width:100%' disabled='disabled' />";
                                }
                            }
                        },
                        {
                            targets: 2,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Nombre != null) {
                                    return "<input type='text' class='alptext' value='" + row.Nombre + "' style='width:100%' data-required='true' />";
                                }
                                else {
                                    return "<input type='text' class='alptext' value='' style='width:100%' data-required='true' />";
                                }
                            }
                        },
                        {
                            targets: 3,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Observacion != null) {
                                    return "<textarea class='alptext' style='width:100%' maxlength='512' data-required='true'>" + row.Observacion + "</textarea > ";
                                }
                                else {
                                    return "<textarea class='alptext' style='width:100%'  maxlength='512' data-required='true'></textarea > ";
                                }
                            }
                        },

                        {
                            targets: 4,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Bolsa != null) {
                                    return "<input type='text' name='cantidadTabla' class='alptext'  maxlength='4' pattern='^[0-9]*$' style='width:100%' value='" + row.Cantidad + "' data-required='true' /> ";
                                }
                                else {
                                    return "<input type='text' name='cantidadTabla' class='alptext'  maxlength='4' pattern='^[0-9]*$' style='width:100%' value='' data-required='true' /> ";
                                }
                            }
                        },
                        {
                            targets: 5,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                var datos = cargarClasificaciones();
                                var options = "";

                                options += "<option value='0'>[Clasificación]</option>";

                                for (var i = 0; i < datos.length; i++) {
                                    if (parseInt(datos[i].Id) === row.ClasificacionId) {
                                        options += "<option value='" + datos[i].Id + "' selected='selected' >" + datos[i].Desc + "</option>";
                                    }
                                    else {
                                        options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                    }
                                }
                                return "<select style='width:100%' data-required='true'>" + options + "</select>";
                            }
                        },
                        {
                            targets: 6,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {

                                var datos = cargarCasilleros();
                                var options = "";

                                options += "<option value='0'>[Casillero]</option>";

                                for (var i = 0; i < datos.length; i++) {
                                    if (parseInt(datos[i].Id) == row.CasilleroId) {
                                        options += "<option value='" + datos[i].Id + "' selected='selected' >" + datos[i].Desc + "</option>";
                                    }
                                    else {
                                        if (row.CasilleroId != 0 && row.CasilleroId != undefined) {
                                            if (datos[i + 1] != undefined) {
                                                if (row.CasilleroId > datos[i].Id && row.CasilleroId < datos[i + 1].Id) {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                    options += "<option value='" + row.CasilleroId + "'selected='selected'>" + row.Casillero + "</option>";
                                                }
                                                else {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                }
                                            }
                                            else {
                                                if (row.CasilleroId > datos[i].Id) {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                    options += "<option value='" + row.CasilleroId + "'selected='selected'>" + row.Casillero + "</option>";
                                                }
                                                else {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                }
                                            }
                                        }
                                        else {
                                            options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                        }
                                    }
                                }

                                return "<select style='width:100%' data-required='true'>" + options + "</select>";
                            }
                        },
                        {
                            targets: 7,
                            render: function (data, type, row, meta) {
                                if (row.Nombre == null) {
                                    return "";
                                }
                                else {
                                    return row.Nombre;
                                }
                            }
                        },
                        {
                            targets: 8,
                            render: function (data, type, row, meta) {
                                if (row.Observacion == null) {
                                    return "";
                                }
                                else {
                                    return row.Observacion;
                                }
                            }
                        },
                        {
                            targets: 9,
                            render: function (data, type, row, meta) {
                                if (row.Cantidad == null) {
                                    return "";
                                }
                                else {
                                    return row.Cantidad;
                                }
                            }
                        },
                        {
                            targets: 10,
                            render: function (data, type, row, meta) {
                                if (row.Casillero == null) {
                                    return "";
                                }
                                else {
                                    return row.Casillero;
                                }
                            }
                        },
                        {
                            targets: 11,
                            render: function (data, type, row, meta) {
                                if (row.Clasificacion == null) {
                                    return "";
                                }
                                else {
                                    return row.Clasificacion;
                                }
                            }
                        }
                    ],
                    "fnRowCallback": function (nRow, data, iDisplayIndex, iDisplayIndexFull) {

                        if (data.TrackingId != "" && data.TrackingId != null) {

                            $('td', nRow).css('background-color', '#DDF4C2');
                        }


                    }
                });

                $("#dt_nuevas_pertenencias").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                ]).draw(false);
            }

            $("#addRow").click(function () {
                $("#dt_nuevas_pertenencias").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                ]).draw(false);
            });

            var dataClasificaciones = null;
            var dataCasilleros = null;
            var datosClasificaciones = [];
            var datosCasilleros = [];
            function cargarCasillerosAll() {
                if (dataCasilleros === null && datosCasilleros.length === 0) {

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "belongings.aspx/getCasillerosAll",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            dataCasilleros += "<option></option>"
                            $.each(response.d, function (index, item) {
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;
                                datosCasilleros.push(tt);
                                dataCasilleros += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                            });
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                return datosCasilleros;
            }

            function cargarCasilleros() {
                if (dataCasilleros === null && datosCasilleros.length === 0) {

                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "belongings.aspx/getCasilleros",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            dataCasilleros += "<option></option>"
                            $.each(response.d, function (index, item) {
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;
                                datosCasilleros.push(tt);
                                dataCasilleros += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                            });
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                return datosCasilleros;
            }


            function cargarClasificaciones() {

                if (dataClasificaciones === null && datosClasificaciones.length === 0) {
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "belongings.aspx/getClasificacion",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        success: function (response) {
                            dataClasificaciones += "<option></option>";

                            $.each(response.d, function (index, item) {
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;
                                datosClasificaciones.push(tt);
                                dataClasificaciones += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                            });
                        },
                        error: function () {

                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");

                        }
                    });
                }
                return datosClasificaciones;
            }

            init();
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $("body").on("click", "#avatar2", function (e) {
                var photo = $(this).context.src;
                $("#foto_detenido").attr("src", photo);
                $("#foto_detenido").attr("style", "width: 250px; height: 250px");
                $("#photo-arrested").modal("show");
            });

            function loadInternos(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/getInternos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tkg: RequestQueryString("tracking") || ""
                    }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#interno');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Interno]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadClasificacion(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/getClasificacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#clasificacion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Clasificacion]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadCasilleros(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/getCasilleros",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#casillero');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Casillero]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            window.emptytable = true;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "belongings.aspx/getlist",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.tracking = $("#ctl00_contenido_tracking").val();
                        parametrosServerSide.emptytable = false;        //window.emptytable;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    null,
                    null,
                    null,
                    {
                        name: "Pertenencia",
                        data: "Pertenencia"
                    },
                    {
                        name: "Observacion",
                        data: "Observacion"
                    },
                    {
                        name: "Bolsa",
                        data: "Bolsa"
                    },
                    {
                        name: "Cantidad",
                        data: "Cantidad"
                    },
                    null

                ],
                columnDefs: [
                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.Fotografia != "" && row.Fotografia != null) {

                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.Fotografia) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + resolveUrl(row.Fotografia) + '" height="10" width="50" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("~/Content/img/avatars/object.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt = "" src = "' + pathfoto + '" height = "10" width = "50"/>' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "";

                            var editar = "";
                            var habilitar = "";

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                edit = "edit";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                edit = "disabled";
                            }

                            var disabled = "";
                            if ($("#<%= editable.ClientID %>").val() == "0") disabled = "disabled";

                            editar = '<a class="btn btn-primary btn-circle ' + edit + ' ' + disabled + '" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Pertenencia + '" data-tracking="' + row.TrackingId + '" data-observacion="' + row.Observacion + '" data-habilitado="' + row.Habilitado + '" data-clasificacion= "' + row.Clasificacion + '" data-casillero= "' + row.CasilleroId + '" data-bolsa="' + row.Bolsa + '" data-cantidad="' + row.Cantidad + '" data-fotografia="' + row.Fotografia + '" title = "Editar" > <i class="glyphicon glyphicon-pencil"></i></a>&nbsp; ';
                            habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem ' + disabled + '" href="javascript:void(0);" data-value="' + row.Pertenencia + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                            if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                                editar = '<a class="btn btn-primary btn-circle ' + edit + ' ' + disabled + '" href="javascript:void(0);" data-id="' + row.Id + '" data-value = "' + row.Pertenencia + '" data-tracking="' + row.TrackingId + '" data-observacion="' + row.Observacion + '" data-habilitado="' + row.Habilitado + '" data-clasificacion= "' + row.Clasificacion + '" data-bolsa="' + row.Bolsa + '" data-cantidad="' + row.Cantidad + '" data-fotografia="' + row.Fotografia + '"  title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;'
                                habilitar = "";
                            }
                            return editar + habilitar;
                        }
                    },
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    }
                ]
            });

            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);
                            $('#edadInterno').text(resultado.obj.Edad);
                            $('#sexoInterno').text(resultado.obj.Sexo);
                            $('#municipioInterno').text(resultado.obj.municipioNombre);
                            $('#domicilioInterno').text(resultado.obj.domiclio);
                            $('#coloniaInterno').text(resultado.obj.coloniaNombre);

                            $("#linkexpediente").attr("href", "belongings.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkvistas").attr("href", "huella.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkestudios").attr("href", "record.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkseñas").attr("href", "signal.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkantropometria").attr("href", "anthropometry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinforme").attr("href", "informedetencion.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkinformacion").attr("href", "personalinformation.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkingreso").attr("href", "entry.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                            $("#linkInfoDetencion").attr("href", "informaciondetencion.aspx?tracking=" + $("#ctl00_contenido_tracking").val() + "&editable=" + $("#<%= editable.ClientID %>").val());
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información del interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            //$('#ctl00_contenido_fecha').mask('99/99/9999', { placeholder: "mm/dd/yyyy" });

            $("body").on("click", ".blockitem", function () {

                var itemnameblock = $(this).attr("data-value");

                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "belongings.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                " " + JSON.parse(data.d).message + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", " " + JSON.parse(data.d).message);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal, " + JSON.parse(data.d).message + ". Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal, " + JSON.parse(data.d).message + "", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function (error) {
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                            "Algo salió mal, " + error + ". Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                        setTimeout(hideMessage, hideTime);
                        ShowError("¡Error! Algo salió mal, " + error + "", "Si el problema persiste, contacte al personal de soporte técnico. ");
                    }
                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });

            function clearModal() {
                $(".input").removeClass('state-success');
                $(".input").removeClass('state-error');
                $(".input").removeClass('valid');
            }

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_estudio").val("0");
                $("#ctl00_contenido_subestudio").val("0");
                $("#ctl00_contenido_elaboro").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#ctl00_contenido_ocupacion").val("0");
                $("#ctl00_contenido_txtestudio").val("");
                //$("#ctl00_contenido_fecha").val("");
            });

            $('#interno').on('select2:opening', function (evt) {
                if (this.disabled) {
                    return false;
                }
            });

            $("body").on("click", ".add", function () {
                loadTablePertenenciasComunes();
                $("#interno").prop("disabled", true);

                limpiar();
                loadInternos($("#idInterno").val());
                //loadClasificacion("0");
                //loadCasilleros("0");
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#form-modal-title").empty();
                $('#habilitado').prop('checked', false);
                $("#form-modal-title").html("<i class='fa fa-pencil'+></i> Agregar Registro");
                $("#form-modal").modal("show");
            });

            $("#ctl00_contenido_estudio").change(function () {
                CargarListadoSubestudios(0, $("#ctl00_contenido_estudio").val());
            });




            $("body").on("click", ".edit", function () {
                limpiar();
                loadTablePertenenciasByPertenencia($(this).attr("data-id"));
                $("#ctl00_contenido_lblMessage").html("");
                //var id = $(this).attr("data-id");
                //var tracking = $(this).attr("data-tracking");
                //var habilitado = $(this).attr("data-habilitado");

                $("#interno").prop("disabled", true);
                loadInternos($("#idInterno").val());


                $("#form-modal-title").empty();
                $("#form-modal-title").html("<i class='fa fa-pencil'+></i> Editar Registro");
                $("#form-modal").modal("show");
            });


            $("body").on("click", ".delete", function () {
                var id = $(this).attr("data-tracking");
                var nombre = $(this).attr("data-value");
                $("#btndelete").attr("data-id", id);
                $("#delete-modal").modal("show");
            });

            function validarCamposEnTabla() {
                var isValid = true;

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {

                    var checkit = this.node().childNodes[0].childNodes[0].checked;

                    if (checkit) {
                        var nombre = this.node().childNodes[2].childNodes[0].getAttribute("data-required");
                        var observacion = this.node().childNodes[3].childNodes[0].getAttribute("data-required");
                        //var bolsa = this.node().childNodes[4].childNodes[0].getAttribute("data-required");                        
                        var cantidad = this.node().childNodes[4].childNodes[0].getAttribute("data-required");
                        var clasificacion = this.node().childNodes[5].childNodes[0].getAttribute("data-required");
                        var casillero = this.node().childNodes[6].childNodes[0].getAttribute("data-required");

                        if (nombre === "true") {
                            if (this.node().childNodes[2].childNodes[0].value === "" ||
                                this.node().childNodes[2].childNodes[0].value === undefined ||
                                this.node().childNodes[2].childNodes[0].value === null) {
                                this.node().childNodes[2].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Pertenencia", "El campo pertenencia es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[2].childNodes[0].removeAttribute('class');
                            }
                        }

                        if (observacion === "true") {
                            if (this.node().childNodes[3].childNodes[0].value === "" ||
                                this.node().childNodes[3].childNodes[0].value === undefined ||
                                this.node().childNodes[3].childNodes[0].value === null) {
                                this.node().childNodes[3].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Observación", "El campo observación es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[3].childNodes[0].removeAttribute('class');
                            }
                        }

                        //if (bolsa === "true") {
                        //    var val = parseInt(this.node().childNodes[4].childNodes[0].value);                                                        

                        //    if (isNaN(val)) {
                        //        this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                        //        ShowError("Bolsa", "El campo bolsa tiene un valor incorrecto.");
                        //        isValid = false;                                
                        //    }                            

                        //    if (val <= 0) {
                        //        this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                        //        ShowError("Bolsa", "El campo bolsa debe tener un valor mayor a cero.");
                        //        isValid = false;
                        //    }


                        //    if (!isNaN(val) && val > 0) {
                        //        this.node().childNodes[4].childNodes[0].removeAttribute('class');
                        //    }
                        //}

                        if (cantidad === "true") {
                            var val = parseInt(this.node().childNodes[4].childNodes[0].value);

                            if (isNaN(val)) {
                                this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Cantidad", "El campo cantidad es obligatorio.");
                                isValid = false;
                            }

                            if (val <= 0) {
                                this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Cantidad", "El campo cantidad debe tener un valor mayor a cero.");
                                isValid = false;
                            }


                            if (!isNaN(val) && val > 0) {
                                this.node().childNodes[4].childNodes[0].removeAttribute('class');
                            }
                        }

                        if (clasificacion === "true") {
                            if (this.node().childNodes[5].childNodes[0].value === "0") {
                                this.node().childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Clasificación", "El campo clasificación es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[5].childNodes[0].removeAttribute('class');
                            }
                        }

                        if (casillero === "true") {
                            if (this.node().childNodes[6].childNodes[0].value === "0") {
                                this.node().childNodes[6].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Casillero", "El campo casillero es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[6].childNodes[0].removeAttribute('class');
                            }
                        }

                        var file = document.getElementById('<% = fotografia.ClientID %>').value;
                        if (file != null && file != '') {

                            if (!validaImagen(file)) {
                                ShowError("Fotografía", "Solo se permiten extensiones .jpg, .jpeg o .png");
                                isValid = false;

                            }
                            else {
                                var validar = $("#Validar1").val();
                                var validar2 = $("#Validar2").val();


                                if ($("#Max").val() != "0") {
                                    if (validar == 'false') {
                                        ShowError("Fotografía", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                        isValid = false;
                                    }

                                }
                                if ($("#Min").val() != "0") {
                                    var s = $("#Min").val() * 1024;
                                    if (validar2 == 'false') {
                                        ShowError("Fotografía", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                        isValid = false;
                                    }
                                }
                            }
                        }

                    }
                });

                return isValid;
            }

            function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }

                }
                return false;
            }
            $("body").on("click", ".save", function () {
                var hasSelected = false;

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {

                    var checkit = this.node().childNodes[0].childNodes[0].checked;

                    if (checkit) {
                        hasSelected = true;
                    }
                });

                if (hasSelected) {
                    if (validarCamposEnTabla()) {
                        guardarImagen();
                    }
                }
                else {
                    ShowAlert("Atención!", " No ha seleccionado alguna pertenencia para registrar o actualizar.");
                }
            });

            function obtenerDatosTabla(imagen) {

                var dataArreglo = new Array();
                var pertenencia;

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {

                    var checkit = this.node().childNodes[0].childNodes[0].checked;

                    if (checkit) {
                        pertenencia = {};
                        pertenencia.Fecha = this.node().childNodes[1].childNodes[0].value;
                        pertenencia.Pertenencia = this.node().childNodes[2].childNodes[0].value;
                        pertenencia.Observacion = this.node().childNodes[3].childNodes[0].value;
                        //pertenencia.Bolsa = this.node().childNodes[4].childNodes[0].value;
                        pertenencia.Cantidad = this.node().childNodes[4].childNodes[0].value;
                        pertenencia.Clasificacion = this.node().childNodes[5].childNodes[0].value;
                        pertenencia.Casillero = this.node().childNodes[6].childNodes[0].value;
                        pertenencia.TrackingId = this.node().childNodes[0].childNodes[0].getAttribute("data-tracking");
                        pertenencia.Id = this.node().childNodes[0].childNodes[0].getAttribute("data-id");

                        if (imagen != null && imagen != undefined && imagen != "") {
                            pertenencia.Fotografia = imagen;
                        }
                        else {
                            pertenencia.Fotografia = this.node().childNodes[0].childNodes[0].getAttribute("data-foto");
                        }

                        dataArreglo.push(pertenencia);
                    }
                });

                return dataArreglo;
            }

            function guardarImagen() {

                var files = $("#ctl00_contenido_fotografia").get(0).files;
                var nombreAvatarAnterior = $("#btnsave").attr("data-fotografia");
                var nombreAvatar = "";

                if (files.length > 0) {

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "../Handlers/FileUploadHandler.ashx?action=4&before=" + nombreAvatarAnterior,
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (Results) {
                            if (Results.exitoso) {
                                nombreAvatar = Results.nombreArchivo;
                                var datos = obtenerDatosTabla(nombreAvatar);

                                Save(datos, $("#interno").val());
                            }
                            else {
                                ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                            }
                        },
                        error: function (err) {
                            ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else {


                    var foto = (nombreAvatarAnterior == undefined) ? "" : nombreAvatarAnterior
                    var datos = obtenerDatosTabla(foto);


                    Save(datos, $("#interno").val());
                }
            }


            function limpiar() {
                //            $("#pertenencia").val("");
                //            $("#observacion").val("");
                //            $("#clasificacion").val("");
                //$("#casillero").val("");
                //            $("#bolsa").val("");
                //            $("#cantidad").val("");
                $("#ctl00_contenido_fotografia").val(null);
                //$("#casillero").val("");
            }

            function validar() {
                var esvalido = true;

                if ($('#interno').val() == "0" || $('#interno').val() == null) {
                    ShowError("Interno", "El interno es obligatorio.");
                    $("#interno").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#interno").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($("#pertenencia").val().split(" ").join("") == "") {
                    ShowError("Pertenencia", "La pertenencia es obligatoria.");
                    $('#pertenencia').parent().removeClass('state-success').addClass("state-error");
                    $('#pertenencia').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#pertenencia').parent().removeClass("state-error").addClass('state-success');
                    $('#pertenencia').addClass('valid');
                }

                if ($("#observacion").val().split(" ").join("") == "") {
                    ShowError("Observación", "La observación es obligatoria.");
                    $('#observacion').parent().removeClass('state-success').addClass("state-error");
                    $('#observacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observacion').parent().removeClass("state-error").addClass('state-success');
                    $('#observacion').addClass('valid');
                }

                if ($('#clasificacion').val() == "0" || $('#interno').val() == null) {
                    ShowError("Clasificación", "La clasificación es obligatoria.");
                    $("#clasificacion").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#clasificacion").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#casillero').val() == "0" || $('#interno').val() == null) {
                    ShowError("casillero", "El casillero es obligatorio.");
                    $("#casillero").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#casillero").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }
                if ($("#bolsa").val().split(" ").join("") == "") {
                    ShowError("Bolsa", "La bolsa es obligatoria.");
                    $('#bolsa').parent().removeClass('state-success').addClass("state-error");
                    $('#bolsa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#bolsa').parent().removeClass("state-error").addClass('state-success');
                    $('#bolsa').addClass('valid');
                }

                if ($("#cantidad").val().split(" ").join("") == "") {
                    ShowError("Cantidad", "La cantidad es obligatoria.");
                    $('#cantidad').parent().removeClass('state-success').addClass("state-error");
                    $('#cantidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#cantidad').parent().removeClass("state-error").addClass('state-success');
                    $('#cantidad').addClass('valid');
                }

                return esvalido;
            }

            function Save(datos, interno) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'pertenencias': datos, interno: interno
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#form-modal").modal("hide");
                            clearModal()
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");

                            var ruta = resolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            location.reload(true);

                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("#btndelete").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "criminologicalstudies.aspx/delete",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El registro fue eliminado.", "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "El registro fue eliminado.");
                            $("#delete-modal").modal("hide");
                            $('#main').waitMe('hide');
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }

                    }
                });
            });

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            function loadInternoId(param) {

                $.ajax({
                    type: "POST",
                    url: "belongings.aspx/getInternoId",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: param,
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        if (resultado.exitoso) {
                            $("#idInterno").val(resultado.Id);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar el Id del interno. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    CargarDatos(param);
                    loadInternoId(param);
                }

                if ($("#ctl00_contenido_KAQWPK").val() == "false") {
                    $("#btnsave").hide();
                    $("#interno").prop('disabled', true);
                    $("#pertenencia").prop('disabled', true);
                    $("#observacion").prop('disabled', true);
                    $("#clasificacion").prop('disabled', true);
                    $("#bolsa").prop('disabled', true);
                    $("#cantidad").prop('disabled', true);
                    $("#ctl00_contenido_fotografia").prop('disabled', true);
                    $("#casillero").prop('disabled', true);
                }

                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    if ($("#<%= editable.ClientID %>").val() == "0") {
                        $("#add_").hide();
                    }
                    else {
                        $("#add_").show();
                    }
                }
                else {
                    $("textarea,input, select").each(function (index, element) { $(this).attr('disabled', true); });
                }

                if ($("#ctl00_contenido_Ingreso").val() == "true") {
                    $("#linkingreso").show();
                }
                if ($("#ctl00_contenido_Informacion_Personal").val() == "true") {
                    $("#linkinformacion").show();
                }
                if ($("#ctl00_contenido_Antropometria").val() == "true") {
                    $("#linkantropometria").show();
                }
                if ($("#ctl00_contenido_Señas_Particulares").val() == "true") {
                    $("#linkseñas").show();
                }
                if ($("#ctl00_contenido_Familiares").val() == "true") {
                    $("#linkfamiliares").show();
                }
                if ($("#ctl00_contenido_Expediente_Medico").val() == "true") {
                    $("#linkexpediente").show();
                }
                if ($("#ctl00_contenido_Adicciones").val() == "true") {
                    $("#linkadicciones").show();
                }

                $("#linkestudios").show();
                $("#linkInfoDetencion").show();
            }

            $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
                var param = RequestQueryString("tracking");
                CargarDatosModal(param);
            });

            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 443px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');

                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 216px)");
            });

        });
    </script>
</asp:Content>