﻿$(function () {
    var fullpath = window.location.href;                    /*Obtiene URL completa */
    var mlseg_warnAfter = 900000; /* 4 horas*/
    var mlseg_redirAfter = 1200000; /* 3.40 horas*/

    var textparam = "";
    var pathname = window.location.pathname;                /*Obtiene sola path sin dominio*/
    var posicionparam = fullpath.indexOf("?");              /*Busca posición del caracter ? (Base cero) */
    if (posicionparam != -1) {                              /*Verifica si se encontro posición del caracter ? */
        textparam = fullpath.substr(posicionparam);         /*Obtiene texto a partir de la posicion del caracter ? */
    }
    var d = new Date();
    var d_ = d.getDate();
    var m = d.getMonth();
    var a = d.getFullYear();
    var h = d.getHours();
    var m_ = d.getMinutes();
    var s = d.getSeconds();
    var f = d_.toString() + m.toString() + a.toString() + h.toString() + m_.toString() + s.toString();
    var urlAlive = $.relativepath + 'keepAlive.aspx?d=' + f;
    var urlSignout = $.relativepath + 'Signout.aspx?url=' + pathname + textparam;
    $.sessionTimeout({
        title: 'La sesión esta apunto de expirar',
        message: '',
        keepAliveButton: 'Si',
        logoutButton: 'No',
        keepAliveUrl: urlAlive,
        keepAlive: true,
        keepAliveInterval: 3000,
        logoutUrl: urlSignout,
        redirUrl: urlSignout,
        warnAfter: mlseg_warnAfter,
        redirAfter: mlseg_redirAfter,
        countdownMessage: 'Tu sesión terminará en {timer}. ¿Deseas permanecer en el sistema?',
        ignoreUserActivity: true,
        countdownSmart: true,
    });
    
    /*
    Ayuda sobre parametros de sessionTimeout https://github.com/orangehill/bootstrap-session-timeout
    title:              Titulo de la ventana 
    message:            Mensaje
    keepAliveButton     Texto del botón para mantener activa la sessión
    logoutButton        Texto del botón para terminar la sessión
    keepAliveUrl        Url para mantener activa la sesión
    keepAlive           True para activar funcionamiento de keepAliveUrl
    keepAliveInterval   Tiempo en milisegundos para entrar a url definida en keepAliveUrl
    logoutUrl           Url a redireccionar cuando presione el boton logoutButton
    redirUrl            Url a redireccionar cuando no presione ningun botón (keepAliveButton y logoutButton)
    warnAfter:          Tiempo en milisegundos despúes que la pagina es abierta y muestra el mensaje de advertencia. 1140000 = 19 minutos  
    redirAfter:         Tiempo en milisegundos a transcurrir una vez mostrada la advertencia y antes de redireccionar a la pagina redirUrl. 1200000 = 20 minutos
    countdownMessage    Mensaje para mostrar los segundos que quedan antes de terminar la sesion
    ignoreUserActivity  Ignorar actividad del usuario
    countdownSmart      True muestre los segundos en el mensaje de advertencia
    */
});

