﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="error-500.aspx.cs" Inherits="Web.Application.error_500" %>

<!DOCTYPE html>
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><html class="no-js"><!--<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta charset="utf-8" />
   <title><%= ConfigurationManager.AppSettings["Titulo"]  %></title>
	<meta name="author" content="Ana Lilia Ramirez"/>
	<meta name="robots" content="noindex, nofollow"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
        
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/css/smartadmin-production-plugins.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/css/smartadmin-production.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/css/smartadmin-skins.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/css/smartadmin-rtl.min.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700" />
    
    <style>
        /*li{
            margin: 10px 0;
        }
        body {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/Content/img/fondo.jpg', sizingMethod='scale');
            -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/Content/img/fondo.jpg', sizingMethod='scale')";
            background: url(/Content/img/fondo.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }*/

        .page-header {
            background: #F0F0F0;
        }
    </style>
</head>
<body>
     <div class="page-header">
        <div style="text-align: center;">
            <img src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/logo.png" id="Img1" style="height: 60px" />
        </div>
        <br />
    </div>

    <div id="page-container" style="padding-top: 63px;">
        <div id="content" class="container">
            <div class="row">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-4">
                    <div class="center-div">
                        <div class="well">
                            <h1 class="font-xl">
                                <span class="" style="color:#138993"><i class="ace-icon fa fa-random"></i> 500 -</span>
                                  Algo salió mal :(
                            </h1>
                     
                            <hr />

                            <h3>Pero estamos trabajando para <i class="ace-icon fa fa-wrench" ></i>corregirlo!</h3>

                            <div>
                                <div class="space"></div>
                                <h6>Mientras tanto, intenta lo siguiente:</h6>

                                <ul class="list-unstyled list-inline">
                                    <li>
                                        <i class="ace-icon fa fa-hand-o-right txt-color-teal"></i>
                                        Lee el manual de usuario.
                                    </li>
                                     <li>
                                        <i class="ace-icon fa fa-hand-o-right txt-color-teal"></i>
                                        Vuelve a iniciar sesión en el sistema.
                                    </li>
                                    <li>
                                        <i class="ace-icon fa fa-hand-o-right txt-color-teal"></i>
                                        Dinos con detalle cómo es que ocurre el error.   
                                    </li>
                                </ul>
                            </div>
                            <hr />
                            <div class="space"></div>
                            <div style="text-align:center">
                                <a href="javascript:history.back()" class="btn btn-lg btn-default">
                                    <i class="ace-icon fa fa-arrow-left"></i>
                                    Regresar
                                </a>
                                <a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/dashboard.aspx" class="btn btn-lg btn-default">
                                    <i class="glyphicon glyphicon-home"></i>
                                    Ir al inicio
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">&nbsp;</div>
            </div>
        </div>
    </div>
</body>
</html>

