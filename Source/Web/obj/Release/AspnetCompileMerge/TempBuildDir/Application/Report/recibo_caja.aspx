﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="recibo_caja.aspx.cs" Inherits="Web.Application.Report.recibo_caja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">   
    <li>Reportes</li>
    <li>Caja</li>
    <li>Recibos de caja</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-file-text-o fa-fw "></i>
                Recibos de pago de caja
            </h1>
        </div>
    </div>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <p></p>
    <div class="scroll">
    <section id="widget-grid" class="">
        <div class="row">            
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-ticket"></i></span>
                        <h2>Recibos de pago de caja </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div class="row">
                                    <section class="col col-md-12" id="BotonCaja">
                                        <a class="btn btn-md btn-default " href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Caja/caja.aspx"" style="padding:5px">
                                            <i class="fa fa-mail-reply"></i> Regresar a caja</a>
                                    </section>
                                </div>
                                <br />
                            <table id="dt_basic_multas" class="table table-striped table-bordered table-hover" width="100%" >
                                <thead>
                                    <tr>                                             
                                        <th data-class="expand">#</th>
                                        <th data-class="expand">Fotografía</th>
                                        <th>Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                        <th>Monto </th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {                        
            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };                     

            //Variables tabla de multas
            var responsiveHelper_dt_basic_multas = undefined;
            var responsiveHelper_datatable_fixed_column_multas = undefined;
            var responsiveHelper_datatable_col_reorder_multas = undefined;
            var responsiveHelper_datatable_tabletools_multas = undefined;

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }
            ValidarBoton();

            function ValidarBoton() {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/corte_caja.aspx/ValidarBoton",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            if (resultado.Boton) {

                                $("#BotonCaja").show();
                            }
                            else {
                                $("#BotonCaja").hide();
                            }
                        }
                        else {
                            ShowError("¡Error! No fue posible validar si mostrar el boton", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }
            var rutaDefaultServer = "";
              getRutaDefaultServer();

            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "recibo_caja.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }
                    }
                });
            }

                function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".imprimir", function () {
                data = [
                    TrackingCalificacion = $(this).attr("data-trackingcalificacion"),
                    TrackingEstatus = $(this).attr("data-trackingestatus"),
                    TrackingCalificacionIngreso = $(this).attr("data-calingtracking")
                ];                                        
                imprimirMulta(data);                    
            });

            function imprimirMulta(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "recibo_caja.aspx/imprimirMulta",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {                            
                            var ruta = ResolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }                                                        
                            $('#main').waitMe('hide');                                                                                  
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! No fue posible guardar el pago de la multa", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $('#dt_basic_multas').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_multas) {
                        responsiveHelper_dt_basic_multas = new ResponsiveDatatablesHelper($('#dt_basic_multas'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_multas.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_multas.respond();
                    $('#dt_basic_multas').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "recibo_caja.aspx/getMultas",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id"
                    },                  
                    {
                        name: "RutaImagen",
                        data: "RutaImagen"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "TotalAPagar",
                        data: "TotalAPagar"
                    },
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false

                    }
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                              if (row.RutaImagen != "" && row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");                                
                                var imgAvatar = resolveUrl(photo);                                
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                return '<div class="text-center">' +
                                    '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "imprimir";
                            var registro =  '<a class="btn btn-success btn-sm ' + edit + '" href="javascript:void(0);" data-trackingCalificacion="' + row.CalificacionTracking + '" data-trackingEstatus = "' + row.EstatusTracking + '" data-totalAPagar="' + row.TotalAPagar + '" data-calingtracking ="' + row.CalificacionIngresoTracking + '" title="Imprimir"><i class="glyphicon glyphicon-file"></i>&nbsp;&nbsp;Generar PDF</a>&nbsp;'

                            return registro;
                        }
                    }
                ]
            });

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
