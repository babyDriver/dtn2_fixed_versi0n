﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="proximosacumplir_report.aspx.cs" Inherits="Web.Application.Report.proximosacumplir_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
    <li>Juez calificador</li>
    <li>Próximos a cumplir</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-file-text-o fa-fw "></i>
              Detenidos próximos a cumplir
            </h1>
        </div>
    </div>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid-proximos" class="">
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                      <span class="widget-icon"><i class="fa fa-file-text-o fa-fw "></i></span>

                        <h2>Detenidos próximos a cumplir </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div id="smart-form-register-entry" class="smart-form">
                            <fieldset>
                               
                                <div class="row">
                                    <section class="col col-md-12">
                                        <a class="btn btn-success reporte" style="padding:5px">
                                            <i class="glyphicon glyphicon-file"></i> Generar PDF</a>
                                    </section>
                                    </div>
                                </fieldset>
                            </div>
                            <table id="dt_basic_recibos" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">#</th>
                                                            <th>No. remisión</th>
                                                            <th>Nombre</th>
                                                            <th>Apellido paterno</th>
                                                            <th data-hide="phone,tablet">Apellido materno</th>
                                        <th>Registro</th>
                                                            <th data-hide="phone,tablet">Situación</th>
                                                            <th data-hide="phone,tablet">Horas</th>
                                        <th>Multa</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        </section>
</div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();

            var responsiveHelper_dt_basic_recibos = undefined;
            var responsiveHelper_datatable_fixed_column_recibos = undefined;
            var responsiveHelper_datatable_col_reorder_recibos = undefined;
            var responsiveHelper_datatable_tabletools_recibos = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            
            $("body").on("click", ".reporte", function () {
                $("#ctl00_contenido_lblMessage").html("");
                pdf();
            });

             $('#dt_basic_recibos').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                 autoWidth: true,
               
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_recibos) {
                        responsiveHelper_dt_basic_recibos = new ResponsiveDatatablesHelper($('#dt_basic_recibos'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_recibos.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_recibos.respond();
                    $('#dt_basic_recibos').waitMe('hide');
                },

                //"createdRow": function (row, data, index) {
                //    if (!data["Habilitado"]) {
                //        $('td', row).eq(1).addClass('strikeout');
                //        $('td', row).eq(2).addClass('strikeout');
                //        $('td', row).eq(3).addClass('strikeout');

                //    }
                //},
                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/proximosacumplir_report.aspx/getDetenidos",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Nombre"
                    },
                    {
                        name: "Remision",
                        data: "Expediente"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },
                    {
                        name: "Situacion",
                        data: "Situacion"
                    },                    
                    {
                        name: "Horas",
                        data: "TotalHoras"
                    },
                    {
                        name: "Multa",
                        data: "TotalAPagar"
                    },
                    {
                        name: "Multa",
                        data: "NombreCompleto",
                        visible: false

                    }
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    }
                ]
            });

            function pdf() {
                
                $.ajax({

                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/proximosacumplir_report.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.message + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.message);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>

