﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="cliente_edit.aspx.cs" Inherits="Web.Application.Admin.cliente_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Admin/cliente.aspx?name=escolaridad">Configuración</a></li>
    <li><a href="cliente.aspx">Clientes</a></li>
    <li>Registro</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-useredit-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-edit"></i></span>
                    <h2>Cliente </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">                        
                        <div id="smart-form-register" class="smart-form">
                            <header id="header_form">
                                Formulario de registro
                            </header>
                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <label>Cliente <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="cliente" id="idcliente" placeholder="Cliente" maxlength="200" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el cliente.</b>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>RFC <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="rfc" id="rfc" placeholder="RFC" maxlength="13" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el RFC.</b>
                                        </label>
                                        <%--<div class="note note-error" style="color: red">Obligatorio</div>--%>
                                    </section>
                                </div>
                            </fieldset>
                                <footer>
                                    <div class="row" style="display: inline-block; float: right; margin-right: 10px;">
                                        <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                        <a style="float: none;" class="btn btn-sm btn-default" href="cliente.aspx"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                                
                                    </div>
                                </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
        </section>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="hideid" name="hideid" runat="server" />
    <input type="hidden" id="hide" name="hide" runat="server" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                document.getElementsByClassName("save")[0].click();
            }
        });
        $(document).ready(function () {

            pageSetUp();                                  

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                $("#ctl00_contenido_usuario").val("");
                $("#ctl00_contenido_nombre").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#ctl00_contenido_email").val("");                                                                
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {                    
                 
                    guardar();
                }
            });            

             function CargarCliente(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "cliente_edit.aspx/getCustomer",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;
                        
                         $("#idcliente").val(data.Nombre);
                         $("#rfc").val(data.Rfc);
                         $('#ctl00_contenido_imgLogo').attr("src", data.Logotipo);
                         $('#ctl00_contenido_hideid').val(data.Id);
                         $('#ctl00_contenido_hide').val(data.TenantId);                         
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del cliente. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function validar() {                
                var esvalido = true;                
              
                if ($("#idcliente").val().split(" ").join("") == "") {
                    ShowError("Cliente", "El cliente es obligatorio.");
                    $('#idcliente').parent().removeClass('state-success').addClass("state-error");
                    $('#idcliente').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#idcliente').parent().removeClass("state-error").addClass('state-success');
                    $('#idcliente').addClass('valid');
                }

                if ($("#rfc").val().split(" ").join("") == "") {
                    ShowError("RFC", "El rfc es obligatorio.");
                    $('#rfc').parent().removeClass('state-success').addClass("state-error");
                    $('#rfc').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#rfc').parent().removeClass("state-error").addClass('state-success');
                    $('#rfc').addClass('valid');
                }

                     

                return esvalido;
            }

            function guardar() {
                startLoading();
                var cliente = ObtenerValores();
                
                $.ajax({
                    type: "POST",
                    url: "cliente_edit.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'cliente': cliente }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del cliente se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del cliente se  " + resultado.mensaje + " correctamente.");                            
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ObtenerValores() {                                
                var cliente = {
                    Nombre: $('#idcliente').val(),
                    Rfc: $('#rfc').val(),                                    
                    Id: $('#ctl00_contenido_hideid').val()                                                                              
                };
                return cliente;
            }

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {                    
                    CargarCliente(param);                    
                }
            }

            init();

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
