<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="users_.aspx.cs" Inherits="Web.Application.Admin.users_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
     <li>Configuración</li>    
     <li>Usuarios</li>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
      
    </style>
    <div class="scroll">
        <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
                Catálogo de usuarios
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry" style="display: none;">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="user_edit.aspx" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
        </div>
    </div>
    
    <p></p>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-users-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"></span>
                        <h2>Buscar usuarios </h2>class="fa fa-search"><
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="select">
                                                <select name="peril" id="perfil" runat="server">
                                                    <option value="0">[Perfil]</option>
                                                </select>
                                                <i></i>
                                            </label>
                                        </section>
                                    
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="nombre" id="nombre" runat="server" placeholder="Nombre" maxlength="256"/>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Usuarios </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%" >
                                <thead>
                                    <tr>
                                        <th data-class="expand">Usuario</th>
                                        <th data-hide="phone,tablet">Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th >Apellido materno</th>
                                        <th >Email</th>
                                        <th>Perfil</th>
                                        <th >Clientes</th>
                                        <th data-hide="phone,tablet">Subcontratos</th>
                                        <th data-hide="phone,tablet">Acciones</th>                                       
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
        </div>
    <div id="blockuser-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;la cuenta de <strong>&nbsp<span id="usernameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">     
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;                           
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal-tabla" tabindex="-1" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-tabla"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-tabla" class="smart-form">                        
                            <div class="row">
                                <fieldset>
                                    <div>                                                                                                                        
                                        <div class="row">
                                            <table id="dt_basic_tabla" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                    <tr>    
                                                        <th></th>
                                                        <th>Id</th>
                                                        <th data-class="expand">Contrato</th>                                                        
                                                        <th data-hide="phone,tablet">Vigencia Inicial</th>
                                                        <th data-hide="phone,tablet">Vigencia Final</th>                                                        
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>      
                                    </div>
                                </fieldset>
                            </div>
                        
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btnsaveTabla"><i class="fa fa-save"></i>&nbsp;Guardar </a>                             
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelTabla"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                             
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
     <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        //$(document).on('shown.bs.modal', function (e) {
        //    $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        //});

        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            //Tabla de modal
            var responsiveHelper_dt_basic_tabla = undefined;
            var responsiveHelper_datatable_fixed_column_tabla = undefined;
            var responsiveHelper_datatable_col_reorder_tabla = undefined;
            var responsiveHelper_datatable_tabletools_tabla = undefined;
            //

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            function loadTable(data_) {
                table = $('#dt_basic_tabla').DataTable({
                    //"lengthMenu": [5, 10, 20, 30, 100],
                    //iDisplayLength: 100,
                    serverSide: false,                    
                    paging: false,
                    "scrollX": true,
                    retrieve: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_tabla) {
                            responsiveHelper_dt_basic_tabla = new ResponsiveDatatablesHelper($('#dt_basic_tabla'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_tabla.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_tabla.respond();
                    },
                    data: data_,
                    columnDefs: [
                        {
                            targets: 0,                            
                            name: "UsuarioId",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.UsuarioId != null) {
                                    return '<input type="checkbox" checked />';
                                }
                                else{
                                    return '<input type="checkbox" />';
                                }
                            }
                        },
                        {
                            targets: 1,                              
                            name: "ContratoId",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.ContratoId + "</div>";
                            }
                        },
                        {
                            targets: 2,                              
                            name: "Nombre",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Nombre + "</div>";
                            }
                        },
                        {
                            targets: 3,                              
                            name: "VigenciaInicial",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.VigenciaInicial + "</div>";
                            }
                        },
                        {
                            targets: 4,                              
                            name: "VigenciaFinal",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.VigenciaFinal + "</div>";
                            }
                        }
                    ]
                });

            }

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (data["IsLockedOut"]) {
                        $('td', row).eq(0).addClass('strikeout');
                        $('td', row).eq(1).addClass('strikeout');                        
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');                        
                        $('td', row).eq(4).addClass('strikeout');                        
                        $('td', row).eq(5).addClass('strikeout');      
                        $('td', row).eq(6).addClass('strikeout');      
                        $('td', row).eq(7).addClass('strikeout');      
                    }
                },
                ajax: {
                    type: "POST",
                    url: "users_.aspx/getusers",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });                       
                        parametrosServerSide.emptytable = window.emptytable;
                        return JSON.stringify(parametrosServerSide);
                    }
                   
                },
                columns: [
                    {
                        name: "Usuario",
                        data: "name"
                    },
                    {
                        name: "UU.Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "UU.ApellidoPaterno",
                        data: "ApellidoPaterno"
                    },
                    {
                        name: "UU.ApellidoMaterno",
                        data: "ApellidoMaterno"
                    },
                    {
                        name: "M.Email",
                        data: "Email"
                    },
                    {
                        name: "R.name",
                        data: "Rol"
                    },
                    null,
                    null,
                    null
                ],
                columnDefs: [
                    {
                        targets: 6,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.Clientes != null) {
                                return row.Clientes;
                            }
                            else {
                                return "No tiene clientes asignados";
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.Subcontratos != null) {
                                return row.Subcontratos;
                            }
                            else {
                                return "No tiene subcontratos asignados";
                            }
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var editarContratos = "";
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "edit";                            
                            var edit2 = "editContrato";
                            var editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="user_edit.aspx?tracking=' + row.UserId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            var habilitar = "";
                            if (row.IsLockedOut) {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                                
                            }
                            else {
                                
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="user_edit.aspx?tracking=' + row.UserId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            //if ($("#ctl00_contenido_KAQWPK").val() == "true") editarContratos = '<a class="btn btn-warning btn-circle ' + edit2 + '" data-tracking="' + row.UserId + '" title="Editar contratos"><i class="glyphicon glyphicon-file"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar =  '<a class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" id="username" value="' + row.name + '" data-id="' + row.name + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return editar + habilitar 
                               ;
                        }
                    }
                ]
              
            });            

            $("#btnsaveTabla").click(function () {

                var data = new Array();
                var contrato;
                var isChecked = false;
                var IdAux;
                var NombreAux;
                var VigenciaInicialAux;
                var VigenciaFinalAux;                

                $("#dt_basic_tabla tbody tr").each(function (index) {
                    $(this).children("td").each(function (index2) {
                        switch (index2) {
                            //Aqui en el subindice 0 accedo al checkbox para saber si la pertenencia esta seleccionada para la devolucion
                            case 0: var campo = $(this).children('input');
                                if (campo.is(':checked')) {
                                    isChecked = true;
                                }
                                break;
                            case 1: if (isChecked) {
                                IdAux = $(this).text();
                            }
                                break;
                            case 2: if (isChecked) {
                                NombreAux = $(this).text();
                            }
                                break;
                            case 3: if (isChecked) {
                                VigenciaInicialAux = $(this).text();
                            }
                                break;
                            case 4: if (isChecked) {
                                VigenciaFinalAux = $(this).text();
                            }
                                break;                            
                        }
                    });                    
                    contrato = {};

                    contrato.Id = IdAux;
                    contrato.Nombre = NombreAux;
                    contrato.VigenciaInicial = VigenciaInicialAux;
                    contrato.VigenciaFinal = VigenciaFinalAux;

                    if (isChecked) {
                        data.push(contrato);
                    }
                    isChecked = false;
                });                                
                
                saveContratos(data, $("#btnsaveTabla").attr('data-tracking'));

            });

            function saveContratos(data, tracking) {
                $.ajax({
                    type: "POST",
                    url: "users_.aspx/saveContratos",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        'contratos': data, 'tracking': tracking
                    }),
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);                        

                        if (r.exitoso) {
                            $("#form-modal-title-tabla").empty();
                            $("#form-modal-tabla").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                " Los registros se actualizaron correctamente.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Los registros se actualizaron correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible registrar los contratos seleccionados. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal y no fue posible registrar los contratos seleccionados. Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar las pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function getContratosByUsuario(data) {
                $.ajax({
                    type: "POST",
                    url: "users_.aspx/getContratosByUsuario",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        'datos': data
                    }),
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);                        
                        $("#form-modal-title-tabla").empty();
                        $("#form-modal-title-tabla").html("Listado de contratos");
                        $("#form-modal-tabla").modal("show");                        
                        loadTable(r.list);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar las pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            $("body").on("click", ".editContrato", function () {

                data = [
                    tracking = $(this).attr('data-tracking')
                ];

                $("#btnsaveTabla").attr('data-tracking', $(this).attr('data-tracking'));

                responsiveHelper_dt_basic_tabla = undefined;
                $('#dt_basic_tabla').DataTable().destroy();
                getContratosByUsuario(data);
                
            });

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");
            });

            $("body").on("click", ".blockuser", function () {
                var usernameblock = $(this).attr("data-id");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", usernameblock);
                $("#blockuser-modal").modal("show");
            });   

            $("#btncontinuar").unbind("click").on("click", function () {
                var username = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "users_.aspx/blockuser",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        username: username,
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                           
                            $("#blockuser-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                " Registro " + JSON.parse(data.d).mensaje + " con éxito.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Registro " + JSON.parse(data.d).mensaje + " con éxito.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });
            init();
            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }

            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
        </script>
</asp:Content>
