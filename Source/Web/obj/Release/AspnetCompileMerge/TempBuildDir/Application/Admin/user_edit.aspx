﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="user_edit.aspx.cs" Inherits="Web.Application.Admin.user_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">    
    <li>Configuración</li>
    <li><a href="users_.aspx">Usuarios</a></li>
    <li>Registro</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">        
        td.strikeout:firs {
            text-decoration: line-through;
        }
      
    </style>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
    <div class="scroll">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-useredit-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-edit"></i></span>
                    <h2>Usuario </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <div id="smart-form-register" class="smart-form">
                            <header id="header_form">
                                Formulario de registro
                            </header>
                            <fieldset>
                                <div class="row">

                                    <section class="col col-4">
                                        <label>Usuario <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="usuario" id="usuario" runat="server" placeholder="Usuario" maxlength="50" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el usuario.</b>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Nombre <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="nombre" id="nombre" runat="server" placeholder="Nombre" maxlength="50" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el nombre.</b>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Apellido paterno <a style="color:red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="nombre" id="paterno" runat="server" placeholder="Apellido paterno" maxlength="50" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el apellido paterno.</b>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Apellido materno</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="nombre" id="materno" runat="server" placeholder="Apellido materno" maxlength="50" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el apellido materno.</b>
                                        </label>                                        
                                    </section>
                                    <section id="emailsection" class="col col-4">
                                        <label>E-mail  </label>
                                        <label class="input">
                                            <i class="icon-append fa fa-envelope"></i>
                                            <input type="email" name="email" id="email" runat="server" placeholder="E-mail" maxlength="50" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el correo electrónico del usuario.</b>
                                            <input type="hidden" id="hide" name="hide" runat="server" />
                                            <input type="hidden" id="hideid" name="hideid" runat="server" />
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Móvil</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-phone"></i>
                                            <input type="tel" name="movil" id="movil" maxlength="19" placeholder="Móvil" data-mask="(999) 999-9999"/>
                                            <b class="tooltip tooltip-bottom-right">Ingrese el celular del usuario.</b>
                                            <input type="hidden" id="Hidden1" name="hide" runat="server" />
                                            <input type="hidden" id="Hidden2" name="hideid" runat="server" />
                                        </label>                                                                                
                                     </section>                         
                                    <%--<section class="col col-4">
                                        <label>Centro de recluisón <a style="color:red">*</a></label>
                                        <label class="select">
                                            <select name="rol" id="centro" runat="server">
                                            </select>
                                            <i></i>
                                        </label>
                                    </section>--%>                                    
                                </div>
                                <div class="row">                                                                       
                                    <section class="col col-4">
                                        <label>Rol <a style="color:red">*</a></label>
                                        <label class="select">
                                            <select name="rol" id="rol" runat="server">
                                            </select>
                                            <i></i>
                                        </label>
                                    </section> 
                                     <section class="col col-4"style="margin-right:0px">
                                        <label>Contraseña <a style="color:red">*</a></label>
                                         <label  class="input">
                                            <i id="showpass" class="icon-append glyphicon glyphicon-eye-open"></i>
                                            <input id="txtcontrasenia" name="txtcontrasenia" type="password" maxlength="4" autocomplete="new-password" class="form-control"style="height:32px"/>
                                            </label>
                                    </section >
                                    <section id="cuipsection" class="col col-4">
                                        <label id="labelcedula">Cédula <a style="color:red">*</a></label>
                                            <label class="input">
                                            <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="cuip" id="cuip"  placeholder="Cédula" maxlength="20" class="alphanumeric alptext" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la cédula.</b>
                                        </label>      
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-2">
                                        <label class="checkbox">
                                            <input type="checkbox" name="habilitado" id="habilitado" /><i></i>
                                            Habilitado</label>
                                    </section>
                                </div>
                                <div class="row" id="generatepassword">
                                    &nbsp;&nbsp;Nota: La contraseña será generada aleatoriamente por el sistema. 
                                </div>
                                <br />                                
                                <div class="row" style=" width:100%;">
                                   <%-- <div class="col-lg-1" >
                                    </div>--%>
                                    <div class="col-lg-10" style="width:100%; margin-left:1%;" >
                                        <table id="dt_basic_tabla" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>    
                                                    <th></th>
                                                    <th>Id</th>
                                                    <th>Contrato</th>
                                                    <th data-class="expand">Subcontrato</th>                                                        
                                                    <th data-hide="phone,tablet">Vigencia Inicial</th>
                                                    <th data-hide="phone,tablet">Vigencia Final</th>  
                                                    <th data-hide="phone,tablet">Cliente</th>
                                                    <th data-hide="phone,tablet">Tipo</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>                                    
                                    <div class="col-lg-1">
                                    </div>
                                </div>   
                            </fieldset>
                                <footer>
                                    <div class="row" style="display: inline-block; float: right; margin-right: 10px;">
                                        <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                        <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar pantalla</a>
                                        <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default restore" id="restorepsw" title="Reestablecer contraseña"><i class="glyphicon glyphicon-random"></i>&nbsp;Restablecer </a>
                                        <%--<a class="btn btn-sm btn-default" href="useredit.aspx"><i class="fa fa-plus"></i>&nbsp;Nuevo </a>--%>
                                        <a style="float: none;" class="btn btn-sm btn-default" href="users_.aspx"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                                    </div>
                                </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-busqueda-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-search"></i></span>
                    <h2>Búsqueda </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body">

                        <table id="dt_basic" class="table table-striped table-bordered table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-class="expand" rowspan="2">Pantallas</th>
                                    <th>Consultar</th>
                                    <th>Registrar</th>
                                    <th>Modificar</th>
                                    <th>Habilitar / Deshabilitar</th>
                                    <th data-hide="always" rowspan="2">Reportes</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </article>
        </div>
        <div id="restorepassword-modal" class="modal fade" tabindex="-1" data-keyboard="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Confirmación
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div id="Div2" class="smart-form">
                            <fieldset>
                                <section>
                                    <p class="center">
                                        ¿Está seguro de restablecer la contraseña de &nbsp<span id="userrestore"></span> ?
                                    </p>
                                </section>
                            </fieldset>
                            <footer>
                                <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                                <a class="btn btn-sm btn-default restorepw" id="btncontinuerestore"><i class="fa fa-save bigger-120"></i>&nbsp;Restablecer</a>&nbsp;
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </section>
    <input type="hidden" id="cuipmemo" value="" />
    <input type="hidden" id="perfilmemo" value="" />
    <input type="hidden" id="reportGrantsChanges" value="" />
    <input type="hidden" id="rolechanger" value="" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                document.getElementsByClassName("save")[0].click();
            }
        });
        $(document).ready(function () {

            pageSetUp();            
              // $("#emailsection").hide();
            //$("#cuipsection").hide();
            $("#generatepassword").hide();
            $("#restorepsw").hide();
            $("#cuipsection").hide();
            //Tabla de contratos
            var responsiveHelper_dt_basic_tabla = undefined;
            var responsiveHelper_datatable_fixed_column_tabla = undefined;
            var responsiveHelper_datatable_col_reorder_tabla = undefined;
            var responsiveHelper_datatable_tabletools_tabla = undefined;

            $("#ctl00_contenido_email").focusout(function () {
            }).blur(function () {

                if ($(this).val() != "") {
                    $("#txtcontrasenia").attr('disabled', 'disabled');
                    $("#ShowPassword").attr('disabled', 'disabled');
                    $("#txtcontrasenia").val("");
                }
                else
                {
                    $("#txtcontrasenia").removeAttr('disabled');
                    $("#ShowPassword").removeAttr('disabled');
                }

                });

            function loadTable(data_) {
                table = $('#dt_basic_tabla').DataTable({
                    "lengthMenu": [5, 10, 20, 30, 100],
                    //iDisplayLength: 100,
                    serverSide: false,
                    paging: true,
                    "scrollX": true,
                    retrieve: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r> " +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon" style="margin-bottom:4px; height:16px; width:11px;"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_tabla) {
                            responsiveHelper_dt_basic_tabla = new ResponsiveDatatablesHelper($('#dt_basic_tabla'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_tabla.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_tabla.respond();
                    },
                    data: data_,
                    columnDefs: [
                        {
                            targets: 0,                            
                            name: "IdUsuario",
                            orderable: false,
                            render: function (data, type, row, meta) {                                
                                if (row.IdUsuario != null && row.Activo != false) {
                                    return '<input type="checkbox" checked/>';
                                }
                                else {
                                    return '<input type="checkbox" />';
                                }                                
                            }
                        },
                        {
                            targets: 1,                              
                            name: "Id",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Id + "</div>";
                            }
                        },
                          {
                            targets: 2,                              
                            name: "Contrato",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Contrato + "</div>";
                            }
                        },
                        {
                            targets: 3,                              
                            name: "Nombre",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Nombre + "</div>";
                            }
                        },
                        {
                            targets: 4,                              
                            name: "VigenciaInicial",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.VigenciaInicial + "</div>";
                            }
                        },
                        {
                            targets: 5,                              
                            name: "VigenciaFinal",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.VigenciaFinal + "</div>";
                            }
                        },
                        {
                            targets: 6,                              
                            name: "Cliente",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Cliente + "</div>";
                            }
                        },
                        {
                            targets: 7,                              
                            name: "Tipo",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Tipo + "</div>";
                            }
                        }
                    ]
                });
            }

          

            $("#ctl00_contenido_rol").change(function () {
                if ($(this).val() == "Médico" || $(this).val() == "Químico" || $(this).val() == "Juez calificador") {
                    $("#labelcedula").html("Cédula <a style='color:red'>*</a>");
                    $("#cuip").attr("placeholder", "Cédula");
                    //$("cuip").placeholder="CUIP";
                    $("#cuipsection").show();
                    $("#cuipsection").show();
                }

                else if ($(this).val() == "Encargado de barandilla") {
                    $("#labelcedula").html("CUIP <a style='color:red'>*</a>");
                    // $("#cuip").attr('placeholder', 'CUIP');
                    // $('#cuip:text').attr('placeholder', 'Some New Text');
                    $("#cuip").attr("placeholder", "CUIP");
                    //$("cuip").placeholder="CUIP";
                    $("#cuipsection").show();
                }
                else {
                    $("#cuipsection").hide();
                }

                if ($("#cuipmemo").val() != "") {
                    if ($("#perfilmemo").val() == $(this).val()) {
                        $("#cuip").val($("#cuipmemo").val());
                    }
                    else {
                        $("#cuip").val("");
                    }
                }

                if ($("#rolechanger").val() == "1") {
                    responsiveHelper_dt_basic = undefined;
                    var id = $('#ctl00_contenido_hideid').val();
                    var rol = $("#ctl00_contenido_rol").val();
                    //window.table.destroy();
                    $('#dt_basic').DataTable().destroy();
                    var p = getpermission(rol, id);
                    var permisosReportes = permisosReportesDefault(rol);
                    createTable(p, false, permisosReportes);
                }
            });


            function getContratosOnTable(param) {
                $.ajax({
                    type: "POST",
                    url: "user_edit.aspx/getContratosOnTable",
                    contentType: "application/json; charset=utf-8",                    
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                       'tracking': param 
                    }),
                    success: function (response) {
                        var r = JSON.parse(response.d);
                        loadTable(r.list);

                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar los contratos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }                        

            function permisos(numColumna) {
                var array = [];

                //var rows_selected = table.column(numColumna).checkboxes.selected();
                var rows_selected = $("#dt_basic").DataTable().column(numColumna).checkboxes.selected();

                $.each(rows_selected, function (index, rowId) {

                    array.push(rowId);

                });                

                return array;

            };
            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                LimpiarFormulario();
                $("#txtcontrasenia").removeAttr('disbled');
                $("#txtcontrasenia").prop('disabled', false);
                $("#ShowPassword").prop('disabled',false);
                $("#ShowPassword").prop("checked", false);
                $("#habilitado").prop("checked", false);
                
                $('#<%= usuario.ClientID %>').removeAttr('disabled');
                CargarPerfiles(0);
                //CargarCentro(0);
                responsiveHelper_dt_basic = undefined;
                //table.destroy();
                $('#dt_basic').DataTable().destroy();
                desmarcarContratos();
                createTable("", true, permisosReportesDefaultClear());
            });            

            function desmarcarContratos() {
                $("#dt_basic_tabla tbody tr").each(function (index) {
                    $(this).children("td").each(function (index2) {
                        switch (index2) {                            
                            case 0: var campo = $(this).children('input');
                                if (campo.is(':checked')) {
                                    campo.prop("checked", false);
                                }
                                break;                            
                        }
                    });                                       
                });
            }

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
              
                if (validar()) {

                    var dataArreglo = new Array();
                    var contrato;
                    
                    $("#dt_basic_tabla").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                        var data;
                        var checkit = this.node().childNodes[0].childNodes[0].checked;

                        if (checkit) {
                            data = this.data();                            
                            contrato = {};
                            contrato.Id = data.Id;
                            contrato.Nombre = data.Nombre;
                            contrato.VigenciaInicial = data.VigenciaInicial;
                            contrato.VigenciaFinal = data.VigenciaFinal;
                            contrato.Tipo = data.Tipo;
                            dataArreglo.push(contrato);
                        }                        
                    });                                        

                    guardar(dataArreglo);
                }
            });           

            function CargarCentro(id) {
                $('#ctl00_contenido_centro').empty();
                $.ajax({

                    type: "POST",
                    url: "user_edit.aspx/getInstituciones",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_centro');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Institución a disposición]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de permisos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var nuevo = 0;
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = false;

            function createTable(_permisos, nuevo, _permisosReportes) {
                var catPermisos = {
                    1: "Consultar",
                    2: "Registrar",
                    3: "Modificar",
                    4: "Eliminar"
                };
                
                var permisos = {

                };
                var permisosReportes = {

                };
                if (!nuevo) {
                    _permisos.map(function iPermisos(p) {
                        /*
                        if (!permisos[p.PantallaId]) permisos[p.PantallaId]={ };
                        permisos[p.PantallaId][p.PermisoId] = p.Activo && p.Habilitado;
                        */
                        if (!permisos[p.PermisoId]) permisos[p.PermisoId] = {};
                        permisos[p.PermisoId][p.PantallaId] = p.Activo && p.Habilitado;
                    });

                    if (_permisosReportes != null) {
                        _permisosReportes.map(function iPermisosReportes(p) {
                            if (!permisosReportes[p.ReporteId]) permisosReportes[p.ReporteId] = {};
                            permisosReportes[p.ReporteId] = p.Activo && p.Habilitado;
                        });
                    }
                }
                window.table = $('#dt_basic').DataTable({
                    deferRender: true,
                    scrollY: 500,
                    scrollCollapse: true,
                    scroller: true,
                    iDisplayLength: 100,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    deferRender: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow, data, x) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();

                        $('#dt_basic').waitMe('hide');

                        OcultarExpanders();
                        //console.log(this.api().rows().every());
                    },
                    initComplete: function (settings) {
                        var api = this.api();
                        $.map(permisos, function (pantallas, permisoId) {
                            api.cells(
                                api.rows(function (idx, data, node) {
                                    var pantallaId = data.Modificar;
                                    return pantallas[pantallaId];
                                }).indexes(),
                                permisoId
                            ).checkboxes.select();
                        });
                    },
                    ajax: {
                        type: "POST",
                        url: "user_edit.aspx/getpermisos",
                        contentType: "application/json; charset=utf-8",
                        async: false,
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            parametrosServerSide.emptytable = window.emptytable;
                            return JSON.stringify(parametrosServerSide);
                        }

                    },
                    columns: [
                        {
                            name: "Nombre",
                            data: "Nombre"
                        },
                        null,
                        null,
                        null,
                        null,
                        {
                            name: "Reportes",
                            data: "Reportes"
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "Nombre",
                            render: function (data, type, row, meta) {
                                return "<label class='OcultarExpander' data-reporte='" + (row.Reportes != null && row.Reportes != "" ? "1" : "0") + "'>" + (row.Nombre != null ? row.Nombre : "") + "</label>";
                            }
                        },
                        {
                            'targets': 1,
                            'data': "Consultar",
                            checkboxes: true,
                        },
                        {
                            'targets': 2,
                            'data': "Registrar",
                            'checkboxes': {
                                'selectRow': false
                            },
                        },
                        {
                            'targets': 3,
                            'data': "Modificar",
                            checkboxes: true
                        },
                        {
                            'targets': 4,
                            'data': "Eliminar",
                            checkboxes: true,
                        },
                        {
                            targets: 5,
                            data: "Reportes",
                            render: function (data, type, row, meta) {
                                if (row.Reportes != null && row.Reportes != "") {
                                    var tabla = DrawTablaReporte(row.Reportes, permisosReportes);
                                   
                                    return tabla;
                                }
                                else {
                                    return "";
                                }
                            }
                        }
                    ]
                });

                setTimeout(function () {
                    $("#rolechanger").val("1");
                }, 3000);
            }

            function OcultarExpanders() {
                $(".OcultarExpander").each(function () {
                    var esReporte = $(this).attr("data-reporte");
                    if (esReporte == "0") {
                        $(this).prev().removeClass("responsiveExpander");
                    }
                    else {
                        $(this).parent().next().children().attr("disabled", "disabled");
                        $(this).parent().next().next().children().attr("disabled", "disabled");
                        $(this).parent().next().next().next().children().attr("disabled", "disabled");
                        $(this).parent().next().next().next().next().children().attr("disabled", "disabled");
                    }
                });
            }

            function DrawTablaReporte(data, permisos) {
                var tabla = '<table class="table table-striped table-bordered table-hover report_table">';
                var reportes = data.split(",");
                for (var i = 0; i < reportes.length; i++) {
                    var valores = reportes[i].split("-");
                    var id = valores[0];
                    var nombre = valores[1];

                    var setValue = permisos[id];
                    if (setValue != undefined && setValue) {
                        tabla += '<tr><td style="width: 3%;"><input type="checkbox" checked class="checkbox-reporte" id="chkReport_' + id + '" /></td><td style="width: 97%;">' + nombre + '</td></tr>';
                    }
                    else {
                        tabla += '<tr><td style="width: 3%;"><input type="checkbox" class="checkbox-reporte" id="chkReport_' + id + '" /></td><td style="width: 97%;">' + nombre + '</td></tr>';
                    }
                    $("#chkReport_" + id).trigger("change");
                }
                tabla += '</tabla>';

                return tabla;
            }

            $("body").on("change", ".checkbox-reporte", function () {
                var data_permisos = $("#reportGrantsChanges").val();
                var splitData_Permisos = data_permisos.split(",");
                var idCheck = $(this).attr("id").split("_")[1];
                var valorCheck = $(this).is(":checked");

                if (splitData_Permisos.includes(idCheck + "_" + !valorCheck)) {
                    var indexPermiso = splitData_Permisos.indexOf(idCheck + "_" + !valorCheck);
                    splitData_Permisos[indexPermiso] = idCheck + "_" + valorCheck;
                }
                else {
                    if (!splitData_Permisos.includes(idCheck + "_" + valorCheck)) {
                        splitData_Permisos.push(idCheck + "_" + valorCheck);
                    }
                }
                data_permisos = splitData_Permisos.join(",");
                if (data_permisos[0] == ",") data_permisos = data_permisos.substring(1);
                $("#reportGrantsChanges").val(data_permisos);
            });

            function getPermisos_Reportes() {
                var array = [];

                var data_permisosReportes = $("#reportGrantsChanges").val();

                if (data_permisosReportes != undefined && data_permisosReportes != "") {
                    var split = data_permisosReportes.split(",");
                    for (var k = 0; k < split.length; k++) {
                        array.push(split[k]);
                    }
                }

                return array;
            }

            function permisosReportesDefaultClear() {
                var obj = [
                    { ReporteId: 1, Activo: false, Habilitado: false },
                    { ReporteId: 2, Activo: false, Habilitado: false },
                    { ReporteId: 3, Activo: false, Habilitado: false },
                    { ReporteId: 4, Activo: false, Habilitado: false },
                    { ReporteId: 5, Activo: false, Habilitado: false },
                    { ReporteId: 6, Activo: false, Habilitado: false },
                    { ReporteId: 7, Activo: false, Habilitado: false },
                    { ReporteId: 8, Activo: false, Habilitado: false },
                    { ReporteId: 9, Activo: false, Habilitado: false },
                    { ReporteId: 10, Activo: false, Habilitado: false },
                    { ReporteId: 11, Activo: false, Habilitado: false },
                    { ReporteId: 12, Activo: false, Habilitado: false },
                    { ReporteId: 13, Activo: false, Habilitado: false },
                    { ReporteId: 14, Activo: false, Habilitado: false },
                    { ReporteId: 15, Activo: false, Habilitado: false },
                    { ReporteId: 16, Activo: false, Habilitado: false },
                    { ReporteId: 17, Activo: false, Habilitado: false },
                    { ReporteId: 18, Activo: false, Habilitado: false },
                    { ReporteId: 19, Activo: false, Habilitado: false },
                    { ReporteId: 20, Activo: false, Habilitado: false },
                    { ReporteId: 21, Activo: false, Habilitado: false },
                    { ReporteId: 22, Activo: false, Habilitado: false },
                    { ReporteId: 23, Activo: false, Habilitado: false },
                    { ReporteId: 24, Activo: false, Habilitado: false },
                    { ReporteId: 25, Activo: false, Habilitado: false },
                    { ReporteId: 26, Activo: false, Habilitado: false },
                    { ReporteId: 27, Activo: false, Habilitado: false },
                    { ReporteId: 28, Activo: false, Habilitado: false },
                    { ReporteId: 29, Activo: false, Habilitado: false },
                    { ReporteId: 30, Activo: false, Habilitado: false },
                    { ReporteId: 31, Activo: false, Habilitado: false },
                    { ReporteId: 32, Activo: false, Habilitado: false },
                    { ReporteId: 33, Activo: false, Habilitado: false },
                    { ReporteId: 34, Activo: false, Habilitado: false },
                    { ReporteId: 35, Activo: false, Habilitado: false },
                    { ReporteId: 36, Activo: false, Habilitado: false },
                    { ReporteId: 37, Activo: false, Habilitado: false }
                ]

                return obj;
            }

            function CargarPerfiles(setperfil) {
                $('#<%= rol.ClientID %>').empty();
                $.ajax({
                    type: "POST",
                    url: "user_edit.aspx/getroles",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ setperfil: setperfil }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#<%= rol.ClientID %>');

                        Dropdown.append(new Option("[Rol]", "0"));

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setperfil != "") {
                            Dropdown.val(setperfil);
                            Dropdown.trigger("change.select2");
                        }

                        $("#cuipsection").hide();

                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de perfiles. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

             function check(id) {
                 ischeck = false;
                 $.ajax({
                     type: "POST",
                     url: "user_edit.aspx/check",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         id: id,
                     }),
                     cache: false,
                     success: function (data) {

                         ischeck = true
                     },
                     error: function () {
                         $('#main').waitMe('hide');
                         ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                     }
                 });

                 return ischeck;
            }
            $('#showpass').click(function () {
            var tipo = document.getElementById("txtcontrasenia");
                  if(tipo.type == "password"){
                      tipo.type = "text";
                  }
                  else {
                      tipo.type = "password";
                  }
            });

                    //CheckBox mostrar contraseña
            $('#ShowPassword').click(function () {
                $('#txtcontrasenia').attr('type', $(this).is(':checked') ? 'text' : 'password');
            });

            function CargarUsuario(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "user_edit.aspx/getuser",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        data = data.d;

                        CargarPerfiles(data.Perfil);
                        $('#<%= email.ClientID %>').val(data.Email);
                        $('#<%= usuario.ClientID %>').val(data.User);
                        $('#ctl00_contenido_usuario').attr('disabled', 'disabled');
                        $('#<%= nombre.ClientID %>').val(data.Nombre);
                        $('#<%= paterno.ClientID %>').val(data.ApellidoPaterno);
                        $('#<%= materno.ClientID %>').val(data.ApellidoMaterno);

                        if (data.Email = "" || data.Email == null) {

                            $("#txtcontrasenia").val(data.Password);
                            $("#txtcontrasenia").removeAttr('disbled');
                            $("#txtcontrasenia").prop('disabled', false);
                        }
                        else {
                            $("#txtcontrasenia").attr('disabled', 'disabled');
                            $("#restorepsw").show();
                        }
                        $("#movil").val(data.Movil);
                        if (data.habilitarcomborol == "False") {
                            $("#ctl00_contenido_rol").prop('disabled', true);
                        }
                        else {
                            $("#ctl00_contenido_rol").prop('disabled', false);
                        }

                        $("#cuipmemo").val(data.CUIP);
                        $("#perfilmemo").val(data.Perfil);

                        $("#cuip").val(data.CUIP);
                        if (data.Perfil == "Médico" || data.Perfil == "Químico" || data.Perfil == "Juez calificador") {
                            $("#cuipsection").show();
                            setTimeout(function () { $('#<%= rol.ClientID %>').trigger("change"); }, 300);
                        }
                        else if (data.Perfil == "Encargado de barandilla") {
                            $("#labelcedula").html("CUIP <a style='color:red'>*</a>");
                            // $("#cuip").attr('placeholder', 'CUIP');
                            // $('#cuip:text').attr('placeholder', 'Some New Text');
                            $("#cuip").attr("placeholder", "CUIP");
                            //$("cuip").placeholder="CUIP";
                            $("#cuipsection").show();
                            setTimeout(function () { $('#<%= rol.ClientID %>').trigger("change"); }, 300);
                        }

                        //CargarCentro(data.CentroId);
                        if (data.Habilitado == true)
                            $('#habilitado').prop('checked', true);
                        $('#<%= hide.ClientID %>').val(data.TrackingId == null ? "" : data.TrackingId);
                        $('#<%= hideid.ClientID %>').val(data.Id == null ? "" : data.Id);
                        $('#generatepassword').empty();
                        createTable(data.Permisos, false, data.PermisosReportes);
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function getpermission(rol, id) {
                var perm = null;
                startLoading();
                $.ajax({
                    url: "user_edit.aspx/getpermisionbydefault",
                    type: 'POST',
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        rol: rol
                    }),
                    success: function (data) {
                        data = data.d;
                        if (data.exitoso) {
                            perm = data.Permisos;
                        }
                        else {
                            var er = data.mensaje
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + er + " Si el problema persiste, contacte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function (response) {
                        $('#main').waitMe('hide');
                        var r = jQuery.parseJSON(response.responseText);
                        ShowError("¡Error!", " No fue posible obtener los permisos del rol seleccionado " + r.Message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
                return perm;
            }

            function permisosReportesDefault(rol) {
                var perm = null;
                startLoading();
                $.ajax({
                    url: "user_edit.aspx/getpermisionReportsbydefault",
                    type: 'POST',
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        rol: rol
                    }),
                    success: function (data) {
                        data = data.d;
                        if (data.exitoso) {
                            perm = data.Permisos;
                        }
                        else {
                            var er = data.mensaje
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + er + " Si el problema persiste, contacte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function (response) {
                        $('#main').waitMe('hide');
                        var r = jQuery.parseJSON(response.responseText);
                        ShowError("¡Error!", " No fue posible obtener los permisos del rol seleccionado " + r.Message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
                return perm;
            }

            function validar() {
                var esvalido = true;

                if ($("#ctl00_contenido_nombre").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#ctl00_contenido_nombre').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_nombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_nombre').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_nombre').addClass('valid');
                }

                if ($("#ctl00_contenido_paterno").val().split(" ").join("") == "") {
                    if ($("#ctl00_contenido_paterno").val().split(" ").join("") == "") {
                        ShowError("El apellido paterno", "El apellido paterno es obligatorio.");
                        $('#ctl00_contenido_paterno').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_paterno').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_paterno').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_paterno').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_email").val().split(" ").join("") != "") {
                    if (!ValidateEmail($('#ctl00_contenido_email').val())) {
                        ShowError("E-mail", "El e-mail no tiene el formato correcto.");
                        $('#ctl00_contenido_email').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_email').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_email').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_email').addClass('valid');
                    }
                }

                if ($('#ctl00_contenido_rol').val() == "0" || $('#ctl00_contenido_rol').val() == null) {
                    ShowError("Rol", "El rol es obligatorio.");
                    $('#ctl00_contenido_rol').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_rol').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_rol').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_rol').addClass('valid');
                }

                if ($("#ctl00_contenido_usuario").val() == "") {
                    ShowError("Usuario", "El usuario es obligatorio.");
                    $('#ctl00_contenido_usuario').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_usuario').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_usuario').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_usuario').addClass('valid');
                }
                if ($("#ctl00_contenido_email").val().split(" ").join("") == "") {

                    if ($("#txtcontrasenia").val() == "") {
                        ShowError("Contraseña", "La contraseña es obligatoria.");
                        $('#txtcontrasenia').parent().removeClass('state-success').addClass("state-error");
                        $('#txtcontrasenia').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#txtcontrasenia').parent().removeClass("state-error").addClass('state-success');
                        $('#txtcontrasenia').addClass('valid');
                    }
                }
                if ($("#ctl00_contenido_rol").val().split(" ").join("") == "") {

                    if ($("#ctl00_contenido_rol").val().split(" ").join("") == "") {

                        ShowError("Rol", "El rol es obligatorio.");
                        $('#ctl00_contenido_rol').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_rol').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_rol').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_rol').addClass('valid');
                    }
                }
                if ($("#ctl00_contenido_rol").val() == "Encargado de barandilla") {
                    if ($("#cuip").val().split(" ").join("") == "") {
                        ShowError("CUIP", "El campo CUIP es obligatorio.");
                        $('#cuip').parent().removeClass('state-success').addClass("state-error");
                        $('#cuip').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#cuip').parent().removeClass("state-error").addClass('state-success');
                        $('#cuip').addClass('valid');
                    }
                }

                if ($("#ctl00_contenido_rol").val() == "Médico" || $("#ctl00_contenido_rol").val() == "Médico" || $("#ctl00_contenido_rol").val() == "Juez calificador") {
                    if ($("#cuip").val().split(" ").join("") == "") {
                        ShowError("Cédula", "El campo cédula es obligatorio.");
                        $('#cuip').parent().removeClass('state-success').addClass("state-error");
                        $('#cuip').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#cuip').parent().removeClass("state-error").addClass('state-success');
                        $('#cuip').addClass('valid');
                    }
                }

                //if ($("#ctl00_contenido_centro").val() == null || $("#ctl00_contenido_centro").val() == 0) {
                //    ShowError("Centro reclusión", "El Institución a disposición es obligatorio.");
                //    $('#ctl00_contenido_centro').parent().removeClass('state-success').addClass("state-error");
                //    $('#ctl00_contenido_centro').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#ctl00_contenido_centro').parent().removeClass("state-error").addClass('state-success');
                //    $('#ctl00_contenido_centro').addClass('valid');
                //}

                //}

                return esvalido;
            }

            function ValidateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            function guardar(dataContratos) {
                startLoading();
                var usuario = ObtenerValores();
                var consultar = permisos(1);
                var registrar = permisos(2);
                var modificar = permisos(3);
                var eliminar = permisos(4);
                var reportes = getPermisos_Reportes();

                $.ajax({
                    type: "POST",
                    url: "user_edit.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'usuario': usuario, 'registrar': registrar, 'consultar': consultar, 'modificar': modificar, 'eliminar': eliminar, 'reportes': reportes, 'contratos': dataContratos }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $("#reportGrantsChanges").val("");
                            $("#cuipmemo").val($("#cuip").val());
                            $("#perfilmemo").val($('#ctl00_contenido_rol').val());
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del usuario se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del usuario se  " + resultado.mensaje + " correctamente.");
                            $('#<%= usuario.ClientID %>').prop('disabled', 'disabled');
                            $('#main').waitMe('hide');
                            if (resultado.Redireccionar) {
                                window.location.href = "../dashboard.aspx";
                            }
                        }
                        else {

                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ObtenerValores() {
                var habilitado = false;
                if ($('#habilitado').is(":checked")) {
                    habilitado = true;
                }

                var usuario = {
                    Email: $('#ctl00_contenido_email').val(),
                    Nombre: $('#ctl00_contenido_nombre').val(),
                    ApellidoPaterno: $('#ctl00_contenido_paterno').val(),
                    ApellidoMaterno: $('#ctl00_contenido_materno').val(),
                    User: $('#ctl00_contenido_usuario').val(),
                    Rol: $('#ctl00_contenido_rol').val(),
                    TrackingId: $('#ctl00_contenido_hide').val(),
                    Id: $('#ctl00_contenido_hideid').val(),
                    Habilitado: habilitado,                    
                    Movil: $("#movil").val(),
                    Password: $("#txtcontrasenia").val(),
                    CUIP:$("#cuip").val()
                    //CentroId: $('#ctl00_contenido_centro').val()

                };

                return usuario;
            }

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    $("#rolechanger").val("0")
                    CargarUsuario(param);          
                    getContratosOnTable(param);
                }
                else {
                    $("rolechanger").val("1");
                    getContratosOnTable("");
                    CargarPerfiles(0);
                    //CargarCentro(0);                    
                    createTable("", true, permisosReportesDefaultClear());
                }                
            }

            function LimpiarFormulario()
            {
                $('#ctl00_contenido_usuario').parent().removeClass('state-success').removeClass("state-error");
                $('#ctl00_contenido_nombre').parent().removeClass('state-success').removeClass("state-error");
                $('#ctl00_contenido_paterno').parent().removeClass('state-success').removeClass("state-error");
                $('#ctl00_contenido_email').parent().removeClass('state-success').removeClass("state-error");
                $('#ctl00_contenido_rol').parent().removeClass('state-success').removeClass("state-error");
                $('#txtcontrasenia').parent().removeClass('state-success').removeClass("state-error");
                $('#cuip').parent().removeClass('state-success').removeClass("state-error");

                $("#ctl00_contenido_usuario").val("");
                $("#ctl00_contenido_nombre").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#ctl00_contenido_email").val("");
                $("#txtcontrasenia").val("");
                $("#cuip").val("");

                $("#cuipmemo").val("");
                $("#perfilmemo").val("");
            }

            function validar_correo() {
                var esvalido = true;

                if ($("#ctl00_contenido_email").val() == "") {
                    ShowError("E-mail", "El e-mail es obligatorio para restablecer la contraseña.");
                    $('#ctl00_contenido_email').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_email').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_email').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_email').addClass('valid');
                }

                if ($("#ctl00_contenido_usuario").val() == "") {
                    ShowError("Usuario", "El usuario es obligatorio para restablecer la contraseeña.");
                    $('#ctl00_contenido_usuario').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_usuario').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_usuario').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_usuario').addClass('valid');
                }

                return esvalido;
            }

            $("body").on("click", ".restore", function () {
                if (validar_correo()) {
                    $('#ctl00_contenido_email').parent().removeClass('state-success');
                    $('#ctl00_contenido_email').parent().removeClass("state-error");
                    $('#ctl00_contenido_usuario').parent().removeClass('state-success');
                    $('#ctl00_contenido_usuario').parent().removeClass("state-error");
                  
                    var userrestore = $("#ctl00_contenido_usuario").val();
                    $("#userrestore").text(userrestore);
                    $("#btncontinuerestore").attr("data-id", userrestore);
                    $("#restorepassword-modal").modal("show");
                }
            });



            $("#btncontinuerestore").unbind("click").on("click", function () {
                startLoading();
                var username = $(this).attr("data-id");
                $.ajax({
                    url: "user_edit.aspx/restorepassword",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        username: username,
                    }),
                    success: function (data) {
                        data = data.d;
                        if (data.exitoso) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La contraseña se restableció correctamente.</div>");
                            setTimeout(hideMessage, hideTime);
                        }
                        else {
                            var er = data.mensaje
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal y no fue posible restablecer la contraseña del usuario " + er + ". Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                        }
                        $("#restorepassword-modal").modal("hide");
                        $('#main').waitMe('hide');
                    },
                    error: function (response) {
                        $("#restorepassword-modal").modal("hide");
                        $('#main').waitMe('hide');
                        var r = jQuery.parseJSON(response.responseText);
                        ShowError("¡Error!", " No fue posible restablecer la contraseña del usuario " + r.Message + ". Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            });

            init();

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
