﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="subcontrato_edit.aspx.cs" Inherits="Web.Application.Admin.subcontrato_edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Admin/catalogo.aspx?name=escolaridad">Configuración</a></li>
    <li><a href="contrato.aspx">Contratos</a></li>
    <li>Registro</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-useredit-1" data-widget-editbutton="false" data-widget-custombutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-edit"></i></span>
                    <h2>Subcontrato </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">                        
                        <div id="smart-form-register" class="smart-form">
                            <header id="header_form">
                                Formulario de registro
                            </header>
                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="input">Contrato <a style="color: red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>                                                
                                            <select name="contrato" id="contrato" style="width: 100%" class="select2"></select>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Subcontrato <a style="color: red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-book"></i>
                                            <input type="text" name="subcontrato" id="subcontrato" placeholder="Subcontrato" maxlength="256" class="alphanumeric" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese el subcontrato.</b>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Vigencia inicial <a style="color: red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar-check-o"></i>
                                            <input type="date" name="vigenciaInicial" id="vigenciaInicial" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la vigencia inicial.</b>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-4"></section>
                                    <section class="col col-4">
                                        <label class="input">Institución <a style="color: red">*</a> </label>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>                                                
                                            <select name="institucion" id="institucion" style="width: 100%" class="select2" disabled="disabled"></select>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label>Vigencia final <a style="color: red">*</a></label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar-check-o"></i>
                                            <input type="date" name="vigenciaFinal" id="vigenciaFinal" />
                                            <input type="hidden" id="hideid" name="hideid" runat="server" />
                                            <input type="hidden" id="hide" name="hide" runat="server" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la vigencia final.</b>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                            <footer>
                                <a class="btn btn-sm btn-default" href="contrato.aspx"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                                
                                <a class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
        </section>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/moment/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();                                  

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                $("#ctl00_contenido_usuario").val("");
                $("#ctl00_contenido_nombre").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#ctl00_contenido_email").val("");                                                                
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    guardar();
                }
            });            

             function CargarSubContrato(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "subcontrato_edit.aspx/getSubcontract",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;                         
                         loadContracts(data.ContratoId);
                         loadInstitutions(data.InstitucionId, data.ContratoId);
                         $("#subcontrato").val(data.Nombre);
                         $("#vigenciaInicial").val(data.VigenciaInicial);                         
                         $("#vigenciaFinal").val(data.VigenciaFinal);                         
                         $('#ctl00_contenido_hideid').val(data.Id);
                         $('#ctl00_contenido_hide').val(data.TrackingId);                         
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function validar() {                
                var esvalido = true;                

                if ($('#institucion').val() == "0" || $('#institucion').val() == null) {
                    ShowError("Institución", "La institución es obligatoria.");
                    $("#institucion").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#institucion").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#contrato').val() == "0" || $('#contrato').val() == null) {
                    ShowError("Contrato", "El contrato es obligatorio.");
                    $("#contrato").select2({ containerCss: { "border-color": "#a90329" } });
                    esvalido = false;
                }
                else {
                    $("#contrato").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($("#subcontrato").val().split(" ").join("") == "") {
                    ShowError("Subcontrato", "El subcontrato es obligatorio.");
                    $('#subcontrato').parent().removeClass('state-success').addClass("state-error");
                    $('#subcontrato').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#subcontrato').parent().removeClass("state-error").addClass('state-success');
                    $('#subcontrato').addClass('valid');
                }

                if ($("#vigenciaInicial").val().split(" ").join("") == "") {
                    ShowError("Vigencia inicial", "La vigencia inicial es obligatoria.");
                    $('#vigenciaInicial').parent().removeClass('state-success').addClass("state-error");
                    $('#vigenciaInicial').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#vigenciaInicial').parent().removeClass("state-error").addClass('state-success');
                    $('#vigenciaInicial').addClass('valid');
                }

                if ($("#vigenciaFinal").val().split(" ").join("") == "") {
                    ShowError("Vigencia final", "La vigencia final es obligatoria.");
                    $('#vigenciaFinal').parent().removeClass('state-success').addClass("state-error");
                    $('#vigenciaFinal').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#vigenciaFinal').parent().removeClass("state-error").addClass('state-success');
                    $('#vigenciaFinal').addClass('valid');
                }

                return esvalido;
            }

            function guardar() {
                startLoading();
                var subcontrato = ObtenerValores();
                
                $.ajax({
                    type: "POST",
                    url: "subcontrato_edit.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'subcontrato': subcontrato }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del subcontrato se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del subcontrato se  " + resultado.mensaje + " correctamente.");                            
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "Ocurrió un error. No fue posible guardar el subcontrato. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function ObtenerValores() {                
                var subcontrato = {                    
                    Nombre: $('#subcontrato').val(),
                    VigenciaInicial: $('#vigenciaInicial').val(),
                    VigenciaFinal: $('#vigenciaFinal').val(),
                    Id: $('#ctl00_contenido_hideid').val(),
                    ContratoId: $('#contrato').val(),
                    InstitucionId: $("#institucion").val(),
                    TrackingId: $('#ctl00_contenido_hide').val(),                    
                };                
                return subcontrato;
            }

            function init() {                
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    CargarSubContrato(param);
                }
                else {                    
                    loadContracts("0");
                    loadInstitutions("0", "0");
                }
            }

            $("#contrato").change(function () {
                loadInstitutions("0", $("#contrato").val());
            });

            function loadContracts(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "subcontrato_edit.aspx/getContracts",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#contrato');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Contrato]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");                            
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadInstitutions(setvalue, id) {
                $.ajax({
                    type: "POST",
                    url: "subcontrato_edit.aspx/getInstitutions",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $('#institucion');
                        if (response.d.length > 0) {                                                        
                            setvalue = response.d[0].Id;
                        }
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Institución]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");                            
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            init();

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
