﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="registro_celda.aspx.cs" Inherits="Web.Application.Control_Celdas.registro_celda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
  
  <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Control_Celdas/celdas.aspx">Control de celdas</a></li>  <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Control_Celdas/movimientos.aspx">Movimientos</a></li>
        <li>Registro</li> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
            <style type="text/css">
         td.strikeout {
            text-decoration: line-through;
        }
    
    </style>
    <div class="scroll">
   <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-personal-2" data-widget-editbutton="false" data-widget-custombutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                    <h2>Registro </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body">
                        <p></p>
                        <p></p>                                                                                                          
                        <div class="row">                                                   
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                    <br />
                                        <div id="Div11" class="smart-form">
                                            <fieldset>
                                            <legend>Remisión</legend>
                                                <div class="row">                                                    
                                                    <section class="col col-5">
                                                        <label class="label">Fecha remisión</label>
                                                         <label class="input">
                                                            <label class='input-group date' id='remisiondatetimepicker'>
                                                                <input type="text" name="Fecha detencion" id="fecharemision" class='input-group date' placeholder="Fecha y hora de ingreso" data-requerido="true" disabled="disabled" />
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <%--<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>--%>
                                                            </label>
                                                        </label> 
                                  
                                                        
                                                        
                                                    </section>
                                                    <section class="col col-5">
                                                        <label >Fecha detención</label>
                                                         <label class="input">
                                                            <label class='input-group date' id='autorizaciondatetimepicker'>
                                                                <input type="text" name="Fecha detencion" id="fecha" class='input-group date' placeholder="Fecha y hora de ingreso" data-requerido="true" disabled="disabled"/>
                                                                 <i class="icon-append fa fa-calendar"></i>
                                                                <%--<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>--%>
                                                                </span>
                                                            </label>
                                                        </label>
                                  
                                                        
                                                    </section>
                                                    </div>
                                                        <div class="row">
                                                    <section class="col col-5">
                                                        <label class="label">Motivo remisión</label>
                                                        <label class="input">
                                                            <textarea id="motivoremision" name="motivoremision" cols="52" rows="5" disabled="disabled"></textarea>                                                            
                                                        </label>                                                        
                                                       
                                                    </section>      
                                                    <section class="col col-5">
                                                        <label class="label">Motivo detención</label>
                                                        <label class="input">
                                                            <textarea id="motivodetencnion" name="motivodetencion" cols="52" rows="5" disabled="disabled"></textarea>                                                            
                                                        </label>                                                        
                                                        
                                                    </section>

                                                    
                                                </div>
                                             <legend>Movimiento</legend>
                                                <br />
                                                <div  class="Row">
                                                       
                                               
                                                <section class="col col-3">
                                                    <label class="label">Descripción <a style="color: red">*</a></label>
                                                    <label class="select">
                                                <select name="descripcioncelda" id="descripcioncelda" >
                                                    
                                                </select>
                                                        </label>
                                                <i></i>
                                        
                                                </section>

                                                <section class="col col-3">
                                                    <label class="label">Fecha y hora <a style="color: red">*</a></label>
                                                  <label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker'>
                                                                <input type="text" name="fechahora" id="fechahora" class='input-group date' placeholder="Fecha y hora de ingreso" data-requerido="true"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                        </label>
                                                <i></i>
                                        
                                                </section>

                                                    <section class="col col-3">
                                                    <label class="label">Celda <a style="color: red">*</a></label>
                                                  <label class="input">
                                                            <label class='input' id='celda'>
                                                                
                                                                <input type="text" name="celda" id="idcelda" class='input' placeholder="Celda" disabled="disabled" data-requerido="true"/>
                                                                
                                                            </label>
                                                       
                                                        </label>
                                                <i></i>
                                        </section>
                                                    <section>
                                                        <br />
                                                        <a  class="btn btn-default buscaCelda" id="buscaCelda"  title="Buscar celda"><i class="fa fa-search"></i>&nbsp; Buscar&nbsp;  </a>

                                                       
                                                    </section>
                                                    <section class="col col-10">
                                                        <label class="label">Observaciones <a style="color: red">*</a></label>
                                                        <label class="input">
                                                            <textarea id="observaciones" name="observaciones" cols="109" rows="5"></textarea>                                                            
                                                        </label>                                                        
                                                    </section>
                                                     <section class="col col-10">
                                                    <label class="label">Responsable <a style="color: red">*</a></label>
                                                    
                                                          <label class="input">
                                                                    <label class='input' id='responsable'>
                                                                
                                                                        <input size="190" type="text" name="idresponsable" id="idresponsable" class='input' placeholder="Responsable" data-requerido="true"/>
                                                                
                                                                    </label>
                                                       
                                                           </label>
                                                       
                                                <i></i>
                                        </section>
                                                    <section class="col col-6">
                                                    <label class="label">Institución <a style="color: red">*</a></label>
                                                    <label class="select">
                                                <select  name="Institucion" id="Institucion" runat="server">
                                                   
                                                </select>
                                                        </label>
                                                <i></i>
                                        
                                                </section>


                                                </div>
                                            </fieldset>
                                            </div>
                                            <br />
                                            <br />                                            
                                        </div>
                                    </div>
                                    <div class="row smart-form">
                                        <footer>
                                            <a href="movimientos.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>                                            
                                            <a class="btn btn-default saveregistro" id="saveregistro"  title="Guardar"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                             <%--<a class="btn btn-default saveCalificacion" id="saveCalificacion"  title="Guardar"><i class="fa fa-save"></i>&nbsp;Guardar </a>--%>
                                        </footer>
                                        <input type="hidden" id="trackingIdCalificacion" runat="server" />
                                        <input type="hidden" id="idCalificacion" runat="server" />      
                                        <input type="hidden" id="trackingInterno" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>                    
        </article>

    <p></p>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-arma-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar catálogo </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-certificate"></i>
                                                <input type="text" name="nombre" runat="server" id="nombre" placeholder="Celda" maxlength="256" />
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div id="BuscaCelda-modal" class="modal fade"  tabindex="-1" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Búsqueda de celdas
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="registro-form" class="smart-form">

                        <div class="modal-body">
                            
                        
                            <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th data-hide="phone,tablet">Capacidad</th>
                                        <th data-hide="phone,tablet">Nivel de peligrosidad</th>
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                        </div>
                           
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save" id="guardatraslado"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>

            <%--<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-arma-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-bomb"></i></span>
                        <h2 id="titlegrid">Celda</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th data-hide="phone,tablet">Capacidad</th>
                                        <th data-hide="phone,tablet">Nivel de peligrosidad</th>
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>--%>
        </div>
    </section>



    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="input">Celda <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="nombre" runat="server" id="itemnombre" class="alphanumeric" placeholder="Nombre" maxlength="256" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese el nombre o número de celda</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input" >Capacidad <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-certificate"></i>
                                        <input type="text" name="capacidad" runat="server" id="itemcapacidad" class="number" placeholder="Capacidad" maxlength="256" />
                                        <b class="tooltip tooltip-bottom-right">Ingrese la capacidad</b>
                                    </label>
                                </section>
                                <section>
                                    <label>Nivel de peligrosidad <a style="color: red">*</a></label>
                                    <label class="select">
                                        <select name="rol" id="dropdown" runat="server">
                                        </select>
                                        <i></i>
                                    </label>
                                </section>
                             
                            </div>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Eliminar 
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    El registro <strong><span id="itemeliminar"></span></strong>&nbsp;será eliminado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Eliminar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de  <strong>&nbsp<span id="itemnameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="history-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="capacidadhistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="history" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistory" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th>Movimiento</th>
                                        <th data-hide="tablet,fablet,phone">Realizado por</th>
                                        <th>Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

       <div id="historycanceled-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="catalogohistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div2" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistorydeleted" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Nombre</th>
                                        <th>Movimiento</th>
                                        <th data-hide="phone,tablet">Realizado por</th>
                                        <th data-hide="phone,tablet">Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="idhistory" runat="server" />
    <input type="hidden" id="CeldaTrackingId"  value="" />
     <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();
            CargarListadoMovimientos(0);
            CargarListadoCentrosReclusion(0);
            // $('#autorizaciondatetimepicker').datetimepicker({
            //     format: 'DD/MM/YYYY HH:mm:ss'

            //});
            $("#autorizaciondatetimepicker").data("DateTimePicker");
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            var param = RequestQueryString("tracking");
                if (param != undefined) {                                        
                    CargarDatos(param);
                    $("#ctl00_contenido_trackingInterno").val(param);
                }


            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
               $('#autorizaciondatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
             $('#remisiondatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
              $('#FehaHoradatetimepicker').datetimepicker({
                            format: 'DD/MM/YYYY HH:mm:ss'
              });

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        

                    }
                },
                ajax: {
                    type: "POST",
                    url: "listaceldas.aspx/getcelda",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });


                        var celda = $("#ctl00_contenido_nombre").val();
                        parametrosServerSide.celda = celda;
                        parametrosServerSide.emptytable = false;        //window.emptytable;

                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                     {
                         name: "Capacidad",
                         data: "Capacidad"
                     },
                     {
                         name: "M.Nombre",
                         data: "Peligrosidad"
                     },
                     null,
                    null

                ],
                columnDefs: [
                    {
                        targets: 3,
                        name: "Habilitado",
                        render: function (data, type, row, meta) {
                            return row.Habilitado ? "Habilitado" : "Deshabilitado"
                        }
                    },
                    {
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "";
                            var editar = "";
                            var habilitar = "";
                            var seleccion = "seleccionacelda";
                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                edit = "edit";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                edit= "disabled";
                            }

                            // deshabilitar estas dos columnas para activar permisos y desactivar las 2 que siguen
                            //if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + ' "href="javascript:void(0);" data-value = "' + row.Nombre + '" data-capacidad="' + row.Capacidad + '" data-tracking="' + row.TrackingId + '" data-Nivel_Peligrosidad="' + row.Nivel_Peligrosidad + '" data-habilitado="' + row.Habilitado + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            //if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Nombre + '" title="' + txtestatus + '" data-tracking="' + row.TrackingId + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';

                            editar = '<a class="btn btn-primary  ' + edit + ' "href="javascript:void(0);" data-value = "' + row.Nombre + '" data-capacidad="' + row.Capacidad + '" data-tracking="' + row.TrackingId + '" data-Nivel_Peligrosidad="' + row.Nivel_Peligrosidad + '" data-habilitado="' + row.Habilitado + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Nombre + '" title="' + txtestatus + '" data-tracking="' + row.TrackingId + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                            editar='&nbsp;<a class="btn btn-primary ' + seleccion + '"data-tracking="' + row.TrackingId + '" data-value="' + row.Nombre + '" title="Seleccionar">Seleccionar</a>&nbsp;';

                            return editar 
                              //habilitar +
                              //  '<a class="btn btn-default btn-circle historial" href="javascript:void(0);" data-value="' + row.Nombre + '" data-tracking="' + row.TrackingId + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>'
                            ;
                        }
                    }
                ]
            });
           
            $("body").on("click", ".seleccionacelda", function () {
                var nombreCelda = $(this).attr("data-value");
                var CeldaTrackingId=$(this).attr("data-tracking");
                $("#idcelda").val(nombreCelda);
                $("#CeldaTrackingId").val(CeldaTrackingId);
                $("#BuscaCelda-modal").modal("hide");
               
            });
            $("body").on("click", ".saveregistro", function () {
                if (validarCeldas())
                {
                    guardarMovimientoCelda();
                }
            });

            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-tracking"));
                $("#blockitem-modal").modal("show");
            });
                //Cambio id por tracking. Revisar funcionalidad.
            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "celda.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho!</strong>" +
                                " El registro  se actualizó correctamente.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", " El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });


            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_itemnombre").val("");
                $("#ctl00_contenido_itemcapacidad").val("");
                $("#ctl00_contenido_dropdown").val("");              
            });
           

            $("body").on("click", ".add", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                $("#ctl00_contenido_itemnombre").val("");
                $("#ctl00_contenido_itemcapacidad").val("");
                $("#ctl00_contenido_peligrosidad").val("");
                CargarListado(0);
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-tracking", "");
                $("#form-modal-title").empty();
                $('#habilitado').prop('checked', false);
                $("#form-modal-title").html("Agregar registro");
                $("#form-modal").modal("show");
            });

            $("body").on("click", ".edit", function () {
                limpiar();
                $("#ctl00_contenido_lblMessage").html("");
                var id = $(this).attr("data-id");
                var tracking = $(this).attr("data-tracking");
                var capacidad = $(this).attr("data-capacidad");
                var Nivel_Peligrosidad = $(this).attr("data-Nivel_Peligrosidad");
                var habilitado = $(this).attr("data-habilitado");

                $("#btnsave").attr("data-id", id);
                $("#btnsave").attr("data-tracking", tracking);
                $("#btnsave").attr("data-habilitado", habilitado);
                $("#ctl00_contenido_itemnombre").val($(this).attr("data-value"));
                $("#ctl00_contenido_itemcapacidad").val($(this).attr("data-capacidad"));
                CargarListado(Nivel_Peligrosidad);
                $("#itemhabilitado").val($(this).attr("data-habilitado"));
                $("#form-modal-title").empty();
                $("#form-modal-title").html("Editar registro");
                $("#form-modal").modal("show");
            });

         


            $("body").on("click", ".delete", function () {
                var id = $(this).attr("data-id");
                var nombre = $(this).attr("data-value");
                $("#itemeliminar").text(nombre);
                $("#btndelete").attr("data-id", id);
                $("#delete-modal").modal("show");
            });
                 $("body").on("click", ".buscaCelda", function () {
                $("#BuscaCelda-modal").modal("show");
                 
                 //ShowSelected();
                window.emptytableadd = false;
                window.tableadd.api().ajax.reload();

            });


            $("body").on("click", ".save", function () {
                var id = $("#btnsave").attr("data-id");
                var tracking = $("#btnsave").attr("data-tracking");
                var habilitado = $("#btnsave").attr("data-habilitado");

                if (id == "") {
                    habilitado = true;
                }
              
                datos = [
                      id = id,
                      tracking = tracking,
                      nombre = $("#ctl00_contenido_itemnombre").val(),
                      capacidad = $("#ctl00_contenido_itemcapacidad").val(),
                      peligrosidad = $("#ctl00_contenido_dropdown").val()

                ];

                if (validar()) {
                    Save(datos);
                }

            });

            function limpiar() {
                $('#ctl00_contenido_itemcapacidad').parent().removeClass('state-success');
                $('#ctl00_contenido_itemcapacidad').parent().removeClass("state-error");
                $('#ctl00_contenido_itemnombre').parent().removeClass('state-success');
                $('#ctl00_contenido_itemnombre').parent().removeClass("state-error");
                $('#ctl00_contenido_dropdown').parent().removeClass('state-success');
                $('#ctl00_contenido_dropdown').parent().removeClass("state-error");
            }

            function validar() {
                var esvalido = true;
                if ($("#ctl00_contenido_itemnombre").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#ctl00_contenido_itemnombre').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemnombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemnombre').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemnombre').addClass('valid');
                }

                if ($("#ctl00_contenido_itemcapacidad").val().split(" ").join("") == "") {

                    ShowError("Descripción", "La capacidad es obligatoria.");
                    $('#ctl00_contenido_itemcapacidad').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_itemcapacidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_itemcapacidad').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_itemcapacidad').addClass('valid');
                }



                if ($("#ctl00_contenido_dropdown").val() == null || $("#ctl00_contenido_dropdown").val() == "0") {
                    ShowError("Nivel de peligrosidad", "El nivel de peligrosidad es obligatorio.");
                    $('#ctl00_contenido_dropdown').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_dropdown').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_dropdown').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_dropdown').addClass('valid');
                }

                return esvalido;
            }

            ///Validaciones
       

             function ObtenerValoresMovimientoCelda() {                                
                var MovimientoCelda = {                    
                    TipoMovimientoId: $("#descripcioncelda").val(),
                    Fechahora:$('#fechahora').val(),
                    CeldaTrackingId: $('#CeldaTrackingId').val(),
                    Observaciones: $("#observaciones").val(),
                    ResponsablieId: $("#idresponsable").val(),
                    InstitucionId:$("#ctl00_contenido_Institucion").val(),
                    TrackingId:""
                    
                };
                return MovimientoCelda;
            }
            function guardarMovimientoCelda() {
                startLoading();
                var MovimientoCelda = ObtenerValoresMovimientoCelda();

                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    MovimientoCelda.TrackingId = param;
                }
                
                $.ajax({
                    type: "POST",
                    url: "registro_celda.aspx/MovimientoCeldaSave",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'MovimientoCelda': MovimientoCelda }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "La información del estado se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!",  + resultado.mensaje + " ");                            
                           
                           // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                          $('#main').waitMe('hide');
                            
                           
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }


                 function validarCeldas() {
                var esvalido = true;

                if ($("#descripcioncelda").val() == "0") {
                    ShowError("Descripción", "El campo descripción es obligatorio");
                    $('#descripcioncelda').parent().removeClass('state-success').addClass("state-error");
                    $('#descripcioncelda').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#descripcioncelda').parent().removeClass("state-error").addClass('state-success');
                    $('#descripcioncelda').addClass('valid');
                     }

                    
                     if ($("#fechahora").val() == ""|| $('#fechahora').val() == null)
                     {
                      ShowError("Fecha y hora", "El campo Fecha y hora es obligatorio");
                    $('#fechahora').parent().removeClass('state-success').addClass("state-error");
                    $('#fechahora').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fechahora').parent().removeClass("state-error").addClass('state-success');
                    $('#fechahora').addClass('valid');
                     }


                        if ($("#celda").text == "")
                     {
                      ShowError("Celda", "El campo celda es obligatorio");
                    $('#celda').parent().removeClass('state-success').addClass("state-error");
                    $('#celda').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#celda').parent().removeClass("state-error").addClass('state-success');
                    $('#celda').addClass('valid');
                     }

              if ($("#observaciones").val() == ""|| $('#observaciones').val() == null)
                     {
                      ShowError("Observaciones", "El campo Observaciones es obligatorio");
                    $('#observaciones').parent().removeClass('state-success').addClass("state-error");
                    $('#observaciones').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observaciones').parent().removeClass("state-error").addClass('state-success');
                    $('#observaciones').addClass('valid');
                     }

                if ($("#idresponsable").val() == "") {
                    ShowError("Responsable", "El campo Responsable es obligatorio");
                    $('#idresponsable').parent().removeClass('state-success').addClass("state-error");
                    $('#idresponsable').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#idresponsable').parent().removeClass("state-error").addClass('state-success');
                    $('#idresponsable').addClass('valid');
                     }

                if ($("#ctl00_contenido_Institucion").val() == "0") {
                    ShowError("Institución", "El campo institución es obligatorio");
                    $('#ctl00_contenido_Institucion').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_Institucion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_Institucion').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_Institucion').addClass('valid');
                     }

                return esvalido;
            }

            ////validaciones

              function CargarListadoMovimientos(id) {
                
                $.ajax({

                    type: "POST",
                    url: "registro_celda.aspx/getListadoMovimiento_Celda_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#descripcioncelda');
                        Dropdown.empty();
                        Dropdown.append(new Option("[seleccione movimiento]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

              function CargarListadoCentrosReclusion(id) {
                
                $.ajax({

                    type: "POST",
                    url: "registro_celda.aspx/getListadoMovimiento_CentroReclusion_Combo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_Institucion');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Seleccione centro]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }


            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "registro_celda.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#motivodetencnion').text(resultado.obj.MotivosDetencion);
                            $('#motivoremision').text(resultado.obj.MotivoRemision);
                            $('#fecharemision').val(resultado.obj.FechaRemision)
                            $('#fecha').val(resultado.obj.FechaDetencion)
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            $('#ctl00_contenido_hideid').val(resultado.obj.Id);
                            $('#ctl00_contenido_hide').val(resultado.obj.TrackingId);
              
                        }
                        else {
                            ShowError("Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }   


            $("#btndelete").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "celda.aspx/delete",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: id,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "El registro fue eliminado del catálogo.", "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El registro fue eliminado del catálogo.");
                            $("#delete-modal").modal("hide");
                            $('#main').waitMe('hide');
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }

                    }
                });
            });


            $("body").on("click", ".historial", function () {
                var id = $(this).attr("data-tracking");
                $('#ctl00_contenido_idhistory').val(id);
                var capacidad = $(this).attr("data-value");
                $("#capacidadhistorial").text(capacidad);
                $("#history-modal").modal("show");

                window.emptytablehistory = false;
                window.tablehistory.api().ajax.reload();

            });

            var responsiveHelper_dt_basichistory = undefined;
            var breakpointHistoryDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistory = true;
           

            $("body").on("click", ".historialcancelado", function () {
                $("#historycanceled-modal").modal("show");

                window.emptytablehistorydeleted = false;
                window.tablehistorydeleted.api().ajax.reload();
            });

            var responsiveHelper_dt_basichistorydeleted = undefined;
            window.emptytablehistorydeleted = true;

            
            $('#ctl00_contenido_itemcapacidad').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
            });
            init();
            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }

            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>



