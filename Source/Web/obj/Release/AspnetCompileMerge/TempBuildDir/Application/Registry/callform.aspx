﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="callform.aspx.cs" Inherits="Web.Application.Registry.callform" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Registry/calls.aspx">Llamadas y eventos</a></li>
    <li>Llamadas</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="scroll">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken"  id="wid-callform-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="glyphicon  glyphicon-edit"></i></span>
                            <h2>Registro </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div id="smart-form-register-entry" class="smart-form">
                                    <header>
                                        Registro de llamada
                                    </header>
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-4">
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Fecha y hora <a style="color:red">*</a></label>
                                                <div class='input-group date' id='fechadatetimepicker'>
                                                    <input type="text" class="form-control" name="Fecha" id="fecha" placeholder="Fecha y hora de ingreso" data-requerido="true"/>
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Folio</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Folio" id="folio" placeholder="Folio" maxlength="50" class="alphanumeric" data-requerido="true" disabled/>
                                                </label>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="label">Involucrado <a style="color:red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Involucrado" id="involucrado" placeholder="Involucrado" maxlength="164" class="alphanumeric alptext" data-requerido="true"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el involucrado.</b>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Descripción <a style="color:red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Descripción" id="descripcion" placeholder="Descripción" maxlength="164" class="alphanumeric alptext" data-requerido="true"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingrese la descripción.</b>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Lugar detención <a style="color:red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Lugar detención" id="lugar" placeholder="Lugar detención" maxlength="164" class="alphanumeric alptext" data-requerido="true"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el lugar de detención.</b>
                                                </label>
                                            </section>                                    
                                        </div>
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="label">País <a style="color:red">*</a></label>
                                                <%--<label class="select">--%>
                                                    <select name="País" id="pais" data-requerido="true" class="select2" style="width:100%;"></select>
                                                    <i></i>
                                                <%--</label>--%>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Estado <a style="color:red">*</a></label>
                                                <%--<label class="select">--%>
                                                    <select name="Estado" id="estado" data-requerido="true" class="select2" style="width:100%;"></select>
                                                    <i></i>
                                                <%--</label>--%>
                                            </section>        
                                            <section class="col col-4">
                                                <label class="label">Municipio <a style="color:red">*</a></label>
                                                <%--<label class="select">--%>
                                                    <select name="Municipio" id="municipio" data-requerido="true" class="select2" style="width:100%;"></select>
                                                    <i></i>
                                                <%--</label>--%>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Colonia <a style="color:red">*</a></label>
                                                <%--<label class="select">--%>
                                                    <select name="Colonia" id="colonia" data-requerido="true" class="select2" style="width:100%;"></select>
                                                    <i></i>
                                                <%--</label>--%>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Código postal <a style="color:red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Código postal" id="codigoPostal" maxlength="20" data-requerido="true" disabled="disabled"/>
                                                </label>
                                            </section>                           
                                        </div>
                                    </fieldset>
                                    <footer>
                                        <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                            <a style="float: none;" href="javascript:void(0);" class="btn btn-default save" id="save_" title="Guardar registro actual"><i class="fa fa-save"></i>&nbsp;Guardar </a>                                
                                            <a style="float: none;" href="calls.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                document.getElementById("save_").click();
            }
        });
        $(document).ready(function () {
            pageSetUp();
            Cargarnumticket();

            $("#pais").change(function () {
                loadStates("0", $("#pais").val());
            });

            $("#estado").change(function () {
                loadCities("0", $("#estado").val());
            });

            $("#municipio").change(function () {
                loadNeighborhood("0", $("#municipio").val());
            });

            $("#colonia").change(function () {
                loadZipCode($("#colonia").val());
            });

            loadCountries("73");
            loadStates("0", 73);

            function loadCountries(setvalue) {
                $.ajax({                    
                    type: "POST",
                    url: "callform.aspx/getCountries",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#pais');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[País]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadStates(setvalue, idPais) {
                $.ajax({                    
                    type: "POST",
                    url: "callform.aspx/getStates",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idPais: idPais
                    }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#estado');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Estado]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadCities(setvalue, idEstado) {
                $.ajax({                    
                    type: "POST",
                    url: "callform.aspx/getCities",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idEstado: idEstado
                    }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#municipio');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Municipio]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadNeighborhood(set, idMunicipio) {                
                $.ajax({
                    type: "POST",
                    url: "callform.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#colonia");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadZipCode(idColonia) {                
                $.ajax({
                    type: "POST",
                    url: "callform.aspx/getZipCode",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idColonia: idColonia
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        $("#codigoPostal").val(resultado.cp);                                                
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $('#fechadatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss',
                widgetPositioning: {
                    horizontal: "auto",
                    vertical: "auto"
                }
            }).on('change', function (e) {
                $(".bootstrap-datetimepicker-widget").toggle();
            });

            $('#fechadatetimepicker').data("DateTimePicker").hide();

            var param = RequestQueryString("tracking");
            (function () {
                // obtenerMunicipios();
                // obtenerSectores();

                if (param !== undefined) {
                    obtenerLlamada(param);
                   // obtener datos
                   // llenar formulario
                }
            }());

            function generarObjeto($items) {
                var obj = {};
                $items.each(function () {
                    var id = this.id;
                    obj[id] = $(this).val();
                });
                return obj;
            }

            function llenarSelect(idSelect, datos) {
                var select = document.getElementById('' + idSelect);

                for (var i = 0; i < datos.length; i++) {
                    var opt = document.createElement('option');
                    opt.innerHTML = datos[i].Nombre;
                    opt.value = datos[i].Id;
                    select.appendChild(opt);
                }
            }

            function camposVacios(_items) {
                var _tmpItems = [].slice.call(_items);
                var esValido = true;
                _tmpItems.map(function (item) {
                    var parent = item.parentNode;
                    var attName = item.getAttribute('name');
                    if (item.nodeName.toLowerCase() === 'select' && item.value === '0') {
                        ShowError('' + attName, 'El campo ' + attName.toLowerCase() + ' es obligatorio.');
                        parent.classList.remove('state-success');
                        parent.classList.add('state-error');
                        item.classList.remove('valid');
                        esValido = false;
                    } else if (item.value === '' || item.value === undefined || item.value.length === 0) {
                        ShowError('' + attName, 'El campo ' + attName.toLowerCase() + ' es obligatorio.');
                        parent.classList.remove('state-success');
                        parent.classList.add('state-error');
                        item.classList.remove('valid');
                        esValido = false;
                    } else {
                        parent.classList.add('state-success');
                        parent.classList.remove('state-error');
                        item.classList.add('valid');
                    }
                });
                return esValido;
            }
            $("#involucrado").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            

            function obtenerLlamada(tkg) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "callform.aspx/getLlamadaByTrackingId",
                    data: JSON.stringify({tracking:tkg }),
                    success: function (response) {
                        response = JSON.parse(response.d);                        
                        endLoading();                        
                        if (response.exitoso) {
                            $("#involucrado").val(response.obj.Involucrado);
                            $("#descripcion").val(response.obj.Descripcion);
                            $("#lugar").val(response.obj.Lugar);
                            loadCountries(response.obj.IdPais);
                            loadStates(response.obj.IdEstado, response.obj.IdPais);
                            loadCities(response.obj.IdMunicipio, response.obj.IdEstado);
                            loadNeighborhood(response.obj.IdColonia, response.obj.IdMunicipio);
                            $("#codigoPostal").val(response.obj.CodigoPostal);
                            $("#folio").val(response.obj.Folio);
                            $("#fecha").val(response.obj.HoraYFecha);

                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." +  response.mensaje);
                        }
                    },
                    error: function (error) {
                        ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + error);
                        endLoading();
                    }
                });
            }

            function obtenerMunicipios() {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "callform.aspx/getListadoMunicipio",
                    success: function (response) {
                        response = JSON.parse(response.d);
                        endLoading();

                        if (response.exitoso) {
                            ShowSuccess("¡Bien!", response.mensaje);
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." +  response.mensaje);
                        }
                    },
                    error: function (error) {
                        ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + error);
                        endLoading();
                    }
                });
            }

            function obtenerSectores() {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "callform.aspx/getListadoSectores",
                    success: function (response) {
                        response = JSON.parse(response.d);
                        endLoading();
                        if (response.exitoso) {
                            ShowSuccess("¡Bien!", response.mensaje);
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                        }
                    },
                    error: function (error) {
                        endLoading();
                    }
                });                
            }

            $("#save_").on("click", function () {
                startLoading();
                var $items = $('[data-requerido]');

                var items_validar = $('[data-requerido="true"]');

                if (!camposVacios(items_validar)) {
                    endLoading();
                    return false;
                }
                
                var obj = generarObjeto($items);
                obj.tracking = param !== undefined ? param : "";                

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "callform.aspx/save",
                    data: JSON.stringify({ csObj: obj }),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso) {
                            $("#folio").val(response.folio);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "La información de la llamada se  " + response.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "La información de la llamada se  " + response.mensaje + " correctamente.");                            
                            $('#main').waitMe('hide');
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico. " + response.mensaje);

                            if (response.fallo == 'fecha') {
                                var fecha = document.getElementById("fecha");
                                fecha.parentNode.classList.remove('state-success');
                                fecha.parentNode.classList.add('state-error');
                            }
                        }
                    },
                    error: function (error) {
                        ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico. " + error);
                        endLoading();
                    }
                });
            });

            function Cargarnumticket() {
            
                $.ajax({
                    type: "POST",
                    url: "callform.aspx/getFolio",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",                   
                        cache: false,
                    success: function (data) {                
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#folio').val(resultado.Numeroticket);
                        }
                        else {
                            ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }

                    },
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
           
        });
    </script>
</asp:Content>
