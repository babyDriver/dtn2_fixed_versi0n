﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="InformacionDetenido.aspx.cs" Inherits="Web.Application.Registry.InformacionDetenido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Información de detenidos</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
            <style type="text/css">
         td.strikeout {
            text-decoration: line-through;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    <div class="scroll">
        <!--
       <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
              Información de detenidos
            </h1>
        </div>
    </div>-->
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
     <section id="widget-grid-0" class="">

            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken"  id="wid-entrya-1" data-widget-editbutton="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="glyphicon  glyphicon-search"></i></span>
                            <h2>Búsqueda de detenidos</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div id="smart-form-register-entry" class="smart-form">                                    
                                    <header>
                                        Criterios de búsqueda
                                    </header>
                                    <fieldset>                                        
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="label">Nombre</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Nombre" id="nombre" placeholder="Nombre" maxlength="256" class="alphanumeric alptext"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Apellido paterno</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Paterno" id="paterno" placeholder="Apellido paterno" maxlength="1000" class="alphanumeric alptext"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Apellido materno</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Apellido Materno" id="materno" placeholder="Apellido materno" maxlength="1000" class="alphanumeric alptext"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">RFC</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="rfc" id="rfc" placeholder="RFC" maxlength="1000" class="alphanumeric"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Edad</label>
                                                <label class="input">
                                                    <input type="number" name="edad" id="edad" placeholder="Edad" class="alptext"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Sexo</label>
                                                <label class="select">
                                                    <select name="sexo" id="sexo">
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Ocupación</label>
                                                <label class="select">
                                                    <select name="ocupacion" id="ocupacion">
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Nacionalidad</label>
                                                <label class="select">
                                                    <select name="nacionalidad" id="nacionalidad">
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Calle</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="calle" id="calle" placeholder="Calle" maxlength="1000" class="alphanumeric alptext"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">País</label>
                                                <label class="select">
                                                    <select name="pais" id="pais" runat="server">
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Estado</label>
                                                <label class="select">
                                                    <select name="estado" id="estado" runat="server">
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Municipio</label>
                                                <label class="select">
                                                    <select name="municipio" id="municipio" runat="server">
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Colonia</label>
                                                <label class="select">
                                                    <select name="colonia" id="colonia">
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Teléfono</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-phone"></i>
                                                    <input type="tel" name="telefono" id="telefono" placeholder="Teléfono" data-mask="(999) 999-9999"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Alias</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="alias" id="alias" placeholder="Alias" maxlength="1000" class="alphanumeric alptext"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Estatura</label>
                                                <label class="input">
                                                    <input type="number" name="estatura" id="estatura" placeholder="Estatura"/>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Peso</label>
                                                <label class="input">
                                                    <input type="number" name="peso" id="peso" placeholder="Peso" maxlength="1000" class="alphanumeric alptext"/>
                                                </label>
                                            </section>
                                        </div>
                                    </fieldset>
                                    <footer>
                                        <a href="javascript:void(0);" class="btn btn-default save" id="save_" title="Guardar registro actual"><i class="fa fa-search"></i>&nbsp;Buscar </a>                                
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
</section>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Detenidos </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover"  width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th data-hide="phone,tablet">Nombre</th>
                                        <th data-hide="phone,tablet">Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                        <th data-hide="always"></th>
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>    

    <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="VYXMBM" runat="server" value=""/>
     <input type="hidden" id="RAWMOV" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>
    
      <div id="photo-arrested" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src="#" alt="fotografía del detenido" />
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id="datospersonales-modal" class="modal fade" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInterno" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="datetime" id="horaRegistro" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right; padding-right: 1px">Motivo de ingreso:</label>
                            <div class="col-md-8">
                                <textarea class="form-control" id="motivo" disabled="disabled"></textarea><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <textarea class="form-control" id="centro" disabled="disabled"></textarea><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="datetime" id="salida" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="estaturaInterno" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="pesoInterno" disabled="disabled" /><p></p>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="Img1" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" class="img-thumbnail same-height-thumbnail" height="240" width="240" /><br /><br />
                    </div>
                    <div class="row">
                        <table id="dt_basic2" class="table table-striped table-bordered table-hover"  width="100%">
                                <thead>
                                    <tr>
                                        <th>Fecha y hora de detención</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                        <th >Motivo de detención</th>
                                        <th>Lugar de detención</th>
                                        <th data-hide="phone,tablet">Institución</th>
                                        <th data-hide="phone,tablet">Situación</th>
                                        <th data-hide="phone,tablet">Fecha y hora salida</th>
                                        <th data-hide="phone,tablet">Tipo de salida</th>
                                        <th data-hide="phone,tablet">Fundamento</th>
                                    </tr>
                                </thead>                               
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                document.querySelector("#save_").click();
            }
        });

        $(document).ready(function () {
            pageSetUp();
            window.emptytable = true;

            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;            

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            var rutaDefaultServer = "";

            getRutaDefaultServer();

            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "entrylist.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }                                             
                    }
                });
            }
         
            //function tabla() {
            responsiveHelper_dt_basic = undefined;
            window.table = $('#dt_basic').dataTable({
                destroy: true,
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                        $('td', row).eq(4).addClass('strikeout');
                        $('td', row).eq(5).addClass('strikeout');
                        $('td', row).eq(6).addClass('strikeout');
                        $('td', row).eq(7).addClass('strikeout');
                    }
                },
                ajax: {
                    url: "InformacionDetenido.aspx/getinterno",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: function (d) {
                        startLoading();
                        d.emptytable = window.emptytable;
                        d.Nombre = $("#nombre").val();
                        d.Paterno = $("#paterno").val();
                        d.Materno = $("#materno").val();
                        d.RFC = $("#rfc").val();
                        d.Edad = $("#edad").val();
                        d.Sexo = $("#sexo").val();
                        d.Ocupacion = $("#ocupacion").val();
                        d.Nacionalidad = $("#nacionalidad").val();
                        d.Calle = $("#calle").val();
                        d.Colonia = $("#colonia").val();
                        d.Telefono = $("#telefono").val();
                        d.Alias = $("#alias").val();
                        d.Estatura = $("#estatura").val();
                        d.Peso = $("#peso").val();
                        d.Pais = $("#ctl00_contenido_pais").val();
                        d.Estado = $("#ctl00_contenido_estado").val();
                        d.Municipio = $("#ctl00_contenido_municipio").val();                                                    
                        d.pages = $('#dt_basic').DataTable().page.info().page || "";
                        return JSON.stringify(d);
                    },
                    dataSrc: "data",
                    dataFilter: function (data) {
                        var json = jQuery.parseJSON(data);
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        endLoading();
                        return JSON.stringify(json);
                    }
                },
                //ajax: {
                //    type: "POST",
                //    url: "InformacionDetenido.aspx/getinterno",
                //    contentType: "application/json; charset=utf-8",
                //    data: function (parametrosServerSide) {
                //        $('#dt_basic').waitMe({
                //            effect: 'bounce',
                //            text: 'Cargando...',
                //            bg: 'rgba(255,255,255,0.7)',
                //            color: '#000',
                //            sizeW: '',
                //            sizeH: '',
                //            source: ''
                //        });
                //        parametrosServerSide.emptytable = false;
                //        parametrosServerSide.Nombre = $("#nombre").val();
                //        parametrosServerSide.Paterno = $("#paterno").val();
                //        parametrosServerSide.Materno = $("#materno").val();
                //        parametrosServerSide.RFC = $("#rfc").val();
                //        parametrosServerSide.Edad = $("#edad").val();
                //        parametrosServerSide.Sexo = $("#sexo").val();
                //        parametrosServerSide.Ocupacion = $("#ocupacion").val();
                //        parametrosServerSide.Nacionalidad = $("#nacionalidad").val();
                //        parametrosServerSide.Calle = $("#calle").val();
                //        parametrosServerSide.Colonia = $("#colonia").val();
                //        parametrosServerSide.Telefono = $("#telefono").val();
                //        parametrosServerSide.Alias = $("#alias").val();
                //        parametrosServerSide.Estatura = $("#estatura").val();
                //        parametrosServerSide.Peso = $("#peso").val();
                //        parametrosServerSide.Pais = $("#ctl00_contenido_pais").val();
                //        parametrosServerSide.Estado = $("#ctl00_contenido_estado").val();
                //        parametrosServerSide.Municipio = $("#ctl00_contenido_municipio").val();
                //        return JSON.stringify(parametrosServerSide);
                //    }

                //},
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    }
                    ,
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "EstatusNombre",
                        data: "EstatusNombre"
                    },
                    null,
                    null                        
                ],
                columnDefs: [                        
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("~/Content/img/avatars/male.png");
                            return '<div class="text-center">'+
                                '<img id="avatar" class="img-thumbnail text-center" alt = "" src = "' + pathfoto + '" height = "10" width = "50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';"/>' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var habilitar = "";

                                habilitar = '<a class="btn btn-success datos" id="datos" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-expediente="'+ row.Expediente +'"></i>&nbsp;Ver información</a>';
                                
                            return habilitar;

                        }
                    },
                    {
                        targets: 9,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            var nombreCompleto = row.Nombre + ' ' + row.Paterno + ' ' + row.Materno;
                            return nombreCompleto;
                        }
                    }
                ]

            });

            dtable = $("#dt_basic").dataTable().api();

            $("#dt_basic_filter input[type='search']")
                .unbind()
                .bind("input", function (e) {

                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
                });

            var $textarea = $("#dt_basic_filter input[type='search']");

            $textarea.on('blur', function () {
                setTimeout(function () {
                    $textarea.focus();
                    $('.btn-group').removeClass('open');
                }, 0);
            });

            $("#dt_basic_filter input[type='search']").keypress(function (e) {
                if (e.charCode === 13) {
                    dtable.search($("#dt_basic_filter input[type='search']").val()).draw();
                }
            });
            //}

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }
            
            $("#save_").on("click", function () {

                if (validar()) {
                    //tabla();
                    window.emptytable = false;
                    $("#dt_basic").DataTable().ajax.reload();
                }
                else {
                    ShowAlert("¡Aviso!", "Debe ingresar al menos un parámetro de búsqueda");
                }
            });
            
            function CargarOcupacion(set) {
                $('#ocupacion').empty();
                $.ajax({

                    type: "POST",
                    url: "InformacionDetenido.aspx/getOcupacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ocupacion');

                        Dropdown.append(new Option("[Ocupación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de ocupaciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarNacionalidad(set) {
                $('#nacionalidad').empty();
                $.ajax({

                    type: "POST",
                    url: "InformacionDetenido.aspx/getNacionalidad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#nacionalidad');

                        Dropdown.append(new Option("[Nacionalidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de nacionalidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarColonia(set, idMunicipio) {
                $('#colonia').empty();
                $.ajax({

                    type: "POST",
                    url: "InformacionDetenido.aspx/getColonia",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $('#colonia');

                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                        }
                        Dropdown.trigger("change");

                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de colonias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarPais(set, combo) {
                $(combo).empty();
                $.ajax({
                    type: "POST",
                    url: "InformacionDetenido.aspx/getPais",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[País]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                        }

                        Dropdown.trigger("change");
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarEstado(setestado, combo, paisid) {

                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "InformacionDetenido.aspx/getEstado",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ paisId: paisid }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Estado]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setestado != "" && setEstado != null) {
                            Dropdown.val(setestado);
                        }

                        Dropdown.trigger("change");
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de estados. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarMunicipio(set, combo, estadoid) {
                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "InformacionDetenido.aspx/getMunicipio",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ estadoId: estadoid }),
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Municipio]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "" && set != null) {
                            Dropdown.val(set);
                        }

                        Dropdown.trigger("change");
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de municipio. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function CargarSexo(set, combo) {
                $(combo).empty();
                $.ajax({

                    type: "POST",
                    url: "InformacionDetenido.aspx/getSexo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $(combo);

                        Dropdown.append(new Option("[Sexo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            init();
            function init() {               
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }
                CargarOcupacion(0);
                CargarNacionalidad(0);
                CargarSexo(0, '#sexo');
                CargarPais(0, '#ctl00_contenido_pais');
            }

             $("body").on("click", ".datos", function () {
                 var id = $(this).attr("data-id");
                 var expediente = $(this).attr("data-expediente");
                 CargarDatosPersonales(id, expediente);
                 cargartablainformacionpersonal(id);
            });
            function cargartablainformacionpersonal(trackingid) {
                responsiveHelper_dt_basic = undefined;
                window.table = $('#dt_basic2').dataTable({
                    destroy: true,
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,                    
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic2'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic2').waitMe('hide');
                    },
                    ajax: {
                        url: "InformacionDetenido.aspx/GetDataInterno",
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: function (d) {
                            startLoading();
                            d.emptytable = false;
                            d.trackingid = trackingid;
                            d.pages = $('#dt_basic').DataTable().page.info().page || "";
                            return JSON.stringify(d);
                        },
                        dataSrc: "data",
                        dataFilter: function (data) {
                            var json = jQuery.parseJSON(data);
                            json.recordsTotal = json.d.recordsTotal;
                            json.recordsFiltered = json.d.recordsFiltered;
                            json.data = json.d.data;
                            endLoading();
                            return JSON.stringify(json);
                        }
                    },
                    columns: [
                        null,
                        {
                            name: "Expediente",
                            data: "Expediente"
                        },
                        {
                            name: "Motivo",
                            data: "Motivo"
                        }
                        ,
                        {
                            name: "LugarDetencion",
                            data: "LugarDetencion"
                        },
                        {
                            name: "Institucion",
                            data: "Institucion"
                        },
                        {
                            name: "Situacion",
                            data: "Situacion",

                        },
                        null,
                        {
                            name: "TipoSalida",
                            data: "TipoSalida",

                        },
                        {
                            name: "Fundamento",
                            data: "Fundamento",

                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,                            
                            render: function (data, type, row, meta) {
                                return toDateTime(row.FechaIngreso);
                            }
                        },
                        {
                            targets: 6,
                            render: function (data, type, row, meta) {
                                var fecha = toDateTime(row.FechaSalida);
                                var fecha2 = new Date(parseFloat(row.FechaSalida.substr(6)));
                                                                
                                if (fecha2.getFullYear() === 0) {
                                    return '';
                                }
                                else {
                                    return fecha;
                                }
                            }
                        }
                    ]
                });

                dtable = $("#dt_basic2").dataTable().api();

                $("#dt_basic2_filter input[type='search']")
                    .unbind()
                    .bind("input", function (e) {

                        if (this.value == "") {
                            dtable.search("").draw();
                        }
                        return;
                    });

                var $textarea = $("#dt_basic2_filter input[type='search']");

                $textarea.on('blur', function () {
                    setTimeout(function () {
                        $textarea.focus();
                        $('.btn-group').removeClass('open');
                    }, 0);
                });

                $("#dt_basic2_filter input[type='search']").keypress(function (e) {
                    if (e.charCode === 13) {
                        dtable.search($("#dt_basic2_filter input[type='search']").val()).draw();
                    }
                });
            }
            function validar() {
                var valido = false;
                if ($("#nombre").val() != "")
                    valido = true;
                if ($("#paterno").val() != "")
                    valido = true;
                if ($("#materno").val() != "")
                    valido = true;
                if ($("#rfc").val() != "")
                    valido = true;
                if ($("#edad").val() != "")
                    valido = true;
                if ($("#sexo").val() != "0" && $("#sexo").val() != null)
                    valido = true;
                if ($("#ocupacion").val() != "0" && $("#ocupacion").val() != null)
                    valido = true;
                if ($("#nacionalidad").val() != "0" && $("#nacionalidad").val() != null)
                    valido = true;
                if ($("#calle").val() != "")
                    valido = true;
                if ($("#colonia").val() != "0" && $("#colonia").val() != null)
                    valido = true;
                if ($("#telefono").val() != "")
                    valido = true;
                if ($("#alias").val() != "")
                    valido = true;
                if ($("#estatura").val() != "")
                    valido = true;
                if ($("#peso").val() != "")
                    valido = true;
                if ($("#ctl00_contenido_pais").val() != "0" && $("#ctl00_contenido_pais").val() != null)
                    valido = true;
                if ($("#ctl00_contenido_estado").val() != "0" && $("#ctl00_contenido_estado").val() != null)
                    valido = true;
                if ($("#ctl00_contenido_municipio").val() != "0" && $("#ctl00_contenido_municipio").val() != null)
                    valido = true;

                return valido;
            }

            function CargarDatosPersonales(trackingid, exp) {
                startLoading();
                  $.ajax({
                      type: "POST",
                      url: "InformacionDetenido.aspx/getdatos",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: trackingid,
                          expediente: exp
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              $('#nombreInterno').val(resultado.obj.Nombre);
                              $('#motivo').val(resultado.obj.Motivo);
                              var imagenAvatar;
                              if (resultado.obj.Imagen != null && resultado.obj.Imagen != "") {
                                  imagenAvatar = resolveUrl(resultado.obj.Imagen);
                              }
                              else {
                                  imagenAvatar = resolveUrl("~/Content/img/avatars/male.png");
                              }
                              $('#Img1').attr("src", imagenAvatar);
                              $('#horaRegistro').val(resultado.obj.Registro);
                              $("#centro").val(resultado.obj.Centro);
                              $("#estaturaInterno").val(resultado.obj.Estatura);
                              $("#pesoInterno").val(resultado.obj.Peso);
                              $('#salida').val(resultado.obj.Salida);
                              $("#datospersonales-modal").modal("show");
                          } else {
                              if (resultado.mensaje == "Información no disponible") {
                                  ShowError("¡Error!", resultado.mensaje);
                              }
                              else {
                                  ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                              }
                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
            }            

            $("#ctl00_contenido_pais").change(function () {
                CargarEstado(0, '#ctl00_contenido_estado', $("#ctl00_contenido_pais").val());
                CargarMunicipio(0, '#ctl00_contenido_municipio', $("#ctl00_contenido_estado").val());
                CargarColonia("0", $("#ctl00_contenido_municipio").val());
            });

            $("#ctl00_contenido_estado").change(function () {
                CargarMunicipio(0, '#ctl00_contenido_municipio', $("#ctl00_contenido_estado").val());
                CargarColonia("0", $("#ctl00_contenido_municipio").val());
            });

            $("#ctl00_contenido_municipio").change(function () {               
                CargarColonia("0", $("#ctl00_contenido_municipio").val());
            });


        });
    </script>
</asp:Content>
