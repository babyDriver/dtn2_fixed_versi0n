﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="eventsform.aspx.cs" Inherits="Web.Application.Registry.eventsform" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Registry/events.aspx">Llamadas y eventos</a></li>
    <li>Eventos</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' />
    <style type="text/css">
        #mapid {
            height: 30vw;
            width: 100%;
        }

        .coordinates {
            background: rgba(0,0,0,0.5);
            color: #fff;
            position: absolute;
            bottom: 40px;
            left: 10px;
            padding: 5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 3px;
            display: none;
        }

        .wrapping {
            /* These are technically the same, but use both */
            overflow-wrap: break-word;
            word-wrap: break-word;
            -ms-word-break: break-all;
            /* This is the dangerous one in WebKit, as it breaks things wherever */
            word-break: break-all;
            /* Instead use this non-standard one: */
            word-break: break-word;
            /* Adds a hyphen where the word breaks, if supported (No Blink) */
            -ms-hyphens: auto;
            -moz-hyphens: auto;
            -webkit-hyphens: auto;
            hyphens: auto;
        }
        td.strikeout {
            text-decoration: line-through;
        }                   
        textarea {            
            margin-top:10px;
            width:100%;
            border: 1px solid #BDBDBD;
        }
        textarea:focus {
            box-shadow: 0 0 5px #5D98CC;            
            border: 1px solid #5D98CC;            
        }
        textarea:hover {
            box-shadow: 0 0 0px #5D98CC;            
            border: 1px solid #5D98CC;            
        }        
     
        input[type=checkbox]{
            -ms-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
            padding: 10px;
            cursor: pointer;
        }
        .checkboxtext{
            font-size: 100%;
            display: inline;       
            color: #004987;
        }
        .checkboxtext:hover{
            cursor:pointer;
        }
        .errorInputTabla{
            background-color:#fff0f0;
            border-color:#A90329;
        }
          .modal {
          overflow-y:auto;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: 400;
            text-align: center;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            white-space: nowrap;
            padding: 6px 12px;
            font-size: 13px;
            line-height: 1.42857143;
        }
    

        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
         #ScrollableContent {
            height: calc(100vh - 150px);
        }
    </style>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:HiddenField ID="tracking" runat="server" />
    <div id="Lmapa"></div>
    <div class="scroll">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-entrya-1" data-widget-editbutton="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="glyphicon  glyphicon-edit"></i></span>
                            <h2>Evento</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div id="smart-form-register-entry" class="smart-form">
                                    <fieldset style="padding-top: 0px">
                                        <div class="row">
                                            <a style="float: right; margin-left: 3px; margin-right: 30px; display: none;" id="add" class="btn btn-primary btn-lg" href="javascript:void(0);"><i class="fa fa-plus"></i> Registro en barandilla</a>
                                        </div>
                                        <div class="row">
                                            <header class="col col-1" style="border-bottom: none;" id="alerta1">Alerta web</header>
                                            <section class="col col-4">
                                                <label class="label">Institución</label>
                                                <select name="Unidad" id="alertaWebInstitucion" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Unidad</label>
                                                <select name="Unidad" id="alertaWebUnidad" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            </section>
                                        </div>
                                        <div class="row table-responsive">
                                            <table id="dt_basic_tabla_eventos" class="table table-striped table-bordered table-hover" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th data-class="expand"></th>
                                                        <th data-class="phone,tablet,desktop">Folio</th>
                                                        <th data-hide="phone,tablet,desktop">Fecha</th>
                                                        <th data-hide="phone,tablet,desktop">Motivo</th>
                                                        <th data-hide="phone,tablet,desktop">Número detenidos</th>
                                                        <th data-hide="phone,tablet,desktop">Detenidos</th>
                                                        <th data-hide="phone,tablet,desktop">Estado</th>
                                                        <th data-hide="phone,tablet,desktop">Municipio</th>
                                                        <th data-hide="phone,tablet,desktop">Colonia</th>
                                                        <th data-hide="phone,tablet,desktop">Número</th>
                                                        <th data-hide="phone,tablet,desktop">Entre calle y calle</th>
                                                        <th data-hide="phone,tablet,desktop">Responsable</th>
                                                        <th data-hide="phone,tablet,desktop">Descripción</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <header id="localizacionHeader"></header>
                                        </div>
                                        <div class="row">
                                            <section class="col col-3">
                                                <label class="label">Llamada</label>
                                                <select name="Llamada" id="llamada" class="select2" style="width: 100%;"></select>
                                            </section>
                                            <section class="col col-3">
                                                <label class="label">Fecha y hora <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <div class='input-group date' id='autorizaciondatetimepicker'>
                                                        <input type="text" name="Fecha y hora" id="fecha" class='form-control' placeholder="Fecha y hora de ingreso" data-requerido="true" />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Folio <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Folio" id="folio" placeholder="Folio" maxlength="50" class="alphanumeric alptext" data-requerido="true" disabled="disabled" />
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Motivo  </label>
                                                <select name="Motivo" id="motivoevento" class="select2" style="width: 100%;" >
                                               
                                                    </select>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-8">
                                                <label class="label">Descripción<a style="color: red">*</a></label>
                                                <label class="input">
                                                    <textarea id="descripcion" name="Descripción" placeholder="Descripción" maxlength="1000" class="form-control alptext" rows="6" data-requerido="true"></textarea>
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la descripción.</b>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Lugar detención <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Lugar detención" id="lugar" placeholder="Lugar detención" maxlength="1000" class="alphanumeric alptext" data-requerido="true" />
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el lugar de detención.</b>
                                                </label>
                                            </section>
                                            <section class="col col-4">
                                                <label class="label">Municipio <a style="color: red">*</a></label>
                                                <select name="Municipio" id="municipio" class="select2" style="width:100%;"></select>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-4">
                                                <label class="label">Colonia <a style="color: red">*</a></label>
                                                <select name="Colonia" id="colonia" data-requerido="true" class="select2" style="width:100%;"></select>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Código postal <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Código postal" id="codigoPostal" placeholder="Código Postal" maxlength="6" class="numeric" data-requerido="true" disabled="disabled" />
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Número detenidos <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Número detenidos" id="numeroDetenidos" placeholder="Número detenidos" maxlength="6" class="number" data-requerido="true" />
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el número de detenidos.</b>
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Latitud</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Latitud" id="latitud" placeholder="Latitud" maxlength="25" class="number" data-requerido="false" />
                                                    <b class="tooltip tooltip-bottom-right">Ingrese la latitud.</b>
                                                </label>
                                            </section>
                                            <section class="col col-2">
                                                <label class="label">Longitud</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="Longitud" id="longitud" placeholder="Longitud" maxlength="25" class="number" data-requerido="false" />
                                                    <b class="tooltip tooltip-bottom-right">Ingrese la longitud.</b>
                                                </label>
                                            </section>
                                        </div>
                                        <footer>
                                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                                <a style="float: none;" href="javascript:void(0);" class="btn btn-default save" id="save_" title="Guardar registro actual"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                                <a style="float: none;" href="events.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                            </div>
                                        </footer>
                                        <div class="row">
                                            <div class="col col-6">
                                                <div class="row">
                                                    <header>Detenidos</header>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col col-12">
                                                        <a href="javascript:void(0);" style="float: left; display: none; font-size: smaller" class="btn btn-default  btn-lg" title="Agregar detenidos" id="linkDetenidos"><i class="fa fa-plus"></i> Agregar detenido</a>
                                                    </div>
                                                </div>
                                                <br />
                                                <table id="dt_basic_tabla_detenidos" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th data-class="expand">Nombre(s)</th>
                                                            <th data-hide="phone,tablet">Apellido paterno</th>
                                                            <th data-hide="phone,tablet">Apellido materno</th>
                                                            <th data-hide="phone,tablet">Sexo</th>
                                                            <%-- <th data-hide="phone,tablet">Edad</th>--%>
                                                            <%--<th data-hide="phone,tablet">Motivo</th><th data-hide="phone,tablet">Edad</th>--%>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col col-6">
                                                <div class="row">
                                                    <header>Unidades involucradas</header>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col col-12">
                                                        <a style="float: left; display: none; font-size: smaller" class="btn btn-default  btn-lg" title="Agregar unidad" id="linkUnidad"><i class="fa fa-plus"></i> Agregar unidad</a>
                                                    </div>
                                                </div>
                                                <br />
                                                <table id="dt_basic_tabla_unidades"  class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th data-class="expand">Unidad</th>
                                                            <th data-hide="phone,tablet">Clave-responsable</th>
                                                            <th data-hide="phone,tablet">Corporación</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <header>Ubicación del evento en el mapa</header>
                                            <br />
                                        </div>
                                        <div id="mapid"></div>
                                        <pre id='coordinates' class='coordinates'></pre>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modalUnidad" data-keyboard="true"  role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-unidad-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label class="label" style="color: dodgerblue">Unidad <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>                                                
                                        <select name="unidad" id="unidad" style="width: 100%"  class="select2" data-requerido-unidad="true"></select>
                                    </label>
                                </section>
                                <section>
                                    <label class="label" style="color: dodgerblue">Clave-responsable <a style="color: red">*</a></label>
                                    <label class="input">
                                        <i class="icon-append fa fa-lock"></i>                                                
                                        <select name="responsable" id="responsable"  style="width: 100%" class="select2" data-requerido-unidad="true"></select>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <!--<a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnGuardarUnidad"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    <div class="modal fade" id="modalDetenido" data-backdrop="static" data-keyboard="true"   role="dialog" >
        <div class="modal-dialog "style="width: 160vh;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modal-detenido-title"></h4>
                </div>
                <div class="modal-body1">
                    <br />
                    <div id="register-detendio-form" class="smart-form">
                        <fieldset>
                            <div class="row" style="width:100%;margin:0 auto;">
                                 
                                    <section class="col col-3">
                                        <label class="label">Nombre(s)</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="nombre"  id="mdnombre" placeholder="Nombre(s)" maxlength="50" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el nombre.</b>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Apellido paterno</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="apellido_paterno"  id="mdpaterno" placeholder="Apellido paterno" maxlength="50" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el apellido paterno.</b>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Apellido materno</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="apellido_materno"  id="mdmaterno" placeholder="Apellido materno" maxlength="50" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el apellido materno.</b>
                                        </label>
                                    </section>
                                <section class="col col-3">
                                        <label class="label">Alias</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="alias"  id="mdalias" placeholder="Alias" maxlength="50" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Alias.</b>
                                        </label>
                                    </section>
                                <%-- <section class="col col-3" style="float:right;">
                                <a style="display: block;/* font-size:smaller; */font: 300 15px/29px 'Open Sans',Helvetica,Arial,sans-serif;" class="btn btn-primary" id="buscar" title="Volver al listado"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                     </section>--%>
                                <br />
                                <br />
                                <table id="dt_nuevo_detenido" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                    <thead>
                                        <tr><th></th>
                                            <th style="width:8%  !important; "></th>
                                            <th style="text-align:center;" >Nombre(s)</th>                                                
                                            <th style="text-align:center">Apellido paterno</th>                                                
                                            <th style="text-align:center">Apellido materno</th>
                                            <th style="text-align:center">Sexo</th>
                                            <!--<th style="text-align:center">Edad</th>-->
                                            <th style="text-align:center;width:470px " >Motivo</th>
                                            <th style="text-align:center">Acciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-md btn-primary" id="addRow"><i class="fa fa-plus"></i>&nbsp;Agregar nueva fila </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnGuardarDetenido"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelDetenido"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="evento-reciente-modal" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa  fa-folder"></i> Eventos recientes</h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-detenido" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section>
                                    <label style="color: dodgerblue">Evento <a style="color: red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="evento" disabled="disabled"  class="select2" style="width:100%; background-color:#eee;"></select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section>
                                    <label style="color: dodgerblue">Detenido <a style="color: red">*</a></label>
                                    <%--<label class="select">--%>
                                        <select name="rol" id="detenido" class="select2" style="width:100%;"></select>
                                        <i></i>
                                    <%--</label>--%>
                                </section>
                                <section>
                                    <label class="input" id="idsexo"></label>
                                </section>
                                <section style="display:none">
                                    <label class="input" id="idedad"></label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default saveDetenido" id="saveDetenido"><i class="fa fa-save"></i>&nbsp;Continuar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelevento"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditarDetenido"  role="dialog" data-keyboard="true" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="titleModalEditarDetenido"></h4>
                </div>
                <div class="modal-body1" >                    
                    <div id="formEditarDetenido" class="smart-form ">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="nav-item active">
                                <a class="nav-link" href="#tab1" data-toggle="tab">Registro del detenido</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab2" data-toggle="tab">Información general del detenido</a>
                            </li>      
                            <li class="nav-item">
                                <a class="nav-link" href="#tab3" data-toggle="tab">Domicilio del detenido</a>
                            </li>   
                            <li class="nav-item">
                                <a class="nav-link" href="#tab4" data-toggle="tab" onclick="reload()">Alias del detenido</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab5" data-toggle="tab" onclick="reload()">Tatuajes</a>
                            </li>
                        </ul>
                        <div class="tab-content padding-10">
                            <div style="margin-left:5px; margin-right:5px" class="tab-pane active" id="tab1">
                                <div class="row smart-form" >
                                    <fieldset>             
                                        <div class="row">
                                            <section class="col col-6">
                                                <label style="color:dodgerblue" class="label">Fecha y hora de ingreso <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <label class='input-group date' id='autorizaciondatetimepickerdet' style="width:100%">
                                                        <input  type="text" name="fecha" id="fechadet" class='input-group date alptext' disabled="disabled" placeholder="Fecha y hora de ingreso" disabled="disabled" />
                                                        <i class="icon-append fa fa-calendar"></i>                                            
                                                    </label>
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label style="color:dodgerblue" class="label">No. remisión</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-hashtag"></i>
                                                    <input  type="text" name="expediente" id="expediente" placeholder="No. de remisión" class="integer alptext" disabled="disabled" maxlength="13" disabled="disabled"/>
                                                </label>
                                            </section>  
                                            </div>
                                            <div class="row">
                                            <section class="col col-6">
                                                <label style="color:dodgerblue" class="label">Nombre(s) <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="nombreDetenido" id="nombreDetenido" disabled="disabled" placeholder="Nombre" maxlength="50" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el nombre.</b>
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label style="color:dodgerblue" class="label">Apellido paterno <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="paterno" id="paterno" disabled="disabled" placeholder="Apellido paterno" maxlength="50" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el apellido paterno.</b>
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label style="color:dodgerblue" class="label">Apellido materno <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-pencil"></i>
                                                    <input type="text" name="materno" id="materno" placeholder="Apellido materno" disabled="disabled" maxlength="50" class="alphanumeric alptext"/>
                                                    <b class="tooltip tooltip-bottom-right">Ingrese el apellido materno.</b>
                                                </label>
                                            </section>
                                            <section  class="col col-6">
                                                <label style="color:dodgerblue" class="label">Fotografía detenido </label>
                                                <label class="input" >
                                                    <i class="icon-append fa fa-file-image-o"></i>
                                                    <asp:FileUpload id="imagenDetenido" runat="server" disabled="disabled" />
                                                    <input type="text" id="imagenDetenidoOriginal" hidden="hidden" style="display:none;" />
                                                    <input type="text" id="idDetenido" hidden="hidden" style="display:none;" />
                                                    <input type="text" id="trackingDetenido" hidden="hidden" style="display:none;" />
                                                </label>
                                            </section>
                                            <section class="col col-6">
										        <label for="Lesion_Visible">Lesiones visibles a simple vista </label> <input name="Lesion_visible" id="Lesion_visible" type="checkbox" value="false" disabled="disabled"> 
                                            </section>
                                            <section class="col col-4"></section>
                                            <section class="col col-4" align="center">
                                                <img id="detenidoEditar" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>  source" width="70" height="70" />
                                            </section>
                                            <section class="col col-4"></section>
                                        </div>
                                    </fieldset>
                                    <footer>
                                        <a href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelDetenido"><i class="fa fa-close"></i>&nbsp;Cerrar </a>                            
                                    </footer>
                                </div>
                            </div>     
                            <div style="margin-left:5px; margin-right:5px" class="tab-pane" id="tab2">
                                <div class="row smart-form" >
                                    <fieldset disabled="disabled">                                                                        
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Fecha de nacimiento <a style="color: red">*</a></label>
                                            <label class="input">
                                                <label class='input-group date'>
                                                    <input type="text" name="fecha" id="Text1" placeholder="Fecha de nacimiento" class="form-control datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                    <b class="tooltip tooltip-bottom-right">Ingresa la fecha de nacimiento.</b>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </label>       
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Nacionalidad <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="nacionalidad" runat="server" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">RFC</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="rfc" id="rfc" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" runat="server" placeholder="RFC" maxlength="13" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa el RFC.</b>
                                            </label>
                                            <input type="hidden" id="trackingidInterno" runat="server" />
                                            <input type="hidden" id="trackingGeneral" runat="server" />
                                            <input type="hidden" id="idGeneral" runat="server" />
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">CURP</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="curp" id="CURP" runat="server" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="CURP" maxlength="18" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la CURP.</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Persona a quien se notifica</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-edit"></i>
                                                <input type="text" name="personanotifica" id="personanotifica"  placeholder="Persona a quien se notifica" maxlength="150" class="alphanumeric alptext" />
                                                <b class="tooltip tooltip-bottom-right">Ingresa la persona a quien se notifica.</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Celular </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-phone"></i>
                                                <input  name="celular" id="celular" maxlength="10" type="tel" placeholder="Celular" data-mask="(999) 999-9999"/>
                                                <b class="tooltip tooltip-bottom-right">Ingresa el celular.</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Escolaridad <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="escolaridad" runat="server" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Religión</label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="religion" runat="server" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Ocupación <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="ocupacion" runat="server" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Estado civil <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="civil" runat="server" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-3">
                                            <label style="color:dodgerblue" class="label">Etnia </label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="etinia" runat="server" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>
                                        <section class="col col-3">
                                            <label style="color:dodgerblue" class="label">Sexo <a style="color: red">*</a></label>
                                            <%--<label class="select">--%>
                                                <select name="centro" id="sexo" runat="server" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section> 
                                        <section class="col col-6">
                                            <label style="color:dodgerblue" class="label">Lengua nativa </label>
                                            <%--<label class="select">--%>
                                                <select name="lenguanativa" id="lenguanativa" class="select2" style="width:100%;"></select>
                                                <i></i>
                                            <%--</label>--%>
                                        </section>     
                                    </fieldset>
                                    <footer>
                                        <a href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelGeneral"><i class="fa fa-close"></i>&nbsp;Cerrar </a>                            
                                    </footer>
                                </div>
                            </div>
                            <div style="margin-left:5px; margin-right:5px" class="tab-pane" id="tab3">
                                <div class="row smart-form">
                                    <div class="row"></div>
                                    <br />
                                    <legend>Domicilio actual</legend>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <br />
                                                <div class="smart-form">
                                                    <fieldset disabled="disabled">
                                                        <legend class="hide">&nbsp;</legend>
                                                        <div class="row">
                                                            <section class="col col-6">
                                                                <label style="color:dodgerblue" class="label">País <a style="color: red">*</a></label>
                                                                <%--<label class="select">--%>
                                                                    <select name="centro" id="paisdomicilio" runat="server" class="select2" style="width:100%;"></select>
                                                                    <i></i>
                                                                <%--</label>--%>
                                                            </section>
                                                            <section class="col col-6" id="sectionestado">
                                                                <label style="color:dodgerblue" class="label">Estado <a style="color: red">*</a></label>
                                                                <%--<label class="select">--%>
                                                                    <select name="centro" id="estadodomicilio" runat="server" class="select2" style="width:100%;"></select>
                                                                    <i></i>
                                                                <%--</label>--%>
                                                            </section>
                                                            <section class="col col-6" id="sectionmunicipio">
                                                                <label style="color:dodgerblue" class="label">Municipio <a style="color: red">*</a></label>
                                                                <%--<label class="select">--%>
                                                                    <select name="centro" id="municipiodomicilio" runat="server" class="select2" style="width:100%;"></select>
                                                                    <i></i>
                                                                <%--</label>--%>
                                                            </section>
                                                            <section class="col col-6" id="sectioncoloniaDomicilio">
                                                                <label style="color:dodgerblue" class="label">Colonia <a style="color: red">*</a></label>
                                                                <%--<label class="select">--%>
                                                                    <select name="centro" id="coloniaSelect" runat="server" class="select2" style="width:100%;"></select>
                                                                    <i></i>
                                                                <%--</label>--%>
                                                            </section>
                                                            <section class="col col-5">
                                                                <label style="color:dodgerblue" class="label">Calle <a style="color: red">*</a></label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-street-view"></i>
                                                                    <input type="text" name="calle" id="calledomicilio" runat="server" placeholder="Calle" maxlength="250" class="alphanumeric alptext" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa la calle.</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-2">
                                                                <label style="color:dodgerblue" class="label">Número <a style="color: red">*</a></label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                    <input type="text" name="numero" id="numerodomicilio" runat="server" placeholder="Número" class="alphanumeric alptext" maxlength="15" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el número.</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-2" id="sectioncpdomicilio">
                                                                <label style="color:dodgerblue" class="label">C.P. <a style="color: red">*</a></label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                    <input type="text" name="cp" id="cpdomicilio" runat="server" placeholder="C.P" maxlength="5" class="integer alptext" disabled="disabled" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el C.P.</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label style="color:dodgerblue" class="label">Teléfono <a style="color: red">*</a></label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-phone"></i>
                                                                    <input type="tel" name="telefono" id="telefonodomicilio" runat="server" placeholder="Teléfono" data-mask="(999) 999-9999" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el teléfono .</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-6" id="sectionlocalidad">
                                                                <label style="color:dodgerblue" class="label">Localidad <a style="color: red">*</a></label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-street-view"></i>
                                                                    <input type="text" name="colonialugar" id="localidad" hidden="hidden" runat="server" placeholder="Localidad" maxlength="250" class="alphanumeric alptext" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa localidad.</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label style="color:dodgerblue" class="label">Latitud</label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-map-marker"></i>
                                                                    <input type="number" step="any" name="latitud" id="latitud" class="alptext"/>
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa la latitud.</b>
                                                                </label>                                                                    
                                                            </section>
                                                            <section class="col col-3">
                                                                <label style="color:dodgerblue" class="label">Longitud</label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-map-marker"></i>
                                                                    <input type="number" step="any" name="longitud" id="longitud" class="alptext"/>
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa la longitud.</b>
                                                                </label>                                                                    
                                                            </section>                                                                
                                                        </div>
                                                        <footer>
                                                            <a id="showMap" class="btn btn-default" title="Volver al listado"><i class="fa fa-map-marker"></i>&nbsp;Ver Mapa </a>                                                                   
                                                        </footer>
                                                    </fieldset>
                                                </div>
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <legend>Domicilio de nacimiento</legend>
                                                <br />
                                                <div id="Div3" class="smart-form">
                                                    <fieldset disabled="disabled">
                                                        <legend class="hide">&nbsp;</legend>
                                                        <div class="row">
                                                            <section class="col col-6">
                                                                <label style="color:dodgerblue" class="label">País  </label>
                                                                <%-- <label class="select">--%>
                                                                    <select name="centro" id="paisnacimineto" runat="server" class="select2" style="width:100%;"></select>
                                                                    <i></i>
                                                                <%--</label>--%>
                                                            </section>
                                                            <section class="col col-6" id="sectionestadon">
                                                                <label style="color:dodgerblue"  class="label">Estado  </label>
                                                                    <select name="centro" id="estadonacimiento" runat="server" class="select2" style="width:100%;"></select>
                                                                    <i></i>
                                                            </section>
                                                            <section class="col col-6" id="sectionmunicipion">
                                                                <label style="color:dodgerblue" class="label">Municipio  </label>
                                                                    <select name="centro" id="municipionacimiento" runat="server" class="select2" style="width:100%;"></select>
                                                                    <i></i>
                                                            </section>
                                                            <section class="col col-6" id="sectioncoloniaNacimiento">
                                                                <label style="color:dodgerblue" class="label">Colonia  </label>
                                                                    <select name="centro" id="coloniaSelectNacimiento" runat="server" class="select2" style="width:100%;"></select>
                                                                    <i></i>
                                                            </section>
                                                            <section class="col col-5">
                                                                <label  style="color:dodgerblue" class="label">Calle  </label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-street-view"></i>
                                                                    <input type="text" name="callelugar" id="callelugar" runat="server" placeholder="Calle" maxlength="250" class="alphanumeric alptext" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa la calle.</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-2">
                                                                <label style="color:dodgerblue" class="label">Número  </label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                    <input type="text" name="numerolugar" id="numerolugar" runat="server" placeholder="Número" maxlength="15" class="alphanumeric alptext" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el número.</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-2" id="sectioncpnacimiento">
                                                                <label style="color:dodgerblue" class="label">C.P.  </label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                                    <input type="text" name="cplugar" id="cplugar" runat="server" placeholder="C.P" maxlength="5" class="integer alptext" disabled="disabled" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el C.P.</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label style="color:dodgerblue" class="label">Teléfono  </label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-phone"></i>
                                                                    <input type="tel" name="telefonolugar" id="telefonolugar" runat="server" placeholder="Teléfono" data-mask="(999) 999-9999" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa el teléfono .</b>
                                                                </label>
                                                            </section>
                                                            <section class="col col-6" id="sectionlocalidadn">
                                                                <label style="color:dodgerblue" class="label">Localidad  </label>
                                                                <label class="input">
                                                                    <i class="icon-append fa fa-street-view"></i>
                                                                    <input type="text" name="colonialugar" id="localidadn" hidden="hidden" runat="server" placeholder="Localidad" maxlength="250" class="alphanumeric" />
                                                                    <b class="tooltip tooltip-bottom-right">Ingresa localidad.</b>
                                                                </label>
                                                            </section>                                                                
                                                        </div>
                                                    </fieldset>
                                                    <footer>                                                                                   
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelDomicilio"><i class="fa fa-close"></i>&nbsp;Cerrar </a>                            
                                                    </footer>
                                                    <input type="hidden" id="trackingDomicilio" runat="server" />
                                                    <input type="hidden" id="idDomicilio" runat="server" />
                                                    <input type="hidden" id="trackingNacimiento" runat="server" />
                                                    <input type="hidden" id="idNacimiento" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-left:5px; margin-right:5px" class="tab-pane" id="tab4" >                                
                                <div class="row smart-form"></div>
                                <br />
                                <legend>Alias / Sobrenombre</legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <fieldset disabled="disabled">                                            
                                            <br />
                                            <br />
                                            <table id="dt_basicAlias" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th data-class="expand">#</th>
                                                        <th>Alias</th>
                                                        <th data-hide="phone,tablet">Acciones</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-left:5px; margin-right:5px" class="tab-pane" id="tab5" >                                
                                <div class="row smart-form"></div>
                                <br />
                                <legend>Tatuajes</legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <fieldset disabled="disabled">                                            
                                                <br />
                                                <br />
                                                <table id="dt_basic_senias" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th data-class="expand">#</th>
                                                            <th >Tipo</th>
                                                            <th data-hide="phone,tablet">Descripción</th>
                                                            <th data-hide="phone,tablet">Cantidad</th>
                                                            <%--<th data-hide="phone,tablet">Acciones</th>--%>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addAlias-modal" data-keyboard="true"  role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="modal_title_alias"></h4>
                </div>
                <div class="modal-body">
                    <div id="addAlia-modal" class="smart-form">
                        <div class="modal-body">
                            <div id="Div5" class="smart-form">
                                <fieldset>
                                    <section>
                                        <div class="row">
                                            <div>
                                                <label style="color:dodgerblue" class="input">Alias/sobrenombre <a style="color: red">*</a></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-user"></i>
                                                    <input type="text" name="alias" runat="server" id="Text2" class="alphanumeric alptext" placeholder="Alias" maxlength="246" title="Alias" />
                                                    <b class="tooltip tooltip-bottom-right">Alias</b>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
                            </div>
                        </div>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal" id="btncancelAlias"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                                
                            <a class="btn btn-sm btn-default" id="btnsaveAlias"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="blockitem-modalalias" class="modal fade"  data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación</h4>
                </div>
                <div class="modal-body">
                    <div id="Div9" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbalias"></span></strong>
                                    &nbsp;el registro de  
                                    <strong>&nbsp<span id="itemnameblockalias"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>&nbsp;
                            <a class="btn btn-sm btn-default" id="btncontinuaralias"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="map"></div>
    <input type="hidden" id="estado" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <input type="hidden" id="L1" />
    <input type="hidden" id="L2" />
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">

//        (function($, window) {
//    'use strict';

//    var MultiModal = function(element) {
//        this.$element = $(element);
//        this.modalCount = 0;
//    };

//    MultiModal.BASE_ZINDEX = 1040;

//    MultiModal.prototype.show = function(target) {
//        var that = this;
//        var $target = $(target);
//        var modalIndex = that.modalCount++;

//        $target.css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20) + 10);

//        // Bootstrap triggers the show event at the beginning of the show function and before
//        // the modal backdrop element has been created. The timeout here allows the modal
//        // show function to complete, after which the modal backdrop will have been created
//        // and appended to the DOM.
//        window.setTimeout(function() {
//            // we only want one backdrop; hide any extras
//            if(modalIndex > 0)
//                $('.modal-backdrop').not(':first').addClass('hidden');

//            that.adjustBackdrop();
//        });
//    };

//    MultiModal.prototype.hidden = function(target) {
//        this.modalCount--;

//        if(this.modalCount) {
//           this.adjustBackdrop();

//            // bootstrap removes the modal-open class when a modal is closed; add it back
//            $('body').addClass('modal-open');
//        }
//    };

//    MultiModal.prototype.adjustBackdrop = function() {
//        var modalIndex = this.modalCount - 1;
//        $('.modal-backdrop:first').css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20));
//    };

//    function Plugin(method, target) {
//        return this.each(function() {
//            var $this = $(this);
//            var data = $this.data('multi-modal-plugin');

//            if(!data)
//                $this.data('multi-modal-plugin', (data = new MultiModal(this)));

//            if(method)
//                data[method](target);
//        });
//    }

//    $.fn.multiModal = Plugin;
//    $.fn.multiModal.Constructor = MultiModal;

//    $(document).on('show.bs.modal', function(e) {
//        $(document).multiModal('show', e.target);
//    });

//    $(document).on('hidden.bs.modal', function(e) {
//        $(document).multiModal('hidden', e.target);
//    });
//}(jQuery, window));

        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 37) {
                e.preventDefault();
                if ($("#evento-reciente-modal").is(":visible")) {
                    document.getElementById("saveDetenido").click();
                }
                else if ($("#modalUnidad").is(":visible")) {
                    document.getElementById("btnGuardarUnidad").click();
                }
                else if ($("#modalDetenido").is(":visible")) {
                    document.getElementById("btnGuardarDetenido").click();
                    
                }
            }
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#evento-reciente-modal").is(":visible")) {
                    document.querySelector("#saveDetenido").click();
                }
                else if ($("#modalDetenido").is(":visible")) {
                    document.querySelector("#btnGuardarDetenido").click();
                }
                else if ($("#modalUnidad").is(":visible")) {
                    document.querySelector("#btnGuardarUnidad").click();
                }
                else {
                    document.querySelector("#save_").click();
                }
            }

            //else if (e.keyCode === 27) {
            //    if ($("#modalUnidad").is(":visible")) {
            //        $("#modalUnidad").modal("hide");
            //    }
            //    else if ($("#evento-reciente-modal").is(":visible")) {
            //        $("#evento-reciente-modal").modal("hide");
            //    }
            //}
        });

        $(document).ready(function () {

        CargarLenguaNativa("0");
        function CargarEtnia(setetnia) {
            $('#ctl00_contenido_etinia').empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getEtnia",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                "scrollY": "100%",
                "scrollX": "0%",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_etinia');

                    Dropdown.append(new Option("[Etnia]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setetnia != "") {
                        Dropdown.val(setetnia);
                        Dropdown.trigger("change");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de etnias. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarEstadoCivil(set) {
            $('#ctl00_contenido_civil').empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getEstadoCivil",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_civil');

                    Dropdown.append(new Option("[Estado Civil]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (set != "") {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de estado civil. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarOcupacion(set) {
            $('#ctl00_contenido_ocupacion').empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getOcupacion",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_ocupacion');

                    Dropdown.append(new Option("[Ocupación]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (set != "") {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de ocupaciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }
            GetLocation();
        function CargarCodigoPostalDomicilio(idColonia) {
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getZipCode",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                "scrollY": "100%",
                "scrollX": "0%",
                cache: false,
                data: JSON.stringify({
                    idColonia: idColonia
                }),
                success: function (response) {
                    var resultado = JSON.parse(response.d);
                    $("#ctl00_contenido_cpdomicilio").val(resultado.cp);
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
            }

            function GetLocation() {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getpositiobycontract",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    "scrollY": "100%",
                    "scrollX": "0%",
                    cache: false,
                    data: JSON.stringify({ LlamadaId: "" }),
                    success: function (response) {
                        response = JSON.parse(response.d);

                        if (response.exitoso) {
                            $("#L1").val(response.obj.Latitud);
                            $("#L2").val(response.obj.Longitud);
                            
                            CargarMapa();
                        } else {

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            
        function CargarCodigoPostalNacimiento(idColonia) {
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getZipCode",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                "scrollY": "100%",
                "scrollX": "0%",
                cache: false,
                data: JSON.stringify({
                    idColonia: idColonia
                }),
                success: function (response) {
                    var resultado = JSON.parse(response.d);
                    $("#ctl00_contenido_cplugar").val(resultado.cp);
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        $(function () {
            $('#myTab a').click(function (e) {
                e.preventDefault()
                $(this).tab('show');
            });
        });

        function CargarEstado(setestado, combo, paisid) {
            $(combo).empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getEstado",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                "scrollY": "100%",
                "scrollX": "0%",
                data: JSON.stringify({ paisId: 73 }),
                cache: false,
                success: function (response) {
                    var Dropdown = $(combo);

                    Dropdown.append(new Option("[Estado]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setestado != "" && setestado != null) {
                        Dropdown.val(setestado);
                        Dropdown.trigger("change.select2");
                    }
                    else {
                        Dropdown.val(0);
                        Dropdown.trigger('change');
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de estados. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarMunicipio(set, combo, estadoid) {
            $(combo).empty();
            $.ajax({

                type: "POST",
                url: "eventsform.aspx/getMunicipio",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ estadoId: estadoid }),
                dataType: "json",
                "scrollY": "100%",
                "scrollX": "0%",
                cache: false,
                success: function (response) {
                    var Dropdown = $(combo);

                    Dropdown.append(new Option("[Municipio]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (set != "" && set != null) {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                    else {
                        Dropdown.val(0);
                        Dropdown.trigger('change');
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de municipio. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }
            
        function CargarPais(set, combo) {
            $(combo).empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getPais",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                "scrollY": "100%",
                "scrollX": "0%",
                cache: false,
                success: function (response) {
                    var Dropdown = $(combo);

                    Dropdown.append(new Option("[País]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (set != "" && set != null) {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                    else {
                        Dropdown.val(0);
                        Dropdown.trigger('change');
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarColonia(set, idMunicipio) {
            console.log(set + " " + idMunicipio);
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getNeighborhoods",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                data: JSON.stringify({
                    idMunicipio: idMunicipio
                }),
                success: function (response) {
                    var Dropdown = $("#ctl00_contenido_coloniaSelect");
                    Dropdown.empty();
                    Dropdown.append(new Option("[Colonia]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (set != "" && set != null) {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                    else {
                        Dropdown.val(0);
                        Dropdown.trigger('change');
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarColoniaNacimiento(set, idMunicipio) {
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getNeighborhoods",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                data: JSON.stringify({
                    idMunicipio: idMunicipio
                }),
                success: function (response) {
                    var Dropdown = $("#ctl00_contenido_coloniaSelectNacimiento");
                    Dropdown.empty();
                    Dropdown.append(new Option("[Colonia]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (set != "" && set != null) {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                    else {
                        Dropdown.val(0);
                        Dropdown.trigger('change');
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarReligion(set) {
            $('#ctl00_contenido_religion').empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getReligion",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_religion');

                    Dropdown.append(new Option("[Religión]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (set != "") {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de religiones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarEscolaridad(set) {
            $('#ctl00_contenido_escolaridad').empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getEscolaridad",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_escolaridad');

                    Dropdown.append(new Option("[Escolaridad]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });
                    if (set != "") {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de escolaridad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarLenguaNativa(setlengua) {
            $('#lenguanativa').empty();
            $.ajax({

                type: "POST",
                url: "eventsform.aspx/getlenguanativa",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#lenguanativa');

                    Dropdown.append(new Option("[Lengua nativa]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setlengua != "") {
                        Dropdown.val(setlengua);
                        Dropdown.trigger("change.select2");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de lengua nativa. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function CargarNacionalidad(set) {
            $('#ctl00_contenido_nacionalidad').empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getNacionalidad",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_nacionalidad');

                    Dropdown.append(new Option("[Nacionalidad]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (set != "") {
                        Dropdown.val(set);
                        Dropdown.trigger("change.select2");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de nacionalidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }
       
        $("body").on("click", "#datos", function () {
            if ($(this).attr("data-DetalledetencionId") == "0") {
                return;
            }
            clearModalDetenido();
            cargarDatosDetenido($(this).attr("data-DetalledetencionId"));
            responsiveHelper_dt_basicAlias = undefined;
            $("dt_basicAlias").DataTable().destroy();
            loadAlias($(this).attr("data-DetalledetencionId"));
        });

          //"lengthMenu": [10, 20, 50, 100],
          //      iDisplayLength: 10,
          //      serverSide: true,
          //      fixedColumns: true,
          //          autoWidth: true,
          //          destroy: true,
          //      "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
          //      "t" +
          //      "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
          //      "autoWidth": true,
          //      "oLanguage": {
          //          "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
          //      },

        var responsiveHelper_dt_basic_senias = undefined;

        function Cargarsenias(id) {
            window.table = $('#dt_basic_senias').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                destroy: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_senias) {
                        responsiveHelper_dt_basic_senias = new ResponsiveDatatablesHelper($('#dt_basic_senias'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_senias.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_senias.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');
                        $('td', row).eq(3).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "eventsform.aspx/getSenal",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                       

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.id = id;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Descripcion",
                        data: "Descripcion"
                    },
                    {
                        name: "Cantidad",
                        data: "Cantidad"
                    }
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                ]
            });
        }

        function cargarDatosDetenido(id) {
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/cargarDatosInterno",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                "scrollY": "100%",
                "scrollX": "0%",
                cache: false,
                data: JSON.stringify({ id: id }),
                success: function (data) {
                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {
                        $("#nombreDetenido").val(resultado.obj.Nombre);
                        $("#paterno").val(resultado.obj.Paterno);
                        $("#materno").val(resultado.obj.Materno);
                        $("#fechadet").val(resultado.obj.Fecha);
                        $("#expediente").val(resultado.obj.Expediente);
                        $("#idDetenido").val(resultado.obj.Id);
                        $("#trackingDetenido").val(resultado.obj.TrackingId);
                        $("#imagenDetenidoOriginal").val(resultado.obj.Foto);
                        $('#ctl00_contenido_Text1').datetimepicker({
                            ampm: true,
                            format: 'DD/MM/YYYY'
                        });
                        Cargarsenias(id);
                        $("#modalEditarDetenido").modal("show");
                        $("#titleModalEditarDetenido").html('<i class="fa fa-pencil" +=""></i> Ver información');

                        if (resultado.obj.Lesion_visible == "True") {

                            $("#Lesion_visible").prop('checked', true);
                        }
                        else {

                            $("#Lesion_visible").prop('checked', false);
                        }

                        if (resultado.obj.Foto != "" && resultado.obj.Foto != null) {
                            var ext = "." + resultado.obj.Foto.split('.').pop();
                            var photo = resultado.obj.Foto.replace(ext, ".thumb");
                            var imgAvatar = resolveUrl(photo);
                            $("#detenidoEditar").attr("src", imgAvatar);
                        } else {
                            pathfoto = resolveUrl(rutaDefaultServer);
                            $("#detenidoEditar").attr("src", pathfoto);
                        }
                        var lengua = resultado.obj.Lenguanativa;

                        var fechaNacimiento = resultado.obj.FechaNacimiento;
                        if (fechaNacimiento != "") {
                            $('#ctl00_contenido_Text1').val(fechaNacimiento).trigger("change");
                        }
                        else {
                            $('#ctl00_contenido_Text1').val(fechaNacimiento);
                        }

                        $("#ctl00_contenido_rfc").val(resultado.obj.RFC);
                        $("#ctl00_contenido_CURP").val(resultado.obj.CURP);
                        $("#personanotifica").val(resultado.obj.Personanotifica);
                        $("#celular").val(resultado.obj.Celularnotifica);
                        $("#lenguanativa").val(resultado.obj.Lenguanativa);
                        $('#ctl00_contenido_trackingidInterno').val(resultado.obj.TrackingId);
                        $('#ctl00_contenido_idGeneral').val(resultado.obj.IdGeneral);
                        $('#ctl00_contenido_trackingGeneral').val(resultado.obj.TrackingIdGeneral),
                            $("#latitud").val(resultado.obj.LatitudDomicilio);
                        $("#longitud").val(resultado.obj.LongitudDomicilio);
                        CargarLenguaNativa(lengua);
                        CargarNacionalidad(resultado.obj.NacionalidadId);
                        CargarEscolaridad(resultado.obj.EscolaridadId);
                        CargarReligion(resultado.obj.ReligionId);
                        CargarSexo2(resultado.obj.SexoId);
                        CargarEstadoCivil(resultado.obj.EstadoCivilId);
                        CargarOcupacion(resultado.obj.OcupacionId);
                        CargarEtnia(resultado.obj.EtniaId);
                        //Domicilio

                        if (resultado.obj.TieneDomicilio) {
                            if (resultado.obj.PaisDomicilio == 73) {
                                $('#sectionlocalidad').hide();
                                $('#sectionmunicipio').show();
                                $('#sectionestado').show();
                                $('#sectioncoloniaDomicilio').show();
                                $('#sectioncpdomicilio').show();
                                CargarPais(resultado.obj.PaisDomicilio, "#ctl00_contenido_paisdomicilio");
                                CargarEstado(resultado.obj.EstadoDomicilio, "#ctl00_contenido_estadodomicilio", resultado.obj.PaisDomicilio);
                                CargarMunicipio(resultado.obj.MunicipioDomicilio, "#ctl00_contenido_municipiodomicilio", resultado.obj.EstadoDomicilio);
                                CargarColonia(resultado.obj.ColoniaIdDomicilio, resultado.obj.MunicipioDomicilio);
                                CargarCodigoPostalDomicilio(resultado.obj.ColoniaIdDomicilio);

                                $("#ctl00_contenido_calledomicilio").val(resultado.obj.CalleDomicilio);
                                $("#ctl00_contenido_numerodomicilio").val(resultado.obj.NumeroDomicilio);
                                $("#ctl00_contenido_telefonodomicilio").val(resultado.obj.TelefonoDomicilio);
                                $("#latitud").val(resultado.obj.LatitudDomicilio);
                                $("#longitud").val(resultado.obj.LongitudDomicilio);
                            }
                            else {
                                $('#sectionlocalidad').show();
                                $('#sectionmunicipio').hide();
                                $('#sectionestado').hide();
                                $('#sectioncoloniaDomicilio').hide();
                                $('#sectioncpdomicilio').hide();
                                $('#ctl00_contenido_localidad').val(resultado.domicilio.LocalidadDomicilio);
                            }

                            $('#<%= trackingDomicilio.ClientID %>').val(resultado.obj.TrackingIdDomicilio);
                            $('#<%= idDomicilio.ClientID %>').val(resultado.obj.IdDomicilio);
                        }
                        else {
                            //CargarPais(73, '#ctl00_contenido_paisdomicilio');
                            //CargarEstado(0, '#ctl00_contenido_estadodomicilio', 73);
                            CargarPais(resultado.obj.PaisDomicilio, "#ctl00_contenido_paisdomicilio");
                            CargarEstado(resultado.obj.EstadoDomicilio, "#ctl00_contenido_estadodomicilio", resultado.obj.PaisDomicilio);
                            CargarMunicipio(resultado.obj.MunicipioDomicilio, "#ctl00_contenido_municipiodomicilio", resultado.obj.EstadoDomicilio);
                            CargarColonia(resultado.obj.ColoniaIdDomicilio, resultado.obj.MunicipioDomicilio);
                            $('#<%= trackingDomicilio.ClientID %>').val("");
                            $('#<%= idDomicilio.ClientID %>').val("");
                            $('#sectionlocalidadn').hide();
                        }

                        if (resultado.obj.TieneNacimiento) {
                            if (resultado.obj.PaisNacimiento == 73) {
                                $('#sectionlocalidadn').hide();
                                $('#sectionmunicipion').show();
                                $('#sectionestadon').show();
                                $('#sectioncoloniaNacimiento').show();
                                $('#sectioncpnacimiento').show();
                                CargarPais(resultado.obj.PaisNacimiento, "#ctl00_contenido_paisnacimineto");
                                CargarEstado(resultado.obj.EstadoNacimiento, "#ctl00_contenido_estadonacimiento", resultado.obj.PaisNacimiento);
                                CargarMunicipio(resultado.obj.MunicipioNacimiento, "#ctl00_contenido_municipionacimiento", resultado.obj.EstadoNacimiento);
                                CargarColoniaNacimiento(resultado.obj.ColoniaIdNacimiento, resultado.obj.MunicipioNacimiento);
                                CargarCodigoPostalNacimiento(resultado.obj.ColoniaIdNacimiento);
                                $("#ctl00_contenido_callelugar").val(resultado.obj.CalleNacimiento);
                                $("#ctl00_contenido_numerolugar").val(resultado.obj.NumeroNacimiento);
                                $("#ctl00_contenido_telefonolugar").val(resultado.obj.TelefonoNacimiento);
                            }
                            else {
                                $('#sectionlocalidadn').show();
                                $('#sectionmunicipion').hide();
                                $('#sectionestadon').hide();
                                $('#sectioncoloniaNacimiento').hide();
                                $('#sectioncpnacimiento').hide();
                                $('#ctl00_contenido_localidadn').val(resultado.nacimiento.LocalidadNacimiento);
                            }

                            $('#<%= trackingNacimiento.ClientID %>').val(resultado.obj.TrackingIdNacimiento);
                            $('#<%= idNacimiento.ClientID %>').val(resultado.obj.IdNacimiento);
                        }
                        else {
                            CargarPais(73, '#ctl00_contenido_paisnacimineto');
                            CargarEstado(0, '#ctl00_contenido_estadonacimiento', 73);
                            $('#domicilioInterno').text("Domicilio no registrado");
                            $('#<%= trackingNacimiento.ClientID %>').val("");
                            $('#<%= idNacimiento.ClientID %>').val("");
                            $('#sectionlocalidad').hide();
                        }
                    }
                    else {
                        $('#main').waitMe('hide');
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                            "Algo salió mal. " + resultado.mensaje + "</div>");
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado);
                    }
                },
                error: function (error) {
                    $('#main').waitMe('hide');
                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                        "Algo salió mal. " + error + "</div>");
                    ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + error);
                }
            });
        }
        
        var rutaDefaultServer = "";

        getRutaDefaultServer();

        function getRutaDefaultServer() {
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getRutaServer",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    var resultado = JSON.parse(data.d);
                    if (resultado.exitoso) {
                        rutaDefaultServer = resultado.rutaDefault;
                    }
                }
            });
        }

        function resolveUrl(url) {
            var baseUrl = "<%= ResolveUrl("~/") %>";
            if (url.indexOf("~/") == 0) {
                url = baseUrl + url.substring(2);
            }
            return url;
        }

        var responsiveHelper_dt_basicAlias = undefined;
        function loadAlias(id) {
            $('#dt_basicAlias').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                destroy: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basicAlias) {
                        responsiveHelper_dt_basicAlias = new ResponsiveDatatablesHelper($('#dt_basicAlias'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basicAlias.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basicAlias.respond();
                    $('#dt_basicAlias').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Activo"]) {
                        $('td', row).eq(1).addClass('strikeout');
                    }
                },
                ajax: {
                    type: "POST",
                    url: "eventsform.aspx/getAlias",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basicAlias').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.id = id;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Alias",
                        data: "Alias"
                    },
                    null
                ],
                columnDefs: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "editAlias";
                            var editar = "";
                            var habilitar = "";

                            if (row.Activo) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a style="padding-left: 8px;" class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-alias = "' + row.Alias + '" data-tracking="' + row.TrackingId + '"  title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitemalias" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-value = "' + row.Alias + '" data-verb = "' + txtestatus + '" title="' + txtestatus + '" style="padding-left: 8px; "><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return editar + habilitar;
                        }
                    }
                ]
            });
        }

        function clearModalDetenido() {
            $(".input").removeClass('state-success');
            $(".input").removeClass('state-error');
            $(".input").removeClass('valid');
            $(".select").removeClass('state-success');
            $(".select").removeClass('state-error');
            $(".select").removeClass('valid');
            $("#ctl00_contenido_imagenDetenido").val("");
        }

        $("body").on("click", ".ed", function () {

        });

        $("body").on("click", ".editarDetenido", function () {
            clearModalDetenido();
            cargarDatosDetenido($(this).attr("data-DetalledetencionId"));
            responsiveHelper_dt_basicAlias = undefined;
            $("dt_basicAlias").DataTable().destroy();
            loadAlias($(this).attr("data-DetalledetencionId"));
        });

        var maps, infowindow;
        var responsiveHelper_dt_nuevo_detenido = undefined;

        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        function obtenercirteriosbusqueda() {
            var filtroconsulta = {
                nombre: $("#mdnombre").val(),
                apellidopaterno: $("md#paterno").val(),
                apellidomaterno: $("#mdmaterno").val(),
                alias: $("#mdalias").val()
            }
            return filtroconsulta;
        }

        var dataSexo = null;
        var datosSexo = [];
        function initMap() {

            maps = new google.maps.Map(document.getElementById('map'), {
                center: { lat: 25.4653403, lng: 0 },
                zoom: 15
            });
            infoWindow = new google.maps.InfoWindow;
            var lat = 25.4653403;
            var lon = -101.0320401;
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getpositiobycontract",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                processdata: true,
                traditional: true,
                data: JSON.stringify({ LlamadaId: "" }),
                cache: false,
                success: function (response) {
                    response = JSON.parse(response.d);

                    if (response.exitoso) {
                        $("#L1").val(response.obj.Latitud);
                        $("#L2").val(response.obj.Longitud);

                        CargarMapa()
                    } else {

                    }
                },
                error: function () {
                    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                }
            });
        }
        var dataMotivo = null;
        var datosMotivo = [];

        $("#addRow").click(function () {
            addrow();
      
        });

        $("#ctl00_contenido_nacionalidad").select2();
        $("#ctl00_contenido_escolaridad").select2();
        $("#ctl00_contenido_religion").select2();
        $("#ctl00_contenido_ocupacion").select2();
        $("#ctl00_contenido_civil").select2();
        $("#ctl00_contenido_etinia").select2();
        $("#ctl00_contenido_sexo").select2();
        $("#lenguanativa").select2();
        $("#ctl00_contenido_paisdomicilio").select2();
        $("#ctl00_contenido_estadodomicilio").select2();
        $("#ctl00_contenido_municipiodomicilio").select2();
        $("#ctl00_contenido_coloniaSelect").select2();
        $("#ctl00_contenido_paisnacimineto").select2();
        $("#ctl00_contenido_estadonacimiento").select2();
        $("#ctl00_contenido_municipionacimiento").select2();
        $("#ctl00_contenido_coloniaSelectNacimiento").select2();

        $(document).on('select2:opening.disabled', ':disabled', function() { return false; })
        $("#ctl00_contenido_nacionalidad").prop('disabled', 'disabled');
        $('#ctl00_contenido_nacionalidad').prop('disabled', true);
        $("#ctl00_contenido_escolaridad").prop('disabled', 'disabled');
        $("#ctl00_contenido_religion").prop('disabled', 'disabled');
        $("#ctl00_contenido_ocupacion").prop('disabled', 'disabled');
        $("#ctl00_contenido_civil").prop('disabled', 'disabled');
        $("#ctl00_contenido_etinia").prop('disabled', 'disabled');
        $("#ctl00_contenido_sexo").prop('disabled', 'disabled');
        $("#lenguanativa").prop('disabled', 'disabled');
        $("#ctl00_contenido_paisdomicilio").prop('disabled', 'disabled');
        $("#ctl00_contenido_estadodomicilio").prop('disabled', 'disabled');
        $("#ctl00_contenido_municipiodomicilio").prop('disabled', 'disabled');
        $("#ctl00_contenido_coloniaSelect").prop('disabled', 'disabled');
        $("#ctl00_contenido_paisnacimineto").prop('disabled', 'disabled');
        $("#ctl00_contenido_estadonacimiento").prop('disabled', 'disabled');
        $("#ctl00_contenido_municipionacimiento").prop('disabled', 'disabled');
        $("#ctl00_contenido_coloniaSelectNacimiento").prop('disabled', 'disabled');

        function LoadNuevoDetenido() {
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getDataDetenidoNuevo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    interno: "0"
                }),
                cache: false,
                success: function (response) {
                    var resultado = JSON.parse(response.d);
                    responsiveHelper_dt_pertenencias_nuevas = undefined;
                    $("#dt_nuevo_detenido").DataTable().destroy();
                    Createtablenuevodetenido(resultado.list);
                    $(".motivoeventogrid").select2();
                },
                error: function () {
                    ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la tabla. Si el problema persiste contacte al soporte técnico del sistema.");
                }
            });
        }

        $("#mdnombre").on("keyup", function (e) {
            if (e.keyCode === 13) {
               // eliminaFilas();
                var filtro = obtenercirteriosbusqueda();
                if (filtro.nombre == "" && filtro.apellidopaterno == "" && filtro.apellidomaterno == "" && filtro.alias == "") {
                    return;
                }
                responsiveHelper_dt_nuevo_detenido = undefined;
                $("#dt_nuevo_detenido").DataTable().destroy();
                window.tableNuevoDetenidos = false;
                LoadResultados(filtro);
            }
        });

        $("#mdpaterno").on("keyup", function (e) {
            if (e.keyCode === 13) {
                //eliminaFilas();
                var filtro = obtenercirteriosbusqueda();
                if (filtro.nombre == "" && filtro.apellidopaterno == "" && filtro.apellidomaterno == "" && filtro.alias == "") {
                    return;
                }
                responsiveHelper_dt_nuevo_detenido = undefined;
                $("#dt_nuevo_detenido").DataTable().destroy();
                window.tableNuevoDetenidos = false;
                LoadResultados(filtro);
            }
        });

        $("#mdmaterno").on("keyup", function (e) {
            if (e.keyCode === 13) {
               // eliminaFilas();
                var filtro = obtenercirteriosbusqueda();
                if (filtro.nombre == "" && filtro.apellidopaterno == "" && filtro.apellidomaterno == "" && filtro.alias == "") {
                    return;
                }
                responsiveHelper_dt_nuevo_detenido = undefined;
                $("#dt_nuevo_detenido").DataTable().destroy();
                window.tableNuevoDetenidos = false;
                LoadResultados(filtro);
            }
        });

        $("#mdalias").on("keyup", function (e) {
            if (e.keyCode === 13) {
               // eliminaFilas();
                var filtro = obtenercirteriosbusqueda();
                if (filtro.nombre == "" && filtro.apellidopaterno == "" && filtro.apellidomaterno == "" && filtro.alias == "") {
                    return;
                }
                responsiveHelper_dt_nuevo_detenido = undefined;
                $("#dt_nuevo_detenido").DataTable().destroy();
                window.tableNuevoDetenidos = false;
                LoadResultados(filtro);
            }
        });


        $("body").on("click", "#buscar", function () {
            //var filtro = obtenercirteriosbusqueda();
            //if (filtro.nombre == "" && filtro.apellidopaterno == "" && filtro.apellidomaterno == "" && filtro.alias == "") {
            //    ShowAlert("¡Atención!", "Debe ingresar al menos un criterio de búsqueda");
            //    return;
            //}
            //LoadResultados(filtro);
            //var table = $('#dt_nuevo_detenido').DataTable();
            ////console.log("cargadatos2");
            //table.columns.adjust().draw();
            //table.retrieve = false;
        });


        function obtenercirteriosbusqueda() {
            var filtroconsulta = {
                nombre: $("#mdnombre").val(),
                apellidopaterno: $("#mdpaterno").val(),
                apellidomaterno: $("#mdmaterno").val(),
                alias: $("#mdalias").val()
            }
            return filtroconsulta;
        }


        function addrow() {
            CrearCamposBusqueda();
            //var datossex = cargarSexo();
            //var optionssex = "";
            //optionssex += "<option value='0'>[Sexo]</option>";

            //for (var i = 0; i < datossex.length; i++) {
            //    optionssex += "<option value='" + datossex[i].Id + "'>" + datossex[i].Desc + "</option>";
            //}

            //var datos = cargaMotivorGrid();
            //var options = "";
            //options += "<option value='0'>[Motivo]</option>";

            //for (var i = 0; i < datos.length; i++) {
            //    if (parseInt(datos[i].Id) == parseInt($("#motivoevento").val())) {
            //        options += "<option value='" + datos[i].Id + "'selected='selected'>" + datos[i].Desc + "</option>";
            //    }
            //    else {
            //        options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
            //    }
            //}

            //$("#dt_nuevo_detenido").DataTable().row.add([
            //    '<input  type="checkbox" class="checar" data-TrackingId="" data-Id="0"/>',
            //    '<a href="javascript:void(0);" style="padding-left: 8.5px;" class="btn btn-xs btn-danger btn-circle removeRow" title="Eliminar"><i class="glyphicon glyphicon-remove"></i></a>',
            //    '<input type="text" value="" style="width:100%" data-required="true" />',
            //    "<input type='text' value='' style='width:100%' data-required='true' />",
            //    "<input type='text' value='' style='width:100%' data-required='true' />",
            //    "<select style='width:100%' data-required='true'>" + optionssex + "</select>",
            //   // "<input type='text' maxlength='3' pattern='^[0-9]*$' value='' style=' width:100%' data-required='true'  />",
            //    "<select name='Motivo' id='motivoeventogrid' class='select2 motivoeventogrid'  style='width:100%' data-required='true'>" + options + "</select>",
            //    '<a style="display: block;/* font-size:smaller; */font: 300 15px/29px "Open Sans",Helvetica,Arial,sans-serif;" class="btn datos btn-primary" href="javascript:void(0);" id="datos" data-DetalledetencionId="0" title="Ver información"><i class="fa fa-bars"></i> Ver información </a>&nbsp;'
            //]).draw(false);

            //$(".motivoeventogrid").select2();
        }

        //function LoadResultados(parametros) {
        //    var table = $('#dt_nuevo_detenido').DataTable();
        //    table.clear().draw;
        //     $.ajax(
        //        {
        //            type: "POST",
        //            url: "eventsform.aspx/getdatos",
        //            contentType: "application/json; charset=utf-8",
        //            dataType: "json",
        //            data: JSON.stringify({
        //                filtro: parametros
        //            }),
        //            cache: false,
        //            success: function (response) {
        //                var resultado = JSON.parse(response.d);
        //                responsiveHelper_dt_nuvo_detenido = undefined;
        //                $("#dt_nuevo_detenido").DataTable().destroy();

        //                $('#dt_nuevo_detenido').dataTable({
        //                    "lengthMenu": [5, 10, 20, 50],
        //                    iDisplayLength: 7,
        //                    serverSide: false,
        //                    paging: true,
        //                    searching: true,
        //                    //"scrollY": "350px",
        //                    //"scrollCollapse": true,
        //                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
        //                        "t" +
        //                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        //                    "oLanguage": {
        //                        "sSearch": '<span hide="hidden" style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
        //                    },
        //                    columnDefs: [
        //                        {
        //                            width: "100px",
        //                            targets: 5
        //                        },
        //                        {
        //                            width: "50",
        //                            targets: 6
        //                        },
        //                        {
        //                            width: "300px",
        //                            targets: 7
        //                        }
        //                    ]
                            
        //                });                       
        //                var list = resultado.list;                        
                       
        //                var datossex = cargarSexo();

        //                var datos = cargaMotivorGrid();
        //                var options = "";
        //                options += "<option value='0'>[Motivo]</option>";

        //                for (var i = 0; i < datos.length; i++) {

        //                    if (parseInt(datos[i].Id) == parseInt($("#motivoevento").val())) {
        //                        options += "<option value='" + datos[i].Id + "'selected='selected'>" + datos[i].Desc + "</option>";
        //                    }
        //                    else {
        //                        options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
        //                    }
        //                }

        //                    for (var row in list) {

        //                        var optionsSex = "";
        //                        optionsSex += "<option value='0'>[Sexo]</option>";

        //                        for (var i = 0; i < datossex.length; i++) {
        //                            //console.log(row.DetalledetencionSexoId);
        //                            if (parseInt(datossex[i].Id) == parseInt(list[row].DetalledetencionSexoId)) {
        //                                optionsSex += "<option value='" + datossex[i].Id + "'selected='selected'>" + datossex[i].Desc + "</option>";
        //                            }
        //                            else {
        //                                optionsSex += "<option value='" + datossex[i].Id + "'>" + datossex[i].Desc + "</option>";
        //                            }
        //                        }

        //                        $("#dt_nuevo_detenido").DataTable().row.add([
        //                            '<input type="checkbox" class="checar" data-TrackingId="' + row.TrackingId + '" data-Id="' + row.Id + '"/>',
        //                            '<a href="javascript:void(0);" style="padding-left: 8.5px;" class="btn btn-xs btn-danger btn-circle removeRow" title="Eliminar"><i class="glyphicon glyphicon-remove"></i></a>',
        //                            "<input disabled='disabled' type='text' value='" + list[row].NombreDetenido + "' style='width:100%' data-required='true' />",
        //                            "<input disabled='disabled' type='text' value='" + list[row].APaternoDetenido + "' style='width:100%' data-required='true' />",
        //                            "<input disabled='disabled' type='text' value='" + list[row].AMaternoDetenido + "' style='width:100%' data-required='true' />",

        //                            "<select style='width:100%' data-required='true'>" + optionsSex + "</select>",
        //                           // "<input type='text' maxlength='3' pattern='^[0-9]*$' value='" + list[row].DetalledetencionEdad + "' style=' width:100%' data-required='true'  />",
        //                            "<select name='Motivo' id='motivoeventogrid' class='select2 motivoeventogrid'  style='width:100%' data-required='true'>" + options + "</select>",
        //                            '<a style="display: block;/* font-size:smaller; */font: 300 15px/29px "Open Sans",Helvetica,Arial,sans-serif;" class="btn datos btn-primary" href="javascript:void(0);" id="datos" data-TrackingId="' + list[row].TrackingId + '" data-DetalledetencionId="' + list[row].Id + '" title="Ver información"><i class="fa fa-bars"></i> Ver información </a>&nbsp;'
        //                        ]).draw(false);

        //                        $(".motivoeventogrid").select2();
        //                    }
                       
        //            },
        //            error: function () {
        //                ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la listado de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
        //            }
        //        });
        //}

        function LoadResultados(parametros) {
            //var table = $('#dt_nuevo_detenido').DataTable();
            //table.clear().draw;

            //var datossex = cargarSexo();
            //var datos = cargaMotivorGrid();

            $('#dt_nuevo_detenido').dataTable({
                "lengthMenu": [5, 10, 20, 50],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>',
                    "sEmptyTable": ' '
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_detenidos) {
                        responsiveHelper_dt_basic_detenidos = new ResponsiveDatatablesHelper($('#dt_nuevo_detenido'), breakpointDefinition);
                    }
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_detenidos.respond();
                    $('#dt_nuevo_detenido').waitMe('hide');
                },
                "initComplete": function (settings, json) {
                    setTimeout(function () {
                        CrearCamposBusqueda();
                    }, 1000);
                },
                ajax: {
                    url: "eventsform.aspx/getdatos",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: function (d) {
                        startLoading();
                        d.emptytable = window.tableNuevoDetenidos;
                        d.pages = $('#dt_nuevo_detenido').DataTable().page.info().page || "";
                        d.filtro = parametros;
                        return JSON.stringify(d);
                    },
                    dataSrc: "data",
                    dataFilter: function (data) {
                        var json = jQuery.parseJSON(data);
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        endLoading();
                        return JSON.stringify(json);
                    }
                },
                columns: [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        data: "TrackingId",
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type="checkbox" class="checar" data-TrackingId="' + row.TrackingId + '" data-Id="' + row.Id + '" />';
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<a href="javascript:void(0);" style="padding-left: 8.5px;" class="btn btn-xs btn-danger btn-circle removeRow" title="Eliminar"><i class="glyphicon glyphicon-remove"></i></a>';
                        }
                    },
                    {
                        targets: 2,
                        data: "NombreDetenido",
                        render: function (data, type, row, meta) {
                            return "<input disabled='disabled' type='text' value='" + row.NombreDetenido + "' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        targets: 3,
                        data: "APaternoDetenido",
                        render: function (data, type, row, meta) {
                            return "<input disabled='disabled' type='text' value='" + row.APaternoDetenido + "' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        targets: 4,
                        data: "AMaternoDetenido",
                        render: function (data, type, row, meta) {
                            return "<input disabled='disabled' type='text' value='" + row.AMaternoDetenido + "' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        targets: 5,
                        data: "DetalledetencionSexoId",
                        render: function (data, type, row, meta) {
                            var optionsSex = "";
                            optionsSex += "<option value='0'>[Sexo]</option>";

                            for (var i = 0; i < datossex.length; i++) {

                                if (parseInt(datossex[i].Id) == parseInt(row.DetalledetencionSexoId)) {
                                    optionsSex += "<option value='" + datossex[i].Id + "'selected='selected'>" + datossex[i].Desc + "</option>";
                                }
                                else {
                                    optionsSex += "<option value='" + datossex[i].Id + "'>" + datossex[i].Desc + "</option>";
                                }
                            }

                            return "<select style='width:100%' data-required='true'>" + optionsSex + "</select>";
                        }
                    },
                    {
                        targets: 6,
                        data: "TrackingId",
                        render: function (data, type, row, meta) {
                            var datos = cargaMotivorGrid();
                            var options = "";
                            options += "<option value='0'>[Motivo]</option>";

                            for (var i = 0; i < datos.length; i++) {

                                if (parseInt(datos[i].Id) == parseInt($("#motivoevento").val())) {
                                    options += "<option value='" + datos[i].Id + "selected='selected'>" + datos[i].Desc + "</option>";
                                }
                                else {
                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                }
                            }

                            return "<select name='Motivo' id='motivoeventogrid' class='select2 motivoeventogrid'  style='width:100%' data-required='true'>" + options + "</select>";
                        }
                    },
                    {
                        targets: 7,
                        render: function (data, type, row, meta) {
                            return '<a style="display: block;/* font-size:smaller; */font: 300 15px/29px "Open Sans",Helvetica,Arial,sans-serif;" class="btn datos btn-primary" href="javascript:void(0);" id="datos" data-TrackingId="' + row.TrackingId + '" data-DetalledetencionId="' + row.Id + '" title="Ver información"><i class="fa fa-bars"></i> Ver información </a>&nbsp;';
                        }
                    },
                ]
            });
        }

        function CrearCamposBusqueda() {
            var header = $("#dt_nuevo_detenido thead");
            var classAux = '';
            if ($("#dt_nuevo_detenido tbody").children('tr:first').length > 0) {
                classAux = $("#dt_nuevo_detenido tbody").children('tr:last')[0].className !== undefined ? $("#dt_nuevo_detenido tbody").children('tr:last')[0].className : "";
            }
            var classTr = classAux === "odd" ? 'even' : 'odd';
            var tr = '<tr role="row" class="' + classTr + '">';
            var kk = 0;

            var optionsSex = '';
            optionsSex += '<option value="0">[Sexo]</option>';

            for (var i = 0; i < datossex.length; i++) {
                optionsSex += '<option value="' + datossex[i].Id + '">' + datossex[i].Desc + '</option>';
            }

            var options = "";
            options += '<option value="0">[Motivo]</option>';

            for (var i = 0; i < datos.length; i++) {
                options += '<option value="' + datos[i].Id + '">' + datos[i].Desc + '</option>';
            }

            $("#dt_nuevo_detenido thead th").each(function () {
                var title = $(this).text();

                switch (title) {
                    case "":
                        if (kk === 0) {
                            var td = '<td style="width: 15px;"><input type="checkbox" class="checar" data-TrackingId="0" data-Id="0" /></td>';
                            tr += td;
                        }
                        else {
                            var td = '<td><a href="javascript:void(0);" style="padding-left: 7px;" class="btn btn-xs btn-danger btn-circle removeRow" title="Eliminar"><i class="glyphicon glyphicon-remove"></i></a></td>';
                            tr += td;
                        }
                        break;
                    case "Nombre(s)":
                        var td = '<td><input type="text" value="" style="width:100%; margin-bottom: 7px;" data-required="true" /></td>';
                        tr += td;
                        break;
                    case "Apellido paterno":
                        var td = '<td><input type="text" value="" style="width:100%; margin-bottom: 7px;" data-required="true" /></td>';
                        tr += td;
                        break;
                    case "Apellido materno":
                        var td = '<td><input type="text" value="" style="width:100%; margin-bottom: 7px;" data-required="true" /></td>';
                        tr += td;
                        break;
                    case "Sexo":
                        var td = '<td><select style="width: 100%; margin-bottom: 8px;" data-required="true">' + optionsSex + '</select></td>';
                        tr += td;
                        break;
                    case "Motivo":
                        var td = '<td><select name="Motivo" id="motivoeventogrid" class="select2 motivoeventogrid" style="width: 100%; margin-bottom: 8px;" data-required="true">' + options + '</select></td>';
                        tr += td;
                        break;
                    case "Acciones":
                        var td = '<td><a style="display: block;/* font-size:smaller; */ //font: 300 15px/29px "Open Sans",Helvetica,Arial,sans-serif;" class="btn datos btn-primary" href="javascript:void(0);" id="datos" data-DetalledetencionId="0" title="Ver información"><i class="fa fa-bars"></i> Ver información </a>&nbsp;</td>';
                        tr += td;
                        break;
                }

                //if (title != "Acciones" && title != "") {
                //    var titleNoSpaces = title.replace(" ", "_").replace(".", "").replace("ó", "o");
                //    var td = '<th><input id="input' + titleNoSpaces + '" type="text" placeholder="' + title + '" /></th>';
                //    tr += td;
                //}
                //else {
                //    tr += '<th></th>';
                //}
                kk++;
            });

            tr += '</tr>';

            //if (val > 0) {
            //    $("#dt_nuevo_detenido tbody").children('tr:last').after(tr);
            //}
            //else {
            //    $(header).children('tr:last').after(tr);
            //}
            //$(header).children('tr:last').after(tr);
            //setTimeout(function () {
            $("#dt_nuevo_detenido tbody").children('tr:first').before(tr);
            //}, 500);                

            //$("#dt_nuevo_detenido thead tr:first th").css("padding", "9px");
        }

        function eliminaFilas2() {
            var table = $('#dt_nuevo_detenido').DataTable();
            $('#dt_nuevo_detenido tbody tr').each(function () {
                $(this).addClass('selected');
                table.row($(this)).remove().draw(false);
            });
         
        }

        function eliminaFilas() {
            var table = $('#dt_nuevo_detenido').DataTable();
            $('#dt_nuevo_detenido tbody tr').each(function (index, tr) {
                var checkit = tr.childNodes[0].childNodes[0].checked;
                if (!checkit) {
                    $(this).addClass('selected');
                    table.row($(this)).remove().draw(false);
                }
            });
      
        }

        function cargaMotivorGrid() {

            if (dataMotivo == null && datosMotivo.length == 0) {

                $.ajax({
                    async: false,
                    type: "POST",
                    url: "eventsform.aspx/getMotivogrid",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        dataMotivo += "<option></option>";

                        $.each(response.d, function (index, item) {
                            var tt = {};
                            tt.Id = item.Id;
                            tt.Desc = item.Desc;

                            datosMotivo.push(tt);
                            dataMotivo += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                        });
                    },
                    error: function () {

                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");

                    }
                });
            }
            return datosMotivo;
        }

        function CargarSexo2(setsexo) {
            $('#ctl00_contenido_sexo').empty();
            $.ajax({
                type: "POST",
                url: "eventsform.aspx/getSexogrid",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (response) {
                    var Dropdown = $('#ctl00_contenido_sexo');

                    Dropdown.append(new Option("[Sexo]", "0"));
                    $.each(response.d, function (index, item) {
                        Dropdown.append(new Option(item.Desc, item.Id));
                    });

                    if (setsexo != "") {
                        Dropdown.val(setsexo);
                        Dropdown.trigger("change.select2");
                    }
                },
                error: function () {
                    ShowError("¡Error!", "No fue posible cargar la lista de sexo. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                }
            });
        }

        function cargarSexo() {
            if (dataSexo == null && datosSexo.length == 0) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "eventsform.aspx/getSexogrid",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        dataSexo += "<option></option>";

                        $.each(response.d, function (index, item) {
                            var tt = {};
                            tt.Id = item.Id;
                            tt.Desc = item.Desc;

                            datosSexo.push(tt);
                            dataSexo += "<option value=" + item.Id + ">" + item.Desc + "</option>";
                        });
                    },
                    error: function () {

                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");

                    }
                });
            }
            return datosSexo;
        }

        $('#modalDetenido').on('hidden.bs.modal', function (e) {
            //eliminaFilas2();
            // eliminaFilas();
            //var table = $('#dt_nuevo_detenido').DataTable();
            //table.clear().draw;
            $("#mdnombre").val("");
            $("#mdpaterno").val("");
            $("#mdmaterno").val("");
            $("#mdalias").val("");
        });        

        $("#modalDetenido").on('shown.bs.modal', function () {
            //var table = $('#dt_nuevo_detenido').DataTable();
            ////console.log("cargadatos2");
            //table.columns.adjust().draw();
            //document.querySelector("#mdnombre").focus();
        });
      
        function validarCamposEnTabla() {
            var isValid = true;

            $('#dt_nuevo_detenido tbody tr').each(function (i, row) {
                if (row.childNodes[2] !== undefined && row.childNodes[3] !== undefined && row.childNodes[4] !== undefined &&
                    row.childNodes[5] !== undefined) {
                    if (row.childNodes[0].childNodes[0].getAttribute('data-trackingid') === "0") {
                        //var nombre = row.childNodes[2].childNodes[0].value;
                        //var paterno = row.childNodes[3].childNodes[0].value;
                        //var materno = row.childNodes[4].childNodes[0].value;
                        //var sexo = row.childNodes[5].childNodes[0].value;
                        var nombre = this.childNodes[2].childNodes[0].getAttribute("data-required");
                        var Apellidopaterno = this.childNodes[3].childNodes[0].getAttribute("data-required");
                        var Apellidomaterno = this.childNodes[4].childNodes[0].getAttribute("data-required");
                        var Sexo = this.childNodes[5].childNodes[0].getAttribute("data-required");

                        if (nombre === "true") {
                            if (this.childNodes[2].childNodes[0].value === "" ||
                                this.childNodes[2].childNodes[0].value === undefined ||
                                this.childNodes[2].childNodes[0].value === null) {
                                this.childNodes[2].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Nombre", "El campo nombre es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.childNodes[2].childNodes[0].removeAttribute('class');
                            }
                        }

                        if (Apellidopaterno === "true") {
                            if (this.childNodes[3].childNodes[0].value === "" ||
                                this.childNodes[3].childNodes[0].value === undefined ||
                                this.childNodes[3].childNodes[0].value === null) {
                                this.childNodes[3].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Apellido paterno", "El campo apellido paterno es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.childNodes[3].childNodes[0].removeAttribute('class');
                            }
                        }

                        if (Apellidomaterno === "true") {
                            if (this.childNodes[4].childNodes[0].value === "" ||
                                this.childNodes[4].childNodes[0].value === undefined ||
                                this.childNodes[4].childNodes[0].value === null) {
                                this.childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Apellido materno", "El campo apellido materno es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.childNodes[4].childNodes[0].removeAttribute('class');
                            }
                        }

                        if (Sexo === "true") {
                            if (this.childNodes[5].childNodes[0].value === "0" ||
                                this.childNodes[5].childNodes[0].value === undefined ||
                                this.childNodes[5].childNodes[0].value === null) {
                                this.childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Sexo", "El sexo es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.childNodes[5].childNodes[0].removeAttribute('class');
                            }
                        }
                    }
                }
            });

            $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                var nombre = this.node().childNodes[2].childNodes[0].getAttribute("data-required");
                var Apellidopaterno = this.node().childNodes[3].childNodes[0].getAttribute("data-required");
                var Apellidomaterno = this.node().childNodes[4].childNodes[0].getAttribute("data-required");
                var Sexo = this.node().childNodes[5].childNodes[0].getAttribute("data-required");
                //console.log('datasi', nombre, Apellidopaterno, Apellidomaterno, Sexo, this.node().childNodes[0].childNodes[0].checked);
                if (this.node().childNodes[0].childNodes[0].checked) {
                    if (nombre === "true") {
                        if (this.node().childNodes[2].childNodes[0].value === "" ||
                            this.node().childNodes[2].childNodes[0].value === undefined ||
                            this.node().childNodes[2].childNodes[0].value === null) {
                            this.node().childNodes[2].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Nombre", "El campo nombre es obligatorio.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[2].childNodes[0].removeAttribute('class');
                        }
                    }

                    if (Apellidopaterno === "true") {
                        if (this.node().childNodes[3].childNodes[0].value === "" ||
                            this.node().childNodes[3].childNodes[0].value === undefined ||
                            this.node().childNodes[3].childNodes[0].value === null) {
                            this.node().childNodes[3].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Apellido paterno", "El campo apellido paterno es obligatorio.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[3].childNodes[0].removeAttribute('class');
                        }
                    }

                    if (Apellidomaterno === "true") {
                        if (this.node().childNodes[4].childNodes[0].value === "" ||
                            this.node().childNodes[4].childNodes[0].value === undefined ||
                            this.node().childNodes[4].childNodes[0].value === null) {
                            this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Apellido materno", "El campo apellido materno es obligatorio.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[4].childNodes[0].removeAttribute('class');
                        }
                    }

                    if (Sexo === "true") {
                        if (this.node().childNodes[5].childNodes[0].value === "0" ||
                            this.node().childNodes[5].childNodes[0].value === undefined ||
                            this.node().childNodes[5].childNodes[0].value === null) {
                            this.node().childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                            ShowError("Sexo", "El sexo es obligatorio.");
                            isValid = false;
                        }
                        else {
                            this.node().childNodes[5].childNodes[0].removeAttribute('class');
                        }
                    }
                }
            });

            return isValid;
        }
                     
        function Createtablenuevodetenido(data) {
            $('#dt_nuevo_detenido').dataTable({
                "lengthMenu": [5, 10, 20, 50],
                iDisplayLength: 7,
                serverSide: false,
                paging: true,
                retrieve: false,
                searching: false,
                //"scrollY": "350px",
                //"scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span hide="hidden" style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_nuevo_detenido) {
                        responsiveHelper_dt_nuevo_detenido = new ResponsiveDatatablesHelper($('#dt_nuevo_detenido'), breakpointDefinition);
                    }
                }, 
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_nuevo_detenido.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_nuevo_detenido.respond();
                    $('#dt_nuevo_detenido').waitMe('hide');
                },
                data: data,
                columns: [
                    //null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                  //  null,
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type="checkbox" class="checar" data-TrackingId="" data-Id="0"/>';
                        }

                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<a class="btn btn-xs btn-danger btn-circle removeRow" title="Eliminar"><i class="glyphicon glyphicon-remove"></i></a>';
                        }
                    },
                    {
                        width: "200px",
                        targets: 2,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            return "<input type='text' value='' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        width: "200px",
                        targets: 3,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                            return "<input type='text' value='' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        width: "200px",
                        targets: 4,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                            return "<input type='text' value='' style='width:100%' data-required='true' />";
                        }
                    },
                    {
                        width: "145px",
                        targets: 5,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var datos = cargarSexo();
                            var options = "";
                            options += "<option value='0'>[Sexo]</option>";

                            for (var i = 0; i < datos.length; i++) {
                                options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                            }

                            return "<select style='width:100%' data-required='true'>" + options + "</select>";
                        }
                    },/*
                    {
                        width: "40px",
                        targets: 6,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                            return "<input type='text' maxlength='3' pattern='^[0-9]*$' value='' style=' width:100%' data-required='true'  />";
                        }
                    },*/
                    {
                        width: "240px",
                        targets: 6,
                        orderable: true,
                        render: function (data, type, row, meta) {
                            // return '<label class="input"><i class="icon-append fa fa-calendar-check-o"></i><input type="datetime-local" name="vigenciaInicial" id="fechaid_'+ row.Id +'" value="'+row.Fecha+'" /><label>'
                            var datos = cargaMotivorGrid();
                            var options = "";
                            options += "<option value='0'>[Motivo]</option>";

                            for (var i = 0; i < datos.length; i++) {

                                if (parseInt(datos[i].Id) == parseInt($("#motivoevento").val())) {
                                    options += "<option value='" + datos[i].Id + "'selected='selected'>" + datos[i].Desc + "</option>";
                                }
                                else {
                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                }
                            }

                            return "<select name='Motivo' id='motivoeventogrid' class='select2 motivoeventogrid'  style='width:100%' data-required='true'>" + options + "</select>";
                        }
                    },
                    {
                        width: "100px",
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<a style="display: block;/* font-size:smaller; */font: 300 30px/29px "Open Sans",Helvetica,Arial,sans-serif;" class="btn datos btn-primary href="#"  id="datos" data-DetalledetencionId="0" title="Ver información"><i class="fa fa-bars"></i> Ver información </a>&nbsp;';

                        }
                    }
                    
                ]
            });
        }

        $("body").on("click", ".checar", function () {

            $("#mdnombre").val("");
            $("#mdpaterno").val("");
            $("#mdmaterno").val("");
            $("#mdalias").val("");

        });

        $("body").on("click", ".removeRow", function () {
            $(this).closest("tr").remove();
            //var table = $('#dt_nuevo_detenido').DataTable();
            //$(this).parent().parent().addClass("selectedRow");
            //table.row('.selectedRow').remove().draw(false);
        });

        $(document).on('keydown', 'input[pattern]', function (e) {
            var input = $(this);
            var oldVal = input.val();
            var regex = new RegExp(input.attr('pattern'), 'g');

            setTimeout(function () {
                var newVal = input.val();
                if (!regex.test(newVal)) {
                    input.val(oldVal);
                }
            }, 0);
        });        

            var datossex = [];
            var datos = [];

            var responsiveHelper_dt_basic_detenidos = undefined;
            var responsiveHelper_datatable_fixed_column_detenidos = undefined;
            var responsiveHelper_datatable_col_reorder_detenidos = undefined;
            var responsiveHelper_datatable_tabletools_detenidos = undefined;

            $("#modalUnidad").on("shown.bs.modal", function () {
                $("#unidad").next().children().children().focus()
            });

            $("#evento-reciente-modal").on("shown.bs.modal", function () {
                $("#detenido").next().children().children().focus()
            });

            $("#modalUnidad").on("hidden.bs.modal", function () {
                document.querySelector("#btnGuardarUnidad").focus();
            });

            $("#evento-reciente-modal").on("hidden.bs.modal", function () {
                document.querySelector("#add").focus();
            });

            $("#modalDetenido").on("hidden.bs.modal", function () {
                document.querySelector("#linkDetenidos").focus();
            });

            $("#motivoevento").select2();
            $("#motivoeventogrid").select2();
            $("#llamada").select2();
            $("#unidad").select2();
            $("#responsable").select2();
            $("#municipio").select2();
            $("#colonia").select2();
            $("#alertaWebInstitucion").select2();
            $("#alertaWebUnidad").select2();
            $("#sexoDetenido").select2();
            $("#evento").select2();
            $("#detenido").select2();
            $('#autorizaciondatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm:ss',
                defaultDate: new Date(),
                autoclose: true
            }).on('change', function (e) {
                $(".bootstrap-datetimepicker-widget").toggle();
            });
            //$('#FehaNacimientodatetimepicker').datetimepicker({
            //    format: 'DD/MM/YYYY'
            //});

            var param = RequestQueryString("tracking");
            var responsiveHelper_dt_basic_tabla_unidades = undefined;
            var responsiveHelper_dt_basic_tabla_detenidos = undefined;
            var responsiveHelper_dt_basic_tabla_eventos = undefined;
            var responsiveHelper_dt_nuevo_detenido = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            getalerta();
            function getalerta() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/GetAlertaWeb",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.d != null) {
                            data = data.d;

                            var alerta1 = data.Denominacion;
                            var alerta2 = data.AlertaWerb;

                            //$("#Denominacin").val(data.Denominacion);

                            //$("#linkEvento").html('<i class="glyphicon glyphicon-folder-open"></i>&nbsp; ' + alerta1 + ' / evento');
                            $("#alerta1").html(alerta1);
                            //$("#alerta2").html('<i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>' + alerta1);
                            //$("#alerta3").html(' <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de ' + alerta2);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de alerta web. Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
            }


            window.table = $('#dt_basic_tabla_detenidos').dataTable({
                "lengthMenu": [5, 20, 50, 100],
                iDisplayLength: 5,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_tabla_detenidos) {
                        responsiveHelper_dt_basic_tabla_detenidos = new ResponsiveDatatablesHelper($('#dt_basic_tabla_detenidos'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_tabla_detenidos.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_tabla_detenidos.respond();
                    $('#dt_basic_tabla_detenidos').waitMe('hide');
                },
                "createdRow": function (row, data, index) {

                },
                ajax: {
                    type: "POST",
                    url: "eventsform.aspx/getDetenidosTabla",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic_tabla_detenidos').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        var trackingid;

                        if (param !== undefined) {
                            trackingid = param;
                        }
                        else {
                            trackingid = "";
                        }

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = trackingid;
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Sexo",
                        data: "Sexo"
                    },/*
                    {
                        name: "Edad",
                        data: "Edad"
                    },*/
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                ],
                columnDefs: [
                    {

                    }
                ]

            });

            $('#dt_basic_tabla_unidades').dataTable({
                "lengthMenu": [5, 20, 50, 100],
                iDisplayLength: 5,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_tabla_unidades) {
                        responsiveHelper_dt_basic_tabla_unidades = new ResponsiveDatatablesHelper($('#dt_basic_tabla_unidades'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_tabla_unidades.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_tabla_unidades.respond();
                    $('#dt_basic_tabla_unidades').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "eventsform.aspx/getUnidadadesTabla",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic_tabla_unidades').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        var trackingid;

                        if (param !== undefined) {
                            trackingid = param;
                        }
                        else {
                            trackingid = "";
                        }

                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.tracking = trackingid;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        name: "Unidad",
                        data: "Unidad"
                    },
                    {
                        name: "ClaveResponsable",
                        data: "ClaveResponsable"
                    },
                    {
                        name: "Corporacion",
                        data: "Corporacion"
                    },
                ],
                columnDefs: [

                ]
            });

            $("#municipio").change(function () {
                loadNeighborhood("0", $("#municipio").val());
            });

            $("#colonia").change(function () {
                loadZipCode($("#colonia").val());
            });

            $("#detenido").change(function () {
                var id = $("#detenido").val();

                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getbyid",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ _id: id }),
                    cache: false,
                    success: function (data) {
                        data = data.d;
                        if (data != null) {
                            if (data.Sexo != undefined) {
                                $("#idsexo").text('Sexo: ' + data.Sexo);
                            }
                            else {
                                $("#idsexo").text('');
                            }
                            if (data.Edad != undefined) {
                                $("#idedad").text('Edad: ' + data.Edad);
                            }
                            else {
                                $("#idedad").text('');
                            }
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de motivos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            });



            $("#llamada").change(function () {
                if ($(this).val() == "0") {
                    loadCities("0", $("#estado").val());
                    loadNeighborhood("0", "0");
                    $("#codigoPostal").val("");
                    $("#descripcion").val("");
                    $("#lugar").val("");
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "eventsform.aspx/getCall",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        processdata: true,
                        traditional: true,
                        data: JSON.stringify({ LlamadaId: $("#llamada").val() }),
                        cache: false,
                        success: function (response) {
                            response = JSON.parse(response.d);

                            if (response.exitoso) {
                                loadCities(response.obj.IdMunicipio, response.obj.IdEstado);
                                loadNeighborhood(response.obj.ColoniaId, response.obj.IdMunicipio);
                                $("#codigoPostal").val(response.obj.CodigoPostal);
                                $("#descripcion").val(response.obj.Descripcion);
                                $("#lugar").val(response.obj.Lugar);
                            } else {
                                ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                            }
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }
            });

            $("#alertaWebInstitucion").change(function () {
                loadUnidadesAW("0", $("#alertaWebInstitucion").val());
                loadEventosAW("0", 0);
            });

            $("#alertaWebUnidad").change(function () {
                loadEventosAW($("#alertaWebUnidad").val(), 0);
            });

            $("body").on("click", "#add", function () {
                var tracking = $(this).attr("data-tracking");

                cargarEvento(tracking);
                cargarDetenido(tracking);

                $("#evento-reciente-modal").modal("show");
            });

            $("#dt_nuevo_detenido").on("click", "a.deleteitem", function () {
                //  e.preventDefault();

                //editor.remove( $(this).closest('tr'), {
                //title: 'Delete record',
                //message: 'Are you sure you wish to remove this record?',
                //buttons: 'Delete'
            });


            //           $('#example').on('click', 'a.editor_remove', function (e) {
            //    e.preventDefault();

            //    editor.remove( $(this).closest('tr'), {
            //        title: 'Delete record',
            //        message: 'Are you sure you wish to remove this record?',
            //        buttons: 'Delete'
            //    } );
            //} );


            function loadFolio() {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getFolio",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        response = JSON.parse(response.d);

                        if (response.exitoso) {
                            $("#folio").val(response.folio);
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadCities(setvalue, idEstado) {
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getCities",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idEstado: idEstado
                    }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#municipio');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Municipio]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de municipios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadMotivos(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getMotivogrid",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({

                    }),
                    success: function (response) {
                        var Dropdown = $("#motivoevento");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Motivo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadNeighborhood(set, idMunicipio) {
                $.ajax({
                    type: "POST",
                    url: "callform.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#colonia");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadUnidad(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getUnidades",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $("#unidad");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Unidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                        $("#unidad").next().children().children().focus()
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de unidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadSexo(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getSexo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $("#sexoDetenido");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Sexo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadResponsable(set) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getResponsables",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                    }),
                    success: function (response) {
                        var Dropdown = $("#responsable");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Clave-responsable]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (set != "") {
                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function loadZipCode(idColonia) {
                $.ajax({
                    type: "POST",
                    url: "callform.aspx/getZipCode",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idColonia: idColonia
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        $("#codigoPostal").val(resultado.cp);
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function generarObjeto($items) {
                var obj = {};
                $items.each(function () {
                    var id = this.id;
                    obj[id] = $(this).val();
                });
                return obj;
            }

            function llenarSelect(idSelect, datos) {
                var select = document.getElementById('' + idSelect);

                for (var i = 0; i < datos.length; i++) {
                    var opt = document.createElement('option');
                    opt.innerHTML = datos[i].Nombre;
                    opt.value = datos[i].Id;
                    select.appendChild(opt);
                }
            }

            function camposVacios(_items) {
                var _tmpItems = [].slice.call(_items);
                var esValido = true;
                _tmpItems.map(function (item) {
                    var parent = item.parentNode;
                    var attName = item.getAttribute('name');
                    if (item.nodeName.toLowerCase() === 'select' && item.value === '0') {
                        ShowError('' + attName, 'El campo ' + attName.toLowerCase() + ' es obligatorio.');
                        parent.classList.remove('state-success');
                        parent.classList.add('state-error');
                        item.classList.remove('valid');
                        esValido = false;
                    } else if (item.value === '' || item.value === undefined || item.value.length === 0) {
                        ShowError('' + attName, 'El campo ' + attName.toLowerCase() + ' es obligatorio.');
                        parent.classList.remove('state-success');
                        parent.classList.add('state-error');
                        item.classList.remove('valid');
                        esValido = false;
                    } else {
                        parent.classList.add('state-success');
                        parent.classList.remove('state-error');
                        item.classList.add('valid');
                    }
                });
                return esValido;
            }

            function validarUnidad() {
                var esvalido = true;
                if ($("#unidad").val() == null || $("#unidad").val() == "0") {
                    ShowError("Unidad", "El campo unidad es obligatorio.");
                    $('#unidad').parent().removeClass('state-success').addClass("state-error");
                    $('#unidad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#unidad').parent().removeClass("state-error").addClass('state-success');
                    $('#unidad').addClass('valid');
                }

                if ($("#responsable").val() == null || $("#responsable").val() == "0") {
                    ShowError("Clave-responsable", "El campo clave-responsable es obligatorio.");
                    $('#responsable').parent().removeClass('state-success').addClass("state-error");
                    $('#responsable').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#responsable').parent().removeClass("state-error").addClass('state-success');
                    $('#responsable').addClass('valid');
                }

                return esvalido;
            }
            $("#nombreDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("#paternoDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("#maternoDetenido").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            function validarResponsable() {
                var esvalido = true;
                if ($("#nombreDetenido").val().split(" ").join("") == "") {
                    ShowError("Nombre", "El campo nombre es obligatorio.");
                    $('#nombreDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#nombreDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#nombreDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#nombreDetenido').addClass('valid');
                }

                if ($("#paternoDetenido").val().split(" ").join("") == "") {
                    ShowError("Apellido paterno", "El campo apellido paterno es obligatorio.");
                    $('#paternoDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#paternoDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#paternoDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#paternoDetenido').addClass('valid');
                }

                if ($("#maternoDetenido").val().split(" ").join("") == "") {
                    ShowError("Apellido materno", "El campo apellido materno es obligatorio.");
                    $('#maternoDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#maternoDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#maternoDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#maternoDetenido').addClass('valid');
                }

                if ($("#sexoDetenido").val() == null || $("#sexoDetenido").val() == "0") {
                    ShowError("Sexo", "El campo sexo es obligatorio.");
                    $('#sexoDetenido').parent().removeClass('state-success').addClass("state-error");
                    $('#sexoDetenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#sexoDetenido').parent().removeClass("state-error").addClass('state-success');
                    $('#sexoDetenido').addClass('valid');
                }


                /*
                if ($("#edad").val() == null || $("#edad").val() == "" || $("#edad").val() <= 0 || $("#edad").val() > 105) {
                    if ($("#edad").val() == null || $("#edad").val() == "") ShowError("Edad", "El campo edad es obligatorio.");
                    else if ($("#edad").val() <= 0) ShowError("Edad", "La edad mínima es de 1 año.");
                    else if ($("#edad").val() > 105) ShowError("Edad", "La edad máxima es de 105 años.");
                    $('#edad').parent().removeClass('state-success').addClass("state-error");
                    $('#edad').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#edad').parent().removeClass("state-error").addClass('state-success');
                    $('#edad').addClass('valid');
                }*/

                return esvalido;
            }

            function obtenerEvento(tkg) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/getEventoByTrackingId",
                    data: JSON.stringify({ tracking: tkg }),
                    success: function (response) {
                        response = JSON.parse(response.d);
                        endLoading();

                        if (response.exitoso) {
                            loadCalls(response.obj.IdLlamada);
                            loadCities(response.obj.IdMunicipio, response.obj.IdEstado);
                            loadNeighborhood(response.obj.ColoniaId, response.obj.IdMunicipio);
                            loadMotivos(response.obj.MotivoId);
                            $("#codigoPostal").val(response.obj.CodigoPostal);
                            $("#descripcion").val(response.obj.Descripcion);
                            $("#lugar").val(response.obj.Lugar);
                            $("#fecha").val(response.obj.HoraYFecha);
                            $("#folio").val(response.obj.Folio);
                            $("#numeroDetenidos").val(response.obj.NumeroDetenidos);

                            $("#latitud").val(response.obj.Latitud);
                            $("#longitud").val(response.obj.Longitud);
                            if ($('#latitud').val() == "" || $('#longitud').val() == "") {

                            }
                            else {
                                $("#L1").val(response.obj.Latitud);
                                $("#L2").val(response.obj.Longitud);
                            }
                            CargarMapa();
                            $('#linkUnidad').show();
                            $('#linkDetenidos').show();

                            window.emptytable = false;
                            responsiveHelper_dt_basic_tabla_eventos = undefined;
                            $("#dt_basic_tabla_eventos").DataTable().destroy();
                            // loadEventosAW(response.obj.UnidadIdAW,response.obj.relacionado);

                            $("#btnGuardarUnidad").attr("data-EventoId", response.obj.Id);
                            $("#btnGuardarDetenido").attr("data-EventoId", response.obj.Id);
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                        }
                    },
                    error: function (error) {

                        endLoading();
                    }
                });
            }

            function loadCalls(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getCalls",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#llamada');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Sin llamada]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de llamadas. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadUnidadesAW(setvalue, institucionId) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getUnidadesAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "institucionId": institucionId }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#alertaWebUnidad');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Unidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadInstitucionesAW(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getInstitucionesAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#alertaWebInstitucion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Institucion]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de instituciones. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            //loadNeighborhood("0", "0");
            function loadEventosAW(idUnidadInstitucion, relacionado) {
                responsiveHelper_dt_basic_tabla_eventos = undefined;
                $("#dt_basic_tabla_eventos").DataTable().destroy();

                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getEventosAW",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idUnidadInstitucion: idUnidadInstitucion
                    }),
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        if (r.latitud != '')
                            $("#latitud").val(r.latitud);

                        if (r.longitud != '')
                            $("#longitud").val(r.longitud);

                        llenarTabla(r.lista, relacionado);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de eventos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });

            }

            function llenarTabla(lista, relacionado) {
                $('#dt_basic_tabla_eventos').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: false,
                    fixedColumns: true,
                    fixedColumns: true,
                    autoWidth: true,
                    "scrollY": "100%",
                    "scrollX": "0%", // // // // // // // // // // // // // // // //// // // // // // // //// // // // // // // //// // // // // // // //// // // // // // // //
                    "scrollCollapse": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_tabla_eventos) {
                            responsiveHelper_dt_basic_tabla_eventos = new ResponsiveDatatablesHelper($('#dt_basic_tabla_eventos'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_tabla_eventos.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_tabla_eventos.respond();
                        $('#dt_basic_tabla_eventos').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                    },
                    data: lista,
                    columnDefs: [
                        {
                            targets: 0,
                            name: "UnidadId",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (param != undefined) {
                                    if (relacionado == 0)
                                        return '<a class="btn btn-info btn-xs relacionar" href="#" data-unidadId="' + row.UnidadId + '">Relacionar</a>';
                                    else
                                        return "";
                                }
                                else {
                                    return "";
                                }
                            }
                        },
                        {
                            targets: 1,
                            data: "Folio",
                            render: function (data, type, row, meta) {
                                return row.Folio;
                            }
                        },
                        {
                            targets: 2,
                            data: "Fecha",
                            render: function (data, type, row, meta) {
                                return row.Fecha;
                            }
                        },
                        {
                            targets: 3,
                            data: "Motivo",
                            render: function (data, type, row, meta) {
                                return row.Motivo;
                            }
                        },
                        {
                            targets: 4,
                            data: "NumeroDetenidos",
                            render: function (data, type, row, meta) {
                                return row.NumeroDetenidos;
                            }
                        },
                        {
                            targets: 5,
                            data: "Detenidos",
                            render: function (data, type, row, meta) {
                                return row.Detenidos;
                            }
                        },
                        {
                            targets: 6,
                            data: "Estado",
                            render: function (data, type, row, meta) {
                                return "<div class='wrapping'>" + row.Estado + "</div>";
                            }
                        },
                        {
                            targets: 7,
                            data: "Municipio",
                            render: function (data, type, row, meta) {
                                return row.Municipio;
                            }
                        },
                        {
                            targets: 8,
                            data: "Colonia",
                            render: function (data, type, row, meta) {
                                return row.Colonia;
                            }
                        },
                        {
                            targets: 9,
                            data: "Numero",
                            render: function (data, type, row, meta) {
                                return row.Numero;
                            }
                        },
                        {
                            targets: 10,
                            data: "EntreCalle",
                            render: function (data, type, row, meta) {
                                return row.EntreCalle;
                            }
                        },
                        {
                            targets: 11,
                            data: "Responsable",
                            render: function (data, type, row, meta) {
                                return "<div class='wrapping'>" + row.Responsable + "</div>";
                            }
                        },
                        {
                            targets: 12,
                            data: "Descripcion",
                            render: function (data, type, row, meta) {
                                return "<div class='wrapping'>" + row.Descripcion + "</div>";
                            }
                        }
                    ]
                });
            }

            function init() {
                if (param !== undefined) {
                    obtenerEvento(param);
                    $('#add').show();
                    $('#add').attr("data-tracking", param);
                    cargarPaisEstadoMunicipio(false);

                }
                else {
                    //loadCountries("73");
                    //loadStates("0", 73);
                    loadMotivos("0");

                    cargarPaisEstadoMunicipio(true);
                    loadFolio();
                }
                loadCalls("0");
                loadInstitucionesAW("0");
                loadUnidadesAW("0", "");
                loadCities(1918, 26);
                if (param !== undefined) {
                    obtenerEvento(param);
                    $('#add').show();
                    $('#add').attr("data-tracking", param);
                    cargarPaisEstadoMunicipio(false);
                }
                else {
                    //loadCountries("73");
                    //loadStates("0", 73);
                    cargarPaisEstadoMunicipio(true);
                    loadFolio();
                }
            }

            function cargarPaisEstadoMunicipio(cargarCombo) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getPaisEstadoMunicipio",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);

                        if (r.exitoso) {
                            $("#estado").val(r.Estado);
                            $("#municipio").val(r.Municipio);
                            if (cargarCombo) {
                                loadCities("1918", r.Estado);
                                loadNeighborhood("0", "1918");
                            }

                            $("#localizacionHeader").text(r.Localizacion);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de unidades. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });

            }
            function cargarDetenidosinregistro(id) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getDetenidossinregistrobarandilla",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $('#detenido');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Detenido]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        Dropdown.val("0");
                        Dropdown.trigger('change');
                        $("#detenido").next().children().children().focus();
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
            function cargarDetenido(id) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getDetenidos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: id
                    }),
                    success: function (response) {
                        var Dropdown = $('#detenido');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Detenido]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        Dropdown.val("0");
                        Dropdown.trigger('change');
                        $("#detenido").next().children().children().focus();
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function cargarEvento(tracking) {
                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/getEvento",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        tracking: tracking
                    }),
                    success: function (response) {
                        var Dropdown = $('#evento');
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        Dropdown.trigger('change');
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar el evento. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function ObtenerValores() {
                var datos = [
                    eventoId = $('#evento').val(),
                    detenidoId = $('#detenido').val()
                ];

                return datos;
            }

            function validar() {
                var esvalido = true;

                if ($("#detenido").val() == null || $("#detenido").val() == 0) {
                    ShowError("Detenido", "El campo de detenido es obligatorio.");
                    $('#detenido').parent().removeClass('state-success').addClass("state-error");
                    $('#detenido').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#detenido').parent().removeClass("state-error").addClass('state-success');
                    $('#detenido').addClass('valid');
                }

                return esvalido;
            }

            function obtenerDatosTabla(eventoId) {
                var dataArreglo = new Array();
                var DetenidoEvento;

                $('#dt_nuevo_detenido tbody tr').each(function (i, row) {
                    DetenidoEvento = {};
                    if (row.childNodes[2] !== undefined && row.childNodes[3] !== undefined && row.childNodes[4] !== undefined &&
                        row.childNodes[5] !== undefined) {
                        if (row.childNodes[0].childNodes[0].getAttribute('data-trackingid') === "0") {
                            DetenidoEvento = {};
                            DetenidoEvento.EventoId = eventoId;
                            DetenidoEvento.Nombre = this.childNodes[2].childNodes[0].value;
                            DetenidoEvento.Paterno = this.childNodes[3].childNodes[0].value;
                            DetenidoEvento.Materno = this.childNodes[4].childNodes[0].value;
                            DetenidoEvento.SexoId = this.childNodes[5].childNodes[0].value
                            DetenidoEvento.Motivo = this.childNodes[6].childNodes[0].value
                            DetenidoEvento.DetalleDetencionId = this.childNodes[7].childNodes[0].getAttribute("data-DetalledetencionId");
                            dataArreglo.push(DetenidoEvento);
                        }
                    }
                });

                $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    DetenidoEvento = {};
                    /*
                    DetenidoEvento.EventoId = eventoId;
                    DetenidoEvento.Nombre = this.node().childNodes[1].childNodes[0].value;
                    DetenidoEvento.Paterno = this.node().childNodes[2].childNodes[0].value;
                    DetenidoEvento.Materno = this.node().childNodes[3].childNodes[0].value;
                    DetenidoEvento.SexoId = this.node().childNodes[4].childNodes[0].value;
                   // DetenidoEvento.Edad = this.node().childNodes[5].childNodes[0].value;
                    DetenidoEvento.Motivo = this.node().childNodes[5].childNodes[0].value;

                    dataArreglo.push(DetenidoEvento);*/

                    if (this.node().childNodes[0].childNodes[0].checked) {
                        DetenidoEvento = {};
                        DetenidoEvento.EventoId = eventoId;
                        DetenidoEvento.Nombre = this.node().childNodes[2].childNodes[0].value;
                        DetenidoEvento.Paterno = this.node().childNodes[3].childNodes[0].value;
                        DetenidoEvento.Materno = this.node().childNodes[4].childNodes[0].value;
                        DetenidoEvento.SexoId = this.node().childNodes[5].childNodes[0].value;
                        DetenidoEvento.Motivo = this.node().childNodes[6].childNodes[0].value;
                        DetenidoEvento.DetalleDetencionId = this.node().childNodes[7].childNodes[0].getAttribute("data-DetalledetencionId");
                        dataArreglo.push(DetenidoEvento);
                    }
                });

                return dataArreglo;
            }

            function GuardarDetenido() {
                startLoading();

                var datos = ObtenerValores();


                $.ajax({
                    type: "POST",
                    url: "eventsform.aspx/saveDetenidoBarandilla",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        datos: datos,
                    }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso && resultado.alerta == false) {
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del detenido se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del detenido se " + resultado.mensaje + " correctamente.");

                            $("#evento-reciente-modal").modal("hide");
                            location.href = "entry.aspx?tracking=" + resultado.id;

                        }
                        else if (resultado.exitoso && resultado.alerta == true) {
                            limpiar();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-times'></i><strong>Atención! </strong>" +
                                resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowAlert("Atención!", resultado.mensaje);
                            $('#main').waitMe('hide');

                            $("#evento-reciente-modal").modal("hide");

                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. ");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("body").on("click", "#saveDetenido", function () {
                $("#ctl00_contenido_lblMessage").html("");

                if (validar()) {
                    GuardarDetenido();
                }
            });


            $("#save_").on("click", function () {
                startLoading();
                var $items = $('[data-requerido]');

                var items_validar = $('[data-requerido="true"]');

                if (!camposVacios(items_validar)) {
                    endLoading();
                    return false;
                }

                var latitud;
                var longitud;

                var obj = generarObjeto($items);
                obj.tracking = param !== undefined ? param : "";
                obj.llamada = $("#llamada").val();
                obj.unidadAW = $("#alertaWebUnidad").val();
                obj.motivoevento=$("#motivoevento").val();

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/save",
                    data: JSON.stringify({ csObj: obj }),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso) {
                            $("#folio").val(response.folio);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del evento se " + response.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del evento se " + response.mensaje + " correctamente.");
                            $('#main').waitMe('hide');

                            if (obj.tracking == "") {
                                var redirect = "";
                                redirect = " <%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/eventsform.aspx?tracking=" + response.tracking;
                                setTimeout(window.location.assign(redirect), 1000);
                            }
                            $('#linkUnidad').show();
                            $('#linkDetenidos').show();
                            $('#add').show();

                            $("#btnGuardarUnidad").attr("data-EventoId", response.EventoId);
                            $("#btnGuardarUnidad").attr("data-tracking", response.tracking);

                            $("#btnGuardarDetenido").attr("data-EventoId", response.EventoId);
                            $("#btnGuardarDetenido").attr("data-tracking", response.tracking);

                            $('#add').attr("data-tracking", response.tracking);
                            //alert(response.tracking);
                            param = response.tracking;

                            window.emptytable = false;
                            responsiveHelper_dt_basic_tabla_eventos = undefined;
                            $("#dt_basic_tabla_eventos").DataTable().destroy();

                            latitud = $("#latitud").val();
                            longitud = $("#longitud").val();

                            loadEventosAW($("#alertaWebUnidad").val());

                            $("#latitud").val(latitud);
                            $("#longitud").val(longitud);
                        }
                        else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico. " + response.mensaje);

                            if (response.fallo == 'fecha') {
                                var fecha = document.getElementById("fecha");
                                fecha.parentNode.classList.remove('state-success');
                                fecha.parentNode.classList.add('state-error');
                            }
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });



            });

            $("#btnGuardarUnidad").on("click", function () {
                startLoading();
                var $items = $('[data-requerido-unidad]');

                if (!validarUnidad()) {
                    endLoading();
                    return false;
                }

                var obj = generarObjeto($items);
                obj.EventoId = $(this).attr("data-EventoId");

                if (param == undefined) {
                    param = $(this).attr("data-tracking");
                }

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/saveUnidad",
                    data: JSON.stringify({ csObj: obj }),
                    success: function (response) {
                        var response = JSON.parse(response.d);
                        if (response.exitoso) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información de la unidad se " + response.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información de la unidad se " + response.mensaje + " correctamente.");
                            $('#main').waitMe('hide');

                            $("#modalUnidad").modal("hide");

                            $("#dt_basic_tabla_unidades").emptytable = false;
                            $("#dt_basic_tabla_unidades").DataTable().table().ajax.reload();
                        } else {
                            if (response.alerta) {
                                $('#main').waitMe('hide');
                                ShowAlert("¡Atención!", response.mensaje);
                            }

                            else {
                                ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                                $('#main').waitMe('hide');
                            }
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
            });


            //   $("body").on("click", "#add", function () {
            //    var tracking = $(this).attr("data-tracking");

            //    cargarEvento(tracking);
            //    cargarDetenido(tracking);

            //    $("#evento-reciente-modal").modal("show");
            //});




            $("#btnGuardarDetenido").on("click", function () {
                startLoading();
                $('select').removeClass('errorInputTabla');
                $('input').removeClass('errorInputTabla');
                var $items = $('[data-requerido-detenido]');

                var filas = 0;

                $("#dt_nuevo_detenido").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    if (this.node().childNodes[0].childNodes[0].checked) {
                        filas++;
                    }
                });

                $('#dt_nuevo_detenido tbody tr').each(function (i, row) {
                    if (row.childNodes[2] !== undefined && row.childNodes[3] !== undefined && row.childNodes[4] !== undefined &&
                        row.childNodes[5] !== undefined) {
                        if (row.childNodes[0].childNodes[0].getAttribute('data-trackingid') === "0") {
                            filas++;
                        }
                    }
                });

                if (filas > 0) {
                    if (!validarCamposEnTabla()) {
                        endLoading();
                        return;
                    }
                }
                else {
                    endLoading();
                    return;
                }

                var list2 = obtenerDatosTabla($(this).attr("data-EventoId"));
                var obj = generarObjeto($items);
                obj.EventoId = $(this).attr("data-EventoId");
                obj.NumeroDetenidos = $("#numeroDetenidos").val();
                //obj.FechaNacimiento = $("#fechanacimiento").val();
                obj.Edad = $("#edad").val();
                if (param == undefined) {
                    param = $(this).attr("data-tracking");
                }

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/saveDetenido",
                    data: JSON.stringify({ csObj: obj, list: list2 }),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso && response.alerta && response.mode !== "") {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Atención! </strong>" +
                                response.mode, "<br /></div>");
                            ShowAlert("¡Atención!", response.mode);
                            setTimeout(hideMessage, hideTime);
                            $("#modalDetenido").modal("hide");                            
                            cargarEvento(param);
                            cargarDetenidosinregistro(param);
                            var valor = response.detenidosidenticos;
                            var tracking = response.tracking;

                            if (valor != "0") {
                                location.href = "eventsform.aspx?tracking=" + tracking;
                            }
                            
                            console.log("refresh");
                            window.emptytable = true;
                            $('#dt_basic_tabla_detenidos').DataTable().ajax.reload();
                        }
                        else if (response.exitoso && response.Alertadetenido == false) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se registro satisfactoriamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se registró satisfactoriamente.");
                            setTimeout(hideMessage, hideTime);
                            $("#modalDetenido").modal("hide");
                            //$('#main').waitMe('hide');
                            cargarEvento(param);
                            cargarDetenidosinregistro(param);
                            var valor = response.detenidosidenticos;
                            var tracking = response.tracking;

                            if (valor != "0") {
                                location.href = "eventsform.aspx?tracking=" + tracking;
                            }
                           /* else {
                                $("#evento-reciente-modal").modal("show");
                            }*/
                           

                            // setTimeout( $("#modalDetenido").modal("hide"),95000);	
                            console.log("refresh");
                            window.emptytable = true;
                            $('#dt_basic_tabla_detenidos').DataTable().ajax.reload();
                        }
                        else if (response.exitoso && response.Alertadetenido == true) {
                            limpiar();
                            ShowAlert("¡Atención!", response.Mensajealerta);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se registró satisfactoriamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se registró satisfactoriamente.");

                            setTimeout(hideMessage, hideTime);

                            $('#main').waitMe('hide');

                            setTimeout($("#modalDetenido").modal("hide"), 95000);
                            console.log("refresh");
                            window.emptytable = true;
                            $('#dt_basic').DataTable().ajax.reload();
                        }

                        else {
                            if (response.mensaje == " \n La fecha de nacimiento debe de ser mayor a un año") {
                                ShowError("Fecha de nacimiento", response.mensaje);
                            }
                            else {
                                ShowError("¡Error!", response.mensaje + " Si el problema persiste contacte al personal de soporte técnico.");
                            }
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
            });

            //function closemodaldetenidoevento()
            //{
            //    $("#modalDetenido").modal("hide");
            //}
            $("body").on("click", "#linkUnidad", function () {
                limpiar();
                loadUnidad("0");
                loadResponsable("0");
                $("#modal-unidad-title").html("<i class='fa fa-pencil'></i>Agregar unidad");
                $("#modalUnidad").modal("show");
            });

            $("body").on("click", "#linkDetenidos", function () {
      
                $("#nombreDetenido").val("");
                $("#paternoDetenido").val("");
                $("#maternoDetenido").val("");
                $("#mdnombre").val("");
                $("#mdpaterno").val("");
                $("#mdmaterno").val("");
                $("#mdalias").val("");
                //eliminaFilas2();
                //eliminaFilas();
               // var filtro = obtenercirteriosbusqueda();
               // LoadResultados(filtro);
                //LoadNuevoDetenido();
                //$("#dt_nuevo_detenido").DataTable().destroy();
                //var table = $('#dt_nuevo_detenido').DataTable();
                //table.clear().draw;
                //addrow();
                $('#nombreDetenido').parent().removeClass('state-success');
                $('#nombreDetenido').parent().removeClass("state-error");
                $('#paternoDetenido').parent().removeClass('state-success');
                $('#paternoDetenido').parent().removeClass("state-error");
                $('#maternoDetenido').parent().removeClass('state-success');
                $('#maternoDetenido').parent().removeClass("state-error");
                $('#sexoDetenido').parent().removeClass('state-success');
                $('#sexoDetenido').parent().removeClass("state-error");
                //$('#fechanacimiento').parent().removeClass('state-success');
                //$("#fechanacimiento").val("");
                $('#edad').parent().removeClass('state-success');
                $("#edad").val("");

                datossex = cargarSexo();
                datos = cargaMotivorGrid();

                var filtro = obtenercirteriosbusqueda();
                $("#dt_nuevo_detenido").DataTable().destroy();
                responsiveHelper_dt_basic_detenidos = undefined;
                window.tableNuevoDetenidos = true;
                LoadResultados(filtro);   

                $("#modal-detenido-title").html("<i class='fa fa-pencil'></i>Agregar detenido");

                $("#modalDetenido").modal("show");
            });

            $('#numeroDetenidos').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            });

            $("body").on("click", ".relacionar", function () {
                var obj = {};

                obj.unidadId = $(this).attr("data-unidadId");
                obj.institucionId = $("#alertaWebInstitucion").val();
                obj.tracking = param !== undefined ? param : "";

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processdata: true,
                    traditional: true,
                    url: "eventsform.aspx/relacionarLlamadaAW",
                    data: JSON.stringify({ csObj: obj }),
                    success: function (response) {
                        endLoading();
                        var response = JSON.parse(response.d);
                        if (response.exitoso) {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información de alerta web se relacionó correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información de alerta web se relacionó correctamente.");
                            $('#main').waitMe('hide');
                            $("#modalDetenido").modal("hide");
                        } else {
                            ShowError("¡Error!", "Ocurrió un error, si el problema persiste contacte al personal de soporte técnico." + response.mensaje);
                        }
                    },
                    error: function (error) {
                        console.log("error", error);
                        endLoading();
                    }
                });
            });

            init();

            function limpiar() {
                $('#unidad').parent().removeClass('state-success');
                $('#unidad').parent().removeClass("state-error");
                $('#responsable').parent().removeClass('state-success');
                $('#responsable').parent().removeClass("state-error");
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
        });
       
        function CargarMapa() {
            //  if ($('#latitud').val() == "" || $('#longitud').val() == "") {

            mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
            var coordinates = document.getElementById('coordinates');
            function onDragEnd() {
                var lngLat = marker.getLngLat();
                coordinates.style.display = 'block';
                coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                let x = lngLat.lng;
                let y = x;
                x = x.toString();
                x = x.substring(0, x.length - 3);
                $('#longitud').val(x);

                x = lngLat.lat;
                let z = x;
                x = x.toString();
                x = x.substring(0, x.length - 3);
                $('#latitud').val(x);
            }
            var Latitud = -89.61086650942;
            var longitud = 20.97689912377;
            Latitud = $("#L1").val();
            longitud = $("#L2").val();

            var map = new mapboxgl.Map({
                container: 'mapid',
                style: 'mapbox://styles/mapbox/streets-v11',
                center: [longitud, Latitud],
                zoom: 16
            });
            var marker = new mapboxgl.Marker({
                draggable: true
            })

                .setLngLat([longitud, Latitud])
                .addTo(map);
            map.addControl(new mapboxgl.NavigationControl());
            marker.on('dragend', onDragEnd);

            //else {

            //    var mymap = L.map('mapid').setView([$('#latitud').val(), $('#longitud').val()], zoom = 16, 13, 16);

            //    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            //        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            //        maxZoom: 18,
            //        id: 'mapbox.streets',
            //        accessToken: 'pk.eyJ1IjoiYXNlc29ydXNpdGVjaCIsImEiOiJjanhtbzh6aW0wNXIwM2NvNjVweHlnd2JxIn0.YbBuq1IIm9cVDgg64NaxcQ'
            //    }).addTo(mymap);

            //    var marker = L.marker([$('#latitud').val(), $('#longitud').val()]).addTo(mymap);

            //}
        }
    </script>
    <script>
        setTimeout(function () {
            if ($('#latitud').val() == "" || $('#longitud').val() == "") {
                mapboxgl.accessToken = 'pk.eyJ1IjoiZWV0aWVubmVmdiIsImEiOiJjanh6cHpsMnQwM2V6M2huNDdkdm9mazk1In0.epgjScAyuVhfzrc1HadIvw';
                var coordinates = document.getElementById('coordinates');
                function onDragEnd() {
                    var lngLat = marker.getLngLat();
                    coordinates.style.display = 'block';
                    coordinates.innerHTML = 'Longitud: ' + lngLat.lng + '<br />Latitud: ' + lngLat.lat;
                    let x = lngLat.lng;
                    let y = x;
                    x = x.toString();
                    x = x.substring(0, x.length - 3);
                    $('#longitud').val(x);

                    x = lngLat.lat;
                    let z = x;
                    x = x.toString();
                    x = x.substring(0, x.length - 3);
                    $('#latitud').val(x);
                }
                var Latitud = -89.61086650942;
                var longitud = 20.97689912377;
                Latitud = $("#L1").val();
                longitud = $("#L2").val();

                $('#latitud').val(Latitud);
                $('#longitud').val(longitud);

                //alert($("#L1").val() + "  " + $("#L2").val());
                var map = new mapboxgl.Map({
                    container: 'mapid',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [longitud, Latitud],
                    zoom: 16
                });
                var marker = new mapboxgl.Marker({
                    draggable: true
                })

                    .setLngLat([longitud, Latitud])
                    .addTo(map);
                map.addControl(new mapboxgl.NavigationControl());
                marker.on('dragend', onDragEnd);
            }
            else {

                //var mymap = L.map('mapid').setView([$('#latitud').val(), $('#longitud').val(), zoom = 16], 13);



                //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                //    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                //    maxZoom: 18,
                //    id: 'mapbox.streets',
                //    accessToken: 'pk.eyJ1IjoiYXNlc29ydXNpdGVjaCIsImEiOiJjanhtbzh6aW0wNXIwM2NvNjVweHlnd2JxIn0.YbBuq1IIm9cVDgg64NaxcQ'
                //}).addTo(mymap);


                //var marker = L.marker([$('#latitud').val(), $('#longitud').val()]).addTo(mymap);

            }

        }, 2500)

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<%= ConfigurationManager.AppSettings["ApiKeyGoogle"]  %>&callback=initMap">
    </script>

</asp:Content>
     
