﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="modulodefensor.aspx.cs" Inherits="Web.Application.Sentence.modulodefensor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
  <li>Defensor público</li>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #dropdown {
            width: 439px;
        }
        
     
        /*input[type=text]:disabled{
            background-color:lightgrey;
            cursor:not-allowed;
        }*/
        /*input[type=datetime]:disabled{
            background-color:lightgrey;
            cursor:not-allowed;
        }*/
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
    </style>
    
<style>
        table {

          overflow-x:auto;
        }
        table td {
          word-wrap: break-word;
          max-width: 400px;
        }
        #dt_basic_observaciones td {
          white-space:inherit;
        }
</style>
    <div class="scroll">
        <!--
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
                Defensor público
            </h1>
        </div>
    </div>-->


    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry" style="display: none;">
        <section class="col col-4">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                
            </div>
        </section>        
    </div>
    <p></p>
         <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-list-alt"></i>
                Defensor público
            </h1>
        </div>
    </div>
    <section id="widget-grid" class="">
        <div class="row">            
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-detenidos-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Detenidos en existencia </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                        <%--<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">--%>
                            <table id="dt_basic" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th >Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <th data-hide="phone,tablet">No. remisión</th>                                     
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <div class="modal fade" id="form-modal-observacion" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form ">
                        <div class="row">
                            <asp:Label ID="Label2" runat="server"></asp:Label>
                        </div>
                        <div class="row">
                            <fieldset>                                                                
                                <section>
                                    <label style="color: dodgerblue" class="input">Observación <a style="color: red">*</a></label>
                                    <label class="input" id="contObs">
                                        <i class="icon-append fa fa-archive"></i>
                                        <input type="text" name="observacion" id="observacion" placeholder="Observación" maxlength="512" class="alphanumeric alptext" />
                                        <b class="tooltip tooltip-bottom-right">Ingresa la observación.</b>
                                    </label>
                                </section>                                                              
                            </fieldset>
                            
                        </div>                        
                        <div class="row">
                            <fieldset>
                                <table id="dt_basic_observaciones" class="table table-striped table-bordered dt-responsive nowrap" width="100%">                         
                                    <thead>
                                        <tr>                                        
                                            <th data-class="expand">#</th>
                                            <th>Observación</th>
                                            <th>Fecha</th>
                                            <th>Usuario</th>                                        
                                            <th data-hide="phone,tablet">Acciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </fieldset>
                        </div>
                            <footer>
                                <div class="row" style="display: inline-block; float: right">
                                    <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btnSaveObservacion"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                    <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelObservacion"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                            
                                </div>
                            </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div id="historyobservacion-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorialobservacion"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-sm-12">
                        <table id="dt_basichistoryobs" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th data-hiden="tablet,fablet,phone">Observaciones</th>
									<th>Movimiento</th>
                                    <th data-hiden="tablet,fablet,phone">Realizado por</th>
                                    <th>Fecha movimiento</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                        </div>
                </div>
            </div>
    </div>
 </div>
    <div class="modal fade" id="form-modal-historial" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-historial"></h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form ">                        
                        <div class="row">
                            <fieldset>
                                <table id="dt_basic_historial" class="table table-striped table-bordered table-hover" width="100%">                         
                                    <thead>
                                        <tr>                                        
                                            <th data-class="expand">#</th>
                                            <th>Movimiento</th>
                                            <th>Fecha</th>
                                            <th>Usuario</th>                                                                                    
                                        </tr>
                                    </thead>
                                </table>
                            </fieldset>
                            <footer>
                                <a class="btn btn-sm btn-default" data-dismiss="modal" id="btnCancelHistorial"><i class="fa fa-close"></i>&nbsp;Cerrar </a>                                                            
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="blockitem-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="Div1" class="smart-form" style="display:block;width:100%">
                        <fieldset>
                            <section>
                                <table id="tableblock" class="table-responsive" width="100%">
                                   <tbody>
                                <tr>
                                    <td>
                                <p class="center" >
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro <strong>&nbsp;<span id="itemnameblock" "></span></strong>?
                                </p>
                                    </td>   
                                    </tr>
                                       </tbody>
                                    </table>
                            </section>
                        </fieldset>
                        <footer>                            
                           
                             <button type="button" class="btn btn-sm btn-default cancel" data-dismiss="modal" >
                                    <i class="fa fa-times bigger-120"></i>&nbsp;Cancelar
                            </button>
                             <button type="button" class="btn btn-sm btn-default save"  id="btncontinuar">
                                     <i class="fa fa-save bigger-120"></i>&nbsp;Continuar
                            </button>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>    
     <input type="hidden" id="HQLNBB" runat="server" value="" />
     <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
     <input type="hidden" id="WERQEQ" runat="server" value=""/>  
     </div>
     <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="icon-append fa fa-user"></i>&nbsp;Datos personales</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Estatura:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Peso:</label>
                            <div class="col-md-8">
                                <input id="pesoInfo" class="form-control" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" width="240" height="240"/>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <div id="photo-arrested" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Fotografía del detenido</h4>
                </div>
                <div class="modal-body">
                    <div id="photo-arrested-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="text-center">
                                    <img id="foto_detenido" class="img-thumbnail text-center" src=" <%= ConfigurationManager.AppSettings["relativepath"]  %> #" alt="fotografía del detenido" />


                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cerrar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <input type="hidden" id="IdHistOb" />
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () { 

            window.addEventListener("keydown", function (e) {
                if (e.ctrlKey && e.keyCode === 71) {
                    e.preventDefault();
                    if ($("#form-modal-observacion").is(":visible")) {
                        document.getElementById("btnSaveObservacion").click();
                    }
                }


            });


            var rutaDefaultServer = "";

            getRutaDefaultServer();

            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "modulodefensor.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }                                             
                    }
                });
            }

             var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
                 $("#ctl00_contenido_Label2").html("");
            }
            $("#btnSaveObservacion").click(function () {

                if (validarObservacion()) {
                    data = [
                        Id = $("#btnSaveObservacion").attr("data-id"),
                        TrackingId = $("#btnSaveObservacion").attr("data-trackingid"),
                        Observacion = $("#observacion").val(),
                        TrackingInternoId = $("#btnSaveObservacion").attr("data-internoid")
                    ];

                    guardarObservacion(data);
                }
                
            });

            function clearModalObservaciones() {
                $("#contObs").removeClass('state-success');
                $("#contObs").removeClass('state-error');
                $("#contObs").removeClass('valid');
                $("#observacion").val("");
                $("#btnSaveObservacion").attr("data-trackingid", "");
                $("#btnSaveObservacion").attr("data-id", "");
            }

            function validarObservacion() {
                var esvalido = true;

                if ($("#observacion").val().split(" ").join("") == "") {
                    ShowError("Observación", "La observación es obligatoria.");
                    $('#observacion').parent().removeClass('state-success').addClass("state-error");
                    $('#observacion').removeClass('valid');
                    esvalido = false;
                }
                else
                {
                    $('#observacion').parent().removeClass("state-error").addClass('state-success');
                    $('#observacion').addClass('valid');
                }

                return esvalido;
            }

            function guardarObservacion(datos) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "modulodefensor.aspx/saveObservacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            clearModalObservaciones();
                            $("#dt_basic_observaciones").DataTable().ajax.reload();                                                        
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_Label2").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");
                            setTimeout(hideMessage, hideTime);
                        } else {
                            ShowError("¡Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("body").on("click", ".historial", function () {
                var tracking = $(this).attr("data-id");
                responsiveHelper_dt_basic_his = undefined;
                $('#dt_basic_historial').DataTable().destroy();
                loadTableHistorial(tracking);
                $("#form-modal-historial").modal('show');
                $("#form-modal-title-historial").empty();
                $("#form-modal-title-historial").html("Movimientos históricos del detenido");
            });

            $("body").on("click", ".observaciones", function () {

                var uno = JSON.parse($("#ctl00_contenido_HQLNBB").val());
                var dos = JSON.parse($("#ctl00_contenido_KAQWPK").val());
                var tres = JSON.parse($("#ctl00_contenido_LCADLW").val());

                if (uno && dos && tres) {
                    $("#observacion").removeAttr("disabled");
                    $("#btnSaveObservacion").show();
                }
                else {
                    $("#observacion").attr("disabled", "disabled");
                    $("#btnSaveObservacion").hide();
                }

                var tracking = $(this).attr("data-id");
                responsiveHelper_dt_basic_obs = undefined;
                $('#dt_basic_observaciones').DataTable().destroy();
                loadTableObservaciones(tracking);
                $("#btnSaveObservacion").attr("data-internoid", tracking);
                $("#btnSaveObservacion").attr("data-id", "");
                $("#btnSaveObservacion").attr("data-trackingid", "");
                $("#observacion").val("");
                $("#form-modal-observacion").modal('show');
                $("#form-modal-title").empty();
                $("#form-modal-title").html('<i class="glyphicon glyphicon-eye-open"></i> Listado de observaciones');
            });

            $("body").on("click", ".editObservacion", function () {         
                $("#observacion").val("");
                $("#btnSaveObservacion").attr("data-trackingid", $(this).attr("data-trackingid"));
                $("#btnSaveObservacion").attr("data-id", $(this).attr("data-id"));
                $("#observacion").val($(this).attr("data-obs"));
            });

            $("body").on("click", ".blockObservacion", function () {
                var subblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(subblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal('show');
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingid = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "modulodefensor.aspx/blockObservacion",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            $("#dt_basic_observaciones").DataTable().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_Label2").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                " ¡Bien hecho! Registro " + JSON.parse(data.d).message + " con éxito.</div>");
                            ShowSuccess("¡Bien hecho!", "Registro " + JSON.parse(data.d).message + " con éxito.");
                            setTimeout(hideMessage, hideTime);
                        }
                        else {
                            $("#ctl00_contenido_Label2").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del contrato. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del contrato. . Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });

            $("#addObservacion").click(function () {
                $("#observacion").val("");
                $("#form-modal-observacion").modal('show');
            });

            pageSetUp();

            //Variables para tabla principal
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            //Variables para tabla de observaciones
            var responsiveHelper_dt_basic_obs = undefined;
            var responsiveHelper_datatable_fixed_column_obs = undefined;
            var responsiveHelper_datatable_col_reorder_obs = undefined;
            var responsiveHelper_datatable_tabletools_obs = undefined;

            //Variables para tabla de historial
            var responsiveHelper_dt_basic_his = undefined;
            var responsiveHelper_datatable_fixed_column_his = undefined;
            var responsiveHelper_datatable_col_reorder_his = undefined;
            var responsiveHelper_datatable_tabletools_his = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            function loadTableObservaciones(tracking) {
                $('#dt_basic_observaciones').dataTable({
                    "lengthMenu": [5, 10, 25, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    //autoWidth: true,
                  
                    
                       "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",                    
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "autoWidth": true,
                    //"oLanguage": {
                    //    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    //},
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_obs) {
                            responsiveHelper_dt_basic_obs = new ResponsiveDatatablesHelper($('#dt_basic_observaciones'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_obs.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_obs.respond();
                        $('#dt_basic_observaciones').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                        }
                    },
                    ajax: {
                        type: "POST",
                        url: "modulodefensor.aspx/getDataObservaciones",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_observaciones').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.TrackingId = tracking;
                            return JSON.stringify(parametrosServerSide);
                        }

                    },
               
                  
                    columns: [                        
                        {
                            name: "TrackingId",
                            data: "TrackingId"
                        },
                        {
                            name: "Observaciones",
                            data: "Observaciones"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "Usuario",
                            data: "Usuario"
                        },                                       
                        null                        
                    ],
                    columnDefs: [                        
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },                                               
                        {
                            targets: 4,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var edit = "editObservacion";
                                var editar = "";
                                var color = "";
                                var txtestatus = "";
                                var icon = "";
                                var habilitar = "";

                                if (row.Habilitado) {
                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                }
                                else {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                                }

                                if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-id="' + row.Id + '" data-trackingid="' + row.TrackingId + '" data-obs="' + row.Observaciones + '" title = "Editar" > <i class="glyphicon glyphicon-pencil"></i></a >&nbsp;';                               
                                if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockObservacion" href="javascript:void(0);" data-value="' + row.Observaciones + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';                                                                                           

                                return editar + habilitar + '<a name="history" class="btn btn-default btn-circle historialOb" href="javascript:void(0);" data-value="' + row.Observaciones + '" data-id="' + row.Id + '" title="Historial"><i class="fa fa-history fa-lg"></i></a>';
                            }
                        }
                    ],
                         

                });
            }

                $("#form-modal-observacion").on('shown.bs.modal', function(){
                 var table = $('#dt_basic_observaciones').DataTable();
                        //console.log("cargadatos2");
                        table.columns.adjust().draw();
            });
            function loadTableHistorial(tracking) {
                $('#dt_basic_historial').dataTable({
                    "lengthMenu": [5, 10, 25, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                 "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",                    
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "autoWidth": true,
                    //"oLanguage": {
                    //    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    //},
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_his) {
                            responsiveHelper_dt_basic_his = new ResponsiveDatatablesHelper($('#dt_basic_historial'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_his.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_his.respond();
                        $('#dt_basic_historial').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');
                            $('td', row).eq(3).addClass('strikeout');
                        }
                    },
                    "order": [[2, "asc"]],
                    ajax: {
                        type: "POST",
                        url: "modulodefensor.aspx/getDataHistorial",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_historial').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.TrackingId = tracking;
                            return JSON.stringify(parametrosServerSide);
                        }

                    },
                    columns: [                        
                        {
                            name: "TrackingId",
                            data: "TrackingId"
                        },
                        {
                            name: "Movimiento",
                            data: "Movimiento"
                        },
                        {
                            name: "Fecha",
                            data: "Fecha"
                        },
                        {
                            name: "Usuario",
                            data: "Usuario"
                        }                                                                             
                    ],
                    columnDefs: [                        
                        {
                            targets: 0,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        }                                                                      
                    ]
                });
            }

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "modulodefensor.aspx/getdata",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });

                        parametrosServerSide.emptytable = false;
                        return JSON.stringify(parametrosServerSide);
                    }

                },
                columns: [
                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    null,
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
               
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    null,
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false

                    }
                ],
                columnDefs: [

                    {
                        data: "TrackingId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },       {
                        targets: 2,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.RutaImagen != null) {
                                var ext = "." + row.RutaImagen.split('.').pop();
                                var photo = row.RutaImagen.replace(ext, ".thumb");
                                var imgAvatar = resolveUrl(photo);
                                return '<div class="text-center">' +
                                    '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                    '<img id="avatar" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                    '</a>' +
                                    '<div>';
                            } else {
                                pathfoto = resolveUrl("~/Content/img/avatars/male.png");
                                return '<div class="text-center">'+
                                    '<img id="avatar" class="img-thumbnail text-center" alt = "" src = "' + pathfoto + '" height = "10" width = "50" onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';"/>' +
                                    '<div>';
                            }
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            var estatus = "Pendiente";                               

                            return row.EstatusNombre;
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        width: "400px",
                        render: function (data, type, row, meta) {
                            var edit = "edit";
                            var editar = "";
                            var color = "";
                            var txtestatus = "";
                            var icon = "";
                            var habilitar = "";                            

                            if (row.Habilitado) {
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            else {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success";
                            }


                         //   if ($("#ctl00_contenido_KAQWPK").val() == "true")
                              //  editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                           // else if($("#ctl00_contenido_WERQEQ").val() == "true")
                               // editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="registersentence.aspx?tracking=' + row.TrackingId + '&nuevo=2' + '" title="Consultar"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';

                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockitem" href="javascript:void(0);" data-value="' + row.Proceso + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>&nbsp;';
                            
                            var action5 = '<a class="btn datos btn-primary href="#"  id="datos" data-id="' + row.TrackingId + '" title="Datos personales"><i class="fa fa-bars"></i> Datos personales</a>&nbsp;';
                            var action6 = '';
                            var action7 = '';
                            //if (row.ObservacionId != 0) {
                            
                            action6 = '<a class="btn observaciones bg-color-red txt-color-white href="#" data-id="' + row.TrackingId + '" title="Observaciones"><i class="glyphicon glyphicon-eye-open"></i> Observaciones</a>&nbsp;';                                                       
                            action7 = '<a class="btn historial bg-color-yellow txt-color-white href="#" data-id="' + row.TrackingId + '" title="Historial de movimientos"><i class="fa fa-history fa-lg"></i> Historial</a>&nbsp;';
                            //}
                            //else {
                            //    action6 = '<a class="btn observaciones btn-primary disabled href="#" data-id="' + row.TrackingId + '" title="Observaciones">Observaciones</a>&nbsp;';
                            //}

                            return action5 + action6 + action7;
                        }
                    }
                ]

            });
            var responsableHelper_dt_basichistoryOb = undefined;
            var breakpointHistoryObDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };
            window.emptytablehistoryOb = true;
            window.tablehistoryOb = $("#dt_basichistoryobs").dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsableHelper_dt_basichistoryOb) {
                        responsableHelper_dt_basichistoryOb = new ResponsiveDatatablesHelper($("#dt_basichistoryobs"), breakpointHistoryObDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsableHelper_dt_basichistoryOb.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings)
                {
                    responsableHelper_dt_basichistoryOb.respond();
                    $("#dt_basichistoryobs").waitMe("hide");
                },
                ajax: {
                    type: "POST",
                    url: "modulodefensor.aspx/GetObservacionesLog",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosserverside) {
                        $("#dt_basichistoryobs").waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        var centroid = $('#IdHistOb').val() || "0";
                        parametrosserverside.centroid = centroid;
                        parametrosserverside.todoscancelados = false;
                        parametrosserverside.emptytable = emptytablehistoryOb;
                        return JSON.stringify(parametrosserverside);
                    }
                },
                columns: [
                    {
                        name: "Observaciones",
                        data: "Observaciones",
                        ordertable: false
                    },
                    {
                        name: "Accion",
                        data: "Accion",
                        ordertable: false
                    },

                    {
                        name: "Creadopor",
                        data: "Creadopor",
                        ordertable: false
                    }
                    ,
                    {
                        name: "Fec_Movto",
                        data: "Fec_Movto",
                        ordertable: false
                    }
                ]
            });

            $("body").on("click", ".historialOb", function () {
                var id = $(this).attr("data-id");
                $("#IdHistOb").val(id);
                var descripcion = $(this).attr("data-value");
                $("#descripcionhistorialobservacion").text(descripcion);

                $("#historyobservacion-modal").modal("show");
                window.emptytablehistoryOb = false;
                window.tablehistoryOb.api().ajax.reload();
            });
            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);

                $("#foto_detenido").error(function () {
                    $(this).unbind("error").attr("src", rutaDefaultServer);
                });
                $("#photo-arrested").modal("show");
            });

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");

                window.emptytable = true;
                window.table.api().ajax.reload();
            });                                                                                                                                  

            if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                $("#addentry").show();
            }                                                                                           

             $("body").on("click", ".datos", function () {
                var id = $(this).attr("data-id");
                CargarDatosPersonales(id);
                
            });

            function CargarDatosPersonales(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "modulodefensor.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Estatura);
                            $("#pesoInfo").val(resultado.obj.Peso);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);

                            var imagenAvatar = resolveUrl(resultado.obj.RutaImagen);
                            if (imagenAvatar != "")
                                $('#imgInfo').attr("src", imagenAvatar);

                            $("#datospersonales-modal").modal("show");
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
        });
    </script>
</asp:Content>
