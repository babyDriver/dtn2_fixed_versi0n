﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="calificacion.aspx.cs" Inherits="Web.Application.Sentence.Calificacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Sentence/sentence_entrylist.aspx">Juez calificador</a></li>
    <li>Calificación del detenido</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style>
        
        td.strikeout {
            text-decoration: line-through;
        }

        input[type="number"]:disabled {



            background: #eee;
            cursor: not-allowed;
        }

        .input-group-addon
        {
             background-color:#FFF;
        }

        .input-group-addon2
        {
             background-color:#808080;
        }

        .input-group .input-group-addon + .form-control
        {
            border-left:none;
        }        
      
        /*input[type=text]:disabled{
            background-color:lightgrey;
            cursor:not-allowed;
        }*/
        input[type=datetime]:disabled{
            background-color:lightgrey;
            cursor:not-allowed;
        }
        .same-height-thumbnail{
            height: 150px;
            margin-top:0;
        }
        #fundamento
        {
            overflow-y:scroll;
        }
         #ScrollableContent {
            height: calc(100vh - 400px);
        }
    </style>

 
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
     

        <section id="widget-grid" class="">
        <div class="row" style="margin:0;">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id=""  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="true">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Información del detenido - Datos generales</h2>
                        <a id="showInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: none;" class="link" title="Ver"><i class="glyphicon glyphicon-plus" style="margin-right: 10px;"></i></a>&nbsp;
                        <a id="hideInfoDetenido" style="color: white; float: right; margin-right: 5px; margin-top: 5px; display: block;" class="link" title="Cerrar"><i class="glyphicon glyphicon-minus" style="margin-right: 10px;"></i></a>&nbsp;
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body" id="SectionInfoDetenido">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <table class="table-responsive">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                                <img width="150" height="180" align="center" class="img-circle" id="avatar" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'"/>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td colspan="2">
                                                <br />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
                                                                <td>&nbsp; <small><span id="nombreInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>
                                                                    <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="edadInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
                                                                <td>&nbsp;  <small><span id="sexoInterno"></span></small>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Domicilio:</strong>
                                                                    <br />
                                                                </span></th>
                                                                <td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
                                    <table class="table-responsive">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <table class="table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
                                                                <td>&nbsp; <small><span id="centroInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">No. remisión:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Colonia:</strong> </span></th>
                                                                <td>&nbsp; <small><span id="coloniaInterno"></span></small></td>
                                                            </tr>
                                                            <tr>
                                                                <th><span><strong style="color: #006ead;">Municipio:</strong></span></th>
                                                                <td>&nbsp; <small><span id="municipioInterno"></span></small></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <a href="javascript:void(0);" class="btn btn-md btn-primary detencion" id="add"><i class="fa fa-plus"></i>&nbsp;Información de detención </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
       <div class="scroll" id="ScrollableContent">
      <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-calificacion-2" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Calificación del detenido</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body">
                            <div class="row">                                                   
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="smart-form">
                                            <fieldset>
                                                <legend>Datos de la calificación</legend>
                                                <div class="row">
                                                    <section class="col col-6">
                                                        <label class="label">Situación del detenido <a style="color: red">*</a></label>
                                                        <label class="select">
                                                            <select name="situacion" id="situacion"></select>
                                                            <i></i>
                                                        </label>
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="label">Institución</label>
                                                        <label class="select">
                                                            <select name="institucion" id="institucion" disabled="disabled"></select>
                                                            <i></i>
                                                        </label>
                                                    </section>
                                                    <section class="col col-lg-12">
                                                        <label class="label">Fundamento <a style="color: red">*</a></label>
                                                        <label class="textarea">
                                                            <textarea id="fundamento" name="fundamento" placeholder="Fundamento" rows="10" style="width:100%" maxlength="6800"></textarea>
                                                            <b class="tooltip tooltip-bottom-right">Ingresa el fundamento.</b>
                                                        </label>
                                                    </section>
                                                    <section class="col col-lg-12" align="center">                                                                                                                
                                                        <label class="label"></label>
                                                        <div align="center">
                                                            ¿Se canaliza a trabajo social?
                                                            Si <input type="radio" id="siTrabajo" name="trabajoSocial" />
                                                            No <input type="radio" id="noTrabajo" name="trabajoSocial" />                                                            
                                                        </div>
                                                    </section> 
                                                </div>
                                            </fieldset>
                                        </div>  
                                        <div class="row">
                                            <div class="col-lg-12">            
                                                <section class="col col-lg-9"></section>
                                                <section class="col col-lg-2" align="right">
                                                    <a href="javascript:void(0);" class="btn btn-default" id="addMotivo" title="Guardar"><i class="fa fa-plus"></i>&nbsp;Agregar motivo </a>
                                                </section>                              
                                                <section class="col col-lg-1"></section>
                                            </div>                                        
                                        </div>
                                        <br />
                                        <div class="smart-form">                                                    
                                            <div class="row">                                                                                                                       
                                                <div class="col-lg-12">            
                                                    <section class="col col-lg-1"></section>
                                                    <section class="col col-lg-10">                                                                      
                                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                                <tr>                                                                            
                                                                    <th>#</th>
                                                                    <th data-class="expand">Artículo</th>                                                        
                                                                    <th data-hide="phone,tablet">Motivo de detención</th>
                                                                    <th data-hide="phone,tablet">Tipo</th>
                                                                    <th data-hide="phone,tablet">Hrs</th>  
                                                                    <th data-hide="phone,tablet">Multa</th>
                                                                    <th data-hide="phone,tablet">Observación</th>
                                                                    <th data-hide="phone,tablet">Acciones</th>
                                                                </tr>
                                                            </thead>
                                                        </table>           
                                                    </section>                                                            
                                                    <section class="col col-lg-1"></section>
                                                </div>                                                            
                                            </div>   
                                            <div class="row">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-3">
                                                    <section class="col col-lg-12">
                                                        <label class="label">Total de horas</label>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                            <input type="text" name="totalDeHoras" id="totalDeHoras" class="numeric"  title="" pattern="^[0-9]*$" />
                                                            <b class="tooltip tooltip-bottom-right">Ingresa el número total de horas.</b>
                                                        </label>                                                        
                                                    </section>                           
                                                </div>
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-4">
                                                    <section class="col col-lg-12">
                                                        <label class="label">Total de multa</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" style="background-color:#eee;">$</span>
                                                            <input type="number" name="totalDeMulta" id="totalDeMulta" class="form-control numeric alptext" step="1" disabled="disabled"  />
                                                            <b class="tooltip tooltip-bottom-right">Ingresa el total de multa.</b>
                                                        </div>                                                
                                                    </section>                           
                                                </div>
                                            </div>      
                                            <div class="row">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-3">
                                                    <section class="col col-lg-12">                                                                                                            
                                                        Solo arresto
                                                        <input type="checkbox" name="soloArresto" id="soloArresto" />                                                                                                                                          
                                                    </section>
                                                </div>
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-4">
                                                    <section class="col col-lg-12">
                                                        <label class="label">Agravante</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">$</span>
                                                            <label class="input">
                                                                <input type="text" maxlength="16" class="form-control  alptext" name="agravante" id="agravante" disabled="disabled" />
                                                                <b class="tooltip tooltip-bottom-right">Ingresa el agravante.</b>
                                                            </label>
                                                        </div>                                                
                                                    </section>  
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-3">                                                            
                                                </div>
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-4">
                                                    <section class="col col-lg-12">
                                                        <label class="label">Ajuste</label>
                                                        <div class="input-group">
                                                            <label class="input">
                                                                <input type="text" maxlength="4" title="" name="ajuste" id="ajuste" style="border-right:none" class="form-control" pattern='^[0-9]*$' />
                                                                <b class="tooltip tooltip-bottom-right">Ingresa el ajuste.</b>
                                                            </label>
                                                            <span class="input-group-addon" style="border-left:none;">%</span>
                                                        </div>
                                                        <%--<label class="input">
                                                            <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                            <input type="number" name="ajuste" id="ajuste" class="numeric" step="1" />
                                                            <b class="tooltip tooltip-bottom-right">Ingresa el ajuste.</b>
                                                        </label>      --%>                                                  
                                                    </section>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-3">                                                             
                                                </div>
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-4">
                                                    <section class="col col-lg-12">
                                                        <label class="label">Total a pagar</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" style="background-color:#eee;">$</span>
                                                            <input type="number" name="totalAPagar" id="totalAPagar" class="form-control numeric" step="1" disabled="disabled" />
                                                            <b class="tooltip tooltip-bottom-right">Ingresa el total a pagar.</b>
                                                        </div>    
                                                        <%--<label class="input">
                                                            <i class="icon-append fa fa-sort-numeric-desc"></i>
                                                            <input type="number" name="totalAPagar" id="totalAPagar" class="numeric" step="1" disabled="disabled" />
                                                            <b class="tooltip tooltip-bottom-right">Ingresa el total a pagar.</b>
                                                        </label>     --%>                                                   
                                                    </section>                                                             
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-11">                                        
                                                    <section class="col col-lg-11">
                                                        <label class="label">Razón</label>
                                                        <label class="textarea">
                                                            <textarea id="razon" name="razon" cols="125" placeholder="Razón" maxlength="6800" rows="10"></textarea>
                                                            <b class="tooltip tooltip-bottom-right">Ingresa la razón.</b>
                                                        </label>
                                                    </section>
                                                    <section class="col col-lg-11">
                                                        <label class="label">Observación  </label>
                                                        <label class="textarea">
                                                            <textarea id="justificacion" name="justificacion" placeholder="Observación" cols="125"maxlength="6800" rows="10"></textarea>
                                                            <b class="tooltip tooltip-bottom-right">Ingresa la observación.</b>
                                                        </label>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row smart-form">
                                        <footer>
                                            <div class="row" style="display: inline-block; float: right">
                                                <a style="float: none;" href="javascript:void(0);" class="btn btn-default saveCalificacion" id="saveCalificacion" title="Guardar"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                                <a style="float: none;" href="sentence_entrylist.aspx" class="btn btn-default" title="Volver al listado"><i class="fa fa-arrow-left"></i>&nbsp;Cancelar </a>
                                            </div>
                                        </footer>

                                        <input type="hidden" id="trackingIdCalificacion" runat="server" />
                                        <input type="hidden" id="idCalificacion" runat="server" />
                                        <input type="hidden" id="trackingInterno" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        </section>



    
    
    <div id="blockuser-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de <strong>&nbsp<span id="usernameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>                            
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="add-modal" class="modal fade"   role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">                                
                                <section>
                                    <label style="color: dodgerblue" >Motivo detención <a style="color: red">*</a></label>                                  
                                        <select name="rol" id="dropdown" runat="server" class="select2" style="width:100%;"/>                                     
                                                                       
                                </section>

                                 <section class="col col-4">
                                    <label  class ="input" id="Tipo" ></label>
                                </section>
                                <section class="col col-4">
                                    <label class ="input" id="SalarioMinimo" ></label>
                                </section>
                               

                                <section class="col col-4">
                                    <label class="input" id="idArticulo"></label>
                                </section>
                                <section class="col col-4">
                                    <label class="input" id="idMultaMinimo"></label>
                                </section>
                                <section class="col col-4">
                                    <label class="input" id="idMultaMaximo"></label>
                                </section>
                                <section class="col col-4">
                                    <label class="input"  id="idHorasdeArresto"></label>
                                </section>                                
                                <section class="col col-4">
                                    <a class="btn btn-sm btn-success" id="btnvsmin">Multa mínima</a>
                                    <%--Multa en salarios mínimos <input type="checkbox" id="checkMinimo" />--%>                                    
                                </section>
                                <section class="col col-4">
                                    <a class="btn btn-sm btn-danger" id="btnvsmax">  Multa máxima </a>
                                    <%--Multa en salarios máximos <input type="checkbox" id="checkMaximo" />--%>
                                </section>
                                <section class="col col-10"id="smultaSugerida"">
                                    <label style="color: dodgerblue" class="label">Multa sugerida <a style="color: red">*</a></label>
                                    <div class="input-group" id="contenedorMulta">
                                        <span class="input-group-addon" style="background-color:#eee;">$</span>                                        
                                        <input  title="" type ="text" class="form-control numeric alptext" name="multaSugerida" id="multaSugerida"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese la multa sugerida.</b>
                                    </div>
                                </section>
                            </div>
                        </fieldset>
                        <footer>                          
                            <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                             
                        </footer>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
    
    <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4><i class="fa fa-user"></i> Información de detención</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Nombre completo:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="nombreInfo" disabled="disabled" type="text" />
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de registro:</label>
                            <div class="col-md-8">
                                <input id="fechaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Motivo:</label>
                            <div class="col-md-8">
                                <textarea id="descripcionInfo" class="form-control" disabled="disabled"></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Institución:</label>
                            <div class="col-md-8">
                                <input id="institucionInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Fecha y hora de salida:</label>
                            <div class="col-md-8">
                                <input id="fechaSalidaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Lugar de detención:</label>
                            <div class="col-md-8">
                                <input id="estaturaInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Clave-responsable:</label>
                            <div class="col-md-8">
                                
                                <textarea id="pesoInfo" class="form-control alptext" disabled="disabled" "></textarea>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="form-group">
                            <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;">Unidad:</label>
                            <div class="col-md-8">
                                <input id="unidadInfo" class="form-control alptext" disabled="disabled" type="text"/>
                            </div>
                        </div>
                        <br />
                        <br />
                    </div>
                    <div class="col-md-4">
                        <img id="imgInfo" class="img-thumbnail same-height-thumbnail" alt="" src="<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png" onerror="this.src='<%= ConfigurationManager.AppSettings["relativepath"]%>Content/img/avatars/male.png'" height="240" width="240" /><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="cantidadmotivos"  />
    <input type="hidden" id="SalarioMin" />
    <input type="hidden" id="vsmin" />
    <input type="hidden" id="vsmax" />
    <input type="hidden" id="Ivsmin" />
    <input type="hidden" id="Ivsmax" />
    <input type="hidden" id="hTipo" />
    <input type="hidden" id="hdelitos" />
    <input type="hidden" id="hfalta" />
    <input type="hidden" id="hotros" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/jquery.maskedinput.js" type="text/javascript"></script>    
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.keyCode === 27) {
                if ($("#add-modal").is(":visible")) {
                    $("#add-modal").modal("hide");
                }
            }
            else if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();
                if ($("#add-modal").is(":visible")) {
                    document.getElementsByClassName("save")[0].click();
                }
                else {
                    document.getElementsByClassName("saveCalificacion")[0].click();
                }
            }
        });
        $(document).ready(function () {
            pageSetUp();
            //#ctl00_contenido_HQLNBB Registrar
            //#ctl00_contenido_KAQWPK Modificar
            //#ctl00_contenido_LCADLW Eliminar
            $("#showInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'block');
                $("#showInfoDetenido").css('display', 'none');

                $("#SectionInfoDetenido").show();
                $("#ScrollableContent").css("height", "calc(100vh - 400px)");
            })
            $("#hideInfoDetenido").click(function () {
                $("#hideInfoDetenido").css('display', 'none');
                $("#showInfoDetenido").css('display', 'block');

                $("#SectionInfoDetenido").hide();
                $("#ScrollableContent").css("height", "calc(100vh - 180px)");
            });

            $("body").on("click", ".detencion", function () {
                $("#datospersonales-modal").modal("show");
                var param = RequestQueryString("tracking");
                CargarDatosModal(param);
            });
            $("body").on("click", "#btnvsmin", function () {

                $("#multaSugerida").val($("#Ivsmin").val());
            });
            $("body").on("click", "#btnvsmax", function () {

                $("#multaSugerida").val($("#Ivsmax").val());
            });

            $("body").on("click", ".watch", function () {
                $("#add-modal").modal('show');
                $("#add-modal-title").empty();
                $("#form-modal-title").html('<i class="glyphicon glyphicon-eye-open" +=""></i> ver registro');
                $("#btnsave").attr("data-trackingid", $(this).attr("data-tracking"));
                $("#btnsave").attr("data-id", $(this).attr("data-id"));
                CargarListado($(this).attr("data-motivoid"));
                $("#ctl00_contenido_dropdown").trigger("change");
                $("#multaSugerida").attr("disabled", "disabled");
                $("#ctl00_contenido_dropdown").attr("disabled", "disabled");
                $("#btnsave").hide();
                //$("#multaSugerida").val($(this).attr("data-multasugerida"));                
            });

            $("body").on("click", ".edit", function () {
                $("#add-modal").modal('show');
                $("#add-modal-title").empty();
                $("#form-modal-title").html('<i class="fa fa-pencil" +=""></i> Editar registro');
                $("#btnsave").attr("data-trackingid", $(this).attr("data-tracking"));
                $("#btnsave").attr("data-id", $(this).attr("data-id"));
                CargarListado($(this).attr("data-motivoid"));
                $("#ctl00_contenido_dropdown").trigger("change");
                $("#multaSugerida").removeAttr("disabled");
                $("#ctl00_contenido_dropdown").removeAttr("disabled");
                $("#btnsave").show();
                //$("#multaSugerida").val($(this).attr("data-multasugerida"));
            });

            $("body").on("click", ".blockMotivo", function () {
                var usernameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-trackingid", $(this).attr("data-id"));
                $("#blockuser-modal").modal("show");
            });

             $("#ctl00_contenido_dropdown").select2();
            $(document).on('keydown', 'input[pattern]', function(e){
              var input = $(this);
              var oldVal = input.val();
              var regex = new RegExp(input.attr('pattern'), 'g');

              setTimeout(function(){
                var newVal = input.val();
                if(!regex.test(newVal)){
                  input.val(oldVal); 
                }
              }, 0);
            });

            $("#agravante").on({
                "focus": function (event) {
                    $(event.target).select();
                },
                "keyup": function (event) {
                    $(event.target).val(function (index, value) {
                        return value.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }
            });


             $(document).on('keydown', 'input[pattern]', function(e){
              var input = $(this);
              var oldVal = input.val();
              var regex = new RegExp(input.attr('pattern'), 'g');
              setTimeout(function(){
                var newVal = input.val();
                if(!regex.test(newVal)){
                  input.val(oldVal); 
                }
              }, 0);
            });
            $("#multaSugerida").on({
                "focus": function(event) {
                    $(event.target).select();
                },
                "keyup": function(event) {
                    $(event.target).val(function(index, value) {
                        return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }                
            });

            $("#checkMinimo").change(function () {
                if ($("#checkMinimo").is(":checked")) {
                    $("#checkMaximo").prop("checked", false);
                    var valMinimo = $("#idMultaMinimo").text();
                    var index = valMinimo.toString().indexOf(":") + 1;
                    var indexFinal = valMinimo.toString().length;
                    var newVal = valMinimo.toString().substr(index, indexFinal);
                    $("#multaSugerida").val(newVal.replace(" ", ""));
                }
                else {
                    if (!$("#checkMinimo").is(":checked")) {
                        $("#multaSugerida").val("");
                    }
                }
            });
            $('#btnvsmin').hide();
            $('#btnvsmax').hide();
            $("#checkMaximo").change(function () {
                if ($("#checkMaximo").is(":checked")) {
                    $("#checkMinimo").prop("checked", false);
                    var valMaximo = $("#idMultaMaximo").text();
                    var index = valMaximo.toString().indexOf(":") + 1;
                    var indexFinal = valMaximo.toString().length;
                    var newVal = valMaximo.toString().substr(index, indexFinal);
                    $("#multaSugerida").val(newVal.replace(" ", ""));
                }
                else {
                    if (!$("#checkMaximo").is(":checked")) {
                        $("#multaSugerida").val("");
                    }
                }
            });

            $("#ctl00_contenido_dropdown").change(function () {
               
                var id = $("#ctl00_contenido_dropdown").val();
               
                if (id == "0")
                {
                    $('#btnvsmin').hide();
                    $('#btnvsmax').hide();
                    $("#idArticulo").text(''); 
                    $("#idMultaMinimo").text(''); 
                    $("#idMultaMaximo").text(''); 
                    $("#idHorasdeArresto").text(''); 
                    $("#SalarioMinimo").text('');
                    $("#Tipo").text('');
                    return;
                }
              
                $("#idArticulo").text($("#ctl00_contenido_dropdown").val() )
                $.ajax({
                    type: "POST",
                    url: "calificacion.aspx/GetMotivoDetencionById",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ _id: id }),
                    cache: false,
                    success: function (data) {
                         data = data.d;  
                        $("#hTipo").val(data.Tipo);
                        if (data.Tipo != "Delito") {
                            var salarioMinimo = data.SalarioMinimo !== null && data.SalarioMinimo !== "" && data.SalarioMinimo !== undefined ? data.SalarioMinimo : "";
                            var multaMinimo = data.MultaMinimo !== null && data.MultaMinimo !== "" && data.MultaMinimo !== undefined ? data.MultaMinimo : "";
                            var multaMaximo = data.MultaMaximo !== null && data.MultaMaximo !== "" && data.MultaMaximo !== undefined ? data.MultaMaximo : "";
                            var tipo = data.Tipo !== null && data.Tipo !== "" && data.Tipo !== undefined ? data.Tipo : "";
                            var articulo = data.Articulo !== null && data.Articulo !== "" && data.Articulo !== undefined ? data.Articulo : "";
                            var horaArresto = data.horaArresto !== null && data.horaArresto !== "" && data.horaArresto !== undefined ? data.horaArresto : "";
                            var importeMultaMinimo = data.ImporteMultaMinimo !== null && data.ImporteMultaMinimo !== "" && data.ImporteMultaMinimo !== undefined ? data.ImporteMultaMinimo : "";
                            var importeMultaMaximo = data.ImporteMultaMaximo !== null && data.ImporteMultaMaximo !== "" && data.ImporteMultaMaximo !== undefined ? data.ImporteMultaMaximo : "";
                            
                            $("#SalarioMin").val(salarioMinimo);
                            $("#vsmin").val(multaMinimo);
                            $("#vsmax").val(multaMaximo);
                            $("#Tipo").text('Tipo: ' + tipo);
                            $("#SalarioMinimo").text(salarioMinimo);
                            $("#idArticulo").text('Artículo: ' + articulo);
                            $("#idMultaMinimo").text('Multa mínima: ' + multaMinimo);
                            $("#idMultaMaximo").text('Multa máxima: ' + multaMaximo);
                            $("#idHorasdeArresto").text('Horas de arresto: ' + horaArresto);
                            $('#btnvsmin').show();
                            $('#btnvsmax').show();
                            $("#Ivsmin").val(importeMultaMinimo);
                            $("#Ivsmax").val(importeMultaMaximo);                           
                            $("#multaSugerida").val("");
                            $("#smultaSugerida").show();
                        }
                        else {
                            var salarioMinimo = data.SalarioMinimo !== null && data.SalarioMinimo !== "" && data.SalarioMinimo !== undefined ? data.SalarioMinimo : "";                            
                            var tipo = data.Tipo !== null && data.Tipo !== "" && data.Tipo !== undefined ? data.Tipo : "";
                            var articulo = data.Articulo !== null && data.Articulo !== "" && data.Articulo !== undefined ? data.Articulo : "";
                            var horaArresto = data.horaArresto !== null && data.horaArresto !== "" && data.horaArresto !== undefined ? data.horaArresto : "";                            

                            $("#Tipo").text('Tipo: ' + tipo);
                            $("#SalarioMinimo").text(salarioMinimo);
                            $("#idArticulo").text('Artículo: ' + articulo);
                            $("#idHorasdeArresto").text('Horas de arresto: ' + horaArresto);
                            $("#idMultaMinimo").text(''); 
                            $("#idMultaMaximo").text(''); 
                            $("#smultaSugerida").hide();
                           
                            $('#btnvsmin').hide();
                            $('#btnvsmax').hide();

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            });
            function CargarDatosModal(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Registry/InformacionDetencion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $("#fechaInfo").val(resultado.obj.FechaDetencion);
                            $("#llamadaInfo").val(resultado.obj.LlamadaNombre);
                            $("#eventoInfo").val(resultado.obj.EventoNombre);
                            $("#folioInfo").val(resultado.obj.Folio);
                            $("#unidadInfo").val(resultado.obj.UnidadNombre);
                            $("#responsableInfo").val(resultado.obj.ResponsableNombre);
                            $("#descripcionInfo").val(resultado.obj.Motivo_evento);
                            //$("#descripcionInfo").val(resultado.obj.Motivo);
                            $("#detalleInfo").val(resultado.obj.Descripcion);
                            $("#lugarInfo").val(resultado.obj.Lugar);
                            $("#paisInfo").val(resultado.obj.PaisNombre);
                            $("#estadoInfo").val(resultado.obj.EstadoNombre);
                            $("#municipioInfo").val(resultado.obj.MunicipioNombre);
                            $("#coloniaInfo").val(resultado.obj.ColoniaNombre);
                            $("#cpInfo").val(resultado.obj.CodigoPostal);
                            $("#estaturaInfo").val(resultado.obj.Lugares);
                            var s = resultado.obj.Unidad1;
                            var t = resultado.obj.Responsable1;

                            $("#pesoInfo").val(t);
                            $("#unidadInfo").val(s);
                            $("#institucionInfo").val(resultado.obj.Centro);
                            $("#fechaSalidaInfo").val(resultado.obj.FechaSalida);
                            $("#nombreInfo").val(resultado.obj.Nombre);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                            $('#imgInfo').attr("src", imagenAvatar);
                            $("#datospersonales-modal").modal("show");
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
                }
    


            $("#addMotivo").click(function () {
                CargarListado("0");
                $("#btnsave").attr("data-id", "");
                $("#btnsave").attr("data-trackingid", "");
                $("#form-modal-title").empty();
                $("#form-modal-title").html('<i class="fa fa-plus" +=""></i> Agregar registro');
                $("#add-modal").modal('show');
            });

            $("#btnsave").click(function () {
                if (validarNuevoMotivo()) {
                    guardar()
                }
            });

            function ObtenerValores() {                                
                var motivoInterno = {
                    Id: $("#btnsave").attr("data-id"),
                    TrackingId: $("#btnsave").attr("data-trackingid"),
                    MotivoDetencionId: $("#ctl00_contenido_dropdown").val(),                    
                    TrackingInternoId: $("#ctl00_contenido_trackingInterno").val(),
                    MultaSugerida: $("#multaSugerida").val()
                };
                
                return motivoInterno;
            }

            function guardar() {
                startLoading();
                var motivoInterno = ObtenerValores();
                
                $.ajax({
                    type: "POST",
                    url: "calificacion.aspx/saveMotivoInterno",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'motivoInterno': motivoInterno }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del motivo de detención se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información del  motivo de detención se  " + resultado.mensaje + " correctamente.");                              
                            $('#main').waitMe('hide');
                            $("#add-modal").modal("hide");
                            $('#dt_basic').DataTable().ajax.reload();
                            setTimeout(hideMessage, hideTime);
                        }
                        else {
                            $('#main').waitMe('hide');
                           
                            if (resultado.mensaje == "El interno ya tiene asignado ese motivo de detención.") {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-alert fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Alerta! </strong>" +
                                    "Algo salió mal. " + resultado.mensaje + "</div>");
                                setTimeout(hideMessage, hideTime);
                                ShowAlert("Motivo de detención interno", resultado.mensaje);
                            }
                            else if (resultado.mensaje == "Para un motivo de tipo \"Falta administrativa\" la multa debe ser mayor a 0.") {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                    "Algo salió mal. " + resultado.mensaje + "</div>");
                                setTimeout(hideMessage, hideTime);
                                ShowError("¡Error! Algo salió mal", resultado.mensaje);
                            }
                            else {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                    "Algo salió mal. " + resultado.mensaje + "</div>");
                                ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                                setTimeout(hideMessage, hideTime);
                            }
                        }
                    }
                });
            }

            function CargarListado(id) {                
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "calificacion.aspx/getListadoMotivos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_dropdown');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Motivo detención]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de niveles de peligrosidad. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            function validarNuevoMotivo() {
                var esvalido = true;

                if ($("#ctl00_contenido_dropdown").val() == "0") {
                    ShowError("Motivo", "El motivo es obligatorio.");
                    $('#motivo').parent().removeClass('state-success').addClass("state-error");
                    $('#motivo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#motivo').parent().removeClass("state-error").addClass('state-success');
                    $('#motivo').addClass('valid');
                }
                if ($("#hTipo").val() != "Delito") {
                    if ($("#multaSugerida").val().split(" ").join("") == "") {
                        ShowError("Multa sugerida", "La multa sugerida es obligatoria.");
                        $('#multaSugerida').parent().removeClass('state-success').addClass("state-error");
                        $('#multaSugerida').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#multaSugerida').parent().removeClass("state-error").addClass('state-success');
                        $('#multaSugerida').addClass('valid');
                    }
                }
                return esvalido;
            }

            init();            

            $("#soloArresto").change(function () {
                if ($("#soloArresto").is(":checked")) {
                    $("#agravante").attr("disabled", "disabled");
                    $("#ajuste").attr("disabled", "disabled");

                    $("#agravante").val("0");
                    $("#ajuste").val("0");
                    $("#totalAPagar").val("0");
                }
                else {
                    $("#agravante").removeAttr("disabled");
                    $("#ajuste").removeAttr("disabled");
                }
            });         

            function horas(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/sentence/calificacion.aspx/gethoras",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            if (resultado.obj.NuevaCalificacion)
                            $("#totalDeHoras").val(resultado.obj.TotalHoras);
                        }
                        else {
                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de las horas. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            //Cargar multas asociadas            

            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,                
                "scrollCollapse": true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",                    
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },	
       
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api()
                    var total = 0.0;
                    var horasMulta = 0;
                    var contadordelitos = 0;
                    var contadorFaltaAdministrativa = 0;
                    var contadorotros = 0;
                    var totalhoraspivote = 0;
                    data.forEach(function (entry) {
                        if (entry.Habilitado) {
                            total += entry.MultaSugerida;
                            horasMulta += entry.HorasdeArresto;
                            if (entry.Tipo_Motivo == "Delito")
                            {
                                contadordelitos += 1;
                            }
                           ;
                               if (entry.Tipo_Motivo == "Falta Administrativa")
                            {
                                contadorFaltaAdministrativa += 1;
                            }

                               if (entry.Tipo_Motivo == "Otros")
                            {
                                contadorotros += 1;
                            }
                        }                        
                    });

                    
                    if (contadorotros > 0 && horasMulta>0)
                    {
                        totalhoraspivote = 0;

                    }
                  
                    if (contadorFaltaAdministrativa > 0 && horasMulta > 36) {
                        totalhoraspivote = 36;

                    }
                    else
                    {
                        totalhoraspivote = horasMulta;
                    }

                    if (contadorFaltaAdministrativa > 0 && contadordelitos == 0)
                    {
                          horasMulta=totalhoraspivote;
                    }

                    if (contadordelitos > 0 && horasMulta > 72) {
                        totalhoraspivote = 72;

                    }
                    else
                    {
                        totalhoraspivote = horasMulta;
                    }

                    $("#hdelitos").val(contadordelitos);
                    $("#hfalta").val(contadorFaltaAdministrativa);
                    $("#hotros").val(contadorotros);
                    
                    var param = RequestQueryString("tracking");

                    horas(param);
                    horasMulta = totalhoraspivote;
                    
                    $("#totalDeMulta").val(total);
                    $("#totalDeHoras").val(horasMulta);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },                
                "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');                                                
                            $('td', row).eq(3).addClass('strikeout');                                                                            
                            $('td', row).eq(4).addClass('strikeout');                                                                            
                            $('td', row).eq(5).addClass('strikeout');                                                                            
                        }
                    },
                ajax: {
                    type: "POST",
                    url: "calificacion.aspx/getCharges",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });                       
                        parametrosServerSide.emptytable = window.emptytable;
                        parametrosServerSide.TrackingId = $("#ctl00_contenido_trackingInterno").val();
                        return JSON.stringify(parametrosServerSide);
                    }
                   
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id"
                    },
                    {
                        name: "Articulo",
                        data: "Articulo"
                    },
                    {
                        name: "Motivo",
                        data: "Motivo"
                    },
                    {
                        name: "Tipo_Motivo",
                        data: "Tipo_Motivo"
                    },
                    {
                        name: "HorasdeArresto",
                        data: "HorasdeArresto"
                    },
                    {
                        name: "MultaSugerida",
                        data: "MultaSugerida"
                    },
                    {
                        name: "Descripcion",
                        data: "Descripcion"
                    },
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 7,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "edit";
                            var editar = '<a class="btn btn-primary btn-circle ' + edit + '" data-tracking="' + row.TrackingIdMotInterno + '" data-motivoId="' + row.MotivoDetencionId + '" data-multaSugerida="' + row.MultaSugerida + '" data-id="' + row.IdMotInterno + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            var habilitar = "";
                            if (!row.Habilitado) {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                                
                            }
                            else {
                                
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "true" && !row.Habilitado) editar = '<a class="btn btn-primary btn-circle watch data-tracking="' + row.TrackingIdMotInterno + '" data-motivoId="' + row.MotivoDetencionId + '" data-multaSugerida="' + row.MultaSugerida + '" data-id="' + row.IdMotInterno + '" title="Ver registro"><i class="glyphicon glyphicon-eye-open"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockMotivo" href="javascript:void(0);" data-value="' + row.Motivo + '" data-id="' + row.TrackingIdMotInterno + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return  editar+habilitar
                               ;
                        }
                    }
                ]
              
            });            

      

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingid = $(this).attr("data-trackingid");
                startLoading();
                $.ajax({
                    url: "calificacion.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: trackingid,
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {                            
                           $('#dt_basic').DataTable().ajax.reload();
                            $("#blockuser-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                " El registro se " + JSON.parse(data.d).mensaje + " correctamente.</div > ");
                            ShowSuccess("¡Bien hecho!", "El registro se " + JSON.parse(data.d).mensaje + " correctamente.");
                            setTimeout(hideMessage, hideTime);
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            ShowError("¡¡Error! Algo salió mal y no fue posible afectar el estatus del usuario. . Si el problema persiste, contacte al personal de soporte técnico.");
                            setTimeout(hideMessage, hideTime);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });

            //Cargar situaciones
            function loadSituations(setvalue) {
                $.ajax({                    
                    type: "POST",
                    url: "calificacion.aspx/getSituations",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#situacion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Situación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de países. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            //Cargar instituciones
            function loadInstitutions(setvalue, idPais) {

                $.ajax({                    
                    type: "POST",
                    url: "calificacion.aspx/getInstitutions",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        idPais: idPais
                    }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#institucion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Institucion]", "0"));

                        if (response.d.length > 0) {
                            setvalue = response.d[0].Id;
                        }

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de estados. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }                        
               $("body").on("click", "#datospersonales", function () {
                //var id = $(this).attr("data-id");
                    var param = RequestQueryString("tracking");
                if (param != undefined) {
                    //MovimientoCelda.TrackingId = param;
                   
                     CargarDatosPersonales(param);
                }
               
               
                
            });
            function CargarDatosPersonales(trackingid) {
                //jc
                
                startLoading();
                  $.ajax({
                      type: "POST",
                      url: "calificacion.aspx/getdatosPersonales",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {                              
                              $('#nombreInterno2').val(resultado.obj.Nombre);
                              $('#edad').val(resultado.obj.Edad);
                              $('#situacionPersonal').val(resultado.obj.Situacion);
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                             
                                   if (resultado.obj.RutaImagen == "")
                            {
                               imagenAvatar = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png"; 
                            }
                            
                              $('#ctl00_contenido_avatar3').attr("src", imagenAvatar)
                              
                              
                              $('#horaRegistro').val(resultado.obj.Registro);
                              $('#salida').val(resultado.obj.Salida);
                              $('#domicilio').val(resultado.obj.Domicilio);
                           
                              if (resultado.obj.TotalHours <= -1.001) {
                                  $("#salida").css("background", "graylight");
                              }
                              else if (resultado.obj.TotalHours >= 0) {
                                  $("#salida").css("background", "blue");
                                  $("#salida").css("color", "white");
                              }
                              else {
                                  $("#salida").css("background", "yellow");                                  
                              }
                              $("#add-datosPersonales").modal("show");
                          } else {
                              ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
            }


            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "calificacion.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {

                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                             var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                                   if (resultado.obj.RutaImagen == "")
                            {
                               imagenAvatar = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png"; 
                            }
                            $('#avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);
                            $('#edadInterno').text(resultado.obj.Edad);
                            $('#sexoInterno').text(resultado.obj.Sexo);

                            $('#coloniaInterno').text(resultado.obj.coloniaNombre);
                            $('#domicilioInterno').text(resultado.obj.domiclio);
                            $('#municipioInterno').text(resultado.obj.municipioNombre);

                            $("#ctl00_contenido_idCalificacion").val(resultado.obj.IdCalificacion);
                            $("#ctl00_contenido_trackingIdCalificacion").val(resultado.obj.TrackingCalificacion);

                            loadSituations(resultado.obj.SituacionId);
                            loadInstitutions(resultado.obj.InstitucionId);
                            $("#fundamento").val(resultado.obj.Fundamento);
                            resultado.obj.TrabajoSocial ? $("#siTrabajo").attr("checked", "checked") : $("#noTrabajo").attr("checked", "checked");
                            $("#totalDeHoras").val(resultado.obj.TotalHoras);
                            if (resultado.obj.SoloArresto) {
                                $("#soloArresto").attr("checked", "checked");
                            }
                            $("#agravante").val(resultado.obj.Agravante);
                            $("#ajuste").val(resultado.obj.Ajuste);
                            $("#totalAPagar").val(resultado.obj.TotalAPagar);
                            $("#razon").val(resultado.obj.Razon);
                            //if (resultado.obj.IdCalificacion != 0)
                            //{
                            //    $('#saveCalificacion').hide();

                            //}
                            if ($("#soloArresto").is(":checked")) {
                                $("#agravante").attr("disabled", "disabled");
                                $("#ajuste").attr("disabled", "disabled");

                                $("#agravante").val("0");
                                $("#ajuste").val("0");
                                $("#totalAPagar").val("0");
                            }
                            else {
                                $("#agravante").removeAttr("disabled");
                                $("#ajuste").removeAttr("disabled");
                            }

                            if (!resultado.obj.Activo && resultado.obj.NuevaCalificacion) {
                                $("#situacion").attr("disabled", "disabled");                    
                                $("#fundamento").attr("disabled", "disabled");
                                $("#siTrabajo").attr("disabled", "disabled");
                                $("#noTrabajo").attr("disabled", "disabled");
                                $("#totalDeHoras").attr("disabled", "disabled");
                                $("#soloArresto").attr("disabled", "disabled");                    
                                $(".form-control").css("background-color", "lightgrey");
                                $(".form-control").css("cursor", "not-allowed");
                                $(".form-control").css("pointer-events", "none");                    
                                $("#razon").attr("disabled", "disabled");
                                $("#saveCalificacion").hide();
                                $("#addMotivo").hide();
                                $("#btnsave").hide();     
                            }
                            else {
                                var uno = JSON.parse($("#ctl00_contenido_HQLNBB").val());
                                var dos = JSON.parse($("#ctl00_contenido_KAQWPK").val());
                                var tres = JSON.parse($("#ctl00_contenido_LCADLW").val());
                    
                                if (uno && dos && tres) {

                                    $("#saveCalificacion").show();
                                    $("#addMotivo").show();
                                    $("#btnsave").show();
                                    $("#situacion").removeAttr("disabled");
                                    $("#fundamento").removeAttr("disabled");
                                    $("#siTrabajo").removeAttr("disabled");
                                    $("#noTrabajo").removeAttr("disabled");
                                    $("#totalDeHoras").removeAttr("disabled");
                                    $("#soloArresto").removeAttr("disabled");
                                    $("#razon").removeAttr("disabled");
                                    $(".form-control").css("pointer-events", "");
                                    $(".form-control").css("cursor", "");
                                    $(".form-control").css("background-color", "");
                                }
                                else {                        
                                    $("#situacion").attr("disabled", "disabled");
                                    $("#fundamento").attr("disabled", "disabled");
                                    $("#siTrabajo").attr("disabled", "disabled");
                                    $("#noTrabajo").attr("disabled", "disabled");
                                    $("#totalDeHoras").attr("disabled", "disabled");
                                    $("#soloArresto").attr("disabled", "disabled");
                                    $(".form-control").css("background-color", "lightgrey");
                                    $(".form-control").css("cursor", "not-allowed");
                                    $(".form-control").css("pointer-events", "none");
                                    $("#razon").attr("disabled", "disabled");
                                    $("#saveCalificacion").hide();
                                    $("#addMotivo").hide();
                                    $("#btnsave").hide();
                                }
                            }                            
                        }
                        else {
                            ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de la detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }            

            $("body").on("click", ".saveCalificacion", function () {
                if (validar()) {

                    var agravante = $("#agravante").val();
                    var agravanteFinal = 0;
                    
                    if (agravante != "") {
                        agravanteFinal = parseFloat(agravante.replace(",",""));
                    }
                    
                    
                    var totalAPagar = parseFloat($("#totalDeMulta").val()) + agravanteFinal;
             
                    if ($("#ajuste").val() != null && $("#ajuste").val() != "" && $("#ajuste").val() != "0" && $("#ajuste").val() != "0.0") {
                        var porcentaje = parseFloat($("#ajuste").val()) / 100;
                        var cantidadPorcentaje = totalAPagar * porcentaje;
                        totalAPagar = totalAPagar - cantidadPorcentaje;
                    }

                    if ($("#soloArresto").is(":checked")) {
                        $("#totalAPagar").val("0");
                    }
                    else {
                        $("#totalAPagar").val(totalAPagar);
                    }

                    if ($("#situacion").val() == "4") {
                        $("#totalAPagar").val("0");
                    }

                    var datos = {
                        Id: $("#ctl00_contenido_idCalificacion").val(),
                        TrackingId: $("#ctl00_contenido_trackingIdCalificacion").val(),
                        TrackingIdInterno: $("#ctl00_contenido_trackingInterno").val(),
                        SituacionId: $("#situacion").val(),
                        InstitucionId: $("#institucion").val(),
                        Fundamento: $("#fundamento").val(),
                        TrabajoSocial: $("#siTrabajo").is(":checked") ? true : false,
                        TotalHoras: $("#totalDeHoras").val(),
                        SoloArresto: $("#soloArresto").is(":checked") ? true : false,
                        TotalDeMultas: $("#totalDeMulta").val(),
                        Agravante: $("#agravante").val(),
                        Ajuste: $("#ajuste").val(),
                        TotalAPagar: $("#totalAPagar").val(),
                        Razon: $("#razon").val()
                    };

                    Save(datos,$("#justificacion").val() );
                }                                
            });                                

            function validar() {
                var esvalido = true;

                if ($('#situacion').val() == "0" || $('#situacion').val() == null) {
                    ShowError("Situación", "La situación es obligatoria.");
                    $('#situacion').parent().removeClass('state-success').addClass("state-error");
                    $('#situacion').removeClass('valid');
                    esvalido = false;
                }
                else {

                    if ($('select[name="situacion"] option:selected').text().toUpperCase() == "FALTA ADMINISTRATIVA" ||
                        $('select[name="situacion"] option:selected').text().toUpperCase() == "A DISPOSICIÓN DE OTRA AUTORIDAD" ||
                        $('select[name="situacion"] option:selected').text().toUpperCase() == "NO CULPABLE" ||
                        $('select[name="situacion"] option:selected').text().toUpperCase() == "POR CONSIGNAR" ||
                        $('select[name="situacion"] option:selected').text().toUpperCase() == "CUSTODIA") {

                        var tableData = $("#dt_basic").DataTable().rows().data();

                        if (tableData.length > 0) {
                            var isActive = true;
                            $("#dt_basic").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                                var data;
                                data = this.data();
                                if (!data.Habilitado) {
                                    isActive = false;
                                }
                            });

                            //if (!isActive) {
                            //    esvalido = false;
                            //    ShowError("Motivos de detención", "La situación que ha seleccionado amerita al menos 1 motivo de detención.");
                            //}
                        }
                        //else {
                        //    esvalido = false;
                        //    ShowError("Motivos de detención", "La situación que ha seleccionado amerita al menos 1 motivo de detención.");
                        //}
                    }
                    else {
                        $('#situacion').parent().removeClass("state-error").addClass('state-success');
                        $('#situacion').addClass('valid');
                    }



                }

                if ($('#institucion').val() == "0" || $('#institucion').val() == null) {
                    ShowError("Institución", "La institución es obligatoria.");
                    $('#institucion').parent().removeClass('state-success').addClass("state-error");
                    $('#institucion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#institucion').parent().removeClass("state-error").addClass('state-success');
                    $('#institucion').addClass('valid');
                }

                if ($("#fundamento").val().split(" ").join("") == "") {
                    ShowError("Fundamento", "El fundamento es obligatorio.");
                    $('#fundamento').parent().removeClass('state-success').addClass("state-error");
                    $('#fundamento').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fundamento').parent().removeClass("state-error").addClass('state-success');
                    $('#fundamento').addClass('valid');
                }

                if ($("#totalDeHoras").val().split(" ").join("") == "") {
                    ShowError("Total de horas", "El total de horas es obligatorio.");
                    $('#totalDeHoras').parent().removeClass('state-success').addClass("state-error");
                    $('#totalDeHoras').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#totalDeHoras').parent().removeClass("state-error").addClass('state-success');
                    $('#totalDeHoras').addClass('valid');
                }
                //if (parseInt($("#totalDeHoras").val()) > 72) {
                //    ShowError("Total de horas", "El total de horas debe ser menor a 72 horas.");
                //    $('#totalDeHoras').parent().removeClass('state-success').addClass("state-error");
                //    $('#totalDeHoras').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#totalDeHoras').parent().removeClass("state-error").addClass('state-success');
                //    $('#totalDeHoras').addClass('valid');
                //}
                if (!$("#siTrabajo").is(":checked") && !$("#noTrabajo").is(":checked")) {
                    ShowError("Trabajo social", "Debe seleccionar si es requerido el trabajo social.");
                    esvalido = false;
                }

                if ($("#ajuste").val() != null && $("#ajuste").val() != "" && $("#ajuste").val() != "0" && $("#ajuste").val() != "0.0") {
                    if ($("#razon").val().split(" ").join("") == "") {
                        ShowError("Razón", "La razón es obligatoria.");
                        $('#razon').parent().removeClass('state-success').addClass("state-error");
                        $('#razon').removeClass('valid');
                        esvalido = false;
                    } else {
                        $('#razon').parent().removeClass("state-error").addClass('state-success');
                        $('#razon').addClass('valid');
                    }
                }

                //if ($("#justificacion").val().split(" ").join("") == "") {
                //    ShowError("Justificacion", "La Justificación es obligatoria.");
                //    $('#justificacion').parent().removeClass('state-success').addClass("state-error");
                //    $('#justificacion').removeClass('valid');
                //    esvalido = false;
                //}
                //else {
                //    $('#justificacion').parent().removeClass("state-error").addClass('state-success');
                //    $('#justificacion').addClass('valid');
                //}

                var contadordelitos = 0;
                var contadorFaltaAdministrativa = 0;
                var contadorotros = 0;
                contadordelitos = parseInt($("#hdelitos").val());
                contadorFaltaAdministrativa = parseInt($("#hfalta").val());
                contadorotros = parseInt($("#hotros").val());

                if (contadordelitos > 0 && parseInt($("#totalDeHoras").val()) > 72) {
                    ShowAlert("¡Atención!", "No se puede exeder mas de 72 horas de arresto cuando tiene delitos");
                    esvalido = false;
                    return esvalido;
                }

                if (contadorFaltaAdministrativa > 0 && contadordelitos == 0 && parseInt($("#totalDeHoras").val()) > 36) {
                    ShowAlert("¡Atención!", "No se puede exeder mas de 36 horas de arresto cuando tiene falta administrativa");
                    esvalido = false;
                    return esvalido;
                }

                if (contadorotros > 0 && contadorFaltaAdministrativa == 0 && contadordelitos == 0 && parseInt($("#totalDeHoras").val()) > 0) {
                    ShowAlert("¡Atención!", "Cuando se tiene motivo de detención \"otros\" no se podrá asignar horas de arresto");
                    esvalido = false;
                    return esvalido;
                }

                return esvalido;
            }

            function limpiarEstilosCampos() {                
                $(".select").removeClass('valid');
                $(".select").removeClass('state-error');
                $(".select").removeClass('state-success');
                $(".input").removeClass('valid');
                $(".input").removeClass('state-error');
                $(".input").removeClass('state-success');                        
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
            function Save(datos,calif) {
                startLoading();                
                $.ajax({                    
                    type: "POST",
                    url: "calificacion.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'datos': datos,
                        descripcion: calif
                    }),
                    cache: false,
                    success: function (data) {                                                
                        if (data.d.exitoso) {                                                                                 
                            $("#ctl00_contenido_idCalificacion").val(data.d.Id);
                            $("#ctl00_contenido_trackingIdCalificacion").val(data.d.TrackingId);

                            $("#totalAPagar").val(data.d.totalapagar);
                            limpiarEstilosCampos();
                          
                            if (data.d.ishit) {
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Atención! </strong>" +
                                data.d.msghit, "<br /></div>");
                                ShowAlert("¡Atencion!", data.d.msghit)
                                setTimeout(hideMessage, hideTime);
                            }
                            else
                            {

                                  $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información se " + data.d.mensaje + " correctamente.", "<br /></div>");
                            ShowSuccess("¡Bien hecho!", "La información se " + data.d.mensaje + " correctamente.");
                                setTimeout(hideMessage, hideTime);
                            }
                            var ruta = ResolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                        } else {
                            ShowError("¡Error! Algo salió mal", data.d.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + data.d.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
              if (url.indexOf("~/") == 0) {
                  url = baseUrl + url.substring(2);
              }
              return url;
            }

        

            function init() {
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    CargarDatos(param);
                    $("#ctl00_contenido_trackingInterno").val(param);                    
                }
                else {
                    var uno = JSON.parse($("#ctl00_contenido_HQLNBB").val());
                    var dos = JSON.parse($("#ctl00_contenido_KAQWPK").val());
                    var tres = JSON.parse($("#ctl00_contenido_LCADLW").val());
                
                    if (uno && dos && tres) {                                        
                        $("#saveCalificacion").show();
                        $("#addMotivo").show();
                        $("#btnsave").show();
                        $("#situacion").removeAttr("disabled");                    
                        $("#fundamento").removeAttr("disabled");
                        $("#siTrabajo").removeAttr("disabled");
                        $("#noTrabajo").removeAttr("disabled");
                        $("#totalDeHoras").removeAttr("disabled");
                        $("#soloArresto").removeAttr("disabled");                    
                        $("#razon").removeAttr("disabled");
                        $(".form-control").css("pointer-events", "");
                        $(".form-control").css("cursor", "");
                        $(".form-control").css("background-color", "");
                    }
                    else {
                        $("#situacion").attr("disabled", "disabled");                    
                        $("#fundamento").attr("disabled", "disabled");
                        $("#siTrabajo").attr("disabled", "disabled");
                        $("#noTrabajo").attr("disabled", "disabled");
                        $("#totalDeHoras").attr("disabled", "disabled");
                        $("#soloArresto").attr("disabled", "disabled");                    
                        $(".form-control").css("background-color", "lightgrey");
                        $(".form-control").css("cursor", "not-allowed");
                        $(".form-control").css("pointer-events", "none");                    
                        $("#razon").attr("disabled", "disabled");
                        $("#saveCalificacion").hide();
                        $("#addMotivo").hide();
                        $("#btnsave").hide();                    
                    }
                }
            }
        });
    </script>
</asp:Content>
