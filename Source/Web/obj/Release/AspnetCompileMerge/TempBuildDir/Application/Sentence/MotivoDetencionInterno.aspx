﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="MotivoDetencionInterno.aspx.cs" Inherits="Web.Application.Sentence.MotivoDetencionInterno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Sentence/sentence_entrylist.aspx">Juez calificador</a></li>
    <li>Motivos detención</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
         td.strikeout {
            text-decoration: line-through;
        }
   
    </style>
    <div class="scroll">
    <div class="row">
        
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    
    
    <p></p>
    <section id="widget-grid" class="">
            <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-list-alt"></i>
                Información personal
            </h1>
        </div>
    </div>
        <div class="row">
         

           <article class="col-sm-12 col-md-12 col-lg-12">
           <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

							<table class="table-responsive">
							<tbody><tr>
								<td colspan="2">
                                    <br />
								
                                    <img  width="150" height="180" align="center" class="img-circle" id="avatar" alt="" runat="server" src="" />
				                </td>
								
							</tr>
						<tr>
								<td colspan="2">
                                    <br />
									 
									<br />	
								</td>
                            <td colspan="2"></td>
								
							</tr>

							
							
						</tbody></table>  


						</div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
						<table class="table-responsive">
							<tbody><tr>
								<td colspan="2">
								<table class="table-responsive">
									<tbody><tr>
										<th><span><strong style="color: #006ead;">Nombre:</strong></span></th>
										<td>&nbsp; <small><span id="nombreInterno"></span></small><br /></td>
									</tr>
									<tr>
										<th>
                                            <span><strong style="color: #006ead;">Edad:</strong></span></th>
                                            <td>&nbsp;&nbsp;<small><span id="edadInterno">Fecha de nacimiento no registrada</span></small></td>
									</tr>
									<tr>
										<th><span><strong style="color: #006ead;">Sexo:</strong></span></th>
										<td>&nbsp;  <small><span id="sexoInterno"></span></small><br /></td>
									</tr>
									<tr>
										<th> <span><strong style="color: #006ead;">Domicilio:</strong> <br></span></th>
										<td>&nbsp;&nbsp;<small><span id="domicilioInterno"></span></small><br /> </td>
									</tr>
									
									
									
								</tbody></table>
								</td>
								
							
							
								

							</tr>
							<tr>
								<td colspan="2">
									<div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
										</div>
								</td>
								<td align="left"><div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div></td>
								<td></td>
							</tr>
							
						</tbody></table>  
						 </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
						<table class="table-responsive">
							<tbody><tr>
								<td colspan="2">
								<table class="table-responsive">
									
									<tbody><tr>
										<th><span><strong style="color: #006ead;">Institución a disposición:</strong></span></th>
										<td>&nbsp; <small><span id="centroInterno"></span></small></td>
									</tr>
									<tr>
										<th> <span><strong style="color: #006ead;">No. de remisión:</strong> </span></th>
										<td>&nbsp; <small><span id="expedienteInterno"></span></small></td>
									</tr>
									<tr>
										<th> <span><strong style="color: #006ead;">Colonia:</strong> </span></th>
										<td>&nbsp; <small><span id="coloniaInterno">No registrada</span></small></td>
									</tr>
									<tr>
										<th> <span><strong style="color: #006ead;">Municipio:</strong></span></th>
										<td>&nbsp; <small><span id="municipioInterno">No registrado</span></small></td>
									</tr>
									
								</tbody></table>
								</td>
								
							
							
								

							</tr>
							<tr>
								<td colspan="2">
									<div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2">
										</div>
								</td>
								<td align="left"><div class=" col-lg-2 col-md-2 col-sm-2 col-xs-2"></div></td>
								<td></td>
							</tr>
							
						</tbody></table>  
						 </div>
        </article>

            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="row" id="addentry">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
            <a class="btn btn-success  idCalificar  " id="idCalificar" ><i class="fa fa-gavel" ></i>&nbsp;Calificar detenido</a>
            <a   class="btn btn-md btn-default  regresar" href="sentence_entrylist.aspx" id="regresar"><i class="fa fa-mail-reply"></i>Regresar </a>
        </div>
    </div>
                <br />
            </article>



            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-motivos-2" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-group"></i></span>
                        <h2>Motivo detención </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%" >
                                <thead>
                                    <tr>
                                        <th data-class="expand">Motivo detención</th>
                                        <th data-class="expand">Artículo</th>                                        
                                        <th data-class="expand">Multa</th>
                                        <th data-class="expand">Horas de arresto</th> 
                                        <th data-hide="phone,tablet">Acciones</th>                                       
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
    <div id="blockuser-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de <strong>&nbsp<span id="usernameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                            
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <div class="row">                                
                                <section>
                                    <label>Motivo detención</label>
                                    <label class="select" id="contenedorSelect">
                                        <select name="rol" id="dropdown" runat="server">
                                        </select>
                                        <i></i>
                                    </label>                                   
                                </section>
                                <section>
                                    <label class ="input" id="SalarioMinimo" ></label>
                                </section>
                                <section>
                                    <label class="input" id="idArticulo"></label>
                                </section>
                                <section>
                                    <label class="input" id="idMultaMinimo"></label>
                                </section>
                                <section>
                                    <label class="input" id="idMultaMaximo"></label>
                                </section>
                                <section>
                                    <label class="input"  id="idHorasdeArresto"></label>
                                </section>                                
                                <section>
                                    <a class="btn btn-sm btn-success" id="btnvsmin">Multa en salarios mínimos</a>
                                    <%--Multa en salarios mínimos <input type="checkbox" id="checkMinimo" />--%>                                    
                                </section>
                                <section>
                                    <a class="btn btn-sm btn-danger" id="btnvsmax">  Multa en salarios máximos </a>
                                    <%--Multa en salarios máximos <input type="checkbox" id="checkMaximo" />--%>
                                </section>
                                <section>
                                    <label class="label">Multa sugerida <a style="color: red">*</a></label>
                                    <div class="input-group" id="contenedorMulta">
                                        <span class="input-group-addon" style="background-color:#eee;">$</span>                                        
                                        <input  title="" type ="text" class="form-control numeric alptext" name="multaSugerida" id="multaSugerida"/>
                                        <b class="tooltip tooltip-bottom-right">Ingrese la multa sugerida.</b>
                                    </div>
                                </section>
                            </div>
                        </fieldset>
                        <footer>                          
                            <!--<a class="btn btn-sm btn-default clear" "><i class="fa fa-eraser"></i>&nbsp;Limpiar formulario</a>-->
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                             
                        </footer>
                    </div>                   
                </div>
            </div>
        </div>
    </div>

     <div id="add-datosPersonales" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4>Datos personales</h4>
                </div>

                <div class="modal-body col">
                    <div class="col-lg-8">
                    <section>
                    <label>Nombre completo</label>
                    <input id="nombreInterno2" style="width:300px" disabled="disabled" /><br /><br />
                    </section>
                    <section>
                    <label style="width:110px;">Edad</label>
                    <input type="text" id="edad" style="width:300px;" disabled="disabled"/><br /><br />
                    </section>
                    <section>
                    <label style="width:110px">Situación</label>
                    <input type="text" id="situacion" style="width:300px" disabled="disabled" /><br /><br />
                    </section>
                        <section>
                    <label style="width:180px">Fecha y hora de registro</label>
                    <input type="datetime" id="horaRegistro" disabled="disabled" />
                        </section>
                        <section>
                        <label style="width:180px">Fecha y hora de salida</label>
                        <input type="datetime" id="salida" disabled="disabled" /><br /><br />
                            <label>Domicilio</label><br />
                    </section>
                        </div>
                    <div class="col-lg-4">
                        <section class="col col-10 text-center">
                           <img id="avatar3" class="img-thumbnail avatar" alt="" runat="server" src="~/Content/img/avatars/male.png" height="120" width="120" /><br /><br />
                        </section>
                    </div>
                    <div>
                        <section>
                            
                            <textarea style="height:90px; width:400px;" id="domicilio" disabled="disabled"></textarea>
                        </section>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="cantidadmotivos"  />
    <input type="hidden" id="SalarioMin" />
    <input type="hidden" id="vsmin" />
    <input type="hidden" id="vsmax" />
       <input type="hidden" id="Ivsmin" />
    <input type="hidden" id="Ivsmax" />
    <input type="hidden" id="hideid" runat="server" />    
    <input type="hidden" id="WERQEQ" runat="server" value="" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
     <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {                        

            $("#multaSugerida").on({
                "focus": function(event) {
                    $(event.target).select();
                },
                "keyup": function(event) {
                    $(event.target).val(function(index, value) {
                        return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                    });
                }                
            });
            $('#btnvsmin').hide();
            $('#btnvsmax').hide();

            $("#checkMinimo").change(function () {
                if ($("#checkMinimo").is(":checked")) {
                    $("#checkMaximo").prop("checked", false);
                    var valMinimo = $("#idMultaMinimo").text();
                    var index = valMinimo.toString().indexOf(":") + 1;
                    var indexFinal = valMinimo.toString().length;
                    var newVal = valMinimo.toString().substr(index, indexFinal);
                    var newValSigns = newVal.replace(" ", "");
                    $("#multaSugerida").val(newValSigns.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ","));
                }
                else {
                    if (!$("#checkMinimo").is(":checked")) {
                        $("#multaSugerida").val("");
                    }
                }
            });

            $("#checkMaximo").change(function () {
                if ($("#checkMaximo").is(":checked")) {
                    $("#checkMinimo").prop("checked", false);
                    var valMaximo = $("#idMultaMaximo").text();
                    var index = valMaximo.toString().indexOf(":") + 1;
                    var indexFinal = valMaximo.toString().length;
                    var newVal = valMaximo.toString().substr(index, indexFinal);
                    var newValSigns = newVal.replace(" ", "");
                    $("#multaSugerida").val(newValSigns.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ","));
                }
                else {
                    if (!$("#checkMaximo").is(":checked")) {
                        $("#multaSugerida").val("");
                    }
                }
            });

            //var usuario = "nombreApellido";

            //if (!exist(usuario)) {
            //    usuario = "iniApellido";
            //}

            //if (!exist(usuario) && nombre.Count > 1) {
            //    usuario = "nombreSegApellido";
            //}

            pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            var _cantidadregistros="0"

            $("#ctl00_contenido_dropdown").change(function () {
              
                var id = $("#ctl00_contenido_dropdown").val();
               
                $("#idArticulo").text($("#ctl00_contenido_dropdown").val() )
                $.ajax({

                    type: "POST",
                    url: "MotivoDetencionInterno.aspx/GetMotivoDetencionById",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ _id: id }),
                    cache: false,
                    success: function (data) {
                         data = data.d;                         
                         //loadCustomers(data.id);      
                        $("#SalarioMinimo").text('Salario Mínimo: $' + data.SalarioMinimo);
                        $("#SalarioMin").val(data.SalarioMinimo);
                        $("#vsmin").val(data.MultaMinimo);
                        $("#vsmax").val(data.MultaMaximo);
                         $("#idArticulo").text('Artículo: '+ data.Articulo); 
                        $("#idMultaMinimo").text('Multa salarios mínimos: ' + data.MultaMinimo); 
                        $("#idMultaMaximo").text('Multa salarios máximos: '+ data.MultaMaximo); 
                        $("#idHorasdeArresto").text('Horas de arresto:' + data.horaArresto); 
                         $('#btnvsmin').show();
                        $('#btnvsmax').show();
                        $("#Ivsmin").val(data.ImporteMultaMinimo);
                        $("#Ivsmax").val(data.ImporteMultaMaximo);
                        // localStorage.setItem("InternoId", data.InternoId);
                        $("#multaSugerida").val("");
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de motivos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            });

       $("body").on("click", "#btnvsmin", function () {
           
               $("#multaSugerida").val($("#Ivsmin").val());
            });
              $("body").on("click", "#btnvsmax", function () {
           
               $("#multaSugerida").val($("#Ivsmax").val());
            });


            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = false;

            function Traecantidadmotivos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "MotivoDetencionInterno.aspx/TieneMotivos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                    
                        if (resultado.exitoso) {
                            
                            
                            $("#cantidadmotivos").val(resultado.obj.registros);
                            _cantidadregistros = resultado.obj.registros;


                            if (_cantidadregistros > 0) {
                                //$('#idCalificar').attr("href", "calificacion.aspx?tracking=" + trackingid);
                                //$('#idCalificar').click();
                                //Response.redirect("sentence/calificacion.aspx?tracking="+trackingid);}
                                location.href = "calificacion.aspx?tracking="+trackingid;
                            }
                            else
                            {
                                location.href = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Sentence/calificacion.aspx?tracking=" + trackingid;
                            }
                        }
                        else {
                            ShowError("Error!", "No fue posible consultar los motivos. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                    }
                });
            }

            function CargarDatos(trackingid) {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "MotivoDetencionInterno.aspx/getdatos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        
                        if (resultado.exitoso) {
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            $('#ctl00_contenido_hideid').val(resultado.obj.Id);
                            $('#ctl00_contenido_hide').val(resultado.obj.TrackingId);
                            $('#nombreInterno').text(resultado.obj.Nombre);
                            $('#expedienteInterno').text(resultado.obj.Expediente);
                            $('#centroInterno').text(resultado.obj.Centro);
                            var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                             var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                                   if (resultado.obj.RutaImagen == "")
                            {
                               imagenAvatar = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png"; 
                            }
                            $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);

                            
                             $('#edadInterno').text(resultado.obj.Edad);
                             if (resultado.obj.Sexo == 1)
                                    $('#sexoInterno').text("Masculino");
                                else
                                $('#sexoInterno').text("Femenino");

                            $('#municipioInterno').text(resultado.obj.municipioNombre);
                            $('#domicilioInterno').text(resultado.obj.domiclio);
                            $('#coloniaInterno').text(resultado.obj.coloniaNombre);
                           
                            $('#ctl00_contenido_hideid').val(resultado.obj.Id);
                            
                        }
                        else {
                            ShowError("Error!", "No fue posible cargar la información del motivo. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }

                        $('#main').waitMe('hide');

                    },
                    error: function () {
                        $('#main').waitMe('hide');
                    }
                });
            }
                   function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
              if (url.indexOf("~/") == 0) {
                  url = baseUrl + url.substring(2);
              }
              return url;
            }

            $("body").on("click", "#datospersonales", function () {
                //var id = $(this).attr("data-id");
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    //MovimientoCelda.TrackingId = param;

                    CargarDatosPersonales(param);
                }
            });
               
               
        
            function CargarDatosPersonales(trackingid) {
                //jc
                
                startLoading();
                  $.ajax({
                      type: "POST",
                      url: "MotivoDetencionInterno.aspx/getdatosPersonales",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                              
                              $('#nombreInterno2').val(resultado.obj.Nombre);
                              $('#edad').val(resultado.obj.Edad);
                              $('#situacion').val(resultado.obj.Situacion);
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                             // alert(imagenAvatar);
                              $('#ctl00_contenido_avatar3').attr("src", imagenAvatar)
                               //$('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).RutaImagen);

                              
                              $('#horaRegistro').val(resultado.obj.Registro);
                              $('#salida').val(resultado.obj.Salida);
                              $('#domicilio').val(resultado.obj.Domicilio);
                                  $("#add-datosPersonales").modal("show");
                          } else {
                              ShowError("Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
            }


            function CargaTabla() {
                window.table = $('#dt_basic').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        if (!data["Habilitado"]) {
                            $('td', row).eq(0).addClass('strikeout');
                            $('td', row).eq(1).addClass('strikeout');
                            $('td', row).eq(2).addClass('strikeout');                                                
                            $('td', row).eq(3).addClass('strikeout');                                                                            
                        }
                    },
                    ajax: {
                        type: "POST",
                        url: "MotivoDetencionInterno.aspx/getCustomers",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            parametrosServerSide.emptytable = window.emptytable;
                           
                            var param2 = RequestQueryString("tracking");
                            if (param2 != undefined) {
                                parametrosServerSide.TrackingProcesoId=param2
                                
                            }
                           
                            return JSON.stringify(parametrosServerSide);                            
                        }                       
                    },                     
                    columns: [
                        {
                            name: "Motivo",
                            data: "Motivo"
                        },
                        {
                            name: "Articulo",
                            data: "Articulo"
                        },
                        {
                            name: "MultaSugerida",
                            data: "MultaSugerida",

                        },
                        {
                            name: "HorasdeArresto",
                            data: "HorasdeArresto"
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 2,
                             orderable: false,
                            render: function (data, type, row, meta) {                                
                               if (row.MultaSugerida > 0) return "<div class='text-right'>" + toCurrency(row.MultaSugerida) + "</div>";
                                else return "0.00";
                                    
                            }
                        },
                        {
                            targets: 4,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var multaSugerida = row.MultaSugerida != null ? row.MultaSugerida.toFixed(2) : 0;                                
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "edit";
                                var editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-tracking="' + row.TrackingIdMotInterno + '" data-motivoId="' + row.MotivoDetencionId + '" data-multaSugerida="' + multaSugerida + '" data-id="' + row.IdMotInterno + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                var habilitar = "";
                                
                                if (!row.Habilitado) {
                                    txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                                }
                                else {
                                    txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                                }
                                if ($("#ctl00_contenido_KAQWPK").val() == "true") {
                                    editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-tracking="' + row.TrackingIdMotInterno + '" data-motivoId="' + row.MotivoDetencionId + '" data-multaSugerida="' + multaSugerida + '"data-id="' + row.IdMotInterno + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                                }
                                else
                                {
                                    
                                    editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="javascript:void(0);" data-tracking="' + row.TrackingIdMotInterno + '" data-motivoId="' + row.MotivoDetencionId + '" data-multaSugerida="' + multaSugerida + '"data-id="' + row.IdMotInterno + '" title="Ver"><i class="glyphicon glyphicon-search"></i></a>&nbsp;';
                                }
                                if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-value="' + row.Motivo + '" data-id="' + row.TrackingIdMotInterno + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';
                                
                                return editar + habilitar
                                    ;
                            }
                        }
                    ]

                });

            }

            /// se llena  combo
             $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_itemnombre").val("");
                $("#ctl00_contenido_itemcapacidad").val("");
                $("#ctl00_contenido_dropdown").val("");              
            });

            
            function CargarListado(id) {
                $('#ctl00_contenido_peligrosidad').empty();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "MotivoDetencionInterno.aspx/getListadoCombo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ item: id }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ctl00_contenido_dropdown');
                        Dropdown.empty();
                        Dropdown.append(new Option("[Motivo detención]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });
                        if (id != "") {
                            Dropdown.val(id);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de motivos de detención. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            // function ShowSelected()
            //    {
            //    /* Para obtener el valor */
            //    var cod = document.getElementById("dropdown").value;
            //    alert(cod);
 
            //    /* Para obtener el texto */
            //    var combo = document.getElementById("dropdown");
            //    var selected = combo.options[combo.selectedIndex].text;
            //    alert(selected);
            ////}
            

            

             $("body").on("click", ".add", function () {
                 clearModal();
                 $("#add-modal").modal("show");
                 $("#add-modal-title").empty();
                 $("#form-modal-title").html("Agregar registro");
                 $("#btnsave").attr("data-trackingid", "");
                 $("#btnsave").attr("data-id", "");
                 CargarListado(0);                 
                 //ShowSelected();
                 window.emptytableadd = false;
                 window.tableadd.api().ajax.reload();
            });

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");
            });

            $("body").on("click", ".edit", function () {
                clearModal();
                $("#add-modal").modal('show');
                $("#add-modal-title").empty();
                $("#form-modal-title").html("Editar registro");
                $("#btnsave").attr("data-trackingid", $(this).attr("data-tracking"));
                $("#btnsave").attr("data-id", $(this).attr("data-id"));
                CargarListado($(this).attr("data-motivoid"));                
                $("#ctl00_contenido_dropdown").trigger("change");
                var multa = $(this).attr("data-multasugerida").replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");                
                $("#multaSugerida").val(multa);
            });

            $("body").on("click", ".blockuser", function () {
                var usernameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-trackingid", $(this).attr("data-id"));
                $("#blockuser-modal").modal("show");
            });   

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingid = $(this).attr("data-trackingid");
                startLoading();
                $.ajax({
                    url: "MotivoDetencionInterno.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        id: trackingid,
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                           
                            $("#blockuser-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho!</strong>" +
                                " Registro " + JSON.parse(data.d).mensaje + " con éxito.</div > ");
                            ShowSuccess("Bien hecho!", "Registro " + JSON.parse(data.d).mensaje + " con éxito.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            ShowError("Error! Algo salió mal y no fue posible afectar el estatus del motivo. Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });
            init();
          

            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#add").show();
                    $("#idCalificar").show();
                }
                else
                {
                    $("#add").hide();
                    $("#idCalificar").hide();
                    $("#btnsave").hide();
                   
                    $("#id_input").prop("disabled", true);

                    $("#ctl00_contenido_dropdown").prop("disabled",true);

                }
                var param = RequestQueryString("tracking");
                if (param != undefined) {
                    CargarDatos(param);
                    CargaTabla();

                    
                }
            }

            

            function CargarContrato(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "MotivoDetencionInterno.aspx/getInterno",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;                         
                         loadCustomers(data.ClienteId);                         
                         $("#motivo").val(data.Motivo);
                         $("#descripcion").val(data.Descripcion);                         
                         $("#articulo").val(data.Articulo); 
                         $("#multa").val(data.Multa); 
                         $("#horaArresto").val(data.horaArresto); 
                         $('#ctl00_contenido_hideid').val(data.Id);
                         $('#ctl00_contenido_hide').val(data.TrackingId);                         
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("Error!", "No fue posible cargar la información del motivo. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }


            function ObtenerValores() {                                
                var contrato = {
                    Id: $("#btnsave").attr("data-id"),
                    TrackingId: $("#btnsave").attr("data-trackingid"),
                    MotivoDetencionId: $("#ctl00_contenido_dropdown").val(),                    
                    InternoId: $('#ctl00_contenido_hideid').val(),
                    MultaSugerida: $("#multaSugerida").val()
                };
                
                return contrato;
            }

            $("body").on("click", "#idCalificar", function () {
              
                    var param = RequestQueryString("tracking");
                if (param != undefined) {
                    Traecantidadmotivos(param);

                   // alert($("#cantidadmotivos").val());

                    //alert(numero);
                    ////alert($("#cantidadmotivos").val());
                    //if (numero != "0") {
                    //    $('#idCalificar').attr("href", "calificacion.aspx?tracking=" + param);

                    //}
                    //else
                    //{
                    //    ShowAlert("Alerta","Registre un motivo de detención")
                    //}
                }
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {                    
                    guardar();                    
                }                                
            }); 

            function validar() {
                var esvalido = true;

                if ($("#ctl00_contenido_dropdown").val() == "0") {
                    ShowError("Motivo", "El motivo es obligatorio.");
                    $('#ctl00_contenido_dropdown').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_dropdown').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_dropdown').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_dropdown').addClass('valid');
                }

                if ($("#multaSugerida").val() == "") {
                    ShowError("Multa sugerida", "La multa sugerida es obligatoria.");
                    $('#multaSugerida').parent().removeClass('state-success').addClass("state-error");
                    $('#multaSugerida').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#multaSugerida').parent().removeClass("state-error").addClass('state-success');
                    $('#multaSugerida').addClass('valid');
                }

                return esvalido;
            }

            function clearModal() {
                $("#ctl00_contenido_dropdown").val("0");
                $("#SalarioMinimo").text('');
                $("#idArticulo").text(''); 
                $("#idMultaMinimo").text(''); 
                $("#idMultaMaximo").text(''); 
                $("#idHorasdeArresto").text('');
                $("#multaSugerida").val("");
                $("#checkMinimo").attr("checked", false);
                $("#checkMaximo").attr("checked", false);
                $("#contenedorMulta").removeClass('state-success');
                $("#contenedorMulta").removeClass('state-error');
                $("#contenedorMulta").removeClass('valid');
                $("#ctl00_contenido_dropdown").removeClass('state-success');
                $("#ctl00_contenido_dropdown").removeClass('state-error');
                $("#ctl00_contenido_dropdown").removeClass('valid');
                $("#contenedorSelect").removeClass('valid');
                $("#contenedorSelect").removeClass('state-success');
                $("#contenedorSelect").removeClass('state-error');
            }

            function guardar() {
                startLoading();
                var contrato = ObtenerValores();
                
                $.ajax({
                    type: "POST",
                    url: "MotivoDetencionInterno.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'contrato': contrato }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            clearModal();
                            //$('#ctl00_contenido_hideid').val(resultado.Id);
                            //$('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "La información del motivo de detención se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            ShowSuccess("Bien hecho!", "La información del  motivo de detención se  " + resultado.mensaje + " correctamente.");  
                            
                            $('#main').waitMe('hide');
                            $("#add-modal").modal("hide");
                            window.emptytable = false;
                             window.table.api().ajax.reload();
                            //  var param = RequestQueryString("tracking");
                            //if (param != undefined) {
                            //    var url = "MotivoDetencionInterno.aspx?tracking=" + param ;
                            //    $(location).attr('href', url);
                           // }
                        }
                        else {
                            $('#main').waitMe('hide');
                           
                            if (resultado.mensaje == "El interno ya tiene asignado ese motivo de detención") {
                                  $("#ctl00_contenido_lblMessage").html("<div class='alert alert-alert fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Alerta! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                                ShowAlert("Motivo de detención interno", resultado.mensaje);
                            }
                            else
                            {
                                 $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                                ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                            }
                        }
                    }
                });
            }


        });
         
        </script>
</asp:Content>
