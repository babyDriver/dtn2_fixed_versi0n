﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="control_pertenencias.aspx.cs" Inherits="Web.Application.Control_Pertenencias.control_pertenencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Control de pertenencias</li>
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }                   
        textarea {            
            margin-top:10px;
            width:100%;
            border: 1px solid #BDBDBD;
        }
        textarea:focus {
            box-shadow: 0 0 5px #5D98CC;            
            border: 1px solid #5D98CC;            
        }
        textarea:hover {
            box-shadow: 0 0 0px #5D98CC;            
            border: 1px solid #5D98CC;            
        }        
     
        input[type=checkbox]{
            -ms-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -webkit-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
            padding: 10px;
            cursor: pointer;
        }
        .checkboxtext{
            font-size: 100%;
            display: inline;       
            color: #004987;
        }
        .checkboxtext:hover{
            cursor:pointer;
        }
        .errorInputTabla{
            background-color:#fff0f0;
            border-color:#A90329;
        }
    </style>
    <link href="../../Content/css/bootstrap.css" rel="stylesheet"/>  
    <div class="scroll">
        <!--
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-group fa-fw "></i>
                Control de pertenencias / evidencias
            </h1>
        </div>
    </div>    -->
     <p></p>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>    
    <section id="widget-grid" class="">                            
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-controlpertenencia-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-list-alt"></i></span>
                        <h2>Control de pertenencias / evidencias </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body"">                            
                            <%--<div id="registrer" class="smart-form">--%>
                                <fieldset>
                                    <legend align="center"><b>Selecciona para filtrar</b></legend>
                                    <div class="row">
                                        <section class="col-md-4">
                                            <%--<label>Libre con pertenencias / evidencias</label>                                            --%>                                            
                                            <span class="checkboxtext" id="spanUno">                                              
                                              <b>Libre con pertenencias / evidencias&nbsp;&nbsp;</b>
                                            </span>
                                            <input type="checkbox" id="egreso" name="egreso" />                                            
                                        </section>                                         
                                        <section class="col-md-4">
                                            <%--<label>Detenidos con pertenencias / evidencias</label>                                            --%>
                                            <span class="checkboxtext" id="spanDos">                                              
                                              <b>Detenidos con pertenencias / evidencias&nbsp;&nbsp;</b>
                                            </span>
                                            <input type="checkbox" checked="checked" id="ingresoReingresoConPertenencias" name="ingresoReingresoConPertenencias" />                                            
                                        </section>                             
                                        <section class="col-md-4">
                                            <%--<label>Detenidos sin pertenencias</label>                                            --%>
                                            <span class="checkboxtext" id="spanTres">                                              
                                              <b>Detenidos sin pertenencias&nbsp;&nbsp;</b>
                                            </span>
                                            <input type="checkbox" id="ingresoReingresoSinPertenencias" name="ingresoReingresoSinPertenencias" />                                            
                                        </section>                             
                                    </div>
                                    <%--<br />--%>
                                   
                                    <div class="row">
                                        <section class="col-md-2">
                                                 <a class="btn btn-success btn-md ShowModal" style="margin-top:22px;padding:5px;height:35px; margin-right:15px;"><i class="fa fa-search"></i> Buscar detenido</a>
                                            </section>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">                                            
                                            <label style="color: #3276b1; text-align: right;font-weight:bold; margin-right:8px">Año </label>
                                            <select id="filterYear" class="select2" ></select>
                                        </div>
                                    </div>
                                    <br />
                                    <%--<div class="row">                                        
                                        <section class="col-md-4">
                                            <a href="javascript:void(0);" class="btn btn-md btn-default add" id="buscarData"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                        </section>                                            
                                    </div>
                                    <br />--%>
                                    
                                 </fieldset>
                            <%--</div>--%>
                            <fieldset>
                                <legend></legend>
                                <div class="row">                                        
                                    <div class="col-lg-6">                                    
                                        <a href="javascript:void(0);" class="btn btn-md btn-default add" id="agregarPertenencia" onclick="reload()"><i class="fa fa-pencil"></i>&nbsp;Agregar/editar pertenencia(s) </a>                                    
                                    </div>                                
                                    <div class="col-lg-6" align="right">
                                        <div class="btn-group" >
                                                <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Reportes control de pertenencias
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="btn-sm " title="Resumen de evidencias" href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/evidencias_report.aspx">
                                                            Resumen de evidencias
                                                        </a>
                                                    </li><li class="divider"></li>
                                                    <li>
                                                        <a class="btn-sm " title="Recibos de devolución" href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/recibo_devolucion.aspx">
                                                            Recibos de devolución
                                                        </a>
                                                    </li><li class="divider"></li>
                                                    <li>
                                                        <a class="btn-sm " title="Recibos de donación" href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/recibo_donacion.aspx">
                                                            Recibos de donación
                                                        </a>
                                                    </li><li class="divider"></li>
                                                </ul>
                                            </div>
                                        <a href="javascript:void(0);" class="btn btn-md btn-primary add disabled" id="devolucion"><i class="fa  fa-retweet"></i>&nbsp;Devolución </a>
                                        <a href="javascript:void(0);" class="btn btn-md btn-success add disabled" id="donar"><i class="fa  fa-cube"></i>&nbsp;Donar/asignar </a>                                                                                                            
                                    </div>                                
                                </div>
                            </fieldset>
                            <br />
                            <table id="dt_basic" class="table table-responsive table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand"></th>                                        
                                        <th>No. remisión</th>
                                        <th data-hide="phone,tablet">Nombre</th>
                                        <th data-hide="phone,tablet">Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <th data-hide="phone,tablet">Clasificación</th>
                                        <th data-hide="phone,tablet">Descripción</th>
                                        <th data-hide="phone,tablet">Cantidad</th>
                                        <th data-hide="phone,tablet">Bolsa</th>
                                        <th data-hide="phone,tablet">Estatus</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>
                            </table>                                                      
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
    <div id="blockuser-modal" class="modal fade" data-backdrop="static" data-keyboard="true" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de <strong>&nbsp<span id="usernameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">                        
                            <div class="row">
                                <div style="width:75%; float:left">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Pertenencia de:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="pertenenciaDe" id="pertenenciaDe" disabled="disabled" class="form-control" style="margin-bottom:20px;" />                                    
                                        </div>                                
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Pertenencia:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="pertenencia" id="pertenencia" disabled="disabled" class="form-control" style="margin-bottom:20px;" />                                    
                                        </div>                                
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Observación:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="observacion" id="observacion" disabled="disabled" class="form-control" style="margin-bottom:20px;" />                                    
                                        </div>                                
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Clasificación:</label>
                                        <div class="col-md-7">
                                            <select name="clasificacion" id="clasificacion" style="width: 100%;" class="select2" disabled="disabled"></select>                                            
                                        </div>                                
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;margin-top:20px;">Bolsa:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="bolsa" id="bolsa" disabled="disabled" class="form-control" style="margin-bottom:20px;margin-top:20px;" />                                    
                                        </div>                                
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Cantidad:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="cantidad" id="cantidad" disabled="disabled" class="form-control" style="margin-bottom:20px;" />                                    
                                        </div>                                
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Usuario que recibió:</label>
                                        <div class="col-md-7">
                                            <input type="text" name="usuario" id="CPusuario" disabled="disabled" class="form-control" style="margin-bottom:20px;" />                                    
                                        </div>                                
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="color:#3276b1; text-align: right;padding-right:15px;">Casillero:</label>
                                        <div class="col-md-7">
                                            <select name="casillero" id="casillero2" style="width: 100%;" class="select2" disabled="disabled"></select>                                            
                                        </div>                                
                                    </div>
                                </div>
                                <div style="width:25%; float:right">
                                     <img id="foto_pertenencia" class="img-thumbnail text-center" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/male.png" alt="fotografía de la pertenencia / evidencia" style="width:150px; height:150px;margin-top:105px;" />                                    
                                </div>                                 
                            </div>                        
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cerrar </a>                             
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="form-modal-tabla"   role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-tabla"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-tabla" class="smart-form">                        
                            <div class="row">
                                <fieldset style="padding: 0px 14px 5px;">
                                    <div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label  style="color: dodgerblue" class="input">Pertenencia de:</label>
                                                    <label class="input">
                                                        <input type="text" id="pertenenciaDeTabla" name="pertenenciaDeTabla" disabled="disabled" />                                            
                                                    </label>
                                                </section>                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label  style="color: dodgerblue" class="input">Usuario que registró:</label>
                                                    <label class="input">
                                                        <input id="usuarioQueRegistro" name="usuarioQueRegistro" class="alptext" />
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>           
                                        <br />
                                        <div class="row">
                                            <table id="dt_basic_tabla" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                    <tr>    
                                                        <th data-class="expand"></th>
                                                        <th>Id</th>
                                                        <th>Nombre</th>
                                                        <th data-hide="phone,tablet">Cantidad</th>
                                                        <th data-hide="phone,tablet">Estatus</th>
                                                        <th data-hide="phone,tablet">Observacion</th>
                                                        <th data-hide="phone,tablet">Clasificacion</th>
                                                        <th data-hide="phone,tablet">Bolsa</th>                                                        
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>      
                                        <br />
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label  style="color: dodgerblue" class="input">Persona que recibe: <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <input id="personaQueRecibe" name="personaQueRecibe" class="alptext" />
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>             
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label  style="color: dodgerblue" class="input">Condición de entrega: <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <textarea id="condicionDeEntrega" name="condicionDeEntrega" class="alptext" ></textarea>
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>            
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label  style="color: dodgerblue">Entrega: <a style="color: red">*</a></label>                                                                                                                                    
                                                    <select name="entrega" id="entrega" style="width: 100%" class="select2"></select>                                            
                                                </section> 
                                            </div>
                                        </div>            
                                    </div>
                                </fieldset>
                            </div>
                       
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" id="btnsaveTabla"><i class="fa fa-save"></i>&nbsp;Guardar </a>                                                         
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelTabla"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                             
                            </div>
                        </footer>>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="form-modal-donacion" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="form-modal-title-donacion"></h4>
                </div>
                <div class="modal-body">
                    <div id="register-form-donacion" class="smart-form">                        
                            <div class="row">
                                <fieldset style="padding: 0px 14px 5px;">
                                    <div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label style="color: dodgerblue" class="input">Pertenencia de:</label>
                                                    <label class="input">
                                                        <input type="text" id="pertenenciaDeDonacion" name="pertenenciaDeDonacion" disabled="disabled" />                                            
                                                    </label>
                                                </section>                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label style="color: dodgerblue" class="input">Usuario que registró:</label>
                                                    <label class="input">
                                                        <input id="usuarioQueRegistroDonacion" name="usuarioQueRegistroDonacion" class="alptext" />
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>           
                                        <br />
                                        <div class="row">
                                            <table id="dt_basic_tabla_donacion" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                    <tr>    
                                                        <th data-class="expand"></th>
                                                        <th>Id</th>
                                                        <th>Nombre</th>
                                                        <th data-hide="phone,tablet">Cantidad</th>
                                                        <th data-hide="phone,tablet">Estatus</th>
                                                        <th data-hide="phone,tablet">Observacion</th>
                                                        <th data-hide="phone,tablet">Clasificacion</th>
                                                        <th data-hide="phone,tablet">Bolsa</th>                                                        
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>      
                                        <br />
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label  style="color: dodgerblue">Institución: <a style="color: red">*</a></label>                                                                                                                                    
                                                    <select name="institucion" id="institucion" style="width: 100%" class="select2"></select>                                            
                                                </section>                                                 
                                            </div>                                            
                                        </div> 
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label style="color: dodgerblue" class="input">Persona que recibe: <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <input id="personaQueRecibeDonacion" name="personaQueRecibeDonacion" class="alptext" />
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>             
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label style="color: dodgerblue" class="input">Condición de entrega: <a style="color: red">*</a></label>
                                                    <label class="input">
                                                        <textarea id="condicionDeEntregaDonacion" name="condicionDeEntregaDonacion" class="alptext"></textarea>
                                                    </label>
                                                </section> 
                                            </div>
                                        </div>            
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <label  style="color: dodgerblue">Entrega: <a style="color: red">*</a></label>                                                                                                                                    
                                                    <select name="entregaDonacion" id="entregaDonacion" style="width: 100%" class="select2"></select>                                            
                                                </section> 
                                            </div>
                                        </div>   
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="col col-lg-12">
                                                    <input type="radio" id="donarId" name="accion" value="Donar" checked="checked" /> Donar <br />
                                                    <input type="radio" id="consignarId" name="accion" value="Consignar" /> Consignar <br />
                                                </section> 
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        
                        
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" id="btnsaveDonacion"><i class="fa fa-save"></i>&nbsp;Guardar </a>                             
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelDonacion"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                             
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal-agregar" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>

                    <h4 class="modal-title" id="form-modal-title-agregar"></h4>
                </div>
                <div class="modal-body">
                    <a href="javascript:void(0);" class="btn btn-md btn-default" id="addRow"><i class="fa fa-plus"></i>&nbsp;Agregar nueva fila </a>
                    <div id="register-form-agregar" class="smart-form ">
                        <div class="row">
                            <fieldset>
                                <table style="width:100%">
                                    <tr>
                                        <td class="col col-6" style="padding:0">
                                            <label style="color: dodgerblue" class="input">Detenido <a style="color: red">*</a></label>
                                        </td>
                                        <td class="col col-6" style="padding:0">
                                            <label style="color: dodgerblue; margin-left:2%" class="input">Número de remisión</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col col-6" style="padding:0">
                                            <select name="internoAgregar" id="internoAgregar" style="width:98%" class="select2" onchange="if (this.selectedIndex) getNumInterno();"></select><i></i>
                                        </td>
                                        <td class="col col-6" style="padding:0">
                                            <label class="input">
                                                <i class="icon-append fa fa-hashtag"></i>
                                                <input type="text" name="numInterno" id="numInterno" disabled="disabled" class="form-control" style="margin:0 0 19px 2%;width:98%;" />                                    
                                            </label>  
                                        </td>
                                    </tr>
                                </table> 
                                
                                <div class="row" style="width:100%;margin:0 auto;">
                                    <table id="dt_nuevas_pertenencias" class="table table-striped table-bordered table-hover" width="100%" >
                                        <thead>
                                            <tr>
                                                <th  style="width:8%  !important; "></th>
                                                <th style="text-align:center;" >Fecha de entrada</th>                                                
                                                <th style="text-align:center">Pertenencia</th>                                                
                                                <th style="text-align:center">Observaciones</th>
                                                <%--<th style="text-align:center">Bolsa</th>--%>
                                                <th style="text-align:center">Cantidad</th>
                                                <th style="text-align:center">Clasificación</th>
                                                <th style="text-align:center">Casillero</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>

                                                <%--<th data-hide="phone,tablet"></th>
                                                <th data-class="expand">Fecha de entrada</th>                                                
                                                <th data-hide="phone,tablet">Pertenencia</th>                                                
                                                <th data-hide="phone,tablet">Observaciones</th>
                                                <th data-hide="phone,tablet">Bolsa</th>
                                                <th data-hide="phone,tablet">Cantidad</th>
                                                <th data-hide="phone,tablet">Clasificación</th>
                                                <th data-hide="phone,tablet">Casillero</th>--%>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <br />
                                <section>
                                    <label class="input" style="color: dodgerblue">Fotografía</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <asp:FileUpload  name="fotografia" id="fotografia" runat="server" />                                        
                                    </label>                                    
                                </section>                                                             
                            </fieldset>
                        </div>
                        <footer>
                            <div class="row" style="display: inline-block; float: right">
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default save" id="btnsaveNueva"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                                <a style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelNueva"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="Tabla_detenidos" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-mg">
            <div class="modal-content row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    
                    <h4><i class="fa fa-user"></i> Detenidos</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                         <div class="row">
                                        <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <%--<label>Detenidos:</label>
                                            <br />--%>
                                            <table id="dt_basic_detenidos" class="table table-responsive table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No. remisión - Detenido</th>
                                                        <th data-hide="phone,tablet">Acciones</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </section>                                        
                                    </div>
                                    <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
    <input type="hidden" id="UENSRA" runat="server" value=""/>
         <input type="hidden" id="Max" value="0" />
        <input type="hidden" id="Min" value="0" />
        <input type="hidden" id="Validar1"  />
        <input type="hidden" id="Validar2"  />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">

        var detenidoActual = 0;

        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                e.preventDefault();

                if ($("#form-modal-agregar").is(":visible")) {
                    document.querySelector("#btnsaveNueva").click();
                }
                else if ($("#form-modal-tabla").is(":visible")) {
                    document.querySelector("#btnsaveTabla").click();
                }
                else if ($("#form-modal-donacion").is(":visible")) {
                    document.querySelector("#btnsaveDonacion").click();
                }
            }

            if (e.keyCode === 27) {
                e.preventDefault();

                if ($("#form-modal-agregar").is(":visible")) {
                    clearPertenenciaNueva();
                    $("#form-modal-agregar").modal("hide");
                }
                else if ($("#form-modal-tabla").is(":visible")) {
                    clearDonacion();
                    $("#form-modal-tabla").modal("hide");
                }
                else if ($("#form-modal-donacion").is(":visible")) {
                    clearDonacion();
                    $("#form-modal-donacion").modal("hide");
                } else if ($("#form-modal").is(":visible")) {
                    
                    $("#form-modal").modal("hide");
                }
            }
        });
        $(document).ready(function () {

            init();

            $(document).on('keydown', '.select2-selection', function (evt) {
                if (evt.which === 32 || evt.which === 13) {
                    $("#clasificacion").select2('close');
                    $("#casillero2").select2('close');
                    $("#internoAgregar").select2('close');
                }
            });

            function getYearsModal(val) {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getYears",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $("#filterYear");
                        Dropdown.append(new Option("", ""));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (val !== "") {
                            Dropdown.val(val).trigger('change.select2');
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar la lista de años. Si el problema persiste, consulta al personal de soporte técnico.");
                    }
                });
            }

            $("#filterYear").change(function () {
                $('#dt_basic').DataTable().ajax.reload();
            });

            getYearsModal(new Date().getFullYear());

            $("#usuarioQueRegistro").val($("#<%= UENSRA.ClientID %>").val());
            $("#usuarioQueRegistroDonacion").val($("#<%= UENSRA.ClientID %>").val());
            $("#usuarioQueRegistro").attr("disabled", "disabled");
            $("#usuarioQueRegistro").css("background", "#eee");
            $("#usuarioQueRegistroDonacion").attr("disabled", "disabled");
            $("#usuarioQueRegistroDonacion").css("background", "#eee");

            //Variables tabla modal
            var responsiveHelper_dt_pertenencias_nuevas = undefined;
            var responsiveHelper_datatable_fixed_column_pertenencias_nuevas = undefined;
            var responsiveHelper_datatable_col_reorder_pertenencias_nuevas = undefined;
            var responsiveHelper_datatable_tabletools_pertenencias_nuevas = undefined;

            $(document).on('keydown', 'input[pattern]', function(e){
              var input = $(this);
              var oldVal = input.val();
              var regex = new RegExp(input.attr('pattern'), 'g');

              setTimeout(function(){
                var newVal = input.val();
                if(!regex.test(newVal)){
                  input.val(oldVal); 
                }
              }, 0);
            });
            function loadTablePertenenciasComunes(interno) {
                $.ajax({                    
                    type: "POST",
                    url: "control_pertenencias.aspx/getPertenenciasComunes",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        interno: interno
                    }),
                    cache: false,
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        responsiveHelper_dt_pertenencias_nuevas = undefined;
                        $("#dt_nuevas_pertenencias").DataTable().destroy();
                        createTable(resultado.list);                        
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de pertenencias comunes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            $("body").on("click", ".ShowModal", function () {
                $("#Tabla_detenidos").modal("show");
            });

            $("#ctl00_contenido_fotografia").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar1").val(false);
                    }
                    else {
                        $("#Validar1").val(true);
                    }
                }
                else {
                    $("#Validar1").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar2").val(false);
                    }
                    else {
                        $("#Validar2").val(true);
                    }
                }
                else {
                    $("#Validar2").val(true);
                }



            });

            GetMinMax();

            function GetMinMax() {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/GetParametrosTamaño",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var max = resultado.TMax;
                            var min = resultado.Tmin;

                            $("#Max").val(max);
                            $("#Min").val(min);
                            // alert(resultado.rutaimagenfrontal);
                            //var imagenAvatar = ResolveUrl(resultado.rutaimagenfrontal);
                            //  imagenfrontalbiometricos = imagenAvatar;

                            //$('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                            //$("#ctl00_contenido_fileUpload1").val(imagenAvatar).clone(true);
                            $('#main').waitMe('hide');
                            //location.reload(true);
                            //CargarDatos(datos.TrackingId);



                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

            function createTable(data) {
                $('#dt_nuevas_pertenencias').dataTable({
                    "lengthMenu": [5, 10, 20, 50],
                    iDisplayLength: 7,
                    serverSide: false,
                    paging: true,
                    retrieve: true,
                    //"scrollY": "350px",
                    //"scrollCollapse": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_pertenencias_nuevas) {
                            responsiveHelper_dt_pertenencias_nuevas = new ResponsiveDatatablesHelper($('#dt_nuevas_pertenencias'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_pertenencias_nuevas.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_pertenencias_nuevas.respond();
                        $('#dt_nuevas_pertenencias').waitMe('hide');
                    },


                    //    "aoColumns": [
                    //{ sWidth: '6%' },
                    //{ sWidth: '18.5%' },
                    //{ sWidth: '12.5%' },
                    //{ sWidth: '12.5%' },
                    //{ sWidth: '12.5%' },
                    //{ sWidth: '12.5%' },
                    //{ sWidth: '12.5%' },
                    //{ sWidth: '12.5%' } ],

                    data: data,
                    columns: [
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        {
                            data: "Nombre",
                            visible: false
                        },
                        {
                            data: "Observacion",
                            visible: false
                        },
                        {
                            data: "Cantidad",
                            visible: false
                        },
                        {
                            data: "Casillero",
                            visible: false
                        },
                        {
                            data: "Clasificacion",
                            visible: false
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Id != null && row.TrackingId != null) {
                                    return "<input type='checkbox' data-tracking='" + row.TrackingId + "' data-id='" + row.Id + "' data-foto='" + row.Fotografia + "'  />";
                                }
                                else {
                                    return "<input type='checkbox' data-tracking='' data-id='' data-foto=''  />";
                                }
                            }
                        },
                        {
                            targets: 1,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                if (row.Fecha != null) {
                                    return "<input type='text' value='" + row.Fecha + "' class='alphanumeric alptext' style='width:100%' disabled='disabled' />";
                                }
                                else {
                                    return "<input type='text' value='' style='width:100%' disabled='disabled' />";
                                }
                            }
                        },
                        {
                            targets: 2,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Nombre != null) {
                                    return "<input type='text' value='" + row.Nombre + "'class'alphanumeric alptext' style='width:100%' data-required='true' />";
                                }
                                else {
                                    return "<input type='text' value='' class='alphanumeric alptext' style='width:100%' data-required='true' />";
                                }
                            }
                        },
                        {
                            targets: 3,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Observacion != null) {
                                    return "<textarea style='width:100%' maxlength='512' class'alphanumeric alptext' data-required='true'>" + row.Observacion + "</textarea > ";
                                }
                                else {
                                    return "<textarea style='width:100%' maxlength='512' class'alphanumeric alptext' data-required='true'></textarea > ";
                                }
                            }
                        },
                        //{
                        //    targets: 4,
                        //    orderable: false,

                        //    render: function (data, type, row, meta) {
                        //        if (row.Cantidad != null) {
                        //            return "<input type='text' name='bolsaTabla' maxlength='4' pattern='^[0-9]*$'  style='width:100%' value='" + row.Cantidad + "' data-required='true' /> ";
                        //        }
                        //        else {
                        //            return "<input type='text' name='bolsaTabla' maxlength='4' pattern='^[0-9]*$'  style='width:100%' value='' data-required='true' /> ";
                        //        }
                        //    }
                        //},
                        {
                            targets: 4,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                if (row.Bolsa != null) {
                                    return "<input type='text' name='cantidadTabla'   maxlength='4' pattern='^[0-9]*$' style='width:100%' value='" + row.Cantidad + "' data-required='true' /> ";
                                }
                                else {
                                    return "<input type='text' name='cantidadTabla'   maxlength='4' pattern='^[0-9]*$' style='width:100%' value='' data-required='true' /> ";
                                }
                            }
                        },
                        {
                            targets: 5,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                var datos = cargarClasificaciones();
                                var options = "";

                                options += "<option value='0'>[Clasificación]</option>";

                                for (var i = 0; i < datos.length; i++) {
                                    if (parseInt(datos[i].Id) === row.ClasificacionId) {
                                        options += "<option value='" + datos[i].Id + "' selected='selected' >" + datos[i].Desc + "</option>";
                                    }
                                    else {
                                        options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                    }
                                }
                                return "<select style='width:100%' data-required='true'>" + options + "</select>";
                            }
                        },
                        {
                            targets: 6,
                            orderable: false,
                            searchable: false,
                            render: function (data, type, row, meta) {
                                var datos = cargarCasilleros();
                                var options = "";

                                options += "<option value='0'>[Casillero]</option>";

                                for (var i = 0; i < datos.length; i++) {
                                    if (parseInt(datos[i].Id) === row.CasilleroId) {
                                        options += "<option value='" + datos[i].Id + "' selected='selected' >" + datos[i].Desc + "</option>";
                                    }
                                    else {
                                        if (row.CasilleroId != 0 && row.CasilleroId != undefined) {
                                            if (datos[i + 1] != undefined) {
                                                if (row.CasilleroId > datos[i].Id && row.CasilleroId < datos[i + 1].Id) {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                    options += "<option value='" + row.CasilleroId + "'selected='selected'>" + row.Casillero + "</option>";
                                                }
                                                else {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                }
                                            }
                                            else {
                                                if (row.CasilleroId > datos[i].Id) {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                    options += "<option value='" + row.CasilleroId + "'selected='selected'>" + row.Casillero + "</option>";
                                                }
                                                else {
                                                    options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                                }
                                            }
                                        }
                                        else {
                                            options += "<option value='" + datos[i].Id + "'>" + datos[i].Desc + "</option>";
                                        }
                                    }
                                }

                                return "<select style='width:100%' data-required='true'>" + options + "</select>";
                            }
                        },
                        {
                            targets: 7,
                            render: function (data, type, row, meta) {
                                if (row.Nombre == null) {
                                    return "";
                                }
                                else {
                                    return row.Nombre;
                                }
                            }
                        },
                        {
                            targets: 8,
                            render: function (data, type, row, meta) {
                                if (row.Observacion == null) {
                                    return "";
                                }
                                else {
                                    return row.Observacion;
                                }
                            }
                        },
                        {
                            targets: 9,
                            render: function (data, type, row, meta) {
                                if (row.Cantidad == null) {
                                    return "";
                                }
                                else {
                                    return row.Cantidad;
                                }
                            }
                        },
                        {
                            targets: 10,
                            render: function (data, type, row, meta) {
                                if (row.Casillero == null) {
                                    return "";
                                }
                                else {
                                    return row.Casillero;
                                }
                            }
                        },
                        {
                            targets: 11,
                            render: function (data, type, row, meta) {
                                if (row.Clasificacion == null) {
                                    return "";
                                }
                                else {
                                    return row.Clasificacion;
                                }
                            }
                        }
                    ],
                    "fnRowCallback": function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                        if (data.TrackingId != "" && data.TrackingId != null) {

                            $('td', nRow).css('background-color', '#DDF4C2');
                        }
                        else {
                            $('td', nRow).css('background-color', '#FFFFFF');
                        }
                    }
                });                                

                $("#dt_nuevas_pertenencias").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                ]).draw(false);
            }            

            $("#addRow").click(function () {
                $("#dt_nuevas_pertenencias").DataTable().row.add([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                ]).draw(false);
            });

            var dataClasificaciones = null;
            var dataCasilleros = null;
            var datosClasificaciones = [];
            var datosCasilleros = [];
            

            function cargarCasilleros() {
                if (dataCasilleros === null && datosCasilleros.length === 0) {
                    
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "control_pertenencias.aspx/getCasilleros",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",                    
                        cache: false,
                        success: function (response) {                            
                            dataCasilleros += "<option></option>"
                            $.each(response.d, function (index, item) {    
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;
                                datosCasilleros.push(tt);
                                dataCasilleros += "<option value=" + item.Id + ">" + item.Desc + "</option>";                                
                            });
                        },
                        error: function () {
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");
                        }
                    });
                }

                return datosCasilleros;
            }
            
            function cargarClasificaciones() { 
                
                if (dataClasificaciones === null && datosClasificaciones.length === 0) {                    
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "control_pertenencias.aspx/getClasificacion",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",                    
                        cache: false,
                        success: function (response) {
                            dataClasificaciones += "<option></option>";
                            
                            $.each(response.d, function (index, item) {
                                var tt = {};
                                tt.Id = item.Id;
                                tt.Desc = item.Desc;

                                datosClasificaciones.push(tt);
                                dataClasificaciones += "<option value=" + item.Id + ">" + item.Desc + "</option>";                                
                            });
                        },
                        error: function () {
                            
                            ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificaciones. Si el problema persiste contacte al soporte técnico del sistema.");
                            
                        }
                    });
                }
                return datosClasificaciones;
            }

            function validarCamposEnTabla() {
                var isValid = true;

                if ($("#internoAgregar").val() == "0") {
                    ShowError("Detenido", "El detenido es obligatorio");
                    $('#internoAgregar').parent().removeClass('state-success').addClass("state-error");
                    $('#internoAgregar').removeClass('valid');
                    isValid = false;
                }
                else {
                    $('#internoAgregar').parent().removeClass("state-error").addClass('state-success');
                    $('#internoAgregar').addClass('valid');
                }
         

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    
                    var checkit = this.node().childNodes[0].childNodes[0].checked;

                    if (checkit) {
                        var nombre = this.node().childNodes[2].childNodes[0].getAttribute("data-required");                        
                        var observacion = this.node().childNodes[3].childNodes[0].getAttribute("data-required");
                        //var bolsa = this.node().childNodes[4].childNodes[0].getAttribute("data-required");                        
                        var cantidad = this.node().childNodes[4].childNodes[0].getAttribute("data-required");                        
                        var clasificacion = this.node().childNodes[5].childNodes[0].getAttribute("data-required");                        
                        var casillero = this.node().childNodes[6].childNodes[0].getAttribute("data-required");    

                        if (nombre === "true") {                            
                            if (this.node().childNodes[2].childNodes[0].value === "" ||
                                this.node().childNodes[2].childNodes[0].value === undefined ||
                                this.node().childNodes[2].childNodes[0].value === null) {                                
                                this.node().childNodes[2].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Pertenencia", "El campo pertenencia es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[2].childNodes[0].removeAttribute('class');
                            }
                        }                        

                        if (observacion === "true") {                            
                            if (this.node().childNodes[3].childNodes[0].value === "" ||
                                this.node().childNodes[3].childNodes[0].value === undefined ||
                                this.node().childNodes[3].childNodes[0].value === null) {                                
                                this.node().childNodes[3].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Observación", "El campo observación es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[3].childNodes[0].removeAttribute('class');
                            }
                        }

                        //if (bolsa === "true") {
                        //    var val = parseInt(this.node().childNodes[4].childNodes[0].value);                                                        

                        //    if (isNaN(val)) {
                        //        this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                        //        ShowError("Bolsa", "El campo bolsa tiene un valor incorrecto.");
                        //        isValid = false;                                
                        //    }                            

                        //    if (val <= 0) {
                        //        this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                        //        ShowError("Bolsa", "El campo bolsa debe tener un valor mayor a cero.");
                        //        isValid = false;
                        //    }


                        //    if (!isNaN(val) && val > 0) {
                        //        this.node().childNodes[4].childNodes[0].removeAttribute('class');
                        //    }
                        //}

                        if (cantidad === "true") {                            
                            var val = parseInt(this.node().childNodes[4].childNodes[0].value);                                                        

                            if (isNaN(val)) {
                                this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Cantidad", "El campo cantidad es obligatorio.");
                                isValid = false;                                
                            }                            

                            if (val <= 0) {
                                this.node().childNodes[4].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Cantidad", "El campo cantidad debe tener un valor mayor a cero.");
                                isValid = false;
                            }


                            if (!isNaN(val) && val > 0) {
                                this.node().childNodes[4].childNodes[0].removeAttribute('class');
                            }
                        }                        

                        if (clasificacion === "true") {                            
                            if (this.node().childNodes[5].childNodes[0].value === "0") {                                
                                this.node().childNodes[5].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Clasificación", "El campo clasificación es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[5].childNodes[0].removeAttribute('class');
                            }
                        }              

                        if (casillero === "true") {                            
                            if (this.node().childNodes[6].childNodes[0].value === "0") {                                
                                this.node().childNodes[6].childNodes[0].setAttribute('class', 'errorInputTabla');
                                ShowError("Casillero", "El campo casillero es obligatorio.");
                                isValid = false;
                            }
                            else {
                                this.node().childNodes[6].childNodes[0].removeAttribute('class');
                            }
                        }  
                        var file = document.getElementById('<% = fotografia.ClientID %>').value;
                if (file != null && file != '') {

                    if (!validaImagen(file)) {
                        ShowError("Fotografía", "Solo se permiten extensiones .jpg, .jpeg o .png");
                        isValid = false;

                    }
                    else {
                        var validar = $("#Validar1").val();
                        var validar2 = $("#Validar2").val();


                        if ($("#Max").val() != "0") {
                            if (validar == 'false') {
                                ShowError("Fotografía", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                isValid = false;
                            }

                        }
                        if ($("#Min").val() != "0") {
                            var s = $("#Min").val() * 1024;
                            if (validar2 == 'false') {
                                ShowError("Fotografía", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                isValid = false;
                            }
                        }
                    }
                }


                    }                        
                });
                
                return isValid;
            }

            $("#internoAgregar").change(function () {
                loadTablePertenenciasComunes($("#internoAgregar").val());
            });

            document.getElementById("internoAgregar").onchange = function() {numInt()};
            function numInt() {
                var x = $("#internoAgregar").val()
                if (x != 0)
                    getRemision(x);
            }
            function getRemision(datos) {
          
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getNumInterno",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        internoId: datos
                    }),                    
                    cache: false,
                    success: function (data) {                        
                  
                        $("#numInterno").val(data.d);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar las pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            $("#spanUno").click(function () {
                if ($("#egreso").is(":checked")) {
                    $("#egreso").prop("checked", false);
                    if (!$("#ingresoReingresoConPertenencias").is(':checked') && !$("#ingresoReingresoConPertenencias").is(':checked')) {
                        $("#ingresoReingresoConPertenencias").prop("checked", true);
                        $("#ingresoReingresoSinPertenencias").prop("checked", false);
                    }
                } else {
                    $("#egreso").prop("checked", true);
                    $("#ingresoReingresoConPertenencias").prop("checked", false);
                    $("#ingresoReingresoSinPertenencias").prop("checked", false);
                }

                $("#egreso").trigger("change");
            });

            $("#spanDos").click(function () {
                if ($("#ingresoReingresoConPertenencias").is(":checked")) {
                    $("#ingresoReingresoConPertenencias").prop("checked", false);
                    if (!$("#egreso").is(':checked') && !$("#ingresoReingresoSinPertenencias").is(':checked')) {
                        $("#ingresoReingresoConPertenencias").prop("checked", true);
                        $("#ingresoReingresoSinPertenencias").prop("checked", false);
                        $("#egreso").prop("checked", false);
                    }
                } else {
                    $("#ingresoReingresoConPertenencias").prop("checked", true);
                    $("#egreso").prop("checked", false);
                    $("#ingresoReingresoSinPertenencias").prop("checked", false);
                }

                $("#ingresoReingresoConPertenencias").trigger("change");
            });

            $("#spanTres").click(function () {
                if ($("#ingresoReingresoSinPertenencias").is(":checked")) {
                    $("#ingresoReingresoSinPertenencias").prop("checked", false);
                    if (!$("#egreso").is(':checked') && !$("#ingresoReingresoConPertenencias").is(':checked')) {
                        $("#ingresoReingresoConPertenencias").prop("checked", true);
                        $("#ingresoReingresoSinPertenencias").prop("checked", false);
                        $("#egreso").prop("checked", false);
                    }
                } else {
                    $("#ingresoReingresoSinPertenencias").prop("checked", true);
                    $("#ingresoReingresoConPertenencias").prop("checked", false);
                    $("#egreso").prop("checked", false);
                }

                $("#ingresoReingresoSinPertenencias").trigger("change");
            });

            $("#bolsaNueva").on("keydown", function (e) {
                if (!((e.keyCode > 95 && e.keyCode < 106)
                    || (e.keyCode > 47 && e.keyCode < 58)
                    || e.keyCode == 8)) {
                    return false;
                }
            });

            $("#cantidadNueva").on("keydown", function (e) {
                if (!((e.keyCode > 95 && e.keyCode < 106)
                    || (e.keyCode > 47 && e.keyCode < 58)
                    || e.keyCode == 8)) {
                    return false;
                }
            });

            $("#internoSelect").change(function () {
                //window.emptytable = false;
                $("#ctl00_contenido_lblMessage").html("");
                //responsiveHelper_dt_basic = undefined
                //$("#dt_basic").DataTable().destroy();
                //obtenerInfo();
            });

            $("#ingresoReingresoSinPertenencias").change(function () {                                
                $("#ctl00_contenido_lblMessage").html("");
                $("#dt_basic").DataTable().ajax.reload();                
                if ($("#ingresoReingresoSinPertenencias").is(':checked')) {
                    $("#ingresoReingresoSinPertenencias").prop("checked", true);
                    $("#ingresoReingresoConPertenencias").prop("checked", false);
                    $("#egreso").prop("checked", false);
                }
                else {
                    if (!$("#ingresoReingresoConPertenencias").is(':checked') && !$("#egreso").is(':checked')) {
                        $("#ingresoReingresoConPertenencias").prop("checked", true);
                    }
                }
            });

            $("#egreso").change(function () {
                $("#ctl00_contenido_lblMessage").html("");
                $("#dt_basic").DataTable().ajax.reload();
                if ($("#egreso").is(':checked')) {
                    $("#ingresoReingresoSinPertenencias").prop("checked", false);
                    $("#ingresoReingresoConPertenencias").prop("checked", false);
                    $("#egreso").prop("checked", true);
                }
                else {
                    if (!$("#ingresoReingresoConPertenencias").is(':checked') && !$("#ingresoReingresoSinPertenencias").is(':checked')) {
                        $("#ingresoReingresoConPertenencias").prop("checked", true);
                    }
                }
            });

            $("#ingresoReingresoConPertenencias").change(function () {
                $("#ctl00_contenido_lblMessage").html("");
                $("#dt_basic").DataTable().ajax.reload();
                if ($("#ingresoReingresoConPertenencias").is(':checked')) {
                    $("#ingresoReingresoSinPertenencias").prop("checked", false);
                    $("#ingresoReingresoConPertenencias").prop("checked", true);
                    $("#egreso").prop("checked", false);
                }
                else {
                    if (!$("#ingresoReingresoSinPertenencias").is(':checked') && !$("#egreso").is(':checked')) {
                        $("#ingresoReingresoConPertenencias").prop("checked", true);
                    }
                }
            });

            $("#dt_basic").click(function () {

                var isChecked = false;

                $("#dt_basic tbody tr").each(function (index) {
                    $(this).children("td").each(function (index2) {
                        switch (index2) {
                            case 0: var campo = $(this).children('input');
                                if (campo.is(':checked')) {
                                    isChecked = true;
                                }
                                break;
                        }
                    });
                });

                if (isChecked) {

                    //Arreglo para Id's de detenido
                    var data = [];
                    var dataRN = [];

                    //Arreglo para validar si hay checkboxes marcados, pero que no son validos
                    var dataIdInvalidos = [];

                    //Arreglo para Id's de pertenencias
                    var dataIdPertenencias = [];

                    $("#dt_basic tbody tr").each(function (index) {
                        $(this).children("td").each(function (index2) {
                            switch (index2) {
                                case 0: var campo = $(this).children('input');
                                    if (campo.is(':checked')) {                                        
                                        if (campo.val() != "on" && campo.val() != "null") {
                                            dataIdPertenencias.push(campo.val());
                                            data.push(campo.attr('data-internoid'));
                                            console.log('campo', campo.attr('data-esvalido'));
                                            if (campo.attr('data-esvalido') == "0")
                                            {
                                                dataIdInvalidos.push(campo.val());
                                                //$("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención!</strong>" +
                                                //" " +campo.attr('data-mensajern')+ "</div>");
                                                //ShowAlert("Atención!",+campo.attr('data-esValido'));
                                                $("#donar").removeClass("btn btn-md btn-success add");
                                                $("#donar").addClass("btn btn-md btn-success add disabled");
                                                $("#devolucion").removeClass("btn btn-md btn-primary add");
                                                $("#devolucion").addClass("btn btn-md btn-primary add disabled");
                                                return;
                                            }
                                        }
                                        else {
                                            dataIdInvalidos.push(campo.val());

                                             if (campo.attr('data-esvalido') == "0")
                                            {
                                                //$("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención!</strong>" +
                                                //" " +campo.attr('data-mensajern')+ "</div>");
                                                //ShowAlert("Atención!",campo.attr('data-mensajern'));
                                                $("#donar").removeClass("btn btn-md btn-success add");
                                                $("#donar").addClass("btn btn-md btn-success add disabled");
                                                $("#devolucion").removeClass("btn btn-md btn-primary add");
                                                $("#devolucion").addClass("btn btn-md btn-primary add disabled");
                                                return;
                                            }
                                            //  if (campo.attr('data-calificacionid') != 0)
                                            //{
                                            //    dataCalificados.push(campo.attr('data-calificacionid'));
                                            //}
                                        }
                                    }
                                    break;
                            }
                        });
                    });

                    //$("input[type=checkbox]:checked").each(function () {

                    //    if ($(this).val() != "on" && $(this).val() != "null") {
                    //        dataIdPertenencias.push($(this).val());
                    //        data.push($(this).attr('data-internoid'));
                    //    }
                    //    else {
                    //        dataIdInvalidos.push($(this).val());
                    //    }

                    //});                                                        
                    //if (dataCalificados.length == 0) {
                    //    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención!</strong>" +
                    //        " Ha seleccionado pertenencia de detenidos que no tienen calificación, por favor seleccione pertenencias con detenidos calificados.</div>");
                    //    ShowAlert("Atención!", " Ha seleccionado detenidos que no tienen calificación, por favor seleccione pertenencias con detenidos calificados, por favor seleccione pertenencias válidas.");
                    //    $("#donar").removeClass("btn btn-md btn-success add");
                    //    $("#donar").addClass("btn btn-md btn-success add disabled");
                    //    $("#devolucion").removeClass("btn btn-md btn-primary add");
                    //    $("#devolucion").addClass("btn btn-md btn-primary add disabled");
                    //    return;
                    //}
                    
                    if (dataIdInvalidos.length > 0) {
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención!</strong>" +
                            "Ha seleccionado detenidos que no tienen pertenencias ó evidencias que han sido entregadas ó donadas, por favor seleccione pertenencias válidas.</div>");
                        setTimeout(hideMessage, hideTime);
                        ShowAlert("¡Atención!", "Ha seleccionado detenidos que no tienen pertenencias ó evidencias que han sido entregadas ó donadas, por favor seleccione pertenencias válidas.");
                        $("#donar").removeClass("btn btn-md btn-success add");
                        $("#donar").addClass("btn btn-md btn-success add disabled");
                        $("#devolucion").removeClass("btn btn-md btn-primary add");
                        $("#devolucion").addClass("btn btn-md btn-primary add disabled");
                        return;
                    }

                    if (dataIdPertenencias.length == 0 && data.length == 0) {
                        $("#donar").removeClass("btn btn-md btn-success add");
                        $("#donar").addClass("btn btn-md btn-success add disabled");
                        $("#devolucion").removeClass("btn btn-md btn-primary add");
                        $("#devolucion").addClass("btn btn-md btn-primary add disabled");
                        return;
                    }

                    $("#donar").removeClass("btn btn-md btn-success add disabled");
                    $("#donar").addClass("btn btn-md btn-success add");
                    $("#devolucion").removeClass("btn btn-md btn-primary add disabled");
                    $("#devolucion").addClass("btn btn-md btn-primary add");

                } else {
                    $("#donar").removeClass("btn btn-md btn-success add");
                    $("#donar").addClass("btn btn-md btn-success add disabled");
                    $("#devolucion").removeClass("btn btn-md btn-primary add");
                    $("#devolucion").addClass("btn btn-md btn-primary add disabled");
                }
            });
            ///
            $("#form-modal-agregar").on('hidden.bs.modal', function () {
                clearPertenenciaNueva();
            });

            $("#form-modal-donacion").on('hidden.bs.modal', function () {
                clearDonacion();
            });

            $("#form-modal-tabla").on('hidden.bs.modal', function () {
                clearDevolucion();
            });

            $("#btncancelTabla").click(function () {
                clearDevolucion();
            });

            $("#btncancelDonacion").click(function () {
                clearDonacion();
            })

            $("#btncancelNueva").click(function () {
                clearPertenenciaNueva();
            });

            function loadCasilleros(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getCasilleros",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#casillero');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Casillero]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadCasilleros2(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getCasillerosAll",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#casillero2');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function guardarImagen() {

                var files = $("#ctl00_contenido_fotografia").get(0).files;
                var nombreAvatarAnterior = "";
                var nombreAvatar = "";

                if (files.length > 0) {

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "../Handlers/FileUploadHandler.ashx?action=4&before=" + nombreAvatarAnterior,
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (Results) {
                            if (Results.exitoso) {
                                nombreAvatar = Results.nombreArchivo;

                                //data = [
                                //    InternoId = $("#internoAgregar").val(),
                                //    Pertenencia = $("#pertenenciaNueva").val(),
                                //    Observacion = $("#observacionNueva").val(),
                                //    Clasificacion = $("#clasificacionPertenencia").val(),
                                //    Bolsa = $("#bolsaNueva").val(),
                                //    Cantidad = $("#cantidadNueva").val(),
                                //    Fotografia = nombreAvatar,
                                //    Casillero = $("#casillero").val(),
                                //];

                                var datos = obtenerDatosTabla(nombreAvatar);

                                savePertenencia(datos, $("#internoAgregar").val());
                            }
                            else {
                                ShowError("Error!", "No fue posible guardar la imagen de la pertenencia. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                            }
                        },
                        error: function (err) {
                            ShowError("Error!", "No fue posible guardar la imagen de la pertenencia. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    });
                }
                else {
                    //data = [
                    //    InternoId = $("#internoAgregar").val(),
                    //    Pertenencia = $("#pertenenciaNueva").val(),
                    //    Observacion = $("#observacionNueva").val(),
                    //    Clasificacion = $("#clasificacionPertenencia").val(),
                    //    Bolsa = $("#bolsaNueva").val(),
                    //    Cantidad = $("#cantidadNueva").val(),
                    //    Fotografia = nombreAvatar,
                    //    Casillero = $("#casillero").val(),
                    //];

                    var datos = obtenerDatosTabla(nombreAvatar);

                    savePertenencia(datos, $("#internoAgregar").val());
                }
            }

            $("#btnsaveNueva").click(function () {                
                var hasSelected = false;

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    
                    var checkit = this.node().childNodes[0].childNodes[0].checked;
                    
                    if (checkit) {
                        hasSelected = true;
                    }                        
                });     

                if (hasSelected) {
                    if (validarCamposEnTabla()) {
                        guardarImagen();                
                    }              
                }
                else {
                    ShowAlert("¡Atención!", " No ha seleccionado alguna pertenencia para registrar o actualizar.");
                }
            });

            function cadena(cadena1) {
                cadena1 = (cadena1 + '').replace(/["'´`]/g, '');
                cadena1 = (cadena1 + '').replace(/<script>/g, '');
                cadena1 = (cadena1 + '').replace(/hacked/g, '');
                cadena1 = (cadena1 + '').replace(/["'´`]/g, '');
                return cadena1;
            }


            function obtenerDatosTabla(imagen) {
                
            
                var dataArreglo = new Array();
                var pertenencia;

                $("#dt_nuevas_pertenencias").DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                    
                    var checkit = this.node().childNodes[0].childNodes[0].checked;

                    if (checkit) {
                       
                       
                        pertenencia = {};
                        pertenencia.Fecha = this.node().childNodes[1].childNodes[0].value;
                        pertenencia.Pertenencia = cadena(this.node().childNodes[2].childNodes[0].value);
                        pertenencia.Observacion =cadena( this.node().childNodes[3].childNodes[0].value);
                        //pertenencia.Bolsa = this.node().childNodes[4].childNodes[0].value;
                        pertenencia.Cantidad = this.node().childNodes[4].childNodes[0].value;
                        pertenencia.Clasificacion = this.node().childNodes[5].childNodes[0].value;
                        pertenencia.Casillero = this.node().childNodes[6].childNodes[0].value;
                        pertenencia.TrackingId = this.node().childNodes[0].childNodes[0].getAttribute("data-tracking");
                        pertenencia.Id = this.node().childNodes[0].childNodes[0].getAttribute("data-id");

                        if (imagen != null && imagen != undefined && imagen != "") {
                            pertenencia.Fotografia = imagen;                            
                        }
                        else {
                            pertenencia.Fotografia = this.node().childNodes[0].childNodes[0].getAttribute("data-foto");                            
                        }

                        dataArreglo.push(pertenencia);                        
                    }                        
                });                     

                return dataArreglo;
            }

            function clearPertenenciaNueva() {
                $("#internoAgregar").select2({ containerCss: { "border-color": "#bdbdbd" } });
                $("#clasificacionPertenencia").select2({ containerCss: { "border-color": "#bdbdbd" } });
                $('#pertenenciaNueva').parent().removeClass('state-success');
                $('#pertenenciaNueva').parent().removeClass("state-error");
                $('#observacionNueva').parent().removeClass('state-success');
                $('#observacionNueva').parent().removeClass("state-error");
                $('#bolsaNueva').parent().removeClass('state-success');
                $('#bolsaNueva').parent().removeClass("state-error");
                $('#cantidadNueva').parent().removeClass('state-success');
                $('#cantidadNueva').parent().removeClass("state-error");
                $('#casillero').parent().removeClass('state-success');
                $('#casillero').parent().removeClass("state-error");
            }

            function validarPertenenciaNueva() {
                var isValid = true;

                if ($('#internoAgregar').val() == "0" || $('#internoAgregar').val() == null) {
                    ShowError("Interno", "El interno es obligatorio.");
                    $("#internoAgregar").select2({ containerCss: { "border-color": "#a90329" } });
                    isValid = false;
                }
                else {
                    $("#internoAgregar").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#clasificacionPertenencia').val() == "0" || $('#clasificacionPertenencia').val() == null) {
                    ShowError("Clasificación", "La clasificación es obligatoria.");
                    $("#clasificacionPertenencia").select2({ containerCss: { "border-color": "#a90329" } });
                    isValid = false;
                }
                else {
                    $("#clasificacionPertenencia").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($("#pertenenciaNueva").val().split(" ").join("") == "") {
                    ShowError("Pertenencia", "La pertenencia es obligatoria.");
                    $('#pertenenciaNueva').parent().removeClass('state-success').addClass("state-error");
                    $('#pertenenciaNueva').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#pertenenciaNueva').parent().removeClass("state-error").addClass('state-success');
                    $('#pertenenciaNueva').addClass('valid');
                }

                if ($("#observacionNueva").val().split(" ").join("") == "") {
                    ShowError("Observación", "La observación es obligatoria.");
                    $('#observacionNueva').parent().removeClass('state-success').addClass("state-error");
                    $('#observacionNueva').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observacionNueva').parent().removeClass("state-error").addClass('state-success');
                    $('#observacionNueva').addClass('valid');
                }

                if ($("#bolsaNueva").val().split(" ").join("") == "") {
                    ShowError("Bolsa", "La bolsa es obligatoria.");
                    $('#bolsaNueva').parent().removeClass('state-success').addClass("state-error");
                    $('#bolsaNueva').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#bolsaNueva').parent().removeClass("state-error").addClass('state-success');
                    $('#bolsaNueva').addClass('valid');
                }

                if ($("#cantidadNueva").val().split(" ").join("") == "") {
                    ShowError("Cantidad", "La cantidad es obligatoria.");
                    $('#cantidadNueva').parent().removeClass('state-success').addClass("state-error");
                    $('#cantidadNueva').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#cantidadNueva').parent().removeClass("state-error").addClass('state-success');
                    $('#cantidadNueva').addClass('valid');
                }

                if ($('#casillero').val() == "0" || $('#casillero').val() == null) {
                    ShowError("casillero", "El casillero es obligatorio.");
                    $("#clasificacionPertenencia").select2({ containerCss: { "border-color": "#a90329" } });
                    isValid = false;
                }
                else {
                    $("#casillero").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                return isValid;
            }

            function clearModalNuevaPertenencia() {
                //$(".input").removeClass('state-success');
                //$(".input").removeClass('state-error');
                //$(".input").removeClass('valid');
                //$("#pertenenciaNueva").val("");
                //$("#observacionNueva").val("");
                //$("#bolsaNueva").val("");
                //$("#cantidadNueva").val("");
                $("#ctl00_contenido_fotografia").val("");
            }
            function validaImagen(file) {
                var extArray = new Array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG");

                var ext = file.slice(file.indexOf(".")).toLowerCase();
                for (var i = 0; i < extArray.length; i++) {
                    if (extArray[i] == ext) {
                        return true;
                    }

                }
                return false;
            }
            function savePertenencia(datos, interno) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/savePertenencia",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'pertenencias': datos, interno: interno
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {                                                        
                            $("#form-modal-agregar").modal("hide");
                            clearModalNuevaPertenencia();
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "La información se " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "La información se " + resultado.mensaje + " correctamente.");

                            var ruta = resolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                            $("#dt_basic").DataTable().ajax.reload();
                            location.reload(true);
                        } else {
                            ShowError("Error! Algo salió mal", resultado.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $("#agregarPertenencia").click(function () {

                if (detenidoActual == 0) {
                    ShowAlert("¡Atencion!", "Es necesario seleccionar un detenido antes de agregar una pertenencia.");
                    return;
                }
                
                if (detenidoActual !== 0) {
                    loadTablePertenenciasComunes(detenidoActual);
                    loadInternosAgregar(detenidoActual);
                }
                
                $("#numInterno").val("");
                //loadCasilleros("0");
                $('#internoAgregar').select2({ dropdownParent: $('#form-modal-agregar') });
                $('#internoAgregar').attr("disabled", "disabled");
                $('#internoAgregar').select2({ containerCss: { "background-color": "#D3D3D3", "cursor": "not-allowed" } });
                //loadClasificacion("0");
                $("#form-modal-title-agregar").empty();
                $("#form-modal-title-agregar").html('<i class="fa fa-pencil" +=""></i>Agregar');
                $("#form-modal-agregar").modal("show");
            });

            function loadClasificacion(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getClasificacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#clasificacionPertenencia');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Clasificación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la clasificación de las pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadInternosAgregar(setvalue) {

                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getInternosAgregar",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        id: setvalue
                    }),
                    success: function (response) {
                        var Dropdown = $('#internoAgregar');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Detenido]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });
                        console.log('da', setvalue);
                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de detenidos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            //table = $('#dt_basic_tabla').DataTable();            
            var responsiveHelper_dt_basic_tabla = undefined;
            var responsiveHelper_datatable_fixed_column_tabla = undefined;
            var responsiveHelper_datatable_col_reorder_tabla = undefined;
            var responsiveHelper_datatable_tabletools_tabla = undefined;

            var responsiveHelper_dt_basic_tabla_donacion = undefined;
            var responsiveHelper_datatable_fixed_column_tabla_donacion = undefined;
            var responsiveHelper_datatable_col_reorder_tabla_donacion = undefined;
            var responsiveHelper_datatable_tabletools_tabla_donacion = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            $("#btnsaveDonacion").click(function () {

                var data = new Array();
                var isChecked = false;
                var pertenencia;
                var isChecked = false;
                var IdAux;
                var NombreAux;
                var CantidadAux;
                var EstatusAux;
                var ObservacionAux;
                var ClasificacionAux;
                var BolsaAux;

                $("#dt_basic_tabla_donacion tbody tr").each(function (index) {
                    $(this).children("td").each(function (index2) {
                        switch (index2) {
                            //Aqui en el subindice 0 accedo al checkbox para saber si la pertenencia esta seleccionada para la devolucion
                            case 0: var campo = $(this).children('input');
                                if (campo.is(':checked')) {
                                    isChecked = true;
                                }
                                break;
                            case 1: if (isChecked) {
                                IdAux = $(this).text();
                            }
                                break;
                            case 2: if (isChecked) {
                                NombreAux = $(this).text();
                            }
                                break;
                            case 3: if (isChecked) {
                                CantidadAux = $(this).text();
                            }
                                break;
                            case 4: if (isChecked) {
                                EstatusAux = $(this).text();
                            }
                                break;
                            case 5: if (isChecked) {
                                ObservacionAux = $(this).text();
                            }
                                break;
                            case 6: if (isChecked) {
                                ClasificacionAux = $(this).text();
                            }
                                break;
                            case 7: if (isChecked) {
                                BolsaAux = $(this).text();
                            }
                                break;
                        }
                    });

                    pertenencia = {};

                    pertenencia.Id = IdAux;
                    pertenencia.Nombre = NombreAux;
                    pertenencia.Cantidad = CantidadAux;
                    pertenencia.Estatus = EstatusAux;
                    pertenencia.Observacion = ObservacionAux;
                    pertenencia.Clasificacion = ClasificacionAux;
                    pertenencia.Bolsa = BolsaAux;

                    if (isChecked) {
                        data.push(pertenencia);
                    }

                    isChecked = false;
                });

                dataGenerales = [
                    pertenenciaDe = $("#pertenenciaDeDonacion").val(),
                    usuarioQueRegistro = $("#usuarioQueRegistroDonacion").val(),
                    personaQueRecibe = $("#personaQueRecibeDonacion").val(),
                    condicion = $("#condicionDeEntregaDonacion").val(),
                    entrega = $("#entregaDonacion").val(),
                    institucion = $("#institucion").val(),
                    donar = $("#donarId").is(":checked"),
                    consignar = $("#consignarId").is(":checked")
                ];

                if (validarDonacion()) {
                    saveDonacion(data, dataGenerales);
                }
            });

            function clearDonacion() {
                $('#pertenenciaDeDonacion').parent().removeClass('state-success');
                $('#pertenenciaDeDonacion').parent().removeClass("state-error");
                $('#usuarioQueRegistroDonacion').parent().removeClass('state-success');
                $('#usuarioQueRegistroDonacion').parent().removeClass("state-error");
                $("#institucion").select2({ containerCss: { "border-color": "#bdbdbd" } });
                $('#personaQueRecibeDonacion').parent().removeClass('state-success');
                $('#personaQueRecibeDonacion').parent().removeClass("state-error");
                $('#condicionDeEntregaDonacion').parent().removeClass('state-success');
                $('#condicionDeEntregaDonacion').parent().removeClass("state-error");
                $("#entregaDonacion").select2({ containerCss: { "border-color": "#bdbdbd" } });
                $("#personaQueRecibeDonacion").val("");
                $("#condicionDeEntregaDonacion").val("");
            }

            function validarDonacion() {
                var isValid = true;

                //if ($('#pertenenciaDeDonacion').val().split(" ").join("") == "") {
                //    ShowError("Pertenencia", "La pertenencia es obligatoria.");
                //    $('#pertenenciaDeDonacion').parent().removeClass('state-success').addClass("state-error");
                //    $('#pertenenciaDeDonacion').removeClass('valid');
                //    isValid = false;
                //}
                //else {
                //    $('#pertenenciaDeDonacion').parent().removeClass("state-error").addClass('state-success');
                //    $('#pertenenciaDeDonacion').addClass('valid');
                //}

                //if ($('#usuarioQueRegistroDonacion').val().split(" ").join("") == "") {
                //    ShowError("Usuario que registro", "El usuario que registro es obligatorio.");
                //    $('#usuarioQueRegistroDonacion').parent().removeClass('state-success').addClass("state-error");
                //    $('#usuarioQueRegistroDonacion').removeClass('valid');
                //    isValid = false;
                //}
                //else {
                //    $('#usuarioQueRegistroDonacion').parent().removeClass("state-error").addClass('state-success');
                //    $('#usuarioQueRegistroDonacion').addClass('valid');
                //}

                if ($('#institucion').val() == "0" || $('#institucion').val() == null) {
                    ShowError("Institución", "La institución es obligatoria.");
                    $("#institucion").select2({ containerCss: { "border-color": "#a90329" } });
                    isValid = false;
                }
                else {
                    $("#institucion").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                if ($('#personaQueRecibeDonacion').val().split(" ").join("") == "") {
                    ShowError("Persona que recibe", "La persona que recibe es obligatoria.");
                    $('#personaQueRecibeDonacion').parent().removeClass('state-success').addClass("state-error");
                    $('#personaQueRecibeDonacion').removeClass('valid');
                    isValid = false;
                }
                else {
                    $('#personaQueRecibeDonacion').parent().removeClass("state-error").addClass('state-success');
                    $('#personaQueRecibeDonacion').addClass('valid');
                }

                if ($('#condicionDeEntregaDonacion').val().split(" ").join("") == "") {
                    ShowError("Condición de entrega", "La condición de entrega es obligatoria.");
                    $('#condicionDeEntregaDonacion').parent().removeClass('state-success').addClass("state-error");
                    $('#condicionDeEntregaDonacion').removeClass('valid');
                    isValid = false;
                }
                else {
                    $('#condicionDeEntregaDonacion').parent().removeClass("state-error").addClass('state-success');
                    $('#condicionDeEntregaDonacion').addClass('valid');
                }

                if ($('#entregaDonacion').val() == "0" || $('#entregaDonacion').val() == null) {
                    ShowError("Entrega", "La entrega es obligatoria.");
                    $("#entregaDonacion").select2({ containerCss: { "border-color": "#a90329" } });
                    isValid = false;
                }
                else {
                    $("#entregaDonacion").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                return isValid;
            }

            function saveDonacion(data, dataGenerales) {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/saveDonacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ data, dataGenerales }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            clearDonacion();
                            $("#form-modal-donacion").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'></button><i class='fa-fw fa fa-times'></i><strong>Bien!</strong>" +
                                "Donación generada exitosamente.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien!", "Donación generada exitosamente.");
                            var ruta = resolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                            obtenerInfo();
                        }
                        else {
                            if (resultado.errores.length > 0) {
                                ShowAlert("¡Atención!", resultado.errores[0]);
                            }
                            else {
                                ShowError("¡Error!", "Ocurró un error, " + resultado.message + ".");
                            }    
                        }
                    }
                });
            }

            $("#btnsaveTabla").click(function () {
                event.preventDefault();

                var data = new Array();
                var pertenencia;
                var isChecked = false;
                var IdAux;
                var NombreAux;
                var CantidadAux;
                var EstatusAux;
                var ObservacionAux;
                var ClasificacionAux;
                var BolsaAux;

                $("#dt_basic_tabla tbody tr").each(function (index) {
                    $(this).children("td").each(function (index2) {
                        switch (index2) {
                            //Aqui en el subindice 0 accedo al checkbox para saber si la pertenencia esta seleccionada para la devolucion
                            case 0: var campo = $(this).children('input');
                                if (campo.is(':checked')) {
                                    isChecked = true;
                                }
                                break;
                            case 1: if (isChecked) {
                                IdAux = $(this).text();
                            }
                                break;
                            case 2: if (isChecked) {
                                NombreAux = $(this).text();
                            }
                                break;
                            case 3: if (isChecked) {
                                CantidadAux = $(this).text();
                            }
                                break;
                            case 4: if (isChecked) {
                                EstatusAux = $(this).text();
                            }
                                break;
                            case 5: if (isChecked) {
                                ObservacionAux = $(this).text();
                            }
                                break;
                            case 6: if (isChecked) {
                                ClasificacionAux = $(this).text();
                            }
                                break;
                            case 7: if (isChecked) {
                                BolsaAux = $(this).text();
                            }
                                break;
                        }
                    });

                    pertenencia = {};

                    pertenencia.Id = IdAux;
                    pertenencia.Nombre = NombreAux;
                    pertenencia.Cantidad = CantidadAux;
                    pertenencia.Estatus = EstatusAux;
                    pertenencia.Observacion = ObservacionAux;
                    pertenencia.Clasificacion = ClasificacionAux;
                    pertenencia.Bolsa = BolsaAux;

                    if (isChecked) {
                        data.push(pertenencia);
                    }

                    isChecked = false;
                });

                dataGenerales = [
                    pertenenciaDe = $("#pertenenciaDeTabla").val(),
                    usuarioQueRegistro = $("#usuarioQueRegistro").val(),
                    personaQueRecibe = $("#personaQueRecibe").val(),
                    condicion = $("#condicionDeEntrega").val(),
                    entrega = $("#entrega").val()
                ];

                if (validarDevolucion()) {
                    saveDevolucion(data, dataGenerales);
                }
            });

            function clearDevolucion() {
                $('#pertenenciaDeTabla').parent().removeClass('state-success');
                $('#pertenenciaDeTabla').parent().removeClass("state-error");
                $('#usuarioQueRegistro').parent().removeClass('state-success');
                $('#usuarioQueRegistro').parent().removeClass("state-error");
                $('#personaQueRecibe').parent().removeClass('state-success');
                $('#personaQueRecibe').parent().removeClass("state-error");
                $('#condicionDeEntrega').parent().removeClass('state-success');
                $('#condicionDeEntrega').parent().removeClass("state-error");
                $("#entrega").select2({ containerCss: { "border-color": "#bdbdbd" } });
                $("#personaQueRecibe").val("");
                $("#condicionDeEntrega").val("");
            }

            function validarDevolucion() {
                var isValid = true;

                //if ($('#pertenenciaDeTabla').val().split(" ").join("") == "") {
                //    ShowError("Pertenencia", "La pertenencia es obligatoria.");
                //    $('#pertenenciaDeTabla').parent().removeClass('state-success').addClass("state-error");
                //    $('#pertenenciaDeTabla').removeClass('valid');
                //    isValid = false;
                //}
                //else {
                //    $('#pertenenciaDeTabla').parent().removeClass("state-error").addClass('state-success');
                //    $('#pertenenciaDeTabla').addClass('valid');
                //}

                //if ($('#usuarioQueRegistro').val().split(" ").join("") == "") {
                //    ShowError("Usuario que registro", "El usuario que registro es obligatorio.");
                //    $('#usuarioQueRegistro').parent().removeClass('state-success').addClass("state-error");
                //    $('#usuarioQueRegistro').removeClass('valid');
                //    isValid = false;
                //}
                //else {
                //    $('#usuarioQueRegistro').parent().removeClass("state-error").addClass('state-success');
                //    $('#usuarioQueRegistro').addClass('valid');
                //}

                if ($('#personaQueRecibe').val().split(" ").join("") == "") {
                    ShowError("Persona que recibe", "La persona que recibe es obligatoria.");
                    $('#personaQueRecibe').parent().removeClass('state-success').addClass("state-error");
                    $('#personaQueRecibe').removeClass('valid');
                    isValid = false;
                }
                else {
                    $('#personaQueRecibe').parent().removeClass("state-error").addClass('state-success');
                    $('#personaQueRecibe').addClass('valid');
                }

                if ($('#condicionDeEntrega').val().split(" ").join("") == "") {
                    ShowError("Condición de entrega", "La condición de entrega es obligatoria.");
                    $('#condicionDeEntrega').parent().removeClass('state-success').addClass("state-error");
                    $('#condicionDeEntrega').removeClass('valid');
                    isValid = false;
                }
                else {
                    $('#condicionDeEntrega').parent().removeClass("state-error").addClass('state-success');
                    $('#condicionDeEntrega').addClass('valid');
                }

                if ($('#entrega').val() == "0" || $('#entrega').val() == null) {
                    ShowError("Entrega", "La entrega es obligatoria.");
                    $("#entrega").select2({ containerCss: { "border-color": "#a90329" } });
                    isValid = false;
                }
                else {
                    $("#entrega").select2({ containerCss: { "border-color": "#bdbdbd" } });
                }

                return isValid;
            }

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            function saveDevolucion(data, dataGenerales) {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ data, dataGenerales }),
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            $("#form-modal-tabla").modal("hide");
                            clearDevolucion();
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'></button><i class='fa-fw fa fa-times'></i><strong>Bien!</strong>" +
                                "Devolución generada exitosamente.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien!", "Devolución generada exitosamente.");
                            var ruta = resolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                            obtenerInfo();
                        }
                        else {
                            if (resultado.errores.length > 0) {
                                ShowAlert("Atención!", resultado.errores[0]);
                            }
                            else {
                                ShowError("Error!", "Ocurró un error, " + resultado.message + ".");
                            }                            
                        }
                    }
                });
            }

            function loadTable(data_) {
                table = $('#dt_basic_tabla').DataTable({
                    //"lengthMenu": [5, 10, 20, 30, 100],
                    //iDisplayLength: 100,
                    serverSide: false,
                    paging: false,
                    "scrollX": true,
                    retrieve: true,
                    "scrollY": "350px",
                    "scrollCollapse": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_tabla) {
                            responsiveHelper_dt_basic_tabla = new ResponsiveDatatablesHelper($('#dt_basic_tabla'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_tabla.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_tabla.respond();
                    },
                    data: data_,
                    columnDefs: [
                        {
                            targets: 0,
                            width: '5%',
                            name: "PertenenciaId",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return '<input type="checkbox" checked />';
                            }
                        },
                        {
                            targets: 1,
                            width: '5%',
                            name: "PertenenciaId",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.PertenenciaId + "</div>";
                            }
                        },
                        {
                            targets: 2,
                            width: '25%',
                            name: "Nombre",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Nombre + "</div>";
                            }
                        },
                        {
                            targets: 3,
                            width: '10%',
                            name: "Cantidad",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Cantidad + "</div>";
                            }
                        },
                        {
                            targets: 4,
                            width: '20%',
                            name: "Estatus",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Estatus + "</div>";
                            }
                        },
                        {
                            targets: 5,
                            width: '20%',
                            name: "Observacion",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Observacion + "</div>";
                            }
                        },
                        {
                            targets: 6,
                            width: '25%',
                            name: "Clasificacion",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Clasificacion + "</div>";
                            }
                        },
                        {
                            targets: 7,
                            width: '10%',
                            name: "Bolsa",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Bolsa + "</div>";
                            }
                        }
                    ]
                });

            }

            function loadTableDonacion(data_) {
                table = $('#dt_basic_tabla_donacion').DataTable({
                    //"lengthMenu": [5, 10, 20, 30, 100],
                    //iDisplayLength: 100,
                    serverSide: false,
                    paging: false,
                    "scrollX": true,
                    retrieve: true,
                    "scrollY": "350px",
                    "scrollCollapse": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span style="margin-bottom:4px; height:16px; width:11px;" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_tabla_donacion) {
                            responsiveHelper_dt_basic_tabla_donacion = new ResponsiveDatatablesHelper($('#dt_basic_tabla_donacion'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_tabla_donacion.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_tabla_donacion.respond();
                    },
                    data: data_,
                    columnDefs: [
                        {
                            targets: 0,
                            width: '5%',
                            name: "PertenenciaId",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return '<input type="checkbox" checked />';
                            }
                        },
                        {
                            targets: 1,
                            width: '5%',
                            name: "PertenenciaId",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.PertenenciaId + "</div>";
                            }
                        },
                        {
                            targets: 2,
                            width: '25%',
                            name: "Nombre",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Nombre + "</div>";
                            }
                        },
                        {
                            targets: 3,
                            width: '10%',
                            name: "Cantidad",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Cantidad + "</div>";
                            }
                        },
                        {
                            targets: 4,
                            width: '20%',
                            name: "Estatus",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Estatus + "</div>";
                            }
                        },
                        {
                            targets: 5,
                            width: '20%',
                            name: "Observacion",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Observacion + "</div>";
                            }
                        },
                        {
                            targets: 6,
                            width: '25%',
                            name: "Clasificacion",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Clasificacion + "</div>";
                            }
                        },
                        {
                            targets: 7,
                            width: '10%',
                            name: "Bolsa",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return "<div class='text-right'>" + row.Bolsa + "</div>";
                            }
                        }
                    ]
                });

            }

            //$("#buscarData").click(function () {
            //    window.emptytable = false;
            //    $("#ctl00_contenido_lblMessage").html("");
            //    $('#dt_basic').DataTable().ajax.reload();
            //});
            
            pageSetUp();

            $("#devolucion").click(function () {
                //Arreglo para Id's de detenido
                var data = [];

                //Arreglo para Id's de pertenencias
                var dataIdPertenencias = [];

                $("input[type=checkbox]:checked").each(function () {

                    if ($(this).val() != "on" && $(this).val() != "null") {
                        dataIdPertenencias.push($(this).val());
                        data.push($(this).attr('data-internoid'));
                    }

                });

                var value = data[0];

                for (i = 0; i < data.length; i++) {
                    if (data[i] != value) {
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención!</strong>" +
                            "Ha seleccionado pertenencias para devolver que pertenecen a diferentes detenidos, por favor seleccione solo las pertenencias del mismo detenido.</div>");
                        setTimeout(hideMessage, hideTime);
                        ShowAlert("Atención!", "Ha seleccionado pertenencias para devolver que pertenecen a diferentes detenidos, por favor seleccione solo las pertenencias del mismo detenido.");
                        return;
                    }
                }

                dataValue = [
                    Id = value
                ];

                if (dataIdPertenencias.length == 0 && data.length == 0) {
                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención!</strong>" +
                        "Ha seleccionado detenidos que no tienen pertenencias ó pertenencias que han sido entregadas, por favor seleccione pertenencias validas.</div>");
                    setTimeout(hideMessage, hideTime);
                    ShowAlert("Atención!", "Ha seleccionado detenidos que no tienen pertenencias ó pertenencias que han sido entregadas, por favor seleccione pertenencias validas.");
                    return;
                }

                responsiveHelper_dt_basic_tabla = undefined;
                $('#dt_basic_tabla').DataTable().destroy();
                getDataFromPertenencias(dataValue, dataIdPertenencias);
                loadEntrega("0");
            });

            $("#donar").click(function () {
                var data = new Array();
                var dataIdPertenencias = [];

                $("input[type=checkbox]:checked").each(function () {
                    if ($(this).val() != "on" && $(this).val() != "null") {
                        dataIdPertenencias.push($(this).val());
                        data.push($(this).attr('data-internoid'));
                    }
                });

                var value = data[0];

                for (i = 0; i < data.length; i++) {
                    if (data[i] != value) {
                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención!</strong>" +
                            "Ha seleccionado pertenencias para donación que pertenecen a diferentes detenidos, por favor seleccione solo las pertenencias del mismo detenido.</div>");
                        setTimeout(hideMessage, hideTime);
                        ShowAlert("Atención!", "Ha seleccionado pertenencias para donación que pertenecen a diferentes detenidos, por favor seleccione solo las pertenencias del mismo detenido");
                        return;
                    }
                }

                dataValue = [
                    Id = value
                ];

                if (dataIdPertenencias.length == 0 && data.length == 0) {
                    $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Atención!</strong>" +
                        "Ha seleccionado detenidos que no tienen pertenencias ó pertenencias que han sido entregadas, por favor seleccione pertenencias validas.</div>");
                    setTimeout(hideMessage, hideTime);
                    ShowAlert("Atención!", "Ha seleccionado detenidos que no tienen pertenencias ó pertenencias que han sido entregadas, por favor seleccione pertenencias validas.");
                    return;
                }

                responsiveHelper_dt_basic_tabla_donacion = undefined;
                $('#dt_basic_tabla_donacion').DataTable().destroy();
                getDataFromPertenenciasDonacion(dataValue, dataIdPertenencias);
                loadInstituciones("0");
                loadEntregaDonacion("0");
            });

            function getDataFromPertenencias(data, dataIdPertenencias) {
                $.ajax({
                    async:false,
                    type: "POST",
                    url: "control_pertenencias.aspx/getPertenencias",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        'datos': data, 'idPertenencias': dataIdPertenencias
                    }),
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);
                        $("#form-modal-title-tabla").empty();
                        $("#form-modal-title-tabla").html('<i class="fa fa-pencil" +=""></i> Detalle del registro');
                        $("#form-modal-tabla").modal("show");
                        $("#pertenenciaDeTabla").val(r.interno);
                        //$("#usuarioQueRegistro").val(r.usuarios);
                        loadTable(r.list);
                     
                        
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar las pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            $("#form-modal-tabla").on('shown.bs.modal', function(){
                 var table = $('#dt_basic_tabla').DataTable();
                        //console.log("cargadatos2");
                        table.columns.adjust().draw();
            });

            function getDataFromPertenenciasDonacion(data, dataIdPertenencias) {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getPertenencias",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        'datos': data, 'idPertenencias': dataIdPertenencias
                    }),
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var r = JSON.parse(response.d);
                        $("#form-modal-title-donacion").empty();
                        $("#form-modal-title-donacion").html('<i class="fa fa-pencil" +=""></i> Detalle del registro');
                        $("#form-modal-donacion").modal("show");
                        $("#pertenenciaDeDonacion").val(r.interno);
                        //$("#usuarioQueRegistroDonacion").val(r.usuarios);
                        loadTableDonacion(r.list);
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar las pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }
             $("#form-modal-donacion").on('shown.bs.modal', function(){
                 var table = $('#dt_basic_tabla_donacion').DataTable();
                        //console.log("cargadatos2");
                        table.columns.adjust().draw();
            });


            $("body").on("click", ".edit", function () {
                loadClasifications($(this).attr("data-clasificacion"));
                loadCasilleros2($(this).attr("data-casillero"));
                var pertenenciaDe = $(this).attr("data-pertenenciaDe") != "null" ? $(this).attr("data-pertenenciaDe") : "";
                var pertenencia = $(this).attr("data-pertenencia") != "null" ? $(this).attr("data-pertenencia") : "";
                var observacion = $(this).attr("data-observacion") != "null" ? $(this).attr("data-observacion") : "";
                var usuario = $(this).attr("data-recibio");                
                
                usuario = usuario.toString().includes("null null null") ? "" : usuario;
                
                $("#pertenenciaDe").val(pertenenciaDe);
                $("#pertenencia").val(pertenencia);
                $("#observacion").val(observacion);
                $("#CPusuario").val(usuario);
              
                $("#bolsa").val($(this).attr("data-bolsa") != "0" ? $(this).attr("data-bolsa") : "");
                $("#cantidad").val($(this).attr("data-cantidad") != "0" ? $(this).attr("data-cantidad") : "");
                var foto = $(this).attr("data-foto");
                var fotoUrl = "";
                var fotoFinal = "";

                if (foto != "" && foto != null) {
                    fotoUrl = resolveUrl(foto);
                    fotoFinal = fotoUrl.replace("//", "/");
                }
                else {
                    fotoFinal = "<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/img/avatars/object.png";
                }

                $("#clasificacion").attr('disabled', 'disabled');
                $("#casillero2").attr('disabled', 'disabled');
                $("#foto_pertenencia").attr("src", fotoFinal);
                $("#form-modal-title").empty();
                $("#form-modal-title").html("Detalle del artículo");
                $("#form-modal").modal("show");
            });

            var dtable = null;

            //Tabla detenidos principal
            var responsiveHelper_dt_basic_detenidos = undefined;
            var responsiveHelper_datatable_fixed_column_detenidos = undefined;
            var responsiveHelper_datatable_col_reorder_detenidos = undefined;
            var responsiveHelper_datatable_tabletools_detenidos = undefined;            

            function loadTableDetenidos() {
                var detId = 0;
                var param = RequestQueryString("valor");
                if (param != undefined) {
                    detId = param;
                }

                $('#dt_basic_detenidos').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,                    
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_detenidos) {
                            responsiveHelper_dt_basic_detenidos = new ResponsiveDatatablesHelper($('#dt_basic_detenidos'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_detenidos.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_detenidos.respond();
                        $('#dt_basic_detenidos').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {                        
                    },
                    ajax: {
                        url: "control_pertenencias.aspx/getDataInternos",
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: function (d) {
                            $('#dt_basic_detenidos').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            d.detId = detId;
                            d.pages = $('#dt_basic_detenidos').DataTable().page.info().page || "";
                            return JSON.stringify(d);
                        },
                        dataSrc: "data",
                        dataFilter: function (data) {
                            var json = jQuery.parseJSON(data);
                            json.recordsTotal = json.d.recordsTotal;
                            json.recordsFiltered = json.d.recordsFiltered;
                            json.data = json.d.data;
                            return JSON.stringify(json);
                        }
                    },                    
                    columns: [
                        null,
                        null,
                        {
                            name: "Expediente",
                            data: "Expediente",
                            visible: false
                        },
                        {
                            name: "Nombre",
                            data: "Nombre",
                            visible: false
                        },
                        {
                            name: "Paterno",
                            data: "Paterno",
                            visible: false
                        },
                        {
                            name: "Materno",
                            data: "Materno",
                            visible: false
                        },
                        {
                            name: "NombreCombpleto",
                            data: "NombreCombpleto",
                            visible: false
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "Nombre",
                            render: function (data, type, row, meta) {
                                var val = row.Expediente + " - " + row.Nombre + " " + row.Paterno + " " + row.Materno;
                                return val;
                            }
                        },
                        {
                            targets: 1,                            
                            render: function (data, type, row, meta) {
                                return '&nbsp;<a href="javascript:void(0);" style="padding: 6px 12px;" class="btn btn-primary selectDetenido" data-id="' + row.Id + '" title="Seleccionar">Seleccionar</a>&nbsp;';
                            }
                        }
                    ]
                });

                dtable = $("#dt_basic_detenidos").dataTable().api();
                
                $("#dt_basic_detenidos_filter input[type='search']")
                    .unbind()
                    .bind("input", function (e) {
                        
                        if (this.value == "") {
                            dtable.search("").draw();
                        }
                        return;
                    });

                var $textarea = $("#dt_basic_detenidos_filter input[type='search']");

                $textarea.on('blur', function () {
                    setTimeout(function () {
                        $textarea.focus();
                        $('.btn-group').removeClass('open');
                    }, 0);
                });

                $("#dt_basic_detenidos_filter input[type='search']").keypress(function (e) {
                    if (e.charCode === 13) {
                        dtable.search($("#dt_basic_detenidos_filter input[type='search']").val()).draw();
                    }                    
                });
            }                      

            $("body").on('click', '.selectDetenido', function () {                
                if ($(this).text() === "Seleccionado") {                    
                    $(this).removeClass('btn-success').addClass('btn-primary');                    
                    this.text = "Seleccionar";
                    detenidoActual = 0;
                    $("#dt_basic").DataTable().ajax.reload();
                   
                }
                else {
                    $('.selectDetenido').text("Seleccionar");
                    $('.selectDetenido').removeClass('btn-success').addClass('btn-primary');
                    $(this).removeClass('btn-primary').addClass('btn-success');
                    this.text = "Seleccionado";
                    detenidoActual = $(this).attr('data-id');
                    $("#dt_basic").DataTable().ajax.reload();
                } 
                $("#Tabla_detenidos").modal("hide");
            });

            function loadInstituciones(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getInstituciones",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#institucion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Institucion]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de instituciones. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadEntregaDonacion(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getEntrega",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#entregaDonacion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Entrega]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de tipos de entrega. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadEntrega(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getEntrega",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#entrega');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Entrega]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de tipos de entrega. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function loadClasifications(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "control_pertenencias.aspx/getClasifications",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#clasificacion');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificación de pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };                       

            obtenerInfo();

            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
             }

            function obtenerInfo() {
                var param = RequestQueryString("valor");
                var id=""
                if (param != undefined) {
                    id = param;

                }
                else
                {
                    id = $("#internoSelect").val();
                }
                dataFiltro = [
                    Uno = ($("#egreso").is(":checked")) ? 1 : 0,
                    Dos = ($("#ingresoReingresoConPertenencias").is(":checked")) ? 1 : 0,
                    Tres = ($("#ingresoReingresoSinPertenencias").is(":checked")) ? 1 : 0,
                    internoId = id
                ];
                
                //$.ajax({
                //    type: "POST",
                //    url: "control_pertenencias.aspx/getData",
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    cache: false,
                //    data: JSON.stringify({
                //        datos: dataFiltro
                //    }),
                //    success: function (response) {        
                //        var r = JSON.parse(response.d);
                //        loadTablePrincipal(r.list);
                //    },
                //    error: function () {
                //        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clasificación de pertenencias. Si el problema persiste contacte al soporte técnico del sistema.");
                //    }
                //});
            }

            function loadPrincipalData() {
                startLoading();
                loadTableDetenidos();

                $('#dt_basic').dataTable({
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,                    
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic').waitMe('hide');
                    },
                    "createdRow": function (row, data, index) {
                        
                    },
                    ajax: {
                        url: "control_pertenencias.aspx/getDataPrincipal",
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: function (d) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            dataFiltro = [
                                Uno = ($("#egreso").is(":checked")) ? 1 : 0,
                                Dos = 1,//($("#ingresoReingresoConPertenencias").is(":checked")) ? 1 : 0,
                                Tres = ($("#ingresoReingresoSinPertenencias").is(":checked")) ? 1 : 0,
                                internoId = detenidoActual || RequestQueryString("valor") || ""
                            ];
                            d.datos = dataFiltro;
                            d.pages = $('#dt_basic').DataTable().page.info().page || "";
                            d.year = $("#filterYear").val() || "";
                            return JSON.stringify(d);
                        },
                        dataSrc: "data",
                        dataFilter: function (data) {
                            var json = jQuery.parseJSON(data);
                            json.recordsTotal = json.d.recordsTotal;
                            json.recordsFiltered = json.d.recordsFiltered;
                            json.data = json.d.data;
                            console.log('data', json.d.data);
                            endLoading();
                            return JSON.stringify(json);
                        }
                    },                    
                    columns: [
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 0,
                            data: "Id",
                            render: function (data, type, row, meta) {
                                if (row.Estatus != "Sin entregar") {
                                    return '<input type="checkbox" value="' + null + '" data-internoId="' + row.IdEstatusInterno + '" data-esValido="' + row.esValido + '" data-mensajeRN="' + row.mensajeRN + '" />';
                                }
                                else {
                                    if (row.habilitadoAux) {
                                        return '<input type="checkbox" value="' + row.PertenenciaId + '" data-internoId="' + row.IdEstatusInterno + '" data-esValido="' + row.esValido + '" data-mensajeRN="' + row.mensajeRN + '" />';
                                    }
                                    else {
                                        return '<input type="checkbox" value="' + null + '" data-internoId="' + row.IdEstatusInterno + '" data-esValido="' + row.esValido + '" data-mensajeRN="' + row.mensajeRN + '" />';
                                    }
                                }
                            }
                        },
                        {
                            targets: 1,
                            data: "Expediente",
                            render: function (data, type, row, meta) {
                                return row.Expediente;
                            }
                        },
                        {
                            targets: 2,
                            data: "Nombre",
                            render: function (data, type, row, meta) {
                                return row.Nombre;
                            }
                        },
                        {
                            targets: 3,
                            data: "paterno",
                            render: function (data, type, row, meta) {
                                return row.Paterno;
                            }
                        },
                        {
                            targets: 4,
                            data: "materno",
                            render: function (data, type, row, meta) {
                                return row.Materno;
                            }
                        },
                        {
                            targets: 5,
                            data: "Clasificacion",
                            render: function (data, type, row, meta) {
                                if (row.Clasificacion != null) {
                                    return row.Clasificacion;
                                }
                                else {
                                    return "Sin pertenencia";
                                }
                            }
                        },
                        {
                            targets: 6,
                            data: "Pertenencia",
                            render: function (data, type, row, meta) {
                                if (row.Pertenencia != null) {
                                    return row.Pertenencia;
                                }
                                else {
                                    return "Sin pertenencia";
                                }
                            }
                        },
                        {
                            targets: 7,
                            data: "Cantidad",
                            render: function (data, type, row, meta) {
                                return row.Cantidad;
                            }
                        },
                        {
                            targets: 8,
                            data: "Bolsa",
                            render: function (data, type, row, meta) {
                                return row.Bolsa;
                            }
                        },
                        {
                            targets: 9,
                            data: "Estatus",
                            render: function (data, type, row, meta) {
                                if (row.Estatus != null) {
                                    return row.Estatus;
                                }
                                else {
                                    return "Sin pertenencia";
                                }
                            }
                        },
                        {
                            targets: 10,
                            orderable: false,
                            render: function (data, type, row, meta) {

                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var edit = "edit";

                                if (row.TrackingId == null) {
                                    edit = "disabled";
                                }

                                //var editar = '<a class="btn btn-primary  ' + edit + '" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-clasificacion="' + row.ClasificacionId + '" title = "Ver" > <i class="glyphicon glyphicon-eye-open">&nbsp;Ver</i></a>&nbsp;';
                                var editar = '';
                                //if ($("#ctl00_contenido_KAQWPK").val() == "true") 
                                editar = '&nbsp;&nbsp;&nbsp;<a class="btn btn-primary  ' + edit + '" href="javascript:void(0);" data-id="' + row.TrackingId + '" data-clasificacion="' + row.ClasificacionId + '" data-pertenenciaDe="' + row.Nombre + ' ' + row.Paterno + ' ' + row.Materno + '" data-pertenencia="' + row.Pertenencia + '" data-observacion="' + row.Observacion + '" data-recibio="' + row.NombreUsuario + '" data-bolsa="' + row.Bolsa + '" data-cantidad="' + row.Cantidad + '" data-foto="' + row.Fotografia + '" data-casillero="' + row.casillero + '" title = "Ver" > <i class="glyphicon glyphicon-eye-open"></i>&nbsp;Ver</a >&nbsp;';

                                return editar;
                            }
                        }
                    ]
                });

                endLoading();                
            }                       

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");
            });

            $("body").on("click", ".blockuser", function () {
                var usernameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockuser-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingid = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "contrato.aspx/blockContract",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();

                            $("#blockuser-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho!</strong>" +
                                " El registro  se actualizó correctamente.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error!", "Algo salió mal y no fue posible afectar el estatus del usuario. . Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });            

            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }

                var uno = JSON.parse($("#ctl00_contenido_HQLNBB").val());
                var dos = JSON.parse($("#ctl00_contenido_KAQWPK").val());
                var tres = JSON.parse($("#ctl00_contenido_LCADLW").val());
                
                if (uno && dos && tres) {                                                            
                    $("#agregarPertenencia").show();
                    $("#devolucion").show();
                    $("#donar").show();
                }
                else {
                    $("#agregarPertenencia").hide();
                    $("#devolucion").hide();
                    $("#donar").hide();
                }

                var estatus = RequestQueryString("status") || "";
                
                if (estatus !== undefined && estatus !== "") {
                    switch (estatus) {
                        case "2":
                                $("#egreso").prop('checked', true);
                                $("#ingresoReingresoConPertenencias").prop('checked', false);
                                $("#ingresoReingresoSinPertenencias").prop('checked', false);
                            break;
                    }
                }

                loadPrincipalData();
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
    <script>
        function reload() {
            $( "#dt_basic_tabla" ).load(window.location.href + " #dt_basic_tabla" );
        }
    </script>
</asp:Content>
