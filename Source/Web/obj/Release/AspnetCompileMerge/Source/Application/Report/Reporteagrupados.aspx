﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="Reporteagrupados.aspx.cs" Inherits="Web.Application.Report.Reporteagrupados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>  
        <li>Juez calificador</li>
    <li>Agrupado de detenidos</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-file-text-o"></i>
                Agrupado de detenidos
            </h1>
        </div>
    </div>
    <section id="widget-grid" class="">
        <div class="row">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </div>
        <div class="row">

            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken jarviswidget-sortable" id="wid-remisiones-0" role="widget" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="flase" data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-file-text-o"></i></span>
                        <h2>Agrupado de detenidos</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                            <div>
                                <label>Title:</label>
                                <input type="text" />
                            </div>
                        </div>
                        </div>
                        <div class="widget-body">
                           
                            <div class="smart-form">
                                <fieldset>
                                <div class="row">
                               <section class="col col-4">
                                    <label class="input" >Fecha de inicio<a style="color:red">*</a></label>
                                                  <label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker'>
                                                                <input type="text" name="fechahora" id="fechahora" class='input-group date' placeholder="Fecha de inicio" data-requerido="true"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                        </label>
                                                <i></i>
                                        
                                                </section>
                                        <section class="col col-4">
                                    <label class="input" >Fecha final<a style="color:red">*</a></label>
                                                  <label class="input">
                                                            <label class='input-group date' id='FehaHoradatetimepicker2'>
                                                                <input type="text" name="fechahora2" id="fechahora2" class='input-group date' placeholder="Fecha final" data-requerido="true"/>
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </label>
                                                        </label>
                                                <i></i>
                                      
                                                </section>
                                    <br />
                                                    <section class="col col-4">
                                                        <a class="btn btn-success btn-md reporte" style="padding:5px" id="generapdf"><i class="glyphicon glyphicon-file"></i> Generar PDF</a>
                                                    </section>
                                                  </div>
                                </fieldset>
                                              
                           
                            </div>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        
                                        <th data-class="expand">#</th>
                                        
                                        <th >No. remisión</th>
                                        <th>Nombre</th>
                                        <th>No. remisión</th>
                                        <th>Nombre</th>
                                    </tr>
                                </thead>
                            </table>
                            </div>
                    </div>
                            </article>
            </div>
        
                             <div class ="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             
        </article>
    </div>
                      
                      

    </section>
        </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server"> 

     <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/highcharts/highmaps.js"></script>

    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/moment/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/chartjs/chart.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function(){

           $('#FehaHoradatetimepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
              $('#FehaHoradatetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });     


                $("body").on("click", "#generapdf", function (e) {
                    if (validarCampos())
                    {
                        ImprimeTrabajoSocial();

                    }
            });

            function ImprimeTrabajoSocial() {
                          startLoading();     
                          var datos = {
                              Fechainicio: $("#fechahora").val(),
                              Fechafin: $("#fechahora2").val()
                          };
                $.ajax({                    
                    type: "POST",
                    url: "Reporteagrupados.aspx/GeneraPdF",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'rangofechas': datos
                    }),
                    cache: false,
                    success: function (data) {                                                
                        if (data.d.exitoso) {                                                                                 
                          
                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>Bien hecho! </strong>" +
                                "Se genero correctamente el reporte.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("Bien hecho!", "Se genero correctamente el reporte .");

                            var ruta = ResolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }
                            LimpiarCampos();
                        }
                        else {
                            if (data.d.mensaje == "No se encontraron resultados") {
                                ShowAlert("¡Atención!",data.d.mensaje);
                            }
                            else {
                                ShowError("Error! Algo salió mal", data.d.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                            }
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + data.d.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "order": [[2, "desc"]],
                ajax: {
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/Reporteagrupados.aspx/getdata",
                     contentType: "application/json; charset=utf-8",
                     data: function (parametrosServerSide) {
                         $('#dt_basic').waitMe({
                             effect: 'bounce',
                             text: 'Cargando...',
                             bg: 'rgba(255,255,255,0.7)',
                             color: '#000',
                             sizeW: '',
                             sizeH: '',
                             source: ''
                         });

                         parametrosServerSide.emptytable = false;
                         return JSON.stringify(parametrosServerSide);
                     }

                 },
                 columns: [
                     null,
                     
                     {
                         name: "Expediente",
                         data: "Expediente"
                     },
                     {
                         name: "DetenidoOriginal",
                         data: "DetenidoOriginal"
                     },
                     {
                         name: "Expediente1",
                         data: "Expediente1"
                     },
                     {
                         name: "Detenido2",
                         data: "Detenido2"
                     },
                     
                     
                 ],
                 columnDefs: [

                    
                     {
                         targets: 0,
                         orderable: false,
                         render: function (data, type, row, meta) {
                             return meta.row + meta.settings._iDisplayStart + 1;
                         }
                     }
                 ]

            });

            function LimpiarCampos()
            {
                $('#fechahora').val("");
                $('#fechahora2').val("");
                $('#fechahora').parent().removeClass('state-success');
                $('#fechahora2').parent().removeClass('state-success');
            }

             function validarCampos()
            {
                var  esvalido = true;   
                 

                if ($("#fechahora").val() == null || $("#fechahora").val().split(" ").join("") == "") {
                    ShowError("Fecha de inicio", "El campo fecha de inicio  es obligatorio.");
                    $('#fechahora').parent().removeClass('state-success').addClass("state-error");
                    $('#fechahora').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fechahora').parent().removeClass("state-error").addClass('state-success');
                    $('#fechahora').addClass('valid');
                }

                  if ($("#fechahora2").val() == null || $("#fechahora2").val().split(" ").join("") == "") {
                    ShowError("Fecha final", "El campo fecha final  es obligatorio.");
                    $('#fechahora2').parent().removeClass('state-success').addClass("state-error");
                    $('#fechahora2').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fechahora2').parent().removeClass("state-error").addClass('state-success');
                    $('#fechahora2').addClass('valid');
                 }
                 
                 

                return esvalido;

            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
