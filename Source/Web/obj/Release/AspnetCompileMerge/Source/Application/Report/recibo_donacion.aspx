﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="recibo_donacion.aspx.cs" Inherits="Web.Application.Report.recibo_donacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
        <li>Control de pertenencias</li>
    <li>Recibos de donación </li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-file-text-o fa-fw "></i>
                Recibos donación de pertenencias / evidencias
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>    
    <p></p>
    <section id="widget-grid" class="">
        <div class="row">            
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-ticket"></i></span>
                        <h2>Recibos donación de pertenencias / evidencias</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div class="row">
                                    <section class="col col-md-12" id="BotonEvidencia">
                                        <a class="btn btn-md btn-default " href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Control_Pertenencias/control_pertenencias.aspx"" style="padding:5px">
                                            <i class="fa fa-mail-reply"></i> Regresar a control de pertenencias</a>
                                    </section>
                                </div>
                            <table id="dt_basic_cortes" class="table table-striped table-bordered table-hover" width="100%" >
                                <thead>
                                    <tr>                                             
                                        <th data-class="expand">#</th>
                                        <th>Pertenencias de</th>
                                        <th>Usuario que registró</th>                                                                                                                                          
                                        <th>Fecha entrega</th>                                                                                                                                          
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
        </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {       
            
            //Variables tabla de cortes
            var responsiveHelper_dt_basic_cortes = undefined;
            var responsiveHelper_datatable_fixed_column_cortes = undefined;
            var responsiveHelper_datatable_col_reorder_cortes = undefined;
            var responsiveHelper_datatable_tabletools_cortes = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            ValidarBoton();

            function ValidarBoton() {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/evidencias_report.aspx/ValidarBoton",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            if (resultado.Boton) {

                                $("#BotonEvidencia").show();
                            }
                            else {
                                $("#BotonEvidencia").hide();
                            }
                        }
                        else {
                            ShowError("¡Error! No fue posible validar si mostrar el boton", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }
            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }            

            $("body").on("click", ".imprimirRecibo", function () {
                var tracking = $(this).attr("data-tracking");
                var trackingInterno = $(this).attr("data-tracking-estatus");
                data = [
                    tracking,
                    trackingInterno
                ];

                generarReciboCorte(data);
            });

            function generarReciboCorte(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "recibo_donacion.aspx/generarReciboDonacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            var ruta = ResolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }                    
                            $('#main').waitMe('hide');
                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El recibo se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "El recibo se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! No fue posible guardar el pago de la multa", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            $('#dt_basic_cortes').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                 "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_cortes) {
                        responsiveHelper_dt_basic_cortes = new ResponsiveDatatablesHelper($('#dt_basic_cortes'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_cortes.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_cortes.respond();
                    $('#dt_basic_cortes').waitMe('hide');
                },
                ajax: {
                    type: "POST",
                    url: "recibo_donacion.aspx/getData",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id"
                    },                  
                    {
                        name: "PertenenciasDe",
                        data: "PertenenciasDe"
                    },
                    {
                        name: "UsuarioQueRegistro",
                        data: "UsuarioQueRegistro"
                    },
                    {
                        name: "Fecha",
                        data: "Fecha"
                    },                    
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },                    
                    {
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "imprimirRecibo";
                            var registro = '<a class="btn btn-success btn-sm ' + edit + '" href="javascript:void(0);" data-tracking="' + row.TrackingId + '" data-tracking-estatus="' + row.TrackingInterno + '" title = "Editar" ><i class="glyphicon glyphicon-file"></i>&nbsp;&nbsp;Generar PDF</a>&nbsp; ';                    

                            return registro;
                        }
                    }
                ]
            });            

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
