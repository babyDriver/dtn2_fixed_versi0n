﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" CodeBehind="reporteTrabajoSocial.aspx.cs" Inherits="Web.Application.Report.reporteTrabajoSocial" %>




<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
    <li>Trabajo social</li>
    <li>Reporte de trabajo social</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
         td.strikeout {
            text-decoration: line-through;
        }
        #dropdown {
            width: 439px;
        }
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-file-text-o fa-fw "></i>
                Reporte trabajo social
            </h1>
        </div>
    </div>
        <div class="row">
            <section class="col col-md-12" id="BotonTrabajo">
                <a class="btn btn-md btn-default" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Trabajo_social/Trabajosocial.aspx" style="padding:5px">
                <i class="fa fa-mail-reply"></i> Regresar a trabajo social</a>
            </section>
        </div>
        <br />
    <div class="row">
     <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken"  id="wid-entrya-1"  data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="glyphicon  glyphicon-edit"></i></span>
                    <h2>Criterios de búsqueda </h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <div id="smart-form-register-entry" class="smart-form">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">Nombre(s)</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="nombre"  id="nombre" placeholder="Nombre" maxlength="50" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el nombre.</b>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Apellido paterno</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="apellido_paterno"  id="paterno" placeholder="Apellido paterno" maxlength="50" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el apellido paterno.</b>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Apellido materno</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="apellido_materno"  id="materno" placeholder="Apellido materno" maxlength="50" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el apellido materno.</b>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">R.F.C.</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="rfc"  id="rfc" placeholder="R.F.C." maxlength="50" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa el R.F.C.</b>
                                        </label>
                                    </section>
                                     <section class="col col-3">
                                        <label class="label">Sexo</label>
                                        <label class="select">
                                            <select name="sexo" id="sexo"></select>
                                            <i></i>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Ocupación</label>
                                        <label class="select">
                                            <select name="ocupacion" id="ocupacion"></select>
                                            <i></i>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Nacionalidad</label>
                                        <label class="select">
                                            <select name="nacionalidad" id="nacionalidad"></select>
                                            <i></i>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Calle</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-pencil"></i>
                                            <input type="text" name="calle"  id="calle" placeholder="Calle" maxlength="150" class="alphanumeric"/>
                                            <b class="tooltip tooltip-bottom-right">Ingresa la calle.</b>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Número</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-sort-numeric-desc"></i>
                                            <input type="text" name="numero" id="numero" placeholder="Número" class="alphanumeric" maxlength="15" />
                                            <b class="tooltip tooltip-bottom-right">Ingresa el número.</b>
                                        </label>
                                    </section> 
                                    <section class="col col-3" id="sectioncoloniaDomicilio">
                                        <label class="label">Colonia</label>
                                        <label class="select">
                                            <select name="centro" id="colonia" >
                                            </select>
                                            <i></i>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Teléfono</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-phone"></i>
                                            <input type="tel" name="telefono" id="telefono"  placeholder="Teléfono" data-mask="(999) 999-9999" />
                                            <b class="tooltip tooltip-bottom-right">Ingresa el teléfono .</b>
                                        </label>
                                    </section>
                                    <section class="col col-3" id="aliass">
                                        <label class="label">Alias</label>
                                        <label class="select">
                                            <select name="centro" id="alias" >
                                            </select>
                                            <i></i>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Estatura</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-sort-numeric-desc"></i>
                                            <input type="text" name="estatura" id="estatura"  placeholder="Estatura" class="alphanumeric" maxlength="15" />
                                            <b class="tooltip tooltip-bottom-right">Ingresa la estatura.</b>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">Peso</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-sort-numeric-desc"></i>
                                            <input type="text" name="peso" id="peso" placeholder="Peso" class="alphanumeric" maxlength="15" />
                                            <b class="tooltip tooltip-bottom-right">Ingresa el peso.</b>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                            <footer>
                                <a class="btn btn-primary" id="buscar" title="Volver al listado"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="row" id="addentry">
        <section class="col col-4">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
              <!--  <a href="javascript:void(0);" class="btn btn-md btn-default calculate" id="calculate"><i class="fa fa-calculator"></i>&nbsp;Cálculo de porcentaje compurgado </a>-->
            </div>
        </section>
        <section class="col col-6">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <%--<a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Sentence/celdas.aspx"" class="btn btn-md btn-default" id="celdas"><i class="fa"></i>Celdas </a>--%>
            </div>
        </section>
    </div>
    <p></p>
    <div class ="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></article>
    </div>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <br />
            <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-group"></i></span>
                    <h2>Listado de personas en trabajo social</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="row"> 
                        <div class="widget-body" >
                            <table id="dt_basic" width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th data-class="expand">#</th>
                                        <th>Fotografía</th>
                                        <th >Nombre</th>
                                        <th>Apellido paterno</th>
                                        <th data-hide="phone,tablet">Apellido materno</th>
                                        <th data-hide="phone,tablet">No. remisión</th>
                                        <th data-hide="phone,tablet">Situación del detenido</th>
                                        <th data-hide="phone,tablet">Acciones</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
    </div>
    <div id="printpdf-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Reporte de extravío
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="printpdf-modal-body" class="smart-form">

                        <div class="modal-body">
                            <div id="printpdf-modal-body-form" class="smart-form">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="row">
                                                <section class="col col-10">
                                                    <img id="avatar" class="img-thumbnail" alt=""  src=" <%= ConfigurationManager.AppSettings["relativepath"]  %> Content/img/avatars/male.png" width="120" height="120" />
                                                </section>
                                            </div>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Nombre <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" readonly="true" name="nombrereportepdf" id="nombrereportepdf" placeholder="Nombre" />
                                                        <b class="tooltip tooltip-bottom-right">Nombre</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Edad <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="edadreportepdf" id="edadreportepdf" placeholder="Edad" class="integer" maxlength="2" />
                                                        <b class="tooltip tooltip-bottom-right">Edad</b>
                                                    </label>
                                                </section>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioBuscandopdf" value="BUSCADO(A)" /><i></i>Buscado
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioExtraviadopdf" value ="EXTRAVIADO(A)" /><i></i>Extraviado
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="radiotipopdf" id="radioInformacionpdf" value="INFORMACION" /><i></i>Información
                                            </label>
                                        </div>
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Alias <a style="color: red">*</a></label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="aliasreportepdf" id="aliasreportepdf" placeholder="Alias" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Alias</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Violento</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="violentopdf" id="violentopdf" placeholder="Violento" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Violento</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Enfermo mental</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="enfermopdf" id="enfermopdf" placeholder="Enfermo mental" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Enfermo mental</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Señas particulares</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="textarea">
                                                        <textarea rows="3" name="senaspdf" id="senaspdf" placeholder="Señas particulares" maxlength="500"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Señas particulares</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Comentarios</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="textarea">
                                                        <textarea rows="3" name="comentariospdf" id="comentariospdf" placeholder="Comentarios" maxlength="500"></textarea>
                                                        <b class="tooltip tooltip-bottom-right">Comentarios</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-9">
                                                    <label>Información de contacto:</label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Institución</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="institucionpdf" id="institucionpdf" placeholder="Institución" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Institución para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Dirección</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="direccionpdf" id="direccionpdf" placeholder="Dirección" maxlength="256" />
                                                        <b class="tooltip tooltip-bottom-right">Dirección para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-3">
                                                    <label>Teléfono</label>
                                                </section>
                                                <section class="col col-9">
                                                    <label class="input">
                                                        <input type="text" name="telefonopdf" id="telefonopdf" placeholder="Telefono" maxlength="50" />
                                                        <b class="tooltip tooltip-bottom-right">Teléfono para contacto</b>
                                                    </label>
                                                </section>
                                            </div>
                                        </div>

                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <input type="hidden" id="trackingidpdf"/>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal" id="cancelarpdfbtn"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default printpdfbtn" id="printpdfbtn"><i class="fa fa-file-pdf-o"></i>&nbsp;Generar reporte </a>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>
     <div id="datospersonales-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4>Datos personales</h4>
                </div>

                <div class="modal-body col">
                    <div class="col-lg-8">
                    <section>
                    <label>Nombre completo</label>
                    <input id="nombreInterno" style="width:300px" disabled="disabled" /><br /><br />
                    </section>
                    <section>
                    <label style="width:110px;">Edad</label>
                    <input type="text" id="edad" style="width:300px;" disabled="disabled"/><br /><br />
                    </section>
                    <section>
                    <label style="width:110px">Situación</label>
                    <input type="text" id="situacion" style="width:300px" disabled="disabled" /><br /><br />
                    </section>
                        <section>
                    <label style="width:180px">Fecha y hora de registro</label>
                    <input type="datetime" id="horaRegistro" disabled="disabled" />
                        </section>
                        <section>
                        <label style="width:180px">Fecha y hora de salida</label>
                        <input type="datetime" id="salida" disabled="disabled" /><br /><br />
                            <label>Domicilio</label><br />
                    </section>
                        </div>
                    <div class="col-lg-4">
                        <section class="col col-10 text-center">
                           <img id="Img1" class="img-thumbnail" alt="" runat="server" src="~/Content/img/avatars/male.png" height="120" width="120" /><br /><br />
                        </section>
                    </div>
                    <div>
                        <section>
                            
                            <textarea style="height:90px; width:400px;" id="domicilio" disabled="disabled"></textarea>
                        </section>
                        </div>
                </div>
            </div>
        </div>
    </div>


    <div id="RegistroExpediente-modal" class="modal fade"  tabindex="-1" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Expediente
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="registromovimiento-form" class="smart-form">

                        <div class="modal-body">
                            
                        
                            <fieldset>
                                <div  class="Row">
                                                       
                                               
                                                <section class="col col-6">
                                                    <label class="input">Motivo <a style="color: red">*</a></label>
                                                   <label class="select"> 
                                                <select name="motivorehabilitacion" id="motivorehabilitacion" >
                                                    
                                                </select>
                                                       </label> 
                                                <i></i>
                                        
                                                </section>

                                                <section class="col col-6">
                                                    <label class="input">Adicción <a style="color: red">*</a></label>
                                                   <label class="select"> 
                                                <select name="adiccion" id="adiccion" >
                                                </select>
                                                   </label>     
                                                <i></i>
                                        
                                                </section>
                             
                                               <section class="col col-6">
                                                    <label class="label">Pandilla <a style="color: red">*</a></label>
                                                  <label class="input">
                                                            <label class='input' id='celda'>
                                                                
                                                                <input type="text" name="txtpandilla" id="txtpandilla" class='input' placeholder="Pandilla"  data-requerido="true"/>
                                                                
                                                            </label>
                                                       
                                                        </label>
                                                <i></i>
                                                </section >
                                                       <section class="col col-6">
                                                    <label class="label">Religión <a style="color: red">*</a></label>
                                                    <label class="select"> 
                                                <select name="religion" id="religion"  >
                                                    
                                                </select>
                                                      </label>
                                                <i></i>
                                        
                                                </section>


                                                    <section class="col col-6">
                                                        <label class="label">Cuadro patológico <a style="color: red">*</a></label>
                                                        <label class="input">
                                                            <textarea id="cuadropatologico" name="cuadropatologico" cols="30" rows="5" placeholder="Cuadro patológico"></textarea>                                                            
                                                        </label>                                                        
                                                    </section>
                                                    <section class="col col-6">
                                                        <label class="label">Observación</label>
                                                        <label class="input">
                                                            <textarea id="observaciones" name="observaciones" cols="30" rows="5"></textarea>                                                            
                                                        </label>                                                        
                                                        
                                                    </section>
                                                     


                                                </div>
                            </fieldset>
                        </div>
                           
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancela"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default saveregistro" id="guardarexpediente"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>

     <div id="RegistroSalidaEfectuada-modal" class="modal fade"  tabindex="-1" data-backdrop="static" data-keyboard="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Salida efectuada
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="registrosalidaefectuada-form" class="smart-form">

                        <div class="modal-body">
                            
                        
                            <fieldset>
                                <div  class="Row">
                                                       
                                         

                                               <section class="col col-8">
                                                        <label class="label">Observación <a style="color: red">*</a></label>
                                                        <label class="input">
                                                            <textarea id="observacion" name="observacion" cols="60" rows="5"></textarea>                                                            
                                                        </label>                                                        
                                                <i></i>
                                                    </section>
                             
                                               <section class="col col-8">
                                                    <label class="label">Responsable <a style="color: red">*</a></label>
                                                  <label class="input">
                                                            <label class='input' id='responsableid'>
                                                                
                                                                <input type="text" name="responsable" id="responsable" class='input' placeholder="Responsable"   data-requerido="true" />
                                                                
                                                            </label>
                                                       
                                                        </label>
                                                <i></i>
                                                </section >
                                                    


                                                 
                                                    
                                                     


                                                </div>
                            </fieldset>
                        </div>
                           
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelaSalidaEfecutada"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default GuardaSalidaEfectuada" id="GuardaSalidaEfectuada"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="RegistroSalidaEfectuadaJuez-modal" class="modal fade"  tabindex="-1" data-backdrop="static" data-keyboard="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Salida efectuada por juez
                    </h4>
                </div>

                <div class="modal-body">
                    <div id="registrosalidaefectuadaJuez-form" class="smart-form">

                        <div class="modal-body">
                            
                        
                            <fieldset>
                                <div  class="Row">
                                                       
                                         

                                               <section class="col col-8">
                                                        <label class="label">Fundamento <a style="color: red">*</a></label>
                                                        <label class="input">
                                                            <textarea id="fundamento" name="fundamento" cols="60" rows="5"></textarea>                                                            
                                                        </label>                                                        
                                                <i></i>
                                                    </section>
                             
                                               
                                                    


                                                 
                                                    
                                                     


                                                </div>
                            </fieldset>
                        </div>
                           
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancelaSalidaEfecutadajuez"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default GuardaSalidaEfectuada" id="GuardaSalidaEfectuadaJuez"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <input type="hidden" id="CeldaTrackingId"  value="" />
    <input type="hidden" id="hide" />
    <input type="hidden" id="hideid"  />
    <input type="hidden" id="TrabajoSocialId"  />
    <input type="hidden" id="Hidden1" runat="server" value="" />
    <input type="hidden" id="Hidden2" runat="server" value="" />
     <input type="hidden" id="Hidden3" runat="server" value=""/>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
     <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();
            ValidarBoton();

            function ValidarBoton() {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/reporteTrabajoSocial.aspx/ValidarBoton",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            if (resultado.Boton) {

                                $("#BotonTrabajo").show();
                            }
                            else {
                                $("#BotonTrabajo").hide();
                            }
                        }
                        else {
                            ShowError("¡Error! No fue posible validar si mostrar el boton", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            CargarGrid("-1");
            var rutaDefaultServer = "";
            getRutaDefaultServer();

            function getRutaDefaultServer() {                                
                $.ajax({
                    type: "POST",
                    url: "reporteTrabajoSocial.aspx/getRutaServer",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,                    
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                      
                        if (resultado.exitoso) {
                            rutaDefaultServer = resultado.rutaDefault;    
                        }
                    }
                });
            }

            function obtenercirteriosbusqueda() {
                var filtroconsulta = {
                    nombre: $("#nombre").val(),
                    apellidopaterno: $("#paterno").val(),
                    apellidomaterno: $("#materno").val(),
                    rfc: $("#rfc").val(),
                    edad: "",
                    sexo: $("#sexo").val(),
                    ocupacion: $("#ocupacion").val(),
                    nacionalidad: $("#nacionalidad").val(),
                    calle: $("#calle").val(),
                    numero: $("#numero").val(),
                    sector: $("#sector").val(),
                    colonia: $("#colonia").val(),
                    telefono: $("#telefono").val(),
                    alias: $("#alias").val(),
                    estatura: $("#estatura").val(),
                    peso: $("#peso").val()
                }
                return filtroconsulta;

            }
            function CargarGrid(idx) {
                var filtroconsulta = obtenercirteriosbusqueda();
                if (filtroconsulta.nombre == "" && filtroconsulta.apellidopaterno == ""
                    && filtroconsulta.apellidomaterno == "" && filtroconsulta.rfc == ""
                    && filtroconsulta.edad == ""
                    && filtroconsulta.sexo == "0" && filtroconsulta.nacionalidad == "0"
                    && filtroconsulta.calle == "" && filtroconsulta.numero == ""
                    && filtroconsulta.colonia == "0"
                    && filtroconsulta.telefono == "" && filtroconsulta.alias == "0"
                    && filtroconsulta.estatura == "" && filtroconsulta.peso == ""
                ) {
                    ShowAlert("¡Aviso!", "Debe ingresar al menos un criterio de búsqueda.")
                    return;
                }
                filtroconsulta.Idx = idx;
                responsiveHelper_dt_basic = undefined;
                $('#dt_basic').dataTable({
                    destroy: true,
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,

                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    //"scrollY":        "350px",
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic.respond();
                        $('#dt_basic').waitMe('hide');
                    },
                    ajax: {
                        type: "POST",
                        url: "reporteTrabajoSocial.aspx/getdata",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.filtroconsulta = filtroconsulta;
                            return JSON.stringify(parametrosServerSide);
                        }

                    },
                    columns: [
                        {
                            data: "TrackingId",
                            targets: 0,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                return "";
                            }
                        },

                        null,
                        null,
                        {
                            name: "Nombre",
                            data: "Nombre"
                        },
                        {
                            name: "Paterno",
                            data: "Paterno"
                        },
                        {
                            name: "Materno",
                            data: "Materno"
                        },

                        {
                            name: "Expediente",
                            data: "Expediente"
                        },
                        null,
                        null,
                        {
                            name: "NombreCompleto",
                            data: "NombreCompleto",
                            visible: false

                        }
                    ],
                    columnDefs: [

                        {
                            data: "TrackingId",
                            targets: 0,
                            orderable: false,
                            visible: false,
                            render: function (data, type, row, meta) {
                                return "";
                            }
                        },

                        {
                            targets: 1,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },

                        {
                            targets: 2,
                            orderable: false,
                            render: function (data, type, row, meta) {

                                if (row.RutaImagen != "" && row.RutaImagen != null) {
                                    var ext = "." + row.RutaImagen.split('.').pop();
                                    var photo = row.RutaImagen.replace(ext, ".thumb");
                                    var imgAvatar = resolveUrl(photo);
                                    return '<div class="text-center">' +
                                        '<a href="#" class="photoview" data-foto="' + resolveUrl(row.RutaImagen) + '" >' +
                                        '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + imgAvatar + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                        '</a>' +
                                        '<div>';
                                } else {
                                    pathfoto = resolveUrl("/Content/img/avatars/male.png");
                                    return '<div class="text-center">' +
                                        '<img id="avatar2" class="img-thumbnail text-center" alt="" src="' + pathfoto + '" height="10" width="50"  onerror="this.onerror=null;this.src=\'' + rutaDefaultServer + '\';" />' +
                                        '<div>';
                                }
                            }

                        },
                        {
                            targets: 7,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var estatus = "Pendiente";

                                return estatus;
                            }
                        },
                        {
                            targets: 8,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var action2 = "";
                                var motivo = "Motivodetencion";

                                action2 = '&nbsp;<a class="btn btn-success  ' + motivo + '"data-tracking="' + row.TrackingId + '"  id="idexpediente" title="Imprimir"><i class="glyphicon glyphicon-file"></i>&nbsp;<font zize="1">Generar PDF</font></a>';
                                return action2;
                            }
                        }
                    ]
                });
            }

            CargarSexo("");
            function CargarSexo(setsexo) {
                $('#sexo').empty();
                $.ajax({

                    type: "POST",
                    url: "reporteTrabajoSocial.aspx/getSexo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#sexo');

                        Dropdown.append(new Option("[Sexo]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (setsexo != "") {

                            Dropdown.val(setsexo);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡¡Error!", "No fue posible cargar la lista de sexo. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            CargarOcupacion("0");
            function CargarOcupacion(set) {
                $('#ocupacion').empty();
                $.ajax({

                    type: "POST",
                    url: "reporteTrabajoSocial.aspx/getOcupacion",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#ocupacion');

                        Dropdown.append(new Option("[Ocupación]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de ocupaciones. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            CargarNacionalidad("0")
            function CargarNacionalidad(set) {
                $('#nacionalidad').empty();
                $.ajax({

                    type: "POST",
                    url: "reporteTrabajoSocial.aspx/getNacionalidad",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#nacionalidad');

                        Dropdown.append(new Option("[Nacionalidad]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de nacionalidades. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            function ImprimeTrabajoSocial() {
                          startLoading();     
                          var datos = {
                              internoId: $("#hideid").val(),
                              trackingId: $("#hide").val()
                          };
                $.ajax({                    
                    type: "POST",
                    url: "reporteTrabajosocial.aspx/imprimereporte",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'datos': datos
                    }),
                    cache: false,
                    success: function (data) {                                                
                        if (data.d.exitoso) {                                                                                 
                            $("#ctl00_contenido_idCalificacion").val(data.d.Id);
                            $("#ctl00_contenido_trackingIdCalificacion").val(data.d.TrackingId);
                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Se genero correctamente el reporte.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Se genero correctamente el reporte .");

                            var ruta = ResolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                        } else {
                            ShowError("¡Error! Algo salió mal", data.d.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + data.d.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }
            CargarColoniaNacimiento("0", "0");
             function CargarColoniaNacimiento(set, idMunicipio) {                
                $.ajax({

                    type: "POST",
                    url: "reporteTrabajosocial.aspx/getNeighborhoods",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#colonia");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Colonia]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }
            CargarAlias("0", "0");
            function CargarAlias(set, idMunicipio) {                
                $.ajax({

                    type: "POST",
                    url: "reporteTrabajosocial.aspx/getAlias",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        idMunicipio: idMunicipio
                    }),
                    success: function (response) {
                        var Dropdown = $("#alias");
                        Dropdown.empty();
                        Dropdown.append(new Option("[Alias]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));

                        });

                        if (set != "") {

                            Dropdown.val(set);
                            Dropdown.trigger("change.select2");

                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar la lista de países. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

             var table = $('#dt_basic').DataTable();
 
         

            $("body").on("click", ".photoview", function (e) {
                var photo = $(this).attr("data-foto");
                $("#foto_detenido").attr("src", photo);
                $("#photo-arrested").modal("show");
            });
            $("body").on("click", "#idexpediente", function () {
                 var CeldaTrackingId = $(this).attr("data-tracking");
                $("#hide").val(CeldaTrackingId);
                
                ImprimeTrabajoSocial();
                 

            });

            function LimpiarExpediente() {
                $("#motivorehabilitacion").val("");
                       $("#adiccion").val("");
                       $('#txtpandilla').val("");
                       $('#religion').val("");
                       $("#cuadropatologico").val("");
                       $("#observaciones").val("");
                $("#motivorehabilitacion").prop('disabled', false);
                       $("#adiccion").prop('disabled', false);
                       $('#txtpandilla').prop('disabled', false);
                       $('#religion').prop('disabled', false);
                       $("#cuadropatologico").prop('disabled', false);
                   $("#observaciones").prop('disabled', false);
                        $("#guardarexpediente").prop('disabled', false);

            }
               $("body").on("click", "#IdVer", function () {
                 var CeldaTrackingId = $(this).attr("data-tracking");
                   $("#motivorehabilitacion").val($(this).attr("data-motivo"));
                       $("#adiccion").val($(this).attr("data-adiccion"));
                       $('#txtpandilla').val($(this).attr("data-pandilla"));
                       $('#religion').val($(this).attr("data-religion"));
                       $("#cuadropatologico").val($(this).attr("data-cuadropatalogico"));
                   $("#observaciones").val($(this).attr("data-Observacion"));

                       $("#motivorehabilitacion").prop('disabled', true);
                       $("#adiccion").prop('disabled', true);
                       $('#txtpandilla').prop('disabled', true);
                       $('#religion').prop('disabled', true);
                       $("#cuadropatologico").prop('disabled', true);
                   $("#observaciones").prop('disabled', true);
                        $("#guardarexpediente").prop('disabled', true);
                $("#guardarexpediente").hide();
                $("#RegistroExpediente-modal").modal("show");

            });

            $("body").on("click", "#GuardaSalidaEfectuada", function () {

                if (ValidarSalidaEfectuada())
                {
                    guardarsalidaefectuada();

                }


            });

            $("body").on("click", "#buscar", function () {

                 CargarGrid();

            });


          $("body").on("click", "#GuardaSalidaEfectuadaJuez", function () {

                        if (ValidarSalidaEfectuadajuez())
                        {
                            guardarsalidaefectuadajuez();

                        }


                    });

            $("body").on("click", "#guardarexpediente", function () {
                if (validarregistroExpediente())
                {
                    guardarExpedienteTrabajoSocial();

                }
            });
            
            function resolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");

                window.emptytable = true;
                window.table.api().ajax.reload();
            });


            $("body").on("click", ".blockitem", function () {
                var itemnameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#itemnameblock").text(itemnameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockitem-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var id = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "sentence_entrylist.aspx/blockitem",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: id
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                            $("#blockitem-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Bien hecho!</strong>" +
                                "El registro  se actualizó correctamente.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "El registro  se actualizó correctamente.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                        }
                        $('#main').waitMe('hide');
                    }

                });
                window.emptytable = true;
                window.table.api().ajax.reload();
            });
            //JGB 22052019
          

            $("body").on("click", ".edit", function () {
                var nombre = $(this).attr("data-value");
                $("#IdNombreInterno").text( nombre);
               
                $("#trasladoInterno-modal").modal("show")
                $("#guardatraslado").attr("TrackingId", $(this).attr("data-tracking"));
                
                CargarListado(0);
                window.emptytableadd = false;
                window.tableadd.api().ajax.reload();
            });

            $("body").on("click", ".save", function () {
                var tracking = $(this).attr("TrackingId")
                if (validarTraslado()) {
                    guardar(tracking);
                    $("#trasladoInterno-modal").modal("hide");
                }
            });
              //$("body").on("click", ".guardatraslado", function () {
              //  $("#ctl00_contenido_lblMessage").html("");
                

              //      guardar();

            function ValidarSalidaEfectuadajuez() {


                var esvalido = true;
            
          

                if ($("#fundamento").val() == ""|| $('#fundamento').val() == null)
                     {
                      ShowError("Fundamento", "Capture un valor para el campo fundamento para poder continuar");
                    $('#fundamento').parent().removeClass('state-success').addClass("state-error");
                    $('#fundamento').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#fundamento').parent().removeClass("state-error").addClass('state-success');
                    $('#fundamento').addClass('valid');
                }

                return esvalido;

            }

            function ValidarSalidaEfectuada() {


                var esvalido = true;
            
                
                 if ($("#observacion").val() == ""|| $('#observacion').val() == null)
                     {
                      ShowError("Observación", "Capture un valor para el campo observación para poder continuar");
                    $('#observacion').parent().removeClass('state-success').addClass("state-error");
                    $('#observacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#observacion').parent().removeClass("state-error").addClass('state-success');
                    $('#observacion').addClass('valid');
                }

                if ($("#responsable").val() == ""|| $('#responsable').val() == null)
                     {
                      ShowError("Responsable", "Capture un valor para el campo responsable para poder continuar");
                    $('#responsable').parent().removeClass('state-success').addClass("state-error");
                    $('#responsable').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#responsable').parent().removeClass("state-error").addClass('state-success');
                    $('#responsable').addClass('valid');
                }

                return esvalido;

            }

                
            function validarregistroExpediente() {
                var esvalido = true;
                
                if ($("#motivorehabilitacion").val() == "0"||$("#motivorehabilitacion").val() == null) {
                    ShowError("Motivo", "Capture un valor para el campo motivorehabilitacion para poder continuar");
                    $('#motivorehabilitacion').parent().removeClass('state-success').addClass("state-error");
                    $('#motivorehabilitacion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#motivorehabilitacion').parent().removeClass("state-error").addClass('state-success');
                    $('#motivorehabilitacion').addClass('valid');
                }

                 if ($("#adiccion").val() == "0"||$("#adiccion").val()==null) {
                    ShowError("Adicción", "Capture un valor para el campo adicción para poder continuar");
                    $('#adiccion').parent().removeClass('state-success').addClass("state-error");
                    $('#adiccion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#adiccion').parent().removeClass("state-error").addClass('state-success');
                    $('#adiccion').addClass('valid');
                }


                if ($("#txtpandilla").val() == "" || $('#txtpandilla').val() == null) {
                    ShowError("Pandilla", "Capture un valor para el campo pandilla para poder continuar");
                    $('#txtpandilla').parent().removeClass('state-success').addClass("state-error");
                    $('#txtpandilla').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#txtpandilla').parent().removeClass("state-error").addClass('state-success');
                    $('#txtpandilla').addClass('valid');
                }

                  if ($("#religion").val() == "0"||$("#religion").val() == null) {
                    ShowError("Religión", "Capture un valor para el campo religión para poder continuar");
                    $('#religion').parent().removeClass('state-success').addClass("state-error");
                    $('#religion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#religion').parent().removeClass("state-error").addClass('state-success');
                    $('#religion').addClass('valid');
                }

                 if ($("#cuadropatologico").val() == ""|| $('#cuadropatologico').val() == null)
                     {
                      ShowError("Cuadro patológico", "Capture un valor para el campo cuadro patológico para poder continuar");
                    $('#cuadropatologico').parent().removeClass('state-success').addClass("state-error");
                    $('#cuadropatologico').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#cuadropatologico').parent().removeClass("state-error").addClass('state-success');
                    $('#cuadropatologico').addClass('valid');
                     }

           

                return esvalido;
            }


            $("body").on("click", ".add", function () {
                $("#ingresocelda-modal").modal("show");

                window.emptytableadd = false;
                window.tableadd.api().ajax.reload();

            });


            $("body").on("click", ".calculate", function () {
                $("#ctl00_contenido_fecha").val("");
                $("#ctl00_contenido_fechac").val("");
                $("#ctl00_contenido_anoss").val("0");
                $("#ctl00_contenido_mesess").val("0");
                $("#ctl00_contenido_diass").val("0");
                $("#ctl00_contenido_anosa").val("0");
                $("#ctl00_contenido_mesesa").val("0");
                $("#ctl00_contenido_diasa").val("0");
                $("#ctl00_contenido_diasc").val("");
                $("#ctl00_contenido_fechac").val("");
                $("#ctl00_contenido_porcentaje").val("");
                limpiar();
                
                
                $("#calculate-modal").modal("show");

            });


            $("body").on("click", ".calculatebtn", function () {

                if (validar()) {


                    limpiar();
                    $("#ctl00_contenido_fechac").val("");
                    $("#ctl00_contenido_diasc").val("");
                    $("#ctl00_contenido_porcentaje").val("");
                    var str = $("#ctl00_contenido_fecha").val();

                    if (/^\d{2}\/\d{2}\/\d{4}$/i.test(str)) {

                        var parts = str.split("/");

                        var day = parts[0] && parseInt(parts[0], 10);
                        var month = parts[1] && parseInt(parts[1], 10);
                        var year = parts[2] && parseInt(parts[2], 10);

                        if (day < 10) {
                            day = '0' + day
                        }
                        if (month < 10) {
                            month = '0' + month
                        }
                        var startdate = year + "/" + month + "/" + day;
                        var durationyear = parseInt($("#ctl00_contenido_anoss").val(), 10) - parseInt($("#ctl00_contenido_anosa").val(), 10);
                        var durationmonth = parseInt($("#ctl00_contenido_mesess").val(), 10) - parseInt($("#ctl00_contenido_mesesa").val(), 10);
                        var durationday = parseInt($("#ctl00_contenido_diass").val(), 10) - parseInt($("#ctl00_contenido_diasa").val(), 10);

                        if (day <= 31 && day >= 1 && month <= 12 && month >= 1) {

                            var expiryDate = new Date(year, month - 1, day);

                            expiryDate.setDate(expiryDate.getDate() + durationday);
                            expiryDate.setMonth(expiryDate.getMonth() + durationmonth);
                            expiryDate.setFullYear(expiryDate.getFullYear() + durationyear);
                            //expiryDate.setMonth(expiryDate.getMonth(), durationmonth);


                            var day = ('0' + expiryDate.getDate()).slice(-2);
                            var month = ('0' + (expiryDate.getMonth() + 1)).slice(-2);

                            var year = expiryDate.getFullYear();

                            var finaldate = year + "/" + month + "/" + day;
                            


                            // $('#ctl00_contenido_diasc').val(expiryDate.getDay());
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();

                            if (dd < 10) {
                                dd = '0' + dd
                            }

                            if (mm < 10) {
                                mm = '0' + mm
                            }

                            today = yyyy + '/' + mm + '/' + dd;

                            var compurgados = moment(today).diff(moment(startdate), 'days');

                            var total = moment(finaldate).diff(moment(startdate), 'days');

                            $("#ctl00_contenido_fechac").val(day + "/" + month + "/" + year);
                            if (compurgados >= 0) {
                                var porcentaje = parseFloat(Math.round(100 * compurgados) / total).toFixed(2);


                                if (total > 0) {
                                    $('#ctl00_contenido_diasc').val(compurgados);
                                    $('#ctl00_contenido_porcentaje').val(porcentaje);
                                }
                                else {
                                    ShowError("¡Error!", "La fecha a partir debe ser mayor a la fecha cumplimiento de sentencia.");
                                }
                            }
                            else {
                                ShowError("¡Error!", "La fecha a partir debe ser mayor a la fecha cumplimiento de sentencia.");
                            }

                        } else {
                            ShowError("¡Error!", "Algo salió mal y no fue posible afectar el estatus del registro. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                        }
                    }
                }


            });

            function validar() {

                var esvalido = true;

                if ($("#ctl00_contenido_fecha").val() == null || $("#ctl00_contenido_fecha").val().split(" ").join("") == "") {
                    ShowError("Fecha a partir", "La fecha a partir es obligatoria.");
                    $('#ctl00_contenido_fecha').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_fecha').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_fecha').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_fecha').addClass('valid');
                }

                if ($("#ctl00_contenido_anoss").val() == null || $("#ctl00_contenido_anoss").val().split(" ").join("") == "") {
                    ShowError("Sentenca años", "Los años son obligatorios.");
                    $('#ctl00_contenido_anoss').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_anoss').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_anoss').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_anoss').addClass('valid');
                }

                if ($("#ctl00_contenido_mesess").val() == null || $("#ctl00_contenido_mesess").val().split(" ").join("") == "") {
                    ShowError("Sentenca meses", "Los meses son obligatorios.");;
                    $('#ctl00_contenido_mesess').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_mesess').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_mesess').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_mesess').addClass('valid');
                }

                if ($("#ctl00_contenido_diass").val() == null || $("#ctl00_contenido_diass").val().split(" ").join("") == "") {
                    ShowError("Sentenca días", "Los días son obligatorios.");
                    $('#ctl00_contenido_diass').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_diass').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_diass').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_diass').addClass('valid');
                }

                if ($("#ctl00_contenido_anosa").val() == null || $("#ctl00_contenido_anosa").val().split(" ").join("") == "") {
                    ShowError("Abono años", "Los años son obligatorios.");
                    $('#ctl00_contenido_anosa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_anosa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_anosa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_anosa').addClass('valid');
                }

                if ($("#ctl00_contenido_mesesa").val() == null || $("#ctl00_contenido_mesesa").val().split(" ").join("") == "") {
                    ShowError("Abono meses", "Los meses son obligatorios.");;
                    $('#ctl00_contenido_mesesa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_mesesa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_mesesa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_mesesa').addClass('valid');
                }

                if ($("#ctl00_contenido_diasa").val() == null || $("#ctl00_contenido_diasa").val().split(" ").join("") == "") {
                    ShowError("Abono días", "Los días son obligatorios.");
                    $('#ctl00_contenido_diasa').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_diasa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_diasa').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_diasa').addClass('valid');
                }

                return esvalido;

            }

            function limpiar() {
                $('#ctl00_contenido_fecha').parent().removeClass('state-success');
                $('#ctl00_contenido_fecha').parent().removeClass("state-error");
                $('#ctl00_contenido_anoss').parent().removeClass('state-success');
                $('#ctl00_contenido_anoss').parent().removeClass("state-error");
                $('#ctl00_contenido_mesess').parent().removeClass('state-success');
                $('#ctl00_contenido_mesess').parent().removeClass("state-error");
                $('#ctl00_contenido_diass').parent().removeClass('state-success');
                $('#ctl00_contenido_diass').parent().removeClass("state-error");
                $('#ctl00_contenido_anosa').parent().removeClass('state-success');
                $('#ctl00_contenido_anosa').parent().removeClass("state-error");
                $('#ctl00_contenido_mesesa').parent().removeClass('state-success');
                $('#ctl00_contenido_mesesa').parent().removeClass("state-error");
                $('#ctl00_contenido_diasa').parent().removeClass('state-success');
                $('#ctl00_contenido_diasa').parent().removeClass("state-error");
            }

            var breakpointAddDefinition = {
                desktop: Infinity,
                tablet: 1024,
                fablet: 768,
                phone: 480
            };

            $("body").on("click", ".printpdf", function () {
                LimpiarModalReporte();

                var nombre = $(this).attr("data-nombre");
                $("#nombrereportepdf").val(nombre);
                var rutaavatar = $(this).attr("data-avatar");
                var imagenAvatar = ResolveUrl(rutaavatar.trim());
                $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                var tracking = $(this).attr("data-tracking");
                $("#trackingidpdf").val(tracking);

                var alias = $(this).attr("data-alias");
                $("#aliasreportepdf").val(alias);

                var fechanacimiento = $(this).attr("data-fn");
                var today = new Date();
                var edad = moment(today).diff(moment(fechanacimiento), 'years');
                if (edad > 0) {
                    $("#edadreportepdf").val(edad);
                }

           

                $("#printpdf-modal").modal("show");
            });

            function LimpiarModalReporte() {
                $('#edadreportepdf').parent().removeClass('state-success');
                $('#edadreportepdf').parent().removeClass("state-error");
                $('#aliasreportepdf').parent().removeClass('state-success');
                $('#aliasreportepdf').parent().removeClass("state-error");
                $('#edadreportepdf').val("");
                $('#aliasreportepdf').val("");
                $('#nombrereportepdf').val("");
                $('#violentopdf').val("");
                $('#enfermopdf').val("");
                $('#comentariospdf').val("");
                $('#trackingidpdf').val("");
                $("#radioBuscandopdf").prop("checked", false);
                $("#radioExtraviadopdf").prop("checked", false);
                $("#radioInformacionpdf").prop("checked", false);
                $('#institucionpdf').val("");
                $('#direccionpdf').val("");
                $('#telefonopdf').val("");
                $('#senaspdf').val("");

            }
                function validarTraslado() {
                var esvalido = true;

                if ($("#ctl00_contenido_dropdown").val() == "0") {
                    ShowError("Delegación", "Capture un valor para el campo delegación para poder continuar");
                    $('#ctl00_contenido_dropdown').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_dropdown').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_dropdown').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_dropdown').addClass('valid');
                }
                return esvalido;
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            $("body").on("click", ".printpdfbtn", function () {
                if (validareporteextravio()) {
                    generareporteextravio();
                }
            });

               function obtenerevaloresSalidaefectuadajuez() {
                var salidaefectuadajuez = {
                    fundamento: $("#fundamento").val(),
                    internoId: $("#hideid").val(),
                    trackingId: $("#hide").val(),
                    TrabajoSocialId: $("#TrabajoSocialId").val()

                }
                return salidaefectuadajuez;

            }
            function limpiarDatoSalidaEfectuadajuez()
            {
                
                $("#fundamento").val("");
                $("#hideid").val("");
                $("#hide").val("");
                $('#fundamento').parent().removeClass('state-success');
                $('#fundamento').parent().removeClass("state-error");
                $
            }
            function guardarsalidaefectuadajuez()
            {
                var salidaefectuadajuez = obtenerevaloresSalidaefectuadajuez();
              
                 $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/guardaSalidaEfectuadaJuez",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'salidaefectuadajuez': salidaefectuadajuez }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!",   "La información se registro correctamente" );                            
                           window.emptytable = false;
                        window.table.api().ajax.reload();
                           // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            $('#main').waitMe('hide');
                            limpiarDatoSalidaEfectuadajuez();
                           $("#RegistroSalidaEfectuadaJuez-modal").modal("hide");
                           
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }




            function obtenerevaloresSalidaefectuada() {
                var salidaefectuada = {
                    observacion: $("#observacion").val(),
                    responsable: $("#responsable").val(),
                    internoId: $("#hideid").val(),
                    trackingId: $("#hide").val(),
                    TrabajoSocialId: $("#TrabajoSocialId").val()
                }
                return salidaefectuada;

            }
            function limpiarDatoSalidaEfectuada()
            {
                $("#observacion").val("");
                $("#responsable").val("");
                $("#hideid").val("");
                $("#hide").val();
                $('#observacion').parent().removeClass('state-success');
                $('#observacion').parent().removeClass("state-error");
                $('#responsable').parent().removeClass('state-success');
                $('#responsable').parent().removeClass("state-error");
            }
            function guardarsalidaefectuada()
            {
                var salidaefectuada = obtenerevaloresSalidaefectuada();

                 $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/guardaSalidaEfectuada",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'salidaefectuada': salidaefectuada }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!",   "La información se registro correctamente" );                            
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                           // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            $('#main').waitMe('hide');
                            $("#RegistroSalidaEfectuada-modal").modal("hide");
                            limpiarDatoSalidaEfectuada();
                           
                           
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ObtenerValoresexpedienteTrabajoSocial() {                                
                var expedienteTrabajoSocial = {
                    motivorehabilitacionId:$("#motivorehabilitacion").val(),
                    adiccionId: $("#adiccion").val(),
                    pandilla:$('#txtpandilla').val(),
                    religionId: $('#religion').val(),
                    cuadropatologico:$("#cuadropatologico").val(),
                    observaciones: $("#observaciones").val(),
                   
                    TrackingId: $("#hide").val()
                    
                };
                return expedienteTrabajoSocial;
            }


            function guardarExpedienteTrabajoSocial() {
                startLoading();
                var expedienteTrabajoSocial = ObtenerValoresexpedienteTrabajoSocial();

                //var param = RequestQueryString("tracking");
                //if (param != undefined) {
                //    MovimientoCelda.TrackingId = param;
                //}
               
                $.ajax({
                    type: "POST",
                    url: "Trabajosocial.aspx/ExpedienteTrabajoSocial",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'expedienteTrabajoSocial': expedienteTrabajoSocial }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                " " + resultado.mensaje, "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!",   "La información se registro correctamente" );                            
                            window.emptytable = false;
                            window.table.api().ajax.reload();
                            LimpiarExpediente();
                            // Response.redirect("estado.aspx");
                            //var url = "estado.aspx"; 
                            $('#main').waitMe('hide');
                            $("#RegistroExpediente-modal").modal("hide");
                           
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }



             $("body").on("click", ".datos", function () {
                var id = $(this).attr("data-id");
                CargarDatosPersonales(id);
                
            });

              $('#dt_basic tbody').on( 'click', 'tr', function () {

                  $("input[type=checkbox]:checked").each(function () {
                      $(this).val() = null;
                  });
                
             } );


            $("body").on("click", "#imprimir", function () {
                var valor = "";
                var contador = 0;
                var trackingId = "";
                 $("input[type=checkbox]:checked").each(function () {
                     trackingId
                     if ($(this).val() != "on" && $(this).val() != "null") {
                         contador += 1;
                         valor = $(this).attr('data-internoid');
                         trackingId=$(this).attr('data-TrackingId');
                     }
                     

                });


                if (valor == "")
                {
                    ShowAlert("Alerta", "selecione un registro");
                    return;
                }
               
                if (contador > 1)
                {
                    ShowAlert("Alerta","Seleccione solo un registro");
                    return;
                }

                $("#hideid").val(valor);
                $("#hide").val(trackingId);
                ImprimeTrabajoSocial();
                

            });


            function ImprimeTrabajoSocial() {
                startLoading();
                var datos = {
                    internoId: $("#hideid").val(),
                    trackingId: $("#hide").val()
                };
                $.ajax({
                    type: "POST",
                    url: "reporteTrabajosocial.aspx/imprimereporte",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        'datos': datos
                    }),
                    cache: false,
                    success: function (data) {
                        if (data.d.exitoso) {
                            $("#ctl00_contenido_idCalificacion").val(data.d.Id);
                            $("#ctl00_contenido_trackingIdCalificacion").val(data.d.TrackingId);

                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "Se genero correctamente el reporte.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Se genero correctamente el reporte .");

                            var ruta = ResolveUrl(data.d.Ubicacion);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }

                        } else {
                            ShowError("¡Error! Algo salió mal", data.d.mensaje + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + data.d.mensaje);
                        $('#main').waitMe('hide');
                    }
                });
            }

            

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
              if (url.indexOf("~/") == 0) {
                  url = baseUrl + url.substring(2);
              }
              return url;
            }
            $("body").on("click", "#edtitarcuerpo", function () {

                $("#cuerpo").prop("disabled",false);
            });
            $("body").on("click", "#salidaefectuada", function () {
                var valor = "";
                var contador = 0;
                var trackingId = "";
                var TrabajoSocialId = "";
                 $("input[type=checkbox]:checked").each(function () {
                     trackingId
                     if ($(this).val() != "on" && $(this).val() != "null") {
                         contador += 1;
                         valor = $(this).attr('data-internoid');
                         trackingId = $(this).attr('data-TrackingId');
                         TrabajoSocialId=$(this).attr('data-trabajoSocialId');
                     }
                     

                });


                if (valor == "")
                {
                    ShowAlert("Alerta", "selecione un registro");
                    return;
                }
               
                if (contador > 1)
                {
                    ShowAlert("Alerta","Seleccione solo un registro");
                    return;
                }
                $("#TrabajoSocialId").val(TrabajoSocialId);
                $("#hideid").val(valor);
                $("#hide").val(trackingId);
                $("#RegistroSalidaEfectuada-modal").modal("show");

            });


            $("body").on("click", "#salidaefectuadajuez", function () {
                var valor = "";
                var contador = 0;
                var trackingId = "";
                 $("input[type=checkbox]:checked").each(function () {
                     trackingId
                     if ($(this).val() != "on" && $(this).val() != "null") {
                         contador += 1;
                         valor = $(this).attr('data-internoid');
                         trackingId=$(this).attr('data-TrackingId');
                     }
                     

                });


                if (valor == "")
                {
                    ShowAlert("Alerta", "selecione un registro");
                    return;
                }
               
                if (contador > 1)
                {
                    ShowAlert("Alerta","Seleccione solo un registro");
                    return;
                }

                $("#hideid").val(valor);
                $("#hide").val(trackingId);
                $("#RegistroSalidaEfectuadaJuez-modal").modal("show");

            });

            function CargarDatosPersonales(trackingid) {
                startLoading();
                  $.ajax({
                      type: "POST",
                      url: "sentence_entrylist.aspx/getdatos",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      data: JSON.stringify({
                          trackingid: trackingid,
                      }),
                      cache: false,
                      success: function (data) {
                          var resultado = JSON.parse(data.d);
                          if (resultado.exitoso) {
                          
                              $('#nombreInterno').val(resultado.obj.Nombre);
                              $('#edad').val(resultado.obj.Edad);
                              $('#situacion').val(resultado.obj.Situacion);
                              var imagenAvatar = ResolveUrl(resultado.obj.RutaImagen);
                              $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                              $('#horaRegistro').val(resultado.obj.Registro);
                              $('#salida').val(resultado.obj.Salida);
                              $('#domicilio').val(resultado.obj.Domicilio);
                                  $("#datospersonales-modal").modal("show");
                          } else {
                              ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");

                          }

                          $('#main').waitMe('hide');

                      },
                      error: function () {
                          $('#main').waitMe('hide');
                          ShowError("¡Error!", "No fue posible cargar la información de  interno. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                      }
                  });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>

