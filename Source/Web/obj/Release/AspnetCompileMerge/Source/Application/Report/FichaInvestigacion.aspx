﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="FichaInvestigacion.aspx.cs" Inherits="Web.Application.Report.FichaInvestigacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
        <li>Barandilla</li>
    <li>Ficha de investigación</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">  
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-search"></i>&nbsp;Ficha de investigación
            </h1>
        </div>
    </div>


    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="scroll">
    <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-remisiones-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-togglebutton="false">
                <header>
                  <span class="widget-icon"><i class="fa fa-search"></i></span>
       
                    <h2> Ficha de investigación</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body">
                        <div id="smart-form-register-entry" class="smart-form">
                        
                            <fieldset>
                               
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Estado</label>
                                        <label class="select">
                                            <select name="Estado" id="Estado">
                                            </select>
                                            
                                        </label>
                                    </section>
                                   
                                    <section class="col col-4">
                                        <label class="label">Municipio</label>
                                        <label class="select">
                                            <select name="Municipio" id="Municipio">
                                            </select>
                                            
                                        </label>
                                    </section>
                                   
                                    <section class="col col-4">
                                        <label class="label">Barandilla</label>
                                        <label class="select">
                                            <select name="Barandilla" id="Barandilla">
                                            </select>
                                            
                                        </label>
                                    </section>
                                   <br />
                                    </div>
                                
                                </fieldset>
                            </div>
                        <table id="dt_basic_recibos" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>No. remisión</th>
                                        <th>Nombre</th>
                                        <th>Edad</th>
                                        <th>Evento</th>
                                        <th>Detención</th>
                                        <th>Motivo</th>
                                        <th>Unidad</th>
                                        <th>Responsable</th>
                                        <th>Situación</th>
                                        <th>Horas</th>
                                        <th>Barandilla</th>
                                        <th>Examen médico</th>
                                        <th>Indicaciones médicas</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
                <footer>
                </footer>
                </div>
        </article>
        </div>
        </section>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="hidcliente" />
    <input type="hidden" id="hidcontrato" value="0" />
    <input type="hidden" id="hidsubcontrato" value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.es.js""></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();

            var responsiveHelper_dt_basic_recibos = undefined;
            var responsiveHelper_datatable_fixed_column_recibos = undefined;
            var responsiveHelper_datatable_col_reorder_recibos = undefined;
            var responsiveHelper_datatable_tabletools_recibos = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            init();
            function init() {
                loadCustomers("0");
                //GetContracts("0");
                //GetsubContracts("0");
                cargarTabla();
            }

            $("body").on("click", ".reporte", function () {
                $("#ctl00_contenido_lblMessage").html("");
                pdf();
            });

            $("#Barandilla").change(function () {
                $("#hidsubcontrato").val($("#Barandilla").val());
                if ($("#Barandilla").val() !== "0") {
                    cargarTabla();
                }
            });

            $("#Municipio").change(function () {
                $("#hidcontrato").val($("#Municipio").val());
                var value = $("#hidsubcontrato").val();
                $("#hidsubcontrato").val("0");
                GetsubContracts(value);
                if ($("#Municipio").val() !== "0") {
                    cargarTabla();
                }
            });

            function pdf(tracking) {

                $.ajax({

                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/FichaInvestigacion.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({

                        tracking: tracking
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            open(resultado.ubicacionarchivo.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.message + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.message);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            $("#Estado").change(function () {
                $("#hidcliente").val($("#Estado").val());
                var value = $("#hidcontrato").val();
                GetContracts("0");
                $("#hidcontrato").val("0");
                $("#hidsubcontrato").val("0");
                GetsubContracts("0");
                if ($("#Estado").val() !== "0") {
                    cargarTabla();
                }
            });

            function cargarTabla() {
                responsiveHelper_dt_basic_recibos = undefined;
                $('#dt_basic_recibos').dataTable({
                    destroy: true,
                    "lengthMenu": [10, 20, 50, 100],
                    iDisplayLength: 10,
                    serverSide: true,
                    fixedColumns: true,
                    autoWidth: true,
                    //"scrollY":        "350px",
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "oLanguage": {
                        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    "preDrawCallback": function () {
                        if (!responsiveHelper_dt_basic_recibos) {
                            responsiveHelper_dt_basic_recibos = new ResponsiveDatatablesHelper($('#dt_basic_recibos'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_dt_basic_recibos.createExpandIcon(nRow);
                    },
                    "drawCallback": function (oSettings) {
                        responsiveHelper_dt_basic_recibos.respond();
                        $('#dt_basic_recibos').waitMe('hide');
                    },
                    ajax: {
                        url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/FichaInvestigacion.aspx/getDetenidos",
                        method: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: function (d) {
                            $('#dt_basic_recibos').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });
                            d.emptytable = false;
                            d.clienteid = $("#hidcliente").val() || "";
                            d.contratoid = $("#hidcontrato").val() || "";
                            d.subcontratoid = $("#hidsubcontrato").val() || "";
                            d.pages = $('#dt_basic_recibos').DataTable().page.info().page || "";
                            return JSON.stringify(d);
                        },
                        dataSrc: "data",
                        dataFilter: function (data) {
                            var json = jQuery.parseJSON(data);
                            json.recordsTotal = json.d.recordsTotal;
                            json.recordsFiltered = json.d.recordsFiltered;
                            json.data = json.d.data;
                            return JSON.stringify(json);
                        }
                    },
                    <%--ajax: {
                        type: "POST",
                        url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/FichaInvestigacion.aspx/getDetenidos",
                        contentType: "application/json; charset=utf-8",
                        data: function (parametrosServerSide) {
                            $('#dt_basic_recibos').waitMe({
                                effect: 'bounce',
                                text: 'Cargando...',
                                bg: 'rgba(255,255,255,0.7)',
                                color: '#000',
                                sizeW: '',
                                sizeH: '',
                                source: ''
                            });

                            parametrosServerSide.emptytable = false;
                            parametrosServerSide.clienteid = $("#hidcliente").val() || "";
                            parametrosServerSide.contratoid = $("#hidcontrato").val() || "";
                            parametrosServerSide.subcontratoid = $("#hidsubcontrato").val() || "";
                            return JSON.stringify(parametrosServerSide);
                        }
                    },--%>
                    columns: [
                        {
                            name: "Expediente",
                            data: "Expediente"
                        },
                        {
                            name: "Nombre",
                            data: "Nombre"
                        },
                        null,
                        {
                            name: "DescripcionEvento",
                            data: "DescripcionEvento"
                        },
                        {
                            name: "FechaAux",
                            data: "FechaAux"
                        },
                        {
                            name: "Motivo",
                            data: "Motivo"
                        },
                        {
                            name: "Unidad",
                            data: "Unidad"
                        },
                        {
                            name: "Responsable",
                            data: "Responsable"
                        },
                        {
                            name: "Situacion",
                            data: "Situacion"
                        },
                        {
                            name: "TotalHoras",
                            data: "TotalHoras"
                        },
                        {
                            name: "Subcontrato",
                            data: "Subcontrato"
                        },
                        {
                            name: "FechasExamen",
                            data: "FechasExamen",
                            orderable: false
                        },
                        {
                            name: "IndicacionesExamen",
                            data: "IndicacionesExamen",
                            orderable: false
                        },
                        null
                    ],
                    columnDefs: [
                        {
                            targets: 2,
                            render: function (data, type, row, meta) {
                                if (row.Edad === 0) {
                                    return '';
                                }
                                else {
                                    return row.Edad;
                                }
                            }
                        },
                        {
                            targets: 13,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                var txtestatus = "";
                                var icon = "";
                                var color = "";
                                var printPDF = "edit";
                                var imprimirPDF = '<a class="btn btn-primary pdf" id="imprimir" data-id="' + row.TrackingIdDetalle + '" title="Generar PDF"><i class="glyphicon glyphicon-file"></i> Generar PDF</a>&nbsp;';

                                return imprimirPDF;
                            }
                        }
                    ]
                });

                dtable = $("#dt_basic_recibos").dataTable().api();

                $("#dt_basic_recibos_filter input[type='search']")
                    .unbind()
                    .bind("input", function (e) {

                        if (this.value == "") {
                            dtable.search("").draw();
                        }
                        return;
                    });

                $("#dt_basic_recibos_filter input[type='search']").keypress(function (e) {
                    if (e.charCode === 13) {
                        dtable.search($("#dt_basic_recibos_filter input[type='search']").val()).draw();
                        e.preventDefault();
                    }
                });
            };
            $("body").on("click", ".pdf", function () {
                var id = $(this).attr("data-id");
                pdf(id);
            });
            function loadCustomers(setvalue) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/FichaInvestigacion.aspx/getCustomers",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#Estado');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));
                        var i = 0;
                        var id = 0;
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                            id = item.Id;
                            i++;
                        });

                        if (i < 2) {
                            setvalue = id;
                            $("#hidcliente").val(id);
                            GetContracts("0");
                        }

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select");
                        }


                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clientes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }


            function GetsubContracts(setvalue) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/FichaInvestigacion.aspx/GetSubcontracts",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Contratoid: $("#hidcontrato").val() }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#Barandilla');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));
                        var id = 0;
                        var i = 0;
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                            i++;
                            id = item.Id;
                        });

                        if (i < 2) {
                            setvalue = id;
                            $("#hidsubcontrato").val(id);                            
                        }
                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }

                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de subcontratos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            function GetContracts(setvalue) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/FichaInvestigacion.aspx/GetContracts",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ Clienteid: $("#hidcliente").val() }),
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#Municipio');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Todos]", "0"));
                        var i = 0;
                        var id = 0;
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                            id = item.Id;
                            i++;
                        });

                        if (i < 2) {
                            setvalue = id;
                            $("#hidcontrato").val(id)
                            GetsubContracts("0");
                        }
                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }

                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de contratos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
             }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>