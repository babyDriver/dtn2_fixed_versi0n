﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="movimiento_caja.aspx.cs" Inherits="Web.Application.Report.movimiento_caja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
    <li>Caja</li>
    <li>Movimientos de caja</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
        #content {
            height: 600px;
        }
    </style>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-file-text-o fa-fw "></i>
                Recibos de movimientos de caja
            </h1>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>    
    <p></p>
    <section id="widget-grid" class="">
        <div class="row">            
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-ticket"></i></span>
                        <h2>Movimientos de caja </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body">
                            <div class="row">
                                    <section class="col col-md-12" id="BotonCaja">
                                        <a class="btn btn-md btn-default " href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Caja/caja.aspx"" style="padding:5px">
                                            <i class="fa fa-mail-reply"></i> Regresar a caja</a>
                                    </section>
                                </div>
                            <table id="dt_basic_cortes" class="table table-striped table-bordered table-hover" width="100%" >
                                <thead>
                                    <tr>                                             
                                        <th data-class="expand">#</th>
                                        <th>Inicio del corte</th>
                                        <th>Fin del corte</th>
                                        <th>Usuario</th>                                                        
                                        <th data-hide="phone,tablet">Acciones</th>
                                    </tr>
                                </thead>                               
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {       
            
            //Variables tabla de cortes
            var responsiveHelper_dt_basic_cortes = undefined;
            var responsiveHelper_datatable_fixed_column_cortes = undefined;
            var responsiveHelper_datatable_col_reorder_cortes = undefined;
            var responsiveHelper_datatable_tabletools_cortes = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            ValidarBoton();

            function ValidarBoton() {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/corte_caja.aspx/ValidarBoton",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            if (resultado.Boton) {

                                $("#BotonCaja").show();
                            }
                            else {
                                $("#BotonCaja").hide();
                            }
                        }
                        else {
                            ShowError("¡Error! No fue posible validar si mostrar el boton", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            function ResolveUrl(url) {
                var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }            

            $('#dt_basic_cortes').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                //"scrollY":        "350px",
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic_cortes) {
                        responsiveHelper_dt_basic_cortes = new ResponsiveDatatablesHelper($('#dt_basic_cortes'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic_cortes.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic_cortes.respond();
                    $('#dt_basic_cortes').waitMe('hide');
                },

                //"createdRow": function (row, data, index) {
                //    if (!data["Habilitado"]) {
                //        $('td', row).eq(1).addClass('strikeout');
                //        $('td', row).eq(2).addClass('strikeout');
                //        $('td', row).eq(3).addClass('strikeout');

                //    }
                //},
                ajax: {
                    type: "POST",
                    url: "movimiento_caja.aspx/getCortes",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        
                        parametrosServerSide.emptytable = false;    
                        return JSON.stringify(parametrosServerSide);
                    }
                },
                columns: [
                    {
                        name: "Id",
                        data: "Id"
                    },                  
                    {
                        name: "FechaInicio",
                        data: "FechaInicio"
                    },
                    {
                        name: "FechaFin",
                        data: "FechaFin"
                    },
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto"
                    },                    
                    null
                ],
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },                    
                    {
                        targets: 4,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var edit = "imprimirCorte";
                            var registro = '<a class="btn btn-success btn-sm ' + edit + '" href="javascript:void(0);" data-tracking="' + row.TrackingId + '" title = "Imprimir" ><i class="glyphicon glyphicon-file"></i>&nbsp;&nbsp;Generar PDF</a>&nbsp; ';                        

                            return registro;
                        }
                    }
                ]
            });

            $("body").on("click", ".imprimirCorte", function () {
                var tracking = $(this).attr("data-tracking");
                generarReciboCorte(tracking);
            });

            function generarReciboCorte(datos) {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "movimiento_caja.aspx/generarReciboCorteDeCaja",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        tracking: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);                        
                        if (resultado.success) {
                            var ruta = ResolveUrl(resultado.ubicacionArchivo);
                            if (ruta != "") {
                                window.open(ruta, '_blank');
                            }                    
                            $('#main').waitMe('hide');
                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "El recibo se " + resultado.message + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "El recibo se " + resultado.message + " correctamente.");                            
                        } else {
                            ShowError("¡Error! No fue posible guardar el pago de la multa", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
