﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="corte_detenidos.aspx.cs" Inherits="Web.Application.Report.corte_detenidos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Reportes</li>
    <li>Barandilla</li>
    <li>Corte de detenidos</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">   
    <div class="scroll">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-file-text-o"></i>
                    Corte de detenidos
                </h1>
            </div>
        </div>
        <div class="row">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                    <div class="jarviswidget" id="wid-corte-detenidos-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-search"></i></span>
                            <h2>Buscar</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                        </div>
                    </div>
                </article>
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-corte-detenidos-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-file-text-o"></i></span>
                            <h2>Corte de detenidos</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-md-12">
                                            <a class="btn btn-success reporte"><i class="fa fa-file"></i>&nbsp;Generar PDF</a>
                                            <a id="BotonBarandilla" class="btn btn-md btn-default" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Registry/entrylist.aspx" style="padding:5px"><i class="fa fa-mail-reply"></i> Regresar a barandilla</a>
                                        </section>
                                    </div>
                                </fieldset>
                                <br />
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No. remisión</th>
                                            <th data-hide="phone,tablet">Evento</th>
                                            <th>Nombre</th>
                                            <th data-hide="phone,tablet">Edad</th>
                                            <th data-hide="phone,tablet">Ingreso</th>
                                            <th data-hide="phone,tablet">Calificación</th>
                                            <th data-hide="phone,tablet">Motivo detención</th>
                                            <th data-hide="phone,tablet">Celda</th>
                                            <th data-hide="phone,tablet">Cumple</th>
                                            <th data-hide="phone,tablet" id="alerta1">Fecha UNIPOL</th>
                                            <th data-hide="phone,tablet" id="alerta2">Folio UNIPOL</th>
                                            <th data-hide="phone,tablet" id="alerta3">Motivo UNIPOL</th>
                                            <th data-hide="phone,tablet">Detención</th>
                                            <th data-hide="phone,tablet">Unidad</th>
                                            <th data-hide="phone,tablet">Responsable unidad</th>
                                            <th data-hide="phone,tablet">Domicilio detención</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/Utilities.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();

            var responsiveHelper_dt_basic = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };
            function ValidarBoton() {
                startLoading();

                $.ajax({
                    type: "POST",
                    url: "<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Report/autorizacionSalida.aspx/ValidarBoton",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            if (resultado.Boton) {
                                $("#BotonBarandilla").show();
                            }
                            else {
                                $("#BotonBarandilla").hide();
                            }
                        }
                        else {
                            ShowError("¡Error! No fue posible validar si mostrar el boton", resultado.message + ". Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        ShowError("¡Error! No fue posible guardar el ingreso", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.message);
                        $('#main').waitMe('hide');
                    }
                });
            }
            ValidarBoton();
            window.emptytable = false;

            getalerta();
            function getalerta() {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "corte_detenidos.aspx/GetAlertaWeb",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (data) {
                        if (data.d != null) {
                            data = data.d;

                            var alerta1 = data.Denominacion;
                            var alerta2 = data.AlertaWerb;

                            //$("#Denominacin").val(data.Denominacion);

                            //$("#linkEvento").html('<i class="glyphicon glyphicon-folder-open"></i>&nbsp; ' + alerta1 + ' / evento');
                            $("#alerta1").html('Fecha ' + alerta2);
                            $("#alerta2").html('Folio ' + alerta2);
                            $("#alerta3").html('Motivo ' + alerta2);
                            //$("#alerta2").html('<i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>' + alerta1);
                            //$("#alerta3").html(' <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i> Detalle de ' + alerta2);
                        }
                        $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información de alerta web. Si el problema persiste, contacte al personal de soporte técnico.");
                    }
                });
            }

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {

                },
                ajax: {
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/corte_detenidos.aspx/getcortedetenidos",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: function (d) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        d.emptytable = false;
                        d.pages = $('#dt_basic').DataTable().page.info().page || "";
                        return JSON.stringify(d);
                    },
                    dataSrc: "data",
                    dataFilter: function (data) {
                        var json = jQuery.parseJSON(data);
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        return JSON.stringify(json);
                    }
                },
                //ajax: {
                //    type: "POST",
                //    url: "corte_detenidos.aspx/getcortedetenidos",
                //    contentType: "application/json; charset=utf-8",
                //    data: function (parametrosServerSide) {
                //        $('#dt_basic').waitMe({
                //            effect: 'bounce',
                //            text: 'Cargando...',
                //            bg: 'rgba(255,255,255,0.7)',
                //            color: '#000',
                //            sizeW: '',
                //            sizeH: '',
                //            source: ''
                //        });
                //        parametrosServerSide.emptytable = false;
                //        return JSON.stringify(parametrosServerSide);
                //    }
                //},
                columns: [
                    {
                        name: "Remision",
                        data: "Remision"
                    },
                    {
                        name: "Evento",
                        data: "Evento"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    null,
                    {
                        name: "Ingreso",
                        data: "Ingreso"
                    },
                    {
                        name: "Calificacion",
                        data: "Calificacion"
                    },
                    {
                        name: "Motivo",
                        data: "Motivo"
                    },
                    {
                        name: "Celda",
                        data: "Celda"
                    },
                    null,
                    null,
                    null,
                    {
                        name: "Motivos",
                        data: "Motivos"
                    },
                    {
                        name: "HoraDetencion",
                        data: "HoraDetencion"
                    },
                    {
                        name: "Unidad",
                        data: "Unidad"
                    },
                    {
                        name: "Responsable",
                        data: "Responsable"
                    },
                    {
                        name: "Lugar",
                        data: "Lugar"
                    }
                ],
                columnDefs: [
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            if (row.Edad !== 0) {
                                return row.Edad;
                            }
                            else {
                                return "";
                            }
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {                                                        
                            if (row.Calificacion !== null) {
                                return row.Cumple;                                                                
                            }
                            else {
                                return "Sin fecha de salida";
                            }
                        }
                    },
                    {
                        targets: 9,                        
                        render: function (data, type, row, meta) {                            
                            if (row.Fecha === "01/01/0001 12:00:00 a. m.") {
                                return '';
                            }
                            else {
                                return row.Fecha;
                            }
                        }
                    },
                    {
                        targets: 10,
                        render: function (data, type, row, meta) {
                            if (row.Folio === "0") {
                                return '';
                            }
                            else {
                                return row.Folio;
                            }
                        }
                    }
                ]
            });

            dtable = $("#dt_basic").dataTable().api();

            $("#dt_basic_filter input[type='search']")
                .unbind()
                .bind("input", function (e) {

                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
                });

            $("#dt_basic_filter input[type='search']").keypress(function (e) {
                if (e.charCode === 13) {
                    dtable.search($("#dt_basic_filter input[type='search']").val()).draw();
                    e.preventDefault();
                }
            });

            $("body").on("click", ".reporte", function () {
                $("#ctl00_contenido_lblMessage").html("");
                pdf();
            });

            function pdf() {
                $.ajax({
                    type: "POST",
                    url: "corte_detenidos.aspx/pdfCorteDetenidos",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.exitoso) {
                            open(resultado.ubicacionarchivo.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error! </strong>" +
                                "Algo salió mal: " + resultado.message + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("Error!", resultado.message);
                        }
                    },
                    error: function () {
                        ShowError("Error!", "No fue posible cargar el reporte. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
        });
    </script>
</asp:Content>
