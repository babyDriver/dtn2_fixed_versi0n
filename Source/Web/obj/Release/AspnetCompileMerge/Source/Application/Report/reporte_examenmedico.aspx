﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="reporte_examenmedico.aspx.cs" Inherits="Web.Application.Report.reporte_examenmedico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Report/reporte_examenmedico.aspx?name=reporte">Reportes</a></li>
    <li>Examen médico</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">    
    <link href="../../Content/css/bootstrap.css" rel="stylesheet"/>
    <div class="scroll">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-file-text-o"></i>
                    Reporte de examen médico
                </h1>
            </div>
        </div>
        <div class="row">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                    <div class="jarviswidget" id="wid-users-0" data-widget-editbutton="true" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-search"></i></span>
                            <h2>Buscar</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                        </div>
                    </div>
                </article>
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-users-1" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false" data-widget-togglebutton="false">
                        <header>
                            <%--<span class="widget-icon"><i class="fa fa-file-text-o"></i></span>--%>
                            <img style="width:33px; height:33px; margin-bottom:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/MedicalTestLineWhite.png" />
                            <h2>Examen médico </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body">
                                <div class="row">
                                    <section class="col col-md-12" style="display:none" id="BotonMedico">
                                        <a class="btn btn-md btn-default" href="<%= ConfigurationManager.AppSettings["relativepath"] %>Application/Examen_medico/examenmedico_list.aspx" style="padding:5px">
                                            <i class="fa fa-mail-reply"></i> Regresar a examen médico</a>
                                    </section>
                                </div>
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th data-class="expand">#</th>
                                            <th>No. remisión</th>
                                            <th>Nombre</th>
                                            <th>Apellido paterno</th>
                                            <th>Apellido materno</th>
                                            <th data-hide="phone,tablet">Sexo</th>
                                            <th>Fecha y hora de evaluación</th>
                                            <th data-hide="phone,tablet">Acciones</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value="" />
    <input type="hidden" id="VYXMBM" runat="server" value="" />
    <input type="hidden" id="RAWMOV" runat="server" value="" />
    <input type="hidden" id="WERQEQ" runat="server" value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"] %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();
            var responsiveHelper_dt_basic = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,

                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {

                },
                //ajax: {
                //    type: "POST",
                //    url: "reporte_examenmedico.aspx/getinterno",
                //    contentType: "application/json; charset=utf-8",
                //    data: function (parametrosServerSide) {
                //        $('#dt_basic').waitMe({
                //            effect: 'bounce',
                //            text: 'Cargando...',
                //            bg: 'rgba(255,255,255,0.7)',
                //            color: '#000',
                //            sizeW: '',
                //            sizeH: '',
                //            source: ''
                //        });
                //        parametrosServerSide.emptytable = false;
                //        return JSON.stringify(parametrosServerSide);
                //    }
                //},
                ajax: {
                    url: "<%= ConfigurationManager.AppSettings["relativepath"]  %>Application/Report/reporte_examenmedico.aspx/getinterno",
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: function (d) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });
                        d.emptytable = false;
                        d.pages = $('#dt_basic').DataTable().page.info().page || "";
                        return JSON.stringify(d);
                    },
                    dataSrc: "data",
                    dataFilter: function (data) {
                        var json = jQuery.parseJSON(data);
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        return JSON.stringify(json);
                    }
                },
                columns: [
                    {
                        data: "DetenidoId",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    null,
                    {
                        name: "Expediente",
                        data: "Expediente"
                    },
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    {
                        name: "Paterno",
                        data: "Paterno"
                    },
                    {
                        name: "Materno",
                        data: "Materno"
                    },
                    {
                        name: "Sexo",
                        data: "Sexo"
                    },
                    {
                        name: "FechaExamenAux",
                        data: "FechaExamenAux"
                    },
                    null,
                    {
                        name: "NombreCompleto",
                        data: "NombreCompleto",
                        visible: false
                    }
                ],
                columnDefs: [
                    {
                        data: "Id",
                        targets: 0,
                        orderable: false,
                        visible: false,
                        render: function (data, type, row, meta) {
                            return "";
                        }
                    },
                    {
                        targets: 1,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        targets: 8,
                        orderable: false,
                        render: function (data, type, row, meta) {

                            var reportePDF = '<a href="javascript:void(0);" class="btn btn-success" id="reportePDF" data-id="' + row.IdDetenido + '" data-trackingId="' + row.TrackingIdExamen + '" data-idDetalle="' + row.IdDetalle + '" title="Reporte PDF"><i class="fa fa-file-pdf-o"></i>&nbsp; Generar PDF</a>&nbsp;';
                            return reportePDF;
                        }
                    }
                ]
            });

            dtable = $("#dt_basic").dataTable().api();

            $("#dt_basic_filter input[type='search']")
                .unbind()
                .bind("input", function (e) {

                    if (this.value == "") {
                        dtable.search("").draw();
                    }
                    return;
                });

            $("#dt_basic_filter input[type='search']").keypress(function (e) {
                if (e.charCode === 13) {
                    dtable.search($("#dt_basic_filter input[type='search']").val()).draw();
                    e.preventDefault();
                }
            });

            var param = RequestQueryString("Accion") || "";
            if (param != "") {
                $("#BotonMedico").show();
            }
            else {
                $("#BotonMedico").hide();
            }
            $("body").on("click", "#reportePDF", function () {
                var datos = {
                    "id": $(this).attr("data-id"),
                    "trackingId": $(this).attr("data-trackingId"),
                    "idDetalle": $(this).attr("data-idDetalle"),
                };
                pdf(JSON.stringify(datos));
            });

            function pdf(datos) {
                $.ajax({
                    type: "POST",
                    url: "reporte_examenmedico.aspx/pdf",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        datos: datos
                    }),
                    cache: false,
                    success: function (data) {
                        var resultado = JSON.parse(data.d);
                        if (resultado.success) {
                            open(resultado.file.replace("~", ""));
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal: " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error!", resultado.mensaje);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "No fue posible imprimr el examen médico del detenido. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
