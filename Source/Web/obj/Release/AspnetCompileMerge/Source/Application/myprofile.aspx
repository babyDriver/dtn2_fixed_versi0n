﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="myprofile.aspx.cs" Inherits="Web.Application.myprofile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
  <li>Mi perfil</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
     <style type="text/css">
        input[type="text"]:disabled {
            cursor: not-allowed;
            background: #eee;
            opacity: 1;
        }
    </style>

    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-myprofile-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="false"  data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon"><i class="glyphicon glyphicon-user"></i></span>
                        <h2>Mi perfil </h2>
                    </header>
                    <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">
                        <div id="smart-form-register" class="smart-form">
                            <header>
                                Formulario de edición
                            </header>
                            <fieldset>
                                <div class="col-lg-2">
                                    <div class="row">
                                        <section class="col col-10 text-center">
                                            <img id="avatar" class="img-thumbnail" alt="" runat="server" src="~/Content/img/avatars/male.png" height="80" width="80" />
                                        </section>
                                        <section class="col col-10">
                                            <label class="input">
                                                <i class="icon-append fa fa-file-image-o"></i>
                                                <asp:FileUpload ID="fileUpload" runat="server" />
                                            </label>
                                        </section>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                             
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="input">Usuario</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="usuario" id="usuario" runat="server" placeholder="Usuario" disabled/>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="input">Perfil</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" readonly="true" name="perfil" id="perfil" runat="server" placeholder="Perfil" class="alphanumeric" title="Perfil de usuario" />
                                                <b class="tooltip tooltip-bottom-right">Perfil de usuario</b>
                                            </label>
                                        </section>                                       
                                        <section class="col col-4">
                                            <label class="input">Nombre <a style="color:red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="nombre" id="nombre" maxlength="256" runat="server" placeholder="Nombre" class="alphanumeric" caption="Nombre" title="Nombre"  />
                                                <b class="tooltip tooltip-bottom-right">Ingrese el nombre</b>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="input">Apellido paterno <a style="color:red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="appaterno" id="appaterno" maxlength="256" runat="server" placeholder="Apellido Paterno" class="alphanumeric" caption="Nombre" title="Apellido paterno"  />
                                                <b class="tooltip tooltip-bottom-right">Ingrese el apellido paterno</b>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="input">Apellido materno</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" name="apmaterno" id="apmaterno" maxlength="256" runat="server" placeholder="Apellido Materno" class="alphanumeric" caption="Nombre" title="Apellido Materno"  />
                                                <b class="tooltip tooltip-bottom-right">Ingrese el apellido apellido materno</b>
                                            </label>
                                        </section>
                                        <section id="secttionemail" class="col col-4">
                                            <label class="input">E-mail <a style="color:red">*</a></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-envelope-o"></i>
                                                <input type="text" name="email" id="email" runat="server" placeholder="E-mail"  class="alphanumeric" title="E-mail" />
                                                <b class="tooltip tooltip-bottom-right">e-mail</b>
                                            </label>
                                            <input type="hidden" id="tracking" runat="server" />
                                            <input type="hidden" id="id" runat="server" />
                                            <input type="hidden" id="avatarOriginal" runat="server" />
                                        </section>
                                        <section class="col col-4">
                                            <label class="input">Contraseña</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-lock"></i>
                                                <input type="password" name="password" id="password" runat="server" title="Contraseña" placeholder="Contraseña" autocomplete="new-password" maxlength="4"/>
                                                <b class="tooltip tooltip-bottom-right">La contraseña debe de tener al menos 4 digitos.</b>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="input">Confirmar contraseña</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-lock"></i>
                                                <input type="password" name="passwordconfirm" id="passwordconfirm" title="Confirmación de contraseña" runat="server" placeholder="Confirmar contraseña" maxlength="4" />
                                                <b class="tooltip tooltip-bottom-right">La contraseña debe de tener al menos 4 digitos.</b>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                            </fieldset>
                            <footer>
                                <a class="btn btn-sm btn-default" href="dashboard.aspx"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                                <a class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            </footer>
                        </div>
                    </div>
                </div>
                </div>
            </article>
        </div>
    </section>

    <input type="hidden" id="Max" value="0" />
        <input type="hidden" id="Min" value="0" />
        <input type="hidden" id="Validar1"  />
        <input type="hidden" id="Validar2"  />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
     <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            pageSetUp();
            $("#secttionemail").hide();
            var validFileExtensions = [".jpg", ".jpeg", ".gif", ".png", ".JPG", ".JPEG", ".GIF", ".PNG"];

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    guardar();
                }
            });

             var hideTime = 5000;
        function hideMessage() {
            $("#ctl00_contenido_lblMessage").html("");
        }

            $('#ctl00_contenido_password').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
            });

            $('#ctl00_contenido_passwordconfirm').on('input', function () { 
                this.value = this.value.replace(/[^0-9]/g,'');
            });
            $("#ctl00_contenido_nombre").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            }); $("#ctl00_contenido_appaterno").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            $("#ctl00_contenido_apmaterno").bind('keypress', function (event) {
                var regex = new RegExp("^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });
            function validar() {
                var esvalido = true;

                if ($("#ctl00_contenido_nombre").val() == null || $("#ctl00_contenido_nombre").val().split(" ").join("")=="") {
                    ShowError("Nombre", "El nombre es obligatorio.");
                    $('#ctl00_contenido_nombre').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_nombre').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_nombre').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_nombre').addClass('valid');
                }

                if ($("#ctl00_contenido_appaterno").val() == null || $("#ctl00_contenido_appaterno").val().split(" ").join("") == "") {
                    ShowError("Apellido paterno", "El apellido paterno es obligatorio.");
                    $('#ctl00_contenido_appaterno').parent().removeClass('state-success').addClass("state-error");
                    $('#ctl00_contenido_appaterno').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#ctl00_contenido_appaterno').parent().removeClass("state-error").addClass('state-success');
                    $('#ctl00_contenido_appaterno').addClass('valid');
                }
                                
                if ($("#ctl00_contenido_passwordconfirm").val() != "" || $("#ctl00_contenido_password").val().split(" ").join("") != "") {
                    if ($("#ctl00_contenido_passwordconfirm").val() != $("#ctl00_contenido_password").val()) {
                        ShowError("Contraseñas", "Las contraseñas no coinciden");
                        $('#ctl00_contenido_password').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_password').removeClass('valid');
                        $('#ctl00_contenido_passwordconfirm').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_passwordconfirm').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#ctl00_contenido_passwordconfirm').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_passwordconfirm').addClass('valid');
                        $('#ctl00_contenido_password').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_password').addClass('valid');
                    }

                    //var re = /^(?=.{8,})(?=(.*\W){1,})/;
                    var re = /^(?=.{4,})/;

                    if (re.test($("#ctl00_contenido_password").val())) {
                        $('#ctl00_contenido_passwordconfirm').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_passwordconfirm').addClass('valid');
                        $('#ctl00_contenido_password').parent().removeClass("state-error").addClass('state-success');
                        $('#ctl00_contenido_password').addClass('valid');
                    }
                    else {
                        ShowError("Contraseñas", "La contraseña debe de tener al menos 4 digitos.");
                        $('#ctl00_contenido_password').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_password').removeClass('valid');
                        $('#ctl00_contenido_passwordconfirm').parent().removeClass('state-success').addClass("state-error");
                        $('#ctl00_contenido_passwordconfirm').removeClass('valid');
                        esvalido = false;
                    }
                }

                var file = document.getElementById('<% = fileUpload.ClientID %>').value;
                if (file != null && file != '') {
                    if (!FileValidation(validFileExtensions, "#ctl00_contenido_fileUpload")) {
                        esvalido = false;
                    }
                    else {
                        var validar = $("#Validar1").val();
                        var validar2 = $("#Validar2").val();


                        if ($("#Max").val() != "0") {
                            if (validar == 'false') {
                                ShowError("Archivo", "Solo se permiten archivos con un peso maximo de " + $("#Max").val() + "mb");
                                $('#ctl00_contenido_fileUpload').parent().removeClass('state-success').addClass("state-error");
                                $('#ctl00_contenido_fileUpload').removeClass('valid');
                                esvalido = false;
                            }
                            else {
                                $('#ctl00_contenido_fileUpload').parent().removeClass("state-error").addClass('state-success');
                                $('#ctl00_contenido_fileUpload').addClass('valid');
                            }

                        }
                        if ($("#Min").val() != "0") {
                            var s = $("#Min").val() * 1024;
                            if (validar2 == 'false') {
                                ShowError("Archivo", "Solo se permiten archivos con un peso minimo de " + $("#Min").val() + "mb");
                                $('#ctl00_contenido_fileUpload').parent().removeClass('state-success').addClass("state-error");
                                $('#ctl00_contenido_fileUpload').removeClass('valid');
                                esvalido = false;
                            }
                            else {
                                $('#ctl00_contenido_fileUpload').parent().removeClass("state-error").addClass('state-success');
                                $('#ctl00_contenido_fileUpload').addClass('valid');
                            }
                        }
                    }
                }

                return esvalido;
            }

            $("#ctl00_contenido_fileUpload").change(function () {
                var s = this.files[0].size;
                var n = (s / 1024);
                var max = $("#Max").val() * 1024;
                var min = $("#Min").val() * 1024;
                if (max > 0) {
                    if (n > max) {
                        $("#Validar1").val(false);
                    }
                    else {
                        $("#Validar1").val(true);
                    }
                }
                else {
                    $("#Validar1").val(true);
                }
                if (min > 0) {
                    if (n < min) {
                        $("#Validar2").val(false);
                    }
                    else {
                        $("#Validar2").val(true);
                    }
                }
                else {
                    $("#Validar2").val(true);
                }

            });


            GetMinMax();

            function GetMinMax() {
                $.ajax({
                    type: "POST",
                    url: "myprofile.aspx/GetParametrosTamaño",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var max = resultado.TMax;
                            var min = resultado.Tmin;

                            $("#Max").val(max);
                            $("#Min").val(min);
                            
                            $('#main').waitMe('hide');
                            
                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }

           function guardar() {
                   var files = $("#ctl00_contenido_fileUpload").get(0).files;

                   var nombreAvatarAnterior = $("#ctl00_contenido_avatarOriginal").val();

                   var nombreAvatar = "";

                   if (files.length > 0) {

                       var data = new FormData();
                       for (var i = 0; i < files.length; i++) {
                           data.append(files[i].name, files[i]);
                       }

                       $.ajax({
                           url: "Handlers/FileUploadHandler.ashx?action=1&before=" + nombreAvatarAnterior,
                           type: "POST",
                           data: data,
                           contentType: false,
                           processData: false,
                           success: function (Results) {
                               if (Results.exitoso) {
                                   nombreAvatar = Results.nombreArchivo;
                                   GuardarPerfil(nombreAvatar);
                               }
                               else {
                                   ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                               }
                           },
                           error: function (err) {
                               ShowError("¡Error!", "No fue posible obtener el avatar. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                           }
                       });
                   }
                   else {
                       GuardarPerfil(nombreAvatar);
                   }
               }

               function GuardarPerfil(rutaAvatar) {
                   startLoading();

                   var datos = ObtenerValores(rutaAvatar);

                   $.ajax({
                       type: "POST",
                       url: "myprofile.aspx/guardar",
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       cache: false,
                       data: JSON.stringify({
                           datos: datos
                       }),
                       success: function (data) {
                           var resultado = JSON.parse(data.d);
                           if (resultado.exitoso) {
                               $('#ctl00_contenido_id').val(resultado.Id);
                               $('#ctl00_contenido_tracking').val(resultado.TrackingId);
                               $('#ctl00_contenido_avatarOriginal').val(resultado.Avatar);
                               var imagenAvatar = ResolveUrl(resultado.Avatar);
                               $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                               $("#ctl00_contenido_fileUpload").val('').clone(true);

                               $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                   "La información de tu perfil se modificó correctamente.", "<br /></div>");
                               ShowSuccess("¡Bien hecho!", "La información de tu perfil se modificó correctamente.");
                               $('#main').waitMe('hide');
                           }
                           else {
                               $('#main').waitMe('hide');
                               $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                   "Algo salió mal. " + resultado.mensaje + "</div>");
                               ShowError("¡Error!", "Algo salió mal. " + resultado.mensaje);
                           }
                           setTimeout(hideMessage, hideTime);
                       }
                   });
               }

               function ObtenerValores(rutaAvatar) {
                   var datos = {
                       Usuario: $('#ctl00_contenido_usuario').val(),
                       Id: $('#ctl00_contenido_id').val(),
                       TrackingId: $('#ctl00_contenido_tracking').val(),
                       Nombre: $('#ctl00_contenido_nombre').val(),
                       ApellidoPaterno: $('#ctl00_contenido_appaterno').val(),
                       ApellidoMaterno: $('#ctl00_contenido_apmaterno').val(),
                       Perfil: $('#ctl00_contenido_perfil').val(),
                       Email: $('#ctl00_contenido_email').val(),
                       Oficina: $('#ctl00_contenido_telefono').val(),
                       Movil: $('#ctl00_contenido_movil').val(),
                       Password: $('#ctl00_contenido_password').val(),
                       Avatar: rutaAvatar,
                   };
                   return JSON.stringify(datos);
               }

               function CargarPerfil(trackingid) {
                   startLoading();
                   $.ajax({
                       type: "POST",
                       url: "myprofile.aspx/cargarPerfil",
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       data: JSON.stringify({
                           trackingId: trackingid,
                       }),
                       cache: false,
                       success: function (data) {
                           $('#ctl00_contenido_id').val(JSON.parse(data.d).Id);
                           $('#ctl00_contenido_tracking').val(JSON.parse(data.d).TrackingId);
                           $('#ctl00_contenido_usuario').attr('disabled', 'disabled');
                           $('#ctl00_contenido_usuario').val(JSON.parse(data.d).Usuario);
                           $('#ctl00_contenido_nombre').val(JSON.parse(data.d).Nombre);
                           $('#ctl00_contenido_appaterno').val(JSON.parse(data.d).ApPaterno);
                           $('#ctl00_contenido_apmaterno').val(JSON.parse(data.d).ApMaterno);
                           $('#ctl00_contenido_perfil').val(JSON.parse(data.d).Perfil);
                           $('#ctl00_contenido_perfil').attr('disabled', 'disabled');
                           //$('#ctl00_contenido_centro').val(JSON.parse(data.d).Centro);
                           //$('#ctl00_contenido_centro').attr('disabled', 'disabled');
                           $('#ctl00_contenido_email').val(JSON.parse(data.d).Email);
                           $('#ctl00_contenido_movil').val(JSON.parse(data.d).Movil);
                           var imagenAvatar = ResolveUrl(JSON.parse(data.d).Avatar);
                           $('#ctl00_contenido_avatar').attr("src", imagenAvatar);
                           $('#ctl00_contenido_avatarOriginal').val(JSON.parse(data.d).AvatarOriginal);
                           $('#main').waitMe('hide');
                       },
                       error: function () {
                           $('#main').waitMe('hide');
                           ShowError("¡Error!", "No fue posible cargar la información del perfil. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                       }
                   });
               }

            function ResolveUrl(url) {
                   var baseUrl = "<%= ResolveUrl("~/") %>";
                if (url.indexOf("~/") == 0) {
                    url = baseUrl + url.substring(2);
                }
                return url;
            }

            function init() {
                CargarPerfil();
            }
            init();
           });
      </script>
</asp:Content>
