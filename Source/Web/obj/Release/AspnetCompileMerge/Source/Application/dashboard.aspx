﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="Web.Application.dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Tablero de control</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <section id="widget-grid" class="">
        <div class="row">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </div>
        <div class="row">
            <div class="scroll">
                <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget" id="wid-dashboard-2" data-widget-colorbutton="false" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-map"></i></span>
                            <h2>Inicio</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                                <div>
                                    <label>Title:</label>
                                    <input type="text" />
                                </div>
                            </div>
                            <div class="widget-body">
                                <%--<div>Bienvenido</div>--%>
                                <div class="smart-form"></div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <section>
                                    <label>Año </label>
                                    <select id="idAnioTrabajo" class="select2"></select>
                                            </section>
                                        </div>
                                </div>
                                <div class="row">
                                     <article class="col-xs-4 col-sm-4 col-md-4 col-lg-4 sortable-grid ui-sortable">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-3" data-widget-editbutton="false" role="widget">
                        <header role="heading" class="ui-sortable-handle">
                           
                            <h2>TOTAL DE DETENIDOS REGISTRADOS &nbsp;<span id="lbltotal">0</span>&nbsp;Detenidos(as)</h2>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>
                        <!-- widget div-->
                        <div role="contentdet">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div id="div_detenidosactivos">
                                    <canvas id="pie-chart" width="521" height="220"></canvas> 
                                </div>
                                <div align="center">
                                <i class="fa fa-male">&nbsp;</i><span id="lblhombres">0</span>&nbsp;<small>Hombres </small>
                                                    &nbsp;<i class="fa fa-female">&nbsp;</i><span id="lblmujeres">0</span>&nbsp;<small>Mujeres </small>
                                                    &nbsp;<i class="fa fa-exclamation-triangle" id="iconsindato" style="display: none;">&nbsp;</i><span id="lblsidato" style="display: none;"></span>&nbsp;&nbsp;<small id="smallsindato" style="display: none;">Sin información
                                                </small>
                                    <br />
                                    </div>
                                <div></div>
                                <div class="alert alert-info fade in" id="mensaje_detenidos" style="display:none">
				                    <button class="close" data-dismiss="alert">
					                ×
				                    </button>
				                    <i class="fa-fw fa fa-info"></i>
				                    <strong>Atención!</strong> No se encontró información de motivos de detención para mostrar.
			                    </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>
                   <article class="col-xs-4 col-sm-4 col-md-4 col-lg-4 sortable-grid ui-sortable">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" role="widget">
                        <header role="heading" class="ui-sortable-handle">
                           
                            <h2>TOTAL DE PERSONAS EN TRABAJO SOCIAL</small>&nbsp;<span id="TotalPersonas">1</span>&nbsp;Personas</h2><br>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>
                        <!-- widget div-->
                        <div role="contentdetper">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div id="div_personas">
                                    <canvas id="pie-chart2" width="521" height="220"></canvas> 
                                </div>
                                <div align="center">
                             <i class="fa fa-male">&nbsp;</i><span id="lblhombrespersonasts">0</span>&nbsp;<small>Hombres </small>
                                                    &nbsp;<i class="fa fa-female">&nbsp;</i><span id="lblmujerespersonasts">0</span>&nbsp;<small>Mujeres </small>
                                                    &nbsp;<i class="fa fa-exclamation-triangle" id="iconsindatoactivos" style="display: none;">&nbsp;</i><span id="lblsidatoactivos" style="display: none;"></span>&nbsp;&nbsp;<small id="smallsindatoactivos" style="display: none;">Sin información
                                                </small>
                                  
                                    </div>
                                <div></div>
                                <div class="alert alert-info fade in" id="mensaje_personas" style="display:none">
				                    <button class="close" data-dismiss="alert">
					                ×
				                    </button>
				                    <i class="fa-fw fa fa-info"></i>
				                    <strong>Atención!</strong> No se encontró información de motivos de detención para mostrar.
			                    </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>

                   <article class="col-xs-4 col-sm-4 col-md-4 col-lg-4 sortable-grid ui-sortable">
                                        <!-- Widget ID (each widget will need unique ID)-->
                                        <div class="jarviswidget jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" role="widget">
                                            <header role="heading" class="ui-sortable-handle">
                           
                                                <h2>HISTÓRICO TOTAL DE DETENIDOS REGISTRADOS &nbsp;<span id="lbltotalh">0</span>&nbsp;Detenidos(as)</h2><br />
                                                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                            </header>
                                            <!-- widget div-->
                                            <div role="contentdethist">
                                                <!-- widget edit box -->
                                                <div class="jarviswidget-editbox">
                                                    <!-- This area used as dropdown edit box -->
                                                </div>
                                                <!-- end widget edit box -->
                                                <!-- widget content -->
                                                <div class="widget-body no-padding">
                                                    <div id="div_historicos">
                                                        <canvas id="pie-chart3" width="521" height="220"></canvas> 
                                                    </div>
                                                    <div align="center">
                                                  <i class="fa fa-male">&nbsp;</i><span id="lblhombresh">0</span>&nbsp;<small>Hombres </small>
                                                                        &nbsp;<i class="fa fa-female">&nbsp;</i><span id="lblmujeresh">0</span>&nbsp;<small>Mujeres </small>
                                                                        &nbsp;<i class="fa fa-exclamation-triangle" id="iconsindatoh" style="display: none;">&nbsp;</i><span id="lblsidatoh" style="display: none;"></span>&nbsp;&nbsp;<small id="smallsindato" style="display: none;">Sin información
                                                                    </small>
                                  
                                                        </div>
                                                    <div></div>
                                                    <div class="alert alert-info fade in" id="mensaje_personas" style="display:none">
				                                        <button class="close" data-dismiss="alert">
					                                    ×
				                                        </button>
				                                        <i class="fa-fw fa fa-info"></i>
				                                        <strong>Atención!</strong> No se encontró información de motivos de detención para mostrar.
			                                        </div>
                                                </div>
                                                <!-- end widget content -->
                                            </div>
                                            <!-- end widget div -->
                                        </div>
                                        <!-- end widget -->
                                    </article>
                   
                   <article class="col-xs-6 col-sm-6 col-md-6 col-lg-6 sortable-grid ui-sortable">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-5" data-widget-editbutton="false" role="widget">
                        <header role="heading" class="ui-sortable-handle">
                           
                            <h2>DETENIDOS REGISTRADOS EN LA SEMANA &nbsp;<span id="lbltotalsemana">1</span >&nbsp;</h2><br />
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>
                        <!-- widget div-->
                        <div role="contentsemana">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div id="div_semana">
                                    <canvas id="pie-chart5" width="521" height="220"></canvas> 
                                </div>
                                <div align="center">
                               <i class="fa fa-male">&nbsp;</i><span id="lblhombressemana">0</span>&nbsp;<small>Hombres </small>
                                                    &nbsp;<i class="fa fa-female">&nbsp;</i><span id="lblmujeressemana">0</span>&nbsp;<small>Mujeres </small>&nbsp;|

                                                <i class="fa fa-male">&nbsp;</i><span id="lblhombressemanah">0</span>&nbsp;<small>Histórico hombres </small>
                                                    &nbsp;<i class="fa fa-female">&nbsp;</i><span id="lblmujeressemanah">0</span>&nbsp;<small>Histórico mujeres </small>
                                                    &nbsp;<i class="fa fa-exclamation-triangle" id="semanah" style="display: none;">&nbsp;</i><span id="semanash" style="display: none;"></span>&nbsp;&nbsp;<small id="smallsindatoactivos" style="display: none;">Sin información
                                                </small>
                                    </div>
                                <div></div>
                                <div class="alert alert-info fade in" id="mensaje_semana" style="display:none">
				                    <button class="close" data-dismiss="alert">
					                ×
				                    </button>
				                    <i class="fa-fw fa fa-info"></i>
				                    <strong>Atención!</strong> No se encontró información de motivos de detención para mostrar.
			                    </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>

                   <article class="col-xs-6 col-sm-6 col-md-6 col-lg-6 sortable-grid ui-sortable">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-6" data-widget-editbutton="false" role="widget">
                        <header role="heading" class="ui-sortable-handle">
                           
                            <h2>DETENIDOS REGISTRADOS EN EL MES DE &nbsp;<span id="lbltotalmes">1</span >&nbsp;</h2><br />
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>
                        <!-- widget div-->
                        <div role="contentdethist">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div id="div_mesactual">
                                    <canvas id="pie-chart4" width="521" height="220"></canvas> 
                                </div>
                                <div align="center">
                                 <i class="fa fa-male">&nbsp;</i><span id="lblhombresmes">0</span>&nbsp;<small>Hombres </small>
                                                    &nbsp;<i class="fa fa-female">&nbsp;</i><span id="lblmujeresmes">0</span>&nbsp;<small>Mujeres </small>&nbsp;
                                                <i class="fa fa-male">&nbsp;</i><span id="lblhombresmesh">0</span>&nbsp;<small>Histórico hombres </small>
                                                    &nbsp;<i class="fa fa-female">&nbsp;</i><span id="lblmujeresmesh">0</span>&nbsp;<small>Histórico mujeres </small>
                                                    &nbsp;<i class="fa fa-exclamation-triangle" id="mesact" style="display: none;">&nbsp;</i><span id="actmes" style="display: none;"></span>&nbsp;&nbsp;<small id="smallsind_os" style="display: none;">Sin información</small>
                                  
                                    </div>
                                <div></div>
                                <div class="alert alert-info fade in" id="mensaje_mesactual" style="display:none">
				                    <button class="close" data-dismiss="alert">
					                ×
				                    </button>
				                    <i class="fa-fw fa fa-info"></i>
				                    <strong>Atención!</strong> No se encontró información de motivos de detención para mostrar.
			                    </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>  



                                </div>
                             
                                <br />
                            </div>
                        </div>
                    </div>
                </article>
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-sortable" id="wid-id-2" data-widget-editbutton="false" role="widget">
                        <header role="heading" class="ui-sortable-handle">
                            <span class="widget-icon">
                                <img style="width:20px; height:20px;margin-top:1px; float:left;" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>content/img/HandCuffs.png" />
                            </span>
                            <h2>Motivos de detención </h2>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>
                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div id="div_delitos">
                                    <div style='overflow-x:scroll;overflow-y:hidden;'>
                                
                                    <canvas id="bar-chart" width="521" height="220"></canvas> 
                                

                                    </div>
                                    
                                </div>                                                                          
                                <div class="alert alert-info fade in" id="mensaje_delitos" style="display:none">
				                    <button class="close" data-dismiss="alert">
					                ×
				                    </button>
				                    <i class="fa-fw fa fa-info"></i>
				                    <strong>Atención!</strong> No se encontró información de motivos de detención para mostrar.
			                    </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>
            </div>
        </div>
       

         <div class="modal fade" id="modal-password"  role="dialog" data-backdrop="static" data-keyboard="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">                    
                        <h4 class="modal-title" id="form-modal-title2">Cambiar contraseña</h4>
                    </div>
                    <div class="modal-body">
                        
                        <div id="register-form2" class="smart-form">
                            <fieldset> 
                                <div class="row">
                                    <div class="col-md-11">
                                        <section class="col-col-4">
                                    
                                    <label style="color: dodgerblue" class="label">Contraseña nueva <a style="color: red">*</a></label>
                                    
                                            <label class="input">
                                                <i class="icon-append fa fa-lock"></i>
                                                <input id="txtcontrasenia" name="txtcontrasenia" type="password" maxlength="4" autocomplete="new-password" class="form-control"/>
                                                <b class="tooltip tooltip-bottom-right">Ingrese la nueva contraseña.</b>
                                                
                                            </label>                                
                                </section>
                        </div>
                                 <div class="col-md-1">
                                     <br />
                                     <section class="col col-3">
                                    <input id="ShowPassword" type="checkbox" style="width:30px;height:20px;" />
                                </section>
                                 </div>    
                                
                                </div>
                               
                            </fieldset>
                            <footer>                            
                                <a class="btn btn-sm btn-default save2" id="btnsave2"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="form-modal"  role="dialog" data-backdrop="static" data-keyboard="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">                    
                        <h4 class="modal-title" id="form-modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <div id="register-form" class="smart-form">
                            <fieldset>                            
                                <section>
                                    <label style="color: dodgerblue" class="input">Subcontrato <a style="color: red">*</a></label>                                    
                                    <select name="contrato" id="idcontrato" style="width: 100%" class="select2"></select>                                    
                                </section>                                                            
                            </fieldset>
                            <footer>                            
                                <a class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>                            
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server"> 
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/highcharts/highmaps.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/moment/moment.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/chartjs/chart.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $("#ctl00_nombreContrato").hide();

            $("#idAnioTrabajo").change(function () {
                loadDashboard($(this).val());
                GetDelitos($(this).val());
            });
            validarBandera();

            if (!loadContratoId()) {
                init();
                
            }
             var fecha = new Date();
            var anio = fecha.getFullYear();
            loadDashboardAniosTrabajo();
            $("#idAnioTrabajo").val(anio);


            $('#ShowPassword').click(function () {

                $('#txtcontrasenia').attr('type', $(this).is(':checked') ? 'text' : 'password');
            });

            $("body").on("click", ".save2", function () {

                if (validatepassword()) {
                    var password = $("#txtcontrasenia").val();
                    savepassword(password);
                }
            });
            $('#txtcontrasenia').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            });

            function savepassword(password) {

                $.ajax({
                    type: "POST",
                    url: "dashboard.aspx/savepassword",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'password': password }),
                    success: function (data) {
                        var resultado = data.d;
                        
                        if (resultado.exitoso) {

                            
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La contraseña se  " + resultado.mensaje + " con exito.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La contraseña se  " + resultado.mensaje + " con exito.");
                            $("#modal-password").modal("hide");
                            $('#main').waitMe('hide');
                            
                        }
                        else {

                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }


            function validatepassword() {
                var esvalido = true;
                if ($("#txtcontrasenia").val() == "") {
                    ShowError("Contraseña", "La contraseña es obligatoria.");
                    $('#txtcontrasenia').parent().removeClass('state-success').addClass("state-error");
                    $('#txtcontrasenia').removeClass('valid');
                    esvalido = false;
                }
                else {
                    if ($("#txtcontrasenia").val().length < 4) {
                        ShowError("Contraseña", "La contraseña deberá contener 4 dígitos.");
                        $('#txtcontrasenia').parent().removeClass('state-success').addClass("state-error");
                        $('#txtcontrasenia').removeClass('valid');
                        esvalido = false;
                    }
                    else {
                        $('#txtcontrasenia').parent().removeClass("state-error").addClass('state-success');
                        $('#txtcontrasenia').addClass('valid');
                    }
                }
                
                return esvalido;
            }

            loadDashboard(anio);
            GetDelitos(anio);
            
            function validarBandera() {
                $.ajax({
                    type: "POST",
                    url: "dashboard.aspx/ValidateBandera",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,

                    success: function (data) {
                        var resultado = JSON.parse(data.d);

                        if (resultado.exitoso) {
                            var valido = resultado.valido;
                            
                            if (valido) {
                                $("#modal-password").modal("show");
                            }
                            

                        }
                        else {
                            $('#main').waitMe('hide');

                            ShowError("¡Error! Algo salió mal", resultado.mensaje);
                        }
                        $('#main').waitMe('hide');
                    }
                });
            }


            function init() {
                $("#form-modal-title").empty();
                $("#form-modal-title").html('<i class="icon-append fa fa-archive"></i> Seleccione el subcontrato que usará');
                $("#form-modal").modal("show");
                $("#idcontrato").select2();
                loadContracts("0");
            }
            function loadDashboardAniosTrabajo(setvalue) {

                $.ajax({                    
                    type: "POST",
                    url: "dashboard.aspx/LoadDashboardAniosTrabajo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idAnioTrabajo');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Selecione año]", "0"));

                        if (response.d.length > 0) {
                            setvalue = response.d[0].Id;
                        }

                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de estados. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }    
            function loadContratoId() {
                var isSetted = false;
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "dashboard.aspx/loadContratoId",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        if (resultado.success) {
                            if (resultado.contratoId != 0) {
                                isSetted = true;
                                localStorage.setItem("nombreContrato", resultado.nombreContrato);
                                $("#ctl00_nombreContrato").text(localStorage.getItem("nombreContrato"));
                                $("#ctl00_nombreContrato").show();
                            }
                            else {
                                if (resultado.hasRegisters) {
                                    isSetted = false;
                                }
                                else {
                                    localStorage.setItem("nombreContrato", "");
                                    $("#ctl00_nombreContrato").text(localStorage.getItem("nombreContrato"));
                                    $("#ctl00_nombreContrato").show();
                                    isSetted = true;
                                    if (resultado.Rol != "Administrador global") {
                                        $("#ctl00_contenido_lblMessage").html("<div class='alert alert-warning fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>Aviso!</strong>" +
                                            " El usuario no tiene contratos asignados, por favor asignarlos en el apartado de usuarios.</div>");
                                        setTimeout(hideMessage, hideTime);
                                        ShowAlert("Aviso!", "El usuario no tiene contratos asignados, por favor asignarlos en el apartado de usuarios.");
                                    }
                                }
                            }
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar el contrato. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });

                return isSetted;
            }

            ////
            function GetDelitos() {
                $("#div_delitos").hide();
                $("#mensaje_delitos").hide();
                $.ajax({
                    type: "POST",
                    url: "dashboard.aspx/getdelitos",
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        centroId: "0"
                    }),
                    success: function (data) {
                        var result = JSON.parse(data.d);
                        if (result.length > 0) {
                            $("#div_delitos").show();
                            var arrayTotal = [];
                            var arrayDelito = [];
                            var arrayColor = [];
                            var valorMaximo = 0;


                            result.forEach(function (item, index) {
                                arrayDelito.push(item.Motivo);
                                arrayTotal.push(item.Cantidad);
                                arrayColor.push('#' + (Math.random() * 0xFFFFFF << 0).toString(16));
                                if (item.total > valorMaximo)
                                    valorMaximo = item.total;
                            });
                            valorMaximo = valorMaximo + 5;
                            new Chart(document.getElementById("bar-chart"), {
                                type: 'bar',
                                data: {
                                    labels: arrayDelito,
                                    datasets: [
                                        {
                                            label: "Motivos de detencion",
                                            backgroundColor: arrayColor,
                                            data: arrayTotal
                                        }
                                    ]
                                },
                                options: {
                                    legend: { display: false },
                                    title: {
                                        display: true,
                                        text: 'Motivos de detención'
                                    },
                                    scales: {
                                        xAxes: [{
                                            display: true,
                                            scaleLabel: {
                                                show: true,
                                                labelString: 'Total de detenidos'
                                            }
                                        }],
                                        yAxes: [{
                                            display: true,
                                            scaleLabel: {
                                                show: true,
                                                labelString: 'Motivo detención'
                                            },
                                            ticks: {
                                                suggestedMin: 0,
                                                suggestedMax: valorMaximo,
                                            }
                                        }]
                                    }

                                }
                            });
                        }
                        else {
                            $("#mensaje_delitos").show();
                        }
                    }
                });
            }

           
             function loadDashboard() {
                var isSetted = false;
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "dashboard.aspx/LoadDashboard",
                    contentType: "application/json; charset=utf-8",
                       data: JSON.stringify({
                        anio: anio
                    }),
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        if (resultado.success) {
                            $("#lblhombres").text(resultado.hombresdetenidos);
                            $("#lblmujeres").text(resultado.Mujeresdetenidas);
                            $("#lbltotal").text(resultado.Totaldetenidos);
                            $("#lblhombresh").text(resultado.hombresdetenidosh);
                            $("#lblmujeresh").text(resultado.Mujeresdetenidash);
                            $("#lbltotalh").text(resultado.Totaldetenidosh);

                          
                            $("#div_detenidosactivos").show();

                              var PieConfig = {
			        type: 'pie',
			        data: {
			            datasets: [{
			                data: [
			                    resultado.hombresdetenidos,
			                    resultado.Mujeresdetenidas,
			                    
			                ],
			                backgroundColor: [
			                    "#0000ff",
			                   
			                    "#ff0080",
			                    
			                ],
			            }],
			            labels: [
			                "Hombres",
			              
			                "Mujeres",
			              
			            ]
			        },
			        options: {
			            responsive: true
			        }
                            };


                            var PieConfig2 = {
			        type: 'pie',
			        data: {
			            datasets: [{
			                data: [
			                    resultado.HTS,
			                    resultado.MTS,
			                    
			                ],
			                backgroundColor: [
			                    "#0000ff",
			                   
			                    "#ff0080",
			                    
			                ],
			            }],
			            labels: [
			                "Hombres",
			              
			                "Mujeres",
			              
			            ]
			        },
			        options: {
			            responsive: true
			        }
			    };

                            var PieConfig3 = {
			        type: 'pie',
			        data: {
			            datasets: [{
			                data: [
			                    resultado.hombresdetenidosh,
			                    resultado.Mujeresdetenidash,
			                    
			                ],
			                backgroundColor: [
			                    "#0000ff",
			                   
			                    "#ff0080",
			                    
			                ],
			            }],
			            labels: [
			                "Hombres",
			              
			                "Mujeres",
			              
			            ]
			        },
			        options: {
			            responsive: true
			        }
                            };
                            var PieConfig4 = {
			        type: 'pie',
			        data: {
			            datasets: [{
			                data: [
			                    resultado.hombresdetenidosmesactual,
                                resultado.Mujeresdetenidasmesactual,
                                resultado.hombresdetenidosmesactualh,
                                resultado.Mujeresdetenidasmesactualh,
			                    
			                ],
			                backgroundColor: [
			                    "#0000ff",
                                "#ff0080",
                                "#2d572c",
                                "#9f51a2",

			                    
			                ],
			            }],
			            labels: [
			                "Hombres",
                            "Mujeres",
                            "Histórico hombres",
                            "Histórico mujeres"
			              
			            ]
			        },
			        options: {
			            responsive: true
			        }
                            };

                            var PieConfig5 = {
			        type: 'pie',
			        data: {
			            datasets: [{
			                data: [
			                    resultado.Hombresdetenidossemana,
                                resultado.Mujeresdetenidassemana,
                                resultado.Hombresdetenidossemanah,
                                resultado.Mujeresdetenidassemanah,
			                    
			                ],
			                backgroundColor: [
			                    "#0000ff",
                                "#ff0080",
                                "#2d572c",
                                "#9f51a2",

			                    
			                ],
			            }],
			            labels: [
			                "Hombres",
                            "Mujeres",
                            "Histórico hombres",
                            "Histórico mujeres"
			              
			            ]
			        },
			        options: {
			            responsive: true
			        }
			    };

                             window.myPie = new Chart(document.getElementById("pie-chart"), PieConfig);
                             window.myPie = new Chart(document.getElementById("pie-chart2"), PieConfig2);
                            window.myPie = new Chart(document.getElementById("pie-chart3"), PieConfig3);
                            window.myPie = new Chart(document.getElementById("pie-chart4"), PieConfig4);
                            window.myPie = new Chart(document.getElementById("pie-chart5"), PieConfig5);



                            $("#TotalPersonas").text(resultado.TotalPersonas);
                            $("#lblhombrespersonasts").text(resultado.HTS);
                            $("#lblmujerespersonasts").text(resultado.MTS);

                            $("#lblhombresmes").text(resultado.hombresdetenidosmesactual);
                            $("#lblmujeresmes").text(resultado.Mujeresdetenidasmesactual);
                            $("#lblhombresmesh").text(resultado.hombresdetenidosmesactualh);
                            $("#lblmujeresmesh").text(resultado.Mujeresdetenidasmesactualh);
                            $("#lbltotalmes").text(resultado.nombredelmes );

                            $("#lblhombressemana").text(resultado.Hombresdetenidossemana);
                            $("#lblmujeressemana").text(resultado.Mujeresdetenidassemana);
                            $("#lblhombressemanah").text(resultado.Hombresdetenidossemanah);
                            $("#lblmujeressemanah").text(resultado.Mujeresdetenidassemanah);
                            $("#lbltotalsemana").text(resultado.TotalSemana);
                        }
                        else {
                            ShowError("¡Error!", resultado.message);
                            console.log(resultado.messagelog);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar el contrato. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });

                return isSetted;
            }
            function loadContracts(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "dashboard.aspx/getContracts",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#idcontrato');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[SubContrato]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de contratos. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            $("#btnsave").click(function () {
                if ($("#idcontrato").val() != null && $("#idcontrato").val() != "0") {
                    data = $("#idcontrato").val();
                    saveContratoId(data);
                    GetDelitos();
                }
                else {
                    ShowError("Subcontrato", "El subcontrato es obligatorio.");
                    $("#idcontrato").select2({ containerCss: { "border-color": "#a90329" } });
                }
            });

            function saveContratoId(data) {
                $.ajax({
                    type: "POST",
                    url: "dashboard.aspx/guardarContratoId",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({
                        'dato': data
                    }),
                    success: function (response) {
                        var resultado = JSON.parse(response.d);
                        if (resultado.success) {
                            if (resultado.contratoId != "0") {
                                localStorage.setItem("nombreContrato", resultado.nombreContrato);
                                $("#ctl00_nombreContrato").text(localStorage.getItem("nombreContrato"));
                                $("#ctl00_nombreContrato").show();
                                $("#idcontrato").select2({ containerCss: { "border-color": "#bdbdbd" } });
                                $("#form-modal-title").empty();
                                $("#form-modal").modal("hide");
                                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                    " El subcontrato se guardo correctamente.</div>");
                                ShowSuccess("¡Bien hecho!", "El subcontrato fue guardado correctamente.");
                                setTimeout(hideMessage, hideTime);
                                loadDashboard();
                            }
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
                                "Algo salió mal y no fue posible guardar el subcontrato seleccionado. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            ShowError("Error! Algo salió mal y no fue posible guardar el subcontrato seleccionado. Si el problema persiste, contacte al personal de soporte técnico.");
                            setTimeout(hideMessage, hideTime);
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible guardar el contrato. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }
        });
    </script>
</asp:Content>
