﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="MotivoEdit.aspx.cs" Inherits="Web.Application.MotivoEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li><a href="<%= ConfigurationManager.AppSettings["relativepath"]%>Application/Admin/catalogo.aspx?name=escolaridad">Administracón</a></li>
    <li><a href="motivo.aspx">Motivos detención</a></li>
    <li>Registro</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-useredit-1" data-widget-editbutton="false" data-widget-custombutton="false">
                <header>
                    <span class="widget-icon"><i class="fa fa-edit"></i></span>
                    <h2>Motivo detención</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                    </div>
                    <div class="widget-body no-padding">                        
                        <div id="smart-form-register" class="smart-form">
                            <header id="header_form">
                                Formulario de registro
                            </header>
                            <fieldset>
                                <div class="row">
                                    
                                    <section class="col col-4">
                                        Motivo detención <a style="color: red">*</a>
                                        <label class="input">
                                           
                                            <input type="text" name="motivo" id="motivo" placeholder="Motivo detención" maxlength="256" class="alphanumeric alptext" style="width: 301px" />
                                            <b class="tooltip tooltip-bottom-right">                        Ingrese el motivo.</b>
                                        </label>
                                    </section>
                                        <section class="col col-4">
                                        Descripción <a style="color: red">*</a>
                                        <label class="input">
                                            
                                            <input type="text" name="descripcion" id="descripcion" placeholder="Descripción" maxlength="256" class="alphanumeric alptext" style="width: 301px" />
                                            <b class="tooltip tooltip-bottom-right">                        Ingrese la descripción.</b>
                                        </label>
                                    </section>
                                   
                                    <section class="col col-4">
                                        Artículo <a style="color: red">*</a>
                                        <label class="input">
                                            
                                            <input type="text" name="articulo" id="articulo" placeholder="Artículo" maxlength="256" class="alphanumeric alptext" style="width: 309px" />
                                            
                                            <b class="tooltip tooltip-bottom-right">Ingrese el motivo de detención.</b>
                                        </label>
                                    </section>
                                  
                                      <section class="col col-4">
                                        Multa <a style="color: red">*</a>
                                        <label class="input">
                                            
                                            <input  title="" type ="text" name="contrato" id="multa" placeholder="multa" pattern="^\d*(\.\d{0,2})?$" maxlength="256" class="alphanumeric" style="width: 307px"  />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la multa.</b>
                                        </label>
                                    </section>
                                    <br />
                                    <br />
                                    <br />
                                      <br />
                                    <br />
                                         <section class="col col-4">
                                        Horas de arresto <a style="color: red">*</a>
                                        <label class="input">
                                            
                                            <input title="" type="text" name="horaArresto" id="horaArresto" placeholder="Horas de arresto"  pattern="^[0-9]*$" maxlength="256" class="alphanumeric" style="width: 255px" />
                                            <b class="tooltip tooltip-bottom-right">Ingrese la(s) hora(s) de arresto.</b>
                                        </label>
                                    </section>
                                    

                                 
                                </div>
                                
                            </fieldset>
                            <footer>
                                <a class="btn btn-sm btn-default" href="motivo.aspx"><i class="fa fa-close"></i>&nbsp;Cancelar </a>                                
                                <a class="btn btn-sm btn-default save"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
        </section>
    <input type="hidden" id="hideid" runat="server" />
    <input type="hidden" id="hide"   runat="server" />
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/dataTables.checkboxes.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/moment/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            pageSetUp();                                  

            $("body").on("click", ".clear", function () {
                $("#ctl00_contenido_lblMessage").html("");
                $('#ctl00_contenido_hideid').val("");
                $('#ctl00_contenido_hide').val("");
                $("#ctl00_contenido_usuario").val("");
                $("#ctl00_contenido_nombre").val("");
                $("#ctl00_contenido_paterno").val("");
                $("#ctl00_contenido_materno").val("");
                $("#ctl00_contenido_email").val("");                                                                
            });

            $("body").on("click", ".save", function () {
                $("#ctl00_contenido_lblMessage").html("");
                if (validar()) {
                    guardar();
                }
            });            

             function CargarContrato(trackingid) {
                 startLoading();
                 $.ajax({
                     type: "POST",
                     url: "MotivoEdit.aspx/getContract",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: JSON.stringify({
                         trackingid: trackingid,
                     }),
                     cache: false,
                     success: function (data) {
                         data = data.d;                         
                         loadCustomers(data.ClienteId);                         
                         $("#motivo").val(data.Motivo);
                         $("#descripcion").val(data.Descripcion);                         
                         $("#articulo").val(data.Articulo); 
                         $("#multa").val(data.Multa); 
                         $("#horaArresto").val(data.horaArresto); 
                         $('#ctl00_contenido_hideid').val(data.Id);
                        
                         $('#ctl00_contenido_hide').val(data.TrackingId);
                        
                         $('#main').waitMe('hide');
                    },
                    error: function () {
                        $('#main').waitMe('hide');
                        ShowError("¡Error!", "No fue posible cargar la información del usuario. Si el problema persiste, por favor, consulte al personal de soporte técnico.");
                    }
                });
            }

                 $(document).on('keydown', 'input[pattern]', function(e){
              var input = $(this);
              var oldVal = input.val();
              var regex = new RegExp(input.attr('pattern'), 'g');

              setTimeout(function(){
                var newVal = input.val();
                if(!regex.test(newVal)){
                  input.val(oldVal); 
                }
              }, 0);
            });

            function validar() {
                var esvalido = true;

                if ($("#motivo").val() == "") {
                    ShowError("motivo", "El motivo de detención es obligatorio.");
                    $('#motivo').parent().removeClass('state-success').addClass("state-error");
                    $('#motivo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#motivo').parent().removeClass("state-error").addClass('state-success');
                    $('#motivo').addClass('valid');
                }

                 if ($("#descripcion").val() == "") {
                    ShowError("descripción", "El campo descripción es obligatorio.");
                    $('#descripcion').parent().removeClass('state-success').addClass("state-error");
                    $('#descripcion').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#descripcion').parent().removeClass("state-error").addClass('state-success');
                    $('#descripcion').addClass('valid');
                }
                //else {
                //    $("#motivo").select2({ containerCss: { "border-color": "#bdbdbd" } });
                //}

                if ($("#articulo").val() == "") {
                    ShowError("articulo", "El artículo es obligatorio.");
                    $('#articulo').parent().removeClass('state-success').addClass("state-error");
                    $('#articulo').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#articulo').parent().removeClass("state-error").addClass('state-success');
                    $('#articulo').addClass('valid');
                }

                if ($("#multa").val() == "") {
                    ShowError("multa", "el campo multa es obligatorio.");
                    $('#multa').parent().removeClass('state-success').addClass("state-error");
                    $('#multa').removeClass('valid');
                    esvalido = false;
                }
                else {
                    $('#multa').parent().removeClass("state-error").addClass('state-success');
                    $('#multa').addClass('valid');
                }

                if ($('#horaArresto').val()== "")
                {
                    ShowError("hora de arresto", "el campo tiene datos invalidos, capture numero.")
                }
                if (isNaN($('#horaArresto').val()) == true) {
                    ShowError("hora de arresto", "el campo tiene datos invalidos, capture numero.")
                  esvalido = false;

                }
                else
                {
                    $('#horaArresto').parent().removeClass("state-error").addClass('state-success');
                    $('#horaArresto').addClass('valid');
                }


               

                return esvalido;
            }
            
            function guardar() {
                startLoading();
                
                var contrato = ObtenerValores();
                
                $.ajax({
                    type: "POST",
                    url: "MotivoEdit.aspx/save",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    data: JSON.stringify({ 'contrato': contrato }),
                    success: function (data) {
                        var resultado = data.d;
                        if (resultado.exitoso) {
                            $('#ctl00_contenido_hideid').val(resultado.Id);
                            $('#ctl00_contenido_hide').val(resultado.TrackingId);
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>x</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho! </strong>" +
                                "La información del motivo de detencion se  " + resultado.mensaje + " correctamente.", "<br /></div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "La información del  motivo de detencion se  " + resultado.mensaje + " correctamente.");                            
                            $('#main').waitMe('hide');
                        }
                        else {
                            $('#main').waitMe('hide');
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error! </strong>" +
                                "Algo salió mal. " + resultado.mensaje + "</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal", "Si el problema persiste, contacte al personal de soporte técnico. " + resultado.mensaje);
                        }
                    }
                });
            }

            function ObtenerValores() {                                
                var contrato = {                    
                    Motivo: $('#motivo').val(),
                    Descripcion: $('#descripcion').val(),
                    Articulo: $('#articulo').val(),                    
                    Multa: $('#multa').val(),
                    horaArresto: $('#horaArresto').val(),
                    Id: $('#ctl00_contenido_hideid').val(),
                    TrackingId:$('#ctl00_contenido_hide').val()
                };
                return contrato;
            }

            function init() {
                loadCustomers("");
                var param = RequestQueryString("tracking");
                if (param != undefined) {                    
                    CargarContrato(param);                    
                }
            }

            function loadCustomers(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "contrato_edit.aspx/getCustomers",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#cliente');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Cliente]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");                            
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de diarios. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            init();

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
