﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="contrato.aspx.cs" Inherits="Web.Application.Admin.contrato" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
    <li>Configuración</li>    
    <li>Contratos y subcontratos</li>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
    <style type="text/css">
        td.strikeout {
            text-decoration: line-through;
        }
    
    </style>
    <link href="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/css/bootstrap.css" rel="stylesheet"/>
    <div class="scroll">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="icon-append fa fa-archive"></i>
                Catálogo de contratos y subcontratos
            </h1>
        </div>
    </div>    
     <p></p>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>    
    <section id="widget-grid" class="">                                    
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-cou-datatable" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-fullscreenbutton="flase" data-widget-collapsed ="false" data-widget-togglebutton="false">
                    <header>
                        <span class="widget-icon" id="icongrid"><i class="icon-append fa fa-archive"></i></span>
                        <h2 id="titlegrid">Contratos</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body"">                            
                            <div id="registrer" class="smart-form">
                                <fieldset>
                                    <div class="row">                                        
                                        <a href="contrato_edit.aspx" class="btn btn-sm btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>                                            
                                    </div>
                                    <br />
                                    <div class="row">
                                        <section class="col-md-3">
                                             <label>Cliente</label>
                                             <select name="cliente" id="cliente" style="width:100%" class="select2"></select>  
                                         </section>
                                        <section class="col-md-9">
                                            <br />
                                            &nbsp;&nbsp;<a class="btn btn-sm btn-default search" id="search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                        </section>
                                    </div>
                                 </fieldset>
                            </div>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Cliente</th>
                                        <th data-hide="phone,tablet">Contrato</th>          
                                        <th data-hide="phone,tablet">Institución</th>
                                        <th data-hide="phone,tablet">Acciones</th>                                          
                                    </tr>
                                </thead>
                            </table>                                                      
                        </div>
                    </div>
                </div>
            </article>
        </div>
        </section>
        </div>
        <%--<div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"><i class="icon-append fa fa-archive"></i></span>
                        <h2>Subcontratos</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox"></div>
                        <div class="widget-body"">                            
                            <div class="smart-form">         
                                <fieldset>
                                    <div class="row">                                        
                                        <a href="subcontrato_edit.aspx" class="btn btn-sm btn-default add" id="addSub"><i class="fa fa-plus"></i>&nbsp;Agregar </a>                                            
                                    </div>
                                    <br />
                                </fieldset>                                
                            </div>
                            <table id="dt_basic_subcontratos" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Cliente</th>
                                        <th data-class="expand">Contrato</th>
                                        <th data-hide="phone,tablet">SubContrato</th>          
                                        <th data-hide="phone,tablet">Institución</th>
                                        <th data-hide="phone,tablet">Acciones</th>                                          
                                    </tr>
                                </thead>
                            </table>                                                      
                        </div>
                    </div>
                </div>
            </article>
        </div>--%>
    <div id="blockuser-modal" class="modal fade" tabindex="-1" data-keyboard="true" >
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verb"></span></strong>&nbsp;el registro de <strong>&nbsp<span id="usernameblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <div class="row" style="display: inline-block; float: right; margin: 0px;">     
                                <a  style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" id="btncontinuar"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;                            
                                <a  style="float: none;" href="javascript:void(0);" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                             </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="blocksub-modal" class="modal fade" data-keyboard="true"  tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Confirmación
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                    ¿Está seguro de <strong><span id="verbSub"></span></strong>&nbsp;el registro <strong>&nbsp<span id="subblock"></span></strong>?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                            <a class="btn btn-sm btn-default" id="btncontinuarSub"><i class="fa fa-save bigger-120"></i>&nbsp;Continuar</a>&nbsp;
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="HQLNBB" runat="server" value="" />
    <input type="hidden" id="KAQWPK" runat="server" value="" />
    <input type="hidden" id="LCADLW" runat="server" value=""/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
    <script type="text/javascript">
        window.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.keyCode === 71) {
                console.log("ctl+g");
                e.preventDefault();

                document.getElementsByClassName("save")[0].click();
            }
        });
        $(document).ready(function () {
            pageSetUp();            

            //Variables tabla de contratos
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;

            //Variables tabla de subcontratos
            var responsiveHelper_dt_basic_sub = undefined;
            var responsiveHelper_datatable_fixed_column_sub = undefined;
            var responsiveHelper_datatable_col_reorder_sub = undefined;
            var responsiveHelper_datatable_tabletools_sub = undefined;

            var breakpointDefinition = {
                tablet: 1024,
                phone: 480
            };

            window.emptytable = false;

            window.table = $('#dt_basic').dataTable({
                "lengthMenu": [10, 20, 50, 100],
                iDisplayLength: 10,
                serverSide: true,
                fixedColumns: true,
                autoWidth: true,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "preDrawCallback": function () {
                    if (!responsiveHelper_dt_basic) {
                        responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                    }
                },
                "rowCallback": function (nRow) {
                    responsiveHelper_dt_basic.createExpandIcon(nRow);
                },
                "drawCallback": function (oSettings) {
                    responsiveHelper_dt_basic.respond();
                    $('#dt_basic').waitMe('hide');
                },
                "createdRow": function (row, data, index) {
                    if (!data["Habilitado"]) {
                        $('td', row).eq(0).addClass('strikeout');
                        $('td', row).eq(1).addClass('strikeout');
                        $('td', row).eq(2).addClass('strikeout');                                                
                    }
                },
                ajax: {
                    type: "POST",
                    url: "contrato.aspx/getContracts",
                    contentType: "application/json; charset=utf-8",
                    data: function (parametrosServerSide) {
                        $('#dt_basic').waitMe({
                            effect: 'bounce',
                            text: 'Cargando...',
                            bg: 'rgba(255,255,255,0.7)',
                            color: '#000',
                            sizeW: '',
                            sizeH: '',
                            source: ''
                        });                       
                        parametrosServerSide.emptytable = false;
                        parametrosServerSide.clienteId = $("#cliente").val();
                        return JSON.stringify(parametrosServerSide);
                    }
                   
                },
                columns: [
                    {
                        name: "Cliente",
                        data: "Cliente"
                    },
                    {
                        name: "Contrato",
                        data: "Contrato"
                    },                    
                    {
                        name: "Nombre",
                        data: "Nombre"
                    },
                    null
                ],
                columnDefs: [
                    {
                        targets: 3,
                        orderable: false,
                        render: function (data, type, row, meta) {                            
                            var txtestatus = "";
                            var icon = "";
                            var color = "";
                            var edit = "edit";
                            //var editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="contrato_edit.aspx?tracking=' + row.TenantId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            var editar = '<a class="btn btn-primary btn-circle ' + edit +"' data-TenantId='"+ row.TenantId+ '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';

                            var habilitar = "";
                            if (!row.Habilitado) {
                                txtestatus = "Habilitar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                                
                            }
                            else {
                                
                                txtestatus = "Deshabilitar"; icon = "ban-circle"; color = "danger";
                            }
                            if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="contrato_edit.aspx?tracking=' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
                            if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blockuser" href="javascript:void(0);" data-value="' + row.Contrato + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

                            return  editar+habilitar
                               ;
                        }
                    }
                ]
              
            });
               $("body").on("click", ".edit", function () {
                   var redirect = "";
                 
                redirect="contrato_edit.aspx?tracking=" + $(this).attr("data-TenantId");  ;
                window.location.assign(redirect); 
            });
            //$('#dt_basic_subcontratos').dataTable({
            //    "lengthMenu": [10, 20, 50, 100],
            //    iDisplayLength: 10,
            //    serverSide: true,
            //    fixedColumns: true,
            //    autoWidth: true,
            //    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            //        "t" +
            //        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            //    "autoWidth": true,
            //    "oLanguage": {
            //        "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
            //    },
            //    "preDrawCallback": function () {
            //        if (!responsiveHelper_dt_basic_sub) {
            //            responsiveHelper_dt_basic_sub = new ResponsiveDatatablesHelper($('#dt_basic_subcontratos'), breakpointDefinition);
            //        }
            //    },
            //    "rowCallback": function (nRow) {
            //        responsiveHelper_dt_basic_sub.createExpandIcon(nRow);
            //    },
            //    "drawCallback": function (oSettings) {
            //        responsiveHelper_dt_basic_sub.respond();
            //        $('#dt_basic_subcontratos').waitMe('hide');
            //    },
            //    "createdRow": function (row, data, index) {
            //        if (!data["Habilitado"]) {
            //            $('td', row).eq(0).addClass('strikeout');
            //            $('td', row).eq(1).addClass('strikeout');
            //            $('td', row).eq(2).addClass('strikeout');
            //            $('td', row).eq(3).addClass('strikeout');                        
            //        }
            //    },
            //    ajax: {
            //        type: "POST",
            //        url: "contrato.aspx/getSubcontracts",
            //        contentType: "application/json; charset=utf-8",
            //        data: function (parametrosServerSide) {
            //            $('#dt_basic_subcontratos').waitMe({
            //                effect: 'bounce',
            //                text: 'Cargando...',
            //                bg: 'rgba(255,255,255,0.7)',
            //                color: '#000',
            //                sizeW: '',
            //                sizeH: '',
            //                source: ''
            //            });                       
            //            parametrosServerSide.emptytable = false;
            //            parametrosServerSide.clienteId = $("#cliente").val();
            //            return JSON.stringify(parametrosServerSide);
            //        }
                   
            //    },
            //    columns: [
            //        {
            //            name: "Cliente",
            //            data: "Cliente"
            //        },
            //        {
            //            name: "Contrato",
            //            data: "Contrato"
            //        },                    
            //        {
            //            name: "Subcontrato",
            //            data: "Subcontrato"
            //        },
            //        {
            //            name: "Nombre",
            //            data: "Nombre"
            //        },
            //        null
            //    ],
            //    columnDefs: [
            //        {
            //            targets: 4,
            //            orderable: false,
            //            render: function (data, type, row, meta) {                            
            //                var txtestatus = "";
            //                var icon = "";
            //                var color = "";
            //                var edit = "edit";
            //                var editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="subcontrato_edit.aspx?tracking=' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
            //                var habilitar = "";
            //                if (!row.Habilitado) {
            //                    txtestatus = "Activar"; icon = "ok-circle"; color = "success"; edit = "disabled";
                                
            //                }
            //                else {
                                
            //                    txtestatus = "Bloquear"; icon = "ban-circle"; color = "danger";
            //                }
            //                if ($("#ctl00_contenido_KAQWPK").val() == "true") editar = '<a class="btn btn-primary btn-circle ' + edit + '" href="subcontrato_edit.aspx?tracking=' + row.TrackingId + '" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;';
            //                if ($("#ctl00_contenido_LCADLW").val() == "true") habilitar = '<a class="btn btn-' + color + ' btn-circle blocksub" href="javascript:void(0);" data-value="' + row.Subcontrato + '" data-id="' + row.TrackingId + '" title="' + txtestatus + '" style=' + txtestatus + '><i class="glyphicon glyphicon-' + icon + '"></i></a>';

            //                return  editar+habilitar
            //                   ;
            //            }
            //        }
            //    ]
              
            //});

            $("body").on("click", ".search", function () {
                window.emptytable = false;
                window.table.api().ajax.reload();
                $("#dt_basic_subcontratos").emptytable = false;
                $("#dt_basic_subcontratos").DataTable().table().ajax.reload();
            });

            $("body").on("click", ".clear", function () {
                $('#ctl00_contenido_perfil').val("0");
            });

            $("body").on("click", ".blockuser", function () {
                var usernameblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#usernameblock").text(usernameblock);
                $("#verb").text(verb);
                $("#btncontinuar").attr("data-id", $(this).attr("data-id"));
                $("#blockuser-modal").modal("show");
            });   

            $("body").on("click", ".blocksub", function () {
                var subblock = $(this).attr("data-value");
                var verb = $(this).attr("style");
                $("#subblock").text(subblock);
                $("#verbSub").text(verb);
                $("#btncontinuarSub").attr("data-id", $(this).attr("data-id"));
                $("#blocksub-modal").modal("show");
            });

            $("#btncontinuar").unbind("click").on("click", function () {
                var trackingid = $(this).attr("data-id");
                startLoading();
                $.ajax({
                    url: "contrato.aspx/blockContract",
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        trackingid: trackingid,
                    }),
                    success: function (data) {
                        if (JSON.parse(data.d).exitoso) {
                            window.emtytable = false;
                            window.table.api().ajax.reload();
                           
                            $("#blockuser-modal").modal("hide");
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
                                " Registro " + JSON.parse(data.d).message + " con éxito.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowSuccess("¡Bien hecho!", "Registro " + JSON.parse(data.d).message + " con éxito.");
                        }
                        else {
                            $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>¡Error!</strong>" +
                                "Algo salió mal y no fue posible afectar el estatus del contrato. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
                            setTimeout(hideMessage, hideTime);
                            ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del contrato. . Si el problema persiste, contacte al personal de soporte técnico.");
                        }
                        $('#main').waitMe('hide');
                    }
                });
            });

            //$("#btncontinuarSub").unbind("click").on("click", function () {
            //    var trackingid = $(this).attr("data-id");
            //    startLoading();
            //    $.ajax({
            //        url: "contrato.aspx/blockSubcontract",
            //        type: 'POST',
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        data: JSON.stringify({
            //            trackingid: trackingid,
            //        }),
            //        success: function (data) {
            //            if (JSON.parse(data.d).exitoso) {
            //                $("#dt_basic_subcontratos").emptytable = false;
            //                $("#dt_basic_subcontratos").DataTable().table().ajax.reload();
                           
            //                $("#blocksub-modal").modal("hide");
            //                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-success fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-check'></i><strong>¡Bien hecho!</strong>" +
            //                  " El registro se " + JSON.parse(data.d).message + " correctamente.</div>");
            //                ShowSuccess("¡Bien hecho!", "El registro se " + JSON.parse(data.d).message + " correctamente.");
            //            }
            //            else {
            //                $("#ctl00_contenido_lblMessage").html("<div class='alert alert-danger fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-times'></i><strong>Error!</strong>" +
            //                    "Algo salió mal y no fue posible afectar el estatus del subcontrato. Si el problema persiste, por favor, consulte al personal de soporte técnico.</div>");
            //                ShowError("¡Error! Algo salió mal y no fue posible afectar el estatus del subcontrato. . Si el problema persiste, contacte al personal de soporte técnico.");
            //            }
            //            $('#main').waitMe('hide');
            //        }
            //    });
            //});

            loadCustomers("0");

            function loadCustomers(setvalue) {
                $.ajax({
                    type: "POST",
                    url: "contrato.aspx/getCustomers",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        var Dropdown = $('#cliente');
                        Dropdown.children().remove();
                        Dropdown.append(new Option("[Cliente]", "0"));
                        $.each(response.d, function (index, item) {
                            Dropdown.append(new Option(item.Desc, item.Id));
                        });

                        if (setvalue != "") {
                            Dropdown.val(setvalue);
                            Dropdown.trigger("change.select2");                            
                        }
                    },
                    error: function () {
                        ShowError("¡Error!", "Ocurrió un error. No fue posible cargar la lista de clientes. Si el problema persiste contacte al soporte técnico del sistema.");
                    }
                });
            }

            init();

            function init() {
                if ($("#ctl00_contenido_HQLNBB").val() == "true") {
                    $("#addentry").show();
                }
            }

            var hideTime = 5000;
            function hideMessage() {
                $("#ctl00_contenido_lblMessage").html("");
            }

        });
    </script>
</asp:Content>
