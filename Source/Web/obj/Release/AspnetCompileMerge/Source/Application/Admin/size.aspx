﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Application/Shared/Main.Master" AutoEventWireup="true" CodeBehind="size.aspx.cs" Inherits="Web.Application.Admin.size" %>
<asp:Content ID="Content1" ContentPlaceHolderID="breadcrums" runat="server">
     <li>Configuración</li>
     <li>Tamaño</li>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contenido" runat="server">
     <style>
        td.strikeout {
            text-decoration: line-through;
        }
    </style>
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-8">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-info-circle"></i>
                Tamaño
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
             <a href="javascript:void(0);" class="btn btn-md btn-default add" id="add"><i class="fa fa-plus"></i>&nbsp;Agregar </a>
        </div>
    </div>
    <p></p>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hide">
                <div class="jarviswidget" id="wid-size-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-search"></i></span>
                        <h2>Buscar </h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <div id="smart-form-register" class="smart-form">
                                <header>
                                    Criterios de búsqueda
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-newspaper-o"></i>
                                                <input type="text" name="buscarnopoliza" runat="server" id="buscarnopoliza" placeholder="No. de póliza" maxlength="20" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input type="text" name="buscarfechainicio" runat="server" id="buscarfechainicio" placeholder="Fecha inicio" class="datepicker" data-dateformat='dd/mm/yy'  />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input type="text" name="buscarfechafin" runat="server" id="buscarfechafin" placeholder="Fecha término" class="datepicker" data-dateformat='dd/mm/yy'  />
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <footer>
                                    <a class="btn bt-sm btn-default clear"><i class="fa fa-eraser"></i>&nbsp;Limpiar </a>
                                    <a class="btn bt-sm btn-default search"><i class="fa fa-search"></i>&nbsp;Buscar </a>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-size-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-sortable="false" data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"><i class="fa fa-info-circle"></i></span>
                        <h2>Tamaño</h2>
                    </header>
                    <div>
                        <div class="jarviswidget-editbox">
                        </div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Descripcion</th>
                                        <th data-hide="phone,tablet" style="width: 10%">Acciones</th>
                                    </tr>
                                    <tr>
                                        <td>Datos</td>
                                        <td><a class="btn btn-primary btn-circle edit" href="javascript:void(0);" title="Editar"><i class="glyphicon glyphicon-pencil"></i></a> 
                                            <a class="btn btn-danger btn-circle delete" href="javascript:void(0);" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></a>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" data-keyboard="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Tamaño
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="register-form" class="smart-form">
                        <fieldset>
                            <section>
                                <div class="row">
                                    <div>
                                        <section>
                                            <label class="input">
                                                <i class="icon-append fa fa-pencil"></i>
                                                <input type="text" name="descripcion" runat="server" id="descripcion" class="input-descripcion" placeholder="Descripción" maxlength="20" title="Descripcion" />
                                                <b class="tooltip tooltip-bottom-right">Descripcón.</b>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default cancel" data-dismiss="modal" id="btncancel"><i class="fa fa-close"></i>&nbsp;Cancelar </a>
                            <a class="btn btn-sm btn-default save" id="btnsave"><i class="fa fa-save"></i>&nbsp;Guardar </a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="delete-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Cancelar tamaño
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="x" class="smart-form">
                        <fieldset>
                            <section>
                                <p class="center">
                                   El tamaño<strong><span id="apelacioneliminar"></span></strong>&nbsp;será cancelado. ¿Está seguro y desea continuar?
                                </p>
                            </section>
                        </fieldset>
                        <footer>
                            <a class="btn btn-sm btn-default" id="btndelete"><i class="fa fa-trash bigger-120"></i>&nbsp;Aceptar</a>&nbsp;
                            <a class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times bigger-120"></i>&nbsp;Cancelar</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="history-modal" class="modal fade" tabindex="-1" data-keyboard="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Historial <span id="descripcionhistorial"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="history" class="row table-responsive">
                        <div class="col-sm-12">
                            <table id="dt_basichistory" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th data-class="expand">Descripción</th>
                                        <th data-hide="tablet,fablet,phone">Fecha movimiento</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="idhistory" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["relativepath"]  %>Content/js/Utilities.js"></script>
     <script type="text/javascript">
         $(document).ready(function () {

             $("body").on("click", ".add", function () {
                 $("#ctl00_contenido_lblMessage").html("");

                 $("#form-modal").modal("show");
             });

             $("body").on("click", ".edit", function () {
                 $("#ctl00_contenido_lblMessage").html("");

                 $("#form-modal").modal("show");
             });

             $("body").on("click", ".delete", function () {

                 $("#delete-modal").modal("show");
             });
         });
     </script>
</asp:Content>
