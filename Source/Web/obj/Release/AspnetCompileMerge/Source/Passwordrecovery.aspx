﻿<%@ Page Language="C#" AutoEventWireup="false" Async="true" CodeBehind="Passwordrecovery.aspx.cs" Inherits="Web.Passwordrecovery" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es-mx" id="extr-page" style="background-color: black;">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title><%= ConfigurationManager.AppSettings["Titulo"].ToString()  %></title>
    <meta name="author" content="David Alvarado" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="robots" content="noindex, nofollow" />

    <style>
    body, html {
    font-family: calibri, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    height:100%;
    margin: 0;    
    width: auto!important;
    overflow-x: hidden!important;    
}

.container {    
    background-image: url('<%= ConfigurationManager.AppSettings[" relativepath "]  %>Content/img/background.png');
    max-width:100%;
    height: 100%;
    display: flex;
    background-size: cover;
    flex-direction: column;
    flex-wrap: wrap;
    flex-grow: 1;
}

@media only screen 
              and (min-width: 768px) 
            {
                .container{
                    background-image: url('<%= ConfigurationManager.AppSettings[" relativepath "]  %>Content/img/bg-1366x768.png');
                }
            }


           @media only screen and (min-width: 400px)
            {
                .container{
                    background-image: url('<%= ConfigurationManager.AppSettings[" relativepath "]  %>Content/img/bg-768x1024.png');
                }
            }

            @media only screen 
              and (min-width: 1024px) 
            {
                .container{
                    background-image: url('<%= ConfigurationManager.AppSettings[" relativepath "]  %>Content/img/bg-1600x900.png');
                }
            }


             @media only screen 
              and (min-width: 1920px) 
            {
                .container{
                    background-image: url('<%= ConfigurationManager.AppSettings[" relativepath "]  %>Content/img/bg-1920x1080.png');
                }
            }

.row{
    flex: 1 0 auto;
    display: flex;    
    flex-direction: column;
    height: auto;
}

.column{
    flex: 1 0 auto;    
    flex-direction: row;
    margin: 0 auto;
    text-align: center;
    max-width:100%;
    height:auto;
}

.logo {
    padding-top: 3vh;
    align-self: flex-end;
    margin-right: 1.2em;
    max-width:20%;
    max-height:20%;
    float: right;
}

.encabezado{           
    text-transform: uppercase;
    color:#00b3e2;
    background-color: rgba(100, 100, 100, 0.7);
    border-radius: 50px;
    width: 100%;
    padding: 10px;
    font-weight: bold;
    font-size: 1.5vw;
}

.form-wrap {
    margin: 0 auto;
    width: 50%;      
    text-align: center;    
    max-width: 18vw;
    height: auto;
}

h2 {
    color: white;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 1.3vw;
}

hr{
    width: 10vw;
    color: #60656b;
}

.form-wrap form .form-background {    
    background-color: rgba(100, 100, 100, 0.7);
    margin-top: 1vh;
    padding: 20px;
    padding-top: 0;
    max-width: 100%;
    text-align: center;    
}

.form-wrap form input {
    background-color: transparent;
    border: none;
    padding: 5px;
    color: #fff;
    border-bottom: 1px solid #a4a6aa;
    margin-top: 1.2vw;
    margin-bottom: 1vw;
    max-width: 100%;
    font-size: 1.5vh;
    
}

.buttons {
    margin-top: .5vw;
    padding-top: 20px;
    text-align: center;    
}

.btn {
    background-color: #60656b;
    text-transform: uppercase;
    border: none;    
    padding-top: 1vw;
    padding-bottom: 1vw;
    padding-right: 5vw;
    padding-left: 5vw;
    color: white;
    font-weight: bold;
    font-size: 1vw;    
    margin-top: 2vh;    
}

.btn.primary {
    background-color: #2d6296;
    padding-top: 1vw;
    padding-bottom: 1vw;
    padding-right: 9vw;
    padding-left: 7vw;
    border: none;
    text-align:center;
}

.footer {    
    position: fixed;
    left: 0;
    bottom: 0;   
    color: #fff;
    font-size: 1vw;
    max-width: 100%;
    text-align: right;
    padding-left: 37vw;
}

.alert {
    font-size: 1vw;
}

@media only screen and (max-width: 812px) {
   .encabezado{           
        font-size: 4vw;
    }

   h2 {
        font-size: 3.7vw;
    }

   .container {    
        background-size: cover;
        background-position: center;    
    }

   .footer {   
        position: fixed;
        bottom: 20vw;           
        width: 100%;
        font-size: 4vw;
        text-align: center;
        padding-left: 0;
    }

   .form-wrap {
        margin-left: 25%;
        width: 50%;      
        text-align: center;    
        max-width: 100%;
        height: auto;
    }

   .form-wrap form .form-background {
        margin: auto;
        padding: 60px;
        padding-bottom: 15px;
        padding-top: 0;
        width: 20vw;
    }

    .form-wrap form input {
        margin-top: 1.2vw;
        margin-bottom: 1vw;
        font-size: 1.5vh;   
        padding-bottom: 8px;
        padding-right:25vw;
        width: 20vw;
        position: relative;
        right: 50px;
    }

    .buttons {
    margin-top: .5vw;
    padding-top: 20px;
    text-align: center;    
}

.btn {
        background-color: #60656b;
        text-transform: uppercase;
        border: none;    
        padding-top: 1vw;
        padding-bottom: 1vw;
        padding-right: 17vw;
        padding-left: 17vw;
        color: white;
        font-weight: bold;
        font-size: 4vw;    
        margin-top: 2vh;    
    }

.btn.primary {
        background-color: #2d6296;
        padding-top: 1vw;
        padding-bottom: 1vw;
        margin-left: 14.5vw;
        border: none;
        text-align:center;
        width: 50vw;
    }

.alert {
    font-size: 2.5vw;

}
}
</style>

    <link rel="shortcut icon" href="Content/img/favicon/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="Content/img/favicon/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700" />
    <link rel="apple-touch-icon" href="Content/img/splash/sptouch-icon-iphone.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="Content/img/splash/touch-icon-ipad.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="Content/img/splash/touch-icon-iphone-retina.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="Content/img/splash/touch-icon-ipad-retina.png" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <link rel="apple-touch-startup-image" href="Content/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
    <link rel="apple-touch-startup-image" href="Content/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
    <link rel="apple-touch-startup-image" href="Content/img/splash/iphone.png" media="screen and (max-device-width: 320px)" />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="column logo">
                <img src='<%= ConfigurationManager.AppSettings[" relativepath "]  %>Content/img/logotipo.png' style="max-width:100%;height:auto;"/>
            </div>
            <div class="column">
                <h3 class="encabezado">
                    Software de control de detenidos
                </h3>
                <h2>Recuperar contraseña</h2>
            </div>
        </div>
        <div class="row">
            <div class="form-wrap">                
                <form runat="server">
                    <div class="form-background">
                        <div class="form-group">
                             <asp:TextBox ID="email" runat="server" MaxLength="50" CssClass="form-control input-email alptext" ForeColor="#ffffff"></asp:TextBox></br>
                             <asp:RequiredFieldValidator CssClass="alert" ID="rfvEmail" ErrorMessage="Correo electrónico requerido" ControlToValidate="email" runat="server" ForeColor = "red" Display = "Dynamic" ValidationGroup="InfoUsuario"/>
                        </div>               

                    </div>

                    <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red"></asp:Label>

                    <asp:Label ID="lblMsg1" runat="server" Text="" ForeColor="Green"></asp:Label>

                    <div class="buttons">
                        <div class="form-group">
                             <asp:Button ID="btnenviar" CssClass="btn primary" runat="server" Text="Enviar" EnableDefaultAppearance="False" ValidationGroup="InfoUsuario"  OnClick="btnRecuperar_Click"/> <br />
                        </div>
                        <br> <br />
                        <div class="form-group">
                            <a class="btn" style="text-decoration: none; color: #fff" href="Login.aspx">Volver</a>
                        </div>                        
                </div>                     
                </form>                
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="footer">
                    <p>Ingresando acepta nuestros <b>Términos</b> y <b>Política de privacidad.</b></p>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("#email").prop("placeholder", "Email");

     
    </script>

    <script src="Content/js/plugin/pace/pace.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script> if (!window.jQuery) { document.write('<script src="Content/js/libs/jquery-2.1.1.min.js"><\/script>'); } </script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script> if (!window.jQuery.ui) { document.write('<script src="Content/js/libs/jquery-ui-1.10.3.min.js"><\/script>'); } </script>
    <script src="Content/js/app.config.js"></script>
    <script src="Content/js/bootstrap/bootstrap.min.js"></script>
    <script src="Content/js/plugin/jquery-validate/jquery.validate.min.js"></script>
    <script src="Content/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <script src="Content/js/app.min.js"></script>
</body>
</html>


