﻿using Business;
using System;
using System.Web;
using System.Web.Security;

namespace Web
{
    public partial class Signout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = "";
            var current_user = HttpContext.Current.User.Identity.Name;
            if (current_user != null)
            {
                var usId = Membership.GetUser().ProviderUserKey;
                if (usId != null)
                {
                    var login = ControlUsuario_login.ObtenerPorusuarioId(Convert.ToInt32(usId.ToString()));
                    if (login != null)
                    {
                        login.Activo = false;
                        ControlUsuario_login.Actualizar(login);
                    }

                }
                if (Request.QueryString["url"] != null)
                {

                    url = Request.QueryString["url"];
                    url = "?ReturnUrl=" + url.Replace("/", "%2f");

                }
            }
           

            Session.Abandon();

            FormsAuthentication.SignOut();

            System.Web.HttpContext.Current.Session.RemoveAll();
            System.Web.HttpContext.Current.Response.Redirect("Login.aspx" + url);

        }
    }
}