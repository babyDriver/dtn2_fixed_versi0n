﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DT
{
    public class QueryAux
    {
        public List<string> select
        {
            get;
            set;
        }

        public Table from
        {
            get;
            set;
        }

        public List<Join> joins
        {
            get;
            set;
        }

        public List<Where> wheres
        {
            get;
            set;
        }

        public string groupBy
        {
            get;
            set;
        }

        public string orderBy
        {
            get;
            set;
        }

        public string limit
        {
            get;
            set;
        }

        public string offset
        {
            get;
            set;
        }


        public QueryAux(List<string> select, Table from, List<Join> joins = null, List<Where> wheres = null, string groupBy = null, string orderBy = null, string limit = null, string offset = null)
        {
            this.select = select;
            this.from = from;
            this.joins = joins;
            this.wheres = wheres;
            this.groupBy = groupBy;
            this.orderBy = orderBy;
            this.limit = limit;
            this.offset = offset;
        }

        public QueryAux()
        {
        }

        public string GenerarConsulta()
        {
            int num = 0;
            StringBuilder stringBuilder = new StringBuilder("SELECT * FROM (SELECT ");
            foreach (string item in select)
            {
                stringBuilder.AppendFormat("{0}{1}", (num == 0) ? "" : ",", item);
                num++;
            }

            stringBuilder.AppendFormat(" FROM {0} {1}", from.nombre, (from.alias != null) ? from.alias : "");
            if (joins != null && joins.Count > 0)
            {
                foreach (Join join in joins)
                {
                    if (join.tipo.Contains(" APPLY "))
                    {
                        stringBuilder.AppendFormat(" {0} {1} {2} ", join.tipo, join.relacion, join.tabla.alias);
                        continue;
                    }

                    stringBuilder.AppendFormat(" {0} JOIN {1} {2} ON {3} ", join.tipo, join.tabla.nombre, join.tabla.alias, join.relacion);
                }
            }

            if (wheres != null && wheres.Count > 0)
            {
                stringBuilder.Append(" WHERE ");
                if (wheres != null && wheres.Count > 0)
                {
                    num = 0;
                    List<string> source = new List<string>
                    {
                        "(",
                        "AND",
                        "SELECT"
                    };
                    foreach (Where w in wheres)
                    {
                        if (num > 0)
                        {
                            stringBuilder.Append(" AND ");
                        }

                        stringBuilder.AppendFormat(arg2: (!new List<string>
                        {
                            "BETWEEN",
                            "NOT BETWEEN",
                            "IN",
                            "NOT IN",
                            "EXISTS"
                        }.Contains(w.operador) && !source.Any((string s) => w.operador.ToUpper().Contains(s))) ? ("'" + w.valor + "'") : w.valor, format: "{0} {1} {2}", arg0: w.columna, arg1: w.operador);
                        num++;
                    }
                }
            }

            if (groupBy != null)
            {
                stringBuilder.Append(" GROUP BY  " + groupBy);
            }

            if (orderBy != null)
            {
                stringBuilder.Append(" ORDER BY  " + orderBy);
            }

            if (limit != null)
            {
                stringBuilder.Append(" LIMIT  " + limit);
            }

            if (offset != null)
            {
                stringBuilder.Append(" OFFSET  " + offset);
            }


            stringBuilder.Append(")soft");
            return stringBuilder.ToString();
        }


        public string GenerarConteo()
        {
            int num = 0;
            StringBuilder stringBuilder = new StringBuilder("SELECT * FROM (SELECT ");
            foreach (string item in select)
            {
                stringBuilder.AppendFormat("{0}{1}", (num == 0) ? "" : ",", item);
                num++;
            }

            stringBuilder.AppendFormat(" FROM {0} {1}", from.nombre, (from.alias != null) ? from.alias : "");
            if (joins != null && joins.Count > 0)
            {
                foreach (Join join in joins)
                {
                    if (join.tipo.Contains(" APPLY "))
                    {
                        stringBuilder.AppendFormat(" {0} {1} {2} ", join.tipo, join.relacion, join.tabla.alias);
                        continue;
                    }

                    stringBuilder.AppendFormat(" {0} JOIN {1} {2} ON {3} ", join.tipo, join.tabla.nombre, join.tabla.alias, join.relacion);
                }
            }

            if (wheres != null && wheres.Count > 0)
            {
                stringBuilder.Append(" WHERE ");
                if (wheres != null && wheres.Count > 0)
                {
                    num = 0;
                    List<string> source = new List<string>
                    {
                        "(",
                        "AND",
                        "SELECT"
                    };
                    foreach (Where w in wheres)
                    {
                        if (num > 0)
                        {
                            stringBuilder.Append(" AND ");
                        }

                        stringBuilder.AppendFormat(arg2: (!new List<string>
                        {
                            "BETWEEN",
                            "NOT BETWEEN",
                            "IN",
                            "NOT IN",
                            "EXISTS"
                        }.Contains(w.operador) && !source.Any((string s) => w.operador.ToUpper().Contains(s))) ? ("'" + w.valor + "'") : w.valor, format: "{0} {1} {2}", arg0: w.columna, arg1: w.operador);
                        num++;
                    }
                }
            }

            if (groupBy != null)
            {
                stringBuilder.Append(" GROUP BY  " + groupBy);
            }

            if (orderBy != null)
            {
                stringBuilder.Append(" ORDER BY  " + orderBy);
            }


            stringBuilder.Append(")soft");
            return stringBuilder.ToString();
        }
    }
}