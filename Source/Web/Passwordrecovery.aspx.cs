﻿using System;
using System.Net;
using System.Web.Security;
using System.Web.Services;
using Business;
using System.Configuration;

namespace Web
{
    public partial class Passwordrecovery : System.Web.UI.Page
    {
        protected static string ReCaptcha_Key = ConfigurationManager.AppSettings["ReCaptcha_Key"].ToString();
        protected static string ReCaptcha_Secret = ConfigurationManager.AppSettings["ReCaptcha_Secret"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string VerifyCaptcha(string response)
        {
            string url = "https://www.google.com/recaptcha/api/siteverify?secret=" + ReCaptcha_Secret + "&response=" + response;
            return (new WebClient()).DownloadString(url);
        }

        protected void btnRecuperar_Click(object sender, EventArgs e)
        {
            
            lblMsg1.Text = string.Empty;
            MembershipUser user;
            try
            {
                if (email.Text.Trim().Contains("@"))
                {
                    if (!ControlSmtp.ValidateEmail(email.Text.Trim()))
                    {
                        MostrarMensaje("Correo inválido, verifique por favor", 1);
                        return;
                    }
                    else
                    {
                        var usuario = ControlUsuario.ObtenerPorEmail(email.Text.Trim());
                        user = Membership.GetUser(usuario.User);
                    }
                }
                else
                {
                    user = Membership.GetUser(email.Text.Trim());
                }

               

                if (!user.IsLockedOut)
                {
                    var rol = Roles.GetRolesForUser(user.UserName)[0];
                    //string pass = user.ResetPassword();

                    ControlSmtp.NotificarPorCorreo(user.Comment, user.UserName, user.GetPassword(), rol, user.Email, string.Empty, string.Empty, Entity.TipoDeMensaje.RecuperacionDeContraseña, string.Empty, string.Empty);
                    MostrarMensaje("Los datos de acceso, se enviaron a la cuenta de e-mail asociada a la cuenta de usuario", 2);

                }

                else
                {

                    MostrarMensaje("Lo sentimos, usuario bloqueado, comuniquese con el Administrador", 1);
                }
            }
            catch (Exception ex)
            {
               
                MostrarMensaje("No fue posible comprobar el usuario.", 1);
            }

        }


        protected void MostrarMensaje(string  mensaje, int tipo)
        {
            if (tipo == 1)
            {
                lblMsg.Text = string.Concat("<div class='alert alert-danger'>¡Error! ", mensaje, "</strong><br /></div><br />");
            }

            if (tipo == 2)
            {
                lblMsg1.Text = string.Concat("<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert'><i class='ace-icon fa fa-times'></i></button><strong><i class='ace-icon fa fa-times'></i> ¡Bien hecho! </strong>",
                                             mensaje, "<br /></div><br />");
                lblMsg.Text = string.Empty;
            }
        }


        

    }
}
