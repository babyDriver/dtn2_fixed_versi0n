﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Classes
{
	public class IPHData
	{
		public string referencia {get;set;}
		public string tipo_formato { get; set; }
		public string descripcion_vehiculo { get; set; }
		public string fecha_incidente { get; set; }
		public string dispuesto_por { get; set; }
		public string institución_policiaca { get; set; }
		public string cargo { get; set; }
		public string unidad { get; set; }
		public string autoridad_alterna { get; set; }
		public string adscripcion_alterna { get; set; }
		public string cargo_alterno { get; set; }
		public string origen_evento { get; set; }
		public string telefono_evento { get; set; }
		public string direccion_evento { get; set; }
		public string hechos_evento { get; set; }
		public string latitud_evento { get; set; }
		public string longitud_evento { get; set; }
		public string nombre_detenido { get; set; }
		public string edad_detenido { get; set; }
		public string alias_detenido { get; set; }
		public string fecha_nacimiento_detenido { get; set; }
		public string sexo_de_detenido { get; set; }
		public string nacionalidad_detenido { get; set; }
		public string direccion_detenido { get; set; }
		public string padecimientos_detenido { get; set; }
		public string grupo_vulnerabilidad_detenido { get; set; }
		public string lesiones_detenido { get; set; }
		public string placa_vehiculo { get; set; }
		public string marca_vehiculo { get; set; }
		public string submarca_vehiculo { get; set; }
		public string modelo_vehiculo { get; set; }
		public string color_vehiculo { get; set; }
		public string fecha_retencion_vehiculo { get; set; }
		public string procedencia_vehiculo { get; set; }
		public string tipo_vehiculo { get; set; }
		public string uso_vehiculo { get; set; }
		public string destino_vehiculo { get; set; }
		public string observaciones_vehiculo { get; set; }
		public string seguro_vehiculo { get; set; }


	}
}