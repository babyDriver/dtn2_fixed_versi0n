﻿using Business;
using System;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;
using System.Web;
using System.Web.Security;
using System.Web.Services;


namespace Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected static string ReCaptcha_Key = ConfigurationManager.AppSettings["ReCaptcha_Key"].ToString();
        protected static string ReCaptcha_Secret = ConfigurationManager.AppSettings["ReCaptcha_Secret"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {
                System.Web.HttpContext.Current.Session.RemoveAll();
                FormsAuthentication.SignOut();
            }           
        }

        [WebMethod]
        public static string VerifyCaptcha(string response)
        {

            string url = "https://www.google.com/recaptcha/api/siteverify?secret=" + ReCaptcha_Secret + "&response=" + response;
            return (new WebClient()).DownloadString(url);

        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            //this.lblMsg.Text = "";
            FormsAuthentication.SignOut();

            //if (txtCaptcha.Text.Equals("")) return;

            string usuario = this.email_us.Text;
            string password = this.clave_us.Text;

            try { 
                
                var estatusV=Membership.ValidateUser(usuario, password);
                if (estatusV)
                {
                    //Redireccciona a URL definida en el webconfig en tag <authentication>

                    Entity.Usuario_login userlogin = new Entity.Usuario_login();

                    MembershipUser usrInf = Membership.GetUser(usuario);
                    var user = ControlUsuario.ObtenerPorUsuario(usrInf.UserName);
                    userlogin.Fechahora = DateTime.Now;
                    userlogin.UsuarioId = user.Id;
                    string hostname = Dns.GetHostName();
                    var fecha1 = DateTime.Now;
                    fecha1 = fecha1.AddMinutes(-fecha1.Minute);
                    fecha1 = fecha1.AddSeconds(-fecha1.Second);
                    fecha1 = fecha1.AddMilliseconds(-fecha1.Millisecond);
                    var fecha3 = fecha1.AddHours(-1);
                    var h = fecha1.Hour.ToString();
                    if (fecha1.Hour < 10) h = "0" + h;
                    var m = fecha1.Minute.ToString();
                    if (fecha1.Minute < 10) m = "0" + m;
                    var s = fecha1.Second.ToString();
                    if (fecha1.Second < 10) s = "0" + s;
                    var f = fecha1.ToShortDateString() + " " + h + ":" + m + ":" + s;
                    h = fecha3.Hour.ToString();
                    if (fecha3.Hour < 10) h = "0" + h;
                    m = fecha3.Minute.ToString();
                    if (fecha3.Minute < 10) m = "0" + m;
                    s = fecha3.Second.ToString();
                    if (fecha3.Second < 10) s = "0" + s;
                    var f2 = fecha3.ToShortDateString() + " " + h + ":" + m + ":" + s;
                    var logins = ControlUsuario_login.ObtenerTodos();
                    foreach (var item in logins)
                    {

                        var fecha2 = item.Fechahora;
                        fecha2 = fecha2.AddMinutes(-fecha2.Minute);
                        fecha2 = fecha2.AddSeconds(-fecha2.Second);
                        fecha2 = fecha2.AddMilliseconds(-fecha2.Millisecond);
                        h = fecha2.Hour.ToString();
                        if (fecha2.Hour < 10) h = "0" + h;
                        m = fecha2.Minute.ToString();
                        if (fecha2.Minute < 10) m = "0" + m;
                        s = fecha2.Second.ToString();
                        if (fecha2.Second < 10) s = "0" + s;
                        var f3 = fecha2.ToShortDateString() + " " + h + ":" + m + ":" + s;

                        var valido = false;
                        if (f == f3 || f2 == f3)
                        {
                            valido = true;
                        }
                        if (!valido)
                        {
                            item.Activo = false;
                            ControlUsuario_login.Actualizar(item);
                        }


                    }
                    //   IPAddress[] ip = Dns.GetHostAddresses(hostname);
                    userlogin.IPequipo = GetIPAddress();// ip[1].ToString();
                    userlogin.Activo = true;
                    var login = ControlUsuario_login.ObtenerPorusuarioId(userlogin.UsuarioId);

                    if (login != null)
                    {
                        if (userlogin.IPequipo != login.IPequipo && login.Activo)
                        {
                            throw new Exception("El usuario ya esta en sesión en otro equipo");
                        }
                    }
                    var idlogin = ControlUsuario_login.guardar(userlogin);

                    HttpCookie ckLocal = new HttpCookie("ckLocal");
                    ckLocal["Id"] = userlogin.UsuarioId.ToString();
                    ckLocal["username"] = user.User;
                    ckLocal["fechaCreacion"] = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                    FormsAuthentication.RedirectFromLoginPage(usuario, false);
                    System.Web.HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session["numeroContrato"] = 0;
                    HttpContext.Current.Session["ipusuario"] = userlogin.IPequipo;
                    Response.Cookies.Add(ckLocal);

                    //Sonor@
                    //if (ddl_AppSelector.SelectedValue == "EventosAPP")
                    //{
                    //    HttpCookie ckLocal = new HttpCookie("ckLocal");
                    //    ckLocal["Id"] = userlogin.UsuarioId.ToString();
                    //    ckLocal["username"] = user.User;
                    //    ckLocal["fechaCreacion"] = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                    //    FormsAuthentication.RedirectFromLoginPage(usuario, false);
                    //    System.Web.HttpContext.Current.Session.RemoveAll();
                    //    HttpContext.Current.Session["numeroContrato"] = 0;
                    //    HttpContext.Current.Session["ipusuario"] = userlogin.IPequipo;
                    //    Response.Cookies.Add(ckLocal);
                    //}
                    //else
                    //{
                    //    #region Cookie
                    //    //Se agregan las siguientes lineas para generar una Cookie que contenga la informacion del usuarios
                    //    HttpCookie ckLocal = new HttpCookie("ckLocal");
                    //    ckLocal["Id"] = userlogin.UsuarioId.ToString();
                    //    ckLocal["userName"] = user.User;
                    //    ckLocal["fechaCreacion"] = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                    //    #endregion
                    //    FormsAuthentication.SetAuthCookie(usuario, false);
                    //    System.Web.HttpContext.Current.Session.RemoveAll();
                    //    HttpContext.Current.Session["numeroContrato"] = 0;
                    //    HttpContext.Current.Session["ipusuario"] = userlogin.IPequipo;
                    //    Response.Cookies.Add(ckLocal);
                    //    Response.Redirect(ConfigurationManager.AppSettings["rutaReportesApp"].ToString());                        

                    //}
                }
                else
                {
                    //Hacer reCaptcha vacio, para que nuevamente lo capture.
                 //  txtCaptcha.Text = "";

                    bool muestraMensaje = false;
                    MembershipUser usrInfo = Membership.GetUser(usuario);
                    if (usrInfo != null)
                    {

                        if (usrInfo.IsLockedOut)
                        {
                            MostrarMensaje("La cuenta ha sido bloqueada. Por favor, consulta al personal de soporte técnico.");
                            muestraMensaje = true;
                        }
                        else if (!usrInfo.IsApproved)
                        {
                            MostrarMensaje("Tu cuenta no ha sido aprobada, consulta al personal de soporte técnico.");
                            muestraMensaje = true;
                        }

                        if (!muestraMensaje) 
                        { 

                            MostrarMensaje("Las credenciales de acceso son incorrectas."); 

                        }

                    }
                    else 
                    {

                        MostrarMensaje("Las credenciales de acceso son incorrectas.");

                    }
                }
            }
            catch (Exception ex) {
             
                MostrarMensaje("Error: " + ex.Message);
            }
        }
        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        protected void MostrarMensaje(string mensaje)
        {

            if (!mensaje.Equals("")) 
            {
                this.lblMsg.Text = mensaje;
            }

        }
    }
}