﻿#region ensamblado DT, Version=1.3.0.0, Culture=neutral, PublicKeyToken=null
// C:\Users\Luis Gonzalez\Documents\cigarrete\control_dtnds_foward\ctrldtnds\Source\Web\bin\DT.dll
// Decompiled with ICSharpCode.Decompiler 6.1.0.5902
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace DT
{
    public class DataTablesAux
    {
        private DbConnection db;

        private string dbEngine;

        public DataTablesAux(DbConnection db)
        {
            this.db = db;
            dbEngine = ((object)db).GetType().Name.Replace("Connection", "");
        }

        public DataTable Generar(Query query, int draw, int start, int length, Search search, List<Order> orders, List<Column> columns)
        {
            try
            {
                string qu3ry = query.GenerarConsulta();
                string text = AgregarBusqueda(qu3ry, columns, search);
                string query2 = AgregarLimit(text, start, length, orders, columns);
                Totals totals = ObtenerTotals(text);
                List<object> data = ExecutarConsulta(query2, query.select);
                return new DataTable(draw, data, totals.total, totals.total);
            }
            catch (Exception ex)
            {
                return ObtenerDataTableVacia(ex.ToString(), draw);
            }
        }

        public static DataTable ObtenerDataTableVacia(string error, int draw)
        {
            return new DataTable(draw, null, 0, 0, error);
        }

        private string AgregarBusqueda(string query, List<Column> columns, Search search)
        {
            StringBuilder stringBuilder = new StringBuilder(query);
            if (!string.IsNullOrEmpty(search.value))
            {
                stringBuilder.Append(" WHERE ");
                int num = 0;
                List<string> source = new List<string>
                {
                    "Lectura",
                    "Escritura",
                    "Eliminar"
                };
                foreach (Column column in columns)
                {
                    string campo = sanitizeColumn(column.data, useAlias: true);
                    if (!source.Any((string s) => campo.Contains(s)))
                    {
                        stringBuilder.AppendFormat(" {0} {1} LIKE '%{2}%'", (num == 0) ? "" : "OR", campo, search.value);
                        num++;
                    }
                }
            }

            return stringBuilder.ToString();
        }

        private string AgregarLimit(string query, int start, int length, List<Order> orders, List<Column> columns)
       {
            StringBuilder stringBuilder = new StringBuilder("");
            StringBuilder stringBuilder2 = new StringBuilder("");
            int num = 0;
            foreach (Order order in orders)
            {
                string arg = sanitizeColumn(columns[order.column].data);
                stringBuilder2.AppendFormat("{0}{1} {2}", (num > 0) ? "," : "", arg, order.dir);
                num++;
            }

            string arg2 = (stringBuilder2.ToString() != "") ? stringBuilder2.ToString() : (sanitizeColumn(columns[0].data) + " ASC");
            string a = dbEngine;
            if (!(a == "MySql"))
            {
                if (!(a == "Sql"))
                {
                    throw new Exception("DataTables no está programado para trabajar con esta conexión de base de datos.");
                }

                stringBuilder.AppendFormat("WITH DataTable AS (SELECT ROW_NUMBER() OVER (ORDER BY {0}) RowNumber,* FROM (", arg2);
                stringBuilder.Append(query);
                stringBuilder.AppendFormat(")) SELECT * FROM DataTable WHERE RowNumber BETWEEN {0} AND {1}", start + 1, start + length);
            }
            else
            {
                stringBuilder.Append(query);
                stringBuilder.AppendFormat(" ORDER BY {0} ", arg2);
                stringBuilder.AppendFormat(" LIMIT {0},{1} ", start, length);
            }

            return stringBuilder.ToString();
        }

        private List<object> ExecutarConsulta(string query, List<string> select)
        {
            DbCommand dbCommand = db.CreateCommand();
            dbCommand.CommandTimeout = 1000;
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = query;
            db.Open();
            DbDataReader dbDataReader = dbCommand.ExecuteReader();
            List<object> list = new List<object>();
            using (dbDataReader)
            {
                while (dbDataReader.Read())
                {
                    IDictionary<string, object> dictionary = new ExpandoObject();
                    foreach (string item2 in select)
                    {
                        string text = "";
                        if (item2.Contains("+") || item2.Contains("-") || item2.Contains("*") || item2.Contains("/") || item2.Contains("stuff((") || item2.Contains("(SELECT "))
                        {
                            string[] array = item2.Split(' ');
                            text = array[array.Length - 1];
                        }
                        else
                        {
                            string[] array2 = item2.Split('.');
                            text = array2[(array2.Length != 1) ? 1 : 0];
                            if (text.Contains(" "))
                            {
                                string[] array3 = text.Split(' ');
                                text = array3[array3.Length - 1];
                            }
                        }

                        dictionary.Add(text, dbDataReader[text]);
                    }

                    Dictionary<string, object> item = dictionary.ToDictionary((KeyValuePair<string, object> x) => x.Key, (KeyValuePair<string, object> x) => x.Value);
                    list.Add(item);
                }
            }

            db.Close();
            return list;
        }

        // remove => string consultaTotal
        private Totals ObtenerTotals(string consultaFiltered)
        {
            DbCommand dbCommand = db.CreateCommand();
            dbCommand.CommandTimeout = 1000;
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = "SELECT (SELECT COUNT(*) FROM(" + consultaFiltered + ")a)Total"; //, (SELECT COUNT(*) FROM(" + consultaFiltered + ")b)Filtered
            db.Open();
            DbDataReader dbDataReader = dbCommand.ExecuteReader();
            Totals totals = new Totals();
            using (dbDataReader)
            {
                if (dbDataReader.Read())
                {
                    totals.total = int.Parse(dbDataReader["Total"].ToString());
                    //totals.filtered = int.Parse(dbDataReader["Filtered"].ToString());
                }
            }

            db.Close();
            return totals;
        }

        private string sanitizeColumn(string campo, bool useAlias = false, bool withTable = false)
        {
            string text2;
            string text3;
            if (campo.Contains(" "))
            {
                string[] array = campo.Split(' ');
                if (campo.Contains("stuff(("))
                {
                    string text = string.Empty;
                    for (int i = 0; i < array.Length - 1; i++)
                    {
                        text = text + " " + array[i];
                    }

                    text2 = text;
                    text3 = array[array.Count() - 1];
                }
                else
                {
                    text2 = array[0];
                    text3 = array[1];
                }
            }
            else
            {
                text2 = campo;
                string[] array2 = campo.Split('.');
                text3 = array2[array2.Count() - 1];
            }

            if (useAlias)
            {
                text2 = text3;
            }
            else if (!withTable)
            {
                string[] array3 = text2.Split('.');
                text2 = array3[array3.Count() - 1];
            }

            return text2;
        }
    }
    class Totals
    {
        public int filtered;

        public int total;
    }
}
