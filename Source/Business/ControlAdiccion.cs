﻿using DataAccess.Adiccion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlAdiccion
    {
        private static ServicioAdiccion servicio = new ServicioAdiccion("SQLConnectionString");

        public static int Guardar(Entity.Adiccion item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.Adiccion item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.Adiccion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Adiccion> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Adiccion ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.Adiccion ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static Entity.Adiccion ObtenerPorAdiccion(Entity.Adiccion item)
        {
            return servicio.ObtenerByAdiccion(item);
        }
    }
}
