﻿using DataAccess.EventosDT;
using System;
using System.Collections.Generic;

namespace Business
{
    public class ControlEventosDT
    {
        private static ServicioEventosDT servicio = new ServicioEventosDT("SQLConnectionString");

        public static List<Entity.EventosDT> ObtenerTodos(object[] data)
        {
            return servicio.ObtenerTodos(data);
        }
    }
}
