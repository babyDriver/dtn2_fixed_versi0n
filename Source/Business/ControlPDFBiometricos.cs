using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Linq;

namespace Business
{
    public class ControlPDFBiometricos
    {
        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }


        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }



        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {


                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                  //  cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));

                    /*
                    string marcadeagua = "               0165-15072020-0945           ";
                    //float positionx = writer.PageSize.Right/2;
                    //float positiony = writer.PageSize.Top / 2;
                    float fontsize = 15f;
                    float rotation = 0f;
                    float positionx = 100;
                    float positiony = 100;

                    BaseFont bf2 = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
                    cb.BeginText();
                    cb.SetColorFill(BaseColor.LIGHT_GRAY);
                    cb.SetFontAndSize(bf2, fontsize);
                  
                    int xx; int y;

                    for (xx = 0; xx < 3; xx++)
                    {
                        for (y = 0; y < 15; y++)
                        {
                            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, marcadeagua, positionx, positiony, rotation);
                            positiony = positiony + 100;
                        }
                        positionx = positionx + 200;
                        positiony = 100;
                    }

                    cb.EndText();
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));*/
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                string dia = String.Format("{0:dd}", DateTime.Now);
                string anio = String.Format("{0:yyyy}", DateTime.Now);
                string hora = String.Format("{0:HH:mm}", DateTime.Now);
                string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }


            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        public static object[] generarReporte(Entity.Fichabiometrico fichabiometrico)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Ficha_Biometrico" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.FichaBiometricos"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try {                
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)

                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();

                TituloFichaBiometrico(documentoPdf, FuenteTitulo, FuenteTituloTabla, fichabiometrico, blau);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }
        private static void TituloFichaBiometrico(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla, Entity.Fichabiometrico fichabiometrico, BaseColor blau)
        {
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1);
            string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font titlefont = new Font(calibrifont, 12, Font.BOLD);
            Font FuenteNegrita = new Font(calibrifont, 10f, Font.BOLD, blau);
            Font negritabold = new Font(calibrifont, 10f, Font.BOLD);
            Font FuenteNormal = new Font(calibrifont, 10f, Font.NORMAL);
            Font bigfont = new Font(calibrifont, 14, Font.BOLD, blau);
            Font smallfont = new Font(calibrifont, 8, Font.NORMAL);
            Font bigtitle = new Font(calibrifont, 14, Font.BOLD, blau);
            Font boldfont = new Font(calibrifont, 10f, Font.BOLD, blau);
            Font fuente = new Font(calibrifont, 10f, Font.BOLD);

            var detalledetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(fichabiometrico.DetenidoId).LastOrDefault();
            var infoDetencion = ControlInformacionDeDetencion.ObtenerPorInternoId(fichabiometrico.DetenidoId);
            var evento = ControlEvento.ObtenerById(infoDetencion.IdEvento);
            var eventodetenido = ControlDetenidoEvento.ObtenerPorId(infoDetencion.IdDetenido_Evento);
            var eventoWS = ControlWSAEvento.ObtenerPorId(evento.IdEventoWS);
            //var contrato = ControlSubcontrato.ObtenerPorId(detalledetencion.ContratoId);
            //var institucion = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);

            string seniasparticulares = "";

            try {
                var seniales = ControlSenal.ObteneTodos();
                foreach (var item in seniales.Where(r => r.DetenidoId == fichabiometrico.DetenidoId && r.Habilitado == 1 && r.Activo))
                {
                    var tiposenial = ControlCatalogo.Obtener(item.Tipo, Convert.ToInt32(Entity.TipoDeCatalogo.tipo_senal));
                    seniasparticulares += tiposenial.Nombre + " " + item.Descripcion + " cantidad " + item.Cantidad.ToString() + Environment.NewLine;
                }
            }
            catch (Exception ex) { seniasparticulares = "Sin señas particulares"; }

            var institucion = ControlInstitucion.ObtenerPorId(detalledetencion.CentroId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);

            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);

            var logo = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logo != "undefined" && logo != "")
            {
                logotipo = logo;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }

            string nombreEstado = "Estado";
            string nombreMunicipio = "Municipio";
            string nombreCentro = institucion.Nombre;

            if (domicilio != null)
            {
                Entity.Estado estado = ControlEstado.ObtenerPorId(Convert.ToInt32(domicilio.EstadoId));

                if (estado != null)
                {
                    nombreEstado = estado.Nombre;
                    Entity.Municipio municipio = ControlMunicipio.Obtener(Convert.ToInt32(domicilio.MunicipioId));
                    if (municipio != null) nombreMunicipio = municipio.Nombre;
                }
            }


            int[] w = new int[] { 2, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            string title = "Ficha biométricos";
            string lugar = nombreCentro;
            PdfPCell cellWithRowspan = new PdfPCell(image, true);
            // The first cell spans 5 rows  
            cellWithRowspan.HorizontalAlignment = Element.ALIGN_CENTER;
            cellWithRowspan.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellWithRowspan.Rowspan = 5;
            cellWithRowspan.Border = 0;
            table.AddCell(cellWithRowspan);

            cell = new PdfPCell(new Phrase(nombreEstado + ", " + nombreMunicipio + ", " + nombreCentro, bigtitle));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(title, bigtitle));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            int[] w2 = new int[] { 30, 50 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;
            BaseColor colorrelleno2 = new BaseColor(128, 192, 255);
            BaseColor color = new BaseColor(System.Drawing.Color.White);
            CeldaConChunk(tabla, boldfont, fuente, "Delegación: ", lugar, 0, 0, 0);
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1);

            PdfPTable tablay = new PdfPTable(2);//Numero de columnas de la tabla            
            tablay.SetWidths(new float[] { 60, 40 }); //Porcentaje ancho
            tablay.HorizontalAlignment = Element.ALIGN_LEFT;//Alineación vertical

            string tieneHIT = "";
            int[] filtro = { detalledetencion.ContratoId, detalledetencion.DetenidoId };
            var hit = ControlCrossreference.GetCrossReferences(filtro);
            if (hit.Count > 0) tieneHIT = "Si";
            else tieneHIT = "No";

            PdfPTable tabladet = new PdfPTable(2);
            tabladet.SetWidthPercentage(new float[] { 58,42 }, PageSize.LETTER);
            var fecha1 = "";
            if(detalledetencion.Fecha !=null)
            {
                var f = Convert.ToDateTime(detalledetencion.Fecha);
                var h1 = f.Hour.ToString();
                if (f.Hour < 10) h1 = "0"+h1;
                var m1 = f.Minute.ToString();
                if (f.Minute < 10) m1 = "0" + m1;
                var s1 = f.Second.ToString();
                if (f.Second < 10) s1 = "0" + s1;
                fecha1 = f.ToShortDateString() + " " + h1 + ":" + m1 + ":" + s1;
            }
            CeldaParaTablas(tabladet, FuenteNormal, "Fecha de registro", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fecha1, 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "HIT", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, tieneHIT, 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tabladet, FuenteNormal, "Apellidos", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Apellidos, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Apellidos", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Apellidos, 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Nombre", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Nombre, 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Alias", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Alias!=null?fichabiometrico.Alias:"", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Sexo", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Sexo != null ? fichabiometrico.Sexo : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Estatura", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Estatura.ToString()!="0"? fichabiometrico.Estatura.ToString():"", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Fecha de nacimiento", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Fechanacimiento != DateTime.MinValue ? fichabiometrico.Fechanacimiento.ToShortDateString() : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Nacionalidad", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Nacionalidad != null ? fichabiometrico.Nacionalidad : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Ocupación", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Ocupacion != null ? fichabiometrico.Ocupacion : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Escolaridad", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Escolaridad != null ? fichabiometrico.Escolaridad : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Estado civil", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Estadocivil != null ? fichabiometrico.Estadocivil : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Lengua nativa", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Lenguanativa != null ? fichabiometrico.Lenguanativa : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Etnia", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, fichabiometrico.Etnia != null ? fichabiometrico.Etnia : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, "Domicilio (Municipio, Estado)", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tabladet, FuenteNormal, (fichabiometrico.Municipio + ", " + fichabiometrico.Estado).Trim()==","?"Sin dato": fichabiometrico.Municipio + ", " + fichabiometrico.Estado, 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            CeldaVacio(tabladet, 2);
            PdfPCell celda_ = new PdfPCell(tabladet);
            celda_.Border = 0;

            tablay.AddCell(celda_);

            var huellas = ControlHuella.ObtenerPorDetenidoId(fichabiometrico.DetenidoId);

            var biometricos = ControlBiometricos.GetByDetenidoId(fichabiometrico.DetenidoId);

           

            string pathfotofrontal = System.Web.HttpContext.Current.Server.MapPath(biometricos.FirstOrDefault(r=>r.Clave== "Face_0.jpg").Rutaimagen);
            string pathfotoPerfilDerecho = System.Web.HttpContext.Current.Server.MapPath(biometricos.FirstOrDefault(r => r.Clave == "Face_1.jpg").Rutaimagen);
            string pathfotoPerfilIzquierdo = System.Web.HttpContext.Current.Server.MapPath(biometricos.FirstOrDefault(r => r.Clave == "Face_2.jpg").Rutaimagen);
            iTextSharp.text.Image imagenfrontal = null;
            iTextSharp.text.Image imagenPerfilDerecho = null;
            iTextSharp.text.Image imagenPerfilIzquierdo = null;
            try
            {
                imagenfrontal = iTextSharp.text.Image.GetInstance(pathfotofrontal);
            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imagenfrontal = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            try
            {
                imagenPerfilDerecho = iTextSharp.text.Image.GetInstance(pathfotoPerfilDerecho);
            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imagenPerfilDerecho = iTextSharp.text.Image.GetInstance(pathfoto);
            }
            try
            {
                imagenPerfilIzquierdo = iTextSharp.text.Image.GetInstance(pathfotoPerfilIzquierdo);
            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imagenPerfilIzquierdo = iTextSharp.text.Image.GetInstance(pathfoto);
            }

            float width = imagenfrontal.Width;
            
            PdfPTable tablan = new PdfPTable(1);//Numero de columnas de la tabla   

            //
            PdfPTable tablaPerfiles = new PdfPTable(2);
            tablaPerfiles.DefaultCell.FixedHeight = 100f;
            PdfPCell perfilDerecho = new PdfPCell(imagenPerfilDerecho, true);
            PdfPCell perfilIZquierdo = new PdfPCell(imagenPerfilIzquierdo, true);
            tablaPerfiles.AddCell(perfilDerecho);
            tablaPerfiles.AddCell(perfilIZquierdo);
            //
            PdfPCell cellWithRowspanif = new PdfPCell(imagenfrontal, true);
            cellWithRowspanif.Image.ScaleAbsolute(50, 50);

            width = 50;
            //// The first cell spans 5 rows  
            //cellWithRowspanif.HorizontalAlignment = Element.ALIGN_CENTER;
            //cellWithRowspanif.VerticalAlignment = Element.ALIGN_MIDDLE;
            //cellWithRowspanif.Rowspan = 1;
            //cellWithRowspanif.Border = 0;
            //cellWithRowspanif.Image.ScaleAbsolute(25, 25);
            //cellWithRowspanif.Image.ScaleToFitHeight=true;


            cellWithRowspanif.AddElement(imagenfrontal);
            //Now find the Image element in the cell and resize it
            foreach (IElement element in cellWithRowspanif.CompositeElements)
            {
                //The inserted image is stored in a PdfPTable, so when you find 
                //the table element just set the table width with the image width, and lock it.
                PdfPTable tblImg = element as PdfPTable;
                if (tblImg != null)
                {
                    tblImg.TotalWidth = width;
               
                    tblImg.LockedWidth = true;
                }

                cellWithRowspanif.Image.ScaleToFitHeight = false;
                tablan.AddCell(cellWithRowspanif);
                tablan.AddCell(tablaPerfiles);
            }
            tablan.WidthPercentage = 25;
            tablay.AddCell(tablan);
     


            //CeldaParaTablas(tablay, FuenteNormal, "fotos", 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);


            //CeldaParaTablas(tablay, FuenteTituloTabla, "Remisión", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
            //CeldaParaTablas(tablay, FuenteTituloTabla, "Detenido", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);


            // Celda(tabla, boldfont, "Fecha del:" + Fecha[0].ToShortDateString() + " al " + Fecha[1].ToShortDateString(), 1, 0, 0, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);

            documentoPdf.Add(table);
            documentoPdf.Add(tabla);

            PdfPTable tablachunk = new PdfPTable(1);
            tablachunk.WidthPercentage = 100;
            tablachunk.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaParaTablas(tablachunk, FuenteTituloTabla, "Datos generales", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_CENTER, Element.ALIGN_LEFT);
            //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);
       
            documentoPdf.Add(tablachunk);
            documentoPdf.Add(tablay);

            tablachunk = new PdfPTable(1);
            tablachunk.WidthPercentage = 100;
            tablachunk.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaVacio(tablachunk, 1);
            CeldaParaTablas(tablachunk, FuenteTituloTabla, "Datos de asignación", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_CENTER, Element.ALIGN_LEFT);
            //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);
            
            documentoPdf.Add(tablachunk);
            PdfPTable tablaz = new PdfPTable(4);//Numero de columnas de la tabla 
            //float[] wd = new float[] {7,12,4,8 };
            //tablaz.SetWidths(wd);
            tablaz.WidthPercentage = 100; //Porcentaje ancho
            //tablaz.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            var idsUsuarios = biometricos.Select(x => x.Creadopor).Distinct();
            var nombreUsuarios = "";
            foreach (var idUsuario in idsUsuarios)
            {
                var usuario = ControlUsuario.Obtener(idUsuario);
                if (usuario != null)
                {
                    nombreUsuarios += usuario.Nombre + " " + usuario.ApellidoPaterno + " " + usuario.ApellidoMaterno + ", ";
                }
            }
            nombreUsuarios = nombreUsuarios.Substring(0, nombreUsuarios.Length - 2);

            CeldaParaTablas(tablaz, FuenteNormal, "Usuario de registro", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, nombreUsuarios, 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "No. remisión", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, fichabiometrico.Remision != null ? fichabiometrico.Remision : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);


            CeldaParaTablas(tablaz, FuenteNormal, "Institución", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, institucion.Nombre, 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "Folio", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal,  eventoWS != null ? eventoWS.Folio : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaz, 4);
            documentoPdf.Add(tablaz);
            tablachunk = new PdfPTable(1);
            tablachunk.WidthPercentage = 100;
            tablachunk.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaParaTablas(tablachunk, FuenteTituloTabla, "Datos de identificación", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_CENTER, Element.ALIGN_LEFT);

            //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);

            documentoPdf.Add(tablachunk);
            tablaz = new PdfPTable(4);//Numero de columnas de la tabla            
            tablaz.WidthPercentage = 100; //Porcentaje ancho
            tablaz.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tablaz, FuenteNormal, "CURP", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, fichabiometrico.CURP!=null?fichabiometrico.CURP:"", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "RFC", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, fichabiometrico.RFC != null ? fichabiometrico.RFC : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "Señas particulares", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            Celda(tablaz, FuenteNormal, seniasparticulares, 3, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaz, 4);
            documentoPdf.Add(tablaz);
            tablachunk = new PdfPTable(1);
            tablachunk.WidthPercentage = 100;
            tablachunk.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaParaTablas(tablachunk, FuenteTituloTabla, "Referencias personales", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_CENTER, Element.ALIGN_LEFT);

            //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);

            documentoPdf.Add(tablachunk);
            tablaz = new PdfPTable(4);//Numero de columnas de la tabla            
            tablaz.WidthPercentage = 100; //Porcentaje ancho
            tablaz.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical

            CeldaParaTablas(tablaz, FuenteNormal, "Nombre a quien se notifica:", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, infoDetencion.Personanotifica!=null?infoDetencion.Personanotifica.ToString():"", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "Celular:", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, infoDetencion.Celular != null ? infoDetencion.Celular : "", 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaVacio(tablaz, 4);
            documentoPdf.Add(tablaz);

            tablachunk = new PdfPTable(1);
            tablachunk.WidthPercentage = 100;
            tablachunk.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaParaTablas(tablachunk, FuenteTituloTabla, "Informe de detención", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_CENTER, Element.ALIGN_LEFT);

            //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);

            documentoPdf.Add(tablachunk);
            tablaz = new PdfPTable(4);//Numero de columnas de la tabla            
            tablaz.WidthPercentage = 100; //Porcentaje ancho
            tablaz.HorizontalAlignment = Element.ALIGN_CENTER;//Alineación vertical
            var h2 = infoDetencion.HoraYFecha.Hour.ToString();
            if (infoDetencion.HoraYFecha.Hour < 10) h2 = "0" + h2;
            var m2 = infoDetencion.HoraYFecha.Minute.ToString();
            if (infoDetencion.HoraYFecha.Minute < 10) m2 = "0" + m2;
            var s2 = infoDetencion.HoraYFecha.Second.ToString();
            if (infoDetencion.HoraYFecha.Second < 10) s2 = "0" + s2;
            var fecha = infoDetencion.HoraYFecha.ToShortDateString() + " " + h2 + ":" + m2 + ":" + s2;
            CeldaParaTablas(tablaz, FuenteNormal, "Fecha y hora:", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, fecha, 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "Evento:", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, evento.Folio.ToString(), 1, 0, 1, color, espacio, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "Descripción del evento:", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            Celda(tablaz, FuenteNormal, infoDetencion.Motivo, 3, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "Detalle/trayectoria unidad:", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            Celda(tablaz, FuenteNormal, infoDetencion.Descripcion, 3, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            CeldaParaTablas(tablaz, FuenteNormal, "Lugar de detención:", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            Celda(tablaz, FuenteNormal, infoDetencion.LugarDetencion, 3, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            var motivo = "";

            try {           
                var m = ControlWSAMotivo.ObtenerPorId(eventodetenido.MotivoId);
                if (m != null) motivo = m.NombreMotivoLlamada;
            }
            catch (Exception) { motivo = "Sin motivo"; }                            
            
            CeldaParaTablas(tablaz, FuenteNormal, "Motivo:", 1, 0, 1, color, espacio, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE);
            Celda(tablaz, FuenteNormal, motivo, 3, 0, 1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
            documentoPdf.Add(tablaz);

            documentoPdf.NewPage();

            tablachunk = new PdfPTable(1);
            tablachunk.WidthPercentage = 100;
            tablachunk.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaParaTablas(tablachunk, FuenteTituloTabla, "Biométricos (iris)", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_CENTER, Element.ALIGN_LEFT);

            //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);

            documentoPdf.Add(tablachunk);
            PdfPTable tablairis = new PdfPTable(2);//Numero de columnas de la tabla            
            tablairis.WidthPercentage = 70; //Porcentaje ancho
            tablairis.HorizontalAlignment = Element.ALIGN_LEFT;//Alineación vertical
            //int u = 1;
            var irisDerecho = biometricos.FirstOrDefault(j => j.Clave == "Iris_2.jpg");
            var irisIzquierdo = biometricos.FirstOrDefault(j => j.Clave == "Iris_1.jpg");
            List<Entity.Biometrico> irisList = new List<Entity.Biometrico>()
            {
                irisDerecho ?? new Entity.Biometrico(),
                irisIzquierdo ?? new Entity.Biometrico()
            };
            string[] nombreIris = new string[]
            {
                "Iris derecho",
                "Iris izquierdo"
            };
            for (int i = 0; i < irisList.Count; i++)
            {
                CeldaParaTablas(tablairis, FuenteNormal, nombreIris[i]/* +" "+ u.ToString()*/, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                //u++;
                string pathfotofrontal2 = System.Web.HttpContext.Current.Server.MapPath(irisList[i].Rutaimagen);
                iTextSharp.text.Image iris = null;
                try
                {
                    iris = iTextSharp.text.Image.GetInstance(pathfotofrontal2);
                }
                catch (Exception)
                {
                    pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    iris = iTextSharp.text.Image.GetInstance(pathfoto);
                }

                float width2 = iris.Width;

                PdfPTable tablan2 = new PdfPTable(1);//Numero de columnas de la tabla   

                PdfPCell cellWithRowspanif2 = new PdfPCell(iris, true);
                cellWithRowspanif2.Image.ScaleAbsolute(50, 50);
                width = 50;
                //// The first cell spans 5 rows  
                //cellWithRowspanif.HorizontalAlignment = Element.ALIGN_CENTER;
                //cellWithRowspanif.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cellWithRowspanif.Rowspan = 1;
                //cellWithRowspanif.Border = 0;
                //cellWithRowspanif.Image.ScaleAbsolute(25, 25);
                //cellWithRowspanif.Image.ScaleToFitHeight=true;

                cellWithRowspanif2.AddElement(iris);
                //Now find the Image element in the cell and resize it
                foreach (IElement element in cellWithRowspanif2.CompositeElements)
                {
                    //The inserted image is stored in a PdfPTable, so when you find 
                    //the table element just set the table width with the image width, and lock it.
                    PdfPTable tblImg = element as PdfPTable;
                    if (tblImg != null)
                    {
                        tblImg.TotalWidth = width;

                        tblImg.LockedWidth = true;
                    }
                    tablan2.AddCell(cellWithRowspanif2);
                }
                tablan2.WidthPercentage = 50;
                tablairis.AddCell(tablan2);
            }

            CeldaVacio(tablairis, 2);
            documentoPdf.Add(tablairis);

            //huellas
            tablachunk = new PdfPTable(1);
            tablachunk.WidthPercentage = 100;
            tablachunk.HorizontalAlignment = Element.ALIGN_CENTER;
            CeldaParaTablas(tablachunk, FuenteTituloTabla, "Biométricos (huellas)", 1, 0, 1, colorrelleno2, espacio, Element.ALIGN_CENTER, Element.ALIGN_LEFT);
            CeldaVacio(tablachunk, 1);
            //CeldaConChunk(tablachunk, FuenteNegrita, FuenteNormal, "", detalle.Motivodetencion.ToString(), 0, 0, 0);

            documentoPdf.Add(tablachunk);
            PdfPTable tablahuellas = new PdfPTable(4);//Numero de columnas de la tabla            
            //tablahuellas.WidthPercentage = 50; //Porcentaje ancho
            tablahuellas.SetWidths(new float[] { 30, 20, 30, 20 });
            tablahuellas.HorizontalAlignment = Element.ALIGN_LEFT;//Alineación vertical
            tablahuellas.DefaultCell.FixedHeight = 95f;
            //int uu = 1;
            List<Entity.Biometrico> huellasBiometricos = new List<Entity.Biometrico>();
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_1.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_6.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_2.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_7.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_3.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_8.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_4.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_9.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_5.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_0_10.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_1.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_6.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_2.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_7.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_3.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_8.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_4.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_9.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_5.bmp")));
            huellasBiometricos.Add(biometricos.FirstOrDefault(h => h.Clave == ("Finger_1_10.bmp")));
            string[] nombreHuellas = new string[]
            {
                "Pulgar derecho plana",
                "Pulgar izquierdo plana",
                "Indice derecho plana",
                "Indice izquierdo plana",
                "Medio derecho plana",
                "Medio izquierdo plana",
                "Anular derecho plana",
                "Anular izquierdo plana",
                "Meñique derecho plana",
                "Meñique izquierdo plana",
                "Pulgar derecho rolada",
                "Pulgar izquierdo rolada",
                "Indice derecho rolada",
                "Indice izquierdo rolada",
                "Medio derecho rolada",
                "Medio izquierdo rolada",
                "Anular derecho rolada",
                "Anular izquierdo rolada",
                "Meñique derecho rolada",
                "Meñique izquierdo rolada"
            };
            for (int i = 0; i < huellasBiometricos.Count; i++)
            {
                CeldaParaTablas(tablahuellas, FuenteNormal, nombreHuellas[i] /*+ " " + u.ToString()*/, 1, 0, 1, color, espacio, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE);
                //uu++;
                string pathfotofrontal2 = "";
                 
                iTextSharp.text.Image iris = null;
                try
                {
                    pathfotofrontal2 = System.Web.HttpContext.Current.Server.MapPath(huellasBiometricos[i].Rutaimagen);
                    iris = iTextSharp.text.Image.GetInstance(pathfotofrontal2);


                }
                catch (Exception)
                {
                    pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                    iris = iTextSharp.text.Image.GetInstance(pathfoto);

                }
                iris.ScaleAbsolute(50, 50);

                PdfPTable tablan2 = new PdfPTable(1);//Numero de columnas de la tabla   

                PdfPCell cellWithRowspanif2 = new PdfPCell(iris, true);
                cellWithRowspanif2.Image.ScaleAbsolute(50, 50);
                width = 50;
                //// The first cell spans 5 rows  
                //cellWithRowspanif.HorizontalAlignment = Element.ALIGN_CENTER;
                //cellWithRowspanif.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cellWithRowspanif.Rowspan = 1;
                //cellWithRowspanif.Border = 0;
                //cellWithRowspanif.Image.ScaleAbsolute(25, 25);
                //cellWithRowspanif.Image.ScaleToFitHeight=true;

                cellWithRowspanif2.AddElement(iris);
                //Now find the Image element in the cell and resize it
                foreach (IElement element in cellWithRowspanif2.CompositeElements)
                {
                    //The inserted image is stored in a PdfPTable, so when you find 
                    //the table element just set the table width with the image width, and lock it.
                    PdfPTable tblImg = element as PdfPTable;
                    if (tblImg != null)
                    {
                        tblImg.TotalWidth = width;

                        tblImg.LockedWidth = true;
                    }

                    tablan2.AddCell(cellWithRowspanif2);
                }
                tablan2.WidthPercentage = 50;
                tablahuellas.AddCell(tablan2);
                CeldaVacio(tablan2, 6);
            }
            foreach (var item in huellasBiometricos)
            {
                
            }
            documentoPdf.Add(tablahuellas);
        }

    }
}
