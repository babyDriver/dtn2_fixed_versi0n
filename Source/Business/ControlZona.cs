﻿using DataAccess.Zona;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Business
{
    public static class ControlZona
    {
        private static ServicioZona servicio = new ServicioZona("SQLConnectionString");

        public static Entity.Zona Obtener(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static Entity.Zona Obtener(System.Guid trackingId)
        {
            return servicio.ObtenerPorTrackingId(trackingId);
        }

        public static List<Entity.Zona> Obtener(bool activos)
        {
            return servicio.ObtenerActivos(activos);
        }

        public static List<Entity.Zona> ObtenerPorRolTipoContrato(string rol, int zona, int empresaid, int tipocontratoid)
        {
            List<Entity.Zona> zonas = new List<Entity.Zona>();
            int zonaid = 0;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString);
            try
            {
                SqlCommand command = new SqlCommand("[Zona_GetByRolTipoContrato_SP]", sqlConnection);
                command.CommandTimeout = 0;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Rol", rol);
                command.Parameters.AddWithValue("@ZonaId", zona);
                command.Parameters.AddWithValue("@EmpresaId", empresaid);
                command.Parameters.AddWithValue("@TipoContratoId", tipocontratoid);
                SqlDataReader dr;
                sqlConnection.Open();
                dr = command.ExecuteReader();
                using (dr)
                {
                    while (dr.Read())
                    {
                        zonaid = Convert.ToInt32(dr["ZonaId"]);
                        zonas.Add(ControlZona.Obtener(zonaid));
                    }
                }
                return zonas;
            }
            catch (Exception ex) { return null; }
            finally { if (sqlConnection.State == ConnectionState.Open) sqlConnection.Close(); }
        }

    }
}