﻿using DataAccess.DevolucionPertenencia;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDevolucionPertenencia
    {
        private static ServicioDevolucionPertenencia servicio = new ServicioDevolucionPertenencia("SQLConnectionString");

        public static int Guardar(Entity.DevolucionPertenencia devolucion)
        {
            return servicio.Guardar(devolucion);
        }

        public static Entity.DevolucionPertenencia ObtenerByTrackingId(Guid guid)
        {
            return servicio.ObtenerByTrackingId(guid);
        }
    }
}
