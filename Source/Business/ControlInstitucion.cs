﻿using DataAccess.Institucion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlInstitucion
    {
        private static ServicioInstitucion servicio = new ServicioInstitucion("SQLConnectionString");

        public static int Guardar(Entity.Institucion item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.Institucion item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.Institucion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.Institucion> ObtenerPorEstadoId(int estadoid)
        {
            return servicio.ObtenerByEstadoId(estadoid);
        }

        public static Entity.Institucion ObtenerPorNombre(string nombre)
        {
            return servicio.ObtenerByNombre(nombre);
        }

        public static List<Entity.Institucion> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.Institucion ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }
    }
}
