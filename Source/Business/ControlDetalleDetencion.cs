﻿using DataAccess.DetalleDetencion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlDetalleDetencion
    {
        private static ServicioDetalleDetencion servicio = new ServicioDetalleDetencion("SQLConnectionString");

        public static int Guardar(Entity.DetalleDetencion estatus)
        {
            return servicio.Guardar(estatus);
        }
        public static List<Entity.DetalleDetencion> ObtenerPorParametro(string[] filtro)
        {
            return servicio.ObtenerporParametros(filtro);
        }
        public static void Actualizar(Entity.DetalleDetencion estatus)
        {
            servicio.Actualizar(estatus);
        }

        public static void ActualizarActivos(Entity.DetalleDetencion estatus)
        {
            servicio.ActualizarActivos(estatus);
        }

        public static Entity.DetalleDetencion ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.DetalleDetencion> ObteneTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.DetalleDetencion ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static List<Entity.DetalleDetencion> ObtenerPorDetenidoId(int id)
        {
            return servicio.ObtenerByDetenidoId(id);
        }

        public static Entity.DetalleDetencion ObtenerPorDetenidoIdActivo(string[] id)
        {
            return servicio.ObtenerByDetenidoIdActivo(id);
        }

        public static Entity.DetalleDetencion ObtenerPorExpediente(string expediente)
        {
            return servicio.ObtenerByExpediente(expediente);
        }
        public static List<Entity.DetalleDetencion> ObtenertodossibExamenmedico()
        {
            return servicio.ObtenerTodossinExamenMedico();
        }
    }
}
