﻿using DataAccess.CertificadoMedicoPsicoFisiologico;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCertificadoPsicoFisiologico
    {
        private static ServicioCertificadoMedicoPsicoFisiologico servicio = new ServicioCertificadoMedicoPsicoFisiologico("SQLConnectionString");

        public static void Actualizar(Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsicoFisiologico)
        {
            servicio.Actualizar(certificadoMedicoPsicoFisiologico);

        }
        public static int Guardar(Entity.CertificadoMedicoPsicoFisiologico certificadoMedicoPsicoFisiologico)
        {

            return servicio.Guardar(certificadoMedicoPsicoFisiologico);
        }

        public static List<Entity.CertificadoMedicoPsicoFisiologico> ObTenerTodo()
        {
            return servicio.ObtenerTodos();

        }

        public static Entity.CertificadoMedicoPsicoFisiologico ObtenerPorId(int Id)
        {
            return servicio.ObtenerById(Id);
        }

        public static Entity.CertificadoMedicoPsicoFisiologico ObtenerPorTrackingId(string TrackingId)
        {
            return servicio.ObtenerByTrackingId(TrackingId);
        }
    }
}
