﻿using DataAccess.ContratoUsuario;
using System.Collections.Generic;
using System;
using Entity;

namespace Business
{
    public class ControlContratoUsuario
    {
        private static ServicioContratoUsuario servicio = new ServicioContratoUsuario("SQLConnectionString");

        public static int Guardar(ContratoUsuario contratoUsuario)
        {
            return servicio.Guardar(contratoUsuario);
        }

        public static List<Entity.ContratoUsuario> ObtenerPorUsuarioId(int id)
        {
            return servicio.ObtenerByUsuarioId(id);
        }

        public static Entity.ContratoUsuario ObtenerPorContratoYUsuario(object[] parametros)
        {
            return servicio.ObtenerByContratoAndUsuario(parametros);
        }

        public static void Actualizar(Entity.ContratoUsuario contratoUsuario)
        {
            servicio.Actualizar(contratoUsuario);
        }

        public static Entity.ContratoUsuario ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id) != null ? servicio.ObtenerById(id):new Entity.ContratoUsuario() ;
        }
    }
}
