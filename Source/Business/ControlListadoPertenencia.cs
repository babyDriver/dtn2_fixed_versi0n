﻿using DataAccess.ListadoPertenencia;
using System.Collections.Generic;
using System;
using Entity;

namespace Business
{
    public class ControlListadoPertenencia
    {
        private static ServicioListadoPertenencia servicio = new ServicioListadoPertenencia("SQLConnectionString");

        public static List<Entity.ListadoPertenencia> ObtenerTodos(object[] parametros)
        {
            return servicio.ObtenerTodos(parametros);
        }

        public static List<Entity.ListadoPertenencia> ObtenerTodosConPertenencias(object[] parametros)
        {
            return servicio.ObtenerTodosConPertenencias(parametros);
        }

        public static List<Entity.ListadoPertenencia> ObtenerDetenidosSinPertenencias(object[] parametros)
        {
            return servicio.ObtenerDetenidosSinPertenencias(parametros);
        }

        public static List<Entity.ListadoPertenencia> ObtenerDetenidosConPertenencias(object[] parametros)
        {
            return servicio.ObtenerDetenidosConPertenencias(parametros);
        }

        public static List<Entity.ListadoPertenencia> ObtenerLibresConPertenencias(object[] parametros)
        {
            return servicio.ObtenerLibresConPertenencias(parametros);
        }
    }
}
