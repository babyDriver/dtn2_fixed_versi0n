﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Linq;

namespace Business
{
    public class ControlPDFReporteAgrupadoDetenidos
    {
        //Crear directorio 
        private static string CrearDirectorioControl(string nombre)
        {
            try
            {

                //Verificar si existe el directorio principal del repositorio de documentos
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio de la obra existe
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs"))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs")));

                //Verificar si el directorio para el grupo documental existe, sino crearlo
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre))))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs/" + nombre)));


                return string.Concat(ConfigurationManager.AppSettings["relativepath"], "/Application/Docs//" + nombre);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("No fue posible crear el repositorio para crear el archivo. ", ex.Message));
            }
        }


        //Crear celdas
        public static void Celda(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas para tablas
        public static void CeldaParaTablas(PdfPTable tabla, Font Fuente, string titulo, int colspan, int bordo, int cantidad, BaseColor colorrelleno, float espacio, int horizontal, int vertical)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(titulo, Fuente));
                celda.Colspan = colspan;
                celda.BorderWidth = bordo;
                celda.HorizontalAlignment = horizontal;
                celda.VerticalAlignment = vertical;
                celda.BackgroundColor = colorrelleno;
                celda.Padding = espacio;
                celda.UseAscender = true;
                tabla.AddCell(celda);
            }

        }

        //Crear celdas vacias
        public static void CeldaVacio(PdfPTable tabla, int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                PdfPCell celda;
                celda = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                celda.BorderWidth = 0;
                tabla.AddCell(celda);
            }

        }



        //para crear el Header y Footer
        public partial class HeaderFooter : PdfPageEventHelper
        {
            PdfContentByte cb;
            PdfTemplate headerTemplate;
            BaseFont bf = null;
            float tamanofuente = 8f;

            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                try
                {
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                    cb = writer.DirectContent;
                    headerTemplate = cb.CreateTemplate(100, 100);
                }
                catch (DocumentException de) { }
                catch (System.IO.IOException ioe) { }
            }

            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                //Encabezado de Página
                Rectangle pageSize = doc.PageSize;
                PdfPTable footerTbl = new PdfPTable(1);
                BaseColor blau = new BaseColor(0, 61, 122);

                float[] TablaAncho = new float[] { 100 };
                footerTbl.SetWidths(TablaAncho);
                footerTbl.TotalWidth = 550;
                Font FuenteCelda = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                PdfPCell cell;
                CeldaParaTablas(footerTbl, FuenteCelda, " ", 1, 0, 1, blau, 0, Element.ALIGN_TOP, Element.ALIGN_TOP);


                string pagina = "Página " + writer.PageNumber + " de ";
                //Necesario para agregar pagina x de y al en footer con GetBottom si es en el header se usa gettop
                {
                    float margenbottom = 11f;
                    float margenright = 280f;
                    cb.BeginText();
                    cb.SetFontAndSize(bf, tamanofuente);
                    cb.SetTextMatrix(doc.PageSize.GetBottom(margenright), doc.PageSize.GetBottom(margenbottom));
                    cb.ShowText(pagina);
                    cb.EndText();
                    float len = bf.GetWidthPoint(pagina, tamanofuente);
                    cb.AddTemplate(headerTemplate, doc.PageSize.GetBottom(margenright) + len, doc.PageSize.GetBottom(margenbottom));
                }

                PdfPTable footerTbl2 = new PdfPTable(2);


                float[] TablaAncho2 = new float[] { 100, 100 };
                footerTbl2.SetWidths(TablaAncho2);
                footerTbl2.TotalWidth = doc.Right - doc.Left;
                Font FuenteCelda2 = FontFactory.GetFont(FontFactory.HELVETICA, tamanofuente, Font.NORMAL);

                //string dia = String.Format("{0:dd}", DateTime.Now);
                //string anio = String.Format("{0:yyyy}", DateTime.Now);
                //string hora = String.Format("{0:HH:mm}", DateTime.Now);
                //string mestexto = ObtenerMesTexto(String.Format("{0:MM}", DateTime.Now));
                //string fechahora = dia + " de " + mestexto + " de " + anio + " " + hora;
                //Chunk chkFecha = new Chunk(fechahora, FuenteCelda);
                Chunk chkFecha = new Chunk("", FuenteCelda);
                Phrase phrfecha = new Phrase(chkFecha);

                //Phrase promad = new Phrase("Promad Soluciones Computarizadas " + anio, FuenteCelda2);
                Phrase promad = new Phrase("", FuenteCelda2);
                cell = new PdfPCell(promad);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);

                cell = new PdfPCell(phrfecha);
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.BorderWidthLeft = 0;
                cell.BorderWidthRight = 0;
                cell.BorderWidthBottom = 0;
                cell.BorderWidthTop = 2;
                cell.BorderColorTop = blau;
                footerTbl2.AddCell(cell);
                footerTbl2.WriteSelectedRows(0, -1, 5, 25, writer.DirectContent);
            }

            private static string ObtenerMesTexto(string mes)
            {
                string mestexto = "";
                switch (mes)
                {
                    case "01":
                        mestexto = "Enero";
                        break;
                    case "02":
                        mestexto = "Febrero";
                        break;
                    case "03":
                        mestexto = "Marzo";
                        break;
                    case "04":
                        mestexto = "Abril";
                        break;
                    case "05":
                        mestexto = "Mayo";
                        break;
                    case "06":
                        mestexto = "Junio";
                        break;
                    case "07":
                        mestexto = "Julio";
                        break;
                    case "08":
                        mestexto = "Agosto";
                        break;
                    case "09":
                        mestexto = "Septiembre";
                        break;
                    case "10":
                        mestexto = "Octubre";
                        break;
                    case "11":
                        mestexto = "Noviembre";
                        break;
                    case "12":
                        mestexto = "Diciembre";
                        break;
                    default:
                        mestexto = " ";
                        break;
                }
                return mestexto;
            }


            //Necesario para incluir pagina x de y al header
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
                headerTemplate.BeginText();
                headerTemplate.SetFontAndSize(bf, tamanofuente);
                headerTemplate.SetTextMatrix(0, 0);
                headerTemplate.ShowText((writer.PageNumber - 1).ToString());
                headerTemplate.EndText();

            }
        }

        public static void CeldaConChunk(PdfPTable tabla, Font FuenteTitulo, Font FuenteValor, string titulo, string valor, int bordo, int horizontal, int vertical)
        {
            PdfPCell celda;
            Chunk chunk = new Chunk(titulo, FuenteTitulo);
            Chunk chunk2 = new Chunk(valor, FuenteValor);
            Paragraph elements = new Paragraph();
            elements.Add(chunk);
            elements.Add(chunk2);
            celda = new PdfPCell(elements);
            celda.BorderWidth = bordo;
            celda.HorizontalAlignment = horizontal;
            celda.VerticalAlignment = vertical;
            tabla.AddCell(celda);
        }

        public static object[] generarReporteAgrupados(List<Entity.AgrupadoReporte> agrupados, DateTime[] Fecha)
        {
            object obj = null;
            var margen = iTextSharp.text.Utilities.MillimetersToPoints(5); //5 milimetros para los margenes
            var espacio = iTextSharp.text.Utilities.MillimetersToPoints(1); //1 milimetro
            Document documentoPdf = new Document(PageSize.LETTER, margen, margen, margen, margen);
            iTextSharp.text.pdf.PdfWriter PdfWriter = null;

            try
            {
                string nombrearchivo = "Reporte_agrupados_" + DateTime.Now.ToString("yyMMddHHmmss") + ".pdf";
                //crear el directorio pasando el nombre de la carpeta
                string directorio = CrearDirectorioControl("01.Agrupados"); directorio = directorio.Replace("//", "/");
                string ubicacionarchivo = directorio + "/" + nombrearchivo;
                string patharchivo = System.Web.HttpContext.Current.Server.MapPath(directorio);
                string urlArchivo = patharchivo + "\\" + nombrearchivo;

                try
                {
                    var Archivo_PDF = new FileInfo(urlArchivo);
                    if (Archivo_PDF.Exists) Archivo_PDF.Delete();
                }
                catch (Exception ex)
                {
                    return new object[] { false, ubicacionarchivo, "El archivo está siendo utilizado por un usuario y no se puede tener acceso para su eliminación. Verifica que el archivo no este abierto." };
                }

                //Definición de fuentes
                BaseColor blau = new BaseColor(0, 61, 122);
                string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
                BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
                Font FuenteNegrita = new Font(fuentecalibri, 10f, Font.BOLD);
                Font FuenteNormal = new Font(fuentecalibri, 10f, Font.NORMAL);
                Font FuenteNegritaBlanca = new Font(fuentecalibri, 10f, Font.BOLD, BaseColor.WHITE);
                Font FuenteTitulo = new Font(fuentecalibri, 18f, Font.BOLD, blau);
                Font FuenteNegrita12 = new Font(fuentecalibri, 14f, Font.BOLD);
                Font FuenteTituloTabla = new Font(fuentecalibri, 13f, Font.BOLD, BaseColor.WHITE);

                try
                {
                    PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(documentoPdf, new FileStream(urlArchivo, FileMode.Create));
                }
                catch (Exception ex) { return new object[] { false, "", ex.Message }; }

                PdfWriter.PageEvent = new HeaderFooter();
                documentoPdf.Open();
               
                TituloAgrupado(documentoPdf, FuenteTitulo, FuenteTituloTabla,  agrupados, blau,Fecha,FuenteNormal);

                return new object[] { true, ubicacionarchivo, "" };
            }
            catch (Exception ex)
            {
                return new object[] { false, "", ex.Message };
            }
            finally
            {
                if (documentoPdf.IsOpen())
                {
                    documentoPdf.Close();
                    PdfWriter.Close();
                }
            }
        }
        private static void TituloAgrupado(Document documentoPdf, Font FuenteTitulo, Font FuenteTituloTabla,  List<Entity.AgrupadoReporte>agrupados, BaseColor blau, DateTime[] Fecha, Font FuenteNormal)
        {
            var space = iTextSharp.text.Utilities.MillimetersToPoints(1);
            string pathfont = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont calibrifont = BaseFont.CreateFont(pathfont, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font titlefont = new Font(calibrifont, 12, Font.BOLD);
            Font FuenteNegrita = new Font(calibrifont, 10f, Font.BOLD, blau);
            Font negritabold = new Font(calibrifont, 10f, Font.BOLD);
           
            Font bigfont = new Font(calibrifont, 14, Font.BOLD, blau);
            Font smallfont = new Font(calibrifont, 8, Font.NORMAL);
            Font bigtitle = new Font(calibrifont, 14, Font.BOLD, blau);
            Font boldfont = new Font(calibrifont, 10f, Font.BOLD, blau);
            Font Normal= new Font(calibrifont, 10f, Font.NORMAL, BaseColor.BLACK);
            Font fuente = new Font(calibrifont, 10f, Font.BOLD);
            string[] id = new string[] {agrupados.LastOrDefault().DetenidoorignalId.ToString(),"true" };
            var detalledetencion = ControlDetalleDetencion.ObtenerPorDetenidoIdActivo(id);
            if(detalledetencion==null)
            {
                detalledetencion = ControlDetalleDetencion.ObtenerPorDetenidoId(agrupados.LastOrDefault().DetenidoorignalId).LastOrDefault();

            }
            var contratoUsuario = ControlContratoUsuario.ObtenerPorId(Convert.ToInt32(HttpContext.Current.Session["numeroContrato"].ToString()));
            var subcontrato = ControlSubcontrato.ObtenerPorId(contratoUsuario.IdContrato);
            var contrato = ControlContrato.ObtenerPorId(contratoUsuario.IdContrato);

            var institucion = ControlInstitucion.ObtenerPorId(contrato.InstitucionId);
            Entity.Domicilio domicilio = ControlDomicilio.ObtenerPorId(institucion.DomicilioId);
            var x = ControlSubcontrato.ObtenerByContratoId(institucion.ContratoId);

            

            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.HorizontalAlignment = Element.ALIGN_CENTER;

            string escudo = "~/Content/img/Escudo.jpg";
            string logo = "~/Content/img/logo.png";

            var logoizq = subcontrato.Banner;
            if (logoizq != "undefined" && !string.IsNullOrEmpty(logoizq))
            {
                logo = logoizq;
            }

            string pathEscudo = System.Web.HttpContext.Current.Server.MapPath(escudo);
            string pathLogo = System.Web.HttpContext.Current.Server.MapPath(logo);
            iTextSharp.text.Image imageEscudo = null;
            iTextSharp.text.Image imageLogo = null;
            try
            {
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }
            catch (Exception)
            {
                pathEscudo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageEscudo = iTextSharp.text.Image.GetInstance(pathEscudo);
            }

            try
            {
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            catch (Exception)
            {
                pathLogo = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                imageLogo = iTextSharp.text.Image.GetInstance(pathLogo);
            }
            var logoDER = subcontrato.Logotipo;
            string logotipo = "~/Content/img/logo.png";

            if (logoDER != "undefined" && logo != "")
            {
                logotipo = logoDER;
            }

            string pathfoto = System.Web.HttpContext.Current.Server.MapPath(logotipo);
            iTextSharp.text.Image image = null;
            try
            {
                image = iTextSharp.text.Image.GetInstance(pathfoto);


            }
            catch (Exception)
            {
                pathfoto = System.Web.HttpContext.Current.Server.MapPath("~/Content/img/logo.png");
                image = iTextSharp.text.Image.GetInstance(pathfoto);

            }
            imageLogo.ScalePercent(95f);
            
                imageLogo.ScaleAbsoluteHeight(100f);
                imageLogo.ScaleAbsoluteWidth(300f);
            
            image.ScaleAbsoluteHeight(60f);
            image.ScaleAbsoluteWidth(70f); 
            Paragraph para = new Paragraph();
            Phrase ph1 = new Phrase();
            Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(imageLogo, 0, 0, true));
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(image, 0, 15, true));
            main.Add(ph1);
            para.Add(main);
            documentoPdf.Add(para);

            string nombreEstado = "Estado";
            string nombreMunicipio = "Municipio";
            string nombreCentro = institucion.Nombre;

            if (domicilio != null)
            {
                Entity.Estado estado = ControlEstado.ObtenerPorId(Convert.ToInt32(domicilio.EstadoId));

                if (estado != null)
                {
                    nombreEstado = estado.Nombre;
                    Entity.Municipio municipio = ControlMunicipio.Obtener(Convert.ToInt32(domicilio.MunicipioId));
                    if (municipio != null) nombreMunicipio = municipio.Nombre;
                }
            }


            int[] w = new int[] { 8, 8 };
            PdfPTable table = new PdfPTable(2);
            table.SetWidths(w);
            table.TotalWidth = 550;
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            PdfPCell cell;

            string title = "Juzgado Calificador";
            string lugar = nombreCentro;
            

            cell = new PdfPCell(new Phrase("Ayuntamiento de " + nombreMunicipio + "," + nombreEstado, bigtitle));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(title, bigtitle));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(nombreMunicipio.ToUpper() + "," + nombreEstado.ToUpper(), titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("Agrupación de detenidos", titlefont));
            cell.BorderWidth = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingLeft = 10;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.BorderWidth = 0;
            cell.Padding = space;
            table.AddCell(cell);

            int[] w2 = new int[] { 30, 50 };
            PdfPTable tabla = new PdfPTable(2);
            tabla.SetWidths(w2);
            tabla.WidthPercentage = 100;

            string rutafuente = System.Web.HttpContext.Current.Server.MapPath("~/Content/fonts/calibril.ttf");
            BaseFont fuentecalibri = BaseFont.CreateFont(rutafuente, BaseFont.CP1252, BaseFont.EMBEDDED);
            Font FuenteNormal2 = new Font(fuentecalibri, 12f, Font.NORMAL);
            Font FuenteNegrita2 = new Font(fuentecalibri, 12f, Font.BOLD, blau);

            CeldaConChunk(tabla, FuenteNegrita2, FuenteNormal2, "Delegación: ", lugar, 0, 0, 0);
            cell = new PdfPCell(new Phrase(" ", boldfont));
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidth = 0;
            cell.Padding = space;
            tabla.AddCell(cell);
            CeldaVacio(tabla, 1);

           // Celda(tabla, boldfont, "Fecha del:" + Fecha[0].ToShortDateString() + " al " + Fecha[1].ToShortDateString(), 1, 0, 0, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE);
          
            documentoPdf.Add(table);
            documentoPdf.Add(tabla);
            int[] w3 = new int[] { 100,100 };
            PdfPTable tabla3 = new PdfPTable(2);
            tabla3.SetWidths(w3);
            tabla3.WidthPercentage = 100;
            //BaseColor blau = new BaseColor(0, 61, 122);
          

            CeldaConChunk(tabla3, FuenteNegrita2, FuenteNormal2, "Del: ", Fecha[0].ToShortDateString(), 0, 0, 0);
            CeldaVacio(tabla3, 1);
            CeldaConChunk(tabla3, FuenteNegrita2, FuenteNormal2, " Al: ", Fecha[1].ToShortDateString(), 0, 0, 0);
            CeldaConChunk(tabla3, FuenteNegrita2, FuenteNormal2, "Registros encontrados: ", agrupados.Count().ToString(), 0, Element.ALIGN_RIGHT, 0);
            

            tabla3.AddCell(cell);
            documentoPdf.Add(tabla3);
            Font blanca = new Font(calibrifont, 12, Font.BOLD, BaseColor.WHITE);
            float[] width = new float[] {2,2,1,2,3};
            PdfPTable _table = new PdfPTable(5);
            _table.WidthPercentage = 100;
            _table.SetWidths(width);

            PdfPCell celda_;

            celda_ = new PdfPCell(new Phrase("Original", blanca));
            celda_.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda_.Colspan = 1;
            celda_.Border = 0;
            celda_.BackgroundColor = new BaseColor(0, 61, 122);
            celda_.VerticalAlignment = Element.ALIGN_LEFT;
            _table.AddCell(celda_);

            celda_ = new PdfPCell(new Phrase(" ", blanca));
            celda_.HorizontalAlignment = Element.ALIGN_CENTER;
            celda_.Colspan = 2;
            celda_.Border = 0;
            celda_.BackgroundColor = new BaseColor(0, 61, 122);
            celda_.VerticalAlignment = Element.ALIGN_LEFT;
            _table.AddCell(celda_); 

            celda_ = new PdfPCell(new Phrase("Agrupado", blanca));
            celda_.HorizontalAlignment = Element.ALIGN_RIGHT;
            celda_.Colspan = 1;
            celda_.Border = 0;
            celda_.BackgroundColor = new BaseColor(0, 61, 122);
            celda_.VerticalAlignment = Element.ALIGN_LEFT;
            _table.AddCell(celda_);
            celda_ = new PdfPCell(new Phrase("", blanca));
            celda_.HorizontalAlignment = Element.ALIGN_CENTER;
            celda_.Colspan = 1;
            celda_.Border = 0;
            celda_.BackgroundColor = new BaseColor(0, 61, 122);
            celda_.VerticalAlignment = Element.ALIGN_LEFT;
            _table.AddCell(celda_);
            celda_ = new PdfPCell(new Phrase("No. remisión", blanca));
            
            celda_.Border = 0;
            celda_.HorizontalAlignment = Element.ALIGN_LEFT;
            celda_.BackgroundColor = new BaseColor(0, 61, 122);
            celda_.VerticalAlignment = Element.ALIGN_LEFT;
            _table.AddCell(celda_);

            celda_ = new PdfPCell(new Phrase("Nombre", blanca));
            celda_.Colspan = 2;
            celda_.Border = 0;
            celda_.HorizontalAlignment = Element.ALIGN_LEFT;
            celda_.BackgroundColor = new BaseColor(0, 61, 122);
            celda_.VerticalAlignment = Element.ALIGN_LEFT;
            _table.AddCell(celda_);

            //CeldaVacio(table, 2);
            celda_ = new PdfPCell(new Phrase("No. remisión", blanca));

            celda_.Border = 0;
            celda_.HorizontalAlignment = Element.ALIGN_LEFT;
            celda_.BackgroundColor = new BaseColor(0, 61, 122);
            celda_.VerticalAlignment = Element.ALIGN_LEFT;
            _table.AddCell(celda_);

            celda_ = new PdfPCell(new Phrase("Nombre", blanca));
            celda_.HorizontalAlignment = Element.ALIGN_LEFT;
            
            celda_.Border = 0;
            celda_.BackgroundColor = new BaseColor(0, 61, 122);
            celda_.VerticalAlignment = Element.ALIGN_LEFT;
            _table.AddCell(celda_);

            List<int> ListaOriginalId = new List<int>();

            foreach (var item in agrupados)
            {
                ListaOriginalId.Add(item.DetenidoorignalId);
            }
            int contador = 0;
            foreach (int item in ListaOriginalId.Distinct())
            {

                string Original = "";
                string originalexpediente = "";
                foreach (var agrupado in agrupados.Where(y=> y.DetenidoorignalId==item))
                {
                    Original = agrupado.Detenidooriginal;
                    originalexpediente= agrupado.Expedienteoriginal;
                }

                BaseColor colorgris = new BaseColor(246, 243, 242);
                BaseColor color = colorgris;
                PdfPCell bottom = new PdfPCell(new Phrase(originalexpediente,FuenteNormal));
            
                bottom.Border = 0;
                if (contador % 2 == 0)
                {
                    bottom.BackgroundColor = colorgris;
                }

                //bottom.BackgroundColor=
                _table.AddCell(bottom);

                bottom = new PdfPCell(new Phrase(Original,FuenteNormal));
                bottom.Colspan = 2;
                bottom.Border = 0;
                if (contador % 2 == 0)
                {
                    bottom.BackgroundColor = colorgris;
                }

                //bottom.BackgroundColor=
                _table.AddCell(bottom);
                PdfPTable nested = new PdfPTable(1);
                
                foreach (var detenido in agrupados.Where(y=> y.DetenidoorignalId==item))
                {
                    PdfPCell cell_ = new PdfPCell(new Phrase(detenido.Expediente,FuenteNormal));
                    cell_.Border = 0;
                    nested.AddCell(cell_);
                    
                  
                }

                PdfPCell nesthousing = new PdfPCell(nested);
                nesthousing.Border = 0;
                nesthousing.Padding = 0f;
                if (contador % 2 == 0)
                {
                    nesthousing.BackgroundColor = colorgris;
                }
                _table.AddCell(nesthousing);
                PdfPTable nested2 = new PdfPTable(1);
                foreach (var detenido in agrupados.Where(y => y.DetenidoorignalId == item))
                {
                    PdfPCell cell_ = new PdfPCell(new Phrase(detenido.Detenidoagrupado,FuenteNormal ));
                    cell_.Border = 0;
                    nested2.AddCell(cell_);


                }
                nesthousing = new PdfPCell(nested2);
                nesthousing.Border = 0;
                nesthousing.Padding = 0f;
                if (contador % 2 == 0)
                {
                    nesthousing.BackgroundColor = colorgris;
                }
                _table.AddCell(nesthousing);
                // table.WidthPercentage = 100;

                contador ++;
            }
            documentoPdf.Add(_table);
        }


    }
}
