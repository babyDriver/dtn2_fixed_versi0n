﻿using DataAccess.AdministracionExamenMedico;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlAdministracionExamenMedico
    {
        private static ServicioAdministracionExamenMedico servicio = new ServicioAdministracionExamenMedico("SQLConnectionString");

        public static int Guardar(Entity.AdministracionExamenMedico administracionExamenMedico)
        {
            return servicio.Guardar(administracionExamenMedico);
        }
        public static Entity.AdministracionExamenMedico obtenerPorDetalledetencion(int DetalledetencionId)
            {
            return servicio.ObtenerPorDetalledetencionID(DetalledetencionId);
            }

    }
}
