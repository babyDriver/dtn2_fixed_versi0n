﻿using DataAccess.ReporteIngresos;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlReporteIngresos
    {
        public static ServicioReporteIngresos servicio = new ServicioReporteIngresos("SQLConnectionString");

        public static List<Entity.ReporteIngresos>GetListado(string[] fechas)
        {
            return servicio.GetListByDates(fechas);
        }
    }
}
