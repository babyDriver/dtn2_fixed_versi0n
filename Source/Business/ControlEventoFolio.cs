﻿using DataAccess.EventoFolio;

namespace Business
{
    public class ControlEventoFolio
    {
        private static ServicioEventoFolio servicio = new ServicioEventoFolio("SQLConnectionString");

        public static Entity.EventoFolio ObtenerPorContratoId(int id)
        {
            return servicio.ObtenerPorContratoId(id);
        }
    }
}
