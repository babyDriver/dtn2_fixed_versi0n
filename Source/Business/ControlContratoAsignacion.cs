﻿using DataAccess.ContratoAsignacion;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlContratoAsignacion
    {
        private static ServicioContratoAsignacion servicio = new ServicioContratoAsignacion("SQLConnectionString");

        public static List<Entity.ContratoAsignacion> ObtenerPorIdUsuario(int id)
        {
            return servicio.ObtenerByIdUsuario(id);
        }

        public static List<object> GetContratos(int id)
        {
            List<object> lista = new List<object> { };

            try
            {
                var listaTemp = ObtenerPorIdUsuario(id);

                foreach (var contrato in listaTemp)
                {
                    object contratoTmp = new
                    {                        
                        ContratoId = contrato.Id,
                        Nombre = contrato.Contrato,
                        VigenciaInicial = contrato.VigenciaInicial.ToShortDateString(),
                        VigenciaFinal = contrato.VigenciaFinal.ToShortDateString(),
                        UsuarioId = contrato.IdUsuario
                    };

                    lista.Add(contratoTmp);
                }

                return lista;
            }
            catch (System.Exception ex)
            {
                return null;
            }
        }
    }
}
