﻿using DataAccess.DatosDetenido;
using System.Collections.Generic;
using System;

namespace Business
{
   public class ControlDatosDetenido
    {
        private static ServicioDatosDetenido servicio = new ServicioDatosDetenido("SQLConnectionString");

        public Entity.DatosDetenido GetByDetalleDetencionId(int detalledetencionid)
        {
            return servicio.GetDatosDetenido(detalledetencionid);

        }
    }
}
