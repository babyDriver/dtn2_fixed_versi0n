﻿using DataAccess.AdicionalExamenMedico;
using System.Collections.Generic;

namespace Business
{
    public class ControlAdicionalExamenMedico
    {
        private static ServicioAdicionalExamenMedico servicio = new ServicioAdicionalExamenMedico("SQLConnectionString");

        public static int Guardar(Entity.AdicionalExamenMedico item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.AdicionalExamenMedico item)
        {
            servicio.Actualizar(item);
        }

        public static List<Entity.AdicionalExamenMedico> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.AdicionalExamenMedico ObtenerPorId(Entity.AdicionalExamenMedico item)
        {
               return servicio.ObtenerById(item);
        }

        public static Entity.AdicionalExamenMedico ObtenerPorTrackingId(Entity.AdicionalExamenMedico item)
        {
            return servicio.ObtenerByTrackingId(item);
        }

        public static List<Entity.AdicionalExamenMedico> ObtenerPorExamenMedicoId(Entity.AdicionalExamenMedico item)
        {
            return servicio.ObtenerByExamenMedicoId(item);
        }

    }
}
