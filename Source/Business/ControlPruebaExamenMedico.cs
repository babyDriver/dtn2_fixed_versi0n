﻿using DataAccess.PruebaExamenMedico;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlPruebaExamenMedico
    {
        private static ServicioPruebaExamenMedico servicio = new ServicioPruebaExamenMedico("SQLConnectionString");

        public static int Guardar(Entity.PruebaExamenMedico item)
        {
            return servicio.Guardar(item);
        }

        public static void Actualizar(Entity.PruebaExamenMedico item)
        {
            servicio.Actualizar(item);
        }

        public static Entity.PruebaExamenMedico ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

   
        public static Entity.PruebaExamenMedico ObtenerPorTrackingId(Guid trackingId)
        {
            return servicio.ObtenerByTrackingId(trackingId);
        }

        public static Entity.PruebaExamenMedico ObtenerPorExamenMedicoId(int id)
        {
            return servicio.ObtenerByExamenMedicoId(id);
        }

    }
}
