﻿using DataAccess.WSAColonia;
using System.Collections.Generic;

namespace Business
{
    public class ControlWSAColonia
    {
        private static ServicioWSAColonia servicio = new ServicioWSAColonia("SQLConnectionString");

        public static Entity.WSAColonia ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static List<Entity.WSAColonia> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static int Guardar(Entity.WSAColonia colonia)
        {
            return servicio.Guardar(colonia);
        }

        public static void Actualizar(Entity.WSAColonia colonia)
        {
            servicio.Actualizar(colonia);
        }
    }
}
