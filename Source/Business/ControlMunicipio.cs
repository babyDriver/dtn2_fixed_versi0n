﻿using DataAccess.Municipio;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public static class ControlMunicipio
    {
        private static ServicioMunicipio servicio = new ServicioMunicipio("SQLConnectionString");

        public static int Guardar(Entity.Municipio municipio)
        { 
            return servicio.Guardar(municipio);
        }
        public static void Actualizar(Entity.Municipio municipio)
        {
            servicio.Actualizar(municipio);
        }
        public static Entity.Municipio Obtener(int id)
        {
            return servicio.ObtenerPorId(id);
        }

        public static Entity.Municipio Obtener(System.Guid trackingId)
        {
            return servicio.ObtenerPorTrackingId(trackingId);
        }

        public static Entity.Municipio ObtenerPorClave(string cvegeo)
        {
            return servicio.ObtenerPorClave(cvegeo);
        }

        public static List<Entity.Municipio> ObtenerPorEstado(int estadoId)
        {
            return servicio.ObtenerPorEstadoId(estadoId).Where(x => x.Habilitado).ToList();
        }

        public static List<Entity.Municipio> ObtenerTodo()
        {
            return servicio.ObtenerTodos().Where(x => x.Habilitado).ToList();
        }

    }
}
