﻿using DataAccess.WSAUnidad;
using System.Collections.Generic;

namespace Business
{
    public class ControlWSAUnidad
    {
        private static ServicioWSAUnidad servicio = new ServicioWSAUnidad("SQLConnectionString");

        public static List<Entity.WSAUnidad> ObtenerTodos()
        {
            return servicio.ObtenerTodos();
        }

        public static Entity.WSAUnidad ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.WSAUnidad> ObtenerTodosPorIdInstitucion(int id)
        {
            return servicio.ObtenerByIdInstitucion(id);
        }

        public static int Guardar(Entity.WSAUnidad unidad)
        {
            return servicio.Guardar(unidad);
        }

        public static void Actualizar(Entity.WSAUnidad unidad)
        {
            servicio.Actualizar(unidad);
        }
    }
}
