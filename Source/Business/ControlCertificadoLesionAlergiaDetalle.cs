﻿using DataAccess.CertificadoLesionAlergiasDetalle;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlCertificadoLesionAlergiaDetalle
    {
        private static ServicioCertificadoLesionAlergiasDetalle servicio = new ServicioCertificadoLesionAlergiasDetalle("SQLConnectionString");


        public static int Guardar(Entity.CertificadoLesionAlergiaDetalle lesionDetalle)
        {

            return servicio.Guardar(lesionDetalle);
        }
        public static List<Entity.CertificadoLesionAlergiaDetalle> ObteneroPortipoLesion(int Id)
        {
            return servicio.ObtenerPorId(Id);
        }
    }
}
