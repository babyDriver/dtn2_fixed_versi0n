﻿using DataAccess.WSAEvtD;

namespace Business
{
    public class ControlWSAEvtD
	{
        private static ServicioWSAEvtD servicio = new ServicioWSAEvtD("SQLConnectionString");
        public static Entity.WSAEvtDtnd ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }
    }
}
