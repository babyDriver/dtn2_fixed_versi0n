﻿using DataAccess.Historial;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlHistorial
    {
        private static ServicioHistorial servicio = new ServicioHistorial("SQLConnectionString");

        public static int Guardar(Entity.Historial item)
        {
            return servicio.Guardar(item);
        }
        public static void Actualizar(Entity.Historial item)
        {
             servicio.Actualizar(item);
        }

        public static Entity.Historial ObtenerPorId(int id)
        {
            return servicio.ObtenerPorId(id);
        }
        public static List<Entity.Historial> ObtenerPorDetenido(int DetenidoId)
        {
            return servicio.ObtenerPorDetenidoId(DetenidoId);
        }
    }
}
