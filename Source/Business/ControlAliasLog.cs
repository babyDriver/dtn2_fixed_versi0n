﻿using DataAccess.AliasLog;
using System.Collections.Generic;
using System;

namespace Business
{
 public  class ControlAliasLog
    {
        private static ServicioAliasLog servicio = new ServicioAliasLog("SQLConnectionString");

        public static void Actualizar(Entity.AliasLog item)
        {
            servicio.Actualizar(item);
        }
        public static Entity.AliasLog ObtenerMovimientoLog(Entity.AliasLog filtro)
        {
            return servicio.Obtener(filtro);
        }
    }
}
