﻿using DataAccess.EstatusPertenencia;
using System.Collections.Generic;
using System;

namespace Business
{
    public class ControlEstatusPertenencia
    {
        private static ServicioEstatusPertenencia servicio = new ServicioEstatusPertenencia("SQLConnectionString");

        public static Entity.EstatusPertenencia ObtenerPorId(int id)
        {
            return servicio.ObtenerById(id);
        }

        public static List<Entity.EstatusPertenencia> ObtenerTodos()
        {
            return servicio.ObtenerTodo();
        }
    }
}
